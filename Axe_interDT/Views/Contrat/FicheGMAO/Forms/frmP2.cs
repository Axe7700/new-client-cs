﻿using Axe_interDT.Shared;
using Axe_interDT.Shared.FilterOfGrids;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Contrat.FicheGMAO.Forms
{
    public partial class frmP2 : Form
    {
        DataTable rsEQM_EquipementP2Imm;
        DataTable rsICA_ImmCategorieInterv;
        DataTable rsFAP_FacArticleP2;
        DataTable rsPEI_PeriodeGammeImm;
        DataTable rsGAI;
        ModAdo tmpAdorsGai = new ModAdo();
        ModAdo tmpAdorsICA_ImmCategorieInterv = new ModAdo();
        ModAdo tmpAdorsEQM_EquipementP2Imm = new ModAdo();
        ModAdo tmpAdorsPEI_PeriodeGammeImm = new ModAdo();
        int lICA;
        int lGAI;
        int lFAP_Article;
        int lEquip;


        const short cColWidth = 600;
        public frmP2()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_fGrilleInit()
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
            //fc_formatGrille ssChapTexte, , False, True
            // fc_formatGrille ssCSS_ContratSchap, , , True
            // fc_formatGrille ssICA_ImmCategorieInterv
            // fc_formatGrille ssEQU_EquipementP2Imm
            // fc_formatGrille ssCOS_ContratChap, Falsef
            // fc_formatGrille ssChapGamme, False
            // fc_formatGrille ssGammeArticle, False
            // fc_formatGrille ssFAP_FacArticleP2
            // fc_formatGrille ssGammePlanning, False
            // fc_formatGrille ssPEI_PeriodeGammeImm, , , True
            // fc_formatGrille ssChapMat
            //' fc_formatGrille ssMAT_MaterielP2
            // fc_formatGrille ssInterventionP2
            fc_DropJour();
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
            fc_DropMois();
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
            fc_DropSemaine();

            //    Screen.MousePointer = vbArrowHourglass
            //    fc_DropMatriculeType

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
            fc_DropIntervenant();
            //    Screen.MousePointer = vbArrowHourglass
            //    fc_DropTypeIntervenant

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
            fc_ssDropTPP_TypePeriodeP2();

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
            fc_DROP_CAI();

            fc_DROP_GAM();

            fc_DropPrestation();

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_DROP_CAI()
        {
            try
            {
                General.sSQL = "SELECT GAM_ID FROM GAM_Gamme where GAM_Code ='";
                if (ssGridGAI.ActiveRow != null)
                {
                    General.sSQL = General.sSQL + General.nz(ssGridGAI.ActiveRow.Cells["GAM_Code"].Value.ToString(), "XXXXX@@X2233") + "'";
                }
                else if (ssGridGAI.Rows.Count > 0)
                {
                    General.sSQL = General.sSQL + General.nz(ssGridGAI.Rows[0].Cells["GAM_Code"].Value.ToString(), "XXXXX@@X2233") + "'";
                }
                else
                {
                    General.sSQL = General.sSQL + "XXXXX@@X2233" + "'";
                }

                using (var tmpAdo = new ModAdo())
                    General.sSQL = tmpAdo.fc_ADOlibelle(General.sSQL);
                General.sSQL = "SELECT CAI_Code AS [Code], CAI_Libelle as [Libellé], CAI_NoAuto "
                    + " FROM CAI_CategoriInterv" + " WHERE GAM_ID =" + General.nz(General.sSQL, 0);
                sheridan.InitialiseCombo(this.ssDROP_CAI, General.sSQL, "Code", true);

                this.ssDROP_CAI.DisplayLayout.Bands[0].Columns["CAI_Noauto"].Hidden = true;
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_DROP_CAI");
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        private void fc_DROP_GAM()
        {
            General.sSQL = "SELECT  GAM_ID, GAM_Code as [Code], GAM_Libelle as [Libellé], GAM_Compteur, GAM_CompteurObli "
                + " FROM    GAM_Gamme" + " ORDER BY GAM_Code ";
            sheridan.InitialiseCombo(this.ssDropGAM, General.sSQL, "Code", true);
            this.ssDropGAM.DisplayLayout.Bands[0].Columns["GAM_ID"].Hidden = true;
            this.ssDropGAM.DisplayLayout.Bands[0].Columns["GAM_Compteur"].Hidden = true;
            this.ssDropGAM.DisplayLayout.Bands[0].Columns["GAM_CompteurObli"].Hidden = true;
           
            
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_ssDropTPP_TypePeriodeP2()
        {
            General.sSQL = "";
            General.sSQL = "SELECT  TPP_Code , TPP_JD, TPP_MD, TPP_JF, TPP_MF, SEM_Code  From TPP_TypePeriodeP2";
            if (ssDropTPP_TypePeriodeP2.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(this.ssDropTPP_TypePeriodeP2, General.sSQL, "TPP_Code", true);
            }
           // this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["TPP_Code"].ValueList = this.ssDropTPP_TypePeriodeP2;
            //verifier Me.ssDropTPP_TypePeriodeP2.ListWidthAutoSize = True
           // this.ssDropTPP_TypePeriodeP2.DisplayLayout.Override.ColumnAutoSizeMode = ColumnAutoSizeMode.VisibleRows;
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_DropJour()
        {
            string[] tabJour = null;
            int i = 0;
            tabJour = new string[31];
            for (i = 0; i <= 30; i++)
            {
                tabJour[i] = Convert.ToString(i + 1);
            }
            sheridan.AlimenteCmbAdditemTableau(ssDropJour, tabJour, "");
           // this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_JD"].ValueList = this.ssDropJour;
          //  this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_JF"].ValueList = this.ssDropJour;

        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_DropSemaine()
        {
            string[] tsSem = new string[8];
            tsSem[0] = "";
            tsSem[1] = modP2.cSDimanche;
            tsSem[2] = modP2.cSLundi;
            tsSem[3] = modP2.cSMardi;
            tsSem[4] = modP2.cSMercredi;
            tsSem[5] = modP2.cSJeudi;
            tsSem[6] = modP2.cSVendredi;
            tsSem[7] = modP2.cSSamedi;
            //-- dimanche est le premier jour de la semaine dans la fonction weekday par défaut.
            sheridan.AlimenteCmbAdditemTableau(ssDropSemaine, tsSem, "");
            ssDropSemaine.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Jours";
          //  this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["SEM_Code"].ValueList = this.ssDropSemaine;
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_DropMois()
        {
            string[] tabJour = null;
            int i = 0;
            tabJour = new string[12];
            for (i = 0; i <= 11; i++)
            {
                tabJour[i] = Convert.ToString(i + 1);
            }
            sheridan.AlimenteCmbAdditemTableau(ssDropMois, tabJour, "");
           // this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_MD"].ValueList = this.ssDropMois;
           // this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_MF"].ValueList = this.ssDropMois;
        }


        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lCop_NoAuto"></param>
        private void fc_Gamme(int lCop_NoAuto)
        {
            string sSql = null;
            double dbSomme = 0;
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
            sSql = "SELECT GAi_ID, GAM_Code, GAI_LIbelle, PDAB_Noauto, GAI_Select, GAI_NePasPlanifier,"
                + " COP_Noauto,GAI_Compteur,GAI_CompteurObli, GAI_NoLigne, CodeImmeuble "
                + " From GAI_GammeImm  where COP_NoAuto = '" + General.nz(lCop_NoAuto, 0) + "'";
            try
            {
                if (General.sPriseEnCompteLocalP2 == "1")
                {
                    sSql = sSql + " AND PDAB_Noauto =" + txtPDAB_Noauto.Text + "";
                }

                sSql = sSql + "  ORDER BY GAI_NoLigne";
                rsGAI = new DataTable();

                rsGAI = tmpAdorsGai.fc_OpenRecordSet(sSql, ssGridGAI, "GAi_ID");
                //ssICA_ImmCategorieInterv.Visible = False
                lGAI = 0;
                //ssICA_ImmCategorieInterv.Enabled = True
                //ssGridGAI.ReBind();
                ssGridGAI.DataSource = rsGAI;        
                ssGridGAI.UpdateData();
                lGAI = 0;
                //TODO VERIFIER CETTE BOUCLE
                if (rsGAI.Rows.Count > 0)
                {
                    //_with1.MoveFirst();
                    foreach (DataRow rsGaiRow in rsGAI.Rows)
                    {
                        lGAI = lGAI + 1;
                        rsGaiRow["GAI_NoLigne"] = lGAI;
                        tmpAdorsGai.Update();
                    }
                    lGAI = rsGAI.Rows.Count;
                    // _with1.MoveFirst();
                }

                //lblniv0 = clibNiv0 & "=> "   '& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text
                //txtNiv0 = ssGridGAI.Columns("GAI_LIbelle").Text

                //ssICA_ImmCategorieInterv.Visible = True
                //lblNiv1 = clibNiv1 & "=> " '& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text
                //txtNiv1 = ssICA_ImmCategorieInterv.Columns("cai_libelle").Text

                //lblNiv2 = cLIbNiv2 & "=> " '& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text
                //txtNiv2 = ssICA_ImmCategorieInterv.Columns("cai_libelle").Text

                lblNiv0.Text = modP2.clibNiv0;
                //& "=> "  '& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text
                //txtNiv0 = ssICA_ImmCategorieInterv.Columns("cai_libelle").Text

                //lblNiv1 = clibNiv1 & "=> " '& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text
                lblNiv1.Text = modP2.clibNiv1 + " sur " + modP2.clibNiv0 + "=> ";
                //& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text
                //txtNiv1 = ssICA_ImmCategorieInterv.Columns("cai_libelle").Text
                if (ssGridGAI.Rows.Count > 0)
                {
                    txtNiv1.Text = ssGridGAI.Rows[0].Cells["GAI_LIbelle"].Value.ToString();

                }
                else
                {
                    txtNiv1.Text = "";
                }

                //lblNiv2 = cLIbNiv2 & "=> " '& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text
                lblNiv2.Text = modP2.cLIbNiv2 + " sur " + modP2.clibNiv1 + "=> ";
                //& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text

                if (ssICA_ImmCategorieInterv.Rows.Count > 0)
                {
                    lblNiv2.Text = ssICA_ImmCategorieInterv.Rows[0].Cells["cai_libelle"].Value.ToString();
                }
                else
                {
                    lblNiv2.Text = "";
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Gamme");
            }
        }


        /// <summary>
        /// tested
        /// </summary>
        private void fc_ICA_ImmCategorieInterv()

        {

            double dbSomme = 0;
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
            General.sSQL = "SELECT     ICC_Noauto, ICC_Select, CAI_Code, CAI_Libelle, CodeImmeuble,"
                + " ICC_Cout, COP_Noauto,  ICC_NoLigne, ICC_Affiche, COS_NoAuto ,PDAB_Noauto, GAi_ID "
                + " FROM  ICC_IntervCategorieContrat";

            //    & " where COP_NoAuto = '" & nz(lCop_NoAuto, 0) & "'"  

            if (ssGridGAI.ActiveRow != null)
            {
                General.sSQL = General.sSQL + " WHERE GAi_ID =" + General.nz(ssGridGAI.ActiveRow.Cells["GAi_ID"].Value, 0);

            }
            else if (ssGridGAI.Rows.Count > 0)
            {
                General.sSQL = General.sSQL + " WHERE GAi_ID =" + General.nz(ssGridGAI.Rows[0].Cells["GAi_ID"].Value, 0);

            }
            else
            {
                General.sSQL = General.sSQL + " WHERE GAi_ID =0";

            }

            if (General.sPriseEnCompteLocalP2 == "1")
            {
                General.sSQL = General.sSQL + " AND PDAB_Noauto =" + txtPDAB_Noauto.Text + "";
            }

            General.sSQL = General.sSQL + "  ORDER BY ICC_NoLigne";

            rsICA_ImmCategorieInterv = new DataTable();
         
            rsICA_ImmCategorieInterv = tmpAdorsICA_ImmCategorieInterv.fc_OpenRecordSet(General.sSQL, ssICA_ImmCategorieInterv, "ICC_Noauto");
            //ssICA_ImmCategorieInterv.Visible = False
            lICA = 0;
            //ssICA_ImmCategorieInterv.Enabled = True
            //ssICA_ImmCategorieInterv.ReBind();
            ssICA_ImmCategorieInterv.DataSource = rsICA_ImmCategorieInterv;
            lICA = 0;
            var _with2 = rsICA_ImmCategorieInterv;
        
            if (rsICA_ImmCategorieInterv.Rows.Count > 0)
            {
                //_with2.MoveFirst();
                foreach (DataRow rsICA_ImmCategorieIntervRow in rsICA_ImmCategorieInterv.Rows)
                {
                    lICA = lICA + 1;
                    rsICA_ImmCategorieIntervRow["ICC_NoLigne"] = lICA;
                    tmpAdorsICA_ImmCategorieInterv.Update();
                    // _with2.MoveNext();
                }
                lICA = rsICA_ImmCategorieInterv.Rows.Count;
                // _with2.MoveFirst();
            }

            //ssICA_ImmCategorieInterv.Visible = True
            lblNiv1.Text = modP2.clibNiv1 + " sur " + modP2.clibNiv0;

            //& "=> " '& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text
            //txtNiv1 = ssICA_ImmCategorieInterv.Columns("cai_libelle").Text

            if (ssGridGAI.ActiveRow != null)
            {
                txtNiv1.Text = ssGridGAI.ActiveRow.Cells["GAI_LIbelle"].Text;
            }
            else if (ssGridGAI.Rows.Count > 0)
            {
                txtNiv1.Text = ssGridGAI.Rows[0].Cells["GAI_LIbelle"].Text;
            }
            else
            {
                txtNiv1.Text = "";
            }
            lblNiv2.Text = modP2.cLIbNiv2 + " sur " + modP2.clibNiv1 + "=> ";
            //& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text

            if (ssICA_ImmCategorieInterv.ActiveRow != null)
            {
                lblNiv2.Text = ssICA_ImmCategorieInterv.ActiveRow.Cells["cai_libelle"].Text;
            }
            else if (ssICA_ImmCategorieInterv.Rows.Count > 0)
            {
                lblNiv2.Text = ssICA_ImmCategorieInterv.Rows[0].Cells["cai_libelle"].Text;
            }
            else
            {
                lblNiv2.Text = "";
            }
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;

        }
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            ssICA_ImmCategorieInterv.UpdateData();
            ssEQU_EquipementP2Imm.UpdateData();
            ssFAP_FacArticleP2.UpdateData();
            ssPEI_PeriodeGammeImm.UpdateData();
            this.Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSupMotif_Click(object sender, EventArgs e)
        {
            string sSql = null;
            int i = 0;
            try
            {
                ssICA_ImmCategorieInterv.UpdateData();
                ssEQU_EquipementP2Imm.UpdateData();

                //If nz(.Columns("ICC_Noauto").value, 0) = 0 Then
                //    MsgBox "Vous devez vous positionner sur un " & clibNiv1 & " avant de sélectionner des prestations."
                //    Exit Sub
                //End If
                //sSQL = "DELETE FROM EQM_EquipementP2Imm" _
                //& " WHERE  ICC_Noauto =  " & .Columns("ICC_Noauto").value _
                //& " AND ( EQU_Select = 0 or EQU_Select is null)"

                //=== suprrime les prestations dans la fiche contrat.
                sSql = "DELETE FROM EQM_EquipementP2Imm" + " FROM         ICC_IntervCategorieContrat INNER JOIN "
                    + " EQM_EquipementP2Imm ON ICC_IntervCategorieContrat.ICC_Noauto = EQM_EquipementP2Imm.ICC_Noauto "
                    + " WHERE     (ICC_IntervCategorieContrat.PDAB_Noauto = " + txtPDAB_Noauto.Text + ") AND (ICC_IntervCategorieContrat.ICC_Select = 0 OR "
                    + " ICC_IntervCategorieContrat.ICC_Select IS NULL)";

                //=== suprimme les motifs dans la fiche contrat.
                General.Execute(sSql);

                sSql = "DELETE FROM ICC_IntervCategorieContrat"
                    + " WHERE     (PDAB_Noauto = " + txtPDAB_Noauto.Text + ") AND (ICC_Select = 0 OR " + " ICC_Select IS NULL)";

                General.Execute(sSql);
                //fc_MAJMotif txtCodeImmeuble
                //fc_ICA_ImmCategorieInterv txtCOP_NoAuto

                fc_ICA_ImmCategorieInterv();
                fc_EQM_EquipementP2Imm();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdSupMotif_Click");
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSuppPrest_Click(object sender, EventArgs e)
        {
            string sSql = null;
            int i = 0;
            try
            {
                ssICA_ImmCategorieInterv.UpdateData();
                ssEQU_EquipementP2Imm.UpdateData();
                if (ssICA_ImmCategorieInterv.ActiveRow != null)
                {
                    if (Convert.ToInt16(General.nz(ssICA_ImmCategorieInterv.ActiveRow.Cells["ICC_Noauto"].Value, 0)) == 0)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez vous positionner sur un " + modP2.clibNiv1 + " avant de sélectionner des prestations.");
                        return;
                    }
                    sSql = "DELETE FROM EQM_EquipementP2Imm" + " WHERE  ICC_Noauto =  "
                        + ssICA_ImmCategorieInterv.ActiveRow.Cells["ICC_Noauto"].Value
                        + " AND ( EQU_Select = 0 or EQU_Select is null)";
                    General.Execute(sSql);
                    fc_EQM_EquipementP2Imm();
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdSuppPrest_Click");
            }

        }

        private void Command1_Click(object sender, EventArgs e)
        {
            ssGridGAI.UpdateData();
            ssICA_ImmCategorieInterv.UpdateData();
            ssEQU_EquipementP2Imm.UpdateData();
            ssFAP_FacArticleP2.UpdateData();
            ssPEI_PeriodeGammeImm.UpdateData();

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_DropGAMO()
        {
            fc_DropCtr();//tested
            fc_DropMoy();//tested
            fc_DropAno();//tested
            fc_DropOpe();//tested
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_DropCtr()
        {
            try
            {
                string sSql = null;
                sSql = "";
                sSql = "SELECT     CTR_Libelle as Contrôle, CTR_ID"
                    + " From CTR_ControleGmao "
                    + " ORDER BY CTR_Libelle DESC";
                sheridan.InitialiseCombo(DropCTR, sSql, "Contrôle", true);
                DropCTR.DisplayLayout.Bands[0].Columns["CTR_ID"].Hidden = true;
                DropCTR.DisplayLayout.Bands[0].Columns[0].Width = cColWidth;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_DropCtr");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_DropMoy()
        {
            try
            {
                string sSql = null;
                sSql = "";
                sSql = "SELECT     MOY_Libelle as Moyen, MOY_ID From MOY_MoyenGmao "
                        + " ORDER BY MOY_Libelle";
                sheridan.InitialiseCombo(DropMOY, sSql, "Moyen", true);
                
                DropMOY.DisplayLayout.Bands[0].Columns["MOY_ID"].Hidden = true;
                DropMOY.DisplayLayout.Bands[0].Columns[0].Width = cColWidth;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "; fc_DropMoy");
            }

        }

        /// <summary>
        /// tested
        /// </summary>
        private void fc_DropAno()
        {
            try
            {
                string sSql = null;
                sSql = "";
                sSql = "SELECT     ANO_Libelle as Anomalie , ANO_ID From ANO_AnomalieGmao "
                        + " ORDER BY ANO_Libelle";

                sheridan.InitialiseCombo(DropANO, sSql, "Anomalie", true);
              
                DropANO.DisplayLayout.Bands[0].Columns["ANO_ID"].Hidden = true;
                DropANO.DisplayLayout.Bands[0].Columns[0].Width = cColWidth;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_DropAno");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_DropOpe()
        {
            try
            {
                string sSql = null;
                sSql = "";
                sSql = "SELECT     OPE_Libelle as Opération, OPE_ID From OPE_OperationGmao "
                        + " ORDER BY OPE_Libelle DESC";
                sheridan.InitialiseCombo(DropOPE, sSql, "Opération", true);
            
                DropOPE.DisplayLayout.Bands[0].Columns["OPE_ID"].Hidden = true;
                DropOPE.DisplayLayout.Bands[0].Columns[0].Width = cColWidth;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_DropOpe");
            }
        }

        private void fc_DropPrestation()
        {
            string sSql = null;
            int lCAI_NoAuto = 0;
            var _with5 = ssDropPrestation;
            sSql = "SELECT     CAI_Noauto"
                + " From CAI_CategoriInterv";
            if (ssICA_ImmCategorieInterv.ActiveRow != null)
            {
                sSql = sSql + " WHERE CAI_Code = '" + ssICA_ImmCategorieInterv.ActiveRow.Cells["CAI_Code"].Text + "'";
            }
            else if (ssICA_ImmCategorieInterv.Rows.Count > 0)
            {

                sSql = sSql + " WHERE CAI_Code = '" + ssICA_ImmCategorieInterv.Rows[0].Cells["CAI_Code"].Text + "'";
            }
            else
            {
                sSql = sSql + " WHERE CAI_Code ='0'";
            }


            using (var tmpAdo = new ModAdo())
                lCAI_NoAuto = Convert.ToInt16(General.nz(tmpAdo.fc_ADOlibelle(sSql), 0));

            sSql = "SELECT     EQU_Code as libellé "
                + " From EQU_EquipementP2"
                + " WHERE  CAI_Noauto = " + lCAI_NoAuto;
            sheridan.InitialiseCombo((this.ssDropPrestation), sSql, "libellé", true);
   
            ssDropPrestation.DisplayLayout.Bands[0].Columns["libellé"].Width = 400;
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_DropIntervenant()
        {
            General.sSQL = "";
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom,Personnel.prenom, Qualification.CodeQualif,"
                + " Qualification.Qualification, Personnel.Kobby, Personnel.Note_NOT"
                + " FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
            sheridan.InitialiseCombo((this.ssDropIntervenant), General.sSQL, "Matricule", true);
            ssDropIntervenant.DisplayLayout.Bands[0].Columns["CodeQualif"].Hidden = true;
            ssDropIntervenant.DisplayLayout.Bands[0].Columns["Kobby"].Hidden = true;
            ssDropIntervenant.DisplayLayout.Bands[0].Columns["Note_NOT"].Hidden = true;
          
        }
        private void fc_DropMatriculeType()
        {
            General.sSQL = "";
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom "
                + " FROM Qualification RIGHT OUTER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif"
                + " WHERE Personnel.Type = 'T'";
            // If ssDropMatricule.Rows = 0 Then
            sheridan.InitialiseCombo((this.ssDropMatricule), General.sSQL, "Matricule", true);
            //  End If
            this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Tech"].ValueList = this.ssDropMatricule;

        }

        private void fc_Rowcolchange()
        {
            try
            {

                fc_updateGrid();

                if (ssEQU_EquipementP2Imm.ActiveRow != null)
                {
                    fc_PEI_PeriodeGammeImm(Convert.ToInt32(General.nz((ssEQU_EquipementP2Imm.ActiveRow.Cells["EQM_NoAuto"].Value), 0)));

                    fc_FAP_FacArticleP2(Convert.ToInt32(General.nz((ssEQU_EquipementP2Imm.ActiveRow.Cells["EQM_NoAuto"].Value), 0)));

                    ssFAP_FacArticleP2.DisplayLayout.Bands[0].Columns["FAP_Designation1"].Header.Caption = modP2.cLibNiv3;
                }
                else if (ssEQU_EquipementP2Imm.Rows.Count > 0)
                {

                    fc_PEI_PeriodeGammeImm(Convert.ToInt32(General.nz((ssEQU_EquipementP2Imm.Rows[0].Cells["EQM_NoAuto"].Value), 0)));

                    fc_FAP_FacArticleP2(Convert.ToInt32(General.nz((ssEQU_EquipementP2Imm.Rows[0].Cells["EQM_NoAuto"].Value), 0)));
                    ssFAP_FacArticleP2.DisplayLayout.Bands[0].Columns["FAP_Designation1"].Header.Caption = modP2.cLibNiv3;

                }
                else
                {
                    fc_PEI_PeriodeGammeImm(0);

                    fc_FAP_FacArticleP2(0);
                    ssFAP_FacArticleP2.DisplayLayout.Bands[0].Columns["FAP_Designation1"].Header.Caption = modP2.cLibNiv3;
                }

                fc_DropPrestation();

                if (ssEQU_EquipementP2Imm.ActiveRow != null)
                {
                    ssFAP_FacArticleP2.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
                    ssPEI_PeriodeGammeImm.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
                }
                else if (ssEQU_EquipementP2Imm.Rows.Count > 0 && string.IsNullOrEmpty(ssEQU_EquipementP2Imm.Rows[0].Cells["EQM_NoAuto"].Text))
                {
                    ssFAP_FacArticleP2.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
                    ssPEI_PeriodeGammeImm.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
                }
                else
                {
                    ssFAP_FacArticleP2.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
                    ssPEI_PeriodeGammeImm.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
                }


                lblNiv3a.Text = modP2.cLibNiv3 + "=> ";
                //& ssEQU_EquipementP2Imm.Columns("TypeInterv").Text & " " & ssEQU_EquipementP2Imm.Columns("EQM_Code").Text
                //        txtNiv3a = ssEQU_EquipementP2Imm.Columns("TypeInterv").Text & " " & ssEQU_EquipementP2Imm.Columns("EQM_Code").Text
                if (ssEQU_EquipementP2Imm.ActiveRow != null)
                {
                    txtNiv3a.Text = ssEQU_EquipementP2Imm.ActiveRow.Cells["EQM_Libelle"].Text + " "
                      + ssEQU_EquipementP2Imm.ActiveRow.Cells["EQM_Code"].Text;
                }
                else if (ssEQU_EquipementP2Imm.Rows.Count > 0)
                {
                    txtNiv3a.Text = ssEQU_EquipementP2Imm.Rows[0].Cells["EQM_Libelle"].Text + " "
                       + ssEQU_EquipementP2Imm.Rows[0].Cells["EQM_Code"].Text;
                }
                else
                {
                    txtNiv3a.Text = " ";
                }

                lblNiv3b.Text = modP2.cLibNiv4 + "=> ";

                //& ssEQU_EquipementP2Imm.Columns("TypeInterv").Text & " " & ssEQU_EquipementP2Imm.Columns("EQM_Code").Text
                //txtNiv3b = ssEQU_EquipementP2Imm.Columns("TypeInterv").Text & " " & ssEQU_EquipementP2Imm.Columns("EQM_Code").Text

                if (ssEQU_EquipementP2Imm.ActiveRow != null)
                {
                    txtNiv3b.Text = ssEQU_EquipementP2Imm.ActiveRow.Cells["EQM_Libelle"].Text + " "
                       + ssEQU_EquipementP2Imm.ActiveRow.Cells["EQM_Code"].Text;
                }
                else if (ssEQU_EquipementP2Imm.Rows.Count > 0)
                {
                    txtNiv3b.Text = ssEQU_EquipementP2Imm.Rows[0].Cells["EQM_Libelle"].Text + " "
                       + ssEQU_EquipementP2Imm.Rows[0].Cells["EQM_Code"].Text;

                }
                else
                {
                    txtNiv3b.Text = "";
                }
                sheridan.fc_ColorLigneEncours(ssEQU_EquipementP2Imm);

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Rowcolchange");
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        private void fc_updateGrid()
        {
            ssICA_ImmCategorieInterv.UpdateData();
            ssEQU_EquipementP2Imm.UpdateData();
            ssFAP_FacArticleP2.UpdateData();
            ssPEI_PeriodeGammeImm.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDROP_CAI_AfterCloseUp(object sender, EventArgs e)
        {
            fc_findCAI_Lib();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDROP_CAI_BeforeDropDown(object sender, CancelEventArgs e)
        {
            fc_DROP_CAI();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDropGAM_AfterCloseUp(object sender, EventArgs e)
        {
            fc_findGAI_Lib();//tested
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssEQU_EquipementP2Imm_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (ssEQU_EquipementP2Imm.ActiveRow != null)
            {

           
            if (string.IsNullOrEmpty(ssEQU_EquipementP2Imm.ActiveRow.Cells["EQM_Code"].Text))
            {
                //        MsgBox "Vous devez saisir un code.", vbInformation, "Avertissement"
                //        Cancel = True
                //        Exit Sub
            }
            }
            //If ssEQU_EquipementP2Imm.Columns("EQM_Libelle").Text = "" Then
            //        MsgBox "Vous devez saisir un libellé.", vbInformation, "Avertissement"
            //        Cancel = True
            //        Exit Sub
            //End If
        }

       

        private void ssEQU_EquipementP2Imm_Error(object sender, ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.Data)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + e.DataErrorInfo.Cell.Column.Key;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }
        private void ssFAP_FacArticleP2_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(ssFAP_FacArticleP2.ActiveRow.Cells["FAP_Designation1"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez une " + modP2.cLibNiv3 + ".", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                return;
            }
        }

        private void ssFAP_FacArticleP2_Error(object sender, ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.Data)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + e.DataErrorInfo.Cell.Column.Key;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }
        private void ssGridGAI_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            try
            {
                tmpAdorsGai.Update();
                ssGridGAI.UpdateData();
                if (ssGridGAI.ActiveRow != null) { 
                string sSql = null;
                int lGAM_ID = 0;
                sSql = "SELECT     GAM_ID"
                    + " From GAM_Gamme "
                    + " WHERE  GAM_Code = '" + ssGridGAI.ActiveRow.Cells["GAM_Code"].Text + "'";
                var tmpAdo = new ModAdo();
                lGAM_ID = Convert.ToInt16(General.nz(tmpAdo.fc_ADOlibelle(sSql), 0));
                modP2.fc_AddGamme(Convert.ToInt32(ssGridGAI.ActiveRow.Cells["GAI_ID"].Value.ToString()), txtCodeImmeuble.Text, lGAM_ID, Convert.ToInt32(txtCop_NoAuto.Text), Convert.ToInt32(txtPDAB_Noauto.Text));
                //fc_AddOp lCAI_NoAuto, _
                //txtCodeImmeuble, txtCop_NoAuto, .Columns("ICC_NoAuto").value
                return;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssGridGAI_AfterUpdate");
            }

        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssICA_ImmCategorieInterv_AfterRowUpdate(object sender, RowEventArgs e)
        {

            int lCAI_NoAuto = 0;
            string sSql = null;
            try
            {
                if (ssICA_ImmCategorieInterv.ActiveRow != null)
                {
                    tmpAdorsICA_ImmCategorieInterv.Update();
                       var _with9 = ssICA_ImmCategorieInterv;
                    sSql = "SELECT     CAI_Noauto" + " From CAI_CategoriInterv"
                         + " WHERE CAI_Code = '" + _with9.ActiveRow.Cells["CAI_Code"].Text + "'";

                    using (var tmpAdo = new ModAdo())
                        lCAI_NoAuto = Convert.ToInt16(General.nz(tmpAdo.fc_ADOlibelle(sSql), 0));
                    modP2.fc_AddOp(Convert.ToString(lCAI_NoAuto), txtCodeImmeuble.Text, Convert.ToInt32(txtCop_NoAuto.Text),
                       Convert.ToInt16(_with9.ActiveRow.Cells["ICC_NoAuto"].Value));
                    fc_EQM_EquipementP2Imm();
                    return;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssICA_ImmCategorieInterv_AfterUpdate");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssICA_ImmCategorieInterv_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            
        }

        /// <summary>
        /// tested
        /// </summary>
        private void fc_findCAI_Lib()
        {
            // ssDROP_CAI.MoveFirst();
            if (ssDROP_CAI.Rows.Count > 0)
            {
                foreach (var ssDROP_CAIRow in ssDROP_CAI.Rows)
                {
                    if (ssICA_ImmCategorieInterv.ActiveRow.Cells["CAI_Code"].Text.ToString().ToUpper()
                        == ssDROP_CAIRow.Cells["code"].Text.ToString().ToUpper())
                    {
                        ssICA_ImmCategorieInterv.ActiveRow.Cells["CAI_Libelle"].Value = ssDROP_CAIRow.Cells["libellé"].Text;
                        ssICA_ImmCategorieInterv.ActiveRow.Cells["CAI_Code"].Value = ssDROP_CAIRow.Cells["code"].Text;
                        ssICA_ImmCategorieInterv.ActiveRow.Cells["ICC_NoAuto"].Value = ssDROP_CAIRow.Cells["CAI_NoAuto"].Text;
                        break; // TODO: might not be correct. Was : Exit For
                    }
                    //  ssDROP_CAI.MoveNext();
                }
             
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_findGAI_Lib()
        {
            //ssDropGAM.MoveFirst();
            if (ssDropGAM.Rows.Count > 0)
            {
                foreach (var ssDropGAMRow in ssDropGAM.Rows)
                {
                    if (ssGridGAI.ActiveRow.Cells["GAM_Code"].Text.ToString().ToUpper()
                        == ssDropGAMRow.Cells["code"].Text.ToString().ToUpper())
                    {
                        ssGridGAI.ActiveRow.Cells["GAI_LIbelle"].Value = ssDropGAMRow.Cells["libellé"].Text;

                        if (string.IsNullOrEmpty(ssDropGAMRow.Cells["GAM_Compteur"].Text))
                        {
                            ssGridGAI.ActiveRow.Cells["GAI_Compteur"].Value = false;
                        }
                        else
                        {
                            ssGridGAI.ActiveRow.Cells["GAI_Compteur"].Value = ssDropGAMRow.Cells["GAM_Compteur"].Text;
                        }

                        if (string.IsNullOrEmpty(ssDropGAMRow.Cells["GAM_CompteurObli"].Text))
                        {
                            ssGridGAI.ActiveRow.Cells["GAI_CompteurObli"].Value = false;
                        }
                        else
                        {
                            ssGridGAI.ActiveRow.Cells["GAI_CompteurObli"].Value = ssDropGAMRow.Cells["GAM_CompteurObli"].Text;
                        }

                        //ssICA_ImmCategorieInterv.Columns("CAI_NoAuto").value = ssDROP_CAI.Columns("CAI_NoAuto").Text
                        break;
                    }
                    //ssDropGAM.MoveNext();
                }
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssICA_ImmCategorieInterv_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            if (ssICA_ImmCategorieInterv.ActiveRow != null)
            {

                if (string.IsNullOrEmpty(ssICA_ImmCategorieInterv.ActiveRow.Cells["CAI_Code"].Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un code.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }

                if (string.IsNullOrEmpty(ssICA_ImmCategorieInterv.ActiveRow.Cells["CAI_Libelle"].Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un libellé.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }
            }
        }

        private void ssICA_ImmCategorieInterv_AfterExitEditMode(object sender, EventArgs e)
        {
            var _with10 = ssICA_ImmCategorieInterv;
           
            if (string.IsNullOrEmpty(_with10.ActiveRow.Cells["GAi_ID"].Text))
            {
                ssICA_ImmCategorieInterv.ActiveRow.Cells["GAi_ID"].Value = ssGridGAI.ActiveRow.Cells["GAi_ID"].Text;
            }

            if (_with10.ActiveRow.Cells["CAI_Code"].Column.Index == ssICA_ImmCategorieInterv.ActiveCell.Column.Index)
            {
                fc_findCAI_Lib();
            }

            if (string.IsNullOrEmpty(_with10.ActiveRow.Cells["Codeimmeuble"].Text))
            {
                _with10.ActiveRow.Cells["Codeimmeuble"].Value = txtCodeImmeuble.Text;
            }

            if (string.IsNullOrEmpty(_with10.ActiveRow.Cells["COP_NoAuto"].Text))
            {
                _with10.ActiveRow.Cells["COP_NoAuto"].Value = txtCop_NoAuto.Text;
            }

            if (string.IsNullOrEmpty(_with10.ActiveRow.Cells["ICC_NoLigne"].Text) && ! string.IsNullOrEmpty(_with10.ActiveRow.Cells["CAI_Code"].Text))
            {
                lICA = lICA + 1;
                _with10.ActiveRow.Cells["ICC_NoLigne"].Value = lICA;
            }

           

            if (ssICA_ImmCategorieInterv.ActiveCell.Column.Index == _with10.ActiveRow.Cells["CAI_CODE"].Column.Index)
            {
                _with10.ActiveRow.Cells["CAI_Code"].Value = _with10.ActiveRow.Cells["CAI_Code"].Text.ToUpper();
            }
            //P3
            if (ssICA_ImmCategorieInterv.ActiveCell.Column.Index == 6)
            {
                if (_with10.ActiveRow.Cells[ssICA_ImmCategorieInterv.ActiveCell.Column.Index].Value.ToString() == "-1")
                {
                    _with10.ActiveRow.Cells["ICC_P3traite"].Value = System.Windows.Forms.CheckState.Unchecked;
                }
            }

            if (General.sPriseEnCompteLocalP2 == "1")
            {
                if (string.IsNullOrEmpty(_with10.ActiveRow.Cells["PDAB_Noauto"].Text))
                {
                    _with10.ActiveRow.Cells["PDAB_Noauto"].Value = txtPDAB_Noauto.Text;
                }
            }

          
        }

        private void ssICA_ImmCategorieInterv_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //eventArgs.dispPromptMsg = 0;
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }


        /// <summary>
        /// tested
        /// </summary>
        private void fc_EQM_EquipementP2Imm()
        {
            try
            {
                int i = 0;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
                fc_updateGrid();//tested
                General.sSQL = "SELECT EQM_NoAuto, CodeImmeuble, ICC_Noauto," + " EQU_TypeIntervenant,EQU_Select, EQM_Code, EQM_Libelle, "
                    + " CTR_ID, CTR_Libelle, MOY_ID, MOY_Libelle, ANO_ID, ANO_Libelle, OPE_ID, OPE_Libelle, "
                    + " EQM_NoLigne, EQM_Affiche, COP_NoAuto" + " ,Duree, DureePlanning, CompteurObli, EQU_NoAuto , EQU_P1Compteur , EQM_Observation "
                    + " From EQM_EquipementP2Imm";

                if (ssICA_ImmCategorieInterv.ActiveRow != null)
                {
                    General.sSQL = General.sSQL + " WHERE (ICC_Noauto =" + General.nz(ssICA_ImmCategorieInterv.ActiveRow.Cells["ICC_Noauto"].Text, 0) + ")";

                }
                else if (ssICA_ImmCategorieInterv.Rows.Count > 0)
                {

                    General.sSQL = General.sSQL + " WHERE (ICC_Noauto =" + General.nz(ssICA_ImmCategorieInterv.Rows[0].Cells["ICC_Noauto"].Text, 0) + ")";
                }
                else
                {
                    General.sSQL = General.sSQL + " WHERE (ICC_Noauto =0)";
                }
                General.sSQL = General.sSQL + " order by EQM_NoLigne";
                lEquip = 0;
                ssEQU_EquipementP2Imm.Visible = false;
           
                rsEQM_EquipementP2Imm = new DataTable();
                rsEQM_EquipementP2Imm = tmpAdorsEQM_EquipementP2Imm.fc_OpenRecordSet(General.sSQL,ssEQU_EquipementP2Imm, "EQM_NoAuto");
                //il faut vérifier cette fonction
                if (rsEQM_EquipementP2Imm.Rows.Count > 0)
                {
                    i = 0;
                    // _with15.MoveFirst();
                    foreach (DataRow rsEQM_EquipementP2ImmRow in rsEQM_EquipementP2Imm.Rows)
                    {
                        i = i + 1;
                        rsEQM_EquipementP2ImmRow["EQM_NoLigne"] = i;
                        tmpAdorsEQM_EquipementP2Imm.Update();
                        //_with15.MoveNext();
                    }
                    // _with15.MoveFirst();
                    lEquip = rsEQM_EquipementP2Imm.Rows.Count;
                }
                // ssEQU_EquipementP2Imm.ReBind();
                ssEQU_EquipementP2Imm.DataSource = rsEQM_EquipementP2Imm;
                ssEQU_EquipementP2Imm.UpdateData();
               ssEQU_EquipementP2Imm.Visible = true;

                if (ssICA_ImmCategorieInterv.ActiveRow != null)
                {
                    if (string.IsNullOrEmpty(ssICA_ImmCategorieInterv.ActiveRow.Cells["CAI_CODE"].Text))
                    {
                        ssEQU_EquipementP2Imm.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
                    }
                    else
                    {
                        ssEQU_EquipementP2Imm.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
                    }
                }
               else if (ssICA_ImmCategorieInterv.Rows.Count > 0)
                {
                    if (string.IsNullOrEmpty(ssICA_ImmCategorieInterv.Rows[0].Cells["CAI_CODE"].Text))
                    {
                        ssEQU_EquipementP2Imm.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
                    }
                    else
                    {
                        ssEQU_EquipementP2Imm.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
                    }
                }
                else
                {
                    ssEQU_EquipementP2Imm.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
                }


                lblNiv0.Text = modP2.clibNiv0;

                //& "=> "  '& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text
                //txtNiv0 = ssICA_ImmCategorieInterv.Columns("cai_libelle").Text
                //lblNiv1 = clibNiv1 & "=> " '& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text

                lblNiv1.Text = modP2.clibNiv1 + " sur " + modP2.clibNiv0 + "=> ";
                //& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text
                //txtNiv1 = ssICA_ImmCategorieInterv.Columns("cai_libelle").Text
                if (ssGridGAI.ActiveRow != null)
                {
                    txtNiv1.Text = ssGridGAI.ActiveRow.Cells["GAI_LIbelle"].Text;
                }
                else if (ssGridGAI.Rows.Count > 0)
                {
                    txtNiv1.Text = ssGridGAI.Rows[0].Cells["GAI_LIbelle"].Text;
                }
                else
                {
                    txtNiv1.Text = "";
                }
                //lblNiv2 = cLIbNiv2 & "=> " '& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text

                lblNiv2.Text = modP2.cLIbNiv2 + " sur " + modP2.clibNiv1 + "=> ";
                //& ssICA_ImmCategorieInterv.Columns("cai_libelle").Text

                if (ssICA_ImmCategorieInterv.ActiveRow != null)
                {
                    txtNiv2.Text = ssICA_ImmCategorieInterv.ActiveRow.Cells["cai_libelle"].Text;
                }

                else if (ssICA_ImmCategorieInterv.Rows.Count > 0)
                {
                    txtNiv2.Text = ssICA_ImmCategorieInterv.Rows[0].Cells["cai_libelle"].Text;
                }
                else
                {
                    txtNiv2.Text = "";
                }
                sheridan.fc_ColorLigneEncours(ssICA_ImmCategorieInterv);

                fc_Rowcolchange();

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_EQM_EquipementP2Imm");
            }

        }
        private void ssEQU_EquipementP2Imm_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //eventArgs.dispPromptMsg = 0;
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        private void fc_FAP_FacArticleP2(int lEQM_NoAuto)
        {
            int i = 0;
            General.sSQL = "SELECT FAP_CodeArticle, FAP_Designation1, FAP_NoAuto,"
                + " EQM_NoAuto, CodeImmeuble, FAP_NoLIgne, FAP_Affiche,COP_NoAuto"
                + " From FAP_FacArticleP2" + " WHERE     (EQM_NoAuto =" + General.nz(lEQM_NoAuto, 0) + ")" + " order by FAP_NoLIgne";
            ssFAP_FacArticleP2.Visible = false;
            lFAP_Article = 0;
            ssFAP_FacArticleP2.Enabled = true;
            rsFAP_FacArticleP2 = new DataTable();
            var tmpAdo = new ModAdo();
            rsFAP_FacArticleP2 = tmpAdo.fc_OpenRecordSet(General.sSQL);

            if (rsFAP_FacArticleP2.Rows.Count > 0)
            {
                // _with21.MoveFirst();
                i = 0;
                foreach (DataRow rsFAP_FacArticleP2Row in rsFAP_FacArticleP2.Rows)
                {
                    i = i + 1;
                    rsFAP_FacArticleP2Row["FAP_NoLIgne"] = i;
                    tmpAdo.Update();
                    // _with21.MoveNext();
                }
                lFAP_Article = rsFAP_FacArticleP2.Rows.Count;
            }
            //ssFAP_FacArticleP2.ReBind();

            ssFAP_FacArticleP2.DataSource = rsFAP_FacArticleP2;
            ssFAP_FacArticleP2.Visible = true;

        }
        private void ssFAP_FacArticleP2_BeforeEnterEditMode(object sender, CancelEventArgs e)
        {
            if (string.IsNullOrEmpty(ssFAP_FacArticleP2.ActiveRow.Cells["FAP_NoLIgne"].Text))
            {
                lFAP_Article = lFAP_Article + 1;
                ssFAP_FacArticleP2.ActiveRow.Cells["FAP_NoLIgne"].Value = lFAP_Article;
            }

            if (string.IsNullOrEmpty(ssFAP_FacArticleP2.ActiveRow.Cells["EQM_NoAuto"].Text))
            {
                ssFAP_FacArticleP2.ActiveRow.Cells["EQM_NoAuto"].Value = ssEQU_EquipementP2Imm.ActiveRow.Cells["EQM_NoAuto"].Text;
            }

            if (ssFAP_FacArticleP2.ActiveRow.Cells["FAP_Affiche"].Text == "-1")
            {
                ssFAP_FacArticleP2.ActiveRow.Cells["FAP_Affiche"].Value = 1;
            }

            if (string.IsNullOrEmpty(ssFAP_FacArticleP2.ActiveRow.Cells["CodeImmeuble"].Text))
            {
                ssFAP_FacArticleP2.ActiveRow.Cells["CodeImmeuble"].Value = txtCodeImmeuble.Text;
            }

            if (string.IsNullOrEmpty(ssFAP_FacArticleP2.ActiveRow.Cells["COP_NoAuto"].Text) && !string.IsNullOrEmpty(txtCop_NoAuto.Text))
            {
                ssFAP_FacArticleP2.ActiveRow.Cells["Cop_NoAuto"].Value = txtCop_NoAuto.Text;
            }
        }
        private void ssFAP_FacArticleP2_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //eventArgs.dispPromptMsg = 0;
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lEQM_NoAuto"></param>
        private void fc_PEI_PeriodeGammeImm(int lEQM_NoAuto)
        {
            //sSQl = "SELECT     PEI_PeriodeGammeImm.*" _
            //& " FROM         PEI_PeriodeGammeImm " _
            //& " WHERE     (EQM_NoAuto =" & nz(ssGammePlanning.Columns("EQM_NoAuto").Text, 0) & ")"

            General.sSQL = " SELECT CodeImmeuble, EQM_NoAuto, PEI_NoAuto, TPP_Code, PEI_Cycle,  PEI_DateDebut, PEI_DateFin, PEI_JD,"
                + " PEI_MD, PEI_JF, PEI_MF,PEI_PassageObli, PEI_ForceJournee ,SEM_Code, PEI_Heure, PEI_Duree,PEI_DureeReel,PEI_DureeVisu, "
                + " PEI_Samedi, PEI_Dimanche, PEI_Pentecote, PEI_Jourferiee," + " PEI_Visite, PEI_ForceVisite,"
                + " PEI_Tech ,PEI_Cout,PEI_MtAchat , PEI_KFO, PEI_KMO, PEI_Calcul ,PEI_TotalHT, PEI_IntAutre,"
                + "  PEI_Intervenant, PEI_APlanifier,COP_NoAuto, PEI_AvisPassage, PEI_AvisNbJour" + " "
                + " From PEI_PeriodeGammeImm " + " WHERE     (EQM_NoAuto =" + General.nz(lEQM_NoAuto, 0) + ")";
           
            rsPEI_PeriodeGammeImm = new DataTable();
            rsPEI_PeriodeGammeImm = tmpAdorsPEI_PeriodeGammeImm.fc_OpenRecordSet(General.sSQL, ssPEI_PeriodeGammeImm,"PEI_NoAuto");
            ssPEI_PeriodeGammeImm.Visible = false;
            ssPEI_PeriodeGammeImm.DataSource = rsPEI_PeriodeGammeImm;
            ssPEI_PeriodeGammeImm.UpdateData();
            ssPEI_PeriodeGammeImm.Visible = true;
            //Timer2.Enabled = True
        }
        private void ssICA_ImmCategorieInterv_Error(object sender, ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.Data)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + e.DataErrorInfo.Cell.Column.Header.Caption;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }


        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPEI_PeriodeGammeImm_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            var _with29 = ssPEI_PeriodeGammeImm;
            if (_with29.ActiveRow != null)
            {
                if (string.IsNullOrEmpty(_with29.ActiveRow.Cells["PEI_DureeReel"].Text) && !string.IsNullOrEmpty(_with29.ActiveRow.Cells["PEI_Duree"].Text))
                {
                    _with29.ActiveRow.Cells["PEI_DureeReel"].Value = General.nz((_with29.ActiveRow.Cells["PEI_Duree"].Text), 0);
                }
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void ssPEI_PeriodeGammeImm_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.ReInitialize)
                return;
            fc_CtrlssPEI_PeriodeGammeImm(e.Row);
            if (General.nz(e.Row.Cells["PEI_IntAutre"].Text, "0").ToString() == "0")
            {
                e.Row.Cells["PEI_Intervenant"].Value = modP2.FC_intervenantImm(txtCodeImmeuble.Text);
            }

            if (General.nz(e.Row.Cells["PEI_IntAutre"].Text, "0").ToString() == "0")
            {
                e.Row.Cells["PEI_Intervenant"].Value = modP2.FC_intervenantImm(txtCodeImmeuble.Text);
            }
            if (e.Row.Cells["PEI_PassageObli"].Value == DBNull.Value || e.Row.Cells["PEI_PassageObli"].Value.ToString() == "0")
            {
                e.Row.Cells["PEI_PassageObli"].Value = false;
            }
            if (e.Row.Cells["PEI_ForceJournee"].Value == DBNull.Value || e.Row.Cells["PEI_ForceJournee"].Value.ToString() == "0")
            {
                e.Row.Cells["PEI_ForceJournee"].Value = false;
            }

            if (e.Row.Cells["PEI_IntAutre"].Value == DBNull.Value || e.Row.Cells["PEI_IntAutre"].Value.ToString() == "0")
            {
                e.Row.Cells["PEI_IntAutre"].Value = false;
            }
            if (e.Row.Cells["PEI_APlanifier"].Value == DBNull.Value || e.Row.Cells["PEI_APlanifier"].Value.ToString() == "0")
            {
                e.Row.Cells["PEI_APlanifier"].Value = false;
            }
            if (e.Row.Cells["PEI_AvisPassage"].Value == DBNull.Value || e.Row.Cells["PEI_AvisPassage"].Value.ToString() == "0")
            {
                e.Row.Cells["PEI_AvisPassage"].Value = false;
            }

           // ssPEI_PeriodeGammeImm.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_CtrlssPEI_PeriodeGammeImm(UltraGridRow row=null)
        {
            int i = 0;
            //--controle certains elements dans la grille.
            var _with30 = ssPEI_PeriodeGammeImm;
            if (row == null) return;
            //-- si la colonne PEI_IntAutre est à 1(signifiant que ce n'est pas l'intervenant
            //-- par defaut de l'immeuble)  alors on débloque PEI_Intervenant colonne sinon
            //-- on la bloque

                if (string.IsNullOrEmpty(row.Cells["PEI_IntAutre"].Text)
                    || row.Cells["PEI_IntAutre"].Text == "0" || row.Cells["PEI_IntAutre"].Text == "False")
            {                
                row.Cells["PEI_Intervenant"].Activation = Activation.Disabled;
                    row.Cells["PEI_Intervenant"].Appearance.BackColor = Color.Gray;
                    ssDropIntervenant.Enabled = false;
                    row.Cells["PEI_Intervenant"].DroppedDown= false;
                }
                else if (row.IsAddRow)
                {
                    row.Cells["PEI_Intervenant"].Activation = Activation.AllowEdit;
                    row.Cells["PEI_Intervenant"].Appearance.BackColor = Color.Gray;
                    ssDropIntervenant.Enabled = false;
                   row.Cells["PEI_Intervenant"].DroppedDown = false;
            }
                else
                {
                    row.Cells["PEI_Intervenant"].Activation = Activation.AllowEdit;
                    row.Cells["PEI_Intervenant"].Appearance.BackColor = Color.Gray;
                    ssDropIntervenant.Enabled = true;
                    row.Cells["PEI_Intervenant"].ValueList = this.ssDropIntervenant;

                }


                if (row.Cells["TPP_Code"].Text.ToString().ToUpper() == modP2.cCycleAnnuel.ToString().ToUpper()
                    || row.Cells["TPP_Code"].Text.ToString().ToUpper() == modP2.cCycleJournee.ToString().ToUpper()
                    || row.Cells["TPP_Code"].Text.ToString().ToUpper() == modP2.cCycleMensuel.ToString().ToUpper())
                {
                    row.Cells["PEI_Cycle"].Activation = Activation.NoEdit;
                    row.Cells["PEI_Cycle"].Appearance.BackColor = Color.White;
                }
                else
                {
                    row.Cells["PEI_Cycle"].Activation = Activation.AllowEdit;
                    row.Cells["PEI_Cycle"].Appearance.BackColor = Color.Gray;
                }

                row.Cells["PEI_DureeVisu"].Activation = Activation.AllowEdit;
                row.Cells["PEI_DureeVisu"].Appearance.BackColor = Color.Gray;

                //--si la colonne PEI_Calcul est à 1(signifiant que le montant n'est pas calculé)
                //-- alors on  débloque PEI_TotalHT sinon on la bloque.
                if (row.Cells["PEI_Calcul"].Text == "1")
                {
                    row.Cells["PEI_TotalHT"].Activation = Activation.NoEdit;
                    row.Cells["PEI_TotalHT"].Appearance.BackColor = Color.White;
                }
                else if (row.IsAddRow)
                {
                    row.Cells["PEI_TotalHT"].Activation = Activation.AllowEdit;
                    row.Cells["PEI_TotalHT"].Appearance.BackColor = Color.Gray;
                }
                else
                {
                    row.Cells["PEI_TotalHT"].Activation = Activation.NoEdit;
                    row.Cells["PEI_TotalHT"].Appearance.BackColor = Color.Gray;
                }

                //--si la colonne PEI_AvisPassage est à 1(signifiant qu'une demande d'avis de passage est demandé)
                //-- alors on  débloque PEI_AvisNbJour sinon on la bloque.
                if (row.Cells["PEI_AvisPassage"].Text == "1")
                {
                    row.Cells["PEI_AvisNbJour"].Activation = Activation.NoEdit;
                    row.Cells["PEI_AvisNbJour"].Appearance.BackColor = Color.White;
                }
                else if (row.IsAddRow)
                {
                    row.Cells["PEI_AvisNbJour"].Activation = Activation.AllowEdit;
                    row.Cells["PEI_AvisNbJour"].Appearance.BackColor = Color.Gray;
                }
                else
                {
                    row.Cells["PEI_AvisNbJour"].Activation = Activation.AllowEdit;
                    row.Cells["PEI_AvisNbJour"].Appearance.BackColor = Color.Gray;
                }
                if (row.Cells["PEI_PassageObli"].Text == "1")
                {
                    row.Cells["PEI_HEURE"].Activation = Activation.NoEdit;
                    row.Cells["PEI_HEURE"].Appearance.BackColor = Color.White;
                }
                else if (row.IsAddRow)
                {
                    row.Cells["PEI_HEURE"].Activation = Activation.AllowEdit;
                    row.Cells["PEI_HEURE"].Appearance.BackColor = Color.Gray;
                }
                else
                {
                    row.Cells["PEI_HEURE"].Activation = Activation.AllowEdit;
                    row.Cells["PEI_HEURE"].Appearance.BackColor = Color.Gray;
                }

                //--si la colonne PEI_AvisPassage est à 1(signifiant qu'une demande d'avis de passage est demandé)
                //-- alors on  débloque PEI_AvisNbJour sinon on la bloque.
                if (row.Cells["PEI_ForceJournee"].Text == "1")
                {
                    row.Cells["SEM_CODE"].Activation = Activation.AllowEdit;
                    row.Cells["SEM_CODE"].Appearance.BackColor = Color.White;

                    if (row.Cells["PEI_PassageObli"].Text != "1")
                    {
                        row.Cells["PEI_HEURE"].Activation = Activation.AllowEdit;
                        row.Cells["PEI_HEURE"].Appearance.BackColor = Color.White;
                    }
                }
                else if (row.IsAddRow)
                {
                    row.Cells["SEM_CODE"].Activation = Activation.NoEdit;
                    row.Cells["SEM_CODE"].Appearance.BackColor = Color.Gray;
                    if (row.Cells["PEI_PassageObli"].Text != "1")
                    {
                        row.Cells["PEI_HEURE"].Activation = Activation.NoEdit;
                        row.Cells["PEI_HEURE"].Appearance.BackColor = Color.Gray;
                    }
                }
                else
                {
                    row.Cells["SEM_CODE"].Activation = Activation.NoEdit;
                    row.Cells["SEM_CODE"].Appearance.BackColor = Color.Gray;
                    if (row.Cells["PEI_PassageObli"].Text != "1")
                    {
                        row.Cells["PEI_HEURE"].Activation = Activation.NoEdit;
                        row.Cells["PEI_HEURE"].Appearance.BackColor = Color.Gray;
                    }
                }
                //--si la colonne PEI_AvisPassage est à 1(signifiant qu'une demande d'avis de passage est demandé)
                //-- alors on  débloque PEI_AvisNbJour sinon on la bloque.
                //    If .Columns("PEI_ForceJournee").Text = "1" Then
                //        .Columns("SEM_CODE").CellStyleSet "Blanc", .Row
                //        .Columns("SEM_CODE").Locked = False
                //        .Columns("PEI_HEURE").CellStyleSet "Blanc", .Row
                //        .Columns("PEI_HEURE").Locked = False
                //    ElseIf .IsAddRow Then
                //        .Columns("SEM_CODE").CellStyleSet "GrisFonce", .Row
                //        .Columns("SEM_CODE").Locked = True
                //        .Columns("PEI_HEURE").CellStyleSet "GrisFonce", .Row
                //        .Columns("PEI_HEURE").Locked = True
                //    Else
                //        .Columns("SEM_CODE").CellStyleSet "GrisFonce", .Row
                //        .Columns("SEM_CODE").Locked = True
                //        .Columns("PEI_HEURE").CellStyleSet "GrisFonce", .Row
                //        .Columns("PEI_HEURE").Locked = True
                //    End If

                row.Cells["PEI_Visite"].Activation = Activation.NoEdit;
                row.Cells["PEI_Visite"].Appearance.BackColor = Color.Gray;


                if (row.Cells["PEI_APlanifier"].Text == "1")
                {
                  /*  for (i = 0; i <= _with30..Column.Index - 1; i++)
                    {
                        //row.Cells[i].Appearance.Tag= "NePASPlannifier";verifier
                    }*/
                }
            

        }

        private void ssPEI_PeriodeGammeImm_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //eventArgs.dispPromptMsg = 0;
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void ssPEI_PeriodeGammeImm_Error(object sender, ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.Data)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + e.DataErrorInfo.Cell.Column.Header.Caption;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }

        private void ssGridGAI_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void frmP2_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);
            
            if (General.sMAJMotifV2 == "1")
            {
                cmdSupMotif.Visible = true;
            }
            else
            {
                cmdSupMotif.Visible = false;
            }           
            fc_Gamme(Convert.ToInt32(txtCop_NoAuto.Text));
            fc_ICA_ImmCategorieInterv();
            //fc_ICA_ImmCategorieInterv txtCop_NoAuto
            fc_EQM_EquipementP2Imm();
            fc_CtrlssPEI_PeriodeGammeImm();
            fc_Rowcolchange();
            fc_fGrilleInit();
            modP2.fc_ParamGmao();
            //fc_DropIntervenant
            //fc_DropMatriculeType
            fc_DropGAMO();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssEQU_EquipementP2Imm_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["EQM_CODE"].ValueList = this.ssDropPrestation;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["ANO_Libelle"].ValueList = DropANO;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["OPE_Libelle"].ValueList = DropOPE;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["CTR_Libelle"].ValueList = DropCTR;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["MOY_Libelle"].ValueList = DropMOY;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["EQU_TypeIntervenant"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["EQM_NoAuto"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["ICC_NoAuto"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["EQU_Select"].Header.Caption = "Sélection";
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["EQU_Select"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["EQU_Select"].Editor.DataFilter = new BooleanColumnDataFilter();
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["EQU_Select"].Header.Caption = "Sélection";
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["EQM_Code"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["EQM_Libelle"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["CTR_ID"].Hidden = true;

            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["CTR_Libelle"].Header.Caption = "Controle";
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["Moy_ID"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["Moy_Libelle"].Header.Caption = "Moyen";
             ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["ANO_ID"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["ANO_Libelle"].Header.Caption = "Anomalie";
             ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["OPE_ID"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["OPE_Libelle"].Header.Caption = "Opération";

            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["EQM_NoLigne"].Header.Caption = "N°Ligne";

             ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["EQM_Affiche"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["COP_NoAuto"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["Duree"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["DureePlanning"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["CompteurObli"].Hidden = true;

            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["EQU_NoAuto"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["EQU_P1Compteur"].Hidden = true;
            ssEQU_EquipementP2Imm.DisplayLayout.Bands[0].Columns["EQM_Observation"].Header.Caption = "Observation";

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGAI_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            this.ssGridGAI.DisplayLayout.Bands[0].Columns["GAM_Code"].ValueList = this.ssDropGAM;
            ssGridGAI.DisplayLayout.DefaultSelectedBackColor = Color.Empty;
            ssGridGAI.DisplayLayout.DefaultSelectedForeColor = Color.Empty;
            ssGridGAI.DisplayLayout.Override.ResetActiveRowAppearance();
            ssGridGAI.DisplayLayout.Override.ResetActiveRowCellAppearance();
            ssGridGAI.DisplayLayout.Override.ResetActiveCellAppearance();
            ssGridGAI.DisplayLayout.Bands[0].Columns["GAI_ID"].Hidden = true;
            ssGridGAI.DisplayLayout.Bands[0].Columns["GAI_Compteur"].Hidden = true;
            ssGridGAI.DisplayLayout.Bands[0].Columns["GAM_Code"].Header.Caption = "Code";
            ssGridGAI.DisplayLayout.Bands[0].Columns["GAI_Libelle"].Header.Caption = "Libellé";
            ssGridGAI.DisplayLayout.Bands[0].Columns["PDAB_NoAuto"].Hidden = true;
            ssGridGAI.DisplayLayout.Bands[0].Columns["GAI_Select"].Hidden = true;

            ssGridGAI.DisplayLayout.Bands[0].Columns["GAI_NePasPlanifier"].Header.Caption = "Compteur à relever";
            ssGridGAI.DisplayLayout.Bands[0].Columns["GAI_NePasPlanifier"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridGAI.DisplayLayout.Bands[0].Columns["GAI_NePasPlanifier"].Editor.DataFilter = new BooleanColumnDataFilter();
            ssGridGAI.DisplayLayout.Bands[0].Columns["COP_NoAuto"].Hidden = true;
            ssGridGAI.DisplayLayout.Bands[0].Columns["GAI_CompteurObli"].Header.Caption = "Compteur Obligatoire";
            ssGridGAI.DisplayLayout.Bands[0].Columns["GAI_CompteurObli"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridGAI.DisplayLayout.Bands[0].Columns["GAI_CompteurObli"].Editor.DataFilter = new BooleanColumnDataFilter();
            ssGridGAI.DisplayLayout.Bands[0].Columns["GAI_NoLigne"].Header.Caption = "N°ligne";
            ssGridGAI.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Hidden = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssICA_ImmCategorieInterv_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["CAI_Code"].ValueList = this.ssDROP_CAI;
            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["ICC_NoAuto"].Hidden = true;
            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["ICC_Select"].Header.Caption = "Sélection";
            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["ICC_Select"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["ICC_Select"].Editor.DataFilter = new BooleanColumnDataFilter();
            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["CAI_Code"].Header.Caption = "Code";
            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["CAI_Libelle"].Header.Caption = "Libellé";
            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Hidden = true;

            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["ICC_Cout"].Hidden = true;
            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["ICC_NoLigne"].Header.Caption = "N°Ligne";
            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["ICC_Affiche"].Hidden = true;
            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["COP_NoAuto"].Hidden = true;
            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["COS_NoAuto"].Hidden = true;
            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["PDAB_NoAuto"].Hidden = true;
            ssICA_ImmCategorieInterv.DisplayLayout.Bands[0].Columns["GAI_ID"].Hidden = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPEI_PeriodeGammeImm_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Intervenant"].ValueList = this.ssDropIntervenant;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["EQM_NoAuto"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_NoAuto"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["TPP_Code"].Header.Caption = "Périodicité";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["TPP_Code"].ValueList = ssDropTPP_TypePeriodeP2;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Cycle"].Header.Caption = "Cycle";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_DateDebut"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_DateFin"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_JD"].Header.Caption = "Jour Debut";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_MD"].Header.Caption = "Mois Debut";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_JF"].Header.Caption = "Jour Fin";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_MF"].Header.Caption = "Mois Fin";
            this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_JD"].ValueList = this.ssDropJour;
            this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_JF"].ValueList = this.ssDropJour;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_MD"].ValueList = ssDropMois;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_MF"].ValueList = ssDropMois;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_PassageObli"].Header.Caption = "Passage Obligatoire";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_PassageObli"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_PassageObli"].Editor.DataFilter = new BooleanColumnDataFilter();
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_ForceJournee"].Header.Caption = "Jour imposé";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_ForceJournee"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_ForceJournee"].Editor.DataFilter = new BooleanColumnDataFilter();
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["SEM_Code"].Header.Caption = "Semaine";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["SEM_Code"].ValueList = ssDropSemaine;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Heure"].Header.Caption = "Heure";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Duree"].Header.Caption = "Duree";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_DureeReel"].Header.Caption = "Durée Planning";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_DureeVisu"].Header.Caption = "Durée(hh:mm:ss)";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Samedi"].Header.Caption = "Samedi non inclus";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Samedi"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Samedi"].Editor.DataFilter = new BooleanColumnDataFilter();
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Dimanche"].Header.Caption = "Dimanche non inclus";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Dimanche"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Dimanche"].Editor.DataFilter = new BooleanColumnDataFilter();
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Pentecote"].Header.Caption = "Pentecote non inclus";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Pentecote"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Pentecote"].Editor.DataFilter = new BooleanColumnDataFilter();
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Jourferiee"].Header.Caption = "Fériées non inclus";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Jourferiee"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Jourferiee"].Editor.DataFilter = new BooleanColumnDataFilter();
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Visite"].Header.Caption = "Nbre visite";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_ForceVisite"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Tech"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Cout"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_MtAchat"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_KFO"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_KMO"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Calcul"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_TotalHT"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_IntAutre"].Header.Caption = "Autre";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_IntAutre"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Intervenant"].Header.Caption = "Intervenant";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_APlanifier"].Header.Caption = "Ne pas planifier";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_APlanifier"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["COP_NoAuto"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_AvisPassage"].Header.Caption = "Avis de passage";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_AvisPassage"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_AvisNbJour"].Header.Caption = "Nb Jours";
   
        
        }

        private void ssGridGAI_Error(object sender, ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.Data)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + e.DataErrorInfo.Cell.Column.Header.Caption;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }

        private void ssGridGAI_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            //if (e.ReInitialize)
            //    return;

            //if (e.Row.Cells["GAI_NePasPlanifier"].Value == DBNull.Value || e.Row.Cells["GAI_NePasPlanifier"].Value.ToString() == "0")
            //{
            //    e.Row.Cells["GAI_NePasPlanifier"].Value = false;
            //}
            //if (e.Row.Cells["GAI_CompteurObli"].Value == DBNull.Value || e.Row.Cells["GAI_CompteurObli"].Value.ToString() == "0")
            //{
            //    e.Row.Cells["GAI_CompteurObli"].Value = false;
            //}

         // ssGridGAI.UpdateData();
        }

        private void ssICA_ImmCategorieInterv_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.ReInitialize)
                return;

            if (e.Row.Cells["ICC_Select"].Value == DBNull.Value || e.Row.Cells["ICC_Select"].Value.ToString() == "0")
            {
                e.Row.Cells["ICC_Select"].Value = false;
            }
           // ssICA_ImmCategorieInterv.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssEQU_EquipementP2Imm_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.ReInitialize)
                return;

            if (e.Row.Cells["EQU_Select"].Value == DBNull.Value || e.Row.Cells["EQU_Select"].Value.ToString() == "0")
            {
                e.Row.Cells["EQU_Select"].Value = false;
            }

           // ssEQU_EquipementP2Imm.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGAI_AfterRowsDeleted(object sender, EventArgs e)
        {
            tmpAdorsGai.Update();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssICA_ImmCategorieInterv_AfterRowsDeleted(object sender, EventArgs e)
        {
            tmpAdorsICA_ImmCategorieInterv.Update();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGAI_AfterRowActivate(object sender, EventArgs e)
        {
            fc_ICA_ImmCategorieInterv();
            fc_EQM_EquipementP2Imm();
            fc_DROP_CAI();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssICA_ImmCategorieInterv_AfterRowActivate(object sender, EventArgs e)
        {
            fc_EQM_EquipementP2Imm();
        }

        private void ssPEI_PeriodeGammeImm_AfterRowActivate(object sender, EventArgs e)
        {
            fc_CtrlssPEI_PeriodeGammeImm();
        }

        private void ssEQU_EquipementP2Imm_AfterRowActivate(object sender, EventArgs e)
        {

             fc_Rowcolchange();
      
        }
     
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssEQU_EquipementP2Imm_AfterRowsDeleted(object sender, EventArgs e)
        {
            tmpAdorsEQM_EquipementP2Imm.Update();
        }
        /// <summary>
        /// /tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssEQU_EquipementP2Imm_AfterRowUpdate(object sender, RowEventArgs e)
        {
            tmpAdorsEQM_EquipementP2Imm.Update();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPEI_PeriodeGammeImm_AfterRowUpdate(object sender, RowEventArgs e)
        {
            tmpAdorsPEI_PeriodeGammeImm.Update();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPEI_PeriodeGammeImm_AfterRowsDeleted(object sender, EventArgs e)
        {
            tmpAdorsPEI_PeriodeGammeImm.Update();
        }

        private void ssGridGAI_AfterExitEditMode(object sender, EventArgs e)
        {
            var _with8 = ssGridGAI;

            if (_with8.ActiveRow.Cells["GAM_Code"].Column.Index == _with8.ActiveCell.Column.Index)//tested
            {
                fc_findGAI_Lib();
            }

            if (string.IsNullOrEmpty(_with8.ActiveRow.Cells["Codeimmeuble"].Text))//tested
            {
                _with8.ActiveRow.Cells["Codeimmeuble"].Value = txtCodeImmeuble.Text;
            }

            if (string.IsNullOrEmpty(_with8.ActiveRow.Cells["COP_NoAuto"].Text))//tested
            {
                _with8.ActiveRow.Cells["COP_NoAuto"].Value = txtCop_NoAuto.Text;
            }

            if (string.IsNullOrEmpty(_with8.ActiveRow.Cells["GAI_NoLigne"].Text) && !string.IsNullOrEmpty(_with8.ActiveRow.Cells["GAM_Code"].Text))//tested
            {
                lGAI = lGAI + 1;
                _with8.ActiveRow.Cells["GAI_NoLigne"].Value = lGAI;
            }

            //If .Columns("ICC_Affiche").Text = "-1" Then
            //    .Columns("ICC_Affiche").value = 1
            //End If

            if (_with8.ActiveCell.Column.Index == _with8.ActiveRow.Cells["GAM_Code"].Column.Index)//tested
            {
                _with8.ActiveRow.Cells["GAM_Code"].Value = _with8.ActiveRow.Cells["GAM_Code"].Text.ToString().ToUpper();
            }

            //If ColIndex = 6 Then 'P3
            //    If .Columns(ColIndex).value = "-1" Then
            //        .Columns("ICC_P3traite").value = vbUnchecked
            //    End If
            //End If

            if (General.sPriseEnCompteLocalP2 == "1")//tested
            {
                if (string.IsNullOrEmpty(_with8.ActiveRow.Cells["PDAB_Noauto"].Text))
                {
                    _with8.ActiveRow.Cells["PDAB_Noauto"].Value = txtPDAB_Noauto.Text;
                }
            }
           
           
        }

        private void ssEQU_EquipementP2Imm_AfterExitEditMode(object sender, EventArgs e)
        {
            string sSql = null;
            DataTable rs = default(DataTable);
            var _with16 = ssEQU_EquipementP2Imm;

            if (string.IsNullOrEmpty(_with16.ActiveRow.Cells["ICC_NoAuto"].Text))
            {
                ssEQU_EquipementP2Imm.ActiveRow.Cells["ICC_NoAuto"].Value = ssICA_ImmCategorieInterv.ActiveRow.Cells["ICC_NoAuto"].Text;
            }
            if (string.IsNullOrEmpty(ssEQU_EquipementP2Imm.ActiveRow.Cells["CodeImmeuble"].Text))
            {
                ssEQU_EquipementP2Imm.ActiveRow.Cells["Codeimmeuble"].Value = txtCodeImmeuble.Text;
            }
            if (string.IsNullOrEmpty(ssEQU_EquipementP2Imm.ActiveRow.Cells["EQM_NoLigne"].Text)  )
            {
                lEquip = lEquip + 1;
                ssEQU_EquipementP2Imm.ActiveRow.Cells["EQM_NoLigne"].Value = Convert.ToString(lEquip);
            }
           
            if (string.IsNullOrEmpty(ssEQU_EquipementP2Imm.ActiveRow.Cells["COP_NoAuto"].Text))
            {
                ssEQU_EquipementP2Imm.ActiveRow.Cells["COP_NoAuto"].Value = txtCop_NoAuto.Text;
            }
          
            if (ssEQU_EquipementP2Imm.ActiveCell.Column.Index == ssEQU_EquipementP2Imm.ActiveRow.Cells["EQM_Code"].Column.Index)
            {
                //    ssEQU_EquipementP2Imm.Columns("EQM_Code").Value = UCase(ssEQU_EquipementP2Imm.Columns("EQM_Code").Value)
            }
           
            if (ssEQU_EquipementP2Imm.ActiveCell.Column.Index == _with16.ActiveRow.Cells["EQM_Code"].Column.Index)
            {
                sSql = "SELECT   EQU_NoAuto, EQU_NoLigne," + " EQU_Observation, Duree, DureePlanning  "
                    + " From EQU_EquipementP2"
                    + " WHERE  EQU_Code ='" + StdSQLchaine.gFr_DoublerQuote(_with16.ActiveRow.Cells["EQm_Code"].Text) + "'";
                var tmpAdo = new ModAdo();
                rs = tmpAdo.fc_OpenRecordSet(sSql);

                if (rs.Rows.Count > 0)
                {
                    _with16.ActiveRow.Cells["EQU_NoAuto"].Value = General.nz(rs.Rows[0]["EQU_NoAuto"], 0);
                    _with16.ActiveRow.Cells["EQM_Observation"].Value = rs.Rows[0]["EQU_Observation"] + "";
                    _with16.ActiveRow.Cells["Duree"].Value = General.nz(rs.Rows[0]["Duree"], 0);
                    _with16.ActiveRow.Cells["DureePlanning"].Value = General.nz(rs.Rows[0]["DureePlanning"], 0);
                }
                rs.Dispose();
                rs = null;
            }
            if (ssEQU_EquipementP2Imm.ActiveCell.Column.Index == _with16.ActiveRow.Cells["CTR_Libelle"].Column.Index)
            {
                _with16.ActiveRow.Cells["CTR_ID"].Value = General.nz(ModP2v2.fc_IdControle(_with16.ActiveRow.Cells["CTR_Libelle"].Text), 0);
            }
            if (ssEQU_EquipementP2Imm.ActiveCell.Column.Index == _with16.ActiveRow.Cells["MOY_Libelle"].Column.Index)
            {
                _with16.ActiveRow.Cells["MOY_ID"].Value = General.nz(ModP2v2.fc_IdMoyen(_with16.ActiveRow.Cells["MOY_Libelle"].Text), 0);
            }
            if (ssEQU_EquipementP2Imm.ActiveCell.Column.Index == _with16.ActiveRow.Cells["ANO_Libelle"].Column.Index)
            {
                _with16.ActiveRow.Cells["ANO_ID"].Value = General.nz(ModP2v2.fc_IdAnomalie(_with16.ActiveRow.Cells["ANO_Libelle"].Text), 0);
            }

            if (ssEQU_EquipementP2Imm.ActiveCell.Column.Index == _with16.ActiveRow.Cells["OPE_Libelle"].Column.Index)
            {
                _with16.ActiveRow.Cells["OPE_ID"].Value = General.nz(ModP2v2.fc_IDOperation(_with16.ActiveRow.Cells["OPE_Libelle"].Text), 0);
            }
        }

        private void ssPEI_PeriodeGammeImm_AfterExitEditMode(object sender, EventArgs e)
        {
            
                int i = 0;
                System.DateTime sDeb = default(System.DateTime);
                System.DateTime sFin = default(System.DateTime);
                System.DateTime dtTemp = default(System.DateTime);
                double dVisite = 0;
                string sInterv = null;
                try
                {
               
                   var row = ssPEI_PeriodeGammeImm.ActiveRow;
                    //controle certains elements dans la grille.
                    fc_CtrlssPEI_PeriodeGammeImm(row);//tested

                    //-- si la columns PEI_IntAutre <> 1 alors on récupére l'intervenant par defaut

                    if (string.IsNullOrEmpty(row.Cells["COP_NoAuto"].Text))//tested
                    {
                        row.Cells["COP_NoAuto"].Value = Convert.ToInt32(General.nz(txtCop_NoAuto.Text, 0).ToString());
                    }
                    if (string.IsNullOrEmpty(row.Cells["CodeImmeuble"].Text))//tested
                    {
                        row.Cells["CodeImmeuble"].Value = txtCodeImmeuble.Text;
                    }
                    if (string.IsNullOrEmpty(row.Cells["PEI_DureeReel"].Text))//tested
                    {
                        row.Cells["PEI_DureeReel"].Value = row.Cells["PEI_Duree"].Value;
                    }

                if (row.Cells["PEI_DureeReel"].IsActiveCell)
                {
                    if (General.IsNumeric(row.Cells["PEI_DureeReel"].Value.ToString()))
                    {
                        row.Cells["PEI_DureeVisu"].Value =
                               DateTime.Now.Date.AddSeconds(Convert.ToDouble(row.Cells["PEI_DureeReel"].Value.ToString()) *3600);
                    }
                }
                //=== recherche l'intervenant par defaut.
                if (string.IsNullOrEmpty(row.Cells["PEI_intAutre"].Text) || row.Cells["PEI_intAutre"].Text == "0")
                    {
                        if (string.IsNullOrEmpty(row.Cells["PEI_Intervenant"].Text) || row.Cells["PEI_Intervenant"].IsActiveCell)

                        {

                        row.Cells["PEI_Intervenant"].Value = modP2.FC_intervenantImm(txtCodeImmeuble.Text);
                        }

                    }

                 

                    if (string.IsNullOrEmpty(row.Cells["EQM_NoAuto"].Text))//tested
                    {
                        row.Cells["EQM_NoAuto"].Value = Convert.ToInt32(ssEQU_EquipementP2Imm.ActiveRow.Cells["EQM_NoAuto"].Value);
                    }


                    if (row.Cells["PEI_AvisPassage"].IsActiveCell)
                    {
                        if (row.Cells["PEI_AvisPassage"].Text == "-1")
                        {
                            row.Cells["PEI_AvisPassage"].Value = 1;
                        }
                        else
                        {
                            row.Cells["PEI_AvisNbJour"].Value = 0;
                        }
                    }

                    //==== valeur par défaut des paramétres GMAO.
                    //=== samedi.
                    if (string.IsNullOrEmpty(row.Cells["PEI_Samedi"].Text))
                    {
                        row.Cells["PEI_Samedi"].Value = modP2.tGmao.lSamedi;
                    }
                  

                    //=== Dimanche.
                    if (string.IsNullOrEmpty(row.Cells["PEI_Dimanche"].Text))
                    {
                        row.Cells["PEI_Dimanche"].Value = modP2.tGmao.lDimanche;
                    }
                   
                    //=== pentecote.
                    if (string.IsNullOrEmpty(row.Cells["PEI_Pentecote"].Text))
                    {
                        row.Cells["PEI_Pentecote"].Value = modP2.tGmao.lPentecote;
                    }
                   

                    //=== jours fériés.
                    if (string.IsNullOrEmpty(row.Cells["PEI_Jourferiee"].Text))
                    {
                        row.Cells["PEI_Jourferiee"].Value = modP2.tGmao.lJoursFeriees;
                    }
                  
                   

                    if (row.Cells["TPP_CODE"].IsActiveCell && row.Cells["TPP_CODE"].DataChanged)
                    {

                        row.Cells["PEI_JD"].Value = DBNull.Value;
                        row.Cells["PEI_JF"].Value = DBNull.Value;
                        row.Cells["PEI_MD"].Value = DBNull.Value;
                        row.Cells["PEI_MF"].Value = DBNull.Value;
                        row.Cells["SEM_Code"].Value = DBNull.Value;

                        for (i = 0; i <= ssDropTPP_TypePeriodeP2.Rows.Count - 1; i++)
                        {
                            if (row.Cells["TPP_CODE"].Value.ToString() == ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_CODE"].Value.ToString())
                            {
                            if (ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_JD"].Value != DBNull.Value)
                            {
                                row.Cells["PEI_JD"].Value = Convert.ToInt32(ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_JD"].Value);
                            }
                               
                                if (ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_JF"].Value != DBNull.Value)
                            {
                                row.Cells["PEI_JF"].Value = Convert.ToInt32(ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_JF"].Value);
                            }
                             
                                if (ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_MD"].Value != DBNull.Value)
                            {
                                row.Cells["PEI_MD"].Value = Convert.ToInt32(ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_MD"].Value);
                            }
                                
                                if (ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_Mf"].Value != DBNull.Value)
                            {
                                row.Cells["PEI_MF"].Value = Convert.ToInt32(ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_Mf"].Value);
                            }
                                
                                break;
                            }

                        }

                    }

                    if ((row.Cells["PEI_JD"].IsActiveCell || row.Cells["PEI_MD"].IsActiveCell)
                        && row.Cells["TPP_Code"].Text.ToUpper() == "Annuel".ToUpper())
                    {
                        if ((string.IsNullOrEmpty(row.Cells["PEI_MD"].Text)))
                        {
                            row.Cells["PEI_JF"].Value = fDate.fc_FinDeMois(row.Cells["PEI_MD"].Text, DateTime.Today.Month.ToString()).Day;
                            row.Cells["PEI_MF"].Value = row.Cells["PEI_MD"].Value;
                        }
                    }

                    //controle si la date du jour et du mois sont cohérent pour le jour debut et le mois debut.
                    if (!string.IsNullOrEmpty(row.Cells["PEI_JD"].Text) && !string.IsNullOrEmpty(row.Cells["PEI_MD"].Text)
                        && fDate.IsValidMonthAndYear(Convert.ToInt32(row.Cells["PEI_MD"].Text), DateTime.Now.Year))
                    {
                        if (!General.IsDate(row.Cells["PEI_JD"].Text + "/" + row.Cells["PEI_MD"].Text + "/" + DateTime.Now.Year))
                        {
                            row.Cells["PEI_JD"].Value = Math.Min(Convert.ToInt32(row.Cells["PEI_JD"].Text), DateTime.DaysInMonth(DateTime.Today.Year, Convert.ToInt32(row.Cells["PEI_MD"].Text)));
                        }

                    }

                    //controle si la date du jour et du mois sont cohérent pour le jour debut et le mois fin.
                    if (!string.IsNullOrEmpty(row.Cells["PEI_JF"].Text) && !string.IsNullOrEmpty(row.Cells["PEI_MF"].Text)
                        && fDate.IsValidMonthAndYear(Convert.ToInt32(row.Cells["PEI_MF"].Text), DateTime.Now.Year))
                    {
                        if (!General.IsDate(row.Cells["PEI_JF"].Text + "/" + row.Cells["PEI_MF"].Text + "/" + DateTime.Now.Year))
                        {
                            row.Cells["PEI_JF"].Value = Math.Min(Convert.ToInt32(row.Cells["PEI_JF"].Text), DateTime.DaysInMonth(DateTime.Today.Year, Convert.ToInt32(row.Cells["PEI_MF"].Text)));
                        }

                    }

                    // met la valeur A (automatique) si le champs PEI_ForceVisite est à vide
                    if (string.IsNullOrEmpty(row.Cells["PEI_ForceVisite"].Text))
                    {
                        row.Cells["PEI_ForceVisite"].Value = "A";
                    }

                //'==== contrôle que la date de début et la date de fin ne dépasse pas une année et contrôle que la
                //'==== date de début est plus petite que la date de fin.
                if (row.Cells["PEI_DateDebut"].IsActiveCell ||  row.Cells["PEI_dateFin"].IsActiveCell)
                {
                    if (General.IsDate(row.Cells["PEI_DateDebut"].Value.ToString()) && General.IsDate(row.Cells["PEI_DateFin"].Value.ToString()))
                    {
                        sDeb = Convert.ToDateTime(row.Cells["PEI_DateDebut"].Value.ToString());
                        sFin = Convert.ToDateTime(row.Cells["PEI_DateFin"].Value.ToString());
                        dtTemp = sDeb.AddYears(1).AddDays(-1);

                        if (sFin > dtTemp)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                                "Erreur de saisie La différence de jours entre la date de début et la date de fin ne doit pas excéder une année.",
                                "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }
                        if (sFin < sDeb)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                                "Erreur de saisie la date de début doit être antérieur à la date de fin.",
                                "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                            return;
                        }
                    }

                    if (General.IsDate(row.Cells["PEI_DateDebut"].Value.ToString()))
                    {
                        sDeb = Convert.ToDateTime(row.Cells["PEI_DateDebut"].Value.ToString());

                        if (sDeb < Convert.ToDateTime(txtCOP_DebutPlanification.Text))
                        {
                            MessageBox.Show(
                                "Erreur de saisie La date de début doit être postérieur ou égale à la date de début de planification.");
                            return;
                        }
                    }
                }

                if (row.Cells["PEI_ForceVisite"].Text.ToUpper() == "A".ToUpper())
                    {
                        if (!string.IsNullOrEmpty(row.Cells["PEI_JD"].Text)
                            && !string.IsNullOrEmpty(row.Cells["PEI_JF"].Text)
                            && !string.IsNullOrEmpty(row.Cells["PEI_MD"].Text)
                            && !string.IsNullOrEmpty(row.Cells["PEI_MF"].Text)
                            && !string.IsNullOrEmpty(row.Cells["TPP_Code"].Text))
                        {
                            sDeb = Convert.ToDateTime(row.Cells["PEI_JD"].Text + "/" + row.Cells["PEI_MD"].Text + "/" + DateTime.Now.Year);

                            if (Convert.ToDouble(row.Cells["PEI_MF"].Text) < Convert.ToDouble(row.Cells["PEI_MD"].Text))
                            {
                                sFin = Convert.ToDateTime(row.Cells["PEI_JF"].Text + "/" + row.Cells["PEI_MF"].Text + "/" + DateTime.Now.Year + 1);
                            }
                            else//tested
                            {
                                sFin = Convert.ToDateTime(row.Cells["PEI_JF"].Text + "/" + row.Cells["PEI_MF"].Text + "/" + DateTime.Now.Year);
                            }

                            switch (row.Cells["TPP_Code"].Text.ToUpper())

                            {
                                case "HEBDOMADAIRE":
                                    {
                                        row.Cells["peI_Visite"].Value = Math.Floor((sFin - sDeb).TotalDays / 7);
                                        break;
                                    }
                                case "MENSUEL":
                                    {
                                        dVisite = Math.Floor((double)General.TotalMonths(sDeb, sFin) + 1);
                                        row.Cells["peI_Visite"].Value = dVisite;
                                        break;
                                    }

                                case "BIMENSUEL":
                                    {
                                        row.Cells["peI_Visite"].Value = Math.Floor((sFin - sDeb).TotalDays / 14);
                                        break;
                                    }

                                case "TRIMESTRIEL":
                                    {
                                        dVisite = General.TotalMonths(sDeb, sFin) + 1;
                                        row.Cells["peI_Visite"].Value = Math.Floor(dVisite / 3) == dVisite / 3
                                            ? Math.Floor(dVisite / 3)
                                            : Math.Floor(dVisite / 3) + 1;
                                        break;
                                    }

                                case "SEMESTRIEL":
                                    {
                                        dVisite = General.TotalMonths(sDeb, sFin) + 1;
                                        row.Cells["peI_Visite"].Value = Math.Floor(dVisite / 6) == dVisite / 6
                                            ? Math.Floor(dVisite / 6)
                                            : Math.Floor(dVisite / 6) + 1;
                                        break;
                                    }

                                case "ANNUEL":
                                    {
                                        dVisite = 1;
                                        row.Cells["peI_Visite"].Value = dVisite;

                                        break;
                                    }
                            }


                            if (row.Cells["TPP_Code"].Text.ToUpper() == modP2.cBiMestriel.ToUpper())
                            {
                                dVisite = General.TotalMonths(sDeb, sFin) + 1;
                                row.Cells["peI_Visite"].Value = Math.Floor(dVisite / 2) == dVisite / 2
                                    ? Math.Floor(dVisite / 2)
                                    : Math.Floor(dVisite / 2) + 1;
                            }


                        }

                    }



                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "ssPEI_PeriodeGammeImm_BeforeExitEditMode");
            }
        }
    }
}
