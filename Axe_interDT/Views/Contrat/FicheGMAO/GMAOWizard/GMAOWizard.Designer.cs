﻿
namespace Axe_interDT.Views.Contrat.FicheGMAO.GMAOWizard
{
    partial class GMAOWizard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.iTalk_TextBox_Small21 = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.iTalk_TextBox_Small22 = new iTalk.iTalk_TextBox_Small2();
            this.iTalk_TextBox_Small23 = new iTalk.iTalk_TextBox_Small2();
            this.cmdAvenant = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.checkBox2 = new System.Windows.Forms.CheckBox();
            this.checkBox3 = new System.Windows.Forms.CheckBox();
            this.checkBox4 = new System.Windows.Forms.CheckBox();
            this.iTalk_RichTextBox1 = new iTalk.iTalk_RichTextBox();
            this.button2 = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.iTalk_TextBox_Small24 = new iTalk.iTalk_TextBox_Small2();
            this.ugResultat = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.button3 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ugResultat)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 12F);
            this.label1.Location = new System.Drawing.Point(3, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(121, 20);
            this.label1.TabIndex = 0;
            this.label1.Text = "Code Immeuble";
            // 
            // iTalk_TextBox_Small21
            // 
            this.iTalk_TextBox_Small21.AccAcceptNumbersOnly = false;
            this.iTalk_TextBox_Small21.AccAllowComma = false;
            this.iTalk_TextBox_Small21.AccBackgroundColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.iTalk_TextBox_Small21.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.iTalk_TextBox_Small21.AccHidenValue = "";
            this.iTalk_TextBox_Small21.AccNotAllowedChars = null;
            this.iTalk_TextBox_Small21.AccReadOnly = false;
            this.iTalk_TextBox_Small21.AccReadOnlyAllowDelete = false;
            this.iTalk_TextBox_Small21.AccRequired = false;
            this.iTalk_TextBox_Small21.BackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.CustomBackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.iTalk_TextBox_Small21.ForeColor = System.Drawing.Color.Black;
            this.iTalk_TextBox_Small21.Location = new System.Drawing.Point(185, 31);
            this.iTalk_TextBox_Small21.Margin = new System.Windows.Forms.Padding(2);
            this.iTalk_TextBox_Small21.MaxLength = 32767;
            this.iTalk_TextBox_Small21.Multiline = false;
            this.iTalk_TextBox_Small21.Name = "iTalk_TextBox_Small21";
            this.iTalk_TextBox_Small21.ReadOnly = false;
            this.iTalk_TextBox_Small21.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.iTalk_TextBox_Small21.Size = new System.Drawing.Size(338, 27);
            this.iTalk_TextBox_Small21.TabIndex = 1;
            this.iTalk_TextBox_Small21.TextAlignment = Infragistics.Win.HAlign.Default;
            this.iTalk_TextBox_Small21.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 12F, System.Drawing.FontStyle.Underline);
            this.label2.ForeColor = System.Drawing.Color.Blue;
            this.label2.Location = new System.Drawing.Point(3, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(65, 20);
            this.label2.TabIndex = 2;
            this.label2.Text = "Contrat";
            // 
            // iTalk_TextBox_Small22
            // 
            this.iTalk_TextBox_Small22.AccAcceptNumbersOnly = false;
            this.iTalk_TextBox_Small22.AccAllowComma = false;
            this.iTalk_TextBox_Small22.AccBackgroundColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small22.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.iTalk_TextBox_Small22.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.iTalk_TextBox_Small22.AccHidenValue = "";
            this.iTalk_TextBox_Small22.AccNotAllowedChars = null;
            this.iTalk_TextBox_Small22.AccReadOnly = false;
            this.iTalk_TextBox_Small22.AccReadOnlyAllowDelete = false;
            this.iTalk_TextBox_Small22.AccRequired = false;
            this.iTalk_TextBox_Small22.BackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small22.CustomBackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small22.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.iTalk_TextBox_Small22.ForeColor = System.Drawing.Color.Black;
            this.iTalk_TextBox_Small22.Location = new System.Drawing.Point(185, 62);
            this.iTalk_TextBox_Small22.Margin = new System.Windows.Forms.Padding(2);
            this.iTalk_TextBox_Small22.MaxLength = 32767;
            this.iTalk_TextBox_Small22.Multiline = false;
            this.iTalk_TextBox_Small22.Name = "iTalk_TextBox_Small22";
            this.iTalk_TextBox_Small22.ReadOnly = false;
            this.iTalk_TextBox_Small22.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.iTalk_TextBox_Small22.Size = new System.Drawing.Size(338, 27);
            this.iTalk_TextBox_Small22.TabIndex = 3;
            this.iTalk_TextBox_Small22.TextAlignment = Infragistics.Win.HAlign.Default;
            this.iTalk_TextBox_Small22.UseSystemPasswordChar = false;
            // 
            // iTalk_TextBox_Small23
            // 
            this.iTalk_TextBox_Small23.AccAcceptNumbersOnly = false;
            this.iTalk_TextBox_Small23.AccAllowComma = false;
            this.iTalk_TextBox_Small23.AccBackgroundColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small23.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.iTalk_TextBox_Small23.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.iTalk_TextBox_Small23.AccHidenValue = "";
            this.iTalk_TextBox_Small23.AccNotAllowedChars = null;
            this.iTalk_TextBox_Small23.AccReadOnly = false;
            this.iTalk_TextBox_Small23.AccReadOnlyAllowDelete = false;
            this.iTalk_TextBox_Small23.AccRequired = false;
            this.iTalk_TextBox_Small23.BackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small23.CustomBackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small23.Enabled = false;
            this.iTalk_TextBox_Small23.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.iTalk_TextBox_Small23.ForeColor = System.Drawing.Color.Black;
            this.iTalk_TextBox_Small23.Location = new System.Drawing.Point(527, 62);
            this.iTalk_TextBox_Small23.Margin = new System.Windows.Forms.Padding(2);
            this.iTalk_TextBox_Small23.MaxLength = 32767;
            this.iTalk_TextBox_Small23.Multiline = false;
            this.iTalk_TextBox_Small23.Name = "iTalk_TextBox_Small23";
            this.iTalk_TextBox_Small23.ReadOnly = true;
            this.iTalk_TextBox_Small23.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.iTalk_TextBox_Small23.Size = new System.Drawing.Size(493, 27);
            this.iTalk_TextBox_Small23.TabIndex = 4;
            this.iTalk_TextBox_Small23.TextAlignment = Infragistics.Win.HAlign.Default;
            this.iTalk_TextBox_Small23.UseSystemPasswordChar = false;
            // 
            // cmdAvenant
            // 
            this.cmdAvenant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAvenant.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAvenant.FlatAppearance.BorderSize = 0;
            this.cmdAvenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAvenant.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.cmdAvenant.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.cmdAvenant.Location = new System.Drawing.Point(1024, 62);
            this.cmdAvenant.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAvenant.Name = "cmdAvenant";
            this.cmdAvenant.Size = new System.Drawing.Size(118, 27);
            this.cmdAvenant.TabIndex = 363;
            this.cmdAvenant.Text = "Compte rendu";
            this.cmdAvenant.UseVisualStyleBackColor = false;
            this.cmdAvenant.Visible = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.button1.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button1.Location = new System.Drawing.Point(1146, 62);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(118, 27);
            this.button1.TabIndex = 364;
            this.button1.Text = "GMAO";
            this.button1.UseVisualStyleBackColor = false;
            this.button1.Visible = false;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Ubuntu", 12F);
            this.checkBox1.Location = new System.Drawing.Point(187, 94);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(47, 24);
            this.checkBox1.TabIndex = 365;
            this.checkBox1.Text = "P1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // checkBox2
            // 
            this.checkBox2.AutoSize = true;
            this.checkBox2.Font = new System.Drawing.Font("Ubuntu", 12F);
            this.checkBox2.Location = new System.Drawing.Point(240, 94);
            this.checkBox2.Name = "checkBox2";
            this.checkBox2.Size = new System.Drawing.Size(47, 24);
            this.checkBox2.TabIndex = 366;
            this.checkBox2.Text = "P2";
            this.checkBox2.UseVisualStyleBackColor = true;
            // 
            // checkBox3
            // 
            this.checkBox3.AutoSize = true;
            this.checkBox3.Font = new System.Drawing.Font("Ubuntu", 12F);
            this.checkBox3.Location = new System.Drawing.Point(293, 94);
            this.checkBox3.Name = "checkBox3";
            this.checkBox3.Size = new System.Drawing.Size(47, 24);
            this.checkBox3.TabIndex = 367;
            this.checkBox3.Text = "P3";
            this.checkBox3.UseVisualStyleBackColor = true;
            // 
            // checkBox4
            // 
            this.checkBox4.AutoSize = true;
            this.checkBox4.Font = new System.Drawing.Font("Ubuntu", 12F);
            this.checkBox4.Location = new System.Drawing.Point(346, 94);
            this.checkBox4.Name = "checkBox4";
            this.checkBox4.Size = new System.Drawing.Size(47, 24);
            this.checkBox4.TabIndex = 368;
            this.checkBox4.Text = "P4";
            this.checkBox4.UseVisualStyleBackColor = true;
            // 
            // iTalk_RichTextBox1
            // 
            this.iTalk_RichTextBox1.AccBorderColor = System.Drawing.Color.Gray;
            this.iTalk_RichTextBox1.AutoWordSelection = false;
            this.iTalk_RichTextBox1.BackColor = System.Drawing.Color.Transparent;
            this.iTalk_RichTextBox1.Font = new System.Drawing.Font("Ubuntu", 12F);
            this.iTalk_RichTextBox1.ForeColor = System.Drawing.Color.DimGray;
            this.iTalk_RichTextBox1.Location = new System.Drawing.Point(185, 155);
            this.iTalk_RichTextBox1.Name = "iTalk_RichTextBox1";
            this.iTalk_RichTextBox1.ReadOnly = false;
            this.iTalk_RichTextBox1.Size = new System.Drawing.Size(835, 126);
            this.iTalk_RichTextBox1.TabIndex = 369;
            this.iTalk_RichTextBox1.WordWrap = true;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.button2.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button2.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.button2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button2.Location = new System.Drawing.Point(187, 123);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(245, 27);
            this.button2.TabIndex = 370;
            this.button2.Text = "Synchroniser les prestations contractuelles";
            this.button2.UseVisualStyleBackColor = false;
            this.button2.Visible = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 12F);
            this.label3.Location = new System.Drawing.Point(1078, 155);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(173, 20);
            this.label3.TabIndex = 371;
            this.label3.Text = "Date démarrage presta";
            // 
            // iTalk_TextBox_Small24
            // 
            this.iTalk_TextBox_Small24.AccAcceptNumbersOnly = false;
            this.iTalk_TextBox_Small24.AccAllowComma = false;
            this.iTalk_TextBox_Small24.AccBackgroundColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small24.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.iTalk_TextBox_Small24.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.iTalk_TextBox_Small24.AccHidenValue = "";
            this.iTalk_TextBox_Small24.AccNotAllowedChars = null;
            this.iTalk_TextBox_Small24.AccReadOnly = false;
            this.iTalk_TextBox_Small24.AccReadOnlyAllowDelete = false;
            this.iTalk_TextBox_Small24.AccRequired = false;
            this.iTalk_TextBox_Small24.BackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small24.CustomBackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small24.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.iTalk_TextBox_Small24.ForeColor = System.Drawing.Color.Black;
            this.iTalk_TextBox_Small24.Location = new System.Drawing.Point(1030, 177);
            this.iTalk_TextBox_Small24.Margin = new System.Windows.Forms.Padding(2);
            this.iTalk_TextBox_Small24.MaxLength = 32767;
            this.iTalk_TextBox_Small24.Multiline = false;
            this.iTalk_TextBox_Small24.Name = "iTalk_TextBox_Small24";
            this.iTalk_TextBox_Small24.ReadOnly = false;
            this.iTalk_TextBox_Small24.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.iTalk_TextBox_Small24.Size = new System.Drawing.Size(281, 27);
            this.iTalk_TextBox_Small24.TabIndex = 372;
            this.iTalk_TextBox_Small24.TextAlignment = Infragistics.Win.HAlign.Default;
            this.iTalk_TextBox_Small24.UseSystemPasswordChar = false;
            // 
            // ugResultat
            // 
            this.ugResultat.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.ugResultat.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ugResultat.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ugResultat.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ugResultat.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ugResultat.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ugResultat.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ugResultat.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ugResultat.DisplayLayout.UseFixedHeaders = true;
            this.ugResultat.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ugResultat.Location = new System.Drawing.Point(3, 350);
            this.ugResultat.Name = "ugResultat";
            this.ugResultat.Size = new System.Drawing.Size(1840, 573);
            this.ugResultat.TabIndex = 373;
            this.ugResultat.Text = "Prestations";
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.button3.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button3.Image = global::Axe_interDT.Properties.Resources.Edit_16x16;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(3, 318);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(212, 27);
            this.button3.TabIndex = 374;
            this.button3.Text = "modifier liste localisation";
            this.button3.UseVisualStyleBackColor = false;
            this.button3.Visible = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.button4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.button4.ForeColor = System.Drawing.SystemColors.ButtonHighlight;
            this.button4.Image = global::Axe_interDT.Properties.Resources.Add_File_16x16;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(3, 928);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(212, 27);
            this.button4.TabIndex = 375;
            this.button4.Text = "Ajouter une prestation";
            this.button4.UseVisualStyleBackColor = false;
            this.button4.Visible = false;
            // 
            // GMAOWizard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.ugResultat);
            this.Controls.Add(this.iTalk_TextBox_Small24);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.iTalk_RichTextBox1);
            this.Controls.Add(this.checkBox4);
            this.Controls.Add(this.checkBox3);
            this.Controls.Add(this.checkBox2);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cmdAvenant);
            this.Controls.Add(this.iTalk_TextBox_Small23);
            this.Controls.Add(this.iTalk_TextBox_Small22);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.iTalk_TextBox_Small21);
            this.Controls.Add(this.label1);
            this.Name = "GMAOWizard";
            this.Size = new System.Drawing.Size(1850, 957);
            ((System.ComponentModel.ISupportInitialize)(this.ugResultat)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private iTalk.iTalk_TextBox_Small2 iTalk_TextBox_Small21;
        private System.Windows.Forms.Label label2;
        private iTalk.iTalk_TextBox_Small2 iTalk_TextBox_Small22;
        private iTalk.iTalk_TextBox_Small2 iTalk_TextBox_Small23;
        public System.Windows.Forms.Button cmdAvenant;
        public System.Windows.Forms.Button button1;
        private System.Windows.Forms.CheckBox checkBox1;
        private System.Windows.Forms.CheckBox checkBox2;
        private System.Windows.Forms.CheckBox checkBox3;
        private System.Windows.Forms.CheckBox checkBox4;
        private iTalk.iTalk_RichTextBox iTalk_RichTextBox1;
        public System.Windows.Forms.Button button2;
        private System.Windows.Forms.Label label3;
        private iTalk.iTalk_TextBox_Small2 iTalk_TextBox_Small24;
        public Infragistics.Win.UltraWinGrid.UltraGrid ugResultat;
        public System.Windows.Forms.Button button3;
        public System.Windows.Forms.Button button4;
    }
}
