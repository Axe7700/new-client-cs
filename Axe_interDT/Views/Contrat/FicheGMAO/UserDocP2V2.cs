﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Appel;
using Axe_interDT.Views.BaseDeDonnees.Gerant;
using Axe_interDT.Views.BaseDeDonnees.Immeuble;
using Axe_interDT.Views.BaseDeDonnees.Immeuble.FicheLocalisation;
using Axe_interDT.Views.BaseDeDonnees.ParametreGMAO;
using Axe_interDT.Views.Contrat.FicheGMAO.Forms;
using Axe_interDT.Views.FicheVisiteEntretient;
using Axe_interDT.Views.Theme.CustomMessageBox;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace Axe_interDT.Views.Contrat.FicheGMAO
{
    public partial class UserDocP2V2 : UserControl
    {
        bool blModif;
        bool blnAjout;

        int lICA;
        int lFAP_Article;
        int lEquip;
        int lMateriel;
        int lCOS_ContratChap;
        int lchapGam;
        int lCSS_ContratSchap;

        const string cStyleCompteurObl = "StyleCompteurObl";
        const string cStyletTOTAL = "TOTAL";

        const string cNiv1 = "A";
        const string cNiv2 = "B";
        const string cNiv3 = "C";
        const string cNiv4 = "D";

        const short cTabNiv1 = 0;
        const short cTabNiv2 = 1;
        const short cTabNiv3 = 2;
        const short cTabNiv4 = 3;

        const string cAnnee = "Année";
        const string cMois = "Mois";
        const string cSemaine = "Semaine";
        const string cJour = "Jour";
        const string cHeure = "Heure";
        const short cChapTexte = 0;
        const short cMateriel = 1;
        const short cGamme = 2;
        const short cChapGamme = 3;
        const short cNature = 4;
        const short cPlanning = 5;
        const short cPlannification = 6;
        const short cAddMatGamme = 1;
        const short cAddGamme = 0;
        const string cActif = "Actif";
        const string c04 = "04";
        const string cA = "A";
        const string c00 = "00";
        const string c03 = "03";
        const string cBloqueFicheP2 = "Bloque";
        const short cColWidth = 250;

        DataTable rsTVA;
        DataTable rsPEI_PeriodeGammeImm;
        ModAdo tmpAdorsPEI_PeriodeGammeImm = new ModAdo();

        DataTable rstmp;
        DataTable rsICA_ImmCategorieInterv;
        DataTable rsChapGamme;
        DataTable rsEQM_EquipementP2Imm;
        DataTable rsGammeFille;
        DataTable rsFAP_FacArticleP2;
        DataTable rsCOD;
        DataTable rsVisite;
        DataTable rsRecap;
        DataTable rsGamme;
        DataTable rsPrestations;

        const string cTotal = "TOTAL";
        string StockID;
        private struct idChapitre
        {
            public int ModCOS_NoAuto;
            public int NewCOS_NoAuto;
        }

        idChapitre[] tIDChap;

        private struct CumulHeure
        {
            public string sMat;
            public string sNom;
            public string sPrenom;
            public double dbDuree;
            public double dbVisite;
            public double dbTotal;
        }
        public UserDocP2V2()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCpteRendu_Click(object sender, EventArgs e)
        {
            //verification ligne par ligne
            try
            {
                if (string.IsNullOrEmpty(fc_CptRendu(txtCodeImmeuble.Text)))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'existe pas de relevé technique pour cet immeuble.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                fc_SavParamPos();
                General.saveInReg(Variable.cUserDocRelTech, "NewVar", txtCodeImmeuble.Text);
                General.saveInReg(Variable.cUserDocRelTech, "Origine", this.Name);
                View.Theme.Theme.Navigate(typeof(UserDocRelTech));

                return;

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";btnCpteRendu_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFindContratRef_Click(object sender, EventArgs e)
        {
            //verification ligne par ligne
            string req = "";
            string where = "";
            try
            {
                req = "SELECT NumContrat as \"N°Contrat\", Avenant as \"N°Avenant\", Codeimmeuble as \"Code immeuble\","
                    + " LibelleCont1 as \"Designation1\",  LibelleCont2 as \"Designation2\" FROM Contrat ";

                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un contrat référent" };
                fg.SetValues(new Dictionary<string, string> { { "Codeimmeuble", txtCodeImmeuble.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtNumContratRef.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtAvtRef.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    fc_loadDataCtr(txtNumContratRef.Text, Convert.ToInt32(General.nz(txtAvtRef.Text, 0)));//tested
                    fg.Dispose();
                    fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtNumContratRef.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtAvtRef.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        fc_loadDataCtr(txtNumContratRef.Text, Convert.ToInt32(General.nz(txtAvtRef.Text, 0)));
                        fg.Dispose();
                        fg.Close();
                    }

                };
                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdFindContratRef_Click");
            }

        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sNumContrat"></param>
        /// <param name="lAvenant"></param>
        private void fc_loadDataCtr(string sNumContrat, int lAvenant)
        {

            string sSQL = null;
            DataTable rs = default(DataTable);

            try
            {
                using (var ModAdors = new ModAdo())
                {
                    sSQL = "SELECT     NumContrat, Avenant, LibelleCont1, DateEffet, DateFin, Resiliee  From Contrat "
                    + " WHERE  NumContrat = '" + StdSQLchaine.gFr_DoublerQuote(sNumContrat) + "' AND Avenant = " + lAvenant;

                    rs = ModAdors.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)//tested
                    {
                        txtDesignatioinCtrRef.Text = rs.Rows[0]["LibelleCont1"] + "";
                        txtDateEffetRef.Text = rs.Rows[0]["DateEffet"] != DBNull.Value ? Convert.ToDateTime(rs.Rows[0]["DateEffet"]).ToShortDateString() + "" : "";
                        txtDateFinRef.Text = rs.Rows[0]["DateFin"] != DBNull.Value ? Convert.ToDateTime(rs.Rows[0]["DateFin"]).ToShortDateString() + "" : "";

                        if (Convert.ToBoolean(General.nz(rs.Rows[0]["Resiliee"], false)) == true)
                        {
                            chkResilie.CheckState = System.Windows.Forms.CheckState.Checked;
                        }
                        else
                        {
                            chkResilie.CheckState = System.Windows.Forms.CheckState.Unchecked;
                        }

                    }
                    else
                    {
                        txtDesignatioinCtrRef.Text = "";
                        txtDateEffetRef.Text = "";
                        txtDateFinRef.Text = "";
                        chkResilie.CheckState = CheckState.Unchecked;
                    }
                    //rs.Close();
                    rs = null;
                    return;
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_loadDataCtr");
            }
        }
        /// <summary>
        /// TODO :==========>   frmCreationModeleCtr et frmChoixModeleCOntrat inexistant dans le projet de VB6
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdModeles_Click(object sender, EventArgs e)
        {
            try
            {
                frmCreationModeleCtr frmCreationModeleCtr = new frmCreationModeleCtr();
                frmChoixModeleContrat frm = new frmChoixModeleContrat();
                string sID = null;
                int i = 0;
                string sSQL = null;



                if (fc_Ctrl() == false)
                {
                    return;
                }

                if (fc_CtrlLocTreev(sID) == false)
                {
                    return;
                }
                sID = StockID;
                General.lChoixModele = 0;
                frm.ShowDialog();

                if (General.lChoixModele == 1)
                {
                    ModVisiteP2.bInsertModeleP2 = false;
                    Forms.frmModeleP2 frmModeleP2 = new Forms.frmModeleP2();
                    frmModeleP2.ssGridModele.DataSource = null;//removeAll()
                    ModVisiteP2.tpModInterP2 = null;

                    frmModeleP2.ShowDialog();

                    if (ModVisiteP2.bInsertModeleP2 == true)
                    {
                        for (i = 0; i <= ModVisiteP2.tpModInterP2.Length - 1; i++)
                        {
                            ModVisiteP2.fc_InsertModele(ModVisiteP2.tpModInterP2[i].lCop_NoAuto, ModVisiteP2.tpModInterP2[i].lPDAB_Noauto, Convert.ToInt32(sID), txtCodeImmeuble.Text, Convert.ToInt32(txtCOP_NoAuto.Text), txtCOP_NoContrat.Text);
                            //MsgBox "ok"
                            //fc_InsertModele 7, 1, CLng(sid), txtCodeimmeuble, txtCOP_NoAuto, txtCOP_NoContrat
                        }
                    }

                    fc_loadTreeView(Convert.ToInt32(txtCOP_NoAuto.Text));

                }
                else if (General.lChoixModele == 2)//Tested
                {

                    var _with2 = frmCreationModeleCtr;

                    frmCreationModeleCtr.txtCOP_NoAuto.Text = txtCOP_NoAuto.Text;

                    frmCreationModeleCtr.txtCodeImmeuble.Text = txtCodeImmeuble.Text;

                    frmCreationModeleCtr.txtMVP_CreePar.Text = General.fncUserName();

                    frmCreationModeleCtr.txtMVP_CreeLe.Text = DateTime.Now.ToString();

                    frmCreationModeleCtr.txtPDAB_Noauto.Text = sID;

                    sSQL = "SELECT     PDAB_Libelle From PDAB_BadgePDA WHERE PDAB_Noauto =" + General.nz(StockID, 0);
                    using (var tmpAdo = new ModAdo())
                        sSQL = tmpAdo.fc_ADOlibelle(sSQL);

                    frmCreationModeleCtr.txtLocalisation.Text = sSQL;

                    frmCreationModeleCtr.ShowDialog();



                }

                //fc_CreateModele "CHAUFFAGE", txtCOP_NoAuto, txtCodeImmeuble, CLng(sID)
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdModeles_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Combo0_BeforeDropDown(object sender, CancelEventArgs e)
        {
            var cmd = sender as UltraCombo;
            int Index = Convert.ToInt32(cmd.Tag);
            string sSQL;
            try
            {
                switch (Index)
                {
                    case 0://tested
                        //=== controle.
                        sSQL = "SELECT  CTR_Libelle as Contrôle, CTR_ID From CTR_ControleGmao ORDER BY CTR_Libelle ";
                        sheridan.InitialiseCombo(Combo0, sSQL, "Contrôle");
                        Combo0.DisplayLayout.Bands[0].Columns["CTR_ID"].Hidden = true;
                        Combo0.DisplayLayout.Bands[0].Columns[0].Width = cColWidth; //verifier cette ligne car cColWidth =6000 est trés long
                        break;

                    case 1://tested
                        //=== moyen.
                        sSQL = "SELECT     MOY_Libelle as Moyen, MOY_ID From MOY_MoyenGmao " + " ORDER BY MOY_Libelle";
                        sheridan.InitialiseCombo(Combo1, sSQL, "Moyen");
                        Combo1.DisplayLayout.Bands[0].Columns["MOY_ID"].Hidden = true;
                        Combo1.DisplayLayout.Bands[0].Columns[0].Width = cColWidth; //verifier cette ligne car cColWidth =6000 est trés long
                        break;

                    case 2:
                        //=== Anomalie.//tested
                        sSQL = "SELECT     ANO_Libelle as Anomalie , ANO_ID From ANO_AnomalieGmao " + " ORDER BY ANO_Libelle";
                        sheridan.InitialiseCombo(Combo2, sSQL, "Anomalie");
                        Combo2.DisplayLayout.Bands[0].Columns["ANO_ID"].Hidden = true;
                        Combo2.DisplayLayout.Bands[0].Columns[0].Width = cColWidth; //verifier cette ligne car cColWidth =6000 est trés long
                        break;


                    case 3:
                        //=== Opération.//tested
                        sSQL = "SELECT     OPE_Libelle as Opération, OPE_ID From OPE_OperationGmao " + " ORDER BY OPE_Libelle ";
                        sheridan.InitialiseCombo(Combo3, sSQL, "Opération");
                        Combo3.DisplayLayout.Bands[0].Columns["OPE_ID"].Hidden = true;
                        Combo3.DisplayLayout.Bands[0].Columns[0].Width = cColWidth; //verifier cette ligne car cColWidth =6000 est trés long
                        break;
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Combo_DropDown");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command8_Click(object sender, EventArgs e)
        {
            try
            {
                rstmp = default(DataTable);
                string sSQL = null;
                ModAdo tmpAdo = new ModAdo();
                UltraTreeNode nodeSelect = new UltraTreeNode();

                sSQL = "SELECT     GAi_ID, GAM_Code, GAI_LIbelle, GAI_Compteur, GAI_CompteurObli "
                    + " FROM   GAI_GammeImm" + " WHERE GAi_ID =" + General.nz(txtGAi_ID.Text, 0);

                rstmp = tmpAdo.fc_OpenRecordSet(sSQL);
                if (rstmp.Rows.Count > 0)
                {
                    //txtCAI_Code = nz(rstmp!CAI_Code, "")
                    rstmp.Rows[0]["GAI_LIbelle"] = txtGAI_LIbelle.Text;
                    rstmp.Rows[0]["GAI_Compteur"] = General.nz(chkGAI_Compteur.CheckState, 0);
                    rstmp.Rows[0]["GAI_CompteurObli"] = General.nz(chkGAI_CompteurObli.CheckState, 0);

                    tmpAdo.Update();

                    if (SSTab4.SelectedTab == SSTab4.Tabs[3])//tested
                    {
                        if ((TreeView1.SelectedNodes != null))
                        {
                            TreeView1.SelectedNodes[0].Text = txtGAI_LIbelle.Text;

                        }
                    }

                }
                //rstmp.Close();

                rstmp = null;
                ssPEI_PeriodeGammeImm.Update();
                ssFAP_FacArticleP2.Update();

                return;
            }

            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command8_Click");
            }
        }

        private void lblGoContrat_Click(object sender, EventArgs e)
        {
            //    MsgBox "Fonctionnalité non installée", vbInformation, "Contrat"
            //    Exit Sub
            string sSQL = null;
            string sCodeContrat = null;

            try
            {
                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    sSQL = "";
                    sSQL = "SELECT MAX(Contrat.DateEffet) as MaxiDate,Contrat.NumContrat AS MaxiNum, Contrat.LibelleCont1,"
                        + " Contrat.CodeArticle, FacArticle.Designation1 "
                        + " From Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle ";

                    //sSQL = sSQL & " WHERE Contrat.CodeImmeuble like '" & sCodeimmeuble & "%' AND Avenant=0"
                    //=== modif du 05 10 2014, remplace dans la requete le like par le =.

                    sSQL = sSQL + " WHERE Contrat.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "' AND Avenant=0";
                    sSQL = sSQL + " GROUP BY Contrat.NumContrat, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1";
                    using (var tmpAdo = new ModAdo())
                        rstmp = tmpAdo.fc_OpenRecordSet(sSQL);

                    if (rstmp.Rows.Count > 0)
                    {
                        //txtContrat.Text = rstmp!Designation1
                        sCodeContrat = rstmp.Rows[0]["MaxiNum"].ToString();
                    }
                    else
                    {
                        //txtContrat.Text = ""
                        sCodeContrat = "";
                    }
                    // rstmp.Close();
                    rstmp = null;

                    if (!string.IsNullOrEmpty(sCodeContrat))
                    {
                        fc_sauver();
                        fc_SavParamPos();
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFicheContrat, sCodeContrat);
                        View.Theme.Theme.Navigate(typeof(UserDocFicheContrat));
                        // ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGFACCONTRAT);
                    }
                }
            }

            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";lblGoContrat_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblGoLocalisation_Click(object sender, EventArgs e)
        {
            try
            {
                fc_SavParamPos();
                General.saveInReg(ModConstantes.cUserDocLocalisation, "txtCodeImmeuble", txtCodeImmeuble.Text);
                View.Theme.Theme.Navigate(typeof(UserDocLocalisation));
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";lblGoLocalisation_Click");
            }
        }

        private void lblGoTypeRevision_Click(object sender, EventArgs e)
        {
            try
            {
                fc_SavParamPos();
                View.Theme.Theme.Navigate(typeof(UserDocParametreV2));

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";lblGoTypeRevision_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_SavParamPos()
        {
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocP2, txtCOP_NoAuto.Text);
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sID"></param>
        /// <returns></returns>
        private bool fc_CtrlLocTreev(string sID)
        {

            bool functionReturnValue = false;
            bool bselect = false;

            //=== stocke dans sid le nuémro de localisation, retourne true si la focntion s'est bien déroulé.

            try
            {
                //=== controle si une localisation a bien été saisie.
                //=== controle qu'une localisation a été sélectionnée.

                if (General.sPriseEnCompteLocalP2 == "1")
                {
                    bselect = false;
                    foreach (UltraTreeNode oNode in TreeView1.Nodes)
                    {
                        if (oNode.Selected == true)
                        {
                            if (General.Left(oNode.Key, 1).ToUpper().ToString() == cNiv1.ToUpper().ToString())
                            {
                                sID = General.Mid(oNode.Key, 2, oNode.Key.Length - 1);
                                bselect = true;
                                StockID = sID;
                                functionReturnValue = true;

                            }
                        }

                    }

                    //===
                    if (TreeView1.Nodes.Count == 0)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez créer au moins une localisation (chaufferie...) avant d'ajouter des prestations,rendez-vous sur la fiche localisation afin d'en créer une.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        functionReturnValue = false;
                        return functionReturnValue;

                    }
                    else if (bselect == false)
                    {

                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner une localisation (chaufferie...) pour pouvoir ajouter une prestation.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        functionReturnValue = false;
                        return functionReturnValue;
                    }
                    functionReturnValue = true;
                    return functionReturnValue;
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";");
                return functionReturnValue;
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAddNiv1_Click(object sender, EventArgs e)
        {
            string sID = "";
            UltraTreeNode oNode = null;
            bool bselect = false;


            try
            {
                if (fc_Ctrl() == false)//tested
                {
                    return;
                }

                //=== controle si une localisation a bien été saisie.
                //=== controle qu'une localisation a été sélectionnée.
                if (General.sPriseEnCompteLocalP2 == "1")
                {
                    if (fc_CtrlLocTreev(sID) == false)//tested
                    {
                        return;
                    }

                    //bselect = False

                    //For Each oNode In TreeView1.Nodes

                    //    If oNode.Selected = True Then

                    //        If UCase(Left(oNode.Key, 1)) = UCase(cNiv1) Then

                    //            sID = Mid(oNode.Key, 2, Len(oNode.Key) - 1)
                    //            bselect = True

                    //        End If
                    //    End If
                    //Next

                    //===
                    //If TreeView1.Nodes.Count = 0 Then
                    //        MsgBox "Vous devez créer au moins une localisation (chaufferie...) avant d'ajouter des prestations," & vbCrLf & "rendez-vous sur la fiche localisation afin d'en créer une.", vbInformation, "Opération annulée"
                    //        Exit Sub
                    //ElseIf bselect = False Then
                    //        MsgBox "Vous devez sélectionner une localisation (chaufferie...) pour pouvoir ajouter une prestation.", vbInformation, "Opération annulée"
                    //        Exit Sub
                    //End If
                }

                frmP2 frmp2 = new frmP2();
                //Todo
                frmp2.Text = "immeuble : " + txtCodeImmeuble.Text;
                //& " - Contrat : " & txtCOP_NoContrat & "/" & txtCOP_Avenant.Text
                frmp2.txtCop_NoAuto.Text = txtCOP_NoAuto.Text;
                frmp2.txtCodeImmeuble.Text = txtCodeImmeuble.Text;
                frmp2.txtPDAB_Noauto.Text = StockID;
                frmp2.txtCOP_DebutPlanification.Text = txtCOP_DateSignature.Text;
                frmp2.ShowDialog();
                fc_sauver();
                fc_loadTreeView(Convert.ToInt32(txtCOP_NoAuto.Text));
                frmp2.Close();

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAddNiv1_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <returns></returns>
        private bool fc_Ctrl()
        {
            bool functionReturnValue = false;
            //=== retourne true si tout aucune erreur n'a été décelé.
            string sSQL = null;

            try
            {

                functionReturnValue = false;
                //=== controle si il existe un contrat.
                if (string.IsNullOrEmpty(txtCOP_NoAuto.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner sur un contrat.", "Opération annulé", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return functionReturnValue;
                }

                //=== controle si une date de début de planification existe.

                sSQL = "SELECT COP_DateSignature FROM COP_ContratP2 WHERE COP_NoAuto =" + General.nz(txtCOP_NoAuto.Text, 0);
                var tmpado = new ModAdo();
                sSQL = tmpado.fc_ADOlibelle(sSQL);
                if (string.IsNullOrEmpty(sSQL))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir et valider une date de début de planification", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    txtCOP_DateSignature.Focus();
                    return functionReturnValue;
                }
                functionReturnValue = true;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Ctrl");
                return functionReturnValue;
            }
        }

        private void cmdAvenant_Click(object sender, EventArgs e)
        {

            string sCodeImmeuble = null;
            int lnumfichestandard = 0;
            DialogResult dg1 = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez vous créer un avenant pour cet immeuble?", "Création d'avenant", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            if (dg1 == DialogResult.No)
            {
                return;
            }
            DialogResult dg2 = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la création d'un nouvel avenant pour cet immeuble ?" + "", "Création d'un nouvel avenant", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
            //== confirmation de création d'un nouvel avenant.
            if (dg2 == DialogResult.No)
            {
                return;
            }
            sCodeImmeuble = txtCodeImmeuble.Text;
            lnumfichestandard = Convert.ToInt32(txtNumFichestandard.Text);
            fc_Ajouter();
            //blnAjout = True
            txtCodeImmeuble.Text = sCodeImmeuble;
            txtNumFichestandard.Text = Convert.ToString(lnumfichestandard);
            //txtCodeParticulier = GetSetting(cFrNomApp, cUserIntervention, "Copro", "")
            fc_ChargeImmeuble(txtCodeImmeuble.Text);
            ModMain.bActivate = true;
            fc_sauver();
            ModMain.bActivate = false;
            //txtDocument = GetSetting(cFrNomApp, cUserIntervention, "Courier", "")
            //txtCAI_code = GetSetting(cFrNomApp, cUserIntervention, "Operation", "")
            //txtNoDevis = GetSetting(cFrNomApp, cUserIntervention, "NoDevis", "")
            fc_InitialiseGrille();

        }

        private void cmdDupliqueInter_Click(object sender, EventArgs e)
        {
            try
            {
                Forms.frmDupliquerPrest frm = new Forms.frmDupliquerPrest();
                /* ToDO Ajouter cette frm
                 * frm.txtCodeimmeuble.Text = txtCodeImmeuble.Text;
                 frm.txtCOP_NoAuto.Text = txtCOP_NoAuto.Text;*/
                frm.ShowDialog();
                fc_loadTreeView(Convert.ToInt32(txtCOP_NoAuto.Text));
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdDupliqueInter_Click");
            }
        }

        private void cmdFindSignataire_Click(object sender, EventArgs e)
        {
            try
            {
                General.sSQL = "SELECT Personnel.Matricule as [Matricule], Personnel.Nom as [NOM],Personnel.prenom as [Prenom], Qualification.CodeQualif as [Code Qualif]," + " Qualification.Qualification as [Qualification], Personnel.NumRadio, Personnel.Note_NOT" + " FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                string sCode = "";
                //On Error GoTo Erreur
                string req = "SELECT Personnel.Matricule as \"Matricule \", Personnel.Nom as \"Nom\", Personnel.prenom as \"Prenom\", Qualification.Qualification as \"Qualification\","
                    + " Personnel.NumRadio as \"Kobby\" FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                string where = "(NonActif is null or NonActif = 0) ";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un signataire" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    //Charge les enregistrements de l'immeuble
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtCOP_Signataire.Text = sCode;
                    lbllibCOP_Signataire.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString(); ;
                    fg.Dispose(); fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        //Charge les enregistrements de l'immeuble
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtCOP_Signataire.Text = sCode;
                        lbllibCOP_Signataire.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString(); ;
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdFindSignataire_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdGénerer_Click(object sender, EventArgs e)
        {
            System.DateTime dtDateDeb = default(System.DateTime);
            System.DateTime dtDateFin = default(System.DateTime);
            System.DateTime dtTemp = default(System.DateTime);
            string sMessage = null;


            try
            {
                if (string.IsNullOrEmpty(txtCOP_NoAuto.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Sélectionnez d'abord un contrat?", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                //===> Mondir le 09.02.2021, https://groupe-dt.mantishub.io/view.php?id=2276
                if (txtCOP_Statut.Text.ToUpper() != "A")
                {
                    CustomMessageBox.Show("Seul le statut A est autorisé pour la simulation", "Simulation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                //===> Fin Modif Mondir

                //=== alerte l'utilisateur qu'il va planifier sur une trop longue période
                if (OptAnneeContrat.Checked == true)
                {
                    if (!General.IsDate(txtCOP_DateSignature.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date de signature", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSTab1.SelectedTab = SSTab1.Tabs[0];
                        return;
                    }
                    txtDateSignatureSimul.Text = txtCOP_DateSignature.Text;
                    dtDateDeb = Convert.ToDateTime(txtCOP_DateSignature.Text);
                    dtDateFin = Convert.ToDateTime(dtDateDeb).AddYears(1);
                    dtDateFin = Convert.ToDateTime(dtDateFin).AddDays(-1);

                }
                else
                {
                    if (!General.IsDate(dtDateDebPlaning.Value.ToString()))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date de debut de planification invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtDateDebPlaning.Focus();
                        return;
                    }
                    if (!General.IsDate(dtDateFinPlaning.Value.ToString()))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date de fin de planification invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtDateFinPlaning.Focus();
                        return;
                    }
                    dtDateDeb = Convert.ToDateTime(dtDateDebPlaning.Value);
                    dtDateFin = Convert.ToDateTime(dtDateFinPlaning.Value);
                }


                dtTemp = Convert.ToDateTime(dtDateDeb).AddYears(1).AddDays(-1);

                if (dtDateFin > dtTemp)
                {
                    DialogResult dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention vous allez simuler une planification sur une période dépassant une année, Voulez vous continuer ? ", "Avertissement", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (dg == DialogResult.No)
                    {
                        return;

                    }
                }

                sMessage = ModP2v2.fc_SimulationCreeCalendrierV2(dtDateDeb, Convert.ToInt32(txtCOP_NoAuto.Text), dtDateFin, true, "", (txtCodeImmeuble.Text), "", "");

                cmdRafraichir_Click(cmdRafraichir, new System.EventArgs());

                if (!string.IsNullOrEmpty(sMessage))
                {
                    Forms.frmErreurP2 frmerreurP2 = new Forms.frmErreurP2();
                    frmerreurP2.txtErreur.Text = sMessage;
                    frmerreurP2.ShowDialog();
                    frmerreurP2.Close();
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdGénerer_Click");
            }
        }

        private void cmdGoP2_Click(object sender, EventArgs e)
        {
            // ' stocke la position de la fiche contrat
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocP2, txtCOP_NoAuto.Text);
            // stocke les paramétres pour la feuille de destination.
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserInterP2, txtCOP_NoAuto.Text, txtCodeImmeuble.Text, txtCOP_NoContrat.Text);
            //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserInterP2);TODO
            View.Theme.Theme.Navigate(typeof(UserIntervP2V2));

        }

        private void cmdGroupeCtr_Click(object sender, EventArgs e)
        {
            string sCode = null;
            try
            {
                string req = "SELECT  codeimmeuble as \"Immeuble\", COP_NoContrat as \"No Contrat\" , COP_Avenant as \"Avenant\", COP_DateSignature as \"Date de Signature\", COP_Statut as \"Statut\", COP_Noauto as  \"ID\"  FROM  COP_ContratP2 ";
                string where = "";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche des contrats" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtCOP_GroupeAvenant.Text = fg.ugResultat.ActiveRow.Cells["Avenant"].Value.ToString();
                    txtCOP_GroupeNoAuto.Text = fg.ugResultat.ActiveRow.Cells["ID"].Value.ToString();
                    txtCOP_GroupeNoContrat.Text = fg.ugResultat.ActiveRow.Cells["No Contrat"].Value.ToString();
                    fg.Dispose(); fg.Close();

                };
                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCOP_GroupeAvenant.Text = fg.ugResultat.ActiveRow.Cells["Avenant"].Value.ToString();
                        txtCOP_GroupeNoAuto.Text = fg.ugResultat.ActiveRow.Cells["ID"].Value.ToString();
                        txtCOP_GroupeNoContrat.Text = fg.ugResultat.ActiveRow.Cells["No Contrat"].Value.ToString();
                        fg.Dispose(); fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdGroupeCtr_Click");
            }
        }

        private void cmdVisu_Click(object sender, EventArgs e)
        {
            try
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("fonction non disponible.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;


                string SelectionFormula = "";
                General.lEditionCtr = 0;
                Forms.Form3 frm3 = new Forms.Form3();
                //.Move Me.Left + cleftForm, (Screen.Height - 15 * Screen.TwipsPerPixelY) - .Height
                frm3.ShowDialog();
                if (General.lEditionCtr == 0)
                {
                    return;
                }
                if (General.lEditionCtr == 1)
                {
                    //===> Mondir le 18.02.2021 Commented in V16.02.2021
                    //ReportDocument cr1 = new ReportDocument();
                    //cr1.Load(General.gsRpt + General.ETATcontrat);
                    ////  cr1.ReportFileName = "C:/gesten/rpt/" & ETATcontrat
                    //SelectionFormula = "{cop_ContratP2.COP_NoAuto} =" + txtCOP_NoAuto.Text;
                    //CrystalReportFormView form = new CrystalReportFormView(cr1, SelectionFormula);
                    //form.Show();
                    //SelectionFormula = "";
                }
                else if (General.lEditionCtr == 2)
                {
                    //===> Mondir le 18.02.2021 Commented in V16.02.2021
                    //ReportDocument crDemandeContrat = new ReportDocument();
                    //crDemandeContrat.Load(General.gsRpt + General.EtatContratOrigine);
                    //SelectionFormula = "{cop_ContratP2.COP_NoAuto} =" + txtCOP_NoAuto.Text;
                    //CrystalReportFormView form = new CrystalReportFormView(crDemandeContrat, SelectionFormula);
                    //form.Show();
                    //SelectionFormula = "";
                }
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdImpression_Click");
            }
        }

        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            fc_Ajouter();
            lblContratResilie.Visible = false;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            fc_annuler();
            //fc_ICA_ImmCategorieInterv();Cette fonction existe dans vb6 mais tous le code est commenté
            //fc_GammePlanning();Cette fonction existe dans vb6 mais tous le code est commenté

            //=== conrôle si ce contrat est liée à un contrat sous traitant  non résilié.
            fc_CtrSSt(txtCodeImmeuble.Text);
        }

        private void fc_annuler()
        {
            var _with9 = this;
            if (blModif == true)
            {
                DialogResult dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous enregistrer vos modifications ?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                switch (dg)
                {
                    case DialogResult.Yes:
                        fc_sauver();
                        break;

                    case DialogResult.No:
                        break;
                    // Vider devis en cours
                    default:
                        return;

                        //Reprendre saisie en cours
                        break;
                }
            }
            fc_InitialiseGrille();
            fc_clear();
            fc_BloqueForm();


        }

        private void CmdEditer_Click(object sender, EventArgs e)
        {
            try
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("fonction non disponible.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;

                if (string.IsNullOrEmpty(txtCOP_DateImpression.Text) || !General.IsDate(txtCOP_DateImpression.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date d'impression valide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    txtCOP_DateImpression.Focus();
                    return;

                }
                //If COP_P1 = 0 And COP_P2 = 0 And COP_P3 = 0 And COP_P4 = 0 Then
                //     MsgBox "Précisez le type du contrat(P1, P2, P3, P4).", vbCritical, ""
                //     SSTab1.Tab = 0
                //     Exit Sub
                //End If
                if (chkCOP_CLIM.CheckState == 0 && chkCOP_CPCU.CheckState == 0 && chkCOP_ELECT.CheckState == 0 && chkCOP_FIOUL.CheckState == 0 && chkCOP_Gaz.CheckState == 0 && chkCOP_SURP.CheckState == 0 && chkCOP_TE.CheckState == 0 && chkCOP_VMC.CheckState == 0 || chkCOP_Murale.CheckState == 0 || chkCOP_VmcDsc.CheckState == 0 || chk6.CheckState == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Précisez l'energie/activité du contrat.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    return;
                }
                txtCOP_Statut.Text = "04";
                fc_sauver();
                //===> Mondir le 18.02.2021 pour ajouter les modifs de la version V16.02.2021
                //ReportDocument cr1 = new ReportDocument();
                //cr1.Load(General.gsRpt + General.ETATcontrat);
                ////  cr1.ReportFileName = "C:/gesten/rpt/" & ETATcontrat            
                //string SelectionFormula = "{cop_ContratP2.COP_NoAuto} =" + txtCOP_NoAuto.Text;
                ////cr1.Destination = Crystal.DestinationConstants.crptToPrinter;
                //CrystalReportFormView forms = new CrystalReportFormView(cr1, SelectionFormula);
                //forms.Show();
                //SelectionFormula = "";

                return;
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";cmdImpression_Click");

            }
        }

        private void fc_LockedReq(Control C, bool bLock)
        {
            foreach (Control CC in C.Controls)
            {
                if (CC.HasChildren)
                    fc_LockedReq(CC, bLock);

                var v = CC;

                if (v is UltraGrid)
                {

                }

                if (v.Name.ToUpper() == ssCOD_ContratP2Detail.Name.ToUpper())
                {
                    var vv = v as UltraGrid;
                    for (int i = 0; i < vv.DisplayLayout.Bands[0].Columns.Count; i++)
                    {
                        vv.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
                    }
                }


                if (v is iTalk.iTalk_TextBox_Small2)
                {
                    if (bLock == true && v.Tag != null && v.Tag.ToString().ToUpper() != cActif.ToUpper())
                    {
                        var ctl = v as iTalk.iTalk_TextBox_Small2;
                        ctl.ReadOnly = true;
                        ctl.ForeColor = Color.Blue;
                    }
                    else if (v.Tag != null && v.Tag.ToString().ToUpper() == cBloqueFicheP2.ToUpper())
                    {
                        var ctl = v as iTalk.iTalk_TextBox_Small2;
                        ctl.ReadOnly = true;
                        ctl.ForeColor = Color.Blue;
                    }
                    else
                    {
                        var ctl = v as iTalk.iTalk_TextBox_Small2;
                        ctl.ReadOnly = false;
                        ctl.ForeColor = Color.Black;

                    }
                }

                /*   if (v is System.Windows.Forms.TextBox)
                   {
                       if (bLock == true && v.Tag != null && v.Tag.ToString().ToUpper() != cActif.ToUpper())
                       {
                           var ctl = v as System.Windows.Forms.TextBox;
                           ctl.ReadOnly = true;
                           ctl.ForeColor = Color.Blue;
                       }
                       else if (v.Tag != null && v.Tag.ToString().ToUpper() != cBloqueFicheP2.ToUpper())
                       {
                           var ctl = v as System.Windows.Forms.TextBox;
                           ctl.ReadOnly = true;
                           ctl.ForeColor = Color.Blue;
                       }
                       else
                       {
                           var ctl = v as System.Windows.Forms.TextBox;
                           ctl.ReadOnly = false;
                           ctl.ForeColor = Color.Black;

                       }
                   }*/

                if (v is UltraCombo)
                {

                }


                if (v is System.Windows.Forms.Button)
                {
                    var vv = v as Button;

                    if (v.Tag == null)
                    {
                        var ctl = v as Button;
                        v.Enabled = bLock;
                    }

                }


            }



        }


        /// <summary>
        /// tested
        /// </summary>
        /// <param name="bLock"></param>
        private void fc_Lock(bool bLock)
        {
            object ctl = null;
            int i = 0;
            bool bEnabled = false;

            //== 05072004 RACHID
            //== deblocage systematique pour l'utilisateur ayant des droits admin.
            if (General.sDroit.ToUpper().ToString() == General.cAdmin.ToUpper().ToString())
            {
                bLock = false;

            }

            bEnabled = !(bLock);

            fc_LockedReq(this, bEnabled);

            //===> Mondir le 18.02.2021 pour ajouter les modifs de la version V16.02.2021
            //if (string.IsNullOrEmpty(txtCOP_SuiviePar.Text))//tested
            //{
            //    txtCOP_SuiviePar.Enabled = true;
            //    txtCOP_SuiviePar.ReadOnly = false;

            //    txtCOP_Technicien.ReadOnly = true; //<================================= Mondir : Not the right place
            //    txtCOP_Technicien.ForeColor = System.Drawing.Color.Blue; //<=========== Mondir : Not the right place
            //}
            //===> Fin Modif mondir

            txtCOP_Technicien.ReadOnly = true; //<================================= Mondir : This is the right place
            txtCOP_Technicien.ForeColor = System.Drawing.Color.Blue; //<=========== Mondir : This is the right place
        }
        private void cmdFindStraitant_Click(object sender, EventArgs e)
        {
            try
            {
                General.sSQL = "SELECT Personnel.Matricule as \"Matricule\", Personnel.Nom as \"NOM\",Personnel.prenom as \"Prenom\", Qualification.CodeQualif as \"Code Qualif\"," + " Qualification.Qualification as \"Qualification\", Personnel.NumRadio, Personnel.Note_NOT  FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                string sCode = "";
                string req = "SELECT Personnel.Matricule as \"Matricule \", Personnel.Nom as \"Nom\",Personnel.prenom as \"Prenom\", Qualification.Qualification as \"Qualification\", Personnel.NumRadio as \"Kobby\" FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                string where = "(Personnel.NonActif is null or Personnel.NonActif = 0)";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un straitant" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtCOP_SStraitant.Text = sCode;
                    lblibCOP_SStraitant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString(); ;
                    fg.Dispose(); fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtCOP_SStraitant.Text = sCode;
                        lblibCOP_SStraitant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString(); ;
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdFindStraitant_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdIntervenant_Click(object sender, EventArgs e)
        {
            //verification ligne par ligne
            try
            {
                General.sSQL = "SELECT Personnel.Matricule as \"Matricule\", Personnel.Nom as \"NOM\",Personnel.prenom as \"Prenom\","
                    + " Qualification.CodeQualif as \"Code Qualif\"," + " Qualification.Qualification as \"Qualification\","
                    + " Personnel.NumRadio, Personnel.Note_NOT  "
                    + "FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";

                string sCode = "";
                string req = "SELECT Personnel.Matricule as \"Matricule \", Personnel.Nom as \"Nom\","
                    + " Personnel.prenom as \"Prenom\", Qualification.Qualification as \"Qualification\","
                    + " Personnel.NumRadio as \"Kobby\" FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";

                string where = "(Personnel.NonActif is null or Personnel.NonActif = 0)";

                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un intervenant" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtCOP_Technicien.Text = sCode;
                    lbllibIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString(); ;
                    fg.Dispose(); fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtCOP_Technicien.Text = sCode;
                        lbllibIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString(); ;
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdIntervenant_Click");
            }
        }
        private void fc_InsertMod(int lBase, object lCop_NoAuto = null)
        {
            //-- si lbase = 1 alors on insére des modeles.
            //-- si lBase = 2 alors on insere une nouvelle version.

            int i = 0;
            string sWhere = null;
            DataTable rs = default(DataTable);
            DataTable rsMod = default(DataTable);
            int lNo = 0;
            int lNum = 0;

            //If lBase = 1 Then
            //    sWhere = ""
            //-- preparation de la clause where pour récupérer les modéles selectionnés.
            //    For i = 0 To UBound(tSelect)
            //        If sWhere = "" Then
            //            sWhere = " where COM_Code='" & tSelect(i) & "'"
            //        Else
            //            sWhere = sWhere & " OR COM_Code='" & tSelect(i) & "'"
            //        End If
            //    Next

            //End If

            //==========Insertion des chapitres.

            //-- récupére les chapitres du contrat.           
            General.sSQL = "SELECT COS_Code, COS_NoAuto, COS_NoLigne, COP_Noauto, CodeImmeuble, P2C_NoAuto, COS_Texte, COS_Mat, COS_Gamme"
                + " FROM COS_ContratChap where COP_NoAuto =" + lCop_NoAuto + " ORDER BY COS_NoLigne";
            var tmpAdors = new ModAdo();
            rs = tmpAdors.fc_OpenRecordSet(General.sSQL);
            //--récupére le numéro de ligne le plus grand.
            lNo = 0;
            if (rs.Rows.Count > 0)
            {
                // rs.MoveLast();
                //Verifier rs.Rows[rs.Rows.Count - 1]["COS_Ligne"]
                lNo = Convert.ToInt32(General.nz(rs.Rows[rs.Rows.Count - 1]["COS_Ligne"], 0));
            }
            lNo = lNo + 1;

            //--boucle sur tous les modéles.
            for (i = 0; i <= General.tSelect.Length; i++)
            {
                sWhere = " WHERE COM_Code='" + General.tSelect[i] + "'";
                //-- récupére le modéle de chapitre.
                General.sSQL = "SELECT COS_Code, COS_NoAuto, COS_NoLigne, COP_Noauto,"
                    + " CodeImmeuble, P2C_NoAuto, COS_Texte, COS_Mat, COS_Gamme "
                    + " FROM   COM_ContratChapMod";
                General.sSQL = General.sSQL + sWhere + " ORDER BY COS_NoLigne";
                var tmpAdorsMod = new ModAdo();
                rsMod = tmpAdorsMod.fc_OpenRecordSet(General.sSQL);
                if (rsMod.Rows.Count > 0)
                {
                    lNum = 0;
                    //rsMod.MoveFirst();
                    foreach (DataRow rsModRow in rsMod.Rows)
                    {
                        Array.Resize(ref tIDChap, lNum + 1);
                        //rs.Rows.Add(rs.NewRow());
                        var NewRow = rs.NewRow();
                        NewRow["COS_Code"] = rsModRow["COS_Code"];
                        NewRow["COS_NoLigne"] = lNo;
                        NewRow["COP_NoAuto"] = rsModRow["COP_NoAuto"];
                        NewRow["CodeImmeuble"] = rsModRow["CodeImmeuble"];
                        NewRow["P2C_NoAuto"] = rsModRow["P2C_NoAuto"];
                        NewRow["COS_Texte"] = rsModRow["COS_Texte"];
                        NewRow["COS_Mat"] = rsModRow["COS_Mat"];
                        NewRow["COS_Gamme"] = rsModRow["COS_Gamme"];
                        rs.Rows.Add(NewRow);
                        tIDChap[lNum].ModCOS_NoAuto = rsModRow["COS_NoAuto"] != DBNull.Value ? Convert.ToInt32(rsModRow["COS_NoAuto"]) : 0;
                        tmpAdors.Update();
                        // rsMod.MoveNext();
                        lNo = lNo + 1;
                        lNum = lNum + 1;
                    }
                }
            }

            //dbBar = 100 / rs.RecordCount

            //If (PB1.value + dbBar) >= 100 Then
            //        PB1.value = 100
            //Else
            //        PB1.value = PB1.value + dbBar
            //End If

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRafraichir_Click(object sender, EventArgs e)
        {
            try
            {
                System.DateTime dtDateDeb = default(System.DateTime);
                System.DateTime dtDateFin = default(System.DateTime);

                if (OptAnneeContrat.Checked == true)
                {
                    if (!General.IsDate(txtCOP_DateSignature.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date de signature", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSTab1.SelectedTab = SSTab1.Tabs[0];
                        return;
                    }
                    txtDateSignatureSimul.Text = txtCOP_DateSignature.Text;
                    dtDateDeb = Convert.ToDateTime(txtCOP_DateSignature.Text);
                    dtDateFin = Convert.ToDateTime(dtDateDeb).AddYears(1);
                    dtDateFin = Convert.ToDateTime(dtDateFin).AddDays(-1);

                }
                else
                {
                    if (!General.IsDate(dtDateDebPlaning.Value.ToString()))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date de debut de planification invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtDateDebPlaning.Focus();
                        return;
                    }

                    if (!General.IsDate(dtDateFinPlaning.Value.ToString()))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date de fin de planification invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        dtDateFinPlaning.Focus();
                        return;
                    }

                    dtDateDeb = Convert.ToDateTime(dtDateDebPlaning.Value);
                    dtDateFin = Convert.ToDateTime(dtDateFinPlaning.Value);
                }

                fc_LoadInterventionP2(dtDateDeb, dtDateFin, Convert.ToInt32(General.nz(txtCOP_NoAuto.Text, 0)), "", "");
                General.saveInReg(Variable.cUserDocP2, "dtDateDebPlaning", dtDateDebPlaning.Value.ToString());
                General.saveInReg(Variable.cUserDocP2, "dtDateFinPlaning", dtDateFinPlaning.Value.ToString());
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRafraichir_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="dDedut"></param>
        /// <param name="dFin"></param>
        /// <param name="lCop_NoAuto"></param>
        /// <param name="sIntervenant"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <returns></returns>
        private bool fc_LoadInterventionP2(System.DateTime dDedut, System.DateTime dFin, int lCop_NoAuto, string sIntervenant, string sCodeImmeuble)
        {
            bool functionReturnValue = false;
            string sSQL = null;
            try
            {

                //===  Rafraîchie la table des listes de taches fusionnées
                //===  devenant ainsi des interventions.

                sSQL = "SELECT     NoIntervention , InterventionP2Simul.CodeImmeuble ,PDAB_Libelle, Article,"
                + " Intervenant, Personnel.Nom, Designation,  DatePrevue,DateVisite, DureeP, CompteurObli, Cop_NoAuto,INT_realise,"
                + " dateRealise, HeureDebut, HeureFin ,heureDebutP, heureFinP,NoSemaine"
                + " FROM InterventionP2Simul "
                + " LEFT OUTER JOIN Immeuble ON "
                + " InterventionP2Simul.CodeImmeuble = Immeuble.CodeImmeuble  "
                + " LEFT OUTER JOIN Personnel ON " + " InterventionP2Simul.Intervenant = Personnel.Matricule "
                + " WHERE " + " InterventionP2Simul.DateVisite>='" + dDedut + "'"
                + " AND InterventionP2Simul.DateVisite <='" + dFin + "'"
                + " AND InterventionP2Simul.CreePar ='" + General.fncUserName() + "'";
                // 
                //& " InterventionP2Simul.DatePrevue>='" & dDedut & "'" _
                //& " AND InterventionP2Simul.DatePrevue <='" & dFin & "'" _
                //& " AND InterventionP2Simul.CreePar ='" & fncUserName & "'"


                //=== Ajoute des critéres.
                if (!string.IsNullOrEmpty(sIntervenant))
                {
                    sSQL = sSQL + " AND InterventionP2Simul.Intervenant='" + StdSQLchaine.gFr_DoublerQuote(sIntervenant) + "'";
                }

                if (lCop_NoAuto != 0)
                {
                    sSQL = sSQL + " AND InterventionP2Simul.COP_NoAuto =" + lCop_NoAuto;
                }

                if (!string.IsNullOrEmpty(sCodeImmeuble))
                {
                    sSQL = sSQL + " AND InterventionP2Simul.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }

                sSQL = sSQL + "   ORDER BY InterventionP2Simul.CodeImmeuble, intervenant,PDAB_Libelle, datevisite ";
                var tmpAdorsVisite = new ModAdo();
                rsVisite = tmpAdorsVisite.fc_OpenRecordSet(sSQL);
                ssGridVisite.DataSource = rsVisite;
                ssGridVisite.UpdateData();
                lblPrestOrigine.Text = Convert.ToString(rsVisite.Rows.Count);

                //==== affiche le détail des visites (gammes filles + taches
                //  fc_DetailVisite CLng(nz(ssGridVisite.Columns("NoIntervention").Text, 0))

                //verifier "ssGridVisite.Columns("NoIntervention").value"
                int c = 0;
                if (ssGridVisite.ActiveRow != null)
                {
                    c = Convert.ToInt32(General.nz((ssGridVisite.ActiveRow.Cells["NoIntervention"].Value), 0));
                    fc_ssGRIDTachesAnnuel(c);
                }
                else if (ssGridVisite.Rows.Count > 0)
                {
                    c = Convert.ToInt32(General.nz((ssGridVisite.Rows[0].Cells["NoIntervention"].Value), 0));
                    fc_ssGRIDTachesAnnuel(c);
                }


                return functionReturnValue;
            }

            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                return functionReturnValue;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lNoIntervention"></param>
        private void fc_ssGRIDTachesAnnuel(int lNoIntervention)
        {
            //    sSQL = "select * from OperationP2Simul where COP_Noauto =" & lCOP_NoAuto & "" _
            //& " order by dateprevue , TPP_Code"
            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                if (optOperation.Checked == true)
                {
                    ssGridTachesAnnuel.Visible = true;
                    ssOpTaches.Visible = false;

                    //sSQL = "SELECT * FROM OperationP2Simul WHERE NointerventionVisite = " & LNointervention & "" _
                    //& " ORDER BY OperationP2Simul.CAI_Libelle, OperationP2Simul.EQM_Libelle , TPP_Code"

                    General.sSQL = "SELECT     CodeImmeuble, PDAB_Libelle AS Localisation, NoInterventionVisite, GAI_LIbelle AS Equipements, "
                        + " CAI_Libelle AS Composant, EQM_Code AS Operation,EQM_Libelle,"
                        + " CTR_Libelle, MOY_Libelle, ANO_Libelle, OPE_Libelle,"
                        + "  TPP_Code AS Periodicite, NoSemaine, Duree "
                        + " From OperationP2Simul"
                        + " WHERE NointerventionVisite = " + lNoIntervention + " ORDER BY Localisation, Equipements, Composant, Operation";
                    using (var tmpAdorsGamme = new ModAdo())
                        rsGamme = tmpAdorsGamme.fc_OpenRecordSet(General.sSQL);
                    ssGridTachesAnnuel.DataSource = rsGamme;
                    ssGridTachesAnnuel.UpdateData();
                    //    With adoOp
                    //        .ConnectionString = adocnn
                    //        .RecordSource = sSQL
                    //        .Refresh
                    //    End With
                }

                else if (optTaches.Checked == true)
                {
                    ssGridTachesAnnuel.Visible = false;
                    ssOpTaches.Visible = true;
                    General.sSQL = "SELECT     OperationP2Simul.DatePrevue, OperationP2Simul.EQM_CODE, "
                        + " TIP2S_TachesInterP2Simul.TIP2_Designation1, "
                        + " TIP2S_TachesInterP2Simul.TIP2_NoLIgne , TIP2S_TachesInterP2Simul.Realise,"
                        + " TIP2S_TachesInterP2Simul.DateRealise, Commentaire "
                        + " FROM         OperationP2Simul INNER JOIN" + " TIP2S_TachesInterP2Simul "
                        + " ON OperationP2Simul.NoIntervention = TIP2S_TachesInterP2Simul.NoInterventionOp"
                        + " WHERE  OperationP2Simul.NoInterventionVisite = " + lNoIntervention + " ORDER BY OperationP2Simul.CAI_Libelle, OperationP2Simul.EQM_CODE, " + " TIP2S_TachesInterP2Simul.TIP2_NoLIgne";
                    DataTable adoOptaches = new DataTable();
                    SqlDataAdapter SDAadoOptaches = new SqlDataAdapter(General.sSQL, General.adocnn);
                    SDAadoOptaches.Fill(adoOptaches);
                    ssOpTaches.DataSource = adoOptaches;
                    ssOpTaches.UpdateData();
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name);
            }

            //lblPrestOrigine = adoOp.Recordset.RecordCount & ""
        }

        private void cmdRechercheChefSecteur_Click(object sender, EventArgs e)
        {
            try
            {
                string sCode = "";
                string sSEC_Code = "";
                //On Error GoTo Erreur
                string req = "SELECT Personnel.Matricule as \"Matricule \", Personnel.Nom as \"Nom\", Personnel.prenom as \"Prenom\", Qualification.Qualification as \"Qualification\", Personnel.SEC_Code as \"SEC_Code\"  FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                string where = "";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un chef secteur" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    // charge les enregistrements de l'immeuble
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtCodeChefSecteur.Text = sCode;
                    lbllibCodeChefSecteur.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    var tmpAdo = new ModAdo();
                    sSEC_Code = tmpAdo.fc_ADOlibelle("Select SEC_Code from personnel where matricule ='" + txtCodeChefSecteur.Text + "'");
                    if (!string.IsNullOrEmpty(txtSEC_Code.Text))
                    {
                        if (txtSEC_Code.Text != sSEC_Code)
                        {
                            DialogResult lMsg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention vous allez changer le secteur, voulez vous continuer.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            if (lMsg == DialogResult.Yes)
                            {
                                txtSEC_Code.Text = sSEC_Code;
                            }

                        }
                    }
                    else
                    {
                        txtSEC_Code.Text = sSEC_Code;
                    }
                    //txtSEC_Code = sSEC_Code             
                    fg.Dispose(); fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        // charge les enregistrements de l'immeuble
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtCodeChefSecteur.Text = sCode;
                        lbllibCodeChefSecteur.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        var tmpAdo = new ModAdo();
                        sSEC_Code = tmpAdo.fc_ADOlibelle("Select SEC_Code from personnel where matricule ='" + txtCodeChefSecteur.Text + "'");
                        if (!string.IsNullOrEmpty(txtSEC_Code.Text))
                        {
                            if (txtSEC_Code.Text != sSEC_Code)
                            {
                                DialogResult lMsg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention vous allez changer le secteur, voulez vous continuer.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                if (lMsg == DialogResult.Yes)
                                {
                                    txtSEC_Code.Text = sSEC_Code;
                                }

                            }
                        }
                        else
                        {
                            txtSEC_Code.Text = sSEC_Code;
                        }
                        //txtSEC_Code = sSEC_Code             
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheChefSecteur_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheCodetat_Click(object sender, EventArgs e)
        {
            //verification ligne par ligne
            try
            {

                string req = "SELECT  ETC_Code as \"Code\" , ETC_Libelle as \"Libelle\" FROM  ETC_Contrat";
                string where = "";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un statuts" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtCOP_Statut.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    lbllibCOP_Statut.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();

                    //===> Mondir le 18.02.2021 pour ajouter les modifs de la version V16.02.2021
                    //=== positionne le DTPicker sur la date du jour.
                    //if (string.IsNullOrEmpty(General.nz(txtCOP_DateAcceptation.Value, "").ToString()))
                    //{
                    //    txtCOP_DateAcceptation.Value = DateTime.Today;
                    //    txtCOP_DateAcceptation.Value = "";
                    //}

                    //=== positionne le DTPicker sur la date du jour.
                    //if (string.IsNullOrEmpty(General.nz(txtCOP_DateDemandeResiliation.Value, "").ToString()))
                    //{
                    //    txtCOP_DateDemandeResiliation.Value = DateTime.Today;
                    //    txtCOP_DateDemandeResiliation.Value = "";
                    //}
                    //===> Fin Modif Mondir
                    fg.Dispose(); fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCOP_Statut.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        lbllibCOP_Statut.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();

                        //===> Mondir le 18.02.2021 pour ajouter les modifs de la version V16.02.2021
                        //=== positionne le DTPicker sur la date du jour.
                        //if (string.IsNullOrEmpty(General.nz(txtCOP_DateAcceptation.Value, "").ToString()))
                        //{
                        //    txtCOP_DateAcceptation.Value = DateTime.Today;
                        //    txtCOP_DateAcceptation.Value = "";
                        //}

                        //=== positionne le DTPicker sur la date du jour.
                        //if (string.IsNullOrEmpty(General.nz(txtCOP_DateDemandeResiliation.Value, "").ToString()))
                        //{
                        //    txtCOP_DateDemandeResiliation.Value = DateTime.Today;
                        //    txtCOP_DateDemandeResiliation.Value = "";
                        //}
                        //===> Fin Modif Mondir
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheCodetat_Click");
            }
        }
        private void fc_libStatut()
        {
            try
            {
                using (var tmpAdo = new ModAdo())
                {
                    lbllibCOP_Statut.Text = tmpAdo.fc_ADOlibelle("SELECT ETC_Libelle FROM  ETC_Contrat WHERE ETC_Code ='" + StdSQLchaine.gFr_DoublerQuote(txtCOP_Statut.Text.Trim()) + "'");
                    return;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_libStatut");
            }
        }

        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            try
            {
                string sCode;
                //On Error GoTo Erreur
                string req = "SELECT CodeImmeuble as \"Nom directeur\",Adresse as \"adresse\", Ville as \"Ville\", anglerue as \"angle de rue\" FROM Immeuble ";
                string where = "";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche des immeubles" };
                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeuble.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtCodeImmeuble.Text = sCode;
                    fc_ChargeImmeuble(sCode);
                    fg.Dispose(); fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtCodeImmeuble.Text = sCode;
                        fc_ChargeImmeuble(sCode);
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheImmeuble_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            try
            {
                string sCode = "";

                string req = "SELECT CodeImmeuble as \"Immeuble\", COP_NoAuto as \"No FICHE GAMAO\" ,COP_Statut as \"Statut\" FROM  COP_ContratP2 ";
                string where = "";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche Fiche GMAO" };
                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeuble.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtCodeImmeuble.Text = sCode;
                    fc_ChargeEnregistrement("", "", false, Convert.ToInt32(fg.ugResultat.ActiveRow.Cells["No FICHE GAMAO"].Value));//tested
                    // fc_AfficheParametre(); cette fonction existe dans vb6 mais son code est commenté
                    SumGamme();//tested
                    fc_Calcul();//tested
                    fc_DetailPrestation(Convert.ToInt32(txtCOP_NoAuto.Text));//tested
                    fc_SavParamPos();
                    fg.Dispose(); fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtCodeImmeuble.Text = sCode;
                        fc_ChargeEnregistrement("", "", false, Convert.ToInt32(fg.ugResultat.ActiveRow.Cells["No FICHE GAMAO"].Value));
                        // fc_AfficheParametre(); cette fonction existe dans vb6 mais son code est commenté
                        SumGamme();
                        fc_Calcul();
                        fc_DetailPrestation(Convert.ToInt32(txtCOP_NoAuto.Text));
                        fc_SavParamPos();
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdRechercher_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            DataTable rs = default(DataTable);
            string sSQL = null;
            sSQL = "SELECT  codeimmeuble, COP_NoAuto From COP_ContratP2 WHERE  COP_NoAuto = " + General.nz(txtCOP_NoAuto.Text, 0);
            try
            {
                using (var tmpAdors = new ModAdo())
                    rs = tmpAdors.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count > 0)
                {
                    if (rs.Rows[0]["CodeImmeuble"].ToString().ToUpper() != txtCodeImmeuble.Text.ToUpper())
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention le n° de fiche GMAO " + txtCOP_NoAuto.Text + " Correspond à l'immeuble " + rs.Rows[0]["CodeImmeuble"] + "Veuillez utiliser la fenêtre de recherche pour sélectionner la bonne fiche GMAO ou appuyer sur la touche entrée sur le cartouche de saisie du code immeuble.", "Erreur", MessageBoxButtons.OKCancel, MessageBoxIcon.Error);
                        // rs.Close();
                        // ERROR: Not supported in C#: OnErrorStatement
                        rs = null;
                        return;
                    }
                }

                //rs.Close();
                rs = null;
                fc_sauver();//tested
                ssCOD_ContratP2Detail.UpdateData();
                Command6_Click(Command6, new System.EventArgs());//tested
                Command5_Click(Command5, new System.EventArgs());//tested
                Command8_Click(Command8, new System.EventArgs());//tested
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdSauver_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command5_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable rstmp = default(DataTable);
                string sSQL = null;

                sSQL = "SELECT CAI_Code, CAI_Libelle, ICC_Affiche,  ICC_Noauto FROM ICC_IntervCategorieContrat  WHERE ICC_NoAuto =" + General.nz(txtICC_NoAuto.Text, 0);
                var tmpAdors = new ModAdo();
                rstmp = tmpAdors.fc_OpenRecordSet(sSQL);
                if (rstmp.Rows.Count > 0)
                {
                    //txtCAI_Code = nz(rstmp!CAI_Code, "")
                    rstmp.Rows[0]["CAI_Libelle"] = txtCAI_Libelle.Text;
                    tmpAdors.Update();

                    if (SSTab4.SelectedTab == SSTab4.Tabs[0])
                    {

                        if ((TreeView1.SelectedNodes != null))
                        {
                            TreeView1.SelectedNodes[0].Text = txtCAI_Libelle.Text;

                        }

                    }
                }
                // rstmp.Close();
                rstmp = null;
                ssPEI_PeriodeGammeImm.UpdateData();
                ssFAP_FacArticleP2.UpdateData();
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command5_Click");
            }
        }

        /// <summary>
        /// Mondir le 28.10.2020, to fix https://groupe-dt.mantishub.io/view.php?id=2064 ===> Sometime the app unselect the treenode ===> Must select the item
        /// </summary>
        /// <param name="Id"></param>
        private void checkSelected(string Id)
        {
            if (TreeView1.SelectedNodes == null || TreeView1.SelectedNodes.Count == 0 || TreeView1.ActiveNode == null)
                foreach (UltraTreeNode Node in TreeView1.Nodes.AsEnumerableReq())
                {
                    if (Node.Key == cNiv4 + Id)
                    {
                        TreeView1.ActiveNode = Node;
                        Node.Selected = true;
                        break;
                    }
                }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command6_Click(object sender, EventArgs e)
        {
            try
            {
                DataTable rstmp = default(DataTable);
                string sSQL = null;
                UltraTreeNode oNodeSelect = new UltraTreeNode();

                if (!string.IsNullOrEmpty(txtDuree.Text))//tested
                {
                    if (!General.IsNumeric(txtDuree.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le champ ''durée'' a un format invalide.");
                        return;
                    }
                }
                if (!string.IsNullOrEmpty(txtDureePlanning.Text))//tested
                {
                    if (!General.IsNumeric(txtDureePlanning.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le champ ''durée planning'' a un format invalide.");
                        return;
                    }
                }

                sSQL = "SELECT ICC_Noauto,EQM_Affiche, EQM_NoAuto, EQM_lIBELLE, EQM_NoAuto,EQM_Observation, duree, dureeplanning,"
                    + " CompteurObli, EQU_P1Compteur ,CTR_ID, CTR_Libelle, MOY_ID, MOY_Libelle, ANO_ID, ANO_Libelle, OPE_ID, OPE_Libelle "
                    + " From EQM_EquipementP2Imm"
                    + " WHERE     (EQM_NoAuto = " + General.nz(txtEQM_NoAuto.Text, 0) + ")";
                var tmpAdo = new ModAdo();
                rstmp = tmpAdo.fc_OpenRecordSet(sSQL);

                //===> Mondir le 28.10.2020, pour corriger : https://groupe-dt.mantishub.io/view.php?id=2064
                checkSelected(txtEQM_NoAuto.Text);
                //===> Fin Modif Mondir

                if (rstmp.Rows.Count > 0)//tested
                {
                    rstmp.Rows[0]["EQM_Libelle"] = txtEQM_LIBELLE.Text;
                    rstmp.Rows[0]["EQM_Observation"] = txtEQM_Observation.Text;
                    rstmp.Rows[0]["Duree"] = General.nz(txtDuree.Text, System.DBNull.Value);
                    rstmp.Rows[0]["DureePlanning"] = General.nz(txtDureePlanning.Text, System.DBNull.Value);
                    rstmp.Rows[0]["CompteurObli"] = General.nz(chkCompteurObli.CheckState, System.DBNull.Value);
                    rstmp.Rows[0]["EQU_P1Compteur"] = General.nz(chkEQU_P1Compteur.CheckState, System.DBNull.Value);
                    if (SSTab4.SelectedTab == SSTab4.Tabs[1])
                    {
                        // TreeView1.SelectedNode.Text = Combo0.Text;
                        if (TreeView1.SelectedNodes != null && TreeView1.SelectedNodes.Count > 0)
                        {
                            TreeView1.SelectedNodes[0].Text = Combo0.Text;
                        }
                    }

                    rstmp.Rows[0]["CTR_Libelle"] = Combo0.Text;
                    rstmp.Rows[0]["CTR_ID"] = General.nz(General.nz(ModP2v2.fc_IdControle(Combo0.Text), txt0.Text), 0);
                    txt0.Text = rstmp.Rows[0]["CTR_ID"].ToString();

                    rstmp.Rows[0]["MOY_Libelle"] = Combo1.Text;
                    rstmp.Rows[0]["MOY_ID"] = General.nz(General.nz(ModP2v2.fc_IdMoyen(Combo1.Text), txt1.Text), 0);
                    txt1.Text = rstmp.Rows[0]["MOY_ID"].ToString();

                    rstmp.Rows[0]["ANO_Libelle"] = Combo2.Text;
                    rstmp.Rows[0]["ANO_ID"] = General.nz(General.nz(ModP2v2.fc_IdAnomalie(Combo2.Text), txt2.Text), 0);
                    txt2.Text = rstmp.Rows[0]["ANO_ID"].ToString();

                    rstmp.Rows[0]["OPE_Libelle"] = Combo3.Text;
                    rstmp.Rows[0]["OPE_ID"] = General.nz(General.nz(ModP2v2.fc_IDOperation(Combo3.Text), txt3.Text), 0);
                    txt3.Text = rstmp.Rows[0]["OPE_ID"].ToString();

                    tmpAdo.Update();
                }

                ssPEI_PeriodeGammeImm.UpdateData();
                ssFAP_FacArticleP2.UpdateData();

                //rstmp.Close();
                rstmp = null;
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command6_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <returns></returns>
        private bool fc_sauver()
        {
            bool functionReturnValue = false;
            string sCode = null;
            string sAdresseClient = null;
            string sNumContrat = null;
            string sSQL = null;
            string sCOP_Duree = null;
            int lMsgbox = 0;


            var _with19 = this;
            sCode = txtCodeImmeuble.Text;

            //--- controle si code client saisie
            if (string.IsNullOrEmpty(sCode))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un code immeuble.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return functionReturnValue;
            }

            //if (string.IsNullOrEmpty(txtCOP_SuiviePar.Text) && ModMain.bActivate == false)
            //{
            //
            //            MsgBox "Validation annulée, le responsable qui suit l'affaire est obligatoire.", vbCritical, "Validation annulée"
            //            If txtCOP_SuiviePar.Enabled = False Or txtCOP_SuiviePar.Locked = True Then
            //                txtCOP_SuiviePar.Enabled = True
            //                txtCOP_SuiviePar.Locked = False
            //            End If
            //            SSTab1.Tab = 0
            //            txtCOP_SuiviePar.SetFocus
            //            Exit Function
            //}

            if (txtCOP_Statut.Text.ToUpper() == c04.ToUpper() && string.IsNullOrEmpty(txtCOP_DateImpression.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le statut est en 04, la date d'impression est obligatoire.", "Validation annulée");
                SSTab1.SelectedTab = SSTab1.Tabs[0];
                txtCOP_DateImpression.Focus();
                return functionReturnValue;
            }

            //===> Mondir le 09.02.2021, commented https://groupe-dt.mantishub.io/view.php?id=2276
            //if (txtCOP_Statut.Text.ToUpper() == c00.ToUpper() || txtCOP_Statut.Text.ToUpper() == c03.ToUpper() && General.IsDate(txtCOP_DateImpression.Text))
            //{
            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention il existe une date d'impression pour ce contrat," + " le statut ne peut pas être en 00 ni en 03, vous devez modifier ce statut.", "Saisie annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            //    return functionReturnValue;
            //}

            if (!string.IsNullOrEmpty(txtCOP_NbLogement.Text))
            {
                if (!General.IsNumeric(txtCOP_NbLogement.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur de saisie, Le nombre de logement doit être de type numérique.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return functionReturnValue;
                }
            }

            if (!string.IsNullOrEmpty(txtCOP_NbChaudiere.Text))
            {
                if (!General.IsNumeric(txtCOP_NbChaudiere.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur de saisie, Le nombre de chaudière doit être de type numérique.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return functionReturnValue;
                }
            }

            if (!string.IsNullOrEmpty(txtCOP_NbEchangeur.Text))
            {
                if (!General.IsNumeric(txtCOP_NbEchangeur.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur de saisie, Le nombre d'échangeur doit être de type numérique.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return functionReturnValue;
                }
            }



            //if (!string.IsNullOrEmpty(txtCOP_NoAuto.Text))
            //{
            //            '== interdit de modifier la durée si un calucul de provision a été éffectué.
            //            sCOP_Duree = fc_ADOlibelle("SELECT COP_Duree FROM COP_COntratP2 WHERE COP_Noauto = " & txtCOP_NoAuto)
            //             If UCase(sCOP_Duree) <> UCase(txtCOP_Duree) Then
            //                    sSQL = fc_ADOlibelle("SELECT P3E_Noauto FROM P3E_EnTete WHERE COP_Noauto =" & txtCOP_NoAuto)
            //                    If sSQL <> "" Then
            //                            MsgBox "Vous ne pouvez pas modifier la durée, " _
            //'                            & " des provisions ont été effectuées pour ce contrat. Cette modification est annulée.", vbCritical, ""
            //                            txtCOP_Duree = sCOP_Duree
            //                    End If
            //             End If
            //}



            if (blnAjout == true)
            {
                //-- Controle si l'immeuble a déjà un contrat.
                //sSQL = "SELECT     codeimmeuble, COP_Avenant, COP_NoContrat, numfichestandard " _
                //'        & " From COP_ContratP2" _
                //'        & " WHERE (codeimmeuble ='" & gFr_DoublerQuote(sCode) & "')"

                sSQL = "SELECT     codeimmeuble, COP_Avenant, COP_NoContrat, numfichestandard "
                    + " From COP_ContratP2" + " WHERE numfichestandard = " + txtNumFichestandard.Text + " order by COP_Avenant DESC";
                var tmpAdo = new ModAdo();
                rstmp = tmpAdo.fc_OpenRecordSet(sSQL);
                if (rstmp.Rows.Count > 0)
                {
                    // If Msgbox("Il existe déjà un contrat associé à cet immeuble, Voulez vous en créer" _
                    //& " un avenant?", vbYesNo, "") = vbNo Then
                    //Exit Function
                    //End If
                    txtCOP_Avenant.Text = Convert.ToInt32(General.nz(rstmp.Rows[0]["COP_Avenant"], 0)) + 1 + "";
                    txtCOP_NoContrat.Text = rstmp.Rows[0]["COP_NoContrat"] + "";
                    txtNumFichestandard.Text = rstmp.Rows[0]["Numfichestandard"] + "";
                    txtCodeImmeuble.Text = rstmp.Rows[0]["CodeImmeuble"] + "";
                    txtCOP_DateDemande.Text = Convert.ToString(DateTime.Now);
                }
                else
                {
                    txtCOP_Avenant.Text = "0";
                    //=== recherche du numéro de contrat.
                    txtCOP_NoContrat.Text = fc_NoContrat(txtCodeImmeuble.Text);
                }

                //=== cree le contrat dans la table cop_contrat
                FC_insert();

                if (txtCOP_Avenant.Text == "0")
                {
                    //l' avenant étant à 0 donc c est un nouvel immeuble
                    //on rattache les gammes par défaut de l'immeuble(crée dans la fiche immeuble)
                    // et on leur attribue in numéro de contrat.
                    // fc_CreeGamme
                }
            }
            else
            {
                if (fc_ControleSaisieErr() == true)//tested
                {
                    return functionReturnValue;
                }

                //== Modif Rachid 07/10/2004 mis à jour de la date de fin de contrat pour les interventions P2
                //== avec la date de fin de contrat.

                //fc_MAJvisiteP2 txtCOP_DateFin, txtCOP_NoAuto
                fc_Avenant();
                fc_maj(sCode);//tested

            }

            GridPrestations.UpdateData();
            fc_DeBloqueForm();
            // Fc_ValideGrille(); cette fonction existe mais son code est commenté

            // fc_CtrlCycle
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

            //--si le contrat est accepté, mise à jour des motifs de l'immeuble.
            if (txtCOP_Statut.Text == "A" || txtCOP_Statut.Text == "AR")
            {
                fc_MAJMotifV2(Convert.ToInt32(txtCOP_NoAuto.Text), txtCodeImmeuble.Text);
            }

            //=== conrôle si ce contrat est liée à un contrat sous traitant  non résilié.
            //=== mise en commentaire de la fonction (dédié à Gesten) la 01/&é/2015.
            //fc_CtrSSt txtCodeimmeuble

            blModif = false;
            return functionReturnValue;
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        private void fc_CtrSSt(string sCodeImmeuble)
        {
            string sSQL = null;
            //=== court circuite la fonction dédié à Gesten le 02/12/2015.
            return;

            lblContratResilie.Visible = false;

            if (txtCOP_Statut.Text.ToUpper() == "R")
            {
                sSQL = "SELECT CST_resilie From CST_ContratSsTraitant " + " WHERE CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                using (var tmpAdo = new ModAdo())
                    sSQL = tmpAdo.fc_ADOlibelle(sSQL);
                if (!string.IsNullOrEmpty(sSQL))
                {
                    if (sSQL == "0")
                    {
                        lblContratResilie.Visible = true;
                    }
                    else
                    {
                        lblContratResilie.Visible = false;
                    }
                }
            }
        }
        private void fc_Avenant()
        {
            DataTable rsAv = default(DataTable);
            int i;
            string sSQL = null;


            //== 21/06/2006.
            //== Si une date de fin de contrat est saisie sur le contrat avenant 0
            //== on reporte cette date de fin de contrat sur les eventuels
            //== avenant en varifiant si cette date est deja
            //== presente sur l(es)avenant.
            //== Dans le cas ou l'utilisateur supprime cette date de fin sur le contrat
            //== principal (avenant 0), le programme controlera et avertira
            //== l'utilisateur qu'une date de fin est presente sur les avenants.
            try
            {
                if (txtCOP_Avenant.Text.Trim() == "0" && string.IsNullOrEmpty(txtCOP_DateFin.Text))
                {
                    sSQL = "SELECT COP_DateFin FROM COP_ContratP2 WHERE COP_Noauto =" + General.nz(txtCOP_NoAuto.Text, 0);
                    var tmpAdo = new ModAdo();
                    sSQL = tmpAdo.fc_ADOlibelle(sSQL);
                    if (General.IsDate(sSQL))
                    {
                        sSQL = "SELECT COP_NoContrat, COP_DateFin FROM COP_ContratP2 " + " WHERE COP_NoContrat ='" + txtCOP_NoContrat.Text + "'" + " AND COP_Avenant <> 0";
                        sSQL = tmpAdo.fc_ADOlibelle(sSQL);
                        if (!string.IsNullOrEmpty(sSQL))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention vous avez supprimé la date de fin de contrat," + " Veuillez contrôler la date de fin de contrat pour les avenants liés à ce contrat. ", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(txtCOP_DateFin.Text))
                {
                    if (txtCOP_Avenant.Text.Trim() == "0" && General.IsDate(txtCOP_DateFin.Text))
                    {
                        sSQL = "SELECT COP_NoContrat, COP_DateFin FROM COP_ContratP2 " + " WHERE COP_NoContrat ='" + txtCOP_NoContrat.Text + "'" + " AND COP_Avenant <> 0";
                        var tmpAdorsAv = new ModAdo();
                        rsAv = tmpAdorsAv.fc_OpenRecordSet(sSQL);
                        i = 0;
                        if (rsAv.Rows.Count > 0)
                        {
                            foreach (DataRow rsAvRow in rsAv.Rows)
                            {
                                if (string.IsNullOrEmpty(General.nz(rsAvRow["COP_dateFin"], "").ToString()))
                                {
                                    rsAvRow["COP_dateFin"] = txtCOP_DateFin.Text;
                                    i = i + 1;
                                    tmpAdorsAv.Update();
                                }
                                // rsAv.MoveNext();
                            }
                        }
                        if (i == 1)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention la date de fin de contrat sera également appliquée à l'avenant du contrat " + " en cours.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else if (i > 1)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention la date de fin de contrat sera également appliquée aux avenants du contrat " + " en cours.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        // rsAv.Close();                  
                        rsAv = null;

                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Avenant");
            }
        }

        private void fc_MAJvisiteP2(string sCOP_DateFin, int lCop_NoAuto)
        {
            string sSQL = null;
            if (!string.IsNullOrEmpty(sCOP_DateFin))
            {
                sSQL = "UPDATE interventionP2 set COP_DateFin ='" + Convert.ToDateTime(sCOP_DateFin) + "'" + " where COP_NoAuto=" + lCop_NoAuto + " and DatePrevue >='" + Convert.ToDateTime(sCOP_DateFin) + "'";
                General.Execute(sSQL);
            }
            else
            {
                using (var tmpAdo = new ModAdo())
                    sSQL = tmpAdo.fc_ADOlibelle("SELECT COP_DateFin FROM COP_ContratP2" + " WHERE COP_Noauto =" + lCop_NoAuto);
                if (!string.IsNullOrEmpty(sSQL))
                {
                    sSQL = "UPDATE interventionP2 set COP_DateFin = null " + " where COP_NoAuto=" + lCop_NoAuto;
                    General.Execute(sSQL);
                }
            }
        }

        private void fc_AddMotifCtr(int lCop_NoAuto, string sCodeImmeuble, string sCodeMotif, string sLibMotif, int lNoLigne)
        {

            string sSQL = null;
            DataTable rsLoc = default(DataTable);
            DataTable rsAdd = default(DataTable);
            int lICC_Noauto = 0;
            int lCAI_NoAuto = 0;

            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                //=== prépare la requete d'ajout des motfis dans la fiche contrat.
                sSQL = "SELECT     CodeImmeuble, CAI_Code, CAI_Libelle, ICC_Noauto, ICC_NoLigne, COP_Noauto, PDAB_Noauto"
                    + " From ICC_IntervCategorieContrat WHERE     (ICC_Noauto = 0)";
                var tmpAdorsAdd = new ModAdo();
                rsAdd = tmpAdorsAdd.fc_OpenRecordSet(sSQL);

                //===rechere les localisations de l'immeuble.
                sSQL = "SELECT     PDAB_BadgePDA.PDAB_Noauto" + " FROM         PDAB_BadgePDA INNER JOIN " + " COP_ContratP2 ON PDAB_BadgePDA.Codeimmeuble = COP_ContratP2.codeimmeuble " + " Where (COP_ContratP2.COP_NoAuto =" + lCop_NoAuto + " ) " + " ORDER BY PDAB_BadgePDA.PDAB_Noauto";
                var tmpAdorsLoc = new ModAdo();
                rsLoc = tmpAdorsLoc.fc_OpenRecordSet(sSQL);

                if (rsLoc.Rows.Count > 0)
                {
                    foreach (DataRow rsLocRow in rsLoc.Rows)
                    {
                        //rsAdd.AddNew();
                        var NewRow = rsAdd.NewRow();
                        NewRow["CodeImmeuble"] = sCodeImmeuble;
                        NewRow["COP_NoAuto"] = lCop_NoAuto;
                        NewRow["PDAB_Noauto"] = rsLocRow["PDAB_Noauto"];
                        NewRow["CAI_Code"] = sCodeMotif;
                        NewRow["CAI_Libelle"] = sLibMotif;
                        NewRow["ICC_NoLigne"] = lNoLigne;
                        rsAdd.Rows.Add(NewRow);
                        tmpAdorsAdd.Update();

                        //rsAdd.MoveLast();
                        //===récupére l'id du motif contrat.

                        // Todo Salma : je dois récupére l'id du motif contrat (le dernier id)
                        //lICC_Noauto = Convert.ToInt32(rsAdd.Rows[rsAdd.Rows.Count - 1]["ICC_Noauto"]);//Verifier

                        //===ajoute les prestations lié au motif.

                        sSQL = "SELECT   CAI_Noauto" + " From CAI_CategoriInterv" + " WHERE CAI_Code = '" + StdSQLchaine.gFr_DoublerQuote(sCodeMotif) + "'";
                        lCAI_NoAuto = Convert.ToInt32(General.nz(tmpAdorsLoc.fc_ADOlibelle(sSQL), 0));
                        modP2.fc_AddOp(Convert.ToString(lCAI_NoAuto), sCodeImmeuble, lCop_NoAuto, lICC_Noauto);

                        // rsLoc.MoveNext();
                    }
                }


                //=== ferme les recordset.
                /*  if ((rsAdd != null))
                  {
                      if (rsAdd.State == ADODB.ObjectStateEnum.adStateOpen)
                      {
                          rsAdd.Close();
                      }

                      rsAdd = null;
                  }*/
                //rsLoc.Close();

                rsLoc = null;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_AddMotifCtr");
            }

        }
        private void fc_MAJMotifV2(int lCop_NoAuto, string sCodeImmeuble)
        {
            //#######===== Nouvelle methode de mise à jours des motifs


            DataTable rs = default(DataTable);
            DataTable rsAdd = default(DataTable);
            DataTable rsCtr = default(DataTable);
            string[] sTatut = null;
            string sCAI_Libelle = null;
            string sSQL = null;
            string sWhere = null;
            int i = 0;
            int lCont = 0;
            int llCAI_NoAuto = 0;
            int lNewCAI_NoAuto = 0;
            bool bFind = false;
            bool bEmpty = false;

            //=== court-circuite la fonction (dédié à Gesten) le 01/12/2015.
            return;

            try
            {
                //=== controle si il existe des motifs dans le contrat.
                sSQL = "SELECT     CodeImmeuble, COP_Noauto From ICC_IntervCategorieContrat" + " WHERE  COP_Noauto = " + lCop_NoAuto;
                var tmpAdo = new ModAdo();
                sSQL = tmpAdo.fc_ADOlibelle(sSQL);

                if (!string.IsNullOrEmpty(sSQL))
                {
                    //=== si il existe des motifs alors on considére les motifs du contrat sont à jour.
                    sWhere = "";
                    bEmpty = false;
                }
                else
                {
                    sWhere = " OR TYM_CODE='CTR'";
                    bEmpty = true;
                }

                i = 0;
                lCont = 0;
                //=== controle si les motifs généraux et eventuellement des motfis contrats sont présent dans la fiche immeuble,
                //=== si négatif on les ajoute.
                sSQL = "SELECT CAI_CODE, CAI_Libelle, TYM_CODE FROM CAI_CategoriInterv WHERE TYM_CODE='GEN' " + sWhere + " order by CAI_CODE";
                rs = tmpAdo.fc_OpenRecordSet(sSQL);

                //=== Stocke les codes des motifs trouvée dans les contrats liée à l'immeuble.
                if (rs.Rows.Count > 0)
                {
                    foreach (DataRow rsRows in rs.Rows)
                    {
                        if (rsRows["CAI_Code"].ToString() != null && !string.IsNullOrEmpty(rsRows["CAI_Code"].ToString()))
                        {
                            Array.Resize(ref sTatut, i + 1);
                            sTatut[i] = General.nz(rsRows["CAI_Code"], "").ToString();
                            i = i + 1;
                            //=== ajoute tous les motifs CTR dans la fiche contrat.
                            if (!string.IsNullOrEmpty(sWhere) && rsRows["TYM_CODE"].ToString().ToUpper() == "CTR".ToUpper())
                            {
                                lCont = lCont + 1;
                                fc_AddMotifCtr(lCop_NoAuto, sCodeImmeuble, General.nz(rsRows["CAI_Code"], "").ToString(), General.nz(rsRows["CAI_Libelle"], "").ToString(), lCont);
                            }
                        }

                    }
                }
                // rs.Close();

                //=== Recupére tous les code des motifs de l'immeuble afin de trouver les motifs manquants.

                sSQL = "SELECT CAI_Code,CAI_NoAuto, CodeImmeuble,CAI_Libelle FROM ICA_ImmCategorieInterv "
                    + " WHERE codeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                rs = tmpAdo.fc_OpenRecordSet(sSQL);

                //=== AJOUT DES MOTIFS SI INEXISTANT DANS FICHE IMMEUBLE.
                for (i = 0; i <= sTatut.Length; i++)
                {
                    bFind = false;
                    foreach (DataRow rsRows in rs.Rows)
                    {
                        if (General.nz(rsRows["CAI_Code"], "").ToString().ToUpper() == sTatut[i].ToUpper().ToString())
                        {
                            bFind = true;
                            break; // TODO: might not be correct. Was : Exit Do
                        }

                    }

                    //=== si code non présent on le crée.
                    if (bFind == false)
                    {
                        sCAI_Libelle = tmpAdo.fc_ADOlibelle("SELECT CAI_Libelle FROM CAI_CategoriInterv WHERE CAI_CODE ='" + sTatut[i] + "'");
                        if (!string.IsNullOrEmpty(sCAI_Libelle))
                        {
                            // rs.AddNew();
                            var NewRow = rs.NewRow();
                            NewRow["CAI_Code"] = sTatut[i];
                            NewRow["CodeImmeuble"] = sCodeImmeuble;
                            NewRow["CAI_Libelle"] = sCAI_Libelle;
                            rs.Rows.Add(NewRow);
                            tmpAdo.Update();

                            //=== Récupére le nouvel ID.
                            lNewCAI_NoAuto = Convert.ToInt32(NewRow["CAI_Noauto"]);

                            //=== Recupére l'ID du motif dans la table CAI_CategoriInterv.

                            llCAI_NoAuto = Convert.ToInt32(tmpAdo.fc_ADOlibelle("SELECT CAI_NoAuto FROM CAI_CategoriInterv WHERE CAI_CODE ='" + sTatut[i] + "'"));

                            //=== Supprime les éventuels doublons.
                            sSQL = "SELECT FacArticle.CodeArticle" + " FROM CAI_CategoriInterv INNER JOIN FacArticle "
                            + " ON CAI_CategoriInterv.CAI_Noauto = FacArticle.CAI_Noauto" + " WHERE (((CAI_CategoriInterv.CAI_Noauto)=" + llCAI_NoAuto + ")) ";

                            rsAdd = tmpAdo.fc_OpenRecordSet(sSQL);
                            foreach (DataRow rsAddRows in rsAdd.Rows)
                            {
                                sSQL = "DELETE FROM IMA_ImmArticle where CODEIMMEUBLE ='" + sCodeImmeuble + "'" + " and codearticle ='" + rsAddRows["CodeArticle"] + "'";
                                General.Execute(sSQL);
                            }
                            // rsAdd.Close();

                            rsAdd = null;

                            //=== Ajoute les articles correspondant à la gamme.
                            ModModeleP2.fc_AddArticle(sCodeImmeuble, lNewCAI_NoAuto, llCAI_NoAuto);
                        }
                    }
                }

                rs = null;
                if (bEmpty == false)
                {
                    ModP2v2.fc_MAJMotif(sCodeImmeuble);
                }

                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Source, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        /// <summary>
        /// tested
        /// </summary>
        /// <returns></returns>
        private bool fc_ControleSaisieErr()
        {
            bool functionReturnValue = false;
            // fc_ControleSaisieErr est à true quand un oublie ou une erreur de saisie a été décelé.

            // controle si un type de contrat a été saisie.
            switch (COP_P1.CheckState | COP_P2.CheckState | COP_P3.CheckState | COP_P4.CheckState)
            {
                case CheckState.Checked:
                    break;
                // ne fait rien
                default:
                    break;
                    //            MsgBox "Vous devez cocher le type de contrat.", vbInformation, ""
                    //            fc_ControleSaisieErr = True
                    //            SSTab1.Tab = 0
                    //            Exit Function
            }

            //controle si une activité ou une energie a bien été saisie.
            //Select Case chkCOP_CLIM Or chkCOP_CPCU Or chkCOP_ELECT Or chkCOP_FIOUL Or chkCOP_Gaz Or chkCOP_SURP Or chkCOP_TE _
            //'            Or chkCOP_VMC Or chkCOP_TeleAlarme Or ChkCOP_EnAttente Or chkCOP_Murale Or chkCOP_Disconnecteur _
            //'            Or ChkCOP_Relevage Or ChkCOP_Particulier Or ChkCOP_EcsOui.value Or ChkCOP_EcsNon.value Or chkCOP_VmcDsc.value _
            //'            Or ChkCOP_CHAUCLIM

            switch (chkCOP_CLIM.CheckState | chkCOP_CPCU.CheckState | chkCOP_ELECT.CheckState | chkCOP_FIOUL.CheckState | chkCOP_Gaz.CheckState | chkCOP_SURP.CheckState | chkCOP_TE.CheckState | chkCOP_VMC.CheckState | chkCOP_TeleAlarme.CheckState | chkCOP_Murale.CheckState | chkCOP_Disconnecteur.CheckState | ChkCOP_Relevage.CheckState | ChkCOP_Ecs.CheckState | chkCOP_VmcDsc.CheckState | ChkCOP_CHAUCLIM.CheckState | chk0.CheckState | chk1.CheckState | chk2.CheckState | chk6.CheckState | ChkCOP_EnAttente.CheckState)
            {

                case CheckState.Checked:
                    break;
                //ne fait rien
                default:
                    break;
                    //ne fait rien

                    //MsgBox "Vous devez cocher un type d'energie ou d'activité de l'immeuble", vbInformation, ""
                    //fc_ControleSaisieErr = True
                    //SSTab1.Tab = 0
                    //Exit Function

            }

            //If UCase(txtCOP_Statut) = "A" And Not IsDate(txtCOP_DateSignature) Then
            //            MsgBox "Le statut du contrat est en A, " _
            //'            & " vous devez saisir une date de debut du cycle contractuel.", vbInformation, ""
            //            fc_ControleSaisieErr = True
            //            SSTab1.Tab = 0
            //            Exit Function
            //
            //End If


            //si le statut est passé en A on oblige la date de prise d'effet.
            //if (txtCOP_Statut.Text.ToUpper().ToString() == "A" && !General.IsDate(txtCOP_DateEffet.Text))
            //{
            //            MsgBox "Le statut du contrat est en A, " _
            //'            & " vous devez saisir une date de prise d'effet.", vbInformation, ""
            //            fc_ControleSaisieErr = True
            //            SSTab1.Tab = 0
            //            Exit Function

            //}

            //if (txtCOP_Statut.Text.ToUpper().ToString() == "A" && txtCOP_DateAcceptation.Value != null && !General.IsDate(txtCOP_DateAcceptation.Value.ToString()))
            //{
            //        If fc_DroitAnalytique = True Then
            //            MsgBox "Le statut du contrat est en A, " _
            //'            & " vous devez saisir une date de prise d'acceptation.", vbInformation, ""
            //            fc_ControleSaisieErr = True
            //            SSTab1.Tab = 0
            //            Exit Function
            //        End If
            //}


            //if (txtCOP_Statut.Text.ToUpper().ToString() == "AR" && txtCOP_DateAcceptation.Value != null && !General.IsDate(txtCOP_DateAcceptation.Value.ToString()))
            //{
            //        If fc_DroitAnalytique = True Then
            //            MsgBox "Le statut du contrat est en AR, " _
            //'            & " vous devez saisir une date de prise d'acceptation.", vbInformation, ""
            //            fc_ControleSaisieErr = True
            //            SSTab1.Tab = 0
            //            Exit Function
            //        End If
            //}

            //si le statut est passé en A on oblige la date de prise d'effet.
            if (txtCOP_Statut.Text.ToUpper().ToString() == "A" && !General.IsDate(txtCOP_DateSignature.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le statut du contrat est en A, " + " vous devez saisir une date de debut de planification.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = true;
                SSTab1.SelectedTab = SSTab1.Tabs[0];
                return functionReturnValue;

            }

            //if (txtCOP_Statut.Text.ToUpper().ToString() == "R" && txtCOP_DateDemandeResiliation.Value != null && !General.IsDate(txtCOP_DateDemandeResiliation.Value.ToString()))
            //{
            //         If fc_DroitAnalytique = True Then
            //            MsgBox "Le statut du contrat est en R, " _
            //'            & " vous devez saisir une date demande de Résilitation.", vbInformation, ""
            //            fc_ControleSaisieErr = True
            //            SSTab1.Tab = 0
            //            Exit Function
            //        End If
            //}

            if (txtCOP_Statut.Text.ToUpper().ToString() == "R" && !General.IsDate(txtCOP_DateFin.Text))
            {
                //            MsgBox "Le statut du contrat est en R, " _
                //'            & " vous devez saisir une date fin de contrat.", vbInformation, ""
                //            fc_ControleSaisieErr = True
                //            SSTab1.Tab = 0
                //            Exit Function

            }

            if (!string.IsNullOrEmpty(txtCOP_DateImpression.Text) && !General.IsDate(txtCOP_DateImpression.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date d'impression n'est pas valide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = true;
                SSTab1.SelectedTab = SSTab1.Tabs[0];
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtCOP_DateEffet.Text) && !General.IsDate(txtCOP_DateEffet.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date de prise d'effet n'est pas valide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = true;
                SSTab1.SelectedTab = SSTab1.Tabs[0];
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtCOP_DateFin.Text) && !General.IsDate(txtCOP_DateFin.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date de prise de fin de contrat n'est pas valide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = true;
                SSTab1.SelectedTab = SSTab1.Tabs[0];
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtCOP_Duree.Text) && !General.IsNumeric(txtCOP_Duree.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La durée du contrat n'est pas valide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = true;
                SSTab1.SelectedTab = SSTab1.Tabs[0];
                return functionReturnValue;
            }

            if (!string.IsNullOrEmpty(txtCOP_DateTaciteReconduction.Text) && !General.IsDate(txtCOP_DateTaciteReconduction.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date de tacite reconduction(TR) n'est pas valide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = true;
                SSTab1.SelectedTab = SSTab1.Tabs[0];
                return functionReturnValue;
            }

            //===> Mondir le 18.02.2021, commnted in V16.02.2021
            //if (txtCOP_DateAcceptation.Value != null && txtCOP_DateAcceptation.Value.ToString() != "" && !General.IsDate(txtCOP_DateAcceptation.Value.ToString()))
            //{
            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date d'acceptation n'est pas valide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    functionReturnValue = true;
            //    SSTab1.SelectedTab = SSTab1.Tabs[0];
            //    return functionReturnValue;
            //}

            //if (txtCOP_DateDemandeResiliation.Value != null && txtCOP_DateDemandeResiliation.Value.ToString() != "" && !General.IsDate(txtCOP_DateDemandeResiliation.Value.ToString()))
            //{
            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date de demande de résiliation n'est pas valide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    functionReturnValue = true;
            //    SSTab1.SelectedTab = SSTab1.Tabs[0];
            //    return functionReturnValue;
            //}
            //===> Fin Modif Mondir
            return functionReturnValue;


            //=== temporaire controle si l'util a bien coché les cases ecs(oui, non).
            //If UCase(App.EXEName) = UCase(cProjection) Then

            //    If ChkCOP_EcsOui.value = 0 And ChkCOP_EcsNon.value = 0 Then
            //        MsgBox "Veuillez préciser si ce contrat prend en charge l'ECS."
            //        fc_ControleSaisieErr = True
            //        SSTab1.Tab = 0
            //        Exit Function
            //    End If
            //    If ChkCOP_EcsOui.value = 1 And ChkCOP_EcsNon.value = 1 Then
            //        MsgBox "Attention vous ne pouvez pas cocher en même temps 'Oui' et 'Non' pour l'ECS.", vbExclamation, ""
            //        fc_ControleSaisieErr = True
            //        SSTab1.Tab = 0
            //        Exit Function
            //    End If


            //End If

        }
        private void fc_CreeGamme()
        {
            string sWhere = null;
            //récupére toutes les gammes du l'immeuble non liée à un contrat.
            General.sSQL = "SELECT ICA_ImmCategorieInterv.CAI_Code, ICA_ImmCategorieInterv.CAI_Libelle,"
                + " ICA_ImmCategorieInterv.CodeImmeuble, COP_NoAuto"
                + " From ICA_ImmCategorieInterv" + " WHERE ICA_ImmCategorieInterv.CodeImmeuble ='" + txtCodeImmeuble.Text + "'"
                + "  and ( COP_NoAuto is  null  or    COP_NoAuto = 0) ";
            var tmpAdo = new ModAdo();
            rstmp = tmpAdo.fc_OpenRecordSet(General.sSQL);
            if (rstmp.Rows.Count > 0)
            {
                foreach (DataRow rstmpRow in rstmp.Rows)
                {
                    //attache ces gammes au contrat en cours.
                    rstmpRow["COP_NoAuto"] = txtCOP_NoAuto.Text;
                    int xx = tmpAdo.Update();
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " and ( ICA_ImmCategorieInterv.CAI_Code ='" + StdSQLchaine.gFr_DoublerQuote(rstmpRow["CAI_Code"].ToString()) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " OR ICA_ImmCategorieInterv.CAI_Code ='" + StdSQLchaine.gFr_DoublerQuote(rstmpRow["CAI_Code"].ToString()) + "'";
                    }

                }
                sWhere = sWhere + ")";
                lblInsertion.Visible = true;
                PB1.Visible = true;

                modP2.fc_CreeGammeFille(sWhere, PB1.Value, lblInsertion, txtCodeImmeuble.Text, Convert.ToInt32(txtCOP_NoContrat.Text));
                lblInsertion.Visible = false;
                PB1.Visible = false;
            }

        }
        private void FC_insert()
        {

            //sSQl = "SELECT     FAP_CodeArticle, FAP_Designation1, FAP_NoAuto, EQM_NoAuto, CodeImmeuble" _
            //& " FROM FAP_FacArticleP2 where  "
            //sSQl = "Insert into COP_ContratP2 ( codeimmeuble, COP_NoContrat, COP_Avenant, annee, mois , ordre)"
            //   sSQl = sSQl & " VALUES ('" & gFr_DoublerQuote(txtCodeImmeuble) _
            //& "','" & gFr_DoublerQuote(txtCOP_NoContrat) & "'," _
            //& txtCOP_Avenant & ",'" & txtannee & "','" & txtMois & "','" & txtordre & "')"
            //    adocnn.Execute sSQl
            try
            {
                General.sSQL = "SELECT codeimmeuble, COP_NoContrat, COP_Avenant, annee, mois , ordre, COP_NoAuto, NumFichestandard, COP_DateDemande"
                             + " , COP_Statut FROM COP_ContratP2 WHERE COP_NoAuto = 0";
                var tmpAdo = new ModAdo();

                rstmp = tmpAdo.fc_OpenRecordSet(General.sSQL);
                // rstmp.AddNew();
                var NewRow = rstmp.NewRow();
                NewRow["CodeImmeuble"] = txtCodeImmeuble.Text;
                NewRow["COP_NoContrat"] = txtCOP_NoContrat.Text;
                NewRow["COP_Avenant"] = txtCOP_Avenant.Text;
                NewRow["Annee"] = txtannee.Text;
                NewRow["mois"] = txtMois.Text;
                NewRow["ordre"] = txtordre.Text;
                NewRow["Numfichestandard"] = txtNumFichestandard.Text;
                NewRow["COP_DateDemande"] = txtCOP_DateDemande.Text;
                NewRow["COP_Statut"] = "00";
                rstmp.Rows.Add(NewRow);
                // en attente
                txtCOP_Statut.Text = "00";

                SqlCommandBuilder cb = new SqlCommandBuilder(tmpAdo.SDArsAdo);
                tmpAdo.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                tmpAdo.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                tmpAdo.SDArsAdo.InsertCommand = insertCmd;

                tmpAdo.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                {

                    if (e.StatementType == StatementType.Insert)
                    {

                        if (e.Command.Parameters["@ID"].Value.ToString() != "")
                            NewRow["COP_NoAuto"] = Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                    }
                });
                int xx = tmpAdo.Update();
                lbllibCOP_Statut.Text = tmpAdo.fc_ADOlibelle("SELECT ETC_Libelle " + " From ETC_Contrat" + " WHERE     (ETC_Code = '" + txtCOP_Statut.Text + "') ");
                //rsTmp.CancelUpdate
                txtCOP_NoAuto.Text = NewRow["COP_NoAuto"].ToString();
                rstmp.Dispose();
                rstmp = null;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";FC_insert");
            }
        }
        private string fc_NoContrat(string sCode)
        {
            DataTable rs = default(DataTable);
            string sAnnee = null;
            string sMois = null;
            string sOrdre = null;
            string sLettre = null;
            string sSecteur = null;
            short iStart = 0;
            // Todo: Salma Verifier Right(Format(Now, "YY"), 2)
            sAnnee = DateTime.Now.ToString("yy");
            sMois = DateTime.Now.Month.ToString("00");

            //recherche du dernier numero de devis pour l annee et le mois
            rs = new DataTable();
            General.SQL = "SELECT Ordre FROM COP_ContratP2 WHERE (Annee ='" + sAnnee + "') AND (Mois ='" + sMois + "') ORDER BY Ordre DESC";
            var tmpAdo = new ModAdo();
            rs = tmpAdo.fc_OpenRecordSet(General.SQL);
            sSecteur = General.Left(tmpAdo.fc_ADOlibelle("select SEC_code from immeuble where codeimmeuble= '" + sCode + "'"), 1);

            if (rs.Rows.Count > 0)
            {
                sOrdre = rs.Rows[0]["ordre"] + "";
                sOrdre = Convert.ToString(1 + Convert.ToInt32(sOrdre));
                if (sOrdre.Length == 1)
                {
                    sOrdre = "00" + sOrdre;
                }
                if (sOrdre.Length == 2)
                {
                    sOrdre = "0" + sOrdre;
                }
            }
            else
            {
                sOrdre = "001";
            }
            // _with22.Close();

            rs = null;
            txtannee.Text = sAnnee;
            txtMois.Text = sMois;
            txtordre.Text = sOrdre;
            //If sPoly = "1" Then
            //     fc_NoContrat = ".C" & sAnnee & "." & sMois & sSecteur & sOrdre
            //Else
            return sAnnee + "." + sMois + ".C" + sSecteur + sOrdre;
            //End If

        }
        /// <summary>
        /// tesed
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_maj(string sCode)
        {
            DataTable rs = default(DataTable);

            General.sSQL = "Update COP_ContratP2 set ";
            General.sSQL = General.sSQL + "COP_Commentaire='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCOP_Commentaire.Text), "") + "',";
            General.sSQL = General.sSQL + "COP_Document='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCOP_Document.Text), "") + "',";
            General.sSQL = General.sSQL + "COP_DateImpression=" + (General.IsDate(txtCOP_DateImpression.Text) ? "'" + Convert.ToDateTime(txtCOP_DateImpression.Text) + "'" : "Null") + ",";
            General.sSQL = General.sSQL + "COP_DateDemande=" + (General.IsDate(txtCOP_DateDemande.Text) ? "'" + txtCOP_DateDemande.Text + "'" : "Null") + ",";
            General.sSQL = General.sSQL + "COP_DateFin=" + (General.IsDate(txtCOP_DateFin.Text) ? "'" + Convert.ToDateTime(txtCOP_DateFin.Text) + "'" : "Null") + ",";
            General.sSQL = General.sSQL + "COP_DateTaciteReconduction=" + (General.IsDate(txtCOP_DateTaciteReconduction.Text) ? "'" + Convert.ToDateTime(txtCOP_DateTaciteReconduction.Text) + "'" : "Null") + ",";

            //===> Mondir le 18.02.2021 commented in V16.02.2021
            //if (txtCOP_DateAcceptation.Value != null)
            //{
            //    General.sSQL = General.sSQL + "COP_DateAcceptation=" + (General.IsDate(txtCOP_DateAcceptation.Value.ToString()) ? "'" + Convert.ToDateTime(txtCOP_DateAcceptation.Value) + "'" : "Null") + ",";
            //}

            //if (txtCOP_DateDemandeResiliation.Value != null)
            //{
            //    General.sSQL = General.sSQL + "COP_DateDemandeResiliation=" + (General.IsDate(txtCOP_DateDemandeResiliation.Value.ToString()) ? "'" + Convert.ToDateTime(txtCOP_DateDemandeResiliation.Value) + "'" : "Null") + ",";
            //}
            //===> Fin Modif Mondir

            General.sSQL = General.sSQL + "COP_DateSignature=" + (General.IsDate(txtCOP_DateSignature.Text) ? "'" + Convert.ToDateTime(txtCOP_DateSignature.Text) + "'" : "Null") + ",";
            General.sSQL = General.sSQL + "COP_DateEffet=" + (General.IsDate(txtCOP_DateEffet.Text) ? "'" + Convert.ToDateTime(txtCOP_DateEffet.Text) + "'" : "Null") + ",";
            General.sSQL = General.sSQL + "COP_Duree=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_Duree.Text), "Null") + ",";
            General.sSQL = General.sSQL + "COP_Statut='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCOP_Statut.Text), "") + "',";
            General.sSQL = General.sSQL + "COP_SStraitant='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCOP_SStraitant.Text), "") + "',";

            //===> Mondir le 18.02.2021 commented in V16.02.2021
            //General.sSQL = General.sSQL + "COP_TotalHT=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_TotalHT.Text), 0) + ",";
            //General.sSQL = General.sSQL + "COP_Coef=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_Coef.Text), 0) + ",";
            //General.sSQL = General.sSQL + "COP_TotalApresCoef=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_TotalApresCoef.Text), 0) + ",";
            //===> Fin Mondif

            General.sSQL = General.sSQL + "COP_MontantGarantie=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_MontantGarantie.Text), 0) + ",";
            General.sSQL = General.sSQL + "COP_NbLogement =" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_NbLogement.Text), 0) + ",";

            //===> Mondir le 18.02.2021 commented in V16.02.2021
            //General.sSQL = General.sSQL + "COP_TotalHTgest=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_TotalHTgest.Text), 0) + ",";
            //General.sSQL = General.sSQL + "COP_Coefgest=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_Coefgest.Text), 0) + ",";
            //General.sSQL = General.sSQL + "COP_TotalApresCoefgest=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_TotalApresCoefgest.Text), 0) + ",";
            //General.sSQL = General.sSQL + "COP_TotalHTst=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_TotalHTST.Text), 0) + ",";
            //General.sSQL = General.sSQL + "COP_Coefst=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_CoefSt.Text), 0) + ",";
            //General.sSQL = General.sSQL + "COP_TotalApresCoefst=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_TotalApresCoefSt.Text), 0) + ",";
            //===> Fin Mondif

            General.sSQL = General.sSQL + "NumFicheStandard=" + General.nz(StdSQLchaine.gFr_NombreAng(txtNumFichestandard.Text), 0) + ",";
            General.sSQL = General.sSQL + "COP_Technicien='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCOP_Technicien.Text), "") + "',";

            //===> Mondir le 18.02.2021 commented in V16.02.2021
            //General.sSQL = General.sSQL + "COP_SuiviePar='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCOP_SuiviePar.Text), "") + "',";
            //General.sSQL = General.sSQL + "COP_Objet='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCOP_Objet.Text), "") + "',";
            //===> Fin Mondif

            //sSQL = sSQL & "COP_P1='" & nz(gFr_DoublerQuote(COP_P1), 0) & "',"
            //sSQL = sSQL & "COP_P2='" & nz(gFr_DoublerQuote(COP_P2), 0) & "',"
            //sSQL = sSQL & "COP_P3='" & nz(gFr_DoublerQuote(COP_P3), 0) & "',"
            //sSQL = sSQL & "COP_P4='" & nz(gFr_DoublerQuote(COP_P4), 0) & "',"
            General.sSQL = General.sSQL + "COP_FIOUl='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chkCOP_FIOUL.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_gaz='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chkCOP_Gaz.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_CPCU ='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chkCOP_CPCU.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_VmcDsc ='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chkCOP_VmcDsc.Checked ? "1" : "0"), 0) + "',";

            General.sSQL = General.sSQL + "COP_ELECT ='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chkCOP_ELECT.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_CLIM='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chkCOP_CLIM.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_VMC='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chkCOP_VMC.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_SURP ='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chkCOP_SURP.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_MURALE ='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chkCOP_Murale.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_TE='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chkCOP_TE.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_TELEALARME='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chkCOP_TeleAlarme.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_EnAttente='" + General.nz(StdSQLchaine.gFr_DoublerQuote(ChkCOP_EnAttente.Checked ? "1" : "0"), 0) + "',";
            // sSQL = sSQL & "COP_Particulier='" & nz(gFr_DoublerQuote(ChkCOP_Particulier), 0) & "',"       
            General.sSQL = General.sSQL + "COP_Disconnecteur='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chkCOP_Disconnecteur.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_Relevage='" + General.nz(StdSQLchaine.gFr_DoublerQuote(ChkCOP_Relevage.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_CHAUCLIM='" + General.nz(StdSQLchaine.gFr_DoublerQuote(ChkCOP_CHAUCLIM.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "NumContratRef='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtNumContratRef.Text), "") + "',";
            General.sSQL = General.sSQL + "AvenantRef=" + General.nz(StdSQLchaine.gFr_NombreAng(txtAvtRef.Text), "Null") + ",";


            if (string.IsNullOrEmpty(txtCOP_GroupeNoContrat.Text))
            {
                txtCOP_GroupeAvenant.Text = "";
                txtCOP_GroupeNoAuto.Text = "";
                General.sSQL = General.sSQL + "COP_GroupeNoContrat='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCOP_GroupeNoContrat.Text), "") + "',";
                General.sSQL = General.sSQL + "COP_GroupeAvenant=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_GroupeAvenant.Text), "Null") + ",";
                General.sSQL = General.sSQL + "COP_GroupeNoAuto=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_GroupeNoAuto.Text), "Null") + ",";
            }
            //===  modif du 16 juin 2008, controle le numero de contrat.
            if (!string.IsNullOrEmpty(txtCOP_GroupeNoContrat.Text))
            {
                using (var tmpAdo = new ModAdo())
                    rs = tmpAdo.fc_OpenRecordSet("SELECT COP_NoContrat, COP_Avenant, COP_NoAuto "
                        + " From COP_ContratP2" + " WHERE COP_NoContrat = '"
                        + StdSQLchaine.gFr_DoublerQuote(txtCOP_GroupeNoContrat.Text) + "'"
                        + " AND COP_Avenant = " + General.nz(txtCOP_GroupeAvenant.Text, 0));
                if (rs.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez attaché cette fiche contrat à un numéro de contrat inexistant.", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                }
                else
                {
                    txtCOP_GroupeAvenant.Text = rs.Rows[0]["COP_Avenant"] + "";
                    txtCOP_GroupeNoAuto.Text = rs.Rows[0]["COP_NoAuto"] + "";

                    General.sSQL = General.sSQL + "COP_GroupeNoContrat='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCOP_GroupeNoContrat.Text), "") + "',";
                    General.sSQL = General.sSQL + "COP_GroupeAvenant=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_GroupeAvenant.Text), "Null") + ",";
                    General.sSQL = General.sSQL + "COP_GroupeNoAuto=" + General.nz(StdSQLchaine.gFr_NombreAng(txtCOP_GroupeNoAuto.Text), "Null") + ",";
                }
                rs.Dispose();

                rs = null;

            }

            //       If ChkCOP_EcsOui.value = 1 Then
            //            sSQL = sSQL & "COP_Ecs = 1,"
            //
            //        ElseIf ChkCOP_EcsNon.value = 1 Then
            //            sSQL = sSQL & "COP_Ecs = 0,"
            //
            //        Else
            //            sSQL = sSQL & "COP_Ecs = NULL,"
            //        End If


            General.sSQL = General.sSQL + "COP_Ecs ='" + General.nz(StdSQLchaine.gFr_DoublerQuote(ChkCOP_Ecs.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_RESEAU ='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chk0.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_BOIS ='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chk1.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_CHAUFFAGE ='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chk2.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_SOLAIRE ='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chk6.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_RendCEE ='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chk3.Checked ? "1" : "0"), 0) + "',";
            General.sSQL = General.sSQL + "COP_SFioul ='" + General.nz(StdSQLchaine.gFr_DoublerQuote(chk4.Checked ? "1" : "0"), 0) + "',";

            General.sSQL = General.sSQL + "COP_Puissance='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCOP_Puissance.Text), 0) + "',";
            General.sSQL = General.sSQL + "COP_NbChaudiere='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCOP_NbChaudiere.Text), 0) + "',";

            General.sSQL = General.sSQL + "COP_NbEchangeur=" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCOP_NbEchangeur.Text), 0) + ",";
            General.sSQL = General.sSQL + "COP_Unite='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCOP_Unite.Text), "") + "',";
            General.sSQL = General.sSQL + "COP_Signataire='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCOP_Signataire.Text), "") + "'";

            //sSQL = sSQL & " where COP_NoContrat='" & txtCOP_NoContrat & "'" _
            //& " and COP_Avenant=" & txtCOP_Avenant

            General.sSQL = General.sSQL + " where COP_Noauto=" + General.nz(txtCOP_NoAuto.Text, 0);
            General.Execute(General.sSQL);


            //SQL = " Update immeuble set "
            //SQL = SQL & "Adresse='" & nz(gFr_DoublerQuote(txtAdresse), "") & "',"
            //SQL = SQL & "Adresse2_IMM='" & nz(gFr_DoublerQuote(txtAdresse2_IMM), "") & "',"
            //SQL = SQL & "AngleRue='" & nz(gFr_DoublerQuote(txtAngleRue), "") & "',"
            //SQL = SQL & "CodePostal='" & nz(gFr_DoublerQuote(txtCodePostal), "") & "',"
            ///        '=== les champs suivant se trouvent desormais dans la table COP_ContratP2.
            ///        SQL = SQL & "P1='" & nz(gFr_DoublerQuote(P1), 0) & "',"
            ///        SQL = SQL & "P2='" & nz(gFr_DoublerQuote(P2), 0) & "',"
            ///        SQL = SQL & "P3='" & nz(gFr_DoublerQuote(P3), 0) & "',"
            ///        SQL = SQL & "P4='" & nz(gFr_DoublerQuote(P4), 0) & "',"
            ///        SQL = SQL & "FIOUl='" & nz(gFr_DoublerQuote(chkFIOUL), 0) & "',"
            ///        SQL = SQL & "gaz='" & nz(gFr_DoublerQuote(chkGaz), 0) & "',"
            ///        SQL = SQL & "CPCU ='" & nz(gFr_DoublerQuote(chkCPCU), 0) & "',"
            ///        SQL = SQL & "ELECT ='" & nz(gFr_DoublerQuote(chkELECT), 0) & "',"
            ///        SQL = SQL & "CLIM='" & nz(gFr_DoublerQuote(chkCLIM), 0) & "',"
            ///        SQL = SQL & "VMC='" & nz(gFr_DoublerQuote(chkVMC), 0) & "',"
            ///        SQL = SQL & "SURP ='" & nz(gFr_DoublerQuote(chkSURP), 0) & "',"
            ///        SQL = SQL & "MURALE ='" & nz(gFr_DoublerQuote(chkMurale), 0) & "',"
            ///        SQL = SQL & "TE='" & nz(gFr_DoublerQuote(chkTE), 0) & "',"
            ///        SQL = SQL & "TELEALARME='" & nz(gFr_DoublerQuote(chkTeleAlarme), 0) & "',"
            ///        SQL = SQL & "EnAttente='" & nz(gFr_DoublerQuote(ChkEnAttente), 0) & "',"
            ///        SQL = SQL & "Disconnecteur='" & nz(gFr_DoublerQuote(chkDisconnecteur), 0) & "',"
            ///        SQL = SQL & "Relevage='" & nz(gFr_DoublerQuote(ChkRelevage), 0) & "',"
            ///        SQL = SQL & "Puissance='" & nz(gFr_DoublerQuote(txtPuissance), 0) & "',"
            ///        SQL = SQL & "Unite='" & nz(gFr_DoublerQuote(txtUnite), "") & "',"

            //=====================
            //SQL = SQL & "ville='" & nz(gFr_DoublerQuote(txtVille), "") & "',"
            //SQL = SQL & "CodeChefSecteur='" & nz(gFr_DoublerQuote(txtCodeChefSecteur), "") & "',"
            //SQL = SQL & "SEC_Code='" & nz(gFr_DoublerQuote(txtSEC_Code), "") & "'"
            //SQL = SQL & " where CodeImmeuble ='" & gFr_DoublerQuote(txtCodeImmeuble) & "'"



            //adocnn.Execute SQL, , adExecuteNoRecords
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            //verification ligne par ligne
            string sDesPathFile = null;
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = @"C:\";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;
            openFileDialog1.DefaultExt = "txt";
            openFileDialog1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.ReadOnlyChecked = true;
            openFileDialog1.ShowReadOnly = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                sDesPathFile = General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\contrats\\" + General.cSociete + "\\" + openFileDialog1.SafeFileName;
                fc_CreeDossier();
                Dossier.fc_DeplaceFichier(openFileDialog1.FileName, sDesPathFile);
                txtCOP_Document.Text = General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\contrats\\" + General.cSociete + "\\" + openFileDialog1.SafeFileName;

            }
        }

        /// <summary>
        /// tested
        /// </summary>
        private void fc_CreeDossier()
        {
            //- creation du dossier client
            string sCode = null;

            try
            {
                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    sCode = txtCodeImmeuble.Text;
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode);
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\correspondance");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\correspondance\\in");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\correspondance\\out");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\photos");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\devis");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\devis\\" + General.cSociete);
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\devis\\ext");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\contrats");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\contrats\\" + General.cSociete);
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\contrats\\ext\\");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\factures");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\factures\\" + General.cSociete);
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\factures\\ext");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\fax");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\fax\\in");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\fax\\out");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\email");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\email\\in");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCode + "\\email\\out");

                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_CreeDossier");
            }
        }
        private void label7_Click(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(txtCOP_Document.Text))
                {
                    ModuleAPI.Ouvrir(txtCOP_Document.Text);
                    //Process.Start(txtCOP_Document.Text);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "label7_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void mnuSupprimer_Click(object sender, EventArgs e)
        {
            bool bselect = false;
            string skey = null;
            string sID = null;
            string sSQL = null;
            UltraTreeNode oNode = null;


            try
            {
                bselect = false;

                //foreach (UltraTreeNode oNode_N1 in TreeView1.Nodes)
                //{

                //                if (oNode_N1.Selected == true)
                //                {
                //                    bselect = true;
                //                    skey = oNode_N1.Key;
                //                    sID = General.Mid(oNode_N1.Key, 2, oNode_N1.Key.Length - 1);
                //                    oNode = oNode_N1;
                //                    break; // TODO: might not be correct. Was : Exit For
                //                }

                //}
                oNode = TreeView1.ActiveNode;
                if (oNode != null)
                {
                    if (oNode.Selected)
                    {
                        bselect = true;
                        skey = oNode.Key;
                        sID = General.Mid(oNode.Key, 2, oNode.Key.Length - 1);

                    }
                }

                if (bselect == false)
                {
                    return;
                }

                if (General.sPriseEnCompteLocalP2 == "1")
                {
                    if (General.Left(oNode.Key, 1).ToString().ToUpper() == cNiv1.ToString().ToUpper())//Tested
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas supprimer une localisation.", "Suppression annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;

                    }
                    else if (General.Left(oNode.Key, 1).ToString().ToUpper() == cNiv2.ToString().ToUpper())//Tested
                    {
                        sSQL = "DELETE FROM GAI_GammeImm  Where GAi_ID = " + sID;
                        General.Execute(sSQL);
                        TreeView1.ActiveNode.Remove();

                    }
                    else if (General.Left(oNode.Key, 1).ToString().ToUpper() == cNiv3.ToString().ToUpper())
                    {//Tested

                        sSQL = "DELETE FROM ICC_IntervCategorieContrat WHERE ICC_Noauto =" + sID;
                        General.Execute(sSQL);
                        TreeView1.ActiveNode.Remove();

                    }
                    else if (General.Left(oNode.Key, 1).ToString().ToUpper() == cNiv4.ToString().ToUpper())
                    {//Tested

                        sSQL = "DELETE FROM EQM_EquipementP2Imm WHERE EQM_NoAuto = " + sID;
                        General.Execute(sSQL);
                        TreeView1.ActiveNode.Remove();
                    }
                }

                else
                {

                    if (General.Left(oNode.Key, 1).ToString().ToUpper() == cNiv1.ToString().ToUpper())
                    {//Tested

                        sSQL = "DELETE FROM ICC_IntervCategorieContrat WHERE ICC_Noauto =" + sID;
                        General.Execute(sSQL);
                        TreeView1.ActiveNode.Remove();

                    }
                    else if (General.Left(oNode.Key, 1).ToString().ToUpper() == cNiv2.ToString().ToUpper())
                    {//Tested

                        sSQL = "DELETE FROM EQM_EquipementP2Imm WHERE EQM_NoAuto = " + sID;
                        General.Execute(sSQL);
                        TreeView1.ActiveNode.Remove();
                    }

                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";mnuSupprimer_Click");
            }
        }

        private void optOperation_CheckedChanged(object sender, EventArgs e)
        {
            if (optOperation.Checked)
            {
                if (ssGridVisite.ActiveRow != null)
                    fc_ssGRIDTachesAnnuel(Convert.ToInt32(General.nz((ssGridVisite.ActiveRow.Cells["NoIntervention"].Value), 0)));
            }
        }

        private void optTaches_CheckedChanged(object sender, EventArgs e)
        {
            if (optTaches.Checked)
            {
                if (ssGridVisite.ActiveRow != null)
                    fc_ssGRIDTachesAnnuel(Convert.ToInt32(General.nz((ssGridVisite.ActiveRow.Cells["NoIntervention"].Value), 0)));
            }
        }
        private void ssCOD_ContratP2Detail_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            try
            {
                var _with23 = ssCOD_ContratP2Detail;
                if (_with23.ActiveRow == null || _with23.ActiveCell == null) return;

                if (string.IsNullOrEmpty(_with23.ActiveRow.Cells["CodeImmeuble"].Text))
                {
                    _with23.ActiveRow.Cells["CodeImmeuble"].Value = txtCodeImmeuble.Text;
                }

                if (string.IsNullOrEmpty(_with23.ActiveRow.Cells["COP_NoAuto"].Text))
                {
                    _with23.ActiveRow.Cells["COP_NoAuto"].Value = txtCOP_NoAuto.Text;
                }

                if (string.IsNullOrEmpty(_with23.ActiveRow.Cells["COP_NoContrat"].Text))
                {
                    _with23.ActiveRow.Cells["COP_NoContrat"].Value = txtCOP_NoContrat.Text;
                }

                if (string.IsNullOrEmpty(_with23.ActiveRow.Cells["COP_avenant"].Text))
                {
                    _with23.ActiveRow.Cells["COP_avenant"].Value = txtCOP_Avenant.Text;
                }

                if (_with23.ActiveCell.Column.Index == _with23.ActiveRow.Cells["COP_P1"].Column.Index)
                {

                    if (_with23.ActiveRow.Cells["COP_P1"].Text == "-1")
                    {
                        _with23.ActiveRow.Cells["COP_P1"].Value = "1";
                    }

                }

                if (_with23.ActiveCell.Column.Index == _with23.ActiveRow.Cells["COP_P2"].Column.Index)
                {

                    if (_with23.ActiveRow.Cells["COP_P2"].Text == "-1")
                    {
                        _with23.ActiveRow.Cells["COP_P2"].Value = "1";
                    }

                }

                if (_with23.ActiveCell.Column.Index == _with23.ActiveRow.Cells["COP_P3"].Column.Index)
                {

                    if (_with23.ActiveRow.Cells["COP_P3"].Text == "-1")
                    {
                        _with23.ActiveRow.Cells["COP_P3"].Value = "1";
                    }

                }

                if (_with23.ActiveCell.Column.Index == _with23.ActiveRow.Cells["COP_P4"].Column.Index)
                {

                    if (_with23.ActiveRow.Cells["COP_P4"].Text == "-1")
                    {
                        _with23.ActiveRow.Cells["COP_P4"].Value = "1";
                    }

                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "ssCOD_ContratP2Detail_BeforeExitEditMode");
            }
        }
        private void ssCOD_ContratP2Detail_BeforeRowInsert(object sender, BeforeRowInsertEventArgs e)
        {
            try
            {
                string sSQL = null;
                if (txtCOP_Statut.Text.ToString().ToUpper() == cA.ToString().ToUpper() && string.IsNullOrEmpty(txtCOP_DateEffet.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie annulée, Le statut du contrat est en A, la date de prise d'effet est obligatoire.", "Saisie annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    e.Cancel = true;
                    return;
                }
                else if (txtCOP_Statut.Text.ToString().ToUpper() == cA.ToString().ToUpper() && General.IsDate(txtCOP_DateEffet.Text))
                {
                    //== autorise la saisie
                }
                else if (General.IsDate(txtCOP_DateImpression.Text))
                {
                    //== autorise la saisie
                    sSQL = "SELECT     COP_DateImpression From COP_ContratP2 WHERE COP_NoAuto =" + txtCOP_NoAuto.Text;
                    using (var tmpAdo = new ModAdo())
                        sSQL = tmpAdo.fc_ADOlibelle(sSQL);
                    if (string.IsNullOrEmpty(sSQL))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez modifié la date d'impression, vous devez d'abord valider.", "Saisie annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    e.Cancel = true;
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La saisie du détail HT n'est possible que si le contrat " + " a une date d'impression et/ou une date de prise d'effet avec le statut A.", "Saisie annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "ssCOD_ContratP2Detail_BeforeRowInsert");
            }
        }
        private void ssCOD_ContratP2Detail_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            try
            {
                int lCount = 0;
                var _with24 = ssCOD_ContratP2Detail;

                if (General.nz(_with24.ActiveRow.Cells["COP_P1"].Text, 0).ToString() == "0" && General.nz(_with24.ActiveRow.Cells["COP_P2"].Text, 0).ToString() == "0" && General.nz(_with24.ActiveRow.Cells["COP_P3"].Text, 0).ToString() == "0" && General.nz(_with24.ActiveRow.Cells["COP_P4"].Text, 0).ToString() == "0")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner le type du contrat (P1, P2 ou P3).", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }

                lCount = 0;
                if (General.nz(_with24.ActiveRow.Cells["COP_P1"].Text, 0).ToString() != "0")
                {
                    lCount = lCount + 1;
                }

                if (General.nz(_with24.ActiveRow.Cells["COP_P2"].Text, 0).ToString() != "0")
                {
                    lCount = lCount + 1;
                }

                if (General.nz(_with24.ActiveRow.Cells["COP_P3"].Text, 0).ToString() != "0")
                {
                    lCount = lCount + 1;
                }
                if (General.nz(_with24.ActiveRow.Cells["COP_P4"].Text, 0).ToString() != "0")
                {
                    lCount = lCount + 1;
                }

                if (lCount > 1)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un seul type du contrat (P1 ou P2 ou P3).", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "");
            }
        }
        private void ssCOD_ContratP2Detail_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            var _with25 = ssCOD_ContratP2Detail;
            var tmpAdo = new ModAdo();
            _with25.ActiveRow.Cells["Libelle"].Value = tmpAdo.fc_ADOlibelle("SELECT  FacArticle.Designation1 "
                + " FROM         FacArticle" + " INNER JOIN CAI_CategoriInterv "
                + " ON FacArticle.CAI_Noauto = CAI_CategoriInterv.CAI_Noauto "
                + " INNER JOIN TYM_TypeDeMotif "
                + " ON CAI_CategoriInterv.TYM_CODE = TYM_TypeDeMotif.TYM_CODE"
                + " WHERE     TYM_TypeDeMotif.TYM_CODE = '" + StdSQLchaine.gFr_DoublerQuote(modP2.cCPREST) + "'"
                + " AND  FacArticle.CodeArticle ='" + _with25.ActiveRow.Cells["CodeArticle"].Text + "'");
            _with25.DisplayLayout.Bands[0].Columns["CodeArticle"].ValueList = ssDropFacArticle;
        }
        private void ssFAP_FacArticleP2_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            try
            {
                if (string.IsNullOrEmpty(ssFAP_FacArticleP2.ActiveRow.Cells["FAP_NoLIgne"].Text))
                {
                    lFAP_Article = lFAP_Article + 1;
                    ssFAP_FacArticleP2.ActiveRow.Cells["FAP_NoLIgne"].Value = lFAP_Article;
                }

                if (string.IsNullOrEmpty(ssFAP_FacArticleP2.ActiveRow.Cells["EQM_NoAuto"].Text))
                {
                    //ssFAP_FacArticleP2.Columns("EQM_NoAuto").value = ssGammeArticle.Columns("EQM_NoAuto").Text
                    ssFAP_FacArticleP2.ActiveRow.Cells["EQM_NoAuto"].Value = txtEQM_NoAuto;
                }

                if (ssFAP_FacArticleP2.ActiveRow.Cells["FAP_Affiche"].Text == "-1")
                {
                    ssFAP_FacArticleP2.ActiveRow.Cells["FAP_Affiche"].Value = 1;
                }

                if (string.IsNullOrEmpty(ssFAP_FacArticleP2.ActiveRow.Cells["CodeImmeuble"].Text))
                {
                    ssFAP_FacArticleP2.ActiveRow.Cells["CodeImmeuble"].Value = txtCodeImmeuble.Text;
                }

                if (string.IsNullOrEmpty(ssFAP_FacArticleP2.ActiveRow.Cells["COP_NoAuto"].Text) & !string.IsNullOrEmpty(txtCOP_NoAuto.Text))
                {
                    ssFAP_FacArticleP2.ActiveRow.Cells["Cop_NoAuto"].Value = txtCOP_NoAuto.Text;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "ssFAP_FacArticleP2_BeforeExitEditMode");
            }

        }
        private void fc_FAP_FacArticleP2(int lEQM_NoAuto)
        {
            int i = 0;
            General.sSQL = "SELECT FAP_CodeArticle, FAP_Designation1, FAP_NoAuto," + " EQM_NoAuto, CodeImmeuble, FAP_NoLIgne, FAP_Affiche,COP_NoAuto"
                + " From FAP_FacArticleP2" + " WHERE     (EQM_NoAuto =" + General.nz(lEQM_NoAuto, 0) + ")" + " order by FAP_NoLIgne";

            ssFAP_FacArticleP2.Visible = false;
            lFAP_Article = 0;
            ssFAP_FacArticleP2.Enabled = true;
            var tmpAdo = new ModAdo();
            rsFAP_FacArticleP2 = tmpAdo.fc_OpenRecordSet(General.sSQL);

            if (rsFAP_FacArticleP2.Rows.Count > 0)
            {
                // rsFAP_FacArticleP2.MoveFirst();
                i = 0;
                foreach (DataRow rsFAP_FacArticleP2Row in rsFAP_FacArticleP2.Rows)
                {
                    i = i + 1;
                    rsFAP_FacArticleP2Row["FAP_Noligne"] = i;
                    tmpAdo.Update();
                    // rsFAP_FacArticleP2.MoveNext();
                }
                lFAP_Article = rsFAP_FacArticleP2.Rows.Count;
            }

            ssFAP_FacArticleP2.DataSource = rsFAP_FacArticleP2;
            ssFAP_FacArticleP2.UpdateData();
            // ssFAP_FacArticleP2.Visible = True
            //fc_FGrille( true);existe dans vb6 masi son code est en commentaire
            // Timer2.Enabled = True
        }

        private void ssFAP_FacArticleP2_Leave(object sender, EventArgs e)
        {
            ssPEI_PeriodeGammeImm.UpdateData();
            ssFAP_FacArticleP2.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridCumul_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            int i = 0;

            var _with27 = ssGridCumul;
            //=== colonne total.
            if (e.Row.Cells["matricule"].Text.ToString().ToUpper() == cTotal.ToString().ToUpper())
            {
                for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                {
                    e.Row.Cells[i].Appearance.BackColor = Color.FromArgb(255, 128, 128);
                }
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridRecap_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            int i = 0;
            double dbTotTemp = 0;
            var _with28 = ssGridRecap;


            if (e.Row.Cells["PEI_IntAutre"].Value == DBNull.Value || e.Row.Cells["PEI_IntAutre"].Value.ToString() == "0")
            {
                e.Row.Cells["PEI_IntAutre"].Value = false;
            }
            if (e.Row.Cells["GAI_Compteur"].Value == DBNull.Value || e.Row.Cells["GAI_Compteur"].Value.ToString() == "0")
            {
                e.Row.Cells["GAI_Compteur"].Value = false;
            }
            if (e.Row.Cells["GAI_CompteurObli"].Value == DBNull.Value || e.Row.Cells["GAI_CompteurObli"].Value.ToString() == "0")
            {
                e.Row.Cells["GAI_CompteurObli"].Value = false;
            }

            if (General.nz(e.Row.Cells["PEI_IntAutre"].Text, "0").ToString() == "0")
            {
                e.Row.Cells["PEI_Intervenant"].Value = modP2.FC_intervenantImm(txtCodeImmeuble.Text);
            }

            //=== compteur.
            //If nz(.Columns("CompteurObli").Text, "0") = "1" Or nz(.Columns("EQU_P1Compteur").Text, "0") = "1" Then
            //    For i = 0 To .Columns.Count - 1
            //        .Columns(i).CellStyleSet cStyleCompteurObl, .Row
            //    Next i
            //End If

            //=== compteur.
            if (General.nz(e.Row.Cells["GAI_Compteur"].Text, "0").ToString() == "1"
                || General.nz(e.Row.Cells["GAI_CompteurObli"].Text, "0").ToString() == "1")
            {
                for (i = 0; i <= e.Row.Cells.Count - 1; i++)
                {
                    e.Row.Cells[i].Appearance.BackColor = Color.YellowGreen;
                }
            }
            dbTotTemp = fc_CalculDuree(Convert.ToDouble(General.nz(e.Row.Cells["PEI_Duree"].Text, 0)), Convert.ToDouble(General.nz(e.Row.Cells["PEI_Visite"].Text, 0)));
            e.Row.Cells["total"].Value = dbTotTemp;
            //ssGridRecap.UpdateData();
            e.Row.Update();
        }
        private void ssPEI_PeriodeGammeImm_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            //int i = 0;
            //System.DateTime sDeb = default(System.DateTime);
            //System.DateTime sFin = default(System.DateTime);
            //System.DateTime dtTemp = default(System.DateTime);
            //double dVisite = 0;
            //string sInterv = null;
            //try
            //{
            //    //=== passage obligatoire.
            //    if (ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_PassageObli"].Column.Index == ssPEI_PeriodeGammeImm.ActiveCell.Column.Index)
            //    {
            //        if (ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_PassageObli"].Text == "-1")
            //        {
            //            ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_PassageObli"].Value = 1;
            //        }
            //    }
            //    //controle certains elements dans la grille.
            //    fc_CtrlssPEI_PeriodeGammeImm();//tested

            //    //-- si la columns PEI_IntAutre <> 1 alors on récupére l'intervenant par defaut
            //    var _with31 = ssPEI_PeriodeGammeImm;
            //    if (string.IsNullOrEmpty(_with31.ActiveRow.Cells["COP_NoAuto"].Text))//tested
            //    {

            //        _with31.ActiveRow.Cells["COP_NoAuto"].Value = Convert.ToInt32(General.nz(txtCOP_NoAuto.Text, 0).ToString());

            //    }

            //    //=== recherche l'intervenant par defaut.
            //    if (string.IsNullOrEmpty(_with31.ActiveRow.Cells["PEI_intAutre"].Text) || _with31.ActiveRow.Cells["PEI_intAutre"].Text == "0")
            //    {
            //        if (string.IsNullOrEmpty(_with31.ActiveRow.Cells["PEI_Intervenant"].Text) ||
            //            _with31.ActiveCell.Column.Index == _with31.ActiveRow.Cells["PEI_intAutre"].Column.Index)
            //        {
            //            //=== modif du 30 08 2010 ajout de la catégorie de l'intervenant.
            //            //sSQL = "SELECT  " _
            //            //& " IntervenantImm_INM.intervenant_INM " _
            //            //& " FROM IntervenantImm_INM " _
            //            //& " WHERE Codeimmeuble_IMM='" & gFr_DoublerQuote(txtCodeimmeuble) & "'" _
            //            //& " AND CATI_CODE ='" & cIntervEntretien & "'" _
            //            //& " order by Nordre_INM"	General.sSQL = "SELECT     CodeDepanneur" + " From Immeuble " + " WHERE CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble) + "'";

            //            //.Columns("PEI_Intervenant").value = fc_ADOlibelle(sSQL)
            //            var tmpAdo = new ModAdo();
            //            _with31.ActiveRow.Cells["PEI_Intervenant"].Value = tmpAdo.fc_ADOlibelle(General.sSQL).ToString();
            //        }

            //    }

            //    //=== compteur obligatoire.
            //    if (_with31.ActiveRow.Cells["PEI_CompteurObli"].Column.Index == _with31.ActiveCell.Column.Index)
            //    {
            //        if (_with31.ActiveRow.Cells["PEI_CompteurObli"].Text == "-1")
            //        {
            //            _with31.ActiveRow.Cells["PEI_CompteurObli"].Value = 1;
            //        }

            //    }

            //    if (string.IsNullOrEmpty(_with31.ActiveRow.Cells["EQM_NoAuto"].Text))//tested
            //    {
            //        _with31.ActiveRow.Cells["EQM_NoAuto"].Value = Convert.ToInt32(txtEQM_NoAuto.Text);
            //    }

            //    if (string.IsNullOrEmpty(_with31.ActiveRow.Cells["CodeImmeuble"].Text))//tested
            //    {
            //        _with31.ActiveRow.Cells["CodeImmeuble"].Value = txtCodeImmeuble.Text;
            //    }

            //    //    If .Columns("EQM_NoAuto").Text = "" Then
            //    //        .Columns("EQM_NoAuto").value = ssGammePlanning.Columns("EQM_NoAuto").value
            //    //    End If


            //    if (_with31.ActiveCell.Column.Index == _with31.ActiveRow.Cells["PEI_APlanifier"].Column.Index)
            //    {
            //        if (_with31.ActiveRow.Cells["PEI_APlanifier"].Text == "-1")
            //        {
            //            _with31.ActiveRow.Cells["PEI_APlanifier"].Value = 1;
            //        }
            //    }


            //    if (_with31.ActiveCell.Column.Index == _with31.ActiveRow.Cells["PEI_ForceJournee"].Column.Index)
            //    {
            //        if (_with31.ActiveRow.Cells["PEI_ForceJournee"].Text == "-1")
            //        {
            //            _with31.ActiveRow.Cells["PEI_ForceJournee"].Value = 1;
            //        }
            //    }

            //    if (_with31.ActiveCell.Column.Index == _with31.ActiveRow.Cells["PEI_AvisPassage"].Column.Index)
            //    {
            //        if (_with31.ActiveRow.Cells["PEI_AvisPassage"].Text == "-1")
            //        {
            //            _with31.ActiveRow.Cells["PEI_AvisPassage"].Value = 1;
            //        }
            //        else
            //        {
            //            _with31.ActiveRow.Cells["PEI_AvisNbJour"].Value = 0;
            //        }
            //    }

            //    //==== valeur par défaut des paramétres GMAO.
            //    //=== samedi.
            //    if (string.IsNullOrEmpty(_with31.ActiveRow.Cells["PEI_Samedi"].Text))
            //    {
            //        _with31.ActiveRow.Cells["PEI_Samedi"].Value = modP2.tGmao.lSamedi;
            //    }
            //    if (_with31.ActiveRow.Cells["PEI_Samedi"].Text == "-1")
            //    {
            //        _with31.ActiveRow.Cells["PEI_Samedi"].Value = 1;
            //    }

            //    //=== Dimanche.
            //    if (string.IsNullOrEmpty(_with31.ActiveRow.Cells["PEI_Dimanche"].Text))
            //    {
            //        _with31.ActiveRow.Cells["PEI_Dimanche"].Value = modP2.tGmao.lDimanche;
            //    }
            //    if (_with31.ActiveRow.Cells["PEI_Dimanche"].Text == "-1")
            //    {
            //        _with31.ActiveRow.Cells["PEI_Dimanche"].Value = 1;
            //    }

            //    //=== pentecote.
            //    if (string.IsNullOrEmpty(_with31.ActiveRow.Cells["PEI_Pentecote"].Text))
            //    {
            //        _with31.ActiveRow.Cells["PEI_Pentecote"].Value = modP2.tGmao.lPentecote;
            //    }
            //    if (_with31.ActiveRow.Cells["PEI_Pentecote"].Text == "-1")
            //    {
            //        _with31.ActiveRow.Cells["PEI_Pentecote"].Value = 1;
            //    }

            //    //=== jours fériés.
            //    if (string.IsNullOrEmpty(_with31.ActiveRow.Cells["PEI_Jourferiee"].Text))
            //    {
            //        _with31.ActiveRow.Cells["PEI_Jourferiee"].Value = modP2.tGmao.lJoursFeriees;
            //    }
            //    if (_with31.ActiveRow.Cells["PEI_Jourferiee"].Text == "-1")
            //    {
            //        _with31.ActiveRow.Cells["PEI_Jourferiee"].Value = 1;
            //    }

            //    if (_with31.ActiveCell.Column.Index == _with31.ActiveRow.Cells["PEI_APlanifier"].Column.Index)
            //    {
            //        if (_with31.ActiveRow.Cells["PEI_APlanifier"].Text == "-1")
            //        {
            //            _with31.ActiveRow.Cells["PEI_APlanifier"].Value = 1;
            //        }
            //    }


            //    /*  if (_with31.ActiveCell.Column.Index == ssPEI_PeriodeGammeImm.ActiveRow.Cells["TPP_CODE"].Column.Index)
            //      {
            //          var _with32 = ssDropTPP_TypePeriodeP2;
            //          ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_JD"].Value = DBNull.Value;
            //          ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_JF"].Value = DBNull.Value;
            //          ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_MD"].Value = DBNull.Value;
            //          ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_MF"].Value = DBNull.Value;
            //          ssPEI_PeriodeGammeImm.ActiveRow.Cells["SEM_Code"].Value = DBNull.Value;

            //          for (i = 0; i <= _with32.Rows.Count - 1; i++)
            //          {
            //              if (ssPEI_PeriodeGammeImm.ActiveRow.Cells["TPP_CODE"].Value == _with32.Rows[i].Cells["TPP_CODE"].Value)
            //              {
            //                  if (!string.IsNullOrEmpty(_with32.Rows[i].Cells["TPP_JD"].Text))
            //                  {
            //                      ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_JD"].Value = Convert.ToInt32(_with32.Rows[i].Cells["TPP_JD"].Value);
            //                  }
            //                  if (!string.IsNullOrEmpty(_with32.Rows[i].Cells["TPP_JF"].Text))
            //                  {
            //                      ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_JF"].Value = Convert.ToInt32(_with32.Rows[i].Cells["TPP_JF"].Value);
            //                  }
            //                  if (!string.IsNullOrEmpty(_with32.Rows[i].Cells["TPP_MD"].Text))
            //                  {
            //                      ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_MD"].Value = Convert.ToInt32(_with32.Rows[i].Cells["TPP_MD"].Value);
            //                  }
            //                  if (!string.IsNullOrEmpty(_with32.Rows[i].Cells["TPP_Mf"].Text))
            //                  {
            //                      ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_MF"].Value = Convert.ToInt32(_with32.Rows[i].Cells["TPP_Mf"].Value);
            //                  }

            //                  //ssPEI_PeriodeGammeImm.Columns("SEM_Code").value = .Columns("SEM_Code").value

            //                  break; // TODO: might not be correct. Was : Exit For
            //              }
            //              // _with32.MoveNext();
            //          }

            //      }*/


            //    var _with33 = ssPEI_PeriodeGammeImm;

            //    if ((_with31.ActiveCell.Column.Index == _with33.ActiveRow.Cells["PEI_JD"].Column.Index
            //        && _with33.ActiveRow.Cells["TPP_Code"].Text.ToString().ToUpper() == "Annuel".ToString().ToUpper())
            //        || (_with33.ActiveCell.Column.Index == _with33.ActiveRow.Cells["PEI_MD"].Column.Index
            //        && _with33.ActiveRow.Cells["TPP_Code"].Text.ToString().ToUpper() == "Annuel".ToString().ToUpper()))
            //    {

            //        _with33.ActiveRow.Cells["PEI_JF"].Value = fDate.fc_FinDeMois(_with33.ActiveRow.Cells["PEI_MD"].Text, Convert.ToString(DateTime.Now.Month));
            //        _with33.ActiveRow.Cells["PEI_MF"].Value = _with33.ActiveRow.Cells["PEI_MD"].Value;

            //    }

            //    //controle si la date du jour et du mois sont cohérent pour le jour debut et le mois debut.
            //    if (!string.IsNullOrEmpty(_with33.ActiveRow.Cells["PEI_JD"].Text) && !string.IsNullOrEmpty(_with33.ActiveRow.Cells["PEI_MD"].Text))//tested
            //    {
            //        do
            //        {
            //            if (!General.IsDate(_with33.ActiveRow.Cells["PEI_JD"].Text + "/" + _with33.ActiveRow.Cells["PEI_MD"].Text + "/" + DateTime.Now.Year))
            //            {
            //                _with33.ActiveRow.Cells["PEI_JD"].Value = Convert.ToInt32(_with33.ActiveRow.Cells["PEI_JD"].Value.ToString()) - 1;
            //            }
            //            else
            //            {
            //                break;
            //            }
            //        } while (true);
            //    }

            //    //controle si la date du jour et du mois sont cohérent pour le jour debut et le mois fin.
            //    if (!string.IsNullOrEmpty(_with33.ActiveRow.Cells["PEI_JF"].Text) && !string.IsNullOrEmpty(_with33.ActiveRow.Cells["PEI_MF"].Text))//tested
            //    {
            //        do
            //        {
            //            if (!General.IsDate(_with33.ActiveRow.Cells["PEI_JF"].Text + "/" + _with33.ActiveRow.Cells["PEI_MF"].Text + "/" + DateTime.Now.Year))
            //            {
            //                _with33.ActiveRow.Cells["PEI_JF"].Value = Convert.ToInt32(_with33.ActiveRow.Cells["PEI_JF"].Value) - 1;
            //            }
            //            else
            //            {
            //                break;
            //            }
            //        } while (true);
            //    }

            //    // met la valeur A (automatique) si le champs PEI_ForceVisite est à vide
            //    if (string.IsNullOrEmpty(_with33.ActiveRow.Cells["PEI_ForceVisite"].Text))//tseed
            //    {
            //        _with33.ActiveRow.Cells["PEI_ForceVisite"].Value = "A";
            //    }


            //    ///    '==== modif du 13 07 2013, les dates sont saisies en jour et mois, on ne saisit plus la date compléte avec l'année.
            //    ///    '==== contrôle que la date de début et la date de fin ne dépasse pas une année et contrôle que la
            //    ///    '==== date de début est plus petite que la date de fin.
            //    ///     If ColIndex = .Columns("PEI_DateDebut").Position Or ColIndex = .Columns("PEI_dateFin").Position Then
            //    ///
            //    ///            .Columns(ColIndex).Text = fc_ValideDate(.Columns(ColIndex).Text)
            //    ///
            //    ///            If IsDate(.Columns("PEI_DateDebut").Text) And IsDate(.Columns("PEI_DateFin").Text) Then
            //    ///
            //    ///                    sDeb = .Columns("PEI_DateDebut").Text
            //    ///                    sFin = .Columns("PEI_DateFin").Text
            //    ///                    dtTemp = DateAdd("yyyy", 1, sDeb) - 1
            //    ///
            //    ///                    If sFin > dtTemp Then
            //    ///                        MsgBox "Erreur de saisie" & vbCrLf & "La différence de jours entre la date de début" _
            //    ///'                                & " et la date de fin ne doit pas excéder une année.", vbExclamation, "Erreur de saisie"
            //    ///                                Cancel = True
            //    ///                                Exit Sub
            //    ///                    ElseIf sFin < sDeb Then
            //    ///                        MsgBox "Erreur de saisie" & vbCrLf & "La date de début doit être antérieur à la date de fin." _
            //    ///'                                & "", vbExclamation, "Erreur de saisie"
            //    ///                                Cancel = True
            //    ///                                Exit Sub
            //    ///                    End If
            //    ///
            //    ///            End If
            //    ///
            //    ///            If IsDate(.Columns("PEI_DateDebut").value) Then
            //    ///                      sDeb = .Columns("PEI_DateDebut").Text
            //    ///                     If sDeb < txtCOP_DateSignature Then
            //    ///                           MsgBox "Erreur de saisie" & vbCrLf & "La date de début doit être postérieur ou égale à la date de début de planification." _
            //    ///'                                & "", vbExclamation, "Erreur de saisie"
            //    ///                                Cancel = True
            //    ///                                Exit Sub
            //    ///                    End If
            //    ///            End If
            //    ///
            //    ///     End If

            //    if (_with33.ActiveRow.Cells["PEI_ForceVisite"].Text.ToString().ToUpper() == "A".ToString().ToUpper())
            //    {
            //        if (!string.IsNullOrEmpty(_with33.ActiveRow.Cells["PEI_JD"].Text) && !string.IsNullOrEmpty(_with33.ActiveRow.Cells["PEI_JF"].Text)
            //                && !string.IsNullOrEmpty(_with33.ActiveRow.Cells["PEI_MD"].Text) && !string.IsNullOrEmpty(_with33.ActiveRow.Cells["PEI_MF"].Text)
            //                && !string.IsNullOrEmpty(_with33.ActiveRow.Cells["TPP_Code"].Text))
            //        {
            //            sDeb = Convert.ToDateTime(_with33.ActiveRow.Cells["PEI_JD"].Text + "/" + _with33.ActiveRow.Cells["PEI_MD"].Text + "/" + DateTime.Now.Year);
            //            if (Convert.ToDouble(_with33.ActiveRow.Cells["PEI_MF"].Text) < Convert.ToDouble(_with33.ActiveRow.Cells["PEI_MD"].Text))
            //            {
            //                sFin = Convert.ToDateTime(_with33.ActiveRow.Cells["PEI_JF"].Text + "/" + _with33.ActiveRow.Cells["PEI_MF"].Text + "/" + DateTime.Now.Year + 1);
            //            }
            //            else//tested
            //            {
            //                sFin = Convert.ToDateTime(_with33.ActiveRow.Cells["PEI_JF"].Text + "/" + _with33.ActiveRow.Cells["PEI_MF"].Text + "/" + DateTime.Now.Year);
            //            }

            //            if (_with33.ActiveRow.Cells["TPP_Code"].Text.ToString().ToUpper() == "Hebdomadaire".ToString().ToUpper())//tested
            //            {
            //                //.Columns("peg_Visite").value = Fix(DateDiff("d", sDeb, sFin, , vbFirstJan1) / 7)                      
            //                _with33.ActiveRow.Cells["peI_Visite"].Value = Math.Floor((sFin - sDeb).TotalDays / 7);

            //            }
            //            else if (_with33.ActiveRow.Cells["TPP_Code"].Text.ToString() == "Mensuel".ToString().ToUpper())
            //            {
            //                dVisite = Math.Floor((sFin - sDeb).TotalDays / 30) + 1;//verifier
            //                _with33.ActiveRow.Cells["peI_Visite"].Value = dVisite;

            //            }
            //            else if (_with33.ActiveRow.Cells["TPP_Code"].Text.ToString().ToUpper() == modP2.cBiMestriel.ToString().ToUpper())
            //            {
            //                dVisite = Math.Floor((sFin - sDeb).TotalDays) + 1;
            //                if (Math.Floor(dVisite / 2) == (dVisite / 2))
            //                {
            //                    _with33.ActiveRow.Cells["peI_Visite"].Value = Math.Floor(dVisite / 2);
            //                }
            //                else
            //                {
            //                    _with33.ActiveRow.Cells["peI_Visite"].Value = Math.Floor(dVisite / 2) + 1;
            //                }
            //            }
            //            else if (_with33.ActiveRow.Cells["TPP_Code"].Text.ToString().ToUpper() == "BiMensuel".ToString().ToUpper())
            //            {
            //                _with33.ActiveRow.Cells["peI_Visite"].Value = Math.Floor((sFin - sDeb).TotalDays / 14);
            //            }
            //            else if (_with33.ActiveRow.Cells["TPP_Code"].Text.ToString().ToUpper() == "Trimestriel".ToString().ToUpper())
            //            {
            //                dVisite = Math.Floor((sFin - sDeb).TotalDays / 30) + 1;//verifier
            //                if (Math.Floor(dVisite / 3) == (dVisite / 3))
            //                {
            //                    _with33.ActiveRow.Cells["peI_Visite"].Value = Math.Floor(dVisite / 3);
            //                }
            //                else
            //                {
            //                    _with33.ActiveRow.Cells["peI_Visite"].Value = Math.Floor(dVisite / 3) + 1;
            //                }

            //            }
            //            else if (_with33.ActiveRow.Cells["TPP_Code"].Text.ToString().ToUpper() == "Semestriel".ToString().ToUpper())
            //            {
            //                dVisite = Math.Floor((sFin - sDeb).TotalDays / 30) + 1;//verifier
            //                if (Math.Floor(dVisite / 6) == (dVisite / 6))
            //                {
            //                    _with33.ActiveRow.Cells["peI_Visite"].Value = Math.Floor(dVisite / 6);
            //                }
            //                else
            //                {
            //                    _with33.ActiveRow.Cells["peI_Visite"].Value = Math.Floor(dVisite / 6) + 1;
            //                }

            //            }
            //            else if (_with33.ActiveRow.Cells["TPP_Code"].Text.ToString().ToUpper() == "Annuel".ToString().ToUpper())
            //            {

            //                dVisite = 1;
            //                _with33.ActiveRow.Cells["peI_Visite"].Value = dVisite;

            //            }
            //        }
            //    }


            //    ///    If UCase(.Columns("PEI_ForceVisite").Text) = UCase("A") Then
            //    ///
            //    ///        If .Columns("TPP_code").Text <> "" And IsDate(.Columns("PEI_DateDebut").Text) And _
            //    ///'                 IsDate(.Columns("PEI_DateFin").Text) Then
            //    ///
            //    ///
            //    ///                 sDeb = .Columns("PEI_DateDebut").Text
            //    ///                 sFin = .Columns("PEI_DateFin").Text
            //    ///
            //    ///
            //    ///                 If UCase(.Columns("TPP_Code").Text) = UCase("Hebdomadaire") Then
            //    ///                        '.Columns("peg_Visite").value = Fix(DateDiff("d", sDeb, sFin, , vbFirstJan1) / 7)
            //    ///                        .Columns("peI_Visite").value = Fix(DateDiff("d", sDeb, sFin) / 7)
            //    ///
            //    ///
            //    ///                 ElseIf UCase(.Columns("TPP_Code").Text) = UCase("Mensuel") Then
            //    ///                         dVisite = DateDiff("m", sDeb, sFin) + 1
            //    ///                        .Columns("peI_Visite").value = dVisite
            //    ///
            //    ///
            //    ///                 ElseIf UCase(.Columns("TPP_Code").Text) = UCase("BiMensuel") Then
            //    ///
            //    ///                        .Columns("peI_Visite").value = Fix(DateDiff("d", sDeb, sFin) / 14)
            //    ///
            //    ///                 ElseIf UCase(.Columns("TPP_Code").Text) = UCase("Trimestriel") Then
            //    ///                        dVisite = DateDiff("m", sDeb, sFin) + 1
            //    ///                        If Fix(dVisite / 3) = (dVisite / 3) Then
            //    ///                           .Columns("peI_Visite").value = Fix(dVisite / 3)
            //    ///                        Else
            //    ///                            .Columns("peI_Visite").value = Fix(dVisite / 3) + 1
            //    ///                        End If
            //    ///
            //    ///                 ElseIf UCase(.Columns("TPP_Code").Text) = UCase("Semestriel") Then
            //    ///                        dVisite = DateDiff("m", sDeb, sFin) + 1
            //    ///                        If Fix(dVisite / 6) = (dVisite / 6) Then
            //    ///                            .Columns("peI_Visite").value = Fix(dVisite / 6)
            //    ///                        Else
            //    ///                            .Columns("peI_Visite").value = Fix(dVisite / 6) + 1
            //    ///                        End If
            //    ///
            //    ///                ElseIf UCase(.Columns("TPP_Code").Text) = UCase("Annuel") Then
            //    ///                        dVisite = 1
            //    ///                        .Columns("peI_Visite").value = dVisite
            //    ///
            //    ///                 End If
            //    ///        End If
            //    ///    End If

            //    //=== si la case du champ PEI_Calcul est coché lui attribué la valeur 1 au lieu de
            //    //=== -1 (valeur de la case à cocher de la grille)
            //    if (_with33.ActiveRow.Cells["PEI_Calcul"].Text == "-1")
            //    {
            //        _with33.ActiveRow.Cells["PEI_Calcul"].Value = 1;
            //    }

            //    //
            //    if (_with33.ActiveRow.Cells["PEI_IntAutre"].Text == "-1")
            //    {
            //        _with33.ActiveRow.Cells["PEI_IntAutre"].Value = 1;
            //    }

            //    //=== modif du 14 07 2013, on ne saisit plus une date mais un moi et une journée.
            //    //=== date de début obligatoire.
            //    ///    If .Columns("PEI_DateDebut").Text = "" Then
            //    ///        MsgBox "La date de début est obligatoire.", vbInformation, "Données manquantes"
            //    ///        Cancel = True
            //    ///        Exit Sub
            //    ///    End If

            //    //=== si la frequence sélectionné est le cycle, une valeur dans la colonne cycle
            //    //=== est obligatoire.
            //    if (General.Left(_with33.ActiveRow.Cells["TPP_Code"].Text.ToString().ToUpper(), 5) == "Cycle".ToString().ToUpper())
            //    {
            //        if (string.IsNullOrEmpty(_with33.ActiveRow.Cells["PEI_Cycle"].Text))
            //        {
            //            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur dans la colonne cycle..", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //            e.Cancel = true;
            //            return;
            //        }
            //        else if (_with33.ActiveRow.Cells["PEI_Cycle"].Text == "0")
            //        {
            //            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas saisir .", "Données obligatoires", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //            e.Cancel = true;
            //            return;
            //        }
            //    }
            //    else
            //    {
            //        _with33.ActiveRow.Cells["PEI_Cycle"].Value = DBNull.Value;
            //    }

            //    ////=== controle la durée.
            //    //if (string.IsNullOrEmpty(_with33.ActiveRow.Cells["PEI_DureeReel"].Text))
            //    //{
            //    //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une durée.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Information);
            //    //    e.Cancel = true;
            //    //    return;
            //    //}

            //    //calcul du déboursé si PEICalcul est a 1 le Total HT n'est pas calculé.
            //    ///    If .Columns("PEI_Calcul").Text <> "1" Then
            //    ///        .Columns("PEI_TotalHT").value = 0
            //    ///        If UCase(.Columns("PEI_Duree").Text) <> "" And .Columns("PEI_Tech").Text <> "" Then
            //    ///           Dim dCout As Double
            //    ///            sSQL = "SELECT PER_Charge From Personnel WHERE     (Matricule = '" & UCase(.Columns("PEI_Tech").Text) & "')"
            //    ///            dCout = nz(fc_ADOlibelle(sSQL), 0)
            //    ///            .Columns("PEI_Cout").value = dCout
            //    ///            If .Columns("PEI_Visite").Text <> "" Then
            //    ///                .Columns("PEI_TotalHT").value = (.Columns("PEI_Duree").Text * dCout) * .Columns("PEI_Visite").Text
            //    ///            End If
            //    ///        End If
            //    ///    End If

            //    return;
            //}
            //catch (Exception ex)
            //{
            //    Erreurs.gFr_debug(ex, this.Name + "");
            //}*/


        }
        private void ssPEI_PeriodeGammeImm_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            var _with34 = ssPEI_PeriodeGammeImm;

            try
            {
                if (e.Row == null) return;
                if (string.IsNullOrEmpty(e.Row.Cells["PEI_DureeReel"].Text) && !string.IsNullOrEmpty(e.Row.Cells["PEI_Duree"].Text))
                {
                    e.Row.Cells["PEI_DureeReel"].Value = General.nz(e.Row.Cells["PEI_Duree"].Text, 0);
                }
                //=== modif du 14 07 2013, on ne saisit plus une date mais un moi et une journée.
                //=== date de début obligatoire.
                ///    If .Columns("PEI_DateDebut").Text = "" Then
                ///        MsgBox "La date de début est obligatoire.", vbInformation, "Données manquantes"
                ///        Cancel = True
                ///        Exit Sub
                ///    End If


                //=== si la frequence sélectionné est le cycle, une valeur dans la colonne cycle
                //=== est obligatoire.
                if (General.Left(e.Row.Cells["TPP_Code"].Text.ToString().ToUpper(), 5) == "Cycle".ToString().ToUpper())
                {
                    if (string.IsNullOrEmpty(e.Row.Cells["PEI_Cycle"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur dans la colonne cycle..", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                    else if (e.Row.Cells["PEI_Cycle"].Text == "0")
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas saisir .", "Données obligatoires", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        return;
                    }
                }
                else
                {
                    e.Row.Cells["PEI_Cycle"].Value = DBNull.Value;
                }


                //=== controle la durée.
                if (string.IsNullOrEmpty(e.Row.Cells["PEI_DureeReel"].Value.ToString()))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une durée.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Row.Cells["PEI_DureeReel"].Activated = true;
                    e.Cancel = true;
                    return;
                }


                return;

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";ssPEI_PeriodeGammeImm_BeforeUpdate");
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPEI_PeriodeGammeImm_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.ReInitialize)
                return;

            //===> Mondir le 18.03.2021, https://groupe-dt.mantishub.io/view.php?id=2361
            if (e.Row.IsAddRow)
            {
                e.Row.Cells["PEI_Samedi"].Value = true;
                e.Row.Cells["PEI_Dimanche"].Value = true;
                e.Row.Cells["PEI_Pentecote"].Value = true;
                e.Row.Cells["PEI_Jourferiee"].Value = true;
            }
            //===> Fin Modif Mondir

            fc_CtrlssPEI_PeriodeGammeImm(e.Row);//tested
            if (General.nz(e.Row.Cells["PEI_IntAutre"].Text, "0").ToString() == "0")
            {
                e.Row.Cells["PEI_Intervenant"].Value = modP2.FC_intervenantImm(txtCodeImmeuble.Text);
            }

            if (General.nz(e.Row.Cells["PEI_IntAutre"].Text, "0").ToString() == "0")
            {
                e.Row.Cells["PEI_Intervenant"].Value = modP2.FC_intervenantImm(txtCodeImmeuble.Text);
            }
            if (e.Row.Cells["PEI_PassageObli"].Value == DBNull.Value || e.Row.Cells["PEI_PassageObli"].Value.ToString() == "0")
            {
                e.Row.Cells["PEI_PassageObli"].Value = false;
            }
            if (e.Row.Cells["PEI_ForceJournee"].Value == DBNull.Value || e.Row.Cells["PEI_ForceJournee"].Value.ToString() == "0")
            {
                e.Row.Cells["PEI_ForceJournee"].Value = false;
            }


            if (e.Row.Cells["PEI_IntAutre"].Value == DBNull.Value || e.Row.Cells["PEI_IntAutre"].Value.ToString() == "0")
            {
                e.Row.Cells["PEI_IntAutre"].Value = false;
            }
            if (e.Row.Cells["PEI_APlanifier"].Value == DBNull.Value || e.Row.Cells["PEI_APlanifier"].Value.ToString() == "0")
            {
                e.Row.Cells["PEI_APlanifier"].Value = false;
            }
            if (e.Row.Cells["PEI_AvisPassage"].Value == DBNull.Value || e.Row.Cells["PEI_AvisPassage"].Value.ToString() == "0")
            {
                e.Row.Cells["PEI_AvisPassage"].Value = false;
            }
            //ssPEI_PeriodeGammeImm.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_CtrlssPEI_PeriodeGammeImm(UltraGridRow row)
        {

            //--controle certains elements dans la grille.
            // var _with36 = ssPEI_PeriodeGammeImm;


            //-- si la colonne PEI_IntAutre est à 1(signifiant que ce n'est pas l'intervenant
            //-- par defaut de l'immeuble)  alors on débloque PEI_Intervenant colonne sinon
            //-- on la bloque
            if (string.IsNullOrEmpty(row.Cells["PEI_IntAutre"].Text) || row.Cells["PEI_IntAutre"].Text == "0")
            {
                row.Cells["PEI_Intervenant"].Activation = Activation.Disabled;
                row.Cells["PEI_Intervenant"].Appearance.BackColor = Color.Gray;
                ssDropIntervenant.Enabled = false;
                row.Cells["PEI_Intervenant"].DroppedDown = false;
            }
            else if (row.IsAddRow)
            {
                row.Cells["PEI_Intervenant"].Activation = Activation.AllowEdit;
                row.Cells["PEI_Intervenant"].Appearance.BackColor = Color.Gray;
                ssDropIntervenant.Enabled = false;
                row.Cells["PEI_Intervenant"].DroppedDown = false;
            }
            else
            {
                row.Cells["PEI_Intervenant"].Activation = Activation.AllowEdit;
                row.Cells["PEI_Intervenant"].Appearance.BackColor = Color.White;
                ssDropIntervenant.Enabled = true;
                row.Cells["PEI_Intervenant"].ValueList = ssDropIntervenant;

            }

            //--si la colonne PEI_Calcul est à 1(signifiant que le montant n'est pas calculé)
            //-- alors on  débloque PEI_TotalHT sinon on la bloque.
            if (row.Cells["PEI_Calcul"].Text == "1")
            {
                row.Cells["PEI_TotalHT"].Appearance.BackColor = Color.White;
                row.Cells["PEI_TotalHT"].Activation = Activation.NoEdit;
            }
            else if (row.IsAddRow)
            {
                row.Cells["PEI_TotalHT"].Appearance.BackColor = Color.Gray;
                row.Cells["PEI_TotalHT"].Activation = Activation.AllowEdit;
            }
            else
            {
                row.Cells["PEI_TotalHT"].Appearance.BackColor = Color.Gray;
                row.Cells["PEI_TotalHT"].Activation = Activation.AllowEdit;
            }

            //--si la colonne PEI_AvisPassage est à 1(signifiant qu'une demande d'avis de passage est demandé)
            //-- alors on  débloque PEI_AvisNbJour sinon on la bloque.
            if (row.Cells["PEI_AvisPassage"].Text == "1")
            {
                row.Cells["PEI_AvisNbJour"].Appearance.BackColor = Color.White;
                row.Cells["PEI_AvisNbJour"].Activation = Activation.NoEdit;
            }
            else if (row.IsAddRow)
            {
                row.Cells["PEI_AvisNbJour"].Appearance.BackColor = Color.Gray;
                row.Cells["PEI_AvisNbJour"].Activation = Activation.AllowEdit;
            }
            else
            {
                row.Cells["PEI_AvisNbJour"].Appearance.BackColor = Color.Gray;
                row.Cells["PEI_AvisNbJour"].Activation = Activation.AllowEdit;
            }

            //===> Mondir le 02.11.2020, https://groupe-dt.mantishub.io/view.php?id=2057
            if (row.Cells["PEI_PassageObli"].Text == "1")
            {
                row.Cells["PEI_HEURE"].Appearance.BackColor = Color.White;
                row.Cells["PEI_HEURE"].Activation = Activation.AllowEdit;
            }
            else if (row.IsAddRow)
            {
                row.Cells["PEI_HEURE"].Appearance.BackColor = Color.Gray;
                row.Cells["PEI_HEURE"].Activation = Activation.NoEdit;
            }
            else
            {
                row.Cells["PEI_HEURE"].Appearance.BackColor = Color.Gray;
                row.Cells["PEI_HEURE"].Activation = Activation.NoEdit;
            }



            //--si la colonne PEI_AvisPassage est à 1(signifiant qu'une demande d'avis de passage est demandé)
            //-- alors on  débloque PEI_AvisNbJour sinon on la bloque.
            if (row.Cells["PEI_ForceJournee"].Text == "1")
            {
                row.Cells["SEM_CODE"].Appearance.BackColor = Color.White;
                row.Cells["SEM_CODE"].Activation = Activation.AllowEdit;

                if (row.Cells["PEI_PassageObli"].Text != "1")
                {
                    row.Cells["PEI_HEURE"].Appearance.BackColor = Color.White;
                    row.Cells["PEI_HEURE"].Activation = Activation.AllowEdit;
                }
            }
            else if (row.IsAddRow)
            {
                row.Cells["SEM_CODE"].Appearance.BackColor = Color.Gray;
                row.Cells["SEM_CODE"].Activation = Activation.NoEdit;
                if (row.Cells["PEI_PassageObli"].Text != "1")
                {
                    row.Cells["PEI_HEURE"].Appearance.BackColor = Color.Gray;
                    row.Cells["PEI_HEURE"].Activation = Activation.NoEdit;
                }
            }
            else
            {
                row.Cells["SEM_CODE"].Appearance.BackColor = Color.Gray;
                row.Cells["SEM_CODE"].Activation = Activation.NoEdit;
                if (row.Cells["PEI_PassageObli"].Text != "1")
                {
                    row.Cells["PEI_HEURE"].Appearance.BackColor = Color.Gray;
                    row.Cells["PEI_HEURE"].Activation = Activation.NoEdit;
                }
            }



        }
        private void ssPEI_PeriodeGammeImm_Leave(object sender, EventArgs e)
        {
            ssPEI_PeriodeGammeImm.UpdateData();
            ssFAP_FacArticleP2.UpdateData();
        }
        private void ssPEI_PeriodeGammeImm_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            DialogResult dialogResult = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "Confirmation", MessageBoxButtons.YesNo);
            if (dialogResult == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
        }
        private void SSTab1_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {

            if (SSTab1.SelectedTab == SSTab1.Tabs[2])
            {
                SumGamme();
                fc_Calcul();

            }

            if (SSTab1.SelectedTab == SSTab1.Tabs[1])//tested
            {
                SumGamme();//tested
                           //fc_AfficheParametre();le code de cette fonction du version vb6 est commenté
            }

            // Fc_ValideGrille();le code de cette fonction du version vb6 est commenté

            txtDateSignatureSimul.Text = txtCOP_DateSignature.Text;

        }

        private void fc_ChargeOngle()
        {
            switch (SSTab1.SelectedTab.Index)
            {
                case cPlannification:
                    fc_CycleEncours();
                    break;
            }
        }
        private void fc_CycleEncours()
        {
            General.sSQL = "";

        }
        /// <summary>
        /// tested
        /// </summary>
        private void SumGamme()
        {
            // total gesten.
            var tmpAdo = new ModAdo();

            General.sSQL = "SELECT     SUM(PEI_PeriodeGammeImm.PEI_TotalHT) AS sum" + " FROM ICC_IntervCategorieContrat INNER JOIN"
                + " EQM_EquipementP2Imm ON ICC_IntervCategorieContrat.icc_Noauto" + " = EQM_EquipementP2Imm.ICC_Noauto INNER JOIN"
                + " PEI_PeriodeGammeImm ON EQM_EquipementP2Imm.EQM_NoAuto" + " = PEI_PeriodeGammeImm.EQM_NoAuto"
                + " WHERE     (ICC_IntervCategorieContrat.COP_NoAuto =" + General.nz(txtCOP_NoAuto.Text, 0) + ")"
                + " AND PEI_PeriodeGammeImm.PEI_Tech LIKE 'tp%'";
            //txtCOP_TotalHTgest.Text = General.nz(tmpAdo.fc_ADOlibelle(General.sSQL), 0).ToString();//tested
            //total soustraitant.            
            General.sSQL = "SELECT     SUM(PEI_PeriodeGammeImm.PEI_TotalHT) AS sum" + " FROM ICC_IntervCategorieContrat INNER JOIN"
                + " EQM_EquipementP2Imm ON ICC_IntervCategorieContrat.ICC_Noauto" + " = EQM_EquipementP2Imm.ICC_Noauto INNER JOIN"
                + " PEI_PeriodeGammeImm ON EQM_EquipementP2Imm.EQM_NoAuto" + " = PEI_PeriodeGammeImm.EQM_NoAuto"
                + " WHERE     (ICC_IntervCategorieContrat.COP_NoAuto =" + General.nz(txtCOP_NoAuto.Text, 0) + ")"
                + " AND PEI_PeriodeGammeImm.PEI_Tech LIKE 'st%'";
            //txtCOP_TotalHTST.Text = General.nz(tmpAdo.fc_ADOlibelle(General.sSQL), 0).ToString();//tested
            //total general
            General.sSQL = "SELECT     SUM(PEI_PeriodeGammeImm.PEI_TotalHT) AS sum" + " FROM ICC_IntervCategorieContrat INNER JOIN"
                + " EQM_EquipementP2Imm ON ICC_IntervCategorieContrat.ICC_Noauto" + " = EQM_EquipementP2Imm.ICC_Noauto INNER JOIN"
                + " PEI_PeriodeGammeImm ON EQM_EquipementP2Imm.EQM_NoAuto" + " = PEI_PeriodeGammeImm.EQM_NoAuto"
                + " WHERE     (ICC_IntervCategorieContrat.COP_NoAuto =" + General.nz(txtCOP_NoAuto.Text, 0) + ")";
            //txtCOP_TotalHT.Text = General.nz(tmpAdo.fc_ADOlibelle(General.sSQL), 0).ToString();
            // If IsNumeric(txtCOP_TotalHT) And IsNumeric(txtCOP_Coef) Then   


            //txtCOP_TotalApresCoef.Text = Convert.ToString(General.FncArrondir(Convert.ToDouble(General.nz(txtCOP_TotalHT.Text, 0)) * Convert.ToDouble(General.nz(txtCOP_Coef.Text, 0))));


            //End If

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSTab2_SelectedTabChanging(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangingEventArgs e)
        {
            try
            {
                //fc_AfficheParametre();le code de cette fonction du version vb6 est commenté

                if (SSTab1.SelectedTab == SSTab1.Tabs[1])//tested
                {
                    fc_RecapPrest();
                }

                txtDateSignatureSimul.Text = txtCOP_DateSignature.Text;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "SSTab2_click");
            }

        }


        /// <summary>
        /// tsted
        /// </summary>
        private void fc_RecapPrest()
        {

            //
            //With Adodc6
            //
            //    .ConnectionString = adocnn
            //
            //    .RecordSource = "SELECT   PDAB_BadgePDA.PDAB_Libelle,  ICC_IntervCategorieContrat.CAI_Libelle," _
            //'        & " ICC_IntervCategorieContrat.COP_Noauto, EQM_EquipementP2Imm.EQM_CODE, " _
            //'        & " PEI_PeriodeGammeImm.TPP_Code" _
            //'        & " , PEI_PeriodeGammeImm.PEI_DateDebut, PEI_PeriodeGammeImm.PEI_DateFin " _
            //'        & " , PEI_PeriodeGammeImm.PEI_Intervenant" _
            //'        & " , PEI_PeriodeGammeImm.PEI_Duree, PEI_PeriodeGammeImm.PEI_DureeReel " _
            //'        & " FROM         PEI_PeriodeGammeImm INNER JOIN" _
            //'        & " EQM_EquipementP2Imm ON PEI_PeriodeGammeImm.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto " _
            //'        & " INNER JOIN ICC_IntervCategorieContrat " _
            //'        & " ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto " _
            //'        & " INNER JOIN PDAB_BadgePDA " _
            //'        & " ON ICC_IntervCategorieContrat.PDAB_Noauto = PDAB_BadgePDA.PDAB_Noauto" _
            //'        & " WHERE ICC_IntervCategorieContrat.cop_NoAuto=" & nz(txtCOP_NoAuto, 0) _
            //'        & " ORDER BY PDAB_BadgePDA.PDAB_Libelle, ICC_IntervCategorieContrat.CAI_Libelle, EQM_EquipementP2Imm.EQM_CODE "
            //
            //    .Refresh
            //
            //End With
            string sAddItem = null;
            string sSQL = null;



            double dbTot = 0;
            double dbTotDuree = 0;
            double dbTotNbVisite = 0;
            double dbTotGeneral = 0;
            double dbTotTemp = 0;
            int i = 0;

            CumulHeure[] tpCumulHeure = null;


            //sSQL = "SELECT   PDAB_BadgePDA.PDAB_Libelle,  ICC_IntervCategorieContrat.CAI_Libelle," _
            //& " ICC_IntervCategorieContrat.COP_Noauto, EQM_EquipementP2Imm.EQM_CODE,EQM_EquipementP2Imm.CompteurObli, EQM_EquipementP2Imm.EQU_P1Compteur, " _
            //& " PEI_PeriodeGammeImm.TPP_Code" _
            //& " , PEI_PeriodeGammeImm.PEI_DateDebut, PEI_PeriodeGammeImm.PEI_DateFin " _
            //& " ,PEI_JD, PEI_MD, PEI_JF, PEI_MF " _
            //& " , PEI_PeriodeGammeImm.PEI_IntAutre , PEI_PeriodeGammeImm.PEI_Intervenant" _
            //& " , PEI_PeriodeGammeImm.PEI_Duree, PEI_PeriodeGammeImm.PEI_DureeReel, PEI_Visite,'' as Total " _
            //& " FROM         PEI_PeriodeGammeImm INNER JOIN" _
            //& " EQM_EquipementP2Imm ON PEI_PeriodeGammeImm.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto " _
            //& " INNER JOIN ICC_IntervCategorieContrat " _
            //& " ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto " _
            //& " INNER JOIN PDAB_BadgePDA " _
            //& " ON ICC_IntervCategorieContrat.PDAB_Noauto = PDAB_BadgePDA.PDAB_Noauto" _
            //& " WHERE ICC_IntervCategorieContrat.cop_NoAuto=" & nz(txtCOP_NoAuto, 0) _
            //& " ORDER BY PDAB_BadgePDA.PDAB_Libelle, ICC_IntervCategorieContrat.CAI_Libelle, EQM_EquipementP2Imm.EQM_CODE "

            try
            {
                sSQL = "SELECT   PDAB_BadgePDA.PDAB_Libelle,  GAI_GammeImm.GAI_LIbelle,GAI_Compteur,GAI_CompteurObli,  ICC_IntervCategorieContrat.CAI_Libelle,"
                        + " ICC_IntervCategorieContrat.COP_Noauto, " + " EQM_EquipementP2Imm.EQM_Code,";
                sSQL = sSQL + " CTR_Libelle, MOY_Libelle, ANO_Libelle , OPE_Libelle, ";
                sSQL = sSQL + " EQM_EquipementP2Imm.CompteurObli , EQM_EquipementP2Imm.EQU_P1Compteur, "
                        + " PEI_PeriodeGammeImm.TPP_Code, PEI_PeriodeGammeImm.PEI_DateDebut, PEI_PeriodeGammeImm.PEI_DateFin, PEI_PeriodeGammeImm.PEI_JD, "
                        + " PEI_PeriodeGammeImm.PEI_MD, PEI_PeriodeGammeImm.PEI_JF, PEI_PeriodeGammeImm.PEI_MF, PEI_PeriodeGammeImm.PEI_IntAutre, "
                        + " PEI_PeriodeGammeImm.PEI_Intervenant, PEI_PeriodeGammeImm.PEI_Duree, PEI_PeriodeGammeImm.PEI_DureeReel, " + " PEI_PeriodeGammeImm.PEI_Visite, '' AS Total ";
                sSQL = sSQL + " FROM         GAI_GammeImm INNER JOIN "
                        + " PDAB_BadgePDA ON GAI_GammeImm.PDAB_Noauto = PDAB_BadgePDA.PDAB_Noauto INNER JOIN "
                        + " PEI_PeriodeGammeImm INNER JOIN "
                        + " EQM_EquipementP2Imm ON PEI_PeriodeGammeImm.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto INNER JOIN "
                        + " ICC_IntervCategorieContrat ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto ON "
                        + " GAI_GammeImm.GAi_ID = ICC_IntervCategorieContrat.GAi_ID";
                sSQL = sSQL + " WHERE ICC_IntervCategorieContrat.cop_NoAuto="
                        + General.nz(txtCOP_NoAuto.Text, 0)
                        + " ORDER BY PDAB_BadgePDA.PDAB_Libelle,GAI_LIbelle,  ICC_IntervCategorieContrat.CAI_Libelle, EQM_EquipementP2Imm.EQM_CODE ";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                using (var tmpAdo = new ModAdo())
                    rsRecap = tmpAdo.fc_OpenRecordSet(sSQL);

                this.ssGridRecap.DataSource = rsRecap;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                ssGridRecap.UpdateData();
                var _with41 = ssGridRecap;
                _with41.DisplayLayout.Bands[0].Columns["PDAB_Libelle"].Header.Caption = "Localisation";
                _with41.DisplayLayout.Bands[0].Columns["GAI_LIbelle"].Header.Caption = modP2.clibNiv0;
                _with41.DisplayLayout.Bands[0].Columns["CAI_Libelle"].Header.Caption = modP2.clibNiv1;
                //.Columns("EQM_CODE").Caption = cLibNv2Tier
                _with41.DisplayLayout.Bands[0].Columns["EQM_CODE"].Header.Caption = modP2.cLIbNiv2;
                _with41.DisplayLayout.Bands[0].Columns["COP_Noauto"].Hidden = true;
                _with41.DisplayLayout.Bands[0].Columns["TPP_Code"].Header.Caption = "Périodicité";
                //_with41.DisplayLayout.Bands[0].Columns["PEI_DateDebut"].Header.Caption = "Date de début";
                // _with41.DisplayLayout.Bands[0].Columns["PEI_DateFin"].Header.Caption = "Date de fin";
                _with41.DisplayLayout.Bands[0].Columns["PEI_Intervenant"].Header.Caption = "Intervenant";
                _with41.DisplayLayout.Bands[0].Columns["PEI_Duree"].Header.Caption = "Durée";
                //_with41.DisplayLayout.Bands[0].Columns["PEI_DureeReel"].Header.Caption = "Durée réel";

                //=== modif du 08 06 2016, masque les champs compteurs au niveau de la table EQM_EquipementP2Imm
                //=== ceci seront au niveau de la table GAI_GammeImm.
                _with41.DisplayLayout.Bands[0].Columns["EQU_P1Compteur"].Hidden = true;
                _with41.DisplayLayout.Bands[0].Columns["CompteurObli"].Hidden = true;

                //lblRecapNiv2 = cLibNv2Tier

                //=== modif du 10 mars 2014, cumul des heures
                //sSQL = "SELECT     PEI_PeriodeGammeImm.PEI_Intervenant, SUM(PEI_PeriodeGammeImm.PEI_Duree) AS totDuree, SUM(PEI_PeriodeGammeImm.PEI_Visite) AS totVisite, " _
                //& " Personnel.Nom , Personnel.Nom ,  PEI_PeriodeGammeImm.PEI_IntAutre " _
                //& " FROM         PEI_PeriodeGammeImm INNER JOIN" _
                //& " EQM_EquipementP2Imm ON PEI_PeriodeGammeImm.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto INNER JOIN " _
                //& " ICC_IntervCategorieContrat ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto INNER JOIN " _
                //& " PDAB_BadgePDA ON ICC_IntervCategorieContrat.PDAB_Noauto = PDAB_BadgePDA.PDAB_Noauto INNER JOIN " _
                //& " Personnel ON PEI_PeriodeGammeImm.PEI_Intervenant = Personnel.Matricule" _
                //& " WHERE ICC_IntervCategorieContrat.cop_NoAuto=" & nz(txtCOP_NoAuto, 0) _
                //& " GROUP BY PEI_PeriodeGammeImm.PEI_Intervenant, Personnel.Nom, Personnel.Prenom, PEI_PeriodeGammeImm.PEI_IntAutre"


                //Set rsCumul = fc_OpenRecordSet(sSQL)



                dbTotDuree = 0;
                dbTotGeneral = 0;
                dbTotNbVisite = 0;
                ssGridCumul.DataSource = null;
                tpCumulHeure = null;
                i = 0;
                if (rsRecap.Rows.Count == 0)
                {
                    return;
                }
                else
                {
                    //  rsRecap.MoveFirst();
                }

                string sMat = null;
                int x = 0;
                bool bexist = false;

                foreach (DataRow rsRecapRow in rsRecap.Rows)
                {
                    Array.Resize(ref tpCumulHeure, i + 1);

                    if (rsRecapRow["PEI_IntAutre"] != DBNull.Value && Convert.ToInt32(General.nz(rsRecapRow["PEI_IntAutre"], 0)) == 1)
                    {
                        sMat = rsRecapRow["PEI_Intervenant"] + "";
                    }
                    else
                    {
                        //sMat = fc_ADOlibelle("SELECT intervenant_INM From IntervenantImm_INM WHERE  Codeimmeuble_IMM = '" _
                        //& gFr_DoublerQuote(txtCodeimmeuble) & "'  AND CATI_code = '" & cIntervEntretien & "'")
                        using (var tmpAdo = new ModAdo())
                            sMat = tmpAdo.fc_ADOlibelle("SELECT     CodeDepanneur" + " From Immeuble "
                                + " WHERE CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'");
                    }
                    bexist = false;


                    for (x = 0; x < tpCumulHeure.Length; x++)
                    {
                        if (tpCumulHeure[x].sMat != null)
                        {
                            if (tpCumulHeure[x].sMat.ToString().ToUpper() == sMat.ToUpper())//tested
                            {
                                //tpCumulHeure(x).sMat = sMat
                                //tpCumulHeure(x).sNom = ""
                                //tpCumulHeure(x).sPrenom = ""
                                //=== duree.

                                tpCumulHeure[x].dbDuree = tpCumulHeure[x].dbDuree + Convert.ToDouble(General.nz(rsRecapRow["PEI_Duree"], 0));
                                //dbTotDuree = dbTotDuree + nz(rsRecap!PEI_Duree, 0)
                                //=== Nbviste.

                                tpCumulHeure[x].dbVisite = tpCumulHeure[x].dbVisite + Convert.ToDouble(General.nz(rsRecapRow["PEI_Visite"], 0));
                                //dbTotNbVisite = dbTotNbVisite + nz(rsRecap!PEI_Visite, 0)
                                //=== Total.
                                dbTotTemp = 0;

                                dbTotTemp = fc_CalculDuree(Convert.ToDouble(General.nz(rsRecapRow["PEI_Duree"], 0)), Convert.ToDouble(General.nz(rsRecapRow["PEI_Visite"], 0)));
                                tpCumulHeure[x].dbTotal = tpCumulHeure[x].dbTotal + dbTotTemp;
                                bexist = true;
                                break;
                            }
                        }

                    }


                    if (bexist == false)//tested
                    {
                        tpCumulHeure[i].sMat = sMat;
                        tpCumulHeure[i].sNom = "";
                        tpCumulHeure[i].sPrenom = "";
                        //=== duree.

                        tpCumulHeure[i].dbDuree = tpCumulHeure[i].dbDuree + Convert.ToDouble(General.nz(rsRecapRow["PEI_Duree"], 0));
                        //dbTotDuree = dbTotDuree + nz(rsRecap!PEI_Duree, 0)
                        //=== Nbviste.

                        tpCumulHeure[i].dbVisite = tpCumulHeure[i].dbVisite + Convert.ToDouble(General.nz(rsRecapRow["PEI_Visite"], 0));
                        //dbTotNbVisite = dbTotNbVisite + nz(rsRecap!PEI_Visite, 0)
                        //=== Total.
                        dbTotTemp = 0;

                        dbTotTemp = fc_CalculDuree(Convert.ToDouble(General.nz(rsRecapRow["PEI_Duree"], 0)), Convert.ToDouble(General.nz(rsRecapRow["PEI_Visite"], 0)));
                        tpCumulHeure[i].dbTotal = tpCumulHeure[i].dbTotal + dbTotTemp;
                        i = i + 1;

                    }

                    // rsRecap.MoveNext();
                }
                string sNom = "";

                DataTable dtCumul = new DataTable();
                dtCumul.Columns.Add("Matricule");
                dtCumul.Columns.Add("Nom");
                dtCumul.Columns.Add("Total Visite");
                dtCumul.Columns.Add("Total Durée");

                var dr = dtCumul.NewRow();

                for (i = 0; i < tpCumulHeure.Length; i++)//============>Tested
                {
                    using (var tmpAdo = new ModAdo())
                        sNom = tmpAdo.fc_ADOlibelle(" SELECT NOM, PRENOM FROM PERSONNEL WHERE MATRICULE = '"
                            + StdSQLchaine.gFr_DoublerQuote(tpCumulHeure[i].sMat) + "'");

                    dr["Matricule"] = tpCumulHeure[i].sMat;
                    dr["Nom"] = sNom;
                    dbTotDuree = dbTotDuree + tpCumulHeure[i].dbDuree;
                    dr["Total Visite"] = tpCumulHeure[i].dbVisite;
                    dbTotNbVisite = dbTotNbVisite + tpCumulHeure[i].dbVisite;
                    dr["Total Durée"] = tpCumulHeure[i].dbTotal;
                    dbTotGeneral = dbTotGeneral + tpCumulHeure[i].dbTotal;
                    dtCumul.Rows.Add(dr.ItemArray);

                    /* sAddItem = "@" + tpCumulHeure[i].sMat + "@%";
                         using (var tmpAdo = new ModAdo())
                             sAddItem = sAddItem + "@" + tmpAdo.fc_ADOlibelle(" SELECT NOM, PRENOM FROM PERSONNEL WHERE MATRICULE = '" 
                                 + StdSQLchaine.gFr_DoublerQuote(tpCumulHeure[i].sMat) + "'") + "@%";
                         sAddItem = sAddItem + "@" + tpCumulHeure[i].sPrenom + "@%";

                         sAddItem = sAddItem + "@" + tpCumulHeure[i].dbDuree + "@%";
                         dbTotDuree = dbTotDuree + tpCumulHeure[i].dbDuree;

                         sAddItem = sAddItem + "@" + tpCumulHeure[i].dbVisite + "@%";
                         dbTotNbVisite = dbTotNbVisite + tpCumulHeure[i].dbVisite;

                         //dbTotTemp = 0
                         //dbTotTemp = fc_CalculDuree(nz(rsCumul!totDuree, 0), nz(rsCumul!totVisite, 0))
                         sAddItem = sAddItem + "@" + tpCumulHeure[i].dbTotal + "@%";
                         dbTotGeneral = dbTotGeneral + tpCumulHeure[i].dbTotal;*/

                    ssGridCumul.DataSource = dtCumul;
                }

                //==== affiche le cumul //tested
                dr["Matricule"] = cTotal;
                dr["Nom"] = "GENERAL";
                dr["Total Visite"] = dbTotNbVisite;
                dr["Total Durée"] = dbTotGeneral;
                dtCumul.Rows.Add(dr.ItemArray);


                /*sAddItem = "@" + cTotal + "@%";
                  sAddItem = sAddItem + "@GENERAL@%";
                  sAddItem = sAddItem + "@@%";
                  sAddItem = sAddItem + "@" + dbTotDuree + "@%";
                  sAddItem = sAddItem + "@" + dbTotNbVisite + "@%";
                  sAddItem = sAddItem + "@" + dbTotGeneral + "@%";*/

                ssGridCumul.DataSource = dtCumul;
                ssGridCumul.UpdateData();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadXXX");
            }
        }
        private double fc_CalculDuree(double dbDuree, double dbNbVisite)
        {
            double functionReturnValue = 0;
            double dbTot = 0;
            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                dbTot = dbDuree * dbNbVisite;
                dbTot = General.FncArrondir(dbTot, 2);
                functionReturnValue = dbTot;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_CalculDuree");
                return functionReturnValue;
            }

        }
        private void fc_fGrilleInit()
        {

            ///''    fc_formatGrille ssICA_ImmCategorieInterv, False
            ///''    fc_formatGrille ssEQU_EquipementP2Imm

            ///''    fc_formatGrille ssGammeArticle, False
            ///
            /// 

            sheridan.fc_formatGrille(ssFAP_FacArticleP2);//TESTED

            ///''    fc_formatGrille ssGammePlanning, False
            ///


            sheridan.fc_formatGrille(ssPEI_PeriodeGammeImm, true, false, true);

            fc_DropJour();//tested
            fc_DropMois();//tested
            fc_DropSemaine();//TESTED
            fc_DropMatriculeType();//tested
            fc_DropIntervenant();//TESTED
            fc_ssDropTPP_TypePeriodeP2();//tested

        }
        private void fc_AfficheGrille()
        {
            switch (SSTab2.SelectedTab.Index)
            {
                case cGamme:
                    //1
                    break;
                ///''            ssICA_ImmCategorieInterv.Visible = True
                ///''            ssEQU_EquipementP2Imm.Visible = True

                case cNature:
                    // 3
                    break;
                ///''            ssGammePlanning.Visible = True
                ///''            ssFAP_FacArticleP2.Visible = True
                case cPlanning:
                    //4
                    break;
                    ///''            ssGammeArticle.Visible = True
                    ///''            ssPEI_PeriodeGammeImm.Visible = True
            }

            Timer1.Enabled = false;
        }

        private void TreeView1_AfterLabelEdit(object sender, NodeEventArgs e)
        {
            //bool Cancel = e.TreeNode.;
            string NewString = e.TreeNode.Text;

            switch (General.Left(e.TreeNode.Key, 1))//verifier
            {
                case cNiv1:
                    txtCAI_Libelle.Text = NewString;//verifier
                    break;
                case cNiv2:
                    break;

                case cNiv3:
                    break;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sID"></param>
        private void fc_TabICC(string sID)
        {
            string sSQL = null;
            DataTable rstmp = default(DataTable);

            sSQL = "SELECT CAI_Code, CAI_Libelle, ICC_Affiche,  ICC_Noauto " + " FROM ICC_IntervCategorieContrat" + " WHERE ICC_NoAuto =" + sID;
            using (var tmpAdo = new ModAdo())
                rstmp = tmpAdo.fc_OpenRecordSet(sSQL);

            if (rstmp.Rows.Count > 0)//tested
            {
                txtCai_code.Text = General.nz(rstmp.Rows[0]["CAI_Code"], "").ToString();
                txtCAI_Libelle.Text = General.nz(rstmp.Rows[0]["CAI_Libelle"], "").ToString();
                chkICC_Affiche.Checked = General.nz(rstmp.Rows[0]["ICC_Affiche"], "0").ToString() == "1" ? true : false;
                txtICC_NoAuto.Text = General.nz(rstmp.Rows[0]["ICC_Noauto"], 0).ToString();
            }
            else
            {
                txtCai_code.Text = "";
                txtCAI_Libelle.Text = "";
                chkICC_Affiche.CheckState = System.Windows.Forms.CheckState.Unchecked;
                txtICC_NoAuto.Text = "";
            }

            //rstmp.Close();        
            rstmp = null;
            SSTab4.Visible = true;
            SSTab4.Tabs[cTabNiv1].Visible = true;
            SSTab4.SelectedTab = SSTab4.Tabs[cTabNiv1];

        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sID"></param>
        private void fc_TabGAI(string sID)
        {
            string sSQL = null;
            DataTable rstmp = default(DataTable);

            sSQL = "SELECT     GAi_ID, GAM_Code, GAI_LIbelle, GAI_Compteur, GAI_CompteurObli " + " FROM         GAI_GammeImm"
                + " WHERE GAi_ID =" + sID;
            using (var tmpAdo = new ModAdo())
                rstmp = tmpAdo.fc_OpenRecordSet(sSQL);

            if (rstmp.Rows.Count > 0)
            {
                txtGAM_Code.Text = General.nz(rstmp.Rows[0]["GAM_Code"], "").ToString();
                txtGAI_LIbelle.Text = General.nz(rstmp.Rows[0]["GAI_LIbelle"], "").ToString();
                chkGAI_Compteur.Checked = General.nz(rstmp.Rows[0]["GAI_Compteur"], 0).ToString() == "1" ? true : false;
                chkGAI_CompteurObli.Checked = General.nz(rstmp.Rows[0]["GAI_CompteurObli"], 0).ToString() == "1" ? true : false;



                //chkICC_Affiche.value = nz(rstmp!ICC_Affiche, vbUnchecked)
                txtGAi_ID.Text = General.nz(rstmp.Rows[0]["GAi_ID"], "").ToString();
            }
            else
            {
                txtGAM_Code.Text = "";
                txtGAI_LIbelle.Text = "";
                //chkICC_Affiche.value = vbUnchecked
                txtGAi_ID.Text = "";
                chkGAI_Compteur.CheckState = System.Windows.Forms.CheckState.Unchecked;
                chkGAI_CompteurObli.CheckState = System.Windows.Forms.CheckState.Unchecked;
            }

            //  rstmp.Close();          
            rstmp = null;
            SSTab4.Visible = true;
            SSTab4.Tabs[cTabNiv4].Visible = true;
            SSTab4.SelectedTab = SSTab4.Tabs[cTabNiv4];
        }
        private void fc_TabEQM(string sID)
        {
            string sSQL = null;
            DataTable rstmp = default(DataTable);

            sSQL = "SELECT EQM_Affiche, EQM_NoAuto, EQM_lIBELLE,EQM_CODE, EQM_NoAuto,EQM_Observation " +
                " , duree, dureeplanning , CompteurObli, EQU_P1Compteur, " +
                " CTR_ID, CTR_Libelle, MOY_ID, MOY_Libelle, ANO_ID, ANO_Libelle, OPE_ID, OPE_Libelle " +
                " From EQM_EquipementP2Imm" + " WHERE     (EQM_NoAuto = " + sID + ")";
            //  & " order by EQM_lIBELLE"
            using (var tmpAdo = new ModAdo())
                rstmp = tmpAdo.fc_OpenRecordSet(sSQL);
            if (rstmp.Rows.Count > 0)//tested
            {
                txtEQM_LIBELLE.Text = General.nz(rstmp.Rows[0]["EQM_CODE"], "").ToString();
                chkEQM_Affiche.Checked = General.nz(rstmp.Rows[0]["EQM_Affiche"], 0).ToString() == "1" ? true : false;
                txtEQM_NoAuto.Text = General.nz(rstmp.Rows[0]["EQM_NoAuto"], "").ToString();
                txtEQM_Observation.Text = General.nz(rstmp.Rows[0]["EQM_Observation"], "").ToString();
                txtDuree.Text = General.nz(rstmp.Rows[0]["Duree"], "").ToString();
                txtDureePlanning.Text = General.nz(rstmp.Rows[0]["DureePlanning"], "").ToString();
                chkCompteurObli.Checked = General.nz(rstmp.Rows[0]["CompteurObli"], 0).ToString() == "1" ? true : false;
                chkEQU_P1Compteur.Checked = General.nz(rstmp.Rows[0]["EQU_P1Compteur"], 0).ToString() == "1" ? true : false;
                Combo0.Text = rstmp.Rows[0]["CTR_Libelle"] + "";
                txt0.Text = rstmp.Rows[0]["CTR_ID"] + "";
                Combo1.Text = rstmp.Rows[0]["MOY_Libelle"] + "";
                txt1.Text = rstmp.Rows[0]["MOY_ID"] + "";
                Combo2.Text = rstmp.Rows[0]["ANO_Libelle"] + "";
                txt2.Text = rstmp.Rows[0]["ANO_ID"] + "";
                Combo3.Text = rstmp.Rows[0]["OPE_Libelle"] + "";
                txt3.Text = rstmp.Rows[0]["OPE_ID"] + "";

            }
            else
            {
                txtEQM_NoAuto.Text = "";
                txtEQM_LIBELLE.Text = "";
                chkEQM_Affiche.CheckState = System.Windows.Forms.CheckState.Unchecked;
                txtEQM_Observation.Text = "";
                txtDuree.Text = "";
                txtDureePlanning.Text = "";
                chkCompteurObli.CheckState = System.Windows.Forms.CheckState.Unchecked;
                chkEQU_P1Compteur.CheckState = System.Windows.Forms.CheckState.Unchecked;
                Combo0.Text = "";
                Combo1.Text = "";
                Combo2.Text = "";
                Combo3.Text = "";
                txt0.Text = "";
                txt1.Text = "";
                txt2.Text = "";
                txt3.Text = "";
            }

            // rstmp.Close();      
            rstmp = null;

            fc_PEI_PeriodeGammeImm(Convert.ToInt32(sID));//tested
            fc_FAP_FacArticleP2(Convert.ToInt32(sID));//tested

            SSTab4.Visible = true;
            SSTab4.Tabs[cTabNiv2].Visible = true;
            SSTab4.SelectedTab = SSTab4.Tabs[cTabNiv2];

        }
        private void fc_TabFAP(string sID)
        {
            string sSQL = null;
            DataTable rstmp = default(DataTable);

            sSQL = "SELECT     FAP_Designation1, FAP_Affiche, FAP_NoAuto " + " From FAP_FacArticleP2" + " WHERE     (FAP_NoAuto =" + sID + ")";
            using (var tmpAdo = new ModAdo())
                rstmp = tmpAdo.fc_OpenRecordSet(sSQL);
            if (rstmp.Rows.Count > 0)
            {
                txtFAP_Designation1.Text = General.nz(rstmp.Rows[0]["FAP_Designation1"], 0).ToString();
                chkFAP_Affiche.Checked = General.nz(rstmp.Rows[0]["FAP_Affiche"], 0).ToString() == "1" ? true : false;
                txtFAP_NoAuto.Text = General.nz(rstmp.Rows[0]["FAP_NoAuto"], 0).ToString();
            }
            else
            {
                txtFAP_Designation1.Text = "";
                chkFAP_Affiche.CheckState = System.Windows.Forms.CheckState.Unchecked;
                txtFAP_NoAuto.Text = "";
            }

            //rstmp.Close();

            rstmp = null;

            SSTab4.Visible = true;
            SSTab4.Tabs[cTabNiv3].Visible = true;
            SSTab4.SelectedTab = SSTab4.Tabs[cTabNiv3];

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="Node"></param>
        private void fc_LoadScreenTreev(UltraTreeNode Node)
        {
            string sID = null;
            short i = 0;
            sID = General.Mid(Node.Key, 2, Node.Key.Length - 1);

            for (i = 0; i <= 3; i++)//tested
            {
                SSTab4.Tabs[i].Visible = false;
            }
            if (General.sPriseEnCompteLocalP2 == "1")//tested
            {
                if (General.Left(Node.Key, 1).ToString().ToUpper() == cNiv2.ToUpper())//tested
                {
                    //fc_TabICC sID
                    fc_TabGAI(sID);
                }
                else if (General.Left(Node.Key, 1).ToString().ToUpper() == cNiv3.ToUpper())//tested
                {
                    fc_TabICC(sID);
                    //fc_TabEQM sID
                }
                else if (General.Left(Node.Key, 1).ToString().ToUpper() == cNiv4.ToUpper())//tested
                {
                    fc_TabEQM(sID);
                }
                else
                {
                    SSTab4.Visible = false;
                }
            }
            else
            {
                if (General.Left(Node.Key, 1).ToString().ToUpper() == cNiv1.ToUpper())//tested
                {
                    fc_TabICC(sID);
                }
                else if (General.Left(Node.Key, 1).ToString().ToUpper() == cNiv2.ToUpper())//tested
                {
                    fc_TabEQM(sID);
                }
                else if (General.Left(Node.Key, 1).ToString().ToUpper() == cNiv3.ToUpper())//tested
                {
                    fc_TabFAP(sID);
                }
                else
                {
                    SSTab4.Visible = false;
                }
            }

            /*for (i = 0; i <= 3; i++)
            {
                SSTab4.Tabs[i].Visible = false;
            }*/
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lEQM_NoAuto"></param>
        private void fc_PEI_PeriodeGammeImm(int lEQM_NoAuto)
        {
            General.sSQL = " SELECT CodeImmeuble, EQM_NoAuto, PEI_NoAuto, TPP_Code,PEI_Cycle, PEI_DateDebut, PEI_DateFin, PEI_JD,"
                + " PEI_MD, PEI_JF, PEI_MF,PEI_PassageObli, PEI_ForceJournee ,SEM_Code, PEI_Heure, PEI_Duree,PEI_DureeReel,PEI_CompteurObli,PEI_DureeVisu," + " PEI_Samedi, PEI_Dimanche, PEI_Pentecote, PEI_Jourferiee," +
                " PEI_Visite, PEI_ForceVisite," + " PEI_Tech ,PEI_Cout,PEI_MtAchat , PEI_KFO, PEI_KMO, PEI_Calcul ,PEI_TotalHT, PEI_IntAutre," + "  PEI_Intervenant, PEI_APlanifier,COP_NoAuto, PEI_AvisPassage, PEI_AvisNbJour" +
                "  " + " From PEI_PeriodeGammeImm " + " WHERE     (EQM_NoAuto =" + General.nz(lEQM_NoAuto, 0) + ")";

            rsPEI_PeriodeGammeImm = tmpAdorsPEI_PeriodeGammeImm.fc_OpenRecordSet(General.sSQL, ssPEI_PeriodeGammeImm, "PEI_NoAuto");
            ssPEI_PeriodeGammeImm.Visible = false;
            ssPEI_PeriodeGammeImm.DataSource = rsPEI_PeriodeGammeImm;
            ssPEI_PeriodeGammeImm.Visible = true;
            ssPEI_PeriodeGammeImm.UpdateData();
        }

        private void txtCodeImmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string sSQL = null;
            int lCop_NoAuto = 0;

            try
            {
                if (KeyAscii == 13)
                {
                    sSQL = "SELECT     COP_NoAuto" + " From COP_ContratP2 WHERE     (codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "')";
                    using (var tmpAdo = new ModAdo())
                        lCop_NoAuto = Convert.ToInt32(General.nz(tmpAdo.fc_ADOlibelle(sSQL), 0));

                    if (lCop_NoAuto != 0)
                    {
                        txtCOP_NoAuto.Text = Convert.ToString(lCop_NoAuto);
                        fc_ChargeEnregistrement("", "", false, Convert.ToInt32(txtCOP_NoAuto.Text));
                        // fc_AfficheParametre();existe dans vb6 mais son code est en commentaire
                        SumGamme();
                        fc_Calcul();
                        fc_DetailPrestation(Convert.ToInt32(txtCOP_NoAuto.Text));

                    }
                    else
                    {
                        CmdRechercher_Click(CmdRechercher, new System.EventArgs());
                        //            With RechercheMultiCritere
                        //                 .Champs0 = txtCodeImmeuble & "*"
                        //                   blControle = True
                        //                    .Show vbModal
                        //                   If nz(.LigneChoisie, "") <> "" Then
                        //                                fc_ChargeEnregistrement "", "", False, CLng(.dbgrid1.Columns("No FICHE GAMAO").value)
                        //                                fc_AfficheParametre
                        //                                SumGamme
                        //                                fc_Calcul
                        //                                fc_DetailPrestation txtCOP_NoAuto
                        //                   End If
                        //            End With
                    }

                    KeyAscii = 0;
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";");
            }
        }

        private void txtCOP_Coef_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            KeyAscii = (short)General.fc_PointVirgule(KeyAscii);
        }

        private void txtCOP_Coef_Validated(object sender, EventArgs e)
        {
            fc_Calcul();
        }

        private void txtCOP_Coefgest_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            KeyAscii = (short)General.fc_PointVirgule(KeyAscii);
        }

        private void txtCOP_Coefgest_Validated(object sender, EventArgs e)
        {
            fc_Calcul();
        }

        private void txtCOP_CoefSt_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            KeyAscii = (short)General.fc_PointVirgule(KeyAscii);
        }

        private void txtCOP_CoefSt_Validated(object sender, EventArgs e)
        {
            fc_Calcul();
        }

        private void txtCOP_Signataire_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                cmdFindSignataire_Click(cmdFindSignataire, new System.EventArgs());
            }

        }

        private void txtCOP_Signataire_Validated(object sender, EventArgs e)
        {
            lbllibCOP_Signataire.Text = fc_libpersonnel(txtCOP_Signataire.Text);
        }

        private void txtCOP_SStraitant_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                cmdFindStraitant_Click(cmdFindStraitant, new System.EventArgs());
            }
        }

        private void txtCOP_Statut_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                cmdRechercheCodetat_Click(cmdRechercheCodetat, new System.EventArgs());
            }
        }

        private void txtCOP_Statut_Validated(object sender, EventArgs e)
        {
            fc_libStatut();
        }

        private void txtCOP_SuiviePar_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                // cmdFindSuiviePar_Click(cmdFindSuiviePar, new System.EventArgs()); le code de cet evenemnt est en commentaire dans vb6
            }
        }
        private string fc_libpersonnel(string sCode)
        {
            string functionReturnValue = null;
            try
            {

                // ERROR: Not supported in C#: OnErrorStatement
                using (var tmpAdo = new ModAdo())
                    functionReturnValue = tmpAdo.fc_ADOlibelle("SELECT prenom, Nom FROM Personnel where Matricule='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'", true);
                return functionReturnValue;
            }

            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_libpersonnel");
                return functionReturnValue;
            }

        }

        private void txtCOP_Technicien_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                cmdIntervenant_Click(cmdIntervenant, new System.EventArgs());
            }
        }

        private void txtCOP_Technicien_Validating(object sender, CancelEventArgs e)
        {
            lbllibIntervenant.Text = fc_libpersonnel(txtCOP_Technicien.Text);
        }

        private void txtCOP_TotalApresCoef_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            KeyAscii = (short)General.fc_PointVirgule(KeyAscii);
        }

        private void txtCOP_TotalApresCoefgest_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            KeyAscii = (short)General.fc_PointVirgule(KeyAscii);
        }

        private void txtCOP_TotalApresCoefSt_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            KeyAscii = (short)General.fc_PointVirgule(KeyAscii);
        }

        private void txtCOP_TotalHTgest_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            KeyAscii = (short)General.fc_PointVirgule(KeyAscii);
        }

        private void txtCOP_TotalHTgest_Validated(object sender, EventArgs e)
        {
            fc_Calcul();
        }

        private void txtCOP_TotalHT_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            KeyAscii = (short)General.fc_PointVirgule(KeyAscii);
        }

        private void txtCOP_TotalHT_Validated(object sender, EventArgs e)
        {
            fc_Calcul();
        }

        private void txtCOP_TotalHTST_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            KeyAscii = (short)General.fc_PointVirgule(KeyAscii);
        }

        private void txtCOP_TotalHTST_Validated(object sender, EventArgs e)
        {
            fc_Calcul();
        }

        /// <summary>
        /// tested
        /// </summary>
        private void fc_Calcul()
        {
            try
            {
                //===> Mondir le 18.02.2021, commnted in V16.02.2021
                //--gesten
                //txtCOP_TotalApresCoefgest.Text = General.FncArrondir(Convert.ToDouble(General.nz(txtCOP_TotalHTgest.Text, 0)) * Convert.ToDouble(General.nz(txtCOP_Coefgest.Text, 0))).ToString();

                //--sous traitant.
                //txtCOP_TotalApresCoefSt.Text = (General.FncArrondir(Convert.ToDouble(General.nz(txtCOP_TotalHTST.Text, 0)) * Convert.ToDouble(General.nz(txtCOP_CoefSt.Text, 0)))).ToString();
                //txtCOP_Coef = (nz(txtCOP_TotalHTST, 1) + nz(txtCOP_Coefgest, 1)) / 2

                //--general
                //txtCOP_TotalHT.Text = (General.FncArrondir(Convert.ToDouble(General.nz(txtCOP_TotalHTgest.Text, 0)) + Convert.ToDouble(General.nz(txtCOP_TotalHTST.Text, 0)), 2)).ToString();
                //if (Convert.ToDouble(txtCOP_TotalHTgest.Text) != 0)
                //{
                //    txtCOP_Coef.Text = (General.FncArrondir((Convert.ToDouble(Convert.ToDouble(txtCOP_TotalHTgest.Text) * Convert.ToDouble(txtCOP_Coefgest.Text))
                //        + Convert.ToDouble(Convert.ToDouble(txtCOP_CoefSt.Text) * Convert.ToDouble(txtCOP_TotalHTST.Text))) / (Convert.ToDouble(txtCOP_TotalHTgest.Text)
                //        + Convert.ToDouble(txtCOP_TotalHTST.Text)), 2)).ToString();
                //}
                //txtCOP_TotalApresCoef.Text = (General.FncArrondir((Convert.ToDouble(General.nz(txtCOP_TotalHT.Text, 0)) * Convert.ToDouble(General.nz(txtCOP_Coef.Text, 0))), 2)).ToString();
                //===> Fin Modif Mondir

                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Source, ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }


        private void fc_recupTVA()
        {
            General.sSQL = "select tva_code, tva_tva from tva_type";

            rsTVA = new DataTable();
            ModAdo ModAdorsTVA = new ModAdo();
            rsTVA = ModAdorsTVA.fc_OpenRecordSet(General.sSQL);

            /* 
             *  rsTVA.CursorLocation = ADODB.CursorLocationEnum.adUseClient;
              rsTVA.CursorType = ADODB.CursorTypeEnum.adOpenStatic;
              rsTVA.LockType = ADODB.LockTypeEnum.adLockBatchOptimistic;
              rsTVA.let_ActiveConnection(General.adocnn.ConnectionString);
              rsTVA.Open(General.sSQL);

              rsTVA.ActiveConnection = null;*/

        }

        private void UserDocP2V2_Load(object sender, EventArgs e)
        {


            /*initule
             foreach (object objcontrole_loopVariable in this.Controls)
             {
                 objcontrole = objcontrole_loopVariable;
                 if (objcontrole is AxSSDataWidgets_B_OLEDB.AxSSOleDBGrid)
                 {
                     //  fc_LoadDimensionGrille Me.Name, objcontrole
                 }
                 else if (objcontrole is VB6.ADODC)
                 {
                     //   objcontrole.LockType = adLockBatchOptimistic
                 }
             }*/

            fc_fGrilleInit();

            // ModMain.fc_Color(this); le code de cette fonstion est en commentaire

            SSTab4.Tabs[3].Text = modP2.clibNiv0;
            lblNiv0.Text = modP2.clibNiv0;

            SSTab4.Tabs[0].Text = modP2.clibNiv1;
            lblNiv1.Text = modP2.clibNiv1;

            SSTab4.Tabs[1].Text = modP2.cLIbNiv2;
            lblNiv2.Text = modP2.cLIbNiv2;

            SSTab4.Tabs[2].Text = modP2.cLibNiv3;
            lblNiv3.Text = modP2.cLibNiv3;

            var _with44 = ssGridTachesAnnuel;
            // .Columns("CAI_Libelle").Caption = clibNiv1
            // .Columns("EQM_CODE").Caption = cLibNv2Tier

            var _with45 = ssOpTaches;
            // _with45.DisplayLayout.Bands[0].Columns["EQM_Libelle"].Header.Caption = modP2.cLibNv2Tier;TODO
            // _with45.DisplayLayout.Bands[0].Columns["TIP2_Designation1"].Header.Caption = modP2.cLibNiv3;TODO

            optOperation.Text = modP2.cLibNv2Tier;

            if (General.sPriseEnCompteLocalP2 == "1")
            {
                //ssGridVisite.DisplayLayout.Bands[0].Columns["PDAB_Libelle"].Hidden = false;TODO
            }
            else
            {
                // ssGridVisite.DisplayLayout.Bands[0].Columns["PDAB_Libelle"].Hidden = true;TODO
            }

            var _with46 = ssGridRecap;

            // TODO : Salma verifier ces lignes
            // _with46.StyleSets.Add(cStyleCompteurObl);
            //  _with46.StyleSets(cStyleCompteurObl).BackColor = 0xc0ffc0;
            //== vert

            var _with47 = ssGridCumul;
            // TODO : Salma verifier ces lignes
            //   _with47.StyleSets.Add(cStyletTOTAL);
            //  _with47.StyleSets(cStyletTOTAL).BackColor = 0x8080ff;
            //== vert

            SSTab1.Tabs[2].Visible = false;
            SSTab1.Tabs[3].Visible = false;
            SSTab1.Tabs[4].Visible = false;

            //===> Mondir le 28.01.2021, ajout des modfis de la version V27.01.2021
            if (General.fncUserName().ToUpper() == "rachid abbouchi".ToUpper() || General.fncUserName().ToUpper() == "mondir".ToUpper())
            {
                cmdTest.Visible = true;
            }
            else
            {
                cmdTest.Visible = false;
            }
            //===> Fin Modif Mondir

        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocP2V2_VisibleChanged(object sender, EventArgs e)
        {
            string sFormulaire = null;

            if (!Visible)
                return;
            /* VERIFIER le apres (menu à gauche dans vb6)
             * 
                if (!string.IsNullOrEmpty(General.LogoSociete) | !Information.IsDBNull(General.LogoSociete))
                {
                    imgLogoSociete.Image = System.Drawing.Image.FromFile(General.LogoSociete);
                    if (Information.IsNumeric(General.imgTop))
                    {
                        imgLogoSociete.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(General.imgTop);
                    }
                    if (Information.IsNumeric(General.imgLeft))
                    {
                        imgLogoSociete.Left = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsX(General.imgLeft);
                    }
                }


                if (!string.IsNullOrEmpty(General.NomSousSociete) & !Information.IsDBNull(General.NomSousSociete))
                {
                    lblNomSociete.Text = General.NomSousSociete;
                    System.Windows.Forms.Application.DoEvents();
                }

                lblLienGoFicheXXX[40].Text = General.sNomLien0;
                lblLienGoFicheXXX[41].Text = General.sNomLien1;
                lblLienGoFicheXXX[39].Text = General.sNomLien2;
                */

            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocP2V2");
            if (General.sPriseEnCompteLocalP2 == "1")
            {
                //=== version avec prise en compte de la localisation
                //cmdDupliqueInter.Visible = True
            }
            else
            {
                cmdDupliqueInter.Visible = false;
            }


            //SSTab2.TabCaption(2) = cGammeLib

            txtCOP_Statut.Tag = cActif;
            txtCOP_DateSignature.Tag = cActif;

            //== modif du 06 10 2004
            //' txtCOP_DateDemande.Tag = cActif

            General.ReinitPage(this);//tested

            rsTVA = null;
            rsPEI_PeriodeGammeImm = null;
            blModif = false;
            blnAjout = false;
            lICA = 0;
            lFAP_Article = 0;
            lEquip = 0;
            lMateriel = 0;
            lCOS_ContratChap = 0;
            lchapGam = 0;
            lCSS_ContratSchap = 0;

            modP2.fc_ParamGmao();//tested

            //=== récupere les dates de simulation du planning.
            dtDateDebPlaning.Value = General.getFrmReg(Variable.cUserDocP2, "dtDateDebPlaning", Convert.ToString(DateTime.Today));
            dtDateFinPlaning.Value = General.getFrmReg(Variable.cUserDocP2, "dtDateFinPlaning", Convert.ToString(DateTime.Today));

            var _with48 = this;

            if (General.sP2version2 == "1")//tested
            {
                sFormulaire = Variable.cUserDocP2;
            }
            else
            {
                sFormulaire = this.Name;
            }

            //=== positionne sur le dernier enregistrement.
            if (ModParametre.fc_RecupParam(sFormulaire) == true)
            {

                //==== charge les enregistremants
                if (ModParametre.sFicheAppelante == Variable.cUserDocStandard)//tested
                {
                    fc_ChargeEnregistrement(ModParametre.sNewVar, "", true);
                }
                else//tested
                {
                    fc_ChargeEnregistrement("", "", false, Convert.ToInt32(ModParametre.sNewVar));
                }

            }


            SSTab1.SelectedTab = SSTab1.Tabs[0];
            SSTab2.SelectedTab = SSTab2.Tabs[0];

            //If txtCOP_DateImpression <> "" Then
            //    fc_Lock True
            //Else

            fc_Lock(false);//tested


            //End If

            fc_DroitContratP2();//tested
            fc_dropArticle();//tested
            fc_DetailPrestation(Convert.ToInt32(General.nz(txtCOP_NoAuto.Text, 0)));
            fc_DroitAnalytique();//tested
            fc_DroitFichier();//tested

            //  GridPrestations.DisplayLayout.Bands[0].Columns["CodePrestation"].ValueList = cmbDrpdwnPrestations;

            General.sSQL = "SELECT CodePrestation AS [Code Prestation],Designation"
                + " FROM GMAO_PrestationsSource ORDER BY CodePrestation";
            sheridan.InitialiseCombo((this.cmbDrpdwnPrestations), General.sSQL, "Code Prestation", true);//tested


            //   GridPrestations.DisplayLayout.Bands[0].Columns["CodePrestation"].ValueList = cmbDrpdwnPrestations;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <returns></returns>
        private bool fc_DroitAnalytique()
        {
            bool functionReturnValue = false;

            string sAut = null;

            //=== court-circuite la fonction le 01/12/2015.
            Frame1.Enabled = true;
            return functionReturnValue;
            using (var tmpAdo = new ModAdo())
                sAut = tmpAdo.fc_ADOlibelle("SELECT AUT_Nom " + " From AUT_Autorisation"
                    + " WHERE AUT_Formulaire = '" + Variable.cUserDocP2 + "' "
                    + " AND AUT_Objet = 'ContratAnalytique'"
                    + " AND AUT_Nom = '" + General.fncUserName() + "'");
            if (!string.IsNullOrEmpty(sAut))
            {
                Frame1.Enabled = true;
                functionReturnValue = true;
                // Command1.Enabled = True
            }
            else
            {
                Frame1.Enabled = false;
                functionReturnValue = false;
                // Command1.Enabled = False
            }
            return functionReturnValue;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <returns></returns>
        private bool fc_DroitFichier()
        {
            bool functionReturnValue = false;

            string sAut = null;

            //=== court-circuite la fonction le 01/12/2015.

            Command1.Enabled = true;
            return functionReturnValue;
            using (var tmpAdo = new ModAdo())
                sAut = tmpAdo.fc_ADOlibelle("SELECT AUT_Nom " + " From AUT_Autorisation"
                    + " WHERE AUT_Formulaire = '" + Variable.cUserDocP2 + "' "
                    + " AND AUT_Objet = 'FichierTransfert'"
                    + " AND AUT_Nom = '" + General.fncUserName() + "'");

            if (!string.IsNullOrEmpty(sAut))
            {

                Command1.Enabled = true;
            }
            else
            {

                Command1.Enabled = false;
            }
            return functionReturnValue;


        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_DropMatriculeType()
        {
            General.sSQL = "";
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Qualification.CodeQualif ,"
                + " Qualification.Qualification, Personnel.Kobby, Personnel.Note_NOT, PER_Charge as Coût"
                + " FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif"
                + " WHERE Personnel.Matricule like 'T%' or Personnel.Matricule like 'S%'";

            // If ssDropMatricule.Rows = 0 Then
            sheridan.InitialiseCombo((this.ssDropMatricule), General.sSQL, "Matricule", true);
            ssDropMatricule.DisplayLayout.Bands[0].Columns["CodeQualif"].Hidden = true;
            ssDropMatricule.DisplayLayout.Bands[0].Columns["Kobby"].Hidden = true;
            ssDropMatricule.DisplayLayout.Bands[0].Columns["Note_NOT"].Hidden = true;
            //  End If
            // 

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_DropIntervenant()
        {
            General.sSQL = "";
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom,Personnel.prenom, Qualification.CodeQualif,"
                + " Qualification.Qualification, Personnel.Kobby, Personnel.Note_NOT"
                + " FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";

            sheridan.InitialiseCombo((this.ssDropIntervenant), General.sSQL, "Matricule", true);
            ssDropIntervenant.DisplayLayout.Bands[0].Columns["CodeQualif"].Hidden = true;
            ssDropIntervenant.DisplayLayout.Bands[0].Columns["Kobby"].Hidden = true;
            ssDropIntervenant.DisplayLayout.Bands[0].Columns["Note_NOT"].Hidden = true;



        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        /// <returns></returns>
        private string fc_CptRendu(string sCodeImmeuble)
        {
            string functionReturnValue = null;
            string sSQL = null;
            try
            {
                sSQL = "SELECT CodeImmeuble From TechReleve WHERE CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                using (var tmpAdo = new ModAdo())
                    sSQL = tmpAdo.fc_ADOlibelle(sSQL);

                if (!string.IsNullOrEmpty(sSQL))//tested
                {
                    btnCpteRendu.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0ffc0);
                }
                else
                {
                    btnCpteRendu.BackColor = System.Drawing.ColorTranslator.FromOle(0xffc0c0);
                }

                functionReturnValue = sSQL;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";");
                return functionReturnValue;
            }
        }
        private void fc_ChargeEnregistrement(string sCode, string sAvenant, bool bFicheStandard = false, int lAutre = 0)
        {
            rstmp = new DataTable();

            if (bFicheStandard == true)
            {
                General.sSQL = "SELECT * From COP_ContratP2" + " WHERE  NumFicheStandard =" + sCode;
                using (var tmpAdo = new ModAdo())
                    rstmp = tmpAdo.fc_OpenRecordSet(General.sSQL);

                if (rstmp.Rows.Count == 0)
                {

                    fc_Ajouter();
                    //blnAjout = True
                    txtCodeImmeuble.Text = General.getFrmReg(Variable.cUserDocP2, "CodeImmeuble", "");
                    txtCOP_DateDemande.Text = General.getFrmReg(Variable.cUserDocP2, "DateSaisie", "");
                    txtNumFichestandard.Text = sCode;
                    //txtCodeParticulier = GetSetting(cFrNomApp, cUserIntervention, "Copro", "")

                    fc_ChargeImmeuble(txtCodeImmeuble.Text);
                    fc_sauver();
                    //txtDocument = GetSetting(cFrNomApp, cUserIntervention, "Courier", "")
                    //txtCAI_code = GetSetting(cFrNomApp, cUserIntervention, "Operation", "")
                    //txtNoDevis = GetSetting(cFrNomApp, cUserIntervention, "NoDevis", "")

                    fc_InitialiseGrille();
                    fc_loadTreeView(Convert.ToInt32(General.nz(txtCOP_NoAuto.Text, 0)));
                    fc_CptRendu(txtCodeImmeuble.Text);
                    return;
                }

            }
            else if (lAutre != 0)//tested
            {

                General.sSQL = "SELECT * From COP_ContratP2" + " WHERE  COP_NoAuto = " + lAutre;
                using (var tmpAdo = new ModAdo())
                    rstmp = tmpAdo.fc_OpenRecordSet(General.sSQL);

            }
            else
            {
                General.sSQL = "";
                General.sSQL = "SELECT * From COP_ContratP2" + " WHERE  COP_NoContrat = '" + General.nz(sCode, 0) + "' and COP_Avenant =" + General.nz(sAvenant, 0);
                using (var tmpAdo = new ModAdo())
                    rstmp = tmpAdo.fc_OpenRecordSet(General.sSQL);

            }

            if (rstmp.Rows.Count > 0)//tested
            {

                txtCOP_DateDemande.Text = General.nz(rstmp.Rows[0]["COP_DateDemande"], "").ToString();
                txtCOP_DateImpression.Text = General.nz(rstmp.Rows[0]["Cop_DateImpression"], "").ToString();
                txtNumFichestandard.Text = General.nz(rstmp.Rows[0]["Numfichestandard"], "").ToString();
                txtCOP_NoContrat.Text = General.nz(rstmp.Rows[0]["COP_NoContrat"], "").ToString();
                txtCodeImmeuble.Text = General.nz(rstmp.Rows[0]["CodeImmeuble"], "").ToString();
                txtCOP_Commentaire.Text = General.nz(rstmp.Rows[0]["COP_Commentaire"], "").ToString();
                txtNumContratRef.Text = rstmp.Rows[0]["NumContratRef"] + "";
                txtAvtRef.Text = rstmp.Rows[0]["AvenantRef"] + "";
                fc_loadDataCtr(txtNumContratRef.Text, Convert.ToInt32(General.nz(txtAvtRef.Text, 0)));//tested
                txtCOP_Document.Text = General.nz(rstmp.Rows[0]["COP_Document"], "").ToString();

                if (string.IsNullOrEmpty(txtCOP_Document.Text))//tested
                {
                    label7.Font = new Font(label7.Font.Name, label7.Font.Size, FontStyle.Underline);
                    label7.ForeColor = System.Drawing.Color.Black;
                }
                else if (!string.IsNullOrEmpty(txtCOP_Document.Text))
                {
                    label7.Font = new Font(label7.Font.Name, label7.Font.Size, FontStyle.Underline);
                    label7.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cForeColorLien);

                }


                txtCOP_DateSignature.Text = rstmp.Rows[0]["COP_DateSignature"] != DBNull.Value ? Convert.ToDateTime(rstmp.Rows[0]["COP_DateSignature"]).ToShortDateString() : "";
                txtCOP_DateFin.Text = General.nz(rstmp.Rows[0]["COP_dateFin"], "").ToString();
                //===> Mondir le 18.02.2021, commented in V16.02.2021
                //txtCOP_DateAcceptation.Value = General.nz(rstmp.Rows[0]["COP_DateAcceptation"], "").ToString();

                //=== positionne le DTPicker sur la date du jour.

                //if (string.IsNullOrEmpty(General.nz(txtCOP_DateAcceptation.Value, "").ToString()))//tested
                //{
                //    txtCOP_DateAcceptation.Value = DateTime.Today;
                //    txtCOP_DateAcceptation.Value = "";
                //}

                //txtCOP_DateDemandeResiliation.Value = General.nz(rstmp.Rows[0]["COP_DateDemandeResiliation"], "").ToString();

                //=== positionne le DTPicker sur la date du jour.

                //if (string.IsNullOrEmpty(General.nz((txtCOP_DateDemandeResiliation.Value), "").ToString()))//tested
                //{
                //    txtCOP_DateDemandeResiliation.Value = DateTime.Today;
                //    txtCOP_DateDemandeResiliation.Value = "";
                //}
                //===> Fin Modif Mondir

                txtCOP_DateTaciteReconduction.Text = General.nz(rstmp.Rows[0]["COP_DateTaciteReconduction"], "").ToString();
                //===> Mondir le 18.02.2021, commented in V16.02.2021
                //txtCOP_Objet.Text = General.nz(rstmp.Rows[0]["COP_Objet"], "").ToString();
                //===> Fin Modif Mondir
                txtCOP_DateEffet.Text = General.nz(rstmp.Rows[0]["COP_DateEffet"], "").ToString();
                txtCOP_Duree.Text = General.nz(rstmp.Rows[0]["COP_Duree"], "").ToString();
                txtCOP_Statut.Text = General.nz(rstmp.Rows[0]["COP_Statut"], "").ToString();
                if (!string.IsNullOrEmpty(txtCOP_Statut.Text))
                {
                    using (var tmpAdo = new ModAdo())
                        lbllibCOP_Statut.Text = tmpAdo.fc_ADOlibelle("SELECT ETC_Libelle " + " From ETC_Contrat" + " WHERE     (ETC_Code = '" + txtCOP_Statut.Text + "') ");
                }

                txtCOP_Technicien.Text = General.nz(rstmp.Rows[0]["COP_Technicien"], "").ToString();
                txtCOP_NoAuto.Text = General.nz(rstmp.Rows[0]["COP_NoAuto"], "").ToString();
                txtannee.Text = General.nz(rstmp.Rows[0]["Annee"], "").ToString();
                txtMois.Text = General.nz(rstmp.Rows[0]["mois"], "").ToString();
                txtordre.Text = General.nz(rstmp.Rows[0]["ordre"], "").ToString();
                txtCOP_Avenant.Text = General.nz(rstmp.Rows[0]["COP_Avenant"], "").ToString();
                txtCOP_SStraitant.Text = General.nz(rstmp.Rows[0]["COP_SStraitant"], "").ToString();

                //===> Mondir le 18.02.2021, commented in V16.02.2021
                //txtCOP_TotalHT.Text = General.nz(rstmp.Rows[0]["COP_TotalHT"], "").ToString();
                //txtCOP_Coef.Text = General.nz(rstmp.Rows[0]["COP_Coef"], "").ToString();
                //txtCOP_TotalApresCoef.Text = General.nz(rstmp.Rows[0]["COP_TotalApresCoef"], "").ToString();
                //===> Fin Modif Mondir

                txtCOP_MontantGarantie.Text = General.nz(rstmp.Rows[0]["COP_MontantGarantie"], "").ToString();
                txtCOP_NbLogement.Text = General.nz(rstmp.Rows[0]["COP_NbLogement"], "").ToString();

                //===> Mondir le 18.02.2021, commented in V16.02.2021
                //txtCOP_TotalHTgest.Text = General.nz(rstmp.Rows[0]["COP_TotalHTgest"], "").ToString();
                //txtCOP_Coefgest.Text = (rstmp.Rows[0]["COP_Coefgest"] == DBNull.Value || rstmp.Rows[0]["COP_Coefgest"].ToString() == "0" ? 1 : rstmp.Rows[0]["COP_Coefgest"]).ToString();
                //txtCOP_TotalApresCoefgest.Text = General.nz(rstmp.Rows[0]["COP_TotalApresCoefgest"], "").ToString();
                //txtCOP_TotalHTST.Text = General.nz(rstmp.Rows[0]["COP_TotalHTst"], "").ToString();
                //txtCOP_CoefSt.Text = (rstmp.Rows[0]["COP_Coefst"] == DBNull.Value || rstmp.Rows[0]["COP_Coefst"].ToString() == "0" ? 1 : rstmp.Rows[0]["COP_Coefgest"]).ToString();
                //txtCOP_TotalApresCoefSt.Text = General.nz(rstmp.Rows[0]["COP_TotalApresCoefst"], "").ToString();
                //===> Fin Modif Mondir

                txtCOP_DateDemande.Text = General.nz(rstmp.Rows[0]["COP_DateDemande"], "").ToString();
                txtCOP_DateImpression.Text = General.nz(rstmp.Rows[0]["Cop_DateImpression"], "").ToString();
                txtNumFichestandard.Text = General.nz(rstmp.Rows[0]["Numfichestandard"], "").ToString();
                txtCOP_Signataire.Text = General.nz(rstmp.Rows[0]["COP_Signataire"], "").ToString();

                //===> Mondir le 18.02.2021, commented in V16.02.2021
                //txtCOP_SuiviePar.Text = General.nz(rstmp.Rows[0]["COP_SuiviePar"], "").ToString();
                //===> Fin Modif Mondir

                //COP_P1 = IIf(rstmp!COP_P1 = True, 1, 0)
                //COP_P2 = IIf(rstmp!COP_P2 = True, 1, 0)
                //COP_P3 = IIf(rstmp!COP_P3 = True, 1, 0)
                //COP_P4 = IIf(rstmp!COP_P4 = True, 1, 0)



                chkCOP_FIOUL.Checked = ((rstmp.Rows[0]["COP_FiOUL"] == DBNull.Value || rstmp.Rows[0]["COP_FiOUL"] == null) ? 0 : rstmp.Rows[0]["COP_FiOUL"]).ToString() == "1";

                chkCOP_Gaz.Checked = ((rstmp.Rows[0]["COP_Gaz"] == DBNull.Value || rstmp.Rows[0]["COP_Gaz"] == null) ? 0 : rstmp.Rows[0]["COP_Gaz"]).ToString() == "1";

                chkCOP_CPCU.Checked = ((rstmp.Rows[0]["COP_CPCU"] == DBNull.Value || rstmp.Rows[0]["COP_CPCU"] == null) ? 0 : rstmp.Rows[0]["COP_CPCU"]).ToString() == "1";

                chkCOP_VmcDsc.Checked = ((rstmp.Rows[0]["COP_VmcDsc"] == DBNull.Value || rstmp.Rows[0]["COP_VmcDsc"] == null) ? 0 : rstmp.Rows[0]["COP_VmcDsc"]).ToString() == "1";

                chkCOP_ELECT.Checked = ((rstmp.Rows[0]["COP_ELECT"] == DBNull.Value || rstmp.Rows[0]["COP_ELECT"] == null) ? 0 : rstmp.Rows[0]["COP_ELECT"]).ToString() == "1";

                chkCOP_CLIM.Checked = ((rstmp.Rows[0]["COP_CLIM"] == DBNull.Value || rstmp.Rows[0]["COP_CLIM"] == null) ? 0 : rstmp.Rows[0]["COP_CLIM"]).ToString() == "1";

                chkCOP_VMC.Checked = ((rstmp.Rows[0]["COP_VMC"] == DBNull.Value || rstmp.Rows[0]["COP_VMC"] == null) ? 0 : rstmp.Rows[0]["COP_VMC"]).ToString() == "1";

                chkCOP_SURP.Checked = ((rstmp.Rows[0]["COP_SURP"] == DBNull.Value || rstmp.Rows[0]["COP_SURP"] == null) ? 0 : rstmp.Rows[0]["COP_SURP"]).ToString() == "1";

                chkCOP_Murale.Checked = ((rstmp.Rows[0]["COP_Murale"] == DBNull.Value || rstmp.Rows[0]["COP_Murale"] == null) ? 0 : rstmp.Rows[0]["COP_Murale"]).ToString() == "1";

                chkCOP_TE.Checked = ((rstmp.Rows[0]["COP_TE"] == DBNull.Value || rstmp.Rows[0]["COP_TE"] == null) ? 0 : rstmp.Rows[0]["COP_TE"]).ToString() == "1";

                chkCOP_TeleAlarme.Checked = ((rstmp.Rows[0]["COP_TeleAlarme"] == DBNull.Value || rstmp.Rows[0]["COP_TeleAlarme"] == null) ? 0 : rstmp.Rows[0]["COP_TeleAlarme"]).ToString() == "1";

                ChkCOP_EnAttente.Checked = ((rstmp.Rows[0]["COP_EnAttente"] == DBNull.Value || rstmp.Rows[0]["COP_EnAttente"] == null) ? 0 : rstmp.Rows[0]["COP_EnAttente"]).ToString() == "1";

                chkCOP_Disconnecteur.Checked = ((rstmp.Rows[0]["COP_Disconnecteur"] == DBNull.Value || rstmp.Rows[0]["COP_Disconnecteur"] == null) ? 0 : rstmp.Rows[0]["COP_Disconnecteur"]).ToString() == "1";

                ChkCOP_Relevage.Checked = ((rstmp.Rows[0]["COP_Relevage"] == DBNull.Value || rstmp.Rows[0]["COP_Relevage"] == null) ? 0 : rstmp.Rows[0]["COP_Relevage"]).ToString() == "1";

                ChkCOP_CHAUCLIM.Checked = ((rstmp.Rows[0]["COP_CHAUCLIM"] == DBNull.Value || rstmp.Rows[0]["COP_CHAUCLIM"] == null) ? 0 : rstmp.Rows[0]["COP_CHAUCLIM"]).ToString() == "1";

                //ChkCOP_EnAttente = IIf(IsNull(rstmp!COP_EnAttente), 0, rstmp!COP_EnAttente)
                //ChkCOP_Particulier = IIf(IsNull(rstmp!COP_Particulier), 0, rstmp!COP_Particulier)


                ChkCOP_Ecs.Checked = ((rstmp.Rows[0]["cop_ecs"] == DBNull.Value || rstmp.Rows[0]["cop_ecs"] == null) ? 0 : rstmp.Rows[0]["cop_ecs"]).ToString() == "1";
                chk0.Checked = ((rstmp.Rows[0]["COP_RESEAU"] == DBNull.Value || rstmp.Rows[0]["COP_RESEAU"] == null) ? 0 : rstmp.Rows[0]["COP_RESEAU"]).ToString() == "1";

                chk1.Checked = ((rstmp.Rows[0]["COP_BOIS"] == DBNull.Value || rstmp.Rows[0]["COP_BOIS"] == null) ? 0 : rstmp.Rows[0]["COP_BOIS"]).ToString() == "1";

                chk2.Checked = ((rstmp.Rows[0]["COP_CHAUFFAGE"] == DBNull.Value || rstmp.Rows[0]["COP_CHAUFFAGE"] == null) ? 0 : rstmp.Rows[0]["COP_CHAUFFAGE"]).ToString() == "1";

                chk6.Checked = ((rstmp.Rows[0]["COP_SOLAIRE"] == DBNull.Value || rstmp.Rows[0]["COP_SOLAIRE"] == null) ? 0 : rstmp.Rows[0]["COP_SOLAIRE"]).ToString() == "1";
                chk3.Checked = ((rstmp.Rows[0]["COP_RendCEE"] == DBNull.Value || rstmp.Rows[0]["COP_RendCEE"] == null) ? 0 : rstmp.Rows[0]["COP_RendCEE"]).ToString() == "1";
                chk4.Checked = ((rstmp.Rows[0]["COP_SFioul"] == DBNull.Value || rstmp.Rows[0]["COP_SFioul"] == null) ? 0 : rstmp.Rows[0]["COP_SFioul"]).ToString() == "1";

                //    ChkCOP_EcsOui.value = 0
                //    ChkCOP_EcsNon.value = 0
                //    If IsNull(rstmp!COP_Ecs) Then
                //        '==ne fait rien
                //    ElseIf nz(rstmp!COP_Ecs, 0) = 1 Then
                //        ChkCOP_EcsOui.value = 1
                //    ElseIf nz(rstmp!COP_Ecs, 0) = 0 Then
                //        ChkCOP_EcsNon.value = 1
                //
                //    End If

                txtCOP_GroupeNoContrat.Text = rstmp.Rows[0]["COP_GroupeNoContrat"].ToString();
                txtCOP_GroupeAvenant.Text = rstmp.Rows[0]["COP_GroupeAvenant"].ToString();
                txtCOP_GroupeNoAuto.Text = rstmp.Rows[0]["COP_GroupeNoAuto"].ToString();
                txtCOP_Puissance.Text = General.nz(rstmp.Rows[0]["COP_puissance"], "").ToString();

                txtCOP_NbChaudiere.Text = General.nz(rstmp.Rows[0]["COP_NbChaudiere"], "").ToString();

                txtCOP_NbEchangeur.Text = General.nz(rstmp.Rows[0]["COP_NbEchangeur"], "").ToString();
                txtCOP_Unite.Text = General.nz(rstmp.Rows[0]["COP_Unite"], "").ToString();

                fc_ChargeImmeuble(txtCodeImmeuble.Text);    //tested
                fc_CtrSSt(txtCodeImmeuble.Text);    //tested            
                fc_loadTreeView(Convert.ToInt32(General.nz(txtCOP_NoAuto.Text, 0)));
                fc_RecapPrest();//tested
                fc_ChargePrestations();//tested
                fc_CptRendu(txtCodeImmeuble.Text);//tested

            }
            else
            {
                fc_clear();
                fc_ChargePrestations();
            }


        }

        public void fc_AddPDABimm(string sCodeImmeuble, bool bAjout, string sLibelle)
        {
            //==== fonction destiné à gérer les badges, crée au moins une localisation par défaut
            //=== si celle ci n'existe pas. si bAjout est à true on
            DataTable rs = default(DataTable);
            string sSQL = null;
            string stemp = null;


            try
            {
                using (var tmpAdo = new ModAdo())
                {
                    sSQL = "SELECT PDAB_Noauto, PDAB_Libelle, PDAB_NonActif, "
                        + " PDAB_LieuBadge, Codeimmeuble, PDAB_ImageCB, Images, PDAB_IDBarre"
                        + " From PDAB_BadgePDA"
                        + " WHERE     Codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";

                    rs = tmpAdo.fc_OpenRecordSet(sSQL);

                    if (rs.Rows.Count == 0 || bAjout == true)//No Tested
                    {
                        //rs.AddNew();
                        var NewRow = rs.NewRow();
                        NewRow["PDAB_NonActif"] = 0;
                        NewRow["CodeImmeuble"] = sCodeImmeuble;
                        if (bAjout == true && !string.IsNullOrEmpty(sLibelle))
                        {
                            NewRow["PDAB_Libelle"] = sLibelle;
                        }
                        else
                        {
                            NewRow["PDAB_Libelle"] = "Chaufferie";
                        }
                        rs.Rows.Add(NewRow);

                        //===> Mondir le 02.11.2020. https://groupe-dt.mantishub.io/view.php?id=2065
                        var locs = new[] { "Paliers", "Parking", "Caves", "Terrasse" };
                        foreach (var loc in locs)
                        {
                            NewRow = rs.NewRow();
                            NewRow["PDAB_NonActif"] = 0;
                            NewRow["CodeImmeuble"] = sCodeImmeuble;
                            NewRow["PDAB_Libelle"] = loc;
                            rs.Rows.Add(NewRow);
                        }
                        //===> Fin Modif Mondir

                        ///stemp = fc_NumeroBadge(sCodeimmeuble)
                        ///rs!PDAB_IDBarre = stemp

                        tmpAdo.Update();
                        // fc_TreeBadge sCodeImmeuble

                    }
                    //rs.Close();
                    rs = null;
                    return;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModPda;fc_AddPDABimm");
            }
        }


        private void fc_loadTreeViewLOCAL(int lCop_NoAuto)
        {
            int lICC_Noauto = 0;
            int lEQM_NoAuto = 0;
            int i = 0;
            object l = null;
            int lPDAB_Noauto = 0;
            int lGAi_ID = 0;
            UltraTreeNode oNode = null;
            UltraTreeNode TN_T1 = new UltraTreeNode();
            UltraTreeNode TN_T2 = new UltraTreeNode();
            UltraTreeNode TN_T3 = new UltraTreeNode();
            UltraTreeNode TN_T4 = new UltraTreeNode();
            bool bselect = false;
            //Dim nodX                As

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
            try
            {

                //=== modif du 01/12/2015, ajout par defaut d'une localisation si inexistante.
                fc_AddPDABimm(txtCodeImmeuble.Text, false, "");

                //sSQL = "SELECT  COP_ContratP2.COP_NoAuto, ICC_IntervCategorieContrat.CAI_Code," _
                //& " ICC_IntervCategorieContrat.CAI_Libelle, ICC_IntervCategorieContrat.ICC_Noauto, " _
                //& " EQM_EquipementP2Imm.EQM_Code, EQM_EquipementP2Imm.EQM_Libelle," _
                //& " EQM_EquipementP2Imm.EQM_NoAuto,FAP_FacArticleP2.FAP_Designation1," _
                //& " FAP_FacArticleP2.FAP_NoAuto, EQM_EquipementP2Imm.EQU_TypeIntervenant," _
                //& " COP_ContratP2.codeimmeuble, PDAB_BadgePDA.PDAB_Libelle, PDAB_BadgePDA.PDAB_Noauto"

                //sSQL = sSQL & " FROM  FAP_FacArticleP2 RIGHT OUTER JOIN" _
                //& " PDAB_BadgePDA INNER JOIN " _
                //& " COP_ContratP2 ON PDAB_BadgePDA.Codeimmeuble = COP_ContratP2.codeimmeuble LEFT OUTER JOIN" _
                //& " ICC_IntervCategorieContrat ON COP_ContratP2.COP_NoAuto = ICC_IntervCategorieContrat.COP_Noauto AND" _
                //& " PDAB_BadgePDA.PDAB_Noauto = ICC_IntervCategorieContrat.PDAB_Noauto LEFT OUTER JOIN" _
                //& " EQM_EquipementP2Imm ON ICC_IntervCategorieContrat.ICC_Noauto = EQM_EquipementP2Imm.ICC_Noauto ON" _
                //& " FAP_FacArticleP2.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto"

                //sSQL = sSQL & " WHERE (COP_ContratP2.COP_NoAuto =" & lCop_NoAuto & ")" _
                //& " ORDER BY PDAB_BadgePDA.PDAB_Noauto,  ICC_IntervCategorieContrat.ICC_NoLigne, ICC_IntervCategorieContrat.ICC_Noauto," _
                //& " EQM_EquipementP2Imm.EQM_NoLigne,EQM_EquipementP2Imm.EQM_NoAuto," _
                //& " FAP_FacArticleP2.FAP_NoLIgne, FAP_FacArticleP2.FAP_NoAuto"

                //=== modif du 20 01 2015.
                General.sSQL = " SELECT     COP_ContratP2.COP_NoAuto, ICC_IntervCategorieContrat.CAI_Code, ICC_IntervCategorieContrat.CAI_Libelle,"
                    + " ICC_IntervCategorieContrat.ICC_Noauto, " + " EQM_EquipementP2Imm.EQM_Code, EQM_EquipementP2Imm.EQM_Libelle, EQM_EquipementP2Imm.EQM_NoAuto, "
                    + " FAP_FacArticleP2.FAP_Designation1, FAP_FacArticleP2.FAP_NoAuto, EQM_EquipementP2Imm.EQU_TypeIntervenant," + " COP_ContratP2.codeimmeuble, "
                    + " PDAB_BadgePDA.PDAB_Libelle, PDAB_BadgePDA.PDAB_Noauto, GAI_GammeImm.GAM_Code, GAI_GammeImm.GAI_LIbelle, "
                    + " GAI_GammeImm.GAi_ID, EQM_EquipementP2Imm.CTR_Libelle, EQM_EquipementP2Imm.CTR_ID ";
                General.sSQL = General.sSQL + " FROM         FAP_FacArticleP2 RIGHT OUTER JOIN " + " EQM_EquipementP2Imm RIGHT OUTER JOIN "
                    + " ICC_IntervCategorieContrat ON EQM_EquipementP2Imm.ICC_Noauto = ICC_IntervCategorieContrat.ICC_Noauto ON "
                    + " FAP_FacArticleP2.EQM_NoAuto = EQM_EquipementP2Imm.EQM_NoAuto RIGHT OUTER JOIN " + " GAI_GammeImm RIGHT OUTER JOIN "
                    + " PDAB_BadgePDA INNER JOIN " + " COP_ContratP2 ON PDAB_BadgePDA.Codeimmeuble = COP_ContratP2.codeimmeuble ON "
                    + " GAI_GammeImm.COP_Noauto = COP_ContratP2.COP_NoAuto AND GAI_GammeImm.PDAB_Noauto = PDAB_BadgePDA.PDAB_Noauto ON "
                    + " ICC_IntervCategorieContrat.GAi_ID = GAI_GammeImm.GAi_ID";
                General.sSQL = General.sSQL + " WHERE (COP_ContratP2.COP_NoAuto =" + lCop_NoAuto + ")"
                    + " ORDER BY PDAB_BadgePDA.PDAB_Noauto,GAI_GammeImm.GAI_ID,   ICC_IntervCategorieContrat.ICC_NoLigne, ICC_IntervCategorieContrat.ICC_Noauto,"
                    + " EQM_EquipementP2Imm.EQM_NoLigne,EQM_EquipementP2Imm.EQM_NoAuto," + " FAP_FacArticleP2.FAP_NoLIgne, FAP_FacArticleP2.FAP_NoAuto";
                using (var tmpAdo = new ModAdo())
                    rstmp = tmpAdo.fc_OpenRecordSet(General.sSQL);
                var _with49 = rstmp;

                //==reinitialise le treview.
                TreeView1.Nodes.Clear();

                if (rstmp.Rows.Count > 0)
                {
                    lICC_Noauto = 0;
                    foreach (DataRow rstmpRow in rstmp.Rows)
                    {

                        //=== Ajout du premier niveau de hiérarchie des localisations.

                        if (rstmpRow["PDAB_Noauto"] != DBNull.Value && lPDAB_Noauto != Convert.ToInt32(rstmpRow["PDAB_Noauto"]))
                        {
                            TN_T1 = TreeView1.Nodes.Add(cNiv1 + rstmpRow["PDAB_Noauto"], rstmpRow["PDAB_Libelle"] + "");
                            TN_T1.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                            lPDAB_Noauto = Convert.ToInt32(rstmpRow["PDAB_Noauto"]);
                        }

                        //=== modif du 20 janvier 2016 ajout d'un niveau de hiearchie.

                        if (rstmpRow["GAi_ID"] != DBNull.Value)
                        {
                            if (lGAi_ID != Convert.ToInt32(rstmpRow["GAi_ID"]))
                            {
                                TN_T2 = TN_T1.Nodes.Add(cNiv2 + rstmpRow["GAi_ID"], rstmpRow["GAI_LIbelle"] + "");
                                TN_T2.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                                lGAi_ID = Convert.ToInt32(rstmpRow["GAi_ID"]);
                            }

                        }

                        //==Ajout du 2 eme niveau de hiérarchie des MOTIFS.
                        if (rstmpRow["ICC_Noauto"] != DBNull.Value)
                        {

                            if (lICC_Noauto != Convert.ToInt32(rstmpRow["ICC_Noauto"]))
                            {
                                //TreeView1.Nodes.Add cNiv1 & !PDAB_Noauto, tvwChild, cNiv2 & !ICC_Noauto, !CAI_Libelle & "", 1, 6

                                TN_T3 = TN_T2.Nodes.Add(cNiv3 + rstmpRow["ICC_Noauto"], rstmpRow["CAI_Libelle"] + "");
                                TN_T3.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                                lICC_Noauto = Convert.ToInt32(rstmpRow["ICC_Noauto"]);
                            }

                        }

                        //==Ajout du 3 eme niveau de hiérarchie des prestations.

                        if (rstmpRow["EQM_NoAuto"] != DBNull.Value)
                        {

                            if (lEQM_NoAuto != Convert.ToInt32(rstmpRow["EQM_NoAuto"]))
                            {
                                //TreeView1.Nodes.Add cNiv2 & !ICC_Noauto, tvwChild, cNiv3 & !EQM_NoAuto, nz(!EQM_Code, ""), 2, 4
                                //TreeView1.Nodes.Add cNiv3 & !ICC_Noauto, tvwChild, cNiv4 & !EQM_NoAuto, nz(!EQM_Code, ""), 2, 4


                                TN_T4 = TN_T3.Nodes.Add(cNiv4 + rstmpRow["EQM_NoAuto"], General.nz(rstmpRow["CTR_Libelle"], "").ToString());
                                TN_T4.Override.NodeAppearance.Image = Properties.Resources.folder_opened16;
                                lEQM_NoAuto = Convert.ToInt32(rstmpRow["EQM_NoAuto"]);
                                if (rstmpRow["EQU_TypeIntervenant"] + "" == "ST")
                                {
                                    ///'' chkSous_Traitant.value = vbChecked
                                }
                                else if (rstmpRow["EQU_TypeIntervenant"] + "" == "RAM")
                                {
                                    ///'' chkRamoneur.value = vbChecked
                                }
                            }

                            //==Ajout du 4 eme niveau de hiérarchie des détails des prestations.

                            if (rstmpRow["FAP_NoAuto"] != DBNull.Value)
                            {
                                //     TreeView1.Nodes.Add cNiv2 & !EQM_NoAuto, tvwChild, cNiv3 & !FAP_NoAuto, nz(!FAP_Designation1, ""), 3, 5
                            }

                        }

                    }
                }
                //_with49.Close();

                rstmp = null;

                bselect = false;

                foreach (UltraTreeNode Node in TreeView1.Nodes)
                {
                    if (Node.Selected == true)
                    {
                        bselect = true;
                    }
                }

                if (bselect == false)
                {
                    SSTab4.Visible = false;
                }


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Loadtreeview;");
            }
        }
        private void fc_loadTreeView(int lCop_NoAuto)
        {
            int lICC_Noauto = 0;
            int lEQM_NoAuto = 0;
            int i = 0;
            System.Windows.Forms.TreeNode oNode = null;
            bool bselect = false;
            //Dim nodX                As

            try
            {

                if (General.sPriseEnCompteLocalP2 == "1")
                {
                    //=== version avec prise en compte de la localisation
                    fc_loadTreeViewLOCAL(lCop_NoAuto);//tested
                    return;
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
                //    chkSous_Traitant.value = vbUnchecked
                //    chkRamoneur.value = vbUnchecked

                General.sSQL = "SELECT  COP_ContratP2.COP_NoAuto, ICC_IntervCategorieContrat.CAI_Code, "
                        + " ICC_IntervCategorieContrat.CAI_Libelle, ICC_IntervCategorieContrat.ICC_Noauto, "
                        + " EQM_EquipementP2Imm.EQM_Code, EQM_EquipementP2Imm.EQM_Libelle, EQM_EquipementP2Imm.EQM_NoAuto,"
                        + " FAP_FacArticleP2.FAP_Designation1,";
                General.sSQL = General.sSQL + " FAP_FacArticleP2.FAP_NoAuto, EQM_EquipementP2Imm.EQU_TypeIntervenant"
                        + " FROM COP_ContratP2 LEFT OUTER JOIN " + " ICC_IntervCategorieContrat "
                        + " ON COP_ContratP2.COP_NoAuto = ICC_IntervCategorieContrat.COP_Noauto"
                        + " LEFT OUTER JOIN" + " EQM_EquipementP2Imm ON ICC_IntervCategorieContrat.ICC_Noauto = EQM_EquipementP2Imm.ICC_Noauto"
                        + " LEFT OUTER JOIN" + " FAP_FacArticleP2 ON EQM_EquipementP2Imm.EQM_NoAuto = FAP_FacArticleP2.EQM_NoAuto";
                General.sSQL = General.sSQL + " WHERE (COP_ContratP2.COP_NoAuto =" + lCop_NoAuto + ")" + " "
                        + " ORDER BY ICC_IntervCategorieContrat.ICC_NoLigne, ICC_IntervCategorieContrat.ICC_Noauto,"
                        + " EQM_EquipementP2Imm.EQM_NoLigne,EQM_EquipementP2Imm.EQM_NoAuto,"
                        + " FAP_FacArticleP2.FAP_NoLIgne, FAP_FacArticleP2.FAP_NoAuto";

                using (var tmpAdo = new ModAdo())
                    rstmp = tmpAdo.fc_OpenRecordSet(General.sSQL);
                var _with50 = rstmp;

                //==reinitialise le treview.
                TreeView1.Nodes.Clear();

                UltraTreeNode TN_T1 = new UltraTreeNode();
                UltraTreeNode TN_T2 = new UltraTreeNode();

                if (rstmp.Rows.Count > 0)
                {
                    lICC_Noauto = 0;
                    foreach (DataRow rstmpRow in rstmp.Rows)
                    {

                        //==Ajout du 1er niveau de hiérarchie.
                        if (rstmpRow["ICC_Noauto"] != DBNull.Value && lICC_Noauto != Convert.ToInt32(rstmpRow["ICC_Noauto"]))
                        {
                            TN_T1 = TreeView1.Nodes.Add(cNiv1 + rstmpRow["ICC_Noauto"], rstmpRow["CAI_Libelle"] + "");
                            lICC_Noauto = Convert.ToInt32(rstmpRow["ICC_Noauto"]);
                        }


                        //==Ajout du 2eme niveau de hiérarchie.

                        if (rstmpRow["EQM_NoAuto"] != DBNull.Value)
                        {

                            if (lEQM_NoAuto != Convert.ToInt32(rstmpRow["EQM_NoAuto"]))
                            {

                                TN_T2 = TN_T1.Nodes.Add(cNiv2 + rstmpRow["EQM_NoAuto"], General.nz(rstmpRow["EQM_CODE"], "").ToString());
                                lEQM_NoAuto = Convert.ToInt32(rstmpRow["EQM_NoAuto"]);
                                if (rstmpRow["EQU_TypeIntervenant"] + "" == "ST")
                                {
                                    ///'' chkSous_Traitant.value = vbChecked
                                }
                                else if (rstmpRow["EQU_TypeIntervenant"] + "" == "RAM")
                                {
                                    ///'' chkRamoneur.value = vbChecked
                                }
                            }

                            //==Ajout du 3eme niveau de hiérarchie.

                            if (rstmpRow["FAP_NoAuto"] != DBNull.Value)
                            {
                                //     TreeView1.Nodes.Add cNiv2 & !EQM_NoAuto, tvwChild, cNiv3 & !FAP_NoAuto, nz(!FAP_Designation1, ""), 3, 5
                            }

                        }

                    }
                }
                //_with50.Close();

                rstmp = null;
                bselect = false;

                foreach (UltraTreeNode Node in TreeView1.Nodes)
                {
                    if (Node.Selected == true)
                    {
                        bselect = true;
                    }
                }
                if (bselect == false)
                {
                    SSTab4.Visible = false;
                }


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Loadtreeview;");
            }
        }

        private void fc_ChargeImmeuble(string sCode)
        {

            General.sSQL = "";
            General.sSQL = "select * from immeuble where CodeImmeuble='" + sCode + "'";
            using (var tmpAdo = new ModAdo())
            {
                rstmp = tmpAdo.fc_OpenRecordSet(General.sSQL);

                if (rstmp.Rows.Count > 0)
                {
                    // me.txtSelCodeImmeuble = nz(rsTmp!CodeImmeuble, "")

                    this.txtAdresse.Text = General.nz(rstmp.Rows[0]["Adresse"], "").ToString();
                    this.txtAdresse2_IMM.Text = General.nz(rstmp.Rows[0]["Adresse2_Imm"], "").ToString();
                    this.txtAngleRue.Text = General.nz(rstmp.Rows[0]["AngleRue"], "").ToString();
                    this.txtCode1.Text = General.nz(rstmp.Rows[0]["Code1"], "").ToString();
                    this.txtCodeImmeuble.Text = General.nz(rstmp.Rows[0]["CodeImmeuble"], "").ToString();
                    this.txtCodePostal.Text = General.nz(rstmp.Rows[0]["CodePostal"], "").ToString();
                    this.txtVille.Text = General.nz(rstmp.Rows[0]["Ville"], "").ToString();
                    //me.txtVilleFact_IMM = nz(rsTmp!VilleFact_IMM, "")
                    this.txtCodeChefSecteur.Text = General.nz(rstmp.Rows[0]["CodeChefSecteur"], "").ToString();
                    if (!string.IsNullOrEmpty(txtCodeChefSecteur.Text))//tested
                    {
                        this.lbllibCodeChefSecteur.Text = tmpAdo.fc_ADOlibelle("select nom from personnel where matricule ='" + txtCodeChefSecteur.Text + "'");
                    }

                    this.txtSEC_Code.Text = General.nz(rstmp.Rows[0]["SEC_Code"], "").ToString();

                    FC_intervenant();//tested

                    if (!string.IsNullOrEmpty(txtCOP_Technicien.Text))//tested
                    {
                        this.lbllibIntervenant.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom,Personnel.prenom " + " From Personnel" + " WHERE Personnel.Matricule ='" + txtCOP_Technicien.Text + "'", true);

                    }
                    else
                    {
                        this.lbllibIntervenant.Text = "";
                    }

                    if (!string.IsNullOrEmpty(txtCOP_SStraitant.Text))
                    {
                        this.lblibCOP_SStraitant.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom,Personnel.prenom " + " From Personnel" + " WHERE Personnel.Matricule ='" + txtCOP_SStraitant.Text + "'", true);

                    }
                    else//tested
                    {
                        this.lblibCOP_SStraitant.Text = "";
                    }

                    if (!string.IsNullOrEmpty(txtCOP_Signataire.Text))
                    {
                        lbllibCOP_Signataire.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom,Personnel.prenom " + " From Personnel" + " WHERE Personnel.Matricule ='" + txtCOP_Signataire.Text + "'", true);
                    }
                    else
                    {
                        lbllibCOP_Signataire.Text = "";
                    }

                    //===> Mondir le 18.02.2021 comnted in V16.02.2021
                    //if (!string.IsNullOrEmpty(txtCOP_SuiviePar.Text))
                    //{
                    // lblCOP_SuiviePar = fc_ADOlibelle("SELECT Personnel.Nom,Personnel.prenom " _
                    //& " From Personnel" _
                    //& " WHERE Personnel.Matricule ='" & txtCOP_SuiviePar & "'", True)
                    //}
                    //else
                    //{
                    //  lblCOP_SuiviePar = ""
                    //}
                    //===> Fin Modif Mondir

                    if (rstmp.Rows[0]["P1"] != DBNull.Value && Convert.ToInt32(rstmp.Rows[0]["P1"]) == 1)
                    {
                        COP_P1.Checked = true;
                    }
                    else
                    {
                        COP_P1.Checked = false;
                    }
                    if (rstmp.Rows[0]["P2"] != DBNull.Value && Convert.ToInt32(rstmp.Rows[0]["P2"]) == 1)
                    {
                        COP_P2.Checked = true;
                    }
                    else
                    {
                        COP_P2.Checked = false;
                    }
                    if (rstmp.Rows[0]["P3"] != DBNull.Value && Convert.ToInt32(rstmp.Rows[0]["P3"]) == 1)
                    {
                        COP_P3.Checked = true;
                    }
                    else
                    {
                        COP_P3.Checked = false;
                    }
                    if (rstmp.Rows[0]["P4"] != DBNull.Value && Convert.ToInt32(rstmp.Rows[0]["P4"]) == 1)
                    {
                        COP_P4.Checked = true;
                    }
                    else
                    {
                        COP_P4.Checked = false;
                    }


                    /* COP_P1.Checked = (rstmp.Rows[0]["P1"].ToString() == "1" || rstmp.Rows[0]["P1"].ToString() == " 0").ToString() == "True";
                     COP_P2.Checked = (rstmp.Rows[0]["P2"].ToString() == "1" || rstmp.Rows[0]["P2"].ToString() == " 0").ToString() == "True";
                     COP_P3.Checked = (rstmp.Rows[0]["P3"].ToString() == "1" || rstmp.Rows[0]["P3"].ToString() == " 0").ToString() == "True";
                     COP_P4.Checked = (rstmp.Rows[0]["P4"].ToString() == "1" || rstmp.Rows[0]["P4"].ToString() == " 0").ToString() == "True";*/

                    ContratResiliee(sCode);//tested
                                           // fc_ICA_ImmCategorieInterv
                                           // fc_GammePlanning

                    fc_InitialiseGrille();//tested
                    fc_recupTVA();
                }
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        private void ContratResiliee(string sCodeImmeuble)
        {
            DataTable adotemp = default(DataTable);
            string LenNumContrat = null;
            DataTable rsContratResilie = default(DataTable);

            if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
            {
                adotemp = new DataTable();
                Text3.Text = "";
                var _with51 = adotemp;
                using (var tmpAdo = new ModAdo())
                    adotemp = tmpAdo.fc_OpenRecordSet("SELECT Contrat.Avenant,contrat.NumContrat,"
                        + " FacArticle.CodeArticle ,Facarticle.Designation1," + " contrat.resiliee"
                        + " FROM FacArticle INNER JOIN Contrat ON FacArticle.CodeArticle = Contrat.CodeArticle"
                        + " Where  Contrat.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'"
                        + " ORDER BY Contrat.Resiliee DESC , Contrat.NumContrat DESC ," + " Contrat.Avenant ");

                if (adotemp.Rows.Count > 0)
                {
                    rsContratResilie = new DataTable();
                    General.sSQL = "SELECT Contrat.NumContrat,FacArticle.Designation1  "
                        + "FROM Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle "
                        + " WHERE (Contrat.Resiliee=0 OR (Contrat.Resiliee=1 AND (Contrat.DateFin>'"
                        + DateTime.Today + "' OR Contrat.DateFin IS NULL))) AND Contrat.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                    using (var tmpAdo = new ModAdo())
                        rsContratResilie = tmpAdo.fc_OpenRecordSet(General.sSQL);
                    if (rsContratResilie.Rows.Count > 0)
                    {
                        //  lblContrat.Visible = False
                    }
                    else
                    {
                        //  lblContrat.Visible = True
                    }
                    //   rsContratResilie.Dispose();
                    rsContratResilie = null;
                    foreach (DataRow adotmpRow in adotemp.Rows)
                    {
                        Text3.Text = Text3.Text + adotmpRow["NumContrat"] + "  " + adotmpRow["avenant"] + "  " + adotmpRow["Designation1"] + "\n";
                        //adotemp.MoveNext();
                    }
                }
                else
                {
                    //  lblContrat.Visible = False
                }
                adotemp.Dispose();
                adotemp = null;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        private void FC_intervenant()
        {
            string sSQL = null;
            DataTable rstmp = default(DataTable);

            //=== modif du 3 008 2010 ajout de la catégorie de l'intervenant.
            //sSQL = "SELECT  " _
            //& " IntervenantImm_INM.intervenant_INM " _
            //& " FROM IntervenantImm_INM " _
            //& " where Codeimmeuble_IMM='" & gFr_DoublerQuote(txtCodeimmeuble) & "'" _
            //& " AND CATI_CODE ='" & cIntervEntretien & "'" _
            //& " order by Nordre_INM"

            sSQL = "SELECT     CodeDepanneur" + " From Immeuble " + " WHERE CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
            using (var tmpAdo = new ModAdo())
                rstmp = tmpAdo.fc_OpenRecordSet(sSQL);
            if (rstmp.Rows.Count > 0)
            {
                txtCOP_Technicien.Text = General.nz(rstmp.Rows[0]["CodeDepanneur"], "").ToString();
            }

        }
        private void fc_clear()
        {

            txtNumFichestandard.Text = "";

            //===> Mondir le 18.02.2021 comnted in V16.02.2021
            //txtCOP_TotalHT.Text = "";
            //txtCOP_Coef.Text = "";
            //txtCOP_TotalApresCoef.Text = "";
            //txtCOP_TotalHTgest.Text = "";
            //txtCOP_Coefgest.Text = "1";
            //txtCOP_TotalApresCoefgest.Text = "";
            //txtCOP_TotalHTST.Text = "";
            //txtCOP_CoefSt.Text = "1";
            //txtCOP_TotalApresCoefSt.Text = "";
            //===> Fin Modif Mondir

            txtCOP_DateDemande.Text = "";
            txtCOP_DateImpression.Text = "";

            COP_P1.CheckState = System.Windows.Forms.CheckState.Unchecked;
            COP_P2.CheckState = System.Windows.Forms.CheckState.Unchecked;
            COP_P3.CheckState = System.Windows.Forms.CheckState.Unchecked;
            COP_P4.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkCOP_FIOUL.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkCOP_Gaz.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkCOP_CPCU.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkCOP_VmcDsc.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkCOP_ELECT.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkCOP_CLIM.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkCOP_VMC.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkCOP_SURP.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkCOP_Murale.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkCOP_TE.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkCOP_TeleAlarme.CheckState = System.Windows.Forms.CheckState.Unchecked;

            //ChkCOP_EnAttente = 0
            //ChkCOP_Particulier = 0
            //ChkCOP_EcsOui = 0
            //ChkCOP_EcsNon = 0
            ChkCOP_Ecs.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chk0.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chk1.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chk2.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chk6.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chk3.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chk4.CheckState = System.Windows.Forms.CheckState.Unchecked;

            chkCOP_Disconnecteur.CheckState = System.Windows.Forms.CheckState.Unchecked;
            ChkCOP_Relevage.CheckState = System.Windows.Forms.CheckState.Unchecked;
            ChkCOP_CHAUCLIM.CheckState = System.Windows.Forms.CheckState.Unchecked;


            txtCOP_Puissance.Text = "";
            txtCOP_NbChaudiere.Text = "";
            txtCOP_NbEchangeur.Text = "";
            txtCOP_Unite.Text = "";

            txtCOP_NoContrat.Text = "";
            txtCodeImmeuble.Text = "";
            txtCOP_Commentaire.Text = "";
            txtNumContratRef.Text = "";
            txtAvtRef.Text = "";
            txtCOP_Document.Text = "";

            txtCOP_DateFin.Text = "";

            //===> Mondir le 18.02.2021 comnted in V16.02.2021
            //txtCOP_DateAcceptation.Value = "";
            //===> Fin Modif Mondir

            txtCOP_DateSignature.Text = "";

            //===> Mondir le 18.02.2021 comnted in V16.02.2021
            txtCOP_DateDemandeResiliation.Value = "";
            //===> Fin Modif Mondir

            txtCOP_DateTaciteReconduction.Text = "";
            txtCOP_Objet.Text = "";
            txtCOP_DateEffet.Text = "";
            txtCOP_Duree.Text = "";

            txtCOP_Statut.Text = "";

            txtCOP_NoAuto.Text = "";
            txtannee.Text = "";
            txtMois.Text = "";
            txtordre.Text = "";
            this.txtSEC_Code.Text = "";
            this.lbllibCodeChefSecteur.Text = "";
            this.txtCodeChefSecteur.Text = "";
            // me.txtSelCodeImmeuble = ""
            this.txtAdresse.Text = "";
            this.txtAdresse2_IMM.Text = "";
            this.txtAngleRue.Text = "";
            this.txtCode1.Text = "";
            this.txtCodeImmeuble.Text = "";
            this.txtCodePostal.Text = "";
            this.txtVille.Text = "";
            // me.txtVilleFact_IMM = ""
            // me.txtSituation = ""
            this.txtCOP_Signataire.Text = "";
            lbllibCOP_Signataire.Text = "";
            //lblCOP_SuiviePar = ""

            //===> Mondir le 18.02.2021 comnted in V16.02.2021
            this.txtCOP_SuiviePar.Text = "";
            //===> Fin Modif Mondir

            txtCOP_GroupeNoContrat.Text = "";
            txtCOP_GroupeAvenant.Text = "";
            txtCOP_GroupeNoAuto.Text = "";
            label7.ForeColor = System.Drawing.Color.Black;
            label7.Font = new Font(label7.Font.Name, label7.Font.Size, FontStyle.Underline);

        }


        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_ssDropTPP_TypePeriodeP2()
        {
            General.sSQL = "";
            General.sSQL = "SELECT  TPP_Code, TPP_JD, TPP_MD, TPP_JF, TPP_MF, SEM_Code" + " From TPP_TypePeriodeP2" + " ORDER by TPP_Noligne";

            if (ssDropTPP_TypePeriodeP2.Rows.Count == 0)
            {
                sheridan.InitialiseCombo((this.ssDropTPP_TypePeriodeP2), General.sSQL, "TPP_Code", true);
                ssDropTPP_TypePeriodeP2.DisplayLayout.Bands[0].Columns[0].Width = 100;
                ssDropTPP_TypePeriodeP2.DisplayLayout.Bands[0].Columns[1].Width = 50;
                ssDropTPP_TypePeriodeP2.DisplayLayout.Bands[0].Columns[2].Width = 50;
                ssDropTPP_TypePeriodeP2.DisplayLayout.Bands[0].Columns[3].Width = 50;
                ssDropTPP_TypePeriodeP2.DisplayLayout.Bands[0].Columns[4].Width = 50;

            }


        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_DropJour()
        {

            string[] tabJour = null;
            int i = 0;
            tabJour = new string[31];
            for (i = 0; i <= 30; i++)
            {
                tabJour[i] = Convert.ToString(i + 1);
            }

            sheridan.AlimenteCmbAdditemTableau(ssDropJour, tabJour, "");


        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_DropSemaine()
        {
            string[] tsSem = new string[8];
            int i = 0;

            tsSem[0] = "";
            tsSem[1] = modP2.cSDimanche;
            tsSem[2] = modP2.cSLundi;
            tsSem[3] = modP2.cSMardi;
            tsSem[4] = modP2.cSMercredi;
            tsSem[5] = modP2.cSJeudi;
            tsSem[6] = modP2.cSVendredi;
            tsSem[7] = modP2.cSSamedi;

            //-- dimanche est le premier jour de la semaine dans la fonction weekday par défaut.

            sheridan.AlimenteCmbAdditemTableau(ssDropSemaine, tsSem, "");
            //ssDropSemaine.DisplayLayout.Bands[0].Columns[0].Header.Caption = "Jours";TODO
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_DropMois()
        {
            string[] tabJour = null;
            int i = 0;
            tabJour = new string[12];
            for (i = 0; i <= 11; i++)
            {
                tabJour[i] = Convert.ToString(i + 1);
            }

            sheridan.AlimenteCmbAdditemTableau(ssDropMois, tabJour, "");

        }

        private void fc_Ajouter()
        {
            var _with52 = this;
            if (blModif == true)
            {
                DialogResult dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous enregistrer vos modifications ?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                switch (dg)
                {
                    case DialogResult.Yes:
                        fc_sauver();
                        break;

                    case DialogResult.No:
                        break;
                    // Vider devis en cours
                    default:
                        return;

                        //Reprendre saisie en cours
                        break;
                }
            }
            fc_InitialiseGrille();
            fc_clear();
            fc_BloqueForm("Ajouter");
            //==reinitialise le treview.
            TreeView1.Nodes.Clear();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sCommande"></param>
        private void fc_BloqueForm(string sCommande = "")
        {

            try
            {
                var _with53 = this;
                _with53.SSTab1.SelectedTab = SSTab1.Tabs[0];
                _with53.SSTab1.Enabled = false;
                _with53.cmdSupprimer.Enabled = false;
                // mode ajout
                if (sCommande.ToUpper() == "Ajouter".ToUpper())
                {
                    _with53.cmdRechercheImmeuble.Enabled = true;
                    _with53.cmdAjouter.Enabled = false;
                    _with53.CmdRechercher.Enabled = false;
                    _with53.CmdSauver.Enabled = true;
                    //active le mode modeAjout
                    blnAjout = true;
                }
                else
                {
                    _with53.SSTab1.Enabled = true;
                    _with53.cmdAjouter.Enabled = true;
                    // .cmdRechercheClient.Enabled = True
                    _with53.cmdRechercheImmeuble.Enabled = false;
                    // .cmdRechercheRaisonSocial.Enabled = True
                    _with53.CmdRechercher.Enabled = true;
                    _with53.CmdSauver.Enabled = true;
                    //desactive le mode modeAjout
                    blnAjout = false;
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_BloqueForm");
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sCommande"></param>
        private void fc_DeBloqueForm(string sCommande = "")
        {


            try
            {
                var _with54 = this;
                //.SSTab1.Tab = 0
                _with54.SSTab1.Enabled = true;
                _with54.cmdAjouter.Enabled = true;
                _with54.CmdRechercher.Enabled = true;
                _with54.cmdSupprimer.Enabled = true;
                _with54.CmdSauver.Enabled = true;
                _with54.cmdRechercheImmeuble.Enabled = false;
                //desactive le mode modeAjout
                blnAjout = false;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_DeBloqueForm");
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        private void fc_InitialiseGrille()
        {
            //fc_ICA_ImmCategorieInterv
            //fc_EQM_EquipementP2Imm
            //fc_GammePlanning
            //fc_FAP_FacArticleP2
            //fc_PEI_PeriodeGammeImm


            fc_DetailPrestation(Convert.ToInt32(General.nz(txtCOP_NoAuto.Text, 0)));//tested
            fc_ChargePrestations();//tested

        }

        private void txtCOP_Unite_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = "";
            sSQL = "SELECT  FacArticle.CodeArticle, " + " FacArticle.Designation1," + " CAI_CategoriInterv.CAI_Noauto "
                + " FROM         FacArticle" + " INNER JOIN CAI_CategoriInterv " + " ON FacArticle.CAI_Noauto = CAI_CategoriInterv.CAI_Noauto "
                + " INNER JOIN TYM_TypeDeMotif " + " ON CAI_CategoriInterv.TYM_CODE = TYM_TypeDeMotif.TYM_CODE"
                + " WHERE     (TYM_TypeDeMotif.TYM_CODE = '" + StdSQLchaine.gFr_DoublerQuote(modP2.cCPREST) + "')";

            sheridan.InitialiseCombo((this.ssDropFacArticle), sSQL, "CodeArticle", true);
            this.ssCOD_ContratP2Detail.DisplayLayout.Bands[0].Columns["CodeArticle"].ValueList = this.ssDropFacArticle;
        }

        /// <summary>
        /// tested
        /// </summary>
        private void fc_dropArticle()
        {
            string sSQL = null;
            sSQL = "SELECT  FacArticle.CodeArticle, " + " FacArticle.Designation1," + " CAI_CategoriInterv.CAI_Noauto "
                + " FROM         FacArticle" + " INNER JOIN CAI_CategoriInterv " + " ON FacArticle.CAI_Noauto = CAI_CategoriInterv.CAI_Noauto "
                + " INNER JOIN TYM_TypeDeMotif " + " ON CAI_CategoriInterv.TYM_CODE = TYM_TypeDeMotif.TYM_CODE"
                + " WHERE     (TYM_TypeDeMotif.TYM_CODE = '" + StdSQLchaine.gFr_DoublerQuote(modP2.cCPREST) + "')";
            sheridan.InitialiseCombo((this.ssDropFacArticle), sSQL, "CodeArticle", true);
            // this.ssCOD_ContratP2Detail.DisplayLayout.Bands[0].Columns["CodeArticle"].ValueList= this.ssDropFacArticle;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lCop_NoAuto"></param>
        private void fc_DetailPrestation(int lCop_NoAuto)
        {
            //=== court-circuite la fonction le 01/12/2015 (voir sil faut la réactiver).
            return;


            /*  rsCOD = new DataTable();
             General.sSQL = "SELECT     COD_cleauto, COP_NoAuto, COP_NoContrat, COP_Avenant, " 
                 + " CodeImmeuble, CodeArticle,'' as libelle,"
                 + " COP_ToatlHT, COP_P1, COP_P2, COP_P3, COP_P4" 
                 + " FROM         COD_ContratP2Detail"
                 + " WHERE COP_Noauto =" + lCop_NoAuto;
             using (var tmpAdo = new ModAdo())          
             rsCOD = tmpAdo.fc_OpenRecordSet(General.sSQL);
             ssCOD_ContratP2Detail.DataSource=rsCOD;*/

        }

        /// <summary>
        /// tested
        /// </summary>
        private void fc_DroitContratP2()
        {
            string sAut = null;

            //==== court circuite la fonction (dédié à Gesten) le 02/12/2015.
            return;


            using (var tmpAdo = new ModAdo())
                sAut = tmpAdo.fc_ADOlibelle("SELECT AUT_Nom "
                                           + " From AUT_Autorisation"
                                           + " WHERE (AUT_Formulaire = '" + Variable.cUserDocP2 + "') "
                                           + " AND (AUT_Nom = '" + General.fncUserName() + "')");

            //=== bloque systématiquement le champs documents.
            txtCOP_Document.ReadOnly = true;
            txtCOP_Document.ForeColor = System.Drawing.Color.Blue;


            //=== modif du 15 02 2016, Aucun droit n'est gérée chez Delostal.
            return;


            if (General.sDroit.ToUpper().ToString() == General.cAdmin.ToUpper().ToString())
                return;

            if (!string.IsNullOrEmpty(sAut))
                return;

            if (string.IsNullOrEmpty(sAut))
            {


                txtAdresse.ReadOnly = true;
                txtAdresse.ForeColor = System.Drawing.Color.Blue;

                txtAdresse2_IMM.ReadOnly = true;
                txtAdresse2_IMM.ForeColor = System.Drawing.Color.Blue;

                txtAngleRue.ReadOnly = true;
                txtAngleRue.ForeColor = System.Drawing.Color.Blue;

                txtCodePostal.ReadOnly = true;
                txtCodePostal.ForeColor = System.Drawing.Color.Blue;

                txtVille.ReadOnly = true;
                txtVille.ForeColor = System.Drawing.Color.Blue;

                txtCode1.ReadOnly = true;
                txtCode1.ForeColor = System.Drawing.Color.Blue;

                txtCOP_Puissance.ReadOnly = true;
                txtCOP_Puissance.ForeColor = System.Drawing.Color.Blue;

                txtCOP_NbChaudiere.ReadOnly = true;
                txtCOP_NbChaudiere.ForeColor = System.Drawing.Color.Blue;

                txtCOP_NbEchangeur.ReadOnly = true;
                txtCOP_NbEchangeur.ForeColor = System.Drawing.Color.Blue;


                txtCOP_Unite.Enabled = false;
                txtCOP_Unite.ForeColor = System.Drawing.Color.Blue;

                txtCOP_MontantGarantie.ReadOnly = true;
                txtCOP_MontantGarantie.ForeColor = System.Drawing.Color.Blue;

                txtCodeChefSecteur.ReadOnly = true;
                txtCodeChefSecteur.ForeColor = System.Drawing.Color.Blue;

                txtSEC_Code.ReadOnly = true;
                txtSEC_Code.ForeColor = System.Drawing.Color.Blue;


                //txtCOP_Objet.ReadOnly = true;
                //txtCOP_Objet.ForeColor = System.Drawing.Color.Blue;

                //txtCOP_Document.Locked = True
                //txtCOP_Document.FORECOLOR = vbBlue

                txtCOP_DateDemande.ReadOnly = true;
                txtCOP_DateDemande.ForeColor = System.Drawing.Color.Blue;

                txtCOP_DateImpression.ReadOnly = true;
                txtCOP_DateImpression.ForeColor = System.Drawing.Color.Blue;

                //txtCOP_DateEffet.ReadOnly = true;
                //txtCOP_DateEffet.ForeColor = System.Drawing.Color.Blue;

                txtCOP_SuiviePar.ReadOnly = true;
                txtCOP_SuiviePar.ForeColor = System.Drawing.Color.Blue;

                txtCOP_Signataire.ReadOnly = true;
                txtCOP_Signataire.ForeColor = System.Drawing.Color.Blue;

                txtCOP_Statut.ReadOnly = true;
                txtCOP_Statut.ForeColor = System.Drawing.Color.Blue;

                txtCOP_Technicien.ReadOnly = true;
                txtCOP_Technicien.ForeColor = System.Drawing.Color.Blue;

                txtCOP_SStraitant.ReadOnly = true;
                txtCOP_SStraitant.ForeColor = System.Drawing.Color.Blue;

                txtCOP_Commentaire.ReadOnly = true;
                txtCOP_Commentaire.ForeColor = System.Drawing.Color.Blue;

                //txtCOP_DateAcceptation.Enabled = false;
                //txtCOP_DateAcceptation.

                //txtCOP_DateDemandeResiliation.Enabled = false;

                txtCOP_DateSignature.ReadOnly = true;
                txtCOP_DateSignature.ForeColor = System.Drawing.Color.Blue;

                //== bouton
                Command1.Enabled = false;
                //cmdFindSuiviePar.Enabled = False
                cmdFindSignataire.Enabled = false;
                cmdRechercheCodetat.Enabled = false;
                cmdIntervenant.Enabled = false;
                cmdFindStraitant.Enabled = false;


                cmdRechercheChefSecteur.Enabled = false;

                //== case à cocher

                COP_P1.Enabled = false;
                COP_P2.Enabled = false;
                COP_P3.Enabled = false;
                COP_P4.Enabled = false;

            }

        }

        /// <summary>
        /// tested
        /// </summary>
        private void fc_ChargePrestations()
        {

            if ((rsPrestations != null))
            {
                if (General.adocnn.State == ConnectionState.Open)
                {
                    rsPrestations.Dispose();
                }
            }
            else
            {
                rsPrestations = new DataTable();
            }
            using (var tmpAdo = new ModAdo())
                rsPrestations = tmpAdo.fc_OpenRecordSet("SELECT CleContrat, CodePrestation, Designation, P2, P3, Nombre, Commentaire "
                    + " FROM GMAO_Prestations WHERE CleContrat=" + General.nz(txtCOP_NoAuto.Text, "0") + " ORDER BY CodePrestation");
            GridPrestations.DataSource = rsPrestations;
            GridPrestations.UpdateData();
        }

        private void cmbDrpdwnPrestations_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT CodePrestation AS [Code Prestation],Designation" + " FROM GMAO_PrestationsSource ORDER BY CodePrestation";
            sheridan.InitialiseCombo((this.cmbDrpdwnPrestations), General.sSQL, "Code Prestation", true);
        }

        private void cmbDrpdwnPrestations_AfterCloseUp(object sender, EventArgs e)
        {
            GridPrestations.ActiveRow.Cells["Designation"].Value = cmbDrpdwnPrestations.ActiveRow.Cells[1].Value;
        }

        private void label32_Click(object sender, EventArgs e)
        {
            fc_SavParamPos();
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, txtCode1.Text);
            View.Theme.Theme.Navigate(typeof(UserDocClient));
        }

        private void LienGoFicheAppel_Click(object sender, EventArgs e)
        {
            if (!string.IsNullOrEmpty(General.nz(txtNumFichestandard.Text, "0").ToString()))
            {

                fc_SavParamPos();
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocStandard, txtNumFichestandard.Text);
                View.Theme.Theme.Navigate(typeof(UserDocStandard));
            }
        }

        private void label23_Click(object sender, EventArgs e)
        {
            fc_SavParamPos();
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, txtCodeImmeuble.Text);
            View.Theme.Theme.Navigate(typeof(UserDocImmeuble));
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView1_AfterSelect(object sender, SelectEventArgs e)
        {
            UltraTreeNode node = TreeView1.ActiveNode;
            ssPEI_PeriodeGammeImm.UpdateData();
            ssFAP_FacArticleP2.UpdateData();

            fc_LoadScreenTreev(node);
        }


        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPEI_PeriodeGammeImm_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["EQM_NoAuto"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_NoAuto"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["TPP_Code"].Header.Caption = "Périodicité";
            this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["TPP_Code"].ValueList = this.ssDropTPP_TypePeriodeP2;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Cycle"].Header.Caption = "Cycle";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_DateDebut"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_DateFin"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_JD"].Header.Caption = "Jour Debut";
            this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_JD"].ValueList = this.ssDropJour;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_MD"].Header.Caption = "Mois Debut";
            this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_MD"].ValueList = this.ssDropMois;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_JF"].Header.Caption = "Jour Fin";
            this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_JF"].ValueList = this.ssDropJour;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_MF"].Header.Caption = "Mois Fin";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_PassageObli"].Header.Caption = "Passage Obligatoire";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_PassageObli"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            //===> Mondir le 18.03.2021, https://groupe-dt.mantishub.io/view.php?id=2361
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_PassageObli"].DefaultCellValue = false;
            //===> Fin Modif Mondir
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_ForceJournee"].Header.Caption = "Jour imposé";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_ForceJournee"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            //===> Mondir le 18.03.2021, https://groupe-dt.mantishub.io/view.php?id=2361
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_ForceJournee"].DefaultCellValue = false;
            //===> Fin Modif Mondir
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["SEM_Code"].Header.Caption = "Semaine";
            this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["SEM_Code"].ValueList = this.ssDropSemaine;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Heure"].Header.Caption = "Heure";
            //===> Mondir le 02.11.2020, added to fix https://groupe-dt.mantishub.io/view.php?id=2059
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Heure"].Format = "HH:mm:ss";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Heure"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.TimeWithSpin;
            //===> Fin Modif Mondir
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Heure"].MaskInput = "hh:mm:ss";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Duree"].Header.Caption = "Durée";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_DureeReel"].Header.Caption = "Durée Planning";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_CompteurObli"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_DureeVisu"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Samedi"].Header.Caption = "Samedi non inclus";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Samedi"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            //===> Mondir le 18.03.2021, https://groupe-dt.mantishub.io/view.php?id=2361
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Samedi"].DefaultCellValue = false;
            //===> Fin Modif Mondir
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Dimanche"].Header.Caption = "Dimanche non inclus";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Dimanche"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            //===> Mondir le 18.03.2021, https://groupe-dt.mantishub.io/view.php?id=2361
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Dimanche"].DefaultCellValue = false;
            //===> Fin Modif Mondir
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Pentecote"].Header.Caption = "Pentocote non inlus";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Pentecote"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            //===> Mondir le 18.03.2021, https://groupe-dt.mantishub.io/view.php?id=2361
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Pentecote"].DefaultCellValue = false;
            //===> Fin Modif Mondir
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Jourferiee"].Header.Caption = "Jours fériés non inclus";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Jourferiee"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            //===> Mondir le 18.03.2021, https://groupe-dt.mantishub.io/view.php?id=2361
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Jourferiee"].DefaultCellValue = false;
            //===> Fin Modif Mondir
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Visite"].Header.Caption = "Nbre Visites";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_ForceVisite"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Tech"].Hidden = true;
            this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Tech"].ValueList = this.ssDropMatricule;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Cout"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_MtAchat"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_KFO"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_KMO"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Calcul"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_TotalHT"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_IntAutre"].Header.Caption = "Autre";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_IntAutre"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            //===> Mondir le 18.03.2021, https://groupe-dt.mantishub.io/view.php?id=2361
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_IntAutre"].DefaultCellValue = false;
            //===> Fin Modif Mondir
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Intervenant"].Header.Caption = "Interevant";
            this.ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_Intervenant"].ValueList = this.ssDropIntervenant;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_APlanifier"].Header.Caption = "Ne pas planifier";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_APlanifier"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            //===> Mondir le 18.03.2021, https://groupe-dt.mantishub.io/view.php?id=2361
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_APlanifier"].DefaultCellValue = false;
            //===> Fin Modif Mondir
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["COP_NoAuto"].Hidden = true;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_AvisPassage"].Header.Caption = "Avis de Passage";
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_AvisPassage"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            //===> Mondir le 18.03.2021, https://groupe-dt.mantishub.io/view.php?id=2361
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_AvisPassage"].DefaultCellValue = false;
            //===> Fin Modif Mondir
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_AvisPassage"].DefaultCellValue = false;
            ssPEI_PeriodeGammeImm.DisplayLayout.Bands[0].Columns["PEI_AvisNbJour"].Header.Caption = "Nb Jour";

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssPEI_PeriodeGammeImm_AfterRowUpdate(object sender, RowEventArgs e)
        {
            tmpAdorsPEI_PeriodeGammeImm.Update();
        }

        private void ssPEI_PeriodeGammeImm_AfterRowsDeleted(object sender, EventArgs e)
        {
            tmpAdorsPEI_PeriodeGammeImm.Update();
        }

        private void ssGridRecap_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_JD"].Header.Caption = "Jour Debut";
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_MD"].Header.Caption = "Mois Debut";
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_JF"].Header.Caption = "Jour Fin";
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_MF"].Header.Caption = "Mois Fin";
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_Visite"].Header.Caption = "Nbre Visites";
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_IntAutre"].Header.Caption = "Autre";
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_IntAutre"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_DureeReel"].Hidden = true;
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_DateDebut"].Hidden = true;
            ssGridRecap.DisplayLayout.Bands[0].Columns["PEI_DateFin"].Hidden = true;
            ssGridRecap.DisplayLayout.Bands[0].Columns["GAI_Compteur"].Header.Caption = "Compteur à relever";
            ssGridRecap.DisplayLayout.Bands[0].Columns["GAI_Compteur"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridRecap.DisplayLayout.Bands[0].Columns["GAI_CompteurObli"].Header.Caption = "Compteur Obligatoire ";
            ssGridRecap.DisplayLayout.Bands[0].Columns["GAI_CompteurObli"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridRecap.DisplayLayout.Bands[0].Columns["CTR_Libelle"].Header.Caption = "Contrôl/Opération";
            ssGridRecap.DisplayLayout.Bands[0].Columns["Moy_Libelle"].Header.Caption = "Moyen";
            ssGridRecap.DisplayLayout.Bands[0].Columns["Ano_Libelle"].Header.Caption = "Anomalie";
            ssGridRecap.DisplayLayout.Bands[0].Columns["OPE_Libelle"].Header.Caption = "Opération";
            ssGridRecap.DisplayLayout.Bands[0].Columns["EQM_CODE"].Hidden = true;
        }
        private void ssOpTaches_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {

            ssOpTaches.DisplayLayout.Bands[0].Columns["Periodicite"].Header.Appearance.BackColor = Color.DarkOrange;
            for (int i = 0; i < ssOpTaches.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                ssOpTaches.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
            }
        }
        private void ssGridVisite_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssGridVisite.DisplayLayout.Bands[0].Columns["NoIntervention"].Header.Caption = "N°Visite (simul)";
            ssGridVisite.DisplayLayout.Bands[0].Columns["DatePrevue"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["Cop_NoAuto"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["INT_realise"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["dateRealise"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["HeureDebut"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["HeureFin"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["HeureDebutP"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["HeureFinP"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["PDAB_Libelle"].Header.Caption = "Localisation";
            ssGridVisite.DisplayLayout.Bands[0].Columns["Nom"].Header.Caption = "Nom d'intervenant";
            ssGridVisite.DisplayLayout.Bands[0].Columns["DureeP"].Header.Caption = "Duree";
            ssGridVisite.DisplayLayout.Bands[0].Columns["CompteurObli"].Header.Caption = "Compteur Obligatoire ";
            ssGridVisite.DisplayLayout.Bands[0].Columns["CompteurObli"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;

            for (int i = 0; i < ssGridVisite.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                ssGridVisite.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
            }
        }

        private void ssGridTachesAnnuel_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Hidden = true;
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["NoInterventionVisite"].Hidden = true;
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["Operation"].Hidden = true;
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["EQM_Libelle"].Hidden = true;
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["CTR_Libelle"].Header.Caption = "Control";
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["Moy_Libelle"].Header.Caption = "Moyen";
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["Ano_Libelle"].Header.Caption = "Anomalie";
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["OPE_Libelle"].Header.Caption = "Opération";
        }

        private void ssGridVisite_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.Row.Cells["CompteurObli"].Value == DBNull.Value || e.Row.Cells["CompteurObli"].Value.ToString() == "0")
            {
                e.Row.Cells["CompteurObli"].Value = false;
            }
            //ssGridVisite.UpdateData();
            e.Row.Update();
        }

        private void ssGridVisite_AfterRowActivate(object sender, EventArgs e)
        {
            if (ssGridVisite.ActiveRow != null)
            {

                fc_ssGRIDTachesAnnuel(Convert.ToInt32(General.nz(ssGridVisite.ActiveRow.Cells["NoIntervention"].Value.ToString(), 0)));
            }
        }

        private void ssPEI_PeriodeGammeImm_AfterExitEditMode(object sender, EventArgs e)
        {


            int i = 0;
            System.DateTime sDeb = default(System.DateTime);
            System.DateTime sFin = default(System.DateTime);
            System.DateTime dtTemp = default(System.DateTime);
            double dVisite = 0;
            string sInterv = null;
            try
            {
                var row = ssPEI_PeriodeGammeImm.ActiveRow;
                //controle certains elements dans la grille.
                fc_CtrlssPEI_PeriodeGammeImm(row);//tested

                //-- si la columns PEI_IntAutre <> 1 alors on récupére l'intervenant par defaut

                if (string.IsNullOrEmpty(row.Cells["COP_NoAuto"].Text))//tested
                {

                    row.Cells["COP_NoAuto"].Value = Convert.ToInt32(General.nz(txtCOP_NoAuto.Text, 0).ToString());

                }

                //=== recherche l'intervenant par defaut.
                if (string.IsNullOrEmpty(row.Cells["PEI_intAutre"].Text) || row.Cells["PEI_intAutre"].Text == "0")
                {
                    if (string.IsNullOrEmpty(row.Cells["PEI_Intervenant"].Text) || row.Cells["PEI_Intervenant"].IsActiveCell)

                    {
                        //=== modif du 30 08 2010 ajout de la catégorie de l'intervenant.
                        //sSQL = "SELECT  " _
                        //& " IntervenantImm_INM.intervenant_INM " _
                        //& " FROM IntervenantImm_INM " _
                        //& " WHERE Codeimmeuble_IMM='" & gFr_DoublerQuote(txtCodeimmeuble) & "'" _
                        //& " AND CATI_CODE ='" & cIntervEntretien & "'" _
                        //& " order by Nordre_INM"	General.sSQL = "SELECT     CodeDepanneur" + " From Immeuble " + " WHERE CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble) + "'";

                        //.Columns("PEI_Intervenant").value = fc_ADOlibelle(sSQL)
                        var tmpAdo = new ModAdo();
                        row.Cells["PEI_Intervenant"].Value = tmpAdo.fc_ADOlibelle(General.sSQL).ToString();
                    }

                }

                //=== compteur obligatoire.
                //if (row.Cells["PEI_CompteurObli"].IsActiveCell)
                //{
                //    if (row.Cells["PEI_CompteurObli"].Text == "-1")
                //    {
                //        row.Cells["PEI_CompteurObli"].Value = 1;
                //    }

                //}

                if (string.IsNullOrEmpty(row.Cells["EQM_NoAuto"].Text))//tested
                {
                    row.Cells["EQM_NoAuto"].Value = Convert.ToInt32(General.nz(txtEQM_NoAuto.Text, 0));
                }

                if (string.IsNullOrEmpty(row.Cells["CodeImmeuble"].Text))//tested
                {
                    row.Cells["CodeImmeuble"].Value = txtCodeImmeuble.Text;
                }

                //    If .Columns("EQM_NoAuto").Text = "" Then
                //        .Columns("EQM_NoAuto").value = ssGammePlanning.Columns("EQM_NoAuto").value
                //    End If


                //if (row.Cells["PEI_APlanifier"].IsActiveCell)
                //{
                //    if (row.Cells["PEI_APlanifier"].Text == "-1")
                //    {
                //       row.Cells["PEI_APlanifier"].Value = 1;
                //    }
                //}


                //if (row.Cells["PEI_ForceJournee"].IsActiveCell)
                //{
                //    if (row.Cells["PEI_ForceJournee"].Text == "-1")
                //    {
                //       row.Cells["PEI_ForceJournee"].Value = 1;
                //    }
                //}

                if (row.Cells["PEI_AvisPassage"].IsActiveCell)
                {
                    if (row.Cells["PEI_AvisPassage"].Text == "-1")
                    {
                        row.Cells["PEI_AvisPassage"].Value = 1;
                    }
                    else
                    {
                        row.Cells["PEI_AvisNbJour"].Value = 0;
                    }
                }

                //==== valeur par défaut des paramétres GMAO.
                //=== samedi.
                if (string.IsNullOrEmpty(row.Cells["PEI_Samedi"].Text))
                {
                    row.Cells["PEI_Samedi"].Value = modP2.tGmao.lSamedi;
                }
                //if (row.Cells["PEI_Samedi"].Text == "-1")
                //{
                //    row.Cells["PEI_Samedi"].Value = 1;
                //}

                //=== Dimanche.
                if (string.IsNullOrEmpty(row.Cells["PEI_Dimanche"].Text))
                {
                    row.Cells["PEI_Dimanche"].Value = modP2.tGmao.lDimanche;
                }
                //if (row.Cells["PEI_Dimanche"].Text == "-1")
                //{
                //    row.Cells["PEI_Dimanche"].Value = 1;
                //}

                //=== pentecote.
                if (string.IsNullOrEmpty(row.Cells["PEI_Pentecote"].Text))
                {
                    row.Cells["PEI_Pentecote"].Value = modP2.tGmao.lPentecote;
                }
                //if (row.Cells["PEI_Pentecote"].Text == "-1")
                //{
                //    row.Cells["PEI_Pentecote"].Value = 1;
                //}

                //=== jours fériés.
                if (string.IsNullOrEmpty(row.Cells["PEI_Jourferiee"].Text))
                {
                    row.Cells["PEI_Jourferiee"].Value = modP2.tGmao.lJoursFeriees;
                }
                //if (row.Cells["PEI_Jourferiee"].Text == "-1")
                //{
                //    row.Cells["PEI_Jourferiee"].Value = 1;
                //}

                //if (row.Cells["PEI_APlanifier"].IsActiveCell)
                //{
                //    if (row.Cells["PEI_APlanifier"].Text == "-1")
                //    {
                //        row.Cells["PEI_APlanifier"].Value = 1;
                //    }
                //}


                if (row.Cells["TPP_CODE"].IsActiveCell && row.Cells["TPP_CODE"].DataChanged)
                {

                    row.Cells["PEI_JD"].Value = DBNull.Value;
                    row.Cells["PEI_JF"].Value = DBNull.Value;
                    row.Cells["PEI_MD"].Value = DBNull.Value;
                    row.Cells["PEI_MF"].Value = DBNull.Value;
                    row.Cells["SEM_Code"].Value = DBNull.Value;

                    for (i = 0; i <= ssDropTPP_TypePeriodeP2.Rows.Count - 1; i++)
                    {
                        if (row.Cells["TPP_CODE"].Value.ToString() == ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_CODE"].Value.ToString())
                        {
                            //if (!string.IsNullOrEmpty(ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_JD"].Text))
                            //{
                            row.Cells["PEI_JD"].Value = ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_JD"].Value;
                            //}
                            //if (!string.IsNullOrEmpty(ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_JF"].Text))
                            //{
                            row.Cells["PEI_JF"].Value = ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_JF"].Value;
                            //}
                            //if (!string.IsNullOrEmpty(ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_MD"].Text))
                            //{
                            row.Cells["PEI_MD"].Value = ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_MD"].Value;
                            //}
                            //if (!string.IsNullOrEmpty(ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_Mf"].Text))
                            //{
                            row.Cells["PEI_MF"].Value = ssDropTPP_TypePeriodeP2.Rows[i].Cells["TPP_Mf"].Value;
                            //}

                            //ssPEI_PeriodeGammeImm.Columns("SEM_Code").value = .Columns("SEM_Code").value

                            break;
                        }

                    }

                }

                //===> Mondir le 02.11.2020, IsActiveCell allways false, to fix https://groupe-dt.mantishub.io/view.php?id=2061
                //if ((row.Cells["PEI_JF"].IsActiveCell || row.Cells["PEI_MF"].IsActiveCell)
                //    && row.Cells["TPP_Code"].Text.ToUpper() == "Annuel".ToUpper())
                //{
                if (row.Cells["TPP_Code"].Text.ToUpper() == "Annuel".ToUpper())
                {
                    row.Cells["PEI_JF"].Value = fDate.fc_FinDeMois(row.Cells["PEI_MD"].Text, DateTime.Today.Month.ToString()).Day;
                    row.Cells["PEI_MF"].Value = row.Cells["PEI_MD"].Value;
                }
                //}

                //controle si la date du jour et du mois sont cohérent pour le jour debut et le mois debut.
                if (!string.IsNullOrEmpty(row.Cells["PEI_JD"].Text) && !string.IsNullOrEmpty(row.Cells["PEI_MD"].Text)
                    && fDate.IsValidMonthAndYear(Convert.ToInt32(row.Cells["PEI_MD"].Text), DateTime.Now.Year))
                {
                    if (!General.IsDate(row.Cells["PEI_JD"].Text + "/" + row.Cells["PEI_MD"].Text + "/" + DateTime.Now.Year))
                    {
                        row.Cells["PEI_JD"].Value = Math.Min(Convert.ToInt32(row.Cells["PEI_JD"].Text), DateTime.DaysInMonth(DateTime.Today.Year, Convert.ToInt32(row.Cells["PEI_MD"].Text)));
                    }

                }

                //controle si la date du jour et du mois sont cohérent pour le jour debut et le mois fin.
                if (!string.IsNullOrEmpty(row.Cells["PEI_JF"].Text) && !string.IsNullOrEmpty(row.Cells["PEI_MF"].Text)
                    && fDate.IsValidMonthAndYear(Convert.ToInt32(row.Cells["PEI_MF"].Text), DateTime.Now.Year))
                {
                    if (!General.IsDate(row.Cells["PEI_JF"].Text + "/" + row.Cells["PEI_MF"].Text + "/" + DateTime.Now.Year))
                    {
                        row.Cells["PEI_JF"].Value = Math.Min(Convert.ToInt32(row.Cells["PEI_JF"].Text), DateTime.DaysInMonth(DateTime.Today.Year, Convert.ToInt32(row.Cells["PEI_MF"].Text)));
                    }
                }


                // met la valeur A (automatique) si le champs PEI_ForceVisite est à vide
                if (string.IsNullOrEmpty(row.Cells["PEI_ForceVisite"].Text))
                {
                    row.Cells["PEI_ForceVisite"].Value = "A";
                }


                ///    '==== modif du 13 07 2013, les dates sont saisies en jour et mois, on ne saisit plus la date compléte avec l'année.
                ///    '==== contrôle que la date de début et la date de fin ne dépasse pas une année et contrôle que la
                ///    '==== date de début est plus petite que la date de fin.
                ///     If ColIndex = .Columns("PEI_DateDebut").Position Or ColIndex = .Columns("PEI_dateFin").Position Then
                ///
                ///            .Columns(ColIndex).Text = fc_ValideDate(.Columns(ColIndex).Text)
                ///
                ///            If IsDate(.Columns("PEI_DateDebut").Text) And IsDate(.Columns("PEI_DateFin").Text) Then
                ///
                ///                    sDeb = .Columns("PEI_DateDebut").Text
                ///                    sFin = .Columns("PEI_DateFin").Text
                ///                    dtTemp = DateAdd("yyyy", 1, sDeb) - 1
                ///
                ///                    If sFin > dtTemp Then
                ///                        MsgBox "Erreur de saisie" & vbCrLf & "La différence de jours entre la date de début" _
                ///'                                & " et la date de fin ne doit pas excéder une année.", vbExclamation, "Erreur de saisie"
                ///                                Cancel = True
                ///                                Exit Sub
                ///                    ElseIf sFin < sDeb Then
                ///                        MsgBox "Erreur de saisie" & vbCrLf & "La date de début doit être antérieur à la date de fin." _
                ///'                                & "", vbExclamation, "Erreur de saisie"
                ///                                Cancel = True
                ///                                Exit Sub
                ///                    End If
                ///
                ///            End If
                ///
                ///            If IsDate(.Columns("PEI_DateDebut").value) Then
                ///                      sDeb = .Columns("PEI_DateDebut").Text
                ///                     If sDeb < txtCOP_DateSignature Then
                ///                           MsgBox "Erreur de saisie" & vbCrLf & "La date de début doit être postérieur ou égale à la date de début de planification." _
                ///'                                & "", vbExclamation, "Erreur de saisie"
                ///                                Cancel = True
                ///                                Exit Sub
                ///                    End If
                ///            End If
                ///
                ///     End If

                if (row.Cells["PEI_ForceVisite"].Text.ToUpper() == "A".ToUpper())
                {
                    if (!string.IsNullOrEmpty(row.Cells["PEI_JD"].Text)
                        && !string.IsNullOrEmpty(row.Cells["PEI_JF"].Text)
                        && !string.IsNullOrEmpty(row.Cells["PEI_MD"].Text)
                        && !string.IsNullOrEmpty(row.Cells["PEI_MF"].Text)
                        && !string.IsNullOrEmpty(row.Cells["TPP_Code"].Text))
                    {
                        if (!string.IsNullOrEmpty(row.Cells["PEI_JD"].Text) && !string.IsNullOrEmpty(row.Cells["PEI_MD"].Text)
                           && fDate.IsValidMonthAndYear(Convert.ToInt32(row.Cells["PEI_MD"].Text), DateTime.Now.Year))
                        {
                            if (General.IsDate(row.Cells["PEI_JD"].Text + "/" + row.Cells["PEI_MD"].Text + "/" + DateTime.Now.Year))
                            {
                                sDeb = Convert.ToDateTime(row.Cells["PEI_JD"].Text + "/" + row.Cells["PEI_MD"].Text + "/" + DateTime.Now.Year);
                            }
                        }
                        if (!string.IsNullOrEmpty(row.Cells["PEI_MF"].Text) && !string.IsNullOrEmpty(row.Cells["PEI_MF"].Text)
                           && fDate.IsValidMonthAndYear(Convert.ToInt32(row.Cells["PEI_MF"].Text), DateTime.Now.Year))
                        {
                            if (General.IsDate(row.Cells["PEI_JF"].Text + "/" + row.Cells["PEI_MF"].Text + "/" + DateTime.Now.Year))
                            {
                                if (Convert.ToDouble(row.Cells["PEI_MF"].Text) < Convert.ToDouble(row.Cells["PEI_MD"].Text))
                                {
                                    if (General.IsDate(row.Cells["PEI_JF"].Text + "/" + row.Cells["PEI_MF"].Text + "/" + Convert.ToInt32(DateTime.Now.Year + 1)))
                                    {
                                        sFin = Convert.ToDateTime(row.Cells["PEI_JF"].Text + "/" + row.Cells["PEI_MF"].Text + "/" + Convert.ToInt32(DateTime.Now.Year + 1));
                                    }

                                }
                                else//tested
                                {
                                    if (General.IsDate(row.Cells["PEI_JF"].Text + "/" + row.Cells["PEI_MF"].Text + "/" + DateTime.Now.Year))
                                    {
                                        sFin = Convert.ToDateTime(row.Cells["PEI_JF"].Text + "/" + row.Cells["PEI_MF"].Text + "/" + DateTime.Now.Year);
                                    }

                                }
                            }

                        }


                        switch (row.Cells["TPP_Code"].Text.ToUpper())

                        {
                            case "HEBDOMADAIRE":
                                {
                                    row.Cells["peI_Visite"].Value = Math.Floor((sFin - sDeb).TotalDays / 7);
                                    break;
                                }
                            case "MENSUEL":
                                {
                                    dVisite = Math.Floor((double)General.TotalMonths(sDeb, sFin) + 1);
                                    row.Cells["peI_Visite"].Value = dVisite;
                                    break;
                                }

                            case "BIMENSUEL":
                                {
                                    row.Cells["peI_Visite"].Value = Math.Floor((sFin - sDeb).TotalDays / 14);
                                    break;
                                }

                            case "TRIMESTRIEL":
                                {
                                    dVisite = General.TotalMonths(sDeb, sFin) + 1;
                                    row.Cells["peI_Visite"].Value = Math.Floor(dVisite / 3) == dVisite / 3
                                        ? Math.Floor(dVisite / 3)
                                        : Math.Floor(dVisite / 3) + 1;
                                    break;
                                }

                            case "SEMESTRIEL":
                                {
                                    dVisite = General.TotalMonths(sDeb, sFin) + 1;
                                    row.Cells["peI_Visite"].Value = Math.Floor(dVisite / 6) == dVisite / 6
                                        ? Math.Floor(dVisite / 6)
                                        : Math.Floor(dVisite / 6) + 1;
                                    break;
                                }

                            case "ANNUEL":
                                {
                                    dVisite = 1;
                                    row.Cells["peI_Visite"].Value = dVisite;

                                    break;
                                }
                        }


                        if (row.Cells["TPP_Code"].Text.ToUpper() == modP2.cBiMestriel.ToUpper())
                        {
                            dVisite = General.TotalMonths(sDeb, sFin) + 1;
                            row.Cells["peI_Visite"].Value = Math.Floor(dVisite / 2) == dVisite / 2
                                ? Math.Floor(dVisite / 2)
                                : Math.Floor(dVisite / 2) + 1;
                        }


                    }

                }


                ///    If UCase(.Columns("PEI_ForceVisite").Text) = UCase("A") Then
                ///
                ///        If .Columns("TPP_code").Text <> "" And IsDate(.Columns("PEI_DateDebut").Text) And _
                ///'                 IsDate(.Columns("PEI_DateFin").Text) Then
                ///
                ///
                ///                 sDeb = .Columns("PEI_DateDebut").Text
                ///                 sFin = .Columns("PEI_DateFin").Text
                ///
                ///
                ///                 If UCase(.Columns("TPP_Code").Text) = UCase("Hebdomadaire") Then
                ///                        '.Columns("peg_Visite").value = Fix(DateDiff("d", sDeb, sFin, , vbFirstJan1) / 7)
                ///                        .Columns("peI_Visite").value = Fix(DateDiff("d", sDeb, sFin) / 7)
                ///
                ///
                ///                 ElseIf UCase(.Columns("TPP_Code").Text) = UCase("Mensuel") Then
                ///                         dVisite = DateDiff("m", sDeb, sFin) + 1
                ///                        .Columns("peI_Visite").value = dVisite
                ///
                ///
                ///                 ElseIf UCase(.Columns("TPP_Code").Text) = UCase("BiMensuel") Then
                ///
                ///                        .Columns("peI_Visite").value = Fix(DateDiff("d", sDeb, sFin) / 14)
                ///
                ///                 ElseIf UCase(.Columns("TPP_Code").Text) = UCase("Trimestriel") Then
                ///                        dVisite = DateDiff("m", sDeb, sFin) + 1
                ///                        If Fix(dVisite / 3) = (dVisite / 3) Then
                ///                           .Columns("peI_Visite").value = Fix(dVisite / 3)
                ///                        Else
                ///                            .Columns("peI_Visite").value = Fix(dVisite / 3) + 1
                ///                        End If
                ///
                ///                 ElseIf UCase(.Columns("TPP_Code").Text) = UCase("Semestriel") Then
                ///                        dVisite = DateDiff("m", sDeb, sFin) + 1
                ///                        If Fix(dVisite / 6) = (dVisite / 6) Then
                ///                            .Columns("peI_Visite").value = Fix(dVisite / 6)
                ///                        Else
                ///                            .Columns("peI_Visite").value = Fix(dVisite / 6) + 1
                ///                        End If
                ///
                ///                ElseIf UCase(.Columns("TPP_Code").Text) = UCase("Annuel") Then
                ///                        dVisite = 1
                ///                        .Columns("peI_Visite").value = dVisite
                ///
                ///                 End If
                ///        End If
                ///    End If

                //=== si la case du champ PEI_Calcul est coché lui attribué la valeur 1 au lieu de
                //=== -1 (valeur de la case à cocher de la grille)
                //if (row.Cells["PEI_Calcul"].Text == "-1")
                //{
                //    row.Cells["PEI_Calcul"].Value = 1;
                //}

                //
                //if (row.Cells["PEI_IntAutre"].Text == "-1")
                //{
                //    row.Cells["PEI_IntAutre"].Value = 1;
                //}

                //=== modif du 14 07 2013, on ne saisit plus une date mais un moi et une journée.
                //=== date de début obligatoire.
                ///    If .Columns("PEI_DateDebut").Text = "" Then
                ///        MsgBox "La date de début est obligatoire.", vbInformation, "Données manquantes"
                ///        Cancel = True
                ///        Exit Sub
                ///    End If





                //calcul du déboursé si PEICalcul est a 1 le Total HT n'est pas calculé.
                ///    If .Columns("PEI_Calcul").Text <> "1" Then
                ///        .Columns("PEI_TotalHT").value = 0
                ///        If UCase(.Columns("PEI_Duree").Text) <> "" And .Columns("PEI_Tech").Text <> "" Then
                ///           Dim dCout As Double
                ///            sSQL = "SELECT PER_Charge From Personnel WHERE     (Matricule = '" & UCase(.Columns("PEI_Tech").Text) & "')"
                ///            dCout = nz(fc_ADOlibelle(sSQL), 0)
                ///            .Columns("PEI_Cout").value = dCout
                ///            If .Columns("PEI_Visite").Text <> "" Then
                ///                .Columns("PEI_TotalHT").value = (.Columns("PEI_Duree").Text * dCout) * .Columns("PEI_Visite").Text
                ///            End If
                ///        End If
                ///    End If

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "");
            }
        }

        private void ssPEI_PeriodeGammeImm_Error(object sender, ErrorEventArgs e)
        {
            if (e.ErrorType == ErrorType.Data)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie de données incorrectes dans la colonne " + e.DataErrorInfo.Cell.Column.Header.Caption);
                e.Cancel = true;
            }

        }

        private void txtAngleRue_TextChanged(object sender, EventArgs e)
        {

        }

        private void ssPEI_PeriodeGammeImm_BeforeExitEditMode_1(object sender, BeforeExitEditModeEventArgs e)
        {
            if (!string.IsNullOrEmpty(ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_JD"].Text)
                && General.IsNumeric(ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_JD"].Text)
                && Convert.ToInt32(ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_JD"].Text) > 31)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisir une valeur valable", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_JD"].Activate();
                return;
            }
            if (!string.IsNullOrEmpty(ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_JF"].Text)
                && General.IsNumeric(ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_JF"].Text)
                && Convert.ToInt32(ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_JF"].Text) > 31)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisir une valeur valable", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_JF"].Activate();
                return;
            }
            if (!string.IsNullOrEmpty(ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_MD"].Text)
                && General.IsNumeric(ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_MD"].Text)
                && Convert.ToInt32(ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_MD"].Text) > 12)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisir une valeur valable", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_MD"].Activate();
                return;
            }
            if (!string.IsNullOrEmpty(ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_MF"].Text)
                && General.IsNumeric(ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_MF"].Text)
                && Convert.ToInt32(ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_MF"].Text) > 12)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisir une valeur valable", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                e.Cancel = true;
                ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_MF"].Activate();
                return;
            }
        }

        private void txtFAP_Designation1_TextChanged(object sender, EventArgs e)
        {

        }

        private void txtFAP_NoAuto_TextChanged(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// ===> Mondir le 28.01.2021, ajout des modfis de la version V27.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdTest_Click(object sender, EventArgs e)
        {
            if (ssPEI_PeriodeGammeImm.ActiveRow == null)
                return;
            //===> Mondir le 29.01.2021, j'ai contacté Rachid, il n'a pas encore terminé la migration de ctte fiche ===> frmParamGMAO
            //===> Mondir le 18.02.2021 pour integrer les modifs de la version V16.02.2021
            frmParamGMAO frmParamGMAO = new frmParamGMAO();
            frmParamGMAO.txtPEI_NoAuto.Text = ssPEI_PeriodeGammeImm.ActiveRow.Cells["PEI_Noauto"].Value?.ToString();
            frmParamGMAO.txtCOP_Noauto.Text = txtCOP_NoAuto.Text;
            frmParamGMAO.txtCodeimmeuble.Text = txtCodeImmeuble.Text;
            frmParamGMAO.txtEQM_Noauto.Text = ssPEI_PeriodeGammeImm.ActiveRow.Cells["EQM_Noauto"].Value.ToString();
            frmParamGMAO.ShowDialog();
        }
    }
}

