﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Excel = Microsoft.Office.Interop.Excel;
using Infragistics.Win.UltraWinGrid;
using Axe_interDT.Views.Contrat.FicheGMAO;

namespace Axe_interDT.Views.Contrat.TriContrat
{
    public partial class UserDocTriContat : UserControl
    {
       
        string sSQL;
        DataTable rsCtr;
        bool bTriAsc;
        int iOldCol;
        string sOrderBy;
        const short cCol = 0;
        public UserDocTriContat()
        {
            InitializeComponent();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_BeforeDropDown(object sender, CancelEventArgs e)
        {
            int i = 0;
            string req = null;
            try
            {
                // ERROR: Not supported in C#: OnErrorStatement
                sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom," + " Qualification.CodeQualif, Qualification.Qualification" + " FROM Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif";
                //req = "";
                if (General.CodeQualifCommercial.Count() > 0)
                {
                    req = req + " WHERE (";
                    for (i = 0; i < General.CodeQualifCommercial.Count(); i++)
                    {
                        if (i < General.CodeQualifCommercial.Count() - 1)
                            req = req + "Personnel.CodeQualif='" + General.CodeQualifCommercial[i] + "' OR ";
                        else
                            req = req + "Personnel.CodeQualif='" + General.CodeQualifCommercial[i] + "'";
                    }
                    req = req + ")";
                }
                if (string.IsNullOrEmpty(req))
                {
                    req = " WHERE (Personnel.NonActif is null or Personnel.NonActif = 0) ";
                }
                else
                {
                    req = req + " AND (Personnel.NonActif is null or Personnel.NonActif = 0)";
                }
                sSQL = sSQL + req + " ORDER BY Personnel.Nom";
                sheridan.InitialiseCombo(cmbCommercial, sSQL, "Matricule");
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbCommercial_DropDown");
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_AfterCloseUp(object sender, EventArgs e)
        {
            if (cmbCommercial.ActiveRow != null)
            {
                if (!string.IsNullOrEmpty(cmbCommercial.ActiveRow.Cells[0].Value + ""))
                {
                    using (var ModAdo = new ModAdo())
                    {
                        lblLibCommercial.Text = ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbCommercial.ActiveRow.Cells[0].Value + "'");
                    }
                }
            }
        }

        private void cmbCommercial_RowSelected(object sender, Infragistics.Win.UltraWinGrid.RowSelectedEventArgs e)
        {
            if (e.Row != null)
                lblLibCommercial.Text = e.Row.Cells[1].Value + "";
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (Convert.ToInt32(e.KeyChar) == 13)
                {
                    string ModAdo_fc_ADOlibelle;
                    using (ModAdo modAdo = new ModAdo())
                    {
                        ModAdo_fc_ADOlibelle = modAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbCommercial.Value + "'");
                    }
                    if (!!string.IsNullOrEmpty(General.nz(ModAdo_fc_ADOlibelle, "").ToString()))
                    {
                        string req = "SELECT Personnel.Matricule as \"Matricule\","
                                + " Personnel.Nom as \"Nom\","
                                + " Personnel.prenom as \"Prenom\","
                                + " Qualification.Qualification as \"Qualification\","
                                + " Personnel.Memoguard as \"MemoGuard\","
                                + " Personnel.NumRadio as \"NumRadio\""
                                + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                        string txtWhere = " (NonActif is null or NonActif = 0)";
                        SearchTemplate st = new SearchTemplate(this, null, req, txtWhere, "");
                        if (General.IsNumeric(cmbCommercial.Text))
                            st.SetValues(new Dictionary<string, string> { { "Matricule", cmbCommercial.Text } });
                        else
                            st.SetValues(new Dictionary<string, string> { { "Nom", cmbCommercial.Text } });
                        st.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbCommercial.Text = ev.Row.GetCellValue("Matricule") + "";
                            lblLibCommercial.Text = ev.Row.GetCellValue("Nom") + "";
                            st.Dispose();
                            st.Close();
                        };

                        st.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13)
                            {
                                if (st.ugResultat.ActiveRow != null)
                                {
                                    cmbCommercial.Text = st.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                                    lblLibCommercial.Text = st.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                                    st.Dispose();
                                    st.Close();
                                }
                            }
                        };
                        st.StartPosition = FormStartPosition.CenterParent;
                        st.ShowDialog();
                    }
                    cmbContrat_Click(cmbContrat, null);

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbCommercial_KeyPress");
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbContrat_Click(object sender, EventArgs e)
        {
            string sWhere = null;

            try
            {
                Cursor.Current = Cursors.WaitCursor;
                
                sSQL = "SELECT     COP_ContratP2.codeimmeuble, immeuble.ville, Table1.Code1, " + " COP_ContratP2.COP_NoAuto, COP_ContratP2.COP_Statut, " + " COP_ContratP2.COP_DateDemande, COP_ContratP2.COP_DateImpression, " + " Contrat.DateEffet, Contrat.DateFin," + "  " + " COP_ContratP2.COP_SuiviePar, " + " COP_ContratP2.COP_DateTaciteReconduction, COP_ContratP2.COP_Objet," + " Personnel.Nom, Immeuble.P1, Immeuble.P2," + " Immeuble.P3 , Immeuble.p4, Contrat.Resiliee";
                sSQL = sSQL + " FROM         COP_ContratP2 INNER JOIN" + " Immeuble ON COP_ContratP2.codeimmeuble = Immeuble.CodeImmeuble INNER JOIN " + " Table1 ON Immeuble.Code1 = Table1.Code1 LEFT OUTER JOIN " + " Contrat ON COP_ContratP2.NumContratRef = Contrat.NumContrat AND COP_ContratP2.AvenantRef = Contrat.Avenant LEFT OUTER JOIN " + " Personnel ON Immeuble.CodeChefSecteur = Personnel.Matricule";
                sWhere = "";
                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Immeuble.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Immeuble.codeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtIntervenant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE     Immeuble.CodeImmeuble IN" + " (SELECT     Codeimmeuble_IMM" + " From IntervenantImm_INM" + " WHERE   intervenant_INM = '" + txtIntervenant.Text + "')";
                    }
                    else
                    {
                        sWhere = sWhere + " AND     Immeuble.CodeImmeuble IN" + " (SELECT     Codeimmeuble_IMM" + " From IntervenantImm_INM" + " WHERE   intervenant_INM = '" + txtIntervenant.Text + "')";
                    }
                }
                if (!string.IsNullOrEmpty(cmbCommercial.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Immeuble.CodeCommercial ='" + cmbCommercial.Value + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Immeuble.CodeCommercial ='" + cmbCommercial.Value + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCPImm.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                       
                        sWhere = " WHERE Immeuble.codepostal = '" + StdSQLchaine.gFr_DoublerQuote(txtCPImm.Text) + "'";
                    }
                    else
                    {
                        
                        sWhere = sWhere + " AND Immeuble.codepostal = '" + StdSQLchaine.gFr_DoublerQuote(txtCPImm.Text) + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtAdresseIm.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                      
                        sWhere = " WHERE Immeuble.adresse like '%" + StdSQLchaine.gFr_DoublerQuote(txtAdresseIm.Text.Replace("*", "%")) + "%'";
                    }
                    else
                    {
                      
                        sWhere = sWhere + " AND Immeuble.adresse like '%" + StdSQLchaine.gFr_DoublerQuote(txtAdresseIm.Text.Replace("*", "%")) + "%'";
                    }
                }
                if (!string.IsNullOrEmpty(txtVilleImm.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                      
                        sWhere = " WHERE Immeuble.ville like '%" + StdSQLchaine.gFr_DoublerQuote(txtVilleImm.Text.Replace("*", "%")) + "%'";
                    }
                    else
                    {
                      
                        sWhere = sWhere + " AND Immeuble.ville like '%" + StdSQLchaine.gFr_DoublerQuote(txtVilleImm.Text.Replace("*", "%")) + "%'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCode1.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                     
                        sWhere = " WHERE Table1.Code1 like '%" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text.Replace("*", "%")) + "%'";
                    }
                    else
                    {
                      
                        sWhere = sWhere + " AND Table1.Code1 like '%" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text.Replace("*", "%")) + "%'";
                    }
                }
                if (!string.IsNullOrEmpty(txtRacineGerant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Table1.Code1 like '" + General.fc_racineGerant(txtRacineGerant.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Table1.Code1 like '" + General.fc_racineGerant(txtRacineGerant.Text) + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_Statut.Text) && !string.IsNullOrEmpty(txtCOP_Statut1.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_Statut ='" + StdSQLchaine.gFr_DoublerQuote(txtCOP_Statut.Text) + "'" + " OR COP_ContratP2.COP_Statut ='" + StdSQLchaine.gFr_DoublerQuote(txtCOP_Statut1.Text) + "')";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_Statut ='" + StdSQLchaine.gFr_DoublerQuote(txtCOP_Statut.Text) + "'" + " OR COP_ContratP2.COP_Statut ='" + StdSQLchaine.gFr_DoublerQuote(txtCOP_Statut1.Text) + "')";
                    }
                }
                else if (!string.IsNullOrEmpty(txtCOP_Statut1.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_Statut ='" + StdSQLchaine.gFr_DoublerQuote(txtCOP_Statut1.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_Statut ='" + StdSQLchaine.gFr_DoublerQuote(txtCOP_Statut1.Text) + "'";
                    }


                }
                else if (!string.IsNullOrEmpty(txtCOP_Statut.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_Statut ='" + StdSQLchaine.gFr_DoublerQuote(txtCOP_Statut.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_Statut ='" + StdSQLchaine.gFr_DoublerQuote(txtCOP_Statut.Text) + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_Objet.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_Objet like '%" + StdSQLchaine.gFr_DoublerQuote(txtCOP_Objet.Text) + "%'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_Objet like '%" + StdSQLchaine.gFr_DoublerQuote(txtCOP_Objet.Text) + "%'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_DateDemandeDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_DateDemande >='" + txtCOP_DateDemandeDe.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_DateDemande >='" + txtCOP_DateDemandeDe.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_DateDemandeAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_DateDemande <='" + txtCOP_DateDemandeAu.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_DateDemande <='" + txtCOP_DateDemandeAu.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_DateImpressionDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_DateImpression >='" + txtCOP_DateImpressionDe.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_DateImpression >='" + txtCOP_DateImpressionDe.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_DateImpressionAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_DateImpression <='" + txtCOP_DateImpressionAu.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_DateImpression <='" + txtCOP_DateImpressionAu.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_DateEffetDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Contrat.DateEffet >='" + txtCOP_DateEffetDe.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Contrat.DateEffet  >='" + txtCOP_DateEffetDe.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_DateEffetAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_DateEffet <='" + txtCOP_DateEffetAu.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_DateEffet <='" + txtCOP_DateEffetAu.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_DateAcceptationDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_DateAcceptation >='" + txtCOP_DateAcceptationDe.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_DateAcceptation >='" + txtCOP_DateAcceptationDe.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_DateAcceptationA.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_DateAcceptation <='" + txtCOP_DateAcceptationA.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_DateAcceptation <='" + txtCOP_DateAcceptationA.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_DateDemandeResiliationDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_DateDemandeResiliation >='" + txtCOP_DateDemandeResiliationDe.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_DateDemandeResiliation >='" + txtCOP_DateDemandeResiliationDe.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_DateDemandeResiliationA.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_DateDemandeResiliation <='" + txtCOP_DateDemandeResiliationA.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_DateDemandeResiliation <='" + txtCOP_DateDemandeResiliationA.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_DateTaciteReconductionDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_DateTaciteReconduction >='" + txtCOP_DateTaciteReconductionDe.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_DateTaciteReconduction >='" + txtCOP_DateTaciteReconductionDe.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_DateTaciteReconductionAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_DateTaciteReconduction <='" + txtCOP_DateTaciteReconductionAu.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_DateTaciteReconduction <='" + txtCOP_DateTaciteReconductionAu.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_DateFinDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Contrat.DateFin >='" + txtCOP_DateFinDe.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Contrat.DateFin >='" + txtCOP_DateFinDe.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_DateFinAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_DateFin <='" + txtCOP_DateFinAu.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_DateFin <='" + txtCOP_DateFinAu.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_SuiviePar.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_SuiviePar ='" + StdSQLchaine.gFr_DoublerQuote(txtCOP_SuiviePar.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_SuiviePar ='" + StdSQLchaine.gFr_DoublerQuote(txtCOP_SuiviePar.Text) + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCOP_Puissance.Text))
                {
                    if (string.IsNullOrEmpty(Combo.Text))
                    {
                        Combo.Text = "=";
                    }
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_Puissance " + Combo.Text + "'" + General.nz(txtCOP_Puissance.Text, 0) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_Puissance " + Combo.Text + "'" + General.nz(txtCOP_Puissance.Text, 0) + "'";
                    }
                }
                if (chkCOP_FIOUL.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE COP_ContratP2.COP_FiOUL = 1 ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND COP_ContratP2.COP_FiOUL = 1 ";
                    }

                }
                if (chkCOP_ELECT.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_ELECT = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  (COP_ContratP2.COP_ELECT = 1)";
                    }
                }
                if (chkCOP_SURP.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_SURP = 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_SURP = 1) ";
                    }

                }
                if (chkCOP_Murale.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  (COP_ContratP2.COP_Murale = 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  (COP_ContratP2.COP_Murale = 1) ";
                    }
                }
                if (chkCOP_Gaz.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_Gaz = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_Gaz = 1)";
                    }
                }
                if (chkCOP_CLIM.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_CLIM = 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_CLIM = 1) ";
                    }
                }

                if (chkCOP_TE.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_TE = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_TE = 1)";
                    }

                }

                if (chkCOP_TeleAlarme.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_TeleAlarme = 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_TeleAlarme = 1) ";
                    }
                }

                if (chkCOP_CPCU.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_CPCU = 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_CPCU = 1) ";
                    }
                }

                if (chkCOP_VmcDsc.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_VmcDsc = 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_VmcDsc = 1) ";
                    }
                }

                if (chkCOP_VMC.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_VMC = 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_VMC = 1) ";
                    }
                }

                if (ChkCOP_EnAttente.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_EnAttente = 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_EnAttente = 1) ";
                    }
                }

                if (chkCOP_Disconnecteur.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_Disconnecteur = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_Disconnecteur = 1)";
                    }

                }

                if (ChkCOP_Relevage.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_Relevage = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_Relevage = 1)";
                    }
                }
                if (chk0.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_RESEAU = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_RESEAU = 1)";
                    }
                }
                if (chk1.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_BOIS = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_BOIS = 1)";
                    }
                }

                if (chk2.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_CHAUFFAGE = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_CHAUFFAGE = 1)";
                    }
                }

                if (chk6.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_SOLAIRE = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_SOLAIRE = 1)";
                    }
                }

                if (chk3.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_RendCEE = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_RendCEE = 1)";
                    }
                }

                if (chk4.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_SFioul = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_SFioul = 1)";
                    }
                }

                if (ChkCOP_Ecs.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_Ecs = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_Ecs = 1)";
                    }
                }

                if (ChkCOP_CHAUCLIM.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (COP_ContratP2.COP_CHAUCLIM = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (COP_ContratP2.COP_CHAUCLIM = 1)";
                    }

                }

                if (COP_P1.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  ( Immeuble.P1 = 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  (Immeuble.P1 = 1) ";
                    }

                }

                if (COP_P2.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Immeuble.P2 = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Immeuble.P2 = 1)";
                    }
                }

                if (COP_P3.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Immeuble.P3 = 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Immeuble.P3 = 1) ";
                    }
                }

                if (COP_P4.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Immeuble.P4 = 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  (Immeuble.P4= 1) ";
                    }

                }
                sSQL = sSQL + sWhere;
                var ModAdo = new ModAdo();
                rsCtr = ModAdo.fc_OpenRecordSet(sSQL);
                ssCtr.DataSource = rsCtr;
                Cursor.Current = Cursors.Arrow;
                lblTotal.Text = "Tot.: " + rsCtr.Rows.Count;
                fc_SaveSetting();
                if (rsCtr.Rows.Count == 0)
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun contrat pour ces paramétres.", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                //ModAdo.Dispose();
                //ModAdo.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbContrat_Click");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssCtr_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            this.ssCtr.DisplayLayout.Bands[0].Columns["codeimmeuble"].Header.Caption = "Code Immeuble";
            this.ssCtr.DisplayLayout.Bands[0].Columns["ville"].Header.Caption = "Ville";
            this.ssCtr.DisplayLayout.Bands[0].Columns["Code1"].Header.Caption = "Gérant";
            this.ssCtr.DisplayLayout.Bands[0].Columns["COP_NoAuto"].Header.Caption = "N° Fiche GMAO";
            this.ssCtr.DisplayLayout.Bands[0].Columns["COP_Statut"].Header.Caption = "Statut";
            this.ssCtr.DisplayLayout.Bands[0].Columns["DateEffet"].Header.Caption = "Date Effet";
            this.ssCtr.DisplayLayout.Bands[0].Columns["DateFin"].Header.Caption = "Date Fin";
            this.ssCtr.DisplayLayout.Bands[0].Columns["Resiliee"].Header.Caption = "Resilié";
            this.ssCtr.DisplayLayout.Bands[0].Columns["COP_DateDemande"].Hidden = true;
            this.ssCtr.DisplayLayout.Bands[0].Columns["COP_DateImpression"].Hidden = true;
            this.ssCtr.DisplayLayout.Bands[0].Columns["COP_SuiviePar"].Hidden = true;
            this.ssCtr.DisplayLayout.Bands[0].Columns["COP_DateTaciteReconduction"].Hidden = true;
            this.ssCtr.DisplayLayout.Bands[0].Columns["COP_Objet"].Hidden = true;
            this.ssCtr.DisplayLayout.Bands[0].Columns["Nom"].Hidden = true;
            this.ssCtr.DisplayLayout.Bands[0].Columns["codeimmeuble"].CellActivation=Activation.NoEdit ;
            this.ssCtr.DisplayLayout.Bands[0].Columns["Code1"].CellActivation=Activation.NoEdit ;
            this.ssCtr.DisplayLayout.Bands[0].Columns["COP_NoAuto"].CellActivation=Activation.NoEdit ;
            this.ssCtr.DisplayLayout.Bands[0].Columns["COP_Statut"].CellActivation=Activation.NoEdit ;
            this.ssCtr.DisplayLayout.Bands[0].Columns["DateEffet"].CellActivation=Activation.NoEdit ;
            this.ssCtr.DisplayLayout.Bands[0].Columns["DateFin"].CellActivation=Activation.NoEdit ;
            this.ssCtr.DisplayLayout.Bands[0].Columns["P1"].CellActivation=Activation.NoEdit ;
            this.ssCtr.DisplayLayout.Bands[0].Columns["P2"].CellActivation=Activation.NoEdit ;
            this.ssCtr.DisplayLayout.Bands[0].Columns["P3"].CellActivation=Activation.NoEdit ;
            this.ssCtr.DisplayLayout.Bands[0].Columns["P4"].CellActivation=Activation.NoEdit ;
            this.ssCtr.DisplayLayout.Bands[0].Columns["Resiliee"].CellActivation=Activation.NoEdit ;
        }
        private void cmBUo_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (General.sAnalyAffTypeOp == "1")
            {
                sSQL = "SELECT     Personnel.Matricule, Personnel.Nom" + " " + " FROM         Personnel INNER JOIN " + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" + " Where (Qualification.Deviseur_QUA = 1) and " + " (Personnel.NonActif is null or Personnel.NonActif = 0) " + " order by Personnel.Nom ";
                sheridan.InitialiseCombo(cmBUo, sSQL, "Matricule");
            }
            else
            {
                sSQL = "";
                sSQL = "Select UO_CODE, UO_Libelle from UO_UO ";
                sheridan.InitialiseCombo(cmBUo, sSQL, "UO_CODE");
            }
        }

        private void cmBUo_Validating(object sender, CancelEventArgs e)
        {
            bool Cancel = e.Cancel;
            //If sAnalyAffTypeOp = "1" Then
            //    lblUO_Libelle = fc_ADOlibelle("SELECT NOM FROM PERSONNEL WHERE matricule ='" & cmBUo.Text & "'")
            //
            //Else
            //
            //    lblUO_Libelle = fc_ADOlibelle("SELECT UO_Libelle FROM UO_UO Where UO_Code ='" & cmBUo.Text & "'")
            //End If
            e.Cancel = Cancel;
        }

        private void cmdFindSuiviePar_Click(object sender, EventArgs e)
        {
            //txtCOP_SuiviePar_KeyPress(txtCOP_SuiviePar, new System.Windows.Forms.KeyPressEventArgs((char)13));
        }

       /// <summary>
       /// TESTED
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        private void cmdInit_Click(object sender, EventArgs e)
        {
            txtAdresseIm.Text = "";
            txtCodeImmeuble.Text = "";
            txtCode1.Text = "";
            txtRacineGerant.Text = "";
            txtCOP_Statut.Text = "";
            txtCOP_Statut1.Text = "";
            //txtSEC_Code = ""
            txtCOP_DateDemandeDe.Text = "";
            txtCOP_DateDemandeAu.Text = "";
            txtCOP_DateImpressionAu.Text = "";
            txtCOP_DateImpressionDe.Text = "";
            txtCOP_DateEffetAu.Text = "";
            txtCOP_DateEffetDe.Text = "";
            txtCOP_DateFinDe.Text = "";
            txtCOP_DateFinAu.Text = "";
            txtCOP_SuiviePar.Text = "";
            txtCOP_Puissance.Text = "";
            lblCOP_SuiviePar.Text = "";
            txtCOP_DateTaciteReconductionDe.Text = "";
            txtCOP_DateTaciteReconductionAu.Text = "";
            txtCOP_Objet.Text = "";
            txtIntervenant.Text = "";
            txtCPImm.Text = "";
            txtVilleImm.Text = "";
            txtCOP_DateAcceptationDe.Text = "";
            txtCOP_DateAcceptationA.Text = "";
            txtCOP_DateDemandeResiliationDe.Text = "";
            txtCOP_DateDemandeResiliationA.Text = "";
            cmBUo.Text = "";
            //OleCombo = ""
            //txtLibAffiliationAstreinte = ""
            lblVille.Text = "";
            lblLibInterv.Text = "";
            txtCodeChefSecteur.Text = "";
            lblCodeChefSecteur.Text = "";
            //txtAmianteAu = ""
            //txtAmianteDu = ""
            //optDemandeTous.value = True
            //optPresenceTous.value = True

            chkCOP_FIOUL.CheckState = CheckState.Unchecked;
            chkCOP_ELECT.CheckState = CheckState.Unchecked;
            chkCOP_SURP.CheckState = CheckState.Unchecked;
            chkCOP_Murale.CheckState = CheckState.Unchecked;
            chkCOP_Gaz.CheckState = CheckState.Unchecked;
            chkCOP_CLIM.CheckState = CheckState.Unchecked;
            chkCOP_TE.CheckState = CheckState.Unchecked;
            chkCOP_TeleAlarme.CheckState = CheckState.Unchecked;
            chkCOP_CPCU.CheckState = CheckState.Unchecked;
            chkCOP_VMC.CheckState = CheckState.Unchecked;
            ChkCOP_EnAttente.CheckState = CheckState.Unchecked;
            chkCOP_Disconnecteur.CheckState = CheckState.Unchecked;
            ChkCOP_Relevage.CheckState = CheckState.Unchecked;
            ChkCOP_CHAUCLIM.CheckState = CheckState.Unchecked;
            chkCOP_VmcDsc.CheckState = CheckState.Unchecked;

            chk0.CheckState = CheckState.Unchecked;
            chk1.CheckState = CheckState.Unchecked;
            chk2.CheckState = CheckState.Unchecked;
            chk6.CheckState = CheckState.Unchecked;
            chk3.CheckState = CheckState.Unchecked;
            chk4.CheckState = CheckState.Unchecked;
            ChkCOP_Ecs.CheckState = CheckState.Unchecked;

            COP_P1.CheckState = CheckState.Unchecked;
            COP_P2.CheckState = CheckState.Unchecked;
            COP_P3.CheckState = CheckState.Unchecked;
            COP_P4.CheckState = CheckState.Unchecked;

            cmbCommercial.Value = "";
            cmbCommercial.Text = "";
            txtINT_AnaCode.Value = "";
            txtINT_AnaCode.Text = "";
            lblLibCommercial.Text = "";
            lblExploit.Text = "";
        }
        
        private void cmdIntervenant_Click(object sender, EventArgs e)
        {
            //sSQL = "SELECT Personnel.Matricule as [Matricule], Personnel.Nom as [NOM],Personnel.prenom as [Prenom], Qualification.CodeQualif as [Code Qualif]," + " Qualification.Qualification as [Qualification], Personnel.NumRadio, Personnel.Note_NOT" + " FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
            //string sCode = "";
            //var _with2 = RechercheMultiCritere;
            //_with2.stxt.Text = "SELECT Personnel.Matricule as [Matricule ]," + " Personnel.Nom as [Nom]," + " Personnel.prenom as [Prenom]," + " Qualification.Qualification as [Qualification]," + " Personnel.NumRadio as [Kobby]" + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
            //_with2.txtWhere.Text = " where (Personnel.NonActif is null or Personnel.NonActif = 0) ";
            //_with2.txtNomFicheAppelante.Text = this.Name + "cmdRechercheIntervenant_Click";
            //_with2.ShowDialog();
            //if (!string.IsNullOrEmpty(General.nz(ref ref _with2.LigneChoisie, ref ref "")))
            //{
            //    sCode = _with2.dbgrid1.Columns(0).value;
            //    txtIntervenant.Text = sCode;
            //    lblLibInterv.Text = _with2.dbgrid1.Columns(1).value + " " + _with2.dbgrid1.Columns(2).value;
            //}
            //else
            //{
            //    return;
            //}
            //RechercheMultiCritere.Close();
            //return;
        }

       /// <summary>
       /// TESTED
       /// </summary>
       /// <param name="sender"></param>
       /// <param name="e"></param>
        private void cmdRechercheAdresse_Click(object sender, EventArgs e)
        {
            try
            {
                txtAdresseIm_KeyPress(txtAdresseIm, new System.Windows.Forms.KeyPressEventArgs((char)13));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheAdresse_Click");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheCommercial_Click(object sender, EventArgs e)
        {
            try
            {
                cmbCommercial_KeyPress(cmbCommercial, new KeyPressEventArgs((char)13));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbCommercial_KeyPress");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            try
            {
                txtCodeImmeuble_KeyPress(txtCodeImmeuble, new System.Windows.Forms.KeyPressEventArgs((char)13));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheImmeuble_Click");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheVilleImm_Click(object sender, EventArgs e)
        {
            try
            {
                txtCPImm_KeyPress(txtCPImm, new System.Windows.Forms.KeyPressEventArgs((char)13));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheImmeuble_Click");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            Excel.Application oXL = default(Excel.Application);
            Excel.Workbook oWB = default(Excel.Workbook);
            Excel.Worksheet oSheet = default(Excel.Worksheet);
            Excel.Range oRng = default(Excel.Range);
            Excel.Range oResizeRange = default(Excel.Range);
            var rsExport = default(DataTable);
            try
            {
                int i = 0;
                int j = 0;
                string sMatricule = null;
                int lLigne = 0;
                Cursor.Current = Cursors.WaitCursor;
                oXL = new Excel.Application();
                oXL.Visible = false;
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;
                using (ModAdo modAdo = new ModAdo())
                {
                    //rsExport = modAdo.fc_OpenRecordSet(rsCtr.source)
                    rsExport = rsCtr;
                    if (rsExport.Rows.Count == 0)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour ces critères.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    i = 1;
                    foreach (DataColumn oField in rsExport.Columns)
                    {
                        oSheet.Cells[1, i].VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                        oSheet.Cells[1, i].value = oField.ColumnName;
                        i++;
                    }
                    Cursor.Current = Cursors.WaitCursor;

                    oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                    oResizeRange.Interior.ColorIndex = 15;
                    oResizeRange.Font.Bold = true;
                    oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                    oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                    lLigne = 3;
                    Cursor.Current = Cursors.WaitCursor;
                    foreach (DataRow rsExportRow in rsExport.Rows)
                    {
                        j = 1;
                        foreach (DataColumn oField in rsExport.Columns)
                        {
                            oSheet.Cells[lLigne, j + 0].value = rsExportRow[oField.ColumnName] + "";
                            j++;
                        }
                        lLigne += 1;
                        oResizeRange.Font.Bold = true;

                        //_with14.Close();
                    }
                    oXL.Visible = true;
                    oXL.UserControl = true;
                    oRng = null;
                    oSheet = null;
                    oWB = null;
                    //oXL.Quit();
                    oXL = null;

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    rsExport = null;
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Error: " + ex.Message, "Error: " + ex.HResult, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command2_Click(object sender, EventArgs e)
        {
            string sCode = "";
            try
            {
                txtCode1_KeyPress(txtCode1, new System.Windows.Forms.KeyPressEventArgs((char)13));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command2_Click");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command3_Click(object sender, EventArgs e)
        {
            string sCode = "";
            try
            {
                txtCOP_Statut_KeyPress(txtCOP_Statut, new System.Windows.Forms.KeyPressEventArgs((char)13));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command3_Click");
            }
        }
        
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command5_Click(object sender, EventArgs e)
        {
            try
            {
                txtVilleImm_KeyPress(txtVilleImm, new System.Windows.Forms.KeyPressEventArgs((char)13));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheImmeuble_Click");
            }
        }

        //this button his parent his visibility is false.
        private void Command6_Click(object sender, EventArgs e)
        {
            //sSQL = "SELECT Personnel.Matricule as [Matricule], Personnel.Nom as [NOM],Personnel.prenom as [Prenom], Qualification.CodeQualif as [Code Qualif]," + " Qualification.Qualification as [Qualification], Personnel.NumRadio, Personnel.Note_NOT" + " FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
            //string sCode = null;
            ////On Error GoTo Erreur

            //var _with4 = RechercheMultiCritere;
            //_with4.stxt.Text = "SELECT Personnel.Matricule as [Matricule ]," + " Personnel.Nom as [Nom]," + " Personnel.prenom as [Prenom]," + " Qualification.Qualification as [Qualification]," + " Personnel.NumRadio as [Kobby]" + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
            //_with4.txtWhere.Text = " where (Personnel.NonActif is null or Personnel.NonActif = 0)";
            //_with4.txtNomFicheAppelante.Text = this.Name + "cmdRechercheIntervenant_Click";
            ////.Move Screen.Width - .Width, (Screen.Height - 15 * Screen.TwipsPerPixelY) - .Height
            ////.Move Me.Left + cleftForm, (Screen.Height - 15 * Screen.TwipsPerPixelY) - .Height
            //_with4.ShowDialog();
            ////UPGRADE_WARNING: Couldn't resolve default property of object nz(RechercheMultiCritere.LigneChoisie, ). Click for more: 'ms-help://MS.VSCC.v90/dv_commoner/local/redirect.htm?keyword="6A50421D-15FE-4896-8A1B-2EC21E9037B2"'
            //if (!string.IsNullOrEmpty(General.nz(ref ref _with4.LigneChoisie, ref ref "")))
            //{
            //    // charge les enregistrements de l'immeuble
            //    txtCodeChefSecteur.Text = _with4.dbgrid1.Columns(0).value;
            //    //txtCOP_SuiviePar = sCode
            //    lblCodeChefSecteur.Text = _with4.dbgrid1.Columns(1).value + " " + _with4.dbgrid1.Columns(2).value;
            //}
            //else
            //{
            //    return;
            //}
            //RechercheMultiCritere.Close();
            return;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command7_Click(object sender, EventArgs e)
        {
            try
            {
                txtCOP_Statut1_KeyPress(txtCOP_Statut1, new System.Windows.Forms.KeyPressEventArgs((char)13));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCOP_Statut1_KeyPress");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocTriContat_VisibleChanged(object sender, EventArgs e)
        {
            int lCop_NoAuto = 0;
            try
            {
                if (!Visible)
                    return;

                View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocTriContrat");
                txtCodeImmeuble.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "CodeImmeuble", "");
                txtCode1.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "Code1", "");
                txtRacineGerant.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "txtRacineGerant", "").Replace("%", "*");
                txtCOP_Statut.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_Statut", "");
                txtCOP_Statut1.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_Statut1", "");
                txtCOP_DateDemandeDe.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_DateDemandeDe", "");
                txtCOP_DateDemandeAu.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_DateDemandeAu", "");
                txtCOP_DateImpressionAu.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_DateImpressionAu", "");
                txtCOP_DateImpressionDe.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_DateImpressionDe", "");
                txtCOP_DateEffetAu.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_DateEffetAu", "");
                txtCOP_DateEffetDe.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_DateEffetDe", "");
                txtCOP_DateFinDe.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_DateFinDe", "");
                txtCOP_DateFinAu.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_DateFinAu", "");
                txtCOP_SuiviePar.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_SuiviePar", "");
                txtCOP_Puissance.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "txtCOP_Puissance", "");
                Combo.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "combo", "");

                cmbCommercial.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "cmbCommercial", "");
                txtINT_AnaCode.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "txtINT_AnaCode", "");

                txtCOP_Objet.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_Objet", "");
                txtCOP_DateTaciteReconductionDe.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_DateTaciteReconductionDe", "");
                txtCOP_DateTaciteReconductionAu.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_DateTaciteReconductionAu", "");
                txtIntervenant.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "Intervenant", "");
                cmBUo.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "UO", "");

                txtCOP_DateAcceptationDe.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_DateAcceptationDe", "");
                txtCOP_DateAcceptationA.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_DateAcceptationA", "");
                txtCOP_DateDemandeResiliationDe.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_DateDemandeResiliationDe", "");
                txtCOP_DateDemandeResiliationA.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_DateDemandeResiliationA", "");

                txtCPImm.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "CPImm", "");
                lblVille.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "Ville", "");
                txtVilleImm.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "VilleImm", "");
                chkCOP_FIOUL.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "FIOUL", "0")));
                chkCOP_ELECT.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "ELECT", "0")));
                chkCOP_SURP.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "SURP", "0")));
                chkCOP_Murale.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "Murale", "0")));
                chkCOP_Gaz.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "Gaz", "0")));
                chkCOP_CLIM.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "CLIM", "0")));
                chkCOP_TE.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "TE", "0")));
                chkCOP_TeleAlarme.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "TeleAlarme", "0")));
                chkCOP_CPCU.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "CPCU", "0")));
                chkCOP_VMC.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "VMC", "0")));
                ChkCOP_EnAttente.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "EnAttente", "0")));
                ChkCOP_Ecs.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "ecs", "0")));
                chk0.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "reseau", "0")));
                chk1.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "bois", "0")));
                chk2.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "Cop_chauffage", "0")));
                chk6.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_SOLAIRE", "0")));
                chk3.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_RendCEE", "0")));
                chk4.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_SFioul", "0")));

                chkCOP_Disconnecteur.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "Disconnecteur", "0")));
                ChkCOP_Relevage.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "Relevage", "0")));
                ChkCOP_CHAUCLIM.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_CHAUCLIM", "0")));
                chkCOP_VmcDsc.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_VmcDsc", "0")));
                COP_P1.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "P1", "0")));
                COP_P2.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "P2", "0")));
                COP_P3.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "P3", "0")));
                COP_P4.CheckState = (CheckState)(Convert.ToInt32(General.getFrmReg(ModConstantes.cUserDocTriContrat, "P4", "0")));
                txtCodeChefSecteur.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "CodeChefSecteur", "");
                txtAdresseIm.Text = General.getFrmReg(ModConstantes.cUserDocTriContrat, "Adresse", "");
                lCop_NoAuto = Convert.ToInt32(General.nz(General.getFrmReg(ModConstantes.cUserDocTriContrat, "COP_NoAuto", "0"), 0));
                //TESTED
                cmbContrat_Click(cmbContrat, null);//TESTED
                if (lCop_NoAuto != 0)//TESTED
                    if ((rsCtr != null))
                        if (rsCtr.Rows.Count > 0)
                            ssCtr.Rows.Where(r => Convert.ToInt32(r.GetCellValue("COP_NoAuto").ToString()) == lCop_NoAuto).ToList().ForEach(r => r.Selected = true);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, ModConstantes.cUserDocTriContrat + ";UserDocTriContat_VisibleChanged");
            }
        }

        private void ssCtr_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            fc_SaveSetting();
            ModParametre.fc_SaveParamPosition("", Variable.cUserDocP2, e.Row.GetCellValue("COP_NoAuto").ToString());
          View.Theme.Theme.Navigate(typeof(UserDocP2V2)); //TO DO doesn't exist .must developped
        }
      
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void fc_SaveSetting()
        {
            if (ssCtr.ActiveRow != null)
                General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_NoAuto", ssCtr.ActiveRow.GetCellValue("COP_NoAuto") + "");
            General.saveInReg(ModConstantes.cUserDocTriContrat, "CodeImmeuble", txtCodeImmeuble.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "Code1", txtCode1.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "txtRacineGerant", General.fc_racineGerant(txtRacineGerant.Text));
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_Statut", txtCOP_Statut.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_Statut1", txtCOP_Statut1.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateDemandeDe", txtCOP_DateDemandeDe.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateDemandeAu", txtCOP_DateDemandeAu.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateImpressionAu", txtCOP_DateImpressionAu.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateImpressionDe", txtCOP_DateImpressionDe.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateEffetAu", txtCOP_DateEffetAu.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateEffetDe", txtCOP_DateEffetDe.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateFinDe", txtCOP_DateFinDe.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateFinAu", txtCOP_DateFinAu.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_SuiviePar", txtCOP_SuiviePar.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "txtCOP_Puissance", txtCOP_Puissance.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "combo", Combo.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateAcceptationDe", txtCOP_DateAcceptationDe.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateAcceptationA", txtCOP_DateAcceptationA.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateDemandeResiliationDe", txtCOP_DateDemandeResiliationDe.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateDemandeResiliationA", txtCOP_DateDemandeResiliationA.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateDemandeResiliationA", txtCOP_DateDemandeResiliationA.Text);
            //in vb they are stock the value of the checkbox so it's 1 or 0 .
            General.saveInReg(ModConstantes.cUserDocTriContrat, "FIOUL", Convert.ToInt32(chkCOP_FIOUL.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "ELECT", Convert.ToInt32(chkCOP_ELECT.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "SURP", Convert.ToInt32(chkCOP_SURP.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "Murale", Convert.ToInt32(chkCOP_Murale.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "Gaz", Convert.ToInt32(chkCOP_Gaz.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "CLIM", Convert.ToInt32(chkCOP_CLIM.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "TE", Convert.ToInt32(chkCOP_TE.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "TeleAlarme", Convert.ToInt32(chkCOP_TeleAlarme.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "CPCU", Convert.ToInt32(chkCOP_CPCU.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "VMC", Convert.ToInt32(chkCOP_VMC.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "EnAttente", Convert.ToInt32(ChkCOP_EnAttente.CheckState).ToString());
            //General.saveInReg(ModConstantes.cUserDocTriContrat, "EnAttente", Convert.ToInt32( chkCOP_EnAttente.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "ecs", Convert.ToInt32(ChkCOP_Ecs.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "reseau", Convert.ToInt32(chk0.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "bois", Convert.ToInt32(chk1.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "Cop_chauffage", Convert.ToInt32(chk2.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_SOLAIRE", Convert.ToInt32(chk6.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_RendCEE", Convert.ToInt32(chk3.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_SFioul", Convert.ToInt32(chk4.CheckState).ToString());

            General.saveInReg(ModConstantes.cUserDocTriContrat, "Disconnecteur", Convert.ToInt32(chkCOP_Disconnecteur.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "Relevage", Convert.ToInt32(ChkCOP_Relevage.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_CHAUCLIM", Convert.ToInt32(ChkCOP_CHAUCLIM.CheckState).ToString());

            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_VmcDsc", Convert.ToInt32(chkCOP_VmcDsc.CheckState).ToString());


            General.saveInReg(ModConstantes.cUserDocTriContrat, "P1", Convert.ToInt32(COP_P1.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "P2", Convert.ToInt32(COP_P2.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "P3", Convert.ToInt32(COP_P3.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "P4", Convert.ToInt32(COP_P4.CheckState).ToString());
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_Objet", txtCOP_Objet.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateTaciteReconductionDe", txtCOP_DateTaciteReconductionDe.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "COP_DateTaciteReconductionAu", txtCOP_DateTaciteReconductionAu.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "Intervenant", txtIntervenant.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "UO", cmBUo.Text);

            General.saveInReg(ModConstantes.cUserDocTriContrat, "CPImm", txtCPImm.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "Ville", lblVille.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "VilleImm", txtVilleImm.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "CodeChefSecteur", txtCodeChefSecteur.Text);

            General.saveInReg(ModConstantes.cUserDocTriContrat, "cmbCommercial", cmbCommercial.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "txtINT_AnaCode", txtINT_AnaCode.Text);
            General.saveInReg(ModConstantes.cUserDocTriContrat, "Adresse", txtAdresseIm.Text);

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtAdresseIm_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                short KeyAscii = Convert.ToInt16(e.KeyChar);
                if (KeyAscii == 13)
                {
                    string ModAdo_fc_ADOlibelle;
                    using (ModAdo modAdo = new ModAdo())
                    {
                        ModAdo_fc_ADOlibelle = modAdo.fc_ADOlibelle("SELECT Immeuble.Adresse FROM Immeuble WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtAdresseIm.Text) + "'");
                    }
                    if (!!string.IsNullOrEmpty(General.nz(ModAdo_fc_ADOlibelle, "").ToString()))
                    {
                        string req = "SELECT " + " adresse as \"Adresse\", CodePostal as \"Code Postal\",   " + " Ville as \"Ville\"" + " " + " FROM Immeuble ";
                        SearchTemplate st = new SearchTemplate(this, null, req, "", "") { Text = "Recherche d'un immeuble" };
                        st.SetValues(new Dictionary<string, string> { { "adresse", txtAdresseIm.Text } });
                        st.ugResultat.DoubleClickRow += (se, ev) =>
                          {
                              txtAdresseIm.Text = ev.Row.GetCellValue("Adresse") + "";
                              st.Dispose();
                              st.Close();
                          };
                        st.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13)
                            {
                                if (st.ugResultat.ActiveRow != null)
                                {
                                    txtAdresseIm.Text = st.ugResultat.ActiveRow.GetCellValue("Adresse") + "";
                                    st.Dispose();
                                    st.Close();
                                }
                            }
                        };
                        st.StartPosition = FormStartPosition.CenterParent;
                        st.ShowDialog();
                    }
                    cmbContrat_Click(cmbContrat, null);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtAdresseIm_KeyPress");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCode1_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                short KeyAscii = Convert.ToInt16(e.KeyChar);
                if (KeyAscii == 13)
                {
                    string ModAdo_fc_ADOlibelle;
                    using (ModAdo modAdo = new ModAdo())
                    {
                        ModAdo_fc_ADOlibelle = modAdo.fc_ADOlibelle("SELECT Table1.Code1 FROM Table1 WHERE Table1.Code1 LIKE '%" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "%'");
                    }
                    if (!!string.IsNullOrEmpty(General.nz(ModAdo_fc_ADOlibelle, "").ToString()))
                    {
                        string req = "SELECT " + "Code1 as \"Nom Directeur\", Nom as \"Raison sociale\", Ville as \"Ville\" FROM Table1 ";
                        SearchTemplate st = new SearchTemplate(this, null, req, "", "") { Text = "Recherche d'un client" };
                        st.SetValues(new Dictionary<string, string> { { "Code1", txtCode1.Text } });
                        st.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtCode1.Text = ev.Row.GetCellValue("Nom Directeur") + "";
                            st.Dispose();
                            st.Close();
                        };
                        st.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13)
                            {
                                if (st.ugResultat.ActiveRow != null)
                                {
                                    txtCode1.Text = st.ugResultat.ActiveRow.GetCellValue("Nom Directeur") + "";
                                    st.Dispose();
                                    st.Close();
                                }
                            }
                        };
                        st.StartPosition = FormStartPosition.CenterParent;
                        st.ShowDialog();
                    }
                    cmbContrat_Click(cmbContrat, null);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCode1_KeyPress");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeImmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                short KeyAscii = Convert.ToInt16(e.KeyChar);
                if (KeyAscii == 13)
                {
                    string ModAdo_fc_ADOlibelle;
                    using (ModAdo modAdo = new ModAdo())
                    {
                        string req = "SELECT Immeuble.CodeImmeuble FROM Immeuble WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
                        ModAdo_fc_ADOlibelle = modAdo.fc_ADOlibelle(req);
                    }
                    if (!!string.IsNullOrEmpty(General.nz(ModAdo_fc_ADOlibelle, "").ToString()))
                    {
                        string req = "SELECT CodeImmeuble as \"Code Immeuble\"," + " Adresse as \"Adresse\"," + " Ville as \"Ville\" , anglerue as \"Angle de rue\", Code1 as \"Gerant\" FROM Immeuble";
                        SearchTemplate st = new SearchTemplate(this, null, req, "", "") { Text = "Recherche d'un immeuble" };
                        st.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeuble.Text } });
                        st.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtCodeImmeuble.Text = ev.Row.GetCellValue("Code Immeuble") + "";
                            st.Dispose();
                            st.Close();
                        };
                        st.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short) ev.KeyChar == 13)
                            {
                                if (st.ugResultat.ActiveRow != null)
                                {
                                    txtCodeImmeuble.Text = st.ugResultat.ActiveRow.GetCellValue("Code Immeuble") + "";
                                    st.Dispose();
                                    st.Close();
                                }
                            }
                        };
                        st.StartPosition = FormStartPosition.CenterParent;
                        st.ShowDialog();
                    }
                    cmbContrat_Click(cmbContrat, null);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCodeImmeuble_KeyPress");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocTriContat_Load(object sender, EventArgs e)
        {
            int lCop_NoAuto = 0;
            //ModMain.fc_Color(this);
            //fc_LoadCombo();
        }

        //this function fill combo. the combo parent his visibility is false.
        private void fc_LoadCombo()
        {
            object sAddItem = null;
            try
            {
                //Combo.DataMode = SSDataWidgets_B_OLEDB.Constants_DataMode.ssDataModeAddItem;
                //Combo.FieldDelimiter = "!";
                //Combo.FieldSeparator = ";";
                //Combo.DataFieldList = "Column 0";
                //Combo.RemoveAll();
                //Combo.Columns[0].Name = "Opérateur";

            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCOP_DateDemandeDe_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                cmbContrat_Click(cmbContrat, null);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCOP_DateDemandeAu_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                cmbContrat_Click(cmbContrat, null);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCOP_DateImpressionAu_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                cmbContrat_Click(cmbContrat, null);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCOP_DateImpressionDe_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                cmbContrat_Click(cmbContrat, null);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCOP_Statut_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                if (KeyAscii == 13)
                {
                    string ModAdo_fc_ADOlibelle;
                    using (ModAdo modAdo = new ModAdo())
                    {
                        ModAdo_fc_ADOlibelle = modAdo.fc_ADOlibelle("SELECT  ETC_Code FROM ETC_Contrat WHERE  ETC_Code LIKE '%" + StdSQLchaine.gFr_DoublerQuote(txtCOP_Statut.Text) + "%'");
                    }
                    if (!!string.IsNullOrEmpty(General.nz(ModAdo_fc_ADOlibelle, "").ToString()))
                    {
                        string req = "SELECT  ETC_Code as \"Code\" , ETC_Libelle as \"Libelle\"" + " FROM  ETC_Contrat";
                        SearchTemplate st = new SearchTemplate(this, null, req, "", "") { Text = "Recherche d'un statut" };
                        st.SetValues(new Dictionary<string, string> { { "ETC_Libelle", txtCOP_Statut.Text } });
                        st.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtCOP_Statut.Text = ev.Row.GetCellValue("Code") + "";
                            st.Dispose();
                            st.Close();
                        };
                        st.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13)
                            {
                                if (st.ugResultat.ActiveRow != null)
                                {
                                    txtCOP_Statut.Text = st.ugResultat.ActiveRow.GetCellValue("Code")+"";
                                    st.Dispose();
                                    st.Close();
                                }
                            }
                        };
                        st.StartPosition = FormStartPosition.CenterParent;
                        st.ShowDialog();
                    }
                    cmbContrat_Click(cmbContrat, null);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCOP_Statut_KeyPress");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCOP_Statut1_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                if (KeyAscii == 13)
                {
                    string ModAdo_fc_ADOlibelle;
                    using (ModAdo modAdo = new ModAdo())
                    {
                        ModAdo_fc_ADOlibelle = modAdo.fc_ADOlibelle("SELECT  ETC_Code FROM ETC_Contrat WHERE  ETC_Code LIKE '%" + StdSQLchaine.gFr_DoublerQuote(txtCOP_Statut1.Text) + "%'");
                    }
                    if (!!string.IsNullOrEmpty(General.nz(ModAdo_fc_ADOlibelle, "").ToString()))
                    {
                        string req = "SELECT  ETC_Code as \"Code\" , ETC_Libelle as \"Libelle\"" + " FROM  ETC_Contrat";
                        SearchTemplate st = new SearchTemplate(this, null, req, "", "") { Text = "Recherche d'un statut" };
                        st.SetValues(new Dictionary<string, string> { { "ETC_Libelle", txtCOP_Statut1.Text } });
                        st.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtCOP_Statut1.Text = ev.Row.GetCellValue("Code") + "";
                            st.Dispose();
                            st.Close();
                        };

                        st.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13)
                            {
                                if (st.ugResultat.ActiveRow != null)
                                {
                                    txtCOP_Statut1.Text = st.ugResultat.ActiveRow.GetCellValue("Code") + "";
                                    st.Dispose();
                                    st.Close();
                                }
                            }
                        };
                        st.StartPosition = FormStartPosition.CenterParent;
                        st.ShowDialog();
                    }
                    cmbContrat_Click(cmbContrat, null);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCOP_Statut1_KeyPress");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCPImm_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                if (KeyAscii == 13)
                {
                    string ModAdo_fc_ADOlibelle;
                    using (ModAdo modAdo = new ModAdo())
                    {
                        ModAdo_fc_ADOlibelle = modAdo.fc_ADOlibelle("SELECT Immeuble.CodePostal FROM Immeuble WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCPImm.Text) + "'");
                    }
                    if (!!string.IsNullOrEmpty(General.nz(ModAdo_fc_ADOlibelle, "").ToString()))
                    {
                        string req = "SELECT " + " distinct CodePostal as \"Code Postal\",   " + " Ville as \"Ville\"" + " " + " FROM Immeuble ";
                        SearchTemplate st = new SearchTemplate(this, null, req, "", "") { Text = "Recherche d'un statut" };
                        st.SetValues(new Dictionary<string, string> { { "CodePostal", txtCPImm.Text } });
                        st.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtCPImm.Text = ev.Row.GetCellValue("Code Postal") + "";
                            st.Dispose();
                            st.Close();
                        };
                        st.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13)
                            {
                                if (st.ugResultat.ActiveRow != null)
                                {
                                    txtCPImm.Text = st.ugResultat.ActiveRow.GetCellValue("Code Postal") + "";
                                    st.Dispose();
                                    st.Close();
                                }
                            }
                        };
                        st.StartPosition = FormStartPosition.CenterParent;
                        st.ShowDialog();
                    }
                    cmbContrat_Click(cmbContrat, null);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCPImm_KeyPress");
            }
        }

        private void txtINT_AnaCode_AfterCloseUp(object sender, EventArgs e)
        {
            using (var modAdo = new ModAdo())
            {
                lblExploit.Text = modAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + StdSQLchaine.gFr_DoublerQuote(txtINT_AnaCode.Text) + "'");
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtINT_AnaCode_BeforeDropDown(object sender, CancelEventArgs e)
        {
            sSQL = "";
            sSQL = "SELECT MATRICULE, Nom, Prenom";
            sSQL = sSQL + " FROM Personnel INNER JOIN SpecifQualif ON ";
            sSQL = sSQL + " Personnel.CodeQualif=SpecifQualif.CodeQualif ";
            sSQL = sSQL + " WHERE SpecifQualif.QualifRExp=1";
            sSQL = sSQL + " and (NonActif is null or NonActif = 0)";
            sheridan.InitialiseCombo(txtINT_AnaCode, sSQL, "MATRICULE");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtINT_AnaCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                cmbContrat_Click(cmbContrat, null);
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtVilleImm_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                if (KeyAscii == 13)
                {
                    string ModAdo_fc_ADOlibelle;
                    using (ModAdo modAdo = new ModAdo())
                    {
                        ModAdo_fc_ADOlibelle = modAdo.fc_ADOlibelle("SELECT Immeuble.Ville FROM Immeuble WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtVilleImm.Text) + "'");
                    }
                    if (!!string.IsNullOrEmpty(General.nz(ModAdo_fc_ADOlibelle, "").ToString()))
                    {
                        string req = "SELECT " + " distinct   " + " Ville as \"Ville\"" + " " + " FROM Immeuble";
                        SearchTemplate st = new SearchTemplate(this, null, req, "", "") { Text = "Recherche d'un immeuble" };
                        st.SetValues(new Dictionary<string, string> { { "Ville", txtVilleImm.Text } });
                        st.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtVilleImm.Text = ev.Row.GetCellValue("Ville") + "";
                            st.Dispose();
                            st.Close();
                        };
                        st.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13)
                            {
                                if (st.ugResultat.ActiveRow != null)
                                {
                                    txtVilleImm.Text = st.ugResultat.ActiveRow.GetCellValue("Ville") + "";
                                    st.Dispose();
                                    st.Close();
                                }
                            }
                        };
                        st.StartPosition = FormStartPosition.CenterParent;
                        st.ShowDialog();
                    }
                    cmbContrat_Click(cmbContrat, null);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCPImm_KeyPress");
            }
        }
    }



}
