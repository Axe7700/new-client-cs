﻿//using Microsoft.VisualBasic;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Analytique;
using Axe_interDT.Views.BaseDeDonnees.Gerant;
using Axe_interDT.Views.BaseDeDonnees.Immeuble;
using Axe_interDT.Views.Contrat;
using Axe_interDT.Views.SharedViews;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;

namespace Axe_interDT.Views.Facturations.VisualiserUneFacture
{
    public partial class UserDocPrintFacture : UserControl
    {
        ModAdo ModAdo1 = new ModAdo();
        public UserDocPrintFacture()
        {
            InitializeComponent();
        }
        //Fiche Analytique not exist
        private void cmdAnalytique_Click(System.Object eventSender, System.EventArgs eventArgs)
        {

            DataTable rs;
            string sSQL = null;
            int lnumfichestandard = 0;


            try
            {
                if (optIntervention.Checked == true || optDevis.Checked == true)
                {
                    sSQL = "SELECT     NumFicheStandard " + " From Intervention "
                        + " WHERE NoFacture = '"
                        + StdSQLchaine.gFr_DoublerQuote(General.Trim(txtNoFacture.Text)) + "'";

                    rs = ModAdo1.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)
                    {
                        lnumfichestandard = Convert.ToInt32(General.nz(rs.Rows[0]["Numfichestandard"], 0));
                    }
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'analytique ne concerne que les factures d'intervention et de devis.");
                    return;
                }

                if (lnumfichestandard != 0)
                {
                    //=== stocke la position de la fiche intervention// txtNointervention not exest!!
                    ///ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, txtNointervention);
                    //===  stocke les paramétres pour la feuille de destination.
                    General.saveInReg(Variable.cUserDocAnalytique2, "NewVar", Convert.ToString(lnumfichestandard));
                    General.saveInReg(Variable.cUserDocAnalytique2, "CodeImmeuble", "");
                    General.saveInReg(Variable.cUserDocAnalytique2, "Gerant", "");

                    View.Theme.Theme.Navigate(typeof(UserDocAnalytique2));
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Navigation annulée, aucune analytique pour cette facture.", "Navigation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";cmdAnalytique_Click");
            }

        }
        //tested
        private void cmdRechercheFacture_Click(Object eventSender, EventArgs eventArgs)
        {
            string requete = "";
            string where = "";


            if (optContrat.Checked == true)
            {
                requete = "SELECT FactEntete.NoFacture AS \"NoFacture\", FactEntete.CodeImmeuble AS \"Code Immeuble\", Immeuble.Code1 AS \"Code Gérant\", FactEntete.NumContrat AS \"N° Contrat\", FactEntete.Avenant AS \"Avenant\" FROM FactEntete INNER JOIN Immeuble ON FactEntete.CodeImmeuble=Immeuble.CodeImmeuble";
            }
            else if (optIntervention.Checked == true)
            {
                requete = "SELECT FactureManuelleEntete.NoFacture as \"NoFacture\", FactureManuelleEntete.NomIntervention as \"CodeImmeuble\", FactureManuelleEntete.DateFacture as \"DateFacture\", FactureManuelleEntete.Client as \"Gérant\" From FactureManuelleEntete";
                where = "FactureManuelleEntete.TypeFacture ='TA'";
            }
            else if (optDevis.Checked == true)
            {
                requete = "SELECT FactureManuelleEntete.NoFacture as \"NoFacture\", FactureManuelleEntete.NomIntervention as \"CodeImmeuble\", FactureManuelleEntete.DateFacture as \"DateFacture\", FactureManuelleEntete.Client as \"Gérant\", GestionStandard.NoDevis AS \"Numero Devis\" From FactureManuelleEntete ";
                requete = requete + " INNER JOIN Intervention ON FactureManuelleEntete.NoFacture=Intervention.NoFacture INNER JOIN GestionStandard ON Intervention.NumFicheStandard=GestionStandard.NumFicheStandard ";
                where = "FactureManuelleEntete.TypeFacture ='TA'";
            }
            else if (optManu.Checked == true)
            {
                requete = "SELECT FactureManuelleEntete.NoFacture as \"NoFacture\", FactureManuelleEntete.NomIntervention as \"CodeImmeuble\", FactureManuelleEntete.DateFacture as \"DateFacture\", FactureManuelleEntete.Client as \"Gérant\" From FactureManuelleEntete";
                where = "FactureManuelleEntete.TypeFacture ='TM'";
            }
            else if (optContratManu.Checked == true)
            {
                requete = "SELECT FactureManuelleEntete.NoFacture as \"NoFacture\", FactureManuelleEntete.NomIntervention as \"CodeImmeuble\", FactureManuelleEntete.DateFacture as \"DateFacture\", FactureManuelleEntete.Client as \"Gérant\" From FactureManuelleEntete";
                where = "FactureManuelleEntete.TypeFacture ='CM'";
            }

            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche de factures" };

            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                txtNoFacture.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                txtNoFacture_KeyPress(txtNoFacture, new KeyPressEventArgs((char)13));

                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyPress += (se, ev) =>
            {
                if (Convert.ToInt32(ev.KeyChar) == 13)
                {
                    txtNoFacture.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtNoFacture_KeyPress(txtNoFacture, new KeyPressEventArgs((char)13));

                    fg.Dispose();
                    fg.Close();
                }

            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();



        }
        //tested
        private void cmdVisu_Click(Object eventSender, EventArgs eventArgs)
        {
            ReportDocument CR = new ReportDocument();
            string SelectionFormula;

            if (GridCorps.Rows.Count == 0)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune facture ne peut être visualisée", "Aperçu d'une facture", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (optContrat.Checked == true)
            {
                //===> Mondir le 20.11.2020, pour corrigé le ticket https://groupe-dt.mantishub.io/view.php?id=2090
                //===> dans la class General ===>  CHEMINEUROONLY = gfr_liaison("Report") + gfr_liaison("CRFacturationEuroOnly");
                //===> Supprimé General.gsRpt +
                if (File.Exists(General.CHEMINEUROONLY))
                {
                    CR.Load(General.CHEMINEUROONLY);
                    SelectionFormula = "{FactEnTete.Nofacture}='" + txtSelNoFacture.Text + "'";
                    CrystalReportFormView C = new CrystalReportFormView(CR, SelectionFormula);
                    C.Text = "Aperçu de la facture contractuelle N°: " + txtSelNoFacture.Text;
                    C.Show();
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce rapport n'existe pas !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else
            {
                if (File.Exists(General.ETATNEWFACTUREMANUEL))
                {
                    CR.Load(General.ETATNEWFACTUREMANUEL);
                    SelectionFormula = "{FactureManuelleEntete.Nofacture}='" + txtSelNoFacture.Text + "'";
                    CrystalReportFormView C = new CrystalReportFormView(CR, SelectionFormula);

                    if (optContratManu.Checked == true)
                    {

                        C.Text = "Aperçu de la facture contractuelle N°: " + txtSelNoFacture.Text;
                    }
                    else if (optDevis.Checked == true)
                    {

                        C.Text = "Aperçu de la facture devis N°: " + txtSelNoFacture.Text;
                    }
                    else if (optIntervention.Checked == true)
                    {

                        C.Text = "Aperçu de la facture intervention N°: " + txtSelNoFacture.Text;
                    }
                    else if (optManu.Checked == true)
                    {

                        C.Text = "Aperçu de la facture manuelle N°: " + txtSelNoFacture.Text;
                    }
                    C.Show();
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce rapport n'existe pas !", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }


            return;

            //fc_ExportCrystalSansFormule "{FactureManuelleEntete.Nofacture}='" & txtSelNoFacture.Text & "'", ETATFACTUREMANUELExport
        }

        //tested
        private void optContrat_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optContrat.Checked)
            {
                txtNoFacture_KeyPress(txtNoFacture, new System.Windows.Forms.KeyPressEventArgs((char)13));
            }
        }
        //tested
        private void optContratManu_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optContratManu.Checked)
            {
                txtNoFacture_KeyPress(txtNoFacture, new System.Windows.Forms.KeyPressEventArgs((char)13));
            }
        }
        //tested
        private void optDevis_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optDevis.Checked)
            {
                txtNoFacture_KeyPress(txtNoFacture, new System.Windows.Forms.KeyPressEventArgs((char)13));
            }
        }
        //tested
        private void optIntervention_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optIntervention.Checked)
            {
                txtNoFacture_KeyPress(txtNoFacture, new System.Windows.Forms.KeyPressEventArgs((char)13));
            }
        }
        //tested
        private void optManu_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optManu.Checked)
            {
                txtNoFacture_KeyPress(txtNoFacture, new KeyPressEventArgs((char)13));
            }
        }
        //tested
        private void txtNoFacture_KeyPress(Object eventSender, KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)eventArgs.KeyChar;
            if (KeyAscii == 13 && !string.IsNullOrEmpty(txtNoFacture.Text))
            {
                txtSelNoFacture.Text = txtNoFacture.Text;
                GridCorps.Rows.AsEnumerable().ToList()?.ForEach(row => row.Delete(false));
                //GridCorps.RemoveAll();
                txtTotalHT.Text = "0.00 €";
                txtTotalTTC.Text = "0.00 €";
                txtTVA196.Text = "0.00 €";
                txtTVA55.Text = "0.00 €";
                if (optContrat.Checked == true)
                {
                    //Tested
                    fc_GetFactureContratAuto(txtNoFacture.Text);
                }
                else if (optContratManu.Checked == true)
                {
                    fc_GetFacture(txtNoFacture.Text, "CM");
                }
                else if (optDevis.Checked == true)
                {
                    if (fc_VerifDevis(txtNoFacture.Text) == true)
                    {
                        fc_GetFacture(txtNoFacture.Text, "TA");
                    }
                }
                else if (optIntervention.Checked == true)
                {
                    fc_GetFacture(txtNoFacture.Text, "TA");
                }
                else if (optManu.Checked == true)
                {
                    fc_GetFacture(txtNoFacture.Text, "TM");
                }
                if (!string.IsNullOrEmpty(lblCodeImmeuble.Text))
                {
                    lblCodeGerant.Text = ModAdo1.fc_ADOlibelle("SELECT Immeuble.Code1 FROM Immeuble WHERE Immeuble.CodeImmeuble='" + lblCodeImmeuble.Text + "'");
                }
                else
                {
                    lblCodeGerant.Text = "";
                }
                Application.DoEvents();
                KeyAscii = 0;
            }
            eventArgs.KeyChar = (char)KeyAscii;
            if (KeyAscii == 0)
            {
                eventArgs.Handled = true;
            }
        }
        //tested
        private bool fc_VerifDevis(string sNofacture)
        {
            if (string.IsNullOrEmpty(ModAdo1.fc_ADOlibelle("SELECT GestionStandard.NoDevis FROM GestionStandard INNER JOIN Intervention ON GestionStandard.NumFicheStandard=Intervention.NumFicheStandard WHERE Intervention.NoFacture='" + sNofacture + "'")))
            {
                return false;
            }
            else
            {
                return true;
            }

        }
        //tested
        private void fc_GetFactureContratAuto(string sNofacture)
        {
            DataTable rsFact;
            string sSelect = null;
            string sGroup = null;
            string sWhere = null;
            string sImmeuble = null;
            string sItem = null;
            double dblTotalTTC = 0;
            double dblTotalHT = 0;
            double dblTotal55 = 0;
            double dblTotal196 = 0;
            double dblTotalTva = 0;
            short i = 0;

            rsFact = new DataTable();

            sSelect = "SELECT FacCorpsEtat.* ";
            sSelect = sSelect + " FROM  FacCorpsEtat ";
            //    sSelect = sSelect & " FactEntete.NumContrat=FacCorpsEtat.NumContrat AND FactEntete.Avenant=FacCorpsEtat.Avenant "

            sWhere = " WHERE FacCorpsEtat.NoFacture='" + sNofacture + "'";

            dblTotal196 = 0;
            dblTotal55 = 0;
            dblTotalHT = 0;
            dblTotalTTC = 0;

            rsFact = ModAdo1.fc_OpenRecordSet(sSelect + sWhere);
            if (rsFact.Rows.Count > 0)
            {

                sImmeuble = ModAdo1.fc_ADOlibelle("SELECT Contrat.CodeImmeuble FROM Contrat WHERE Contrat.NumContrat='" + rsFact.Rows[0]["NumContrat"] + "' AND Contrat.Avenant='0'");
                txtCodeContrat.Text = rsFact.Rows[0]["NumContrat"] + "";
                txtContrat.Text = ModAdo1.fc_ADOlibelle("SELECT Contrat.LibelleCont1 FROM Contrat WHERE Contrat.NumContrat='" + rsFact.Rows[0]["NumContrat"] + "" + "'");

                DataTable dt1 = new DataTable();

                dt1.Columns.Add("Designation");
                dt1.Columns.Add("Qte");
                dt1.Columns.Add("Prix Unitaire HT");
                dt1.Columns.Add("Taux TVA");
                dt1.Columns.Add("Montant TVA");
                dt1.Columns.Add("Total HT");
                dt1.Columns.Add("Total TTC");
                dt1.Columns.Add("Noligne");
                dt1.Columns.Add("CleFacture");

                var dr1 = dt1.NewRow();
                foreach (DataRow R in rsFact.Rows)
                {

                    dblTotalTva = 0;

                    dr1["Designation"] = "- Campagne de chauffe " + R["Annee"] + "" + "\t";
                    dr1["Qte"] = "";
                    dr1["Prix Unitaire HT"] = "";
                    dr1["Taux TVA"] = "";
                    dr1["Montant TVA"] = "";
                    dr1["Total HT"] = "";
                    dr1["Total TTC"] = "";
                    dr1["Noligne"] = "";
                    dr1["CleFacture"] = "";

                    dt1.Rows.Add(dr1.ItemArray);
                    GridCorps.DataSource = dt1;

                    dr1["Designation"] = "* " + R["LibelleCont1"] + "";
                    dr1["Qte"] = "";
                    dr1["Prix Unitaire HT"] = "";
                    dr1["Taux TVA"] = "";
                    dr1["Montant TVA"] = "";
                    dr1["Total HT"] = "";
                    dr1["Total TTC"] = "";
                    dr1["Noligne"] = "";
                    dr1["CleFacture"] = "";

                    dt1.Rows.Add(dr1.ItemArray);
                    GridCorps.DataSource = dt1;

                    if (!string.IsNullOrEmpty(General.nz(R["LibelleCont2"], "").ToString()))
                    {
                        dr1["Designation"] = "* " + R["LibelleCont2"] + "" + "\t";
                        dr1["Qte"] = "";
                        dr1["Prix Unitaire HT"] = "";
                        dr1["Taux TVA"] = "";
                        dr1["Montant TVA"] = "";
                        dr1["Total HT"] = "";
                        dr1["Total TTC"] = "";
                        dr1["Noligne"] = "";
                        dr1["CleFacture"] = "";

                        dt1.Rows.Add(dr1.ItemArray);
                        GridCorps.DataSource = dt1;
                    }

                    dr1["Designation"] = "Echéance du " + R["Calandate"];
                    dr1["Qte"] = "";
                    dr1["Prix Unitaire HT"] = "";
                    dr1["Taux TVA"] = "";
                    dr1["Montant TVA"] = "";
                    dr1["Total HT"] = "";
                    dr1["Total TTC"] = "";
                    dr1["Noligne"] = "";
                    dr1["CleFacture"] = "";

                    dt1.Rows.Add(dr1.ItemArray);
                    GridCorps.DataSource = dt1;


                    dr1["Designation"] = "Période du " + R["pcdu"] + " au " + R["Au"];
                    dr1["Qte"] = "";
                    dr1["Prix Unitaire HT"] = "";
                    dr1["Taux TVA"] = "";
                    dr1["Montant TVA"] = "";
                    dr1["Total HT"] = "";
                    dr1["Total TTC"] = "";
                    dr1["Noligne"] = "";
                    dr1["CleFacture"] = "";

                    dt1.Rows.Add(dr1.ItemArray);
                    GridCorps.DataSource = dt1;

                    dr1["Designation"] = "Montant de base au " + R["DateBase"] + " = " + R["BaseContractuelle"];
                    dr1["Qte"] = "";
                    dr1["Prix Unitaire HT"] = "";
                    dr1["Taux TVA"] = "";
                    dr1["Montant TVA"] = "";
                    dr1["Total HT"] = "";
                    dr1["Total TTC"] = "";
                    dr1["Noligne"] = "";
                    dr1["CleFacture"] = "";

                    dt1.Rows.Add(dr1.ItemArray);
                    GridCorps.DataSource = dt1;


                    for (i = 1; i <= 3; i++)
                    {
                        if (!string.IsNullOrEmpty(General.nz(R["Indice" + i], "").ToString()))
                        {
                            dr1["Designation"] = "Indice " + R["Indice" + i] + (R["Connu"] + "" == "1" ? " connu " : " réel ") + " au " + R["Date" + i] + "    = " + R["Valeur" + i];
                            dr1["Qte"] = "";
                            dr1["Prix Unitaire HT"] = "";
                            dr1["Taux TVA"] = "";
                            dr1["Montant TVA"] = "";
                            dr1["Total HT"] = "";
                            dr1["Total TTC"] = "";
                            dr1["Noligne"] = "";
                            dr1["CleFacture"] = "";

                            dt1.Rows.Add(dr1.ItemArray);
                            GridCorps.DataSource = dt1;
                        }
                    }

                    for (i = 1; i <= 3; i++)
                    {
                        if (!string.IsNullOrEmpty(General.nz(R["Indice" + i + "Bis"], "").ToString()))
                        {
                            dr1["Designation"] = "Indice " + R["Indice" + i + "Bis"] + (R["Connu"] + "" == "1" ? " connu " : " réel ") + " au " + R["DateFacture"] + "    = " + R["Valeur" + i + "Bis"];
                            dr1["Qte"] = "";
                            dr1["Prix Unitaire HT"] = "";
                            dr1["Taux TVA"] = "";
                            dr1["Montant TVA"] = "";
                            dr1["Total HT"] = "";
                            dr1["Total TTC"] = "";
                            dr1["Noligne"] = "";
                            dr1["CleFacture"] = "";

                            dt1.Rows.Add(dr1.ItemArray);
                            GridCorps.DataSource = dt1;
                        }
                    }

                    dr1["Designation"] = "Facturation par " + R["Ratio"] + "";
                    dr1["Qte"] = "";
                    dr1["Prix Unitaire HT"] = "";
                    dr1["Taux TVA"] = "";
                    dr1["Montant TVA"] = "";
                    dr1["Total HT"] = "";
                    dr1["Total TTC"] = "";
                    dr1["Noligne"] = "";
                    dr1["CleFacture"] = "";

                    dt1.Rows.Add(dr1.ItemArray);
                    GridCorps.DataSource = dt1;



                    dr1["Designation"] = R["FormuleImpression"] + "";
                    dr1["Qte"] = "";
                    dr1["Prix Unitaire HT"] = "";
                    dr1["Taux TVA"] = R["TVA"] + "";
                    dr1["Montant TVA"] = General.FncArrondir(Convert.ToDouble(Convert.ToDouble(General.nz(R["resultatRatio"], "0")) * Convert.ToDouble(General.nz(R["TVA"], "0"))) / 100);
                    dr1["Total HT"] = General.nz(R["resultatRatio"], "0");
                    dr1["Total TTC"] = General.FncArrondir(Convert.ToDouble(General.nz(R["resultatRatio"], "0")) + (Convert.ToDouble(General.nz(R["resultatRatio"], "0")) * Convert.ToDouble(General.nz(R["TVA"], "0")) / 100));
                    dr1["Noligne"] = "";
                    dr1["CleFacture"] = "";

                    dt1.Rows.Add(dr1.ItemArray);
                    GridCorps.DataSource = dt1;

                    dblTotalTva = General.FncArrondir(Convert.ToDouble(General.nz(R["resultatRatio"], "0")) * (Convert.ToDouble(General.nz(R["TVA"], "0")) / 100));

                    if (R["TVA"] + "" == "5,5" || R["TVA"] + "" == "7" || R["TVA"] + "" == "10")
                    {
                        dblTotal55 = dblTotal55 + dblTotalTva;
                    }

                    else
                    {

                        dblTotal196 = dblTotal196 + dblTotalTva;
                    }

                    dblTotalHT = dblTotalHT + General.FncArrondir(Convert.ToDouble(General.nz(R["resultatRatio"], "0")));
                    dblTotalTTC = dblTotalTTC + General.FncArrondir(Convert.ToDouble(General.nz(R["resultatRatio"], "0")) + (Convert.ToDouble(General.nz(R["resultatRatio"], "0")) * (Convert.ToDouble(General.nz(R["TVA"], "0")) / 100)));

                }
            }

            txtTotalHT.Text = Convert.ToString(General.FncArrondir(Convert.ToDouble(General.nz(dblTotalHT, "0")))) + " €";
            txtTotalTTC.Text = Convert.ToString(General.FncArrondir(Convert.ToDouble(General.nz(dblTotalTTC, "0")))) + " €";
            txtTVA196.Text = Convert.ToString(General.FncArrondir(Convert.ToDouble(General.nz(dblTotal196, "0")))) + " €";
            txtTVA55.Text = Convert.ToString(General.FncArrondir(Convert.ToDouble(General.nz(dblTotal55, "0")))) + " €";

            lblCodeImmeuble.Text = sImmeuble;

            rsFact.Clear();
            ModAdo1.Dispose();
            rsFact = null;
        }
        //tested
        private void fc_GetFacture(string sNofacture, string sTypeFacture)
        {
            DataTable rsFact;
            string sSelect = null;
            string sGroup = null;
            string sWhere = null;
            string sImmeuble = null;
            string sItem = null;
            double dblTotalTTC = 0;
            double dblTotalHT = 0;
            double dblTotal55 = 0;
            double dblTotal196 = 0;
            double dblTotalTva = 0;

            rsFact = new DataTable();

            sSelect = "SELECT FactureManuelleEntete.CleFactureManuelle,FactureManuelleEntete.Affaire,FactureManuelleDetail.NumeroLigne,";
            sSelect = sSelect + "FactureManuelleDetail.Designation,FactureManuelleDetail.Quantite,";
            sSelect = sSelect + "FactureManuelleDetail.PrixUnitaire,FactureManuelleDetail.MontantLigne,";
            sSelect = sSelect + "FactureManuelleDetail.TVA ";
            sSelect = sSelect + " FROM FactureManuelleEntete INNER JOIN FactureManuelleDetail ON ";
            sSelect = sSelect + " FactureManuelleEntete.CleFactureManuelle=FactureManuelleDetail.CleFactureManuelle ";

            sWhere = " WHERE FactureManuelleEntete.NoFacture='" + sNofacture + "' AND FactureManuelleEntete.TypeFacture='" + sTypeFacture + "' ORDER BY FactureManuelleDetail.NumeroLigne ";

            dblTotal196 = 0;
            dblTotal55 = 0;
            dblTotalHT = 0;
            dblTotalTTC = 0;

            rsFact = ModAdo1.fc_OpenRecordSet(sSelect + sWhere);
            if (rsFact.Rows.Count > 0)
            {

                sImmeuble = rsFact.Rows[0]["Affaire"] + "";

                DataTable dt2 = new DataTable();

                dt2.Columns.Add("Designation");
                dt2.Columns.Add("Qte");
                dt2.Columns.Add("Prix Unitaire HT");
                dt2.Columns.Add("Taux TVA");

                dt2.Columns.Add("Montant TVA");
                dt2.Columns.Add("Total HT");
                dt2.Columns.Add("Total TTC");
                dt2.Columns.Add("Noligne");
                dt2.Columns.Add("CleFacture");

                var dr2 = dt2.NewRow();
                foreach (DataRow Dr in rsFact.Rows)
                {
                    dblTotalTva = 0;
                    //Dr["TVA"] = General.Replace(Dr["TVA"].ToString(), ".", ",");


                    dr2["Designation"] = Dr["Designation"];
                    dr2["Qte"] = Dr["Quantite"];
                    dr2["Prix Unitaire HT"] = Dr["Prixunitaire"];
                    dr2["Taux TVA"] = Dr["TVA"];

                    if (string.IsNullOrEmpty(Dr["Prixunitaire"].ToString()))
                    {
                        dr2["Montant TVA"] = General.FncArrondir(Convert.ToDouble(General.nz(Dr["MontantLigne"], "0")) * (Convert.ToDouble(General.nz(Dr["TVA"], "0")) / 100));
                    }
                    else
                    {
                        dr2["Montant TVA"] = General.FncArrondir(Convert.ToDouble(General.nz(Dr["Prixunitaire"], "0")) * Convert.ToDouble(General.nz(Dr["TVA"], "0")) / 100);
                    }

                    if (string.IsNullOrEmpty(Dr["Prixunitaire"].ToString()))
                    {
                        dr2["Total HT"] = General.FncArrondir(Convert.ToDouble(General.nz(Dr["MontantLigne"], "0")));
                    }
                    else
                    {
                        dr2["Total HT"] = General.FncArrondir(Convert.ToDouble(General.nz(Dr["Prixunitaire"], "0")) * Convert.ToDouble(General.nz(Dr["Quantite"], "1")));
                    }


                    if (string.IsNullOrEmpty(Dr["Prixunitaire"].ToString()))
                    {
                        dr2["Total TTC"] = General.FncArrondir(Convert.ToDouble(General.nz(Dr["MontantLigne"], "0")) + (Convert.ToDouble(General.nz(Dr["MontantLigne"], "0")) * (Convert.ToDouble(General.nz(Dr["TVA"], "0")) / 100)));
                    }
                    else
                    {
                        //dr2["Total TTC"] = General.FncArrondir((Convert.ToDouble(General.nz(Dr["Prixunitaire"], "0")) * Convert.ToDouble(General.nz(Dr["Quantite"], "1"))) * (Convert.ToDouble(General.nz(Dr["Prixunitaire"], "0")) * Convert.ToDouble(General.nz(Dr["TVA"], "0")) / 100));
                        //=== modif du 0901 2019 * remplé par +
                        dr2["Total TTC"] = General.FncArrondir((Convert.ToDouble(General.nz(Dr["Prixunitaire"], "0")) * Convert.ToDouble(General.nz(Dr["Quantite"], "1"))) + (Convert.ToDouble(General.nz(Dr["Prixunitaire"], "0")) * Convert.ToDouble(General.nz(Dr["TVA"], "0")) / 100));
                    }

                    dr2["Noligne"] = Dr["NumeroLigne"];
                    dr2["CleFacture"] = Dr["CleFactureManuelle"];



                    if (string.IsNullOrEmpty(Dr["Prixunitaire"].ToString()))
                    {
                        dblTotalTva = General.FncArrondir(Convert.ToDouble(General.nz(Dr["MontantLigne"], "0")) * (Convert.ToDouble(General.nz(Dr["TVA"], "0")) / 100));
                    }
                    else
                    {
                        dblTotalTva = ((Convert.ToDouble(General.nz(Dr["Prixunitaire"], "0")) * Convert.ToDouble(General.nz(Dr["TVA"], "0")) / 100));
                    }

                    if (Dr["TVA"] + "" == "5,5" || Dr["TVA"] + "" == "7" || Dr["TVA"] + "" == "10")
                    {
                        dblTotal55 = dblTotal55 + dblTotalTva;
                    }
                    else
                    {
                        dblTotal196 = dblTotal196 + dblTotalTva;
                    }


                    if (string.IsNullOrEmpty(Dr["Prixunitaire"].ToString()))
                    {
                        dblTotalHT = dblTotalHT + General.FncArrondir(Convert.ToDouble(General.nz(Dr["MontantLigne"], "0")));
                    }
                    else
                    {
                        dblTotalHT = dblTotalHT + (Convert.ToDouble(General.nz(Dr["Prixunitaire"], "0")) * Convert.ToDouble(General.nz(Dr["Quantite"], "1")));
                    }


                    if (string.IsNullOrEmpty(Dr["Prixunitaire"].ToString()))
                    {
                        dblTotalTTC = dblTotalTTC + General.FncArrondir(Convert.ToDouble(General.nz(Dr["MontantLigne"], "0")) + (Convert.ToDouble(General.nz(Dr["MontantLigne"], "0")) * (Convert.ToDouble(General.nz(Dr["TVA"], "0")) / 100)));
                    }
                    else
                    {
                        //dblTotalTTC = dblTotalTTC + ((Convert.ToDouble(General.nz(Dr["Prixunitaire"], "0")) * Convert.ToDouble(General.nz(Dr["Quantite"], "1"))) * (Convert.ToDouble(General.nz(Dr["Prixunitaire"], "0")) * Convert.ToDouble(General.nz(Dr["TVA"], "0")) / 100));
                        //=== modif du 0901 2019 * remplé par +
                        dblTotalTTC = dblTotalTTC + ((Convert.ToDouble(General.nz(Dr["Prixunitaire"], "0")) * Convert.ToDouble(General.nz(Dr["Quantite"], "1"))) + (Convert.ToDouble(General.nz(Dr["Prixunitaire"], "0")) * Convert.ToDouble(General.nz(Dr["TVA"], "0")) / 100));
                    }

                    dt2.Rows.Add(dr2.ItemArray);
                    GridCorps.DataSource = dt2;

                }
            }

            txtTotalHT.Text = Convert.ToString(General.FncArrondir(Convert.ToDouble(General.nz(dblTotalHT, "0")))) + " €";
            txtTotalTTC.Text = Convert.ToString(General.FncArrondir(Convert.ToDouble(General.nz(dblTotalTTC, "0")))) + " €";
            txtTVA196.Text = Convert.ToString(General.FncArrondir(Convert.ToDouble(General.nz(dblTotal196, "0")))) + " €";
            txtTVA55.Text = Convert.ToString(General.FncArrondir(Convert.ToDouble(General.nz(dblTotal55, "0")))) + " €";
            lblCodeImmeuble.Text = sImmeuble;
            fc_RechercheContrat(ref sImmeuble);
            rsFact.Clear();
            ModAdo1.Dispose();
            rsFact = null;

        }
        //tested
        private void fc_RechercheContrat(ref string sCodeImmeuble)
        {
            object blnContrat = null;

            try
            {
                string LenNumContrat = null;

                LenNumContrat = ModAdo1.fc_ADOlibelle("SELECT MAX(LEN(NUMCONTRAT)) AS MaxNum FROM Contrat WHERE CodeImmeuble='" + sCodeImmeuble + "'");

                General.sSQL = "";
                General.sSQL = "SELECT MAX(Contrat.DateEffet) as MaxiDate,Contrat.NumContrat AS MaxiNum, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1 "
                    + " From Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle ";
                General.sSQL = General.sSQL + " WHERE Contrat.CodeImmeuble = '"
                    + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "' AND Avenant=0";
                General.sSQL = General.sSQL + " GROUP BY Contrat.NumContrat, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1";
                if ((General.rstmp != null))
                {
                    General.rstmp = null;
                }
                General.rstmp = ModAdo1.fc_OpenRecordSet(General.sSQL);
                if (General.rstmp.Rows.Count > 0)
                {
                    txtContrat.Text = General.rstmp.Rows[0]["Designation1"] + "";
                    txtCodeContrat.Text = General.rstmp.Rows[0]["MaxiNum"] + "";
                }
                else
                {
                    txtContrat.Text = "";
                    txtCodeContrat.Text = "";
                }
                General.rstmp.Clear();
                txtContrat.ForeColor = ColorTranslator.FromOle(0xff0000);
                //Si j'ai un contrat, je vérifie qu'il n'est pas résilié
                if (!string.IsNullOrEmpty(txtCodeContrat.Text))
                {
                    General.sSQL = "SELECT Contrat.NumContrat,FacArticle.Designation1 FROM Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle WHERE (Contrat.Resiliee=0 OR (Contrat.Resiliee=1 AND (Contrat.DateFin>'"
                        + DateTime.Today
                        + "' OR Contrat.DateFin IS NULL))) AND Contrat.CodeImmeuble='"
                        + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                    General.rstmp = ModAdo1.fc_OpenRecordSet(General.sSQL);
                    if (General.rstmp.Rows.Count > 0)
                    {
                        txtCodeContrat.Text = General.rstmp.Rows[0]["NumContrat"].ToString();
                        txtContrat.Text = General.rstmp.Rows[0]["Designation1"].ToString();
                        blnContrat = true;
                        txtContrat.ForeColor = ColorTranslator.FromOle(0xff0000);
                    }
                    else
                    {
                        blnContrat = false;
                        txtContrat.ForeColor = ColorTranslator.FromOle(0xc0);
                    }
                }


                General.rstmp.Clear();
                ModAdo1.Dispose();

                General.rstmp = null;

                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + " fc_RechercheContrat ");
            }

        }

        private void UserDocPrintFacture_VisibleChanged(object sender, EventArgs e)
        {

            if (!Visible)
                return;

            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocPrintFacture");
            General.open_conn();

        }
        //tested
        private void GridCorps_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            this.GridCorps.DisplayLayout.Bands[0].Columns["Noligne"].Hidden = true;
            this.GridCorps.DisplayLayout.Bands[0].Columns["CleFacture"].Hidden = true;
        }
        //tested
        private void Label40_Click(object sender, EventArgs e)
        {
            var LBL = sender as Label;
            int Index = Convert.ToInt32(LBL.Tag);

            switch (Index)
            {
                case 0:
                    //Lien vers Immeuble
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, lblCodeImmeuble.Text);
                    // ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocImmeuble);
                    View.Theme.Theme.Navigate(typeof(UserDocImmeuble));
                    break;
                case 1:
                    //Lien vers gérant
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, lblCodeGerant.Text);
                    // ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocClient);
                    View.Theme.Theme.Navigate(typeof(UserDocClient));
                    break;
                case 2:
                    //Lien vers Contrat
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFicheContrat, txtCodeContrat.Text);
                    //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGFACCONTRAT);
                    View.Theme.Theme.Navigate(typeof(UserDocFicheContrat));
                    break;
            }
        }
    }
}
