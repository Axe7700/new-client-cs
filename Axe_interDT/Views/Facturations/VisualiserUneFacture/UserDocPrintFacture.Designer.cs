namespace Axe_interDT.Views.Facturations.VisualiserUneFacture
{
    partial class UserDocPrintFacture
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.optManu = new System.Windows.Forms.RadioButton();
            this.optIntervention = new System.Windows.Forms.RadioButton();
            this.optDevis = new System.Windows.Forms.RadioButton();
            this.optContrat = new System.Windows.Forms.RadioButton();
            this.optContratManu = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdRechercheFacture = new System.Windows.Forms.Button();
            this.txtNoFacture = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.Label42 = new System.Windows.Forms.LinkLabel();
            this.Label41 = new System.Windows.Forms.LinkLabel();
            this.Label40 = new System.Windows.Forms.LinkLabel();
            this.txtContrat = new iTalk.iTalk_TextBox_Small2();
            this.lblCodeGerant = new iTalk.iTalk_TextBox_Small2();
            this.lblCodeImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdAnalytique = new System.Windows.Forms.Button();
            this.cmdVisu = new System.Windows.Forms.Button();
            this.GridCorps = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtTotalTTC = new System.Windows.Forms.Label();
            this.txtTotalHT = new System.Windows.Forms.Label();
            this.txtTVA55 = new System.Windows.Forms.Label();
            this.txtTVA196 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCodeContrat = new iTalk.iTalk_TextBox_Small2();
            this.txtSelNoFacture = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel1.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCorps)).BeginInit();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60F));
            this.tableLayoutPanel1.Controls.Add(this.Frame1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 62F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1844, 61);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.Transparent;
            this.Frame1.Controls.Add(this.tableLayoutPanel7);
            this.Frame1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Frame1.Location = new System.Drawing.Point(740, 3);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(1101, 56);
            this.Frame1.TabIndex = 515;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "Type de facture";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 5;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel7.Controls.Add(this.optManu, 4, 0);
            this.tableLayoutPanel7.Controls.Add(this.optIntervention, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.optDevis, 3, 0);
            this.tableLayoutPanel7.Controls.Add(this.optContrat, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.optContratManu, 2, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1095, 35);
            this.tableLayoutPanel7.TabIndex = 0;
            // 
            // optManu
            // 
            this.optManu.AutoSize = true;
            this.optManu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optManu.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optManu.Location = new System.Drawing.Point(879, 3);
            this.optManu.Name = "optManu";
            this.optManu.Size = new System.Drawing.Size(213, 29);
            this.optManu.TabIndex = 542;
            this.optManu.Text = "Manuelle";
            this.optManu.UseVisualStyleBackColor = true;
            this.optManu.CheckedChanged += new System.EventHandler(this.optManu_CheckedChanged);
            // 
            // optIntervention
            // 
            this.optIntervention.AutoSize = true;
            this.optIntervention.Checked = true;
            this.optIntervention.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optIntervention.Location = new System.Drawing.Point(3, 3);
            this.optIntervention.Name = "optIntervention";
            this.optIntervention.Size = new System.Drawing.Size(213, 29);
            this.optIntervention.TabIndex = 538;
            this.optIntervention.TabStop = true;
            this.optIntervention.Text = "Intervention";
            this.optIntervention.UseVisualStyleBackColor = true;
            this.optIntervention.CheckedChanged += new System.EventHandler(this.optIntervention_CheckedChanged);
            // 
            // optDevis
            // 
            this.optDevis.AutoSize = true;
            this.optDevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optDevis.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optDevis.Location = new System.Drawing.Point(660, 3);
            this.optDevis.Name = "optDevis";
            this.optDevis.Size = new System.Drawing.Size(213, 29);
            this.optDevis.TabIndex = 541;
            this.optDevis.Text = "Devis";
            this.optDevis.UseVisualStyleBackColor = true;
            this.optDevis.CheckedChanged += new System.EventHandler(this.optDevis_CheckedChanged);
            // 
            // optContrat
            // 
            this.optContrat.AutoSize = true;
            this.optContrat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optContrat.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optContrat.Location = new System.Drawing.Point(222, 3);
            this.optContrat.Name = "optContrat";
            this.optContrat.Size = new System.Drawing.Size(213, 29);
            this.optContrat.TabIndex = 539;
            this.optContrat.Text = "Contrat auto";
            this.optContrat.UseVisualStyleBackColor = true;
            this.optContrat.CheckedChanged += new System.EventHandler(this.optContrat_CheckedChanged);
            // 
            // optContratManu
            // 
            this.optContratManu.AutoSize = true;
            this.optContratManu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optContratManu.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optContratManu.Location = new System.Drawing.Point(441, 3);
            this.optContratManu.Name = "optContratManu";
            this.optContratManu.Size = new System.Drawing.Size(213, 29);
            this.optContratManu.TabIndex = 540;
            this.optContratManu.Text = "Contrat manu";
            this.optContratManu.UseVisualStyleBackColor = true;
            this.optContratManu.CheckedChanged += new System.EventHandler(this.optContratManu_CheckedChanged);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 27.27273F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 72.72727F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(731, 56);
            this.tableLayoutPanel5.TabIndex = 516;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 3;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel6.Controls.Add(this.cmdRechercheFacture, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtNoFacture, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(725, 35);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // cmdRechercheFacture
            // 
            this.cmdRechercheFacture.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheFacture.FlatAppearance.BorderSize = 0;
            this.cmdRechercheFacture.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheFacture.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheFacture.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheFacture.Location = new System.Drawing.Point(695, 0);
            this.cmdRechercheFacture.Margin = new System.Windows.Forms.Padding(0);
            this.cmdRechercheFacture.Name = "cmdRechercheFacture";
            this.cmdRechercheFacture.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheFacture.TabIndex = 507;
            this.cmdRechercheFacture.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechercheFacture, "Recherche d\'un numéro de facture");
            this.cmdRechercheFacture.UseVisualStyleBackColor = false;
            this.cmdRechercheFacture.Click += new System.EventHandler(this.cmdRechercheFacture_Click);
            // 
            // txtNoFacture
            // 
            this.txtNoFacture.AccAcceptNumbersOnly = false;
            this.txtNoFacture.AccAllowComma = false;
            this.txtNoFacture.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoFacture.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoFacture.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoFacture.AccHidenValue = "";
            this.txtNoFacture.AccNotAllowedChars = null;
            this.txtNoFacture.AccReadOnly = false;
            this.txtNoFacture.AccReadOnlyAllowDelete = false;
            this.txtNoFacture.AccRequired = false;
            this.txtNoFacture.BackColor = System.Drawing.Color.White;
            this.txtNoFacture.CustomBackColor = System.Drawing.Color.White;
            this.txtNoFacture.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoFacture.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNoFacture.ForeColor = System.Drawing.Color.Black;
            this.txtNoFacture.Location = new System.Drawing.Point(89, 2);
            this.txtNoFacture.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoFacture.MaxLength = 32767;
            this.txtNoFacture.Multiline = false;
            this.txtNoFacture.Name = "txtNoFacture";
            this.txtNoFacture.ReadOnly = false;
            this.txtNoFacture.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoFacture.Size = new System.Drawing.Size(604, 27);
            this.txtNoFacture.TabIndex = 509;
            this.txtNoFacture.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoFacture.UseSystemPasswordChar = false;
            this.txtNoFacture.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtNoFacture_KeyPress);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(81, 35);
            this.label33.TabIndex = 508;
            this.label33.Text = "N° Facture";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 6;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.Label42, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.Label41, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.Label40, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtContrat, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblCodeGerant, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.lblCodeImmeuble, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 70);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1844, 32);
            this.tableLayoutPanel2.TabIndex = 511;
            // 
            // Label42
            // 
            this.Label42.AutoSize = true;
            this.Label42.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label42.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label42.Location = new System.Drawing.Point(1234, 0);
            this.Label42.Name = "Label42";
            this.Label42.Size = new System.Drawing.Size(63, 32);
            this.Label42.TabIndex = 520;
            this.Label42.TabStop = true;
            this.Label42.Tag = "2";
            this.Label42.Text = "Contrat";
            this.Label42.Click += new System.EventHandler(this.Label40_Click);
            // 
            // Label41
            // 
            this.Label41.AutoSize = true;
            this.Label41.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label41.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label41.Location = new System.Drawing.Point(629, 0);
            this.Label41.Name = "Label41";
            this.Label41.Size = new System.Drawing.Size(57, 32);
            this.Label41.TabIndex = 519;
            this.Label41.TabStop = true;
            this.Label41.Tag = "1";
            this.Label41.Text = "Gérant";
            this.Label41.Click += new System.EventHandler(this.Label40_Click);
            // 
            // Label40
            // 
            this.Label40.AutoSize = true;
            this.Label40.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label40.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label40.Location = new System.Drawing.Point(3, 0);
            this.Label40.Name = "Label40";
            this.Label40.Size = new System.Drawing.Size(78, 32);
            this.Label40.TabIndex = 518;
            this.Label40.TabStop = true;
            this.Label40.Tag = "0";
            this.Label40.Text = "Immeuble";
            this.Label40.Click += new System.EventHandler(this.Label40_Click);
            // 
            // txtContrat
            // 
            this.txtContrat.AccAcceptNumbersOnly = false;
            this.txtContrat.AccAllowComma = false;
            this.txtContrat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtContrat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtContrat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtContrat.AccHidenValue = "";
            this.txtContrat.AccNotAllowedChars = null;
            this.txtContrat.AccReadOnly = false;
            this.txtContrat.AccReadOnlyAllowDelete = false;
            this.txtContrat.AccRequired = false;
            this.txtContrat.BackColor = System.Drawing.Color.White;
            this.txtContrat.CustomBackColor = System.Drawing.Color.White;
            this.txtContrat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtContrat.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtContrat.ForeColor = System.Drawing.Color.Black;
            this.txtContrat.Location = new System.Drawing.Point(1302, 2);
            this.txtContrat.Margin = new System.Windows.Forms.Padding(2);
            this.txtContrat.MaxLength = 32767;
            this.txtContrat.Multiline = false;
            this.txtContrat.Name = "txtContrat";
            this.txtContrat.ReadOnly = false;
            this.txtContrat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtContrat.Size = new System.Drawing.Size(540, 27);
            this.txtContrat.TabIndex = 514;
            this.txtContrat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtContrat.UseSystemPasswordChar = false;
            // 
            // lblCodeGerant
            // 
            this.lblCodeGerant.AccAcceptNumbersOnly = false;
            this.lblCodeGerant.AccAllowComma = false;
            this.lblCodeGerant.AccBackgroundColor = System.Drawing.Color.White;
            this.lblCodeGerant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblCodeGerant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblCodeGerant.AccHidenValue = "";
            this.lblCodeGerant.AccNotAllowedChars = null;
            this.lblCodeGerant.AccReadOnly = false;
            this.lblCodeGerant.AccReadOnlyAllowDelete = false;
            this.lblCodeGerant.AccRequired = false;
            this.lblCodeGerant.BackColor = System.Drawing.Color.White;
            this.lblCodeGerant.CustomBackColor = System.Drawing.Color.White;
            this.lblCodeGerant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCodeGerant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblCodeGerant.ForeColor = System.Drawing.Color.Black;
            this.lblCodeGerant.Location = new System.Drawing.Point(691, 2);
            this.lblCodeGerant.Margin = new System.Windows.Forms.Padding(2);
            this.lblCodeGerant.MaxLength = 32767;
            this.lblCodeGerant.Multiline = false;
            this.lblCodeGerant.Name = "lblCodeGerant";
            this.lblCodeGerant.ReadOnly = false;
            this.lblCodeGerant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblCodeGerant.Size = new System.Drawing.Size(538, 27);
            this.lblCodeGerant.TabIndex = 513;
            this.lblCodeGerant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblCodeGerant.UseSystemPasswordChar = false;
            // 
            // lblCodeImmeuble
            // 
            this.lblCodeImmeuble.AccAcceptNumbersOnly = false;
            this.lblCodeImmeuble.AccAllowComma = false;
            this.lblCodeImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.lblCodeImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblCodeImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblCodeImmeuble.AccHidenValue = "";
            this.lblCodeImmeuble.AccNotAllowedChars = null;
            this.lblCodeImmeuble.AccReadOnly = false;
            this.lblCodeImmeuble.AccReadOnlyAllowDelete = false;
            this.lblCodeImmeuble.AccRequired = false;
            this.lblCodeImmeuble.BackColor = System.Drawing.Color.White;
            this.lblCodeImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.lblCodeImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCodeImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblCodeImmeuble.ForeColor = System.Drawing.Color.Black;
            this.lblCodeImmeuble.Location = new System.Drawing.Point(86, 2);
            this.lblCodeImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.lblCodeImmeuble.MaxLength = 32767;
            this.lblCodeImmeuble.Multiline = false;
            this.lblCodeImmeuble.Name = "lblCodeImmeuble";
            this.lblCodeImmeuble.ReadOnly = false;
            this.lblCodeImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblCodeImmeuble.Size = new System.Drawing.Size(538, 27);
            this.lblCodeImmeuble.TabIndex = 512;
            this.lblCodeImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblCodeImmeuble.UseSystemPasswordChar = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cmdAnalytique);
            this.flowLayoutPanel1.Controls.Add(this.cmdVisu);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1672, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(178, 38);
            this.flowLayoutPanel1.TabIndex = 540;
            // 
            // cmdAnalytique
            // 
            this.cmdAnalytique.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAnalytique.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdAnalytique.FlatAppearance.BorderSize = 0;
            this.cmdAnalytique.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnalytique.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnalytique.ForeColor = System.Drawing.Color.White;
            this.cmdAnalytique.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAnalytique.Location = new System.Drawing.Point(2, 2);
            this.cmdAnalytique.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnalytique.Name = "cmdAnalytique";
            this.cmdAnalytique.Size = new System.Drawing.Size(85, 35);
            this.cmdAnalytique.TabIndex = 571;
            this.cmdAnalytique.Text = "Analytique";
            this.cmdAnalytique.UseVisualStyleBackColor = false;
            this.cmdAnalytique.Click += new System.EventHandler(this.cmdAnalytique_Click);
            // 
            // cmdVisu
            // 
            this.cmdVisu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdVisu.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdVisu.FlatAppearance.BorderSize = 0;
            this.cmdVisu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdVisu.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdVisu.ForeColor = System.Drawing.Color.White;
            this.cmdVisu.Image = global::Axe_interDT.Properties.Resources.Eye_16x16;
            this.cmdVisu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdVisu.Location = new System.Drawing.Point(91, 2);
            this.cmdVisu.Margin = new System.Windows.Forms.Padding(2);
            this.cmdVisu.Name = "cmdVisu";
            this.cmdVisu.Size = new System.Drawing.Size(85, 35);
            this.cmdVisu.TabIndex = 572;
            this.cmdVisu.Tag = "";
            this.cmdVisu.Text = "     Visu";
            this.toolTip1.SetToolTip(this.cmdVisu, "Aperçu avant Impression");
            this.cmdVisu.UseVisualStyleBackColor = false;
            this.cmdVisu.Click += new System.EventHandler(this.cmdVisu_Click);
            // 
            // GridCorps
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridCorps.DisplayLayout.Appearance = appearance1;
            this.GridCorps.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridCorps.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridCorps.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridCorps.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridCorps.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridCorps.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridCorps.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridCorps.DisplayLayout.MaxColScrollRegions = 1;
            this.GridCorps.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridCorps.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridCorps.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridCorps.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridCorps.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridCorps.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridCorps.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridCorps.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridCorps.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridCorps.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridCorps.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridCorps.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridCorps.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridCorps.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridCorps.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridCorps.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridCorps.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridCorps.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridCorps.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridCorps.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridCorps.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridCorps.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridCorps.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridCorps.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GridCorps.Location = new System.Drawing.Point(3, 108);
            this.GridCorps.Name = "GridCorps";
            this.GridCorps.Size = new System.Drawing.Size(1844, 756);
            this.GridCorps.TabIndex = 412;
            this.GridCorps.Text = "ultraGrid1";
            this.GridCorps.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridCorps_InitializeLayout);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel1, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.GridCorps, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel3, 0, 3);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 38);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 67F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1850, 919);
            this.tableLayoutPanel4.TabIndex = 544;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel3.Controls.Add(this.txtTotalTTC, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtTotalHT, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtTVA55, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtTVA196, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label2, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.label3, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.label4, 3, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 870);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1844, 46);
            this.tableLayoutPanel3.TabIndex = 512;
            this.tableLayoutPanel3.Visible = false;
            // 
            // txtTotalTTC
            // 
            this.txtTotalTTC.AutoSize = true;
            this.txtTotalTTC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTotalTTC.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTotalTTC.Location = new System.Drawing.Point(3, 23);
            this.txtTotalTTC.Name = "txtTotalTTC";
            this.txtTotalTTC.Size = new System.Drawing.Size(455, 23);
            this.txtTotalTTC.TabIndex = 391;
            // 
            // txtTotalHT
            // 
            this.txtTotalHT.AutoSize = true;
            this.txtTotalHT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTotalHT.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTotalHT.Location = new System.Drawing.Point(464, 23);
            this.txtTotalHT.Name = "txtTotalHT";
            this.txtTotalHT.Size = new System.Drawing.Size(455, 23);
            this.txtTotalHT.TabIndex = 390;
            // 
            // txtTVA55
            // 
            this.txtTVA55.AutoSize = true;
            this.txtTVA55.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTVA55.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTVA55.Location = new System.Drawing.Point(925, 23);
            this.txtTVA55.Name = "txtTVA55";
            this.txtTVA55.Size = new System.Drawing.Size(455, 23);
            this.txtTVA55.TabIndex = 389;
            // 
            // txtTVA196
            // 
            this.txtTVA196.AutoSize = true;
            this.txtTVA196.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTVA196.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTVA196.Location = new System.Drawing.Point(1386, 23);
            this.txtTVA196.Name = "txtTVA196";
            this.txtTVA196.Size = new System.Drawing.Size(455, 23);
            this.txtTVA196.TabIndex = 388;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(455, 23);
            this.label1.TabIndex = 384;
            this.label1.Text = "Total TTC";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(464, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(455, 23);
            this.label2.TabIndex = 385;
            this.label2.Text = "Total HT";
            this.label2.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(925, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(455, 23);
            this.label3.TabIndex = 386;
            this.label3.Text = "TVA 5.5 %";
            this.label3.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label4.Location = new System.Drawing.Point(1386, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(455, 23);
            this.label4.TabIndex = 387;
            this.label4.Text = "TVA 19.6 %";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel8.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 2;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel8.TabIndex = 545;
            // 
            // txtCodeContrat
            // 
            this.txtCodeContrat.AccAcceptNumbersOnly = false;
            this.txtCodeContrat.AccAllowComma = false;
            this.txtCodeContrat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeContrat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeContrat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeContrat.AccHidenValue = "";
            this.txtCodeContrat.AccNotAllowedChars = null;
            this.txtCodeContrat.AccReadOnly = false;
            this.txtCodeContrat.AccReadOnlyAllowDelete = false;
            this.txtCodeContrat.AccRequired = false;
            this.txtCodeContrat.BackColor = System.Drawing.Color.White;
            this.txtCodeContrat.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeContrat.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCodeContrat.ForeColor = System.Drawing.Color.Black;
            this.txtCodeContrat.Location = new System.Drawing.Point(147, 0);
            this.txtCodeContrat.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeContrat.MaxLength = 32767;
            this.txtCodeContrat.Multiline = false;
            this.txtCodeContrat.Name = "txtCodeContrat";
            this.txtCodeContrat.ReadOnly = false;
            this.txtCodeContrat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeContrat.Size = new System.Drawing.Size(135, 27);
            this.txtCodeContrat.TabIndex = 542;
            this.txtCodeContrat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeContrat.UseSystemPasswordChar = false;
            this.txtCodeContrat.Visible = false;
            // 
            // txtSelNoFacture
            // 
            this.txtSelNoFacture.AccAcceptNumbersOnly = false;
            this.txtSelNoFacture.AccAllowComma = false;
            this.txtSelNoFacture.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSelNoFacture.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSelNoFacture.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtSelNoFacture.AccHidenValue = "";
            this.txtSelNoFacture.AccNotAllowedChars = null;
            this.txtSelNoFacture.AccReadOnly = false;
            this.txtSelNoFacture.AccReadOnlyAllowDelete = false;
            this.txtSelNoFacture.AccRequired = false;
            this.txtSelNoFacture.BackColor = System.Drawing.Color.White;
            this.txtSelNoFacture.CustomBackColor = System.Drawing.Color.White;
            this.txtSelNoFacture.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtSelNoFacture.ForeColor = System.Drawing.Color.Black;
            this.txtSelNoFacture.Location = new System.Drawing.Point(8, 1);
            this.txtSelNoFacture.Margin = new System.Windows.Forms.Padding(2);
            this.txtSelNoFacture.MaxLength = 32767;
            this.txtSelNoFacture.Multiline = false;
            this.txtSelNoFacture.Name = "txtSelNoFacture";
            this.txtSelNoFacture.ReadOnly = false;
            this.txtSelNoFacture.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSelNoFacture.Size = new System.Drawing.Size(135, 27);
            this.txtSelNoFacture.TabIndex = 510;
            this.txtSelNoFacture.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSelNoFacture.UseSystemPasswordChar = false;
            this.txtSelNoFacture.Visible = false;
            // 
            // UserDocPrintFacture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.SystemColors.ControlLightLight;
            this.Controls.Add(this.tableLayoutPanel8);
            this.Controls.Add(this.txtCodeContrat);
            this.Controls.Add(this.txtSelNoFacture);
            this.Name = "UserDocPrintFacture";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Visualiser une facture";
            this.VisibleChanged += new System.EventHandler(this.UserDocPrintFacture_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.Frame1.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridCorps)).EndInit();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.Button cmdRechercheFacture;
        public System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtNoFacture;
        public iTalk.iTalk_TextBox_Small2 txtSelNoFacture;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public iTalk.iTalk_TextBox_Small2 lblCodeImmeuble;
        public iTalk.iTalk_TextBox_Small2 txtContrat;
        public iTalk.iTalk_TextBox_Small2 lblCodeGerant;
        public System.Windows.Forms.GroupBox Frame1;
        public System.Windows.Forms.RadioButton optContratManu;
        public System.Windows.Forms.RadioButton optDevis;
        public System.Windows.Forms.RadioButton optManu;
        public System.Windows.Forms.RadioButton optIntervention;
        public System.Windows.Forms.RadioButton optContrat;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdAnalytique;
        public System.Windows.Forms.Button cmdVisu;
        private System.Windows.Forms.LinkLabel Label42;
        private System.Windows.Forms.LinkLabel Label41;
        private System.Windows.Forms.LinkLabel Label40;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridCorps;
        public iTalk.iTalk_TextBox_Small2 txtCodeContrat;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label txtTVA55;
        public System.Windows.Forms.Label txtTVA196;
        public System.Windows.Forms.Label txtTotalTTC;
        public System.Windows.Forms.Label txtTotalHT;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
    }
}
