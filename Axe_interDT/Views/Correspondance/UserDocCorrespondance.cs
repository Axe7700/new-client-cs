﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.Views.SharedViews;
using CrystalDecisions.CrystalReports.Engine;
using Microsoft.Office.Interop.Word;

namespace Axe_interDT.Views.Correspondance
{
    public partial class UserDocCorrespondance : UserControl
    {
        public UserDocCorrespondance()
        {
            InitializeComponent();
        }

        //TODO .crystal report ==> some lines must be validated 
        private void cmdEditRobinet_Click(object sender, EventArgs e)
        {
            string sEtat = null;
            string sSQL = null;
            ReportDocument CR1 = new ReportDocument();
            sSQL = "UPDATE IMMEUBLE set RevRobinet = 0 where RevRobinet is null";
            General.Execute(sSQL);
            CR1.Load(General.ETATRevisionRobinet);
            /**
             *  CR1.Formulas(0) = "AnneeSaison ='" & txtAnneesaison & "'"
             *  CR1.Formulas(1) = "AnneeAlumage ='" & txtAnneeAllumage & "'"
             *  CR1.Formulas(2) = "DateDu ='" & txtDate & "'"
             *  CR1.Formulas(3) = "DateReponse ='" & txtdateReponse & "'"
             *///TODO. ==> id don't understund this lines.

            // CR.Formulas(0) = "Mois ='" & SSdbMois.Text & "'"


            CrystalReportFormView cr = new CrystalReportFormView(CR1, sEtat);
            cr.Show();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdImprimer_Click(object sender, EventArgs e)
        {
            string sEtat = null;
            string sSQL = null;
            sSQL = "UPDATE IMMEUBLE set ReceptionDTA = 0 where ReceptionDTA is null";
            General.Execute(sSQL);
            sSQL = "UPDATE    Immeuble Set DTAPositif = 0 WHERE     (DTAPositif IS NULL)";
            General.Execute(sSQL);
            sSQL = "UPDATE    Immeuble Set DTANegatif = 0 WHERE     (DTANegatif IS NULL)";
            General.Execute(sSQL);
            sSQL = "UPDATE    Immeuble Set DTAPositifSo = 0 WHERE     (DTAPositifSo IS NULL)";
            General.Execute(sSQL);
            //sEtat = "{Contrat.Resiliee} = 0 AND {Contrat.nonFacturable} = 0 " _
            //& " AND {immeuble.ReceptionDTA} = 1"
            //sEtat = " {immeuble.ReceptionDTA} = 0 and  {Contrat.Resiliee} = false " '  AND {Contrat.nonFacturable} = false"
            sEtat = "{Contrat.Resiliee} = false ";
            sEtat = sEtat + " AND {Immeuble.DTANegatif} = 0 AND {Immeuble.DTAPositif} = 0 AND {Immeuble.DTAPositifSo} = 0";
            if (OptSyndic.Checked == true)
            {

                if (!string.IsNullOrEmpty(txtCommercial.Text))
                {
                    sEtat = sEtat + " AND {Immeuble.CodeCommercial} ='" + txtCommercial.Text + "'";

                }
                if (!string.IsNullOrEmpty(txtSyndic1.Text))
                {
                    sEtat = sEtat + " AND {Table1.code1} >='" + txtSyndic1.Text + "'";
                }
                if (!string.IsNullOrEmpty(txtSyndic2.Text))
                {
                    sEtat = sEtat + " AND {Table1.code1} <='" + txtSyndic2.Text + "'";
                }

            }
            else if (OptImmeuble.Checked == true)
            {

                if (!string.IsNullOrEmpty(txtImmeuble1.Text))
                {
                    sEtat = sEtat + " AND {immeuble.codeimmeuble} >='" + txtImmeuble1.Text + "'";
                }
                if (!string.IsNullOrEmpty(txtImmeuble2.Text))
                {
                    sEtat = sEtat + " AND {immeuble.codeimmeuble} <='" + txtImmeuble2.Text + "'";
                }
            }
            if (General.sReleveCoproPoprietaire == "1")
            {
                if (!Opt1.Checked)
                {
                    //=== syndic, autres.
                    //sEtat = sEtat & " AND ({Qualification.Qualification} <> 'PART' or {Qualification.Qualification} is null))"
                    sEtat = sEtat + " AND ({Qualification.CodeQualif} <> 'PART' or isnull ({Qualification.CodeQualif}) ) ";
                }
                else if (!Opt2.Checked)
                {
                    //'=== Proprietaire.
                    sEtat = sEtat + " and {Qualification.CodeQualif} = 'part' ";
                }
            }
            ReportDocument CR1 = new ReportDocument();
            CR1.Load(General.ETATCourrierClient);
            CrystalReportFormView cr = new CrystalReportFormView(CR1, sEtat);
            cr.Show();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocCorrespondance_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;
            var sql = "SELECT CodeImmeuble,NCompte FROM IMMEUBLE order by CodeImmeuble";
            var ModAdo = new ModAdo();
            //General.CHEMINBASE ==> in General they set General.CHEMINBASE = adocnn.ConnectionString
            var DataTable = ModAdo.fc_OpenRecordSet(sql, null, "", General.adocnn);
            txtImmeuble1.DataSource = DataTable;
            txtImmeuble2.DataSource = DataTable;
            sheridan.InitialiseCombo(txtSyndic1, "SELECT Code1 FROM Table1 order by Code1", "Code1", false, General.adocnn);
            sheridan.InitialiseCombo(txtSyndic2, "SELECT Code1 FROM Table1 order by Code1", "Code1", false, General.adocnn);
            sheridan.InitialiseCombo(txtCommercial, "SELECT CodeParticulier,CodeImmeuble,NCompte FROM ImmeublePart order by CodeParticulier", "", false,General.adocnn);

            if (General.sReleveCoproPoprietaire == "1")
                Frame1.Visible = true;
            else
                Frame1.Visible = false;
            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocCorrespondance");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptImmeuble_CheckedChanged(object sender, EventArgs e)
        {
            if (OptImmeuble.Checked)
            {
                txtImmeuble1.Text = "";
                txtImmeuble2.Text = "";
                FrameGerant.Visible = false;
                FrameImmeuble.Visible = true;
                txtImmeuble1.Focus();
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptSyndic_CheckedChanged(object sender, EventArgs e)
        {
            if (OptSyndic.Checked)
            {
                txtSyndic1.Text = "";
                txtSyndic2.Text = "";
                txtCommercial.Text = "";
                FrameGerant.Visible = true;
                FrameImmeuble.Visible = false;
                txtSyndic1.Focus();
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCommercial_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT DISTINCT Personnel.Matricule, Personnel.Nom " + " FROM Personnel INNER JOIN Table1 ON Personnel.Matricule=Table1.Commercial ORDER BY Personnel.Matricule";

            sheridan.InitialiseCombo(txtCommercial, General.sSQL, "Matricule", false);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtImmeuble1_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            txtImmeuble1.DisplayLayout.Bands[0].Columns["NCompte"].Hidden = true;
            txtImmeuble1.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Header.Caption = "";
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtImmeuble2_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            txtImmeuble2.DisplayLayout.Bands[0].Columns["NCompte"].Hidden = true;
            txtImmeuble2.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Header.Caption = "";
        }
    }
}
