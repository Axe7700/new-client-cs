namespace Axe_interDT.Views.Correspondance
{
    partial class UserDocCorrespondance
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab13 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdImprimer = new System.Windows.Forms.Button();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.FrameImmeuble = new System.Windows.Forms.TableLayoutPanel();
            this.txtImmeuble1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtImmeuble2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.OptSyndic = new System.Windows.Forms.RadioButton();
            this.OptImmeuble = new System.Windows.Forms.RadioButton();
            this.FrameGerant = new System.Windows.Forms.TableLayoutPanel();
            this.txtSyndic1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSyndic2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCommercial = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.Frame1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.Opt1 = new System.Windows.Forms.RadioButton();
            this.Opt2 = new System.Windows.Forms.RadioButton();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.lblNoIntervention = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.txtDate = new iTalk.iTalk_TextBox_Small2();
            this.txtdateReponse = new iTalk.iTalk_TextBox_Small2();
            this.txtAnneeAllumage = new iTalk.iTalk_TextBox_Small2();
            this.txtAnneesaison = new iTalk.iTalk_TextBox_Small2();
            this.cmdEditRobinet = new System.Windows.Forms.Button();
            this.SSTab1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.FrameImmeuble.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtImmeuble1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImmeuble2)).BeginInit();
            this.FrameGerant.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyndic1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyndic2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommercial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).BeginInit();
            this.Frame1.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).BeginInit();
            this.SSTab1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.tableLayoutPanel1);
            this.ultraTabPageControl1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1848, 933);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.cmdImprimer, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.ultraGroupBox1, 0, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1848, 933);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // cmdImprimer
            // 
            this.cmdImprimer.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmdImprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdImprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdImprimer.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdImprimer.FlatAppearance.BorderSize = 0;
            this.cmdImprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImprimer.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdImprimer.Image = global::Axe_interDT.Properties.Resources.Printer_24x24;
            this.cmdImprimer.Location = new System.Drawing.Point(1781, 2);
            this.cmdImprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdImprimer.Name = "cmdImprimer";
            this.cmdImprimer.Size = new System.Drawing.Size(65, 42);
            this.cmdImprimer.TabIndex = 543;
            this.cmdImprimer.UseVisualStyleBackColor = false;
            this.cmdImprimer.Click += new System.EventHandler(this.cmdImprimer_Click);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.tableLayoutPanel2);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 50);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1842, 880);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "COURRIER DTA";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 116F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.FrameImmeuble, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.OptSyndic, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.OptImmeuble, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.FrameGerant, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.Frame1, 1, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 21);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 39F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1836, 856);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // FrameImmeuble
            // 
            this.FrameImmeuble.ColumnCount = 6;
            this.FrameImmeuble.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.FrameImmeuble.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.FrameImmeuble.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.FrameImmeuble.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.FrameImmeuble.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.FrameImmeuble.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.FrameImmeuble.Controls.Add(this.txtImmeuble1, 0, 0);
            this.FrameImmeuble.Controls.Add(this.label3, 0, 0);
            this.FrameImmeuble.Controls.Add(this.label4, 2, 0);
            this.FrameImmeuble.Controls.Add(this.txtImmeuble2, 3, 0);
            this.FrameImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FrameImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.FrameImmeuble.Location = new System.Drawing.Point(119, 42);
            this.FrameImmeuble.Name = "FrameImmeuble";
            this.FrameImmeuble.RowCount = 1;
            this.FrameImmeuble.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.FrameImmeuble.Size = new System.Drawing.Size(1714, 32);
            this.FrameImmeuble.TabIndex = 540;
            this.FrameImmeuble.Visible = false;
            // 
            // txtImmeuble1
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtImmeuble1.DisplayLayout.Appearance = appearance1;
            this.txtImmeuble1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtImmeuble1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.txtImmeuble1.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtImmeuble1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.txtImmeuble1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtImmeuble1.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.txtImmeuble1.DisplayLayout.MaxColScrollRegions = 1;
            this.txtImmeuble1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtImmeuble1.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txtImmeuble1.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.txtImmeuble1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtImmeuble1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.txtImmeuble1.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtImmeuble1.DisplayLayout.Override.CellAppearance = appearance8;
            this.txtImmeuble1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtImmeuble1.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.txtImmeuble1.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.txtImmeuble1.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.txtImmeuble1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtImmeuble1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.txtImmeuble1.DisplayLayout.Override.RowAppearance = appearance11;
            this.txtImmeuble1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtImmeuble1.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.txtImmeuble1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtImmeuble1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtImmeuble1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtImmeuble1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtImmeuble1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtImmeuble1.Location = new System.Drawing.Point(37, 3);
            this.txtImmeuble1.Name = "txtImmeuble1";
            this.txtImmeuble1.Size = new System.Drawing.Size(513, 27);
            this.txtImmeuble1.TabIndex = 503;
            this.txtImmeuble1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.txtImmeuble1_InitializeLayout);
            // 
            // label3
            // 
            this.label3.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(20, 32);
            this.label3.TabIndex = 384;
            this.label3.Text = "Du";
            // 
            // label4
            // 
            this.label4.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label4.Location = new System.Drawing.Point(556, 6);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(27, 19);
            this.label4.TabIndex = 384;
            this.label4.Text = "Au";
            // 
            // txtImmeuble2
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtImmeuble2.DisplayLayout.Appearance = appearance13;
            this.txtImmeuble2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtImmeuble2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.txtImmeuble2.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtImmeuble2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.txtImmeuble2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtImmeuble2.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.txtImmeuble2.DisplayLayout.MaxColScrollRegions = 1;
            this.txtImmeuble2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtImmeuble2.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txtImmeuble2.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.txtImmeuble2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtImmeuble2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.txtImmeuble2.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtImmeuble2.DisplayLayout.Override.CellAppearance = appearance20;
            this.txtImmeuble2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtImmeuble2.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.txtImmeuble2.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.txtImmeuble2.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.txtImmeuble2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtImmeuble2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.txtImmeuble2.DisplayLayout.Override.RowAppearance = appearance23;
            this.txtImmeuble2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtImmeuble2.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.txtImmeuble2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtImmeuble2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtImmeuble2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtImmeuble2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtImmeuble2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtImmeuble2.Location = new System.Drawing.Point(589, 3);
            this.txtImmeuble2.Name = "txtImmeuble2";
            this.txtImmeuble2.Size = new System.Drawing.Size(513, 27);
            this.txtImmeuble2.TabIndex = 504;
            this.txtImmeuble2.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.txtImmeuble2_InitializeLayout);
            // 
            // OptSyndic
            // 
            this.OptSyndic.AutoSize = true;
            this.OptSyndic.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.OptSyndic.Location = new System.Drawing.Point(3, 3);
            this.OptSyndic.Name = "OptSyndic";
            this.OptSyndic.Size = new System.Drawing.Size(83, 23);
            this.OptSyndic.TabIndex = 538;
            this.OptSyndic.Text = "GERANT";
            this.OptSyndic.UseVisualStyleBackColor = true;
            this.OptSyndic.CheckedChanged += new System.EventHandler(this.OptSyndic_CheckedChanged);
            // 
            // OptImmeuble
            // 
            this.OptImmeuble.AutoSize = true;
            this.OptImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.OptImmeuble.Location = new System.Drawing.Point(3, 42);
            this.OptImmeuble.Name = "OptImmeuble";
            this.OptImmeuble.Size = new System.Drawing.Size(102, 23);
            this.OptImmeuble.TabIndex = 538;
            this.OptImmeuble.Text = "IMMEUBLE";
            this.OptImmeuble.UseVisualStyleBackColor = true;
            this.OptImmeuble.CheckedChanged += new System.EventHandler(this.OptImmeuble_CheckedChanged);
            // 
            // FrameGerant
            // 
            this.FrameGerant.ColumnCount = 6;
            this.FrameGerant.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.FrameGerant.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.FrameGerant.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.FrameGerant.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.FrameGerant.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 101F));
            this.FrameGerant.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.FrameGerant.Controls.Add(this.txtSyndic1, 0, 0);
            this.FrameGerant.Controls.Add(this.label33, 0, 0);
            this.FrameGerant.Controls.Add(this.label1, 2, 0);
            this.FrameGerant.Controls.Add(this.label2, 4, 0);
            this.FrameGerant.Controls.Add(this.txtSyndic2, 3, 0);
            this.FrameGerant.Controls.Add(this.txtCommercial, 5, 0);
            this.FrameGerant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FrameGerant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.FrameGerant.Location = new System.Drawing.Point(119, 3);
            this.FrameGerant.Name = "FrameGerant";
            this.FrameGerant.RowCount = 1;
            this.FrameGerant.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.FrameGerant.Size = new System.Drawing.Size(1714, 33);
            this.FrameGerant.TabIndex = 541;
            // 
            // txtSyndic1
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtSyndic1.DisplayLayout.Appearance = appearance25;
            this.txtSyndic1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtSyndic1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.txtSyndic1.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtSyndic1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.txtSyndic1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtSyndic1.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.txtSyndic1.DisplayLayout.MaxColScrollRegions = 1;
            this.txtSyndic1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSyndic1.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txtSyndic1.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.txtSyndic1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtSyndic1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.txtSyndic1.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtSyndic1.DisplayLayout.Override.CellAppearance = appearance32;
            this.txtSyndic1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtSyndic1.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.txtSyndic1.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.txtSyndic1.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.txtSyndic1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtSyndic1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.txtSyndic1.DisplayLayout.Override.RowAppearance = appearance35;
            this.txtSyndic1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtSyndic1.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.txtSyndic1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtSyndic1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtSyndic1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtSyndic1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSyndic1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtSyndic1.Location = new System.Drawing.Point(37, 3);
            this.txtSyndic1.Name = "txtSyndic1";
            this.txtSyndic1.Size = new System.Drawing.Size(509, 27);
            this.txtSyndic1.TabIndex = 0;
            // 
            // label33
            // 
            this.label33.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(20, 33);
            this.label33.TabIndex = 384;
            this.label33.Text = "Du";
            // 
            // label1
            // 
            this.label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(552, 7);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(27, 19);
            this.label1.TabIndex = 384;
            this.label1.Text = "Au";
            // 
            // label2
            // 
            this.label2.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(1100, 7);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(92, 19);
            this.label2.TabIndex = 384;
            this.label2.Text = "Commercial";
            // 
            // txtSyndic2
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtSyndic2.DisplayLayout.Appearance = appearance37;
            this.txtSyndic2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtSyndic2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.txtSyndic2.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtSyndic2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.txtSyndic2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtSyndic2.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.txtSyndic2.DisplayLayout.MaxColScrollRegions = 1;
            this.txtSyndic2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtSyndic2.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txtSyndic2.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.txtSyndic2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtSyndic2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.txtSyndic2.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtSyndic2.DisplayLayout.Override.CellAppearance = appearance44;
            this.txtSyndic2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtSyndic2.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.txtSyndic2.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.txtSyndic2.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.txtSyndic2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtSyndic2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.txtSyndic2.DisplayLayout.Override.RowAppearance = appearance47;
            this.txtSyndic2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtSyndic2.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.txtSyndic2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtSyndic2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtSyndic2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtSyndic2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSyndic2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtSyndic2.Location = new System.Drawing.Point(585, 3);
            this.txtSyndic2.Name = "txtSyndic2";
            this.txtSyndic2.Size = new System.Drawing.Size(509, 27);
            this.txtSyndic2.TabIndex = 1;
            // 
            // txtCommercial
            // 
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtCommercial.DisplayLayout.Appearance = appearance49;
            this.txtCommercial.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtCommercial.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance50.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance50.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance50.BorderColor = System.Drawing.SystemColors.Window;
            this.txtCommercial.DisplayLayout.GroupByBox.Appearance = appearance50;
            appearance51.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtCommercial.DisplayLayout.GroupByBox.BandLabelAppearance = appearance51;
            this.txtCommercial.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance52.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance52.BackColor2 = System.Drawing.SystemColors.Control;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance52.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtCommercial.DisplayLayout.GroupByBox.PromptAppearance = appearance52;
            this.txtCommercial.DisplayLayout.MaxColScrollRegions = 1;
            this.txtCommercial.DisplayLayout.MaxRowScrollRegions = 1;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtCommercial.DisplayLayout.Override.ActiveCellAppearance = appearance53;
            appearance54.BackColor = System.Drawing.SystemColors.Highlight;
            appearance54.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txtCommercial.DisplayLayout.Override.ActiveRowAppearance = appearance54;
            this.txtCommercial.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtCommercial.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            this.txtCommercial.DisplayLayout.Override.CardAreaAppearance = appearance55;
            appearance56.BorderColor = System.Drawing.Color.Silver;
            appearance56.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtCommercial.DisplayLayout.Override.CellAppearance = appearance56;
            this.txtCommercial.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtCommercial.DisplayLayout.Override.CellPadding = 0;
            appearance57.BackColor = System.Drawing.SystemColors.Control;
            appearance57.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance57.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance57.BorderColor = System.Drawing.SystemColors.Window;
            this.txtCommercial.DisplayLayout.Override.GroupByRowAppearance = appearance57;
            appearance58.TextHAlignAsString = "Left";
            this.txtCommercial.DisplayLayout.Override.HeaderAppearance = appearance58;
            this.txtCommercial.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtCommercial.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.BorderColor = System.Drawing.Color.Silver;
            this.txtCommercial.DisplayLayout.Override.RowAppearance = appearance59;
            this.txtCommercial.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance60.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtCommercial.DisplayLayout.Override.TemplateAddRowAppearance = appearance60;
            this.txtCommercial.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtCommercial.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtCommercial.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtCommercial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCommercial.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCommercial.Location = new System.Drawing.Point(1201, 3);
            this.txtCommercial.Name = "txtCommercial";
            this.txtCommercial.Size = new System.Drawing.Size(510, 27);
            this.txtCommercial.TabIndex = 2;
            this.txtCommercial.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.txtCommercial_BeforeDropDown);
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.Opt1);
            this.Frame1.Controls.Add(this.Opt2);
            this.Frame1.Location = new System.Drawing.Point(119, 80);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(200, 70);
            this.Frame1.TabIndex = 542;
            // 
            // Opt1
            // 
            this.Opt1.AutoSize = true;
            this.Opt1.Location = new System.Drawing.Point(7, 35);
            this.Opt1.Name = "Opt1";
            this.Opt1.Size = new System.Drawing.Size(110, 23);
            this.Opt1.TabIndex = 1;
            this.Opt1.Text = "propriétaire";
            this.Opt1.UseVisualStyleBackColor = true;
            // 
            // Opt2
            // 
            this.Opt2.AutoSize = true;
            this.Opt2.Checked = true;
            this.Opt2.Location = new System.Drawing.Point(7, 6);
            this.Opt2.Name = "Opt2";
            this.Opt2.Size = new System.Drawing.Size(124, 23);
            this.Opt2.TabIndex = 0;
            this.Opt2.TabStop = true;
            this.Opt2.Text = "Syndics, Autre";
            this.Opt2.UseVisualStyleBackColor = true;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.ultraGroupBox2);
            this.ultraTabPageControl2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1519, 835);
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.tableLayoutPanel5);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ultraGroupBox2.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1519, 835);
            this.ultraGroupBox2.TabIndex = 0;
            this.ultraGroupBox2.Text = "Revision de robinet";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 3;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 227F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 47.36842F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 52.63158F));
            this.tableLayoutPanel5.Controls.Add(this.lblNoIntervention, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.label5, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.label6, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.label7, 0, 4);
            this.tableLayoutPanel5.Controls.Add(this.txtDate, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.txtdateReponse, 1, 2);
            this.tableLayoutPanel5.Controls.Add(this.txtAnneeAllumage, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.txtAnneesaison, 1, 4);
            this.tableLayoutPanel5.Controls.Add(this.cmdEditRobinet, 2, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 21);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 6;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 47F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1513, 811);
            this.tableLayoutPanel5.TabIndex = 0;
            // 
            // lblNoIntervention
            // 
            this.lblNoIntervention.AutoSize = true;
            this.lblNoIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblNoIntervention.Location = new System.Drawing.Point(3, 47);
            this.lblNoIntervention.Name = "lblNoIntervention";
            this.lblNoIntervention.Size = new System.Drawing.Size(96, 19);
            this.lblNoIntervention.TabIndex = 540;
            this.lblNoIntervention.Text = "Date d\'envoi";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label5.Location = new System.Drawing.Point(3, 77);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(125, 19);
            this.label5.TabIndex = 540;
            this.label5.Text = "Date de réponse";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label6.Location = new System.Drawing.Point(3, 107);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(158, 30);
            this.label6.TabIndex = 540;
            this.label6.Text = "Année d\'allumage du chauffage";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label7.Location = new System.Drawing.Point(3, 137);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(200, 19);
            this.label7.TabIndex = 540;
            this.label7.Text = "Année de saison de chauffe";
            // 
            // txtDate
            // 
            this.txtDate.AccAcceptNumbersOnly = false;
            this.txtDate.AccAllowComma = false;
            this.txtDate.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDate.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDate.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDate.AccHidenValue = "";
            this.txtDate.AccNotAllowedChars = null;
            this.txtDate.AccReadOnly = false;
            this.txtDate.AccReadOnlyAllowDelete = false;
            this.txtDate.AccRequired = false;
            this.txtDate.BackColor = System.Drawing.Color.White;
            this.txtDate.CustomBackColor = System.Drawing.Color.White;
            this.txtDate.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDate.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDate.ForeColor = System.Drawing.Color.Black;
            this.txtDate.Location = new System.Drawing.Point(229, 49);
            this.txtDate.Margin = new System.Windows.Forms.Padding(2);
            this.txtDate.MaxLength = 32767;
            this.txtDate.Multiline = false;
            this.txtDate.Name = "txtDate";
            this.txtDate.ReadOnly = false;
            this.txtDate.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDate.Size = new System.Drawing.Size(605, 27);
            this.txtDate.TabIndex = 541;
            this.txtDate.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDate.UseSystemPasswordChar = false;
            // 
            // txtdateReponse
            // 
            this.txtdateReponse.AccAcceptNumbersOnly = false;
            this.txtdateReponse.AccAllowComma = false;
            this.txtdateReponse.AccBackgroundColor = System.Drawing.Color.White;
            this.txtdateReponse.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtdateReponse.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtdateReponse.AccHidenValue = "";
            this.txtdateReponse.AccNotAllowedChars = null;
            this.txtdateReponse.AccReadOnly = false;
            this.txtdateReponse.AccReadOnlyAllowDelete = false;
            this.txtdateReponse.AccRequired = false;
            this.txtdateReponse.BackColor = System.Drawing.Color.White;
            this.txtdateReponse.CustomBackColor = System.Drawing.Color.White;
            this.txtdateReponse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtdateReponse.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtdateReponse.ForeColor = System.Drawing.Color.Black;
            this.txtdateReponse.Location = new System.Drawing.Point(229, 79);
            this.txtdateReponse.Margin = new System.Windows.Forms.Padding(2);
            this.txtdateReponse.MaxLength = 32767;
            this.txtdateReponse.Multiline = false;
            this.txtdateReponse.Name = "txtdateReponse";
            this.txtdateReponse.ReadOnly = false;
            this.txtdateReponse.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtdateReponse.Size = new System.Drawing.Size(605, 27);
            this.txtdateReponse.TabIndex = 541;
            this.txtdateReponse.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtdateReponse.UseSystemPasswordChar = false;
            // 
            // txtAnneeAllumage
            // 
            this.txtAnneeAllumage.AccAcceptNumbersOnly = false;
            this.txtAnneeAllumage.AccAllowComma = false;
            this.txtAnneeAllumage.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAnneeAllumage.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAnneeAllumage.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtAnneeAllumage.AccHidenValue = "";
            this.txtAnneeAllumage.AccNotAllowedChars = null;
            this.txtAnneeAllumage.AccReadOnly = false;
            this.txtAnneeAllumage.AccReadOnlyAllowDelete = false;
            this.txtAnneeAllumage.AccRequired = false;
            this.txtAnneeAllumage.BackColor = System.Drawing.Color.White;
            this.txtAnneeAllumage.CustomBackColor = System.Drawing.Color.White;
            this.txtAnneeAllumage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAnneeAllumage.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtAnneeAllumage.ForeColor = System.Drawing.Color.Black;
            this.txtAnneeAllumage.Location = new System.Drawing.Point(229, 109);
            this.txtAnneeAllumage.Margin = new System.Windows.Forms.Padding(2);
            this.txtAnneeAllumage.MaxLength = 32767;
            this.txtAnneeAllumage.Multiline = false;
            this.txtAnneeAllumage.Name = "txtAnneeAllumage";
            this.txtAnneeAllumage.ReadOnly = false;
            this.txtAnneeAllumage.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAnneeAllumage.Size = new System.Drawing.Size(605, 27);
            this.txtAnneeAllumage.TabIndex = 541;
            this.txtAnneeAllumage.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAnneeAllumage.UseSystemPasswordChar = false;
            // 
            // txtAnneesaison
            // 
            this.txtAnneesaison.AccAcceptNumbersOnly = false;
            this.txtAnneesaison.AccAllowComma = false;
            this.txtAnneesaison.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAnneesaison.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAnneesaison.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtAnneesaison.AccHidenValue = "";
            this.txtAnneesaison.AccNotAllowedChars = null;
            this.txtAnneesaison.AccReadOnly = false;
            this.txtAnneesaison.AccReadOnlyAllowDelete = false;
            this.txtAnneesaison.AccRequired = false;
            this.txtAnneesaison.BackColor = System.Drawing.Color.White;
            this.txtAnneesaison.CustomBackColor = System.Drawing.Color.White;
            this.txtAnneesaison.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAnneesaison.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtAnneesaison.ForeColor = System.Drawing.Color.Black;
            this.txtAnneesaison.Location = new System.Drawing.Point(229, 139);
            this.txtAnneesaison.Margin = new System.Windows.Forms.Padding(2);
            this.txtAnneesaison.MaxLength = 32767;
            this.txtAnneesaison.Multiline = false;
            this.txtAnneesaison.Name = "txtAnneesaison";
            this.txtAnneesaison.ReadOnly = false;
            this.txtAnneesaison.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAnneesaison.Size = new System.Drawing.Size(605, 27);
            this.txtAnneesaison.TabIndex = 541;
            this.txtAnneesaison.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAnneesaison.UseSystemPasswordChar = false;
            // 
            // cmdEditRobinet
            // 
            this.cmdEditRobinet.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmdEditRobinet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdEditRobinet.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdEditRobinet.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdEditRobinet.FlatAppearance.BorderSize = 0;
            this.cmdEditRobinet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdEditRobinet.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdEditRobinet.Image = global::Axe_interDT.Properties.Resources.Printer_24x24;
            this.cmdEditRobinet.Location = new System.Drawing.Point(1446, 2);
            this.cmdEditRobinet.Margin = new System.Windows.Forms.Padding(2);
            this.cmdEditRobinet.Name = "cmdEditRobinet";
            this.cmdEditRobinet.Size = new System.Drawing.Size(65, 42);
            this.cmdEditRobinet.TabIndex = 542;
            this.cmdEditRobinet.UseVisualStyleBackColor = false;
            this.cmdEditRobinet.Click += new System.EventHandler(this.cmdEditRobinet_Click);
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl2);
            this.SSTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSTab1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSTab1.Location = new System.Drawing.Point(0, 0);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.SSTab1.Size = new System.Drawing.Size(1850, 957);
            this.SSTab1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Excel;
            this.SSTab1.TabIndex = 410;
            ultraTab13.TabPage = this.ultraTabPageControl1;
            ultraTab13.Text = "Courrier DTA";
            ultraTab1.TabPage = this.ultraTabPageControl2;
            ultraTab1.Text = "Révision de robinet";
            this.SSTab1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab13,
            ultraTab1});
            this.SSTab1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1848, 933);
            // 
            // UserDocCorrespondance
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.SSTab1);
            this.Name = "UserDocCorrespondance";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "UserDocCorrespondance";
            this.VisibleChanged += new System.EventHandler(this.UserDocCorrespondance_VisibleChanged);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.FrameImmeuble.ResumeLayout(false);
            this.FrameImmeuble.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtImmeuble1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtImmeuble2)).EndInit();
            this.FrameGerant.ResumeLayout(false);
            this.FrameGerant.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyndic1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtSyndic2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtCommercial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Frame1)).EndInit();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            this.ultraTabPageControl2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).EndInit();
            this.SSTab1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public Infragistics.Win.UltraWinTabControl.UltraTabControl SSTab1;
        public Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        public Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.RadioButton OptSyndic;
        public System.Windows.Forms.RadioButton OptImmeuble;
        private System.Windows.Forms.TableLayoutPanel FrameGerant;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel FrameImmeuble;
        public Infragistics.Win.UltraWinGrid.UltraCombo txtImmeuble1;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label4;
        public Infragistics.Win.UltraWinGrid.UltraCombo txtImmeuble2;
        public Infragistics.Win.UltraWinGrid.UltraCombo txtSyndic1;
        public Infragistics.Win.UltraWinGrid.UltraCombo txtSyndic2;
        public Infragistics.Win.UltraWinGrid.UltraCombo txtCommercial;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        public System.Windows.Forms.Label lblNoIntervention;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label7;
        public iTalk.iTalk_TextBox_Small2 txtDate;
        public iTalk.iTalk_TextBox_Small2 txtdateReponse;
        public iTalk.iTalk_TextBox_Small2 txtAnneeAllumage;
        public iTalk.iTalk_TextBox_Small2 txtAnneesaison;
        public System.Windows.Forms.Button cmdEditRobinet;
        public System.Windows.Forms.Button cmdImprimer;
        private Infragistics.Win.Misc.UltraGroupBox Frame1;
        private System.Windows.Forms.RadioButton Opt1;
        private System.Windows.Forms.RadioButton Opt2;
    }
}
