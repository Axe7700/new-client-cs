﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Contrat.FicheGMAO.Forms;
using Axe_interDT.Views.Intervention;
using Axe_interDT.Views.Intervention.Forms;
using Axe_interDT.Views.Theme.CustomMessageBox;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using iTalk;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Axe_interDT.Views.FicheVisiteEntretient
{
    public partial class UserIntervP2V2 : UserControl
    {
        private struct tpNumSemaine
        {
            public string sNumSemaine;
            public string sDateDu;
            public string sDateAu;
        }

        private struct tpMois
        {
            public string sNumMois;
            public string sLibelle;
            public string sDateDu;
            public string sDateAu;
        }

        //===> Mondir le 01.02.2021, ajout des modfis de la version V29.01.2021
        public string sMatRow { get; set; }
        //===> Fin Modif Mondir

        DataTable rsVisiteSimul;
        ModAdo rsVisiteSimulAdo = new ModAdo();
        DataTable rsVisiteConsult;
        ModAdo rsVisiteConsultAdo = new ModAdo();
        DataTable rsVisite;
        ModAdo rsVisiteAdo = new ModAdo();
        DataTable rsOpP2;
        ModAdo rsOpP2Ado = new ModAdo();
        DataTable rsOpP2Histo;
        ModAdo rsOpP2HistoAdo = new ModAdo();
        DataTable rsOpTaches;
        ModAdo rsOpTachesAdo = new ModAdo();
        DataTable rsOpTachesHisto;
        ModAdo rsOpTachesHistoAdo = new ModAdo();
        DataTable rsHisto;
        ModAdo rsHistoAdo = new ModAdo();
        DataTable rsGammeSimul;
        ModAdo rsGammeSimulAdo = new ModAdo();

        //===> Mondir le 28.01.2021, ajout des modfis de la version V27.01.2021
        private DataTable rsGMAOinter;
        private ModAdo rsGMAOinterModAdo = new ModAdo();
        private DataTable rsGMAOimmeuble;
        private ModAdo rsGMAOimmeubleModAdo = new ModAdo();
        private DataTable rsGMAOcds;
        private ModAdo rsGMAOcdsModAdo = new ModAdo();

        public string sOrderByGAMOinterv { get; set; }
        public string sOrderByGAMOimmeuble { get; set; }
        public string sOrderByGAMOSecteur { get; set; }
        //===> Fin Modif Mondir

        const short cGridListe = 1;
        const short cPlanningGraphique = 3;
        const short cCreationVisite = 2;
        const short cTachesAnnuel = 0;
        const short cAvisDePassage = 4;
        const short cNbeGammeFille = 5;
        const string cGammeFille = "Gamme fille";
        const string cGammeLib = "Gamme";

        string sOrderByPlanif;
        string sOrderByHisto;
        string sOrderBySimul;


        int iOldColHisto;
        int iOldCol;

        bool bTriAsc;

        object lTab;

        public UserIntervP2V2()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cdmSend2PDA_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cdmSend2PDA_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            if (General.sEnvoiePdaCorrective == "1")
            {

                fc_sendCreate(false);

            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La fonction PDA n'est pas installée.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            ///'fc_LoadRAMI
            ///'fc_SavPosFormPlan
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="bModeSuppession"></param>
        private void fc_sendCreate(bool bModeSuppession)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_sendCreate() - Enter In The Function - bModeSuppession = {bModeSuppession}");
            //===> Fin Modif Mondir
            DataTable rsSend = default(DataTable);
            DataTable rsID = default(DataTable);
            DataTable rsSw = default(DataTable);
            ModAdo rsSendAdo = new ModAdo();
            ModAdo rsIDAdo = new ModAdo();
            ModAdo rsSwAdo = new ModAdo();
            string sErreur = null;
            string sSQLID = null;
            bool bok = false;
            string[] sTab = null;
            string sWhere = null;
            string sWhereTab = null;
            try
            {

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;


                ssGridVisite.UpdateData();

                if (rsVisite == null)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'y a aucune intervention à envoyer.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    return;
                }

                //Set rsSend = fc_OpenRecordSet(rsRAMISend.Source)
                rsSend = rsVisite;
                rsSendAdo = rsVisiteAdo;
                if (rsSend.Rows.Count == 0)
                {

                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Opération annulée, il n'y a aucune intervention à envoyer.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    rsSend.Dispose();

                    rsSend = null;

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    return;
                }

                if (bModeSuppession == true)//TESTED
                {
                    //===prepare la clause where.
                    rsSw = rsVisite;

                    if (rsSw.Rows.Count > 0)
                    {

                        //=== filtre les enregistrements selectionées.
                        //rsSw.Filter = ADODB.FilterGroupEnum.adFilterNone;
                        // rsSw.Filter = " selection = 1";

                        var rsSwFilter = rsSw.Select("selection = 1");

                        if (rsSwFilter.Count() == 0)
                        {
                            return;
                        }

                        //=== prépare la clause where avec les numéros d'interventions séléectionnées.//tESTED
                        sWhere = "";
                        foreach (DataRow rsSwFilterRow in rsSwFilter)
                        {
                            if (string.IsNullOrEmpty(sWhere))
                            {
                                sWhere = " WHERE (nointervention =" + rsSwFilterRow["NoIntervention"];
                            }
                            else
                            {
                                sWhere = sWhere + " OR nointervention =" + rsSwFilterRow["NoIntervention"];
                            }
                            // rsSw.MoveNext();
                        }
                        if (!string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = sWhere + ")";
                        }
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune intervention sélectionnée.", "Opération annulée");

                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                        return;
                    }
                    rsSw.Dispose();
                    rsSw = null;

                    //=== extrait la clause WHERE du recordset source.

                    sSQLID = rsSendAdo.SDArsAdo.SelectCommand.CommandText;
                    //sTab = Split(sSQLID, "WHERE")
                    string[] stringSeparators = new string[] { "WHERE" };
                    sTab = sSQLID.Split(stringSeparators, StringSplitOptions.None);

                    sWhereTab = sTab[1];

                    //sSQLID = "SELECT     Intervention.Selection, Intervention.NoInter, Intervention.NoLigne, Intervention.CodeEtatPDA, " _
                    //& "  Intervention.NoIntervention AS ID, Intervention.CodeImmeuble, Intervention.PDAB_Noauto, Immeuble.Adresse, Immeuble.CodePostal," _
                    //& "   Immeuble.Ville, Intervention.PDAB_Libelle, Intervention.CodeEtat, Intervention.Intervenant, Personnel.Nom, Personnel.Prenom," _
                    //& "  Intervention.DatePrevue, Intervention.Secteur,  SEC_Secteur.SEC_Libelle AS NomSecteur, SEC_Secteur.SEC_Code AS PrenomSecteur, " _
                    //& "  Intervention.PriseRV, Intervention.InfoRV, Intervention.DateRV, Intervention.HeureRV, Intervention.CommRV," _
                    //& "  Intervention.DateRealise, Intervention.ID_PDA, Intervention.PDAB_IDBarre, Intervention.Cop_NoAuto," _
                    //& " Intervention.COP_NoContrat, Intervention.CreeLe, Intervention.CreePar, Intervention.ModifierLe, Intervention.ModifierPar," _
                    //& "  Intervention.EnvoyeLe ,Intervention.CompteurObli, Intervention.EnvoyePar, Intervention.Nointervention, PDAID_PDA_ID.PDAID_ID, Immeuble.longitude, Immeuble.latitude "

                    //sSQLID = sSQLID & " FROM         Intervention INNER JOIN" _
                    //& "  Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN " _
                    //& " PDAID_PDA_ID ON Intervention.NoInter = PDAID_PDA_ID.PDAID_NoIntervention LEFT OUTER JOIN " _
                    //& "  SEC_Secteur ON Intervention.Secteur = SEC_Secteur.SEC_Code LEFT OUTER JOIN " _
                    //& "  Personnel ON Intervention.Intervenant = Personnel.Matricule"


                    sSQLID = "SELECT     Intervention.Selection, Intervention.NumFicheStandard, Intervention.NoIntervention, Intervention.NoLigne, Intervention.CodeEtat, "
                        + " Intervention.NoIntervention AS ID, Intervention.CodeImmeuble, Intervention.PDAB_Noauto, Immeuble.Adresse, Immeuble.CodePostal, Immeuble.Ville, "
                        + " Intervention.PDAB_Libelle, Intervention.Intervenant, Personnel.Nom, Intervention.DateSaisie, Personnel.Prenom, Intervention.DatePrevue, "
                        + " Intervention.DateVisite, Intervention.HeurePrevue, Intervention.HeureDebutP, Intervention.CodeEtatPDA, Intervention.Article, Intervention.Designation, "
                        + " Intervention.CompteurObli, Intervention.DateRealise, Intervention.ID_PDA, Intervention.PDAB_IDBarre, Intervention.COP_NoAuto, "
                        + " Intervention.COP_NoContrat, Intervention.CreeLe, Intervention.CreePar, Intervention.ModifierLe, Intervention.ModifierPar, Intervention.EnvoyeLe, "
                        + " Intervention.EnvoyePar, Intervention.HeureFinP, Immeuble.Latitude, Immeuble.Longitude, Intervention.DPeriodeDu, Intervention.DPeriodeAu, "
                        + " Intervention.ALAR_Code AS ALAR_Code_Inter, Intervention.NoDecade, Intervention.Commentaire ";

                    sSQLID = sSQLID + " FROM         Intervention INNER JOIN "
                        + " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN "
                        + " Personnel ON Intervention.Intervenant = Personnel.Matricule ";




                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sSQLID = sSQLID + " WHERE " + sWhere;
                    }
                    else
                    {
                        sSQLID = sSQLID + sWhere + " AND " + sWhereTab;
                    }

                    rsID = rsIDAdo.fc_OpenRecordSet(sSQLID);

                    if (rsID.Rows.Count == 0)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'y a aucune visite à supprimer sur le PDA, " + "\n" + "veuillez vérifier si cette visite a bien été envoyé et/ou contrôler" + " le statut PDA des visites à supprimer si celui-ci permet la suppression.", "Suppression annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        rsID.Dispose();

                        rsID = null;

                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                        return;
                    }

                    bok = fc_CtrlData(ref rsID, ref sErreur, bModeSuppession);
                }
                else
                {

                    bok = fc_CtrlData(ref rsSend, ref sErreur, bModeSuppession);
                }

                if (bok == false)
                {
                    //===> Mondir le 21.09.2020, Load even after error (While clicking on Annuler or No)
                    fc_LoadInterP2();
                    //===> Fin Modif Mondir
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    txtErreur.Text = sErreur;
                    return;
                }

                if (bModeSuppession == true)
                {
                    //=== SI + 1, active la version v2 des ramoanges (PDA).

                    if (General.sPDAv2Ramonage == "1")//tESTED
                    {
                        ModPDA.fc_SendInterventionPDA(rsIDAdo, bModeSuppession, ModPDA.TypeInterPdaPreventif, DateTime.Now);
                    }
                    else
                    {
                        // ModPDA.fc_SendRamonagePDA(ref rsID, ref bModeSuppession);TODO DANS MODPDA
                    }

                    // rsID.Close
                    // Set rsID = Nothing
                }
                else
                {
                    //=== SI + 1, active la version v2 des ramoanges (PDA).
                    if (General.sPDAv2Ramonage == "1")//TESTED
                    {
                        //=== focntion en prod
                        ModPDA.fc_SendInterventionPDA(rsSendAdo, bModeSuppession, ModPDA.TypeInterPdaPreventif, DateTime.Now);
                    }
                    else
                    {
                        //=== fonction qui n'est plus utilisé
                        //fc_SendRamonagePDA rsSend, bModeSuppession
                    }
                }
                fc_LoadInterP2();


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_sendCreate");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="rs"></param>
        /// <param name="sErreur"></param>
        /// <param name="bModeSuppression"></param>
        /// <returns></returns>
        public bool fc_CtrlData(ref DataTable rs, ref string sErreur, bool bModeSuppression)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_CtrlData() - Enter In The Function - sErreur = {sErreur} - bModeSuppression = {bModeSuppression}");
            //===> Fin Modif Mondir
            bool functionReturnValue = false;
            string sSQL = null;
            int i = 0;
            string sSatutPDA = null;
            string sSQLID = null;
            //=== retourne true si aucune erreur decelés.

            try
            {
                //rs.MoveFirst();
                sErreur = "";
                functionReturnValue = true;

                i = 0;


                var rsFilter = rs.Select(" Selection = 1");

                foreach (DataRow rsFilterRow in rsFilter)
                {

                    //=== compte le nombre d'intervention à envoyer.

                    if (Convert.ToInt32(General.nz(rsFilterRow["Selection"], 0)) == 1)
                    {
                        i = i + 1;
                    }
                    else
                    {
                        continue;
                    }

                    //=== controle la présence de l'intervenant.

                    if (string.IsNullOrEmpty(General.nz(rsFilterRow["Intervenant"], "").ToString()))
                    {
                        sErreur = sErreur + "Ligne " + rsFilterRow["NoLigne"] + " => il manque l'intervenant." + "\n";
                        functionReturnValue = false;
                    }
                    else
                    {
                        // sSQL = fc_ADOlibelle("SELECT NoPDA FROM Personnel WHERE Matricule='" & gFr_DoublerQuote(rs!Intervenant) & "'")
                        //If sSQL = "" Then
                        //        '=== intervenant sans numéro de PDA.
                        //        sErreur = sErreur & "Ligne " & rs!NoLigne & " => Le matricule " & rs!Intervenant & " n'a pas de numéro de PDA," _
                        //'            & " veuillez-vous rendre sur la fiche personnel afin de renseigner ce champ." & vbCrLf
                        //        fc_CtrlData = False
                        //
                        //ElseIf Not IsNumeric(sSQL) Then
                        //        '=== intervenant avec numéro de PDA erroné (non numérique).
                        //        sErreur = sErreur & "Ligne " & rs!NoLigne & " => Le matricule " & rs!Intervenant & " a un numéro de PDA Incorrect," _
                        //'            & " veuillez-vous rendre sur la fiche personnel afin de modifier son numéro." & vbCrLf
                        //        fc_CtrlData = False
                        //Else
                        //    rs!ID_PDA = CLng(sSQL)
                        //    rs.Update

                        //End If

                    }

                    //=== controle le code barre existe bien.
                    //If nz(rs!PDAB_IDBarre, "") = "" Then
                    //    sSQLID = "SELECT     PDAB_IDBarre From PDAB_BadgePDA WHERE PDAB_Noauto = " & rs!PDAB_Noauto
                    //    sSQLID = fc_ADOlibelle(sSQLID)
                    //    If sSQLID = "" Then
                    //        sErreur = sErreur & "Ligne " & rs!NoLigne & " => il manque le code barre pour la localisation " & rs!RAMI_LibelleLoc & " de l'immeuble " & rs!CodeImmeuble & "." & vbCrLf
                    //        fc_CtrlData = False
                    //    End If
                    //End If


                    if (bModeSuppression == false)
                    {

                        //=== controle le satut PDA, si celui ci est déja envoyé.
                        sSatutPDA = General.UCase(General.nz(rsFilterRow["CodeEtatPDA"], ""));

                        if (sSatutPDA == General.UCase(ModPDA.cEnvoye) || sSatutPDA == General.UCase(ModPDA.cREcu))
                        {
                            sErreur = sErreur + "Ligne " + rsFilterRow["NoLigne"] + " => Cette intervention a déjà été transmise sur un PDA,"
                                + " veuillez d'abord la supprimer sur le PDA en question afin de pouvoir la renvoyer à nouveau." + "\n";
                            functionReturnValue = false;

                        }
                        else if (sSatutPDA == ModPDA.cDebut.ToLower())
                        {
                            sErreur = sErreur + "Ligne " + rsFilterRow["NoLigne"] + " => Cette intervention a déjà été démarré sur un PDA,"
                                + " veuillez d'abord la supprimer sur le PDA en question afin de pouvoir la renvoyer à nouveau." + "\n";
                            functionReturnValue = false;
                        }

                        //===> Mondir le 14.10.2020, controle is l'intervention est terminé 04 ===> https://groupe-dt.mantishub.io/view.php?id=2026
                        var CodeEtat = rsFilterRow["CodeEtat"]?.ToString();
                        if (!string.IsNullOrEmpty(CodeEtat))
                        {
                            if (CodeEtat.ToLower() == "04")
                            {
                                sErreur = sErreur + "Ligne " + rsFilterRow["NoLigne"] + " => Cette intervention est déjà réalisée (04)." + "\n";
                                functionReturnValue = false;
                            }
                        }
                        //===> Fin Modif Mondir


                        if (string.IsNullOrEmpty(General.nz(rsFilterRow["longitude"], "").ToString())
                            || string.IsNullOrEmpty(General.nz(rsFilterRow["latitude"], "").ToString()))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'immeuble " + rsFilterRow["CodeImmeuble"]
                                + " n'est pas géo-localisé, vous ne pouvez pas transmettre cette intervention sur un PDA,"
                                + "\n" + " veuillez-vous rendre sur la fiche immeuble pour géo-localiser  cet immeuble.", "Intervention non transmise", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            functionReturnValue = false;

                        }

                    }


                    // rs.MoveNext();
                }

                // rs.Filter = ADODB.FilterGroupEnum.adFilterNone;

                //sErreur = sErreur & "Ligne " & rs!RAMI_Noligne & " =>" & vbCrLf
                if (i == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas sélectionné d'intervention à envoyer.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = false;
                }
                else if (i > 0 && string.IsNullOrEmpty(sErreur) && bModeSuppression == false)
                {
                    //===> Mondir le 21.09.2020, reported by Xavier, when click on Annuler it continue ===> == DialogResult.No to != DialogResult.Yes
                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous allez transmettre " + i + " intervention(s).", "Transmission des intervention", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez cliqué sur non, l'opération d'envoie des interventions est annulée.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        functionReturnValue = false;
                    }
                }
                else if (i > 0 && string.IsNullOrEmpty(sErreur) && bModeSuppression == true)//TESTED
                {
                    //===> Mondir le 21.09.2020, reported by Xavier, when click on Annuler it continue ===> == DialogResult.No to != DialogResult.Yes
                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous allez supprimer " + i + " intervention(s).", "Transmission des intervention", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question) != DialogResult.Yes)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez cliqué sur non, l'opération d'envoie des interventions est annulée.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        functionReturnValue = false;
                    }
                }
                else if (!string.IsNullOrEmpty(sErreur))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'envoie est annulé, des anomalies ont été détectées," + "\n" + "veuillez consulter la liste des anomalies présente sous la grille", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";fc_CtrlData");
                return functionReturnValue;
            }

        }

        private void ChangerIntervenant(string sInterv)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ChangerIntervenant() - Enter In The Function - sInterv = {sInterv}");
            //===> Fin Modif Mondir
            var Error = "";
            foreach (DataRow r in rsVisite.Rows)
            {
                //===> Mondir le 14.10.2020, check code etat, on peut pas changer l'intervenant si code etat != 00 to fix https://groupe-dt.mantishub.io/view.php?id=2026
                if (Convert.ToInt32(General.nz(r["Selection"], 0)) == 1)
                {
                    if (r["CodeEtat"]?.ToString() != "00")
                    {
                        Error +=
                            $"Ligne {r["NoLigne"]} => Vous ne pouvez pas changer l'intervenant des interventions en {r["CodeEtat"]}\n";
                    }
                }
            }

            if (!string.IsNullOrEmpty(Error))
            {
                CustomMessageBox.Show("L'envoie est annulé, des anomalies ont été détectées," + "\n" + "veuillez consulter la liste des anomalies présente sous la grille", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtErreur.Text = Error;
                return;
            }

            foreach (DataRow r in rsVisite.Rows)
            {
                if (Convert.ToInt32(General.nz(r["Selection"], 0)) == 1)
                {


                    r["Intervenant"] = sInterv;
                    using (var tmp = new ModAdo())
                    {
                        string nom = tmp.fc_ADOlibelle("SELECT  NOM " + " From Personnel"
                             + " WHERE  Matricule ='" + StdSQLchaine.gFr_DoublerQuote(sInterv) + "'");
                        string prenom = tmp.fc_ADOlibelle("SELECT  PRENOM " + " From Personnel"
                            + " WHERE  Matricule ='" + StdSQLchaine.gFr_DoublerQuote(sInterv) + "'");
                        r["Nom"] = nom + prenom;
                        General.Execute($"UPDATE Intervention  SET  Intervention.Intervenant = '{sInterv}'" +
                                          $" WHERE  nointervention={r["nointervention"]}");

                    }
                }
                else if (Convert.ToInt32(General.nz(r["Selection"], 0)) != 0)
                {
                    ///'''''''''''''''''''''''  Stop
                }
                // rsVisite.MoveNext();
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAplliquerIntervenat_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAplliquerIntervenat_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            string sInterv = null;
            UltraGridRow book = null;
            int lRowNumber = 0;

            try
            {
                string req = "SELECT Personnel.Matricule as \"Matricule\"," + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\","
                     + " Qualification.Qualification as \"Qualification\"," + " Personnel.NoPDA as \"NoPDA\" "
                     + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                string where = " (Personnel.NonActif is null or Personnel.NonActif = 0)";

                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche intervenant" };

                fg.ugResultat.DoubleClickRow += (se, ev) =>
                { // charge les enregistrements du technicien.
                    sInterv = fg.ugResultat.ActiveRow.Cells["Matricule"].Value.ToString();
                    if (rsVisite.Rows.Count == 0)
                    {
                        return;
                    }

                    ssGridVisite.UpdateData();

                    book = ssGridVisite.Rows[0];

                    if (ssGridVisite.ActiveRow != null)
                        lRowNumber = ssGridVisite.ActiveRow.Index;

                    //===> Mondir le 14.10.2020, look at ChangerIntervenant() diff
                    ChangerIntervenant(sInterv);
                    //===> Fin Modif Mondir

                    ssGridVisite.DataSource = rsVisite;

                    if (lRowNumber >= 0)
                    {
                        ssGridVisite.Rows[lRowNumber].Activated = true;
                    }

                    //===> Mondir le 06.11.2020, added to fix https://groupe-dt.mantishub.io/view.php?id=2046
                    ssGridVisite.UpdateData();


                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        // charge les enregistrements du technicien.
                        sInterv = fg.ugResultat.ActiveRow.Cells["Matricule"].Value.ToString();
                        if (rsVisite.Rows.Count == 0)
                        {
                            return;
                        }

                        ssGridVisite.UpdateData();

                        book = ssGridVisite.Rows[0];

                        if (ssGridVisite.ActiveRow != null)
                            lRowNumber = ssGridVisite.ActiveRow.Index;

                        //===> Mondir le 14.10.2020, look at ChangerIntervenant() diff
                        ChangerIntervenant(sInterv);
                        //===> Fin Modif Mondir

                        ssGridVisite.DataSource = rsVisite;

                        if (lRowNumber >= 0)
                        {
                            ssGridVisite.Rows[lRowNumber].Activated = true;
                        }

                        //===> Mondir le 06.11.2020, added to fix https://groupe-dt.mantishub.io/view.php?id=2046
                        ssGridVisite.UpdateData();

                        fg.Dispose(); fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();


                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAplliquerIntervenat_Click");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCleanSimul_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdCleanSimul_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            fc_InitSimul();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_InitSimul()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_InitSimul() - Enter In The Function");
            //===> Fin Modif Mondir
            ((dtDateDebPlaningBisSimul.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            ((dtDateFinPlaningBisSimul.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            //dtDateDebPlaningBisSimul.Value = System.DBNull.Value;
            //dtDateFinPlaningBisSimul.Value = System.DBNull.Value;
            txtCOP_NoContratSimul.Text = "";
            txtCOP_AvenantSimul.Text = "";
            txtCodeImmeubleSimul.Text = "";

            txtIntervenantSimul.Text = "";
            lbllibIntervenantSimul.Text = "";
            txtCAI_CodeSimul.Text = "";
            txtPrestationSimul.Text = "";
            chkPEI_APlanifier.CheckState = System.Windows.Forms.CheckState.Unchecked;
            txtCOP_NoautoSimul.Text = "";

            //chkEQU_P1CompteurSimul.value = 0
            optEQU_P1CompteurSimul0.Checked = false;
            optEQU_P1CompteurSimul1.Checked = false;
            optEQU_P1CompteurSimul2.Checked = true;

        }
        //private bool fc_erreurConsult() fct dans un tabs invisbile
        //{
        //    bool functionReturnValue = false;
        //    try
        //    {
        //        functionReturnValue = false;

        //        if (!General.IsDate(dtDateDebPlaningConsult.Text))
        //        {
        //            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date de debut de planification invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //            dtDateDebPlaningConsult.Focus();
        //            functionReturnValue = true;
        //            return functionReturnValue;
        //        }

        //        if (!General.IsDate(dtDateFinPlaningConsul.Text))
        //        {
        //            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date de fin de planification invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //            dtDateFinPlaningConsul.Focus();
        //            functionReturnValue = true;
        //            return functionReturnValue;
        //        }
        //        return functionReturnValue;
        //    }
        //    catch (Exception ex)
        //    {
        //        Erreurs.gFr_debug(ex, this.Name + ";fc_erreurConsult");
        //        functionReturnValue = true;
        //        return functionReturnValue;
        //    }

        //}


        /// <summary>
        /// tested
        /// </summary>
        /// <returns></returns>
        private bool fc_erreurSimul()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_erreurSimul() - Enter In The Function");
            //===> Fin Modif Mondir
            bool functionReturnValue = false;

            try
            {
                functionReturnValue = false;

                if (((dtDateDebPlaningBisSimul.ButtonsLeft[0]) as StateEditorButton).Checked == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date de debut de planification invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtDateDebPlaning.Focus();
                    functionReturnValue = true;
                    return functionReturnValue;
                }

                if (((dtDateFinPlaningBisSimul.ButtonsLeft[0]) as StateEditorButton).Checked == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date de fin de planification invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    dtDateFinPlaning.Focus();
                    functionReturnValue = true;
                    return functionReturnValue;
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_erreurSimul");
                functionReturnValue = true;
                return functionReturnValue;
            }


        }
        /// <summary>
        /// tETSED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCleanVisites_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdCleanVisites_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            fc_initPlan();
        }
        /// <summary>
        /// Tested mais txtCOP_Avenant.Text is not found in vb6
        /// </summary>
        private void fc_initPlan()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_initPlan() - Enter In The Function");
            //===> Fin Modif Mondir
            txtMoisPlanif.Text = "";
            dtDateDebPlaning.Text = "";
            dtDateFinPlaning.Text = "";
            txtIntervSecteur.Text = "";
            lblLibIntervSecteur.Text = "";
            txtIntervenant.Text = "";
            lbllibIntervenant.Text = "";
            txtCodeImmeuble.Text = "";
            txtCOP_NoAuto.Text = "";
            txtCOP_NoContrat.Text = "";
            //txtCOP_Avenant.Text = "";control not found in vb
            cmbStatutPDA.Text = "";
            //chkEQU_P1Compteur.value = 0
            OptEQU_P1Compteur0.Checked = false;
            OptEQU_P1Compteur1.Checked = true;
            OptEQU_P1Compteur2.Checked = false;

        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_InitHisto()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_InitHisto() - Enter In The Function");
            //===> Fin Modif Mondir
            ((DTDatePrevueDe.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            DTDatePrevueDe.Value = DTDatePrevueDe.NullText;

            ((dtDateVisiteLe.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            dtDateVisiteLe.Value = dtDateVisiteLe.NullText;

            ((dtDateVisiteAu.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            dtDateVisiteAu.Value = dtDateVisiteAu.NullText;

            ((DTDatePrevueAu.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            DTDatePrevueAu.Value = DTDatePrevueAu.NullText;

            ((DTDateRealiseDe.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            DTDateRealiseDe.Value = DTDateRealiseDe.NullText;

            ((DTDateRealiseAu.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            DTDateRealiseAu.Value = DTDateRealiseAu.NullText;

            txtCodeImmeubleHisto.Text = "";
            txtMatEntretien.Text = "";
            lblLibEntretien.Text = "";
            txtIntervenantHisto.Text = "";
            lbllibIntervenantHisto.Text = "";
            txtCode1Histo.Text = "";
            cmbStatutPDAhisto.Text = "";

            ((DTCreeParDe.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            DTCreeParDe.Value = DTCreeParDe.NullText;

            ((dtCreeParAu.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            dtCreeParAu.Value = dtCreeParAu.NullText;

            ((dtEnvoyeLeHisto1.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            dtEnvoyeLeHisto1.Value = dtEnvoyeLeHisto1.NullText;

            ((dtEnvoyeLeHisto2.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            dtEnvoyeLeHisto2.Value = dtEnvoyeLeHisto2.NullText;
            //cmBUo.Text = "";  TODO control not found in vb

            txtStatutHisto.Text = "";
            txtNoInterventionHisto.Text = "";
            chkInterIncompléte.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkCommGardien.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkAnomalie.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkTrxUrgent.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkMatRemplace.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkDevisEtablir.CheckState = System.Windows.Forms.CheckState.Unchecked;

            OptEQU_P1CompteurHisto0.Checked = false;
            OptEQU_P1CompteurHisto1.Checked = false;
            OptEQU_P1CompteurHisto2.Checked = true;

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdClient_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdClient_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            string sCode = null;

            try
            {

                string req = "SELECT " + "Code1 as \"Nom Directeur\", Nom as \"Raison sociale\", Ville as \"Ville\" FROM Table1 ";

                string where = "";

                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche client" };
                fg.SetValues(new Dictionary<string, string> { { "Code1", txtCode1Histo.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    // charge les enregistrements de l'immeuble

                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtCode1Histo.Text = sCode;
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        // charge les enregistrements de l'immeuble

                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtCode1Histo.Text = sCode;
                        fg.Dispose(); fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command1_Click");
                return;
            }

        }
        /// <summary>
        ///     Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCocher_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdCocher_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            ssGridVisite.UpdateData();

            try
            {
                if (rsVisite == null)
                {
                    return;
                }

                // rsVisite.MoveFirst();

                if (rsVisite.Rows.Count == 0)
                {
                    return;
                }


                foreach (DataRow r in rsVisite.Rows)
                {
                    r["Selection"] = 1;
                    //  rsVisiteAdo.Update();
                    // rsVisite.MoveNext();
                }

                ssGridVisite.DataSource = rsVisite;

                //===> Mondir le 21.09.2020, pour corrigé https://groupe-dt.mantishub.io/view.php?id=1945
                foreach (DataRow row in rsVisite?.Rows)
                {
                    var xx = General.Execute($"UPDATE Intervention SET Selection= '1'" +
                                     $" WHERE  nointervention={row["nointervention"]}");
                }
                //===> Fin Modif Mondir

                //===> Mondir le 06.11.2020, added to fix https://groupe-dt.mantishub.io/view.php?id=2046
                ssGridVisite.UpdateData();

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdCocher_Click");
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDecocher_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdDecocher_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            ssGridVisite.UpdateData();

            try
            {
                if (rsVisite == null)
                {
                    return;
                }

                // rsVisite.MoveFirst();

                if (rsVisite.Rows.Count == 0)
                {
                    return;
                }


                foreach (DataRow r in rsVisite.Rows)
                {
                    r["Selection"] = 0;
                    //rsVisiteAdo.Update();
                    // rsVisite.MoveNext();
                }

                ssGridVisite.DataSource = rsVisite;
                ssGridVisite.UpdateData();

                //===> Mondir le 21.09.2020, pour corrigé https://groupe-dt.mantishub.io/view.php?id=1945
                foreach (DataRow row in rsVisite?.Rows)
                {
                    var xx = General.Execute($"UPDATE Intervention SET Selection= '0'" +
                                             $" WHERE  nointervention={row["nointervention"]}");
                }
                //===> Fin Modif Mondir
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmddecocher_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdEditer_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"CmdEditer_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            fc_exportExcel(rsVisiteSimul, rsVisiteSimulAdo.SDArsAdo, true);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExportHisto_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdExportHisto_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Fonction en cours de Développement", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            return;

            ssGridHisto.UpdateData();
            fc_exportExcel(rsHisto, rsHistoAdo.SDArsAdo, false);
        }


        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sWhere"></param>
        private void fc_ExportDetail(string sWhere = "")
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ExportDetail() - Enter In The Function - sWhere = {sWhere}");
            //===> Fin Modif Mondir
            try
            {
                string sSQL = null;

                DataTable rsExport = default(DataTable);
                ModAdo rsExportAdo = new ModAdo();

                Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
                Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
                Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
                Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
                Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);

                int i = 0;
                int lLigne = 0;
                int j = 0;

                // ADODB.Field oField = default(ADODB.Field);


                //sSQL = "SELECT     InterventionP2Simul.NoIntervention, OperationP2Simul.CodeImmeuble, OperationP2Simul.EQM_Code AS prestation, OperationP2Simul.DatePrevue" _
                //& " FROM         InterventionP2Simul INNER JOIN " _
                //& " OperationP2Simul ON InterventionP2Simul.NoIntervention = OperationP2Simul.NoInterventionVisite"

                sSQL = "SELECT InterventionP2Simul.NoIntervention, OperationP2Simul.CodeImmeuble, Immeuble.Adresse, Immeuble.CodePostal, Immeuble.Ville, "
                    //===> Mondir le 18.12.2020 ajout des colonnes  : PDAB_Libelle AS Localisation, GAI_LIbelle AS Equipements, demande faite par https://groupe-dt.mantishub.io/view.php?id=2132
                    + " OperationP2Simul.CAI_Libelle as Motif   , OperationP2Simul.EQM_Code AS prestation, OperationP2Simul.DatePrevue, OperationP2Simul.PDAB_Libelle AS Localisation, OperationP2Simul.GAI_LIbelle AS Equipements "
                    //===> Fin Modif Mondir
                    + " FROM         InterventionP2Simul INNER JOIN "
                    + " OperationP2Simul ON InterventionP2Simul.NoIntervention = OperationP2Simul.NoInterventionVisite INNER JOIN "
                    + " Immeuble ON InterventionP2Simul.CodeImmeuble = Immeuble.CodeImmeuble";

                sWhere = sWhere.Replace("utilisateur", "InterventionP2Simul.utilisateur");
                sWhere = sWhere.Replace("intervenant", "InterventionP2Simul.intervenant");
                sWhere = sWhere.Replace("datePrevue", "OperationP2Simul.datePrevue");
                sSQL = sSQL + " " + sWhere;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                rsExport = rsExportAdo.fc_OpenRecordSet(sSQL);

                if (rsExport.Rows.Count == 0)
                {
                    rsExport.Dispose();
                    rsExport = null;
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    return;
                }

                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;

                var _with3 = rsExport;

                //=== Titre du champs.
                //oSheet.Cells(1, 1).value = "STATISTQUE DE L'INTERVENANT " & fc_ADOlibelle("SELECT Prenom, nom FROM personnel WHERE matricule ='" & cmbIntervenant & "'", True) & " du " & txtPeriodeDu & " au " & txtPeriodeAu
                //oSheet.Cells(1, 1).Font.Bold = True
                //=== affiche les noms des champs.
                i = 1;
                foreach (DataColumn oField in rsExport.Columns)
                {
                    oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Cells[1, i].value = oField.ColumnName;
                    i = i + 1;
                }

                lLigne = 3;

                //_with3.MoveFirst();

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                foreach (DataRow rsExportRows in rsExport.Rows)
                {
                    j = 1;
                    //  oSheet.Cells(lLigne, 1).value = sANT_MatCode
                    foreach (DataColumn rsExportCol in rsExport.Columns)
                    {

                        oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                        j = j + 1;
                    }
                    lLigne = lLigne + 1;
                    //_with3.MoveNext();
                }
                //Set oResizeRange = oSheet.Range("A" & 3, "B" & j).Resize(ColumnSize:=I - 1)

                oResizeRange = oSheet.Range["A1", "D" + lLigne + 3];
                ///oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
                //formate la ligne en gras.

                //=== Ajuste la taille des colonnes.
                oResizeRange.EntireColumn.AutoFit();
                oResizeRange = oSheet.Range["A1", "D1"].Resize[ColumnSize: i - 1];

                oResizeRange.Interior.ColorIndex = 15;

                //oSheet.ListObjects.Add(xlSrcRange, oResizeRange, , xlNo).Name = "Tableau3"
                //oSheet.ListObjects("Tableau3").TableStyle = "TableStyleMedium12"

                oXL.Visible = true;
                oXL.UserControl = true;

                oRng = null;
                oSheet = null;
                oWB = null;
                oXL = null;

                rsExport.Dispose();
                rsExport = null;


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_ExportDetail");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="SDArs"></param>
        /// <param name="bsimul"></param>
        private void fc_exportExcel(DataTable rs, SqlDataAdapter SDArs, bool bsimul)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_exportExcel() - Enter In The Function - bsimul = {bsimul}");
            //===> Fin Modif Mondir
            try
            {
                Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
                Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
                Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
                Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
                Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);

                //ADODB.Field oField = default(ADODB.Field);

                int i = 0;
                int j = 0;
                int lPos = 0;

                int lLigne = 0;

                string sSQL = null;
                string sMatricule = null;
                string sWhere = null;
                string sWhereBIS = null;

                DataTable rsExport = default(DataTable);
                //DataTable rs = new DataTable();
                bool bok = false;

                if (rs == null)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement en cours.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }


                if (OptPrestation.Checked == true)//tested
                {
                    lPos = SDArs.SelectCommand.CommandText.IndexOf("WHERE");
                    sWhereBIS = General.Mid(SDArs.SelectCommand.CommandText, lPos, General.Len(SDArs.SelectCommand.CommandText) - lPos);

                    fc_ExportDetail(sWhereBIS);

                    return;
                }


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                // Start Excel and get Application object.
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                // Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;

                rsExport = rs;

                // rsExport.MoveFirst();

                var _with4 = rsExport;

                if (_with4.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour ces critères.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                //== affiche les noms des champs.

                i = 1;
                foreach (DataColumn oField in rsExport.Columns)
                {

                    oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    bok = false;

                    if (General.UCase("NoInter") == oField.ColumnName.ToUpper() || General.UCase("Nointervention") == oField.ColumnName.ToUpper())
                    {
                        if (bsimul == true && General.UCase(oField.ColumnName) == General.UCase("nointervention"))
                        {

                            oSheet.Cells[1, i].value = "No intervention";
                        }
                        else if (bsimul == false && General.UCase(oField.ColumnName) == General.UCase("NoInter"))
                        {

                            oSheet.Cells[1, i].value = "No intervention";

                        }

                        bok = true;
                    }
                    else if (General.UCase("CodeEtatPDA") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Statut PDA";
                        bok = true;
                    }
                    else if (General.UCase("Codeimmeuble") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Codeimmeuble";
                        bok = true;
                    }
                    else if (General.UCase("Adresse") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Adresse";
                        bok = true;
                    }
                    else if (General.UCase("CodePostal") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "CodePostal";
                        bok = true;
                    }
                    else if (General.UCase("Ville") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Ville";
                        bok = true;
                    }
                    else if (General.UCase("Localisation") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Localisation";
                        bok = true;
                    }

                    else if (General.UCase("CodeEtat") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Statut Intervention";
                        bok = true;
                    }
                    else if (General.UCase("Intervenant") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Intervenant";
                        bok = true;
                    }
                    else if (General.UCase("Nom") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Nom";
                        bok = true;
                    }
                    else if (General.UCase("DatePrevue") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Date prévue";
                        bok = true;
                    }
                    else if (General.UCase("InfoRV") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Info ramonage";
                        bok = true;
                    }
                    else if (General.UCase("DateRV") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Date de rendez-vous";
                        bok = true;
                    }
                    else if (General.UCase("HeureRV") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Heure rendez-vous";
                        bok = true;
                    }
                    else if (General.UCase("DateRealise") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Date réalisée";
                        bok = true;
                    }
                    else if (General.UCase("COP_NoContrat") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "No contrat";
                        bok = true;
                    }

                    else if (General.UCase("CreeLe") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Crée le";
                        bok = true;
                    }
                    else if (General.UCase("ID_PDA") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "N°PDA";
                        bok = true;
                    }
                    else if (General.UCase("secteur") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Secteur";
                        bok = true;
                    }
                    else if (General.UCase("NomSecteur") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Nom secteur";
                        bok = true;
                    }
                    else if (General.UCase("CompteurObli") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Compteur Obligatoire";
                        bok = true;
                    }

                    //=== modif du 12 02 2014, ajout des champs
                    else if (General.UCase("PriseRV") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Prise de rendez vous";
                        bok = true;
                    }

                    else if (General.UCase("HeureDebut") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Heure début";
                        bok = true;
                    }

                    else if (General.UCase("HeureFin") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Heure fin";
                        bok = true;
                    }
                    else if (General.UCase("CreePar") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Crée Par";
                        bok = true;
                    }

                    else if (General.UCase("CommRV") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Commentaire sur rendez-vous";
                        bok = true;
                    }

                    else if (General.UCase("Anomalie") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Anomalie";
                        bok = true;
                    }

                    else if (General.UCase("TrxUrgent") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Trx urgent";
                        bok = true;
                    }

                    else if (General.UCase("CommTxUrgent") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Commentaire sur travaux urgent";
                        bok = true;
                    }

                    else if (General.UCase("DevisEtablir") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Devis à établir";
                        bok = true;
                    }
                    else if (General.UCase("CommDevis") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Commentaire sur devis";
                        bok = true;
                    }

                    else if (General.UCase("MatRemplace") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Matériel remplacé";
                        bok = true;
                    }

                    else if (General.UCase("CommMatRemplace") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Commentaire sur matériel remplacé";
                        bok = true;
                    }
                    else if (General.UCase("NomSignataire") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Nom signataire";
                        bok = true;
                    }

                    else if (General.UCase("CommSignataire") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Commentaire sur signataire";
                        bok = true;
                    }

                    //                          Case "InfoGardienTRaitee"
                    //                                  oSheet.Cells(1, i).value = "Commantaire sur signataire"
                    //                                  bok = True
                    else if (General.UCase("MotifSan") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Motif scan";
                        bok = true;
                    }

                    else if (General.UCase("CommMotifScan") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Commentaire sur scan";
                        bok = true;
                    }

                    else if (General.UCase("GPS_Longi") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "GPS longi";
                        bok = true;
                    }

                    else if (General.UCase("GPS_Latt") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "GPS Latt";
                        bok = true;
                    }

                    else if (General.UCase("ModifierLe") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Modifié le";
                        bok = true;
                    }

                    else if (General.UCase("ModifierPar") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Modifié par";
                        bok = true;
                    }
                    else if (General.UCase("EnvoyeLe") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Envoyé le";
                        bok = true;
                    }

                    else if (General.UCase("EnvoyePar") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Envoyé par";
                        bok = true;
                    }
                    else if (General.UCase("DureeP") == oField.ColumnName.ToUpper())
                    {

                        oSheet.Cells[1, i].value = "Durée";
                        bok = true;
                    }

                    //oSheet.Cells(1, i).value = oField.Name
                    if (bok == true)
                    {
                        i = i + 1;
                    }
                }



                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                // Starting at E1, fill headers for the number of columns selected.

                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Interior.ColorIndex = 15;
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                lLigne = 3;


                //==== affiche les données.
                // _with4.MoveFirst();

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                foreach (DataRow rsExportRows in rsExport.Rows)
                {
                    j = 1;
                    //  oSheet.Cells(lLigne, 1).value = sANT_MatCode
                    foreach (DataColumn rsExportCol in rsExport.Columns)
                    {
                        bok = false;


                        if (General.UCase("NoInter") == rsExportCol.ColumnName.ToUpper() || General.UCase("Nointervention") == rsExportCol.ColumnName.ToUpper())
                        {

                            if (bsimul == true && General.UCase(rsExportCol.ColumnName) == General.UCase("nointervention"))
                            {

                                oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            }
                            else if (bsimul == false && General.UCase(rsExportCol.ColumnName) == General.UCase("NoInter"))
                            {

                                oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];

                            }

                            //oSheet.Cells(lLigne, j + 0).value = oField.value
                            bok = true;
                        }
                        else if (General.UCase("CodeEtatPDA") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;

                        }


                        else if (General.UCase("Codeimmeuble") == rsExportCol.ColumnName.ToUpper())
                        {
                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("Adresse") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;

                        }
                        else if (General.UCase("CodePostal") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;

                        }
                        else if (General.UCase("Ville") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("Localisation") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("CodeEtat") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("Intervenant") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("Nom") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        //===> Mondir le 18.12.2020, pour corriger ce ticket : https://groupe-dt.mantishub.io/view.php?id=2122
                        //else if (General.UCase("DatePrevue") == rsExportCol.ColumnName.ToUpper())
                        //{

                        //    oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName].ToString();
                        //    bok = true;
                        //}
                        else if (General.UCase("DateVisite") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName].IsDate() ? rsExportRows[rsExportCol.ColumnName].ToDate().ToString("dd/MM/yyyy") : "";
                            bok = true;
                        }
                        //===> Fin Modif Mondir
                        else if (General.UCase("InfoRV") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("DateRV") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("HeureRV") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("DateRealise") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("COP_NoContrat") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        else if (General.UCase("CreeLe") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("ID_PDA") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        else if (General.UCase("secteur") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("NomSecteur") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("CompteurObli") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = (Convert.ToInt32(rsExportRows[rsExportCol.ColumnName]) == 1 ? "Oui" : "");
                            bok = true;
                        }

                        //=== modif du 12 02 2014, ajout des champs
                        else if (General.UCase("PriseRV") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = (Convert.ToInt32(rsExportRows[rsExportCol.ColumnName]) == 1 ? "Oui" : "");
                            bok = true;
                        }

                        else if (General.UCase("HeureDebut") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("HeureFin") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        else if (General.UCase("CreePar") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        else if (General.UCase("CommRV") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        else if (General.UCase("Anomalie") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        else if (General.UCase("TrxUrgent") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].valuee = (Convert.ToInt32(rsExportRows[rsExportCol.ColumnName]) == 1 ? "Oui" : "");
                            bok = true;
                        }

                        else if (General.UCase("CommTxUrgent") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        else if (General.UCase("DevisEtablir") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = (Convert.ToInt32(rsExportRows[rsExportCol.ColumnName]) == 1 ? "Oui" : "");
                            bok = true;
                        }

                        else if (General.UCase("CommDevis") == rsExportCol.ColumnName.ToUpper())
                        {
                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        else if (General.UCase("MatRemplace") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = (Convert.ToInt32(rsExportRows[rsExportCol.ColumnName]) == 1 ? "Oui" : "");
                            bok = true;
                        }

                        else if (General.UCase("CommMatRemplace") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("NomSignataire") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        else if (General.UCase("CommSignataire") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        //                          Case "InfoGardienTRaitee"
                        //                                oSheet.Cells(lLigne, j + 0).value = oField.value
                        //                                bok = True
                        else if (General.UCase("MotifSan") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("CommMotifScan") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("GPS_Longi") == rsExportCol.ColumnName)
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        else if (General.UCase("GPS_Latt") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        else if (General.UCase("ModifierLe") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        else if (General.UCase("ModifierPar") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("EnvoyeLe") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }

                        else if (General.UCase("EnvoyePar") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        else if (General.UCase("DureeP") == rsExportCol.ColumnName.ToUpper())
                        {

                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            bok = true;
                        }
                        if (bok == true)
                        {
                            j = j + 1;
                        }
                    }
                    lLigne = lLigne + 1;

                }

                //_with4.MoveNext();

                ///oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oXL.Visible = true;
                oXL.UserControl = true;

                oRng = null;

                oSheet = null;

                oWB = null;

                oXL = null;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                //  _with4.Dispose();

                // rsExport = null;


                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_ExportExcel");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExportPlan_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdExportPlan_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            ssGridVisite.UpdateData();
            ssGridTachesAnnuel.UpdateData();
            fc_exportExcel(rsVisite, rsVisiteAdo.SDArsAdo, false);
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFindGMAO_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdFindGMAO_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                string sCode = null;


                string req = "SELECT  codeimmeuble as \"Immeuble\", COP_NoAuto as \"No FICHE GAMAO\" ,"
                    + "  COP_Statut as \"Statut\" " + " FROM  COP_ContratP2 ";
                string where = "";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche Fiche GMAO" };
                fg.SetValues(new Dictionary<string, string> { { "codeimmeuble", txtCodeImmeuble.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtCOP_NoautoSimul.Text = fg.ugResultat.ActiveRow.Cells["No FICHE GAMAO"].Value.ToString();
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCOP_NoautoSimul.Text = fg.ugResultat.ActiveRow.Cells["No FICHE GAMAO"].Value.ToString();
                        fg.Dispose(); fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();




                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdRechercher_Click");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFindGmaoCreat_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdFindGmaoCreat_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                string sCode = null;


                string req = "SELECT  codeimmeuble as \"Immeuble\", COP_NoAuto as \"No FICHE GAMAO\" ,"
                    + "  COP_Statut as \"Statut\" " + " FROM  COP_ContratP2 ";
                string where = "";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche Fiche GMAO" };
                fg.SetValues(new Dictionary<string, string> { { "codeimmeuble", txtCodeImmeuble.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    //===> Mondir le 11.02.2021 changer txtCOP_NoAutoSimul par txtCOP_NoAuto
                    txtCOP_NoAuto.Text = fg.ugResultat.ActiveRow.Cells["No FICHE GAMAO"].Value.ToString();
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        //===> Mondir le 11.02.2021 changer txtCOP_NoAutoSimul par txtCOP_NoAuto
                        txtCOP_NoAuto.Text = fg.ugResultat.ActiveRow.Cells["No FICHE GAMAO"].Value.ToString();
                        fg.Dispose(); fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();




                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdFindGmaoCreat_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFindStraitantSimul_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdFindStraitantSimul_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                string sCode = null;


                string req = "SELECT Personnel.Matricule as \"Matricule\","
                    + " Personnel.Nom as \"Nom\","
                    + " Personnel.prenom as \"Prenom\","
                    + " Qualification.Qualification as \"Qualification\","
                    + " Personnel.NumRadio as \"Kobby\" "
                    + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                string where = " (Personnel.NonActif is null or Personnel.NonActif = 0)";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'un intervenant" };
                fg.SetValues(new Dictionary<string, string> { { "codeimmeuble", txtCodeImmeuble.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtIntervenantSimul.Text = sCode;
                    lbllibIntervenantSimul.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " "
                                                 + fg.ugResultat.ActiveRow.Cells[3].Value.ToString();

                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtIntervenantSimul.Text = sCode;
                        lbllibIntervenantSimul.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " "
                                                     + fg.ugResultat.ActiveRow.Cells[3].Value.ToString();

                        fg.Dispose(); fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();




                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdFindGmaoCreat_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadHisto()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_LoadHisto() - Enter In The Function");
            //===> Fin Modif Mondir
            string sSQL = null;
            string sWhere = null;
            UltraGridRow book = null;
            int lRowNumber = 0;
            int lCompteurP1 = 0;

            try
            {


                // sSQL = "SELECT   Intervention.NumFicheStandard ,Intervention.NoIntervention,  Intervention.NoLigne, Intervention.CodeEtatPDA, " _
                //& "  Intervention.CodeImmeuble, Intervention.PDAB_Noauto, Immeuble.Adresse, Immeuble.CodePostal, Immeuble.Ville, " _
                //& " Intervention.PDAB_Libelle AS Localisation, Intervention.CodeEtat, Intervention.Intervenant, Personnel.Nom, Personnel.Prenom, " _
                //& " Intervention.DatePrevue,Intervention.CompteurObli, Intervention.PriseRV, Intervention.InfoRV, Intervention.DateRV, Intervention.HeureRV, " _
                //& " Intervention.DateRealise, Intervention.HeureDebut, Intervention.HeureFin, Intervention.Cop_NoAuto, " _
                //& " Intervention.COP_NoContrat, Intervention.CreeLe, Intervention.CreePar, Intervention.ID_PDA, Intervention.PDAB_IDBarre, " _
                //& "   Intervention.CommRV , " _
                //& " Intervention.PDAB_NoautoScanne, Intervention.Anomalie, Intervention.TrxUrgent, Intervention.CommTxUrgent, " _
                //& " Intervention.DevisEtablir, Intervention.CommDevis, Intervention.MatRemplace, Intervention.CommMatRemplace, " _
                //& " Intervention.NomSignataire, Intervention.CommSignataire,  " _
                //& " Intervention.CommentairePDA, Intervention.GPS_Longi, Intervention.GPS_Latt, Intervention.ModifierLe, " _
                //& " Intervention.ModifierPar , Intervention.EnvoyeLe, Intervention.EnvoyePar " _
                //& "FROM         Intervention INNER JOIN" _
                //& " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble " _
                //& " LEFT OUTER JOIN " _
                //& " Personnel ON Intervention.Intervenant = Personnel.Matricule  " _
                //& " "
                sSQL = "SELECT Intervention.NumFicheStandard ,Intervention.NoIntervention,  Intervention.NoLigne,Intervention.CodeEtat,TypeCodeEtat.LibelleCodeEtat,"
                        + "  " + "  Intervention.CodeImmeuble, Intervention.PDAB_Noauto, Immeuble.Adresse, Immeuble.CodePostal, Immeuble.Ville, "
                        + " Intervention.PDAB_Libelle AS Localisation,  Intervention.Intervenant, Personnel.Nom, Personnel.Prenom, "
                        + " Intervention.DatePrevue, Intervention.DateVisite,Intervention.CompteurObli,  "
                        + " Intervention.DateRealise, Intervention.HeureDebut, Intervention.HeureFin, Intervention.Cop_NoAuto, "
                        + " Intervention.COP_NoContrat,Intervention.CodeEtatPDA, Intervention.CreeLe, Intervention.CreePar, Intervention.ID_PDA, Intervention.PDAB_IDBarre, "
                        + "   " + " Intervention.PDAB_NoautoScanne, Intervention.Anomalie,  "
                        + " Intervention.DevisEtablir,  " + " Intervention.NomSignataire,   "
                        + " Intervention.CommentairePDA, Intervention.GPS_Longi, Intervention.GPS_Latt, Intervention.ModifierLe, "
                        + " Intervention.ModifierPar , Intervention.EnvoyeLe, Intervention.EnvoyePar ";
                //+ "FROM         Intervention INNER JOIN"
                //+ " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble " + " LEFT OUTER JOIN "
                //+ " Personnel ON Intervention.Intervenant = Personnel.Matricule  " + " LEFT OUTER JOIN TypeCodeEtat "
                //+ "  ON Intervention.CodeEtat = TypeCodeEtat.CodeEtat ";

                if (General.sGMAOcriterePartImmeuble == "1")
                {
                    if (Opt1.Checked || Opt2.Checked)
                    {
                        //'=== copro/particulier ou immeuble
                        sSQL = sSQL + " FROM         Intervention INNER JOIN"
                                    + " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble LEFT OUTER JOIN "
                                    + " Gestionnaire ON Immeuble.CodeGestionnaire = Gestionnaire.CodeGestinnaire AND Immeuble.Code1 = Gestionnaire.Code1 LEFT OUTER JOIN "
                                    + " Personnel ON Intervention.Intervenant = Personnel.Matricule LEFT OUTER JOIN "
                                    + " TypeCodeEtat ON Intervention.CodeEtat = TypeCodeEtat.CodeEtat ";
                    }
                    else
                    {
                        sSQL = sSQL + "FROM         Intervention INNER JOIN"
                                    + " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble "
                                    + " LEFT OUTER JOIN "
                                    + " Personnel ON Intervention.Intervenant = Personnel.Matricule  "
                                    + " LEFT OUTER JOIN TypeCodeEtat "
                                    + "  ON Intervention.CodeEtat = TypeCodeEtat.CodeEtat ";
                    }
                }
                else
                {
                    sSQL = sSQL + "FROM         Intervention INNER JOIN"
                                + " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble "
                                + " LEFT OUTER JOIN "
                                + " Personnel ON Intervention.Intervenant = Personnel.Matricule  "
                                + " LEFT OUTER JOIN TypeCodeEtat "
                                + "  ON Intervention.CodeEtat = TypeCodeEtat.CodeEtat ";
                }

                sWhere = " WHERE VisitePDA = 1 ";

                if (chkInterIncompléte.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.NoIntervention IN (SELECT     NoInterventionVisite" + " From OperationP2"
                                + " WHERE     (Realise IS NULL OR" + " Realise = 0)  )";
                    }
                    else
                    {
                        sWhere = sWhere + " AND RAMI_RamonageInterv.RAMI_Noauto IN (SELECT     NoInterventionVisite " + " From OperationP2"
                                + " WHERE     (Realise IS NULL OR" + " Realise = 0))";
                    }
                }

                if (General.sGMAOcriterePartImmeuble == "1")
                {
                    if (Opt2.Checked)
                    {
                        //'=== copro/particulier
                        sWhere = sWhere + " and CodeQualif_Qua ='" + General.cPart + "'";
                    }
                    else if (Opt1.Checked)
                    {
                        //'=== immeuble.
                        sWhere = sWhere + " and (CodeQualif_Qua <>'" + General.cPart + "' OR CodeQualif_Qua is null)";
                    }
                }
                if (!string.IsNullOrEmpty(txtMatEntretien.Text))
                {
                    //    If sWhere = "" Then
                    //        sWhere = " WHERE RAMI_RamonageInterv.Codeimmeuble iN( SELECT     Codeimmeuble_IMM" _
                    //'                    & " From IntervenantImm_INM" _
                    //'                    & " WHERE  CATI_code = '" & cIntervEntretien & "'" _
                    //'                    & " AND intervenant_INM = '" & gFr_DoublerQuote(txtMatEntretien) & "')"
                    //    Else
                    //        sWhere = " WHERE RAMI_RamonageInterv.Codeimmeuble iN( SELECT     Codeimmeuble_IMM" _
                    //'                    & " From IntervenantImm_INM" _
                    //'                    & " WHERE  CATI_code = '" & cIntervEntretien & "'" _
                    //'                    & " AND intervenant_INM = '" & gFr_DoublerQuote(txtMatEntretien) & "')"
                    //    End If
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE Intervention.Secteur ='" + StdSQLchaine.gFr_DoublerQuote(txtMatEntretien.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND Intervention.Secteur ='" + StdSQLchaine.gFr_DoublerQuote(txtMatEntretien.Text) + "'";
                    }
                }

                if (((DTDatePrevueDe.ButtonsLeft[0]) as StateEditorButton).Checked && !string.IsNullOrEmpty(DTDatePrevueDe.Value.ToString()))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.DatePrevue >='" + DTDatePrevueDe.Value + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.DatePrevue >='" + DTDatePrevueDe.Value + "'";
                    }
                }

                if (((dtDateVisiteLe.ButtonsLeft[0]) as StateEditorButton).Checked && !string.IsNullOrEmpty(dtDateVisiteLe.Value.ToString()))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.DateVisite >='" + dtDateVisiteLe.Value + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.DateVisite >='" + dtDateVisiteLe.Value + "'";
                    }
                }

                if (((dtDateVisiteAu.ButtonsLeft[0]) as StateEditorButton).Checked && !string.IsNullOrEmpty(dtDateVisiteAu.Value.ToString()))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.DateVisite <='" + dtDateVisiteAu.Value + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.DateVisite <='" + dtDateVisiteAu.Value + "'";
                    }
                }


                if (((DTDatePrevueAu.ButtonsLeft[0]) as StateEditorButton).Checked && !string.IsNullOrEmpty(DTDatePrevueAu.Value.ToString()))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.DatePrevue <='" + DTDatePrevueAu.Value + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.DatePrevue <='" + DTDatePrevueAu.Value + "'";
                    }
                }

                if (((DTDateRealiseDe.ButtonsLeft[0]) as StateEditorButton).Checked && !string.IsNullOrEmpty(DTDateRealiseDe.Value.ToString()))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.DateRealise >='" + DTDateRealiseDe.Value + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.DateRealise >='" + DTDateRealiseDe.Value + "'";
                    }
                }


                if (((DTDateRealiseAu.ButtonsLeft[0]) as StateEditorButton).Checked && !string.IsNullOrEmpty(DTDateRealiseAu.Value.ToString()))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.DateRealise <='" + DTDateRealiseAu.Value + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.DateRealise <='" + DTDateRealiseAu.Value + "'";
                    }
                }

                if (((DTCreeParDe.ButtonsLeft[0]) as StateEditorButton).Checked && !string.IsNullOrEmpty(DTCreeParDe.Value.ToString()))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.CreeLe >='" + DTCreeParDe.Value + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.CreeLe >='" + DTCreeParDe.Value + "'";
                    }
                }


                if (((dtCreeParAu.ButtonsLeft[0]) as StateEditorButton).Checked && !string.IsNullOrEmpty(dtCreeParAu.Value.ToString()))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.CreeLe <='" + dtCreeParAu.Value + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.CreeLe <='" + dtCreeParAu.Value + "'";
                    }
                }

                if (((dtEnvoyeLeHisto1.ButtonsLeft[0]) as StateEditorButton).Checked && !string.IsNullOrEmpty(dtEnvoyeLeHisto1.Value.ToString()))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.EnvoyeLe >='" + dtEnvoyeLeHisto1.Value + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.EnvoyeLe >='" + dtEnvoyeLeHisto1.Value + "'";
                    }
                }


                if (((dtEnvoyeLeHisto2.ButtonsLeft[0]) as StateEditorButton).Checked && !string.IsNullOrEmpty(dtEnvoyeLeHisto2.Value.ToString()))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.EnvoyeLe <='" + dtEnvoyeLeHisto2.Value + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.EnvoyeLe <='" + dtEnvoyeLeHisto2.Value + "'";
                    }
                }



                if (chkTrxUrgent.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.TrxUrgent = 1";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.TrxUrgent = 1";
                    }
                }

                if (chkMatRemplace.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.MatRemplace = 1";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.MatRemplace = 1";
                    }
                }

                if (chkDevisEtablir.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.DevisEtablir = 1";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.DevisEtablir = 1";
                    }
                }

                if (chkAnomalie.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.Anomalie <> ''";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.Anomalie <> ''";
                    }
                }

                if (chkCommGardien.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE     ((CommSignataire IS NOT NULL) AND (CommSignataire <> '')) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  ((CommSignataire IS NOT NULL) AND (CommSignataire <> ''))";
                    }
                }


                if (!string.IsNullOrEmpty(txtCodeImmeubleHisto.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE  Intervention.Codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeubleHisto.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND Intervention.Codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeubleHisto.Text) + "'";
                    }
                }


                //If txtMatEntretien <> "" Then
                //
                //    If sWhere = "" Then
                //        sWhere = " WHERE  Intervention.Secteur ='" & gFr_DoublerQuote(txtMatEntretien) & "'"
                //    Else
                //        sWhere = sWhere & " AND Intervention.Secteur ='" & gFr_DoublerQuote(txtMatEntretien) & "'"
                //    End If
                //End If
                //
                //
                //
                //If txtMatEntretien <> "" Then
                //
                //    If sWhere = "" Then
                //        sWhere = " WHERE  Intervention.Secteur ='" & gFr_DoublerQuote(txtMatEntretien) & "'"
                //    Else
                //        sWhere = sWhere & " AND Intervention.Secteur ='" & gFr_DoublerQuote(txtMatEntretien) & "'"
                //    End If
                //End If

                if (!string.IsNullOrEmpty(txtIntervenantHisto.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE  Intervention.Intervenant ='" + StdSQLchaine.gFr_DoublerQuote(txtIntervenantHisto.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND Intervention.Intervenant ='" + StdSQLchaine.gFr_DoublerQuote(txtIntervenantHisto.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtCode1Histo.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE immeuble.code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1Histo.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND immeuble.code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1Histo.Text) + "'";
                    }

                }

                //If txtCode1Histo <> "" Then
                //     If sWhere = "" Then
                //        sWhere = " WHERE immeuble.code1='" & gFr_DoublerQuote(txtCode1Histo) & "'"
                //    Else
                //        sWhere = sWhere & " AND immeuble.code1='" & gFr_DoublerQuote(txtCode1Histo) & "'"
                //    End If
                //
                //End If

                if (!string.IsNullOrEmpty(cmbStatutPDAhisto.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE  Intervention.CodeEtatPDA ='" + StdSQLchaine.gFr_DoublerQuote(cmbStatutPDAhisto.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND Intervention.CodeEtatPDA ='" + StdSQLchaine.gFr_DoublerQuote(cmbStatutPDAhisto.Text) + "'";
                    }
                }

                /*cmBUo.Text control not found in vb
                 * if (!string.IsNullOrEmpty(cmBUo.Text))
                 {
                     if (string.IsNullOrEmpty(sWhere))
                     {

                         sWhere = "WHERE immeuble.uo_code='" + StdSQLchaine.gFr_DoublerQuote(cmBUo.Text) + "'";
                     }
                     else
                     {

                         sWhere = sWhere + " AND immeuble.uo_code='" + StdSQLchaine.gFr_DoublerQuote(cmBUo.Text) + "'";
                     }

                 }*/



                if (!string.IsNullOrEmpty(txtStatutHisto.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE  Intervention.CodeEtat ='" + StdSQLchaine.gFr_DoublerQuote(txtStatutHisto.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND Intervention.CodeEtat ='" + StdSQLchaine.gFr_DoublerQuote(txtStatutHisto.Text) + "'";
                    }

                }


                if (!string.IsNullOrEmpty(txtNoInterventionHisto.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE  Intervention.NoIntervention ='" + StdSQLchaine.gFr_DoublerQuote(txtNoInterventionHisto.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND Intervention.NoIntervention ='" + StdSQLchaine.gFr_DoublerQuote(txtNoInterventionHisto.Text) + "'";
                    }

                }


                if (OptEQU_P1CompteurHisto0.Checked == true)
                {
                    lCompteurP1 = 1;
                }
                else if (OptEQU_P1CompteurHisto1.Checked == true)
                {
                    lCompteurP1 = 2;
                }
                else if (OptEQU_P1CompteurHisto2.Checked == true)
                {
                    lCompteurP1 = 3;
                }

                if (string.IsNullOrEmpty(General.sFIltreInterP1))
                {
                    lCompteurP1 = 3;
                }

                if (lCompteurP1 == 1)
                {
                    //=== uniquement avec cpt P1
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.NoIntervention IN (SELECT     NoInterventionVisite" + " From OperationP2"
                                + " WHERE     EQU_P1Compteur = 1 )";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  Intervention.NoIntervention IN (SELECT     NoInterventionVisite" + " From OperationP2"
                                + " WHERE     EQU_P1Compteur = 1 )";
                    }
                }
                else if (lCompteurP1 == 2)
                {
                    //=== exclure avec cpt P1
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  Intervention.NoIntervention IN (SELECT     NoInterventionVisite" + " From OperationP2"
                                + " WHERE     (EQU_P1Compteur is null OR EQU_P1Compteur = 0) )";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  Intervention.NoIntervention IN (SELECT     NoInterventionVisite" + " From OperationP2"
                                + " WHERE     (EQU_P1Compteur is null OR EQU_P1Compteur = 0) )";

                    }

                }
                else if (lCompteurP1 == 3)
                {
                    //=== aucun filtre, prends toutes les prestations avec ou sans compteur P1
                }


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                if (string.IsNullOrEmpty(sOrderByHisto))
                {
                    sOrderByHisto = " order by Intervention.DatePrevue, Intervention.Codeimmeuble";
                }
                else
                {

                }

                rsHisto = rsHistoAdo.fc_OpenRecordSet(sSQL + sWhere + sOrderByHisto);
                //Set rsRAMIhisto = fc_OpenRecordSet(sSQL & sWhere)



                ssGridHisto.DataSource = rsHisto;
                ssGridHisto.UpdateData();

                if (ssGridHisto.Rows.Count > 0)
                {
                    book = ssGridHisto.Rows[0];
                    ssGridHisto.Rows[lRowNumber].Activated = true;
                }

                else if (ssGridHisto.ActiveRow != null && lRowNumber >= 0)
                {

                    ssGridHisto.ActiveRow = book;
                    lRowNumber = ssGridHisto.ActiveRow.Index;
                    ssGridHisto.Rows[lRowNumber].Activated = true;
                }

                lblTotalHisto.Text = Convert.ToString(rsHisto.Rows.Count);

                if (ssGridHisto.ActiveRow != null)
                {
                    fc_ssGRIDTachesAnnuelHisto(Convert.ToInt32(General.nz((ssGridHisto.ActiveRow.Cells["NoIntervention"].Value), 0)));
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                FC_SaveParam();

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_loadHisto");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lNoIntervention"></param>
        private void fc_ssGRIDTachesAnnuelHisto(int lNoIntervention)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ssGRIDTachesAnnuelHisto() - Enter In The Function - lNoIntervention = {lNoIntervention}");
            //===> Fin Modif Mondir
            string sSQL = null;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
                if (optOperationHisto.Checked == true)//tested
                {

                    ssGridTachesAnnuelHisto.Visible = true;
                    ssOpeTachesHisto.Visible = false;

                    //sSQL = "SELECT     NoIntervention, CodeImmeuble,   " _
                    //& " CAI_Libelle, EQM_code, TPP_Code, Intervenant, NoSemaine, DatePrevue, " _
                    //& " DateRealise, INT_Realisee, Deplacement, Duree, HeureDebut, HeureFin, INT_J," _
                    //& " INT_M, INT_Annee, PEI_Cout, PEI_ForceVisite, SEM_Code," _
                    //& " COP_NoContrat, Commentaire " _
                    //& " FROM         OperationP2" _
                    //& " WHERE NoInterventionVisite =" & lNointervention _
                    //& " order by dateprevue , TPP_Code, CodeImmeuble, PDAB_Libelle"

                    sSQL = "SELECT     CodeImmeuble, PDAB_Libelle AS Localisation, NoInterventionVisite,INT_Realisee,  GAI_LIbelle AS Equipements, "
                        + " CAI_Libelle AS Composant, EQM_Code AS Operation," + " EQM_Libelle, "
                        + " CTR_Libelle, MOY_Libelle, ANO_Libelle, OPE_Libelle,"
                        + " TPP_Code AS Periodicite, NoSemaine, Duree "
                        + " From OperationP2 " + " WHERE NumFicheStandard = " + lNoIntervention
                        + " ORDER BY Localisation, Equipements, Composant, Operation";

                    rsOpP2Histo = rsOpP2HistoAdo.fc_OpenRecordSet(sSQL);
                    ssGridTachesAnnuelHisto.DataSource = rsOpP2Histo;
                    ssGridTachesAnnuelHisto.UpdateData();
                }
                else if (optTachesHisto.Checked == true)//tested
                {

                    ssGridTachesAnnuelHisto.Visible = false;
                    ssOpeTachesHisto.Visible = true;

                    sSQL = "SELECT     OperationP2.DatePrevue, OperationP2.EQM_code, " + " TIP2_TachesInterP2.TIP2_Designation1, "
                        + " TIP2_TachesInterP2.TIP2_NoLIgne, TIP2_TachesInterP2.Realise, " + " TIP2_TachesInterP2.DateRealise,"
                        + " OperationP2.NoIntervention , OperationP2.NoInterventionVisite" + " FROM OperationP2 INNER JOIN"
                        + " TIP2_TachesInterP2 "
                        + " ON OperationP2.NoIntervention = TIP2_TachesInterP2.NoInterventionOp  WHERE NoInterventionVisite =" + lNoIntervention
                        + " ORDER BY OperationP2.DatePrevue, OperationP2.EQM_code, TIP2_TachesInterP2.TIP2_NoLIgne";


                    rsOpTachesHisto = rsOpTachesHistoAdo.fc_OpenRecordSet(sSQL);
                    ssOpeTachesHisto.DataSource = rsOpTachesHisto;
                    ssOpeTachesHisto.UpdateData();
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_ssGRIDTachesAnnuelHisto;");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdHistoRamonage_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdHistoRamonage_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            fc_LoadHisto();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdImmeubleHisto_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdImmeubleHisto_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                string sCode = null;


                string req = "SELECT " + "CodeImmeuble as \"Code immeuble\", " + " Adresse as \"adresse\", Ville as \"Ville\" "
                    + " , anglerue as \"angle de rue\" " + " FROM Immeuble";
                string where = "";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'immeuble" };
                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeubleHisto.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString(); ;
                    txtCodeImmeubleHisto.Text = sCode;

                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString(); ;
                        txtCodeImmeubleHisto.Text = sCode;

                        fg.Dispose(); fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();




                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdImmeubleHisto_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdinitHistorique_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdinitHistorique_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            fc_InitHisto();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdintervenant_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdintervenant_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                string sCode = null;


                string req = "SELECT Personnel.Matricule as \"Matricule \"," + " Personnel.Nom as \"Nom\","
                    + " Personnel.prenom as \"Prenom\"," + " Qualification.Qualification as \"Qualification\","
                    + " Personnel.NumRadio as \"Kobby\" "
                    + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                string where = "(Personnel.NonActif is null or Personnel.NonActif = 0)";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'immeuble" };
                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeubleHisto.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {

                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtIntervenant.Text = sCode;
                    lbllibIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtIntervenant.Text = sCode;
                        lbllibIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                        fg.Dispose(); fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();




                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdintervenant_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdIntervHisto_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdIntervHisto_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                string sCode = null;


                string req = "SELECT Personnel.Matricule as \"Matricule \"," + " Personnel.Nom as \"Nom\","
                    + " Personnel.prenom as \"Prenom\"," + " Qualification.Qualification as \"Qualification\","
                    + " Personnel.NumRadio as \"Kobby\" "
                    + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                string where = "(Personnel.NonActif is null or Personnel.NonActif = 0)";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'immeuble" };
                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeubleHisto.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {

                    txtIntervenantHisto.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    lbllibIntervenantHisto.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtIntervenantHisto.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        lbllibIntervenantHisto.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                        fg.Dispose(); fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();




                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdIntervHisto_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdLegende0_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"CmdLegende0_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            var cmd = sender as Button;
            int index = Convert.ToInt32(cmd.Tag);
            FC_SaveParam();
        }
        /// <summary>
        /// Tested mais  la fct  devModeCrystal.fc_ExportCrystal ne creer pas le fichier pdf afin de l'exporter
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdListing_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdListing_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            DataTable rsInfo = default(DataTable);

            string sInfoStat = null;
            string sINfoPrest = null;
            string sInfoDevis = null;
            string sCaption = null;
            string sSelectionFormula = null;
            string sReturn = null;

            System.DateTime dtNow = default(System.DateTime);
            try
            {

                dtNow = DateTime.Now;
                cmdListing.Enabled = false;


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                fc_LoadInterP2();//tsted


                // rsInfo = ModAdo.fc_OpenRecordSet(rsVisite.Source);
                rsInfo = rsVisite;
                sCaption = cmdListing.Text;

                cmdListing.Text = "Création du listing en cours";


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                foreach (DataRow rsInfoRow in rsInfo.Rows)
                {
                    Debug.Print(General.nz(rsInfoRow["NoIntervention"], 0).ToString());

                    ModPDA.fc_infostat(Convert.ToInt32(General.nz(rsInfoRow["NoIntervention"], 0).ToString()),
                                                 General.nz(rsInfoRow["Intervenant"], "").ToString(),
                                                 General.nz(rsInfoRow["CodeImmeuble"], "").ToString(), ref sInfoStat, ref sINfoPrest, ref sInfoDevis,
                                                 Convert.ToDateTime(dtDateDebPlaning.Text), Convert.ToDateTime(dtDateFinPlaning.Text), Convert.ToString(true), dtNow);
                    // rsInfo.MoveNext();

                }

                //rsInfo.Dispose();
                //rsInfo = null;

                sSelectionFormula = "{Intervention.DateSaisie}>=date(" + Convert.ToDateTime(dtDateDebPlaning.Text).ToString("yyyy,MM,dd") + ")"
                    + " and {Intervention.DateSaisie}<=date(" + Convert.ToDateTime(dtDateFinPlaning.Text).ToString("yyyy,MM,dd") + ")";

                if (!string.IsNullOrEmpty(txtIntervenant.Text))
                {
                    sSelectionFormula = sSelectionFormula + " and {Intervention.Intervenant}='" + txtIntervenant.Text + "'";
                }
                sSelectionFormula = sSelectionFormula + " and {Intervention.VisitePDA}= 1 ";



                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];

                ModCrystalPDF.tabFormulas[0].sNom = "";

                sReturn = devModeCrystal.fc_ExportCrystal(sSelectionFormula, General.gsRpt + General.CHEMINETATPRESTv2, "", "", "", "", System.Windows.Forms.CheckState.Checked);
                //sReturn = fc_ExportCrystalSansFormule(strRequete, CHEMINEUROONLY)
                if (!string.IsNullOrEmpty(sReturn))
                {

                    ModuleAPI.Ouvrir(sReturn);

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                }

                cmdListing.Text = "Listing";

                cmdListing.Enabled = true;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }
            catch (Exception ex)
            {
                cmdListing.Text = "Listing";
                Erreurs.gFr_debug(ex, this.Name + ";cmdListing_Click");
                cmdListing.Enabled = true;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMatEntretien_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdMatEntretien_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                string sCode = null;


                string req = "SELECT Personnel.Matricule as \"Matricule \"," + " Personnel.Nom as \"Nom\","
                    + " Personnel.prenom as \"Prenom\"," + " Qualification.Qualification as \"Qualification\","
                    + " Personnel.NumRadio as \"Kobby\" "
                    + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                string where = "(Personnel.NonActif is null or Personnel.NonActif = 0)";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'immeuble" };
                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeubleHisto.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {

                    txtMatEntretien.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    lblLibEntretien.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtMatEntretien.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        lblLibEntretien.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                        fg.Dispose(); fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();



                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "; cmdMatEntretien_Click");
            }
        }

        //visible=false
        private void cmdPrestationSimul_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdPrestationSimul_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            string sSQL = null;

            if (string.IsNullOrEmpty(txtCAI_CodeSimul.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez d'abord sélectionner un motif.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            sSQL = " SELECT EQU_Code as \"Prestation\" " + " FROM  CAI_CategoriInterv INNER JOIN" + " EQU_EquipementP2 "
                + " ON CAI_CategoriInterv.CAI_Noauto = EQU_EquipementP2.CAI_Noauto" + "  ";


            string where = " CAI_CategoriInterv.CAI_Code ='" + txtCAI_CodeSimul.Text + "'";
            SearchTemplate fg = new SearchTemplate(this, null, sSQL, where, "Order By EQU_Code") { Text = "Recherche d'immeuble" };

            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {

                txtPrestationSimul.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                fg.Dispose(); fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtPrestationSimul.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                    fg.Dispose(); fg.Close();
                }
            };

            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();

            return;
        }
        //visible=false
        private void cmdMotifSimul_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdMotifSimul_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            string sSQL = null;
            string sCode = null;
            sSQL = "SELECT CAI_Code as \"Code Motif\",  CAI_Libelle as \"Libelle\" " + " FROM CAI_CategoriInterv";
            string where = "";
            SearchTemplate fg = new SearchTemplate(this, null, sSQL, where, "") { Text = "Recherche d'immeuble" };

            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {

                txtCAI_CodeSimul.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                fg.Dispose(); fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {
                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    txtCAI_CodeSimul.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fg.Dispose(); fg.Close();
                }
            };

            fg.StartPosition = FormStartPosition.CenterScreen;
            fg.ShowDialog();




            return;
        }
        //visible=false
        private void cmdRapportPDA_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRapportPDA_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            fc_RapportPDA();

            fc_LoadHisto();

        }
        private void fc_RapportPDA()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_RapportPDA() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                var _with21 = ssGridHisto;
                frmPDAv2P2 frm = new frmPDAv2P2();
                frm.txtNoInter.Text = _with21.ActiveRow.Cells["NoInter"].Text;
                frm.ShowDialog();
                frm.Close();


                return;
            }
            catch (Exception ex) { Erreurs.gFr_debug(ex, this.Name + ";fc_RapportPDA"); }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRechercheImmeuble_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                string sCode = null;


                string req = "SELECT " + "CodeImmeuble as \"Ref Immeuble\", " + " Adresse as \"adresse\", Ville as \"Ville\" "
                    + " , anglerue as \"angle de rue\" " + " FROM Immeuble";
                string where = "";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche des immeuble" };
                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeuble.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtCodeImmeuble.Text = sCode;

                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString(); ;
                        txtCodeImmeuble.Text = sCode;

                        fg.Dispose(); fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();




                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheImmeuble_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeubleSimul_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRechercheImmeubleSimul_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                string sCode = null;


                string req = "SELECT " + "CodeImmeuble as \"Ref Immeuble\", " + " Adresse as \"adresse\", Ville as \"Ville\" "
                    + " , anglerue as \"angle de rue\" " + " FROM Immeuble";
                string where = "";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche des immeuble" };
                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeubleSimul.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtCodeImmeubleSimul.Text = sCode;

                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString(); ;
                        txtCodeImmeubleSimul.Text = sCode;

                        fg.Dispose(); fg.Close();
                    }
                };

                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();




                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheImmeubleSimul_Click");
            }
        }
        /// <summary>
        /// tETSED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"CmdSauver_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            ssGridVisite.UpdateData();
            ssGridTachesAnnuel.UpdateData();
            ssOpeTaches.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSimulation_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdSimulation_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            System.DateTime dtDateDeb = default(System.DateTime);
            System.DateTime dtDateFin = default(System.DateTime);
            System.DateTime dtTemp = default(System.DateTime);
            int lCopNoAuto = 0;
            int loptEQU_P1CompteurSimul = 0;
            string sMessageErr = null;
            bool bNePasPlan = false;

            if (fc_erreurSimul() == true)//Tested
            {
                return;
            }

            //===> Mondir le 09.02.2021, https://groupe-dt.mantishub.io/view.php?id=2276
            //===> Mondir le 11.02.2021, ajout de cette condition pour corriger https://groupe-dt.mantishub.io/view.php?id=2285
            if (txtCOP_NoautoSimul.Text != "")
            {
                using (var tmpModAdo = new ModAdo())
                {
                    string req = "SELECT COP_Statut FROM  COP_ContratP2 " +
                                 $"WHERE COP_NoAuto = '{txtCOP_NoautoSimul.Text}'";
                    req = tmpModAdo.fc_ADOlibelle(req);
                    if (req.ToUpper() != "A")
                    {
                        CustomMessageBox.Show("Seul le statut A est autorisé pour la simulation", "Simulation annulée",
                            MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
            }
            //===> Fin Modif Mondir

            //=== recherche de l'id du contrat.
            if (!string.IsNullOrEmpty(txtCOP_NoContratSimul.Text))
            {
                //    If txtCOP_AvenantSimul = "" Then
                //            txtCOP_AvenantSimul = "0"
                //
                //    ElseIf Not IsNumeric(txtCOP_AvenantSimul) Then
                //            txtCOP_AvenantSimul = "0"
                //
                //    End If
                //
                //     sSQL = "SELECT     COP_NoAuto " _
                //'         & " From COP_ContratP2" _
                //'         & " WHERE COP_NoContrat = '" & gFr_DoublerQuote(txtCOP_NoContratSimul) & "'" _
                //'         & " AND COP_Avenant =" & txtCOP_AvenantSimul & ""
                //
                //     sSQL = fc_ADOlibelle(sSQL)
                //
                //     lCopNoAuto = nz(sSQL, 0)
                //
                //     If lCopNoAuto = 0 Then
                //
                //         MsgBox "Ce numéro de contrat n'existe pas.", vbInformation, "Opération annulée"
                //         Exit Sub
                //     End If

            }



            lCopNoAuto = Convert.ToInt32(General.nz(txtCOP_NoautoSimul.Text, 0));

            //=== alerte l'utilisateur qu'il va planifier sur une trop longue période

            dtDateDeb = Convert.ToDateTime(dtDateDebPlaningBisSimul.Value);

            dtDateFin = Convert.ToDateTime(dtDateFinPlaningBisSimul.Value);

            dtTemp = dtDateDeb.AddMonths(1).AddDays(-1);

            //=== modif du 24 10 2014, ajout d'une variable globale contenant le cumul des heures.
            ModP2v2.dtTotDureeP2 = 0;

            if (dtDateFin > dtTemp)
            {
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention vous allez planifier une période dépassant un mois," + "\n" + " Voulez vous continuer ? ", "Avertissement", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Information) == DialogResult.No)
                {
                    return;
                }

            }

            if (chkPEI_APlanifier.CheckState == CheckState.Checked)
            {
                bNePasPlan = true;
            }
            else
            {
                bNePasPlan = false;
            }

            if (optEQU_P1CompteurSimul0.Checked == true)
            {
                loptEQU_P1CompteurSimul = 1;
            }
            else if (optEQU_P1CompteurSimul1.Checked == true)
            {
                loptEQU_P1CompteurSimul = 2;
            }
            else if (optEQU_P1CompteurSimul2.Checked == true)
            {
                loptEQU_P1CompteurSimul = 3;
            }

            if (string.IsNullOrEmpty(General.sFIltreInterP1))
            {
                loptEQU_P1CompteurSimul = 3;
            }
            //sMessageErr = fc_SimulationCreeCalendrierV2(dtDateDebPlaningBisSimul, lCopNoAuto, _
            //dtDateFinPlaningBisSimul, True, txtIntervenantSimul, txtCodeImmeubleSimul, txtCAI_CodeSimul _
            //, txtPrestationSimul, , bNePasPlan, nz(chkEQU_P1CompteurSimul.value, 0))

            sMessageErr = ModP2v2.fc_SimulationCreeCalendrierV2(Convert.ToDateTime(dtDateDebPlaningBisSimul.Value), lCopNoAuto, Convert.ToDateTime(dtDateFinPlaningBisSimul.Value), true,
                txtIntervenantSimul.Text, txtCodeImmeubleSimul.Text, txtCAI_CodeSimul.Text, txtPrestationSimul.Text, "", bNePasPlan, loptEQU_P1CompteurSimul);

            cmdAfficheSimul_Click(cmdAfficheSimul, new System.EventArgs());

            txtTotduree.Text = Convert.ToString(ModP2v2.dtTotDureeP2);

            FC_SaveParam();

            if (!string.IsNullOrEmpty(sMessageErr))
            {
                frmErreurP2 frm = new frmErreurP2();
                frm.txtErreur.Text = sMessageErr;
                frm.ShowDialog();
                frm.Close();
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="dDedut"></param>
        /// <param name="dFin"></param>
        /// <param name="lCop_NoAuto"></param>
        /// <param name="sIntervenant"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sCodeImmeubleAu"></param>
        /// <param name="sCai_Code"></param>
        /// <param name="sEQM_Code"></param>
        /// <returns></returns>
        private bool fc_LoadIntervP2Simul(System.DateTime dDedut, System.DateTime dFin, int lCop_NoAuto,
            string sIntervenant, string sCodeImmeuble, string sCodeImmeubleAu = "", string sCai_Code = "", string sEQM_Code = "")
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_LoadIntervP2Simul() - Enter In The Function - dDedut = {dDedut} - dFin = {dFin} - lCop_NoAuto = {lCop_NoAuto} " +
                                        $"- sIntervenant = {sIntervenant} - sCodeImmeuble = {sCodeImmeuble} - sCodeImmeubleAu = {sCodeImmeubleAu} - sCai_Code = {sCai_Code} - sEQM_Code = {sEQM_Code}");
            //===> Fin Modif Mondir
            bool functionReturnValue = false;

            string sWhere = null;

            try
            {
                //===  Rafraichis la tables des listes de taches fusionnées
                //===  devenant ainsi des interventions.


                General.sSQL = "SELECT     NoIntervention ,InterventionP2Simul.CodeImmeuble,immeuble.Adresse, immeuble.CodePostal, immeuble.Ville, PDAB_Libelle,  Article,"
                    + " Intervenant, Personnel.Nom, Designation,  DatePrevue,DateVisite, DureeP, "
                    + " Cop_NoAuto,INT_realise, dateRealise, HeureDebut, HeureFin ,heureDebutP, heureFinP,CompteurObli,NoSemaine"
                    + " FROM InterventionP2Simul " + " LEFT OUTER JOIN Immeuble ON " + " InterventionP2Simul.CodeImmeuble = Immeuble.CodeImmeuble  "
                    + " LEFT OUTER JOIN Personnel ON " + " InterventionP2Simul.Intervenant = Personnel.Matricule " + " WHERE " + " InterventionP2Simul.DateVisite>='"
                    + dDedut + "'" + " AND InterventionP2Simul.DateVisite <='" + dFin + "'" + " AND utilisateur ='" + General.fncUserName() + "'";

                //& " InterventionP2Simul.DateVisite>='" & dDedut & "'" _
                //& " AND InterventionP2Simul.DateVisite <='" & dFin & "'" _
                //& " AND utilisateur ='" & fncUserName & "'"


                //& " InterventionP2Simul.DatePrevue>='" & dDedut & "'" _
                //& " AND InterventionP2Simul.DatePrevue <='" & dFin & "'" _
                //& " AND utilisateur ='" & fncUserName & "'"


                //=== Ajoute des critéres.
                if (!string.IsNullOrEmpty(sIntervenant))
                {

                    sWhere = sWhere + " AND InterventionP2Simul.Intervenant='" + StdSQLchaine.gFr_DoublerQuote(sIntervenant) + "'";
                }

                if (lCop_NoAuto != 0)
                {
                    sWhere = sWhere + " AND InterventionP2Simul.COP_NoAuto =" + lCop_NoAuto;
                }

                if (!string.IsNullOrEmpty(sCodeImmeuble))
                {
                    if (!string.IsNullOrEmpty(sCodeImmeuble) && string.IsNullOrEmpty(sCodeImmeubleAu))
                    {


                        sWhere = sWhere + " AND InterventionP2Simul.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";

                    }
                    else
                    {

                        sWhere = sWhere + " AND InterventionP2Simul.CodeImmeuble >= '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";

                        sWhere = sWhere + " AND InterventionP2Simul.CodeImmeuble <= '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeubleAu) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(sCai_Code))
                {

                }

                if (!string.IsNullOrEmpty(sEQM_Code))
                {


                }

                if (string.IsNullOrEmpty(sOrderBySimul))
                {
                    General.sSQL = General.sSQL + sWhere + "   ORDER BY InterventionP2Simul.CodeImmeuble, intervenant, datePrevue ";
                }
                else
                {
                    General.sSQL = General.sSQL + sWhere + sOrderBySimul;
                }

                rsVisiteSimul = rsVisiteSimulAdo.fc_OpenRecordSet(General.sSQL);

                ssGridVisiteSimul.DataSource = rsVisiteSimul;
                ssGridVisiteSimul.UpdateData();
                lblTotalSimul.Text = "total " + rsVisiteSimul.Rows.Count;

                //==== affiche le détail des visites (gammes filles + taches
                //  fc_DetailVisite CLng(nz(ssGridVisite.Columns("NoIntervention").Text, 0))
                if (ssGridVisiteSimul.Rows.Count > 0)
                    fc_ssGRIDTachesAnnuelSimul(Convert.ToInt32(General.nz(ssGridVisiteSimul.Rows[0].Cells["NoIntervention"].Value, 0)));

                return functionReturnValue;

            }
            catch (Exception ex)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Warning);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                return functionReturnValue;
            }
        }
        private void fc_LoadTailleEchelle(string sPeriode, AxPLANNINGLib.AxPlanning PL)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_LoadTailleEchelle() - Enter In The Function - sPeriode = {sPeriode}");
            //===> Fin Modif Mondir
            PL.PlanningCtrlRefresh = false;
            if (General.UCase(sPeriode) == General.UCase("AM"))
            {

                PL.SetScaleMinorWidth(1, Convert.ToInt16(General.getFrmReg(ModConstantes.cUserInterP2V2, PL.Name + "TailleEchelleAM", Convert.ToString(PL.GetScaleMinorWidth(1)))));
            }
            else if (General.UCase(sPeriode) == General.UCase("MS"))
            {
                PL.SetScaleMinorWidth(1, Convert.ToInt16(General.getFrmReg(ModConstantes.cUserInterP2V2, PL.Name + "TailleEchelleMS", Convert.ToString(PL.GetScaleMinorWidth(1)))));
            }
            else if (General.UCase(sPeriode) == General.UCase("MJ"))//tested
            {
                PL.SetScaleMinorWidth(1, Convert.ToInt16(General.getFrmReg(ModConstantes.cUserInterP2V2, PL.Name + "TailleEchelleMJ", Convert.ToString(PL.GetScaleMinorWidth(1)))));
            }
            else if (General.UCase(sPeriode) == General.UCase("SJ"))
            {
                PL.SetScaleMinorWidth(1, Convert.ToInt16(General.getFrmReg(ModConstantes.cUserInterP2V2, PL.Name + "TailleEchelleSJ", Convert.ToString(PL.GetScaleMinorWidth(1)))));
            }
            else if (General.UCase(sPeriode) == General.UCase("HJ"))
            {
                PL.SetScaleMinorWidth(1, Convert.ToInt16(General.getFrmReg(ModConstantes.cUserInterP2V2, PL.Name + "TailleEchelleJH", Convert.ToString(PL.GetScaleMinorWidth(1)))));
            }


            PL.PlanningCtrlRefresh = true;


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAffichePlan_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAffichePlan_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            fc_LoadInterP2();
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadInterP2()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_LoadInterP2() - Enter In The Function");
            //===> Fin Modif Mondir
            int lCopNoAuto = 0;
            int loptEQU_P1Compteur = 0;

            try
            {

                if (fc_ErreurPlan() == true)
                {
                    return;
                }

                //fc_SaveParamForm

                if (optOperation.Checked == false && optTaches.Checked == false)
                {
                    optOperation.Checked = true;
                }

                //=== recherche de l'ID du contrat.
                if (!string.IsNullOrEmpty(txtCOP_NoContrat.Text))
                {
                    //
                    //    If txtCOP_Avenant = "" Then
                    //            txtCOP_Avenant = "0"
                    //
                    //    ElseIf Not IsNumeric(txtCOP_Avenant) Then
                    //            txtCOP_Avenant = "0"
                    //    End If
                    //
                    //     sSQL = "SELECT     COP_NoAuto" _
                    //'         & " From COP_ContratP2" _
                    //'         & " WHERE COP_NoContrat = '" & gFr_DoublerQuote(txtCOP_NoContrat) & "'" _
                    //'         & " AND COP_Avenant =" & txtCOP_Avenant & ""
                    //     sSQL = fc_ADOlibelle(sSQL)
                    //
                    //     lCopNoAuto = nz(sSQL, 0)
                    //
                    //     If lCopNoAuto = 0 Then
                    //
                    //         MsgBox "Ce numéro de contrat n'existe pas.", vbInformation, "Opération annulée"
                    //         Exit Sub
                    //     End If

                }



                lCopNoAuto = Convert.ToInt32(General.nz(txtCOP_NoAuto.Text, 0));

                if (OptEQU_P1Compteur0.Checked == true)
                {
                    loptEQU_P1Compteur = 1;
                }
                else if (OptEQU_P1Compteur1.Checked == true)
                {
                    loptEQU_P1Compteur = 2;
                }
                else if (OptEQU_P1Compteur2.Checked == true)
                {
                    loptEQU_P1Compteur = 3;
                }

                if (string.IsNullOrEmpty(General.sFIltreInterP1))
                {
                    loptEQU_P1Compteur = 3;
                }

                fc_InterventionP2(Convert.ToDateTime(dtDateDebPlaning.Text), Convert.ToDateTime(dtDateFinPlaning.Text), lCopNoAuto, txtIntervenant.Text, txtCodeImmeuble.Text, txtIntervSecteur.Text, loptEQU_P1Compteur);//tested

                FC_SaveParam();//tested

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAffichePlan_Click");
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="rstmp"></param>
        private void fc_GraphiqueP2Fusion(ref DataTable rstmp)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_GraphiqueP2Fusion() - Enter In The Function - rstmp.Rows.Count = {rstmp?.Rows?.Count}");
            //===> Fin Modif Mondir
            int i = 0;
            int j = 0;


            string[] tInter = null;
            string[] tCodeImmeuble = null;

            string Label = null;
            string numEvent = null;
            string sCodeImmeuble = null;
            string sIntervenant = null;
            string sEQM_Code = null;

            System.DateTime bBegin = default(System.DateTime);
            System.DateTime bEnd = default(System.DateTime);

            short NumEventType = 0;
            try
            {

                if (rstmp == null)
                {

                    return;
                }
                if (rstmp.Rows.Count == 0)
                {
                    return;
                }

                var _with28 = rstmp;
                // _with28.MoveFirst();

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                //--Bloque le rafraichissement du calendrier.
                PL1.PlanningCtrlRefresh = false;
                var tmp = new ModAdo();
                j = 1;
                foreach (DataRow r in rstmp.Rows)//tested
                {

                    if (sCodeImmeuble != General.nz(r["CodeImmeuble"], "").ToString() || sIntervenant != General.nz(r["Intervenant"], "").ToString())
                    {
                        PL1.AddFolder(-1, 30, (uint)System.Drawing.ColorTranslator.ToWin32(Color.SkyBlue), (uint)System.Drawing.ColorTranslator.ToWin32(Color.Black), "arial", 40, false, false);

                        PL1.SetFolderText(j, 1, General.nz(r["CodeImmeuble"], "").ToString());
                        //PL1.SetFolderText j, 2, nz(rstmp("Intervenant").value, "")

                        PL1.SetFolderText(j, 2, tmp.fc_ADOlibelle("SELECT Nom, Prenom FROM Personnel" + " where matricule='" + r["Intervenant"] + "'", true));
                        Array.Resize(ref tInter, j + 1);

                        tInter[j] = General.nz(r["Intervenant"], "").ToString();
                        Array.Resize(ref tCodeImmeuble, j + 1);

                        tCodeImmeuble[j] = General.nz(r["CodeImmeuble"], "").ToString();
                        j = j + 1;
                    }
                    else
                    {

                    }

                    sCodeImmeuble = General.nz(r["CodeImmeuble"], "").ToString();

                    sIntervenant = General.nz(r["Intervenant"], "").ToString();
                    // sEQM_Code = nz(!EQM_Code, "")
                    // rstmp.MoveNext();
                }

                //-- Ajout de deux types d'évenements réalisé ou non réliséee

                // rstmp.MoveFirst();
                foreach (DataRow r in rstmp.Rows)
                {
                    //If Not IsNull(rstmp!heureDebutP) Then
                    //    bBegin = DateValue(rstmp!DatePrevue) + rstmp!heureDebutP
                    //Else
                    //    bBegin = DateValue(rstmp!DatePrevue) + #9:00:00 AM#
                    //End If

                    //TODO dans vb lorsque r["heureDebutP"]=01/01/1900 et rstmp!DatePrevue=n'importe quel date
                    //=>   bBegin = DateValue(rstmp!DatePrevue) + rstmp!heureDebutP

                    if (!string.IsNullOrEmpty(r["heureDebutP"].ToString()))
                    {
                        int h = Convert.ToDateTime(r["heureDebutP"]).Hour;
                        int m = Convert.ToDateTime(r["heureDebutP"]).Minute;
                        int s = Convert.ToDateTime(r["heureDebutP"]).Second;
                        if (Convert.ToDateTime(r["heureDebutP"].ToString()).ToShortDateString() == "01/01/1900")
                        {

                            bBegin = Convert.ToDateTime(r["DateVisite"].ToString()).AddDays(2).AddHours(h).AddMinutes(m).AddSeconds(s);
                        }
                        else
                        {
                            bBegin = Convert.ToDateTime(r["DateVisite"].ToString()).AddHours(h).AddMinutes(m).AddSeconds(s);
                        }
                    }
                    else
                    {

                        bBegin = Convert.ToDateTime(r["DateVisite"]).Add(new TimeSpan(0, 9, 0, 0));
                    }

                    //Label = rstmp!EQM_Code
                    //If Not IsNull(rstmp!heureFinP) Then
                    //    bEnd = DateValue(rstmp!DatePrevue) + rstmp!heureFinP
                    //Else
                    //    bEnd = DateValue(rstmp!DatePrevue) + #6:00:00 PM#
                    //End If


                    if (!string.IsNullOrEmpty(r["heureFinP"].ToString()))
                    {
                        int h = Convert.ToDateTime(r["heureFinP"]).Hour;
                        int m = Convert.ToDateTime(r["heureFinP"]).Minute;
                        int s = Convert.ToDateTime(r["heureFinP"]).Second;
                        bEnd = Convert.ToDateTime(r["DateVisite"].ToString()).AddHours(h).AddMinutes(m).AddSeconds(s);

                    }
                    else
                    {

                        bEnd = Convert.ToDateTime(r["DateVisite"]).Add(new TimeSpan(0, 18, 0, 0));
                    }


                    j = 0;
                    for (j = 1; j <= tInter.Length - 1; j++)
                    {
                        if (General.UCase(tInter[j]) == General.UCase(r["Intervenant"]) && General.UCase(tCodeImmeuble[j]) == General.UCase(r["CodeImmeuble"]))
                        {
                            if (General.IsDate(r["DateRealise"].ToString()))
                            {
                                //vert si intervention réalisée

                                NumEventType = PL1.AddEventType();

                                PL1.SetEventTypeColor(NumEventType, (uint)System.Drawing.ColorTranslator.ToWin32(Color.MediumSpringGreen));

                            }
                            else
                            {

                                NumEventType = PL1.AddEventType();
                                // rouge si intervention non réalisé.

                                PL1.SetEventTypeColor(NumEventType, (uint)System.Drawing.ColorTranslator.ToWin32(Color.Red));

                            }


                            numEvent = Convert.ToString(PL1.AddEvent(j, (short)NumEventType, -1, bBegin, bEnd, Label, -1, -1));
                            //--attache le n°intervention à l'événement.
                            PL1.SetEventExternalCode(Convert.ToInt32(numEvent), r["NoIntervention"].ToString());
                            // PL1.SetEventBubble numEvent, rstmp!EQM_Code
                        }
                    }
                    //rstmp.MoveNext();
                }

                //--Rafraichis les controles du calendrier.
                PL1.PlanningCtrlRefresh = true;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }
            catch (Exception ex)
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
                PL1.PlanningCtrlRefresh = true;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="dDeb"></param>
        /// <param name="dFin"></param>
        /// <param name="PL1"></param>
        private void fc_initFormatCalendrier(System.DateTime dDeb, System.DateTime dFin, AxPLANNINGLib.AxPlanning PL1)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_initFormatCalendrier() - Enter In The Function - dDeb = {dDeb} - dFin = {dFin}");
            //===> Fin Modif Mondir
            int colWidht = 0;
            System.DateTime dtDate = default(System.DateTime);
            int i = 0;
            string[] tsJferiee = null;

            var _with29 = PL1;

            // colWidht = .SplitPos - cDifColWith


            _with29.PlanningCtrlRefresh = false;
            //-- Limite du calendrier.

            PL1.PlanningBeginTime = dDeb.Date.Add(new TimeSpan(0, 0, 0, 0));// #12:00:01 AM#

            PL1.PlanningEndTime = dFin.Date.Add(new TimeSpan(0, 23, 49, 59));//#11:49:59 PM#
            //--Position du split.
            _with29.SplitPos = 300;

            //--supprime toutes les rubriques.
            _with29.DeleteColumn(3);
            _with29.DeleteColumn(2);
            _with29.DeleteColumn(1);

            PL1.ClearFolders();
            PL1.ClearEvents();

            // .PlanningCtrlRefresh = False
            //ajoute les rubriques.
            if (optListeTaches.Checked == true)
            {
                _with29.AddColumn(1, 120, "Gamme Fille ", Calendrier.TXT_LEFT);
                _with29.AddColumn(2, 150, "Intervenant", Calendrier.TXT_LEFT);
                _with29.AddColumn(3, 120, "Code Immeuble", Calendrier.TXT_LEFT);
            }
            else//tested
            {
                _with29.AddColumn(1, 120, "CodeImmeuble", Calendrier.TXT_LEFT);
                _with29.AddColumn(2, 150, "Intervenant", Calendrier.TXT_LEFT);
            }

            //precise la couleur des jours fériées.
            _with29.NonWorkingDayColor = System.Drawing.Color.Violet;
            //bleu
            _with29.WeeklyDayColor = System.Drawing.Color.MediumTurquoise;
            //rose
            _with29.FolderTitleHeader = "";




            // cherche les jours fériées. ===>Tested
            dtDate = dDeb;
            do
            {
                modP2.fPaques(dtDate);
                for (i = 0; i <= modP2.tabDPaque.Length - 1; i++)
                {
                    if (modP2.tabDPaque[i] >= dDeb && modP2.tabDPaque[i] <= dFin)
                    {
                        PL1.SetNonWorkingDay(modP2.tabDPaque[i]);
                    }
                }


                modP2.fc_JFeriee(ref dtDate, ref tsJferiee);
                for (i = 0; i <= tsJferiee.Length - 1; i++)
                {
                    if (Convert.ToDateTime(tsJferiee[i]) >= dDeb && Convert.ToDateTime(tsJferiee[i]) <= dFin)
                    {
                        PL1.SetNonWorkingDay(Convert.ToDateTime(tsJferiee[i]));
                    }
                }
                tsJferiee = null;
                dtDate = dtDate.AddYears(1);
                if (dtDate > dFin)
                    break;
            } while (true);


            PL1.PlanningBeginTime = dDeb.Date.Add(new TimeSpan(0, 0, 0, 1));

            PL1.PlanningEndTime = dFin.Date.Add(new TimeSpan(0, 23, 49, 59));
            _with29.PlanningCtrlRefresh = true;


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="dDedut"></param>
        /// <param name="dFin"></param>
        /// <param name="lCop_NoAuto"></param>
        /// <param name="sIntervenant"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sMatSecteur"></param>
        /// <param name="lCompteurP1"></param>
        /// <returns></returns>
        private bool fc_InterventionP2(System.DateTime dDedut, System.DateTime dFin, int lCop_NoAuto, string sIntervenant, string sCodeImmeuble, string sMatSecteur, int lCompteurP1)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_InterventionP2() - Enter In The Function - dDedut = {dDedut} - dFin = {dFin} - lCop_NoAuto = {lCop_NoAuto} - sIntervenant = {sIntervenant} " +
                                        $"- sCodeImmeuble = {sCodeImmeuble} - sMatSecteur = {sMatSecteur} - lCompteurP1 = {lCompteurP1}");
            //===> Fin Modif Mondir
            bool functionReturnValue = false;

            string sWhereSQL = null;
            int i = 0;
            UltraGridRow book = null;
            int lRowNumber = 0;

            try
            {
                //===  Rafraichis la tables des listes de taches fusionnées
                //===  devenant ainsi des interventions.
                //sSQL = "SELECT     NoIntervention , InterventionP2.CodeImmeuble  ,PDAB_Libelle,  Article," _
                //& " Intervenant, Personnel.Nom, Designation,  DatePrevue, Duree, Cop_NoAuto," _
                //& " INT_realise, dateRealise, HeureDebut, HeureFin ,heureDebutP, heureFinP,NoSemaine" _
                //& " FROM InterventionP2 " _
                //& " LEFT OUTER JOIN Immeuble ON " _
                //& " InterventionP2.CodeImmeuble = Immeuble.CodeImmeuble " _
                //& " LEFT OUTER JOIN Personnel ON " _
                //& " InterventionP2.Intervenant = Personnel.Matricule " _
                //& " WHERE InterventionP2.DatePrevue>='" & dDedut & "'" _
                //& " AND InterventionP2.DatePrevue <='" & dFin & "'" _
                //& " AND GMAO_Visite = 1"





                General.sSQL = "SELECT     Intervention.Selection, Intervention.NumFicheStandard , Intervention.nointervention, Intervention.NoLigne,Intervention.CodeEtat,  "
                        + " Intervention.NoIntervention AS ID, Intervention.CodeImmeuble, Intervention.PDAB_Noauto, Immeuble.Adresse, Immeuble.CodePostal,"
                        + " Immeuble.Ville, Intervention.PDAB_Libelle,  Intervention.Intervenant, Personnel.Nom, Intervention.DateSaisie, Personnel.Prenom , "
                        + "  Intervention.DatePrevue ,Intervention.DateVisite, Intervention.HeurePrevue, heureDebutP,Intervention.CodeEtatPDA,Intervention.Article, Intervention.Designation, "
                        + " Intervention.CompteurObli,   " + " Intervention.DateRealise,Intervention.ID_PDA, Intervention.PDAB_IDBarre, Intervention.Cop_NoAuto, Intervention.COP_NoContrat, "
                        + " Intervention.CreeLe , Intervention.CreePar, Intervention.ModifierLe, Intervention.ModifierPar, " + " Intervention.EnvoyeLe, Intervention.EnvoyePar "
                        + " , heureFinP, Immeuble.Latitude, Immeuble.Longitude, "
                        + "  Intervention.DPeriodeDu, Intervention.DPeriodeAu , Intervention.ALAR_Code as ALAR_Code_Inter, Intervention.NoDecade, intervention.Commentaire ";

                if (General.sGMAOcriterePartImmeuble == "1")
                {
                    if (Opt3.Checked || Opt4.Checked)
                    {
                        //'=== copro/particulier ou immeuble
                        General.sSQL = General.sSQL + " FROM         Intervention INNER JOIN"
                                     + " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN "
                                     + " Personnel ON Intervention.Intervenant = Personnel.Matricule LEFT OUTER JOIN "
                                     + " Gestionnaire ON Immeuble.Code1 = Gestionnaire.Code1 AND Immeuble.CodeGestionnaire = Gestionnaire.CodeGestinnaire ";

                    }
                    else
                    {
                        General.sSQL = General.sSQL + " FROM         Intervention INNER JOIN"
                                               + " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN "
                                               + " Personnel ON Intervention.Intervenant = Personnel.Matricule   "
                                               + " ";
                    }
                }
                else
                {
                    General.sSQL = General.sSQL + " FROM         Intervention INNER JOIN"
                                              + " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN "
                                              + " Personnel ON Intervention.Intervenant = Personnel.Matricule   "
                                              + " ";
                }

                sWhereSQL = "";

                //sWhereSQL = " WHERE Intervention.DatePrevue>='" & dDedut & "'" _
                //& " AND Intervention.DatePrevue <='" & dFin & "'" _
                //

                sWhereSQL = " WHERE Intervention.DateVisite>='" + dDedut + "'" + " AND Intervention.DateVisite <='" + dFin + "'";
                sWhereSQL = sWhereSQL + " and VisitePDA = 1 ";

                if (General.sGMAOcriterePartImmeuble == "1")
                {
                    if (Opt3.Checked)
                    {
                        //=== copro/particulier
                        sWhereSQL = sWhereSQL + " and CodeQualif_Qua ='" + General.cPart + "'";
                    }
                    else if (Opt4.Checked)
                    {
                        //== immeuble.
                        sWhereSQL = sWhereSQL + " and (CodeQualif_Qua <>'" + General.cPart + "' OR CodeQualif_Qua is null)";
                    }
                }

                if (!string.IsNullOrEmpty(sMatSecteur))
                {

                    sWhereSQL = sWhereSQL + " AND Intervention.Secteur ='" + StdSQLchaine.gFr_DoublerQuote(sMatSecteur) + "'";

                }

                //=== Ajoute des critéres.
                if (!string.IsNullOrEmpty(sIntervenant))
                {

                    sWhereSQL = sWhereSQL + " AND Intervention.Intervenant='" + StdSQLchaine.gFr_DoublerQuote(sIntervenant) + "'";
                }

                if (lCop_NoAuto != 0)
                {
                    sWhereSQL = sWhereSQL + " AND Intervention.COP_NoAuto =" + lCop_NoAuto;
                }

                if (!string.IsNullOrEmpty(sCodeImmeuble))
                {
                    sWhereSQL = sWhereSQL + " AND Intervention.CodeImmeuble  = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";
                }


                if (!string.IsNullOrEmpty(cmbStatutPDA.Text))
                {


                    sWhereSQL = sWhereSQL + " AND Intervention.CodeEtatPDA  = '" + StdSQLchaine.gFr_DoublerQuote(cmbStatutPDA.Text) + "'";

                }

                if (lCompteurP1 == 1)
                {
                    //=== uniquement avec cpt P1
                    //sWhereSQL = sWhereSQL & " AND ( EQM_EquipementP2Imm.EQU_P1Compteur = 1)"
                    sWhereSQL = sWhereSQL + " AND  Intervention.NoIntervention IN (SELECT     NoInterventionVisite" + " From OperationP2" + " WHERE     EQU_P1Compteur = 1 )";

                }
                else if (lCompteurP1 == 2)
                {
                    //=== exclure avec cpt P1
                    //sWhereSQL = sWhereSQL & " AND  (EQM_EquipementP2Imm.EQU_P1Compteur is null OR EQM_EquipementP2Imm.EQU_P1Compteur = 0)"
                    sWhereSQL = sWhereSQL + " AND  Intervention.NoIntervention IN (SELECT     NoInterventionVisite" + " From OperationP2"
                            + " WHERE     (EQU_P1Compteur is null OR EQU_P1Compteur = 0) )";

                }
                else if (lCompteurP1 == 3)
                {
                    //=== aucun filtre, prends toutes les prestations avec ou sans compteur P1
                }


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                if (string.IsNullOrEmpty(sOrderByPlanif))
                {
                    sOrderByPlanif = " ORDER BY Intervention.datePrevue,  Intervention.CodeImmeuble, Intervention.intervenant  ";
                }


                ssGridVisite.UpdateData();

                rsVisite = rsVisiteAdo.fc_OpenRecordSet(General.sSQL + sWhereSQL);
                i = 1;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                if (rsVisite.Rows.Count > 0)
                {
                    foreach (DataRow rsVisiteRow in rsVisite.Rows)
                    {
                        rsVisiteRow["NoLigne"] = i;
                        rsVisiteRow["Selection"] = 0;
                        i = i + 1;

                    }
                }
                ssGridVisite.DataSource = rsVisite;

                //===> Mondir le 06.11.2020, added to fix https://groupe-dt.mantishub.io/view.php?id=2046
                ssGridVisite.UpdateData();

                if (lRowNumber >= 0 && ssGridVisite.ActiveRow != null)
                {
                    ssGridVisite.ActiveRow = book;
                    lRowNumber = ssGridVisite.ActiveRow.Index;
                    ssGridVisite.Rows[lRowNumber].Activated = true;

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                }
                else if (ssGridVisite.Rows.Count > 0)
                {
                    book = ssGridVisite.Rows[0];
                    ssGridVisite.Rows[lRowNumber].Activated = true;
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                lbltotal2.Text = "Total :" + rsVisite.Rows.Count;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                if (ssGridVisite.ActiveRow != null)
                    fc_ssGRIDTachesAnnuel(Convert.ToInt32(General.nz(ssGridVisite.ActiveRow.Cells["NoIntervention"].Value.ToString(), 0).ToString()));//tested

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                fc_initFormatCalendrier(Convert.ToDateTime(dtDateDebPlaning.Text), Convert.ToDateTime(dtDateFinPlaning.Text), PL1);//tested


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                fc_GraphiqueP2Fusion(ref rsVisite);//tested

                FC_SaveParam();//tested


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return functionReturnValue;

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";fc_interventionP2");

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                return functionReturnValue;
            }
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command3_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"Command3_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            if (General.sEnvoiePdaCorrective == "1")
            {
                fc_sendCreate(true);

            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La fonction PDA n'est pas installée.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdTailleMoins_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdTailleMoins_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            var cmd = sender as Button;
            int index = Convert.ToInt32(cmd.Tag);


            try
            {
                switch (index)
                {

                    case 0:

                        PL1.PlanningCtrlRefresh = false;
                        PL1.SetScaleMinorWidth(1, (short)(PL1.GetScaleMinorWidth(1) - 10));
                        PL1.PlanningCtrlRefresh = true;
                        fc_SaveTailleEchelle(txtPeriode.Text, PL1);
                        break;

                    case 1:
                        break;

                }

                return;
            }
            catch (Exception ex)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Information);
            }


        }
        private void fc_SaveTailleEchelle(string sPeriode, AxPLANNINGLib.AxPlanning PL)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_SaveTailleEchelle() - Enter In The Function - sPeriode = {sPeriode}");
            //===> Fin Modif Mondir
            PL.PlanningCtrlRefresh = false;

            if (General.UCase("AM") == General.UCase(sPeriode))
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, PL.Name + "." + "TailleEchelleAM", Convert.ToString(PL.GetScaleMinorWidth(1)));
            }
            else if (General.UCase("MS") == General.UCase(sPeriode))
            {


                General.saveInReg(ModConstantes.cUserInterP2V2, PL.Name + "." + "TailleEchelleMS", Convert.ToString(PL.GetScaleMinorWidth(1)));
            }
            else if (General.UCase("MJ") == General.UCase(sPeriode))
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, PL.Name + "." + "TailleEchelleMJ", Convert.ToString(PL.GetScaleMinorWidth(1)));
            }
            else if (General.UCase("SJ") == General.UCase(sPeriode))
            {

                General.saveInReg(ModConstantes.cUserInterP2V2, PL.Name + "." + "TailleEchelleSJ", Convert.ToString(PL.GetScaleMinorWidth(1)));
            }
            else if (General.UCase("JH") == General.UCase(sPeriode))
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, PL.Name + "." + "TailleEchelleJH", Convert.ToString(PL.GetScaleMinorWidth(1)));

            }
            PL.PlanningCtrlRefresh = true;


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdTaillePlus_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdTaillePlus_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            var cmd = sender as Button;
            int index = Convert.ToInt32(cmd.Tag);

            switch (index)
            {

                case 0:
                    PL1.PlanningCtrlRefresh = false;
                    PL1.SetScaleMinorWidth(1, (short)(PL1.GetScaleMinorWidth(1) + 10));
                    PL1.PlanningCtrlRefresh = true;
                    fc_SaveTailleEchelle(txtPeriode.Text, PL1);
                    break;

                case 1:
                    break;

            }
        }


        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridHisto_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridHisto_DoubleClickRow() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                if (ssGridHisto.ActiveRow == null) return;
                //Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ssGridHisto.ActiveRow.Cells["HeureDebut"].Column.Format.ToString());
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, ssGridHisto.ActiveRow.Cells["NoIntervention"].Value.ToString());
                View.Theme.Theme.Navigate(typeof(UserIntervention));
                return;
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";ssGridHisto_DblClick");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridHisto_AfterRowActivate(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridHisto_AfterRowActivate() - Enter In The Function");
            //===> Fin Modif Mondir
            if (ssGridHisto.ActiveRow != null)
            {

                fc_ssGRIDTachesAnnuelHisto(Convert.ToInt32(General.nz((ssGridHisto.ActiveRow.Cells["NumFicheStandard"].Value), 0)));
            }

        }

        private void ssGridTachesAnnuel_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //e.DisplayPromptMsg = false;
            //if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            //{
            //    e.Cancel = true;
            //}
        }

        private void ssGridTachesAnnuel_Leave(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridTachesAnnuel_Leave() - Enter In The Function");
            //===> Fin Modif Mondir
            ssGridTachesAnnuel.UpdateData();
        }



        private void ssGridVisite_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridVisite_BeforeExitEditMode() - Enter In The Function");
            //===> Fin Modif Mondir
            var _with33 = ssGridVisite;
            //If .Columns("RAMI_Statut").Position = ColIndex Then
            //        If UCase("RAMI_Statut") = UCase(.Columns(ColIndex).Name) Then
            //            .Columns("RAMI_LibelleStatut").value = fc_ADOlibelle("select  RAMS_Libelle " _
            //'                & " FROM  RAMS_Statut WHERE RAMS_Code ='" & gFr_DoublerQuote(.Columns("RAMI_Statut").Text) & "'")
            //        End If

            //If ColIndex = .Columns("RAMI_Select").Position Then
            if (General.UCase("Selection") == General.UCase(ssGridVisite.ActiveCell.Column.Key))
            {
                if (_with33.ActiveRow.Cells["Selection"].Text == "-1")
                {
                    _with33.ActiveRow.Cells["Selection"].Value = 1;
                }
            }


            //If ColIndex = .Columns("RAMI_PriseRV").Position Then
            if (General.UCase("PriseRV") == General.UCase(ssGridVisite.ActiveCell.Column.Key))
            {
                if (_with33.ActiveRow.Cells["PriseRV"].Text == "-1")
                {
                    _with33.ActiveRow.Cells["PriseRV"].Value = 1;
                }
            }

            if (!_with33.ActiveRow.Cells["Intervenant"].Text.IsNullOrEmpty())
            {
                CheckIntervenant();
            }
        }

        /// Mondir le 15.04.2021 https://groupe-dt.mantishub.io/view.php?id=2389#c5866
        /// </summary>
        /// <returns></returns>
        private bool CheckIntervenant()
        {
            using (var tmpModAdo = new ModAdo())
            {
                var req = $"SELECT Nom FROM Personnel WHERE NonActif = 0 AND Matricule = '{ssGridVisite.ActiveRow.Cells["Intervenant"].Text}'";
                req = tmpModAdo.fc_ADOlibelle(req);
                if (string.IsNullOrEmpty(req))
                {
                    //===> Mondir le 12.04.2021, https://groupe-dt.mantishub.io/view.php?id=2389#c5846
                    //CustomMessageBox.Show("Veuillez vérifier que cet Intervenant existe et qu'il est actif", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CustomMessageBox.Show("Cet intervenant n'existe pas ou est inactif\nModification annulée", "Modification annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //functionReturnValue = false;
                    //return functionReturnValue;
                    var interventant = tmpModAdo.fc_ADOlibelle($"SELECT Intervenant FROM Intervention WHERE NoIntervention = '{ssGridVisite.ActiveRow.Cells["NoIntervention"].Text}'");
                    ssGridVisite.ActiveRow.Cells["Intervenant"].Value = interventant;
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridVisite_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridVisite_InitializeRow() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                if (e.ReInitialize)
                    return;
                string sSQL = null;
                int i = 0;

                var _with35 = ssGridVisite;

                var tmp = new ModAdo();

                sSQL = "SELECT NOM , PRENOM FROM PERSONNEL WHERE MATRICULE ='" + StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["Intervenant"].Text) + "'";
                sSQL = tmp.fc_ADOlibelle(sSQL, true);
                e.Row.Cells["Nom"].Value = sSQL;

                if (e.Row.Cells["Selection"].Value == DBNull.Value)
                {
                    e.Row.Cells["Selection"].Value = false;
                }

                if (e.Row.Cells["CompteurObli"].Value == DBNull.Value)
                {
                    e.Row.Cells["CompteurObli"].Value = false;
                }

                e.Row.Cells["CodeEtatPDA"].Appearance.ForeColor = Color.Red;
                e.Row.Cells["NoLigne"].Appearance.ForeColor = Color.Blue;
                e.Row.Cells["CodeImmeuble"].Appearance.ForeColor = Color.Blue;
                e.Row.Cells["Ville"].Appearance.ForeColor = Color.Blue;
                e.Row.Cells["Nom"].Appearance.ForeColor = Color.Blue;
                e.Row.Cells["CreeLe"].Appearance.ForeColor = Color.Blue;
                e.Row.Cells["CreePar"].Appearance.ForeColor = Color.Blue;
                e.Row.Cells["ModifierLe"].Appearance.ForeColor = Color.Blue;
                e.Row.Cells["ModifierPar"].Appearance.ForeColor = Color.Blue;
                e.Row.Cells["EnvoyeLe"].Appearance.ForeColor = Color.Blue;
                e.Row.Cells["EnvoyePar"].Appearance.ForeColor = Color.Blue;
                e.Row.Cells["Selection"].Appearance.BackColor = Color.Yellow;

                //*  Todo
                //_with64.StyleSets.Add(ModPDA.cNomRvAPrendre);
                //_with64.StyleSets(ModPDA.cNomRvAPrendre).BackColor = ModPDA.cColorRVaPrendrex;

                //_with64.StyleSets.Add(ModPDA.cNomRvPris);
                //_with64.StyleSets(ModPDA.cNomRvPris).BackColor = ModPDA.cColorRVPrisx;

                //_with64.StyleSets.Add(ModPDA.cNomSansRv);
                //_with64.StyleSets(ModPDA.cNomSansRv).BackColor = ModPDA.cColorSansRVx;



                //.Columns("RAMI_LibelleStatut").value = fc_ADOlibelle("SELECT  RAMS_Libelle as libelle " _
                //& " FROM RAMS_Statut where        RAMS_Code='" & .Columns("RAMI_Statut").Text & "'")*/


                //======= couleur rendez vous.
                //======= prise de rendez vou : si rendez vous pris on effecte la valeur 2, si pas de rendez vous la valeur 0
                //=== et si 1 rendez vous a prendre.
                //=== rendez vous à prendre.
                //If nz(.Columns("PriseRV").Text, "0") = "0" Then
                //    For i = 0 To .Columns.Count - 1
                //        .Columns(i).CellStyleSet cNomSansRv, .Row
                //    Next i
                //End If

                //If .Columns("PriseRV").Text = "1" Then
                //    For i = 0 To .Columns.Count - 1
                //        .Columns(i).CellStyleSet cNomRvAPrendre, .Row
                //    Next i
                //End If

                //        If .Columns("RAMI_PriseRV").Text = "2" Then
                //            For i = 0 To .Columns.Count - 1
                //                .Columns(i).CellStyleSet cNomRvPris, .Row
                //            Next i
                //        End If

                //If IsDate(.Columns("DateRV").Text) Then
                //    For i = 0 To .Columns.Count - 1
                //        .Columns(i).CellStyleSet cNomRvPris, .Row
                //    Next i
                //End If


                // e.Row.Cells["Selection"].CellStyleSet(ModPDA.cSelection, _with35.Row);
                e.Row.Update();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssGridVisite_RowLoaded");
            }

        }

        private void ssOpeTaches_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssOpeTaches_BeforeRowsDeleted() - Enter In The Function");
            //===> Fin Modif Mondir
            e.Cancel = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void ssOpeTaches_Error(object sender, ErrorEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssOpeTaches_Error() - Enter In The Function");
            //===> Fin Modif Mondir
            if (e.ErrorType == ErrorType.Data)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + e.DataErrorInfo.Cell.Column.Key;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }

        }

        private void ssGridTachesAnnuel_Error(object sender, ErrorEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridTachesAnnuel_Error() - Enter In The Function");
            //===> Fin Modif Mondir
            if (e.ErrorType == ErrorType.Data)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + e.DataErrorInfo.Cell.Column.Key;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lNoIntervention"></param>
        private void fc_ssGRIDTachesAnnuel(int lNoIntervention)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ssGRIDTachesAnnuel() - Enter In The Function - lNoIntervention = {lNoIntervention}");
            //===> Fin Modif Mondir
            try
            {
                string sSQL = null;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
                if (optOperation.Checked == true)//tested
                {

                    ssGridTachesAnnuel.Visible = true;
                    ssOpeTaches.Visible = false;

                    //sSQL = "SELECT     NoIntervention, CodeImmeuble,   " _
                    //& " CAI_Libelle, EQM_code, TPP_Code, Intervenant, NoSemaine, DatePrevue, " _
                    //& " DateRealise, INT_Realisee, Deplacement, Duree, HeureDebut, HeureFin, INT_J," _
                    //& " INT_M, INT_Annee, PEI_Cout, PEI_ForceVisite, SEM_Code," _
                    //& " COP_NoContrat, Commentaire " _
                    //& " FROM         OperationP2" _
                    //& " WHERE NoInterventionVisite =" & LNointervention _
                    //& " order by dateprevue , TPP_Code, CodeImmeuble, PDAB_Libelle"


                    sSQL = "SELECT     CodeImmeuble, PDAB_Libelle AS Localisation, NoInterventionVisite, GAI_LIbelle AS Equipements, "
                        + " CAI_Libelle AS Composant, EQM_Code AS Operation," + " EQM_Libelle, " + " CTR_Libelle, MOY_Libelle, ANO_Libelle, OPE_Libelle, "
                        + " TPP_Code AS Periodicite, NoSemaine, Duree " + " From OperationP2 " + " WHERE NumFicheStandard = " + lNoIntervention
                        + " ORDER BY Localisation, Equipements, Composant, Operation";


                    rsOpP2 = rsOpP2Ado.fc_OpenRecordSet(sSQL);
                    ssGridTachesAnnuel.DataSource = rsOpP2;
                    ssGridTachesAnnuel.UpdateData();

                }
                else if (optTaches.Checked == true)
                {

                    ssGridTachesAnnuel.Visible = false;
                    ssOpeTaches.Visible = true;

                    sSQL = "SELECT     OperationP2.DatePrevue, OperationP2.EQM_code, " + " TIP2_TachesInterP2.TIP2_Designation1, "
                        + " TIP2_TachesInterP2.TIP2_NoLIgne, TIP2_TachesInterP2.Realise, " + " TIP2_TachesInterP2.DateRealise,"
                        + " OperationP2.NoIntervention , OperationP2.NoInterventionVisite" + " FROM OperationP2 INNER JOIN"
                        + " TIP2_TachesInterP2 "
                        + " ON OperationP2.NoIntervention = TIP2_TachesInterP2.NoInterventionOp  WHERE NoInterventionVisite =" + lNoIntervention
                        + " ORDER BY OperationP2.DatePrevue, OperationP2.EQM_code, TIP2_TachesInterP2.TIP2_NoLIgne";


                    rsOpTaches = rsOpTachesAdo.fc_OpenRecordSet(sSQL);
                    ssOpeTaches.DataSource = rsOpTaches;
                    ssOpeTaches.UpdateData();
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_ssGRIDTachesAnnuel;");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="dtDateCreate"></param>
        /// <returns></returns>
        private bool fc_AutoriseUserCreate(System.DateTime dtDateCreate)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_AutoriseUserCreate() - Enter In The Function - dtDateCreate = {dtDateCreate}");
            //===> Fin Modif Mondir
            bool functionReturnValue = false;
            //=== empeche deux utilisateur de plannifier en même temps.
            string sSQL = null;
            string sCritere = null;
            DataTable rs = default(DataTable);
            ModAdo rsAdo = new ModAdo();
            string sUtil = null;
            try
            {

                functionReturnValue = false;

                sSQL = "SELECT   TOP 1  P2PU_Noauto, P2PU_Utilisateur, P2PU_Date, P2PU_EnCours, P2PU_Critere" + " From P2PU_P2PlannUtil "
                    + " Where (P2PU_EnCours = 1) " + " ORDER BY P2PU_Date DESC";


                rs = rsAdo.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count == 0)//tested
                {

                    sCritere = " - Mois: " + txtMoisPlanif.Text + "\n";
                    if (!string.IsNullOrEmpty(txtIntervSecteur.Text))
                    {
                        sCritere = sCritere + " - Secteur immeuble: " + txtIntervSecteur.Text + "\n";
                    }

                    if (!string.IsNullOrEmpty(txtIntervenant.Text))
                    {
                        sCritere = sCritere + " - intervenant: " + txtIntervenant.Text + "\n";
                    }
                    if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                    {
                        sCritere = sCritere + " - Codeimmeuble: " + txtCodeImmeuble.Text + "\n";
                    }

                    if (!string.IsNullOrEmpty(txtCOP_NoContrat.Text))
                    {
                        sCritere = sCritere + " - No contrat: " + txtCOP_NoContrat.Text + "\n"
                            + " - Avenant: " +
                            // + txtCOP_Avenant.Text + control not found in vb
                            "\n" + " - Cop_noauto: "
                            + txtCOP_NoAuto.Text;
                    }

                    var newR = rs.NewRow();

                    newR["P2PU_Utilisateur"] = General.Left(General.fncUserName(), 50);
                    newR["P2PU_Date"] = dtDateCreate;
                    newR["P2PU_EnCours"] = 1;
                    newR["P2PU_Critere"] = General.Left(sCritere, 300);
                    rs.Rows.Add(newR.ItemArray);
                    rsAdo.Update();

                    rs.Dispose();

                    rs = null;
                    functionReturnValue = true;
                    return functionReturnValue;
                }

                sSQL = "SELECT USR_Name From USR_Users " + " WHERE USR_Nt ='" + rs.Rows[0]["P2PU_Utilisateur"].ToString() + "'";
                sSQL = rsAdo.fc_ADOlibelle(sSQL);

                if (!string.IsNullOrEmpty(sSQL))
                {
                    sUtil = " (" + sSQL + ")";
                }

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention une création des visites d'entretien est en cours d'exécution par l'utilisateur "
                    + rs.Rows[0]["P2PU_Utilisateur"].ToString() + sUtil + " depuis le " + rs.Rows[0]["P2PU_Date"].ToString() + "," + "\n"
                    + " Vous ne pouvez pas créer de visites d'entretien tant que celle-ci n'est pas terminée." + "\n"
                    + "Veuillez patienter avant de relancer cette création.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                rs.Dispose();

                rs = null;
                return functionReturnValue;
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";fc_AutoriseUserCreate");
                return functionReturnValue;
            }

        }
        private void fc_DebloqueCreation()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_DebloqueCreation() - Enter In The Function");
            //===> Fin Modif Mondir
            string sSQL = null;

            sSQL = "UPDATE    P2PU_P2PlannUtil" + " Set P2PU_EnCours = 0 , P2PU_TermineLe ='" + DateTime.Now + "', P2PU_TerminePar ='"
                + General.Left(General.fncUserName(), 50) + "'" + " WHERE     (P2PU_EnCours = 1)";

            General.Execute(sSQL);

        }

        private void cmdVisites_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdVisites_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            System.DateTime dtDateDeb = default(System.DateTime);
            System.DateTime dtDateFin = default(System.DateTime);
            System.DateTime dtTemp = default(System.DateTime);
            int lCopNoAuto = 0;
            System.DateTime dtCreate = default(System.DateTime);
            int loptEQU_P1Compteur = 0;
            bool bAutorise = false;
            string sSQLPeriode = null;
            //    If txtCOP_NoAuto = "" Then
            //        MsgBox "Sélectionnez d'abord un contrat?", vbInformation, ""
            //        Exit Sub
            //    End If
            try
            {
                cmdVisites.Enabled = false;

                if (fc_ErreurPlan() == true)
                {
                    cmdVisites.Enabled = true;
                    return;
                }

                if (string.IsNullOrEmpty(txtMoisPlanif.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez indiquer le mois de planification souhaité.", "Pèriode de planification manquante", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtMoisPlanif.Focus();
                    cmdVisites.Enabled = true;
                    return;
                }

                //===> Mondir le 09.02.2021, https://groupe-dt.mantishub.io/view.php?id=2276
                //===> Mondir le 11.02.2021, ajout de cette condition pour corriger https://groupe-dt.mantishub.io/view.php?id=2285
                if (txtCOP_NoAuto.Text != "")
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        string req = "SELECT COP_Statut FROM  COP_ContratP2 " +
                                     $"WHERE COP_NoAuto = '{txtCOP_NoAuto.Text}'";
                        req = tmpModAdo.fc_ADOlibelle(req);
                        if (req.ToUpper() != "A")
                        {
                            CustomMessageBox.Show("Seul le statut A est autorisé pour la création des visites",
                                "Simulation annulée",
                                MessageBoxButtons.OK, MessageBoxIcon.Information);
                            cmdVisites.Enabled = true;
                            return;
                        }
                    }
                }
                //===> Fin Modif Mondir


                //=== recherche de l'ID du contrat.
                //If txtCOP_NoContrat <> "" Then
                //
                //        If txtCOP_Avenant = "" Then
                //                txtCOP_Avenant = "0"
                //
                //        ElseIf Not IsNumeric(txtCOP_Avenant) Then
                //                txtCOP_Avenant = "0"
                //        End If
                //
                //         sSQL = "SELECT     COP_NoAuto" _
                //'                & " From COP_ContratP2" _
                //'                & " WHERE COP_NoContrat = '" & gFr_DoublerQuote(txtCOP_NoContrat) & "'" _
                //'                & " AND COP_Avenant =" & txtCOP_Avenant & ""
                //         sSQL = fc_ADOlibelle(sSQL)
                //
                //         lCopNoAuto = nz(sSQL, 0)
                //
                //         If lCopNoAuto = 0 Then
                //                MsgBox "Ce numéro de contrat n'existe pas.", vbInformation, "Opération annulée"
                //                cmdVisites.Enabled = True
                //                Exit Sub
                //         End If
                //End If


                lCopNoAuto = Convert.ToInt32(General.nz(txtCOP_NoAuto.Text, 0));

                // ModMajPDA.fc_SaveParamForm(this, ModConstantes.cUserInterP2V2);

                //=== alerte l'utilisateur qu'il va planifier sur une trop longue période
                dtDateDeb = Convert.ToDateTime(dtDateDebPlaning.Text);
                dtDateFin = Convert.ToDateTime(dtDateFinPlaning.Text);

                dtTemp = DateTime.Now.AddMonths(6).AddDays(-1);

                if (dtDateFin > dtTemp)
                {
                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention vous allez planifier une période dépassant un mois," + "\n" + " Voulez vous continuer ? ", "Avertissement", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {

                        cmdVisites.Enabled = true;
                        return;

                    }
                }

                sSQLPeriode = "SELECT     AUT_Nom"
                            + " From AUT_Autorisation "
                            + " WHERE     (AUT_Formulaire = '" + this.Name + "') "
                            + " AND (AUT_Objet = 'CreationVisiteSur6Mois') "
                            + " AND (AUT_Nom = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "')";
                using (var tmp = new ModAdo())
                    sSQLPeriode = tmp.fc_ADOlibelle(sSQLPeriode);


                //=== la planification ne peut que se faire l'année en cours sauf pour le mois de janvier.
                bAutorise = false;

                if (string.IsNullOrEmpty(sSQLPeriode))
                {
                    if (dtDateDeb > DateTime.Now.AddMonths(5))
                    {

                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas planifier des visites d'entretiens au-delà de six mois.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmdVisites.Enabled = true;
                        return;
                    }
                }
                else
                {
                    if (dtDateDeb.Year != DateTime.Today.Year)
                    {

                        if (dtDateDeb.Month == 1 && dtDateFin.AddDays(1).Day > 61)
                        {

                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La planification réel des visites ne peut pas être exécuté plus de deux mois à l'avance par rapport à la date du jour, veuillez utiliser la simulation.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        else if (dtDateDeb.Month == 1 && dtDateFin.AddDays(1).Day < 61)
                        {
                            //===  autorise la création.
                            bAutorise = true;
                        }
                        else
                        {

                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La planification ne peut se faire que pour l'année en cours.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        if (bAutorise == false)
                        {
                            cmdVisites.Enabled = true;
                            return;
                        }
                    }
                }



                dtCreate = DateTime.Now;
                if (fc_AutoriseUserCreate(dtCreate) == false)
                {
                    //=== empeche deux utilisateur de plannifier en même temps.

                    cmdVisites.Enabled = true;
                    return;
                }

                if (OptEQU_P1Compteur0.Checked == true)
                {
                    loptEQU_P1Compteur = 1;
                }
                else if (OptEQU_P1Compteur1.Checked == true)
                {
                    loptEQU_P1Compteur = 2;
                }
                else if (OptEQU_P1Compteur2.Checked == true)
                {
                    loptEQU_P1Compteur = 3;
                }

                if (string.IsNullOrEmpty(General.sFIltreInterP1))
                {
                    loptEQU_P1Compteur = 3;
                }

                //txtErreur = fc_SimulationCreeCalendrierV2(dtDateDebPlaning, lCopNoAuto, _
                //dtDateFinPlaning, False, txtIntervenant, txtCodeimmeuble, "", "", txtIntervSecteur, , nz(chkEQU_P1Compteur.value, 0))

                txtErreur.Text = ModP2v2.fc_SimulationCreeCalendrierV2(Convert.ToDateTime(dtDateDebPlaning.Text), lCopNoAuto, Convert.ToDateTime(dtDateFinPlaning.Text), false, txtIntervenant.Text, txtCodeImmeuble.Text, "", "", txtIntervSecteur.Text, false, loptEQU_P1Compteur);

                cmdAffichePlan_Click(cmdAffichePlan, new System.EventArgs());


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                if (!string.IsNullOrEmpty(txtErreur.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention des erreurs ont été décelés, veuillez consulter le détail indiqué en dessous de la grille.");
                }

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Planification des visites terminée.", "Planification", MessageBoxButtons.OK, MessageBoxIcon.Information);


                cmdVisites.Enabled = true;
                fc_DebloqueCreation();

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdVisites_Click");
                cmdVisites.Enabled = true;
                fc_DebloqueCreation();
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <returns></returns>
        private bool fc_ErreurPlan()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ErreurPlan() - Enter In The Function");
            //===> Fin Modif Mondir
            bool functionReturnValue = false;
            functionReturnValue = false;

            if (!General.IsDate(dtDateDebPlaning.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date de debut de planification invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dtDateDebPlaning.Focus();
                functionReturnValue = true;
                return functionReturnValue;
            }

            if (!General.IsDate(dtDateFinPlaning.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date de fin de planification invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                dtDateFinPlaning.Focus();
                functionReturnValue = true;
                return functionReturnValue;
            }
            return functionReturnValue;

        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_DropMaticule()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_DropMaticule() - Enter In The Function");
            //===> Fin Modif Mondir
            //=== alimente la combo liée a la grille des intervenants avce la liste du personnel

            General.sSQL = "";
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom,Personnel.prenom, Qualification.CodeQualif,"
                + " Qualification.Qualification, Personnel.Kobby, Personnel.Note_NOT, personnel.NoPDA "
                + " FROM Qualification RIGHT JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif"
                + " where  (Personnel.NonActif is null or Personnel.NonActif = 0)" + " order by Personnel.Nom";

            sheridan.InitialiseCombo(ssDropMatricule, General.sSQL, "");

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserIntervP2V2_VisibleChanged(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"UserIntervP2V2_VisibleChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            string sDateDebutSaison = null;
            string sDateFinSaison = null;
            string sNumMois = null;
            string sNumSem = null;
            object objcontrole = null;

            //===> Mondir le 22.09.2020 pour ajouter les modifs de la version V21.09.2020
            string sSQlbloque = "";
            //===> Fin Modif Mondir

            if (!Visible)
                return;


            //===> Mondir le 22.09.2020 pour ajouter les modifs de la version V21.09.2020
            sSQlbloque = "SELECT     AUT_Formulaire" +
                " From AUT_Autorisation " +
                " WHERE        (AUT_Formulaire = N'" + ModConstantes.cUserInterP2V2 + "') " + //<=== Mondir, changer Name par ModConstantes.cUserInterP2V2 car celui qui a migré cette fiche n'a pas respecté le nom de la fiche
                " AND (AUT_Objet = N'DebloqueGMAO') " +
                //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
                " AND AUT_Nom = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'";
            //===> Fin Modif Mondir

            cmdDebloquer.Visible = false;
            var tmpModAdo = new ModAdo();
            sSQlbloque = tmpModAdo.fc_ADOlibelle(sSQlbloque);
            if (!string.IsNullOrEmpty(sSQlbloque))
                cmdDebloquer.Visible = true;
            else
                cmdDebloquer.Visible = false;
            //===> Fin Modif Mondir

            //=== initialise les critres  ====>Tested
            fc_initPlan();
            fc_InitHisto();
            fc_InitSimul();

            //=== recupére les criteres enregistré dans la base de registre.  ====>Tested
            fc_GetParam();
            fc_LibintervenantSimul();
            fc_libCreatIntervenant();
            fc_libAgentRamoHisto();
            fc_libAgentSecteurHisto();

            //=== tESTED
            fc_DropMaticule();

            //================Tested
            cmbStatutPDAhisto.DataSource = null;
            DataTable cmbStatutPDAhistoDT = new DataTable();
            cmbStatutPDAhistoDT.Columns.Add("PDA");
            cmbStatutPDAhistoDT.Rows.Add(ModPDA.cAnnulee);
            cmbStatutPDAhistoDT.Rows.Add(ModPDA.cDemandeAnnulation);
            cmbStatutPDAhistoDT.Rows.Add(ModPDA.cAnnulationRefuse);
            cmbStatutPDAhistoDT.Rows.Add(ModPDA.cEnvoye);
            cmbStatutPDAhistoDT.Rows.Add(ModPDA.cErreurTransmis);
            cmbStatutPDAhistoDT.Rows.Add(ModPDA.cNonEnvoye);
            cmbStatutPDAhistoDT.Rows.Add(ModPDA.cREcu);
            cmbStatutPDAhistoDT.Rows.Add(ModPDA.cDebut);
            cmbStatutPDAhistoDT.Rows.Add(ModPDA.cFin);
            cmbStatutPDAhistoDT.Rows.Add(ModPDA.cPasFait);
            cmbStatutPDAhisto.DataSource = cmbStatutPDAhistoDT;

            //================Tested
            cmbStatutPDA.DataSource = null;
            DataTable cmbStatutPDADT = new DataTable();
            cmbStatutPDADT.Columns.Add("PDA");
            cmbStatutPDADT.Rows.Add(ModPDA.cAnnulee);
            cmbStatutPDADT.Rows.Add(ModPDA.cDemandeAnnulation);
            cmbStatutPDADT.Rows.Add(ModPDA.cAnnulationRefuse);
            cmbStatutPDADT.Rows.Add(ModPDA.cEnvoye);
            cmbStatutPDADT.Rows.Add(ModPDA.cErreurTransmis);
            cmbStatutPDADT.Rows.Add(ModPDA.cNonEnvoye);
            cmbStatutPDADT.Rows.Add(ModPDA.cREcu);
            cmbStatutPDADT.Rows.Add(ModPDA.cDebut);
            cmbStatutPDADT.Rows.Add(ModPDA.cFin);
            cmbStatutPDADT.Rows.Add(ModPDA.cPasFait);
            cmbStatutPDA.DataSource = cmbStatutPDADT;


            ///'''''        'fc_chargeStyleColorInter GridInterventions
            ///'''''
            ///'''''        sNumMois = cmbMois.Text
            ///'''''        sNumSem = cmbSemaine.Text
            ///'''''
            ///'''''        If Month(Date) <= CInt(nz(sMoisDebutSaison, "07")) Then
            ///'''''                sDateDebutSaison = "01/" & sMoisDebutSaison & "/" & Year(Date) - 1
            ///'''''                sDateFinSaison = DateAdd("yyyy", 1, CDate(sDateDebutSaison))
            ///'''''                sDateFinSaison = DateAdd("d", -1, CDate(sDateFinSaison))
            ///'''''        Else
            ///'''''                sDateDebutSaison = "01/" & sMoisDebutSaison & "/" & (Year(Date))
            ///'''''                sDateFinSaison = DateAdd("yyyy", 1, CDate(sDateDebutSaison))
            ///'''''                sDateFinSaison = DateAdd("d", -1, CDate(sDateFinSaison))
            ///'''''        End If
            ///'''''
            ///'''''        fc_ChargeComboSem sDateDebutSaison, sDateFinSaison
            ///'''''        cmbSemaine.Text = ""
            ///'''''        cmbSemaine.Text = sNumSem
            ///'''''
            ///'''''        'fc_ChargeComboMois txtSaisonDu.Text, txtSaisonAu.Text
            ///'''''        'cmbMois.Text = ""
            ///'''''        'cmbMois.Text = sNumMois
            ///'''''
            ///'''''        GridInterventions.StyleSets("NOSENT").BackColor = &HC0C0FF
            ///'''''        GridInterventions.StyleSets("NOSENT").FORECOLOR = &H0&
            ///'''''
            ///'''''        GridInterventions.StyleSets("SENT").BackColor = &HC0FFC0
            ///'''''        GridInterventions.StyleSets("SENT").FORECOLOR = &H0&
            ///'''''
            ///'''''        ssPlanCharge.StyleSets("OK").BackColor = &HC0FFC0
            ///'''''        ssPlanCharge.StyleSets("OK").FORECOLOR = &H0&
            ///'''''
            ///'''''        ssPlanCharge.StyleSets("EXCES").BackColor = &HC0C0FF
            ///'''''        ssPlanCharge.StyleSets("EXCES").FORECOLOR = &H0&
            ///'''''
            ///'''''        ssPlanCharge.StyleSets("MANQUE").BackColor = &H80C0FF
            ///'''''        ssPlanCharge.StyleSets("MANQUE").FORECOLOR = &H0&
            ///'''''
            ///'''''
            ///'''''        SSTab1.TabVisible(3) = False
            ///'''''        SSTab1.TabsPerRow = 5
            ///'''''
            ///'''''
            //=== Charge l'échelle des calendriers.
            txtPeriode.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, PL1.Name + "Echelle", "AM");

            fc_Echelle(txtPeriode.Text, PL1);

            fc_Echelle(General.getFrmReg(ModConstantes.cUserInterP2V2, PL1.Name + "Echelle", "AM"), PL1);

            fc_LoadTailleEchelle(txtPeriode.Text, PL1);
            ///'''''
            ///'''''        fc_DropIntervenant
            ///'''''        fc_Tab
            ///'''''
            //==== renseigne les dates de debut et de fin de planning avec le mois en cours
            //        Dim dtDateDebPlaning As Date
            //        Dim dtDateFinPlaning As Date

            if (!General.IsDate(dtDateDebPlaning.Text))
            {
                dtDateDebPlaning.Text = "01/" + DateTime.Today.Month.ToString("00") + "/" + DateTime.Today.Year;
                //dtDateDebPlaningSimul = "01/" & Format(Month(Date), "00") & "/" & Year(Date)
            }

            if (!General.IsDate(dtDateFinPlaning.Text))
            {
                //dtDateFinPlaning = "01/" & Month(Date) & "/" & Year(Date)
                dtDateFinPlaning.Text = Convert.ToString(fDate.fc_FinDeMois(Convert.ToString(DateTime.Today.Month), Convert.ToString(DateTime.Today.Year)));
                // dtDateFinPlaningSimul = fc_FinDeMois(Month(Date), Year(Date))
            }
            ///'''''
            ///'''''        fc_GetParam
            ///'''''
            ///'''''
            ///'''''        txtCOP_NoAuto = GetSetting(cFrNomApp, Me.Name, "Newvar", "")
            ///'''''        txtCodeImmeuble = GetSetting(cFrNomApp, Me.Name, "CodeImmeuble", "")
            ///'''''        'ssMois = GetSetting(cFrNomApp, cUserInterP2V2, "ssMois", "")
            ///'''''        'txtannee = nz(GetSetting(cFrNomApp, cUserInterP2V2, "txtAnnee", ""), Year(Date))
            ///'''''        'ssMoisAu = GetSetting(cFrNomApp, cUserInterP2V2, "ssMoisAu", "")
            ///'''''        'txtAnneeAu = nz(GetSetting(cFrNomApp, cUserInterP2V2, "txtAnneeAu", ""), Year(Date))
            ///'''''        txtIntervenant = GetSetting(cFrNomApp, Me.Name, "txtIntervenant", "")
            ///'''''        lTab = nz(GetSetting(cFrNomApp, Me.Name, "Onglet", 0), 0)
            ///'''''
            ///'''''        txtMoisPlanif.Text = GetSetting(cFrNomApp, Me.Name, "MoisPlanif", Format(Date, "mm/yyyy"))
            ///'''''        txtMoisPlanif_Validate False
            ///'''''
            ///'''''        SSTab1.Tab = lTab
            ///'''''
            ///'''''        'fc_formatGrille ssGridDetailVisite, False
            ///'''''        fc_formatGrille ssGridVisite, False
            ///'''''
            ///'''''        'fc_CheckVerrou
            ///'''''
            ///'''''        For Each objcontrole In Me.Controls
            ///'''''            If TypeOf objcontrole Is SSOleDBGrid Then
            ///'''''               fc_LoadDimensionGrille Me.Name, objcontrole
            ///'''''            End If
            ///'''''        Next

            // fc_dropStatut

            if (General.sGMAOcriterePartImmeuble == "1")
            {
                frame1112.Visible = true;
                frame1113.Visible = true;
                //===> Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021
                Opt_6.Checked = true;
                //===> Fin Modif Mondir
            }
            else
            {
                frame1112.Visible = false;
                frame1113.Visible = false;
            }

            //===> Mondir le 02.02.2021, fill datatable of Gridimm with columns
            var GridimmDataTable = new DataTable();
            GridimmDataTable.Columns.Add("Codeimmeuble");
            GridimmDataTable.Columns.Add("JanvDuree");
            GridimmDataTable.Columns.Add("FevDuree");
            GridimmDataTable.Columns.Add("MarsDuree");
            GridimmDataTable.Columns.Add("AvrilDuree");
            GridimmDataTable.Columns.Add("MaiDuree");
            GridimmDataTable.Columns.Add("JuinDuree");
            GridimmDataTable.Columns.Add("JuilletDuree");
            GridimmDataTable.Columns.Add("AoutDuree");
            GridimmDataTable.Columns.Add("SepDuree");
            GridimmDataTable.Columns.Add("OctDuree");
            GridimmDataTable.Columns.Add("NovDuree");
            GridimmDataTable.Columns.Add("DecDuree");
            Gridimm.DataSource = GridimmDataTable;
            //===> FIn Modif Mondir
        }
        /// <summary>
        /// tested
        /// </summary>
        private void FC_SaveParam()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"FC_SaveParam() - Enter In The Function");
            //===> Fin Modif Mondir
            //===Onglet Simulation.
            General.saveInReg(ModConstantes.cUserInterP2V2, txtCodeImmeubleSimul.Name, txtCodeImmeubleSimul.Text);

            if (((dtDateDebPlaningBisSimul.ButtonsLeft[0]) as StateEditorButton).Checked)
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "dtDateDebPlaningBisSimul", General.nz((dtDateDebPlaningBisSimul.Value), "null").ToString());

            }
            else
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "dtDateDebPlaningBisSimul", General.nz((dtDateDebPlaningBisSimul.NullText), "null").ToString());

            }
            if (((dtDateFinPlaningBisSimul.ButtonsLeft[0]) as StateEditorButton).Checked)
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "dtDateFinPlaningBisSimul", General.nz((dtDateFinPlaningBisSimul.Value), "null").ToString());
            }
            else
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "dtDateFinPlaningBisSimul", General.nz((dtDateFinPlaningBisSimul.NullText), "null").ToString());
            }

            General.saveInReg(ModConstantes.cUserInterP2V2, txtIntervenantSimul.Name, txtIntervenantSimul.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, txtCOP_AvenantSimul.Name, txtCOP_AvenantSimul.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, txtCOP_NoContratSimul.Name, txtCOP_NoContratSimul.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, txtCAI_CodeSimul.Name, txtCAI_CodeSimul.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, txtPrestationSimul.Name, txtPrestationSimul.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, chkPEI_APlanifier.Name, Convert.ToString(chkPEI_APlanifier.CheckState));
            //SaveSetting cFrNomApp, Me.Name, chkEQU_P1CompteurSimul.Name, chkEQU_P1CompteurSimul.value
            General.saveInReg(ModConstantes.cUserInterP2V2, optEQU_P1CompteurSimul0.Name, Convert.ToString(optEQU_P1CompteurSimul0.Checked));
            General.saveInReg(ModConstantes.cUserInterP2V2, optEQU_P1CompteurSimul1.Name, Convert.ToString(optEQU_P1CompteurSimul1.Checked));
            General.saveInReg(ModConstantes.cUserInterP2V2, optEQU_P1CompteurSimul2.Name, Convert.ToString(optEQU_P1CompteurSimul2.Checked));
            General.saveInReg(ModConstantes.cUserInterP2V2, txtCOP_NoautoSimul.Name, txtCOP_NoautoSimul.Text);



            //Onglet consultation.
            //SaveSetting cFrNomApp, Me.Name, txtCodeImmeubleConsult.Name, txtCodeImmeubleConsult.Text
            //SaveSetting cFrNomApp, Me.Name, dtDateDebPlaningConsult.Name, dtDateDebPlaningConsult.Text
            //SaveSetting cFrNomApp, Me.Name, dtDateFinPlaningConsul.Name, dtDateFinPlaningConsul.Text
            //SaveSetting cFrNomApp, Me.Name, txtintervenantConsult.Name, txtintervenantConsult.Text
            //SaveSetting cFrNomApp, Me.Name, txtCOP_AvenantConsult.Name, txtCOP_AvenantConsult.Text
            //SaveSetting cFrNomApp, Me.Name, txtCOP_NoContratConsult.Name, txtCOP_NoContratConsult.Text
            //SaveSetting cFrNomApp, Me.Name, txtCAI_CodeConsult.Name, txtCAI_CodeConsult.Text
            //SaveSetting cFrNomApp, Me.Name, txtPrestationConsult.Name, txtPrestationConsult.Text
            //SaveSetting cFrNomApp, Me.Name, txtCodeEtatConsult.Name, txtCodeEtatConsult.Text


            //====Onglet Planification/ transfert des insterventions..
            General.saveInReg(ModConstantes.cUserInterP2V2, txtMoisPlanif.Name, txtMoisPlanif.Text);

            General.saveInReg(ModConstantes.cUserInterP2V2, txtIntervSecteur.Name, txtIntervSecteur.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, lblLibIntervSecteur.Name, lblLibIntervSecteur.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, txtIntervenant.Name, txtIntervenant.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, lbllibIntervenant.Name, lbllibIntervenant.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, txtCodeImmeuble.Name, txtCodeImmeuble.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, txtCOP_NoAuto.Name, txtCOP_NoAuto.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, txtCOP_NoContrat.Name, txtCOP_NoContrat.Text);
            //General.saveInReg(ModConstantes.cUserInterP2V2, txtCOP_Avenant.Name, txtCOP_Avenant.Text); TODO txtCOP_Avenant.Text not found in vb
            General.saveInReg(ModConstantes.cUserInterP2V2, cmbStatutPDA.Name, cmbStatutPDA.Text);
            //SaveSetting cFrNomApp, Me.Name, chkEQU_P1Compteur.Name, nz(chkEQU_P1Compteur.value, 0)
            General.saveInReg(ModConstantes.cUserInterP2V2, OptEQU_P1Compteur0.Name, Convert.ToString(OptEQU_P1Compteur0.Checked));
            General.saveInReg(ModConstantes.cUserInterP2V2, OptEQU_P1Compteur1.Name, Convert.ToString(OptEQU_P1Compteur1.Checked));
            General.saveInReg(ModConstantes.cUserInterP2V2, OptEQU_P1Compteur2.Name, Convert.ToString(OptEQU_P1Compteur2.Checked));
            General.saveInReg(ModConstantes.cUserInterP2V2, txtCOP_NoAuto.Name, txtCOP_NoAuto.Text);


            //Onglet Avis de passage
            General.saveInReg(ModConstantes.cUserInterP2V2, txtCOP_NoAutoAvis.Name, txtCOP_NoAuto.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, txtCodeImmeubleAvis.Name, txtCodeImmeubleAvis.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, dtDateDebPlaningAvis.Name, dtDateDebPlaningAvis.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, dtDateFinPlaningAvis.Name, dtDateFinPlaningAvis.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, txtIntervenantAvis.Name, txtIntervenantAvis.Text);

            //===Onglet histo
            if (((dtDateVisiteLe.ButtonsLeft[0]) as StateEditorButton).Checked)
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "dtDateVisiteLe", General.nz((dtDateVisiteLe.Value), "null").ToString());
            }
            else
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "dtDateVisiteLe", General.nz((dtDateVisiteLe.NullText), "null").ToString());
            }

            if (((dtDateVisiteAu.ButtonsLeft[0]) as StateEditorButton).Checked)
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "dtDateVisiteAu", General.nz((dtDateVisiteAu.Value), "null").ToString());
            }
            else
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "dtDateVisiteAu", General.nz((dtDateVisiteAu.NullText), "null").ToString());
            }

            if (((DTDatePrevueDe.ButtonsLeft[0]) as StateEditorButton).Checked)
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "DTDatePrevueDe", General.nz((DTDatePrevueDe.Value), "null").ToString());

            }
            else
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "DTDatePrevueDe", General.nz((DTDatePrevueDe.NullText), "null").ToString());

            }
            if (((DTDatePrevueDe.ButtonsLeft[0]) as StateEditorButton).Checked)
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "DTDatePrevueDe", General.nz((DTDatePrevueDe.Value), "null").ToString());

            }
            else
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "DTDatePrevueDe", General.nz((DTDatePrevueDe.NullText), "null").ToString());

            }
            if (((DTDatePrevueAu.ButtonsLeft[0]) as StateEditorButton).Checked)
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "DTDatePrevueAu", General.nz((DTDatePrevueAu.Value), "null").ToString());

            }
            else
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "DTDatePrevueAu", General.nz((DTDatePrevueAu.NullText), "null").ToString());

            }

            if (((DTDateRealiseDe.ButtonsLeft[0]) as StateEditorButton).Checked)
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "DTDateRealiseDe", General.nz((DTDateRealiseDe.Value), "null").ToString());

            }
            else
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "DTDateRealiseDe", General.nz((DTDateRealiseDe.NullText), "null").ToString());

            }



            if (((DTDateRealiseAu.ButtonsLeft[0]) as StateEditorButton).Checked)
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "DTDateRealiseAu", General.nz((DTDateRealiseAu.Value), "null").ToString());

            }
            else
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "DTDateRealiseAu", General.nz((DTDateRealiseAu.NullText), "null").ToString());

            }

            if (((DTCreeParDe.ButtonsLeft[0]) as StateEditorButton).Checked)
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "DTCreeParDe", General.nz((DTCreeParDe.Value), "null").ToString());

            }
            else
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "DTCreeParDe", General.nz((DTCreeParDe.NullText), "null").ToString());

            }

            if (((dtCreeParAu.ButtonsLeft[0]) as StateEditorButton).Checked)
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "dtCreeParAu", General.nz((dtCreeParAu.Value), "null").ToString());

            }
            else
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "dtCreeParAu", General.nz((dtCreeParAu.NullText), "null").ToString());

            }

            if (((dtEnvoyeLeHisto1.ButtonsLeft[0]) as StateEditorButton).Checked)
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "dtEnvoyeLeHisto1", General.nz((dtEnvoyeLeHisto1.Value), "null").ToString());

            }
            else
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "dtEnvoyeLeHisto1", General.nz((dtEnvoyeLeHisto1.NullText), "null").ToString());

            }

            if (((dtEnvoyeLeHisto2.ButtonsLeft[0]) as StateEditorButton).Checked)
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "dtEnvoyeLeHisto2", General.nz((dtEnvoyeLeHisto2.Value), "null").ToString());

            }
            else
            {
                General.saveInReg(ModConstantes.cUserInterP2V2, "dtEnvoyeLeHisto2", General.nz((dtEnvoyeLeHisto2.NullText), "null").ToString());

            }

            General.saveInReg(ModConstantes.cUserInterP2V2, "chkInterIncompléte", Convert.ToString(chkInterIncompléte.CheckState));
            General.saveInReg(ModConstantes.cUserInterP2V2, "chkCommGardien", Convert.ToString(chkCommGardien.CheckState));
            General.saveInReg(ModConstantes.cUserInterP2V2, "chkAnomalie", Convert.ToString(chkAnomalie.CheckState));
            General.saveInReg(ModConstantes.cUserInterP2V2, "chkTrxUrgent", Convert.ToString(chkTrxUrgent.CheckState));
            General.saveInReg(ModConstantes.cUserInterP2V2, "chkMatRemplace", Convert.ToString(chkMatRemplace.CheckState));
            General.saveInReg(ModConstantes.cUserInterP2V2, "chkDevisEtablir", Convert.ToString(chkDevisEtablir.CheckState));

            // General.saveInReg(ModConstantes.cUserInterP2V2, "cmBUo", cmBUo.Text);todo not found in vb

            General.saveInReg(ModConstantes.cUserInterP2V2, "txtStatutHisto", txtStatutHisto.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, "txtNoInterventionHisto", txtNoInterventionHisto.Text);

            General.saveInReg(ModConstantes.cUserInterP2V2, "txtCodeImmeubleHisto", txtCodeImmeubleHisto.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, "txtMatEntretien", txtMatEntretien.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, "lblLibEntretien", lblLibEntretien.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, "txtIntervenantHisto", txtIntervenantHisto.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, "lbllibIntervenantHisto", lbllibIntervenantHisto.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, "txtCode1Histo", txtCode1Histo.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, "cmbStatutPDAhisto", cmbStatutPDAhisto.Text);
            General.saveInReg(ModConstantes.cUserInterP2V2, OptEQU_P1CompteurHisto0.Name, Convert.ToString(OptEQU_P1CompteurHisto0.Checked));
            General.saveInReg(ModConstantes.cUserInterP2V2, OptEQU_P1CompteurHisto1.Name, Convert.ToString(OptEQU_P1CompteurHisto1.Checked));
            General.saveInReg(ModConstantes.cUserInterP2V2, OptEQU_P1CompteurHisto2.Name, Convert.ToString(OptEQU_P1CompteurHisto2.Checked));


        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_GetParam()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_GetParam() - Enter In The Function");
            //===> Fin Modif Mondir
            //=== onglet simulation.
            txtCodeImmeubleSimul.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtCodeImmeubleSimul", "");

            if (General.UCase(General.getFrmReg(ModConstantes.cUserInterP2V2, "dtDateDebPlaningBisSimul", "Null")) == General.UCase("Null"))
            {

                dtDateDebPlaningBisSimul.Value = System.DBNull.Value;
                ((dtDateDebPlaningBisSimul.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            }
            else
            {
                dtDateDebPlaningBisSimul.Value = General.getFrmReg(ModConstantes.cUserInterP2V2, "dtDateDebPlaningBisSimul", Convert.ToString(DateTime.Today));
                ((dtDateDebPlaningBisSimul.ButtonsLeft[0]) as StateEditorButton).Checked = true;
            }

            if (General.UCase(General.getFrmReg(ModConstantes.cUserInterP2V2, "dtDateFinPlaningBisSimul", "Null")) == General.UCase("Null"))
            {

                dtDateFinPlaningBisSimul.Value = System.DBNull.Value;
                ((dtDateFinPlaningBisSimul.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            }
            else
            {
                dtDateFinPlaningBisSimul.Value = General.getFrmReg(ModConstantes.cUserInterP2V2, "dtDateFinPlaningBisSimul", Convert.ToString(DateTime.Today));
                ((dtDateFinPlaningBisSimul.ButtonsLeft[0]) as StateEditorButton).Checked = true;
            }


            txtCOP_NoautoSimul.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtCOP_NoautoSimul", "");

            txtIntervenantSimul.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtIntervenantSimul", "");
            txtCOP_NoContratSimul.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtCOP_NoContratSimul", "");
            txtCOP_AvenantSimul.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtCOP_AvenantSimul", "");
            txtCAI_CodeSimul.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtCAI_CodeSimul", "");
            txtPrestationSimul.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtPrestationSimul", "");
            chkPEI_APlanifier.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "chkPEI_APlanifier", Convert.ToString(0)).ToString() == "1";
            //chkEQU_P1CompteurSimul.value = GetSetting(cFrNomApp, Me.Name, "chkEQU_P1CompteurSimul", 0)
            optEQU_P1CompteurSimul0.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "optEQU_P1CompteurSimul0", Convert.ToString(false)).ToString() == "Vrai";
            optEQU_P1CompteurSimul1.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "optEQU_P1CompteurSimul1", Convert.ToString(false)).ToString() == "Vrai";
            optEQU_P1CompteurSimul2.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "optEQU_P1CompteurSimul2", Convert.ToString(false)).ToString() == "Vrai";
            if (optEQU_P1CompteurSimul0.Checked == false && optEQU_P1CompteurSimul1.Checked == false && optEQU_P1CompteurSimul2.Checked == false)
            {
                optEQU_P1CompteurSimul2.Checked = true;
            }


            //=== onglet consultation.
            //txtCodeImmeubleConsult = GetSetting(cFrNomApp, Me.Name, "txtCodeImmeubleConsult", "")
            //dtDateDebPlaningConsult = GetSetting(cFrNomApp, Me.Name, "dtDateDebPlaningConsult", Date)
            //dtDateDebPlaningBisConsult = dtDateDebPlaningConsult
            //dtDateFinPlaningConsul = GetSetting(cFrNomApp, Me.Name, "dtDateFinPlaningConsul", Date)
            //dtDateFinPlaningBisConsult = dtDateFinPlaningConsul
            //txtintervenantConsult = GetSetting(cFrNomApp, Me.Name, "txtintervenantConsult", "")
            //txtCOP_NoContratConsult = GetSetting(cFrNomApp, Me.Name, "txtCOP_NoContratConsult", "")
            //txtCOP_AvenantConsult = GetSetting(cFrNomApp, Me.Name, "txtCOP_AvenantConsult", "")
            //txtCAI_CodeConsult = GetSetting(cFrNomApp, Me.Name, "txtCAI_CodeConsult", "")
            //txtPrestationConsult = GetSetting(cFrNomApp, Me.Name, "txtPrestationConsult", "")
            //txtCodeEtatConsult = GetSetting(cFrNomApp, Me.Name, "txtCodeEtatConsult", "")


            //==== onglet creation/ transfert visites
            txtMoisPlanif.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtMoisPlanif", "");

            txtMoisPlanif_Validated(txtMoisPlanif, new System.ComponentModel.CancelEventArgs(false));

            txtIntervSecteur.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtIntervSecteur", "");
            lblLibIntervSecteur.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "lblLibIntervSecteur", "");
            txtIntervenant.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtIntervenant", "");
            lbllibIntervenant.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "lbllibIntervenant", "");
            txtCodeImmeuble.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtCodeImmeuble", "");
            txtCOP_NoAuto.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtCOP_NoAuto", "");
            txtCOP_NoContrat.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtCOP_NoContrat", "");
            // txtCOP_Avenant.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtCOP_Avenant", "");not found vb
            cmbStatutPDA.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "cmbStatutPDA", "");
            //chkEQU_P1Compteur.value = GetSetting(cFrNomApp, Me.Name, "chkEQU_P1Compteur", 0)
            OptEQU_P1Compteur0.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "OptEQU_P1Compteur0", Convert.ToString(false)).ToString() == "Vrai";
            OptEQU_P1Compteur1.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "OptEQU_P1Compteur1", Convert.ToString(false)).ToString() == "Vrai";
            OptEQU_P1Compteur2.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "OptEQU_P1Compteur2", Convert.ToString(false)).ToString() == "Vrai";
            if (OptEQU_P1Compteur0.Checked == false && OptEQU_P1Compteur1.Checked == false && OptEQU_P1Compteur2.Checked == false)
            {
                OptEQU_P1Compteur1.Checked = true;
            }
            txtCOP_NoAuto.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtCOP_NoAuto", "");


            //==== onglet historique.

            if (General.UCase(General.getFrmReg(ModConstantes.cUserInterP2V2, "DTDatePrevueDe", "Null")) == General.UCase("Null"))
            {

                DTDatePrevueDe.Value = DTDatePrevueDe.NullText;
                ((DTDatePrevueDe.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            }
            else
            {
                DTDatePrevueDe.Value = General.getFrmReg(ModConstantes.cUserInterP2V2, "DTDatePrevueDe", Convert.ToString(DateTime.Today));
                ((DTDatePrevueDe.ButtonsLeft[0]) as StateEditorButton).Checked = true;
            }


            if (General.UCase(General.getFrmReg(ModConstantes.cUserInterP2V2, "dtDateVisiteLe", "Null")) == General.UCase("Null"))
            {

                dtDateVisiteLe.Value = dtDateVisiteLe.NullText;
                ((dtDateVisiteLe.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            }
            else
            {
                dtDateVisiteLe.Value = General.getFrmReg(ModConstantes.cUserInterP2V2, "dtDateVisiteLe", Convert.ToString(DateTime.Today));
                ((dtDateVisiteLe.ButtonsLeft[0]) as StateEditorButton).Checked = true;
            }

            if (General.UCase(General.getFrmReg(ModConstantes.cUserInterP2V2, "DTDatePrevueAu", "Null")) == General.UCase("Null"))
            {

                DTDatePrevueAu.Value = DTDatePrevueAu.NullText;
                ((DTDatePrevueAu.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            }
            else
            {
                DTDatePrevueAu.Value = General.getFrmReg(ModConstantes.cUserInterP2V2, "DTDatePrevueAu", Convert.ToString(DateTime.Today));
                ((DTDatePrevueAu.ButtonsLeft[0]) as StateEditorButton).Checked = true;
            }

            if (General.UCase(General.getFrmReg(ModConstantes.cUserInterP2V2, "dtDateVisiteAu", "Null")) == General.UCase("Null"))
            {

                dtDateVisiteAu.Value = dtDateVisiteAu.NullText;
                ((dtDateVisiteAu.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            }
            else
            {
                dtDateVisiteAu.Value = General.getFrmReg(ModConstantes.cUserInterP2V2, "dtDateVisiteAu", Convert.ToString(DateTime.Today));
                ((dtDateVisiteAu.ButtonsLeft[0]) as StateEditorButton).Checked = true;
            }

            if (General.UCase(General.getFrmReg(ModConstantes.cUserInterP2V2, "DTDateRealiseDe", "Null")) == General.UCase("Null"))
            {

                DTDateRealiseDe.Value = DTDateRealiseDe.NullText;
                ((DTDateRealiseDe.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            }
            else
            {
                DTDateRealiseDe.Value = General.getFrmReg(ModConstantes.cUserInterP2V2, "DTDateRealiseDe", Convert.ToString(DateTime.Today));
                ((DTDateRealiseDe.ButtonsLeft[0]) as StateEditorButton).Checked = true;
            }

            if (General.UCase(General.getFrmReg(ModConstantes.cUserInterP2V2, "DTDateRealiseAu", "Null")) == General.UCase("Null"))
            {

                DTDateRealiseAu.Value = DTDateRealiseAu.NullText;
                ((DTDateRealiseAu.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            }
            else
            {
                DTDateRealiseAu.Value = General.getFrmReg(ModConstantes.cUserInterP2V2, "DTDateRealiseAu", Convert.ToString(DateTime.Today));
                ((DTDateRealiseAu.ButtonsLeft[0]) as StateEditorButton).Checked = true;
            }

            if (General.UCase(General.getFrmReg(ModConstantes.cUserInterP2V2, "DTCreeParDe", "Null")) == General.UCase("Null"))
            {

                DTCreeParDe.Value = DTCreeParDe.NullText;
                ((DTCreeParDe.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            }
            else
            {
                DTCreeParDe.Value = General.getFrmReg(ModConstantes.cUserInterP2V2, "DTCreeParDe", Convert.ToString(DateTime.Today));
                ((DTCreeParDe.ButtonsLeft[0]) as StateEditorButton).Checked = true;
            }

            if (General.UCase(General.getFrmReg(ModConstantes.cUserInterP2V2, "dtCreeParAu", "Null")) == General.UCase("Null"))
            {

                dtCreeParAu.Value = dtCreeParAu.NullText;
                ((dtCreeParAu.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            }
            else
            {
                dtCreeParAu.Value = General.getFrmReg(ModConstantes.cUserInterP2V2, "dtCreeParAu", Convert.ToString(DateTime.Today));
                ((dtCreeParAu.ButtonsLeft[0]) as StateEditorButton).Checked = true;
            }

            if (General.UCase(General.getFrmReg(ModConstantes.cUserInterP2V2, "dtEnvoyeLeHisto1", "Null")) == General.UCase("Null"))
            {

                dtEnvoyeLeHisto1.Value = dtEnvoyeLeHisto1.NullText;
                ((dtEnvoyeLeHisto1.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            }
            else
            {
                dtEnvoyeLeHisto1.Value = General.getFrmReg(ModConstantes.cUserInterP2V2, "dtEnvoyeLeHisto1", Convert.ToString(DateTime.Today));
                ((dtEnvoyeLeHisto1.ButtonsLeft[0]) as StateEditorButton).Checked = true;
            }

            if (General.UCase(General.getFrmReg(ModConstantes.cUserInterP2V2, "dtEnvoyeLeHisto2", "Null")) == General.UCase("Null"))
            {

                dtEnvoyeLeHisto2.Value = dtEnvoyeLeHisto2.NullText;
                ((dtEnvoyeLeHisto2.ButtonsLeft[0]) as StateEditorButton).Checked = false;
            }
            else
            {
                dtEnvoyeLeHisto2.Value = General.getFrmReg(ModConstantes.cUserInterP2V2, "dtEnvoyeLeHisto2", Convert.ToString(DateTime.Today));
                ((dtEnvoyeLeHisto2.ButtonsLeft[0]) as StateEditorButton).Checked = true;
            }

            chkInterIncompléte.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "chkInterIncompléte", Convert.ToString(0)).ToString() == "1";
            chkCommGardien.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "chkCommGardien", Convert.ToString(0)).ToString() == "1";
            chkAnomalie.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "chkAnomalie", Convert.ToString(0)).ToString() == "1";
            chkTrxUrgent.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "chkTrxUrgent", Convert.ToString(0)).ToString() == "1";
            chkMatRemplace.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "chkMatRemplace", Convert.ToString(0)).ToString() == "1";
            chkDevisEtablir.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "chkDevisEtablir", Convert.ToString(0)).ToString() == "1";

            // cmBUo.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "cmBUo", "");not found

            txtStatutHisto.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtStatutHisto", "");
            txtNoInterventionHisto.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtNoInterventionHisto", "");
            txtMatEntretien.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtMatEntretien", "");
            lblLibEntretien.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "lblLibEntretien", "");
            txtIntervenantHisto.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtIntervenantHisto", "");
            lbllibIntervenantHisto.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "lbllibIntervenantHisto", "");
            txtCode1Histo.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtCode1Histo", "");
            cmbStatutPDAhisto.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "cmbStatutPDAhisto", "");

            txtCodeImmeubleHisto.Text = General.getFrmReg(ModConstantes.cUserInterP2V2, "txtCodeImmeubleHisto", "");

            OptEQU_P1CompteurHisto0.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "OptEQU_P1CompteurHisto0", Convert.ToString(false)).ToString() == "Vrai";
            OptEQU_P1CompteurHisto1.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "OptEQU_P1CompteurHisto1", Convert.ToString(false)).ToString() == "Vrai";
            OptEQU_P1CompteurHisto2.Checked = General.getFrmReg(ModConstantes.cUserInterP2V2, "OptEQU_P1CompteurHisto2", Convert.ToString(false)).ToString() == "Vrai";
            if (OptEQU_P1CompteurHisto0.Checked == false && OptEQU_P1CompteurHisto1.Checked == false && OptEQU_P1CompteurHisto2.Checked == false)
            {
                OptEQU_P1CompteurHisto2.Checked = true;
            }

        }

        private void ssGridVisite_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridVisite_BeforeRowsDeleted() - Enter In The Function");
            //===> Fin Modif Mondir
            e.Cancel = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridVisite_AfterRowActivate(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridVisite_AfterRowActivate() - Enter In The Function");
            //===> Fin Modif Mondir
            ssGridTachesAnnuel.UpdateData();
            if (ssGridVisite.ActiveRow != null)
            {
                fc_ssGRIDTachesAnnuel(Convert.ToInt32(General.nz(ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value, 0).ToString()));
            }
        }

        private void optOperation_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"optOperation_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            if (optOperation.Checked)
            {
                if (ssGridVisite.ActiveRow != null)
                    fc_ssGRIDTachesAnnuel(Convert.ToInt32(General.nz(ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value, 0).ToString()));
            }
        }

        private void optTaches_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"optTaches_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            if (optTaches.Checked)
            {
                if (ssGridVisite.ActiveRow != null)
                    fc_ssGRIDTachesAnnuel(Convert.ToInt32(General.nz(ssGridVisite.ActiveRow.Cells["NoIntervention"].Value, 0).ToString()));
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridVisiteSimul_AfterRowActivate(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridVisiteSimul_AfterRowActivate() - Enter In The Function");
            //===> Fin Modif Mondir
            if (ssGridVisiteSimul.ActiveRow != null)
                fc_ssGRIDTachesAnnuelSimul(Convert.ToInt32(General.nz(ssGridVisiteSimul.ActiveRow.Cells["NoIntervention"].Value, 0).ToString()));
        }
        private void txtCAI_CodeSimul_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtCAI_CodeSimul_KeyPress() - Enter In The Function - e.KeyChar = {(short)(e.KeyChar)}");
            //===> Fin Modif Mondir
            short KeyAscii = (short)(e.KeyChar);

            if (KeyAscii == 13)
            {
                cmdMotifSimul_Click(cmdMotifSimul, new System.EventArgs());
                KeyAscii = 0;
            }


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCode1Histo_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtCode1Histo_KeyPress() - Enter In The Function - e.KeyChar = {(short)(e.KeyChar)}");
            //===> Fin Modif Mondir
            short KeyAscii = (short)(e.KeyChar);

            if (KeyAscii == 13)
            {
                cmdClient_Click(cmdClient, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeImmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtCodeImmeuble_KeyPress() - Enter In The Function - e.KeyChar = {(short)(e.KeyChar)}");
            //===> Fin Modif Mondir
            short KeyAscii = (short)(e.KeyChar);

            if (KeyAscii == 13)
            {
                cmdRechercheImmeuble_Click(cmdRechercheImmeuble, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeImmeubleHisto_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtCodeImmeubleHisto_KeyPress() - Enter In The Function - e.KeyChar = {(short)(e.KeyChar)}");
            //===> Fin Modif Mondir
            short KeyAscii = (short)(e.KeyChar);

            if (KeyAscii == 13)
            {
                cmdImmeubleHisto_Click(cmdImmeubleHisto, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeImmeubleSimul_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtCodeImmeubleSimul_KeyPress() - Enter In The Function - e.KeyChar = {(short)(e.KeyChar)}");
            //===> Fin Modif Mondir
            short KeyAscii = (short)(e.KeyChar);

            if (KeyAscii == 13)
            {
                cmdRechercheImmeubleSimul_Click(cmdRechercheImmeubleSimul, new System.EventArgs());
                KeyAscii = 0;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIntervenant_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtIntervenant_KeyPress() - Enter In The Function - e.KeyChar = {(short)(e.KeyChar)}");
            //===> Fin Modif Mondir
            short KeyAscii = (short)(e.KeyChar);

            if (KeyAscii == 13)
            {
                cmdintervenant_Click(cmdintervenant, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIntervenant_Validated(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtIntervenant_Validated() - Enter In The Function");
            //===> Fin Modif Mondir
            fc_libCreatIntervenant();

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_libCreatIntervenant()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_libCreatIntervenant() - Enter In The Function");
            //===> Fin Modif Mondir
            string sSQL = null;

            sSQL = "SELECT Nom, Prenom From Personnel WHERE Matricule = '" + StdSQLchaine.gFr_DoublerQuote(txtIntervenant.Text) + "'";
            using (var tmpAdo = new ModAdo())
            {
                sSQL = tmpAdo.fc_ADOlibelle(sSQL, true);
            }
            lbllibIntervenant.Text = sSQL;

            return;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIntervenantHisto_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtIntervenantHisto_KeyPress() - Enter In The Function - e.KeyChar = {(short)(e.KeyChar)}");
            //===> Fin Modif Mondir
            short KeyAscii = (short)(e.KeyChar);

            if (KeyAscii == 13)
            {
                cmdIntervHisto_Click(cmdIntervHisto, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIntervenantHisto_Validated(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtIntervenantHisto_Validated() - Enter In The Function");
            //===> Fin Modif Mondir
            fc_libAgentRamoHisto();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_libAgentRamoHisto()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_libAgentRamoHisto() - Enter In The Function");
            //===> Fin Modif Mondir
            string sSQL = null;

            sSQL = "SELECT Nom, Prenom From Personnel WHERE Matricule = '" + StdSQLchaine.gFr_DoublerQuote(txtIntervenantHisto.Text) + "'";
            using (var tmpAdo = new ModAdo())
            {
                sSQL = tmpAdo.fc_ADOlibelle(sSQL, true);
            }
            lbllibIntervenantHisto.Text = sSQL;
        }

        private void txtIntervenantSimul_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtIntervenantSimul_KeyPress() - Enter In The Functio - e.KeyChar = {(short)(e.KeyChar)}");
            //===> Fin Modif Mondir
            short KeyAscii = (short)(e.KeyChar);

            if (KeyAscii == 13)
            {
                cmdFindStraitantSimul_Click(cmdFindStraitantSimul, new System.EventArgs());
                KeyAscii = 0;
            }
        }

        private void txtIntervenantSimul_Validated(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtIntervenantSimul_Validated() - Enter In The Function");
            //===> Fin Modif Mondir
            fc_LibintervenantSimul();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LibintervenantSimul()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_LibintervenantSimul() - Enter In The Function");
            //===> Fin Modif Mondir
            string sSQL = null;


            sSQL = "SELECT nom, prenom from personnel where matricule ='" + StdSQLchaine.gFr_DoublerQuote(txtIntervenantSimul.Text) + "'";
            using (var tmpAdo = new ModAdo())
            {
                lbllibIntervenantSimul.Text = tmpAdo.fc_ADOlibelle(sSQL, true);
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMatEntretien_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtMatEntretien_KeyPress() - Enter In The Function - e.KeyChar = {(short)(e.KeyChar)}");
            //===> Fin Modif Mondir
            short KeyAscii = (short)(e.KeyChar);

            if (KeyAscii == 13)
            {
                cmdMatEntretien_Click(cmdMatEntretien, new System.EventArgs());
                // KeyAscii = 0;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMatEntretien_Validated(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtMatEntretien_Validated() - Enter In The Function");
            //===> Fin Modif Mondir
            fc_libAgentSecteurHisto();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_libAgentSecteurHisto()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_libAgentSecteurHisto() - Enter In The Function");
            //===> Fin Modif Mondir
            string sSQL = null;

            sSQL = "SELECT Nom, Prenom From Personnel WHERE Matricule = '" + StdSQLchaine.gFr_DoublerQuote(txtMatEntretien.Text) + "'";
            using (var tmpAdo = new ModAdo())
            {
                sSQL = tmpAdo.fc_ADOlibelle(sSQL, true);
            }
            lblLibEntretien.Text = sSQL;


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMoisPlanif_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtMoisPlanif_KeyPress() - Enter In The Function - e.KeyChar = {(short)(e.KeyChar)}");
            //===> Fin Modif Mondir
            short KeyAscii = (short)(e.KeyChar);

            if (KeyAscii == 13)
            {
                txtMoisPlanif_Validated(txtMoisPlanif, new System.ComponentModel.CancelEventArgs(false));
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtMoisPlanif_Validated(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtMoisPlanif_Validated() - Enter In The Function");
            //===> Fin Modif Mondir
            if (txtMoisPlanif.Text.Contains("/"))
            {
                txtMoisPlanif.Text = fDate.fc_ValideDate("01/" + txtMoisPlanif.Text);
            }
            else
            {
                if (!string.IsNullOrEmpty(txtMoisPlanif.Text))
                {
                    txtMoisPlanif.Text = fDate.fc_ValideDate("01" + txtMoisPlanif.Text);
                }
                else
                {
                    txtMoisPlanif.Text = fDate.fc_ValideDate(Convert.ToString(DateTime.Today));
                }
            }

            if (General.IsDate(txtMoisPlanif.Text))
            {

                dtDateFinPlaning.Text = fDate.fc_FinDeMois(Convert.ToDateTime(txtMoisPlanif.Text).ToString("MM"), Convert.ToDateTime(txtMoisPlanif.Text).ToString("yyyy")).ToString("dd/MM/yyyy");
                dtDateFinPlaningBis.Value = fDate.fc_FinDeMois(Convert.ToDateTime(txtMoisPlanif.Text).ToString("MM"), Convert.ToDateTime(txtMoisPlanif.Text).ToString("yyyy")).ToString("dd/MM/yyyy");
                txtMoisPlanif.Text = Convert.ToDateTime(txtMoisPlanif.Text).ToString("MM/yyyy");
                dtDateDebPlaning.Text = "01/" + txtMoisPlanif.Text;
                dtDateDebPlaningBis.Value = Convert.ToDateTime("01/" + txtMoisPlanif.Text);
            }
        }
        private void fc_Echelle(string sPeriode, AxPLANNINGLib.AxPlanning PL)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_Echelle() - Enter In The Function - sPeriode = {sPeriode}");
            //===> Fin Modif Mondir
            PL.PlanningCtrlRefresh = false;



            if (General.UCase("AM") == General.UCase(sPeriode))
            {

                if (PL.FirstScaleMajorType == Calendrier.T_DAY && PL.FirstScaleMinorType == Calendrier.T_HOUR)
                {
                    PL.FirstScaleMajorType = Calendrier.T_WEEK;
                    PL.FirstScaleMinorType = Calendrier.T_DAY;
                }

                if (PL.FirstScaleMajorType == Calendrier.T_WEEK && PL.FirstScaleMinorType == Calendrier.T_DAY)
                {
                    PL.FirstScaleMajorType = Calendrier.T_MONTH;
                    PL.FirstScaleMinorType = Calendrier.T_DAY;
                }

                if (PL.FirstScaleMajorType == Calendrier.T_MONTH && PL.FirstScaleMinorType == Calendrier.T_DAY)
                {
                    PL.FirstScaleMajorType = Calendrier.T_MONTH;
                    PL.FirstScaleMinorType = Calendrier.T_WEEK;
                }

                if (PL.FirstScaleMajorType == Calendrier.T_MONTH && PL.FirstScaleMinorType == Calendrier.T_WEEK)
                {
                    PL.FirstScaleMajorType = Calendrier.T_YEAR;
                    PL.FirstScaleMinorType = Calendrier.T_MONTH;
                }
            }

            else if (General.UCase("MS") == General.UCase(sPeriode))
            {
                //descendant.
                if (PL.FirstScaleMajorType == Calendrier.T_YEAR && PL.FirstScaleMinorType == Calendrier.T_MONTH)
                {
                    PL.FirstScaleMinorType = Calendrier.T_WEEK;
                    PL.FirstScaleMajorType = Calendrier.T_MONTH;

                }
                //ascendant.
                if (PL.FirstScaleMajorType == Calendrier.T_DAY && PL.FirstScaleMinorType == Calendrier.T_HOUR)
                {
                    PL.FirstScaleMajorType = Calendrier.T_WEEK;
                    PL.FirstScaleMinorType = Calendrier.T_DAY;
                }

                if (PL.FirstScaleMajorType == Calendrier.T_WEEK && PL.FirstScaleMinorType == Calendrier.T_DAY)
                {
                    PL.FirstScaleMajorType = Calendrier.T_MONTH;
                    PL.FirstScaleMinorType = Calendrier.T_DAY;
                }

                if (PL.FirstScaleMajorType == Calendrier.T_MONTH && PL.FirstScaleMinorType == Calendrier.T_DAY)
                {
                    PL.FirstScaleMajorType = Calendrier.T_MONTH;
                    PL.FirstScaleMinorType = Calendrier.T_WEEK;
                }
            }

            else if (General.UCase("MJ") == General.UCase(sPeriode))
            {

                //descendant.
                if (PL.FirstScaleMajorType == Calendrier.T_YEAR && PL.FirstScaleMinorType == Calendrier.T_MONTH)
                {
                    PL.FirstScaleMinorType = Calendrier.T_WEEK;
                    PL.FirstScaleMajorType = Calendrier.T_MONTH;

                }

                if (PL.FirstScaleMajorType == Calendrier.T_MONTH && PL.FirstScaleMinorType == Calendrier.T_WEEK)
                {
                    PL.FirstScaleMajorType = Calendrier.T_MONTH;
                    PL.FirstScaleMinorType = Calendrier.T_DAY;
                }
                //ascendant.
                if (PL.FirstScaleMajorType == Calendrier.T_DAY && PL.FirstScaleMinorType == Calendrier.T_HOUR)
                {
                    PL.FirstScaleMajorType = Calendrier.T_WEEK;
                    PL.FirstScaleMinorType = Calendrier.T_DAY;
                }

                if (PL.FirstScaleMajorType == Calendrier.T_WEEK && PL.FirstScaleMinorType == Calendrier.T_DAY)
                {
                    PL.FirstScaleMajorType = Calendrier.T_MONTH;
                    PL.FirstScaleMinorType = Calendrier.T_DAY;
                }
                PL.FirstScaleMinorFormat = 5;
                //Ven 12
            }

            else if (General.UCase("SJ") == General.UCase(sPeriode))
            {

                //descendant.
                if (PL.FirstScaleMajorType == Calendrier.T_YEAR && PL.FirstScaleMinorType == Calendrier.T_MONTH)
                {
                    PL.FirstScaleMajorType = Calendrier.T_MONTH;
                    PL.FirstScaleMinorType = Calendrier.T_WEEK;
                }

                if (PL.FirstScaleMajorType == Calendrier.T_MONTH && PL.FirstScaleMinorType == Calendrier.T_WEEK)
                {
                    PL.FirstScaleMajorType = Calendrier.T_MONTH;
                    PL.FirstScaleMinorType = Calendrier.T_DAY;
                }

                if (PL.FirstScaleMajorType == Calendrier.T_MONTH && PL.FirstScaleMinorType == Calendrier.T_DAY)
                {
                    PL.FirstScaleMajorType = Calendrier.T_WEEK;
                    PL.FirstScaleMinorType = Calendrier.T_DAY;
                }

                //ascendant.
                if (PL.FirstScaleMajorType == Calendrier.T_DAY && PL.FirstScaleMinorType == Calendrier.T_HOUR)
                {
                    PL.FirstScaleMajorType = Calendrier.T_WEEK;
                    PL.FirstScaleMinorType = Calendrier.T_DAY;
                }
            }

            else if (General.UCase("JH") == General.UCase(sPeriode))
            {

                //descendant.
                if (PL.FirstScaleMajorType == Calendrier.T_YEAR & PL.FirstScaleMinorType == Calendrier.T_MONTH)
                {
                    PL.FirstScaleMinorType = Calendrier.T_WEEK;
                    PL.FirstScaleMajorType = Calendrier.T_MONTH;

                }

                if (PL.FirstScaleMajorType == Calendrier.T_MONTH & PL.FirstScaleMinorType == Calendrier.T_WEEK)
                {
                    PL.FirstScaleMajorType = Calendrier.T_MONTH;
                    PL.FirstScaleMinorType = Calendrier.T_DAY;
                }

                if (PL.FirstScaleMajorType == Calendrier.T_MONTH & PL.FirstScaleMinorType == Calendrier.T_DAY)
                {
                    PL.FirstScaleMajorType = Calendrier.T_WEEK;
                    PL.FirstScaleMinorType = Calendrier.T_DAY;
                }

                if (PL.FirstScaleMajorType == Calendrier.T_WEEK & PL.FirstScaleMinorType == Calendrier.T_DAY)
                {
                    PL.FirstScaleMinorType = Calendrier.T_HOUR;
                    PL.FirstScaleMajorType = Calendrier.T_DAY;

                }
                PL.FirstScaleMajorFormat = 3;
                // jj/mm/aaaa
            }


            PL.PlanningCtrlRefresh = true;

        }
        private void optTachesSimul_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"optTachesSimul_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            if (optTachesSimul.Checked)
            {
                object variable = ssGridVisiteSimul.ActiveRow != null ? ssGridVisiteSimul.ActiveRow.Cells["NoIntervention"].Value : 0;
                fc_ssGRIDTachesAnnuelSimul(Convert.ToInt32(General.nz(variable, 0)));
            }
        }

        private void optOperationSimul_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"optOperationSimul_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            if (optOperationSimul.Checked)
            {
                object variable = ssGridVisiteSimul.ActiveRow != null ? ssGridVisiteSimul.ActiveRow.Cells["NoIntervention"].Value : 0;
                fc_ssGRIDTachesAnnuelSimul(Convert.ToInt32(General.nz(variable, 0)));
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optPlanCharge_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"optPlanCharge_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            if (optPlanCharge.Checked)
            {
                ssGridTachesAnnuelSimul.Visible = false;
                ssOpTachesSimul.Visible = false;
                ssPlanCharge.Visible = true;
                pctPlanCharge.Visible = true;

                fc_PlanCharge(Convert.ToDateTime(General.nz(dtDateDebPlaningBisSimul.Value, DateTime.Now)), Convert.ToDateTime(General.nz(dtDateFinPlaningBisSimul.Value, DateTime.Now)));
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optPlanChargeIntervenant_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"optPlanChargeIntervenant_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            if (optPlanChargeIntervenant.Checked)
            {
                fc_PlanCharge(Convert.ToDateTime(General.nz(dtDateDebPlaningBisSimul.Value, DateTime.Now)), Convert.ToDateTime(General.nz(dtDateFinPlaningBisSimul.Value, DateTime.Now)));
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optPlanChargeChantier_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"optPlanChargeChantier_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            if (optPlanChargeChantier.Checked)
            {
                fc_PlanCharge(Convert.ToDateTime(General.nz(dtDateDebPlaningBisSimul.Value, DateTime.Now)), Convert.ToDateTime(General.nz(dtDateFinPlaningBisSimul.Value, DateTime.Now)));
            }
        }

        private void fc_PlanCharge(System.DateTime dDebut, System.DateTime dFin)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_PlanCharge() - Enter In The Function - dDebut = {dDebut} - dFin = {dFin}");
            //===> Fin Modif Mondir
            int i = 0;
            DataTable rsPlanCharge = default(DataTable);
            ModAdo rsPlanChargeAdo = new ModAdo();
            string sMoisEncours = null;
            string sCodeEnCours = null;
            DataTable sAddItem = new DataTable();
            int iField = 0;
            int iOldField = 0;
            double nbHeuresEnt = 0;
            double[] tabHeuresEnt = new double[12];

            General.gsUtilisateur = General.fncUserName();

            rsPlanCharge = new DataTable();


            nbHeuresEnt = Convert.ToInt32(General.sPartGMAO) * Convert.ToInt32(General.nz(General.sNbHeuresTotales, 0)) / 100;

            General.sSQL = "SELECT Mois, VentilationEnt FROM GMAO_VentilationEntretien ";
            General.sSQL = General.sSQL + " WHERE CodeUO = '" + General.strCodeUO + "'";
            General.sSQL = General.sSQL + " ORDER BY MOIS";

            rsPlanCharge = rsPlanChargeAdo.fc_OpenRecordSet(General.sSQL);
            if (rsPlanCharge.Rows.Count > 0)
            {
                //rsPlanCharge.MoveFirst();
                foreach (DataRow r in rsPlanCharge.Rows)
                {

                    tabHeuresEnt[Convert.ToInt32(General.nz(Convert.ToInt32(r["mois"]) - 1, 0))] = Convert.ToDouble(General.nz(r["VentilationEnt"], 0)) / 100 * nbHeuresEnt;
                    // rsPlanCharge.MoveNext();
                }
            }
            rsPlanCharge.Dispose();

            ssPlanCharge.DataSource = null;
            //ssPlanCharge.Columns.RemoveAll();

            if (optPlanChargeChantier.Checked == true)
            {
                General.sSQL = "SELECT     InterventionP2Simul.CodeImmeuble [Immeuble], "
                    + " MONTH(DateVisite) AS MoisPrevu, YEAR(DateVisite) AS AnneePrevu, SUM(DureeP) AS TotDuree"
                    + " FROM InterventionP2Simul " + " LEFT OUTER JOIN Immeuble ON "
                    + " InterventionP2Simul.CodeImmeuble = Immeuble.CodeImmeuble  "
                    + " LEFT OUTER JOIN Personnel ON " + " InterventionP2Simul.Intervenant = Personnel.Matricule "
                    + " WHERE InterventionP2Simul.DateVisite>='" + dDebut + "'"
                    + " AND InterventionP2Simul.DateVisite <='" + dFin + "'"
                    + " AND InterventionP2Simul.Article = '" + General.sTypeIntP2 + "'"
                    + " AND Utilisateur ='" + General.gsUtilisateur + "'"
                    + " GROUP BY InterventionP2Simul.CodeImmeuble,   YEAR(DateVisite), MONTH(DateVisite)"
                    + " ORDER BY InterventionP2Simul.CodeImmeuble,  YEAR(DateVisite), MONTH(DateVisite)";

                //ssPlanCharge.Columns.Add(0);
                //ssPlanCharge.Columns[0].Caption = "Immeuble";
                //ssPlanCharge.Columns[0].Name = "immeuble";

                //ssPlanCharge.DisplayLayout.Bands[0].Columns.Add("Immeuble");
                sAddItem.Columns.Add("Immeuble");
            }
            else if (optPlanChargeIntervenant.Checked == true)
            {
                General.sSQL = "SELECT     InterventionP2Simul.Intervenant + ' - ' + Personnel.Nom AS Technicien , "
                    + " MONTH(DateVisite) AS MoisPrevu, YEAR(DateVisite) AS AnneePrevu, SUM(DureeP) AS TotDuree"
                    + " FROM InterventionP2Simul " + " LEFT OUTER JOIN Immeuble ON "
                    + " InterventionP2Simul.CodeImmeuble = Immeuble.CodeImmeuble  " + " LEFT OUTER JOIN Personnel ON "
                    + " InterventionP2Simul.Intervenant = Personnel.Matricule ";


                General.sSQL = General.sSQL + " WHERE InterventionP2Simul.DateVisite>='" + dDebut + "'"
                    + " AND InterventionP2Simul.DateVisite <='" + dFin + "'";
                General.sSQL = General.sSQL + " AND InterventionP2Simul.Article = '" + General.sTypeIntP2 + "'"
                    + " AND Utilisateur ='" + General.gsUtilisateur + "'"
                    + " GROUP BY InterventionP2Simul.Intervenant, Personnel.Nom, YEAR(DateVisite), MONTH(DateVisite) "
                    + " ORDER BY InterventionP2Simul.Intervenant, Personnel.Nom, YEAR(DateVisite), MONTH(DateVisite)";

                //ssPlanCharge.Columns.Add(0);
                //ssPlanCharge.Columns[0].Caption = "Intervenant";
                //ssPlanCharge.Columns[0].Name = "Intervenant";
                //ssPlanCharge.DisplayLayout.Bands[0].Columns.Add("Intervenant");
                sAddItem.Columns.Add("Intervenant");
                //Par defaut : charge par technicien
            }
            else
            {
                General.sSQL = "SELECT     InterventionP2Simul.Intervenant + ' - ' + Personnel.Nom AS Technicien , "
                    + " MONTH(DateVisite) AS MoisPrevu, YEAR(DateVisite) AS AnneePrevu, SUM(DureeP) AS TotDuree"
                    + " FROM InterventionP2Simul " + " LEFT OUTER JOIN Immeuble ON "
                    + " InterventionP2Simul.CodeImmeuble = Immeuble.CodeImmeuble  "
                    + " LEFT OUTER JOIN Personnel ON " + " InterventionP2Simul.Intervenant = Personnel.Matricule "
                    + " WHERE InterventionP2Simul.DateVisite>='" + dDebut + "'" + " AND InterventionP2Simul.DateVisite <='"
                    + dFin + "'" + " AND InterventionP2Simul.Article = '" + General.sTypeIntP2 + "'"
                    + " AND Utilisateur ='" + General.gsUtilisateur + "'"
                    + " GROUP BY InterventionP2Simul.Intervenant, Personnel.Nom, YEAR(DateVisite), MONTH(DateVisite) "
                    + " ORDER BY InterventionP2Simul.Intervenant, Personnel.Nom, YEAR(DateVisite), MONTH(DateVisite)";

                //ssPlanCharge.Columns.Add(0);
                //ssPlanCharge.Columns[0].Caption = "Intervenant";
                //ssPlanCharge.Columns[0].Name = "Intervenant";
                //ssPlanCharge.DisplayLayout.Bands[0].Columns.Add("Intervenant");
                sAddItem.Columns.Add("Intervenant");
            }

            var NbrMois = (((Convert.ToDateTime(dFin).Year - Convert.ToDateTime(dDebut).Year) * 12) + Convert.ToDateTime(dFin).Month - Convert.ToDateTime(dDebut).Month) + 1;

            for (i = 1; i <= NbrMois; i++)
            {
                sMoisEncours = Convert.ToString(dDebut.AddMonths(i - 1));
                sMoisEncours = Convert.ToDateTime(sMoisEncours).ToString("MMM yyyy");


                //ssPlanCharge.Columns.Add(i);
                //ssPlanCharge.Columns[i].Caption = sMoisEncours;
                //ssPlanCharge.Columns[i].Name = sMoisEncours;
                //ssPlanCharge.Columns[i].TagVariant = tabHeuresEnt[dDebut.AddMonths(i - 1).Month + 1];
                //ssPlanCharge.DisplayLayout.Bands[0].Columns.Add(""+i);
                sAddItem.Columns.Add(sMoisEncours);
            }

            sCodeEnCours = "";

            iOldField = 0;
            iField = 0;

            var dr = sAddItem.NewRow();
            rsPlanCharge = rsPlanChargeAdo.fc_OpenRecordSet(General.sSQL);

            if (rsPlanCharge.Rows.Count > 0)
            {
                sCodeEnCours = rsPlanCharge.Rows[0][0].ToString() + "";

                foreach (DataRow r in rsPlanCharge.Rows)
                {
                    if (sCodeEnCours != r[0] + "")
                    {
                        if (!string.IsNullOrEmpty(sCodeEnCours) /*&& dr != null*/)
                        {
                            sAddItem.Rows.Add(dr.ItemArray);

                            ssPlanCharge.DataSource = sAddItem;
                            sCodeEnCours = r[0] + "";

                            dr = sAddItem.NewRow();
                            iOldField = 0;
                            iField = 0;
                        }
                    }

                    if (sCodeEnCours == "00000 - DUPRAT")
                    {
                        Debug.Print("");
                    }

                    var cDateFin = Convert.ToDateTime("01/" + r["MoisPrevu"].ToString() + "/" + r["AnneePrevu"].ToString());
                    iField = (((Convert.ToDateTime(cDateFin).Year - Convert.ToDateTime(dDebut).Year) * 12) + Convert.ToDateTime(cDateFin).Month - Convert.ToDateTime(dDebut).Month);

                    for (i = iOldField; i <= iField; i++)
                    {
                        if (i == iField)
                        {
                            dr[0] = sCodeEnCours;
                            dr[iField + 1] = r["TotDuree"];//.ToString("0.00");

                        }
                        else
                        {
                            dr[0] = sCodeEnCours + "\n" + "";

                        }
                    }
                    iOldField = iField + 1;
                }

                sAddItem.Rows.Add(dr.ItemArray);

            }

            ssPlanCharge.DataSource = sAddItem;

            rsPlanCharge.Dispose();
            rsPlanCharge = null;

        }

        private void fc_ssGRIDTachesAnnuelSimul(int lNoIntervention)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ssGRIDTachesAnnuelSimul() - Enter In The Function - lNoIntervention = {lNoIntervention}");
            //===> Fin Modif Mondir
            if (optOperationSimul.Checked == true)//tested
            {

                ssGridTachesAnnuelSimul.Visible = true;
                ssOpTachesSimul.Visible = false;

                //sSQL = "SELECT * from OperationP2Simul where NointerventionVisite =" & LNointervention & "" _
                //& " ORDER BY OperationP2Simul.CAI_Libelle, OperationP2Simul.EQM_CODE, TPP_Code"


                General.sSQL = "SELECT     CodeImmeuble, PDAB_Libelle AS Localisation, NoInterventionVisite, GAI_LIbelle AS Equipements, "
                    + " CAI_Libelle AS Composant, EQM_Code AS Operation," + " EQM_Libelle, " + " CTR_Libelle, MOY_Libelle, ANO_Libelle, OPE_Libelle,"
                    + " TPP_Code AS Periodicite, NoSemaine, Duree " + " From OperationP2Simul" + " WHERE NointerventionVisite = " + lNoIntervention
                    + " ORDER BY Localisation, Equipements, Composant, Operation";

                rsGammeSimul = rsGammeSimulAdo.fc_OpenRecordSet(General.sSQL);

                ssGridTachesAnnuelSimul.DataSource = rsGammeSimul;



                //    With adoOpSimul
                //        .ConnectionString = adocnn
                //        .RecordSource = sSQL
                //        .Refresh
                //    End With

            }
            else if (optTachesSimul.Checked == true)
            {

                ssGridTachesAnnuelSimul.Visible = false;
                ssOpTachesSimul.Visible = true;

                General.sSQL = "SELECT     OperationP2Simul.DatePrevue, OperationP2Simul.EQM_Code, " + " TIP2S_TachesInterP2Simul.TIP2_Designation1, "
                    + " TIP2S_TachesInterP2Simul.TIP2_NoLIgne , TIP2S_TachesInterP2Simul.Realise," + " TIP2S_TachesInterP2Simul.DateRealise"
                    + " FROM         OperationP2Simul INNER JOIN" + " TIP2S_TachesInterP2Simul "
                    + " ON OperationP2Simul.NoIntervention = TIP2S_TachesInterP2Simul.NoInterventionOp"
                    + " WHERE  OperationP2Simul.NoInterventionVisite = " + lNoIntervention + " ORDER BY OperationP2Simul.CAI_Libelle, OperationP2Simul.EQM_Code, "
                    + " TIP2S_TachesInterP2Simul.TIP2_NoLIgne";

                rsOpTaches = rsOpTachesAdo.fc_OpenRecordSet(General.sSQL);

                ssGridTachesAnnuelSimul.DataSource = rsOpTaches;


            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAfficheSimul_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAfficheSimul_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            int lCopNoAuto = 0;

            try
            {

                if (fc_erreurSimul() == true)//tested
                {
                    return;
                }

                //fc_SaveParamForm

                if (optOperationSimul.Checked == false && optTachesSimul.Checked == false)
                {
                    optOperationSimul.Checked = true;
                }

                if (!string.IsNullOrEmpty(txtCOP_NoContratSimul.Text))
                {
                    //    If txtCOP_AvenantSimul = "" Then
                    //            txtCOP_AvenantSimul = "0"
                    //
                    //    ElseIf Not IsNumeric(txtCOP_AvenantSimul) Then
                    //            txtCOP_AvenantSimul = "0"
                    //
                    //    End If
                    //
                    //     sSQL = "SELECT  COP_NoAuto" _
                    //'         & " From COP_ContratP2" _
                    //'         & " WHERE COP_NoContrat = '" & gFr_DoublerQuote(txtCOP_NoContratSimul) & "'" _
                    //'         & " AND COP_Avenant =" & txtCOP_AvenantSimul & ""
                    //
                    //     sSQL = fc_ADOlibelle(sSQL)
                    //
                    //     lCopNoAuto = nz(sSQL, 0)
                    //
                    //     If lCopNoAuto = 0 Then
                    //
                    //         MsgBox "Ce numéro de contrat n'existe pas.", vbInformation, "Opération annulée"
                    //         Exit Sub
                    //     End If

                }


                fc_LoadIntervP2Simul(Convert.ToDateTime(dtDateDebPlaningBisSimul.Value), Convert.ToDateTime(dtDateFinPlaningBisSimul.Value), lCopNoAuto, txtIntervenantSimul.Text, txtCodeImmeubleSimul.Text, "");

                FC_SaveParam();

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAfficheSimul_Click");
            }
        }

        private void UserIntervP2V2_Load(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"UserIntervP2V2_Load() - Enter In The Function");
            //===> Fin Modif Mondir
            string sDroitCreat = null;


            // ModMain.fc_Color(this); cette fct est commenté dans ce module

            var _with53 = ssGridTachesAnnuelSimul;
            //.Caption = cLIbNiv2
            //.Columns("CAI_Libelle").Caption = clibNiv1
            //.Columns("EQM_CODE").Caption = cLibNv2Tier

            //var _with54 = ssGridTachesAnnuelConsult;
            //_with54.Text = modP2.cLIbNiv2;
            //_with54.Columns("CAI_Libelle").Caption = modP2.clibNiv1;
            //_with54.Columns("EQM_Code").Caption = modP2.cLibNv2Tier;


            var _with55 = ssGridTachesAnnuel;
            //.Caption = cLIbNiv2
            //.Columns("CAI_Libelle").Caption = clibNiv1
            //.Columns("EQM_CODE").Caption = cLibNv2Tier

            var _with56 = ssOpTachesSimul;
            _with56.Text = modP2.cLibNiv3 + "(simulation)";
            //_with56.Columns("EQM_CODE").Caption = modP2.cLibNv2Tier;
            //_with56.Columns("TIP2_Designation1").Caption = modP2.cLibNiv3;


            //var _with57 = ssOpTachesConsult;
            //_with57.Text = modP2.cLibNiv3 + "(simulation)";
            //_with57.Columns("EQM_CODE").Caption = modP2.cLibNv2Tier;
            //_with57.Columns("TIP2_Designation1").Caption = modP2.cLibNiv3;

            var _with58 = ssOpeTaches;
            _with58.Text = modP2.cLibNiv3 + "(simulation)";
            //_with58.Columns("EQM_CODE").Caption = modP2.cLibNv2Tier;
            //_with58.Columns("TIP2_Designation1").Caption = modP2.cLibNiv3;

            var _with59 = ssGridTachesAnnuelHisto;
            _with59.Text = modP2.cLIbNiv2;
            //.Columns("CAI_Libelle").Caption = clibNiv1
            //.Columns("EQM_CODE").Caption = cLibNv2Tier

            var _with60 = ssOpeTachesHisto;
            _with60.Text = modP2.cLibNiv3 + "(simulation)";
            //_with60.Columns("EQM_CODE").Caption = modP2.cLibNv2Tier;
            //_with60.Columns("TIP2_Designation1").Caption = modP2.cLibNiv3;


            var _with61 = ssGAvisDEPassage;
            //_with61.Columns("CAI_Libelle").Caption = modP2.clibNiv1;
            //_with61.Columns("EQM_CODE").Caption = modP2.cLibNv2Tier;
            //_with61.Columns("Codeimmeuble").Caption = modP2.cCodeImmeuble;
            //Modif Ludovic : suite suppression des colonnes CodeNumOrdre et CodeChaufferie
            //Fusionnées dans la colonne "CodeImmeuble"

            //        .Columns("CodeNumOrdre").Caption = cCodeNumOrdre
            //        .Columns("CodeChaufferie").Caption = cCodeChaufferie
            //        .Columns("CodeNumOrdre").Caption = cCodeNumOrdre
            //        .Columns("CodeChaufferie").Caption = cCodeChaufferie

            var _with62 = ssGridVisiteSimul;
            //_with62.Columns("Codeimmeuble").Caption = modP2.cCodeImmeuble;
            //Modif Ludovic : suite suppression des colonnes CodeNumOrdre et CodeChaufferie
            //Fusionnées dans la colonne "CodeImmeuble"

            //        .Columns("CodeNumOrdre").Caption = cCodeNumOrdre
            //        .Columns("CodeChaufferie").Caption = cCodeChaufferie


            var _with63 = ssGridVisite;

            //Modif Ludovic : suite suppression des colonnes CodeNumOrdre et CodeChaufferie
            //Fusionnées dans la colonne "CodeImmeuble"

            //        .Columns("CodeNumOrdre").Caption = cCodeNumOrdre
            //        .Columns("CodeChaufferie").Caption = cCodeChaufferie


            optOperation.Text = modP2.cLibNv2Tier;
            optTaches.Text = modP2.cLibNiv3;

            optOperationHisto.Text = modP2.cLibNv2Tier;
            optTachesHisto.Text = modP2.cLibNiv3;

            optOperationSimul.Text = modP2.cLibNv2Tier;
            //optOperationConsult.Text = modP2.cLibNv2Tier;

            optTachesSimul.Text = modP2.cLibNiv3;
            //optTachesConsult.Text = modP2.cLibNiv3;

            //optOperationConsult.Checked = true;
            optOperationSimul.Checked = true;

            //optEtatOpConsult.Text = modP2.cLibEtatDetailOp;


            var tmp = new ModAdo();
            sDroitCreat = tmp.fc_ADOlibelle("SELECT AUT_Nom " + " From AUT_Autorisation" + " WHERE AUT_Formulaire = '"
                + ModConstantes.cUserInterP2V2 + "' " + " and AUT_Objet ='" + ModConstantes.cCreationVisiteP2 + "'" + " AND AUT_Nom = '" + General.fncUserName() + "'");
            //=== onglets inutiles.
            //SSTab1.Tabs[1].Visible = false;
            //SSTab1.Tabs[3].Visible = false;

            //===> Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021
            if (General.sChargeGmao == "1")
            {
                SSTab1.Tabs[1].Visible = true;
                Opt_6.Checked = true;
            }
            else
            {
                SSTab1.Tabs[1].Visible = false;
            }
            //===> Fin Modif Mondir

            //SSTab1.TabVisible(5) = False
            //If sDroitCreat <> "" Then
            //        SSTab1.TabsPerRow = 4
            //        '=== doit sur bouton creation.
            //        sDroitCreat = fc_ADOlibelle("SELECT AUT_Nom " _
            //'                                & " From AUT_Autorisation" _
            //'                                & " WHERE AUT_Formulaire = '" & cUserInterP2V2 & "' " _
            //'                                & " and AUT_Objet ='" & cCreationVisiteP2Button & "'" _
            //'                                & " AND AUT_Nom = '" & fncUserName & "'")
            //
            //        If sDroitCreat <> "" Then
            //                cmdVisites.Enabled = True
            //        Else
            //                cmdVisites.Enabled = False
            //        End If
            //
            //
            //Else
            //        SSTab1.TabVisible(0) = False
            //        SSTab1.TabVisible(2) = False
            //        SSTab1.TabVisible(4) = False
            //        SSTab1.TabsPerRow = 1
            //End If
            //
            //
            //
            //'=== modif du 14 04 2015, applique des droits sur la case à
            //'=== cocher P1 compteur.
            //sDroitCreat = fc_ADOlibelle("SELECT AUT_Nom " _
            //'                                & " From AUT_Autorisation" _
            //'                                & " WHERE AUT_Formulaire = '" & cUserInterP2V2 & "' " _
            //'                                & " and AUT_Objet ='" & cCreationVisiteP1Compt & "'" _
            //'                                & " AND AUT_Nom = '" & fncUserName & "'")
            //
            //If sDroitCreat <> "" Then
            //        'chkEQU_P1Compteur.Enabled = True
            //        OptEQU_P1Compteur(0).Enabled = True
            //        OptEQU_P1Compteur(1).Enabled = True
            //        OptEQU_P1Compteur(2).Enabled = True
            //
            //Else
            //        'chkEQU_P1Compteur.Enabled = False
            //        OptEQU_P1Compteur(0).Enabled = False
            //        OptEQU_P1Compteur(1).Enabled = False
            //        OptEQU_P1Compteur(2).Enabled = False
            //End If
            //
            //If sFIltreInterP1 = "1" Then
            //        frame11(7).Visible = True
            //        frame11(8).Visible = True
            //        frame11(9).Visible = True
            //
            //Else
            //        frame11(7).Visible = False
            //        frame11(8).Visible = False
            //        frame11(9).Visible = False
            //End If

            //fc_GetParamForm Me
            //===> Mondir le 05.11.2020, pour corriger https://groupe-dt.mantishub.io/view.php?id=2048
            dtDateDebPlaningBisSimul.AutoValidateDate();
            dtDateFinPlaningBisSimul.AutoValidateDate();
            //===> Fin Modif Mondir
        }

        private void txtPrestationSimul_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtPrestationSimul_KeyPress() - Enter In The Function - e.KeyChar = {(short)(e.KeyChar)}");
            //===> Fin Modif Mondir
            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdPrestationSimul_Click(cmdPrestationSimul, new System.EventArgs());
                KeyAscii = 0;
            }
        }

        private void ssGridHisto_Error(object sender, ErrorEventArgs e)
        {

        }

        private void ssGridHisto_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridHisto_BeforeRowsDeleted() - Enter In The Function");
            //===> Fin Modif Mondir
            e.Cancel = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void ssGridTachesAnnuelHisto_Error(object sender, ErrorEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridTachesAnnuelHisto_Error() - Enter In The Function");
            //===> Fin Modif Mondir
            if (e.ErrorType == ErrorType.Data)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + e.DataErrorInfo.Cell.Column.Key;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }

        private void ssGridTachesAnnuelHisto_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridTachesAnnuelHisto_BeforeRowsDeleted() - Enter In The Function");
            //===> Fin Modif Mondir
            e.Cancel = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void ssOpeTachesHisto_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssOpeTachesHisto_BeforeRowsDeleted() - Enter In The Function");
            //===> Fin Modif Mondir
            e.Cancel = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void ssOpeTachesHisto_Error(object sender, ErrorEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssOpeTachesHisto_Error() - Enter In The Function");
            //===> Fin Modif Mondir
            if (e.ErrorType == ErrorType.Data)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + e.DataErrorInfo.Cell.Column.Key;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }

        private void ssGridVisiteSimul_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridVisiteSimul_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir
            ssGridVisiteSimul.DisplayLayout.Bands[0].Columns["NoIntervention"].Header.Caption = "N°Visite (simul)";
            ssGridVisiteSimul.DisplayLayout.Bands[0].Columns["DatePrevue"].Hidden = true;
            ssGridVisiteSimul.DisplayLayout.Bands[0].Columns["Cop_NoAuto"].Hidden = true;
            ssGridVisiteSimul.DisplayLayout.Bands[0].Columns["INT_realise"].Hidden = true;
            ssGridVisiteSimul.DisplayLayout.Bands[0].Columns["dateRealise"].Hidden = true;
            ssGridVisiteSimul.DisplayLayout.Bands[0].Columns["HeureDebut"].Hidden = true;
            ssGridVisiteSimul.DisplayLayout.Bands[0].Columns["HeureFin"].Hidden = true;
            ssGridVisiteSimul.DisplayLayout.Bands[0].Columns["HeureDebutP"].Hidden = true;
            ssGridVisiteSimul.DisplayLayout.Bands[0].Columns["HeureFinP"].Hidden = true;
            ssGridVisiteSimul.DisplayLayout.Bands[0].Columns["PDAB_Libelle"].Header.Caption = "Localisation";
            ssGridVisiteSimul.DisplayLayout.Bands[0].Columns["Nom"].Header.Caption = "Nom d'intervenant";
            ssGridVisiteSimul.DisplayLayout.Bands[0].Columns["DureeP"].Header.Caption = "Duree";
            ssGridVisiteSimul.DisplayLayout.Bands[0].Columns["CompteurObli"].Header.Caption = "Compteur Obligatoire ";
            ssGridVisiteSimul.DisplayLayout.Bands[0].Columns["CompteurObli"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;

            for (int i = 0; i < ssGridVisiteSimul.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                ssGridVisiteSimul.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
            }
        }

        private void ssGridVisite_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridVisite_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir
            ssGridVisite.DisplayLayout.Bands[0].Columns["Selection"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridVisite.DisplayLayout.Bands[0].Columns["NumFicheStandard"].Header.Caption = "N°Fiche Appel";
            ssGridVisite.DisplayLayout.Bands[0].Columns["nointervention"].Header.Caption = "N°intervention";
            ssGridVisite.DisplayLayout.Bands[0].Columns["nointervention"].CellActivation = Activation.NoEdit;
            ssGridVisite.DisplayLayout.Bands[0].Columns["NoLigne"].Header.Caption = "N°Ligne";
            ssGridVisite.DisplayLayout.Bands[0].Columns["NoLigne"].CellActivation = Activation.NoEdit;
            ssGridVisite.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Header.Caption = modP2.cCodeImmeuble;
            ssGridVisite.DisplayLayout.Bands[0].Columns["CodeImmeuble"].CellActivation = Activation.NoEdit;
            ssGridVisite.DisplayLayout.Bands[0].Columns["CodeEtatPDA"].Header.Caption = "Statut PDA";
            ssGridVisite.DisplayLayout.Bands[0].Columns["CodeEtatPDA"].CellActivation = Activation.NoEdit;
            ssGridVisite.DisplayLayout.Bands[0].Columns["Ville"].CellActivation = Activation.NoEdit;
            ssGridVisite.DisplayLayout.Bands[0].Columns["EnvoyePar"].CellActivation = Activation.NoEdit;
            ssGridVisite.DisplayLayout.Bands[0].Columns["EnvoyeLe"].CellActivation = Activation.NoEdit;
            ssGridVisite.DisplayLayout.Bands[0].Columns["ModifierPar"].CellActivation = Activation.NoEdit;
            ssGridVisite.DisplayLayout.Bands[0].Columns["ModifierLe"].CellActivation = Activation.NoEdit;
            ssGridVisite.DisplayLayout.Bands[0].Columns["CreeLe"].CellActivation = Activation.NoEdit;
            ssGridVisite.DisplayLayout.Bands[0].Columns["CreePar"].CellActivation = Activation.NoEdit;
            ssGridVisite.DisplayLayout.Bands[0].Columns["Nom"].CellActivation = Activation.NoEdit;
            ssGridVisite.DisplayLayout.Bands[0].Columns["heureDebutP"].CellActivation = Activation.NoEdit;
            ssGridVisite.DisplayLayout.Bands[0].Columns["heureDebutP"].Header.Caption = "Heure Prévue";
            ssGridVisite.DisplayLayout.Bands[0].Columns["Designation"].Header.Caption = "Libellé article";
            ssGridVisite.DisplayLayout.Bands[0].Columns["PDAB_Noauto"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["DatePrevue"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["ID"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["Adresse"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["CodePostal"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["Prenom"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["CodePostal"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["PDAB_Libelle"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["PDAB_IDBarre"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["COP_NoContrat"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["COP_Noauto"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["HeurePrevue"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["ID_PDA"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["CompteurObli"].Header.Caption = "Compteur Obligatoire ";
            ssGridVisite.DisplayLayout.Bands[0].Columns["CompteurObli"].CellActivation = Activation.NoEdit;
            ssGridVisite.DisplayLayout.Bands[0].Columns["CompteurObli"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;

            ssGridVisite.DisplayLayout.Bands[0].Columns["Latitude"].Hidden = true;
            ssGridVisite.DisplayLayout.Bands[0].Columns["Longitude"].Hidden = true;

            this.ssGridVisite.DisplayLayout.Bands[0].Columns["Intervenant"].ValueList = this.ssDropMatricule;

            ssGridVisite.DisplayLayout.Bands[0].Columns["heureFinP"].Format = "HH:mm:ss";
            ssGridVisite.DisplayLayout.Bands[0].Columns["heureDebutP"].Format = "HH:mm:ss";
        }

        private void ssGridTachesAnnuel_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridTachesAnnuel_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["NoInterventionVisite"].Hidden = true;
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["OPE_Libelle"].Header.Caption = "Opération";
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["ANO_Libelle"].Header.Caption = "Anomalie";
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["MOY_Libelle"].Header.Caption = "Moyen ";
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["CTR_Libelle"].Header.Caption = "Controle ";
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["EQM_Libelle"].Hidden = true;
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["Operation"].Hidden = true;

            for (int i = 0; i < ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns.Count - 1; i++)
            {
                ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridVisite_CellChange(object sender, CellEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridVisite_CellChange() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                if (e.Cell.Column.Key.ToUpper() == "Intervenant".ToUpper())
                {
                    var row = ssDropMatricule.Rows.Cast<UltraGridRow>().FirstOrDefault(r => r.Cells[0].Value.ToString() == e.Cell.Text);
                    if (row != null)
                    {
                        ssGridVisite.ActiveRow.Cells["Intervenant"].Value = row.Cells[0].Value;
                        ssGridVisite.ActiveRow.Cells["Nom"].Value = row.Cells[1].Value.ToString() + row.Cells[2].Value.ToString();

                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssGridVisite_CellChange");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridVisite_AfterExitEditMode(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridVisite_AfterExitEditMode() - Enter In The Function");
            //===> Fin Modif Mondir
            if (ssGridVisite.ActiveCell.Column.Key.ToUpper() == "Selection".ToUpper() && ssGridVisite.ActiveCell.DataChanged)
            {
                General.Execute($"UPDATE Intervention SET Selection= '{ssGridVisite.ActiveRow.Cells["Selection"].Value.ToString()}'" +
                   $" WHERE  nointervention={ssGridVisite.ActiveRow.Cells["nointervention"].Value.ToString()} and  NumFicheStandard  = {ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value.ToString()}");

            }
            if (ssGridVisite.ActiveCell.Column.Key.ToUpper() == "CodeEtat".ToUpper() && ssGridVisite.ActiveCell.DataChanged)
            {
                General.Execute($"UPDATE Intervention SET CodeEtat= '{ssGridVisite.ActiveRow.Cells["CodeEtat"].Value.ToString()}'" +
                   $" WHERE  nointervention={ssGridVisite.ActiveRow.Cells["nointervention"].Value.ToString()} and  NumFicheStandard  = {ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value.ToString()}");

            }
            if (ssGridVisite.ActiveCell.Column.Key.ToUpper() == "Intervenant".ToUpper() && ssGridVisite.ActiveCell.DataChanged)
            {
                General.Execute($"UPDATE Intervention  SET  Intervention.Intervenant = '{ssGridVisite.ActiveRow.Cells["Intervenant"].Value.ToString()}'" +
                   $" WHERE  nointervention={ssGridVisite.ActiveRow.Cells["nointervention"].Value.ToString()} and  NumFicheStandard  = {ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value.ToString()}");

            }
            if (ssGridVisite.ActiveCell.Column.Key.ToUpper() == "DateSaisie".ToUpper() && ssGridVisite.ActiveCell.DataChanged)
            {
                General.Execute($"UPDATE Intervention SET DateSaisie= '{ssGridVisite.ActiveRow.Cells["DateSaisie"].Value.ToString()}'" +
                   $" WHERE  nointervention={ssGridVisite.ActiveRow.Cells["nointervention"].Value.ToString()} and  NumFicheStandard  = {ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value.ToString()}");

            }
            if (ssGridVisite.ActiveCell.Column.Key.ToUpper() == "DateVisite".ToUpper() && ssGridVisite.ActiveCell.DataChanged)
            {
                General.Execute($"UPDATE Intervention SET DateVisite= '{ssGridVisite.ActiveRow.Cells["DateVisite"].Value.ToString()}'" +
                   $" WHERE  nointervention={ssGridVisite.ActiveRow.Cells["nointervention"].Value.ToString()} and  NumFicheStandard  = {ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value.ToString()}");

            }
            if (ssGridVisite.ActiveCell.Column.Key.ToUpper() == "Article".ToUpper() && ssGridVisite.ActiveCell.DataChanged)
            {
                General.Execute($"UPDATE Intervention SET Article= '{ssGridVisite.ActiveRow.Cells["Article"].Value.ToString()}'" +
                   $" WHERE  nointervention={ssGridVisite.ActiveRow.Cells["nointervention"].Value.ToString()} and  NumFicheStandard  = {ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value.ToString()}");

            }
            if (ssGridVisite.ActiveCell.Column.Key.ToUpper() == "DateRealise".ToUpper() && ssGridVisite.ActiveCell.DataChanged)
            {
                //===> Mondir le 05.11.2020, ajout la condition is date et modifier le format sans heure https://groupe-dt.mantishub.io/view.php?id=2076
                var date = ssGridVisite.ActiveRow.Cells["DateRealise"].Value.IsDate()
                    ? ssGridVisite.ActiveRow.Cells["DateRealise"].Value.ToDate()
                        .ToString(General.FormatDateSansHeureSQL)
                    : "NULL";
                General.Execute($"UPDATE Intervention SET DateRealise= '{date}'" +
                   $" WHERE  nointervention={ssGridVisite.ActiveRow.Cells["nointervention"].Value.ToString()} and  NumFicheStandard  = {ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value.ToString()}");

            }
            if (ssGridVisite.ActiveCell.Column.Key.ToUpper() == "Libellé article".ToUpper() && ssGridVisite.ActiveCell.DataChanged)
            {
                General.Execute($"UPDATE Intervention SET Designation= '{ssGridVisite.ActiveRow.Cells["Designation"].Value.ToString()}'" +
                   $" WHERE  nointervention={ssGridVisite.ActiveRow.Cells["nointervention"].Value.ToString()} and  NumFicheStandard  = {ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value.ToString()}");

            }
            if (ssGridVisite.ActiveCell.Column.Key.ToUpper() == "DPeriodeDu".ToUpper() && ssGridVisite.ActiveCell.DataChanged)
            {
                General.Execute($"UPDATE Intervention SET DPeriodeDu= '{ssGridVisite.ActiveRow.Cells["DPeriodeDu"].Value.ToString()}'" +
                   $" WHERE  nointervention={ssGridVisite.ActiveRow.Cells["nointervention"].Value.ToString()} and  NumFicheStandard  = {ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value.ToString()}");

            }
            if (ssGridVisite.ActiveCell.Column.Key.ToUpper() == "DPeriodeAu".ToUpper() && ssGridVisite.ActiveCell.DataChanged)
            {
                General.Execute($"UPDATE Intervention SET DPeriodeAu= '{ssGridVisite.ActiveRow.Cells["DPeriodeAu"].Value.ToString()}'" +
                   $" WHERE  nointervention={ssGridVisite.ActiveRow.Cells["nointervention"].Value.ToString()} and  NumFicheStandard  = {ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value.ToString()}");

            }
            if (ssGridVisite.ActiveCell.Column.Key.ToUpper() == "ALAR_Code_Inter".ToUpper() && ssGridVisite.ActiveCell.DataChanged)
            {
                General.Execute($"UPDATE Intervention SET ALAR_Code_Inter= '{ssGridVisite.ActiveRow.Cells["ALAR_Code_Inter"].Value.ToString()}'" +
                   $" WHERE  nointervention={ssGridVisite.ActiveRow.Cells["nointervention"].Value.ToString()} and  NumFicheStandard  = {ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value.ToString()}");

            }
            if (ssGridVisite.ActiveCell.Column.Key.ToUpper() == "NoDecade".ToUpper() && ssGridVisite.ActiveCell.DataChanged)
            {
                General.Execute($"UPDATE Intervention SET NoDecade= '{ssGridVisite.ActiveRow.Cells["NoDecade"].Value.ToString()}'" +
                   $" WHERE  nointervention={ssGridVisite.ActiveRow.Cells["nointervention"].Value.ToString()} and  NumFicheStandard  = {ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value.ToString()}");

            }
            if (ssGridVisite.ActiveCell.Column.Key.ToUpper() == "Commentaire".ToUpper() && ssGridVisite.ActiveCell.DataChanged)
            {
                General.Execute($"UPDATE Intervention SET Commentaire= '{ssGridVisite.ActiveRow.Cells["Commentaire"].Value.ToString()}'" +
                   $" WHERE  nointervention={ssGridVisite.ActiveRow.Cells["nointervention"].Value.ToString()} and  NumFicheStandard  = {ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value.ToString()}");

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridVisite_Error(object sender, ErrorEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridVisite_Error() - Enter In The Function");
            //===> Fin Modif Mondir
            if (e.ErrorType == ErrorType.Data)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + e.DataErrorInfo.Cell.Column.Key;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridVisiteSimul_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridVisiteSimul_InitializeRow() - Enter In The Function");
            //===> Fin Modif Mondir
            if (e.Row.Cells["CompteurObli"].Value == DBNull.Value)
            {
                e.Row.Cells["CompteurObli"].Value = false;
                e.Row.Update();
            }
            //ssGridVisiteSimul.UpdateData();

        }

        private void ssGridTachesAnnuelSimul_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridTachesAnnuelSimul_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir
            ssGridTachesAnnuelSimul.DisplayLayout.Bands[0].Columns["NoInterventionVisite"].Hidden = true;
            ssGridTachesAnnuelSimul.DisplayLayout.Bands[0].Columns["OPE_Libelle"].Header.Caption = "Opération";
            ssGridTachesAnnuelSimul.DisplayLayout.Bands[0].Columns["ANO_Libelle"].Header.Caption = "Anomalie";
            ssGridTachesAnnuelSimul.DisplayLayout.Bands[0].Columns["MOY_Libelle"].Header.Caption = "Moyen ";
            ssGridTachesAnnuelSimul.DisplayLayout.Bands[0].Columns["CTR_Libelle"].Header.Caption = "Controle ";
            ssGridTachesAnnuelSimul.DisplayLayout.Bands[0].Columns["EQM_Libelle"].Hidden = true;
            ssGridTachesAnnuelSimul.DisplayLayout.Bands[0].Columns["Operation"].Hidden = true;

            for (int i = 0; i < ssGridTachesAnnuelSimul.DisplayLayout.Bands[0].Columns.Count - 1; i++)
            {
                ssGridTachesAnnuelSimul.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
            }
        }


        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DTDatePrevueDe_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"DTDatePrevueDe_BeforeDropDown() - Enter In The Function");
            //===> Fin Modif Mondir
            var btn = sender as UltraDateTimeEditor;
            ((btn.ButtonsLeft[0]) as StateEditorButton).Checked = true;
        }

        private void ssOpeTachesHisto_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssOpeTachesHisto_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir
            ssOpeTachesHisto.DisplayLayout.Bands[0].Columns["DatePrevue"].Hidden = true;

            ssOpeTachesHisto.DisplayLayout.Bands[0].Columns["Realise"].Hidden = true;
            ssOpeTachesHisto.DisplayLayout.Bands[0].Columns["DateRealise"].Hidden = true;
            ssOpeTachesHisto.DisplayLayout.Bands[0].Columns["NoIntervention"].Hidden = true;
            ssOpeTachesHisto.DisplayLayout.Bands[0].Columns["NoInterventionVisite"].Hidden = true;


            ssOpeTachesHisto.DisplayLayout.Bands[0].Columns["EQM_code"].Header.Caption = "Prestation";
            ssOpeTachesHisto.DisplayLayout.Bands[0].Columns["TIP2_Designation1"].Header.Caption = "Nature des prestations";
            ssOpeTachesHisto.DisplayLayout.Bands[0].Columns["TIP2_NoLIgne"].Header.Caption = "N° Ligne";


        }

        private void ssGridTachesAnnuelHisto_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridTachesAnnuelHisto_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir
            ssGridTachesAnnuelHisto.DisplayLayout.Bands[0].Columns["NoInterventionVisite"].Hidden = true;
            ssGridTachesAnnuelHisto.DisplayLayout.Bands[0].Columns["OPE_Libelle"].Header.Caption = "Opération";
            ssGridTachesAnnuelHisto.DisplayLayout.Bands[0].Columns["ANO_Libelle"].Header.Caption = "Anomalie";
            ssGridTachesAnnuelHisto.DisplayLayout.Bands[0].Columns["MOY_Libelle"].Header.Caption = "Moyen ";
            ssGridTachesAnnuelHisto.DisplayLayout.Bands[0].Columns["CTR_Libelle"].Header.Caption = "Controle ";
            ssGridTachesAnnuelHisto.DisplayLayout.Bands[0].Columns["INT_Realisee"].Header.Caption = "Réalisée ";
            ssGridTachesAnnuelHisto.DisplayLayout.Bands[0].Columns["EQM_Libelle"].Hidden = true;
            ssGridTachesAnnuelHisto.DisplayLayout.Bands[0].Columns["Operation"].Hidden = true;
            ssGridTachesAnnuelHisto.DisplayLayout.Bands[0].Columns["INT_Realisee"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;

            for (int i = 0; i < ssGridTachesAnnuelHisto.DisplayLayout.Bands[0].Columns.Count - 1; i++)
            {
                ssGridTachesAnnuelHisto.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridHisto_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridHisto_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir
            ssGridHisto.DisplayLayout.Bands[0].Columns["NumFicheStandard"].Header.Caption = "N°Fiche Appel";
            ssGridHisto.DisplayLayout.Bands[0].Columns["nointervention"].Header.Caption = "N°intervention";
            ssGridHisto.DisplayLayout.Bands[0].Columns["NoLigne"].Header.Caption = "N°Ligne";
            ssGridHisto.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Header.Caption = "CodeImmeuble";
            ssGridHisto.DisplayLayout.Bands[0].Columns["CodeEtat"].Header.Caption = "Statut";
            ssGridHisto.DisplayLayout.Bands[0].Columns["LibelleCodeEtat"].Header.Caption = "Libelle ";
            ssGridHisto.DisplayLayout.Bands[0].Columns["CodeEtatPDA"].Header.Caption = "Statut PDA";
            ssGridHisto.DisplayLayout.Bands[0].Columns["PDAB_Noauto"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["DatePrevue"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["Localisation"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["Adresse"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["CodePostal"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["Prenom"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["CodePostal"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["COP_NoContrat"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["COP_Noauto"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["ID_PDA"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["DevisEtablir"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["PDAB_IDBarre"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["CommentairePDA"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["Anomalie"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["PDAB_NoautoScanne"].Hidden = true;
            ssGridHisto.DisplayLayout.Bands[0].Columns["CompteurObli"].Header.Caption = "Compteur Obligatoire ";
            ssGridHisto.DisplayLayout.Bands[0].Columns["CompteurObli"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;

            ssGridHisto.DisplayLayout.Bands[0].Columns["CreeLe"].Format = "dd/MM/yyyy HH:mm:ss";

            this.ssGridHisto.DisplayLayout.Bands[0].Columns["Intervenant"].ValueList = this.ssDropMatricule;

            for (int i = 0; i < ssGridHisto.DisplayLayout.Bands[0].Columns.Count - 1; i++)
            {
                ssGridHisto.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.NoEdit;
            }

            ssGridHisto.DisplayLayout.Bands[0].Columns["HeureFin"].Format = "dd/MM/yyyy HH:mm:ss";
            ssGridHisto.DisplayLayout.Bands[0].Columns["HeureDebut"].Format = "dd/MM/yyyy HH:mm:ss";

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridHisto_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridHisto_InitializeRow() - Enter In The Function");
            //===> Fin Modif Mondir
            if (e.Row.Cells["CompteurObli"].Value == DBNull.Value)
            {
                e.Row.Cells["CompteurObli"].Value = false;
            }

            if (e.Row.Cells["HeureDebut"].Value != DBNull.Value)
            {

                if (e.Row.Cells["HeureDebut"].Value.ToString().Contains("01/01/1900"))
                {
                    e.Row.Cells["HeureDebut"].Column.Format = "dd/MM/yyyy";
                }
                else
                {
                    e.Row.Cells["HeureDebut"].Column.Format = "HH:mm:ss";
                }
            }

            if (e.Row.Cells["HeureFin"].Value != DBNull.Value)
            {
                if (e.Row.Cells["HeureFin"].Value.ToString().Contains("01/01/1900"))
                {
                    e.Row.Cells["HeureFin"].Column.Format = "dd/MM/yyyy";
                }
                else
                {
                    e.Row.Cells["HeureFin"].Column.Format = "HH:mm:ss";
                }
            }

            e.Row.Cells["CodeEtatPDA"].Appearance.ForeColor = Color.Red;
            e.Row.Cells["NoLigne"].Appearance.ForeColor = Color.Blue;
            e.Row.Cells["CodeImmeuble"].Appearance.ForeColor = Color.Blue;
            e.Row.Cells["Ville"].Appearance.ForeColor = Color.Blue;
            e.Row.Cells["Nom"].Appearance.ForeColor = Color.Blue;
            e.Row.Cells["CreeLe"].Appearance.ForeColor = Color.Blue;
            e.Row.Cells["CreePar"].Appearance.ForeColor = Color.Blue;
            e.Row.Cells["ModifierLe"].Appearance.ForeColor = Color.Blue;
            e.Row.Cells["ModifierPar"].Appearance.ForeColor = Color.Blue;
            e.Row.Cells["EnvoyeLe"].Appearance.ForeColor = Color.Blue;
            e.Row.Cells["EnvoyePar"].Appearance.ForeColor = Color.Blue;
            e.Row.Update();
            //ssGridHisto.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optOperationHisto_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"optOperationHisto_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            if (optOperationHisto.Checked)
            {
                if (ssGridHisto.ActiveRow != null)
                    fc_ssGRIDTachesAnnuelHisto(Convert.ToInt32(General.nz((ssGridHisto.ActiveRow.Cells["NoIntervention"].Value), 0)));
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optTachesHisto_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"optTachesHisto_CheckedChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            if (optTachesHisto.Checked)
            {
                if (ssGridHisto.ActiveRow != null)
                    fc_ssGRIDTachesAnnuelHisto(Convert.ToInt32(General.nz((ssGridHisto.ActiveRow.Cells["NoIntervention"].Value), 0)));

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void dtDateDebPlaningBisSimul_AfterEditorButtonCheckStateChanged(object sender, EditorButtonEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"dtDateDebPlaningBisSimul_AfterEditorButtonCheckStateChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            var btn = sender as UltraDateTimeEditor;
            var dtValue = btn.Value;
            if (((btn.ButtonsLeft[0]) as StateEditorButton).Checked == false)
            {
                btn.ReadOnly = true;
                btn.Value = dtValue;
                btn.Parent.BackColor = Color.LightGray;
                return;
            }
            btn.ReadOnly = false;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAppliquer0_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAppliquer0_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                var cmd = sender as Button;
                int Index = Convert.ToInt32(cmd.Tag);

                switch (Index)
                {

                    case 0:
                        if (optAM0.Checked == true)//tested
                        {
                            txtPeriode.Text = "AM";
                            General.saveInReg(ModConstantes.cUserInterP2V2, PL1.Name + "Echelle", "AM");

                        }
                        else if (optMS0.Checked == true)//tested
                        {
                            txtPeriode.Text = "MS";
                            General.saveInReg(ModConstantes.cUserInterP2V2, PL1.Name + "Echelle", "MS");

                        }
                        else if (optMJ0.Checked == true)//tested
                        {
                            txtPeriode.Text = "MJ";
                            General.saveInReg(ModConstantes.cUserInterP2V2, PL1.Name + "Echelle", "MJ");

                        }
                        else if (optJH0.Checked == true)//tested
                        {
                            txtPeriode.Text = "JH";
                            General.saveInReg(ModConstantes.cUserInterP2V2, PL1.Name + "Echelle", "JH");

                        }

                        fc_Echelle(txtPeriode.Text, PL1);

                        fc_LoadTailleEchelle(txtPeriode.Text, PL1);
                        break;

                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAppliquer_Click");
            }
        }

        private void ssGridVisite_AfterCellActivate(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridVisite_AfterCellActivate() - Enter In The Function");
            //===> Fin Modif Mondir
            ssGridTachesAnnuel.UpdateData();
            if (ssGridVisite.ActiveCell != null)
            {
                fc_ssGRIDTachesAnnuel(Convert.ToInt32(General.nz(ssGridVisite.ActiveRow.Cells["NumFicheStandard"].Value, 0).ToString()));
            }
        }

        /// <summary>
        /// Mondir le 22.09.2020 pour ajouté les modifs de la version 21.09.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDebloquer_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdDebloquer_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            string sSQL = "";
            try
            {
                if (MessageBox.Show("Voulez-vous débloquer la création des visites d'entretiens ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                sSQL = "UPDATE       P2PU_P2PlannUtil" +
                     " SET                P2PU_EnCours = 0," +
                     " P2PU_TerminePar = '" + General.Left(StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + " - Forcé", 50) + "'," +
                     " P2PU_TermineLe = '" + DateTime.Now + "'" +
                     " WHERE        (P2PU_EnCours = 1)";
                General.Execute(sSQL);

                MessageBox.Show("Mise à jour confirmé.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
            }
        }

        private void dtDateDebPlaningBisSimul_Validating(object sender, CancelEventArgs e)
        {

        }

        private void dtDateDebPlaningBisSimul_BeforeExitEditMode(object sender, Infragistics.Win.BeforeExitEditModeEventArgs e)
        {
            //var date = dtDateDebPlaningBisSimul.Text;
            //if (date.IsDate())
            //    return;
            //var newDate = date.GetCloseDateTime();
            //if (newDate != null)
            //{
            //    dtDateDebPlaningBisSimul.Value = newDate.Value;
            //}
        }

        private void dtDateDebPlaningBisSimul_AfterExitEditMode(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// //===> Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021 ===> Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmd_1_Click(object sender, EventArgs e)
        {
            string sFichier = "";
            string sWhere = "";

            var Index = (sender as Button).Tag.ToInt();

            try
            {
                switch (Index)
                {
                    case 0: //===> button cmd(0) not used
                        break;
                    case 1:
                        if (txt_0.Text != "")
                        {
                            if (sWhere == "")
                            {
                                sWhere = " WHERE Immeuble.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txt_0.Text) + "'";
                            }
                            else
                            {
                                sWhere = sWhere + " AND Immeuble.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txt_0.Text) + "'";
                            }
                        }

                        if (txt_1.Text != "")
                        {
                            if (sWhere == "")
                            {
                                sWhere = " WHERE Immeuble.Code1='" + StdSQLchaine.gFr_DoublerQuote(txt_1.Text) + "'";
                            }
                            else
                            {
                                sWhere = sWhere + " AND Immeuble.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(txt_1.Text) + "'";
                            }
                        }

                        if (txt_4.Text != "")
                        {
                            if (sWhere == "")
                            {
                                sWhere = " WHERE Immeuble.CodeChefSecteur='" + StdSQLchaine.gFr_DoublerQuote(txt_4.Text) + "'";
                            }
                            else
                            {
                                sWhere = sWhere + " AND Immeuble.CodeChefSecteur ='" + StdSQLchaine.gFr_DoublerQuote(txt_4.Text) + "'";
                            }
                        }

                        if (txt_2.Text != "")
                        {
                            if (sWhere == "")
                            {
                                sWhere = " WHERE  (Immeuble.CodeDepanneur='" + StdSQLchaine.gFr_DoublerQuote(txt_2.Text) + "'"
                                    + " OR (PEI_PeriodeGammeImm.PEI_IntAutre = 1 "
                                    + " and PEI_PeriodeGammeImm.PEI_Intervenant =" + StdSQLchaine.gFr_DoublerQuote(txt_2.Text) + " ))";
                            }
                            else
                            {
                                sWhere = sWhere + " AND (Immeuble.CodeDepanneur='" + StdSQLchaine.gFr_DoublerQuote(txt_2.Text) + "'"
                                    + " OR (PEI_PeriodeGammeImm.PEI_IntAutre = 1 "
                                    + " and PEI_PeriodeGammeImm.PEI_Intervenant =" + StdSQLchaine.gFr_DoublerQuote(txt_2.Text) + " ))";
                            }
                        }

                        GridGroupInterv.Visible = false;
                        GridGroupImm.Visible = false;
                        GridGroupSecteur.Visible = false;

                        //===> Mondir le 01.02.2021, ajout des modfis de la version V29.01.2021
                        Check_0.Visible = false;
                        Check_0.Checked = false;
                        txt_6.Visible = false;
                        Gridimm.Visible = false;
                        sMatRow = "";
                        txt_6.Text = "";
                        Label56_24.Text = "";
                        //===> Fin Modif Mondir

                        if (Opt_6.Checked) //=== => intervenant.
                        {
                            ModP2v2.fc_LoadParamGMAO(sWhere, 1, Label56_25);
                            GridGroupInterv.Visible = true;
                            fc_LoadGmaoInterv();

                            //===> Mondir le 01.02.2021, ajout des modfis de la version V29.01.2021
                            Check_0.Visible = true;
                            txt_6.Visible = true;
                            Gridimm.Visible = true;
                            if (Check_0.Checked)
                            {
                                fc_LoadImmGmao();
                            }
                            //===> Fin Modif Mondir
                        }
                        else if (Opt_7.Checked) //=== => immeuble.
                        {
                            ModP2v2.fc_LoadParamGMAO(sWhere, 2, Label56_25);
                            GridGroupImm.Visible = true;
                            fc_LoadGmaoImmeuble();
                        }
                        else if (Opt_8.Checked) //=== => chef de secteur.
                        {
                            ModP2v2.fc_LoadParamGMAO(sWhere, 3, Label56_25);
                            GridGroupSecteur.Visible = true;
                            //fc_LoadGmaoImmeuble sWhere
                            fc_LoadGmaoChefSecteur();

                            //===> Mondir le 01.02.2021, ajout des modfis de la version V29.01.2021
                            Check_0.Visible = true;
                            txt_6.Visible = true;
                            Gridimm.Visible = true;
                            if (Check_0.Checked)
                            {
                                fc_LoadImmGmao();
                            }
                            //===> Fin Modif Mondir
                        }
                        break;
                    case 2:

                        string requete = " SELECT Personnel.Matricule as \"Matricule\"," +
                                        " Personnel.Nom as \"Nom\"," +
                                        " Personnel.prenom as \"Prenom\"," +
                                        " Qualification.Qualification as \"Qualification\"," +
                                        " Personnel.NoPDA as \"NoPDA\"" +
                                        " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                        string where_order = " (Personnel.NonActif is null or Personnel.NonActif = 0)";
                        SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche intervenant" };
                        //fg.SetValues(new Dictionary<string, string> { { "column", iTalk_TextBox_Small21.Text } });
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txt_2.Text = fg.ugResultat.ActiveRow.Cells["Matricule"].Value.ToString();
                            txt_3.Text = fg.ugResultat.ActiveRow.Cells["NOM"].Value + " " + fg.ugResultat.ActiveRow.Cells["Prenom"].Value;
                            fg.Dispose();
                            fg.Close();
                        };

                        fg.ugResultat.KeyDown += (se, ev) =>
                        {

                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                txt_2.Text = fg.ugResultat.ActiveRow.Cells["Matricule"].Value.ToString();
                                txt_3.Text = fg.ugResultat.ActiveRow.Cells["NOM"].Value + " " + fg.ugResultat.ActiveRow.Cells["Prenom"].Value;
                                fg.Dispose();
                                fg.Close();
                            }
                        };
                        fg.StartPosition = FormStartPosition.CenterParent;
                        fg.ShowDialog();
                        break;
                    case 3:
                        string requete2 = "SELECT Personnel.Matricule as \"Matricule\"," +
                                         " Personnel.Nom as \"Nom\"," +
                                         " Personnel.prenom as \"Prenom\"," +
                                         " Qualification.Qualification as \"Qualification\"," +
                                         " Personnel.NoPDA as \"NoPDA\"" +
                                         " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                        string where_order2 = " (Personnel.NonActif is null or Personnel.NonActif = 0)";
                        SearchTemplate fg2 = new SearchTemplate(this, null, requete2, where_order2, "") { Text = "Recherche intervenant" };
                        //fg.SetValues(new Dictionary<string, string> { { "column", iTalk_TextBox_Small21.Text } });
                        fg2.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txt_4.Text = fg2.ugResultat.ActiveRow.Cells["Matricule"].Value.ToString();
                            txt_5.Text = fg2.ugResultat.ActiveRow.Cells["NOM"].Value + " " + fg2.ugResultat.ActiveRow.Cells["Prenom"].Value;
                            fg2.Dispose();
                            fg2.Close();
                        };

                        fg2.ugResultat.KeyDown += (se, ev) =>
                        {

                            if (ev.KeyCode == Keys.Enter && fg2.ugResultat.ActiveRow != null)
                            {
                                txt_4.Text = fg2.ugResultat.ActiveRow.Cells["Matricule"].Value.ToString();
                                txt_5.Text = fg2.ugResultat.ActiveRow.Cells["NOM"].Value + " " + fg2.ugResultat.ActiveRow.Cells["Prenom"].Value;
                                fg2.Dispose();
                                fg2.Close();
                            }
                        };
                        fg2.StartPosition = FormStartPosition.CenterParent;
                        fg2.ShowDialog();
                        break;
                    case 4:
                        string requete3 = "SELECT " + "CodeImmeuble as \"CodeImmeuble\", " +
                                          " Adresse as \"adresse\", Ville as \"Ville\"" +
                                          " , anglerue as \"AngleDeRue\"" +
                                          " FROM Immeuble ";
                        string where_order3 = "";
                        SearchTemplate fg3 = new SearchTemplate(this, null, requete3, where_order3, "") { Text = "Recherche d'immeuble" };
                        fg3.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txt_0.Text } });
                        fg3.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txt_0.Text = fg3.ugResultat.ActiveRow.Cells["CodeImmeuble"].Value.ToString();
                            fg3.Dispose();
                            fg3.Close();
                        };

                        fg3.ugResultat.KeyDown += (se, ev) =>
                        {

                            if (ev.KeyCode == Keys.Enter && fg3.ugResultat.ActiveRow != null)
                            {
                                txt_0.Text = fg3.ugResultat.ActiveRow.Cells["CodeImmeuble"].Value.ToString();
                                fg3.Dispose();
                                fg3.Close();
                            }
                        };
                        fg3.StartPosition = FormStartPosition.CenterParent;
                        fg3.ShowDialog();
                        break;
                    case 5:
                        string requete4 =
                            "SELECT " +
                            "Code1 as \"Nom Directeur\", Nom as \"Raison sociale\", Ville as \"Ville\" FROM Table1 ";
                        string where_order4 = "";
                        SearchTemplate fg4 = new SearchTemplate(this, null, requete4, where_order4, "") { Text = "Recherche client" };
                        fg4.SetValues(new Dictionary<string, string> { { "Code1", txt_1.Text } });
                        fg4.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txt_1.Text = fg4.ugResultat.ActiveRow.Cells["Nom Directeur"].Value.ToString();
                            fg4.Dispose();
                            fg4.Close();
                        };

                        fg4.ugResultat.KeyDown += (se, ev) =>
                        {

                            if (ev.KeyCode == Keys.Enter && fg4.ugResultat.ActiveRow != null)
                            {
                                txt_1.Text = fg4.ugResultat.ActiveRow.Cells["Nom Directeur"].Value.ToString();
                                fg4.Dispose();
                                fg4.Close();
                            }
                        };
                        fg4.StartPosition = FormStartPosition.CenterParent;
                        fg4.ShowDialog();
                        break;
                    case 6:
                        fc_exportExcelGMAO();
                        break;
                    case 7:
                        txt_0.Text = "";
                        txt_1.Text = "";
                        txt_2.Text = "";
                        txt_3.Text = "";
                        txt_4.Text = "";
                        txt_5.Text = "";
                        break;

                }
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
            }
        }

        /// <summary>
        /// Mondir le 01.02.2021, ajout des modfis de la version V29.01.2021 ===> TEsted
        /// </summary>
        private void fc_LoadImmGmao()
        {
            string sWhere = "";
            ModP2v2.ParamGMAOx[] tGMAO = null;
            ModP2v2.GoupImmeuble[] tpGoupImmeuble = null;
            int i;
            string sAdditem = "";

            try
            {
                if (Opt_8.Checked)
                {
                    if (GridGroupSecteur.ActiveRow?.Cells["CodeChefSecteur"].Text != "")
                    {
                        if (sMatRow.ToUpper() == GridGroupSecteur.ActiveRow?.Cells["CodeChefSecteur"].Text.ToUpper())
                        {
                            //== grille déja rafraichi.
                            return;
                        }

                        sWhere = " WHERE Immeuble.CodeChefSecteur='" + StdSQLchaine.gFr_DoublerQuote(GridGroupSecteur.ActiveRow.Cells["CodeChefSecteur"].Text) + "'";
                        sMatRow = GridGroupSecteur.ActiveRow.Cells["CodeChefSecteur"].Text;
                    }
                    else
                    {
                        return;
                    }
                }
                else if (Opt_6.Checked)
                {
                    if (GridGroupInterv.ActiveRow?.Cells["Intervenant"].Text != "")
                    {
                        if (sMatRow.ToUpper() == GridGroupInterv.ActiveRow?.Cells["Intervenant"].Text.ToUpper())
                        {
                            //== grille déja rafraichi.
                            return;
                        }

                        sWhere = " WHERE  (Immeuble.CodeDepanneur='" + StdSQLchaine.gFr_DoublerQuote(GridGroupInterv.ActiveRow.Cells["Intervenant"].Text) + "'"
                                 + " OR (PEI_PeriodeGammeImm.PEI_IntAutre = 1 "
                                 + " and PEI_PeriodeGammeImm.PEI_Intervenant =" +
                                 StdSQLchaine.gFr_DoublerQuote(GridGroupInterv.ActiveRow.Cells["Intervenant"].Text) + " ))";
                        sMatRow = GridGroupInterv.ActiveRow.Cells["Intervenant"].Text;
                    }
                    else
                    {
                        return;
                    }
                }
                else
                {
                    return;
                }

                tGMAO = ModP2v2.fc_LoadParamGMAO(sWhere, 4, Label56_25);

                tpGoupImmeuble = ModP2v2.fc_GroupImmeuble(tGMAO, Label56_25, true);

                var GridimmDataSource = Gridimm.DataSource as DataTable;
                GridimmDataSource?.Rows.Clear();

                if (GridimmDataSource != null)
                {
                    for (i = 0; i < tpGoupImmeuble?.Length; i++)
                    {
                        var row = GridimmDataSource.NewRow();
                        row["Codeimmeuble"] = tpGoupImmeuble[i].sCodeImmeuble;
                        row["JanvDuree"] = tpGoupImmeuble[i].dbDurJanv;
                        row["FevDuree"] = tpGoupImmeuble[i].dbDurFev;
                        row["MarsDuree"] = tpGoupImmeuble[i].dbDurMars;
                        row["AvrilDuree"] = tpGoupImmeuble[i].dbDurAvril;
                        row["MaiDuree"] = tpGoupImmeuble[i].dbDurMai;
                        row["JuinDuree"] = tpGoupImmeuble[i].dbDurJuin;
                        row["JuilletDuree"] = tpGoupImmeuble[i].dbDurJuillet;
                        row["AoutDuree"] = tpGoupImmeuble[i].dbDurAout;
                        row["SepDuree"] = tpGoupImmeuble[i].dbDurSept;
                        row["OctDuree"] = tpGoupImmeuble[i].dbDurOct;
                        row["NovDuree"] = tpGoupImmeuble[i].dbDurNov;
                        row["DecDuree"] = tpGoupImmeuble[i].dbDurDec;
                        GridimmDataSource.Rows.Add(row);
                    }

                    Gridimm.DataSource = GridimmDataSource;
                }

                if (Opt_8.Checked)
                {
                    if (GridGroupSecteur.ActiveRow != null)
                    {
                        txt_6.Text = GridGroupSecteur.ActiveRow.Cells["CodeChefSecteur"].Text + " - " + GridGroupSecteur.ActiveRow.Cells["NomChefSecteur"].Text + " - " + tpGoupImmeuble.Length + " site(s)";
                    }
                }
                else if (Opt_6.Checked)
                {
                    if (GridGroupInterv.ActiveRow != null)
                    {
                        txt_6.Text = GridGroupInterv.ActiveRow.Cells["Intervenant"].Text + " - " + GridGroupInterv.ActiveRow.Cells["NomIntervenant"].Text + " - " + tpGoupImmeuble.Length + " site(s)";
                    }
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021
        /// </summary>
        private void fc_exportExcelGMAO()
        {
            Excel.Application oXL;
            Excel.Workbook oWB;
            Excel.Worksheet oSheet;
            Excel.Range oRng;
            Excel.Range oResizeRange;
            DataTable rsExport = null;
            SqlDataAdapter SDArsExport = null;
            SqlCommandBuilder SCBrsExport = null;
            //DataColumn oField;
            int i;
            int j;
            int lLigne;
            string sMatricule = "";
            const int cCol = 0;
            bool bok;
            int lTot = 0;

            try
            {
                Cursor = Cursors.WaitCursor;
                //' Start Excel and get Application object.
                oXL = new Excel.Application();
                oXL.Visible = false;

                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;

                rsExport = new DataTable();
                if (Opt_6.Checked)
                {
                    rsExport = rsGMAOinter;
                }
                else if (Opt_7.Checked)
                {
                    rsExport = rsGMAOimmeuble;
                }
                else if (Opt_8.Checked)
                {
                    rsExport = rsGMAOcds;
                }

                if (rsExport == null || rsExport.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                        "Aucun enregistrement pour ces critères.", "", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    return;
                }

                lTot = rsExport.Rows.Count;

                Label56_25.Text = "Export en cours";

                //== affiche les noms des champs.

                i = 1;
                foreach (DataColumn oField in rsExport.Columns)
                {
                    oSheet.Cells[1, i].VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Cells[1, i].value = oField.ColumnName;
                    i++;
                }

                Cursor = Cursors.WaitCursor;

                //Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];

                oResizeRange.Interior.ColorIndex = 15;
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Excel.XlBorderWeight.xlThin;
                oResizeRange.Borders.Weight = 1;

                lLigne = 2;

                Cursor = Cursors.WaitCursor;
                foreach (DataRow rsExportRow in rsExport.Rows)
                {
                    j = 1;
                    foreach (DataColumn rsExportColumn in rsExport.Columns)
                    {
                        oSheet.Cells[lLigne, j + cCol].value = rsExportRow[rsExportColumn.ColumnName];
                        j++;
                    }

                    Label56_25.Text = "Export en cours - " + (lLigne - 1) + "/" + lTot;
                    lLigne++;
                    Application.DoEvents();
                }

                // 'formate la ligne en gras.
                oResizeRange.Font.Bold = true;
                oXL.Visible = true;
                oXL.UserControl = true;
                oRng = null;
                oSheet = null;
                oWB = null;
                oXL = null;
                Cursor = Cursors.Default;
                rsExport = null;

                Label56_25.Text = "Export terminée";
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021 ===> Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridGroupImm_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            if (GridGroupImm.ActiveRow == null)
                return;

            string sSQLgmao = "";
            sSQLgmao = "SELECT     COP_NoAuto From COP_ContratP2 WHERE  codeimmeuble = '" +
                       StdSQLchaine.gFr_DoublerQuote(GridGroupImm.ActiveRow.Cells["codeimmeuble"].Text) +
                       "' ORDER BY COP_NoAuto DESC";
            using (var tmpModAdo = new ModAdo())
                sSQLgmao = tmpModAdo.fc_ADOlibelle(sSQLgmao);

            if (!sSQLgmao.IsNullOrEmpty())
            {
                ModParametre.fc_SaveParamPosition(ModConstantes.cUserInterP2V2, Variable.cUserDocImmeuble,
                    GridGroupImm.ActiveRow.Cells["codeimmeuble"].Text);
                ModParametre.fc_SaveParamPosition("", Variable.cUserDocP2, sSQLgmao);
                //===> Mondir le 01.02.2021, ased Rachid, UserDocP2 not used
                //View.Theme.Theme.Navigate(typeof(UserDocP2));
            }
            else
            {
                CustomMessageBox.Show("Il n'y a pas de fiche GMAO associée à cet immeuble.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }
        }

        //===================================
        //Private Sub GridGroupImm_HeadClick(ByVal ColIndex As Integer)
        //Dim sNomColone          As String

        //If ColIndex = iOldCol Then
        //    bTriAsc = Not bTriAsc
        //    Else
        //bTriAsc = True
        //    End If

        //    sNomColone = UCase(GridGroupImm.Columns(ColIndex).Name)


        //sOrderByGAMOimmeuble = " Order By " & GridGroupImm.Columns(ColIndex).Name & IIf(bTriAsc, " ", " Desc")

        //Screen.MousePointer = 11

        //fc_LoadGmaoImmeuble

        //    Screen.MousePointer = 1

        //iOldCol = ColIndex
        //    End Sub
        //===================================

        //===================================
        //Private Sub GridGroupInterv_HeadClick(ByVal ColIndex As Integer)
        //Dim sNomColone          As String

        //If ColIndex = iOldCol Then
        //    bTriAsc = Not bTriAsc
        //    Else
        //bTriAsc = True
        //    End If

        //    sNomColone = UCase(GridGroupInterv.Columns(ColIndex).Name)


        //sOrderByGAMOinterv = " Order By " & GridGroupInterv.Columns(ColIndex).Name & IIf(bTriAsc, " ", " Desc")

        //Screen.MousePointer = 11

        //fc_LoadGmaoInterv

        //    Screen.MousePointer = 1

        //iOldCol = ColIndex
        //    End Sub
        //===================================


        //===================================
        //Private Sub GridGroupSecteur_HeadClick(ByVal ColIndex As Integer)
        //Dim sNomColone          As String

        //If ColIndex = iOldCol Then
        //    bTriAsc = Not bTriAsc
        //    Else
        //bTriAsc = True
        //    End If

        //    sNomColone = UCase(GridGroupSecteur.Columns(ColIndex).Name)


        //sOrderByGAMOSecteur = " Order By " & GridGroupSecteur.Columns(ColIndex).Name & IIf(bTriAsc, " ", " Desc")

        //Screen.MousePointer = 11

        //fc_LoadGmaoChefSecteur

        //    Screen.MousePointer = 1

        //iOldCol = ColIndex
        //    End Sub
        //===================================

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021 ===> TEsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_2_TextChanged(object sender, EventArgs e)
        {
            var Index = (sender as iTalk_TextBox_Small2).Tag.ToInt();
            try
            {
                var sSQl = "";
                switch (Index)
                {
                    case 2:
                        sSQl = "SELECT   TOP (1) Nom, Prenom From Personnel WHERE Matricule = '" +
                               StdSQLchaine.gFr_DoublerQuote(txt_2.Text) + "'";
                        using (var tmpModAdo = new ModAdo())
                            txt_3.Text = tmpModAdo.fc_ADOlibelle(sSQl);
                        break;
                    case 4:
                        sSQl = "SELECT   TOP (1) Nom, Prenom From Personnel WHERE Matricule = '" + StdSQLchaine.gFr_DoublerQuote(txt_4.Text) + "'";
                        using (var tmpModAdo = new ModAdo())
                            txt_5.Text = tmpModAdo.fc_ADOlibelle(sSQl);
                        break;
                }
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
            }
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021 ====> Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txt_0_KeyPress(object sender, KeyPressEventArgs e)
        {
            var Index = (sender as iTalk_TextBox_Small2).Tag.ToInt();

            try
            {
                switch (Index)
                {
                    case 0:
                        if ((int)e.KeyChar == 13)
                        {
                            cmd_1_Click(cmd_4, null);
                        }
                        break;
                    case 1:
                        if ((int)e.KeyChar == 13)
                        {
                            cmd_1_Click(cmd_5, null);
                        }
                        break;
                    case 2:
                        if ((int)e.KeyChar == 13)
                        {
                            cmd_1_Click(cmd_2, null);
                        }
                        break;
                    case 4:
                        if ((int)e.KeyChar == 13)
                        {
                            cmd_1_Click(cmd_3, null);
                        }
                        break;
                }
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
            }
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021 ===> Tested
        /// </summary>
        private void fc_LoadGmaoInterv()
        {
            string sSQl = "";

            try
            {
                sSQl =
                    "SELECT     ID, Intervenant, NomIntervenant, ChefDeSecteur, NomChefDeSecteur, JanvDuree, FevDuree,"
                    + " MarsDuree, AvrilDuree, MaiDuree, JuinDuree, JuilletDuree, AoutDuree, SepDuree, OctDuree, NovDuree,"
                    + " DecDuree, CreePar,"
                    + "  CreeLe "
                    + " FROM            Temp_GMAOChargeIntervenant"
                    + " WHERE CreePar ='" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'";

                sSQl = sSQl + sOrderByGAMOinterv;

                Cursor = Cursors.WaitCursor;

                rsGMAOinter = rsGMAOinterModAdo.fc_OpenRecordSet(sSQl);

                Cursor = Cursors.Arrow;
                GridGroupInterv.DataSource = rsGMAOinter;

                Label56_24.Text = rsGMAOinter.Rows.Count.ToString();
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridGroupInterv_AfterRowUpdate(object sender, RowEventArgs e)
        {
            rsGMAOinterModAdo.Update();
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridGroupInterv_AfterRowsDeleted(object sender, EventArgs e)
        {
            rsGMAOinterModAdo.Update();
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridGroupInterv_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = false;
            }
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021 ===> Tested
        /// </summary>
        private void fc_LoadGmaoImmeuble()
        {
            string sSQl = "";

            try
            {
                sSQl = "SELECT     ID, Codeimmeuble, JanvDuree, FevDuree,"
                       + " MarsDuree, AvrilDuree, MaiDuree, JuinDuree, JuilletDuree, AoutDuree, SepDuree, OctDuree, NovDuree,"
                       + " DecDuree, CreePar,"
                       + "  CreeLe "
                       + " FROM            Temp_GMAOChargeImmeuble";

                sSQl = sSQl + " WHERE CreePar ='" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'";


                sSQl = sSQl + sOrderByGAMOimmeuble;

                Cursor = Cursors.WaitCursor;

                rsGMAOimmeuble = rsGMAOimmeubleModAdo.fc_OpenRecordSet(sSQl);

                Cursor = Cursors.Arrow;

                GridGroupImm.DataSource = rsGMAOimmeuble;

                Label56_24.Text = GridGroupImm.Rows.Count.ToString();
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridGroupImm_AfterRowUpdate(object sender, RowEventArgs e)
        {
            rsGMAOimmeubleModAdo.Update();
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridGroupImm_AfterRowsDeleted(object sender, EventArgs e)
        {
            rsGMAOimmeubleModAdo.Update();
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridGroupImm_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = false;
            }
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021 ===> Tested
        /// </summary>
        private void fc_LoadGmaoChefSecteur()
        {
            string sSQl = "";

            try
            {

                sSQl = "SELECT     ID, CodeChefSecteur, NomChefSecteur, JanvDuree, FevDuree,"
                    + " MarsDuree, AvrilDuree, MaiDuree, JuinDuree, JuilletDuree, AoutDuree, SepDuree, OctDuree, NovDuree,"
                    + " DecDuree, CreePar,"
                    + "  CreeLe "
                    + " FROM            Temp_GMAOChargeChefSecteur";

                sSQl = sSQl + " WHERE CreePar ='" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'";


                sSQl = sSQl + sOrderByGAMOSecteur;

                Cursor = Cursors.WaitCursor;

                rsGMAOcds = rsGMAOcdsModAdo.fc_OpenRecordSet(sSQl);

                Cursor = Cursors.Arrow;

                GridGroupSecteur.DataSource = rsGMAOcds;

                Label56_24.Text = rsGMAOcds.Rows.Count.ToString();
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridGroupSecteur_AfterRowUpdate(object sender, RowEventArgs e)
        {
            rsGMAOcdsModAdo.Update();
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridGroupSecteur_AfterRowsDeleted(object sender, EventArgs e)
        {
            rsGMAOcdsModAdo.Update();
        }

        /// <summary>
        /// Mondir le 29.01.2021, ajout des modfis de la version V27.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridGroupSecteur_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = false;
            }
        }

        /// <summary>
        /// Mondir le 01.02.2021, ajout des modfis de la version V29.01.2021 ===> Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Check_0_CheckedChanged(object sender, EventArgs e)
        {
            if (Check_0.Checked)
            {
                fc_LoadImmGmao();
            }
        }

        /// <summary>
        /// Mondir le 01.02.2021, ajout des modfis de la version V29.01.2021
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridGroupInterv_AfterCellActivate(object sender, EventArgs e)
        {
            if (Check_0.Checked)
            {
                fc_LoadImmGmao();
            }
        }

        /// <summary>
        /// Mondir le 01.02.2021, ajout des modfis de la version V29.01.2021 ===> TEsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridGroupSecteur_AfterCellActivate(object sender, EventArgs e)
        {
            if (Check_0.Checked)
            {
                fc_LoadImmGmao();
            }
        }

        private void Gridimm_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["Codeimmeuble"].Header.Caption = "Code immeuble";
            e.Layout.Bands[0].Columns["JanvDuree"].Header.Caption = "Janvier";
            e.Layout.Bands[0].Columns["FevDuree"].Header.Caption = "Février";
            e.Layout.Bands[0].Columns["MarsDuree"].Header.Caption = "Mars";
            e.Layout.Bands[0].Columns["AvrilDuree"].Header.Caption = "Avril";
            e.Layout.Bands[0].Columns["MaiDuree"].Header.Caption = "Mai";
            e.Layout.Bands[0].Columns["JuinDuree"].Header.Caption = "Juin";
            e.Layout.Bands[0].Columns["JuilletDuree"].Header.Caption = "Juillet";
            e.Layout.Bands[0].Columns["AoutDuree"].Header.Caption = "Août";
            e.Layout.Bands[0].Columns["SepDuree"].Header.Caption = "Septembre";
            e.Layout.Bands[0].Columns["OctDuree"].Header.Caption = "Octobre";
            e.Layout.Bands[0].Columns["NovDuree"].Header.Caption = "Novembre";
            e.Layout.Bands[0].Columns["DecDuree"].Header.Caption = "Décembre";
        }

        private void GridGroupSecteur_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["ID"].Hidden = true;
            e.Layout.Bands[0].Columns["CodeChefSecteur"].Header.Caption = "Code Chef Secteur";
            e.Layout.Bands[0].Columns["NomChefSecteur"].Header.Caption = "Nom Chef Secteur";
            e.Layout.Bands[0].Columns["JanvDuree"].Header.Caption = "Janvier";
            e.Layout.Bands[0].Columns["FevDuree"].Header.Caption = "Février";
            e.Layout.Bands[0].Columns["MarsDuree"].Header.Caption = "Mars";
            e.Layout.Bands[0].Columns["AvrilDuree"].Header.Caption = "Avril";
            e.Layout.Bands[0].Columns["MaiDuree"].Header.Caption = "Mai";
            e.Layout.Bands[0].Columns["JuinDuree"].Header.Caption = "Juin";
            e.Layout.Bands[0].Columns["JuilletDuree"].Header.Caption = "Juillet";
            e.Layout.Bands[0].Columns["AoutDuree"].Header.Caption = "Août";
            e.Layout.Bands[0].Columns["SepDuree"].Header.Caption = "Septembre";
            e.Layout.Bands[0].Columns["OctDuree"].Header.Caption = "Octobre";
            e.Layout.Bands[0].Columns["NovDuree"].Header.Caption = "Novembre";
            e.Layout.Bands[0].Columns["DecDuree"].Header.Caption = "Décembre";
            e.Layout.Bands[0].Columns["CreePar"].Hidden = true;
            e.Layout.Bands[0].Columns["CreeLe"].Hidden = true;
        }

        private void GridGroupInterv_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["ID"].Hidden = true;
            e.Layout.Bands[0].Columns["Intervenant"].Header.Caption = "Intervenant";
            e.Layout.Bands[0].Columns["NomIntervenant"].Header.Caption = "Nom Intervenant";
            //e.Layout.Bands[0].Columns["CodeChefSecteur"].Header.Caption = "Code Chef Secteur";
            //e.Layout.Bands[0].Columns["NomChefSecteur"].Header.Caption = "Nom Chef Secteur";
            e.Layout.Bands[0].Columns["JanvDuree"].Header.Caption = "Janvier";
            e.Layout.Bands[0].Columns["FevDuree"].Header.Caption = "Février";
            e.Layout.Bands[0].Columns["MarsDuree"].Header.Caption = "Mars";
            e.Layout.Bands[0].Columns["AvrilDuree"].Header.Caption = "Avril";
            e.Layout.Bands[0].Columns["MaiDuree"].Header.Caption = "Mai";
            e.Layout.Bands[0].Columns["JuinDuree"].Header.Caption = "Juin";
            e.Layout.Bands[0].Columns["JuilletDuree"].Header.Caption = "Juillet";
            e.Layout.Bands[0].Columns["AoutDuree"].Header.Caption = "Août";
            e.Layout.Bands[0].Columns["SepDuree"].Header.Caption = "Septembre";
            e.Layout.Bands[0].Columns["OctDuree"].Header.Caption = "Octobre";
            e.Layout.Bands[0].Columns["NovDuree"].Header.Caption = "Novembre";
            e.Layout.Bands[0].Columns["DecDuree"].Header.Caption = "Décembre";
            e.Layout.Bands[0].Columns["CreePar"].Hidden = true;
            e.Layout.Bands[0].Columns["CreeLe"].Hidden = true;
        }
    }
}
