namespace Axe_interDT.Views.FicheVisiteEntretient
{
    partial class UserIntervP2V2
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserIntervP2V2));
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton1 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton2 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton3 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton4 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton5 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton6 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton7 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton8 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton9 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton10 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton11 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton12 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton13 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton14 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab7 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab4 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab5 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab6 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab8 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl5 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.ssOpeTaches = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ssGridTachesAnnuel = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtErreur = new iTalk.iTalk_RichTextBox();
            this.label12 = new System.Windows.Forms.Label();
            this.ssGridVisite = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.optOperation = new System.Windows.Forms.RadioButton();
            this.optTaches = new System.Windows.Forms.RadioButton();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdCocher = new System.Windows.Forms.Button();
            this.Cmd18 = new System.Windows.Forms.Button();
            this.Cmd17 = new System.Windows.Forms.Button();
            this.ultraTabPageControl6 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdAppliquer0 = new System.Windows.Forms.Button();
            this.groupBox5 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.label37 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label36 = new System.Windows.Forms.Label();
            this.optJH0 = new System.Windows.Forms.RadioButton();
            this.groupBox10 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.Text10 = new iTalk.iTalk_TextBox_Small2();
            this.cmdTailleMoins = new System.Windows.Forms.Button();
            this.cmdTaillePlus = new System.Windows.Forms.Button();
            this.Text2 = new iTalk.iTalk_TextBox_Small2();
            this.optMJ0 = new System.Windows.Forms.RadioButton();
            this.optAM0 = new System.Windows.Forms.RadioButton();
            this.optMS0 = new System.Windows.Forms.RadioButton();
            this.PL1 = new AxPLANNINGLib.AxPlanning();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.ssPlanCharge = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ssOpTachesSimul = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ssGridTachesAnnuelSimul = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label4 = new System.Windows.Forms.Label();
            this.chkPEI_APlanifier = new System.Windows.Forms.CheckBox();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.dtDateDebPlaningBisSimul = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtDateFinPlaningBisSimul = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtCodeImmeubleSimul = new iTalk.iTalk_TextBox_Small2();
            this.txtIntervenantSimul = new iTalk.iTalk_TextBox_Small2();
            this.txtCOP_NoautoSimul = new iTalk.iTalk_TextBox_Small2();
            this.cmdRechercheImmeubleSimul = new System.Windows.Forms.Button();
            this.cmdFindStraitantSimul = new System.Windows.Forms.Button();
            this.cmdFindGMAO = new System.Windows.Forms.Button();
            this.lblTotalSimul = new iTalk.iTalk_TextBox_Small2();
            this.frame117 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.optEQU_P1CompteurSimul2 = new System.Windows.Forms.RadioButton();
            this.optEQU_P1CompteurSimul0 = new System.Windows.Forms.RadioButton();
            this.optEQU_P1CompteurSimul1 = new System.Windows.Forms.RadioButton();
            this.groupBox11 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.OptPrestation = new System.Windows.Forms.RadioButton();
            this.OptIntervention = new System.Windows.Forms.RadioButton();
            this.lbllibIntervenantSimul = new iTalk.iTalk_TextBox_Small2();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.frame115 = new System.Windows.Forms.GroupBox();
            this.optVisites = new System.Windows.Forms.RadioButton();
            this.optListeTaches = new System.Windows.Forms.RadioButton();
            this.txtPeriode = new iTalk.iTalk_TextBox_Small2();
            this.chkLecture = new System.Windows.Forms.CheckBox();
            this.lblLibIntervSecteur = new iTalk.iTalk_TextBox_Small2();
            this.txtUtilVerrou = new iTalk.iTalk_TextBox_Small2();
            this.label45 = new System.Windows.Forms.Label();
            this.cmdIntervSecteur = new System.Windows.Forms.Button();
            this.txtIntervSecteur = new iTalk.iTalk_TextBox_Small2();
            this.ssGridVisiteSimul = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.SSOleDBGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.optTachesSimul = new System.Windows.Forms.RadioButton();
            this.optPlanCharge = new System.Windows.Forms.RadioButton();
            this.optOperationSimul = new System.Windows.Forms.RadioButton();
            this.txtTotduree = new iTalk.iTalk_TextBox_Small2();
            this.label6 = new System.Windows.Forms.Label();
            this.pctPlanCharge = new System.Windows.Forms.Panel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.optPlanChargeChantier = new System.Windows.Forms.RadioButton();
            this.optPlanChargeIntervenant = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.frame1110 = new System.Windows.Forms.GroupBox();
            this.cmdPrestationSimul = new System.Windows.Forms.Button();
            this.cmdMotifSimul = new System.Windows.Forms.Button();
            this.cmdRechercheContratSimul = new System.Windows.Forms.Button();
            this.txtPrestationSimul = new iTalk.iTalk_TextBox_Small2();
            this.txtCOP_AvenantSimul = new iTalk.iTalk_TextBox_Small2();
            this.txtCOP_NoContratSimul = new iTalk.iTalk_TextBox_Small2();
            this.txtCAI_CodeSimul = new iTalk.iTalk_TextBox_Small2();
            this.label41 = new System.Windows.Forms.Label();
            this.label40 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdSimulation = new System.Windows.Forms.Button();
            this.CmdEditer = new System.Windows.Forms.Button();
            this.cmdCleanSimul = new System.Windows.Forms.Button();
            this.cmdAfficheSimul = new System.Windows.Forms.Button();
            this.ultraTabPageControl7 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.cmd_3 = new System.Windows.Forms.Button();
            this.cmd_2 = new System.Windows.Forms.Button();
            this.cmd_5 = new System.Windows.Forms.Button();
            this.cmd_4 = new System.Windows.Forms.Button();
            this.txt_1 = new iTalk.iTalk_TextBox_Small2();
            this.label47 = new System.Windows.Forms.Label();
            this.label46 = new System.Windows.Forms.Label();
            this.label44 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txt_0 = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.txt_3 = new iTalk.iTalk_TextBox_Small2();
            this.txt_2 = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.txt_5 = new iTalk.iTalk_TextBox_Small2();
            this.txt_4 = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.Label56_25 = new iTalk.iTalk_TextBox_Small2();
            this.groupBox13 = new System.Windows.Forms.GroupBox();
            this.Opt_8 = new System.Windows.Forms.RadioButton();
            this.Opt_7 = new System.Windows.Forms.RadioButton();
            this.Opt_6 = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmd_7 = new System.Windows.Forms.Button();
            this.cmd_6 = new System.Windows.Forms.Button();
            this.cmd_1 = new System.Windows.Forms.Button();
            this.Label56_24 = new iTalk.iTalk_TextBox_Small2();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Gridimm = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txt_6 = new iTalk.iTalk_TextBox_Small2();
            this.Check_0 = new System.Windows.Forms.CheckBox();
            this.GridGroupInterv = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.GridGroupImm = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.GridGroupSecteur = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.SSTab2 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage2 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.frame1112 = new System.Windows.Forms.FlowLayoutPanel();
            this.Opt3 = new System.Windows.Forms.RadioButton();
            this.Opt4 = new System.Windows.Forms.RadioButton();
            this.Opt5 = new System.Windows.Forms.RadioButton();
            this.dtDateDebPlaning = new iTalk.iTalk_TextBox_Small2();
            this.label13 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtIntervenant = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.txtCOP_NoAuto = new iTalk.iTalk_TextBox_Small2();
            this.cmdintervenant = new System.Windows.Forms.Button();
            this.cmdRechercheImmeuble = new System.Windows.Forms.Button();
            this.cmdFindGmaoCreat = new System.Windows.Forms.Button();
            this.frame118 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.OptEQU_P1Compteur2 = new System.Windows.Forms.RadioButton();
            this.OptEQU_P1Compteur0 = new System.Windows.Forms.RadioButton();
            this.OptEQU_P1Compteur1 = new System.Windows.Forms.RadioButton();
            this.lbltotal2 = new iTalk.iTalk_TextBox_Small2();
            this.cmbStatutPDA = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtMoisPlanif = new iTalk.iTalk_TextBox_Small2();
            this.dtDateDebPlaningBis = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtDateFinPlaningBis = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtDateFinPlaning = new iTalk.iTalk_TextBox_Small2();
            this.lbllibIntervenant = new iTalk.iTalk_TextBox_Small2();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.Command3 = new System.Windows.Forms.Button();
            this.cmdVisites = new System.Windows.Forms.Button();
            this.cdmSend2PDA = new System.Windows.Forms.Button();
            this.CmdLegende0 = new System.Windows.Forms.Button();
            this.cmdListing = new System.Windows.Forms.Button();
            this.cmdDebloquer = new System.Windows.Forms.Button();
            this.cmdExportPlan = new System.Windows.Forms.Button();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.cmdCleanVisites = new System.Windows.Forms.Button();
            this.cmdAffichePlan = new System.Windows.Forms.Button();
            this.Frame13 = new System.Windows.Forms.GroupBox();
            this.chkDevisEtablir = new System.Windows.Forms.CheckBox();
            this.chkMatRemplace = new System.Windows.Forms.CheckBox();
            this.chkTrxUrgent = new System.Windows.Forms.CheckBox();
            this.chkAnomalie = new System.Windows.Forms.CheckBox();
            this.chkCommGardien = new System.Windows.Forms.CheckBox();
            this.chkInterIncompléte = new System.Windows.Forms.CheckBox();
            this.txtStatutHisto = new iTalk.iTalk_TextBox_Small2();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.ssGAvisDEPassage = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.groupBox7 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.label18 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtCOP_NoAutoAvis = new iTalk.iTalk_TextBox_Small2();
            this.Command6 = new System.Windows.Forms.Button();
            this.Command5 = new System.Windows.Forms.Button();
            this.txtCodeImmeubleAvis = new iTalk.iTalk_TextBox_Small2();
            this.label16 = new System.Windows.Forms.Label();
            this.txtIntervenantAvis = new iTalk.iTalk_TextBox_Small2();
            this.lbllibIntervenantAvis = new iTalk.iTalk_TextBox_Small2();
            this.Command7 = new System.Windows.Forms.Button();
            this.label17 = new System.Windows.Forms.Label();
            this.dtDateDebPlaningAvis = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtDateFinPlaningAvis = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.button1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox8 = new System.Windows.Forms.GroupBox();
            this.ssGridTachesAnnuelHisto = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ssOpeTachesHisto = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.groupBox9 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.label21 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.dtDateVisiteLe = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtDateVisiteAu = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.txtCodeImmeubleHisto = new iTalk.iTalk_TextBox_Small2();
            this.txtMatEntretien = new iTalk.iTalk_TextBox_Small2();
            this.txtIntervenantHisto = new iTalk.iTalk_TextBox_Small2();
            this.frame119 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.OptEQU_P1CompteurHisto2 = new System.Windows.Forms.RadioButton();
            this.OptEQU_P1CompteurHisto0 = new System.Windows.Forms.RadioButton();
            this.OptEQU_P1CompteurHisto1 = new System.Windows.Forms.RadioButton();
            this.lblTotalHisto = new iTalk.iTalk_TextBox_Small2();
            this.label25 = new System.Windows.Forms.Label();
            this.label26 = new System.Windows.Forms.Label();
            this.label27 = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.DTDatePrevueDe = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.DTDateRealiseDe = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.DTDatePrevueAu = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.DTDateRealiseAu = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.DTCreeParDe = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtEnvoyeLeHisto1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtCreeParAu = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.dtEnvoyeLeHisto2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.label31 = new System.Windows.Forms.Label();
            this.label32 = new System.Windows.Forms.Label();
            this.cmdIntervHisto = new System.Windows.Forms.Button();
            this.cmdMatEntretien = new System.Windows.Forms.Button();
            this.cmdImmeubleHisto = new System.Windows.Forms.Button();
            this.label24 = new System.Windows.Forms.Label();
            this.label34 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.cmbStatutPDAhisto = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtNoInterventionHisto = new iTalk.iTalk_TextBox_Small2();
            this.txtCode1Histo = new iTalk.iTalk_TextBox_Small2();
            this.cmdFindNoInter = new System.Windows.Forms.Button();
            this.cmdClient = new System.Windows.Forms.Button();
            this.lblLibEntretien = new iTalk.iTalk_TextBox_Small2();
            this.lbllibIntervenantHisto = new iTalk.iTalk_TextBox_Small2();
            this.frame1113 = new System.Windows.Forms.FlowLayoutPanel();
            this.Opt2 = new System.Windows.Forms.RadioButton();
            this.Opt1 = new System.Windows.Forms.RadioButton();
            this.Opt0 = new System.Windows.Forms.RadioButton();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdinitHistorique = new System.Windows.Forms.Button();
            this.Command2 = new System.Windows.Forms.Button();
            this.cmdExportHisto = new System.Windows.Forms.Button();
            this.cmdHistoRamonage = new System.Windows.Forms.Button();
            this.CmdLegende2 = new System.Windows.Forms.Button();
            this.cmdRapportPDA = new System.Windows.Forms.Button();
            this.groupBox12 = new System.Windows.Forms.GroupBox();
            this.ssDropMatricule = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ssGridHisto = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.optTachesHisto = new System.Windows.Forms.RadioButton();
            this.optOperationHisto = new System.Windows.Forms.RadioButton();
            this.ultraTabPageControl8 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.frame1111 = new System.Windows.Forms.GroupBox();
            this.label43 = new System.Windows.Forms.Label();
            this.label42 = new System.Windows.Forms.Label();
            this.cmdRechercheContrat = new System.Windows.Forms.Button();
            this.txtCOP_NoContrat = new iTalk.iTalk_TextBox_Small2();
            this.SSTab1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.ultraTabPageControl5.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.groupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssOpeTaches)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridTachesAnnuel)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridVisite)).BeginInit();
            this.flowLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.ultraTabPageControl6.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.groupBox5.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            this.groupBox10.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.PL1)).BeginInit();
            this.ultraTabPageControl1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssPlanCharge)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssOpTachesSimul)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridTachesAnnuelSimul)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateDebPlaningBisSimul)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateFinPlaningBisSimul)).BeginInit();
            this.frame117.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.groupBox11.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.frame115.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridVisiteSimul)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            this.pctPlanCharge.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.frame1110.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.ultraTabPageControl7.SuspendLayout();
            this.tableLayoutPanel22.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            this.tableLayoutPanel26.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            this.groupBox13.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Gridimm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridGroupInterv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridGroupImm)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridGroupSecteur)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab2)).BeginInit();
            this.SSTab2.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.frame1112.SuspendLayout();
            this.frame118.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatutPDA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateDebPlaningBis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateFinPlaningBis)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            this.Frame13.SuspendLayout();
            this.ultraTabPageControl3.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGAvisDEPassage)).BeginInit();
            this.groupBox7.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateDebPlaningAvis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateFinPlaningAvis)).BeginInit();
            this.flowLayoutPanel5.SuspendLayout();
            this.ultraTabPageControl4.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.groupBox8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridTachesAnnuelHisto)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssOpeTachesHisto)).BeginInit();
            this.groupBox9.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateVisiteLe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateVisiteAu)).BeginInit();
            this.frame119.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DTDatePrevueDe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTDateRealiseDe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTDatePrevueAu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTDateRealiseAu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTCreeParDe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEnvoyeLeHisto1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCreeParAu)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEnvoyeLeHisto2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatutPDAhisto)).BeginInit();
            this.frame1113.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.groupBox12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropMatricule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridHisto)).BeginInit();
            this.tableLayoutPanel17.SuspendLayout();
            this.ultraTabPageControl8.SuspendLayout();
            this.frame1111.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).BeginInit();
            this.SSTab1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl5
            // 
            this.ultraTabPageControl5.Controls.Add(this.tableLayoutPanel10);
            this.ultraTabPageControl5.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl5.Name = "ultraTabPageControl5";
            this.ultraTabPageControl5.Size = new System.Drawing.Size(1840, 654);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 1;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Controls.Add(this.groupBox3, 0, 3);
            this.tableLayoutPanel10.Controls.Add(this.txtErreur, 0, 5);
            this.tableLayoutPanel10.Controls.Add(this.label12, 0, 4);
            this.tableLayoutPanel10.Controls.Add(this.ssGridVisite, 0, 1);
            this.tableLayoutPanel10.Controls.Add(this.flowLayoutPanel4, 0, 2);
            this.tableLayoutPanel10.Controls.Add(this.flowLayoutPanel3, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 6;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 25F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(1840, 654);
            this.tableLayoutPanel10.TabIndex = 0;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.ssOpeTaches);
            this.groupBox3.Controls.Add(this.ssGridTachesAnnuel);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox3.Location = new System.Drawing.Point(3, 254);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(1834, 185);
            this.groupBox3.TabIndex = 570;
            this.groupBox3.TabStop = false;
            // 
            // ssOpeTaches
            // 
            this.ssOpeTaches.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ssOpeTaches.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssOpeTaches.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssOpeTaches.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssOpeTaches.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssOpeTaches.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssOpeTaches.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ssOpeTaches.DisplayLayout.UseFixedHeaders = true;
            this.ssOpeTaches.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssOpeTaches.Location = new System.Drawing.Point(3, 20);
            this.ssOpeTaches.Name = "ssOpeTaches";
            this.ssOpeTaches.Size = new System.Drawing.Size(1828, 162);
            this.ssOpeTaches.TabIndex = 572;
            this.ssOpeTaches.Text = "Opération (Simulation)";
            this.ssOpeTaches.Visible = false;
            this.ssOpeTaches.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssOpeTaches_BeforeRowsDeleted);
            this.ssOpeTaches.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.ssOpeTaches_Error);
            // 
            // ssGridTachesAnnuel
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridTachesAnnuel.DisplayLayout.Appearance = appearance1;
            this.ssGridTachesAnnuel.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridTachesAnnuel.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridTachesAnnuel.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridTachesAnnuel.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ssGridTachesAnnuel.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridTachesAnnuel.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ssGridTachesAnnuel.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridTachesAnnuel.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridTachesAnnuel.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridTachesAnnuel.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ssGridTachesAnnuel.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssGridTachesAnnuel.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridTachesAnnuel.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridTachesAnnuel.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridTachesAnnuel.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridTachesAnnuel.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridTachesAnnuel.DisplayLayout.Override.CellAppearance = appearance8;
            this.ssGridTachesAnnuel.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridTachesAnnuel.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridTachesAnnuel.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ssGridTachesAnnuel.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ssGridTachesAnnuel.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridTachesAnnuel.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ssGridTachesAnnuel.DisplayLayout.Override.RowAppearance = appearance11;
            this.ssGridTachesAnnuel.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridTachesAnnuel.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridTachesAnnuel.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ssGridTachesAnnuel.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridTachesAnnuel.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridTachesAnnuel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridTachesAnnuel.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.ssGridTachesAnnuel.Location = new System.Drawing.Point(3, 20);
            this.ssGridTachesAnnuel.Name = "ssGridTachesAnnuel";
            this.ssGridTachesAnnuel.Size = new System.Drawing.Size(1828, 162);
            this.ssGridTachesAnnuel.TabIndex = 412;
            this.ssGridTachesAnnuel.Text = "ultraGrid1";
            this.ssGridTachesAnnuel.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridTachesAnnuel_InitializeLayout);
            this.ssGridTachesAnnuel.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssGridTachesAnnuel_BeforeRowsDeleted);
            this.ssGridTachesAnnuel.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.ssGridTachesAnnuel_Error);
            this.ssGridTachesAnnuel.Leave += new System.EventHandler(this.ssGridTachesAnnuel_Leave);
            // 
            // txtErreur
            // 
            this.txtErreur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtErreur.AutoWordSelection = false;
            this.txtErreur.BackColor = System.Drawing.Color.Transparent;
            this.txtErreur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtErreur.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtErreur.ForeColor = System.Drawing.Color.Black;
            this.txtErreur.Location = new System.Drawing.Point(3, 465);
            this.txtErreur.Name = "txtErreur";
            this.txtErreur.ReadOnly = false;
            this.txtErreur.Size = new System.Drawing.Size(1834, 186);
            this.txtErreur.TabIndex = 569;
            this.txtErreur.WordWrap = true;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 442);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(1834, 20);
            this.label12.TabIndex = 415;
            this.label12.Text = "Anomalie(s) rencontrée(s) sur transfert des interventions vers PDA";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ssGridVisite
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridVisite.DisplayLayout.Appearance = appearance13;
            this.ssGridVisite.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridVisite.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridVisite.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridVisite.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ssGridVisite.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridVisite.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ssGridVisite.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridVisite.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridVisite.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridVisite.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ssGridVisite.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridVisite.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridVisite.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridVisite.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridVisite.DisplayLayout.Override.CellAppearance = appearance20;
            this.ssGridVisite.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridVisite.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridVisite.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ssGridVisite.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ssGridVisite.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridVisite.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ssGridVisite.DisplayLayout.Override.RowAppearance = appearance23;
            this.ssGridVisite.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridVisite.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridVisite.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ssGridVisite.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridVisite.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridVisite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridVisite.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.ssGridVisite.Location = new System.Drawing.Point(3, 38);
            this.ssGridVisite.Name = "ssGridVisite";
            this.ssGridVisite.Size = new System.Drawing.Size(1834, 185);
            this.ssGridVisite.TabIndex = 412;
            this.ssGridVisite.Text = "ultraGrid1";
            this.ssGridVisite.AfterCellActivate += new System.EventHandler(this.ssGridVisite_AfterCellActivate);
            this.ssGridVisite.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridVisite_InitializeLayout);
            this.ssGridVisite.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssGridVisite_InitializeRow);
            this.ssGridVisite.AfterExitEditMode += new System.EventHandler(this.ssGridVisite_AfterExitEditMode);
            this.ssGridVisite.AfterRowActivate += new System.EventHandler(this.ssGridVisite_AfterRowActivate);
            this.ssGridVisite.CellChange += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.ssGridVisite_CellChange);
            this.ssGridVisite.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.ssGridVisite_BeforeExitEditMode);
            this.ssGridVisite.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssGridVisite_BeforeRowsDeleted);
            this.ssGridVisite.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.ssGridVisite_Error);
            // 
            // flowLayoutPanel4
            // 
            this.flowLayoutPanel4.Controls.Add(this.optOperation);
            this.flowLayoutPanel4.Controls.Add(this.optTaches);
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(1, 227);
            this.flowLayoutPanel4.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(1838, 23);
            this.flowLayoutPanel4.TabIndex = 414;
            // 
            // optOperation
            // 
            this.optOperation.AutoSize = true;
            this.optOperation.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optOperation.Location = new System.Drawing.Point(3, 3);
            this.optOperation.Name = "optOperation";
            this.optOperation.Size = new System.Drawing.Size(98, 23);
            this.optOperation.TabIndex = 538;
            this.optOperation.TabStop = true;
            this.optOperation.Text = "Opération";
            this.optOperation.UseVisualStyleBackColor = true;
            this.optOperation.Visible = false;
            this.optOperation.CheckedChanged += new System.EventHandler(this.optOperation_CheckedChanged);
            // 
            // optTaches
            // 
            this.optTaches.AutoSize = true;
            this.optTaches.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optTaches.Location = new System.Drawing.Point(119, 1);
            this.optTaches.Margin = new System.Windows.Forms.Padding(15, 1, 1, 1);
            this.optTaches.Name = "optTaches";
            this.optTaches.Size = new System.Drawing.Size(182, 23);
            this.optTaches.TabIndex = 539;
            this.optTaches.TabStop = true;
            this.optTaches.Text = "Nature des opérations";
            this.optTaches.UseVisualStyleBackColor = true;
            this.optTaches.Visible = false;
            this.optTaches.CheckedChanged += new System.EventHandler(this.optTaches_CheckedChanged);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.cmdCocher);
            this.flowLayoutPanel3.Controls.Add(this.Cmd18);
            this.flowLayoutPanel3.Controls.Add(this.Cmd17);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(1, 1);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(458, 33);
            this.flowLayoutPanel3.TabIndex = 0;
            // 
            // cmdCocher
            // 
            this.cmdCocher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdCocher.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCocher.FlatAppearance.BorderSize = 0;
            this.cmdCocher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCocher.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCocher.ForeColor = System.Drawing.Color.White;
            this.cmdCocher.Image = ((System.Drawing.Image)(resources.GetObject("cmdCocher.Image")));
            this.cmdCocher.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdCocher.Location = new System.Drawing.Point(2, 2);
            this.cmdCocher.Margin = new System.Windows.Forms.Padding(2);
            this.cmdCocher.Name = "cmdCocher";
            this.cmdCocher.Size = new System.Drawing.Size(122, 30);
            this.cmdCocher.TabIndex = 388;
            this.cmdCocher.Text = "     Tous cocher";
            this.cmdCocher.UseVisualStyleBackColor = false;
            this.cmdCocher.Click += new System.EventHandler(this.cmdCocher_Click);
            // 
            // Cmd18
            // 
            this.Cmd18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Cmd18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Cmd18.FlatAppearance.BorderSize = 0;
            this.Cmd18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cmd18.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmd18.ForeColor = System.Drawing.Color.White;
            this.Cmd18.Image = ((System.Drawing.Image)(resources.GetObject("Cmd18.Image")));
            this.Cmd18.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Cmd18.Location = new System.Drawing.Point(128, 2);
            this.Cmd18.Margin = new System.Windows.Forms.Padding(2);
            this.Cmd18.Name = "Cmd18";
            this.Cmd18.Size = new System.Drawing.Size(122, 30);
            this.Cmd18.TabIndex = 391;
            this.Cmd18.Text = "   Tous décocher";
            this.Cmd18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Cmd18.UseVisualStyleBackColor = false;
            this.Cmd18.Click += new System.EventHandler(this.cmdDecocher_Click);
            // 
            // Cmd17
            // 
            this.Cmd17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Cmd17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Cmd17.FlatAppearance.BorderSize = 0;
            this.Cmd17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cmd17.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmd17.ForeColor = System.Drawing.Color.White;
            this.Cmd17.Image = ((System.Drawing.Image)(resources.GetObject("Cmd17.Image")));
            this.Cmd17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Cmd17.Location = new System.Drawing.Point(254, 2);
            this.Cmd17.Margin = new System.Windows.Forms.Padding(2);
            this.Cmd17.Name = "Cmd17";
            this.Cmd17.Size = new System.Drawing.Size(193, 30);
            this.Cmd17.TabIndex = 392;
            this.Cmd17.Text = "     Affecter un intervenant";
            this.Cmd17.UseVisualStyleBackColor = false;
            this.Cmd17.Click += new System.EventHandler(this.cmdAplliquerIntervenat_Click);
            // 
            // ultraTabPageControl6
            // 
            this.ultraTabPageControl6.Controls.Add(this.tableLayoutPanel4);
            this.ultraTabPageControl6.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl6.Name = "ultraTabPageControl6";
            this.ultraTabPageControl6.Size = new System.Drawing.Size(1840, 654);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 1;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel16, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.PL1, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1840, 654);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 8;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 130F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel16.Controls.Add(this.cmdAppliquer0, 7, 0);
            this.tableLayoutPanel16.Controls.Add(this.groupBox5, 6, 0);
            this.tableLayoutPanel16.Controls.Add(this.optJH0, 3, 0);
            this.tableLayoutPanel16.Controls.Add(this.groupBox10, 5, 0);
            this.tableLayoutPanel16.Controls.Add(this.optMJ0, 2, 0);
            this.tableLayoutPanel16.Controls.Add(this.optAM0, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.optMS0, 1, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 1;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(1834, 51);
            this.tableLayoutPanel16.TabIndex = 544;
            // 
            // cmdAppliquer0
            // 
            this.cmdAppliquer0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAppliquer0.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAppliquer0.FlatAppearance.BorderSize = 0;
            this.cmdAppliquer0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAppliquer0.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAppliquer0.ForeColor = System.Drawing.Color.White;
            this.cmdAppliquer0.Image = global::Axe_interDT.Properties.Resources.Ok_16x16;
            this.cmdAppliquer0.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAppliquer0.Location = new System.Drawing.Point(1736, 10);
            this.cmdAppliquer0.Margin = new System.Windows.Forms.Padding(2, 10, 2, 2);
            this.cmdAppliquer0.Name = "cmdAppliquer0";
            this.cmdAppliquer0.Size = new System.Drawing.Size(96, 35);
            this.cmdAppliquer0.TabIndex = 545;
            this.cmdAppliquer0.Text = "    Appliquer";
            this.cmdAppliquer0.UseVisualStyleBackColor = false;
            this.cmdAppliquer0.Click += new System.EventHandler(this.cmdAppliquer0_Click);
            // 
            // groupBox5
            // 
            this.groupBox5.BackColor = System.Drawing.Color.Transparent;
            this.groupBox5.Controls.Add(this.tableLayoutPanel18);
            this.groupBox5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox5.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox5.Location = new System.Drawing.Point(1574, 0);
            this.groupBox5.Margin = new System.Windows.Forms.Padding(0, 0, 10, 0);
            this.groupBox5.Name = "groupBox5";
            this.groupBox5.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox5.Size = new System.Drawing.Size(150, 51);
            this.groupBox5.TabIndex = 543;
            this.groupBox5.TabStop = false;
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 2;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Controls.Add(this.label37, 1, 1);
            this.tableLayoutPanel18.Controls.Add(this.panel3, 0, 1);
            this.tableLayoutPanel18.Controls.Add(this.panel2, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.label36, 1, 0);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(0, 17);
            this.tableLayoutPanel18.Margin = new System.Windows.Forms.Padding(8, 4, 4, 4);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 2;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(150, 34);
            this.tableLayoutPanel18.TabIndex = 0;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label37.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(68, 17);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(79, 17);
            this.label37.TabIndex = 385;
            this.label37.Text = "Jours Fériés";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Violet;
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(5, 18);
            this.panel3.Margin = new System.Windows.Forms.Padding(5, 1, 1, 1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(59, 15);
            this.panel3.TabIndex = 1;
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.MediumTurquoise;
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(5, 1);
            this.panel2.Margin = new System.Windows.Forms.Padding(5, 1, 1, 1);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(59, 15);
            this.panel2.TabIndex = 0;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label36.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(68, 0);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(79, 17);
            this.label36.TabIndex = 384;
            this.label36.Text = "Week end";
            // 
            // optJH0
            // 
            this.optJH0.AutoSize = true;
            this.optJH0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optJH0.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optJH0.Location = new System.Drawing.Point(358, 3);
            this.optJH0.Name = "optJH0";
            this.optJH0.Size = new System.Drawing.Size(108, 45);
            this.optJH0.TabIndex = 540;
            this.optJH0.TabStop = true;
            this.optJH0.Tag = "0";
            this.optJH0.Text = "Jour/Heure";
            this.optJH0.UseVisualStyleBackColor = true;
            // 
            // groupBox10
            // 
            this.groupBox10.BackColor = System.Drawing.Color.Transparent;
            this.groupBox10.Controls.Add(this.tableLayoutPanel19);
            this.groupBox10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox10.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox10.Location = new System.Drawing.Point(1454, 0);
            this.groupBox10.Margin = new System.Windows.Forms.Padding(10, 0, 10, 0);
            this.groupBox10.Name = "groupBox10";
            this.groupBox10.Padding = new System.Windows.Forms.Padding(0);
            this.groupBox10.Size = new System.Drawing.Size(110, 51);
            this.groupBox10.TabIndex = 411;
            this.groupBox10.TabStop = false;
            this.groupBox10.Text = "Taille cellules";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 4;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel19.Controls.Add(this.Text10, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.cmdTailleMoins, 1, 0);
            this.tableLayoutPanel19.Controls.Add(this.cmdTaillePlus, 2, 0);
            this.tableLayoutPanel19.Controls.Add(this.Text2, 3, 0);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(0, 17);
            this.tableLayoutPanel19.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 1;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(110, 34);
            this.tableLayoutPanel19.TabIndex = 0;
            // 
            // Text10
            // 
            this.Text10.AccAcceptNumbersOnly = false;
            this.Text10.AccAllowComma = false;
            this.Text10.AccBackgroundColor = System.Drawing.Color.White;
            this.Text10.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text10.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text10.AccHidenValue = "";
            this.Text10.AccNotAllowedChars = null;
            this.Text10.AccReadOnly = false;
            this.Text10.AccReadOnlyAllowDelete = false;
            this.Text10.AccRequired = false;
            this.Text10.BackColor = System.Drawing.Color.White;
            this.Text10.CustomBackColor = System.Drawing.Color.White;
            this.Text10.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Text10.ForeColor = System.Drawing.Color.Black;
            this.Text10.Location = new System.Drawing.Point(2, 2);
            this.Text10.Margin = new System.Windows.Forms.Padding(2);
            this.Text10.MaxLength = 32767;
            this.Text10.Multiline = false;
            this.Text10.Name = "Text10";
            this.Text10.ReadOnly = false;
            this.Text10.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text10.Size = new System.Drawing.Size(1, 27);
            this.Text10.TabIndex = 583;
            this.Text10.Tag = "0";
            this.Text10.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text10.UseSystemPasswordChar = false;
            this.Text10.Visible = false;
            // 
            // cmdTailleMoins
            // 
            this.cmdTailleMoins.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdTailleMoins.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdTailleMoins.FlatAppearance.BorderSize = 0;
            this.cmdTailleMoins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdTailleMoins.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdTailleMoins.ForeColor = System.Drawing.Color.White;
            this.cmdTailleMoins.Location = new System.Drawing.Point(27, 7);
            this.cmdTailleMoins.Margin = new System.Windows.Forms.Padding(7, 7, 7, 5);
            this.cmdTailleMoins.Name = "cmdTailleMoins";
            this.cmdTailleMoins.Size = new System.Drawing.Size(21, 22);
            this.cmdTailleMoins.TabIndex = 579;
            this.cmdTailleMoins.Text = "-";
            this.cmdTailleMoins.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdTailleMoins.UseVisualStyleBackColor = false;
            this.cmdTailleMoins.Click += new System.EventHandler(this.cmdTailleMoins_Click);
            // 
            // cmdTaillePlus
            // 
            this.cmdTaillePlus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdTaillePlus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdTaillePlus.FlatAppearance.BorderSize = 0;
            this.cmdTaillePlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdTaillePlus.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdTaillePlus.ForeColor = System.Drawing.Color.White;
            this.cmdTaillePlus.Location = new System.Drawing.Point(62, 7);
            this.cmdTaillePlus.Margin = new System.Windows.Forms.Padding(7, 7, 7, 5);
            this.cmdTaillePlus.Name = "cmdTaillePlus";
            this.cmdTaillePlus.Size = new System.Drawing.Size(21, 22);
            this.cmdTaillePlus.TabIndex = 580;
            this.cmdTaillePlus.Text = "+";
            this.cmdTaillePlus.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdTaillePlus.UseVisualStyleBackColor = false;
            this.cmdTaillePlus.Click += new System.EventHandler(this.cmdTaillePlus_Click);
            // 
            // Text2
            // 
            this.Text2.AccAcceptNumbersOnly = false;
            this.Text2.AccAllowComma = false;
            this.Text2.AccBackgroundColor = System.Drawing.Color.White;
            this.Text2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text2.AccHidenValue = "";
            this.Text2.AccNotAllowedChars = null;
            this.Text2.AccReadOnly = false;
            this.Text2.AccReadOnlyAllowDelete = false;
            this.Text2.AccRequired = false;
            this.Text2.BackColor = System.Drawing.Color.White;
            this.Text2.CustomBackColor = System.Drawing.Color.White;
            this.Text2.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Text2.ForeColor = System.Drawing.Color.Black;
            this.Text2.Location = new System.Drawing.Point(92, 2);
            this.Text2.Margin = new System.Windows.Forms.Padding(2);
            this.Text2.MaxLength = 32767;
            this.Text2.Multiline = false;
            this.Text2.Name = "Text2";
            this.Text2.ReadOnly = false;
            this.Text2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text2.Size = new System.Drawing.Size(1, 27);
            this.Text2.TabIndex = 582;
            this.Text2.Text = "AM";
            this.Text2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text2.UseSystemPasswordChar = false;
            this.Text2.Visible = false;
            // 
            // optMJ0
            // 
            this.optMJ0.AutoSize = true;
            this.optMJ0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optMJ0.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optMJ0.Location = new System.Drawing.Point(249, 3);
            this.optMJ0.Name = "optMJ0";
            this.optMJ0.Size = new System.Drawing.Size(103, 45);
            this.optMJ0.TabIndex = 541;
            this.optMJ0.TabStop = true;
            this.optMJ0.Tag = "0";
            this.optMJ0.Text = "Mois/Jours";
            this.optMJ0.UseVisualStyleBackColor = true;
            // 
            // optAM0
            // 
            this.optAM0.AutoSize = true;
            this.optAM0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optAM0.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optAM0.Location = new System.Drawing.Point(3, 3);
            this.optAM0.Name = "optAM0";
            this.optAM0.Size = new System.Drawing.Size(110, 45);
            this.optAM0.TabIndex = 539;
            this.optAM0.TabStop = true;
            this.optAM0.Tag = "0";
            this.optAM0.Text = "Année/Mois";
            this.optAM0.UseVisualStyleBackColor = true;
            // 
            // optMS0
            // 
            this.optMS0.AutoSize = true;
            this.optMS0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optMS0.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optMS0.Location = new System.Drawing.Point(119, 3);
            this.optMS0.Name = "optMS0";
            this.optMS0.Size = new System.Drawing.Size(124, 45);
            this.optMS0.TabIndex = 538;
            this.optMS0.TabStop = true;
            this.optMS0.Tag = "0";
            this.optMS0.Text = "Mois/Semaine";
            this.optMS0.UseVisualStyleBackColor = true;
            // 
            // PL1
            // 
            this.PL1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PL1.Enabled = true;
            this.PL1.Location = new System.Drawing.Point(3, 60);
            this.PL1.Name = "PL1";
            this.PL1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("PL1.OcxState")));
            this.PL1.Size = new System.Drawing.Size(1834, 591);
            this.PL1.TabIndex = 1;
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.tableLayoutPanel1);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1848, 933);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.groupBox2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.groupBox6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.groupBox1, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel20, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 170F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 43.08942F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 56.91058F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1848, 933);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.ssPlanCharge);
            this.groupBox2.Controls.Add(this.ssOpTachesSimul);
            this.groupBox2.Controls.Add(this.ssGridTachesAnnuelSimul);
            this.groupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox2.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox2.Location = new System.Drawing.Point(3, 541);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(1842, 368);
            this.groupBox2.TabIndex = 414;
            this.groupBox2.TabStop = false;
            // 
            // ssPlanCharge
            // 
            this.ssPlanCharge.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssPlanCharge.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssPlanCharge.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssPlanCharge.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssPlanCharge.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssPlanCharge.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ssPlanCharge.DisplayLayout.UseFixedHeaders = true;
            this.ssPlanCharge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssPlanCharge.Location = new System.Drawing.Point(3, 20);
            this.ssPlanCharge.Name = "ssPlanCharge";
            this.ssPlanCharge.Size = new System.Drawing.Size(1836, 345);
            this.ssPlanCharge.TabIndex = 573;
            this.ssPlanCharge.Text = "Plan de charge";
            this.ssPlanCharge.Visible = false;
            // 
            // ssOpTachesSimul
            // 
            this.ssOpTachesSimul.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ssOpTachesSimul.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssOpTachesSimul.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssOpTachesSimul.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssOpTachesSimul.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssOpTachesSimul.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssOpTachesSimul.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ssOpTachesSimul.DisplayLayout.UseFixedHeaders = true;
            this.ssOpTachesSimul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssOpTachesSimul.Location = new System.Drawing.Point(3, 20);
            this.ssOpTachesSimul.Name = "ssOpTachesSimul";
            this.ssOpTachesSimul.Size = new System.Drawing.Size(1836, 345);
            this.ssOpTachesSimul.TabIndex = 572;
            this.ssOpTachesSimul.Text = "Opération (Similuation)";
            this.ssOpTachesSimul.Visible = false;
            // 
            // ssGridTachesAnnuelSimul
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Appearance = appearance25;
            this.ssGridTachesAnnuelSimul.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridTachesAnnuelSimul.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridTachesAnnuelSimul.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridTachesAnnuelSimul.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.ssGridTachesAnnuelSimul.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridTachesAnnuelSimul.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.ssGridTachesAnnuelSimul.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridTachesAnnuelSimul.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.CellAppearance = appearance32;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.RowAppearance = appearance35;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridTachesAnnuelSimul.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.ssGridTachesAnnuelSimul.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridTachesAnnuelSimul.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridTachesAnnuelSimul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridTachesAnnuelSimul.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.ssGridTachesAnnuelSimul.Location = new System.Drawing.Point(3, 20);
            this.ssGridTachesAnnuelSimul.Name = "ssGridTachesAnnuelSimul";
            this.ssGridTachesAnnuelSimul.Size = new System.Drawing.Size(1836, 345);
            this.ssGridTachesAnnuelSimul.TabIndex = 412;
            this.ssGridTachesAnnuelSimul.Text = "ultraGrid1";
            this.ssGridTachesAnnuelSimul.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridTachesAnnuelSimul_InitializeLayout);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.tableLayoutPanel2);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox6.Location = new System.Drawing.Point(0, 50);
            this.groupBox6.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Padding = new System.Windows.Forms.Padding(3, 3, 3, 1);
            this.groupBox6.Size = new System.Drawing.Size(1848, 170);
            this.groupBox6.TabIndex = 411;
            this.groupBox6.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 7;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.06515F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.06515F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.97935F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.89035F));
            this.tableLayoutPanel2.Controls.Add(this.label4, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.chkPEI_APlanifier, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.dtDateDebPlaningBisSimul, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.dtDateFinPlaningBisSimul, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtCodeImmeubleSimul, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtIntervenantSimul, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtCOP_NoautoSimul, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.cmdRechercheImmeubleSimul, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.cmdFindStraitantSimul, 4, 2);
            this.tableLayoutPanel2.Controls.Add(this.cmdFindGMAO, 4, 3);
            this.tableLayoutPanel2.Controls.Add(this.lblTotalSimul, 6, 4);
            this.tableLayoutPanel2.Controls.Add(this.frame117, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.groupBox11, 6, 1);
            this.tableLayoutPanel2.Controls.Add(this.lbllibIntervenantSimul, 3, 2);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 5;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1842, 149);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(504, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(26, 30);
            this.label4.TabIndex = 573;
            this.label4.Text = "au";
            // 
            // chkPEI_APlanifier
            // 
            this.chkPEI_APlanifier.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.chkPEI_APlanifier, 2);
            this.chkPEI_APlanifier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkPEI_APlanifier.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.chkPEI_APlanifier.Location = new System.Drawing.Point(3, 123);
            this.chkPEI_APlanifier.Name = "chkPEI_APlanifier";
            this.chkPEI_APlanifier.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.chkPEI_APlanifier.Size = new System.Drawing.Size(495, 26);
            this.chkPEI_APlanifier.TabIndex = 570;
            this.chkPEI_APlanifier.Text = "\"Prestation coché \"Ne pas planifier";
            this.chkPEI_APlanifier.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkPEI_APlanifier.UseVisualStyleBackColor = true;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(112, 30);
            this.label33.TabIndex = 384;
            this.label33.Text = "Prévue le";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(112, 30);
            this.label1.TabIndex = 385;
            this.label1.Text = "immeuble";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(112, 30);
            this.label2.TabIndex = 386;
            this.label2.Text = "Intervenant";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 90);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(112, 30);
            this.label3.TabIndex = 387;
            this.label3.Text = "N° Fiche GMAO";
            // 
            // dtDateDebPlaningBisSimul
            // 
            appearance37.BackColorDisabled = System.Drawing.Color.White;
            appearance37.BackColorDisabled2 = System.Drawing.Color.White;
            this.dtDateDebPlaningBisSimul.Appearance = appearance37;
            stateEditorButton1.ButtonStyle = Infragistics.Win.UIElementButtonStyle.PopupBorderless;
            stateEditorButton1.CheckState = System.Windows.Forms.CheckState.Checked;
            appearance38.BackColor = System.Drawing.Color.White;
            stateEditorButton1.PressedAppearance = appearance38;
            this.dtDateDebPlaningBisSimul.ButtonsLeft.Add(stateEditorButton1);
            this.dtDateDebPlaningBisSimul.DateTime = new System.DateTime(2009, 3, 3, 0, 0, 0, 0);
            this.dtDateDebPlaningBisSimul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtDateDebPlaningBisSimul.Location = new System.Drawing.Point(121, 3);
            this.dtDateDebPlaningBisSimul.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.dtDateDebPlaningBisSimul.Name = "dtDateDebPlaningBisSimul";
            this.dtDateDebPlaningBisSimul.Size = new System.Drawing.Size(377, 26);
            this.dtDateDebPlaningBisSimul.TabIndex = 571;
            this.dtDateDebPlaningBisSimul.Value = new System.DateTime(2009, 3, 3, 0, 0, 0, 0);
            this.dtDateDebPlaningBisSimul.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTDatePrevueDe_BeforeDropDown);
            this.dtDateDebPlaningBisSimul.BeforeExitEditMode += new Infragistics.Win.BeforeExitEditModeEventHandler(this.dtDateDebPlaningBisSimul_BeforeExitEditMode);
            this.dtDateDebPlaningBisSimul.AfterExitEditMode += new System.EventHandler(this.dtDateDebPlaningBisSimul_AfterExitEditMode);
            this.dtDateDebPlaningBisSimul.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.dtDateDebPlaningBisSimul_AfterEditorButtonCheckStateChanged);
            this.dtDateDebPlaningBisSimul.Validating += new System.ComponentModel.CancelEventHandler(this.dtDateDebPlaningBisSimul_Validating);
            // 
            // dtDateFinPlaningBisSimul
            // 
            stateEditorButton2.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless;
            stateEditorButton2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dtDateFinPlaningBisSimul.ButtonsLeft.Add(stateEditorButton2);
            this.dtDateFinPlaningBisSimul.DateTime = new System.DateTime(2009, 3, 3, 0, 0, 0, 0);
            this.dtDateFinPlaningBisSimul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtDateFinPlaningBisSimul.Location = new System.Drawing.Point(536, 3);
            this.dtDateFinPlaningBisSimul.Margin = new System.Windows.Forms.Padding(3, 3, 3, 5);
            this.dtDateFinPlaningBisSimul.Name = "dtDateFinPlaningBisSimul";
            this.dtDateFinPlaningBisSimul.Size = new System.Drawing.Size(377, 26);
            this.dtDateFinPlaningBisSimul.TabIndex = 572;
            this.dtDateFinPlaningBisSimul.Value = new System.DateTime(2009, 3, 3, 0, 0, 0, 0);
            this.dtDateFinPlaningBisSimul.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTDatePrevueDe_BeforeDropDown);
            this.dtDateFinPlaningBisSimul.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.dtDateDebPlaningBisSimul_AfterEditorButtonCheckStateChanged);
            // 
            // txtCodeImmeubleSimul
            // 
            this.txtCodeImmeubleSimul.AccAcceptNumbersOnly = false;
            this.txtCodeImmeubleSimul.AccAllowComma = false;
            this.txtCodeImmeubleSimul.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeubleSimul.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeubleSimul.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeubleSimul.AccHidenValue = "";
            this.txtCodeImmeubleSimul.AccNotAllowedChars = null;
            this.txtCodeImmeubleSimul.AccReadOnly = false;
            this.txtCodeImmeubleSimul.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeubleSimul.AccRequired = false;
            this.txtCodeImmeubleSimul.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel2.SetColumnSpan(this.txtCodeImmeubleSimul, 3);
            this.txtCodeImmeubleSimul.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeubleSimul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeImmeubleSimul.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeImmeubleSimul.ForeColor = System.Drawing.Color.Black;
            this.txtCodeImmeubleSimul.Location = new System.Drawing.Point(120, 32);
            this.txtCodeImmeubleSimul.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeubleSimul.MaxLength = 32767;
            this.txtCodeImmeubleSimul.Multiline = false;
            this.txtCodeImmeubleSimul.Name = "txtCodeImmeubleSimul";
            this.txtCodeImmeubleSimul.ReadOnly = false;
            this.txtCodeImmeubleSimul.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeubleSimul.Size = new System.Drawing.Size(794, 27);
            this.txtCodeImmeubleSimul.TabIndex = 573;
            this.txtCodeImmeubleSimul.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeubleSimul.UseSystemPasswordChar = false;
            this.txtCodeImmeubleSimul.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeImmeubleSimul_KeyPress);
            // 
            // txtIntervenantSimul
            // 
            this.txtIntervenantSimul.AccAcceptNumbersOnly = false;
            this.txtIntervenantSimul.AccAllowComma = false;
            this.txtIntervenantSimul.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntervenantSimul.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntervenantSimul.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntervenantSimul.AccHidenValue = "";
            this.txtIntervenantSimul.AccNotAllowedChars = null;
            this.txtIntervenantSimul.AccReadOnly = false;
            this.txtIntervenantSimul.AccReadOnlyAllowDelete = false;
            this.txtIntervenantSimul.AccRequired = false;
            this.txtIntervenantSimul.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel2.SetColumnSpan(this.txtIntervenantSimul, 2);
            this.txtIntervenantSimul.CustomBackColor = System.Drawing.Color.White;
            this.txtIntervenantSimul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntervenantSimul.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtIntervenantSimul.ForeColor = System.Drawing.Color.Black;
            this.txtIntervenantSimul.Location = new System.Drawing.Point(120, 62);
            this.txtIntervenantSimul.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntervenantSimul.MaxLength = 32767;
            this.txtIntervenantSimul.Multiline = false;
            this.txtIntervenantSimul.Name = "txtIntervenantSimul";
            this.txtIntervenantSimul.ReadOnly = false;
            this.txtIntervenantSimul.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntervenantSimul.Size = new System.Drawing.Size(411, 27);
            this.txtIntervenantSimul.TabIndex = 574;
            this.txtIntervenantSimul.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntervenantSimul.UseSystemPasswordChar = false;
            this.txtIntervenantSimul.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIntervenantSimul_KeyPress);
            this.txtIntervenantSimul.Validated += new System.EventHandler(this.txtIntervenantSimul_Validated);
            // 
            // txtCOP_NoautoSimul
            // 
            this.txtCOP_NoautoSimul.AccAcceptNumbersOnly = false;
            this.txtCOP_NoautoSimul.AccAllowComma = false;
            this.txtCOP_NoautoSimul.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCOP_NoautoSimul.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCOP_NoautoSimul.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCOP_NoautoSimul.AccHidenValue = "";
            this.txtCOP_NoautoSimul.AccNotAllowedChars = null;
            this.txtCOP_NoautoSimul.AccReadOnly = false;
            this.txtCOP_NoautoSimul.AccReadOnlyAllowDelete = false;
            this.txtCOP_NoautoSimul.AccRequired = false;
            this.txtCOP_NoautoSimul.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel2.SetColumnSpan(this.txtCOP_NoautoSimul, 3);
            this.txtCOP_NoautoSimul.CustomBackColor = System.Drawing.Color.White;
            this.txtCOP_NoautoSimul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCOP_NoautoSimul.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCOP_NoautoSimul.ForeColor = System.Drawing.Color.Black;
            this.txtCOP_NoautoSimul.Location = new System.Drawing.Point(120, 92);
            this.txtCOP_NoautoSimul.Margin = new System.Windows.Forms.Padding(2);
            this.txtCOP_NoautoSimul.MaxLength = 32767;
            this.txtCOP_NoautoSimul.Multiline = false;
            this.txtCOP_NoautoSimul.Name = "txtCOP_NoautoSimul";
            this.txtCOP_NoautoSimul.ReadOnly = false;
            this.txtCOP_NoautoSimul.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCOP_NoautoSimul.Size = new System.Drawing.Size(794, 27);
            this.txtCOP_NoautoSimul.TabIndex = 576;
            this.txtCOP_NoautoSimul.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCOP_NoautoSimul.UseSystemPasswordChar = false;
            // 
            // cmdRechercheImmeubleSimul
            // 
            this.cmdRechercheImmeubleSimul.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheImmeubleSimul.FlatAppearance.BorderSize = 0;
            this.cmdRechercheImmeubleSimul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheImmeubleSimul.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheImmeubleSimul.Image = ((System.Drawing.Image)(resources.GetObject("cmdRechercheImmeubleSimul.Image")));
            this.cmdRechercheImmeubleSimul.Location = new System.Drawing.Point(919, 33);
            this.cmdRechercheImmeubleSimul.Name = "cmdRechercheImmeubleSimul";
            this.cmdRechercheImmeubleSimul.Size = new System.Drawing.Size(25, 19);
            this.cmdRechercheImmeubleSimul.TabIndex = 578;
            this.cmdRechercheImmeubleSimul.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechercheImmeubleSimul, "Recherche d\'un chantier");
            this.cmdRechercheImmeubleSimul.UseVisualStyleBackColor = false;
            this.cmdRechercheImmeubleSimul.Click += new System.EventHandler(this.cmdRechercheImmeubleSimul_Click);
            // 
            // cmdFindStraitantSimul
            // 
            this.cmdFindStraitantSimul.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFindStraitantSimul.FlatAppearance.BorderSize = 0;
            this.cmdFindStraitantSimul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFindStraitantSimul.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdFindStraitantSimul.Image = ((System.Drawing.Image)(resources.GetObject("cmdFindStraitantSimul.Image")));
            this.cmdFindStraitantSimul.Location = new System.Drawing.Point(919, 63);
            this.cmdFindStraitantSimul.Name = "cmdFindStraitantSimul";
            this.cmdFindStraitantSimul.Size = new System.Drawing.Size(25, 19);
            this.cmdFindStraitantSimul.TabIndex = 579;
            this.cmdFindStraitantSimul.Tag = "";
            this.toolTip1.SetToolTip(this.cmdFindStraitantSimul, "Recherche d\'un intervenant");
            this.cmdFindStraitantSimul.UseVisualStyleBackColor = false;
            this.cmdFindStraitantSimul.Click += new System.EventHandler(this.cmdFindStraitantSimul_Click);
            // 
            // cmdFindGMAO
            // 
            this.cmdFindGMAO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFindGMAO.FlatAppearance.BorderSize = 0;
            this.cmdFindGMAO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFindGMAO.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdFindGMAO.Image = ((System.Drawing.Image)(resources.GetObject("cmdFindGMAO.Image")));
            this.cmdFindGMAO.Location = new System.Drawing.Point(919, 93);
            this.cmdFindGMAO.Name = "cmdFindGMAO";
            this.cmdFindGMAO.Size = new System.Drawing.Size(25, 19);
            this.cmdFindGMAO.TabIndex = 580;
            this.cmdFindGMAO.Tag = "";
            this.toolTip1.SetToolTip(this.cmdFindGMAO, "Recherche d\'un intervenant");
            this.cmdFindGMAO.UseVisualStyleBackColor = false;
            this.cmdFindGMAO.Click += new System.EventHandler(this.cmdFindGMAO_Click);
            // 
            // lblTotalSimul
            // 
            this.lblTotalSimul.AccAcceptNumbersOnly = false;
            this.lblTotalSimul.AccAllowComma = false;
            this.lblTotalSimul.AccBackgroundColor = System.Drawing.Color.White;
            this.lblTotalSimul.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblTotalSimul.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblTotalSimul.AccHidenValue = "";
            this.lblTotalSimul.AccNotAllowedChars = null;
            this.lblTotalSimul.AccReadOnly = false;
            this.lblTotalSimul.AccReadOnlyAllowDelete = false;
            this.lblTotalSimul.AccRequired = false;
            this.lblTotalSimul.BackColor = System.Drawing.Color.White;
            this.lblTotalSimul.CustomBackColor = System.Drawing.Color.White;
            this.lblTotalSimul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTotalSimul.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lblTotalSimul.ForeColor = System.Drawing.Color.Black;
            this.lblTotalSimul.Location = new System.Drawing.Point(1513, 122);
            this.lblTotalSimul.Margin = new System.Windows.Forms.Padding(2);
            this.lblTotalSimul.MaxLength = 32767;
            this.lblTotalSimul.Multiline = false;
            this.lblTotalSimul.Name = "lblTotalSimul";
            this.lblTotalSimul.ReadOnly = false;
            this.lblTotalSimul.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblTotalSimul.Size = new System.Drawing.Size(327, 27);
            this.lblTotalSimul.TabIndex = 582;
            this.lblTotalSimul.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblTotalSimul.UseSystemPasswordChar = false;
            // 
            // frame117
            // 
            this.frame117.BackColor = System.Drawing.Color.Transparent;
            this.frame117.Controls.Add(this.tableLayoutPanel3);
            this.frame117.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frame117.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.frame117.Location = new System.Drawing.Point(950, 3);
            this.frame117.Name = "frame117";
            this.tableLayoutPanel2.SetRowSpan(this.frame117, 4);
            this.frame117.Size = new System.Drawing.Size(558, 114);
            this.frame117.TabIndex = 581;
            this.frame117.TabStop = false;
            this.frame117.Text = "P1";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.optEQU_P1CompteurSimul2, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.optEQU_P1CompteurSimul0, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.optEQU_P1CompteurSimul1, 0, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(552, 91);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // optEQU_P1CompteurSimul2
            // 
            this.optEQU_P1CompteurSimul2.AutoSize = true;
            this.optEQU_P1CompteurSimul2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optEQU_P1CompteurSimul2.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optEQU_P1CompteurSimul2.Location = new System.Drawing.Point(3, 59);
            this.optEQU_P1CompteurSimul2.Name = "optEQU_P1CompteurSimul2";
            this.optEQU_P1CompteurSimul2.Size = new System.Drawing.Size(546, 29);
            this.optEQU_P1CompteurSimul2.TabIndex = 540;
            this.optEQU_P1CompteurSimul2.TabStop = true;
            this.optEQU_P1CompteurSimul2.Tag = "2";
            this.optEQU_P1CompteurSimul2.Text = "Avec ou sans compteur P1";
            this.optEQU_P1CompteurSimul2.UseVisualStyleBackColor = true;
            // 
            // optEQU_P1CompteurSimul0
            // 
            this.optEQU_P1CompteurSimul0.AutoSize = true;
            this.optEQU_P1CompteurSimul0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optEQU_P1CompteurSimul0.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optEQU_P1CompteurSimul0.Location = new System.Drawing.Point(3, 3);
            this.optEQU_P1CompteurSimul0.Name = "optEQU_P1CompteurSimul0";
            this.optEQU_P1CompteurSimul0.Size = new System.Drawing.Size(546, 22);
            this.optEQU_P1CompteurSimul0.TabIndex = 538;
            this.optEQU_P1CompteurSimul0.TabStop = true;
            this.optEQU_P1CompteurSimul0.Tag = "0";
            this.optEQU_P1CompteurSimul0.Text = "Uniquement Compteur P1";
            this.optEQU_P1CompteurSimul0.UseVisualStyleBackColor = true;
            // 
            // optEQU_P1CompteurSimul1
            // 
            this.optEQU_P1CompteurSimul1.AutoSize = true;
            this.optEQU_P1CompteurSimul1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optEQU_P1CompteurSimul1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optEQU_P1CompteurSimul1.Location = new System.Drawing.Point(3, 31);
            this.optEQU_P1CompteurSimul1.Name = "optEQU_P1CompteurSimul1";
            this.optEQU_P1CompteurSimul1.Size = new System.Drawing.Size(546, 22);
            this.optEQU_P1CompteurSimul1.TabIndex = 539;
            this.optEQU_P1CompteurSimul1.TabStop = true;
            this.optEQU_P1CompteurSimul1.Tag = "1";
            this.optEQU_P1CompteurSimul1.Text = "Exclure les Compteur P1";
            this.optEQU_P1CompteurSimul1.UseVisualStyleBackColor = true;
            // 
            // groupBox11
            // 
            this.groupBox11.BackColor = System.Drawing.Color.Transparent;
            this.groupBox11.Controls.Add(this.tableLayoutPanel21);
            this.groupBox11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox11.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox11.Location = new System.Drawing.Point(1514, 33);
            this.groupBox11.Name = "groupBox11";
            this.tableLayoutPanel2.SetRowSpan(this.groupBox11, 3);
            this.groupBox11.Size = new System.Drawing.Size(325, 84);
            this.groupBox11.TabIndex = 583;
            this.groupBox11.TabStop = false;
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 1;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel21.Controls.Add(this.OptPrestation, 0, 0);
            this.tableLayoutPanel21.Controls.Add(this.OptIntervention, 0, 1);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 2;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(319, 61);
            this.tableLayoutPanel21.TabIndex = 0;
            // 
            // OptPrestation
            // 
            this.OptPrestation.AutoSize = true;
            this.OptPrestation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptPrestation.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptPrestation.Location = new System.Drawing.Point(3, 3);
            this.OptPrestation.Name = "OptPrestation";
            this.OptPrestation.Size = new System.Drawing.Size(313, 24);
            this.OptPrestation.TabIndex = 539;
            this.OptPrestation.Text = "Prestation";
            this.OptPrestation.UseVisualStyleBackColor = true;
            // 
            // OptIntervention
            // 
            this.OptIntervention.AutoSize = true;
            this.OptIntervention.Checked = true;
            this.OptIntervention.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptIntervention.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptIntervention.Location = new System.Drawing.Point(3, 33);
            this.OptIntervention.Name = "OptIntervention";
            this.OptIntervention.Size = new System.Drawing.Size(313, 25);
            this.OptIntervention.TabIndex = 538;
            this.OptIntervention.TabStop = true;
            this.OptIntervention.Text = "Intervention";
            this.OptIntervention.UseVisualStyleBackColor = true;
            // 
            // lbllibIntervenantSimul
            // 
            this.lbllibIntervenantSimul.AccAcceptNumbersOnly = false;
            this.lbllibIntervenantSimul.AccAllowComma = false;
            this.lbllibIntervenantSimul.AccBackgroundColor = System.Drawing.Color.White;
            this.lbllibIntervenantSimul.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lbllibIntervenantSimul.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lbllibIntervenantSimul.AccHidenValue = "";
            this.lbllibIntervenantSimul.AccNotAllowedChars = null;
            this.lbllibIntervenantSimul.AccReadOnly = false;
            this.lbllibIntervenantSimul.AccReadOnlyAllowDelete = false;
            this.lbllibIntervenantSimul.AccRequired = false;
            this.lbllibIntervenantSimul.BackColor = System.Drawing.Color.White;
            this.lbllibIntervenantSimul.CustomBackColor = System.Drawing.Color.White;
            this.lbllibIntervenantSimul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbllibIntervenantSimul.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lbllibIntervenantSimul.ForeColor = System.Drawing.Color.Black;
            this.lbllibIntervenantSimul.Location = new System.Drawing.Point(535, 62);
            this.lbllibIntervenantSimul.Margin = new System.Windows.Forms.Padding(2);
            this.lbllibIntervenantSimul.MaxLength = 32767;
            this.lbllibIntervenantSimul.Multiline = false;
            this.lbllibIntervenantSimul.Name = "lbllibIntervenantSimul";
            this.lbllibIntervenantSimul.ReadOnly = true;
            this.lbllibIntervenantSimul.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lbllibIntervenantSimul.Size = new System.Drawing.Size(379, 27);
            this.lbllibIntervenantSimul.TabIndex = 575;
            this.lbllibIntervenantSimul.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lbllibIntervenantSimul.UseSystemPasswordChar = false;
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.frame115);
            this.groupBox1.Controls.Add(this.ssGridVisiteSimul);
            this.groupBox1.Controls.Add(this.SSOleDBGrid1);
            this.groupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox1.Location = new System.Drawing.Point(3, 223);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1842, 277);
            this.groupBox1.TabIndex = 413;
            this.groupBox1.TabStop = false;
            // 
            // frame115
            // 
            this.frame115.BackColor = System.Drawing.Color.Transparent;
            this.frame115.Controls.Add(this.optVisites);
            this.frame115.Controls.Add(this.optListeTaches);
            this.frame115.Controls.Add(this.txtPeriode);
            this.frame115.Controls.Add(this.chkLecture);
            this.frame115.Controls.Add(this.lblLibIntervSecteur);
            this.frame115.Controls.Add(this.txtUtilVerrou);
            this.frame115.Controls.Add(this.label45);
            this.frame115.Controls.Add(this.cmdIntervSecteur);
            this.frame115.Controls.Add(this.txtIntervSecteur);
            this.frame115.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.frame115.Location = new System.Drawing.Point(391, 87);
            this.frame115.Name = "frame115";
            this.frame115.Size = new System.Drawing.Size(339, 151);
            this.frame115.TabIndex = 413;
            this.frame115.TabStop = false;
            this.frame115.Visible = false;
            // 
            // optVisites
            // 
            this.optVisites.AutoSize = true;
            this.optVisites.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optVisites.Location = new System.Drawing.Point(50, 157);
            this.optVisites.Name = "optVisites";
            this.optVisites.Size = new System.Drawing.Size(71, 23);
            this.optVisites.TabIndex = 613;
            this.optVisites.TabStop = true;
            this.optVisites.Text = "Visites";
            this.optVisites.UseVisualStyleBackColor = true;
            // 
            // optListeTaches
            // 
            this.optListeTaches.AutoSize = true;
            this.optListeTaches.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optListeTaches.Location = new System.Drawing.Point(26, 131);
            this.optListeTaches.Name = "optListeTaches";
            this.optListeTaches.Size = new System.Drawing.Size(152, 23);
            this.optListeTaches.TabIndex = 612;
            this.optListeTaches.TabStop = true;
            this.optListeTaches.Text = "Listes Des Taches ";
            this.optListeTaches.UseVisualStyleBackColor = true;
            // 
            // txtPeriode
            // 
            this.txtPeriode.AccAcceptNumbersOnly = false;
            this.txtPeriode.AccAllowComma = false;
            this.txtPeriode.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPeriode.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPeriode.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtPeriode.AccHidenValue = "";
            this.txtPeriode.AccNotAllowedChars = null;
            this.txtPeriode.AccReadOnly = false;
            this.txtPeriode.AccReadOnlyAllowDelete = false;
            this.txtPeriode.AccRequired = false;
            this.txtPeriode.BackColor = System.Drawing.Color.White;
            this.txtPeriode.CustomBackColor = System.Drawing.Color.White;
            this.txtPeriode.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtPeriode.ForeColor = System.Drawing.Color.Black;
            this.txtPeriode.Location = new System.Drawing.Point(9, 102);
            this.txtPeriode.Margin = new System.Windows.Forms.Padding(2);
            this.txtPeriode.MaxLength = 32767;
            this.txtPeriode.Multiline = false;
            this.txtPeriode.Name = "txtPeriode";
            this.txtPeriode.ReadOnly = false;
            this.txtPeriode.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPeriode.Size = new System.Drawing.Size(122, 27);
            this.txtPeriode.TabIndex = 611;
            this.txtPeriode.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPeriode.UseSystemPasswordChar = false;
            this.txtPeriode.Visible = false;
            // 
            // chkLecture
            // 
            this.chkLecture.AutoSize = true;
            this.chkLecture.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.chkLecture.Location = new System.Drawing.Point(132, 49);
            this.chkLecture.Name = "chkLecture";
            this.chkLecture.Size = new System.Drawing.Size(124, 23);
            this.chkLecture.TabIndex = 610;
            this.chkLecture.Text = "Lecture seule";
            this.chkLecture.UseVisualStyleBackColor = true;
            this.chkLecture.Visible = false;
            // 
            // lblLibIntervSecteur
            // 
            this.lblLibIntervSecteur.AccAcceptNumbersOnly = false;
            this.lblLibIntervSecteur.AccAllowComma = false;
            this.lblLibIntervSecteur.AccBackgroundColor = System.Drawing.Color.White;
            this.lblLibIntervSecteur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblLibIntervSecteur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblLibIntervSecteur.AccHidenValue = "";
            this.lblLibIntervSecteur.AccNotAllowedChars = null;
            this.lblLibIntervSecteur.AccReadOnly = false;
            this.lblLibIntervSecteur.AccReadOnlyAllowDelete = false;
            this.lblLibIntervSecteur.AccRequired = false;
            this.lblLibIntervSecteur.BackColor = System.Drawing.Color.White;
            this.lblLibIntervSecteur.CustomBackColor = System.Drawing.Color.White;
            this.lblLibIntervSecteur.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lblLibIntervSecteur.ForeColor = System.Drawing.Color.Black;
            this.lblLibIntervSecteur.Location = new System.Drawing.Point(6, 76);
            this.lblLibIntervSecteur.Margin = new System.Windows.Forms.Padding(2);
            this.lblLibIntervSecteur.MaxLength = 32767;
            this.lblLibIntervSecteur.Multiline = false;
            this.lblLibIntervSecteur.Name = "lblLibIntervSecteur";
            this.lblLibIntervSecteur.ReadOnly = false;
            this.lblLibIntervSecteur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblLibIntervSecteur.Size = new System.Drawing.Size(122, 27);
            this.lblLibIntervSecteur.TabIndex = 609;
            this.lblLibIntervSecteur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblLibIntervSecteur.UseSystemPasswordChar = false;
            this.lblLibIntervSecteur.Visible = false;
            // 
            // txtUtilVerrou
            // 
            this.txtUtilVerrou.AccAcceptNumbersOnly = false;
            this.txtUtilVerrou.AccAllowComma = false;
            this.txtUtilVerrou.AccBackgroundColor = System.Drawing.Color.White;
            this.txtUtilVerrou.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtUtilVerrou.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtUtilVerrou.AccHidenValue = "";
            this.txtUtilVerrou.AccNotAllowedChars = null;
            this.txtUtilVerrou.AccReadOnly = false;
            this.txtUtilVerrou.AccReadOnlyAllowDelete = false;
            this.txtUtilVerrou.AccRequired = false;
            this.txtUtilVerrou.BackColor = System.Drawing.Color.White;
            this.txtUtilVerrou.CustomBackColor = System.Drawing.Color.White;
            this.txtUtilVerrou.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtUtilVerrou.ForeColor = System.Drawing.Color.Black;
            this.txtUtilVerrou.Location = new System.Drawing.Point(6, 47);
            this.txtUtilVerrou.Margin = new System.Windows.Forms.Padding(2);
            this.txtUtilVerrou.MaxLength = 32767;
            this.txtUtilVerrou.Multiline = false;
            this.txtUtilVerrou.Name = "txtUtilVerrou";
            this.txtUtilVerrou.ReadOnly = false;
            this.txtUtilVerrou.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtUtilVerrou.Size = new System.Drawing.Size(122, 27);
            this.txtUtilVerrou.TabIndex = 608;
            this.txtUtilVerrou.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtUtilVerrou.UseSystemPasswordChar = false;
            this.txtUtilVerrou.Visible = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(2, 21);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(136, 19);
            this.label45.TabIndex = 607;
            this.label45.Text = "Secteur immeuble";
            // 
            // cmdIntervSecteur
            // 
            this.cmdIntervSecteur.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdIntervSecteur.FlatAppearance.BorderSize = 0;
            this.cmdIntervSecteur.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdIntervSecteur.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdIntervSecteur.Image = ((System.Drawing.Image)(resources.GetObject("cmdIntervSecteur.Image")));
            this.cmdIntervSecteur.Location = new System.Drawing.Point(214, 21);
            this.cmdIntervSecteur.Name = "cmdIntervSecteur";
            this.cmdIntervSecteur.Size = new System.Drawing.Size(24, 19);
            this.cmdIntervSecteur.TabIndex = 606;
            this.cmdIntervSecteur.UseVisualStyleBackColor = false;
            this.cmdIntervSecteur.Visible = false;
            // 
            // txtIntervSecteur
            // 
            this.txtIntervSecteur.AccAcceptNumbersOnly = false;
            this.txtIntervSecteur.AccAllowComma = false;
            this.txtIntervSecteur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntervSecteur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntervSecteur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntervSecteur.AccHidenValue = "";
            this.txtIntervSecteur.AccNotAllowedChars = null;
            this.txtIntervSecteur.AccReadOnly = false;
            this.txtIntervSecteur.AccReadOnlyAllowDelete = false;
            this.txtIntervSecteur.AccRequired = false;
            this.txtIntervSecteur.BackColor = System.Drawing.Color.White;
            this.txtIntervSecteur.CustomBackColor = System.Drawing.Color.White;
            this.txtIntervSecteur.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtIntervSecteur.ForeColor = System.Drawing.Color.Black;
            this.txtIntervSecteur.Location = new System.Drawing.Point(134, 18);
            this.txtIntervSecteur.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntervSecteur.MaxLength = 32767;
            this.txtIntervSecteur.Multiline = false;
            this.txtIntervSecteur.Name = "txtIntervSecteur";
            this.txtIntervSecteur.ReadOnly = false;
            this.txtIntervSecteur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntervSecteur.Size = new System.Drawing.Size(75, 27);
            this.txtIntervSecteur.TabIndex = 503;
            this.txtIntervSecteur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntervSecteur.UseSystemPasswordChar = false;
            this.txtIntervSecteur.Visible = false;
            // 
            // ssGridVisiteSimul
            // 
            this.ssGridVisiteSimul.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ssGridVisiteSimul.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssGridVisiteSimul.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridVisiteSimul.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridVisiteSimul.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridVisiteSimul.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridVisiteSimul.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ssGridVisiteSimul.DisplayLayout.UseFixedHeaders = true;
            this.ssGridVisiteSimul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridVisiteSimul.Location = new System.Drawing.Point(3, 20);
            this.ssGridVisiteSimul.Name = "ssGridVisiteSimul";
            this.ssGridVisiteSimul.Size = new System.Drawing.Size(1836, 254);
            this.ssGridVisiteSimul.TabIndex = 572;
            this.ssGridVisiteSimul.Text = "Interventions-Visites";
            this.ssGridVisiteSimul.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridVisiteSimul_InitializeLayout);
            this.ssGridVisiteSimul.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssGridVisiteSimul_InitializeRow);
            this.ssGridVisiteSimul.AfterRowActivate += new System.EventHandler(this.ssGridVisiteSimul_AfterRowActivate);
            // 
            // SSOleDBGrid1
            // 
            appearance39.BackColor = System.Drawing.SystemColors.Window;
            appearance39.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBGrid1.DisplayLayout.Appearance = appearance39;
            this.SSOleDBGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance40.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance40.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance40.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.Appearance = appearance40;
            appearance41.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance41;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance42.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance42.BackColor2 = System.Drawing.SystemColors.Control;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance42.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance42;
            this.SSOleDBGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            appearance43.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance43;
            appearance44.BackColor = System.Drawing.SystemColors.Highlight;
            appearance44.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance44;
            this.SSOleDBGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance45.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.Override.CardAreaAppearance = appearance45;
            appearance46.BorderColor = System.Drawing.Color.Silver;
            appearance46.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBGrid1.DisplayLayout.Override.CellAppearance = appearance46;
            this.SSOleDBGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance47.BackColor = System.Drawing.SystemColors.Control;
            appearance47.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance47.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance47.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance47.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance47;
            appearance48.TextHAlignAsString = "Left";
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderAppearance = appearance48;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBGrid1.DisplayLayout.Override.RowAppearance = appearance49;
            this.SSOleDBGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance50.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance50;
            this.SSOleDBGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSOleDBGrid1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.SSOleDBGrid1.Location = new System.Drawing.Point(3, 20);
            this.SSOleDBGrid1.Name = "SSOleDBGrid1";
            this.SSOleDBGrid1.Size = new System.Drawing.Size(1836, 254);
            this.SSOleDBGrid1.TabIndex = 412;
            this.SSOleDBGrid1.Text = "ultraGrid1";
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 7;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.81043F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.81043F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.81043F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 26.65877F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.81043F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 14.09953F));
            this.tableLayoutPanel5.Controls.Add(this.optTachesSimul, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.optPlanCharge, 2, 0);
            this.tableLayoutPanel5.Controls.Add(this.optOperationSimul, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtTotduree, 6, 0);
            this.tableLayoutPanel5.Controls.Add(this.label6, 5, 0);
            this.tableLayoutPanel5.Controls.Add(this.pctPlanCharge, 3, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(1, 504);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1846, 33);
            this.tableLayoutPanel5.TabIndex = 415;
            // 
            // optTachesSimul
            // 
            this.optTachesSimul.AutoSize = true;
            this.optTachesSimul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optTachesSimul.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optTachesSimul.Location = new System.Drawing.Point(262, 3);
            this.optTachesSimul.Name = "optTachesSimul";
            this.optTachesSimul.Size = new System.Drawing.Size(253, 27);
            this.optTachesSimul.TabIndex = 545;
            this.optTachesSimul.Text = "Nature des opérations";
            this.optTachesSimul.UseVisualStyleBackColor = true;
            this.optTachesSimul.Visible = false;
            this.optTachesSimul.CheckedChanged += new System.EventHandler(this.optTachesSimul_CheckedChanged);
            // 
            // optPlanCharge
            // 
            this.optPlanCharge.AutoSize = true;
            this.optPlanCharge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optPlanCharge.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optPlanCharge.Location = new System.Drawing.Point(521, 3);
            this.optPlanCharge.Name = "optPlanCharge";
            this.optPlanCharge.Size = new System.Drawing.Size(253, 27);
            this.optPlanCharge.TabIndex = 540;
            this.optPlanCharge.Text = "Plan de charge";
            this.optPlanCharge.UseVisualStyleBackColor = true;
            this.optPlanCharge.CheckedChanged += new System.EventHandler(this.optPlanCharge_CheckedChanged);
            // 
            // optOperationSimul
            // 
            this.optOperationSimul.AutoSize = true;
            this.optOperationSimul.Checked = true;
            this.optOperationSimul.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optOperationSimul.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optOperationSimul.Location = new System.Drawing.Point(3, 3);
            this.optOperationSimul.Name = "optOperationSimul";
            this.optOperationSimul.Size = new System.Drawing.Size(253, 27);
            this.optOperationSimul.TabIndex = 538;
            this.optOperationSimul.TabStop = true;
            this.optOperationSimul.Text = "Gammes";
            this.optOperationSimul.UseVisualStyleBackColor = true;
            this.optOperationSimul.Visible = false;
            this.optOperationSimul.CheckedChanged += new System.EventHandler(this.optOperationSimul_CheckedChanged);
            // 
            // txtTotduree
            // 
            this.txtTotduree.AccAcceptNumbersOnly = false;
            this.txtTotduree.AccAllowComma = false;
            this.txtTotduree.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTotduree.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTotduree.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTotduree.AccHidenValue = "";
            this.txtTotduree.AccNotAllowedChars = null;
            this.txtTotduree.AccReadOnly = false;
            this.txtTotduree.AccReadOnlyAllowDelete = false;
            this.txtTotduree.AccRequired = false;
            this.txtTotduree.BackColor = System.Drawing.Color.White;
            this.txtTotduree.CustomBackColor = System.Drawing.Color.White;
            this.txtTotduree.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.txtTotduree.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtTotduree.ForeColor = System.Drawing.Color.Black;
            this.txtTotduree.Location = new System.Drawing.Point(1601, 4);
            this.txtTotduree.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotduree.MaxLength = 32767;
            this.txtTotduree.Multiline = false;
            this.txtTotduree.Name = "txtTotduree";
            this.txtTotduree.ReadOnly = false;
            this.txtTotduree.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTotduree.Size = new System.Drawing.Size(243, 27);
            this.txtTotduree.TabIndex = 543;
            this.txtTotduree.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTotduree.UseSystemPasswordChar = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(1505, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(91, 33);
            this.label6.TabIndex = 542;
            this.label6.Text = "Total Durée";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // pctPlanCharge
            // 
            this.pctPlanCharge.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel5.SetColumnSpan(this.pctPlanCharge, 2);
            this.pctPlanCharge.Controls.Add(this.tableLayoutPanel6);
            this.pctPlanCharge.Dock = System.Windows.Forms.DockStyle.Fill;
            this.pctPlanCharge.Location = new System.Drawing.Point(779, 2);
            this.pctPlanCharge.Margin = new System.Windows.Forms.Padding(2);
            this.pctPlanCharge.Name = "pctPlanCharge";
            this.pctPlanCharge.Size = new System.Drawing.Size(721, 29);
            this.pctPlanCharge.TabIndex = 544;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.optPlanChargeChantier, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.optPlanChargeIntervenant, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(719, 27);
            this.tableLayoutPanel6.TabIndex = 0;
            // 
            // optPlanChargeChantier
            // 
            this.optPlanChargeChantier.AutoSize = true;
            this.optPlanChargeChantier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optPlanChargeChantier.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optPlanChargeChantier.Location = new System.Drawing.Point(362, 3);
            this.optPlanChargeChantier.Name = "optPlanChargeChantier";
            this.optPlanChargeChantier.Size = new System.Drawing.Size(354, 21);
            this.optPlanChargeChantier.TabIndex = 539;
            this.optPlanChargeChantier.Text = "Par chantier";
            this.optPlanChargeChantier.UseVisualStyleBackColor = true;
            this.optPlanChargeChantier.CheckedChanged += new System.EventHandler(this.optPlanChargeChantier_CheckedChanged);
            // 
            // optPlanChargeIntervenant
            // 
            this.optPlanChargeIntervenant.AutoSize = true;
            this.optPlanChargeIntervenant.Checked = true;
            this.optPlanChargeIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optPlanChargeIntervenant.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optPlanChargeIntervenant.Location = new System.Drawing.Point(3, 3);
            this.optPlanChargeIntervenant.Name = "optPlanChargeIntervenant";
            this.optPlanChargeIntervenant.Size = new System.Drawing.Size(353, 21);
            this.optPlanChargeIntervenant.TabIndex = 538;
            this.optPlanChargeIntervenant.TabStop = true;
            this.optPlanChargeIntervenant.Text = "Par intervenant";
            this.optPlanChargeIntervenant.UseVisualStyleBackColor = true;
            this.optPlanChargeIntervenant.CheckedChanged += new System.EventHandler(this.optPlanChargeIntervenant_CheckedChanged);
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 2;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 60.64935F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 39.35065F));
            this.tableLayoutPanel20.Controls.Add(this.frame1110, 0, 0);
            this.tableLayoutPanel20.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(3, 3);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 1;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(1842, 44);
            this.tableLayoutPanel20.TabIndex = 417;
            // 
            // frame1110
            // 
            this.frame1110.BackColor = System.Drawing.Color.Transparent;
            this.frame1110.Controls.Add(this.cmdPrestationSimul);
            this.frame1110.Controls.Add(this.cmdMotifSimul);
            this.frame1110.Controls.Add(this.cmdRechercheContratSimul);
            this.frame1110.Controls.Add(this.txtPrestationSimul);
            this.frame1110.Controls.Add(this.txtCOP_AvenantSimul);
            this.frame1110.Controls.Add(this.txtCOP_NoContratSimul);
            this.frame1110.Controls.Add(this.txtCAI_CodeSimul);
            this.frame1110.Controls.Add(this.label41);
            this.frame1110.Controls.Add(this.label40);
            this.frame1110.Controls.Add(this.label39);
            this.frame1110.Controls.Add(this.label38);
            this.frame1110.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frame1110.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.frame1110.Location = new System.Drawing.Point(3, 3);
            this.frame1110.Name = "frame1110";
            this.frame1110.Size = new System.Drawing.Size(1111, 38);
            this.frame1110.TabIndex = 416;
            this.frame1110.TabStop = false;
            this.frame1110.Visible = false;
            // 
            // cmdPrestationSimul
            // 
            this.cmdPrestationSimul.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdPrestationSimul.FlatAppearance.BorderSize = 0;
            this.cmdPrestationSimul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPrestationSimul.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdPrestationSimul.Image = ((System.Drawing.Image)(resources.GetObject("cmdPrestationSimul.Image")));
            this.cmdPrestationSimul.Location = new System.Drawing.Point(405, 13);
            this.cmdPrestationSimul.Name = "cmdPrestationSimul";
            this.cmdPrestationSimul.Size = new System.Drawing.Size(25, 20);
            this.cmdPrestationSimul.TabIndex = 508;
            this.cmdPrestationSimul.UseVisualStyleBackColor = false;
            this.cmdPrestationSimul.Click += new System.EventHandler(this.cmdPrestationSimul_Click);
            // 
            // cmdMotifSimul
            // 
            this.cmdMotifSimul.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdMotifSimul.FlatAppearance.BorderSize = 0;
            this.cmdMotifSimul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMotifSimul.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdMotifSimul.Image = ((System.Drawing.Image)(resources.GetObject("cmdMotifSimul.Image")));
            this.cmdMotifSimul.Location = new System.Drawing.Point(374, 13);
            this.cmdMotifSimul.Name = "cmdMotifSimul";
            this.cmdMotifSimul.Size = new System.Drawing.Size(25, 20);
            this.cmdMotifSimul.TabIndex = 507;
            this.cmdMotifSimul.UseVisualStyleBackColor = false;
            this.cmdMotifSimul.Click += new System.EventHandler(this.cmdMotifSimul_Click);
            // 
            // cmdRechercheContratSimul
            // 
            this.cmdRechercheContratSimul.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheContratSimul.FlatAppearance.BorderSize = 0;
            this.cmdRechercheContratSimul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheContratSimul.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheContratSimul.Image = ((System.Drawing.Image)(resources.GetObject("cmdRechercheContratSimul.Image")));
            this.cmdRechercheContratSimul.Location = new System.Drawing.Point(343, 12);
            this.cmdRechercheContratSimul.Name = "cmdRechercheContratSimul";
            this.cmdRechercheContratSimul.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheContratSimul.TabIndex = 506;
            this.cmdRechercheContratSimul.UseVisualStyleBackColor = false;
            // 
            // txtPrestationSimul
            // 
            this.txtPrestationSimul.AccAcceptNumbersOnly = false;
            this.txtPrestationSimul.AccAllowComma = false;
            this.txtPrestationSimul.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPrestationSimul.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPrestationSimul.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtPrestationSimul.AccHidenValue = "";
            this.txtPrestationSimul.AccNotAllowedChars = null;
            this.txtPrestationSimul.AccReadOnly = false;
            this.txtPrestationSimul.AccReadOnlyAllowDelete = false;
            this.txtPrestationSimul.AccRequired = false;
            this.txtPrestationSimul.BackColor = System.Drawing.Color.White;
            this.txtPrestationSimul.CustomBackColor = System.Drawing.Color.White;
            this.txtPrestationSimul.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtPrestationSimul.ForeColor = System.Drawing.Color.Black;
            this.txtPrestationSimul.Location = new System.Drawing.Point(290, 11);
            this.txtPrestationSimul.Margin = new System.Windows.Forms.Padding(2);
            this.txtPrestationSimul.MaxLength = 32767;
            this.txtPrestationSimul.Multiline = false;
            this.txtPrestationSimul.Name = "txtPrestationSimul";
            this.txtPrestationSimul.ReadOnly = false;
            this.txtPrestationSimul.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPrestationSimul.Size = new System.Drawing.Size(48, 27);
            this.txtPrestationSimul.TabIndex = 505;
            this.txtPrestationSimul.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPrestationSimul.UseSystemPasswordChar = false;
            this.txtPrestationSimul.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPrestationSimul_KeyPress);
            // 
            // txtCOP_AvenantSimul
            // 
            this.txtCOP_AvenantSimul.AccAcceptNumbersOnly = false;
            this.txtCOP_AvenantSimul.AccAllowComma = false;
            this.txtCOP_AvenantSimul.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCOP_AvenantSimul.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCOP_AvenantSimul.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCOP_AvenantSimul.AccHidenValue = "";
            this.txtCOP_AvenantSimul.AccNotAllowedChars = null;
            this.txtCOP_AvenantSimul.AccReadOnly = false;
            this.txtCOP_AvenantSimul.AccReadOnlyAllowDelete = false;
            this.txtCOP_AvenantSimul.AccRequired = false;
            this.txtCOP_AvenantSimul.BackColor = System.Drawing.Color.White;
            this.txtCOP_AvenantSimul.CustomBackColor = System.Drawing.Color.White;
            this.txtCOP_AvenantSimul.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCOP_AvenantSimul.ForeColor = System.Drawing.Color.Black;
            this.txtCOP_AvenantSimul.Location = new System.Drawing.Point(252, 11);
            this.txtCOP_AvenantSimul.Margin = new System.Windows.Forms.Padding(2);
            this.txtCOP_AvenantSimul.MaxLength = 32767;
            this.txtCOP_AvenantSimul.Multiline = false;
            this.txtCOP_AvenantSimul.Name = "txtCOP_AvenantSimul";
            this.txtCOP_AvenantSimul.ReadOnly = false;
            this.txtCOP_AvenantSimul.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCOP_AvenantSimul.Size = new System.Drawing.Size(34, 27);
            this.txtCOP_AvenantSimul.TabIndex = 504;
            this.txtCOP_AvenantSimul.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCOP_AvenantSimul.UseSystemPasswordChar = false;
            // 
            // txtCOP_NoContratSimul
            // 
            this.txtCOP_NoContratSimul.AccAcceptNumbersOnly = false;
            this.txtCOP_NoContratSimul.AccAllowComma = false;
            this.txtCOP_NoContratSimul.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCOP_NoContratSimul.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCOP_NoContratSimul.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCOP_NoContratSimul.AccHidenValue = "";
            this.txtCOP_NoContratSimul.AccNotAllowedChars = null;
            this.txtCOP_NoContratSimul.AccReadOnly = false;
            this.txtCOP_NoContratSimul.AccReadOnlyAllowDelete = false;
            this.txtCOP_NoContratSimul.AccRequired = false;
            this.txtCOP_NoContratSimul.BackColor = System.Drawing.Color.White;
            this.txtCOP_NoContratSimul.CustomBackColor = System.Drawing.Color.White;
            this.txtCOP_NoContratSimul.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCOP_NoContratSimul.ForeColor = System.Drawing.Color.Black;
            this.txtCOP_NoContratSimul.Location = new System.Drawing.Point(190, 11);
            this.txtCOP_NoContratSimul.Margin = new System.Windows.Forms.Padding(2);
            this.txtCOP_NoContratSimul.MaxLength = 32767;
            this.txtCOP_NoContratSimul.Multiline = false;
            this.txtCOP_NoContratSimul.Name = "txtCOP_NoContratSimul";
            this.txtCOP_NoContratSimul.ReadOnly = false;
            this.txtCOP_NoContratSimul.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCOP_NoContratSimul.Size = new System.Drawing.Size(33, 27);
            this.txtCOP_NoContratSimul.TabIndex = 503;
            this.txtCOP_NoContratSimul.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCOP_NoContratSimul.UseSystemPasswordChar = false;
            // 
            // txtCAI_CodeSimul
            // 
            this.txtCAI_CodeSimul.AccAcceptNumbersOnly = false;
            this.txtCAI_CodeSimul.AccAllowComma = false;
            this.txtCAI_CodeSimul.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCAI_CodeSimul.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCAI_CodeSimul.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCAI_CodeSimul.AccHidenValue = "";
            this.txtCAI_CodeSimul.AccNotAllowedChars = null;
            this.txtCAI_CodeSimul.AccReadOnly = false;
            this.txtCAI_CodeSimul.AccReadOnlyAllowDelete = false;
            this.txtCAI_CodeSimul.AccRequired = false;
            this.txtCAI_CodeSimul.BackColor = System.Drawing.Color.White;
            this.txtCAI_CodeSimul.CustomBackColor = System.Drawing.Color.White;
            this.txtCAI_CodeSimul.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCAI_CodeSimul.ForeColor = System.Drawing.Color.Black;
            this.txtCAI_CodeSimul.Location = new System.Drawing.Point(227, 11);
            this.txtCAI_CodeSimul.Margin = new System.Windows.Forms.Padding(2);
            this.txtCAI_CodeSimul.MaxLength = 32767;
            this.txtCAI_CodeSimul.Multiline = false;
            this.txtCAI_CodeSimul.Name = "txtCAI_CodeSimul";
            this.txtCAI_CodeSimul.ReadOnly = false;
            this.txtCAI_CodeSimul.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCAI_CodeSimul.Size = new System.Drawing.Size(21, 27);
            this.txtCAI_CodeSimul.TabIndex = 502;
            this.txtCAI_CodeSimul.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCAI_CodeSimul.UseSystemPasswordChar = false;
            this.txtCAI_CodeSimul.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCAI_CodeSimul_KeyPress);
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(158, 11);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(31, 19);
            this.label41.TabIndex = 387;
            this.label41.Text = "Avt";
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(65, 11);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(46, 19);
            this.label40.TabIndex = 386;
            this.label40.Text = "Motif";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(97, 11);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(81, 19);
            this.label39.TabIndex = 385;
            this.label39.Text = "Prestation";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(2, 11);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(82, 19);
            this.label38.TabIndex = 384;
            this.label38.Text = "N° Contrat";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.cmdSimulation);
            this.flowLayoutPanel1.Controls.Add(this.CmdEditer);
            this.flowLayoutPanel1.Controls.Add(this.cmdCleanSimul);
            this.flowLayoutPanel1.Controls.Add(this.cmdAfficheSimul);
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1541, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(298, 38);
            this.flowLayoutPanel1.TabIndex = 412;
            // 
            // cmdSimulation
            // 
            this.cmdSimulation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSimulation.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSimulation.FlatAppearance.BorderSize = 0;
            this.cmdSimulation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSimulation.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSimulation.ForeColor = System.Drawing.Color.White;
            this.cmdSimulation.Image = ((System.Drawing.Image)(resources.GetObject("cmdSimulation.Image")));
            this.cmdSimulation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSimulation.Location = new System.Drawing.Point(194, 2);
            this.cmdSimulation.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSimulation.Name = "cmdSimulation";
            this.cmdSimulation.Size = new System.Drawing.Size(102, 35);
            this.cmdSimulation.TabIndex = 568;
            this.cmdSimulation.Text = "    Simulation";
            this.cmdSimulation.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdSimulation.UseVisualStyleBackColor = false;
            this.cmdSimulation.Click += new System.EventHandler(this.cmdSimulation_Click);
            // 
            // CmdEditer
            // 
            this.CmdEditer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdEditer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdEditer.FlatAppearance.BorderSize = 0;
            this.CmdEditer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdEditer.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdEditer.Image = ((System.Drawing.Image)(resources.GetObject("CmdEditer.Image")));
            this.CmdEditer.Location = new System.Drawing.Point(130, 2);
            this.CmdEditer.Margin = new System.Windows.Forms.Padding(2);
            this.CmdEditer.Name = "CmdEditer";
            this.CmdEditer.Size = new System.Drawing.Size(60, 35);
            this.CmdEditer.TabIndex = 540;
            this.CmdEditer.Tag = "";
            this.CmdEditer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.CmdEditer, "Exporter la sélection");
            this.CmdEditer.UseVisualStyleBackColor = false;
            this.CmdEditer.Click += new System.EventHandler(this.CmdEditer_Click);
            // 
            // cmdCleanSimul
            // 
            this.cmdCleanSimul.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdCleanSimul.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCleanSimul.FlatAppearance.BorderSize = 0;
            this.cmdCleanSimul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCleanSimul.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCleanSimul.Image = ((System.Drawing.Image)(resources.GetObject("cmdCleanSimul.Image")));
            this.cmdCleanSimul.Location = new System.Drawing.Point(66, 2);
            this.cmdCleanSimul.Margin = new System.Windows.Forms.Padding(2);
            this.cmdCleanSimul.Name = "cmdCleanSimul";
            this.cmdCleanSimul.Size = new System.Drawing.Size(60, 35);
            this.cmdCleanSimul.TabIndex = 373;
            this.cmdCleanSimul.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdCleanSimul.UseVisualStyleBackColor = false;
            this.cmdCleanSimul.Click += new System.EventHandler(this.cmdCleanSimul_Click);
            // 
            // cmdAfficheSimul
            // 
            this.cmdAfficheSimul.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAfficheSimul.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAfficheSimul.FlatAppearance.BorderSize = 0;
            this.cmdAfficheSimul.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAfficheSimul.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAfficheSimul.Image = ((System.Drawing.Image)(resources.GetObject("cmdAfficheSimul.Image")));
            this.cmdAfficheSimul.Location = new System.Drawing.Point(2, 2);
            this.cmdAfficheSimul.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAfficheSimul.Name = "cmdAfficheSimul";
            this.cmdAfficheSimul.Size = new System.Drawing.Size(60, 35);
            this.cmdAfficheSimul.TabIndex = 374;
            this.cmdAfficheSimul.Tag = "";
            this.cmdAfficheSimul.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAfficheSimul, "Rechercher");
            this.cmdAfficheSimul.UseVisualStyleBackColor = false;
            this.cmdAfficheSimul.Visible = false;
            this.cmdAfficheSimul.Click += new System.EventHandler(this.cmdAfficheSimul_Click);
            // 
            // ultraTabPageControl7
            // 
            this.ultraTabPageControl7.Controls.Add(this.tableLayoutPanel22);
            this.ultraTabPageControl7.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl7.Name = "ultraTabPageControl7";
            this.ultraTabPageControl7.Size = new System.Drawing.Size(1848, 933);
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 1;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel22.Controls.Add(this.tableLayoutPanel23, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.panel1, 0, 1);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel22.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 2;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 23.15112F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 76.84888F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(1848, 933);
            this.tableLayoutPanel22.TabIndex = 0;
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 2;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel23.Controls.Add(this.tableLayoutPanel24, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.tableLayoutPanel28, 1, 0);
            this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel23.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel23.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 1;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(1848, 215);
            this.tableLayoutPanel23.TabIndex = 0;
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 3;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 132F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 35F));
            this.tableLayoutPanel24.Controls.Add(this.cmd_3, 2, 3);
            this.tableLayoutPanel24.Controls.Add(this.cmd_2, 2, 2);
            this.tableLayoutPanel24.Controls.Add(this.cmd_5, 2, 1);
            this.tableLayoutPanel24.Controls.Add(this.cmd_4, 2, 0);
            this.tableLayoutPanel24.Controls.Add(this.txt_1, 1, 1);
            this.tableLayoutPanel24.Controls.Add(this.label47, 0, 3);
            this.tableLayoutPanel24.Controls.Add(this.label46, 0, 2);
            this.tableLayoutPanel24.Controls.Add(this.label44, 0, 1);
            this.tableLayoutPanel24.Controls.Add(this.label11, 0, 0);
            this.tableLayoutPanel24.Controls.Add(this.txt_0, 1, 0);
            this.tableLayoutPanel24.Controls.Add(this.tableLayoutPanel25, 1, 2);
            this.tableLayoutPanel24.Controls.Add(this.tableLayoutPanel26, 1, 3);
            this.tableLayoutPanel24.Controls.Add(this.tableLayoutPanel27, 1, 4);
            this.tableLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel24.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel24.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 5;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(924, 215);
            this.tableLayoutPanel24.TabIndex = 375;
            // 
            // cmd_3
            // 
            this.cmd_3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmd_3.FlatAppearance.BorderSize = 0;
            this.cmd_3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd_3.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmd_3.Image = ((System.Drawing.Image)(resources.GetObject("cmd_3.Image")));
            this.cmd_3.Location = new System.Drawing.Point(892, 93);
            this.cmd_3.Name = "cmd_3";
            this.cmd_3.Size = new System.Drawing.Size(25, 20);
            this.cmd_3.TabIndex = 510;
            this.cmd_3.Tag = "3";
            this.cmd_3.UseVisualStyleBackColor = false;
            this.cmd_3.Click += new System.EventHandler(this.cmd_1_Click);
            // 
            // cmd_2
            // 
            this.cmd_2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmd_2.FlatAppearance.BorderSize = 0;
            this.cmd_2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd_2.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmd_2.Image = ((System.Drawing.Image)(resources.GetObject("cmd_2.Image")));
            this.cmd_2.Location = new System.Drawing.Point(892, 63);
            this.cmd_2.Name = "cmd_2";
            this.cmd_2.Size = new System.Drawing.Size(25, 20);
            this.cmd_2.TabIndex = 509;
            this.cmd_2.Tag = "2";
            this.cmd_2.UseVisualStyleBackColor = false;
            this.cmd_2.Click += new System.EventHandler(this.cmd_1_Click);
            // 
            // cmd_5
            // 
            this.cmd_5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmd_5.FlatAppearance.BorderSize = 0;
            this.cmd_5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd_5.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmd_5.Image = ((System.Drawing.Image)(resources.GetObject("cmd_5.Image")));
            this.cmd_5.Location = new System.Drawing.Point(892, 33);
            this.cmd_5.Name = "cmd_5";
            this.cmd_5.Size = new System.Drawing.Size(25, 20);
            this.cmd_5.TabIndex = 508;
            this.cmd_5.Tag = "5";
            this.cmd_5.UseVisualStyleBackColor = false;
            this.cmd_5.Click += new System.EventHandler(this.cmd_1_Click);
            // 
            // cmd_4
            // 
            this.cmd_4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmd_4.FlatAppearance.BorderSize = 0;
            this.cmd_4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd_4.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmd_4.Image = ((System.Drawing.Image)(resources.GetObject("cmd_4.Image")));
            this.cmd_4.Location = new System.Drawing.Point(892, 3);
            this.cmd_4.Name = "cmd_4";
            this.cmd_4.Size = new System.Drawing.Size(25, 20);
            this.cmd_4.TabIndex = 507;
            this.cmd_4.Tag = "4";
            this.cmd_4.UseVisualStyleBackColor = false;
            this.cmd_4.Click += new System.EventHandler(this.cmd_1_Click);
            // 
            // txt_1
            // 
            this.txt_1.AccAcceptNumbersOnly = false;
            this.txt_1.AccAllowComma = false;
            this.txt_1.AccBackgroundColor = System.Drawing.Color.White;
            this.txt_1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt_1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt_1.AccHidenValue = "";
            this.txt_1.AccNotAllowedChars = null;
            this.txt_1.AccReadOnly = false;
            this.txt_1.AccReadOnlyAllowDelete = false;
            this.txt_1.AccRequired = false;
            this.txt_1.BackColor = System.Drawing.Color.White;
            this.txt_1.CustomBackColor = System.Drawing.Color.White;
            this.txt_1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt_1.ForeColor = System.Drawing.Color.Black;
            this.txt_1.Location = new System.Drawing.Point(134, 32);
            this.txt_1.Margin = new System.Windows.Forms.Padding(2);
            this.txt_1.MaxLength = 32767;
            this.txt_1.Multiline = false;
            this.txt_1.Name = "txt_1";
            this.txt_1.ReadOnly = false;
            this.txt_1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt_1.Size = new System.Drawing.Size(753, 27);
            this.txt_1.TabIndex = 390;
            this.txt_1.Tag = "1";
            this.txt_1.TextAlignment = Infragistics.Win.HAlign.Default;
            this.txt_1.UseSystemPasswordChar = false;
            this.txt_1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_0_KeyPress);
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label47.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(3, 90);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(126, 30);
            this.label47.TabIndex = 388;
            this.label47.Text = "Chef de secteur";
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label46.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(3, 60);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(126, 30);
            this.label46.TabIndex = 387;
            this.label46.Text = "Intervenant";
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label44.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(3, 30);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(126, 30);
            this.label44.TabIndex = 386;
            this.label44.Text = "Gérant";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 0);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(126, 30);
            this.label11.TabIndex = 385;
            this.label11.Text = "Immeuble";
            // 
            // txt_0
            // 
            this.txt_0.AccAcceptNumbersOnly = false;
            this.txt_0.AccAllowComma = false;
            this.txt_0.AccBackgroundColor = System.Drawing.Color.White;
            this.txt_0.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt_0.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt_0.AccHidenValue = "";
            this.txt_0.AccNotAllowedChars = null;
            this.txt_0.AccReadOnly = false;
            this.txt_0.AccReadOnlyAllowDelete = false;
            this.txt_0.AccRequired = false;
            this.txt_0.BackColor = System.Drawing.Color.White;
            this.txt_0.CustomBackColor = System.Drawing.Color.White;
            this.txt_0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_0.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt_0.ForeColor = System.Drawing.Color.Black;
            this.txt_0.Location = new System.Drawing.Point(134, 2);
            this.txt_0.Margin = new System.Windows.Forms.Padding(2);
            this.txt_0.MaxLength = 32767;
            this.txt_0.Multiline = false;
            this.txt_0.Name = "txt_0";
            this.txt_0.ReadOnly = false;
            this.txt_0.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt_0.Size = new System.Drawing.Size(753, 27);
            this.txt_0.TabIndex = 389;
            this.txt_0.Tag = "0";
            this.txt_0.TextAlignment = Infragistics.Win.HAlign.Default;
            this.txt_0.UseSystemPasswordChar = false;
            this.txt_0.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_0_KeyPress);
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 2;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.Controls.Add(this.txt_3, 1, 0);
            this.tableLayoutPanel25.Controls.Add(this.txt_2, 0, 0);
            this.tableLayoutPanel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel25.Location = new System.Drawing.Point(132, 60);
            this.tableLayoutPanel25.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 1;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(757, 30);
            this.tableLayoutPanel25.TabIndex = 392;
            // 
            // txt_3
            // 
            this.txt_3.AccAcceptNumbersOnly = false;
            this.txt_3.AccAllowComma = false;
            this.txt_3.AccBackgroundColor = System.Drawing.Color.White;
            this.txt_3.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt_3.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt_3.AccHidenValue = "";
            this.txt_3.AccNotAllowedChars = null;
            this.txt_3.AccReadOnly = false;
            this.txt_3.AccReadOnlyAllowDelete = false;
            this.txt_3.AccRequired = false;
            this.txt_3.BackColor = System.Drawing.Color.White;
            this.txt_3.CustomBackColor = System.Drawing.Color.White;
            this.txt_3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_3.Enabled = false;
            this.txt_3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt_3.ForeColor = System.Drawing.Color.Black;
            this.txt_3.Location = new System.Drawing.Point(380, 2);
            this.txt_3.Margin = new System.Windows.Forms.Padding(2);
            this.txt_3.MaxLength = 32767;
            this.txt_3.Multiline = false;
            this.txt_3.Name = "txt_3";
            this.txt_3.ReadOnly = false;
            this.txt_3.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt_3.Size = new System.Drawing.Size(375, 27);
            this.txt_3.TabIndex = 392;
            this.txt_3.Tag = "3";
            this.txt_3.TextAlignment = Infragistics.Win.HAlign.Default;
            this.txt_3.UseSystemPasswordChar = false;
            // 
            // txt_2
            // 
            this.txt_2.AccAcceptNumbersOnly = false;
            this.txt_2.AccAllowComma = false;
            this.txt_2.AccBackgroundColor = System.Drawing.Color.White;
            this.txt_2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt_2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt_2.AccHidenValue = "";
            this.txt_2.AccNotAllowedChars = null;
            this.txt_2.AccReadOnly = false;
            this.txt_2.AccReadOnlyAllowDelete = false;
            this.txt_2.AccRequired = false;
            this.txt_2.BackColor = System.Drawing.Color.White;
            this.txt_2.CustomBackColor = System.Drawing.Color.White;
            this.txt_2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt_2.ForeColor = System.Drawing.Color.Black;
            this.txt_2.Location = new System.Drawing.Point(2, 2);
            this.txt_2.Margin = new System.Windows.Forms.Padding(2);
            this.txt_2.MaxLength = 32767;
            this.txt_2.Multiline = false;
            this.txt_2.Name = "txt_2";
            this.txt_2.ReadOnly = false;
            this.txt_2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt_2.Size = new System.Drawing.Size(374, 27);
            this.txt_2.TabIndex = 391;
            this.txt_2.Tag = "2";
            this.txt_2.TextAlignment = Infragistics.Win.HAlign.Default;
            this.txt_2.UseSystemPasswordChar = false;
            this.txt_2.TextChanged += new System.EventHandler(this.txt_2_TextChanged);
            this.txt_2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_0_KeyPress);
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 2;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.Controls.Add(this.txt_5, 0, 0);
            this.tableLayoutPanel26.Controls.Add(this.txt_4, 0, 0);
            this.tableLayoutPanel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel26.Location = new System.Drawing.Point(132, 90);
            this.tableLayoutPanel26.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 1;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(757, 30);
            this.tableLayoutPanel26.TabIndex = 393;
            // 
            // txt_5
            // 
            this.txt_5.AccAcceptNumbersOnly = false;
            this.txt_5.AccAllowComma = false;
            this.txt_5.AccBackgroundColor = System.Drawing.Color.White;
            this.txt_5.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt_5.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt_5.AccHidenValue = "";
            this.txt_5.AccNotAllowedChars = null;
            this.txt_5.AccReadOnly = false;
            this.txt_5.AccReadOnlyAllowDelete = false;
            this.txt_5.AccRequired = false;
            this.txt_5.BackColor = System.Drawing.Color.White;
            this.txt_5.CustomBackColor = System.Drawing.Color.White;
            this.txt_5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_5.Enabled = false;
            this.txt_5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt_5.ForeColor = System.Drawing.Color.Black;
            this.txt_5.Location = new System.Drawing.Point(380, 2);
            this.txt_5.Margin = new System.Windows.Forms.Padding(2);
            this.txt_5.MaxLength = 32767;
            this.txt_5.Multiline = false;
            this.txt_5.Name = "txt_5";
            this.txt_5.ReadOnly = false;
            this.txt_5.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt_5.Size = new System.Drawing.Size(375, 27);
            this.txt_5.TabIndex = 393;
            this.txt_5.Tag = "5";
            this.txt_5.TextAlignment = Infragistics.Win.HAlign.Default;
            this.txt_5.UseSystemPasswordChar = false;
            // 
            // txt_4
            // 
            this.txt_4.AccAcceptNumbersOnly = false;
            this.txt_4.AccAllowComma = false;
            this.txt_4.AccBackgroundColor = System.Drawing.Color.White;
            this.txt_4.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt_4.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt_4.AccHidenValue = "";
            this.txt_4.AccNotAllowedChars = null;
            this.txt_4.AccReadOnly = false;
            this.txt_4.AccReadOnlyAllowDelete = false;
            this.txt_4.AccRequired = false;
            this.txt_4.BackColor = System.Drawing.Color.White;
            this.txt_4.CustomBackColor = System.Drawing.Color.White;
            this.txt_4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txt_4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt_4.ForeColor = System.Drawing.Color.Black;
            this.txt_4.Location = new System.Drawing.Point(2, 2);
            this.txt_4.Margin = new System.Windows.Forms.Padding(2);
            this.txt_4.MaxLength = 32767;
            this.txt_4.Multiline = false;
            this.txt_4.Name = "txt_4";
            this.txt_4.ReadOnly = false;
            this.txt_4.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt_4.Size = new System.Drawing.Size(374, 27);
            this.txt_4.TabIndex = 392;
            this.txt_4.Tag = "4";
            this.txt_4.TextAlignment = Infragistics.Win.HAlign.Default;
            this.txt_4.UseSystemPasswordChar = false;
            this.txt_4.TextChanged += new System.EventHandler(this.txt_2_TextChanged);
            this.txt_4.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txt_0_KeyPress);
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 2;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.Controls.Add(this.Label56_25, 1, 0);
            this.tableLayoutPanel27.Controls.Add(this.groupBox13, 0, 0);
            this.tableLayoutPanel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel27.Location = new System.Drawing.Point(132, 120);
            this.tableLayoutPanel27.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 1;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(757, 95);
            this.tableLayoutPanel27.TabIndex = 511;
            // 
            // Label56_25
            // 
            this.Label56_25.AccAcceptNumbersOnly = false;
            this.Label56_25.AccAllowComma = false;
            this.Label56_25.AccBackgroundColor = System.Drawing.Color.White;
            this.Label56_25.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Label56_25.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Label56_25.AccHidenValue = "";
            this.Label56_25.AccNotAllowedChars = null;
            this.Label56_25.AccReadOnly = false;
            this.Label56_25.AccReadOnlyAllowDelete = false;
            this.Label56_25.AccRequired = false;
            this.Label56_25.BackColor = System.Drawing.Color.White;
            this.Label56_25.CustomBackColor = System.Drawing.Color.White;
            this.Label56_25.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Label56_25.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label56_25.ForeColor = System.Drawing.Color.Black;
            this.Label56_25.Location = new System.Drawing.Point(380, 66);
            this.Label56_25.Margin = new System.Windows.Forms.Padding(2);
            this.Label56_25.MaxLength = 32767;
            this.Label56_25.Multiline = false;
            this.Label56_25.Name = "Label56_25";
            this.Label56_25.ReadOnly = false;
            this.Label56_25.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Label56_25.Size = new System.Drawing.Size(375, 27);
            this.Label56_25.TabIndex = 512;
            this.Label56_25.Tag = "25";
            this.Label56_25.TextAlignment = Infragistics.Win.HAlign.Default;
            this.Label56_25.UseSystemPasswordChar = false;
            // 
            // groupBox13
            // 
            this.groupBox13.Controls.Add(this.Opt_8);
            this.groupBox13.Controls.Add(this.Opt_7);
            this.groupBox13.Controls.Add(this.Opt_6);
            this.groupBox13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox13.Location = new System.Drawing.Point(3, 5);
            this.groupBox13.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.groupBox13.Name = "groupBox13";
            this.groupBox13.Size = new System.Drawing.Size(372, 87);
            this.groupBox13.TabIndex = 511;
            this.groupBox13.TabStop = false;
            // 
            // Opt_8
            // 
            this.Opt_8.AutoSize = true;
            this.Opt_8.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Opt_8.Location = new System.Drawing.Point(6, 60);
            this.Opt_8.Name = "Opt_8";
            this.Opt_8.Size = new System.Drawing.Size(217, 23);
            this.Opt_8.TabIndex = 615;
            this.Opt_8.TabStop = true;
            this.Opt_8.Tag = "8";
            this.Opt_8.Text = "Charge par chef de secteur";
            this.Opt_8.UseVisualStyleBackColor = true;
            // 
            // Opt_7
            // 
            this.Opt_7.AutoSize = true;
            this.Opt_7.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Opt_7.Location = new System.Drawing.Point(6, 31);
            this.Opt_7.Name = "Opt_7";
            this.Opt_7.Size = new System.Drawing.Size(176, 23);
            this.Opt_7.TabIndex = 614;
            this.Opt_7.TabStop = true;
            this.Opt_7.Tag = "7";
            this.Opt_7.Text = "Charge par immeuble";
            this.Opt_7.UseVisualStyleBackColor = true;
            // 
            // Opt_6
            // 
            this.Opt_6.AutoSize = true;
            this.Opt_6.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Opt_6.Location = new System.Drawing.Point(6, 2);
            this.Opt_6.Name = "Opt_6";
            this.Opt_6.Size = new System.Drawing.Size(188, 23);
            this.Opt_6.TabIndex = 613;
            this.Opt_6.TabStop = true;
            this.Opt_6.Tag = "6";
            this.Opt_6.Text = "Charge par intervenant";
            this.Opt_6.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 2;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.Controls.Add(this.flowLayoutPanel7, 1, 0);
            this.tableLayoutPanel28.Controls.Add(this.Label56_24, 1, 1);
            this.tableLayoutPanel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel28.Location = new System.Drawing.Point(924, 0);
            this.tableLayoutPanel28.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 2;
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.Size = new System.Drawing.Size(924, 215);
            this.tableLayoutPanel28.TabIndex = 376;
            // 
            // flowLayoutPanel7
            // 
            this.flowLayoutPanel7.Controls.Add(this.cmd_7);
            this.flowLayoutPanel7.Controls.Add(this.cmd_6);
            this.flowLayoutPanel7.Controls.Add(this.cmd_1);
            this.flowLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel7.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(465, 3);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(456, 101);
            this.flowLayoutPanel7.TabIndex = 376;
            // 
            // cmd_7
            // 
            this.cmd_7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmd_7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmd_7.FlatAppearance.BorderSize = 0;
            this.cmd_7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd_7.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_7.Image = ((System.Drawing.Image)(resources.GetObject("cmd_7.Image")));
            this.cmd_7.Location = new System.Drawing.Point(394, 2);
            this.cmd_7.Margin = new System.Windows.Forms.Padding(2);
            this.cmd_7.Name = "cmd_7";
            this.cmd_7.Size = new System.Drawing.Size(60, 35);
            this.cmd_7.TabIndex = 541;
            this.cmd_7.Tag = "7";
            this.cmd_7.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmd_7.UseVisualStyleBackColor = false;
            this.cmd_7.Click += new System.EventHandler(this.cmd_1_Click);
            // 
            // cmd_6
            // 
            this.cmd_6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmd_6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmd_6.FlatAppearance.BorderSize = 0;
            this.cmd_6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd_6.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_6.Image = ((System.Drawing.Image)(resources.GetObject("cmd_6.Image")));
            this.cmd_6.Location = new System.Drawing.Point(330, 2);
            this.cmd_6.Margin = new System.Windows.Forms.Padding(2);
            this.cmd_6.Name = "cmd_6";
            this.cmd_6.Size = new System.Drawing.Size(60, 35);
            this.cmd_6.TabIndex = 543;
            this.cmd_6.Tag = "6";
            this.cmd_6.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmd_6, "Exporter la sélection");
            this.cmd_6.UseVisualStyleBackColor = false;
            this.cmd_6.Click += new System.EventHandler(this.cmd_1_Click);
            // 
            // cmd_1
            // 
            this.cmd_1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmd_1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmd_1.FlatAppearance.BorderSize = 0;
            this.cmd_1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmd_1.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmd_1.Image = ((System.Drawing.Image)(resources.GetObject("cmd_1.Image")));
            this.cmd_1.Location = new System.Drawing.Point(266, 2);
            this.cmd_1.Margin = new System.Windows.Forms.Padding(2);
            this.cmd_1.Name = "cmd_1";
            this.cmd_1.Size = new System.Drawing.Size(60, 35);
            this.cmd_1.TabIndex = 542;
            this.cmd_1.Tag = "1";
            this.cmd_1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmd_1, "Rechercher");
            this.cmd_1.UseVisualStyleBackColor = false;
            this.cmd_1.Click += new System.EventHandler(this.cmd_1_Click);
            // 
            // Label56_24
            // 
            this.Label56_24.AccAcceptNumbersOnly = false;
            this.Label56_24.AccAllowComma = false;
            this.Label56_24.AccBackgroundColor = System.Drawing.Color.White;
            this.Label56_24.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Label56_24.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Label56_24.AccHidenValue = "";
            this.Label56_24.AccNotAllowedChars = null;
            this.Label56_24.AccReadOnly = false;
            this.Label56_24.AccReadOnlyAllowDelete = false;
            this.Label56_24.AccRequired = false;
            this.Label56_24.BackColor = System.Drawing.Color.White;
            this.Label56_24.CustomBackColor = System.Drawing.Color.White;
            this.Label56_24.Dock = System.Windows.Forms.DockStyle.Bottom;
            this.Label56_24.Enabled = false;
            this.Label56_24.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label56_24.ForeColor = System.Drawing.Color.Black;
            this.Label56_24.Location = new System.Drawing.Point(464, 186);
            this.Label56_24.Margin = new System.Windows.Forms.Padding(2);
            this.Label56_24.MaxLength = 32767;
            this.Label56_24.Multiline = false;
            this.Label56_24.Name = "Label56_24";
            this.Label56_24.ReadOnly = false;
            this.Label56_24.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Label56_24.Size = new System.Drawing.Size(458, 27);
            this.Label56_24.TabIndex = 513;
            this.Label56_24.Tag = "24";
            this.Label56_24.TextAlignment = Infragistics.Win.HAlign.Default;
            this.Label56_24.UseSystemPasswordChar = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Gridimm);
            this.panel1.Controls.Add(this.txt_6);
            this.panel1.Controls.Add(this.Check_0);
            this.panel1.Controls.Add(this.GridGroupInterv);
            this.panel1.Controls.Add(this.GridGroupImm);
            this.panel1.Controls.Add(this.GridGroupSecteur);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 218);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1842, 712);
            this.panel1.TabIndex = 1;
            // 
            // Gridimm
            // 
            this.Gridimm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Gridimm.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.Gridimm.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.Gridimm.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.Gridimm.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.Gridimm.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.Gridimm.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.Gridimm.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.Gridimm.DisplayLayout.UseFixedHeaders = true;
            this.Gridimm.Location = new System.Drawing.Point(4, 473);
            this.Gridimm.Name = "Gridimm";
            this.Gridimm.Size = new System.Drawing.Size(1835, 236);
            this.Gridimm.TabIndex = 577;
            this.Gridimm.Text = "Charge par immeuble";
            this.Gridimm.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.Gridimm_InitializeLayout);
            // 
            // txt_6
            // 
            this.txt_6.AccAcceptNumbersOnly = false;
            this.txt_6.AccAllowComma = false;
            this.txt_6.AccBackgroundColor = System.Drawing.Color.White;
            this.txt_6.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txt_6.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txt_6.AccHidenValue = "";
            this.txt_6.AccNotAllowedChars = null;
            this.txt_6.AccReadOnly = false;
            this.txt_6.AccReadOnlyAllowDelete = false;
            this.txt_6.AccRequired = false;
            this.txt_6.BackColor = System.Drawing.Color.White;
            this.txt_6.CustomBackColor = System.Drawing.Color.White;
            this.txt_6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txt_6.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txt_6.Location = new System.Drawing.Point(791, 441);
            this.txt_6.Margin = new System.Windows.Forms.Padding(2);
            this.txt_6.MaxLength = 32767;
            this.txt_6.Multiline = false;
            this.txt_6.Name = "txt_6";
            this.txt_6.ReadOnly = true;
            this.txt_6.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txt_6.Size = new System.Drawing.Size(375, 27);
            this.txt_6.TabIndex = 576;
            this.txt_6.Tag = "25";
            this.txt_6.Text = "6";
            this.txt_6.TextAlignment = Infragistics.Win.HAlign.Default;
            this.txt_6.UseSystemPasswordChar = false;
            // 
            // Check_0
            // 
            this.Check_0.AutoSize = true;
            this.Check_0.Location = new System.Drawing.Point(640, 442);
            this.Check_0.Name = "Check_0";
            this.Check_0.Size = new System.Drawing.Size(146, 23);
            this.Check_0.TabIndex = 575;
            this.Check_0.Tag = "0";
            this.Check_0.Text = "Affichier les sites";
            this.Check_0.UseVisualStyleBackColor = true;
            this.Check_0.CheckedChanged += new System.EventHandler(this.Check_0_CheckedChanged);
            // 
            // GridGroupInterv
            // 
            this.GridGroupInterv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridGroupInterv.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridGroupInterv.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridGroupInterv.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridGroupInterv.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridGroupInterv.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridGroupInterv.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridGroupInterv.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.GridGroupInterv.DisplayLayout.UseFixedHeaders = true;
            this.GridGroupInterv.Location = new System.Drawing.Point(3, 3);
            this.GridGroupInterv.Name = "GridGroupInterv";
            this.GridGroupInterv.Size = new System.Drawing.Size(1836, 433);
            this.GridGroupInterv.TabIndex = 574;
            this.GridGroupInterv.Text = "Charge par intervenant";
            this.GridGroupInterv.AfterCellActivate += new System.EventHandler(this.GridGroupInterv_AfterCellActivate);
            this.GridGroupInterv.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridGroupInterv_InitializeLayout);
            this.GridGroupInterv.AfterRowsDeleted += new System.EventHandler(this.GridGroupInterv_AfterRowsDeleted);
            this.GridGroupInterv.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.GridGroupInterv_AfterRowUpdate);
            this.GridGroupInterv.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridGroupInterv_BeforeRowsDeleted);
            // 
            // GridGroupImm
            // 
            this.GridGroupImm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridGroupImm.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridGroupImm.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridGroupImm.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridGroupImm.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridGroupImm.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridGroupImm.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridGroupImm.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.GridGroupImm.DisplayLayout.UseFixedHeaders = true;
            this.GridGroupImm.Location = new System.Drawing.Point(3, 3);
            this.GridGroupImm.Name = "GridGroupImm";
            this.GridGroupImm.Size = new System.Drawing.Size(1836, 706);
            this.GridGroupImm.TabIndex = 573;
            this.GridGroupImm.Text = "Charge par immeuble";
            this.GridGroupImm.AfterRowsDeleted += new System.EventHandler(this.GridGroupImm_AfterRowsDeleted);
            this.GridGroupImm.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.GridGroupImm_AfterRowUpdate);
            this.GridGroupImm.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridGroupImm_BeforeRowsDeleted);
            this.GridGroupImm.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.GridGroupImm_DoubleClickRow);
            // 
            // GridGroupSecteur
            // 
            this.GridGroupSecteur.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridGroupSecteur.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridGroupSecteur.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridGroupSecteur.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridGroupSecteur.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridGroupSecteur.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridGroupSecteur.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridGroupSecteur.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.GridGroupSecteur.DisplayLayout.UseFixedHeaders = true;
            this.GridGroupSecteur.Location = new System.Drawing.Point(3, 3);
            this.GridGroupSecteur.Name = "GridGroupSecteur";
            this.GridGroupSecteur.Size = new System.Drawing.Size(1836, 706);
            this.GridGroupSecteur.TabIndex = 572;
            this.GridGroupSecteur.Text = "Charge par chef de secteur";
            this.GridGroupSecteur.AfterCellActivate += new System.EventHandler(this.GridGroupSecteur_AfterCellActivate);
            this.GridGroupSecteur.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridGroupSecteur_InitializeLayout);
            this.GridGroupSecteur.AfterRowsDeleted += new System.EventHandler(this.GridGroupSecteur_AfterRowsDeleted);
            this.GridGroupSecteur.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.GridGroupSecteur_AfterRowUpdate);
            this.GridGroupSecteur.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridGroupSecteur_BeforeRowsDeleted);
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.tableLayoutPanel7);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1848, 933);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.SSTab2, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.groupBox4, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.flowLayoutPanel2, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 3;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 199F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(1848, 933);
            this.tableLayoutPanel7.TabIndex = 1;
            // 
            // SSTab2
            // 
            this.SSTab2.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SSTab2.Controls.Add(this.ultraTabSharedControlsPage2);
            this.SSTab2.Controls.Add(this.ultraTabPageControl5);
            this.SSTab2.Controls.Add(this.ultraTabPageControl6);
            this.SSTab2.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.SSTab2.Location = new System.Drawing.Point(3, 252);
            this.SSTab2.Name = "SSTab2";
            this.SSTab2.SharedControlsPage = this.ultraTabSharedControlsPage2;
            this.SSTab2.Size = new System.Drawing.Size(1842, 678);
            this.SSTab2.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Excel;
            this.SSTab2.TabIndex = 413;
            ultraTab1.TabPage = this.ultraTabPageControl5;
            ultraTab1.Text = "Résultat par grille";
            ultraTab2.TabPage = this.ultraTabPageControl6;
            ultraTab2.Text = "graphique";
            this.SSTab2.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab1,
            ultraTab2});
            this.SSTab2.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage2
            // 
            this.ultraTabSharedControlsPage2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage2.Name = "ultraTabSharedControlsPage2";
            this.ultraTabSharedControlsPage2.Size = new System.Drawing.Size(1840, 654);
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.tableLayoutPanel8);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox4.Location = new System.Drawing.Point(0, 50);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Size = new System.Drawing.Size(1848, 199);
            this.groupBox4.TabIndex = 411;
            this.groupBox4.TabStop = false;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 7;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.72387F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.72387F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 19.72387F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.8284F));
            this.tableLayoutPanel8.Controls.Add(this.frame1112, 0, 5);
            this.tableLayoutPanel8.Controls.Add(this.dtDateDebPlaning, 3, 0);
            this.tableLayoutPanel8.Controls.Add(this.label13, 0, 4);
            this.tableLayoutPanel8.Controls.Add(this.label5, 2, 0);
            this.tableLayoutPanel8.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.label8, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.label9, 0, 2);
            this.tableLayoutPanel8.Controls.Add(this.label10, 0, 3);
            this.tableLayoutPanel8.Controls.Add(this.txtIntervenant, 1, 1);
            this.tableLayoutPanel8.Controls.Add(this.txtCodeImmeuble, 1, 2);
            this.tableLayoutPanel8.Controls.Add(this.txtCOP_NoAuto, 1, 3);
            this.tableLayoutPanel8.Controls.Add(this.cmdintervenant, 4, 1);
            this.tableLayoutPanel8.Controls.Add(this.cmdRechercheImmeuble, 4, 2);
            this.tableLayoutPanel8.Controls.Add(this.cmdFindGmaoCreat, 4, 3);
            this.tableLayoutPanel8.Controls.Add(this.frame118, 6, 0);
            this.tableLayoutPanel8.Controls.Add(this.lbltotal2, 6, 4);
            this.tableLayoutPanel8.Controls.Add(this.cmbStatutPDA, 1, 4);
            this.tableLayoutPanel8.Controls.Add(this.txtMoisPlanif, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.dtDateDebPlaningBis, 5, 1);
            this.tableLayoutPanel8.Controls.Add(this.dtDateFinPlaningBis, 5, 2);
            this.tableLayoutPanel8.Controls.Add(this.dtDateFinPlaning, 5, 0);
            this.tableLayoutPanel8.Controls.Add(this.lbllibIntervenant, 2, 1);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 6;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(1842, 176);
            this.tableLayoutPanel8.TabIndex = 0;
            // 
            // frame1112
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.frame1112, 4);
            this.frame1112.Controls.Add(this.Opt3);
            this.frame1112.Controls.Add(this.Opt4);
            this.frame1112.Controls.Add(this.Opt5);
            this.frame1112.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frame1112.Location = new System.Drawing.Point(1, 151);
            this.frame1112.Margin = new System.Windows.Forms.Padding(1);
            this.frame1112.Name = "frame1112";
            this.frame1112.Size = new System.Drawing.Size(869, 28);
            this.frame1112.TabIndex = 612;
            // 
            // Opt3
            // 
            this.Opt3.AutoSize = true;
            this.Opt3.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Opt3.Location = new System.Drawing.Point(3, 3);
            this.Opt3.Name = "Opt3";
            this.Opt3.Size = new System.Drawing.Size(186, 23);
            this.Opt3.TabIndex = 538;
            this.Opt3.Tag = "3";
            this.Opt3.Text = "Particulier/Propriétaire";
            this.Opt3.UseVisualStyleBackColor = true;
            // 
            // Opt4
            // 
            this.Opt4.AutoSize = true;
            this.Opt4.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Opt4.Location = new System.Drawing.Point(195, 3);
            this.Opt4.Name = "Opt4";
            this.Opt4.Size = new System.Drawing.Size(96, 23);
            this.Opt4.TabIndex = 541;
            this.Opt4.Tag = "4";
            this.Opt4.Text = "Immeuble";
            this.Opt4.UseVisualStyleBackColor = true;
            // 
            // Opt5
            // 
            this.Opt5.AutoSize = true;
            this.Opt5.Checked = true;
            this.Opt5.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Opt5.Location = new System.Drawing.Point(297, 3);
            this.Opt5.Name = "Opt5";
            this.Opt5.Size = new System.Drawing.Size(64, 23);
            this.Opt5.TabIndex = 540;
            this.Opt5.TabStop = true;
            this.Opt5.Tag = "5";
            this.Opt5.Text = "Tous.";
            this.Opt5.UseVisualStyleBackColor = true;
            // 
            // dtDateDebPlaning
            // 
            this.dtDateDebPlaning.AccAcceptNumbersOnly = false;
            this.dtDateDebPlaning.AccAllowComma = false;
            this.dtDateDebPlaning.AccBackgroundColor = System.Drawing.Color.White;
            this.dtDateDebPlaning.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.dtDateDebPlaning.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.dtDateDebPlaning.AccHidenValue = "";
            this.dtDateDebPlaning.AccNotAllowedChars = null;
            this.dtDateDebPlaning.AccReadOnly = false;
            this.dtDateDebPlaning.AccReadOnlyAllowDelete = false;
            this.dtDateDebPlaning.AccRequired = false;
            this.dtDateDebPlaning.BackColor = System.Drawing.Color.White;
            this.dtDateDebPlaning.CustomBackColor = System.Drawing.Color.White;
            this.dtDateDebPlaning.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtDateDebPlaning.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.dtDateDebPlaning.ForeColor = System.Drawing.Color.Black;
            this.dtDateDebPlaning.Location = new System.Drawing.Point(568, 2);
            this.dtDateDebPlaning.Margin = new System.Windows.Forms.Padding(2);
            this.dtDateDebPlaning.MaxLength = 32767;
            this.dtDateDebPlaning.Multiline = false;
            this.dtDateDebPlaning.Name = "dtDateDebPlaning";
            this.dtDateDebPlaning.ReadOnly = true;
            this.dtDateDebPlaning.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.dtDateDebPlaning.Size = new System.Drawing.Size(301, 27);
            this.dtDateDebPlaning.TabIndex = 489;
            this.dtDateDebPlaning.TextAlignment = Infragistics.Win.HAlign.Left;
            this.dtDateDebPlaning.UseSystemPasswordChar = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 120);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(185, 30);
            this.label13.TabIndex = 584;
            this.label13.Text = "Status PDA";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(499, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(64, 30);
            this.label5.TabIndex = 573;
            this.label5.Text = "===> Du";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(185, 30);
            this.label7.TabIndex = 384;
            this.label7.Text = "Mois (au format mm/yyyy)";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 30);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(185, 30);
            this.label8.TabIndex = 385;
            this.label8.Text = "Intervenant";
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 60);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(185, 30);
            this.label9.TabIndex = 386;
            this.label9.Text = "Immeuble";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 90);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(185, 30);
            this.label10.TabIndex = 387;
            this.label10.Text = "N° Fiche GMAO";
            // 
            // txtIntervenant
            // 
            this.txtIntervenant.AccAcceptNumbersOnly = false;
            this.txtIntervenant.AccAllowComma = false;
            this.txtIntervenant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntervenant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntervenant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntervenant.AccHidenValue = "";
            this.txtIntervenant.AccNotAllowedChars = null;
            this.txtIntervenant.AccReadOnly = false;
            this.txtIntervenant.AccReadOnlyAllowDelete = false;
            this.txtIntervenant.AccRequired = false;
            this.txtIntervenant.BackColor = System.Drawing.Color.White;
            this.txtIntervenant.CustomBackColor = System.Drawing.Color.White;
            this.txtIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntervenant.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtIntervenant.ForeColor = System.Drawing.Color.Black;
            this.txtIntervenant.Location = new System.Drawing.Point(193, 32);
            this.txtIntervenant.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntervenant.MaxLength = 32767;
            this.txtIntervenant.Multiline = false;
            this.txtIntervenant.Name = "txtIntervenant";
            this.txtIntervenant.ReadOnly = false;
            this.txtIntervenant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntervenant.Size = new System.Drawing.Size(301, 27);
            this.txtIntervenant.TabIndex = 587;
            this.txtIntervenant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntervenant.UseSystemPasswordChar = false;
            this.txtIntervenant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIntervenant_KeyPress);
            this.txtIntervenant.Validated += new System.EventHandler(this.txtIntervenant_Validated);
            // 
            // txtCodeImmeuble
            // 
            this.txtCodeImmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeImmeuble.AccAllowComma = false;
            this.txtCodeImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeuble.AccHidenValue = "";
            this.txtCodeImmeuble.AccNotAllowedChars = null;
            this.txtCodeImmeuble.AccReadOnly = false;
            this.txtCodeImmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeuble.AccRequired = false;
            this.txtCodeImmeuble.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel8.SetColumnSpan(this.txtCodeImmeuble, 3);
            this.txtCodeImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeImmeuble.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeImmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeImmeuble.Location = new System.Drawing.Point(193, 62);
            this.txtCodeImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeuble.MaxLength = 32767;
            this.txtCodeImmeuble.Multiline = false;
            this.txtCodeImmeuble.Name = "txtCodeImmeuble";
            this.txtCodeImmeuble.ReadOnly = false;
            this.txtCodeImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeuble.Size = new System.Drawing.Size(676, 27);
            this.txtCodeImmeuble.TabIndex = 589;
            this.txtCodeImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeuble.UseSystemPasswordChar = false;
            this.txtCodeImmeuble.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeImmeuble_KeyPress);
            // 
            // txtCOP_NoAuto
            // 
            this.txtCOP_NoAuto.AccAcceptNumbersOnly = false;
            this.txtCOP_NoAuto.AccAllowComma = false;
            this.txtCOP_NoAuto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCOP_NoAuto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCOP_NoAuto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCOP_NoAuto.AccHidenValue = "";
            this.txtCOP_NoAuto.AccNotAllowedChars = null;
            this.txtCOP_NoAuto.AccReadOnly = false;
            this.txtCOP_NoAuto.AccReadOnlyAllowDelete = false;
            this.txtCOP_NoAuto.AccRequired = false;
            this.txtCOP_NoAuto.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel8.SetColumnSpan(this.txtCOP_NoAuto, 3);
            this.txtCOP_NoAuto.CustomBackColor = System.Drawing.Color.White;
            this.txtCOP_NoAuto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCOP_NoAuto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCOP_NoAuto.ForeColor = System.Drawing.Color.Black;
            this.txtCOP_NoAuto.Location = new System.Drawing.Point(193, 92);
            this.txtCOP_NoAuto.Margin = new System.Windows.Forms.Padding(2);
            this.txtCOP_NoAuto.MaxLength = 32767;
            this.txtCOP_NoAuto.Multiline = false;
            this.txtCOP_NoAuto.Name = "txtCOP_NoAuto";
            this.txtCOP_NoAuto.ReadOnly = false;
            this.txtCOP_NoAuto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCOP_NoAuto.Size = new System.Drawing.Size(676, 27);
            this.txtCOP_NoAuto.TabIndex = 590;
            this.txtCOP_NoAuto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCOP_NoAuto.UseSystemPasswordChar = false;
            // 
            // cmdintervenant
            // 
            this.cmdintervenant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdintervenant.FlatAppearance.BorderSize = 0;
            this.cmdintervenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdintervenant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdintervenant.Image = ((System.Drawing.Image)(resources.GetObject("cmdintervenant.Image")));
            this.cmdintervenant.Location = new System.Drawing.Point(874, 33);
            this.cmdintervenant.Name = "cmdintervenant";
            this.cmdintervenant.Size = new System.Drawing.Size(24, 19);
            this.cmdintervenant.TabIndex = 578;
            this.cmdintervenant.Tag = "Recherche d\'un intervenant";
            this.cmdintervenant.UseVisualStyleBackColor = false;
            this.cmdintervenant.Click += new System.EventHandler(this.cmdintervenant_Click);
            // 
            // cmdRechercheImmeuble
            // 
            this.cmdRechercheImmeuble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheImmeuble.FlatAppearance.BorderSize = 0;
            this.cmdRechercheImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheImmeuble.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheImmeuble.Image = ((System.Drawing.Image)(resources.GetObject("cmdRechercheImmeuble.Image")));
            this.cmdRechercheImmeuble.Location = new System.Drawing.Point(874, 63);
            this.cmdRechercheImmeuble.Name = "cmdRechercheImmeuble";
            this.cmdRechercheImmeuble.Size = new System.Drawing.Size(24, 19);
            this.cmdRechercheImmeuble.TabIndex = 579;
            this.cmdRechercheImmeuble.Tag = "Recherche d\'un chantier";
            this.cmdRechercheImmeuble.UseVisualStyleBackColor = false;
            this.cmdRechercheImmeuble.Click += new System.EventHandler(this.cmdRechercheImmeuble_Click);
            // 
            // cmdFindGmaoCreat
            // 
            this.cmdFindGmaoCreat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFindGmaoCreat.FlatAppearance.BorderSize = 0;
            this.cmdFindGmaoCreat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFindGmaoCreat.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdFindGmaoCreat.Image = ((System.Drawing.Image)(resources.GetObject("cmdFindGmaoCreat.Image")));
            this.cmdFindGmaoCreat.Location = new System.Drawing.Point(874, 93);
            this.cmdFindGmaoCreat.Name = "cmdFindGmaoCreat";
            this.cmdFindGmaoCreat.Size = new System.Drawing.Size(24, 19);
            this.cmdFindGmaoCreat.TabIndex = 580;
            this.cmdFindGmaoCreat.UseVisualStyleBackColor = false;
            this.cmdFindGmaoCreat.Click += new System.EventHandler(this.cmdFindGmaoCreat_Click);
            // 
            // frame118
            // 
            this.frame118.BackColor = System.Drawing.Color.Transparent;
            this.frame118.Controls.Add(this.tableLayoutPanel9);
            this.frame118.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frame118.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.frame118.Location = new System.Drawing.Point(1209, 3);
            this.frame118.Name = "frame118";
            this.tableLayoutPanel8.SetRowSpan(this.frame118, 4);
            this.frame118.Size = new System.Drawing.Size(630, 114);
            this.frame118.TabIndex = 581;
            this.frame118.TabStop = false;
            this.frame118.Text = "P1";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 1;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel9.Controls.Add(this.OptEQU_P1Compteur2, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.OptEQU_P1Compteur0, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.OptEQU_P1Compteur1, 0, 1);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 3;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(624, 91);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // OptEQU_P1Compteur2
            // 
            this.OptEQU_P1Compteur2.AutoSize = true;
            this.OptEQU_P1Compteur2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptEQU_P1Compteur2.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptEQU_P1Compteur2.Location = new System.Drawing.Point(3, 55);
            this.OptEQU_P1Compteur2.Name = "OptEQU_P1Compteur2";
            this.OptEQU_P1Compteur2.Size = new System.Drawing.Size(618, 33);
            this.OptEQU_P1Compteur2.TabIndex = 540;
            this.OptEQU_P1Compteur2.TabStop = true;
            this.OptEQU_P1Compteur2.Tag = "2";
            this.OptEQU_P1Compteur2.Text = "Avec ou sans compteur P1";
            this.OptEQU_P1Compteur2.UseVisualStyleBackColor = true;
            // 
            // OptEQU_P1Compteur0
            // 
            this.OptEQU_P1Compteur0.AutoSize = true;
            this.OptEQU_P1Compteur0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptEQU_P1Compteur0.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptEQU_P1Compteur0.Location = new System.Drawing.Point(3, 3);
            this.OptEQU_P1Compteur0.Name = "OptEQU_P1Compteur0";
            this.OptEQU_P1Compteur0.Size = new System.Drawing.Size(618, 20);
            this.OptEQU_P1Compteur0.TabIndex = 538;
            this.OptEQU_P1Compteur0.TabStop = true;
            this.OptEQU_P1Compteur0.Tag = "0";
            this.OptEQU_P1Compteur0.Text = "Uniquement Compteur P1";
            this.OptEQU_P1Compteur0.UseVisualStyleBackColor = true;
            // 
            // OptEQU_P1Compteur1
            // 
            this.OptEQU_P1Compteur1.AutoSize = true;
            this.OptEQU_P1Compteur1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptEQU_P1Compteur1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptEQU_P1Compteur1.Location = new System.Drawing.Point(3, 29);
            this.OptEQU_P1Compteur1.Name = "OptEQU_P1Compteur1";
            this.OptEQU_P1Compteur1.Size = new System.Drawing.Size(618, 20);
            this.OptEQU_P1Compteur1.TabIndex = 539;
            this.OptEQU_P1Compteur1.TabStop = true;
            this.OptEQU_P1Compteur1.Tag = "1";
            this.OptEQU_P1Compteur1.Text = "Exclure les Compteur P1";
            this.OptEQU_P1Compteur1.UseVisualStyleBackColor = true;
            // 
            // lbltotal2
            // 
            this.lbltotal2.AccAcceptNumbersOnly = false;
            this.lbltotal2.AccAllowComma = false;
            this.lbltotal2.AccBackgroundColor = System.Drawing.Color.White;
            this.lbltotal2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lbltotal2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lbltotal2.AccHidenValue = "";
            this.lbltotal2.AccNotAllowedChars = null;
            this.lbltotal2.AccReadOnly = false;
            this.lbltotal2.AccReadOnlyAllowDelete = false;
            this.lbltotal2.AccRequired = false;
            this.lbltotal2.BackColor = System.Drawing.Color.White;
            this.lbltotal2.CustomBackColor = System.Drawing.Color.White;
            this.lbltotal2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbltotal2.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lbltotal2.ForeColor = System.Drawing.Color.Black;
            this.lbltotal2.Location = new System.Drawing.Point(1208, 122);
            this.lbltotal2.Margin = new System.Windows.Forms.Padding(2);
            this.lbltotal2.MaxLength = 32767;
            this.lbltotal2.Multiline = false;
            this.lbltotal2.Name = "lbltotal2";
            this.lbltotal2.ReadOnly = false;
            this.lbltotal2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lbltotal2.Size = new System.Drawing.Size(632, 27);
            this.lbltotal2.TabIndex = 582;
            this.lbltotal2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lbltotal2.UseSystemPasswordChar = false;
            // 
            // cmbStatutPDA
            // 
            this.tableLayoutPanel8.SetColumnSpan(this.cmbStatutPDA, 3);
            appearance51.BackColor = System.Drawing.SystemColors.Window;
            appearance51.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbStatutPDA.DisplayLayout.Appearance = appearance51;
            this.cmbStatutPDA.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbStatutPDA.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance52.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance52.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance52.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbStatutPDA.DisplayLayout.GroupByBox.Appearance = appearance52;
            appearance53.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbStatutPDA.DisplayLayout.GroupByBox.BandLabelAppearance = appearance53;
            this.cmbStatutPDA.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance54.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance54.BackColor2 = System.Drawing.SystemColors.Control;
            appearance54.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance54.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbStatutPDA.DisplayLayout.GroupByBox.PromptAppearance = appearance54;
            this.cmbStatutPDA.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbStatutPDA.DisplayLayout.MaxRowScrollRegions = 1;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            appearance55.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbStatutPDA.DisplayLayout.Override.ActiveCellAppearance = appearance55;
            appearance56.BackColor = System.Drawing.SystemColors.Highlight;
            appearance56.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbStatutPDA.DisplayLayout.Override.ActiveRowAppearance = appearance56;
            this.cmbStatutPDA.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbStatutPDA.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance57.BackColor = System.Drawing.SystemColors.Window;
            this.cmbStatutPDA.DisplayLayout.Override.CardAreaAppearance = appearance57;
            appearance58.BorderColor = System.Drawing.Color.Silver;
            appearance58.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbStatutPDA.DisplayLayout.Override.CellAppearance = appearance58;
            this.cmbStatutPDA.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbStatutPDA.DisplayLayout.Override.CellPadding = 0;
            appearance59.BackColor = System.Drawing.SystemColors.Control;
            appearance59.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance59.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance59.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance59.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbStatutPDA.DisplayLayout.Override.GroupByRowAppearance = appearance59;
            appearance60.TextHAlignAsString = "Left";
            this.cmbStatutPDA.DisplayLayout.Override.HeaderAppearance = appearance60;
            this.cmbStatutPDA.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbStatutPDA.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            appearance61.BorderColor = System.Drawing.Color.Silver;
            this.cmbStatutPDA.DisplayLayout.Override.RowAppearance = appearance61;
            this.cmbStatutPDA.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance62.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbStatutPDA.DisplayLayout.Override.TemplateAddRowAppearance = appearance62;
            this.cmbStatutPDA.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbStatutPDA.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbStatutPDA.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbStatutPDA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbStatutPDA.Location = new System.Drawing.Point(194, 123);
            this.cmbStatutPDA.Name = "cmbStatutPDA";
            this.cmbStatutPDA.Size = new System.Drawing.Size(674, 27);
            this.cmbStatutPDA.TabIndex = 591;
            // 
            // txtMoisPlanif
            // 
            this.txtMoisPlanif.AccAcceptNumbersOnly = false;
            this.txtMoisPlanif.AccAllowComma = false;
            this.txtMoisPlanif.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMoisPlanif.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMoisPlanif.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMoisPlanif.AccHidenValue = "";
            this.txtMoisPlanif.AccNotAllowedChars = null;
            this.txtMoisPlanif.AccReadOnly = false;
            this.txtMoisPlanif.AccReadOnlyAllowDelete = false;
            this.txtMoisPlanif.AccRequired = false;
            this.txtMoisPlanif.BackColor = System.Drawing.Color.White;
            this.txtMoisPlanif.CustomBackColor = System.Drawing.Color.White;
            this.txtMoisPlanif.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMoisPlanif.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtMoisPlanif.ForeColor = System.Drawing.Color.Black;
            this.txtMoisPlanif.Location = new System.Drawing.Point(193, 2);
            this.txtMoisPlanif.Margin = new System.Windows.Forms.Padding(2);
            this.txtMoisPlanif.MaxLength = 32767;
            this.txtMoisPlanif.Multiline = false;
            this.txtMoisPlanif.Name = "txtMoisPlanif";
            this.txtMoisPlanif.ReadOnly = false;
            this.txtMoisPlanif.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMoisPlanif.Size = new System.Drawing.Size(301, 27);
            this.txtMoisPlanif.TabIndex = 586;
            this.txtMoisPlanif.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMoisPlanif.UseSystemPasswordChar = false;
            this.txtMoisPlanif.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMoisPlanif_KeyPress);
            this.txtMoisPlanif.Validated += new System.EventHandler(this.txtMoisPlanif_Validated);
            // 
            // dtDateDebPlaningBis
            // 
            stateEditorButton3.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless;
            stateEditorButton3.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dtDateDebPlaningBis.ButtonsLeft.Add(stateEditorButton3);
            this.dtDateDebPlaningBis.DateTime = new System.DateTime(2009, 3, 3, 0, 0, 0, 0);
            this.dtDateDebPlaningBis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtDateDebPlaningBis.Location = new System.Drawing.Point(904, 33);
            this.dtDateDebPlaningBis.Name = "dtDateDebPlaningBis";
            this.dtDateDebPlaningBis.Size = new System.Drawing.Size(299, 26);
            this.dtDateDebPlaningBis.TabIndex = 487;
            this.dtDateDebPlaningBis.Value = new System.DateTime(2009, 3, 3, 0, 0, 0, 0);
            this.dtDateDebPlaningBis.Visible = false;
            // 
            // dtDateFinPlaningBis
            // 
            stateEditorButton4.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless;
            stateEditorButton4.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dtDateFinPlaningBis.ButtonsLeft.Add(stateEditorButton4);
            this.dtDateFinPlaningBis.DateTime = new System.DateTime(2009, 3, 3, 0, 0, 0, 0);
            this.dtDateFinPlaningBis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtDateFinPlaningBis.Location = new System.Drawing.Point(904, 63);
            this.dtDateFinPlaningBis.Name = "dtDateFinPlaningBis";
            this.dtDateFinPlaningBis.Size = new System.Drawing.Size(299, 26);
            this.dtDateFinPlaningBis.TabIndex = 488;
            this.dtDateFinPlaningBis.Value = new System.DateTime(2009, 3, 3, 0, 0, 0, 0);
            this.dtDateFinPlaningBis.Visible = false;
            // 
            // dtDateFinPlaning
            // 
            this.dtDateFinPlaning.AccAcceptNumbersOnly = false;
            this.dtDateFinPlaning.AccAllowComma = false;
            this.dtDateFinPlaning.AccBackgroundColor = System.Drawing.Color.White;
            this.dtDateFinPlaning.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.dtDateFinPlaning.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.dtDateFinPlaning.AccHidenValue = "";
            this.dtDateFinPlaning.AccNotAllowedChars = null;
            this.dtDateFinPlaning.AccReadOnly = false;
            this.dtDateFinPlaning.AccReadOnlyAllowDelete = false;
            this.dtDateFinPlaning.AccRequired = false;
            this.dtDateFinPlaning.BackColor = System.Drawing.Color.White;
            this.dtDateFinPlaning.CustomBackColor = System.Drawing.Color.White;
            this.dtDateFinPlaning.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtDateFinPlaning.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.dtDateFinPlaning.ForeColor = System.Drawing.Color.Black;
            this.dtDateFinPlaning.Location = new System.Drawing.Point(903, 2);
            this.dtDateFinPlaning.Margin = new System.Windows.Forms.Padding(2);
            this.dtDateFinPlaning.MaxLength = 32767;
            this.dtDateFinPlaning.Multiline = false;
            this.dtDateFinPlaning.Name = "dtDateFinPlaning";
            this.dtDateFinPlaning.ReadOnly = true;
            this.dtDateFinPlaning.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.dtDateFinPlaning.Size = new System.Drawing.Size(301, 27);
            this.dtDateFinPlaning.TabIndex = 490;
            this.dtDateFinPlaning.TextAlignment = Infragistics.Win.HAlign.Left;
            this.dtDateFinPlaning.UseSystemPasswordChar = false;
            // 
            // lbllibIntervenant
            // 
            this.lbllibIntervenant.AccAcceptNumbersOnly = false;
            this.lbllibIntervenant.AccAllowComma = false;
            this.lbllibIntervenant.AccBackgroundColor = System.Drawing.Color.White;
            this.lbllibIntervenant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lbllibIntervenant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lbllibIntervenant.AccHidenValue = "";
            this.lbllibIntervenant.AccNotAllowedChars = null;
            this.lbllibIntervenant.AccReadOnly = false;
            this.lbllibIntervenant.AccReadOnlyAllowDelete = false;
            this.lbllibIntervenant.AccRequired = false;
            this.lbllibIntervenant.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel8.SetColumnSpan(this.lbllibIntervenant, 2);
            this.lbllibIntervenant.CustomBackColor = System.Drawing.Color.White;
            this.lbllibIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbllibIntervenant.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lbllibIntervenant.ForeColor = System.Drawing.Color.Black;
            this.lbllibIntervenant.Location = new System.Drawing.Point(498, 32);
            this.lbllibIntervenant.Margin = new System.Windows.Forms.Padding(2);
            this.lbllibIntervenant.MaxLength = 32767;
            this.lbllibIntervenant.Multiline = false;
            this.lbllibIntervenant.Name = "lbllibIntervenant";
            this.lbllibIntervenant.ReadOnly = true;
            this.lbllibIntervenant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lbllibIntervenant.Size = new System.Drawing.Size(371, 27);
            this.lbllibIntervenant.TabIndex = 588;
            this.lbllibIntervenant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lbllibIntervenant.UseSystemPasswordChar = false;
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.Command3);
            this.flowLayoutPanel2.Controls.Add(this.cmdVisites);
            this.flowLayoutPanel2.Controls.Add(this.cdmSend2PDA);
            this.flowLayoutPanel2.Controls.Add(this.CmdLegende0);
            this.flowLayoutPanel2.Controls.Add(this.cmdListing);
            this.flowLayoutPanel2.Controls.Add(this.cmdDebloquer);
            this.flowLayoutPanel2.Controls.Add(this.cmdExportPlan);
            this.flowLayoutPanel2.Controls.Add(this.CmdSauver);
            this.flowLayoutPanel2.Controls.Add(this.cmdCleanVisites);
            this.flowLayoutPanel2.Controls.Add(this.cmdAffichePlan);
            this.flowLayoutPanel2.Controls.Add(this.Frame13);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1848, 50);
            this.flowLayoutPanel2.TabIndex = 412;
            // 
            // Command3
            // 
            this.Command3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command3.FlatAppearance.BorderSize = 0;
            this.Command3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command3.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command3.ForeColor = System.Drawing.Color.White;
            this.Command3.Image = ((System.Drawing.Image)(resources.GetObject("Command3.Image")));
            this.Command3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command3.Location = new System.Drawing.Point(1695, 2);
            this.Command3.Margin = new System.Windows.Forms.Padding(2);
            this.Command3.Name = "Command3";
            this.Command3.Size = new System.Drawing.Size(151, 35);
            this.Command3.TabIndex = 580;
            this.Command3.Text = "     Supp.Interv sur PDA";
            this.Command3.UseVisualStyleBackColor = false;
            this.Command3.Click += new System.EventHandler(this.Command3_Click);
            // 
            // cmdVisites
            // 
            this.cmdVisites.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdVisites.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdVisites.FlatAppearance.BorderSize = 0;
            this.cmdVisites.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdVisites.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdVisites.ForeColor = System.Drawing.Color.White;
            this.cmdVisites.Image = ((System.Drawing.Image)(resources.GetObject("cmdVisites.Image")));
            this.cmdVisites.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdVisites.Location = new System.Drawing.Point(1574, 2);
            this.cmdVisites.Margin = new System.Windows.Forms.Padding(2);
            this.cmdVisites.Name = "cmdVisites";
            this.cmdVisites.Size = new System.Drawing.Size(117, 35);
            this.cmdVisites.TabIndex = 569;
            this.cmdVisites.Text = "    Création Inter.";
            this.cmdVisites.UseVisualStyleBackColor = false;
            this.cmdVisites.Click += new System.EventHandler(this.cmdVisites_Click);
            // 
            // cdmSend2PDA
            // 
            this.cdmSend2PDA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cdmSend2PDA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cdmSend2PDA.FlatAppearance.BorderSize = 0;
            this.cdmSend2PDA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cdmSend2PDA.Font = new System.Drawing.Font("Ubuntu", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cdmSend2PDA.ForeColor = System.Drawing.Color.White;
            this.cdmSend2PDA.Image = ((System.Drawing.Image)(resources.GetObject("cdmSend2PDA.Image")));
            this.cdmSend2PDA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cdmSend2PDA.Location = new System.Drawing.Point(1477, 2);
            this.cdmSend2PDA.Margin = new System.Windows.Forms.Padding(2);
            this.cdmSend2PDA.Name = "cdmSend2PDA";
            this.cdmSend2PDA.Size = new System.Drawing.Size(93, 35);
            this.cdmSend2PDA.TabIndex = 570;
            this.cdmSend2PDA.Tag = "";
            this.cdmSend2PDA.Text = "        Envoyer";
            this.toolTip1.SetToolTip(this.cdmSend2PDA, "Envoi sur les PDA, les interventions préventives aux techniciens.");
            this.cdmSend2PDA.UseVisualStyleBackColor = false;
            this.cdmSend2PDA.Click += new System.EventHandler(this.cdmSend2PDA_Click);
            // 
            // CmdLegende0
            // 
            this.CmdLegende0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdLegende0.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.CmdLegende0.FlatAppearance.BorderSize = 0;
            this.CmdLegende0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdLegende0.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdLegende0.ForeColor = System.Drawing.Color.White;
            this.CmdLegende0.Image = ((System.Drawing.Image)(resources.GetObject("CmdLegende0.Image")));
            this.CmdLegende0.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdLegende0.Location = new System.Drawing.Point(1388, 2);
            this.CmdLegende0.Margin = new System.Windows.Forms.Padding(2);
            this.CmdLegende0.Name = "CmdLegende0";
            this.CmdLegende0.Size = new System.Drawing.Size(85, 35);
            this.CmdLegende0.TabIndex = 579;
            this.CmdLegende0.Tag = "0";
            this.CmdLegende0.Text = "     Légende";
            this.CmdLegende0.UseVisualStyleBackColor = false;
            this.CmdLegende0.Click += new System.EventHandler(this.CmdLegende0_Click);
            // 
            // cmdListing
            // 
            this.cmdListing.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdListing.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdListing.FlatAppearance.BorderSize = 0;
            this.cmdListing.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdListing.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmdListing.ForeColor = System.Drawing.Color.White;
            this.cmdListing.Image = ((System.Drawing.Image)(resources.GetObject("cmdListing.Image")));
            this.cmdListing.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdListing.Location = new System.Drawing.Point(1298, 2);
            this.cmdListing.Margin = new System.Windows.Forms.Padding(2);
            this.cmdListing.Name = "cmdListing";
            this.cmdListing.Size = new System.Drawing.Size(86, 35);
            this.cmdListing.TabIndex = 568;
            this.cmdListing.Text = "     Listing";
            this.cmdListing.UseVisualStyleBackColor = false;
            this.cmdListing.Click += new System.EventHandler(this.cmdListing_Click);
            // 
            // cmdDebloquer
            // 
            this.cmdDebloquer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdDebloquer.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdDebloquer.FlatAppearance.BorderSize = 0;
            this.cmdDebloquer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDebloquer.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDebloquer.ForeColor = System.Drawing.Color.White;
            this.cmdDebloquer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdDebloquer.Location = new System.Drawing.Point(1209, 2);
            this.cmdDebloquer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdDebloquer.Name = "cmdDebloquer";
            this.cmdDebloquer.Size = new System.Drawing.Size(85, 35);
            this.cmdDebloquer.TabIndex = 581;
            this.cmdDebloquer.Tag = "0";
            this.cmdDebloquer.Text = "Débloquer";
            this.cmdDebloquer.UseVisualStyleBackColor = false;
            this.cmdDebloquer.Click += new System.EventHandler(this.cmdDebloquer_Click);
            // 
            // cmdExportPlan
            // 
            this.cmdExportPlan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdExportPlan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExportPlan.FlatAppearance.BorderSize = 0;
            this.cmdExportPlan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExportPlan.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExportPlan.Image = ((System.Drawing.Image)(resources.GetObject("cmdExportPlan.Image")));
            this.cmdExportPlan.Location = new System.Drawing.Point(1157, 2);
            this.cmdExportPlan.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExportPlan.Name = "cmdExportPlan";
            this.cmdExportPlan.Size = new System.Drawing.Size(48, 34);
            this.cmdExportPlan.TabIndex = 540;
            this.cmdExportPlan.Tag = "";
            this.cmdExportPlan.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdExportPlan, "Exporter la sélection");
            this.cmdExportPlan.UseVisualStyleBackColor = false;
            this.cmdExportPlan.Click += new System.EventHandler(this.cmdExportPlan_Click);
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.Image = ((System.Drawing.Image)(resources.GetObject("CmdSauver.Image")));
            this.CmdSauver.Location = new System.Drawing.Point(1105, 2);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(48, 35);
            this.CmdSauver.TabIndex = 578;
            this.CmdSauver.Tag = "";
            this.toolTip1.SetToolTip(this.CmdSauver, "Enregistrer");
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // cmdCleanVisites
            // 
            this.cmdCleanVisites.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdCleanVisites.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCleanVisites.FlatAppearance.BorderSize = 0;
            this.cmdCleanVisites.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCleanVisites.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCleanVisites.Image = ((System.Drawing.Image)(resources.GetObject("cmdCleanVisites.Image")));
            this.cmdCleanVisites.Location = new System.Drawing.Point(1053, 2);
            this.cmdCleanVisites.Margin = new System.Windows.Forms.Padding(2);
            this.cmdCleanVisites.Name = "cmdCleanVisites";
            this.cmdCleanVisites.Size = new System.Drawing.Size(48, 35);
            this.cmdCleanVisites.TabIndex = 373;
            this.cmdCleanVisites.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdCleanVisites.UseVisualStyleBackColor = false;
            this.cmdCleanVisites.Click += new System.EventHandler(this.cmdCleanVisites_Click);
            // 
            // cmdAffichePlan
            // 
            this.cmdAffichePlan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAffichePlan.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAffichePlan.FlatAppearance.BorderSize = 0;
            this.cmdAffichePlan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAffichePlan.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAffichePlan.Image = ((System.Drawing.Image)(resources.GetObject("cmdAffichePlan.Image")));
            this.cmdAffichePlan.Location = new System.Drawing.Point(1001, 2);
            this.cmdAffichePlan.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAffichePlan.Name = "cmdAffichePlan";
            this.cmdAffichePlan.Size = new System.Drawing.Size(48, 35);
            this.cmdAffichePlan.TabIndex = 374;
            this.cmdAffichePlan.Tag = "";
            this.cmdAffichePlan.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAffichePlan, "Rechercher");
            this.cmdAffichePlan.UseVisualStyleBackColor = false;
            this.cmdAffichePlan.Click += new System.EventHandler(this.cmdAffichePlan_Click);
            // 
            // Frame13
            // 
            this.Frame13.BackColor = System.Drawing.Color.Transparent;
            this.Frame13.Controls.Add(this.chkDevisEtablir);
            this.Frame13.Controls.Add(this.chkMatRemplace);
            this.Frame13.Controls.Add(this.chkTrxUrgent);
            this.Frame13.Controls.Add(this.chkAnomalie);
            this.Frame13.Controls.Add(this.chkCommGardien);
            this.Frame13.Controls.Add(this.chkInterIncompléte);
            this.Frame13.Controls.Add(this.txtStatutHisto);
            this.Frame13.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Frame13.Location = new System.Drawing.Point(613, 3);
            this.Frame13.Name = "Frame13";
            this.Frame13.Size = new System.Drawing.Size(383, 168);
            this.Frame13.TabIndex = 411;
            this.Frame13.TabStop = false;
            this.Frame13.Visible = false;
            // 
            // chkDevisEtablir
            // 
            this.chkDevisEtablir.AutoSize = true;
            this.chkDevisEtablir.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.chkDevisEtablir.Location = new System.Drawing.Point(214, 73);
            this.chkDevisEtablir.Name = "chkDevisEtablir";
            this.chkDevisEtablir.Size = new System.Drawing.Size(124, 23);
            this.chkDevisEtablir.TabIndex = 575;
            this.chkDevisEtablir.Text = "Devis à établir";
            this.chkDevisEtablir.UseVisualStyleBackColor = true;
            this.chkDevisEtablir.Visible = false;
            // 
            // chkMatRemplace
            // 
            this.chkMatRemplace.AutoSize = true;
            this.chkMatRemplace.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.chkMatRemplace.Location = new System.Drawing.Point(207, 47);
            this.chkMatRemplace.Name = "chkMatRemplace";
            this.chkMatRemplace.Size = new System.Drawing.Size(128, 23);
            this.chkMatRemplace.TabIndex = 574;
            this.chkMatRemplace.Text = "Mat. remplacé";
            this.chkMatRemplace.UseVisualStyleBackColor = true;
            this.chkMatRemplace.Visible = false;
            // 
            // chkTrxUrgent
            // 
            this.chkTrxUrgent.AutoSize = true;
            this.chkTrxUrgent.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.chkTrxUrgent.Location = new System.Drawing.Point(37, 126);
            this.chkTrxUrgent.Name = "chkTrxUrgent";
            this.chkTrxUrgent.Size = new System.Drawing.Size(133, 23);
            this.chkTrxUrgent.TabIndex = 573;
            this.chkTrxUrgent.Text = "Dde Trx urgent";
            this.chkTrxUrgent.UseVisualStyleBackColor = true;
            this.chkTrxUrgent.Visible = false;
            // 
            // chkAnomalie
            // 
            this.chkAnomalie.AutoSize = true;
            this.chkAnomalie.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.chkAnomalie.Location = new System.Drawing.Point(37, 100);
            this.chkAnomalie.Name = "chkAnomalie";
            this.chkAnomalie.Size = new System.Drawing.Size(157, 23);
            this.chkAnomalie.TabIndex = 572;
            this.chkAnomalie.Text = "Anomalie constaté";
            this.chkAnomalie.UseVisualStyleBackColor = true;
            this.chkAnomalie.Visible = false;
            // 
            // chkCommGardien
            // 
            this.chkCommGardien.AutoSize = true;
            this.chkCommGardien.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.chkCommGardien.Location = new System.Drawing.Point(37, 74);
            this.chkCommGardien.Name = "chkCommGardien";
            this.chkCommGardien.Size = new System.Drawing.Size(195, 23);
            this.chkCommGardien.TabIndex = 571;
            this.chkCommGardien.Text = "Commentaire Signataire";
            this.chkCommGardien.UseVisualStyleBackColor = true;
            this.chkCommGardien.Visible = false;
            // 
            // chkInterIncompléte
            // 
            this.chkInterIncompléte.AutoSize = true;
            this.chkInterIncompléte.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.chkInterIncompléte.Location = new System.Drawing.Point(37, 48);
            this.chkInterIncompléte.Name = "chkInterIncompléte";
            this.chkInterIncompléte.Size = new System.Drawing.Size(195, 23);
            this.chkInterIncompléte.TabIndex = 570;
            this.chkInterIncompléte.Text = "Intervention incomplète";
            this.chkInterIncompléte.UseVisualStyleBackColor = true;
            this.chkInterIncompléte.Visible = false;
            // 
            // txtStatutHisto
            // 
            this.txtStatutHisto.AccAcceptNumbersOnly = false;
            this.txtStatutHisto.AccAllowComma = false;
            this.txtStatutHisto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtStatutHisto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtStatutHisto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtStatutHisto.AccHidenValue = "";
            this.txtStatutHisto.AccNotAllowedChars = null;
            this.txtStatutHisto.AccReadOnly = false;
            this.txtStatutHisto.AccReadOnlyAllowDelete = false;
            this.txtStatutHisto.AccRequired = false;
            this.txtStatutHisto.BackColor = System.Drawing.Color.White;
            this.txtStatutHisto.CustomBackColor = System.Drawing.Color.White;
            this.txtStatutHisto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtStatutHisto.ForeColor = System.Drawing.Color.Black;
            this.txtStatutHisto.Location = new System.Drawing.Point(27, 20);
            this.txtStatutHisto.Margin = new System.Windows.Forms.Padding(2);
            this.txtStatutHisto.MaxLength = 32767;
            this.txtStatutHisto.Multiline = false;
            this.txtStatutHisto.Name = "txtStatutHisto";
            this.txtStatutHisto.ReadOnly = false;
            this.txtStatutHisto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtStatutHisto.Size = new System.Drawing.Size(135, 27);
            this.txtStatutHisto.TabIndex = 502;
            this.txtStatutHisto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtStatutHisto.UseSystemPasswordChar = false;
            this.txtStatutHisto.Visible = false;
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.tableLayoutPanel11);
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(1848, 933);
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.ssGAvisDEPassage, 0, 2);
            this.tableLayoutPanel11.Controls.Add(this.groupBox7, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.flowLayoutPanel5, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 3;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 85F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(1848, 933);
            this.tableLayoutPanel11.TabIndex = 0;
            // 
            // ssGAvisDEPassage
            // 
            appearance63.BackColor = System.Drawing.SystemColors.Window;
            appearance63.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGAvisDEPassage.DisplayLayout.Appearance = appearance63;
            this.ssGAvisDEPassage.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGAvisDEPassage.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance64.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance64.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance64.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance64.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGAvisDEPassage.DisplayLayout.GroupByBox.Appearance = appearance64;
            appearance65.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGAvisDEPassage.DisplayLayout.GroupByBox.BandLabelAppearance = appearance65;
            this.ssGAvisDEPassage.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance66.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance66.BackColor2 = System.Drawing.SystemColors.Control;
            appearance66.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance66.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGAvisDEPassage.DisplayLayout.GroupByBox.PromptAppearance = appearance66;
            this.ssGAvisDEPassage.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGAvisDEPassage.DisplayLayout.MaxRowScrollRegions = 1;
            appearance67.BackColor = System.Drawing.SystemColors.Window;
            appearance67.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGAvisDEPassage.DisplayLayout.Override.ActiveCellAppearance = appearance67;
            appearance68.BackColor = System.Drawing.SystemColors.Highlight;
            appearance68.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGAvisDEPassage.DisplayLayout.Override.ActiveRowAppearance = appearance68;
            this.ssGAvisDEPassage.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.Yes;
            this.ssGAvisDEPassage.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.ssGAvisDEPassage.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.ssGAvisDEPassage.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGAvisDEPassage.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance69.BackColor = System.Drawing.SystemColors.Window;
            this.ssGAvisDEPassage.DisplayLayout.Override.CardAreaAppearance = appearance69;
            appearance70.BorderColor = System.Drawing.Color.Silver;
            appearance70.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGAvisDEPassage.DisplayLayout.Override.CellAppearance = appearance70;
            this.ssGAvisDEPassage.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGAvisDEPassage.DisplayLayout.Override.CellPadding = 0;
            appearance71.BackColor = System.Drawing.SystemColors.Control;
            appearance71.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance71.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance71.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance71.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGAvisDEPassage.DisplayLayout.Override.GroupByRowAppearance = appearance71;
            appearance72.TextHAlignAsString = "Left";
            this.ssGAvisDEPassage.DisplayLayout.Override.HeaderAppearance = appearance72;
            this.ssGAvisDEPassage.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGAvisDEPassage.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance73.BackColor = System.Drawing.SystemColors.Window;
            appearance73.BorderColor = System.Drawing.Color.Silver;
            this.ssGAvisDEPassage.DisplayLayout.Override.RowAppearance = appearance73;
            this.ssGAvisDEPassage.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGAvisDEPassage.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance74.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGAvisDEPassage.DisplayLayout.Override.TemplateAddRowAppearance = appearance74;
            this.ssGAvisDEPassage.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGAvisDEPassage.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGAvisDEPassage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGAvisDEPassage.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.ssGAvisDEPassage.Location = new System.Drawing.Point(3, 133);
            this.ssGAvisDEPassage.Name = "ssGAvisDEPassage";
            this.ssGAvisDEPassage.Size = new System.Drawing.Size(1842, 797);
            this.ssGAvisDEPassage.TabIndex = 412;
            this.ssGAvisDEPassage.Text = "ultraGrid1";
            // 
            // groupBox7
            // 
            this.groupBox7.BackColor = System.Drawing.Color.Transparent;
            this.groupBox7.Controls.Add(this.tableLayoutPanel12);
            this.groupBox7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox7.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox7.Location = new System.Drawing.Point(0, 45);
            this.groupBox7.Margin = new System.Windows.Forms.Padding(0);
            this.groupBox7.Name = "groupBox7";
            this.groupBox7.Size = new System.Drawing.Size(1848, 85);
            this.groupBox7.TabIndex = 411;
            this.groupBox7.TabStop = false;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 10;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel12.Controls.Add(this.label18, 0, 1);
            this.tableLayoutPanel12.Controls.Add(this.label14, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.label15, 3, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtCOP_NoAutoAvis, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.Command6, 2, 0);
            this.tableLayoutPanel12.Controls.Add(this.Command5, 5, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtCodeImmeubleAvis, 4, 0);
            this.tableLayoutPanel12.Controls.Add(this.label16, 6, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtIntervenantAvis, 7, 0);
            this.tableLayoutPanel12.Controls.Add(this.lbllibIntervenantAvis, 8, 0);
            this.tableLayoutPanel12.Controls.Add(this.Command7, 9, 0);
            this.tableLayoutPanel12.Controls.Add(this.label17, 3, 1);
            this.tableLayoutPanel12.Controls.Add(this.dtDateDebPlaningAvis, 1, 1);
            this.tableLayoutPanel12.Controls.Add(this.dtDateFinPlaningAvis, 4, 1);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 2;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(1842, 62);
            this.tableLayoutPanel12.TabIndex = 1;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(3, 30);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(152, 32);
            this.label18.TabIndex = 584;
            this.label18.Text = "Date de planification";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label14.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 0);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(152, 30);
            this.label14.TabIndex = 384;
            this.label14.Text = "N° Fiche Contrat";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(542, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 30);
            this.label15.TabIndex = 385;
            this.label15.Text = "Immeuble";
            // 
            // txtCOP_NoAutoAvis
            // 
            this.txtCOP_NoAutoAvis.AccAcceptNumbersOnly = false;
            this.txtCOP_NoAutoAvis.AccAllowComma = false;
            this.txtCOP_NoAutoAvis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCOP_NoAutoAvis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCOP_NoAutoAvis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCOP_NoAutoAvis.AccHidenValue = "";
            this.txtCOP_NoAutoAvis.AccNotAllowedChars = null;
            this.txtCOP_NoAutoAvis.AccReadOnly = false;
            this.txtCOP_NoAutoAvis.AccReadOnlyAllowDelete = false;
            this.txtCOP_NoAutoAvis.AccRequired = false;
            this.txtCOP_NoAutoAvis.BackColor = System.Drawing.Color.White;
            this.txtCOP_NoAutoAvis.CustomBackColor = System.Drawing.Color.White;
            this.txtCOP_NoAutoAvis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCOP_NoAutoAvis.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCOP_NoAutoAvis.ForeColor = System.Drawing.Color.Black;
            this.txtCOP_NoAutoAvis.Location = new System.Drawing.Point(160, 2);
            this.txtCOP_NoAutoAvis.Margin = new System.Windows.Forms.Padding(2);
            this.txtCOP_NoAutoAvis.MaxLength = 32767;
            this.txtCOP_NoAutoAvis.Multiline = false;
            this.txtCOP_NoAutoAvis.Name = "txtCOP_NoAutoAvis";
            this.txtCOP_NoAutoAvis.ReadOnly = false;
            this.txtCOP_NoAutoAvis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCOP_NoAutoAvis.Size = new System.Drawing.Size(347, 27);
            this.txtCOP_NoAutoAvis.TabIndex = 502;
            this.txtCOP_NoAutoAvis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCOP_NoAutoAvis.UseSystemPasswordChar = false;
            // 
            // Command6
            // 
            this.Command6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command6.FlatAppearance.BorderSize = 0;
            this.Command6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command6.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Command6.Image = ((System.Drawing.Image)(resources.GetObject("Command6.Image")));
            this.Command6.Location = new System.Drawing.Point(512, 3);
            this.Command6.Name = "Command6";
            this.Command6.Size = new System.Drawing.Size(24, 21);
            this.Command6.TabIndex = 579;
            this.Command6.Tag = "";
            this.toolTip1.SetToolTip(this.Command6, "Recherche d\'un contrat");
            this.Command6.UseVisualStyleBackColor = false;
            // 
            // Command5
            // 
            this.Command5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command5.FlatAppearance.BorderSize = 0;
            this.Command5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command5.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Command5.Image = ((System.Drawing.Image)(resources.GetObject("Command5.Image")));
            this.Command5.Location = new System.Drawing.Point(977, 3);
            this.Command5.Name = "Command5";
            this.Command5.Size = new System.Drawing.Size(24, 21);
            this.Command5.TabIndex = 580;
            this.Command5.Tag = "";
            this.toolTip1.SetToolTip(this.Command5, "Recherche d\'un chantier");
            this.Command5.UseVisualStyleBackColor = false;
            // 
            // txtCodeImmeubleAvis
            // 
            this.txtCodeImmeubleAvis.AccAcceptNumbersOnly = false;
            this.txtCodeImmeubleAvis.AccAllowComma = false;
            this.txtCodeImmeubleAvis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeubleAvis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeubleAvis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeubleAvis.AccHidenValue = "";
            this.txtCodeImmeubleAvis.AccNotAllowedChars = null;
            this.txtCodeImmeubleAvis.AccReadOnly = false;
            this.txtCodeImmeubleAvis.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeubleAvis.AccRequired = false;
            this.txtCodeImmeubleAvis.BackColor = System.Drawing.Color.White;
            this.txtCodeImmeubleAvis.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeubleAvis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeImmeubleAvis.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeImmeubleAvis.ForeColor = System.Drawing.Color.Black;
            this.txtCodeImmeubleAvis.Location = new System.Drawing.Point(625, 2);
            this.txtCodeImmeubleAvis.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeubleAvis.MaxLength = 32767;
            this.txtCodeImmeubleAvis.Multiline = false;
            this.txtCodeImmeubleAvis.Name = "txtCodeImmeubleAvis";
            this.txtCodeImmeubleAvis.ReadOnly = false;
            this.txtCodeImmeubleAvis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeubleAvis.Size = new System.Drawing.Size(347, 27);
            this.txtCodeImmeubleAvis.TabIndex = 503;
            this.txtCodeImmeubleAvis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeubleAvis.UseSystemPasswordChar = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(1007, 0);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(90, 30);
            this.label16.TabIndex = 582;
            this.label16.Text = "Intervenant";
            // 
            // txtIntervenantAvis
            // 
            this.txtIntervenantAvis.AccAcceptNumbersOnly = false;
            this.txtIntervenantAvis.AccAllowComma = false;
            this.txtIntervenantAvis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntervenantAvis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntervenantAvis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntervenantAvis.AccHidenValue = "";
            this.txtIntervenantAvis.AccNotAllowedChars = null;
            this.txtIntervenantAvis.AccReadOnly = false;
            this.txtIntervenantAvis.AccReadOnlyAllowDelete = false;
            this.txtIntervenantAvis.AccRequired = false;
            this.txtIntervenantAvis.BackColor = System.Drawing.Color.White;
            this.txtIntervenantAvis.CustomBackColor = System.Drawing.Color.White;
            this.txtIntervenantAvis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntervenantAvis.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtIntervenantAvis.ForeColor = System.Drawing.Color.Black;
            this.txtIntervenantAvis.Location = new System.Drawing.Point(1102, 2);
            this.txtIntervenantAvis.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntervenantAvis.MaxLength = 32767;
            this.txtIntervenantAvis.Multiline = false;
            this.txtIntervenantAvis.Name = "txtIntervenantAvis";
            this.txtIntervenantAvis.ReadOnly = false;
            this.txtIntervenantAvis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntervenantAvis.Size = new System.Drawing.Size(347, 27);
            this.txtIntervenantAvis.TabIndex = 504;
            this.txtIntervenantAvis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntervenantAvis.UseSystemPasswordChar = false;
            // 
            // lbllibIntervenantAvis
            // 
            this.lbllibIntervenantAvis.AccAcceptNumbersOnly = false;
            this.lbllibIntervenantAvis.AccAllowComma = false;
            this.lbllibIntervenantAvis.AccBackgroundColor = System.Drawing.Color.White;
            this.lbllibIntervenantAvis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lbllibIntervenantAvis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lbllibIntervenantAvis.AccHidenValue = "";
            this.lbllibIntervenantAvis.AccNotAllowedChars = null;
            this.lbllibIntervenantAvis.AccReadOnly = false;
            this.lbllibIntervenantAvis.AccReadOnlyAllowDelete = false;
            this.lbllibIntervenantAvis.AccRequired = false;
            this.lbllibIntervenantAvis.BackColor = System.Drawing.Color.White;
            this.lbllibIntervenantAvis.CustomBackColor = System.Drawing.Color.White;
            this.lbllibIntervenantAvis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbllibIntervenantAvis.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lbllibIntervenantAvis.ForeColor = System.Drawing.Color.Black;
            this.lbllibIntervenantAvis.Location = new System.Drawing.Point(1453, 2);
            this.lbllibIntervenantAvis.Margin = new System.Windows.Forms.Padding(2);
            this.lbllibIntervenantAvis.MaxLength = 32767;
            this.lbllibIntervenantAvis.Multiline = false;
            this.lbllibIntervenantAvis.Name = "lbllibIntervenantAvis";
            this.lbllibIntervenantAvis.ReadOnly = false;
            this.lbllibIntervenantAvis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lbllibIntervenantAvis.Size = new System.Drawing.Size(347, 27);
            this.lbllibIntervenantAvis.TabIndex = 505;
            this.lbllibIntervenantAvis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lbllibIntervenantAvis.UseSystemPasswordChar = false;
            // 
            // Command7
            // 
            this.Command7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command7.FlatAppearance.BorderSize = 0;
            this.Command7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command7.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Command7.Image = ((System.Drawing.Image)(resources.GetObject("Command7.Image")));
            this.Command7.Location = new System.Drawing.Point(1805, 3);
            this.Command7.Name = "Command7";
            this.Command7.Size = new System.Drawing.Size(24, 21);
            this.Command7.TabIndex = 587;
            this.Command7.Tag = "";
            this.toolTip1.SetToolTip(this.Command7, "Recherche d\'un intervenant");
            this.Command7.UseVisualStyleBackColor = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(542, 30);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(78, 32);
            this.label17.TabIndex = 583;
            this.label17.Text = "au";
            // 
            // dtDateDebPlaningAvis
            // 
            this.tableLayoutPanel12.SetColumnSpan(this.dtDateDebPlaningAvis, 2);
            this.dtDateDebPlaningAvis.DateTime = new System.DateTime(2019, 5, 6, 0, 0, 0, 0);
            this.dtDateDebPlaningAvis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtDateDebPlaningAvis.Location = new System.Drawing.Point(161, 33);
            this.dtDateDebPlaningAvis.Name = "dtDateDebPlaningAvis";
            this.dtDateDebPlaningAvis.Size = new System.Drawing.Size(375, 26);
            this.dtDateDebPlaningAvis.TabIndex = 506;
            this.dtDateDebPlaningAvis.Value = new System.DateTime(2019, 5, 6, 0, 0, 0, 0);
            // 
            // dtDateFinPlaningAvis
            // 
            this.tableLayoutPanel12.SetColumnSpan(this.dtDateFinPlaningAvis, 2);
            this.dtDateFinPlaningAvis.DateTime = new System.DateTime(2019, 5, 6, 0, 0, 0, 0);
            this.dtDateFinPlaningAvis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtDateFinPlaningAvis.Location = new System.Drawing.Point(626, 33);
            this.dtDateFinPlaningAvis.Name = "dtDateFinPlaningAvis";
            this.dtDateFinPlaningAvis.Size = new System.Drawing.Size(375, 26);
            this.dtDateFinPlaningAvis.TabIndex = 507;
            this.dtDateFinPlaningAvis.Value = new System.DateTime(2019, 5, 6, 0, 0, 0, 0);
            // 
            // flowLayoutPanel5
            // 
            this.flowLayoutPanel5.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel5.Controls.Add(this.button1);
            this.flowLayoutPanel5.Controls.Add(this.button2);
            this.flowLayoutPanel5.Controls.Add(this.button16);
            this.flowLayoutPanel5.Location = new System.Drawing.Point(1627, 3);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(218, 39);
            this.flowLayoutPanel5.TabIndex = 0;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = ((System.Drawing.Image)(resources.GetObject("button1.Image")));
            this.button1.Location = new System.Drawing.Point(2, 2);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(60, 35);
            this.button1.TabIndex = 375;
            this.button1.Tag = "";
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.button1, "Rechercher");
            this.button1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = ((System.Drawing.Image)(resources.GetObject("button2.Image")));
            this.button2.Location = new System.Drawing.Point(66, 2);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(60, 35);
            this.button2.TabIndex = 376;
            this.button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.button16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button16.FlatAppearance.BorderSize = 0;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.button16.ForeColor = System.Drawing.Color.White;
            this.button16.Image = ((System.Drawing.Image)(resources.GetObject("button16.Image")));
            this.button16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button16.Location = new System.Drawing.Point(130, 2);
            this.button16.Margin = new System.Windows.Forms.Padding(2);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(85, 35);
            this.button16.TabIndex = 403;
            this.button16.Text = "   Clean";
            this.button16.UseVisualStyleBackColor = false;
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.tableLayoutPanel13);
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(1848, 933);
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 1;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Controls.Add(this.groupBox8, 0, 4);
            this.tableLayoutPanel13.Controls.Add(this.groupBox9, 0, 1);
            this.tableLayoutPanel13.Controls.Add(this.flowLayoutPanel6, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.groupBox12, 0, 2);
            this.tableLayoutPanel13.Controls.Add(this.tableLayoutPanel17, 0, 3);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 5;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 245F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(1848, 933);
            this.tableLayoutPanel13.TabIndex = 1;
            // 
            // groupBox8
            // 
            this.groupBox8.BackColor = System.Drawing.Color.Transparent;
            this.groupBox8.Controls.Add(this.ssGridTachesAnnuelHisto);
            this.groupBox8.Controls.Add(this.ssOpeTachesHisto);
            this.groupBox8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox8.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox8.Location = new System.Drawing.Point(3, 630);
            this.groupBox8.Name = "groupBox8";
            this.groupBox8.Size = new System.Drawing.Size(1842, 300);
            this.groupBox8.TabIndex = 414;
            this.groupBox8.TabStop = false;
            // 
            // ssGridTachesAnnuelHisto
            // 
            appearance75.BackColor = System.Drawing.SystemColors.Window;
            appearance75.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Appearance = appearance75;
            this.ssGridTachesAnnuelHisto.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridTachesAnnuelHisto.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance76.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance76.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance76.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance76.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridTachesAnnuelHisto.DisplayLayout.GroupByBox.Appearance = appearance76;
            appearance77.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridTachesAnnuelHisto.DisplayLayout.GroupByBox.BandLabelAppearance = appearance77;
            this.ssGridTachesAnnuelHisto.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance78.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance78.BackColor2 = System.Drawing.SystemColors.Control;
            appearance78.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance78.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridTachesAnnuelHisto.DisplayLayout.GroupByBox.PromptAppearance = appearance78;
            this.ssGridTachesAnnuelHisto.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridTachesAnnuelHisto.DisplayLayout.MaxRowScrollRegions = 1;
            appearance79.BackColor = System.Drawing.SystemColors.Window;
            appearance79.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.ActiveCellAppearance = appearance79;
            appearance80.BackColor = System.Drawing.SystemColors.Highlight;
            appearance80.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.ActiveRowAppearance = appearance80;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance81.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.CardAreaAppearance = appearance81;
            appearance82.BorderColor = System.Drawing.Color.Silver;
            appearance82.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.CellAppearance = appearance82;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.CellPadding = 0;
            appearance83.BackColor = System.Drawing.SystemColors.Control;
            appearance83.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance83.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance83.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance83.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.GroupByRowAppearance = appearance83;
            appearance84.TextHAlignAsString = "Left";
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.HeaderAppearance = appearance84;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance85.BackColor = System.Drawing.SystemColors.Window;
            appearance85.BorderColor = System.Drawing.Color.Silver;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.RowAppearance = appearance85;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance86.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridTachesAnnuelHisto.DisplayLayout.Override.TemplateAddRowAppearance = appearance86;
            this.ssGridTachesAnnuelHisto.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridTachesAnnuelHisto.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridTachesAnnuelHisto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridTachesAnnuelHisto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.ssGridTachesAnnuelHisto.Location = new System.Drawing.Point(3, 20);
            this.ssGridTachesAnnuelHisto.Name = "ssGridTachesAnnuelHisto";
            this.ssGridTachesAnnuelHisto.Size = new System.Drawing.Size(1836, 277);
            this.ssGridTachesAnnuelHisto.TabIndex = 573;
            this.ssGridTachesAnnuelHisto.Text = "ultraGrid1";
            this.ssGridTachesAnnuelHisto.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridTachesAnnuelHisto_InitializeLayout);
            this.ssGridTachesAnnuelHisto.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssGridTachesAnnuelHisto_BeforeRowsDeleted);
            this.ssGridTachesAnnuelHisto.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.ssGridTachesAnnuelHisto_Error);
            // 
            // ssOpeTachesHisto
            // 
            this.ssOpeTachesHisto.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ssOpeTachesHisto.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssOpeTachesHisto.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssOpeTachesHisto.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssOpeTachesHisto.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssOpeTachesHisto.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssOpeTachesHisto.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ssOpeTachesHisto.DisplayLayout.UseFixedHeaders = true;
            this.ssOpeTachesHisto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssOpeTachesHisto.Location = new System.Drawing.Point(3, 20);
            this.ssOpeTachesHisto.Name = "ssOpeTachesHisto";
            this.ssOpeTachesHisto.Size = new System.Drawing.Size(1836, 277);
            this.ssOpeTachesHisto.TabIndex = 572;
            this.ssOpeTachesHisto.Text = "Opération (Similuation)";
            this.ssOpeTachesHisto.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssOpeTachesHisto_InitializeLayout);
            this.ssOpeTachesHisto.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssOpeTachesHisto_BeforeRowsDeleted);
            this.ssOpeTachesHisto.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.ssOpeTachesHisto_Error);
            // 
            // groupBox9
            // 
            this.groupBox9.BackColor = System.Drawing.Color.Transparent;
            this.groupBox9.Controls.Add(this.tableLayoutPanel14);
            this.groupBox9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox9.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox9.Location = new System.Drawing.Point(3, 53);
            this.groupBox9.Name = "groupBox9";
            this.groupBox9.Size = new System.Drawing.Size(1842, 239);
            this.groupBox9.TabIndex = 411;
            this.groupBox9.TabStop = false;
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 9;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel14.Controls.Add(this.label19, 2, 0);
            this.tableLayoutPanel14.Controls.Add(this.label20, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.label21, 0, 3);
            this.tableLayoutPanel14.Controls.Add(this.label22, 0, 4);
            this.tableLayoutPanel14.Controls.Add(this.label23, 0, 5);
            this.tableLayoutPanel14.Controls.Add(this.dtDateVisiteLe, 1, 0);
            this.tableLayoutPanel14.Controls.Add(this.dtDateVisiteAu, 3, 0);
            this.tableLayoutPanel14.Controls.Add(this.txtCodeImmeubleHisto, 1, 3);
            this.tableLayoutPanel14.Controls.Add(this.txtMatEntretien, 1, 4);
            this.tableLayoutPanel14.Controls.Add(this.txtIntervenantHisto, 1, 5);
            this.tableLayoutPanel14.Controls.Add(this.frame119, 8, 2);
            this.tableLayoutPanel14.Controls.Add(this.lblTotalHisto, 8, 6);
            this.tableLayoutPanel14.Controls.Add(this.label25, 5, 0);
            this.tableLayoutPanel14.Controls.Add(this.label26, 7, 0);
            this.tableLayoutPanel14.Controls.Add(this.label27, 5, 1);
            this.tableLayoutPanel14.Controls.Add(this.label28, 7, 1);
            this.tableLayoutPanel14.Controls.Add(this.label29, 0, 1);
            this.tableLayoutPanel14.Controls.Add(this.label30, 0, 2);
            this.tableLayoutPanel14.Controls.Add(this.DTDatePrevueDe, 1, 1);
            this.tableLayoutPanel14.Controls.Add(this.DTDateRealiseDe, 1, 2);
            this.tableLayoutPanel14.Controls.Add(this.DTDatePrevueAu, 3, 1);
            this.tableLayoutPanel14.Controls.Add(this.DTDateRealiseAu, 3, 2);
            this.tableLayoutPanel14.Controls.Add(this.DTCreeParDe, 6, 0);
            this.tableLayoutPanel14.Controls.Add(this.dtEnvoyeLeHisto1, 6, 1);
            this.tableLayoutPanel14.Controls.Add(this.dtCreeParAu, 8, 0);
            this.tableLayoutPanel14.Controls.Add(this.dtEnvoyeLeHisto2, 8, 1);
            this.tableLayoutPanel14.Controls.Add(this.label31, 2, 1);
            this.tableLayoutPanel14.Controls.Add(this.label32, 2, 2);
            this.tableLayoutPanel14.Controls.Add(this.cmdIntervHisto, 4, 5);
            this.tableLayoutPanel14.Controls.Add(this.cmdMatEntretien, 4, 4);
            this.tableLayoutPanel14.Controls.Add(this.cmdImmeubleHisto, 4, 3);
            this.tableLayoutPanel14.Controls.Add(this.label24, 5, 3);
            this.tableLayoutPanel14.Controls.Add(this.label34, 5, 4);
            this.tableLayoutPanel14.Controls.Add(this.label35, 5, 5);
            this.tableLayoutPanel14.Controls.Add(this.cmbStatutPDAhisto, 6, 5);
            this.tableLayoutPanel14.Controls.Add(this.txtNoInterventionHisto, 6, 3);
            this.tableLayoutPanel14.Controls.Add(this.txtCode1Histo, 6, 4);
            this.tableLayoutPanel14.Controls.Add(this.cmdFindNoInter, 7, 3);
            this.tableLayoutPanel14.Controls.Add(this.cmdClient, 7, 4);
            this.tableLayoutPanel14.Controls.Add(this.lblLibEntretien, 3, 4);
            this.tableLayoutPanel14.Controls.Add(this.lbllibIntervenantHisto, 3, 5);
            this.tableLayoutPanel14.Controls.Add(this.frame1113, 0, 6);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel14.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 7;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(1836, 216);
            this.tableLayoutPanel14.TabIndex = 0;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(521, 0);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(26, 30);
            this.label19.TabIndex = 473;
            this.label19.Text = "au";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(3, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(136, 30);
            this.label20.TabIndex = 384;
            this.label20.Text = "Date visite le";
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(3, 90);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(136, 30);
            this.label21.TabIndex = 385;
            this.label21.Text = "immeuble";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(3, 120);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(136, 30);
            this.label22.TabIndex = 386;
            this.label22.Text = "Secteur Immeuble";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(3, 150);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(136, 30);
            this.label23.TabIndex = 387;
            this.label23.Text = "Intervenant";
            // 
            // dtDateVisiteLe
            // 
            stateEditorButton5.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dtDateVisiteLe.ButtonsLeft.Add(stateEditorButton5);
            this.dtDateVisiteLe.DateTime = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.dtDateVisiteLe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtDateVisiteLe.Location = new System.Drawing.Point(145, 3);
            this.dtDateVisiteLe.Name = "dtDateVisiteLe";
            this.dtDateVisiteLe.Size = new System.Drawing.Size(370, 26);
            this.dtDateVisiteLe.TabIndex = 571;
            this.dtDateVisiteLe.Value = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.dtDateVisiteLe.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTDatePrevueDe_BeforeDropDown);
            this.dtDateVisiteLe.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.dtDateDebPlaningBisSimul_AfterEditorButtonCheckStateChanged);
            // 
            // dtDateVisiteAu
            // 
            stateEditorButton6.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless;
            stateEditorButton6.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dtDateVisiteAu.ButtonsLeft.Add(stateEditorButton6);
            this.dtDateVisiteAu.DateTime = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.dtDateVisiteAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtDateVisiteAu.Location = new System.Drawing.Point(553, 3);
            this.dtDateVisiteAu.Name = "dtDateVisiteAu";
            this.dtDateVisiteAu.Size = new System.Drawing.Size(370, 26);
            this.dtDateVisiteAu.SpinButtonAlignment = Infragistics.Win.ButtonAlignment.Left;
            this.dtDateVisiteAu.TabIndex = 572;
            this.dtDateVisiteAu.Value = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.dtDateVisiteAu.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTDatePrevueDe_BeforeDropDown);
            this.dtDateVisiteAu.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.dtDateDebPlaningBisSimul_AfterEditorButtonCheckStateChanged);
            // 
            // txtCodeImmeubleHisto
            // 
            this.txtCodeImmeubleHisto.AccAcceptNumbersOnly = false;
            this.txtCodeImmeubleHisto.AccAllowComma = false;
            this.txtCodeImmeubleHisto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeubleHisto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeubleHisto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeubleHisto.AccHidenValue = "";
            this.txtCodeImmeubleHisto.AccNotAllowedChars = null;
            this.txtCodeImmeubleHisto.AccReadOnly = false;
            this.txtCodeImmeubleHisto.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeubleHisto.AccRequired = false;
            this.txtCodeImmeubleHisto.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel14.SetColumnSpan(this.txtCodeImmeubleHisto, 3);
            this.txtCodeImmeubleHisto.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeubleHisto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeImmeubleHisto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeImmeubleHisto.ForeColor = System.Drawing.Color.Black;
            this.txtCodeImmeubleHisto.Location = new System.Drawing.Point(144, 92);
            this.txtCodeImmeubleHisto.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeubleHisto.MaxLength = 32767;
            this.txtCodeImmeubleHisto.Multiline = false;
            this.txtCodeImmeubleHisto.Name = "txtCodeImmeubleHisto";
            this.txtCodeImmeubleHisto.ReadOnly = false;
            this.txtCodeImmeubleHisto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeubleHisto.Size = new System.Drawing.Size(780, 27);
            this.txtCodeImmeubleHisto.TabIndex = 581;
            this.txtCodeImmeubleHisto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeubleHisto.UseSystemPasswordChar = false;
            this.txtCodeImmeubleHisto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeImmeubleHisto_KeyPress);
            // 
            // txtMatEntretien
            // 
            this.txtMatEntretien.AccAcceptNumbersOnly = false;
            this.txtMatEntretien.AccAllowComma = false;
            this.txtMatEntretien.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMatEntretien.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMatEntretien.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMatEntretien.AccHidenValue = "";
            this.txtMatEntretien.AccNotAllowedChars = null;
            this.txtMatEntretien.AccReadOnly = false;
            this.txtMatEntretien.AccReadOnlyAllowDelete = false;
            this.txtMatEntretien.AccRequired = false;
            this.txtMatEntretien.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel14.SetColumnSpan(this.txtMatEntretien, 2);
            this.txtMatEntretien.CustomBackColor = System.Drawing.Color.White;
            this.txtMatEntretien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMatEntretien.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtMatEntretien.ForeColor = System.Drawing.Color.Black;
            this.txtMatEntretien.Location = new System.Drawing.Point(144, 122);
            this.txtMatEntretien.Margin = new System.Windows.Forms.Padding(2);
            this.txtMatEntretien.MaxLength = 32767;
            this.txtMatEntretien.Multiline = false;
            this.txtMatEntretien.Name = "txtMatEntretien";
            this.txtMatEntretien.ReadOnly = false;
            this.txtMatEntretien.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMatEntretien.Size = new System.Drawing.Size(404, 27);
            this.txtMatEntretien.TabIndex = 582;
            this.txtMatEntretien.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMatEntretien.UseSystemPasswordChar = false;
            this.txtMatEntretien.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMatEntretien_KeyPress);
            this.txtMatEntretien.Validated += new System.EventHandler(this.txtMatEntretien_Validated);
            // 
            // txtIntervenantHisto
            // 
            this.txtIntervenantHisto.AccAcceptNumbersOnly = false;
            this.txtIntervenantHisto.AccAllowComma = false;
            this.txtIntervenantHisto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntervenantHisto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntervenantHisto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntervenantHisto.AccHidenValue = "";
            this.txtIntervenantHisto.AccNotAllowedChars = null;
            this.txtIntervenantHisto.AccReadOnly = false;
            this.txtIntervenantHisto.AccReadOnlyAllowDelete = false;
            this.txtIntervenantHisto.AccRequired = false;
            this.txtIntervenantHisto.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel14.SetColumnSpan(this.txtIntervenantHisto, 2);
            this.txtIntervenantHisto.CustomBackColor = System.Drawing.Color.White;
            this.txtIntervenantHisto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntervenantHisto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtIntervenantHisto.ForeColor = System.Drawing.Color.Black;
            this.txtIntervenantHisto.Location = new System.Drawing.Point(144, 152);
            this.txtIntervenantHisto.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntervenantHisto.MaxLength = 32767;
            this.txtIntervenantHisto.Multiline = false;
            this.txtIntervenantHisto.Name = "txtIntervenantHisto";
            this.txtIntervenantHisto.ReadOnly = false;
            this.txtIntervenantHisto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntervenantHisto.Size = new System.Drawing.Size(404, 27);
            this.txtIntervenantHisto.TabIndex = 584;
            this.txtIntervenantHisto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntervenantHisto.UseSystemPasswordChar = false;
            this.txtIntervenantHisto.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIntervenantHisto_KeyPress);
            this.txtIntervenantHisto.Validated += new System.EventHandler(this.txtIntervenantHisto_Validated);
            // 
            // frame119
            // 
            this.frame119.BackColor = System.Drawing.Color.Transparent;
            this.frame119.Controls.Add(this.tableLayoutPanel15);
            this.frame119.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frame119.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.frame119.Location = new System.Drawing.Point(1462, 63);
            this.frame119.Name = "frame119";
            this.tableLayoutPanel14.SetRowSpan(this.frame119, 4);
            this.frame119.Size = new System.Drawing.Size(371, 114);
            this.frame119.TabIndex = 481;
            this.frame119.TabStop = false;
            this.frame119.Text = "P1";
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 1;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel15.Controls.Add(this.OptEQU_P1CompteurHisto2, 0, 2);
            this.tableLayoutPanel15.Controls.Add(this.OptEQU_P1CompteurHisto0, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.OptEQU_P1CompteurHisto1, 0, 1);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 3;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(365, 91);
            this.tableLayoutPanel15.TabIndex = 0;
            // 
            // OptEQU_P1CompteurHisto2
            // 
            this.OptEQU_P1CompteurHisto2.AutoSize = true;
            this.OptEQU_P1CompteurHisto2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptEQU_P1CompteurHisto2.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptEQU_P1CompteurHisto2.Location = new System.Drawing.Point(3, 55);
            this.OptEQU_P1CompteurHisto2.Name = "OptEQU_P1CompteurHisto2";
            this.OptEQU_P1CompteurHisto2.Size = new System.Drawing.Size(359, 33);
            this.OptEQU_P1CompteurHisto2.TabIndex = 540;
            this.OptEQU_P1CompteurHisto2.TabStop = true;
            this.OptEQU_P1CompteurHisto2.Tag = "2";
            this.OptEQU_P1CompteurHisto2.Text = "Avec ou sans compteur P1";
            this.OptEQU_P1CompteurHisto2.UseVisualStyleBackColor = true;
            // 
            // OptEQU_P1CompteurHisto0
            // 
            this.OptEQU_P1CompteurHisto0.AutoSize = true;
            this.OptEQU_P1CompteurHisto0.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptEQU_P1CompteurHisto0.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptEQU_P1CompteurHisto0.Location = new System.Drawing.Point(3, 3);
            this.OptEQU_P1CompteurHisto0.Name = "OptEQU_P1CompteurHisto0";
            this.OptEQU_P1CompteurHisto0.Size = new System.Drawing.Size(359, 20);
            this.OptEQU_P1CompteurHisto0.TabIndex = 538;
            this.OptEQU_P1CompteurHisto0.TabStop = true;
            this.OptEQU_P1CompteurHisto0.Tag = "0";
            this.OptEQU_P1CompteurHisto0.Text = "Uniquement Compteur P1";
            this.OptEQU_P1CompteurHisto0.UseVisualStyleBackColor = true;
            // 
            // OptEQU_P1CompteurHisto1
            // 
            this.OptEQU_P1CompteurHisto1.AutoSize = true;
            this.OptEQU_P1CompteurHisto1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptEQU_P1CompteurHisto1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptEQU_P1CompteurHisto1.Location = new System.Drawing.Point(3, 29);
            this.OptEQU_P1CompteurHisto1.Name = "OptEQU_P1CompteurHisto1";
            this.OptEQU_P1CompteurHisto1.Size = new System.Drawing.Size(359, 20);
            this.OptEQU_P1CompteurHisto1.TabIndex = 539;
            this.OptEQU_P1CompteurHisto1.TabStop = true;
            this.OptEQU_P1CompteurHisto1.Tag = "1";
            this.OptEQU_P1CompteurHisto1.Text = "Exclure les Compteur P1";
            this.OptEQU_P1CompteurHisto1.UseVisualStyleBackColor = true;
            // 
            // lblTotalHisto
            // 
            this.lblTotalHisto.AccAcceptNumbersOnly = false;
            this.lblTotalHisto.AccAllowComma = false;
            this.lblTotalHisto.AccBackgroundColor = System.Drawing.Color.White;
            this.lblTotalHisto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblTotalHisto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblTotalHisto.AccHidenValue = "";
            this.lblTotalHisto.AccNotAllowedChars = null;
            this.lblTotalHisto.AccReadOnly = false;
            this.lblTotalHisto.AccReadOnlyAllowDelete = false;
            this.lblTotalHisto.AccRequired = false;
            this.lblTotalHisto.BackColor = System.Drawing.Color.White;
            this.lblTotalHisto.CustomBackColor = System.Drawing.Color.White;
            this.lblTotalHisto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lblTotalHisto.ForeColor = System.Drawing.Color.Black;
            this.lblTotalHisto.Location = new System.Drawing.Point(1461, 182);
            this.lblTotalHisto.Margin = new System.Windows.Forms.Padding(2);
            this.lblTotalHisto.MaxLength = 32767;
            this.lblTotalHisto.Multiline = false;
            this.lblTotalHisto.Name = "lblTotalHisto";
            this.lblTotalHisto.ReadOnly = false;
            this.lblTotalHisto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblTotalHisto.Size = new System.Drawing.Size(105, 27);
            this.lblTotalHisto.TabIndex = 482;
            this.lblTotalHisto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblTotalHisto.UseSystemPasswordChar = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(959, 0);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(91, 30);
            this.label25.TabIndex = 483;
            this.label25.Text = "Créé Le";
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label26.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(1432, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(24, 30);
            this.label26.TabIndex = 484;
            this.label26.Text = "au";
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label27.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(959, 30);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(91, 30);
            this.label27.TabIndex = 485;
            this.label27.Text = "Envoyé le";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label28.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(1432, 30);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(24, 30);
            this.label28.TabIndex = 486;
            this.label28.Text = "au";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label29.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(3, 30);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(136, 30);
            this.label29.TabIndex = 487;
            this.label29.Text = "Prévue le ";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label30.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(3, 60);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(136, 30);
            this.label30.TabIndex = 488;
            this.label30.Text = "Réalisé le";
            // 
            // DTDatePrevueDe
            // 
            stateEditorButton7.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless;
            stateEditorButton7.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DTDatePrevueDe.ButtonsLeft.Add(stateEditorButton7);
            this.DTDatePrevueDe.DateTime = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.DTDatePrevueDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DTDatePrevueDe.Location = new System.Drawing.Point(145, 33);
            this.DTDatePrevueDe.Name = "DTDatePrevueDe";
            this.DTDatePrevueDe.Size = new System.Drawing.Size(370, 26);
            this.DTDatePrevueDe.TabIndex = 575;
            this.DTDatePrevueDe.Value = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.DTDatePrevueDe.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTDatePrevueDe_BeforeDropDown);
            this.DTDatePrevueDe.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.dtDateDebPlaningBisSimul_AfterEditorButtonCheckStateChanged);
            // 
            // DTDateRealiseDe
            // 
            stateEditorButton8.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless;
            stateEditorButton8.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DTDateRealiseDe.ButtonsLeft.Add(stateEditorButton8);
            this.DTDateRealiseDe.DateTime = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.DTDateRealiseDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DTDateRealiseDe.Location = new System.Drawing.Point(145, 63);
            this.DTDateRealiseDe.Name = "DTDateRealiseDe";
            this.DTDateRealiseDe.Size = new System.Drawing.Size(370, 26);
            this.DTDateRealiseDe.TabIndex = 579;
            this.DTDateRealiseDe.Value = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.DTDateRealiseDe.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTDatePrevueDe_BeforeDropDown);
            this.DTDateRealiseDe.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.dtDateDebPlaningBisSimul_AfterEditorButtonCheckStateChanged);
            // 
            // DTDatePrevueAu
            // 
            stateEditorButton9.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless;
            stateEditorButton9.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DTDatePrevueAu.ButtonsLeft.Add(stateEditorButton9);
            this.DTDatePrevueAu.DateTime = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.DTDatePrevueAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DTDatePrevueAu.Location = new System.Drawing.Point(553, 33);
            this.DTDatePrevueAu.Name = "DTDatePrevueAu";
            this.DTDatePrevueAu.Size = new System.Drawing.Size(370, 26);
            this.DTDatePrevueAu.TabIndex = 576;
            this.DTDatePrevueAu.Value = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.DTDatePrevueAu.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTDatePrevueDe_BeforeDropDown);
            this.DTDatePrevueAu.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.dtDateDebPlaningBisSimul_AfterEditorButtonCheckStateChanged);
            // 
            // DTDateRealiseAu
            // 
            stateEditorButton10.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless;
            stateEditorButton10.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DTDateRealiseAu.ButtonsLeft.Add(stateEditorButton10);
            this.DTDateRealiseAu.DateTime = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.DTDateRealiseAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DTDateRealiseAu.Location = new System.Drawing.Point(553, 63);
            this.DTDateRealiseAu.Name = "DTDateRealiseAu";
            this.DTDateRealiseAu.Size = new System.Drawing.Size(370, 26);
            this.DTDateRealiseAu.TabIndex = 580;
            this.DTDateRealiseAu.Value = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.DTDateRealiseAu.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTDatePrevueDe_BeforeDropDown);
            this.DTDateRealiseAu.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.dtDateDebPlaningBisSimul_AfterEditorButtonCheckStateChanged);
            // 
            // DTCreeParDe
            // 
            stateEditorButton11.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless;
            stateEditorButton11.CheckState = System.Windows.Forms.CheckState.Checked;
            this.DTCreeParDe.ButtonsLeft.Add(stateEditorButton11);
            this.DTCreeParDe.DateTime = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.DTCreeParDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DTCreeParDe.Location = new System.Drawing.Point(1056, 3);
            this.DTCreeParDe.Name = "DTCreeParDe";
            this.DTCreeParDe.Size = new System.Drawing.Size(370, 26);
            this.DTCreeParDe.TabIndex = 573;
            this.DTCreeParDe.Value = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.DTCreeParDe.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTDatePrevueDe_BeforeDropDown);
            this.DTCreeParDe.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.dtDateDebPlaningBisSimul_AfterEditorButtonCheckStateChanged);
            // 
            // dtEnvoyeLeHisto1
            // 
            stateEditorButton12.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless;
            stateEditorButton12.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dtEnvoyeLeHisto1.ButtonsLeft.Add(stateEditorButton12);
            this.dtEnvoyeLeHisto1.DateTime = new System.DateTime(2019, 5, 6, 0, 0, 0, 0);
            this.dtEnvoyeLeHisto1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtEnvoyeLeHisto1.Location = new System.Drawing.Point(1056, 33);
            this.dtEnvoyeLeHisto1.Name = "dtEnvoyeLeHisto1";
            this.dtEnvoyeLeHisto1.Size = new System.Drawing.Size(370, 26);
            this.dtEnvoyeLeHisto1.TabIndex = 577;
            this.dtEnvoyeLeHisto1.Value = new System.DateTime(2019, 5, 6, 0, 0, 0, 0);
            this.dtEnvoyeLeHisto1.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTDatePrevueDe_BeforeDropDown);
            this.dtEnvoyeLeHisto1.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.dtDateDebPlaningBisSimul_AfterEditorButtonCheckStateChanged);
            // 
            // dtCreeParAu
            // 
            stateEditorButton13.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless;
            stateEditorButton13.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dtCreeParAu.ButtonsLeft.Add(stateEditorButton13);
            this.dtCreeParAu.DateTime = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.dtCreeParAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtCreeParAu.Location = new System.Drawing.Point(1462, 3);
            this.dtCreeParAu.Name = "dtCreeParAu";
            this.dtCreeParAu.Size = new System.Drawing.Size(371, 26);
            this.dtCreeParAu.TabIndex = 574;
            this.dtCreeParAu.Value = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.dtCreeParAu.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTDatePrevueDe_BeforeDropDown);
            this.dtCreeParAu.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.dtDateDebPlaningBisSimul_AfterEditorButtonCheckStateChanged);
            // 
            // dtEnvoyeLeHisto2
            // 
            stateEditorButton14.ButtonStyle = Infragistics.Win.UIElementButtonStyle.FlatBorderless;
            stateEditorButton14.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dtEnvoyeLeHisto2.ButtonsLeft.Add(stateEditorButton14);
            this.dtEnvoyeLeHisto2.DateTime = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.dtEnvoyeLeHisto2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtEnvoyeLeHisto2.Location = new System.Drawing.Point(1462, 33);
            this.dtEnvoyeLeHisto2.Name = "dtEnvoyeLeHisto2";
            this.dtEnvoyeLeHisto2.Size = new System.Drawing.Size(371, 26);
            this.dtEnvoyeLeHisto2.TabIndex = 578;
            this.dtEnvoyeLeHisto2.Value = new System.DateTime(2011, 7, 26, 0, 0, 0, 0);
            this.dtEnvoyeLeHisto2.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTDatePrevueDe_BeforeDropDown);
            this.dtEnvoyeLeHisto2.AfterEditorButtonCheckStateChanged += new Infragistics.Win.UltraWinEditors.EditorButtonEventHandler(this.dtDateDebPlaningBisSimul_AfterEditorButtonCheckStateChanged);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label31.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(521, 30);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(26, 30);
            this.label31.TabIndex = 489;
            this.label31.Text = "au";
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label32.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(521, 60);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(26, 30);
            this.label32.TabIndex = 498;
            this.label32.Text = "au";
            // 
            // cmdIntervHisto
            // 
            this.cmdIntervHisto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdIntervHisto.FlatAppearance.BorderSize = 0;
            this.cmdIntervHisto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdIntervHisto.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdIntervHisto.Image = ((System.Drawing.Image)(resources.GetObject("cmdIntervHisto.Image")));
            this.cmdIntervHisto.Location = new System.Drawing.Point(929, 153);
            this.cmdIntervHisto.Name = "cmdIntervHisto";
            this.cmdIntervHisto.Size = new System.Drawing.Size(24, 20);
            this.cmdIntervHisto.TabIndex = 480;
            this.toolTip1.SetToolTip(this.cmdIntervHisto, "Recherche de l\'intervenant");
            this.cmdIntervHisto.UseVisualStyleBackColor = false;
            this.cmdIntervHisto.Click += new System.EventHandler(this.cmdIntervHisto_Click);
            // 
            // cmdMatEntretien
            // 
            this.cmdMatEntretien.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdMatEntretien.FlatAppearance.BorderSize = 0;
            this.cmdMatEntretien.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMatEntretien.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdMatEntretien.Image = ((System.Drawing.Image)(resources.GetObject("cmdMatEntretien.Image")));
            this.cmdMatEntretien.Location = new System.Drawing.Point(929, 123);
            this.cmdMatEntretien.Name = "cmdMatEntretien";
            this.cmdMatEntretien.Size = new System.Drawing.Size(24, 20);
            this.cmdMatEntretien.TabIndex = 479;
            this.toolTip1.SetToolTip(this.cmdMatEntretien, "Recherche de l\'intervenant");
            this.cmdMatEntretien.UseVisualStyleBackColor = false;
            this.cmdMatEntretien.Click += new System.EventHandler(this.cmdMatEntretien_Click);
            // 
            // cmdImmeubleHisto
            // 
            this.cmdImmeubleHisto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdImmeubleHisto.FlatAppearance.BorderSize = 0;
            this.cmdImmeubleHisto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImmeubleHisto.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdImmeubleHisto.Image = ((System.Drawing.Image)(resources.GetObject("cmdImmeubleHisto.Image")));
            this.cmdImmeubleHisto.Location = new System.Drawing.Point(929, 93);
            this.cmdImmeubleHisto.Name = "cmdImmeubleHisto";
            this.cmdImmeubleHisto.Size = new System.Drawing.Size(24, 20);
            this.cmdImmeubleHisto.TabIndex = 478;
            this.toolTip1.SetToolTip(this.cmdImmeubleHisto, "Recherche de l\'immeuble");
            this.cmdImmeubleHisto.UseVisualStyleBackColor = false;
            this.cmdImmeubleHisto.Click += new System.EventHandler(this.cmdImmeubleHisto_Click);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(959, 90);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(91, 30);
            this.label24.TabIndex = 499;
            this.label24.Text = "N° Interv.";
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label34.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(959, 120);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(91, 30);
            this.label34.TabIndex = 400;
            this.label34.Text = "Client";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label35.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(959, 150);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(91, 30);
            this.label35.TabIndex = 401;
            this.label35.Text = "Statuts PDA";
            // 
            // cmbStatutPDAhisto
            // 
            appearance87.BackColor = System.Drawing.SystemColors.Window;
            appearance87.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbStatutPDAhisto.DisplayLayout.Appearance = appearance87;
            this.cmbStatutPDAhisto.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbStatutPDAhisto.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance88.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance88.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance88.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance88.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbStatutPDAhisto.DisplayLayout.GroupByBox.Appearance = appearance88;
            appearance89.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbStatutPDAhisto.DisplayLayout.GroupByBox.BandLabelAppearance = appearance89;
            this.cmbStatutPDAhisto.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance90.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance90.BackColor2 = System.Drawing.SystemColors.Control;
            appearance90.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance90.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbStatutPDAhisto.DisplayLayout.GroupByBox.PromptAppearance = appearance90;
            this.cmbStatutPDAhisto.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbStatutPDAhisto.DisplayLayout.MaxRowScrollRegions = 1;
            appearance91.BackColor = System.Drawing.SystemColors.Window;
            appearance91.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbStatutPDAhisto.DisplayLayout.Override.ActiveCellAppearance = appearance91;
            appearance92.BackColor = System.Drawing.SystemColors.Highlight;
            appearance92.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbStatutPDAhisto.DisplayLayout.Override.ActiveRowAppearance = appearance92;
            this.cmbStatutPDAhisto.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbStatutPDAhisto.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance93.BackColor = System.Drawing.SystemColors.Window;
            this.cmbStatutPDAhisto.DisplayLayout.Override.CardAreaAppearance = appearance93;
            appearance94.BorderColor = System.Drawing.Color.Silver;
            appearance94.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbStatutPDAhisto.DisplayLayout.Override.CellAppearance = appearance94;
            this.cmbStatutPDAhisto.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbStatutPDAhisto.DisplayLayout.Override.CellPadding = 0;
            appearance95.BackColor = System.Drawing.SystemColors.Control;
            appearance95.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance95.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance95.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance95.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbStatutPDAhisto.DisplayLayout.Override.GroupByRowAppearance = appearance95;
            appearance96.TextHAlignAsString = "Left";
            this.cmbStatutPDAhisto.DisplayLayout.Override.HeaderAppearance = appearance96;
            this.cmbStatutPDAhisto.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbStatutPDAhisto.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance97.BackColor = System.Drawing.SystemColors.Window;
            appearance97.BorderColor = System.Drawing.Color.Silver;
            this.cmbStatutPDAhisto.DisplayLayout.Override.RowAppearance = appearance97;
            this.cmbStatutPDAhisto.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance98.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbStatutPDAhisto.DisplayLayout.Override.TemplateAddRowAppearance = appearance98;
            this.cmbStatutPDAhisto.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbStatutPDAhisto.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbStatutPDAhisto.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbStatutPDAhisto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbStatutPDAhisto.Location = new System.Drawing.Point(1056, 153);
            this.cmbStatutPDAhisto.Name = "cmbStatutPDAhisto";
            this.cmbStatutPDAhisto.Size = new System.Drawing.Size(370, 27);
            this.cmbStatutPDAhisto.TabIndex = 589;
            // 
            // txtNoInterventionHisto
            // 
            this.txtNoInterventionHisto.AccAcceptNumbersOnly = false;
            this.txtNoInterventionHisto.AccAllowComma = false;
            this.txtNoInterventionHisto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoInterventionHisto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoInterventionHisto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoInterventionHisto.AccHidenValue = "";
            this.txtNoInterventionHisto.AccNotAllowedChars = null;
            this.txtNoInterventionHisto.AccReadOnly = false;
            this.txtNoInterventionHisto.AccReadOnlyAllowDelete = false;
            this.txtNoInterventionHisto.AccRequired = false;
            this.txtNoInterventionHisto.BackColor = System.Drawing.Color.White;
            this.txtNoInterventionHisto.CustomBackColor = System.Drawing.Color.White;
            this.txtNoInterventionHisto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoInterventionHisto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoInterventionHisto.ForeColor = System.Drawing.Color.Black;
            this.txtNoInterventionHisto.Location = new System.Drawing.Point(1055, 92);
            this.txtNoInterventionHisto.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoInterventionHisto.MaxLength = 32767;
            this.txtNoInterventionHisto.Multiline = false;
            this.txtNoInterventionHisto.Name = "txtNoInterventionHisto";
            this.txtNoInterventionHisto.ReadOnly = false;
            this.txtNoInterventionHisto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoInterventionHisto.Size = new System.Drawing.Size(372, 27);
            this.txtNoInterventionHisto.TabIndex = 587;
            this.txtNoInterventionHisto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoInterventionHisto.UseSystemPasswordChar = false;
            // 
            // txtCode1Histo
            // 
            this.txtCode1Histo.AccAcceptNumbersOnly = false;
            this.txtCode1Histo.AccAllowComma = false;
            this.txtCode1Histo.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCode1Histo.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCode1Histo.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCode1Histo.AccHidenValue = "";
            this.txtCode1Histo.AccNotAllowedChars = null;
            this.txtCode1Histo.AccReadOnly = false;
            this.txtCode1Histo.AccReadOnlyAllowDelete = false;
            this.txtCode1Histo.AccRequired = false;
            this.txtCode1Histo.BackColor = System.Drawing.Color.White;
            this.txtCode1Histo.CustomBackColor = System.Drawing.Color.White;
            this.txtCode1Histo.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCode1Histo.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCode1Histo.ForeColor = System.Drawing.Color.Black;
            this.txtCode1Histo.Location = new System.Drawing.Point(1055, 122);
            this.txtCode1Histo.Margin = new System.Windows.Forms.Padding(2);
            this.txtCode1Histo.MaxLength = 32767;
            this.txtCode1Histo.Multiline = false;
            this.txtCode1Histo.Name = "txtCode1Histo";
            this.txtCode1Histo.ReadOnly = false;
            this.txtCode1Histo.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCode1Histo.Size = new System.Drawing.Size(372, 27);
            this.txtCode1Histo.TabIndex = 588;
            this.txtCode1Histo.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCode1Histo.UseSystemPasswordChar = false;
            this.txtCode1Histo.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCode1Histo_KeyPress);
            // 
            // cmdFindNoInter
            // 
            this.cmdFindNoInter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFindNoInter.FlatAppearance.BorderSize = 0;
            this.cmdFindNoInter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFindNoInter.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdFindNoInter.Image = ((System.Drawing.Image)(resources.GetObject("cmdFindNoInter.Image")));
            this.cmdFindNoInter.Location = new System.Drawing.Point(1432, 93);
            this.cmdFindNoInter.Name = "cmdFindNoInter";
            this.cmdFindNoInter.Size = new System.Drawing.Size(24, 19);
            this.cmdFindNoInter.TabIndex = 405;
            this.cmdFindNoInter.UseVisualStyleBackColor = false;
            this.cmdFindNoInter.Visible = false;
            // 
            // cmdClient
            // 
            this.cmdClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdClient.FlatAppearance.BorderSize = 0;
            this.cmdClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClient.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdClient.Image = ((System.Drawing.Image)(resources.GetObject("cmdClient.Image")));
            this.cmdClient.Location = new System.Drawing.Point(1432, 123);
            this.cmdClient.Name = "cmdClient";
            this.cmdClient.Size = new System.Drawing.Size(24, 19);
            this.cmdClient.TabIndex = 406;
            this.cmdClient.UseVisualStyleBackColor = false;
            this.cmdClient.Click += new System.EventHandler(this.cmdClient_Click);
            // 
            // lblLibEntretien
            // 
            this.lblLibEntretien.AccAcceptNumbersOnly = false;
            this.lblLibEntretien.AccAllowComma = false;
            this.lblLibEntretien.AccBackgroundColor = System.Drawing.Color.White;
            this.lblLibEntretien.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblLibEntretien.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblLibEntretien.AccHidenValue = "";
            this.lblLibEntretien.AccNotAllowedChars = null;
            this.lblLibEntretien.AccReadOnly = false;
            this.lblLibEntretien.AccReadOnlyAllowDelete = false;
            this.lblLibEntretien.AccRequired = false;
            this.lblLibEntretien.BackColor = System.Drawing.Color.White;
            this.lblLibEntretien.CustomBackColor = System.Drawing.Color.White;
            this.lblLibEntretien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibEntretien.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lblLibEntretien.ForeColor = System.Drawing.Color.Black;
            this.lblLibEntretien.Location = new System.Drawing.Point(552, 122);
            this.lblLibEntretien.Margin = new System.Windows.Forms.Padding(2);
            this.lblLibEntretien.MaxLength = 32767;
            this.lblLibEntretien.Multiline = false;
            this.lblLibEntretien.Name = "lblLibEntretien";
            this.lblLibEntretien.ReadOnly = true;
            this.lblLibEntretien.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblLibEntretien.Size = new System.Drawing.Size(372, 27);
            this.lblLibEntretien.TabIndex = 583;
            this.lblLibEntretien.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblLibEntretien.UseSystemPasswordChar = false;
            // 
            // lbllibIntervenantHisto
            // 
            this.lbllibIntervenantHisto.AccAcceptNumbersOnly = false;
            this.lbllibIntervenantHisto.AccAllowComma = false;
            this.lbllibIntervenantHisto.AccBackgroundColor = System.Drawing.Color.White;
            this.lbllibIntervenantHisto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lbllibIntervenantHisto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lbllibIntervenantHisto.AccHidenValue = "";
            this.lbllibIntervenantHisto.AccNotAllowedChars = null;
            this.lbllibIntervenantHisto.AccReadOnly = false;
            this.lbllibIntervenantHisto.AccReadOnlyAllowDelete = false;
            this.lbllibIntervenantHisto.AccRequired = false;
            this.lbllibIntervenantHisto.BackColor = System.Drawing.Color.White;
            this.lbllibIntervenantHisto.CustomBackColor = System.Drawing.Color.White;
            this.lbllibIntervenantHisto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbllibIntervenantHisto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lbllibIntervenantHisto.ForeColor = System.Drawing.Color.Black;
            this.lbllibIntervenantHisto.Location = new System.Drawing.Point(552, 152);
            this.lbllibIntervenantHisto.Margin = new System.Windows.Forms.Padding(2);
            this.lbllibIntervenantHisto.MaxLength = 32767;
            this.lbllibIntervenantHisto.Multiline = false;
            this.lbllibIntervenantHisto.Name = "lbllibIntervenantHisto";
            this.lbllibIntervenantHisto.ReadOnly = true;
            this.lbllibIntervenantHisto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lbllibIntervenantHisto.Size = new System.Drawing.Size(372, 27);
            this.lbllibIntervenantHisto.TabIndex = 585;
            this.lbllibIntervenantHisto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lbllibIntervenantHisto.UseSystemPasswordChar = false;
            // 
            // frame1113
            // 
            this.tableLayoutPanel14.SetColumnSpan(this.frame1113, 4);
            this.frame1113.Controls.Add(this.Opt2);
            this.frame1113.Controls.Add(this.Opt1);
            this.frame1113.Controls.Add(this.Opt0);
            this.frame1113.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frame1113.Location = new System.Drawing.Point(1, 181);
            this.frame1113.Margin = new System.Windows.Forms.Padding(1);
            this.frame1113.Name = "frame1113";
            this.frame1113.Size = new System.Drawing.Size(924, 34);
            this.frame1113.TabIndex = 611;
            // 
            // Opt2
            // 
            this.Opt2.AutoSize = true;
            this.Opt2.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Opt2.Location = new System.Drawing.Point(3, 3);
            this.Opt2.Name = "Opt2";
            this.Opt2.Size = new System.Drawing.Size(186, 23);
            this.Opt2.TabIndex = 538;
            this.Opt2.Tag = "2";
            this.Opt2.Text = "Particulier/Propriétaire";
            this.Opt2.UseVisualStyleBackColor = true;
            // 
            // Opt1
            // 
            this.Opt1.AutoSize = true;
            this.Opt1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Opt1.Location = new System.Drawing.Point(195, 3);
            this.Opt1.Name = "Opt1";
            this.Opt1.Size = new System.Drawing.Size(96, 23);
            this.Opt1.TabIndex = 539;
            this.Opt1.Tag = "1";
            this.Opt1.Text = "Immeuble";
            this.Opt1.UseVisualStyleBackColor = true;
            // 
            // Opt0
            // 
            this.Opt0.AutoSize = true;
            this.Opt0.Checked = true;
            this.Opt0.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Opt0.Location = new System.Drawing.Point(297, 3);
            this.Opt0.Name = "Opt0";
            this.Opt0.Size = new System.Drawing.Size(64, 23);
            this.Opt0.TabIndex = 540;
            this.Opt0.TabStop = true;
            this.Opt0.Tag = "0";
            this.Opt0.Text = "Tous.";
            this.Opt0.UseVisualStyleBackColor = true;
            // 
            // flowLayoutPanel6
            // 
            this.flowLayoutPanel6.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel6.Controls.Add(this.cmdinitHistorique);
            this.flowLayoutPanel6.Controls.Add(this.Command2);
            this.flowLayoutPanel6.Controls.Add(this.cmdExportHisto);
            this.flowLayoutPanel6.Controls.Add(this.cmdHistoRamonage);
            this.flowLayoutPanel6.Controls.Add(this.CmdLegende2);
            this.flowLayoutPanel6.Controls.Add(this.cmdRapportPDA);
            this.flowLayoutPanel6.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(1348, 3);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(497, 44);
            this.flowLayoutPanel6.TabIndex = 412;
            // 
            // cmdinitHistorique
            // 
            this.cmdinitHistorique.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdinitHistorique.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdinitHistorique.FlatAppearance.BorderSize = 0;
            this.cmdinitHistorique.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdinitHistorique.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdinitHistorique.Image = ((System.Drawing.Image)(resources.GetObject("cmdinitHistorique.Image")));
            this.cmdinitHistorique.Location = new System.Drawing.Point(435, 2);
            this.cmdinitHistorique.Margin = new System.Windows.Forms.Padding(2);
            this.cmdinitHistorique.Name = "cmdinitHistorique";
            this.cmdinitHistorique.Size = new System.Drawing.Size(60, 35);
            this.cmdinitHistorique.TabIndex = 373;
            this.cmdinitHistorique.Tag = "";
            this.cmdinitHistorique.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdinitHistorique, "Effacer tous les champs .");
            this.cmdinitHistorique.UseVisualStyleBackColor = false;
            this.cmdinitHistorique.Click += new System.EventHandler(this.cmdinitHistorique_Click);
            // 
            // Command2
            // 
            this.Command2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command2.FlatAppearance.BorderSize = 0;
            this.Command2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command2.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command2.Image = ((System.Drawing.Image)(resources.GetObject("Command2.Image")));
            this.Command2.Location = new System.Drawing.Point(371, 2);
            this.Command2.Margin = new System.Windows.Forms.Padding(2);
            this.Command2.Name = "Command2";
            this.Command2.Size = new System.Drawing.Size(60, 35);
            this.Command2.TabIndex = 583;
            this.Command2.Tag = "";
            this.toolTip1.SetToolTip(this.Command2, "Enregistrer");
            this.Command2.UseVisualStyleBackColor = false;
            // 
            // cmdExportHisto
            // 
            this.cmdExportHisto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdExportHisto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExportHisto.FlatAppearance.BorderSize = 0;
            this.cmdExportHisto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExportHisto.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExportHisto.Image = ((System.Drawing.Image)(resources.GetObject("cmdExportHisto.Image")));
            this.cmdExportHisto.Location = new System.Drawing.Point(307, 2);
            this.cmdExportHisto.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExportHisto.Name = "cmdExportHisto";
            this.cmdExportHisto.Size = new System.Drawing.Size(60, 35);
            this.cmdExportHisto.TabIndex = 582;
            this.cmdExportHisto.Tag = "";
            this.cmdExportHisto.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdExportHisto, "Exporter la selection");
            this.cmdExportHisto.UseVisualStyleBackColor = false;
            this.cmdExportHisto.Click += new System.EventHandler(this.cmdExportHisto_Click);
            // 
            // cmdHistoRamonage
            // 
            this.cmdHistoRamonage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdHistoRamonage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdHistoRamonage.FlatAppearance.BorderSize = 0;
            this.cmdHistoRamonage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdHistoRamonage.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdHistoRamonage.Image = ((System.Drawing.Image)(resources.GetObject("cmdHistoRamonage.Image")));
            this.cmdHistoRamonage.Location = new System.Drawing.Point(243, 2);
            this.cmdHistoRamonage.Margin = new System.Windows.Forms.Padding(2);
            this.cmdHistoRamonage.Name = "cmdHistoRamonage";
            this.cmdHistoRamonage.Size = new System.Drawing.Size(60, 35);
            this.cmdHistoRamonage.TabIndex = 374;
            this.cmdHistoRamonage.Tag = "";
            this.cmdHistoRamonage.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdHistoRamonage, "Rechercher");
            this.cmdHistoRamonage.UseVisualStyleBackColor = false;
            this.cmdHistoRamonage.Click += new System.EventHandler(this.cmdHistoRamonage_Click);
            // 
            // CmdLegende2
            // 
            this.CmdLegende2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdLegende2.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.CmdLegende2.FlatAppearance.BorderSize = 0;
            this.CmdLegende2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdLegende2.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdLegende2.ForeColor = System.Drawing.Color.White;
            this.CmdLegende2.Image = ((System.Drawing.Image)(resources.GetObject("CmdLegende2.Image")));
            this.CmdLegende2.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdLegende2.Location = new System.Drawing.Point(140, 2);
            this.CmdLegende2.Margin = new System.Windows.Forms.Padding(2);
            this.CmdLegende2.Name = "CmdLegende2";
            this.CmdLegende2.Size = new System.Drawing.Size(99, 35);
            this.CmdLegende2.TabIndex = 581;
            this.CmdLegende2.Tag = "2";
            this.CmdLegende2.Text = "      Légende";
            this.CmdLegende2.UseVisualStyleBackColor = false;
            this.CmdLegende2.Click += new System.EventHandler(this.CmdLegende0_Click);
            // 
            // cmdRapportPDA
            // 
            this.cmdRapportPDA.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdRapportPDA.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdRapportPDA.FlatAppearance.BorderSize = 0;
            this.cmdRapportPDA.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRapportPDA.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRapportPDA.ForeColor = System.Drawing.Color.White;
            this.cmdRapportPDA.Image = ((System.Drawing.Image)(resources.GetObject("cmdRapportPDA.Image")));
            this.cmdRapportPDA.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdRapportPDA.Location = new System.Drawing.Point(13, 2);
            this.cmdRapportPDA.Margin = new System.Windows.Forms.Padding(2);
            this.cmdRapportPDA.Name = "cmdRapportPDA";
            this.cmdRapportPDA.Size = new System.Drawing.Size(123, 35);
            this.cmdRapportPDA.TabIndex = 580;
            this.cmdRapportPDA.Tag = " ";
            this.cmdRapportPDA.Text = "    Rapport PDA";
            this.cmdRapportPDA.UseVisualStyleBackColor = false;
            this.cmdRapportPDA.Visible = false;
            this.cmdRapportPDA.Click += new System.EventHandler(this.cmdRapportPDA_Click);
            // 
            // groupBox12
            // 
            this.groupBox12.BackColor = System.Drawing.Color.Transparent;
            this.groupBox12.Controls.Add(this.ssDropMatricule);
            this.groupBox12.Controls.Add(this.ssGridHisto);
            this.groupBox12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox12.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox12.Location = new System.Drawing.Point(3, 298);
            this.groupBox12.Name = "groupBox12";
            this.groupBox12.Size = new System.Drawing.Size(1842, 299);
            this.groupBox12.TabIndex = 413;
            this.groupBox12.TabStop = false;
            // 
            // ssDropMatricule
            // 
            appearance99.BackColor = System.Drawing.SystemColors.Window;
            appearance99.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssDropMatricule.DisplayLayout.Appearance = appearance99;
            this.ssDropMatricule.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssDropMatricule.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance100.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance100.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance100.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance100.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropMatricule.DisplayLayout.GroupByBox.Appearance = appearance100;
            appearance101.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropMatricule.DisplayLayout.GroupByBox.BandLabelAppearance = appearance101;
            this.ssDropMatricule.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance102.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance102.BackColor2 = System.Drawing.SystemColors.Control;
            appearance102.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance102.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropMatricule.DisplayLayout.GroupByBox.PromptAppearance = appearance102;
            this.ssDropMatricule.DisplayLayout.MaxColScrollRegions = 1;
            this.ssDropMatricule.DisplayLayout.MaxRowScrollRegions = 1;
            appearance103.BackColor = System.Drawing.SystemColors.Window;
            appearance103.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssDropMatricule.DisplayLayout.Override.ActiveCellAppearance = appearance103;
            appearance104.BackColor = System.Drawing.SystemColors.Highlight;
            appearance104.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssDropMatricule.DisplayLayout.Override.ActiveRowAppearance = appearance104;
            this.ssDropMatricule.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssDropMatricule.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance105.BackColor = System.Drawing.SystemColors.Window;
            this.ssDropMatricule.DisplayLayout.Override.CardAreaAppearance = appearance105;
            appearance106.BorderColor = System.Drawing.Color.Silver;
            appearance106.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssDropMatricule.DisplayLayout.Override.CellAppearance = appearance106;
            this.ssDropMatricule.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssDropMatricule.DisplayLayout.Override.CellPadding = 0;
            appearance107.BackColor = System.Drawing.SystemColors.Control;
            appearance107.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance107.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance107.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance107.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropMatricule.DisplayLayout.Override.GroupByRowAppearance = appearance107;
            appearance108.TextHAlignAsString = "Left";
            this.ssDropMatricule.DisplayLayout.Override.HeaderAppearance = appearance108;
            this.ssDropMatricule.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssDropMatricule.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance109.BackColor = System.Drawing.SystemColors.Window;
            appearance109.BorderColor = System.Drawing.Color.Silver;
            this.ssDropMatricule.DisplayLayout.Override.RowAppearance = appearance109;
            this.ssDropMatricule.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance110.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssDropMatricule.DisplayLayout.Override.TemplateAddRowAppearance = appearance110;
            this.ssDropMatricule.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssDropMatricule.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssDropMatricule.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ssDropMatricule.Location = new System.Drawing.Point(292, 59);
            this.ssDropMatricule.Name = "ssDropMatricule";
            this.ssDropMatricule.Size = new System.Drawing.Size(186, 27);
            this.ssDropMatricule.TabIndex = 503;
            this.ssDropMatricule.Visible = false;
            // 
            // ssGridHisto
            // 
            appearance111.BackColor = System.Drawing.SystemColors.Window;
            appearance111.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridHisto.DisplayLayout.Appearance = appearance111;
            this.ssGridHisto.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridHisto.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance112.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance112.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance112.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance112.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridHisto.DisplayLayout.GroupByBox.Appearance = appearance112;
            appearance113.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridHisto.DisplayLayout.GroupByBox.BandLabelAppearance = appearance113;
            this.ssGridHisto.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance114.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance114.BackColor2 = System.Drawing.SystemColors.Control;
            appearance114.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance114.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridHisto.DisplayLayout.GroupByBox.PromptAppearance = appearance114;
            this.ssGridHisto.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridHisto.DisplayLayout.MaxRowScrollRegions = 1;
            appearance115.BackColor = System.Drawing.SystemColors.Window;
            appearance115.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridHisto.DisplayLayout.Override.ActiveCellAppearance = appearance115;
            appearance116.BackColor = System.Drawing.SystemColors.Highlight;
            appearance116.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridHisto.DisplayLayout.Override.ActiveRowAppearance = appearance116;
            this.ssGridHisto.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssGridHisto.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridHisto.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridHisto.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridHisto.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance117.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridHisto.DisplayLayout.Override.CardAreaAppearance = appearance117;
            appearance118.BorderColor = System.Drawing.Color.Silver;
            appearance118.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridHisto.DisplayLayout.Override.CellAppearance = appearance118;
            this.ssGridHisto.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridHisto.DisplayLayout.Override.CellPadding = 0;
            appearance119.BackColor = System.Drawing.SystemColors.Control;
            appearance119.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance119.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance119.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance119.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridHisto.DisplayLayout.Override.GroupByRowAppearance = appearance119;
            appearance120.TextHAlignAsString = "Left";
            this.ssGridHisto.DisplayLayout.Override.HeaderAppearance = appearance120;
            this.ssGridHisto.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridHisto.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance121.BackColor = System.Drawing.SystemColors.Window;
            appearance121.BorderColor = System.Drawing.Color.Silver;
            this.ssGridHisto.DisplayLayout.Override.RowAppearance = appearance121;
            this.ssGridHisto.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridHisto.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance122.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridHisto.DisplayLayout.Override.TemplateAddRowAppearance = appearance122;
            this.ssGridHisto.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridHisto.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridHisto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridHisto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.ssGridHisto.Location = new System.Drawing.Point(3, 20);
            this.ssGridHisto.Name = "ssGridHisto";
            this.ssGridHisto.Size = new System.Drawing.Size(1836, 276);
            this.ssGridHisto.TabIndex = 412;
            this.ssGridHisto.Text = "ultraGrid1";
            this.ssGridHisto.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridHisto_InitializeLayout);
            this.ssGridHisto.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssGridHisto_InitializeRow);
            this.ssGridHisto.AfterRowActivate += new System.EventHandler(this.ssGridHisto_AfterRowActivate);
            this.ssGridHisto.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssGridHisto_BeforeRowsDeleted);
            this.ssGridHisto.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.ssGridHisto_Error);
            this.ssGridHisto.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.ssGridHisto_DoubleClickRow);
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 7;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.8999F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.41558F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 1.298701F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 23.21981F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.8999F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.8999F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 12.2807F));
            this.tableLayoutPanel17.Controls.Add(this.optTachesHisto, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.optOperationHisto, 0, 0);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Location = new System.Drawing.Point(0, 600);
            this.tableLayoutPanel17.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 1;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(1848, 27);
            this.tableLayoutPanel17.TabIndex = 415;
            // 
            // optTachesHisto
            // 
            this.optTachesHisto.AutoSize = true;
            this.optTachesHisto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optTachesHisto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optTachesHisto.Location = new System.Drawing.Point(241, 3);
            this.optTachesHisto.Name = "optTachesHisto";
            this.optTachesHisto.Size = new System.Drawing.Size(445, 21);
            this.optTachesHisto.TabIndex = 539;
            this.optTachesHisto.Text = "Nature des opérations";
            this.optTachesHisto.UseVisualStyleBackColor = true;
            this.optTachesHisto.CheckedChanged += new System.EventHandler(this.optTachesHisto_CheckedChanged);
            // 
            // optOperationHisto
            // 
            this.optOperationHisto.AutoSize = true;
            this.optOperationHisto.Checked = true;
            this.optOperationHisto.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optOperationHisto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optOperationHisto.Location = new System.Drawing.Point(3, 3);
            this.optOperationHisto.Name = "optOperationHisto";
            this.optOperationHisto.Size = new System.Drawing.Size(232, 21);
            this.optOperationHisto.TabIndex = 538;
            this.optOperationHisto.TabStop = true;
            this.optOperationHisto.Text = "Gammes";
            this.optOperationHisto.UseVisualStyleBackColor = true;
            this.optOperationHisto.CheckedChanged += new System.EventHandler(this.optOperationHisto_CheckedChanged);
            // 
            // ultraTabPageControl8
            // 
            this.ultraTabPageControl8.Controls.Add(this.frame1111);
            this.ultraTabPageControl8.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl8.Name = "ultraTabPageControl8";
            this.ultraTabPageControl8.Size = new System.Drawing.Size(1848, 933);
            // 
            // frame1111
            // 
            this.frame1111.BackColor = System.Drawing.Color.Transparent;
            this.frame1111.Controls.Add(this.label43);
            this.frame1111.Controls.Add(this.label42);
            this.frame1111.Controls.Add(this.cmdRechercheContrat);
            this.frame1111.Controls.Add(this.txtCOP_NoContrat);
            this.frame1111.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.frame1111.Location = new System.Drawing.Point(850, 150);
            this.frame1111.Name = "frame1111";
            this.frame1111.Size = new System.Drawing.Size(239, 103);
            this.frame1111.TabIndex = 412;
            this.frame1111.TabStop = false;
            this.frame1111.Visible = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(6, 54);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(31, 19);
            this.label43.TabIndex = 608;
            this.label43.Text = "Avt";
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(2, 21);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(79, 19);
            this.label42.TabIndex = 607;
            this.label42.Text = "N°Contrat";
            // 
            // cmdRechercheContrat
            // 
            this.cmdRechercheContrat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheContrat.FlatAppearance.BorderSize = 0;
            this.cmdRechercheContrat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheContrat.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheContrat.Image = ((System.Drawing.Image)(resources.GetObject("cmdRechercheContrat.Image")));
            this.cmdRechercheContrat.Location = new System.Drawing.Point(214, 21);
            this.cmdRechercheContrat.Name = "cmdRechercheContrat";
            this.cmdRechercheContrat.Size = new System.Drawing.Size(24, 19);
            this.cmdRechercheContrat.TabIndex = 606;
            this.cmdRechercheContrat.UseVisualStyleBackColor = false;
            this.cmdRechercheContrat.Visible = false;
            // 
            // txtCOP_NoContrat
            // 
            this.txtCOP_NoContrat.AccAcceptNumbersOnly = false;
            this.txtCOP_NoContrat.AccAllowComma = false;
            this.txtCOP_NoContrat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCOP_NoContrat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCOP_NoContrat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCOP_NoContrat.AccHidenValue = "";
            this.txtCOP_NoContrat.AccNotAllowedChars = null;
            this.txtCOP_NoContrat.AccReadOnly = false;
            this.txtCOP_NoContrat.AccReadOnlyAllowDelete = false;
            this.txtCOP_NoContrat.AccRequired = false;
            this.txtCOP_NoContrat.BackColor = System.Drawing.Color.White;
            this.txtCOP_NoContrat.CustomBackColor = System.Drawing.Color.White;
            this.txtCOP_NoContrat.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCOP_NoContrat.ForeColor = System.Drawing.Color.Black;
            this.txtCOP_NoContrat.Location = new System.Drawing.Point(86, 21);
            this.txtCOP_NoContrat.Margin = new System.Windows.Forms.Padding(2);
            this.txtCOP_NoContrat.MaxLength = 32767;
            this.txtCOP_NoContrat.Multiline = false;
            this.txtCOP_NoContrat.Name = "txtCOP_NoContrat";
            this.txtCOP_NoContrat.ReadOnly = false;
            this.txtCOP_NoContrat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCOP_NoContrat.Size = new System.Drawing.Size(122, 27);
            this.txtCOP_NoContrat.TabIndex = 503;
            this.txtCOP_NoContrat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCOP_NoContrat.UseSystemPasswordChar = false;
            this.txtCOP_NoContrat.Visible = false;
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl2);
            this.SSTab1.Controls.Add(this.ultraTabPageControl3);
            this.SSTab1.Controls.Add(this.ultraTabPageControl4);
            this.SSTab1.Controls.Add(this.ultraTabPageControl7);
            this.SSTab1.Controls.Add(this.ultraTabPageControl8);
            this.SSTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSTab1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.SSTab1.Location = new System.Drawing.Point(0, 0);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.SSTab1.Size = new System.Drawing.Size(1850, 957);
            this.SSTab1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Excel;
            this.SSTab1.TabIndex = 410;
            ultraTab3.TabPage = this.ultraTabPageControl1;
            ultraTab3.Text = "Simulation";
            ultraTab7.TabPage = this.ultraTabPageControl7;
            ultraTab7.Text = "Charges";
            ultraTab4.TabPage = this.ultraTabPageControl2;
            ultraTab4.Text = "Génération/Transfert vers PDA";
            ultraTab5.TabPage = this.ultraTabPageControl3;
            ultraTab5.Text = "Avis de passage";
            ultraTab6.TabPage = this.ultraTabPageControl4;
            ultraTab6.Text = "Historique des visites=>PDA";
            ultraTab8.TabPage = this.ultraTabPageControl8;
            ultraTab8.Text = "tab1";
            ultraTab8.Visible = false;
            this.SSTab1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab3,
            ultraTab7,
            ultraTab4,
            ultraTab5,
            ultraTab6,
            ultraTab8});
            this.SSTab1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1848, 933);
            // 
            // UserIntervP2V2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.SSTab1);
            this.Name = "UserIntervP2V2";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Load += new System.EventHandler(this.UserIntervP2V2_Load);
            this.VisibleChanged += new System.EventHandler(this.UserIntervP2V2_VisibleChanged);
            this.ultraTabPageControl5.ResumeLayout(false);
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel10.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssOpeTaches)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridTachesAnnuel)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridVisite)).EndInit();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.ultraTabPageControl6.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.groupBox5.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            this.groupBox10.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.PL1)).EndInit();
            this.ultraTabPageControl1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.groupBox2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssPlanCharge)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssOpTachesSimul)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridTachesAnnuelSimul)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateDebPlaningBisSimul)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateFinPlaningBisSimul)).EndInit();
            this.frame117.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.groupBox11.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel21.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.frame115.ResumeLayout(false);
            this.frame115.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridVisiteSimul)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.pctPlanCharge.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel20.ResumeLayout(false);
            this.frame1110.ResumeLayout(false);
            this.frame1110.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ultraTabPageControl7.ResumeLayout(false);
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel23.ResumeLayout(false);
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tableLayoutPanel24.PerformLayout();
            this.tableLayoutPanel25.ResumeLayout(false);
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel27.ResumeLayout(false);
            this.groupBox13.ResumeLayout(false);
            this.groupBox13.PerformLayout();
            this.tableLayoutPanel28.ResumeLayout(false);
            this.flowLayoutPanel7.ResumeLayout(false);
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Gridimm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridGroupInterv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridGroupImm)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridGroupSecteur)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SSTab2)).EndInit();
            this.SSTab2.ResumeLayout(false);
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel8.PerformLayout();
            this.frame1112.ResumeLayout(false);
            this.frame1112.PerformLayout();
            this.frame118.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatutPDA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateDebPlaningBis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateFinPlaningBis)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.Frame13.ResumeLayout(false);
            this.Frame13.PerformLayout();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssGAvisDEPassage)).EndInit();
            this.groupBox7.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateDebPlaningAvis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateFinPlaningAvis)).EndInit();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.ultraTabPageControl4.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.groupBox8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssGridTachesAnnuelHisto)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssOpeTachesHisto)).EndInit();
            this.groupBox9.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel14.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateVisiteLe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDateVisiteAu)).EndInit();
            this.frame119.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.DTDatePrevueDe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTDateRealiseDe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTDatePrevueAu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTDateRealiseAu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTCreeParDe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEnvoyeLeHisto1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtCreeParAu)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtEnvoyeLeHisto2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatutPDAhisto)).EndInit();
            this.frame1113.ResumeLayout(false);
            this.frame1113.PerformLayout();
            this.flowLayoutPanel6.ResumeLayout(false);
            this.groupBox12.ResumeLayout(false);
            this.groupBox12.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropMatricule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridHisto)).EndInit();
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.ultraTabPageControl8.ResumeLayout(false);
            this.frame1111.ResumeLayout(false);
            this.frame1111.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).EndInit();
            this.SSTab1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        public System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdCleanSimul;
        public System.Windows.Forms.Button cmdAfficheSimul;
        public System.Windows.Forms.Button cmdSimulation;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.CheckBox chkPEI_APlanifier;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtDateDebPlaningBisSimul;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtDateFinPlaningBisSimul;
        public System.Windows.Forms.Label label4;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeubleSimul;
        public iTalk.iTalk_TextBox_Small2 txtIntervenantSimul;
        public iTalk.iTalk_TextBox_Small2 txtCOP_NoautoSimul;
        public System.Windows.Forms.Button cmdRechercheImmeubleSimul;
        public System.Windows.Forms.Button cmdFindStraitantSimul;
        public System.Windows.Forms.Button cmdFindGMAO;
        public System.Windows.Forms.GroupBox frame117;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.RadioButton optEQU_P1CompteurSimul2;
        public System.Windows.Forms.RadioButton optEQU_P1CompteurSimul0;
        public System.Windows.Forms.RadioButton optEQU_P1CompteurSimul1;
        public iTalk.iTalk_TextBox_Small2 lblTotalSimul;
        public System.Windows.Forms.RadioButton OptPrestation;
        public System.Windows.Forms.RadioButton OptIntervention;
        public System.Windows.Forms.Button CmdEditer;
        public System.Windows.Forms.GroupBox groupBox1;
        public System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        public System.Windows.Forms.RadioButton optPlanCharge;
        public System.Windows.Forms.RadioButton optOperationSimul;
        public System.Windows.Forms.Label label6;
        public iTalk.iTalk_TextBox_Small2 txtTotduree;
        private System.Windows.Forms.Panel pctPlanCharge;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        public System.Windows.Forms.RadioButton optPlanChargeChantier;
        public System.Windows.Forms.RadioButton optPlanChargeIntervenant;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        public System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label10;
        public iTalk.iTalk_TextBox_Small2 txtIntervenant;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeuble;
        public iTalk.iTalk_TextBox_Small2 txtCOP_NoAuto;
        public System.Windows.Forms.Button cmdintervenant;
        public System.Windows.Forms.Button cmdRechercheImmeuble;
        public System.Windows.Forms.Button cmdFindGmaoCreat;
        public System.Windows.Forms.GroupBox frame118;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        public System.Windows.Forms.RadioButton OptEQU_P1Compteur2;
        public System.Windows.Forms.RadioButton OptEQU_P1Compteur0;
        public System.Windows.Forms.RadioButton OptEQU_P1Compteur1;
        public iTalk.iTalk_TextBox_Small2 lbltotal2;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        public System.Windows.Forms.Button cmdAffichePlan;
        public System.Windows.Forms.Button cmdCleanVisites;
        public System.Windows.Forms.Button cmdListing;
        public System.Windows.Forms.Button cmdExportPlan;
        public System.Windows.Forms.Button cmdVisites;
        public System.Windows.Forms.Button cdmSend2PDA;
        public System.Windows.Forms.Button CmdSauver;
        public System.Windows.Forms.Button CmdLegende0;
        public System.Windows.Forms.Button Command3;
        public System.Windows.Forms.Label label13;
        public iTalk.iTalk_TextBox_Small2 txtMoisPlanif;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtDateDebPlaningBis;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtDateFinPlaningBis;
        public iTalk.iTalk_TextBox_Small2 dtDateDebPlaning;
        public iTalk.iTalk_TextBox_Small2 dtDateFinPlaning;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl6;
        public System.Windows.Forms.Button cmdCocher;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        public System.Windows.Forms.RadioButton optOperation;
        public System.Windows.Forms.RadioButton optTaches;
        public System.Windows.Forms.Label label12;
        public iTalk.iTalk_RichTextBox txtErreur;
        public System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.Button button16;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        public System.Windows.Forms.Label label14;
        public System.Windows.Forms.Label label15;
        public iTalk.iTalk_TextBox_Small2 txtCOP_NoAutoAvis;
        public System.Windows.Forms.Button Command6;
        public System.Windows.Forms.Button Command5;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeubleAvis;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.Label label17;
        public iTalk.iTalk_TextBox_Small2 txtIntervenantAvis;
        public iTalk.iTalk_TextBox_Small2 lbllibIntervenantAvis;
        public System.Windows.Forms.Button Command7;
        public System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        public System.Windows.Forms.GroupBox groupBox8;
        public System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.Label label21;
        public System.Windows.Forms.Label label22;
        public System.Windows.Forms.Label label23;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtDateVisiteLe;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeubleHisto;
        public iTalk.iTalk_TextBox_Small2 txtMatEntretien;
        public iTalk.iTalk_TextBox_Small2 txtIntervenantHisto;
        public System.Windows.Forms.Button cmdImmeubleHisto;
        public System.Windows.Forms.Button cmdMatEntretien;
        public System.Windows.Forms.Button cmdIntervHisto;
        public System.Windows.Forms.GroupBox frame119;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        public System.Windows.Forms.RadioButton OptEQU_P1CompteurHisto2;
        public System.Windows.Forms.RadioButton OptEQU_P1CompteurHisto0;
        public System.Windows.Forms.RadioButton OptEQU_P1CompteurHisto1;
        public iTalk.iTalk_TextBox_Small2 lblTotalHisto;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        public System.Windows.Forms.Button cmdHistoRamonage;
        public System.Windows.Forms.Button cmdinitHistorique;
        public System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        public System.Windows.Forms.RadioButton optTachesHisto;
        public System.Windows.Forms.RadioButton optOperationHisto;
        public System.Windows.Forms.Button cmdRapportPDA;
        public System.Windows.Forms.Button CmdLegende2;
        public System.Windows.Forms.Button Command2;
        public System.Windows.Forms.Button cmdExportHisto;
        public System.Windows.Forms.Label label31;
        public System.Windows.Forms.Label label25;
        public System.Windows.Forms.Label label26;
        public System.Windows.Forms.Label label27;
        public System.Windows.Forms.Label label28;
        public System.Windows.Forms.Label label29;
        public System.Windows.Forms.Label label30;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor DTDatePrevueDe;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor DTDateRealiseDe;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor DTDatePrevueAu;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor DTDateRealiseAu;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor DTCreeParDe;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEnvoyeLeHisto1;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtCreeParAu;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtEnvoyeLeHisto2;
        public System.Windows.Forms.Label label32;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.Label label34;
        public System.Windows.Forms.Label label35;
        public iTalk.iTalk_TextBox_Small2 txtNoInterventionHisto;
        public iTalk.iTalk_TextBox_Small2 txtCode1Histo;
        public System.Windows.Forms.Button cmdFindNoInter;
        public System.Windows.Forms.Button cmdClient;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtDateDebPlaningAvis;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtDateFinPlaningAvis;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtDateVisiteAu;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.RadioButton optMJ0;
        public System.Windows.Forms.RadioButton optJH0;
        public System.Windows.Forms.RadioButton optAM0;
        public System.Windows.Forms.RadioButton optMS0;
        public System.Windows.Forms.GroupBox groupBox10;
        public System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        public System.Windows.Forms.Button cmdTaillePlus;
        public System.Windows.Forms.Button cmdTailleMoins;
        public System.Windows.Forms.Label label37;
        public System.Windows.Forms.Label label36;
        public iTalk.iTalk_TextBox_Small2 Text2;
        public System.Windows.Forms.GroupBox Frame13;
        public iTalk.iTalk_TextBox_Small2 txtStatutHisto;
        public System.Windows.Forms.Button cmdRechercheContrat;
        public System.Windows.Forms.CheckBox chkDevisEtablir;
        public System.Windows.Forms.CheckBox chkMatRemplace;
        public System.Windows.Forms.CheckBox chkTrxUrgent;
        public System.Windows.Forms.CheckBox chkAnomalie;
        public System.Windows.Forms.CheckBox chkCommGardien;
        public System.Windows.Forms.CheckBox chkInterIncompléte;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        public System.Windows.Forms.GroupBox frame1110;
        public System.Windows.Forms.Label label41;
        public System.Windows.Forms.Label label40;
        public System.Windows.Forms.Label label39;
        public System.Windows.Forms.Label label38;
        public iTalk.iTalk_TextBox_Small2 txtCOP_AvenantSimul;
        public iTalk.iTalk_TextBox_Small2 txtCOP_NoContratSimul;
        public iTalk.iTalk_TextBox_Small2 txtCAI_CodeSimul;
        public iTalk.iTalk_TextBox_Small2 txtPrestationSimul;
        public System.Windows.Forms.Button cmdPrestationSimul;
        public System.Windows.Forms.Button cmdMotifSimul;
        public System.Windows.Forms.Button cmdRechercheContratSimul;
        public System.Windows.Forms.GroupBox frame1111;
        public iTalk.iTalk_TextBox_Small2 txtCOP_NoContrat;
        public System.Windows.Forms.Label label42;
        public System.Windows.Forms.Label label43;
        public System.Windows.Forms.GroupBox frame115;
        public System.Windows.Forms.CheckBox chkLecture;
        public iTalk.iTalk_TextBox_Small2 lblLibIntervSecteur;
        public iTalk.iTalk_TextBox_Small2 txtUtilVerrou;
        public System.Windows.Forms.Label label45;
        public System.Windows.Forms.Button cmdIntervSecteur;
        public iTalk.iTalk_TextBox_Small2 txtIntervSecteur;
        public iTalk.iTalk_TextBox_Small2 txtPeriode;
        public System.Windows.Forms.RadioButton optVisites;
        public System.Windows.Forms.RadioButton optListeTaches;
        private AxPLANNINGLib.AxPlanning PL1;
        public System.Windows.Forms.Button Cmd18;
        public System.Windows.Forms.Button Cmd17;
        public System.Windows.Forms.RadioButton optTachesSimul;
        public System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        public iTalk.iTalk_TextBox_Small2 Text10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        public System.Windows.Forms.Button cmdAppliquer0;
        public iTalk.iTalk_TextBox_Small2 lbllibIntervenantSimul;
        public iTalk.iTalk_TextBox_Small2 lbllibIntervenant;
        public iTalk.iTalk_TextBox_Small2 lblLibEntretien;
        public iTalk.iTalk_TextBox_Small2 lbllibIntervenantHisto;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl SSTab1;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinGrid.UltraGrid SSOleDBGrid1;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssGridVisiteSimul;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssGridTachesAnnuelSimul;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssOpTachesSimul;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssPlanCharge;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbStatutPDA;
        private Infragistics.Win.UltraWinTabControl.UltraTabControl SSTab2;
        private Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl5;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssGridVisite;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssGridTachesAnnuel;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssOpeTaches;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssGAvisDEPassage;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssOpeTachesHisto;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssGridHisto;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssGridTachesAnnuelHisto;
        private Infragistics.Win.UltraWinGrid.UltraCombo cmbStatutPDAhisto;
        private Infragistics.Win.UltraWinGrid.UltraCombo ssDropMatricule;
        private System.Windows.Forms.FlowLayoutPanel frame1113;
        public System.Windows.Forms.RadioButton Opt2;
        public System.Windows.Forms.RadioButton Opt1;
        public System.Windows.Forms.RadioButton Opt0;
        private System.Windows.Forms.FlowLayoutPanel frame1112;
        public System.Windows.Forms.RadioButton Opt3;
        public System.Windows.Forms.RadioButton Opt5;
        public System.Windows.Forms.RadioButton Opt4;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.Button cmdDebloquer;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        public System.Windows.Forms.Label label47;
        public System.Windows.Forms.Label label46;
        public System.Windows.Forms.Label label44;
        public System.Windows.Forms.Label label11;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl8;
        public System.Windows.Forms.Button cmd_3;
        public System.Windows.Forms.Button cmd_2;
        public System.Windows.Forms.Button cmd_5;
        public System.Windows.Forms.Button cmd_4;
        private iTalk.iTalk_TextBox_Small2 txt_1;
        private iTalk.iTalk_TextBox_Small2 txt_0;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        private iTalk.iTalk_TextBox_Small2 txt_3;
        private iTalk.iTalk_TextBox_Small2 txt_2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        private iTalk.iTalk_TextBox_Small2 txt_5;
        private iTalk.iTalk_TextBox_Small2 txt_4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private iTalk.iTalk_TextBox_Small2 Label56_25;
        private System.Windows.Forms.GroupBox groupBox13;
        public System.Windows.Forms.RadioButton Opt_8;
        public System.Windows.Forms.RadioButton Opt_7;
        public System.Windows.Forms.RadioButton Opt_6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        public System.Windows.Forms.Button cmd_7;
        public System.Windows.Forms.Button cmd_6;
        public System.Windows.Forms.Button cmd_1;
        private iTalk.iTalk_TextBox_Small2 Label56_24;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridGroupSecteur;
        private System.Windows.Forms.Panel panel1;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridGroupInterv;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridGroupImm;
        public Infragistics.Win.UltraWinGrid.UltraGrid Gridimm;
        private iTalk.iTalk_TextBox_Small2 txt_6;
        private System.Windows.Forms.CheckBox Check_0;
    }
}
