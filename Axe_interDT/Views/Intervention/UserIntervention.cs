﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Analytique;
using Axe_interDT.Views.Appel;
using Axe_interDT.Views.Appel.Forms;
using Axe_interDT.Views.BaseDeDonnees.Gerant;
using Axe_interDT.Views.BaseDeDonnees.Immeuble;
using Axe_interDT.Views.Commande.UserPreCommande;
using Axe_interDT.Views.Contrat;
using Axe_interDT.Views.Devis;
using Axe_interDT.Views.Devis.Forms;
using Axe_interDT.Views.Devis.Historique_Devis;
using Axe_interDT.Views.DispatchIntervention;
using Axe_interDT.Views.DispatchIntervention.HistoriqueIntervention;
using Axe_interDT.Views.Intervention.Forms;
using Axe_interDT.Views.Les_Articles;
using Axe_interDT.Views.SharedViews;
using Axe_interDT.Views.Theme.CustomMessageBox;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Text.RegularExpressions;
using System.Windows.Forms;

namespace Axe_interDT.Views.Intervention
{
    public partial class UserIntervention : UserControl
    {

        DataTable Adodc6 = null;
        ModAdo modAdoAdodc6 = null;
        bool showAlerteSuccess = false;
        const string cREGIE = "1";
        const string cDEVIS = "2";
        const string cCONTRAT = "3";
        const string cSAISON = "4";
        const string cAFSC = "AFSC";
        const string cAFI = "AFI";
        const string cProduit = "5";
        const string cNOBON = "le type d'intervention que vous avez selectionné ne permet pas la création de bons de commande";
        bool blnEntree;
        const uint CW_USEDEFAULT = 0x80000000;

        int LongNoIntervention;
        int longNumficheStandard;
        string strArticle;
        /// Mondir - If You Gonna Use This boolAutorise, Must Test A condition In SaisIntervention() Function
        bool boolAutorise { get; set; }
        int longNombreInterv;

        int longJobNumber;
        bool boolPro;
        string strphrase;
        string sUtilisateur;


        object fso;

        string[] tSqlCrystal;

        Color cFindColor = Color.FromArgb(34, 135, 134);
        Color cNotFoundColor1 = Color.FromArgb(55, 84, 96);
        Color cNotFoundColor2 = Color.FromArgb(85, 115, 128);

        const string cZZ = "ZZ";

        System.DateTime dtLoad;

        int lEtatEnvoie;

        const int cRose = 0xc0c0ff;
        // Custom Fields By Ahmed

        // Fin Custom fields
        private DataTable dtInterventionsByAppel = new DataTable();
        private int pos = 0;
        public UserIntervention()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="adoSaisie"></param>
        private void SaisIntervention(ModAdo adoSaisie)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"SaisIntervention() - Enter In The Function - adoSaisie.rsAdo.Rows.Count = {adoSaisie?.rsAdo?.Rows?.Count}");
            //===> Fin Modif Mondir
            //-----récupére les champs de la table intervention
            int lngNbFact = 0;
            DataTable rsNbFact = default(DataTable);
            ModAdo modAdorsNbFact = null;
            try
            {

                var _with1 = adoSaisie.rsAdo;
                LongNoIntervention = Convert.ToInt32(_with1.Rows[0]["NoIntervention"] + "");
                txtNoIntervention.Text = _with1.Rows[0]["NoIntervention"] + "";
                txtArticle.Text = _with1.Rows[0]["Article"] + "";
                strArticle = _with1.Rows[0]["Article"] + "";
                txtDesignation.Text = _with1.Rows[0]["Designation"] + "";
                txtSousArticle.Text = _with1.Rows[0]["SousArticle"] + "";
                txtCommentaire.Text = _with1.Rows[0]["Commentaire"] + "";

                txtTelephoneContact.Text = General.nz(_with1.Rows[0]["TelephoneContact"], "").ToString();

                if (txtCommentaire.Text.Length > 81)
                {
                    txtCommentaire.iTalkTB.Select(81, txtCommentaire.Text.Length - 81);
                }

                //txtCommentaire2 = !Commentaire2 & ""
                //txtCommentaire3 = !Commentaire3 & ""
                txtSituation.Text = _with1.Rows[0]["situation"] + "";
                chkVisiteEntretien.CheckState = General.nz(_with1.Rows[0]["VisiteEntretien"], 0).ToString() == "1" ? CheckState.Checked : CheckState.Unchecked;

                ComboTYMO_ID.Text = _with1.Rows[0]["TYMO_ID"] + "";

                if (_with1.Rows[0]["Wave"] == DBNull.Value || string.IsNullOrEmpty(_with1.Rows[0]["Wave"].ToString()))
                {
                    cmdWave.BackColor = cNotFoundColor1;
                    txtWave.Text = "";
                }
                else
                {
                    cmdWave.BackColor = cFindColor;
                    txtWave.Text = _with1.Rows[0]["Wave"] + "";
                }

                modAdorsNbFact = new ModAdo();
                rsNbFact = modAdorsNbFact.fc_OpenRecordSet("SELECT DISTINCT NoFacture FROM Intervention WHERE NumFicheStandard=" + txtNumFicheStandard.Text +
                    " AND NoFacture<>'' AND NOT NoFacture IS NULL AND NOT LEFT(NoFacture,2)='XX'");
                lngNbFact = 0;
                if (rsNbFact.Rows.Count > 0)
                {
                    foreach (DataRow rsNbFactRow in rsNbFact.Rows)
                    {
                        txtNoFacture.Text = rsNbFactRow["NoFacture"] + "";
                        lngNbFact = lngNbFact + 1;
                    }
                }
                modAdorsNbFact.Close();
                rsNbFact = null;
                if (lngNbFact >= 1)
                {
                    btnFacture.BackColor = cFindColor;
                    if (lngNbFact > 1)
                    {
                        btnFacture.Text = lngNbFact + " Factures";
                    }
                    else
                    {
                        btnFacture.Text = "1 Facture";
                    }
                }
                else
                {
                    btnFacture.BackColor = cNotFoundColor2;
                    btnFacture.Text = "Facture";
                }


                //             txtNoFacture.Text = !NoFacture & ""
                if (_with1.Rows[0]["Courrier"].ToString() != "" && Convert.ToBoolean(_with1.Rows[0]["Courrier"]) == true)
                {
                    //lblVisu.ForeColor = &HFF0000
                }
                else
                {
                    //lblVisu.ForeColor = &H0&
                }
                //si le recordset
                if (boolAutorise == true)
                {
                    //envoye en paramétre est de type Pessimistic
                    //ou optimistic
                    if (_with1.Rows[0]["Intervenant"] == DBNull.Value || string.IsNullOrEmpty(_with1.Rows[0]["Intervenant"].ToString()))
                    {
                        _with1.Rows[0]["Intervenant"] = "000";
                        adoSaisie.Update();
                        txtIntervenant.Text = "000";
                        txtMatriculeIntervenant.Text = "000";
                    }
                    else
                    {
                        txtIntervenant.Text = _with1.Rows[0]["Intervenant"] + "";
                        txtMatriculeIntervenant.Text = _with1.Rows[0]["Intervenant"] + "";
                    }
                    boolAutorise = false;
                }
                else
                {
                    txtIntervenant.Text = _with1.Rows[0]["Intervenant"] + "";
                    txtMatriculeIntervenant.Text = _with1.Rows[0]["Intervenant"] + "";
                }
                txtDateRealise.Text = _with1.Rows[0]["DateRealise"] + "";
                txtDateSaisie.Text = _with1.Rows[0]["DateSaisie"] != DBNull.Value ? Convert.ToDateTime(_with1.Rows[0]["DateSaisie"]).ToString() : "" + "";
                txtDatePrevue.Text = _with1.Rows[0]["DatePrevue"] != DBNull.Value ? Convert.ToDateTime(_with1.Rows[0]["DatePrevue"]).ToShortDateString() : "" + "";
                txt16.Text = General.nz(_with1.Rows[0]["DureePrevue"], "").ToString();

                txtDateVisite.Text = _with1.Rows[0]["DateVisite"] != DBNull.Value ? Convert.ToDateTime(_with1.Rows[0]["DateVisite"]).ToShortDateString() : "" + "";

                if (string.IsNullOrEmpty(General.nz(_with1.Rows[0]["Duree"], "").ToString()))
                {
                    txtDuree.Text = "  :  :  ";
                }
                else
                {
                    if (General.IsDate(_with1.Rows[0]["Duree"].ToString()))
                        txtDuree.Text = Convert.ToDateTime(_with1.Rows[0]["Duree"]).ToString("HH:mm:ss");
                }


                if (string.IsNullOrEmpty(General.nz(_with1.Rows[0]["HeureDebut"], "").ToString()))
                {
                    txtDebut.Text = "  :  :  ";
                }
                else
                {
                    if (General.IsDate(_with1.Rows[0]["HeureDebut"].ToString()))
                        txtDebut.Text = Convert.ToDateTime(_with1.Rows[0]["HeureDebut"]).ToString("HH:mm:ss");
                }

                if (string.IsNullOrEmpty(General.nz(_with1.Rows[0]["HeureFin"], "").ToString()))
                {
                    txtFin.Text = "  :  :  ";
                }
                else
                {
                    if (General.IsDate(_with1.Rows[0]["HeureFin"].ToString()))
                        txtFin.Text = Convert.ToDateTime(_with1.Rows[0]["HeureFin"]).ToString("HH:mm:ss");
                }


                if (string.IsNullOrEmpty(General.nz(_with1.Rows[0]["HeurePrevue"], "").ToString()))
                {
                    txtHeurePrevue.Text = "  :  ";
                }
                else
                {
                    if (General.IsDate(_with1.Rows[0]["HeurePrevue"].ToString()))
                        txtHeurePrevue.Text = Convert.ToDateTime(_with1.Rows[0]["HeurePrevue"]).ToString("HH:mm");
                }


                if (string.IsNullOrEmpty(General.nz(_with1.Rows[0]["heureDebutP"], "").ToString()))
                {
                    txtheureDebutP.Text = "  :  ";
                }
                else
                {
                    if (General.IsDate(_with1.Rows[0]["heureDebutP"].ToString()))
                        txtheureDebutP.Text = Convert.ToDateTime(_with1.Rows[0]["heureDebutP"]).ToString("HH:mm");
                }

                txtDebourse.Text = _with1.Rows[0]["Debourse"] + "";

                txtDeplacement.Text = _with1.Rows[0]["Deplacement"] + "";
                txtCodeEtat.Text = _with1.Rows[0]["CodeEtat"] + "";
                txtDispatcheur.Text = _with1.Rows[0]["dispatcheur"] + "";
                if (_with1.Rows[0]["LivraisonFioul"].ToString() != "" && Convert.ToBoolean(_with1.Rows[0]["LivraisonFioul"]) == true)
                {
                    CheckFioul.CheckState = System.Windows.Forms.CheckState.Checked;
                }
                else
                {
                    CheckFioul.CheckState = System.Windows.Forms.CheckState.Unchecked;
                }

                if (General.nz(_with1.Rows[0]["CompteurObli"], 0).ToString() == "1")
                {
                    chkCompteurObli.CheckState = System.Windows.Forms.CheckState.Checked;
                }
                else
                {
                    chkCompteurObli.CheckState = System.Windows.Forms.CheckState.Unchecked;
                }

                if (General.nz(_with1.Rows[0]["Urgence"], 0).ToString() == "1")
                {
                    chkUrgence.CheckState = System.Windows.Forms.CheckState.Checked;
                }
                else
                {
                    chkUrgence.CheckState = System.Windows.Forms.CheckState.Unchecked;
                }

                if (_with1.Rows[0]["Sinistre"].ToString() != "" && Convert.ToBoolean(_with1.Rows[0]["Sinistre"]) == true)
                {
                    CheckSinistre.CheckState = System.Windows.Forms.CheckState.Checked;
                }
                else
                {
                    CheckSinistre.CheckState = System.Windows.Forms.CheckState.Unchecked;
                }

                if (_with1.Rows[0]["TrvxParticulier"].ToString() != "" && Convert.ToBoolean(_with1.Rows[0]["TrvxParticulier"]) == true)
                {
                    CheckTrvx.CheckState = System.Windows.Forms.CheckState.Checked;
                }
                else
                {
                    CheckTrvx.CheckState = System.Windows.Forms.CheckState.Unchecked;
                }

                if (_with1.Rows[0]["Vidange"].ToString() != "" && Convert.ToBoolean(_with1.Rows[0]["Vidange"]) == true)
                {
                    CheckVidange.CheckState = System.Windows.Forms.CheckState.Checked;
                }
                else
                {
                    CheckVidange.CheckState = System.Windows.Forms.CheckState.Unchecked;
                }

                chk0.CheckState = General.nz(_with1.Rows[0]["AvisDePassage"].ToString(), 0).ToString() == "1" ? CheckState.Checked : CheckState.Unchecked;

                using (var tmpModAdo = new ModAdo())
                    if (tmpModAdo.fc_ADOlibelle("SELECT TOP 1 NumFicheStandard FROM BonDeCommande WHERE NumFicheStandard=" + txtNumFicheStandard.Text + "") == txtNumFicheStandard.Text)
                    {
                        btnAchat.BackColor = cFindColor;
                        //lblGoAchat.Enabled = True
                    }
                    else
                    {
                        btnAchat.BackColor = cNotFoundColor1;
                        Label17.Enabled = true;
                    }

                //             If !Achat = True Then
                //               btnAchat.BackColor = cFindColor
                //             Else
                //                'lblAchat.ForeColor = &H0&
                //                btnAchat.BackColor = &HFFC0C0
                //             End If
                this.chkSansPauseDejeuner.CheckState = General.nz(_with1.Rows[0]["SansPauseDejeuner"], 0).ToString() == "1" ? CheckState.Checked : CheckState.Unchecked;
                if (_with1.Rows[0]["acompte"] != DBNull.Value)
                {
                    txtAcompte.Text = _with1.Rows[0]["acompte"].ToString();
                }
                else
                {
                    txtAcompte.Text = "";
                }
                txt0.Text = General.nz(_with1.Rows[0]["RecuLe"], "").ToString();
                txt1.Text = General.nz(_with1.Rows[0]["ArrivePrevue"], "").ToString();

                if (_with1.Rows[0]["NoDevis"] != DBNull.Value)
                {
                    btnDebourse.BackColor = cFindColor;
                    txtNoDevis.Text = _with1.Rows[0]["NoDevis"] + "";
                }
                else
                {
                    btnDebourse.BackColor = System.Drawing.ColorTranslator.FromOle(0xffc0c0);
                    txtNoDevis.Text = "";
                }

                fc_InitMajPDA();

                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, txtNoIntervention.Text);

                //== modif du 06/09/01
                // Set rs2 = New ADODB.Recordset
                //== recherxche ddu jobUmber
                //SQL = "SELECT INFORM.JobNumber" _
                //& " From INFORM" _
                //& " WHERE INFORM.NumIntervention=" & adoSaisie!Nointervention
                //rs2.Open SQL, adocnn
                //If rs2.EOF Then
                //    txtJobNumber = ""
                //Else
                //    txtJobNumber = rs2!jobNumber
                //End If
                //rs2.Close

                //Set rs2 = Nothing
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SaisIntervention");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_InitMajPDA()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_InitMajPDA() - Enter In The Function");
            //===> Fin Modif Mondir
            //=== code état.
            txt4.Text = txtCodeEtat.Text;
            //=== designation.
            txt5.Text = txtDesignation.Text;
            //=== commentaire
            txt6.Text = txtCommentaire.Text;
            //=== intervenant.
            txt9.Text = txtIntervenant.Text;
            //=== heure prevue.
            txt10.Text = txtHeurePrevue.Text;
            //=== Date réalisée.
            txt11.Text = txtDateRealise.Text;
            //== date prevue.
            txt12.Text = txtDateVisite.Text;
            //=== heure prevue.
            txt13.Text = txtHeurePrevue.Text;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnAchat_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"btnAchat_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            bool bcontrole = false;
            object[] tabSetting = new object[2];
            object[] tabTemp = new object[1];

            string MaxOrdre = null;
            int cleAutoFNS = 0;

            // attente fin de saison de chauffe interdit la creation des bons.
            if (txtCodeEtat.Text == cAFSC)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le statut " + txtCodeEtat.Text + " ne vous permet pas de créer" + " un bon de commande.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return;
            }

            //'' === modif du 23 07 2019

            //'If txtNoDevis <> "" Then

            //'        If fc_CtrResponsableTravaux(txtNoDevis, 0) = False Then

            //'                Exit Sub

            //'        End If

            //'End If


            bcontrole = false;
            if (fc_sauver(ref bcontrole) == true)
            {
                using (var tmpModAdo = new ModAdo())
                    MaxOrdre = tmpModAdo.fc_ADOlibelle("SELECT MAX(Ordre) as MaxOrdre FROM BonDeCommande WHERE NumFicheStandard=" + txtNumFicheStandard.Text);

                //==Fonction en cours de developpement.
                if (General.sPrecommande == "1")
                {

                    if (string.IsNullOrEmpty(MaxOrdre))
                    {
                        using (var tmpModAdo = new ModAdo())
                            if (string.IsNullOrEmpty(tmpModAdo.fc_ADOlibelle("SELECT NoDevis FROM GestionStandard WHERE NumFicheStandard=" + txtNumFicheStandard.Text)))
                            {

                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention, cette intervention ne fait pas suite à un devis." + "\n" +
                                    "Vous ne pouvez pas acheter pour plus de " + General.nz(tmpModAdo.fc_ADOlibelle("SELECT Personnel.PouvoirAchat FROM Personnel " +
                                    " WHERE Login='" + StdSQLchaine.gFr_DoublerQuote(sUtilisateur) + "'"), General.gfr_liaison("MontantMaxAchats", "100")) +
                                    " € sans autorisation.", "Achat sans devis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                    }

                    General.saveInReg(Variable.cUserPreCommande, "Origine", this.Name);
                    General.saveInReg(Variable.cUserPreCommande, "NewVar", txtNoIntervention.Text);
                    General.saveInReg(Variable.cUserPreCommande, "NumFicheStandard", txtNumFicheStandard.Text);
                    View.Theme.Theme.Navigate(typeof(UserPreCommande));

                    //Se positionne sur le dernier achat réalisé
                }
                else if (!string.IsNullOrEmpty(MaxOrdre) && MaxOrdre != "0")
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        cleAutoFNS = Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT CodeFourn FROM BonDeCommande WHERE NumFicheStandard=" + txtNumFicheStandard.Text + " AND Ordre=" + MaxOrdre), 0));
                        General.saveInReg("UserBCmdHead", "NoBCmd", tmpModAdo.fc_ADOlibelle("SELECT NoBonDeCommande FROM BonDeCommande WHERE NumFicheStandard=" + txtNumFicheStandard.Text + " AND Ordre=" + MaxOrdre));
                        General.saveInReg("UserBCmdHead", "NoInt", tmpModAdo.fc_ADOlibelle("SELECT NoIntervention FROM BonDeCommande WHERE NumFicheStandard=" + txtNumFicheStandard.Text + " AND Ordre=" + MaxOrdre));
                        General.saveInReg("UserBCmdHead", "NoImmeuble", txtCodeImmeuble.Text);
                        General.saveInReg("UserBCmdHead", "NoIntAchat", txtNoIntervention.Text);
                        General.saveInReg("UserBCmdHead", "NoFNS", tmpModAdo.fc_ADOlibelle("SELECT Code FROM FournisseurArticle WHERE Cleauto=" + cleAutoFNS));
                        //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUderBCmdBody);
                    }
                    //Achat inexistant : Se positionne en création de bon de commande
                }
                else
                {
                    General.saveInReg("UserBCmdHead", "NoInt", txtNoIntervention.Text);
                    General.saveInReg("UserBCmdHead", "NoIntAchat", txtNoIntervention.Text);
                    General.saveInReg("UserBCmdHead", "NoImmeuble", txtCodeImmeuble.Text);
                    General.saveInReg("UserBCmdHead", "NoBcmd", "");
                    using (var tmpModAdo = new ModAdo())
                        if (string.IsNullOrEmpty(tmpModAdo.fc_ADOlibelle("SELECT NoDevis FROM GestionStandard WHERE NumFicheStandard=" + txtNumFicheStandard.Text)))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention, cette intervention ne fait pas suite à un devis." + "\n" + "Vous ne pouvez pas acheter pour plus de " +
                                General.nz(tmpModAdo.fc_ADOlibelle("SELECT Personnel.PouvoirAchat FROM Personnel WHERE Login='" + StdSQLchaine.gFr_DoublerQuote(sUtilisateur) + "'"),
                                General.gfr_liaison("MontantMaxAchats", "100")) + " € sans autorisation.", "Achat sans devis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserBCmdHead);
                }
            }
        }

        /// <summary>
        /// Mondir le 03.07.2020, This function will check if CodeImmeuble in GestionStandard is diff than CodeImmeuble In Intevention
        /// Probleme found from https://groupe-dt.mantishub.io/view.php?id=1892
        /// </summary>
        /// <returns></returns>
        private bool ValidateCodeImmeuble()
        {
            if (General._ExecutionMode == General.ExecutionMode.Prod)
                return true;

            var numFicheStandard = txtNumFicheStandard.Text;
            var numIntervention = txtNoIntervention.Text;

            using (var tmpModAdo = new ModAdo())
            {
                var codeImmeubleGestionStandard =
                    tmpModAdo.fc_ADOlibelle($"SELECT CodeImmeuble FROM GestionStandard WHERE NumFicheStandard = '{numFicheStandard}'");
                var codeImmeubleIntervention = tmpModAdo.fc_ADOlibelle($"SELECT CodeImmeuble FROM Intervention WHERE NoIntervention = '{numIntervention}'");
                if (codeImmeubleGestionStandard.ToUpper() != txtCodeImmeuble.Text.ToUpper() ||
                    codeImmeubleGestionStandard.ToUpper() != codeImmeubleIntervention.ToUpper())
                {
                    CustomMessageBox.Show("Code Immeuble sur la fiche Intervention et sur la fiche appel est différent\nClickez sur OK pour charger la fiche", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    fc_ChargeEnregistrement(txtNoIntervention.Text);
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="bcontrole"></param>
        /// <returns></returns>
        private bool fc_sauver(ref bool bcontrole)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_sauver() - Enter In The Function - bcontrole = {bcontrole}");
            //===> Fin Modif Mondir
            bool functionReturnValue = false;

            bool boolVerifCodeEtat = false;
            string strCodeEtatReel = null;
            string sMaxNoMaj = null;

            try
            {

                functionReturnValue = true;

                //===> Mondir : look at ValidateCodeImmeuble
                if (!ValidateCodeImmeuble())
                {
                    return functionReturnValue;
                }
                //===> Fin Modif Mondir

                if (fc_CrtlDonneeConccurente() == true)
                {
                    fc_ChargeEnregistrement(txtNoIntervention.Text);
                    return functionReturnValue;
                }

                if (string.IsNullOrEmpty(txtNoIntervention.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune intervention en cours.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return functionReturnValue;
                }

                if (string.IsNullOrEmpty(txtArticle.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le type d'opération est obligatoire.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    functionReturnValue = false;
                    return functionReturnValue;
                }

                if (string.IsNullOrEmpty(txtDispatcheur.Text))
                {
                    bcontrole = true;
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le dispatcheur est obligatoire.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = false;
                    return functionReturnValue;
                }

                //=== modif du 19 05 2016.
                if (!string.IsNullOrEmpty(txtTelephoneContact.Text))
                {
                    if (!General.IsNumeric(txtTelephoneContact.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le numéro de téléphone est de format incorrect, celui doit être numérique et sous la forme 0607080910.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        functionReturnValue = false;
                        return functionReturnValue;
                    }
                }

                //=== modif du 19 06 2015, le statut ZZ ne peut pas être affecté à une intervention liées à un devis.
                //=== cette modif sera prise en compte à partir d'une date défini.
                if (General.IsDate(txtDateSaisie.Text) && Convert.ToDateTime(txtDateSaisie.Text) >= Convert.ToDateTime("19/06/2015"))
                {
                    if (btnDevis.BackColor == cFindColor)
                    {
                        if (txtCodeEtat.Text.ToUpper() == cZZ)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas affecter un code état ZZ à une intervention liée à un devis.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            functionReturnValue = false;
                            return functionReturnValue;
                        }

                    }
                }

                //Mondir 19.05.2020 : https://groupe-dt.mantishub.io/view.php?id=1775
                //Mondir 19.06.2020 : Check If The User Wannt To Chnage CodeEtat To D, And The Old CodeEtat Is Not D, And The Devis Is T
                using (var tmpModAdo = new ModAdo())
                    if (txtCodeEtat.Text.ToLower() == "d" && tmpModAdo.fc_ADOlibelle($"SELECT CodeEtat FROM Intervention WHERE NoIntervention = '{txtNoIntervention.Text}'").ToLower() != "d")
                    {
                        using (var tmpMoAdo = new ModAdo())
                        {
                            var etatDevis =
                                tmpMoAdo.fc_ADOlibelle($"SELECT CodeEtat FROM DevisEnTete WHERE NumeroDevis = '{txtNoDevis.Text}'");
                            if (etatDevis.ToLower() == "t")
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas changer le code état en D sur les interventions liées à un devis dont le Statut est T",
                                    "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                functionReturnValue = false;
                                return functionReturnValue;
                            }
                        }
                    }
                //if (General.IsDate(txtDateRealise.Text) && Convert.ToDateTime(txtDateRealise.Text).Date < DateTime.Now.Date)
                //{
                //    if (CustomMessageBox.Show("Vous avez choisi une date dans le passé, voulez-vous continuer?", "", MessageBoxButtons.YesNo) == DialogResult.No)
                //    {
                //        functionReturnValue = false;
                //        return functionReturnValue;
                //    }
                //}

                //        If txtINT_AnaCode = "" Then
                //            MsgBox "Le code analytique est obligatoire.", vbCritical, ""
                //            fc_sauver = False
                //            Exit Function
                //        End If
                //        If ssComboActivite = "" Then
                //            MsgBox "L'activité est obligatoire.", vbCritical, ""
                //            fc_sauver = False
                //            Exit Function
                //        End If

                var _with18 = this;
                //                If .OptContrat = False And .Optdevis = False And .optRegie = False _
                //'                And .OptSaison = False And optProduit = False Then
                //                    MsgBox "La nature de l' intervention est obligatoire.", vbCritical, ""
                //                    fc_sauver = False
                //                    Exit Function
                //                End If

                if (string.IsNullOrEmpty(txtCodeEtat.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code état de l'intervention est obligatoire", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    functionReturnValue = false;
                    return functionReturnValue;
                }

                using (var tmpModAdo = new ModAdo())
                    strCodeEtatReel = General.nz(tmpModAdo.fc_ADOlibelle("SELECT CodeEtat FROM Intervention " + " WHERE NoIntervention=" + txtNoIntervention.Text + ""), "").ToString();

                if (strCodeEtatReel.ToUpper() != General.cEtatZZ.ToUpper() && txtCodeEtat.Text.ToUpper() == General.cEtatZZ.ToUpper())
                {
                    using (var tmpModAdo = new ModAdo())
                        if (!string.IsNullOrEmpty(tmpModAdo.fc_ADOlibelle("Select NoBonDeCommande FROM BonDeCommande " + " WHERE NoIntervention =" + General.nz(txtNoIntervention.Text, 0))))
                        {

                            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention Vous allez passer en " + General.cEtatZZ + " une intervention " + " qui est attachée à un bon de commande. Voulez vous continuer?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            {
                                txtCodeEtat.Text = strCodeEtatReel;

                            }
                        }
                }

                //=== modif du 19 06 2015, si le  code état de l'intervention
                //=== avant sa mise à jour est égale à une chaine vide alors on considére
                //=== que l'intervention vient d'étre crée.
                if (string.IsNullOrEmpty(strCodeEtatReel))
                {
                    strCodeEtatReel = txtCodeEtat.Text;
                }

                switch (txtCodeEtat.Text)
                {

                    case "00":
                        if (strCodeEtatReel == "00")
                        {
                            boolVerifCodeEtat = true;
                        }
                        else
                        {
                            boolVerifCodeEtat = false;
                        }
                        break;

                    case "01":
                        if (strCodeEtatReel == "01" || strCodeEtatReel == "00")
                        {
                            boolVerifCodeEtat = true;
                        }
                        else
                        {
                            boolVerifCodeEtat = false;
                        }
                        break;

                    case "03":
                        if (strCodeEtatReel == "03" || strCodeEtatReel == "00" || strCodeEtatReel == "0A" || strCodeEtatReel == "01")
                        {
                            boolVerifCodeEtat = true;
                        }
                        else
                        {
                            boolVerifCodeEtat = false;
                        }
                        break;

                    case "04":
                        if (strCodeEtatReel == "04" || strCodeEtatReel == "00" || strCodeEtatReel == "0A" || strCodeEtatReel == "03" || strCodeEtatReel == "01")
                        {
                            boolVerifCodeEtat = true;
                        }
                        else
                        {
                            boolVerifCodeEtat = false;
                        }
                        break;

                    case "AF":
                        if (strCodeEtatReel == "AF" || strCodeEtatReel == "04")
                        {
                            boolVerifCodeEtat = true;
                        }
                        else
                        {
                            boolVerifCodeEtat = false;
                        }
                        break;

                    case "ZZ":
                        if (strCodeEtatReel == "ZZ" || strCodeEtatReel == "04" || strCodeEtatReel == "00")
                        {
                            boolVerifCodeEtat = true;
                        }
                        else
                        {
                            boolVerifCodeEtat = false;
                        }
                        break;

                    case "D":
                        if (strCodeEtatReel == "D" || strCodeEtatReel == "04")
                        {
                            boolVerifCodeEtat = true;
                        }
                        else
                        {
                            boolVerifCodeEtat = false;
                        }
                        break;
                    default:
                        boolVerifCodeEtat = true;
                        break;
                        //            Case "S"
                        //
                        //            Case "T"

                }


                //===> Mondir le 01.10.2020, https://groupe-dt.mantishub.io/view.php?id=1587
                var desabledStatut = new[] { "0a", "01", "02", "03" };
                if (desabledStatut.Contains(txtCodeEtat.Text.ToLower()))
                {
                    if (txt9.Text.ToUpper() != txtIntervenant.Text.ToUpper())
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas modifier le code d'intervenant des interventions dont le statut est 0A, 01, 02, 03", "Opération non autorisée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtIntervenant.Text = txt9.Text;
                        bcontrole = true;
                        return functionReturnValue;
                    }
                }
                //===> Fin Modif Mondir

                //===> Mondir le 26.03.2021, moved this code from fc_CtrlDataPDA() ===> https://groupe-dt.mantishub.io/view.php?id=2376
                //=== controle si le statut a été modifié.
                var sOldCodeEtat = txt4.Text;
                var bSendPda = false;
                if (sOldCodeEtat.ToUpper() != txtCodeEtat.Text.ToUpper())
                {

                    if (CModCodeEtat.fc_CtrlCodeEtat(sOldCodeEtat, txtCodeEtat.Text, ref bSendPda) == false)
                    {

                        var sMessage = "Attention le changement du statut " + sOldCodeEtat + " en " + txtCodeEtat.Text + " n'est pas autorisé, le statut. Le statut va reprendre la valeur " + sOldCodeEtat + ".";
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMessage, "Opération non autorisée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtCodeEtat.Text = txtOldCodeEtat.Text;
                        bcontrole = true;
                        return functionReturnValue;
                    }
                }

                //===> Mondir le 14.10.2020, Mantis = 1587, the application always send old data to PDA, the correct way is to update the database first then send to PDA
                //===> I moved the function after the update query
                //fc_CtrlDataPDA();

                //===> Mondir le 09.04.2021 https://groupe-dt.mantishub.io/view.php?id=2389
                CheckIntervenant();


                General.sSQL = "Update Intervention set ";
                //sSql = sSql & "Achat=" & nz(gFr_NombreAng(txtAchat), "0") & ","
                General.sSQL = General.sSQL + "Acompte=" + General.nz(StdSQLchaine.gFr_NombreAng(txtAcompte.Text), 0) + ",";
                General.sSQL = General.sSQL + "Article='" + StdSQLchaine.gFr_DoublerQuote(txtArticle.Text) + "" + "',";
                //sSql = sSql & "BisDateSaisie='" & nz(gFr_DoublerQuote(txtBisDateSaisie), "") & "',"
                //sSql = sSql & "BisDuree='" & nz(gFr_DoublerQuote(txtBisDuree), "null") & "',"
                //sSql = sSql & "BisHeureDebut='" & nz(gFr_DoublerQuote(txtBisHeureDebut), "") & "',"
                //sSql = sSql & "BisHeureFin='" & nz(gFr_DoublerQuote(txtBisHeureFin), "") & "',"
                General.sSQL = General.sSQL + "CodeEtat='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCodeEtat.Text), "00") + "',";
                General.sSQL = General.sSQL + "CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "" + "',";
                General.sSQL = General.sSQL + "CodeParticulier='" + StdSQLchaine.gFr_DoublerQuote(txtCodeParticulier.Text) + "" + "',";
                General.sSQL = General.sSQL + "Commentaire='" + StdSQLchaine.gFr_DoublerQuote(txtCommentaire.Text) + "" + "',";


                if (!string.IsNullOrEmpty(txtDateRealise.Text) && General.IsDate(txtDateRealise.Text))
                {
                    General.sSQL = General.sSQL + " DateRealise='" + Convert.ToDateTime(txtDateRealise.Text).ToString(General.FormatDateSansHeureSQL) + "',";
                }
                else
                {
                    General.sSQL = General.sSQL + " DateRealise=null,";
                }

                if (!string.IsNullOrEmpty(txtDatePrevue.Text) && General.IsDate(txtDatePrevue.Text))
                {
                    General.sSQL = General.sSQL + " dateprevue='" + Convert.ToDateTime(txtDatePrevue.Text).ToString(General.FormatDateSansHeureSQL) + "',";
                }
                else
                {
                    General.sSQL = General.sSQL + " dateprevue=null,";
                }

                if (General.IsNumeric(txt16.Text))
                {
                    General.sSQL = General.sSQL + " DureePrevue=" + txt16.Text + ",";
                }
                else
                {
                    General.sSQL = General.sSQL + " DureePrevue=null,";
                }

                if (!string.IsNullOrEmpty(txtDateVisite.Text) && General.IsDate(txtDateVisite.Text))
                {
                    General.sSQL = General.sSQL + " DateVisite='" + Convert.ToDateTime(txtDateVisite.Text).ToString(General.FormatDateSQL) + "',";
                }
                else
                {
                    General.sSQL = General.sSQL + " DateVisite = null,";
                }

                //Si le code état de la fiche d'intervention a changé, pas de modification de ce même code
                // et des éléments horaires liés : Heure de début, heure de fin ...

                if (boolVerifCodeEtat == true)
                {
                    if (!string.IsNullOrEmpty(txtDuree.Text) && General.IsDate(txtDuree.Text))
                    {
                        General.sSQL = General.sSQL + " Duree='" + Convert.ToDateTime(General.nz(StdSQLchaine.gFr_NombreAng(txtDuree.Text), "01/01/1900 00:00:00")).ToString(General.FormatDateSQL) + "',";
                    }
                    else
                    {
                        General.sSQL = General.sSQL + " Duree=null,";
                    }

                    if (txtDebut.Text.Trim() != "  :  :  ".Trim() && General.IsDate(txtDebut.Text))
                    {
                        General.sSQL = General.sSQL + " HeureDebut='" + txtDebut.Text + "',";
                    }
                    else
                    {
                        General.sSQL = General.sSQL + " HeureDebut=null,";
                    }

                    if (txtHeurePrevue.Text.Trim() != "  :  ".Trim() && General.IsDate(txtHeurePrevue.Text))
                    {
                        General.sSQL = General.sSQL + " HeurePrevue='" + txtHeurePrevue.Text + "',";
                    }
                    else
                    {
                        General.sSQL = General.sSQL + " HeurePrevue=null,";
                    }

                    if (txtheureDebutP.Text.Trim() != "  :  ".Trim() && General.IsDate(txtheureDebutP.Text))
                    {
                        General.sSQL = General.sSQL + " heureDebutP='" + txtheureDebutP.Text + "',";
                    }
                    else
                    {
                        General.sSQL = General.sSQL + " heureDebutP=null,";
                    }


                    if (txtFin.Text.Trim() != "  :  :  ".Trim() && General.IsDate(txtFin.Text))
                    {
                        General.sSQL = General.sSQL + " HeureFin='" + txtFin.Text + "',";
                    }
                    else
                    {
                        General.sSQL = General.sSQL + " HeureFin=null,";
                    }

                    General.sSQL = General.sSQL + "Deplacement='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtDeplacement.Text), "0") + "',";

                }

                //sSql = sSql & "Courrier=" & nz(gFr_NombreAng(txtCourrier), "0") & ","
                General.sSQL = General.sSQL + "CptREndu_INT='" + StdSQLchaine.gFr_DoublerQuote(txtCptREndu_INT.Text) + "',";
                General.sSQL = General.sSQL + "Designation='" + StdSQLchaine.gFr_DoublerQuote(txtDesignation.Text) + "',";
                General.sSQL = General.sSQL + "Dispatcheur='" + StdSQLchaine.gFr_DoublerQuote(txtDispatcheur.Value.ToString()) + "',";
                General.sSQL = General.sSQL + "Intervenant='" + StdSQLchaine.gFr_DoublerQuote(txtMatriculeIntervenant.Text) + "',";
                General.sSQL = General.sSQL + "LivraisonFioul=" + General.nz(((int)CheckFioul.CheckState), "0") + ",";
                General.sSQL = General.sSQL + "CompteurObli=" + General.nz(((int)chkCompteurObli.CheckState), "0") + ",";
                //=== il existe deux champs releve de compteur, controler à terme quel sera le champs
                //=== à utiliser.
                General.sSQL = General.sSQL + "Rel_compteur=" + General.nz(((int)chkCompteurObli.CheckState), "0") + ",";

                General.sSQL = General.sSQL + "Urgence=" + General.nz(((int)chkUrgence.CheckState), "0") + ",";



                General.sSQL = General.sSQL + "NoDevis='" + StdSQLchaine.gFr_DoublerQuote(txtNoDevis.Text) + "',";
                //sSQL = sSQL & "NoFacture='" & gFr_DoublerQuote(txtNoFacture) & "',"
                General.sSQL = General.sSQL + "NumFicheStandard=" + StdSQLchaine.gFr_NombreAng(txtNumFicheStandard.Text) + ",";
                General.sSQL = General.sSQL + "INT_AnaActivite='" + StdSQLchaine.gFr_DoublerQuote(txtINT_AnaActivite.Text) + "',";
                General.sSQL = General.sSQL + "INT_AnaCode='" + StdSQLchaine.gFr_DoublerQuote(ssComboActivite.Text + txtChefSecteur2.Value) + "',";
                General.sSQL = General.sSQL + "INT_Rapport1='" + StdSQLchaine.gFr_DoublerQuote(txtINT_Rapport1.Text) + "',";
                General.sSQL = General.sSQL + "INT_Rapport2='" + StdSQLchaine.gFr_DoublerQuote(txtINT_Rapport2.Text) + "',";
                General.sSQL = General.sSQL + "INT_Rapport3='" + StdSQLchaine.gFr_DoublerQuote(TXTINT_Rapport3.Text) + "',";
                //sSql = sSql & "INT_TypeIntervention=" & nz(gFr_NombreAng(txtINT_TypeIntervention), 0) & ","
                //sSql = sSql & "INT_facturable=" & nz(gFr_NombreAng(chkINT_facturable), 0) & ","

                General.sSQL = General.sSQL + "Sinistre=" + General.nz(((int)ChekSinistre.CheckState), "0") + ",";
                General.sSQL = General.sSQL + "SansPauseDejeuner=" + General.nz(((int)chkSansPauseDejeuner.CheckState), "0") + ",";

                General.sSQL = General.sSQL + "Situation='" + StdSQLchaine.gFr_DoublerQuote(txtSituation.Text) + "',";

                General.sSQL = General.sSQL + "TYMO_ID='" + StdSQLchaine.gFr_DoublerQuote(ComboTYMO_ID.Text) + "',";

                General.sSQL = General.sSQL + "VisiteEntretien =" + General.nz(((int)chkVisiteEntretien.CheckState), 0) + ",";

                General.sSQL = General.sSQL + "TelephoneContact ='" + txtTelephoneContact.Text + "',";

                General.sSQL = General.sSQL + "SousArticle='" + StdSQLchaine.gFr_DoublerQuote(txtSousArticle.Text) + "',";
                General.sSQL = General.sSQL + "TrvxParticulier=" + General.nz(((int)CheckTrvx.CheckState), "0") + ",";
                General.sSQL = General.sSQL + "Vidange=" + General.nz(StdSQLchaine.gFr_NombreAng((int)CheckVidange.CheckState), "0") + ",";

                General.sSQL = General.sSQL + "AvisDePassage =" + General.nz((int)chk0.CheckState, "0") + ",";


                //sSQl = sSQl & "Wave='" & nz(gFr_DoublerQuote(txtWave), "") & "',"
                General.sSQL = General.sSQL + "Debourse=" + Convert.ToDouble(General.nz((txtDebourse.Text), "0")) + ",";
                //== modif du 01 06 2016 ajout de champs.
                General.sSQL = General.sSQL + " ModifierLe=  '" + DateTime.Now + "',";
                General.sSQL = General.sSQL + "ModifierPar ='" + General.Left(StdSQLchaine.gFr_DoublerQuote(General.fncUserName()), 50) + "',";

                using (var tmpModAdo = new ModAdo())
                    sMaxNoMaj = tmpModAdo.fc_ADOlibelle("SELECT SUM(NoMaj) AS MaxNoMaj From Intervention WHERE  NoIntervention =" + txtNoIntervention.Text);

                General.sSQL = General.sSQL + " NoMaj = " + Convert.ToInt32(Convert.ToInt32(General.nz(sMaxNoMaj, 0)) + 1) + ",";

                General.sSQL = General.sSQL + "DossierIntervention='" + txtDossierIntervention.Text + "'";
                General.sSQL = General.sSQL + " WHERE NoIntervention =" + txtNoIntervention.Text;
                int xx = General.Execute(General.sSQL);

                dtLoad = DateTime.Now;
                General.sSQL = "UPDATE Immeuble SET  ";
                General.sSQL = General.sSQL + "Immeuble.Gardien ='" + StdSQLchaine.gFr_DoublerQuote(txtGardien.Text + "") + "', ";
                General.sSQL = General.sSQL + "Immeuble.CodeAcces1 ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeAcces1.Text + "") + "', ";
                General.sSQL = General.sSQL + "Immeuble.CodeAcces2 ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeAcces2.Text + "") + "', ";
                General.sSQL = General.sSQL + "Immeuble.BoiteAClef = " + (int)chkBoite.CheckState + " ,";
                General.sSQL = General.sSQL + "Immeuble.SitBoiteAClef ='" + StdSQLchaine.gFr_DoublerQuote(txtSitBoite.Text + "") + "', ";
                General.sSQL = General.sSQL + "Immeuble.SpecifAcces ='" + StdSQLchaine.gFr_DoublerQuote(txtSpecifAcces.Text) + "" + "', ";
                General.sSQL = General.sSQL + "Immeuble.TelGardien ='" + txtTel.Text + "" + "' ";
                //===> Mondir le 18.03.2021, https://groupe-dt.mantishub.io/view.php?id=1381
                //===> Mondir le 14.05.2021, https://groupe-dt.mantishub.io/view.php?id=1381#c6039
                //General.sSQL = General.sSQL + "Immeuble.situation ='" + txtSitBoite.Text + "" + "' ";
                //===> Fin Modif Mondir
                General.sSQL = General.sSQL + "WHERE Immeuble.CodeImmeuble ='" + txtCodeImmeuble.Text + "'";
                xx = General.Execute(General.sSQL);

                if (!string.IsNullOrEmpty(txtNoDevis.Text))
                {
                    General.sSQL = "UPDATE DevisEnTete set NoEnregistrement ='" + txtNoEnregistrement.Text + "'" + " WHERE NumeroDevis ='" + txtNoDevis.Text + "'";
                    xx = General.Execute(General.sSQL);

                }

                RechercheLibelleEtat();

                if (txtDebut.Text.Trim() != "  :  :  ".Trim() && txtFin.Text.Trim() != "  :  :  ".Trim() && !string.IsNullOrEmpty(txtDuree.Text))
                {
                    stockeAnalytique();
                }

                if (txtCodeEtat.Text != txtOldCodeEtat.Text)
                {
                    General.fc_HistoStatut(Convert.ToInt32(txtNoIntervention.Text), txtCodeImmeuble.Text, "Manuelle", txtOldCodeEtat.Text, txtCodeEtat.Text, txtDateRealise.Text,
                        txtHeurePrevue.Text, chkCompteurObli.CheckState == CheckState.Checked ? 1 : 0, chkUrgence.CheckState == CheckState.Checked ? 1 : 0, txtDatePrevue.Text,
                    chkVisiteEntretien.CheckState == CheckState.Checked ? 1 : 0);
                }

                //===> Mondir le 14.10.2020, moved the this function to this place
                fc_CtrlDataPDA();

                return functionReturnValue;
            }
            catch (Exception ex)
            {

                //=== gestion des interventions envoyées sur PDA.
                //fc_CtrlDataPDA

                Erreurs.gFr_debug(ex, this.Name + ";fc_sauver");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// Mondir le 15.04.2021 https://groupe-dt.mantishub.io/view.php?id=2389#c5866
        /// </summary>
        /// <returns></returns>
        private bool CheckIntervenant()
        {
            using (var tmpModAdo = new ModAdo())
            {
                var req = $"SELECT Nom FROM Personnel WHERE NonActif = 0 AND Matricule = '{txtIntervenant.Text}'";
                req = tmpModAdo.fc_ADOlibelle(req);
                if (string.IsNullOrEmpty(req))
                {
                    //===> Mondir le 12.04.2021, https://groupe-dt.mantishub.io/view.php?id=2389#c5846
                    //CustomMessageBox.Show("Veuillez vérifier que cet Intervenant existe et qu'il est actif", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    CustomMessageBox.Show("Cet intervenant n'existe pas ou est inactif\nModification annulée", "Modification annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //functionReturnValue = false;
                    //return functionReturnValue;
                    var interventant = tmpModAdo.fc_ADOlibelle($"SELECT Intervenant FROM Intervention WHERE NoIntervention = '{txtNoIntervention.Text}'");
                    txtIntervenant.Text = interventant;
                    return false;
                }
            }
            return true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool fc_CrtlDonneeConccurente()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_CrtlDonneeConccurente() - Enter In The Function");
            //===> Fin Modif Mondir
            bool functionReturnValue = false;
            //=== modif du 01 Juin 2016, gére la gestion conccurentiel des données,
            //=== si un utilisateur a modifié les données pendant qu'un autre utilisateur
            //=== se trouve sur la méme intervention. Cette fonction retourne true
            //=== di cette intervention a été modifé entre temps.
            string sSQL = null;
            System.DateTime dtCompare = default(System.DateTime);
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;

            try
            {
                functionReturnValue = false;

                sSQL = "SELECT      ModifierLe, ModifierPar" + " From Intervention " + " WHERE  NoIntervention = " + General.nz(txtNoIntervention.Text, 0);
                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(rs.Rows[0]["ModifierLe"].ToString()) && General.IsDate(rs.Rows[0]["ModifierLe"].ToString()))
                    {
                        dtCompare = Convert.ToDateTime(rs.Rows[0]["ModifierLe"]);
                        if (dtCompare > dtLoad)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'utilisateur " + rs.Rows[0]["ModifierPar"] + " a modifié cette intervention à " + dtCompare.ToString("HH:mm:ss") + " le " +
                                dtCompare.Date.ToString("dd/MM/yyyy") + "." + "\n" +
                                "Vos modifications ne seront donc pas prises en comptes.  " + "\n" +
                                " Après la lecture de ce message l'intervention va se charger avec les éléments modifiés par cet utilisateur" +
                                " et vous pourrez  ainsi à nouveau modifier cette intervention.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);

                            functionReturnValue = true;
                        }
                    }
                }
                modAdors.Close();
                return functionReturnValue;
                //
                //dtLoad
                //bMaj
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_DonneeConccurente");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        /// <param name="blFisheStandard"></param>
        private void fc_ChargeEnregistrement(string sCode, bool blFisheStandard = false)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ChargeEnregistrement() - Enter In The Function - sCode = {sCode} - blFisheStandard = {blFisheStandard}");
            //===> Fin Modif Mondir
            string sCodeImmeuble = null;
            DataTable rsNbInterv = default(DataTable);
            ModAdo modAdorsNbInterv = null;
            bool boolNewInterv = false;
            string AnalytiqueActivite = null;
            DataTable rsNbFact = default(DataTable);
            ModAdo modAdorsNbFact = null;
            int lngNbFact = 0;
            int j = 0;
            string sSQldispacht;
            DataTable rsDisp = new DataTable();
            ModAdo rsDispAdo = new ModAdo();
            //lEtatEnvoie = 0

            General.sSQL = "";
            General.sSQL = "SELECT Intervention.* From Intervention";
            boolNewInterv = false;
            if (blFisheStandard == true) longNumficheStandard = Convert.ToInt32(sCode);

            //=== modif du 01 06 2016, stocke la date heure du chargement
            //=== des données afin de gérer l'acces concurentiel des données.
            dtLoad = DateTime.Now;

            //=== si la fiche appelante est la fiche standard alors on effectue une recherche si
            //=== l'intervention n' existe pas alors on la creer



            if (blFisheStandard == true)
            {
                General.sSQL = General.sSQL + " where Numfichestandard =" + sCode;

                General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);
                txtselCAI_code.Text = General.getFrmReg(Variable.cUserIntervention, "Operation", "");

                if (General.rstmp.Rows.Count == 0)
                {
                    boolNewInterv = true;
                    //creation de la fiche intervention
                    txtCodeImmeuble.Text = General.getFrmReg(Variable.cUserIntervention, "CodeImmeuble", "");
                    txtDateSaisie.Text = General.getFrmReg(Variable.cUserIntervention, "DateSaisie", "");
                    txtCodeParticulier.Text = General.getFrmReg(Variable.cUserIntervention, "Copro", "");
                    txtDocument.Text = General.getFrmReg(Variable.cUserIntervention, "Courier", "");
                    txtCAI_code.Text = General.getFrmReg(Variable.cUserIntervention, "Operation", "");
                    txtDatePrevue.Text = DateTime.Today.ToShortDateString();

                    General.rstmp.Rows.Add(General.rstmp.NewRow());
                    General.rstmp.Rows[0]["Numfichestandard"] = sCode;
                    General.rstmp.Rows[0]["CodeImmeuble"] = txtCodeImmeuble.Text;
                    General.rstmp.Rows[0]["DateSaisie"] = General.nz(txtDateSaisie.Text, DateTime.Now);
                    General.rstmp.Rows[0]["Courrier"] = General.nz(txtDocument.Text, 0).ToString() == "1";
                    General.rstmp.Rows[0]["CodeParticulier"] = txtCodeParticulier.Text;
                    General.rstmp.Rows[0]["CAI_Code"] = txtCAI_code.Text;
                    General.rstmp.Rows[0]["DatePrevue"] = DateTime.Today;
                    General.rstmp.Rows[0]["CreeLe"] = DateTime.Now;
                    General.rstmp.Rows[0]["CreePar"] = General.Left(General.fncUserName(), 50);

                    //'=== modif du 12 03 2019, ajout du dispatcheur si existant.
                    sSQldispacht = "SELECT     DISPATCH.Matricule, DISPATCH.Nom, DISPATCH.Prenom"
                               + " FROM         Immeuble INNER JOIN"
                               + " Personnel ON Immeuble.CodeRespExploit = Personnel.Matricule INNER JOIN "
                               + " Personnel AS DISPATCH ON Personnel.Dispatcheur = DISPATCH.Initiales "
                               + " WHERE     (Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "')";
                    rsDisp = rsDispAdo.fc_OpenRecordSet(sSQldispacht);

                    if (rsDisp.Rows.Count > 0)
                    {
                        txtDispatcheur.Text = rsDisp.Rows[0]["Matricule"] + "";
                        txtLibDispatch.Text = rsDisp.Rows[0]["Nom"] + "";
                        General.rstmp.Rows[0]["dispatcheur"] = txtDispatcheur.Text;
                    }
                    int xx = General.modAdorstmp.Update();

                    //            fc_OpenConnSage
                    //            AnalytiqueActivite = fc_ADOlibelle("SELECT CA_Num FROM F_COMPTEA WHERE ActiviteTravaux='1'", False, adoSage)
                    //            fc_CloseConnSage
                    //            adocnn.Execute "Update GESTIONSTANDARD set intervention = '1', AnalytiqueActivite='" & AnalytiqueActivite & "' WHERE NumFicheStandard=" & sCode
                    xx = General.Execute("Update GESTIONSTANDARD set intervention = '1' WHERE NumFicheStandard=" + sCode);
                    using (var tmpModAdo = new ModAdo())
                        LongNoIntervention = Convert.ToInt32(tmpModAdo.fc_ADOlibelle("SELECT MAX(NoIntervention) FROM Intervention"));
                    General.rstmp.Rows[0]["NoIntervention"] = LongNoIntervention;
                    this.txtNumFicheStandard.Text = General.nz(General.rstmp.Rows[0]["Numfichestandard"], "").ToString();

                    txtNoIntervention.Text = Convert.ToString(LongNoIntervention);
                }
            }
            else
            {
                General.modAdorstmp = new ModAdo();
                General.sSQL = General.sSQL + " where NoIntervention =" + sCode;
                General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);
            }


            if (General.rstmp.Rows.Count > 0)
            {
                if (!string.IsNullOrEmpty(General.rstmp.Rows[0]["ModifierLe"].ToString()) && General.IsDate(General.rstmp.Rows[0]["ModifierLe"].ToString()))
                {
                    dtLoad = Convert.ToDateTime(General.rstmp.Rows[0]["ModifierLe"]);
                }
                else
                {
                    dtLoad = DateTime.Now;
                }
                this.txtNumFicheStandard.Text = General.nz(General.rstmp.Rows[0]["Numfichestandard"], "").ToString();
                this.txtNoIntervention.Text = General.nz(General.rstmp.Rows[0]["NoIntervention"], "").ToString();
                if (!string.IsNullOrEmpty(General.nz(General.rstmp.Rows[0]["NoIntervention"], "").ToString()))
                    LongNoIntervention = Convert.ToInt32(General.nz(General.rstmp.Rows[0]["NoIntervention"], ""));


                //=== affiche le lien vers le bon de commande si celui ci exite
                using (var tmpModAdo = new ModAdo())
                    if (tmpModAdo.fc_ADOlibelle("SELECT TOP 1 NumFicheStandard FROM BonDeCommande WHERE NumFicheStandard=" + txtNumFicheStandard.Text + "") == txtNumFicheStandard.Text)
                    {
                        btnAchat.BackColor = cFindColor;
                        //lblGoAchat.Enabled = True
                    }
                    else
                    {
                        btnAchat.BackColor = cNotFoundColor1;
                        Label17.Enabled = true;
                    }

                this.txtAcompte.Text = General.nz(General.rstmp.Rows[0]["acompte"], "").ToString();
                this.txtArticle.Text = General.nz(General.rstmp.Rows[0]["Article"], "").ToString();
                txt0.Text = General.nz(General.rstmp.Rows[0]["RecuLe"], "").ToString();
                txt1.Text = General.nz(General.rstmp.Rows[0]["ArrivePrevue"], "").ToString();
                //Me.txtBisDateSaisie = nz(rstmp!BisDateSaisie, "")
                //Me.txtBisDuree = nz(rsTmp!BisDuree, "")
                //Me.txtBisHeureDebut = nz(rsTmp!BisHeureDebut, "")
                //Me.txtBisHeuin = nz(rsTmp!BisHeuin, "")
                if (General.rstmp.Rows[0]["CodeEtat"] != DBNull.Value)
                {
                    this.txtCodeEtat.Text = General.nz(General.rstmp.Rows[0]["CodeEtat"], "00").ToString();
                }
                else
                {
                    this.txtCodeEtat.Text = "00";
                }
                txtOldCodeEtat.Text = txtCodeEtat.Text;
                //=== utilisé pour la gestion des envoies d'inter sur PDA.
                txt4.Text = txtCodeEtat.Text;
                txt7.Text = General.rstmp.Rows[0]["EnvoyeLe"] + "";
                txt8.Text = General.rstmp.Rows[0]["MajCorParPDA"] + "";

                using (var tmpModAdo = new ModAdo())
                    this.lbllibcodeetat.Text = tmpModAdo.fc_ADOlibelle("SELECT  LibelleCodeEtat" + " FROM TypeCodeEtat" + " where codeetat ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeEtat.Text) + "'");
                this.txtCodeImmeuble.Text = General.nz(General.rstmp.Rows[0]["CodeImmeuble"], "").ToString();
                this.txtCodeParticulier.Text = General.nz(General.rstmp.Rows[0]["CodeParticulier"], "").ToString();
                this.txtCommentaire.Text = General.nz(General.rstmp.Rows[0]["Commentaire"], "").ToString();
                //=== utilisé pour la gestion des envoies d'inter sur PDA.
                txt6.Text = General.nz(General.rstmp.Rows[0]["Commentaire"], "").ToString();

                if (txtCommentaire.Text.Length > 81)
                {
                    txtCommentaire.iTalkTB.Select(91, txtCommentaire.Text.Length - 81);
                }

                this.txtCptREndu_INT.Text = General.nz(General.rstmp.Rows[0]["CptREndu_INT"], "").ToString();

                if (!string.IsNullOrEmpty(General.rstmp.Rows[0]["DatePrevue"].ToString()) && General.IsDate(General.rstmp.Rows[0]["DatePrevue"].ToString()))
                {
                    this.txtDatePrevue.Text = Convert.ToDateTime(General.rstmp.Rows[0]["DatePrevue"]).ToShortDateString();
                }
                else
                {
                    this.txtDatePrevue.Text = "";
                }
                this.txt16.Text = General.nz(General.rstmp.Rows[0]["DureePrevue"], "").ToString();

                if (!string.IsNullOrEmpty(General.rstmp.Rows[0]["DateVisite"].ToString()) && General.IsDate(General.rstmp.Rows[0]["DateVisite"].ToString()))
                {
                    this.txtDateVisite.Text = Convert.ToDateTime(General.rstmp.Rows[0]["DateVisite"]).ToShortDateString();
                }
                else
                {
                    this.txtDateVisite.Text = "";
                }

                txt12.Text = txtDateVisite.Text;

                if (!string.IsNullOrEmpty(General.rstmp.Rows[0]["DateRealise"].ToString()) && General.IsDate(General.rstmp.Rows[0]["DateRealise"].ToString()))
                {
                    this.txtDateRealise.Text = Convert.ToDateTime(General.rstmp.Rows[0]["DateRealise"]).ToShortDateString();
                    Dtpick0.Value = Convert.ToDateTime(txtDateRealise.Text);
                }
                else
                {
                    this.txtDateRealise.Text = "";
                    Dtpick0.Value = DateTime.Now;
                }

                txt11.Text = General.rstmp.Rows[0]["DateRealise"] != DBNull.Value ? Convert.ToDateTime(General.rstmp.Rows[0]["DateRealise"]).ToShortDateString() : "";

                this.txtDateSaisie.Text = General.rstmp.Rows[0]["DateSaisie"] != DBNull.Value ? Convert.ToDateTime(General.rstmp.Rows[0]["DateSaisie"]).ToString() : "";
                this.txtDeplacement.Text = General.nz(General.rstmp.Rows[0]["Deplacement"], "").ToString();
                this.txtDesignation.Text = General.nz(General.rstmp.Rows[0]["Designation"], "").ToString();
                //=== utilisé pour la gestion des envoies d'inter sur PDA.
                txt5.Text = General.nz(General.rstmp.Rows[0]["Designation"], "").ToString();
                this.txtDispatcheur.Value = General.nz(General.rstmp.Rows[0]["dispatcheur"], "");

                if (General.nz(General.rstmp.Rows[0]["dispatcheur"], "") != DBNull.Value
                    && !(string.IsNullOrEmpty(General.nz(General.rstmp.Rows[0]["dispatcheur"], "").ToString())))
                {
                    using (var tmpModAdo = new ModAdo())
                        this.txtLibDispatch.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + General.nz(StdSQLchaine.gFr_DoublerQuote(General.rstmp.Rows[0]["dispatcheur"].ToString()), "") + "'");
                }
                else
                {
                    this.txtLibDispatch.Text = "";
                }
                this.txtCAI_code.Text = General.nz(General.rstmp.Rows[0]["CAI_Code"], "").ToString();
                // recherche du libelle du dispatcheur
                //            Me.lbllibDispatcheur = fc_ADOlibelle("SELECT Personnel.Nom" _
                //'                                                    & " From Personnel" _
                //'                                             & " WHERE Personnel.Matricule ='" & txtDispatcheur & "'")

                if (General.rstmp.Rows[0]["Duree"] != DBNull.Value && !string.IsNullOrEmpty(General.rstmp.Rows[0]["Duree"].ToString()))
                {
                    if (General.IsDate(General.rstmp.Rows[0]["Duree"].ToString()))
                    {
                        this.txtDuree.Text = Convert.ToDateTime(General.nz(General.rstmp.Rows[0]["Duree"], "")).ToString("HH:mm:ss");
                    }
                    else
                    {
                        this.txtDuree.Text = "";
                    }
                }
                else
                {
                    this.txtDuree.Text = "";
                }
                this.txtDebourse.Text = Convert.ToDouble(General.nz(General.rstmp.Rows[0]["Debourse"], "0")).ToString("##0.00");
                //            Me.txtHeureDebut = nz(rsTmp!Heuredebut, "")
                //            Me.txtHeuin = nz(rsTmp!Heuin, "")

                if (General.rstmp.Rows[0]["HeureDebut"] != DBNull.Value && !string.IsNullOrEmpty(General.rstmp.Rows[0]["HeureDebut"].ToString())
                    && General.IsDate(General.rstmp.Rows[0]["HeureDebut"]))
                {
                    this.txtDebut.Text = Convert.ToDateTime(General.nz(General.rstmp.Rows[0]["HeureDebut"], "")).ToString("HH:mm:ss");

                }
                else
                {
                    this.txtDebut.Text = "  :  :  ";
                }

                if (General.rstmp.Rows[0]["HeureDebutPDA"] != DBNull.Value && !string.IsNullOrEmpty(General.rstmp.Rows[0]["HeureDebutPDA"].ToString())
                    && General.IsDate(General.rstmp.Rows[0]["HeureDebutPDA"]))
                {
                    txt14.Text = Convert.ToDateTime(General.nz(General.rstmp.Rows[0]["HeureDebutPDA"], "")).ToString("HH:mm:ss");
                }
                else
                {
                    txt14.Text = "";
                }


                //txtDebut.BackColor = System.Drawing.ColorTranslator.FromOle(Convert.ToInt32(General.cBlanc));
                txtDebut.BackColor = Color.White;

                if (General.IsDate(txtDebut.Text) && General.IsDate(txt14.Text))
                {
                    if (System.Math.Abs((Convert.ToDateTime(txt14.Text) - Convert.ToDateTime(txtDebut.Text)).TotalMinutes) > 2)
                    {
                        //txtDebut.BackColor = System.Drawing.ColorTranslator.FromOle(cRose);
                        txtDebut.BackColor = Color.Pink;
                        //txtHeureDebutPDA.BackColor = cRose
                    }

                }
                if (Convert.ToInt32(General.nz(General.rstmp.Rows[0]["Encaissement"], 0)) == 1)
                {
                    cmd1.BackColor = cFindColor;
                }
                else
                {
                    cmd1.BackColor = Color.FromArgb(192, 192, 255);
                }

                if (General.rstmp.Rows[0]["HeureFin"] != DBNull.Value && !string.IsNullOrEmpty(General.rstmp.Rows[0]["HeureFin"].ToString())
                    && General.IsDate(General.rstmp.Rows[0]["HeureFin"]))
                {
                    this.txtFin.Text = Convert.ToDateTime(General.nz(General.rstmp.Rows[0]["HeureFin"], "")).ToString("HH:mm:ss");
                    // ======> Mondir le 18.05.2020 : This event forces calculating the duration, this line added by somebody else !
                    //if (General.IsDate(txtFin.Text) && General.IsDate(txtDebut.Text))//force the leaving event of txtFin textbox to update txtDuree
                    //    txtFin_Validating(txtDebut, new CancelEventArgs(false));
                }
                else
                {
                    this.txtFin.Text = "  :  :  ";
                }

                if (General.rstmp.Rows[0]["HeureFinPDA"] != DBNull.Value && !string.IsNullOrEmpty(General.rstmp.Rows[0]["HeureFinPDA"].ToString())
                    && General.IsDate(General.rstmp.Rows[0]["HeureFinPDA"]))
                {
                    txt15.Text = Convert.ToDateTime(General.nz(General.rstmp.Rows[0]["HeureFinPDA"], "")).ToString("HH:mm:ss");
                }
                else
                {
                    txt15.Text = "";
                }

                //txtFin.BackColor = System.Drawing.ColorTranslator.FromOle(Convert.ToInt32(General.cBlanc));
                txtFin.BackColor = Color.White;

                if (General.IsDate(txtFin.Text) && General.IsDate(txt15.Text))
                {
                    if (System.Math.Abs((Convert.ToDateTime(txt15.Text) - Convert.ToDateTime(txtFin.Text)).TotalMinutes) > 2)
                    {
                        //txtFin.BackColor = System.Drawing.ColorTranslator.FromOle(cRose);
                        txtFin.BackColor = Color.Pink;
                        //txtHeuinPDA.BackColor = cRose

                    }

                }

                if (General.rstmp.Rows[0]["HeurePrevue"] != DBNull.Value && !string.IsNullOrEmpty(General.rstmp.Rows[0]["HeurePrevue"].ToString())
                    && General.IsDate(General.rstmp.Rows[0]["HeurePrevue"]))
                {
                    this.txtHeurePrevue.Text = Convert.ToDateTime(General.nz(General.rstmp.Rows[0]["HeurePrevue"], "")).ToString("HH:mm");
                    txt10.Text = txtHeurePrevue.Text;
                }
                else
                {
                    this.txtHeurePrevue.Text = "  :  ";
                    txt10.Text = txtHeurePrevue.Text;
                }

                if (General.rstmp.Rows[0]["heureDebutP"] != DBNull.Value && !string.IsNullOrEmpty(General.rstmp.Rows[0]["heureDebutP"].ToString())
                    && General.IsDate(General.rstmp.Rows[0]["heureDebutP"]))
                {
                    this.txtheureDebutP.Text = Convert.ToDateTime(General.nz(General.rstmp.Rows[0]["heureDebutP"], "")).ToString("HH:mm");
                }
                else
                {
                    this.txtheureDebutP.Text = "  :  ";
                }

                txt13.Text = txtHeurePrevue.Text;

                lblNomIntervenant.Text = "";

                if (boolNewInterv == true)
                {
                    this.txtIntervenant.Text = "000";
                    this.txtMatriculeIntervenant.Text = "000";
                }
                else
                {
                    this.txtIntervenant.Text = General.nz(General.rstmp.Rows[0]["Intervenant"], "").ToString();
                    this.txtMatriculeIntervenant.Text = General.nz(General.rstmp.Rows[0]["Intervenant"], "").ToString();
                    using (var tmpModAdo = new ModAdo())
                        lblNomIntervenant.Text = tmpModAdo.fc_ADOlibelle("SELECT NOM FROM PERSONNEL WHERE MATRICULE ='" + StdSQLchaine.gFr_DoublerQuote(txtIntervenant.Text) + "'");
                }

                txt9.Text = txtIntervenant.Text;

                //            Me.lbllibIntervenant = fc_ADOlibelle("SELECT Personnel.Nom" _
                //'                                                    & " From Personnel" _
                //'                                             & " WHERE Personnel.Matricule ='" & txtIntervenant & "'")
                this.CheckFioul.CheckState = General.nz(General.rstmp.Rows[0]["LivraisonFioul"], 0).ToString() == "1" || General.nz(General.rstmp.Rows[0]["LivraisonFioul"], 0).ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;

                chkCompteurObli.CheckState = General.nz(General.rstmp.Rows[0]["CompteurObli"], 0).ToString() == "1" || General.nz(General.rstmp.Rows[0]["CompteurObli"], 0).ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;
                chkUrgence.CheckState = General.nz(General.rstmp.Rows[0]["Urgence"], 0).ToString() == "1" || General.nz(General.rstmp.Rows[0]["Urgence"], 0).ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;

                var sSQL1 = "SELECT     count(*) " + " From InterventionDoc " + " Where Nointervention = " + General.rstmp.Rows[0]["NoIntervention"].ToString();
                var modadoLibelle = new ModAdo();
                var count = Convert.ToInt32(modadoLibelle.fc_ADOlibelle(sSQL1));
                if (count > 0)
                {
                    cmdDetailPrestV2.BackColor = cFindColor;
                }
                else
                {
                    cmdDetailPrestV2.BackColor = cNotFoundColor1;
                }
                //modadoLibelle.Close();
                //if (General.nz(General.rstmp.Rows[0]["MajCorParPDA"], 0).ToString() == "1")
                //{
                //    cmdDetailPrestV2.BackColor = cFindColor;
                //}
                //else
                //{
                //    cmdDetailPrestV2.BackColor = cNotFoundColor1;
                //}

                this.txtNoDevis.Text = General.rstmp.Rows[0]["NoDevis"].ToString() + "";

                if (!string.IsNullOrEmpty(txtNoDevis.Text))
                {
                    btnDevis.BackColor = cFindColor;
                }
                else
                {
                    btnDevis.BackColor = cNotFoundColor2;
                }

                this.txtNoFacture.Text = General.rstmp.Rows[0]["NoFacture"].ToString() + "";

                modAdorsNbFact = new ModAdo();
                rsNbFact = modAdorsNbFact.fc_OpenRecordSet("SELECT DISTINCT NoFacture FROM Intervention " + " WHERE NumFicheStandard=" +
                    txtNumFicheStandard.Text + " AND NoFacture<>'' " + " AND NOT NoFacture IS NULL AND NOT LEFT(NoFacture,2)='XX'");

                lngNbFact = 0;
                if (rsNbFact.Rows.Count > 0)
                {
                    //rsNbFact.MoveFirst
                    foreach (DataRow rsNbFactRow in rsNbFact.Rows)
                    {

                        txtNoFacture.Text = rsNbFactRow["NoFacture"].ToString() + "";
                        //==modif du 12 05 2005, prepare la requete pour afficher les factures dans crystal.
                        Array.Resize(ref tSqlCrystal, lngNbFact + 1);
                        tSqlCrystal[lngNbFact] = rsNbFactRow["NoFacture"].ToString() + "";
                        lngNbFact = lngNbFact + 1;
                    }
                }
                modAdorsNbFact.Close();

                if (lngNbFact >= 1)
                {
                    btnFacture.BackColor = cFindColor;
                    if (lngNbFact > 1)
                    {
                        btnFacture.Text = lngNbFact + " Factures";
                    }
                    else
                    {
                        btnFacture.Text = "1 Facture";
                    }
                }
                else
                {
                    btnFacture.BackColor = cNotFoundColor2;
                    btnFacture.Text = "Facture";
                }


                txt3.Text = General.rstmp.Rows[0]["GMAO_Visite"].ToString() + "";

                //            If txtNoFacture.Text <> "" And Left(txtNoFacture.Text, 2) <> "XX" Then
                //                btnFacture.BackColor = cFindColor
                //             Else
                //                btnFacture.BackColor = &HFFC0C0
                //             End If
                this.ChekSinistre.CheckState = General.nz(General.rstmp.Rows[0]["Sinistre"], 0).ToString() == "1" || General.nz(General.rstmp.Rows[0]["Sinistre"], 0).ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;

                this.chkSansPauseDejeuner.CheckState = General.nz(General.rstmp.Rows[0]["SansPauseDejeuner"], 0).ToString() == "1" || General.nz(General.rstmp.Rows[0]["SansPauseDejeuner"], 0).ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;

                //            txtINT_TypeIntervention = nz(rsTmp!INT_TypeIntervention, "")
                //            Me.txtINT_DateProduit = nz(rsTmp!INT_DateProduit, "")
                //            If txtINT_TypeIntervention = cREGIE Then
                //                    optRegie.value = True
                //            ElseIf txtINT_TypeIntervention = cDEVIS Then
                //                    Optdevis.value = True
                //            ElseIf txtINT_TypeIntervention = cCONTRAT Then
                //                    OptContrat.value = True
                //            ElseIf txtINT_TypeIntervention = cSAISON Then
                //                    OptSaison.value = True
                //            ElseIf txtINT_TypeIntervention = cProduit Then
                //                    optProduit.value = True
                //            End If
                //            Me.chkINT_facturable = IIf(rsTmp!INT_facturable = True, 1, 0)
                this.txtSituation.Text = General.nz(General.rstmp.Rows[0]["situation"], "").ToString();
                ComboTYMO_ID.Text = General.nz(General.rstmp.Rows[0]["TYMO_ID"], "").ToString();

                chkVisiteEntretien.CheckState = General.nz(General.rstmp.Rows[0]["VisiteEntretien"], 0).ToString() == "1" ? CheckState.Checked : CheckState.Unchecked;
                txtTelephoneContact.Text = General.nz(General.rstmp.Rows[0]["TelephoneContact"], "").ToString();

                this.txtSousArticle.Text = General.nz(General.rstmp.Rows[0]["SousArticle"], "").ToString();
                this.CheckTrvx.CheckState = General.nz(General.rstmp.Rows[0]["TrvxParticulier"], 0).ToString() == "1" || General.nz(General.rstmp.Rows[0]["TrvxParticulier"], 0).ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;

                if (General.rstmp.Rows[0]["DossierIntervention"] != DBNull.Value)
                {
                    txtDossierIntervention.Text = Convert.ToString(General.nz(General.rstmp.Rows[0]["DossierIntervention"], 0)) == "True" ? "1" : "0";
                }
                else
                {
                    txtDossierIntervention.Text = "0";
                }

                if (txtDossierIntervention.Text != "0")
                {
                    cmdDossierIntervention.BackColor = cFindColor;
                }
                else
                {
                    cmdDossierIntervention.BackColor = cNotFoundColor2;
                }

                if (General.rstmp.Rows[0]["Vidange"] == DBNull.Value || string.IsNullOrEmpty(General.rstmp.Rows[0]["Vidange"].ToString()) ||
                    General.rstmp.Rows[0]["Vidange"].ToString().ToUpper() == "FAUX" || General.rstmp.Rows[0]["Vidange"].ToString().ToUpper() == "FALSE")
                {
                    this.CheckVidange.CheckState = System.Windows.Forms.CheckState.Unchecked;
                }
                else
                {
                    this.CheckVidange.CheckState = System.Windows.Forms.CheckState.Checked;
                }

                chk0.CheckState = General.nz(General.rstmp.Rows[0]["AvisDePassage"], 0).ToString() == "1" ? CheckState.Checked : CheckState.Unchecked;

                if (General.rstmp.Rows[0]["NoDevis"] == DBNull.Value || string.IsNullOrEmpty(General.rstmp.Rows[0]["NoDevis"].ToString()))
                {
                    this.btnDebourse.BackColor = cNotFoundColor1;
                }
                else
                {
                    this.btnDebourse.BackColor = cFindColor;
                }

                if (General.rstmp.Rows[0]["Wave"] == DBNull.Value || string.IsNullOrEmpty(General.rstmp.Rows[0]["Wave"].ToString()))
                {
                    this.cmdWave.BackColor = cNotFoundColor1;
                    this.txtWave.Text = "";
                }
                else
                {
                    this.cmdWave.BackColor = cFindColor;
                    this.txtWave.Text = General.rstmp.Rows[0]["Wave"].ToString() + "";
                }

                this.txtINT_AnaActivite.Text = General.rstmp.Rows[0]["INT_AnaActivite"].ToString() + "";
                this.txtINT_Rapport1.Text = General.nz(General.rstmp.Rows[0]["INT_Rapport1"], "").ToString();
                this.txtINT_Rapport2.Text = General.nz(General.rstmp.Rows[0]["INT_Rapport2"], "").ToString();
                this.TXTINT_Rapport3.Text = General.nz(General.rstmp.Rows[0]["INT_Rapport3"], "").ToString();
                this.txtINT_AnaCode.Text = General.Right(General.nz(General.rstmp.Rows[0]["INT_AnaCode"], "").ToString(), 3);
                this.ssComboActivite.Text = General.Left(General.nz(General.rstmp.Rows[0]["INT_AnaCode"], "").ToString(), 1);

                foreach (DataRow rstmpRow in General.rstmp.Rows)
                {
                    if (!string.IsNullOrEmpty(General.strTabParam[3]))
                    {
                        int xx = General.Execute("UPDATE Intervention SET Intervention.NoDevis='" + General.strTabParam[3] + "'" + " WHERE Intervention.NoIntervention='" + rstmpRow["NoIntervention"].ToString() + "'");
                        this.btnDebourse.BackColor = cFindColor;
                        General.strTabParam[3] = "";
                        //rsTmp.Update "NoDevis", CStr(strTabParam(3))
                    }
                }

                modAdorsNbInterv = new ModAdo();
                rsNbInterv = modAdorsNbInterv.fc_OpenRecordSet("SELECT Intervention.NoIntervention FROM Intervention WHERE Intervention.NumFicheStandard=" +
                    txtNumFicheStandard.Text + "");

                if (rsNbInterv.Rows.Count > 0)
                {
                    longNombreInterv = rsNbInterv.Rows.Count;
                }
                else
                {
                    longNombreInterv = 0;
                }

                modAdorsNbInterv.Close();

                if (longNombreInterv > 1)
                {
                    cmdPremier.Enabled = true;
                    cmdSuivant.Enabled = true;
                    cmdPrecedent.Enabled = true;
                    cmdDernier.Enabled = true;
                }
                else
                {
                    cmdPremier.Enabled = false;
                    cmdSuivant.Enabled = false;
                    cmdPrecedent.Enabled = false;
                    cmdDernier.Enabled = false;
                }
                //this.txtJobNumber.Text = Convert.ToString(longNombreInterv);
                txtPage.Text = Convert.ToString(1);

                //=== recherche du deviseur.
                if (General.sActiveVersionPz == "1")
                {
                    //=== version dédié à pz.
                    frame65.Visible = false;

                    General.sSQL = "SELECT DevisEntete.NumeroDevis,TelPart," + "DevisEntete.CodeDeviseur, DevisEntete.NoEnregistrement " + "FROM DevisEntete" +
                        " WHERE DevisEntete.NumeroDevis ='" + General.rstmp.Rows[0]["NoDevis"].ToString() + "'";

                }
                else
                {
                    frame65.Visible = true;

                    General.sSQL = "SELECT DevisEntete.NumeroDevis,TelPart," + "DevisEntete.CodeDeviseur " + "FROM DevisEntete" + " WHERE DevisEntete.NumeroDevis ='" +
                        General.rstmp.Rows[0]["NoDevis"].ToString() + "'";

                }

                General.modAdorstmp.Close();
                General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);

                if (General.rstmp.Rows.Count > 0)
                {
                    txtDeviseur2.Value = General.rstmp.Rows[0]["CodeDeviseur"].ToString() + "";
                    using (var tmpModAdo = new ModAdo())
                        txtDeviseur.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + StdSQLchaine.gFr_DoublerQuote(General.rstmp.Rows[0]["CodeDeviseur"].ToString()) + "'");

                    if (General.sActiveVersionPz == "1")
                    {
                        txtNoEnregistrement.Text = General.rstmp.Rows[0]["NoEnregistrement"].ToString() + "";
                        if (!string.IsNullOrEmpty(txtNoEnregistrement.Text))
                        {
                            frame65.Visible = true;
                        }
                    }
                    if (string.IsNullOrEmpty(txtTelephoneContact.Text))
                    {
                        txtTelephoneContact.Text = General.rstmp.Rows[0]["TelPart"].ToString();
                    }

                }
                General.modAdorstmp.Close();


                General.rstmp = General.modAdorstmp.fc_OpenRecordSet("SELECT TechReleve.Obs, Immeuble.Observations as [ComImmeuble], Table1.Observations as [ComGerant] " +
                    "FROM (Table1 RIGHT JOIN Immeuble ON Table1.Code1 = Immeuble.Code1) LEFT JOIN TechReleve ON Immeuble.CodeImmeuble = TechReleve.CodeImmeuble where " +
                    "Immeuble.CodeImmeuble ='" + txtCodeImmeuble.Text + "'");
                if (General.rstmp.Rows.Count > 0)
                {
                    if (!string.IsNullOrEmpty(General.nz(General.rstmp.Rows[0]["Obs"], "").ToString()))
                    {
                        Command2.BackColor = cFindColor;
                    }
                    else if (!string.IsNullOrEmpty(General.nz(General.rstmp.Rows[0]["ComImmeuble"], "").ToString()))
                    {
                        Command2.BackColor = cFindColor;
                    }
                    else if (!string.IsNullOrEmpty(General.nz(General.rstmp.Rows[0]["ComGerant"], "").ToString()))
                    {
                        Command2.BackColor = cFindColor;
                    }
                    else
                    {
                        Command2.BackColor = cNotFoundColor1;
                    }
                }
                else
                {
                    Command2.BackColor = cNotFoundColor1;
                }
                General.modAdorstmp.Close();

                // Me.txtINT_AnaCode = nz(rsTmp!INT_AnaCode, "")
                if (string.IsNullOrEmpty(txtselCAI_code.Text))
                {
                    using (var tmpModAdo = new ModAdo())
                        txtselCAI_code.Text = tmpModAdo.fc_ADOlibelle("SELECT cai_code" + " From gestionstandard" + " WHERE numfichestandard =" + txtNumFicheStandard.Text);
                }

                fc_RechercheImmeuble(txtCodeImmeuble.Text);

                //            FC_intervenant txtCodeImmeuble
                //            fc_FindIntervenant txtCodeImmeuble

                txtCommentaire_TextChanged(txtCommentaire, new System.EventArgs());
                txtCommentaire_Leave(txtCommentaire, new System.EventArgs());

                if ((General.rstmp != null))
                {
                    General.modAdorstmp.Dispose();
                }

                //============> Mondir 04.06.2020 : RefOS Forgoten after adding modification !
                General.sSQL = "SELECT GestionStandard.Document, CodeOrigine1, RefOS FROM GestionStandard " + " WHERE GestionStandard.NumFicheStandard = " + txtNumFicheStandard.Text;

                General.modAdorstmp = new ModAdo();
                General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);

                if (General.rstmp.Rows.Count > 0)
                {
                    if (General.rstmp.Rows[0]["Document"] == DBNull.Value || string.IsNullOrEmpty(General.rstmp.Rows[0]["Document"].ToString()))
                    {
                        btnOrigine.BackColor = cNotFoundColor2;
                    }
                    else
                    {
                        btnOrigine.BackColor = cFindColor;
                    }
                    fc_origine(General.nz(General.rstmp.Rows[0]["CodeOrigine1"], "").ToString());

                    if (General.rstmp.Rows[0]["RefOS"] != DBNull.Value || !string.IsNullOrEmpty(General.rstmp.Rows[0]["RefOS"].ToString()))
                    {
                        cmd2.BackColor = cNotFoundColor2;
                    }
                    else
                    {
                        cmd2.BackColor = cFindColor;
                    }
                }

                General.modAdorstmp.Close();


                General.sSQL = "SELECT DevisEntete.NumeroDevis, " + "DevisEntete.CodeImmeuble " + "FROM DevisEntete " + " WHERE DevisEntete.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";

                General.modAdorstmp = new ModAdo();
                General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);
                if (General.rstmp.Rows.Count > 0)
                {
                    this.btnHistoDevis.BackColor = cFindColor;
                }
                else
                {
                    this.btnHistoDevis.BackColor = cNotFoundColor1;
                }
                General.modAdorstmp.Close();

                if (string.IsNullOrEmpty(txtCial.Text))
                {
                    General.sSQL = "SELECT Table1.commercial FROM Table1 WHERE Table1.Code1='" + txtCode1.Text + "'";

                    General.modAdorstmp = new ModAdo();
                    General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);
                    if (General.rstmp.Rows.Count > 0)
                    {
                        txtCial2.Value = General.rstmp.Rows[0]["Commercial"].ToString() + "";
                        if (General.rstmp.Rows[0]["Commercial"] != DBNull.Value && !string.IsNullOrEmpty(General.rstmp.Rows[0]["Commercial"].ToString()))
                        {
                            using (var tmpModAdo = new ModAdo())
                                txtCial.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + txtCial2.Value + "'");
                        }
                    }
                }

                General.sSQL = "SELECT Intervention.NumFicheStandard FROM Intervention WHERE Intervention.NumFicheStandard=" + txtNumFicheStandard.Text +
                    " AND (Intervention.CodeEtat='ZZ' OR Intervention.CodeEtat='AF' OR Intervention.CodeEtat='E' OR Intervention.CodeEtat='S' OR Intervention.CodeEtat='T')";
                General.modAdorstmp = new ModAdo();
                General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);
                if (General.rstmp.Rows.Count > 0)
                {
                    cmdAnalytique.BackColor = cFindColor;
                }
                else
                {
                    cmdAnalytique.BackColor = cNotFoundColor2;
                }

            }
            else
            {

            }

            if (General.sActiveVersionPz == "1")
            {
                //=== ne fait rien.
            }
            else
            {
                fc_Dev();
            }

            // initialise à blanc tous les champs
            // fc_clear
            blnEntree = false;

            //   End If
            //  blModif = True
        }

        private string GetInterventionPosition(int longNoIntervention)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"GetInterventionPosition() - Enter In The Function - longNoIntervention = {longNoIntervention}");
            //===> Fin Modif Mondir
            ModAdo modAdorsNbFact = new ModAdo();
            dtInterventionsByAppel = modAdorsNbFact.fc_OpenRecordSet($"SELECT * FROM Intervention WHERE NumFicheStandard= '{txtNumFicheStandard.Text }' order by NoIntervention ASC");
            modAdorsNbFact.Close();

            var position = 0;
            if (dtInterventionsByAppel.Rows.Count > 0)

                position = dtInterventionsByAppel
                .AsEnumerable()
                .Select(row => row) // ie. project the col(s) needed
                .ToList()
                .FindIndex(x => x["NoIntervention"].ToString() == longNoIntervention.ToString()) + 1;

            return position + "/" + dtInterventionsByAppel.Rows.Count;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_RechercheImmeuble(string sCode)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_RechercheImmeuble() - Enter In The Function - sCode = {sCode}");
            //===> Fin Modif Mondir
            short i = 0;

            General.sSQL = "";
            //===> Mondir le 19.10.2020, j'ai ajouté la colonne CodeRespExploit pour corriger CodeRespExploit
            General.sSQL = "SELECT CodeRespExploit, Immeuble.Adresse, Immeuble.Adresse2_IMM," + "Immeuble.CodePostal, Immeuble.Ville," + "  Immeuble.Observations, Immeuble.ObservationComm_IMM," + " Immeuble.RaisonSocial_IMM, Immeuble.codeAcces1, Immeuble.CodeAcces2," + " Immeuble.code1, Immeuble.sec_code," + " Immeuble.CodeImmeuble, Immeuble.CS1, Immeuble.Gardien, " + " Immeuble.CodeDepanneur, Immeuble.BoiteAClef, Immeuble.SitBoiteAClef, " + " Immeuble.SpecifAcces, Immeuble.TelGardien, Immeuble.CodeCommercial, " + " Immeuble.CodeDispatcheur, Immeuble.CodeDepanneur, Immeuble.CodeChefSecteur, " + " Immeuble.CodeDepanneur1,AngleRue,immeuble.P1,immeuble.P2,immeuble.P3,immeuble.P4 " + " , immeuble.DTAPositif " + " From Immeuble" + " WHERE Immeuble.CodeImmeuble ='" + sCode + "'";
            General.modAdorstmp = new ModAdo();
            General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);

            if (General.rstmp.Rows.Count > 0)
            {

                this.chkDTAPositif.CheckState = General.nz(General.rstmp.Rows[0]["DTAPositif"], 0).ToString() == "1" ? CheckState.Checked : CheckState.Unchecked;
                if ((int)chkDTAPositif.CheckState == 1)
                {
                    lblDTAPositif.Visible = true;
                }
                else
                {
                    lblDTAPositif.Visible = false;
                }
                this.txtAdresse.Text = General.nz(General.rstmp.Rows[0]["Adresse"], "").ToString();
                //            Me.txtAdresse2_IMM = nz(rstmp!Adresse2_IMM, "")
                this.txtAdresse2_IMM.Text = General.nz(General.rstmp.Rows[0]["AngleRue"], "").ToString();
                this.txtCodePostal.Text = General.nz(General.rstmp.Rows[0]["CodePostal"], "").ToString();
                this.txtVille.Text = General.nz(General.rstmp.Rows[0]["Ville"], "").ToString();
                this.txtCode1.Text = General.nz(General.rstmp.Rows[0]["Code1"], "").ToString();
                this.txtRaisonSocial_IMM.Text = General.nz(General.rstmp.Rows[0]["RaisonSocial_IMM"], "").ToString();
                this.txtCodeAcces1.Text = General.nz(General.rstmp.Rows[0]["codeAcces1"], "").ToString();
                this.txtCodeAcces2.Text = General.nz(General.rstmp.Rows[0]["CodeAcces2"], "").ToString();
                this.txtGardien.Text = General.nz(General.rstmp.Rows[0]["Gardien"], "").ToString();
                this.txtSpecifAcces.Text = General.nz(General.rstmp.Rows[0]["SpecifAcces"], "").ToString();
                this.txtTel.Text = General.nz(General.rstmp.Rows[0]["TelGardien"], "").ToString();
                this.txtSitBoite.Text = General.nz(General.rstmp.Rows[0]["SitBoiteAClef"], "").ToString();
                using (var tmpModAdo = new ModAdo())
                    this.txtCial.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" +
                        General.nz(StdSQLchaine.gFr_DoublerQuote(General.rstmp.Rows[0]["CodeCommercial"].ToString()), "") + "'");
                this.CHKP1.CheckState = (General.nz(General.rstmp.Rows[0]["P1"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["P1"], "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked);
                this.CHKP2.CheckState = (General.nz(General.rstmp.Rows[0]["P2"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["P2"], "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked);
                this.CHKP3.CheckState = (General.nz(General.rstmp.Rows[0]["P3"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["P3"], "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked);
                this.CHKP4.CheckState = (General.nz(General.rstmp.Rows[0]["P4"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["P4"], "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked);

                //            If IsNull(Me.txtDispatcheur.value) And Me.txtDispatcheur.value = "" Then
                //                Me.txtDispatcheur.value = nz(rsTmp!CodeDispatcheur, "")
                //                Me.txtLibDispatch.Text = fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel XHERE Personnel.Matricule='" & nz(rsTmp!CodeDispatcheur, "") & "'")
                //            End If
                using (var tmpModAdo = new ModAdo())
                    this.txtDepan.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + General.nz(StdSQLchaine.gFr_DoublerQuote(General.rstmp.Rows[0]["CodeDepanneur"].ToString()), "") + "'");
                //            If txtMatriculeIntervenant.Text = "" Then
                //                'Me.txtIntervenant.value = fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" & nz(rstmp!CodeDepanneur, "") & "'")
                //                Me.txtIntervenant.value = nz(rstmp!CodeDepanneur, "")
                //                Me.txtMatriculeIntervenant.Text = nz(rstmp!CodeDepanneur, "")
                //            End If
                using (var tmpModAdo = new ModAdo())
                using (var tmpModAdo2 = new ModAdo())
                    this.txtChefSecteur.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Initiales='" +
                        tmpModAdo2.fc_ADOlibelle("SELECT Personnel.CSecteur FROM Personnel WHERE Personnel.Matricule='" +
                        General.nz(General.rstmp.Rows[0]["CodeDepanneur"], "") + "'") + "'");
                //            If rstmp!CodeDepanneur1 <> "" Then
                using (var tmpModAdo = new ModAdo())
                using (var tmpModAdo2 = new ModAdo())
                    this.txtDepan2.Text = tmpModAdo2.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Initiales='" +
                        tmpModAdo.fc_ADOlibelle("SELECT Personnel.Dep2 FROM Personnel WHERE Personnel.Matricule='" + General.nz(StdSQLchaine.gFr_DoublerQuote(General.rstmp.Rows[0]["CodeDepanneur"].ToString()), "") + "'") + "'");
                //==modif rachid du 28 DEC 2004, ajout du champs responsable et rondier

                //===> Mondir le 19.10.2020 https://groupe-dt.mantishub.io/view.php?id=2042 ==> affiché CodeRespExploit  au lieu de CodeDepanneur
                using (var tmpModAdo = new ModAdo())
                {
                    using (var tmpModAdo2 = new ModAdo())
                    {
                        txtRespExpl.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Initiales='" +
                            tmpModAdo2.fc_ADOlibelle("SELECT Personnel.CRespExploit FROM Personnel WHERE Personnel.Matricule='" +
                            General.nz(StdSQLchaine.gFr_DoublerQuote(General.rstmp.Rows[0]["CodeRespExploit"].ToString()), "") + "'") + "'");
                    }
                }
                //txtRespExpl.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Initiales='" +
                //    tmpModAdo2.fc_ADOlibelle("SELECT Personnel.CRespExploit FROM Personnel WHERE Personnel.Matricule='" +
                //    General.nz(StdSQLchaine.gFr_DoublerQuote(General.rstmp.Rows[0]["CodeDepanneur"].ToString()), "") + "'") + "'");
                //===> Fin Modif Mondir

                using (var tmpModAdo = new ModAdo())
                using (var tmpModAdo2 = new ModAdo())
                    txtRondier.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Initiales='" +
                        tmpModAdo2.fc_ADOlibelle("SELECT Personnel.Rondier FROM Personnel WHERE Personnel.Matricule='" + General.nz(StdSQLchaine.gFr_DoublerQuote(General.rstmp.Rows[0]["CodeDepanneur"].ToString()), "") + "'") + "'");
                //====
                //            Else
                //                Me.txtDepan2 = fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" & nz(rstmp!Codedepanneur, "") & "'")
                //            End If

                if (General.rstmp.Rows[0]["BoiteAClef"] != DBNull.Value && Convert.ToBoolean(General.rstmp.Rows[0]["BoiteAClef"]))
                {
                    this.chkBoite.CheckState = System.Windows.Forms.CheckState.Checked;
                }
                else
                {
                    this.chkBoite.CheckState = System.Windows.Forms.CheckState.Unchecked;
                }

                if (string.IsNullOrEmpty(this.txtINT_AnaCode.Text))
                {
                    this.txtINT_AnaCode.Text = General.nz(General.rstmp.Rows[0]["SEC_Code"], "").ToString();
                }

                //            If Not IsNull(rstmp!CodeImmeuble) And Not rstmp!CodeImmeuble = "" Then
                //                sSQl = "SELECT ImmeublePart.Nom FROM ImmeublePart WHERE ImmeublePart.CodeImmeuble='" & rstmp!CodeImmeuble & "'"
                //                rstmp.Close
                //                Set rstmp = fc_OpenRecordSet(sSQl)
                //                i = 0
                //                If Not rstmp.EOF And Not rstmp.BOF Then
                //                    rstmp.MoveFirst
                //                    While Not rstmp.EOF
                //                        txtParticulier.Text = txtParticulier.Text & rstmp!Nom & vbCrLf
                //                        rstmp.MoveNext
                //                    Wend
                //                End If
                //            End If

                General.modAdorstmp.Close();

                General.sSQL = "";
                General.sSQL = "SELECT TechReleve.CodeImmeuble FROM TechReleve WHERE TechReleve.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
                General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);

                if (General.rstmp.Rows.Count > 0)
                {
                    cmd2.BackColor = cFindColor;
                }
                else
                {
                    cmd2.BackColor = cNotFoundColor2;
                }

                General.modAdorstmp.Close();

                General.sSQL = "";
                General.sSQL = "SELECT Nom_IMC FROM IMC_ImmCorrespondant WHERE CodeImmeuble_IMM='" + sCode + "' AND PresidentCS_IMC='-1'";
                General.modAdorstmp = new ModAdo();
                General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);

                if (General.rstmp.Rows.Count > 0)
                {
                    this.txtCodeParticulier.Text = General.rstmp.Rows[0]["nom_imc"] + "";
                }
                General.modAdorstmp.Close();

                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    fc_RechercheContrat(txtCodeImmeuble.Text);
                }
            }
            else
            {
                this.txtRaisonSocial_IMM.Text = "";
                this.txtAdresse.Text = "";
                this.txtAdresse2_IMM.Text = "";
                this.txtCodePostal.Text = "";
                this.txtVille.Text = "";
                this.txtCode1.Text = "";
                this.txtCodeAcces1.Text = "";
                this.txtCodeAcces2.Text = "";
                this.txtGardien.Text = "";
                this.txtSpecifAcces.Text = "";
                this.txtTel.Text = "";
                this.txtSitBoite.Text = "";
                this.chkBoite.CheckState = System.Windows.Forms.CheckState.Unchecked;
                this.txtCial.Text = "";
                this.txtDispatcheur.Text = "";
                this.txtDepan.Text = "";
                this.txtChefSecteur.Text = "";
                this.txtDepan2.Text = "";
                this.CHKP1.CheckState = System.Windows.Forms.CheckState.Unchecked;
                this.CHKP2.CheckState = System.Windows.Forms.CheckState.Unchecked;
                this.CHKP3.CheckState = System.Windows.Forms.CheckState.Unchecked;
                this.CHKP4.CheckState = System.Windows.Forms.CheckState.Unchecked;
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        private void fc_RechercheContrat(string sCodeImmeuble)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_RechercheContrat() - Enter In The Function - sCodeImmeuble = {sCodeImmeuble}");
            //===> Fin Modif Mondir
            try
            {
                string LenNumContrat = null;

                //        LenNumContrat = fc_ADOlibelle("SELECT MAX(LEN(NUMCONTRAT)) AS MaxNum FROM Contrat WHERE CodeImmeuble='" & sCodeImmeuble & "'")

                //        sSQl = ""
                //        sSQl = "SELECT MAX(Contrat.NumContrat) AS MaxiNum, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1 " _
                //'                & " From Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle "
                //            sSQl = sSQl & " WHERE Contrat.CodeImmeuble like '" & sCodeImmeuble & "%' AND Avenant=0"
                //            sSQl = sSQl & " AND LEN(NumContrat)=" & nz(LenNumContrat, "''")
                //            sSQl = sSQl & " GROUP BY Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1"

                General.sSQL = "";

                //=== modif du 11 juin 2018.
                //sSQL = "SELECT MAX(Contrat.DateEffet) as MaxiDate,Contrat.NumContrat AS MaxiNum, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1 " _
                //& " From Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle "
                //sSQL = sSQL & " WHERE Contrat.CodeImmeuble like '" & sCodeImmeuble & "%' AND Avenant=0"
                //sSQL = sSQL & " GROUP BY Contrat.NumContrat, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1"

                General.sSQL = "SELECT MAX(Contrat.DateEffet) as MaxiDate,Contrat.NumContrat AS MaxiNum, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1 " + " From Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle ";
                General.sSQL = General.sSQL + " WHERE Contrat.CodeImmeuble = '" + sCodeImmeuble + "' AND Avenant=0";
                General.sSQL = General.sSQL + " GROUP BY Contrat.NumContrat, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1";

                General.modAdorstmp = new ModAdo();
                General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);

                if (General.rstmp.Rows.Count > 0)
                {
                    txtcontrat.Text = General.rstmp.Rows[0]["Designation1"] + "";
                    txtCodeContrat.Text = General.rstmp.Rows[0]["MaxiNum"] + "";
                }
                else
                {
                    txtcontrat.Text = "";
                    txtCodeContrat.Text = "";
                }

                General.modAdorstmp.Close();

                //Si j'ai un contrat, je vérifie qu'il n'est pas résilié
                if (!string.IsNullOrEmpty(txtCodeContrat.Text))
                {
                    // sSQL = "SELECT Contrat.NumContrat,FacArticle.Designation1 FROM Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle " _
                    //& " WHERE (Contrat.Resiliee=0 OR (Contrat.Resiliee=1 AND (Contrat.DateFin>'" & Date & "' OR Contrat.DateFin IS NULL)))" _
                    //& " AND Contrat.CodeImmeuble='" & sCodeImmeuble & "' order by avenant"

                    //=== modif du 09 01 2017, modif sur critéres.
                    //===> Mondir le 02.04.2021, code moved to ModContrat https://groupe-dt.mantishub.io/view.php?id=2306
                    //General.sSQL = "SELECT Contrat.NumContrat,FacArticle.Designation1 FROM Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle " +
                    //    " WHERE (Contrat.Resiliee=0 OR (Contrat.Resiliee=1 AND (Contrat.DateFin>'" + DateTime.Today.ToString(General.FormatDateSQL) + "')))" +
                    //    " AND Contrat.CodeImmeuble='" + sCodeImmeuble + "' order by avenant";

                    //General.modAdorstmp = new ModAdo();
                    //General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);
                    General.rstmp = ModContrat.fc_GetActiveContract(sCodeImmeuble);
                    //===> Fin Modif Mondir

                    if (General.rstmp.Rows.Count > 0)
                    {
                        txtcontrat.Text = General.rstmp.Rows[0]["Designation1"] + "";
                        txtCodeContrat.Text = General.rstmp.Rows[0]["NumContrat"] + "";
                        lblContratResilie.Visible = false;
                        txtcontrat.ForeColor = System.Drawing.ColorTranslator.FromOle(0xff0000);
                    }
                    else
                    {
                        lblContratResilie.Visible = true;
                        txtcontrat.ForeColor = System.Drawing.ColorTranslator.FromOle(0xc0);
                    }
                }

                //===> Mondir le 04.06.2021, https://groupe-dt.mantishub.io/view.php?id=2445#c6096
                checkContrat();
                //===> Fin Modif Mondir

                General.modAdorstmp?.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "RechercheContrat");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCommentaire_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtCommentaire_TextChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            int lngSMS = 0;

            lngSMS = (LongNoIntervention.ToString("#####00000") + "/" + General.Left(txtMemoGuard.Text, 4) + "/" + General.Left(txtAdresse.Text, 23)
                + "/" + General.Left(txtCodePostal.Text, 5) + "/" + General.Left(txtCodeAcces1.Text, 8) + "/" + General.Left(txtVille.Text, 10) +
                "/" + General.Left(txtSituation.Text, 12) + "/" + General.Left(txtDesignation.Text, 25) + "/").Length;

            var diff = 160 - lngSMS - txtCommentaire.Text.Length;
            if (General.IsNumeric(diff.ToString()))
            {
                if (Convert.ToInt32(diff) >= 0)
                {
                    //        Frame12.ForeColor = &H80000008
                    Frame12.Text = "Commentaires => " + Convert.ToInt32(diff) + " caractères restants pour le SMS";
                }
                else
                {
                    //        Frame12.ForeColor = &HC0&
                    Frame12.Text = "Commentaires => les " + System.Math.Abs(Convert.ToInt32(diff)) + " derniers caractères ne seront pas dans le SMS";
                }
            }


        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCommentaire_Leave(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtCommentaire_Leave() - Enter In The Function");
            //===> Fin Modif Mondir
            int lngSMS = 0;

            lngSMS = (LongNoIntervention.ToString("#####00000") + "/" + General.Left(txtMemoGuard.Text, 4) + "/" + General.Left(txtAdresse.Text, 23) +
                "/" + General.Left(txtCodePostal.Text, 8) + "/" + General.Left(txtCodeAcces1.Text, 5) + "/" + General.Left(txtVille.Text, 10) + "/" + General.Left(txtSituation.Text, 12) +
                "/" + General.Left(txtDesignation.Text, 25) + "/").Length;

            if (txtCommentaire.Text.Length > 160 - lngSMS)
            {
                txtCommentaire.iTalkTB.Select(160 - lngSMS, txtCommentaire.Text.Length - 160 - lngSMS);
            }
            ToolTip1.SetToolTip(txtCommentaire.iTalkTB, (160 - lngSMS) + " caractères disponibles pour le SMS");

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_origine(string sCode)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_origine() - Enter In The Function - sCode = {sCode}");
            //===> Fin Modif Mondir
            // positionne les boutons options
            // en donnant la valeur true au bouton option on active l'événement click de celui ci,
            // dans cet événement une fonction est lancée, mais si ce bouton a déja la valeur true
            // l'événement ne sera pas activé, pour le forcer on initialise sa valeur à false.

            try
            {
                var _with25 = this;

                if (sCode == Convert.ToString(General.cTel))
                {
                    btnOrigine.Text = General.cTelLbl;
                }
                else if (sCode == Convert.ToString(General.cFax))
                {
                    btnOrigine.Text = General.cFaxlbl;
                }
                else if (sCode == Convert.ToString(General.cInternet))
                {
                    btnOrigine.Text = General.cInternetlbl;
                }
                else if (sCode == Convert.ToString(General.cRepondeur))
                {
                    btnOrigine.Text = General.cRepondeurlbl;
                }
                else if (sCode == Convert.ToString(General.cCourrier))
                {
                    btnOrigine.Text = General.cCourrierlbl;
                }
                else if (sCode == Convert.ToString(General.cRadio))
                {
                    btnOrigine.Text = General.cRadiolbl;
                }
                else if (sCode == Convert.ToString(General.cDirect))
                {
                    btnOrigine.Text = General.cDirectlbl;
                }
                else if (sCode == Convert.ToString(General.CEmail))
                {
                    btnOrigine.Text = General.cEmaillbl;
                }
                else if (sCode == Convert.ToString(General.CAstreinte))
                {
                    btnOrigine.Text = "Astreinte";
                }
                else
                {
                    btnOrigine.Text = General.cInconnulbl;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " fc_Option ");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Dev()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_Dev() - Enter In The Function");
            //===> Fin Modif Mondir
            string sSQL = null;


            lblRenovation.Visible = false;
            if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
            {

                sSQL = "SELECT NumeroDevis  From DevisEnTete " + " WHERE CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'" + " AND (RenovationChauff = 1) ";
                using (var tmpModAdo = new ModAdo())
                    sSQL = tmpModAdo.fc_ADOlibelle(sSQL);
                if (!string.IsNullOrEmpty(sSQL))
                {
                    lblRenovation.Visible = true;
                }

            }



        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_CtrlDataPDA()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_CtrlDataPDA() - Enter In The Function");
            //===> Fin Modif Mondir
            string sMessage = null;
            string sOldCodeEtat = null;
            bool bSendPda = false;

            if (General.sActiveCtlSatutInter != "1")
            {
                return;
            }


            try
            {
                //=== controle si l'intervention a été envoyé au moins une fois
                //=== et qu'elle n'a pas été terminé (si MajCorParPDA = 1 indique que l'intervention
                //=== a été terminé au moins une fois).
                //+=== txt(7) => EnvoyeLe, txt(8) => MajCorParPDA.

                sOldCodeEtat = txt4.Text;

                //===> Mondir le 26.03.2021, i moved this code to fc_sauver()
                //=== controle si le statut a été modifié.
                //if (sOldCodeEtat.ToUpper() != txtCodeEtat.Text.ToUpper())
                //{

                //    if (CModCodeEtat.fc_CtrlCodeEtat(sOldCodeEtat, txtCodeEtat.Text, ref bSendPda) == false)
                //    {

                //        sMessage = "Attention le changement du statut " + sOldCodeEtat + " en " + txtCodeEtat.Text + " n'est pas autorisé, le statut. Le statut va reprendre la valeur " + sOldCodeEtat + ".";
                //        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMessage, "Opération non autorisée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //        txtCodeEtat.Text = txtOldCodeEtat.Text;

                //        return;
                //    }
                //}


                //===txt(7) = EnvoyeLe
                //====txt(8) = MajCorParPDA
                // see issue https://groupe-dt.mantishub.io/view.php?id=1411
                string[] statusAutoriserMajPda = { "0A", "01", "02" };

                //===> Mondir le 14.10.2020, commented the line bellow, to remote the condition General.nz(txt8.Text, 0).ToString() == "0"
                //===> didnt get a reply from Rachid about the column MajCorParPDA ==> MajCorParPDA
                //if (statusAutoriserMajPda.Contains(txtCodeEtat.Text) && General.IsDate(txt7.Text) && General.nz(txt8.Text, 0).ToString() == "0")
                if (statusAutoriserMajPda.Contains(txtCodeEtat.Text) && General.IsDate(txt7.Text))
                {
                    sOldCodeEtat = txt4.Text;
                    //=== controle si le statut a été modifié.
                    if (sOldCodeEtat.ToUpper() != txtCodeEtat.Text.ToUpper())
                    {

                        if (CModCodeEtat.fc_CtrlCodeEtat(sOldCodeEtat, txtCodeEtat.Text, ref bSendPda) == false)
                        {

                            sMessage = "Attention le changement du statut " + sOldCodeEtat + " en " + txtCodeEtat.Text + " n'est pas autorisé.";
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMessage, "Opération non autorisée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtCodeEtat.Text = txtOldCodeEtat.Text;
                            return;
                        }

                        if (txtCodeEtat.Text.ToUpper() == "ZZ".ToUpper())
                        {
                            //=== l'intervention a été envoyé sur un PDA, le code
                            //=== état ZZ annule l'intervention sur le PDA.
                            sMessage = "Attention cette intervention a été transmise sur le PDA de " + fc_NomInterv() + "," + " si vous affectez le statut ZZ à cette intervention, celle-ci sera automatiquement supprimée du PDA." + "\n" +
                                "Voulez-vous continuer ?";

                            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMessage, "PDA", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                //=== indique que l'inter est envoéy sur le PDA.
                                lEtatEnvoie = 3;
                                //=== envoie une demande de suppression de l'intervention.
                                fc_sendPDAcorrective(Convert.ToInt32(txtNoIntervention.Text), true, DateTime.Now, "");
                                //txt(4) = txtCodeEtat
                                fc_InitMajPDA();
                                return;

                            }
                            else
                            {
                                txtCodeEtat.Text = txt4.Text;
                                //=== envoie annulé par l'utilisateur.
                                lEtatEnvoie = 2;
                            }
                        }
                    }

                    //==== controle si l'intervenant a été modifié.
                    if (txt9.Text.ToUpper() != txtIntervenant.Text.ToUpper())
                    {

                        //=== l'intervention a été envoyé sur un PDA, le code
                        //=== état ZZ annule l'intervention sur le PDA.
                        sMessage = "Attention cette intervention a été transmise sur le PDA de " + fc_NomInterv() + "," + " si vous changez d'intervenant celle-ci sera automatiquement supprimée du PDA et renvoyé sur le PDA du matricule" +
                            txtIntervenant.Text + "." + "\n" + "Voulez-vous continuer ?";

                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMessage, "PDA", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            //=== indique que l'inter est envoyé sur le PDA.
                            lEtatEnvoie = 3;

                            //=== envoie une demande de suppression de l'intervention.
                            fc_sendPDAcorrective(Convert.ToInt32(txtNoIntervention.Text), true, DateTime.Now, txt9.Text);
                            //fc_sendPDAcorrective txtNoIntervention, False, Now, ""
                            //=== modif du 19 10 2016, force l'intervenant.
                            fc_sendPDAcorrective(Convert.ToInt32(txtNoIntervention.Text), false, DateTime.Now, txtIntervenant.Text);
                            //txt(9) = txtIntervenant
                            fc_InitMajPDA();
                            return;
                        }
                        else
                        {
                            txtIntervenant.Text = txt9.Text;
                            //=== envoie annulé par l'utilisateur.
                            lEtatEnvoie = 2;
                        }

                    }

                    //=== contrôle si un des champs suivants a été modifiés : désignation, commentaire, heure prévue,
                    //=== date réalisée.
                    if (txt5.Text != txtDesignation.Text
                        || txt6.Text != txtCommentaire.Text
                        || txt10.Text != txtHeurePrevue.Text
                        || txt11.Text != txtDateRealise.Text
                        || txt12.Text != txtDateVisite.Text
                        || txt13.Text != txtHeurePrevue.Text)
                    {


                        sMessage = "Attention cette intervention a été transmise sur le PDA de " + fc_NomInterv().Trim() + "," +
                            " vous venez de modifier cette intervention, pour afficher ces modifications sur le PDA," + "\n" +
                            " celle-ci doit être  retransmise." + "\n\n" + "Voulez-vous retransmettre cette intervention ?";

                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMessage, "PDA", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            //=== indique que l'inter est envoéy sur le PDA.
                            lEtatEnvoie = 3;

                            fc_sendPDAcorrective(Convert.ToInt32(txtNoIntervention.Text), false, DateTime.Now, "");
                            fc_InitMajPDA();
                            return;
                        }
                        else
                        {
                            fc_InitMajPDA();
                            //=== envoie annulé par l'utilisateur.
                            lEtatEnvoie = 2;
                        }

                    }
                }

                //If Date = #6/17/2016# And UCase(fncUserName) = UCase("rachid abbouchi") Then
                //    Exit Sub
                //End If
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_CtrlDataPDA");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void RechercheLibelleEtat()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"RechercheLibelleEtat() - Enter In The Function");
            //===> Fin Modif Mondir
            DataTable adotemp = default(DataTable);
            ModAdo modAdoadotemp = null;
            string strSql = null;

            try
            {
                if (!string.IsNullOrEmpty(txtCodeEtat.Text))
                {
                    modAdoadotemp = new ModAdo();
                    var _with8 = adotemp;
                    strSql = "SELECT TypeCodeEtat.LibelleCodeEtat From TypeCodeEtat Where TypeCodeEtat.CodeEtat ='" + txtCodeEtat.Text + "'";
                    _with8 = modAdoadotemp.fc_OpenRecordSet(strSql);
                    if (_with8.Rows.Count > 0)
                    {
                        lbllibcodeetat.Text = _with8.Rows[0]["LibelleCodeEtat"] + "";
                    }
                    else
                    {
                        lbllibcodeetat.Text = "";
                    }
                    modAdoadotemp.Close();
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";RechercheLibelleEtat");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        public void stockeAnalytique()
        {

            string sSqlAnalytique = null;




        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private string fc_NomInterv()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_NomInterv() - Enter In The Function");
            //===> Fin Modif Mondir
            string sNom = null;

            using (var tmpModAdo = new ModAdo())
                sNom = tmpModAdo.fc_ADOlibelle("SELECT     Personnel.Nom, Personnel.Prenom" + " FROM         INMT_MsgTomtom LEFT OUTER JOIN " + " Personnel ON INMT_MsgTomtom.Matricule = Personnel.Matricule " +
                    " Where INMT_MsgTomtom.Nointervention = " + General.nz(txtNoIntervention.Text, 0) + " ORDER BY INMT_MsgTomtom.INMT_DateEnvoie DESC", true);

            return sNom;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lNoIntervention"></param>
        /// <param name="bModeSuppession"></param>
        /// <param name="dtNow"></param>
        /// <param name="sMatImposer"></param>
        /// <returns></returns>
        private bool fc_sendPDAcorrective(int lNoIntervention, bool bModeSuppession, System.DateTime dtNow, string sMatImposer)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_sendPDAcorrective() - Enter In The Function - lNoIntervention = {lNoIntervention} - bModeSuppession = {bModeSuppession} - dtNow = {dtNow} - sMatImposer = {sMatImposer}");
            //===> Fin Modif Mondir
            bool functionReturnValue = false;

            string sCodeEtat = null;
            string sSQL = null;
            DataTable rsSend = default(DataTable);
            ModAdo modAdorsSend = null;

            try
            {
                functionReturnValue = true;

                sSQL = "SELECT  Intervention.NoIntervention, Intervention.Intervenant, Intervention.CodeImmeuble, " + " Immeuble.Adresse, Immeuble.Ville, Immeuble.CodePostal, " +
                    " Immeuble.AngleRue,immeuble.Latitude, immeuble.Longitude, Intervention.DateSaisie, Intervention.DatePrevue, Intervention.DateRealise," +
                    " Intervention.CodeEtat, Intervention.Article, Intervention.EnvoyePar, Intervention.EnvoyeLe, " + " Intervention.Designation , FacArticle.Urgent, Intervention.Commentaire, Intervention.HeurePrevue, " +
                    " Intervention.CompteurObli,  Intervention.Urgence, Intervention.HeurePrevue , Intervention.CodeEtatPDA, Intervention.TelephoneContact, " +
                    " Intervention.DateVisite, Intervention.Selection, Intervention.NumFicheStandard, MajCorParPDA, FacArticle.ALAR_Code, " +
                    " Intervention.DPeriodeDu, Intervention.DPeriodeAu , Intervention.ALAR_Code as ALAR_Code_Inter, Intervention.NoDecade, Intervention.Situation " +
                    " FROM         Intervention INNER JOIN " + " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN " + " FacArticle ON Intervention.Article = FacArticle.CodeArticle " +
                    " WHERE     Intervention.NoIntervention =" + lNoIntervention;

                modAdorsSend = new ModAdo();
                rsSend = modAdorsSend.fc_OpenRecordSet(sSQL);

                if (fc_cCtrlErrData(rsSend, bModeSuppession) == true)
                {
                    functionReturnValue = false;
                    return functionReturnValue;
                }



                if (bModeSuppession == true)
                {
                    //  fc_SendInterventionPDA rsID, bModeSuppession, TypeInterPdaCorrective
                    // rsID.Close
                    // Set rsID = Nothing
                    //=== controle si il s'agit d'une visite d'entretien.
                    if (txt3.Text == "1")
                    {
                        ModPDA.fc_SendInterventionPDA(modAdorsSend, bModeSuppession, ModPDA.TypeInterPdaPreventif, dtNow, true, sMatImposer, Variable.cUserIntervention);
                    }
                    else
                    {
                        ModPDA.fc_SendInterventionPDA(modAdorsSend, bModeSuppession, ModPDA.TypeInterPdaCorrective, dtNow, false, sMatImposer);
                    }

                }
                else
                {
                    //=== controle si il s'agit d'une visite d'entretien.
                    if (txt3.Text == "1")
                    {
                        rsSend.Rows[0]["Selection"] = 1;
                        var NoIntervention = rsSend.Rows[0]["NoIntervention"].ToString();
                        //modAdorsSend.Update();
                        int xx = General.Execute($"UPDATE Intervention SET Selection = 1 WHERE NoIntervention = {NoIntervention}");
                        ModPDA.fc_SendInterventionPDA(modAdorsSend, bModeSuppession, ModPDA.TypeInterPdaPreventif, dtNow, true, sMatImposer);
                    }
                    else
                    {
                        ModPDA.fc_SendInterventionPDA(modAdorsSend, bModeSuppession, ModPDA.TypeInterPdaCorrective, dtNow, false, sMatImposer);
                    }
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                //rsInter.Close
                //Set rsInter = Nothing

                Erreurs.gFr_debug(ex, this.Name + "fc_sendPDAcorrective");
                return functionReturnValue;
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="rs"></param>
        /// <param name="bModeSupp"></param>
        /// <returns></returns>
        private bool fc_cCtrlErrData(DataTable rs, bool bModeSupp = false)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_cCtrlErrData() - Enter In The Function - rs.Rows.Count = {rs?.Rows?.Count} - bModeSupp = {bModeSupp}");
            //===> Fin Modif Mondir
            bool functionReturnValue = false;
            //=== retourne true en cas d'erreur.
            try
            {
                functionReturnValue = false;

                if (rs.Rows.Count == 0)
                {

                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Une erreur est survenue, l'envoie vers le PDA est annulée, veuillez contacter votre éditeur.", "Erreur PDA", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = true;
                    return functionReturnValue;
                }

                if (rs.Rows[0]["HeurePrevue"] != DBNull.Value && (rs.Rows[0]["DatePrevue"] == DBNull.Value && rs.Rows[0]["DateRealise"] == DBNull.Value))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi une heure prévue sans saisir de date prévue, veuillez corriger cette anomalie.", "Intervention non transmise", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = true;
                    return functionReturnValue;
                }

                //If IsNull(rs!DatePrevue) And IsNull(rs!DateRealise) Then
                if (rs.Rows[0]["DateRealise"] == DBNull.Value && bModeSupp == false)
                {
                    //MsgBox "Vous devez saisir une date réalisée (ou une date prévue), veuillez corriger cette anomalie.", vbInformation, "Intervention non transmise"
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date réalisée, veuillez corriger cette anomalie.", "Intervention non transmise", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = true;
                    return functionReturnValue;
                }

                if (string.IsNullOrEmpty(General.nz(rs.Rows[0]["longitude"], "").ToString()) || string.IsNullOrEmpty(General.nz(rs.Rows[0]["latitude"], "").ToString()))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet immeuble n'est pas géo-localisé, vous ne pouvez pas transmettre cette intervention sur un PDA," + "\n" +
                        " veuillez-vous rendre sur la fiche immeuble pour géo-localiser cet immeuble.", "Intervention non transmise", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = true;
                    return functionReturnValue;
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                //Do While rs.EOF = False
                //   If nz(rs!NoPDA, 0) = 0 Then
                //       MsgBox "L'intervenant " & rs!Prenom & " " & rs!Nom & "n 'a pas de numéro de PDA,  l'envoie sur le PDA est annulée. ", vbInformation, "Opération annulée"
                //       fc_cCtrlErrData = True
                //       Exit Function
                //   End If
                //
                //   rs.MoveNext
                //Loop

                //=== mlo

                Erreurs.gFr_debug(ex, this.Name + ";fc_cCtrlErrData");
                return functionReturnValue;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCpteRendu_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"btnCpteRendu_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            General.saveInReg(Variable.cUserDocRelTech, "NewVar", txtCodeImmeuble.Text);
            General.saveInReg(Variable.cUserDocRelTech, "Origine", this.Name);
            View.Theme.Theme.Navigate(typeof(UserDocRelTech));

            //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocRelTech);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void btnDebourse_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"btnDebourse_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                ReportDocument cr = new ReportDocument();
                if (cr.FileName.ToUpper() != General.strDTDebourseAffaire.ToUpper())
                    cr.Load(General.gsRpt + General.strDTDebourseAffaire);
                string SelectionFormula = "{DevisEnTete.NumeroDevis} ='" + txtNoDevis.Text + "'";
                CrystalReportFormView ReportForm = new CrystalReportFormView(cr, SelectionFormula);
                ReportForm.Show();

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";btnDebourse_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void btnDevis_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"btnDevis_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            bool bcontrole = false;
            bcontrole = false;
            if (btnHistoDevis.BackColor == cFindColor)
            {
                if (fc_sauver(ref bcontrole) == true)
                {
                    if (!string.IsNullOrEmpty(txtNoDevis.Text))
                    {
                        // stocke les paramétres pour la feuille de destination.
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, txtNoDevis.Text);
                        View.Theme.Theme.Navigate(typeof(UserDocDevis));

                    }
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFacture_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"btnFacture_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            DataTable adotemp = default(DataTable);
            ModAdo modAdoadotemp = null;
            int i = 0;
            string sCrystalOld = null;
            string sCrystal = null;

            try
            {
                //Dim strNoFacture As String
                //    On Error GoTo err

                //    Set adotemp = New ADODB.Recordset
                //    adotemp.Open "Select NoFacture from intervention where NoIntervention = " & LongNoIntervention, adocnn
                //    If adotemp.EOF = False And adotemp.BOF = False Then
                //        '----Si le numero de facture commence par XX, c'est qu'il
                //        '----s'agit d'une facture temporaire.
                //        If Not (IsNull(adotemp!NoFacture)) And adotemp!NoFacture <> "" Then
                //
                //            If Left(adotemp!NoFacture, 2) <> "XX" Then
                //
                //                strNoFacture = adotemp!NoFacture & ""
                //                adotemp.Close
                //                adotemp.Open "SELECT DateFacture" _
                //'                    & " From FactureManuelleEntete" _
                //'                    & " WHERE NoFacture ='" & strNoFacture & "'", adocnn
                //                If adotemp!DateFacture < #6/15/2001# Then
                //                    cr.ReportFileName = ETATOLDFACTUREMANUEL
                //                Else
                //                    cr.ReportFileName = ETATNEWFACTUREMANUEL
                //                End If
                //                cr.SelectionFormula = "{FactureManuelleEntete.NoFacture}='" & strNoFacture & "'"
                //                cr.Destination = crptToWindow
                //                cr.Action = 1
                //            End If
                //        Else
                //            MsgBox "il n'y a pas de facture associée a cette intervention", vbExclamation, "Facture non trouvée"
                //        End If
                //        adotemp.Close
                //    End If
                //    Set adotemp = Nothing

                //==  modif du 12 05 2005

                //try
                //{
                //    i = tSqlCrystal.Length;
                //}
                //catch (Exception ex)
                Variable.sImmFact = "";
                Variable.lFactAVoir = 0;
                Variable.lFactInterOuDevis = 0;
                if (General.sEvolution == "1")
                {
                    bool bcontrol = false;
                    if (fc_sauver(ref bcontrol) == false)
                    {
                        return;
                    }
                    General.fc_CreateFact(true, false, txtCodeEtat.Text, txtNoDevis.Text, txtCodeImmeuble.Text, this, Convert.ToInt32(General.nz(txtNoIntervention.Text, 0)), Convert.ToInt32(General.nz(txtNumFicheStandard.Text, 0)));
                    return;
                }
                if (tSqlCrystal == null)
                {
                    //Program.SaveException(ex);
                    //== indice en dehors de la plage.
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("il n'y a pas de facture associée a cette intervention", "Facture non trouvée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    return;
                }
                i = tSqlCrystal.Length;
                modAdoadotemp = new ModAdo();
                for (i = 0; i <= tSqlCrystal.Length - 1; i++)
                {
                    modAdoadotemp = new ModAdo();
                    adotemp = modAdoadotemp.fc_OpenRecordSet("SELECT DateFacture" + " From FactureManuelleEntete" + " WHERE NoFacture ='" + tSqlCrystal[i] + "'");
                    if (General.IsDate(adotemp.Rows[0]["DateFacture"]) && Convert.ToDateTime(adotemp.Rows[0]["DateFacture"]) < Convert.ToDateTime("15/06/2001"))
                    {
                        if (string.IsNullOrEmpty(sCrystalOld))
                        {
                            sCrystalOld = "{FactureManuelleEntete.NoFacture}='" + tSqlCrystal[i] + "'";
                        }
                        else
                        {
                            sCrystalOld = sCrystalOld + " or {FactureManuelleEntete.NoFacture}='" + tSqlCrystal[i] + "'";
                        }

                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sCrystal))
                        {
                            sCrystal = "{FactureManuelleEntete.NoFacture}='" + tSqlCrystal[i] + "'";
                        }
                        else
                        {
                            sCrystal = sCrystal + " or {FactureManuelleEntete.NoFacture}='" + tSqlCrystal[i] + "'";
                        }
                    }
                    modAdoadotemp.Close();
                }

                modAdoadotemp?.Dispose();

                if (!string.IsNullOrEmpty(sCrystalOld))
                {

                    ReportDocument cr = new ReportDocument();

                    cr.Load(General.ETATOLDFACTUREMANUEL);
                    string SelectionFormula = sCrystalOld;
                    CrystalReportFormView Form = new CrystalReportFormView(cr, SelectionFormula);
                    Form.Show();

                }

                if (!string.IsNullOrEmpty(sCrystal))
                {
                    ReportDocument cr = new ReportDocument();
                    cr.Load(General.ETATNEWFACTUREMANUEL);
                    string SelectionFormula = sCrystal;
                    CrystalReportFormView Form = new CrystalReportFormView(cr, SelectionFormula);
                    Form.Show();

                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdVisuFacture_Click");
            }



        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFicheAppel_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"btnFicheAppel_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            bool bcontrole = false;
            bcontrole = false;
            if (fc_sauver(ref bcontrole) == true)
            {
                if (!string.IsNullOrEmpty(txtNoIntervention.Text))
                {
                    // stocke la position de la fiche immeuble
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, txtNoIntervention.Text);
                    // stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocStandard, txtNumFicheStandard.Text);
                }
                View.Theme.Theme.Navigate(typeof(UserDocStandard));
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void btnHistoDevis_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"btnHistoDevis_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            bool bcontrole = false;
            bcontrole = false;
            if (btnHistoDevis.BackColor == cFindColor)
            {
                if (fc_sauver(ref bcontrole) == true)
                {
                    if (!string.IsNullOrEmpty(txtNoIntervention.Text))
                    {
                        // stocke la position de la fiche immeuble
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, txtNoIntervention.Text);
                        // stocke les paramétres pour la feuille de destination.
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocHistoDevis, txtCodeImmeuble.Text);

                        View.Theme.Theme.Navigate(typeof(UserDocHistoDevis));
                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnOrigine_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"btnOrigine_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            bool boolFolder = false;

            try
            {
                General.modAdorstmp = new ModAdo();
                var _with3 = General.rstmp;
                _with3 = General.modAdorstmp.fc_OpenRecordSet("SELECT GestionStandard.Document From GestionStandard WHERE GestionStandard.NumFicheStandard = " + txtNumFicheStandard.Text);
                if (_with3.Rows.Count > 0)
                {
                    if (_with3.Rows[0]["Document"] != DBNull.Value && !string.IsNullOrEmpty(_with3.Rows[0]["Document"].ToString()))
                    {

                        boolFolder = File.Exists(_with3.Rows[0]["Document"].ToString());

                        if (boolFolder == false)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La fiche standard contient une référence à un fichier, mais celui-çi n'éxiste pas", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        ModuleAPI.Ouvrir(_with3.Rows[0]["Document"].ToString());
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun document associé à cette intervention", "Document non trouvé", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                General.modAdorstmp.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdVisu_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void chkSansPauseDejeuner_CheckStateChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"chkSansPauseDejeuner_CheckStateChanged() - Enter In The Function - chkSansPauseDejeuner.Checked = {chkSansPauseDejeuner.Checked}");
            //===> Fin Modif Mondir
            if (General.IsDate(txtFin.Text) && txtFin.Text.Trim() != "  :  :  ".Trim() && General.IsDate(txtDebut.Text) && txtDebut.Text.Trim() != "  :  :  ".Trim())
            {
                txtDuree.Text = General.fc_calcDuree(Convert.ToDateTime(txtDebut.Text), Convert.ToDateTime(txtFin.Text), Convert.ToInt32(General.nz(((int)chkSansPauseDejeuner.CheckState), 0)));
                if (General.IsDate(txtDateRealise.Text))
                {
                    txtDebourse.Text = General.fc_CalcDebourse(Convert.ToDateTime(txtDuree.Text), txtIntervenant.Text, Convert.ToDateTime(txtDateRealise.Text));
                }
                else
                {
                    txtDebourse.Text = General.fc_CalcDebourse(Convert.ToDateTime(txtDuree.Text), txtIntervenant.Text, General.cDefDateRealise);
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdDernier_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdDernier_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            //Mondir : added to fix https://groupe-dt.mantishub.io/view.php?id=1745
            //Must wait untail load is finished to load the next inter
            //no exception in log, but the user has clicked two times on the next button, the load of the infos failded
            cmdDernier.Enabled = false;

            //===> Mondir le 26.03.2021, all commented code above has been commented to fix the ticket https://groupe-dt.mantishub.io/view.php?id=2289#c5716
            Navigate(4);

            //DataTable adorech = default(DataTable);
            //ModAdo modAdoadorech = null;
            //try
            //{
            //    longNumficheStandard = Convert.ToInt32(txtNumFicheStandard.Text);
            //    modAdoadorech = new ModAdo();
            //    var _with4 = adorech;
            //    _with4 = modAdoadorech.fc_OpenRecordSet("SELECT Intervention.* FROM Intervention WHERE  Intervention.NumFicheStandard = " + longNumficheStandard +
            //        " ORDER BY NoIntervention DESC");
            //    if (_with4.Rows.Count > 0)
            //    {
            //        LongNoIntervention = Convert.ToInt32(_with4.Rows[0]["NoIntervention"]);
            //        //txtJobNumber.Text = .RecordCount
            //        SaisIntervention(modAdoadorech);
            //        // rechercheArticle
            //        if (!string.IsNullOrEmpty(txtCodeEtat.Text))
            //        {
            //            RechercheLibelleEtat();
            //        }
            //        else
            //        {
            //            lbllibcodeetat.Text = "";
            //        }
            //        modAdoadorech.Close();
            //    }
            //    modAdoadorech?.Dispose();
            //}
            //catch (Exception ex)
            //{
            //    Erreurs.gFr_debug(ex, this.Name + ";cmdDernier_Click");
            //}

            cmdDernier.Enabled = true;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdDetailPrestation_Click(System.Object eventSender, System.EventArgs eventArgs)
        {//tester par Salma 
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdDetailPrestation_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                frmDetailPrestations frmDetailPrestations = new frmDetailPrestations();
                frmDetailPrestations.txtMois.Text = General.fc_ReturnMonth(Convert.ToDateTime(txtDateSaisie.Text));
                frmDetailPrestations.txtNoIntervention.Text = txtNoIntervention.Text;

                frmDetailPrestations.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdDetailPrestation_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdDetailPrestV2_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdDetailPrestV2_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                frmPDAv2P2 frmPDAv2P2 = new frmPDAv2P2();
                var _with5 = frmPDAv2P2;
                _with5.txtNoInter.Text = txtNoIntervention.Text;

                if ((int)chkVisiteEntretien.CheckState == 1)
                {
                    _with5.FraP2.Visible = true;

                }
                else
                {
                    _with5.FraP2.Visible = false;
                    _with5.MinimumSize = new Size(837, 563);
                    _with5.Height = 563;

                }


                frmPDAv2P2.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command5_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdDetPDA_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdDetPDA_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            if (General.sActiveEnvoieSurPDA == "1")
            {
                frmDetInterPDA frmDetInterPDA = new frmDetInterPDA();
                frmDetInterPDA.txtNointervention.Text = txtNoIntervention.Text;
                frmDetInterPDA.ShowDialog();
                frmDetInterPDA.Close();

            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Formulaire en cours de développement");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdDossierIntervention_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdDossierIntervention_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;

                General.File1EnCours = General.getFrmReg("App", "File1EnCours", "");
                General.Drive1EnCours = General.getFrmReg("App", "Drive1EnCours", "");
                General.Dir1EnCours = General.getFrmReg("App", "Dir1EnCours", "");

                if (!string.IsNullOrEmpty(txtNoIntervention.Text))
                {
                    if (Dossier.fc_ControleDossier(General.DossierIntervention + txtNoIntervention.Text) == false)
                    {
                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez-vous créer un dossier pour cette intervention ?", "Création d'un dossier d'intervention", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            Dossier.fc_CreateDossier(General.DossierIntervention + txtNoIntervention.Text);
                            cmdDossierIntervention.BackColor = cFindColor;
                            int xx = General.Execute("UPDATE Intervention set Intervention.DossierIntervention=1 WHERE Intervention.NoIntervention='" + txtNoIntervention.Text + "'");
                        }
                        else
                        {
                            return;
                        }
                    }
                    frmDossierIntervention frmDossierIntervention = new frmDossierIntervention();
                    var _with6 = frmDossierIntervention;

                    try
                    {
                        //foreach(var drive in System.IO.DriveInfo.GetDrives())
                        //{
                        //    _with6.Drive1.Items.Add(drive.Name);
                        //}
                        if (string.IsNullOrEmpty(General.Drive1EnCours))
                        {
                            if (General._ExecutionMode == General.ExecutionMode.Dev)
                                _with6.Drive1.Drive = "C:\\";
                            else
                                _with6.Drive1.Drive = "W:\\";
                        }
                        else
                        {
                            _with6.Drive1.Drive = General.Drive1EnCours;
                        }
                    }
                    catch (Exception ex)
                    {
                        Program.SaveException(ex);
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Lecteur " + General.Drive1EnCours);
                        General.saveInReg("App", "Drive1EnCours", "");
                    }

                    try
                    {
                        if (string.IsNullOrEmpty(General.Dir1EnCours))
                        {
                            if (General._ExecutionMode == General.ExecutionMode.Dev)
                                _with6.Dir1.Path = "C:\\";
                            else
                                _with6.Dir1.Path = "W:\\";
                        }
                        else
                        {
                            _with6.Dir1.Path = General.Dir1EnCours;
                        }

                    }
                    catch (Exception ex)
                    {
                        Program.SaveException(ex);
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Dossier " + General.Dir1EnCours);
                        General.saveInReg("App", "Dir1EnCours", "");
                    }

                    try
                    {
                        if (string.IsNullOrEmpty(General.File1EnCours))
                        {
                            _with6.File1.Path = _with6.Dir1.Path;
                        }
                        else
                        {
                            _with6.File1.Path = General.File1EnCours;
                        }
                    }
                    catch (Exception ex)
                    {
                        Program.SaveException(ex);
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Dossier " + General.File1EnCours);
                        General.saveInReg("App", "File1EnCours", "");
                    }


                    _with6.DirNoInter.Path = General.DossierIntervention + txtNoIntervention.Text;

                    _with6.FileIntervention.Path = General.DossierIntervention + txtNoIntervention.Text;
                    _with6.File1.Pattern = "*.doc;*.xls;*.tif;*.pcx;*.htm;*.html;*.txt;*.bmp;*.gif;*.jpg;*.jpeg;*.pdf;*.msg";
                    _with6.lblIntervention.Text = _with6.lblIntervention.Text + " l'Intervention N° : " + txtNoIntervention.Text;
                    _with6.Text = _with6.Text + " Intervention";
                    _with6.ShowDialog();
                    txtDossierIntervention.Text = "1";
                    frmDossierIntervention.Close();
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "cmdDossierIntervention_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdDupliquer_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdDupliquer_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            int longMsgBx = 0;
            int i = 0;
            string sNodevis = null;
            string sSQLDev = null;

            try
            {
                //=== modif du 13 02 27, interdit la cré&tionde devis si
                //=== celui ci est cloturé.
                if (General.sBloqueAchatSurDevis == "1")
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        sNodevis = tmpModAdo.fc_ADOlibelle("SELECT NoDevis FROM GestionStandard WHERE NumFicheStandard=" + txtNumFicheStandard.Text);
                        if (!string.IsNullOrEmpty(sNodevis))
                        {
                            sSQLDev = "SELECT     ClotureDevis From DevisEnTete WHERE  NumeroDevis = '" + StdSQLchaine.gFr_DoublerQuote(sNodevis) + "'";

                            sSQLDev = tmpModAdo.fc_ADOlibelle(sSQLDev);
                            if (sSQLDev == "1")
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le devis " + sNodevis + " a été clôturé, vous ne pouvez pas dubliquer cette intervention." + "\n" +
                                    "Seul un responsable peut vous autoriser à créer une intervention.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                        }
                    }
                }

                longNumficheStandard = Convert.ToInt32(txtNumFicheStandard.Text);
                longMsgBx = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Désirez vous dupliquer cette intervention?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (longMsgBx != 7)
                {
                    if (string.IsNullOrEmpty(txtArticle.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez insérer un code aticle", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        txtArticle.Focus();
                        return;
                    }
                    //--------Le bouton cmdDupliquer sert à crée une autre intervention
                    //qui est la copie de l'intervention présente sans l'intervenant
                    //CmdSauver_Click(CmdSauver, new System.EventArgs());
                    Save();
                    this.txtCodeEtat.Text = "00";
                    using (var tmpModAdo = new ModAdo())
                        this.lbllibcodeetat.Text = tmpModAdo.fc_ADOlibelle("SELECT  LibelleCodeEtat" + " FROM TypeCodeEtat"
                            + " where codeetat ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeEtat.Text) + "'");
                    txtDatePrevue.Text = "";
                    txt16.Text = "";
                    txtDateVisite.Text = "";
                    txtheureDebutP.Text = "  :  ";
                    txtHeurePrevue.Text = "  :  ";
                    txtDateRealise.Text = "";
                    txtDebut.Text = "  :  :  ";
                    txtFin.Text = "  :  :  ";
                    txtDuree.Text = "  :  :  ";
                    txtHeureDebut.Text = "  :  ";
                    txtDeplacement.Text = "";
                    txtAcompte.Text = "";
                    txt0.Text = "";
                    txt1.Text = "";
                    txtDebourse.Text = "";
                    ComboTYMO_ID.Text = "";
                    chk0.CheckState = System.Windows.Forms.CheckState.Unchecked;
                    txt7.Text = ""; //=== date d'envoie sur le PDA.

                    //txtIntervenant.Value = "000"
                    General.modAdorstmp = new ModAdo();
                    General.rstmp = General.modAdorstmp.fc_OpenRecordSet("SELECT * FROM Intervention WHERE NoIntervention=0");
                    var NewRow = General.rstmp.NewRow();
                    NewRow["CodeImmeuble"] = txtCodeImmeuble.Text;
                    NewRow["Article"] = txtArticle.Text;
                    NewRow["Designation"] = txtDesignation.Text;
                    NewRow["SousArticle"] = txtSousArticle.Text;
                    NewRow["Commentaire"] = txtCommentaire.Text;
                    NewRow["situation"] = txtSituation.Text;
                    NewRow["TYMO_ID"] = ComboTYMO_ID.Text;
                    NewRow["TelephoneContact"] = General.nz(txtTelephoneContact.Text, System.DBNull.Value);
                    NewRow["Deplacement"] = txtDeplacement.Text;
                    NewRow["NoDevis"] = (!string.IsNullOrEmpty(txtNoDevis.Text) ? txtNoDevis.Text : "");
                    NewRow["CodeEtat"] = txtCodeEtat.Text + "";
                    NewRow["Numfichestandard"] = txtNumFicheStandard.Text;

                    NewRow["GMAO_Visite"] = General.nz(txt3.Text, System.DBNull.Value);
                    //If txtDateSaisie <> "" Then
                    txtDateSaisie.Text = DateTime.Now.ToString();
                    NewRow["DateSaisie"] = txtDateSaisie.Text;
                    //End If

                    if (!string.IsNullOrEmpty(txtDatePrevue.Text))
                    {
                        NewRow["DatePrevue"] = txtDatePrevue.Text;
                    }

                    if (!string.IsNullOrEmpty(txtDateVisite.Text))
                    {
                        NewRow["DateVisite"] = txtDateVisite.Text;
                    }

                    if (txtHeurePrevue.Text.Trim() != "  :  ".Trim())
                    {
                        NewRow["HeurePrevue"] = txtHeurePrevue.Text;
                    }

                    if (txtheureDebutP.Text.Trim() != "  :  ".Trim())
                    {
                        NewRow["heureDebutP"] = txtheureDebutP.Text;
                    }


                    NewRow["Intervenant"] = "000";
                    txtIntervenant.Text = "000";
                    txtMatriculeIntervenant.Text = "000";
                    if ((int)CheckFioul.CheckState == 1)
                    {
                        NewRow["LivraisonFioul"] = true;
                    }
                    else
                    {
                        NewRow["LivraisonFioul"] = false;
                    }

                    if ((int)chkCompteurObli.CheckState == 1)
                    {
                        NewRow["CompteurObli"] = 1;
                    }
                    else
                    {
                        NewRow["CompteurObli"] = 0;
                    }

                    if ((int)chkUrgence.CheckState == 1)
                    {
                        NewRow["Urgence"] = 1;
                    }
                    else
                    {
                        NewRow["Urgence"] = 0;
                    }

                    if ((int)CheckSinistre.CheckState == 1)
                    {
                        NewRow["Sinistre"] = true;
                    }
                    else
                    {
                        NewRow["Sinistre"] = false;
                    }

                    if ((int)CheckTrvx.CheckState == 1)
                    {
                        NewRow["TrvxParticulier"] = true;
                    }
                    else
                    {
                        NewRow["TrvxParticulier"] = false;
                    }
                    if ((int)CheckVidange.CheckState == 1)
                    {
                        NewRow["Vidange"] = true;
                    }
                    else
                    {
                        NewRow["Vidange"] = false;
                    }

                    NewRow["AvisDePassage"] = General.nz((int)chk0.CheckState, 0);


                    if (btnOrigine.BackColor == cFindColor)
                    {
                        NewRow["Courrier"] = true;
                    }
                    General.rstmp.Rows.Add(NewRow);
                    int xx = General.modAdorstmp.Update();

                    fc_InitMajPDA();

                    //récupére le numéro d'intervention qui est un champs No auto
                    using (var tmpModAdo = new ModAdo())
                    {
                        LongNoIntervention = Convert.ToInt32(tmpModAdo.fc_ADOlibelle("SELECT MAX(NoIntervention) FROM Intervention"));
                        txtNoIntervention.Text = tmpModAdo.fc_ADOlibelle("SELECT MAX(NoIntervention) FROM Intervention");
                    }
                    General.modAdorstmp.Close();
                    General.modAdorstmp = new ModAdo();
                    General.rstmp = General.modAdorstmp.fc_OpenRecordSet("SELECT Intervention.NoIntervention FROM Intervention WHERE Intervention.NumFicheStandard='" + longNumficheStandard + "'");

                    txtJobNumber.Text = (General.rstmp.Rows.Count).ToString();
                    longNombreInterv = (General.rstmp.Rows.Count);
                    txtPage.Text = Convert.ToString((General.rstmp.Rows.Count));
                    btnAchat.BackColor = System.Drawing.ColorTranslator.FromOle(0xffc0c0);
                    btnFacture.BackColor = System.Drawing.ColorTranslator.FromOle(0xffc0c0);
                    btnDebourse.BackColor = System.Drawing.ColorTranslator.FromOle(0xffc0c0);
                    cmdPremier.Enabled = true;
                    cmdSuivant.Enabled = true;
                    cmdPrecedent.Enabled = true;
                    cmdDernier.Enabled = true;

                    //===> Mondir le 11.05.2021 https://groupe-dt.mantishub.io/view.php?id=2289
                    Navigate(0);
                    //===> Fin Modif Mondir
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdDupliquer_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void CmdEditer_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"CmdEditer_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            string sReturn = null;

            try
            {
                //    fc_ImprimeBacs gsRpt & ETATINTERVENTION, "{Intervention.NoIntervention} =" & txtNoIntervention.Text
                //
                //    Exit Sub

                //If RAPPORTINTERVENTIONV2 <> "" Then
                //        sReturn = fc_ExportCrystalSansFormule("{Intervention.NoIntervention} =" & txtNoIntervention.Text, gsRpt & RAPPORTINTERVENTIONV2)
                //        'MsgBox "ouvrir"
                //        Ouvrir Me.hwnd, sReturn
                //Else
                ReportDocument cr = new ReportDocument();
                cr.Load(General.gsRpt + General.ETATINTERVENTION);
                string SelectionFormula = "{Intervention.NoIntervention} =" + txtNoIntervention.Text;
                CrystalReportFormView Form = new CrystalReportFormView(cr, SelectionFormula);
                Form.Show();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdImpression_Click");
            }
            //    End If
        }
        private void Save()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"Save() - Enter In The Function");
            //===> Fin Modif Mondir
            bool bcontrole = false;

            bcontrole = false;

            if (fc_sauver(ref bcontrole) == false)
                return;

            if (bcontrole == true)
                return;
            showAlerteSuccess = true;
            fc_ChargeEnregistrement((txtNoIntervention.Text));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void CmdSauver_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"CmdSauver_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            Save();
            if (showAlerteSuccess)
                View.Theme.Theme.AfficheAlertSucces();
            showAlerteSuccess = false;

            //===> Mondir le 11.05.2021 https://groupe-dt.mantishub.io/view.php?id=2289
            Navigate(0);
            //===> Fin Modif Mondir
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdHistoMessage_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdHistoMessage_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                frmHistoMessage frmHistoMessage = new frmHistoMessage();
                frmHistoMessage.txtNointervention.Text = txtNoIntervention.Text;
                frmHistoMessage.ShowDialog();
                frmHistoMessage.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdHistoMessage_Click");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdHistorique_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdHistorique_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            bool bcontrole = false;
            bcontrole = false;
            if (fc_sauver(ref bcontrole) == true)
            {
                if (!string.IsNullOrEmpty(txtNoIntervention.Text))
                {
                    // stocke la position de la fiche intervention
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, txtNoIntervention.Text);
                    // stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocHistoInterv, txtCodeImmeuble.Text);
                }
                View.Theme.Theme.Navigate(typeof(UserDocHistoriqueInterv));
                //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocHistoInterv);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdMail_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdMail_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            string MonDossier = null;
            string Contenu = null;
            string Msg = null;
            string FichierCrystalName = null;
            string OrigineAppel = null;
            int i = 0;
            int FIN = 0;
            int Debut = 0;
            string sCodeGestionnaire = null;
            string sReturn = null;
            DataTable rsDoc = default(DataTable);
            ModAdo modAdorsDoc = null;
            bool bSelectImm;
            string sSQLImm;
            try
            {
                ModCourrier.EnvoieMail = false;
                bSelectImm = false;

                if (General.RAPPORTINTERVENTIONV2_1 == "1")
                {
                    General.sFileRapportPhoto = "";
                    frmRapportInter frm = new frmRapportInter();
                    frm.txtNoIntervention.Text = txtNoIntervention.Text;
                    frm.ShowDialog();
                    bSelectImm = true;
                }


                if (Dossier.fc_ControleDossier(General.DossierIntervention + txtNoIntervention.Text + "\\Mail") == true)
                {
                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Un mail a déjà été envoyé, souhaitez-vous le consulter ?", "Mail existant", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                    {
                        frmDossierMail frmDossierMail = new frmDossierMail();
                        var _with9 = frmDossierMail;
                        _with9.FileIntervention.Tag = General.DossierIntervention + txtNoIntervention.Text + "\\Mail";
                        _with9.lblIntervention.Text = _with9.lblIntervention.Text + " la fiche d'intervention N°" + txtNoIntervention.Text;
                        _with9.ShowDialog();
                        frmDossierMail.Close();
                        return;
                    }
                }

                MonDossier = General.DossierIntervention + txtNoIntervention.Text + "\\";

                Contenu = MonDossier;
                ModCourrier.FichierAttache = "";

                if (Directory.Exists(Contenu))
                    foreach (var str in Directory.GetFiles(Contenu))
                    {
                        if (string.IsNullOrEmpty(ModCourrier.FichierAttache))
                            ModCourrier.FichierAttache = str;
                        else
                            ModCourrier.FichierAttache += "\t" + str;
                        //if (!string.IsNullOrEmpty(str))
                        //{
                        //    ModuleMail.FichierAttache = ModuleMail.FichierAttache + str;
                        //}
                        //if (!string.IsNullOrEmpty(Contenu))
                        //{
                        //    ModuleMail.FichierAttache = ModuleMail.FichierAttache + "\t";
                        //}
                    }

                if (!string.IsNullOrEmpty(txtWave.Text))
                {
                    if (string.IsNullOrEmpty(ModCourrier.FichierAttache))
                    {
                        ModCourrier.FichierAttache = txtWave.Text;
                    }
                    else
                    {
                        ModCourrier.FichierAttache = ModCourrier.FichierAttache + "\t" + txtWave.Text;
                    }
                }

                using (var tmpModAdo = new ModAdo())
                    OrigineAppel = tmpModAdo.fc_ADOlibelle("SELECT GestionStandard.Document From GestionStandard WHERE GestionStandard.NumFicheStandard=" + txtNumFicheStandard.Text);

                if (!string.IsNullOrEmpty(OrigineAppel))
                {
                    if (string.IsNullOrEmpty(ModCourrier.FichierAttache))
                    {
                        ModCourrier.FichierAttache = OrigineAppel;
                    }
                    else
                    {
                        ModCourrier.FichierAttache = ModCourrier.FichierAttache + "\t" + OrigineAppel;
                    }
                }

                if (!string.IsNullOrEmpty(General.RAPPORTINTERVENTIONV2))
                {
                    //' fc_stokeimage txtNoIntervention
                    if (!string.IsNullOrEmpty(General.sFileRapportPhoto) && General.RAPPORTINTERVENTIONV2_1 == "1")
                    {
                        FichierCrystalName = General.sFileRapportPhoto;
                    }
                    else
                    {
                        FichierCrystalName = devModeCrystal.fc_ExportCrystalSansFormule("{Intervention.NoIntervention} =" + txtNoIntervention.Text, General.gsRpt + General.RAPPORTINTERVENTIONV2);
                    }

                    if (string.IsNullOrEmpty(ModCourrier.FichierAttache))
                    {
                        ModCourrier.FichierAttache = FichierCrystalName;
                    }
                    else
                    {
                        ModCourrier.FichierAttache = ModCourrier.FichierAttache + "\t" + FichierCrystalName;
                    }
                }
                else
                {
                    ReportDocument CR2 = new ReportDocument();
                    CR2.Load(General.gsRpt + General.ETATINTERVENTION);
                    string SelectionFormula = "{Intervention.NoIntervention} =" + txtNoIntervention.Text;
                    FichierCrystalName = Environment.GetFolderPath(Environment.SpecialFolder.Desktop) + "\\AperçuInterventionNo-" + txtNoIntervention.Text + ".doc";
                    CR2.RecordSelectionFormula = SelectionFormula;

                    ExportOptions CrExportOptions;
                    DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                    PdfRtfWordFormatOptions CrFormatTypeOptions = new PdfRtfWordFormatOptions();
                    CrDiskFileDestinationOptions.DiskFileName = FichierCrystalName;
                    CrExportOptions = CR2.ExportOptions;
                    {
                        CrExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        CrExportOptions.ExportFormatType = ExportFormatType.WordForWindows;
                        CrExportOptions.DestinationOptions = CrDiskFileDestinationOptions;
                        CrExportOptions.FormatOptions = CrFormatTypeOptions;
                    }

                    CR2.Export();

                    if (string.IsNullOrEmpty(ModCourrier.FichierAttache))
                    {
                        ModCourrier.FichierAttache = FichierCrystalName;
                    }
                    else
                    {
                        ModCourrier.FichierAttache = ModCourrier.FichierAttache + "\t" + FichierCrystalName;
                    }
                }


                General.sSQL = "SELECT     TypeFichier, Chemin ,ID" + " From InterventionDoc "
                    + " Where Nointervention = " + General.nz((txtNoIntervention.Text), 0) + " ORDER BY TypeFichier";

                modAdorsDoc = new ModAdo();
                rsDoc = modAdorsDoc.fc_OpenRecordSet(General.sSQL);
                foreach (DataRow rsDocRow in rsDoc.Rows)
                {
                    if (General.RAPPORTINTERVENTIONV2_1 == "1")
                    {
                        //'=== modif du 14 01 2019, controle si ces fichiers sont déja intégrées dans l'état rapport d'intervention.
                        sSQLImm = "SELECT     INP_ID,  Nointervention, ID_DOC"
                                   + " From INP_InterDocPrint "
                                   + " WHERE Nointervention  = " + txtNoIntervention.Text
                                   + " and INP_UserName ='" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'"
                                   + " and ID_DOC ='" + General.nz(rsDocRow["ID"], 0) + "'";
                        using (var tmp = new ModAdo())
                            sSQLImm = tmp.fc_ADOlibelle(sSQLImm);

                        if (string.IsNullOrEmpty(sSQLImm))
                        {
                            if (string.IsNullOrEmpty(ModCourrier.FichierAttache))
                            {
                                ModCourrier.FichierAttache = General.DossierIntervention + rsDocRow["Chemin"] + "";
                            }
                            else
                            {
                                ModCourrier.FichierAttache = ModCourrier.FichierAttache + "\t" + General.DossierIntervention + rsDocRow["Chemin"] + "";
                            }
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(ModCourrier.FichierAttache))
                        {
                            ModCourrier.FichierAttache = General.DossierIntervention + rsDocRow["Chemin"] + "";
                        }
                        else
                        {
                            ModCourrier.FichierAttache = ModCourrier.FichierAttache + "\t" + General.DossierIntervention + rsDocRow["Chemin"] + "";
                        }
                    }

                }
                modAdorsDoc.Close();

                if (Dossier.fc_ControleDossier(General.DossierIntervention + txtNoIntervention.Text) == false)
                {
                    Dossier.fc_CreateDossier(General.DossierIntervention + txtNoIntervention.Text);
                }

                ModMain.bActivate = true;

                frmMail frmMail = new frmMail();
                var _with10 = frmMail;
                Msg = CorpsMessage(ModCourrier.MessageMail);
                _with10.txtMessage.Text = "";
                _with10.txtMessage.Text = Msg;

                _with10.txtDocuments.Text = ModCourrier.FichierAttache;
                _with10.txtDossierSauvegarde.Text = "";
                _with10.txtDossierSauvegarde.Text = General.DossierIntervention + txtNoIntervention.Text + "\\Mail\\";

                using (var tmpModAdo = new ModAdo())
                {
                    sCodeGestionnaire = tmpModAdo.fc_ADOlibelle("SELECT CodeGestionnaire from Immeuble" + " Where CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'");
                    _with10.SSOleDBcmbA.Text = tmpModAdo.fc_ADOlibelle("SELECT Gestionnaire.Email_GES" + " FROM Gestionnaire " + " WHERE  code1 ='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'" +
                        " AND CodeGestinnaire='" + StdSQLchaine.gFr_DoublerQuote(sCodeGestionnaire) + "'");
                }
                _with10.ShowDialog();
                ModCourrier.FichierAttache = "";
                //        .GridDocuments.MoveFirst
                //        For i = 0 To .GridDocuments.Rows - 1
                //            If .GridDocuments.Columns(0).Text = "1" Then
                //                If FichierAttache = "" Then
                //                    FichierAttache = .GridDocuments.Columns(1).Text
                //                Else
                //                    FichierAttache = FichierAttache & vbTab & .GridDocuments.Columns(1).Text
                //                End If
                //            End If
                //            .GridDocuments.MoveNext
                //        Next
                frmMail.Close();

                if (ModCourrier.EnvoieMail == true)
                {

                    //        If InitialiseEtOuvreOutlook = True Then
                    //            If CreateMail(AdresseMail, list, ObjetMail, Msg, FichierAttache, , DossierIntervention & txtNoIntervention.Text & "\Mail\") = True Then
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Message Envoyé", "Mail intervention n°" + txtNoIntervention.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //            End If
                    //         End If

                    File.Delete(FichierCrystalName);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdImpression_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="CommentairesPerso"></param>
        /// <returns></returns>
        private string CorpsMessage(string CommentairesPerso)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"CorpsMessage() - Enter In The Function - CommentairesPerso = {CommentairesPerso}");
            //===> Fin Modif Mondir
            string Corps = null;

            Corps = "Le " + DateTime.Today + " à " + DateTime.Now.TimeOfDay + "\n\n";

            Corps = Corps + "Commentaires personnels :" + "\n";
            Corps = Corps + CommentairesPerso + "\n\n";

            //Corps = Corps & "Détail de l'intervention :" & vbCrLf
            //
            //Corps = Corps & "Code immeuble : " & txtCodeImmeuble.Text & vbTab
            //Corps = Corps & " Adresse : " & txtAdresse.Text & vbTab & txtAdresse2_IMM.Text & vbTab
            //Corps = Corps & "CP : " & txtCodePostal.Text & " Ville : " & txtVille.Text & vbCrLf
            //
            //Corps = Corps & "Contrat : " & txtcontrat.Text & vbTab & " Gerant : " & txtCode1.Text & vbTab
            //Corps = Corps & " Président du conseil syndical : " & txtCodeParticulier.Text & vbCrLf & vbCrLf
            //
            //Corps = Corps & "Accés : " & vbCrLf
            //Corps = Corps & "Gardien : " & txtGardien.Text & vbTab & " Boîte à clé : " & ConvertBooleen(chkBoite.Value) & vbTab

            Corps = Corps + "L'intervention est liée à la fiche d'appel N° : " + txtNumFicheStandard.Text + "\n\n";

            //Corps = Corps & vbCrLf & "<file://" & gsCheminPackage & PROGUserDocStandard & ">" & vbCrLf & vbCrLf
            return Corps;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdMAJ_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdMAJ_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            DataTable adors = default(DataTable);
            ModAdo modAdoDataTable = null;
            DataTable adotemp = default(DataTable);
            ModAdo modAdoadotemp = null;
            DataTable rsMail = default(DataTable);
            ModAdo modAdorsMail = null;

            string strSql = null;
            string strNumRadio = null;
            string sTechTom = null;
            string sIdImm = null;
            string sSQLmail = null;
            string sAdresseMail = null;
            string sType = null;
            string sBodyMail = null;
            string sSQLPers = null;
            string sSmsV2 = null;

            short sNoFile = 0;
            int lOrdre = 0;

            bool bcontrole = false;
            bool blnEnvoieOnde = false;
            bool bEtatV2 = false;

            int lNoauto = 0;
            int lSMS = 0;
            int lPDA = 0;
            int lIDlog = 0;

            System.DateTime dtNow = default(System.DateTime);

            string sLogtomtom = null;
            string sID = null;
            string sLog = null;

            bool bNotExecute = false;


            bcontrole = false;

            try
            {
                lSMS = 0;
                lPDA = 0;
                bNotExecute = false;

                if (DateTime.Today.ToString("dd/MM/yyyy") == "20/03/2018" && General.fncUserName().ToUpper() == "rachid abbouchi".ToUpper())
                {
                    //Exit Sub
                    bNotExecute = true;
                    //goto PDA;
                    if (fc_sendPDAcorrective(Convert.ToInt32(txtNoIntervention.Text), false, dtNow, "") == false)
                    {
                        sLog = sLog + "fc_sendPDAcorrective = false" + "\n";
                        return;
                    }
                    else
                    {
                        sLog = sLog + "fc_sendPDAcorrective = true" + "\n";
                        txt7.Text = Convert.ToString(dtNow);
                        lPDA = 1;
                    }
                    if (bNotExecute == true)
                    {
                        txtCodeEtat.Text = "0A";

                        sLog = sLog + "Code état: " + txtCodeEtat.Text + "\n";


                        General.Execute("UPDATE Intervention SET Intervention.CodeEtat ='0A' " + " WHERE Intervention.NoIntervention =" + LongNoIntervention + "");

                        fc_InitMajPDA();
                        fc_sauver(ref bcontrole);
                        sLog = sLog + "FIN " + "\n";
                        sLog = sLog + "\n";

                        ModLog.fc_Log(ModLog.cMobilite, sLog, sID);
                        return;
                    }
                }

                blnEnvoieOnde = true;

                if (string.IsNullOrEmpty(txtDispatcheur.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("vous devez saisir un Code Dispatcheur", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDispatcheur.Focus();
                    return;
                }
                //'=== modif du 20 05 2019, bloque l'envoie des interventions si le matricule est 000(en attente).
                if (General.UCase(txtIntervenant.Text) == General.UCase("000"))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le matricule 000 ne permet pas la transmission de l'intervention." + "\n" + "Veuillez d 'abord changer l'intervenant.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                //=== modif du 23 07 2019
                if (txtNoDevis.Text != "")
                {
                    if (General.fc_CtrResponsableTravaux(txtNoDevis.Text, 0) == false)
                        return;
                }

                //===> Mondir le 15.04.2021 https://groupe-dt.mantishub.io/view.php?id=2389#c5866
                if (!CheckIntervenant())
                {
                    return;
                }

                sSmsV2 = "";

                dtNow = DateTime.Now;

                if (txtCodeEtat.Text == "00" || string.IsNullOrEmpty(txtCodeEtat.Text))
                {

                    //=== genere un ID LOG.
                    Random R = new Random();
                    lIDlog = R.Next(1, 1000);
                    sID = DateTime.Now.ToShortDateString();
                    sID = sID.Replace("/", "");
                    sID = sID + " " + DateTime.Now.ToString("HH:mm:ss").Replace(":", "") + "-" + lIDlog;

                    sLog = "" + "\n";
                    sLog = sLog + "DEBUT ENVOIE ID " + sID + "\n";
                    sLog = sLog + "DATABASE : " + General.adocnn.Database + "\n";
                    sLog = sLog + "codeimmeuble : " + txtCodeImmeuble.Text + "\n";
                    sLog = sLog + "Code état: " + txtCodeEtat.Text + "\n";
                    sLog = sLog + "intervention : " + txtNoIntervention.Text + "\n";
                    sLog = sLog + "intervenant : " + txtIntervenant.Text + "\n";

                    //=== indique qu'une demande d'envoie a été demandé par l'utilisateur (clic sur bouton envoie).
                    lEtatEnvoie = 1;

                    //=== modif du 12 octobre 2016.
                    if (string.IsNullOrEmpty(txtDateRealise.Text))
                    {
                        txtDateRealise.Text = DateTime.Today.ToString(General.FormatDateSansHeureSQL);
                    }
                    if (General.IsDate(txtDateRealise.Text))
                    {
                        sLog = sLog + "lEtatEnvoie = 1; Date " + Convert.ToDateTime(txtDateRealise.Text).ToShortDateString() + "\n";
                    }

                    //CmdSauver_Click(CmdSauver, new System.EventArgs());
                    Save();
                    //=== modif du 19 10 2016, riniatilise la variable lEtatEnvoie.
                    lEtatEnvoie = 0;

                    sLog = sLog + "Save complet; lEtatEnvoie = 0; sActiveEnvoieSurPDA = " + General.sActiveEnvoieSurPDA + "." + "\n";

                    if (General.sActiveEnvoieSurPDA == "1")
                    {

                        //=== envoie annulé par l'utilisateur.
                        if (lEtatEnvoie == 2)
                        {
                            sLog = sLog + "lEtatEnvoie = 2: exit sub." + "\n";
                            return;
                        }

                        sLog = sLog + " lEtatEnvoie = " + lEtatEnvoie + "\n";

                        //=== envoie sur PDA déjà effectué (voir fonction fc_ctrlPDA par exemple).
                        if (lEtatEnvoie != 3)
                        {
                            //=== controle que l'intervenant a l'autorisation de recevoir
                            //=== les interventions sur PDA.
                            sSQLPers = "SELECT ActiverPDA From Personnel WHERE Matricule ='" + StdSQLchaine.gFr_DoublerQuote(txtIntervenant.Text) + "'";
                            using (var tmpModAdo = new ModAdo())
                                sSQLPers = tmpModAdo.fc_ADOlibelle(sSQLPers);

                            sLog = sLog + "Intervenant = " + txtIntervenant.Text + " ActiverPDA = " + General.nz(sSQLPers, "null") + "\n";

                            if (sSQLPers == "1")
                            {
                                //sSmsV2 = "1"
                                //=== modif du 22 08 2016, desactive l'envoie du nouveau format de sms.
                                sSmsV2 = "";
                                PDA:

                                //dtNow = Now
                                //=== controle si des modifications ont eu lieu sur l'intervention
                                //=== a partir de l'envoie de celle-ci.
                                if (fc_sendPDAcorrective(Convert.ToInt32(txtNoIntervention.Text), false, dtNow, "") == false)
                                {
                                    sLog = sLog + "fc_sendPDAcorrective = false" + "\n";
                                    return;
                                }
                                else
                                {
                                    sLog = sLog + "fc_sendPDAcorrective = true" + "\n";
                                    txt7.Text = Convert.ToString(dtNow);
                                    lPDA = 1;
                                }
                                if (bNotExecute == true)
                                {
                                    goto suite2;
                                }
                            }
                        }
                    }


                    if (General.adocnn.Database.ToUpper() != "AXECIEL_DELOSTAL_PDA".ToUpper())
                    {

                        modAdoadotemp = new ModAdo();
                        strSql = "";
                        var _with11 = adotemp;
                        sLog = sLog + "SMS" + "\n";
                        //        .Close
                        _with11 = modAdoadotemp.fc_OpenRecordSet("SELECT Personnel.NumRadio From Personnel Where Personnel.Matricule ='" + txtMatriculeIntervenant.Text + "'");
                        if (_with11.Rows.Count > 0)
                        {
                            bool _SUITE = false;
                            if (_with11.Rows[0]["Numradio"] == DBNull.Value || string.IsNullOrEmpty(_with11.Rows[0]["Numradio"].ToString()))
                            {
                                modAdoadotemp?.Close();
                                blnEnvoieOnde = false;

                                sLog = sLog + "SMS blnEnvoieOnde = False" + "\n";
                                _SUITE = true;
                            }
                            else
                            {
                                strNumRadio = _with11.Rows[0]["Numradio"] + "";
                            }

                            if (!_SUITE)
                                sLog = sLog + "SMS numradio: " + General.nz(_with11.Rows[0]["Numradio"], "null") + "" + "\n";

                        }
                        else
                        {
                            SUITE:
                            modAdoadotemp?.Close();
                            blnEnvoieOnde = false;

                            sLog = sLog + "SMS blnEnvoieOnde = False" + "\n";
                        }


                        //If sTomtom <> "1" Then
                        if (blnEnvoieOnde == true)
                        {
                            modAdoadotemp?.Close();
                            sLog = sLog + "SMS blnEnvoieOnde = true" + "\n";

                            //=== modif du 30/05/2018.
                            if (General.sDesactiveInform != "1")
                            {

                                sLog = sLog + "SMS sDesactiveInform = " + General.nz(General.sDesactiveInform, "null") + "\n";

                                longJobNumber = 1;
                                //recherche du dernier NumInterne + 1
                                longJobNumber = NumeroInform(1);
                                //txtJobNumber = longJobNumber
                                if (boolPro == true)
                                    return;
                                _with11 = modAdoadotemp.fc_OpenRecordSet("Select * from INFORM order by Numinterne");
                                var NewRow = _with11.NewRow();
                                NewRow["NumInterne"] = longJobNumber;
                                NewRow["jobNumber"] = longJobNumber.ToString("#####00000");
                                NewRow["numIntervention"] = LongNoIntervention;
                                _with11.Rows.Add(NewRow);
                                int xx = modAdoadotemp.Update();
                                //End If
                                modAdoadotemp.Close();
                            }

                            modAdoadotemp?.Close();
                        }
                        //End If
                        //Open FICHIER2 & Format(longJobNumber, "#####00000") For Output As 1
                        //== Envoie un fichier vers LibNet.
                        sLog = sLog + "ouverture fichier" + "\n";

                        //StreamWriter SW = new StreamWriter(General.LIBNET + LongNoIntervention.ToString("#####00000"));

                        //=== modif du 04 11 2011, augmente la taille des codes acces
                        if (sSmsV2 == "1")
                        {
                            strphrase = General.Left(txtCodeImmeuble.Text, 20) + "-" + General.Left(txtCodePostal.Text, 5) + "-" + General.Left(txtVille.Text, 15) + "-" + General.Left(txtDesignation.Text, 45) + "-" + txtDateRealise.Text;

                            if (General.IsDate(txtHeurePrevue.Text))
                            {
                                strphrase = strphrase + "-" + Convert.ToDateTime(txtHeurePrevue.Text).Hour.ToString("00") + "h" + Convert.ToDateTime(txtHeurePrevue.Text).Minute.ToString("00");
                            }
                        }
                        else
                        {
                            strphrase = LongNoIntervention.ToString("#####00000") + "#" + General.Left(txtMemoGuard.Text, 4) + "#" + General.Left(txtAdresse.Text, 23) + "/" + General.Left(txtCodePostal.Text, 5) + "/" +
                                General.Left(txtCodeAcces1.Text, 8) + "/" + General.Left(txtVille.Text, 10) + "/" + General.Left(txtSituation.Text, 12) + "/" + General.Left(txtDesignation.Text, 25) + "/" +
                                General.Left(txtCommentaire.Text, 77);
                        }
                        sLog = sLog + "Contenu fichier: " + strphrase + "\n";

                        sLog = sLog + "fin fichier" + "\n";

                        if (General._ExecutionMode == General.ExecutionMode.Dev)
                            File.WriteAllText(@"C:\DATA\Donnee\Delostal\data\InterventionsDT\" + LongNoIntervention.ToString("#####00000"), General.Left(strphrase, 160), Encoding.Default);

                        else
                            File.WriteAllText(General.LIBNET + LongNoIntervention.ToString("#####00000"), General.Left(strphrase, 160), Encoding.Default);


                        //SW.Write();
                        //SW.Close();

                        lSMS = 1;

                    }


                    if (General.adocnn.Database.ToUpper() != "AXECIEL_DELOSTAL_PDA".ToUpper())
                    {
                        //=== modif du 04 02 2015, Envoie de mail.
                        if (General.sSendInterMail == "1")
                        {

                            sBodyMail = LongNoIntervention.ToString("#####00000") + "#" + General.Left(txtMemoGuard.Text, 4) + "#" + txtAdresse.Text + "/" + txtCodePostal.Text + "/" + txtCodeAcces1.Text + "/" + txtVille.Text + "/" + txtSituation.Text + "/" + txtDesignation.Text + "/" + txtCommentaire.Text;

                            sSQLmail = "SELECT     Matricule, Email  From Personnel WHERE     (EtatV = 1)";

                            modAdorsMail = new ModAdo();
                            rsMail = modAdorsMail.fc_OpenRecordSet(sSQLmail);

                            bEtatV2 = false;

                            foreach (DataRow rsMailRow in rsMail.Rows)
                            {
                                if (rsMailRow["Matricule"].ToString().ToUpper() == txtIntervenant.Text.ToUpper())
                                {
                                    bEtatV2 = true;
                                    sAdresseMail = rsMailRow["Email"] + "";
                                    break;
                                }
                            }
                            modAdorsMail.Close();

                            if (bEtatV2 == true)
                            {
                                if (string.IsNullOrEmpty(sAdresseMail))
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("L'envoie de mail a échoué, l'intervenant " + txtIntervenant.Text + " a une adresse mail inexistante ou erronée.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                }
                                else
                                {
                                    if (ModCourrier.fc_CtrlAdresseMail(sAdresseMail) == true)
                                    {
                                        ModCourrier.fc_SendLMailCDO(General.sMailServiceInfo, sAdresseMail.Trim(), "intervention au " + txtCodeImmeuble.Text, sBodyMail);
                                    }
                                }
                            }
                        }
                    }
                    //End With

                    if (lSMS == 1 && lPDA == 1)
                    {
                        sType = "PDA+SMS";
                    }
                    else if (lSMS == 1 && lPDA == 0)
                    {
                        sType = "SMS";
                    }
                    else if (lSMS == 0 && lPDA == 1)
                    {
                        sType = "PDA";
                    }

                    sLog = sLog + "Mode communication: " + sType + "\n";
                    sLog = sLog + "FIN ENVOIE: " + sID + "\n";

                    fc_historiseMes(sType, strphrase, txtIntervenant.Text, lPDA, lSMS);
                    suite2:

                    txtCodeEtat.Text = "0A";

                    sLog = sLog + "Code état: " + txtCodeEtat.Text + "\n";


                    var xxx = General.Execute("UPDATE Intervention SET Intervention.CodeEtat ='0A' " + " WHERE Intervention.NoIntervention =" + LongNoIntervention + "");

                    fc_InitMajPDA();
                    fc_sauver(ref bcontrole);
                    sLog = sLog + "FIN " + "\n";
                    sLog = sLog + "\n";

                    ModLog.fc_Log(ModLog.cMobilite, sLog, sID);



                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code etat est à " + txtCodeEtat.Text + ", vous ne pouvez pas envoyer d' intervention ", "opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdMAJ_Click");
            }
        }




        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sType"></param>
        /// <param name="strphrase"></param>
        /// <param name="sMat"></param>
        /// <param name="lPDA"></param>
        /// <param name="lSMS"></param>
        /// <returns></returns>
        private int fc_historiseMes(string sType, string strphrase, string sMat, int lPDA, int lSMS)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_historiseMes() - Enter In The Function - sType = {sType} - strphrase = {strphrase} - sMat = {sMat} - lPDA = {lPDA} - lSMS = {lSMS}");
            //===> Fin Modif Mondir
            int functionReturnValue = 0;

            DataTable adors = default(DataTable);
            ModAdo modAdoadors = null;
            int lOrdre = 0;
            string sSQL = null;
            System.DateTime dtNow = default(System.DateTime);

            //=== historisation du message.
            sSQL = "SELECT     INMT_NoAuto, Nointervention, INMT_Ordre, Codeimmeuble, " + " INMT_DateEnvoie, INMT_StatutInter, INMT_StatutTomtom, INMT_LibelleStatut" +
                " , INMT_Texte, INMT_DateMaj, INMT_Type, INMT_PDA, INMT_SMS, Matricule  " + " From INMT_MsgTomtom" + " Where Nointervention =" + General.nz(txtNoIntervention.Text, 0) + " ORDER BY INMT_Ordre DESC";

            modAdoadors = new ModAdo();
            adors = modAdoadors.fc_OpenRecordSet(sSQL);

            if (adors.Rows.Count > 0)
            {
                lOrdre = Convert.ToInt32(Convert.ToInt32(General.nz(adors.Rows[0]["INMT_Ordre"], 0)) + 1);
            }
            else
            {
                lOrdre = 1;
            }

            functionReturnValue = lOrdre;

            var NewRow = adors.NewRow();

            dtNow = DateTime.Now;

            NewRow["NoIntervention"] = txtNoIntervention.Text;
            NewRow["INMT_Ordre"] = lOrdre;
            NewRow["CodeImmeuble"] = txtCodeImmeuble.Text;
            NewRow["INMT_DateEnvoie"] = dtNow;
            NewRow["INMT_StatutInter"] = "0A";
            NewRow["INMT_StatutTomtom"] = "";
            NewRow["INMT_LibelleStatut"] = "";
            NewRow["INMT_Texte"] = General.nz(strphrase, System.DBNull.Value);
            NewRow["INMT_DateMaj"] = dtNow;
            NewRow["INMT_Type"] = sType;
            NewRow["INMT_PDA"] = lPDA;
            NewRow["INMT_SMS"] = lSMS;
            NewRow["Matricule"] = sMat;
            adors.Rows.Add(NewRow);
            int xx = modAdoadors.Update();
            modAdoadors.Close();
            return functionReturnValue;


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Total"></param>
        /// <returns></returns>
        public int NumeroInform(int Total)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"NumeroInform() - Enter In The Function - Total = {Total}");
            //===> Fin Modif Mondir
            int functionReturnValue = 0;

            DataTable rsInform = default(DataTable);
            ModAdo modAdorsInform = null;
            int i = 0;
            int X = 0;

            try
            {
                i = 0;
                boolPro = false;

                modAdorsInform = new ModAdo();
                for (i = 0; i <= 10; i++)
                {
                    rsInform = modAdorsInform.fc_OpenRecordSet("SELECT NumInform.Autorise, NumInform.NumeroInform From NumInform WHERE NumInform.Autorise=0");
                    if (rsInform.Rows.Count > 0)
                    {

                        rsInform.Rows[0]["Autorise"] = true;
                        X = Convert.ToInt32(General.nz(rsInform.Rows[0]["NumeroInform"], 0));
                        int xx = modAdorsInform.Update();



                        var NewRow = rsInform.NewRow();
                        NewRow["NumeroInform"] = Total + X;
                        rsInform.Rows.Add(NewRow);
                        xx = modAdorsInform.Update();

                        functionReturnValue = X;

                        modAdorsInform.Close();
                        break;
                    }

                    modAdorsInform?.Close();
                    boolPro = true;
                }

                modAdorsInform?.Dispose();

                if (boolPro == true)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention une erreur est survenue sur la numérotation des fiches à envoyer.Les fiches n'ont pas été envoyé, Veuillez contacter votre administrateur", "Attention envoie annulé", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "module1;prcLiaison");
                return functionReturnValue;
            }
        }
        //Public Function Date1JourSemaine(strJour As String, dtDate As Date) As String
        //    Dim strJourBis        As String
        //    strJourBis = WeekdayName(Weekday(dtDate, vbMonday), , vbMonday)
        //    Do While UCase(strJour) <> UCase(strJourBis)
        //        dtDate = DateAdd("d", 1, dtDate)
        //        strJourBis = WeekdayName(Weekday(dtDate, vbMonday), , vbMonday)
        //    Loop
        //    Date1JourSemaine = dtDate
        //End Function

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdPrecedent_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdPrecedent_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            //Mondir : added to fix https://groupe-dt.mantishub.io/view.php?id=1745
            //Must wait untail load is finished to load the next inter
            //no exception in log, but the user has clicked two times on the next button, the load of the infos failded
            cmdPrecedent.Enabled = false;

            //===> Mondir le 26.03.2021, all commented code above has been commented to fix the ticket https://groupe-dt.mantishub.io/view.php?id=2289#c5716
            Navigate(2);

            //DataTable adorech = default(DataTable);
            //ModAdo modAdoadorech = null;

            //try
            //{
            //    longNumficheStandard = Convert.ToInt32(txtNumFicheStandard.Text);
            //    LongNoIntervention = Convert.ToInt32(txtNoIntervention.Text);

            //    var _with12 = adorech;
            //    modAdoadorech = new ModAdo();
            //    _with12 = modAdoadorech.fc_OpenRecordSet("SELECT Intervention.* From Intervention WHERE  Intervention.NumFicheStandard = " + longNumficheStandard +
            //        " AND Intervention.NoIntervention <'" + LongNoIntervention + "' order by NoIntervention DESC ");
            //    if (_with12.Rows.Count > 0)
            //    {
            //        LongNoIntervention = Convert.ToInt32(_with12.Rows[0]["NoIntervention"]);
            //        SaisIntervention(modAdoadorech);
            //        //fc_ChargeEnregistrement sNewVar
            //        // rechercheArticle
            //        if (!string.IsNullOrEmpty(txtCodeEtat.Text))
            //        {
            //            RechercheLibelleEtat();
            //        }
            //        else
            //        {
            //            lbllibcodeetat.Text = "";
            //        }
            //        modAdoadorech.Close();
            //    }
            //    modAdoadorech?.Dispose();
            //}
            //catch (Exception ex)
            //{
            //    Erreurs.gFr_debug(ex, this.Name + ";cmdPrecedent_Click");
            //}

            cmdPrecedent.Enabled = true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdPremier_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdPremier_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            //Mondir : added to fix https://groupe-dt.mantishub.io/view.php?id=1745
            //Must wait untail load is finished to load the next inter
            //no exception in log, but the user has clicked two times on the next button, the load of the infos failded
            cmdPremier.Enabled = false;

            //===> Mondir le 26.03.2021, all commented code above has been commented to fix the ticket https://groupe-dt.mantishub.io/view.php?id=2289#c5716
            Navigate(1);

            //DataTable adorech = default(DataTable);
            //ModAdo modAdoadorech = null;
            //If boolHisto = True Then
            //        With adoHisto
            //            .Resync
            //            .MoveFirst
            //            strCodeImmeuble = !CodeImmeuble & ""
            //            PrepIntervention
            //            LongNoIntervention = !NoIntervention
            //            SaisIntervention adoHisto
            //        End With
            //Else

            //try
            //{
            //    longNumficheStandard = Convert.ToInt32(txtNumFicheStandard.Text);

            //    modAdoadorech = new ModAdo();
            //    var _with13 = adorech;
            //    _with13 = modAdoadorech.fc_OpenRecordSet("SELECT Intervention.* From Intervention " +
            //                                             "WHERE  Intervention.NumFicheStandard = " + longNumficheStandard +
            //                                             " order by NoIntervention ");
            //    if (_with13.Rows.Count > 0)
            //    {
            //        LongNoIntervention = Convert.ToInt32(_with13.Rows[0]["NoIntervention"]);
            //        //txtJobNumber.Text = "1"
            //        SaisIntervention(modAdoadorech);
            //        // rechercheArticle
            //        if (!string.IsNullOrEmpty(txtCodeEtat.Text))
            //        {
            //            RechercheLibelleEtat();
            //        }
            //        else
            //        {
            //            lbllibcodeetat.Text = "";
            //        }
            //        modAdoadorech.Close();
            //    }
            //    modAdoadorech?.Dispose();
            //}
            //catch (Exception ex)
            //{
            //    Erreurs.gFr_debug(ex, this.Name + ";cmdPremier_Click");
            //}

            //End If

            cmdPremier.Enabled = true;

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sEtat"></param>
        /// <returns></returns>
        private string fc_ControleEtat(string sEtat)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"sEtat() - Enter In The Function - sEtat = {sEtat}");
            //===> Fin Modif Mondir
            string sAnalytiqueActivite = null;
            DataTable rsCodeEtat = default(DataTable);
            ModAdo modAdorsCodeEtat = null;

            modAdorsCodeEtat = new ModAdo();
            rsCodeEtat = modAdorsCodeEtat.fc_OpenRecordSet("SELECT AnalytiqueDevis,AnalytiqueContrat,AnalytiqueTravaux,RazAnalytique FROM TypeCodeEtat WHERE CodeEtat='" + sEtat + "'");

            if (rsCodeEtat.Rows.Count > 0)
            {
                if (rsCodeEtat.Rows[0]["RazAnalytique"] != DBNull.Value && Convert.ToInt32(rsCodeEtat.Rows[0]["RazAnalytique"]) == 1)
                {
                    txtINT_AnaActivite.Text = "";
                }
                else
                {
                    //===@@@ modif du 29 05 2017, desactive Sage.
                    if (General.sDesActiveSage == "1")
                    {
                        txtINT_AnaActivite.Text = "";
                    }
                    else
                    {
                        SAGE.fc_OpenConnSage();
                        using (var tmpModAdo = new ModAdo())
                            sAnalytiqueActivite = tmpModAdo.fc_ADOlibelle("SELECT CA_NUM FROM F_COMPTEA WHERE ActiviteDevis=" + General.nz(rsCodeEtat.Rows[0]["AnalytiqueDevis"], "0") +
                                " AND ActiviteTravaux=" + General.nz(rsCodeEtat.Rows[0]["AnalytiqueTravaux"], "0") + " AND ActiviteContrat=" + General.nz(rsCodeEtat.Rows[0]["AnalytiqueContrat"], "0"), false, SAGE.adoSage);
                        SAGE.fc_CloseConnSage();
                    }
                    if (!string.IsNullOrEmpty(sAnalytiqueActivite))
                    {
                        txtINT_AnaActivite.Text = sAnalytiqueActivite;
                    }
                }
            }
            modAdorsCodeEtat.Close();

            return sEtat;

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdRechercheIntervenant_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRechercheIntervenant_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {

                General.sSQL = "SELECT Personnel.Matricule as \"Matricule\", Personnel.Nom as \"NOM\",Personnel.prenom as \"Prenom\",Personnel.Memoguard, Qualification.CodeQualif as \"Code Qualif\"," +
                    " Qualification.Qualification as \"Qualification\", Personnel.NumRadio " + " FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";


                string requete = "SELECT Personnel.Matricule as \"Matricule \"," + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\"," + " Qualification.Qualification as \"Qualification\"," +
                " Personnel.Memoguard as \"MemoGuard\"," + " Personnel.NumRadio as \"NumRadio\"" + " FROM Personnel LEFT JOIN Qualification ON Personnel.CodeQualif=Qualification.CodeQualif";
                string where_order = " (NonActif is null or NonActif = 0) ";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "" };
                if (General.IsNumeric(txtIntervenant.Text))
                    fg.SetValues(new Dictionary<string, string> { { "Matricule", txtIntervenant.Text } });
                else
                    fg.SetValues(new Dictionary<string, string> { { "Nom", txtIntervenant.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    // charge les enregistrements de l'immeuble
                    txtIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtMatriculeIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtMemoGuard.Text = fg.ugResultat.ActiveRow.Cells[5].Value.ToString();
                    //lbllibIntervenant = .dbgrid1.Columns(1).value
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        // charge les enregistrements de l'immeuble
                        txtIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtMatriculeIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtMemoGuard.Text = fg.ugResultat.ActiveRow.Cells[5].Value.ToString();
                        //lbllibIntervenant = .dbgrid1.Columns(1).value
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void CmdRechercheTech_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"CmdRechercheTech_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            string sCode = null;


            try
            {
                string requete = "SELECT  Matricule as \"Matricule\", Nom as \"Nom\", Prenom as \"Prenom\" From Personnel";
                string where_order = " (NonActif is null or NonActif = 0) ";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Associer un dispatcheur à l'intervention N° " + txtNoIntervention.Text };
                if (General.IsNumeric(txtLibDispatch.Text))
                    fg.SetValues(new Dictionary<string, string> { { "Matricule", txtLibDispatch.Text } });
                else
                    fg.SetValues(new Dictionary<string, string> { { "Nom", txtLibDispatch.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtDispatcheur.Value = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtLibDispatch.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtDispatcheur.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtLibDispatch.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheTech_Click");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void CmdRetour_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"CmdRetour_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            bool bcontrole = false;
            bcontrole = false;

            if (fc_sauver(ref bcontrole) == true)
            {
                General.saveInReg(Variable.cUserDispatch, "NewVar", txtNoIntervention.Text);
                //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDispatch);
                View.Theme.Theme.Navigate(typeof(UserDispatch));
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdSave_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdSave_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            CmdSauver_Click(CmdSauver, new System.EventArgs());
        }

        private void cmdStatutHisto_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdStatutHisto_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            //=== historique des rapports
            frmHistoStatutInter frmHistoStatutInter = new frmHistoStatutInter();
            frmHistoStatutInter.txtNointervention.Text = txtNoIntervention.Text;
            frmHistoStatutInter.ShowDialog();
        }


        /// <summary>
        /// Mondir le 26.03.2021, https://groupe-dt.mantishub.io/view.php?id=2289
        /// direction == 0 ==> Same Intervention, update position, 1/5 or 2/5 ...
        /// direction == 1 ===> First
        /// direction == 2 ===> Left
        /// direction == 3 ===> Right
        /// direction == 4 ===> Last
        /// </summary>
        /// <param name="direction"></param>
        /// <returns></returns>
        private void Navigate(int direction)
        {
            var nextNoIntervention = 0;
            var longNumficheStandard = Convert.ToInt32(txtNumFicheStandard.Text);
            var longNoIntervention = Convert.ToInt32(txtNoIntervention.Text);

            var modAdoadorech = new ModAdo();
            //var query = $@"SELECT NoIntervention FROM Intervention
            //            WHERE NumFicheStandard = '{longNumficheStandard}'
            //            ORDER BY
            //            CASE WHEN Intervention.DateRealise IS NOT NULL THEN Intervention.DateRealise END,
            //            CASE WHEN Intervention.DateRealise IS NULL THEN Intervention.DateSaisie END,
            //            CASE WHEN Intervention.HeureDebut IS NOT NULL THEN CONVERT(TIME, Intervention.HeureDebut) END,
            //            CASE WHEN Intervention.HeureDebut IS NULL THEN CONVERT(TIME, '23:59:59') END";

            //===> Mondir le 12.05.2021, Xavier a demandé une autre modif https://groupe-dt.mantishub.io/view.php?id=2289#c6012
            //var query = $@"SELECT * FROM Intervention
            //               WHERE NumFicheStandard = '{longNumficheStandard}'
            //               ORDER BY
            //                CASE
            //                 WHEN Intervention.DateRealise IS NOT NULL THEN 1
            //                 WHEN Intervention.DateRealise IS NULL THEN 2 END ASC,
            //                   Intervention.DateRealise,
            //                   CASE
            //                 WHEN Intervention.HeureDebut IS NOT NULL THEN CONVERT(TIME, Intervention.HeureDebut)
            //                 WHEN Intervention.HeureDebut IS NULL THEN CONVERT(TIME, '23:59:59') END";

            //  var query = $@"SELECT * FROM Intervention
            //                 WHERE NumFicheStandard = '{longNumficheStandard}'
            //                 ORDER BY
            //                  CASE
            //                   WHEN Intervention.DateRealise IS NOT NULL THEN 1
            //                   WHEN Intervention.DateRealise IS NULL THEN 2 END ASC,
            //                     Intervention.DateRealise,
            //Intervention.NoIntervention,
            //                     CASE
            //                   WHEN Intervention.HeureFin IS NOT NULL THEN CONVERT(TIME, Intervention.HeureFin)
            //                   WHEN Intervention.HeureFin IS NULL THEN CONVERT(TIME, '23:59:59') END";
            //===> Mondir le 14.05.2021, https://groupe-dt.mantishub.io/view.php?id=2289#c6024
            var query = $@"SELECT NoIntervention, Intervention.DateRealise, Intervention.HeureFin FROM Intervention
            WHERE NumFicheStandard = '{longNumficheStandard}'
            ORDER BY

            CASE
                WHEN Intervention.DateRealise IS NOT NULL THEN 1

            WHEN Intervention.DateRealise IS NULL THEN 2 END,
            Intervention.DateRealise,
            CASE
                WHEN Intervention.HeureFin IS NOT NULL THEN 1

            WHEN Intervention.HeureFin IS NULL THEN 2 END,
            Intervention.HeureFin,
            Intervention.NoIntervention";
            //===> Fin Modif Mondir

            var dt = modAdoadorech.fc_OpenRecordSet(query);

            if (dt.Rows.Count > 0)
            {
                var currentInterventionIndex = dt.Rows.Cast<DataRow>()
                                                      .ToList()
                                                      .FindIndex(i => i["NoIntervention"].ToString() == longNoIntervention.ToString());
                switch (direction)
                {
                    case 1:
                        nextNoIntervention = dt.Rows[0]["NoIntervention"].ToInt();
                        break;
                    case 2:
                        currentInterventionIndex--;
                        if (currentInterventionIndex < 0)
                            currentInterventionIndex = 0;
                        nextNoIntervention = dt.Rows[currentInterventionIndex]["NoIntervention"].ToInt();
                        break;
                    case 3:
                        currentInterventionIndex++;
                        if (currentInterventionIndex > dt.Rows.Count - 1)
                            currentInterventionIndex = dt.Rows.Count - 1;
                        nextNoIntervention = dt.Rows[currentInterventionIndex]["NoIntervention"].ToInt();
                        break;
                    case 4:
                        nextNoIntervention = dt.Rows[dt.Rows.Count - 1]["NoIntervention"].ToInt();
                        break;
                }

                txtJobNumber.Text = $"{currentInterventionIndex + 1}/{dt.Rows.Count}";

                //Dont need to load data coz we only need to update the textbox txtJobNumber.Text
                if (direction == 0)
                {
                    return;
                }

                var modAdoIntervention = new ModAdo();
                modAdoIntervention.fc_OpenRecordSet($"SELECT * FROM Intervention WHERE NoIntervention = '{nextNoIntervention}'");

                SaisIntervention(modAdoIntervention);
                // rechercheArticle
                if (!string.IsNullOrEmpty(txtCodeEtat.Text))
                {
                    RechercheLibelleEtat();
                }
                else
                {
                    lbllibcodeetat.Text = "";
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdSuivant_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdSuivant_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            //Mondir : added to fix https://groupe-dt.mantishub.io/view.php?id=1745
            //Must wait untail load is finished to load the next inter
            //no exception in log, but the user has clicked two times on the next button, the load of the infos failded
            cmdSuivant.Enabled = false;


            //===> Mondir le 26.03.2021, all commented code above has been commented to fix the ticket https://groupe-dt.mantishub.io/view.php?id=2289#c5716
            Navigate(3);
            //DataTable adorech = default(DataTable);
            //ModAdo modAdoadorech = null;

            try
            {
                //longNumficheStandard = Convert.ToInt32(txtNumFicheStandard.Text);
                //LongNoIntervention = Convert.ToInt32(txtNoIntervention.Text);
                //modAdoadorech = new ModAdo();

                //var _with19 = adorech;
                //_with19 = modAdoadorech.fc_OpenRecordSet("SELECT Intervention.* From Intervention WHERE  Intervention.NumFicheStandard = " + longNumficheStandard +
                //    " AND Intervention.NoIntervention >" + LongNoIntervention + " ORDER BY NoIntervention ");
                //if (_with19.Rows.Count > 0)
                //{
                //    LongNoIntervention = Convert.ToInt32(_with19.Rows[_with19.Rows.Count - 1]["NoIntervention"]);

                //    //txtJobNumber.Text = General.IsNumeric(jobNumber)  ? (Convert.ToInt32(jobNumber) + 1).ToString() : "0";

                //    SaisIntervention(modAdoadorech);
                //    //fc_ChargeEnregistrement sNewVar
                //    // rechercheArticle
                //    if (!string.IsNullOrEmpty(txtCodeEtat.Text))
                //    {
                //        RechercheLibelleEtat();
                //    }
                //    else
                //    {
                //        lbllibcodeetat.Text = "";
                //    }
                //    modAdoadorech.Close();
                //}
                //modAdoadorech.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdSuivant_Click");
            }

            cmdSuivant.Enabled = true;
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdSupInterPDA_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdSupInterPDA_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            string sInter = null;
            string sTexte = null;

            try
            {
                if (string.IsNullOrEmpty(txtIntervenant.Text))
                {

                    sTexte = "L'intervenant est obligatoire.";
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sTexte, "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (string.IsNullOrEmpty(txtNoIntervention.Text))
                {

                    sTexte = "Le numéro d'intervention est obligatoire.";
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sTexte, "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                if (txtCodeEtat.Text.ToUpper() == "03")
                {
                    sTexte = "L'intervention a été commencée, elle ne peut donc pas être supprimée.";
                    return;
                }

                //===> Mondir le 18.01.2021 pour corriger https://groupe-dt.mantishub.io/view.php?id=2179
                using (var tmpModAdo = new ModAdo())
                {
                    var interInDb = tmpModAdo.fc_ADOlibelle($"SELECT Intervenant FROM Intervention WHERE NoIntervention = '{txtNoIntervention.Text}'");
                    if (interInDb != txtIntervenant.Text)
                    {
                        Program.SaveException(null, $"cmdSupInterPDA_Click - Try to cancel Inter but no success");
                        CustomMessageBox.Show("L'intervenant dans la base de données est différent de celui affiché sur votre écran.\nRechargez cette fiche et réessayez.\nSi le problème persiste, veuillez contacter AXECIEL.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                //===> Fin Modif Mondir

                using (var tmpModAdo = new ModAdo())
                    sInter = tmpModAdo.fc_ADOlibelle("Select nom from personnel where matricule ='" + StdSQLchaine.gFr_DoublerQuote(txtMatriculeIntervenant.Text) + "'");


                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous supprimer cette intervention sur le PDA de " + sInter + ".", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {

                    return;
                }


                if (fc_sendPDAcorrective(Convert.ToInt32(txtNoIntervention.Text), true, DateTime.Now, txtIntervenant.Text) == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Une erreur est survenue, l'opération a été annulée.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                else
                {
                    sTexte = "Intervention supprimée sur le PDA";
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sTexte, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtCodeEtat.Text = "00";

                    General.sActiveCtlSatutInter = "0";
                    //CmdSauver_Click(CmdSauver, new System.EventArgs());
                    Save();
                    General.sActiveCtlSatutInter = "1";

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdSupInterPDA_Click");
            }


        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdVideo_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdVideo_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            string sNameFile = null;
            int j = 0;
            string[] tName = null;
            string sCodeImmeuble = null;

            try
            {
                sCodeImmeuble = txtCodeImmeuble.Text;

                frmAfficheFichier frmAfficheFichier = new frmAfficheFichier();
                frmAfficheFichier.LstFichier.Items.Clear();

                Video.fc_RecherheVideo(sCodeImmeuble, ref sNameFile, false, true);

                if (!string.IsNullOrEmpty(sNameFile))
                {
                    tName = sNameFile.Split(';');
                    for (j = 0; j <= tName.Length - 1; j++)
                    {
                        if (tName[j] != "")
                            frmAfficheFichier.LstFichier.Items.Add(General.sCheminDossier + "\\" + sCodeImmeuble + "\\Video\\" + tName[j]);


                    }
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le système n'a pas trouvé de vidéo pour l'immeuble " + sCodeImmeuble + ".", "Aucun résultat", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                frmAfficheFichier.lblTexte.Text = "Le système à trouvé " + j + " vidéo(s)." + "\n" + "Veuillez double cliquer sur un des fichier pour la visualiser.";
                frmAfficheFichier.ShowDialog();
                frmAfficheFichier.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdVideo_Click");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdWave_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdWave_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            bool boolFolder = false;
            bool boolMp3 = false;
            string sMp3 = null;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                if (!string.IsNullOrEmpty(txtWave.Text))
                {
                    //== modifie l'extension wav en mp3 et ouvre le fichier si existant.
                    sMp3 = General.Mid(txtWave.Text, 1, txtWave.Text.Length - 3) + "mp3";
                    // sMp3 = txtWave
                    boolMp3 = File.Exists(sMp3);
                    fso = null;
                    if (boolMp3 == true)
                    {
                        ModuleAPI.Ouvrir(sMp3);
                        return;
                    }
                    else
                    {
                        boolFolder = File.Exists(txtWave.Text);
                        fso = null;
                        if (boolFolder == true)
                        {
                            ModuleAPI.Ouvrir(txtWave.Text);
                            return;
                        }
                    }
                    if (boolFolder == false && boolMp3 == false)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le fichier son a été déplacé ou supprimé.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    ModuleAPI.Ouvrir(sMp3);
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun rapport d'intervention vocal n'est associé a cette intervention", "Document non trouvé", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdWave_Click");
            }


            //Dim strKobby        As String
            // On Error Resume Next
            //     Close #1
            // On Error GoTo 0
            // On Error GoTo Erreur
            //        ' si la validation ne se passe pas correctement, on annule l'envoie de message.
            //        If fc_sauver = False Then
            //            Exit Sub
            //        End If
            //        If txtIntervenant = "" Then
            //            MsgBox "Veuilez saisir un intervenant."
            //            Exit Sub
            //        End If
            //        If UCase(txtCodeEtat) = UCase("TD") _
            //'            Or UCase(txtCodeEtat) = UCase("TK") Or UCase(txtCodeEtat) = UCase("P") Then
            //
            //        Else
            //
            //            MsgBox "la transmission vers le Kobby ne se fait uniquement avec les" _
            //'            & " statuts TK ou TD." _
            //'            , vbCritical, "Opération annulée"
            //            Exit Sub
            //        End If
            //
            //        strKobby = FindNoKobby(txtIntervenant)
            //        If strKobby = "" Then
            //            MsgBox "Le message n'a pas été envoyé, l' intervenant " & lblLibIntervenant & " ne possede pas de numéro de kobby", vbCritical, ""
            //            Exit Sub
            //        End If
            //
            //        Open CHEMINKOBBY & txtNoIntervention & ".TXT" For Output As 1
            //        Print #1, fncUserName
            //        Print #1, strKobby
            //        strKobby = " No:" & txtNoIntervention & "-" & txtAdresse & " " & Trim(txtAdresse2_IMM) & " " & Trim(txtVille) & " " & Trim(txtDesignation) & " " & txtCommentaire & " " & Trim(txtCommentaire2) & " " & Trim(txtCommentaire3)
            //        If txtCodeAcces1 <> "" Then
            //            strKobby = strKobby & " code " & txtCodeAcces1
            //        End If
            //        If txtCodeAcces2 <> "" Then
            //            strKobby = strKobby & " code " & txtCodeAcces2
            //        End If
            //
            //        Print #1, Left(strKobby, 380)
            //        Close #1
            //        txtCodeEtat = "TI"
            //         lbllibcodeetat = fc_ADOlibelle("SELECT  LibelleCodeEtat" _
            //'                                  & " From TypeCodeEtat where CodeEtat='" & txtCodeEtat & "'")
            //
            //Exit Sub
            //Erreur:
            //MsgBox err.Description, vbCritical, err.Source

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Command1_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"Command1_Click() - Enter In The Function");
            //===> Fin Modif Mondir


            //==============================> Hey you ! <==============================
            //===> Mondir le 19.01.2021, please check LoadArticle() and txtArticle_Validating if are you going to change this event
            //===>  Fin Modif Mondir


            string sCode = null;

            try
            {
                string requete = "SELECT CodeArticle as \"Code opération\", Designation1 as \"Designation\"" + " From FacArticle";
                //=== modif du 13 12 2015, ajout du champs Masquer.
                string where_order = " Masquer IS NULL OR Masquer =  " + General.fc_bln(false);
                if (General.sCaterogieArticleInter == "1")
                {
                    //===> Mondir le 19.01.2021, ajouter ( dans la reuqette pour corriger #2175
                    where_order = " (CodeCategorieArticle ='I' OR CodeCategorieArticle ='GP') AND (Masquer IS NULL OR Masquer =   " + General.fc_bln(false) + ") ";
                }
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "" };
                fg.SetValues(new Dictionary<string, string> { { "CodeArticle", txtArticle.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    // charge les enregistrements de l'immeuble
                    txtArticle.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtDesignation.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    using (var tmpModAdo = new ModAdo())
                    {
                        chkCompteurObli.CheckState = (CheckState)Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT SaisieCompteur FROM FacArticle where CodeArticle='" + StdSQLchaine.gFr_DoublerQuote(txtArticle.Text) + "'"), 0));
                        chkUrgence.CheckState = (CheckState)Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Urgent FROM FacArticle where CodeArticle='" + StdSQLchaine.gFr_DoublerQuote(txtArticle.Text) + "'"), 0));
                    }
                    //===> Mondir le 19.01.2021, commented to fix https://groupe-dt.mantishub.io/view.php?id=2175
                    //txtArticle_Validating(txtArticle, new System.ComponentModel.CancelEventArgs(false));
                    //===> Fin Modif Mondir
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        // charge les enregistrements de l'immeuble
                        txtArticle.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtDesignation.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        using (var tmpModAdo = new ModAdo())
                        {
                            chkCompteurObli.CheckState = (CheckState)Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT SaisieCompteur FROM FacArticle where CodeArticle='" + StdSQLchaine.gFr_DoublerQuote(txtArticle.Text) + "'"), 0));
                            chkUrgence.CheckState = (CheckState)Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Urgent FROM FacArticle where CodeArticle='" + StdSQLchaine.gFr_DoublerQuote(txtArticle.Text) + "'"), 0));
                        }
                        //===> Mondir le 19.01.2021, commented to fix https://groupe-dt.mantishub.io/view.php?id=2175
                        //txtArticle_Validating(txtArticle, new System.ComponentModel.CancelEventArgs(false));
                        //===> Fin Modif Mondir
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }

        }

        /// <summary>
        /// TODO : Mondir - Must Test
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Command2_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"Command2_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                frmAfficheTexte frmAfficheTexte = new frmAfficheTexte();
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                var _with21 = new DataTable();
                ModAdo modAdoAdodc1 = new ModAdo();

                // GetSetting(cFrNomApp, "App", "Source_OLEDB", "")
                _with21 = modAdoAdodc1.fc_OpenRecordSet("SELECT TechReleve.Obs as [Commentaire technique], Immeuble.Observations as [Commentaire Immeuble], Table1.Observations as [Commentaire Gérant] FROM (Table1 RIGHT JOIN Immeuble ON Table1.Code1 = Immeuble.Code1) LEFT JOIN TechReleve ON Immeuble.CodeImmeuble = TechReleve.CodeImmeuble where Immeuble.CodeImmeuble ='" + txtCodeImmeuble.Text + "'");

                var _with22 = frmAfficheTexte.SSOleDBGrid1;

                _with22.DataSource = _with21;

                _with22.DisplayLayout.Bands[0].Columns[0].Width = 310;
                _with22.DisplayLayout.Bands[0].Columns[1].Width = 310;
                _with22.DisplayLayout.Bands[0].Columns[2].Width = 310;
                _with22.DisplayLayout.Bands[0].Override.DefaultRowHeight = 300;
                _with22.DisplayLayout.Bands[0].Columns[0].CellActivation = Activation.NoEdit;
                _with22.DisplayLayout.Bands[0].Columns[1].CellActivation = Activation.NoEdit;
                _with22.DisplayLayout.Bands[0].Columns[2].CellActivation = Activation.NoEdit;
                frmAfficheTexte.ShowDialog();
                frmAfficheTexte.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Command2_Click");
            }
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdAnalytique_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAnalytique_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            bool bcontrole = false;
            bcontrole = false;
            if (fc_sauver(ref bcontrole) == true)
            {
                //    If txtNoIntervention <> "" Then
                //        ' stocke la position de la fiche intervention
                //        fc_SaveParamPosition Me.Name, cUserIntervention, txtNoIntervention
                //        ' stocke les paramétres pour la feuille de destination.
                //        SaveSetting cFrNomApp, cUserAnalytiqueAffaire, "NumFicheStandard", txtNumFichestandard.Text
                //        SaveSetting cFrNomApp, cUserAnalytiqueAffaire, "CodeImmeuble", txtCodeImmeuble.Text
                //        SaveSetting cFrNomApp, cUserAnalytiqueAffaire, "AnalytiqueActivité", ""
                //        SaveSetting cFrNomApp, cUserAnalytiqueAffaire, "Gerant", ""
                //        SaveSetting cFrNomApp, cUserAnalytiqueAffaire, "AppelDu", ""
                //        SaveSetting cFrNomApp, cUserAnalytiqueAffaire, "AppelAu", ""
                //        SaveSetting cFrNomApp, cUserAnalytiqueAffaire, "ImmeubleDu", ""
                //        SaveSetting cFrNomApp, cUserAnalytiqueAffaire, "ImmeubleAu", ""
                //
                //        If txtCodeEtat.Text = "ZZ" Then
                //            SaveSetting cFrNomApp, cUserAnalytiqueAffaire, "AnalytiqueP2", True
                //            SaveSetting cFrNomApp, cUserAnalytiqueAffaire, "AnalytiqueTravaux", False
                //        ElseIf txtCodeEtat.Text = "AF" Or txtCodeEtat.Text = "S" Or txtCodeEtat.Text = "T" Or txtCodeEtat.Text = "E" Or txtCodeEtat.Text = "D" Then
                //            SaveSetting cFrNomApp, cUserAnalytiqueAffaire, "AnalytiqueTravaux", True
                //            SaveSetting cFrNomApp, cUserAnalytiqueAffaire, "AnalytiqueP2", False
                //        End If
                //
                //    End If
                //        fc_Navigue Me, gsCheminPackage & PROGUserAnalytiqueAffaire



                if (!string.IsNullOrEmpty(txtNoIntervention.Text))
                {
                    // stocke la position de la fiche intervention
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, txtNoIntervention.Text);
                    // stocke les paramétres pour la feuille de destination.

                    General.saveInReg(Variable.cUserDocAnalytique2, "NewVar", txtNumFicheStandard.Text);

                    General.saveInReg(Variable.cUserDocAnalytique2, "CodeImmeuble", "");
                    General.saveInReg(Variable.cUserDocAnalytique2, "Gerant", "");

                    //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocAnalytique2);
                    View.Theme.Theme.Navigate(typeof(UserDocAnalytique2));
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Navigation annulée, aucune intervention en cours.", "Navigation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

                }


            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Command3_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //
            //
            //
            //
            //          This Button Is Hidden
            //
            //
            //
            //
            //


            //ADODB.Recordset rsIntervDebourse = default(ADODB.Recordset);
            //string sqlIntervDebourse = null;
            //int nbDemiHeure = 0;

            //sqlIntervDebourse = "SELECT HeureDebut,HeureFin,Duree,Debourse FROM Intervention";
            //sqlIntervDebourse = sqlIntervDebourse + " WHERE (NOT HeureFin IS NUL AND HeureFin<>''";
            //sqlIntervDebourse = sqlIntervDebourse + " AND NOT HeureDebut IS NULL and HeueDebut<>'')";
            //sqlIntervDebourse = sqlIntervDebourse + " OR (NOT Duree IS NULL AND Duree<>'')";
            //rsIntervDebourse = new ADODB.Recordset();

            //rsIntervDebourse.Open(sqlIntervDebourse, General.adocnn, ADODB.CursorTypeEnum.adOpenStatic);

            //if (!rsIntervDebourse.EOF & !rsIntervDebourse.bof)
            //{
            //    rsIntervDebourse.MoveFirst();
            //    while (!rsIntervDebourse.EOF)
            //    {

            //        rsIntervDebourse.MoveNext();
            //    }
            //}


            //if (General.nz(ref (txtDebourse.Text), ref "0") == "0" | txtDebourse.Text == "0.00" & Information.IsDate(txtDuree.Text))
            //{
            //    //Découpage en demi-heure : 1800 secondes
            //    if ((((DateAndTime.Hour(Convert.ToDateTime(txtDuree.Text)) * 3600) + (DateAndTime.Minute(Convert.ToDateTime(txtDuree.Text)) * 60) + DateAndTime.Second(Convert.ToDateTime(txtDuree.Text))) % 1800) > 0)
            //    {
            //        nbDemiHeure = Convert.ToInt32(((DateAndTime.Hour(Convert.ToDateTime(txtDuree.Text)) * 3600) + (DateAndTime.Minute(Convert.ToDateTime(txtDuree.Text)) * 60) + DateAndTime.Second(Convert.ToDateTime(txtDuree.Text))) / 1800) + 1;
            //    }
            //    else
            //    {
            //        nbDemiHeure = Convert.ToInt32(((DateAndTime.Hour(Convert.ToDateTime(txtDuree.Text)) * 3600) + (DateAndTime.Minute(Convert.ToDateTime(txtDuree.Text)) * 60) + DateAndTime.Second(Convert.ToDateTime(txtDuree.Text))) / 1800);
            //    }
            //    //Taux horaire moyen : 26 €
            //    //Déplacement moyen : 26 €
            //    txtDebourse.Text = Convert.ToString(nbDemiHeure * (26 / 4) + 26);
            //}

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Label39_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"Label39_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            //    ' stocke la position de la fiche intervention
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, txtNoIntervention.Text);
            //    ParamVisu = "Type d'operation"
            //    ' Envoie vers le document
            View.Theme.Theme.Navigate(typeof(UserDocArticle));
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Label42_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"Label42_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            lblGoClient_DblClick();
        }

        //TODO : Mondir - Has Something To Do With The Menu
        //private void Label5_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    short Index = Label5.GetIndex(eventSender);

        //    switch (Index)
        //    {
        //        case 4:
        //            General.LienSite = "http://www.gecet.net";
        //            ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocSite);
        //            break;
        //        default:
        //            lblGoImmeuble_DblClick();
        //            break;
        //    }

        //}

        //TODO : Mondir - Has Something To Do With The Menu
        //private void LabelAppel_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    short Index = LabelAppel.GetIndex(eventSender);
        //    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        //    ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocStandard);
        //}

        //TODO : Mondir - Has Something To Do With The Menu
        //private void LabelDispatchDevis_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    short Index = LabelDispatchDevis.GetIndex(eventSender);
        //    ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDispatchDevis);
        //}

        //TODO : Mondir - Has Something To Do With The Menu
        //private void LabelDispatchIntervention_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        //    ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDispatch);
        //}

        //TODO : Mondir - Has Something To Do With The Menu
        //private void LabelGerant_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    short Index = LabelGerant.GetIndex(eventSender);
        //    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        //    ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocClient);
        //}

        //TODO : Mondir - Has Something To Do With The Menu
        //private void LabelImmeuble_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    short Index = LabelImmeuble.GetIndex(eventSender);
        //    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        //    ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocImmeuble);
        //}

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Label17_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"Label17_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            //TODO : Mondir - This Button Is Hidden
            //bool bcontrole = false;
            //bcontrole = false;

            //object[] tabSetting = new object[2];
            //object[] tabTemp = new object[1];

            //// attente fin de saison de chauffe interdit la creation des bons.
            //if (txtCodeEtat.Text == cAFSC)
            //{
            //    Interaction.MsgBox("Le statut " + txtCodeEtat.Text + " ne vous permet pas de créer" + " un bon de commande.", MsgBoxStyle.Critical, "");
            //    return;
            //}
            ////If OptSaison = True Then
            ////    MsgBox cNOBON, vbCritical, ""
            ////    Exit Sub
            ////End If
            ////If UCase(txtCodeEtat) = "G" And chkINT_facturable.Value = 1 Then
            ////    MsgBox "Le statut G (garantie) ne peut étre facturable, Décochez la case facturable.", vbCritical, ""
            ////    Exit Sub
            ////End If
            ////If UCase(txtCodeEtat) = "GT" And chkINT_facturable.Value = 1 Then
            ////    MsgBox "Le statut GT (garantie) ne peut étre facturable, Décochez la case facturable.", vbCritical, ""
            ////    Exit Sub
            ////End If

            ////If optRegie = True And chkINT_facturable.value = 0 And UCase(txtCodeEtat) <> "G" Then
            ////    MsgBox "Pour créer un bon de commande en Regie, cocher la case facturable.", vbCritical, ""
            ////    Exit Sub
            ////End If
            ////If Optdevis = True And chkINT_facturable.value = 0 Then
            ////    MsgBox "Pour créer un bon de commande pour le devis, cocher la case facturable.", vbCritical, ""
            ////    Exit Sub
            ////End If

            //string MaxOrdre = null;
            //int cleAutoFNS = 0;


            //if (fc_sauver(ref bcontrole) == true)
            //{

            //    MaxOrdre = ModAdo.fc_ADOlibelle("SELECT MAX(Ordre) as MaxOrdre FROM BonDeCommande WHERE NumFicheStandard=" + txtNumFicheStandard.Text);

            //    if (!string.IsNullOrEmpty(MaxOrdre) & MaxOrdre != "0")
            //    {
            //        cleAutoFNS = Convert.ToInt32(General.nz(ref ModAdo.fc_ADOlibelle("SELECT CodeFourn FROM BonDeCommande WHERE NumFicheStandard=" + txtNumFicheStandard.Text + " AND Ordre=" + MaxOrdre), ref 0));
            //        Interaction.SaveSetting(General.cFrNomApp, "UserBCmdHead", "NoBCmd", ModAdo.fc_ADOlibelle("SELECT NoBonDeCommande FROM BonDeCommande WHERE NumFicheStandard=" + txtNumFicheStandard.Text + " AND Ordre=" + MaxOrdre));
            //        Interaction.SaveSetting(General.cFrNomApp, "UserBCmdHead", "NoInt", ModAdo.fc_ADOlibelle("SELECT NoIntervention FROM BonDeCommande WHERE NumFicheStandard=" + txtNumFicheStandard.Text + " AND Ordre=" + MaxOrdre));
            //        Interaction.SaveSetting(General.cFrNomApp, "UserBCmdHead", "NoImmeuble", txtCodeImmeuble.Text);
            //        Interaction.SaveSetting(General.cFrNomApp, "UserBCmdHead", "NoFNS", ModAdo.fc_ADOlibelle("SELECT Code FROM FournisseurArticle WHERE Cleauto=" + cleAutoFNS));
            //        ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUderBCmdBody);
            //    }
            //    else
            //    {
            //        Interaction.SaveSetting(General.cFrNomApp, "UserBCmdHead", "NoInt", txtNoIntervention.Text);
            //        Interaction.SaveSetting(General.cFrNomApp, "UserBCmdHead", "NoImmeuble", txtCodeImmeuble.Text);
            //        Interaction.SaveSetting(General.cFrNomApp, "UserBCmdHead", "NoBcmd", "");
            //        ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserBCmdHead);
            //    }

            //    //    SQL = "SELECT BonDeCommande.NoBonDeCommande FROM BonDeCommande WHERE BonDeCommande.NoIntervention='" & txtNoIntervention.Text & "'"
            //    //
            //    //    If rstmp Is Nothing Then
            //    //        Set rstmp = New ADODB.Recordset
            //    //    Else
            //    //        If rstmp.State = adStateOpen Then
            //    //            rstmp.Close
            //    //        End If
            //    //    End If
            //    //
            //    //    rstmp.Open SQL, adocnn, adOpenStatic, adLockReadOnly
            //    //
            //    //        fc_SaveParamPosition Me.Name, cUserBCmdHead, ""
            //    //        tabSetting(0) = "NoInt"
            //    //        tabSetting(1) = txtNoIntervention.Text
            //    //        tabTemp(0) = tabSetting
            //    //        Call stockRegParam("UserBCmdHead", tabTemp)
            //    //        tabSetting(0) = "NoImmeuble"
            //    //        tabSetting(1) = txtCodeImmeuble
            //    //        tabTemp(0) = tabSetting
            //    //        Call stockRegParam("UserBCmdHead", tabTemp)

            //    //    If rstmp.RecordCount <= 1 Then
            //    //        If Not rstmp.EOF And Not rstmp.BOF Then
            //    //            SaveSetting cFrNomApp, "UserBCmdHead", "NoBcmd", rstmp!NoBonDeCommande
            //    //        Else
            //    //            SaveSetting cFrNomApp, "UserBCmdHead", "NoBcmd", ""
            //    //        End If
            //    //        fc_Navigue Me, gsCheminPackage & PROGUserBCmdHead
            //    //    Else
            //    //        'SaveSetting cFrNomApp, "UserBCmdHead", "NoBcmd", ""
            //    //        fc_Navigue Me, gsCheminPackage & PROGUserBCLivraison
            //    //    End If
            //    //
            //    //    rstmp.Close
            //    //    Set rstmp = Nothing

            //}
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void lblGoClient_DblClick()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"lblGoClient_DblClick() - Enter In The Function");
            //===> Fin Modif Mondir
            bool bcontrole = false;
            bcontrole = false;

            if (fc_sauver(ref bcontrole) == true)
            {

                if (!string.IsNullOrEmpty(txtNoIntervention.Text))
                {
                    // stocke la position de la fiche immeuble
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, txtNoIntervention.Text);
                    // stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, txtCode1.Text);
                }
                View.Theme.Theme.Navigate(typeof(UserDocClient));
            }
        }






        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void ssComboActivite_DropDown(System.Object eventSender, System.EventArgs eventArgs)
        {


            ///
            ///
            ///
            ///  Mondir : This Controle Is Hidden !!
            ///
            ///
            ///



            //General.sSQL = "SELECT ACT_Code, ACT_Libelle" + " FROM  ACT_AnaActivite order by ACT_Code";
            //sheridan.InitialiseCombo((this.ssComboActivite), Adodc5, General.sSQL, "ACT_Code");
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txt_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txt_Validating() - Enter In The Function");
            //===> Fin Modif Mondir
            var txt = eventSender as iTalk.iTalk_TextBox_Small2;
            int Index = Convert.ToInt32(txt.Tag);

            switch (Index)
            {
                case 16:
                    if (!General.IsNumeric(txt16.Text) && !string.IsNullOrEmpty(txt16.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(" durée prévue doit être de type numérique.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        eventArgs.Cancel = true;
                    }
                    break;

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtArticle_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtArticle_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir
            int KeyAscii = (int)(eventArgs.KeyChar);
            if (KeyAscii == 13)
            {
                using (var tmpModAdo = new ModAdo())
                    if (string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT CodeArticle FROM FacArticle WHERE CodeArticle='" + StdSQLchaine.gFr_DoublerQuote(txtArticle.Text) + "'"), "").ToString()))
                    {
                        General.blControle = true;
                        Command1_Click(Command1, new System.EventArgs());
                        KeyAscii = 0;
                    }
                    else
                    {
                        txtDesignation.Text = General.nz(tmpModAdo.fc_ADOlibelle("SELECT DESIGNATION1 FROM FacArticle where CodeArticle='" + StdSQLchaine.gFr_DoublerQuote(txtArticle.Text) + "'"), "").ToString();
                        chkCompteurObli.CheckState = (CheckState)Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT SaisieCompteur FROM FacArticle where CodeArticle='" + StdSQLchaine.gFr_DoublerQuote(txtArticle.Text) + "'"), 0));
                        chkUrgence.CheckState = (CheckState)Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Urgent FROM FacArticle where CodeArticle='" + StdSQLchaine.gFr_DoublerQuote(txtArticle.Text) + "'"), 0));
                    }
            }
        }

        /// <summary>
        /// Mondir le 19.01.2021 pour corriger #2175
        /// </summary>
        /// <returns></returns>
        private bool LoadArticle()
        {
            //===> Mondir 19.01.2021, Ajout du LOG
            Program.SaveException(null, $"LoadArticle() - Enter In The Function");
            //===> Fin Modif Mondir

            string requete = "SELECT CodeArticle, Designation1, SaisieCompteur, Urgent From FacArticle";
            //=== modif du 13 12 2015, ajout du champs Masquer.
            string where_order = " WHERE Masquer IS NULL OR Masquer =  " + General.fc_bln(false);
            if (General.sCaterogieArticleInter == "1")
            {
                //===> Mondir le 19.01.2021, ajouter ( dans la reuqette pour corriger #2175
                where_order = " WHERE (CodeCategorieArticle ='I' OR CodeCategorieArticle ='GP') AND (Masquer IS NULL OR Masquer =   " + General.fc_bln(false) + ") ";
            }

            where_order += $" AND CodeArticle = '{txtArticle.Text.ToUpper()}'";

            using (var tmpModAdo = new ModAdo())
            {
                var dt = tmpModAdo.fc_OpenRecordSet(requete + where_order);
                if (dt.Rows.Count == 0)
                {
                    CustomMessageBox.Show("Le code d'opération n'existe pas", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return false;
                }
                else
                {
                    txtDesignation.Text = dt.Rows[0]["Designation1"]?.ToString();
                    chkCompteurObli.CheckState = (CheckState)Convert.ToInt32(General.nz(dt.Rows[0]["SaisieCompteur"], 0));
                    chkUrgence.CheckState = (CheckState)Convert.ToInt32(General.nz(dt.Rows[0]["Urgent"], 0));
                }
            }

            return true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtArticle_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtArticle_Validating() - Enter In The Function");
            //===> Fin Modif Mondir

            //===> Mondir le 19.01.2021 pour corriger #2175
            if (!LoadArticle())
            {
                eventArgs.Cancel = true;
            }
            //===> Fin Modif Mondir


            //bool Cancel = eventArgs.Cancel;
            //string sCodeCat = null;
            //int i = 0;
            // If blControle = False And txtArticle <> "" Then
            //        sSQl = ""
            //        sSQl = "SELECT FAC_TransKobby, FAC_TransDispapt, FAC_AttentMat," _
            //'            & " FAC_Contratuel, FAC_Facturable, FAC_NonFacturable, Designation1, ACT_Code" _
            //'            & " From IMA_ImmArticle" _
            //'            & " WHERE CodeArticle = '" & txtArticle & "' and codeimmeuble ='" & txtCodeImmeuble & "'"
            //
            //        Set rsTmp = fc_OpenRecordSet(sSQl)
            //
            //        If Not (rsTmp.BOF Or rsTmp.EOF) Then
            //            If rsTmp!FAC_TransKobby = True Then
            //                txtCodeEtat = "TK"
            //            ElseIf rsTmp!FAC_TransDispapt = True Then
            //                txtCodeEtat = "TD"
            //            ElseIf rsTmp!FAC_AttentMat = True Then
            //                txtCodeEtat = "AM"
            //            End If
            //            If UCase(txtArticle) = UCase("TRXDE") Then
            //                    Optdevis.value = True
            //            End If
            //            If UCase(txtArticle) = UCase("TRXRE") Then
            //                    optRegie.value = True
            //                    chkINT_facturable.value = 1
            //            End If
            //            If nz(rsTmp!ACT_Code, "") <> ssComboActivite And ssComboActivite <> "" Then
            //                'If MsgBox("Le code activité du type d'opération que vous avez sélectionné" _
            //'                '    & " est différent de celui present sur l'intervention. Voulez vous le mettre à jour?", vbYesNo, "") = vbYes Then
            //                    'récupére le code activité du type d'opération.
            //                    ssComboActivite = nz(rsTmp!ACT_Code, "")
            //                'End If
            //            Else
            //                ssComboActivite = nz(rsTmp!ACT_Code, "")
            //            End If
            //
            //            ' positionne la nature selon le type analytique du type d'opération.
            //            If rsTmp!ACT_Code = "1" Then
            //                'ne fait rien
            //            ElseIf rsTmp!ACT_Code = "2" Then 'P2
            //                    OptContrat.value = True
            //            ElseIf rsTmp!ACT_Code = "3" Then ' P1 Régie
            //                    optProduit.value = True
            //            ElseIf rsTmp!ACT_Code = "4" Then 'P3
            //                    OptContrat.value = True
            //            ElseIf rsTmp!ACT_Code = "5" Then ' FINANCEMENT
            //                'ne fait rien
            //            ElseIf rsTmp!ACT_Code = "6" Then ' FORFAIT P1
            //                    OptContrat.value = True
            //            ElseIf rsTmp!ACT_Code = "7" Then
            //                    optProduit.value = True
            //            End If
            //
            //            'sCodeCat = fc_ADOlibelle("select cai_noauto from facarticle where codearticle='" & txtArticle & "'")
            //            'If sCodeCat = "26" Then
            //            '        optProduit.value = True
            //            'End If
            //
            //            txtDesignation = nz(rsTmp!Designation1, "")
            //            lbllibcodeetat = fc_ADOlibelle("SELECT  LibelleCodeEtat" _
            //'                        & " From TypeCodeEtat where CodeEtat='" & txtCodeEtat & "'")
            //        Else
            //            Cancel = True
            //            MsgBox "Ce type d'opétation n'existe pas"
            //        End If
            //    Else
            //        txtDesignation = ""
            //   End If
            //Exit Sub
            //eventArgs.Cancel = Cancel;
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeEtat_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            //===> Mondir 18.01.2021, Ajout du LOG txtCodeEtat.Text
            Program.SaveException(null, $"txtCodeEtat_TextChanged() - Enter In The Function - txtCodeEtat.Text = {txtCodeEtat.Text}");
            //===> Fin Modif Mondir
            using (var tmpModAdo = new ModAdo())
                lbllibcodeetat.Text = tmpModAdo.fc_ADOlibelle("SELECT  LibelleCodeEtat" + " FROM TypeCodeEtat" + " where codeetat ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeEtat.Text) + "'");
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void cmdRechercheCodetat_Click()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRechercheCodetat_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            string sCode = "";
            try
            {
                string requete = "SELECT CodeEtat AS \"CodeEtat\", LibelleCodeEtat as \"Libelle\" From TypeCodeEtat";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    if (sCode != "")
                    {
                        txtCodeEtat.Text = sCode;
                        using (var tmpModAdo = new ModAdo())
                            lbllibcodeetat.Text = tmpModAdo.fc_ADOlibelle("SELECT  LibelleCodeEtat FROM TypeCodeEtat where CodeEtat ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeEtat.Text) + "'");
                    }
                    else
                    {
                        txtCodeEtat.Text = "";
                        lbllibcodeetat.Text = "";
                    }
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        if (sCode != "")
                        {
                            txtCodeEtat.Text = sCode;
                            using (var tmpModAdo = new ModAdo())
                                lbllibcodeetat.Text = tmpModAdo.fc_ADOlibelle("SELECT  LibelleCodeEtat FROM TypeCodeEtat where CodeEtat ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeEtat.Text) + "'");
                        }
                        else
                        {
                            txtCodeEtat.Text = "";
                            lbllibcodeetat.Text = "";
                        }
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeEtat_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtCodeEtat_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir
            int KeyAscii = (int)(eventArgs.KeyChar);
            if (KeyAscii == 13)
            {
                cmdRechercheCodetat_Click();
                General.blControle = true;
                KeyAscii = 0;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeEtat_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtCodeEtat_Validating() - Enter In The Function");
            //===> Fin Modif Mondir
            bool Cancel = eventArgs.Cancel;
            if (!string.IsNullOrEmpty(txtCodeEtat.Text))
            {
                if (!string.IsNullOrEmpty(fc_ControleEtat(txtCodeEtat.Text)))
                {
                    using (var tmpModAdo = new ModAdo())
                        lbllibcodeetat.Text = tmpModAdo.fc_ADOlibelle("SELECT  LibelleCodeEtat" + " From TypeCodeEtat where CodeEtat='" + StdSQLchaine.gFr_DoublerQuote(txtCodeEtat.Text) + "'");
                    if (string.IsNullOrEmpty(lbllibcodeetat.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Code état inexistant." + General.cMulti, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Cancel = true;
                    }
                }
                else
                {
                    txtCodeEtat.Text = "";
                    lbllibcodeetat.Text = "";
                }
            }
            else
            {
                lbllibcodeetat.Text = "";
            }
            eventArgs.Cancel = Cancel;
        }





        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtDatePrevue_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtDatePrevue_Validating() - Enter In The Function");
            //===> Fin Modif Mondir
            bool Cancel = eventArgs.Cancel;
            if (!General.IsDate(txtDatePrevue.Text) && !string.IsNullOrEmpty(txtDatePrevue.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(General.cMSGDate, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Cancel = true;
            }
            eventArgs.Cancel = Cancel;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtDateSaisie_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtDateSaisie_Validating() - Enter In The Function");
            //===> Fin Modif Mondir
            bool Cancel = eventArgs.Cancel;
            if (!General.IsDate(txtDateSaisie.Text) && !string.IsNullOrEmpty(txtDateSaisie.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(General.cMSGDate, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Cancel = true;
            }
            eventArgs.Cancel = Cancel;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtDateVisite_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtDateVisite_Validating() - Enter In The Function");
            //===> Fin Modif Mondir
            bool Cancel = eventArgs.Cancel;
            if (!General.IsDate(txtDateVisite.Text) && !string.IsNullOrEmpty(txtDateVisite.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(General.cMSGDate, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Cancel = true;
            }
            eventArgs.Cancel = Cancel;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtDebourse_Leave(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtDebourse_Leave() - Enter In The Function");
            //===> Fin Modif Mondir
            if (!string.IsNullOrEmpty(txtDebourse.Text) && General.IsNumeric(txtDebourse.Text))
            {
                txtDebourse.Text = Convert.ToDouble(txtDebourse.Text).ToString("##0.00");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtDebut_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtDebut_Validating() - Enter In The Function");
            //===> Fin Modif Mondir
            bool Cancel = eventArgs.Cancel;
            if (General.IsDate(txtDebut.Text) && txtDebut.Text.Trim() != "  :  :  ".Trim())
            {
                if (General.IsDate(txtFin.Text) && txtFin.Text.Trim() != "  :  :  ".Trim())
                {
                    //if (!Regex.IsMatch(txtFin.Text, "\\d\\d:\\d\\d:\\d\\d") || !Regex.IsMatch(txtDebut.Text, "\\d\\d:\\d\\d:\\d\\d"))
                    //    return;

                    txtDuree.Text = General.fc_calcDuree(Convert.ToDateTime(txtDebut.Text), Convert.ToDateTime(txtFin.Text), Convert.ToInt32(General.nz(((int)chkSansPauseDejeuner.CheckState), 0)));
                    //txtDebourse.Text = fc_CalcDebourse(txtDuree.Text, , , txtIntervenant)
                    if (General.IsDate(txtDateRealise.Text))
                    {
                        txtDebourse.Text = General.fc_CalcDebourse(Convert.ToDateTime(txtDuree.Text), txtIntervenant.Text, Convert.ToDateTime(txtDateRealise.Text));
                    }
                    else
                    {
                        txtDebourse.Text = General.fc_CalcDebourse(Convert.ToDateTime(txtDuree.Text), txtIntervenant.Text, General.cDefDateRealise);
                    }
                }

            }
            else if (txtDebut.Text.Trim() != "  :  :  ".Trim() && !General.IsDate(txtDebut.Text))
            {
                CustomMessageBox.Show("l'heure visite saisie est invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtDebut.Focus();
                return;

            }
            eventArgs.Cancel = Cancel;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtDispatcheur_ClickEvent(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtDispatcheur_ClickEvent() - Enter In The Function");
            //===> Fin Modif Mondir
            using (var tmpModAdo = new ModAdo())
                txtLibDispatch.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + txtDispatcheur.Value + "'");

        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtFin_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtFin_Validating() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                bool Cancel = eventArgs.Cancel;
                if (General.IsDate(txtFin.Text) && txtFin.Text.Trim() != "  :  :  ".Trim())
                {
                    //if (!Regex.IsMatch(txtFin.Text, "\\d\\d:\\d\\d:\\d\\d") || !Regex.IsMatch(txtDebut.Text, "\\d\\d:\\d\\d:\\d\\d"))
                    //    return;
                    if (General.IsDate(txtDebut.Text) && txtDebut.Text != "  :  :  ".Trim())
                    {
                        txtDuree.Text = General.fc_calcDuree(Convert.ToDateTime(txtDebut.Text), Convert.ToDateTime(txtFin.Text), Convert.ToInt32(General.nz((int)(chkSansPauseDejeuner.CheckState), 0)));

                        //txtDebourse.Text = fc_CalcDebourse(txtDuree.Text, , , txtIntervenant)
                        if (General.IsDate(txtDateRealise.Text))
                        {
                            txtDebourse.Text = General.fc_CalcDebourse(Convert.ToDateTime(txtDuree.Text), txtIntervenant.Text, Convert.ToDateTime(txtDateRealise.Text));
                        }
                        else
                        {
                            txtDebourse.Text = General.fc_CalcDebourse(Convert.ToDateTime(txtDuree.Text), txtIntervenant.Text, General.cDefDateRealise);
                        }
                        // txtDebourse.Text = fc_CalcDebourse(txtDuree.Text, , True, txtIntervenant, txtDateSaisie)
                    }
                }
                else if (txtFin.Text.Trim() != "  :  :  ".Trim() && !General.IsDate(txtFin.Text))
                {
                    CustomMessageBox.Show("l'heure visite saisie est invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtFin.Focus();
                    return;
                }

                eventArgs.Cancel = Cancel;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtHeureDebut_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtHeureDebut_Validating() - Enter In The Function");
            //===> Fin Modif Mondir
            bool Cancel = eventArgs.Cancel;
            if (!General.IsDate(txtHeureDebut.Text) && !string.IsNullOrEmpty(txtHeureDebut.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(General.cMSGTime, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Cancel = true;
            }
            eventArgs.Cancel = Cancel;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtHeureFin_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtHeureFin_Validating() - Enter In The Function");
            //===> Fin Modif Mondir
            bool Cancel = eventArgs.Cancel;
            if (!General.IsDate(txtHeureFin.Text) && !string.IsNullOrEmpty(txtHeureFin.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(General.cMSGTime, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                Cancel = true;
            }
            eventArgs.Cancel = Cancel;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtINT_AnaCode_DropDown(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtINT_AnaCode_DropDown() - Enter In The Function");
            //===> Fin Modif Mondir
            //  sSQl = ""
            // sSQl = "SELECT ANA_Analytique.Code" _
            //& " FROM ANA_Analytique"

            //
            ///
            ///
            ///
            ///    Mondir -   This Controle Is Hidden
            /// 
            ///
            ///

            General.sSQL = "";
            General.sSQL = "Select sec_Code, Sec_Libelle from SEC_Secteur ";

            sheridan.InitialiseCombo(txtINT_AnaCode, General.sSQL, "sec_Code");
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtINT_Rapport1_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtINT_Rapport1_TextChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            fc_ChangeEtatR();

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_ChangeEtatR()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ChangeEtatR() - Enter In The Function");
            //===> Fin Modif Mondir
            if (blnEntree == false)
            {
                if (!string.IsNullOrEmpty(txtINT_Rapport1.Text))
                {
                    if (txtCodeEtat.Text != "F" && txtCodeEtat.Text != "AF" && txtCodeEtat.Text != "AFI" && txtCodeEtat.Text != "AFF" && txtCodeEtat.Text != "AFSC")
                    {
                        txtCodeEtat.Text = "RI";
                    }
                    // If Optdevis.value = True Then
                    //        txtCodeEtat = "AF"
                    // End If
                    // If optRegie.value = True And chkINT_facturable.value Then
                    //        txtCodeEtat = "AFI"
                    // End If
                    using (var tmpModAdo = new ModAdo())
                        lbllibcodeetat.Text = tmpModAdo.fc_ADOlibelle("SELECT  LibelleCodeEtat" + " From TypeCodeEtat where CodeEtat='" + StdSQLchaine.gFr_DoublerQuote(txtCodeEtat.Text) + "'");
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtINT_Rapport2_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtINT_Rapport2_TextChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            fc_ChangeEtatR();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void TXTINT_Rapport3_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"TXTINT_Rapport3_TextChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            fc_ChangeEtatR();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtIntervenant_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            //===> Mondir 18.01.2021, Ajout du LOG txtIntervenant.Text
            Program.SaveException(null, $"txtIntervenant_TextChanged() - Enter In The Function - txtIntervenant.Text = {txtIntervenant.Text}");
            //===> Fin Modif Mondir
            txtMatriculeIntervenant.Text = txtIntervenant.Text;
            using (var tmpModAdo = new ModAdo())
            {
                txtMemoGuard.Text = tmpModAdo.fc_ADOlibelle("SELECT MemoGuard FROM Personnel where matricule='" + StdSQLchaine.gFr_DoublerQuote(txtMatriculeIntervenant.Text) + "'");
                lblNomIntervenant.Text = tmpModAdo.fc_ADOlibelle("SELECT NOM FROM PERSONNEL WHERE MATRICULE ='" + StdSQLchaine.gFr_DoublerQuote(txtIntervenant.Text) + "'");
            }
        }
        //
        //Private Sub txtIntervenant_Click()
        //    txtMatriculeIntervenant.Text = txtIntervenant.value
        //    txtMemoGuard.Text = fc_ADOlibelle("SELECT MemoGuard FROM Personnel where matricule='" & txtMatriculeIntervenant & "'")
        //End Sub

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtIntervenant_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtIntervenant_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir
            short KeyAscii = (short)(eventArgs.KeyChar);
            if (KeyAscii == 13)
            {
                cmdRechercheIntervenant_Click(cmdRechercheIntervenant, new System.EventArgs());
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtLibDispatch_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtLibDispatch_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir
            short KeyAscii = (short)(eventArgs.KeyChar);
            if (KeyAscii == 13)
            {
                CmdRechercheTech_Click(cmdRechercheTech, new System.EventArgs());
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        public void fc_ComboDisp()
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ComboDisp() - Enter In The Function");
            //===> Fin Modif Mondir
            short i = 0;
            DataTable rstmp = default(DataTable);
            ModAdo modAdorstmp = null;
            string req = null;

            modAdorstmp = new ModAdo();

            req = "SELECT Personnel.Matricule,Personnel.Nom,Personnel.Prenom,Personnel.CodeQualif," + " Qualification.CodeQualif,Qualification.Qualification " +
                "FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif ";

            if (General.nbCodeDispatch > 0)
            {
                req = req + " WHERE ";
                for (i = 1; i <= (General.nbCodeDispatch); i++)
                {
                    if (i < (General.nbCodeDispatch))
                    {
                        req = req + "Personnel.CodeQualif='" + General.CodeQualifDispatch[i] + "' OR ";
                    }
                    else
                    {
                        req = req + "Personnel.CodeQualif='" + General.CodeQualifDispatch[i] + "'";
                    }
                }
                req = req + " ORDER BY Personnel.Matricule ";
            }

            rstmp = modAdorstmp.fc_OpenRecordSet(req);

            txtDispatcheur.DataSource = rstmp;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ComboTYMO_ID_DropDown(object sender, CancelEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ComboTYMO_ID_DropDown() - Enter In The Function");
            //===> Fin Modif Mondir
            string sSQL = null;

            sSQL = "SELECT     TYMO_ID as code, TYMO_LIBELLE as libelle , TYMO_Heure as [Heure]" + " From TYMO_TypeDeMoment ORDER BY TYMO_ID";

            sheridan.InitialiseCombo(ComboTYMO_ID, sSQL, "code");
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserIntervention_VisibleChanged(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"UserIntervention_VisibleChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            if (!Visible)
                return;

            //Dtpick0.Value = new DateTime(2020,01,01);

            View.Theme.Theme.MainForm.mainMenu1.ShowHomeMenu("UserDocClient");

            int lnbF = 0;

            //        If Date = #3/3/2010# Then
            //              sTomtom = "1"
            //        End If

            //If sTomtom = "1" Then
            cmdHistoMessage.Visible = true;
            //Else
            //      cmdHistoMessage.Visible = False
            // End If
            //=== version dédié à pz.
            if (General.sCreatePrestV2 == "1")
            {
                cmdDetailPrestation.Visible = true;
            }
            else
            {
                cmdDetailPrestation.Visible = false;
            }

            if (General.sActiveVersionPz == "1")
            {
                CHKP1.Visible = false;
                CHKP2.Visible = false;
                CHKP3.Visible = false;
                CHKP4.Visible = false;
                frame65.Text = "devis";
                Label621.Visible = true;
                txtNoEnregistrement.Visible = true;
                frame65.Enabled = true;

            }
            else
            {
                CHKP1.Visible = true;
                CHKP2.Visible = true;
                CHKP3.Visible = true;
                CHKP4.Visible = true;
                frame65.Text = "Type de contrat";
                Label621.Visible = false;
                txtNoEnregistrement.Visible = false;
                frame65.Enabled = false;
            }

            lblRenovation.Visible = false;
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            //ouvre la connection avec la base
            General.open_conn();
            //récupére le nom de l'utilisateur.
            sUtilisateur = General.fncUserName();

            //TODO : Mondir - Must Set Company Logo
            //if (!string.IsNullOrEmpty(General.LogoSociete) || !Information.IsDBNull(General.LogoSociete))
            //{
            //    imgLogoSociete.Image = System.Drawing.Image.FromFile(General.LogoSociete);
            //    if (Information.IsNumeric(General.imgTop))
            //    {
            //        imgLogoSociete.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(General.imgTop);
            //    }
            //    if (Information.IsNumeric(General.imgLeft))
            //    {
            //        imgLogoSociete.Left = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsX(General.imgLeft);
            //    }
            //}
            //TODO : Mondir - Must Set Company Name
            //if (!string.IsNullOrEmpty(General.NomSousSociete) && !General.IsDBNull(General.NomSousSociete))
            //{
            //    lblNomSociete.Text = General.NomSousSociete;
            //    System.Windows.Forms.Application.DoEvents();
            //}

            blnEntree = true;
            modAdoAdodc6 = new ModAdo();
            Adodc6 = modAdoAdodc6.fc_OpenRecordSet("SELECT Matricule,Nom, NumRadio  FROM Personnel order by Nom");

            fc_ComboDisp();

            var _with24 = this;
            //- positionne sur le dernier enregistrement.
            if (ModParametre.fc_RecupParam(this.Name) == true)
            {
                // charge les enregistremants
                if (ModParametre.sFicheAppelante == Variable.cUserDocStandard)
                {
                    fc_ChargeEnregistrement(ModParametre.sNewVar, true);
                }
                else
                {
                    fc_ChargeEnregistrement(ModParametre.sNewVar);
                }
            }

            //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
            //===> ajout de la condition en bas, cette condition est banale mais je l'ai ajouté comme un point de repair pour les prochaine MAJ
            if (General.fncUserName().ToUpper() != "rachid abbouchi".ToUpper() &&
                DateTime.Now.ToString("dd/MM/yyyy") != "10/16/2020")
            {
                //==== controle si il existe un fichier dans le dossier video
                if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    string tmp = "";
                    lnbF = Video.fc_RecherheVideo(txtCodeImmeuble.Text, ref tmp, true);
                    if (lnbF > 0)
                    {
                        cmdVideo.BackColor = cFindColor;
                    }
                    else
                    {
                        cmdVideo.BackColor = System.Drawing.ColorTranslator.FromOle(0xffc0c0);
                    }
                }
            }

            //TODO : Mondir - This Has Something TODO With Navigation
            //lblNavigation[0].Text = General.sNomLien0;
            //lblNavigation[1].Text = General.sNomLien1;
            //lblNavigation[2].Text = General.sNomLien2;


            if (General.sFicheContratV2 == "1")
            {
                cmdDetailPrestV2.Visible = true;
                chkCompteurObli.Visible = true;
                cmdStatutHisto.Visible = true;
                chkUrgence.Visible = true;
                Label185.Visible = true;
                txtDateVisite.Visible = true;
                Label195.Visible = true;
                txtheureDebutP.Visible = true;
                groupBox9.Text = "";
                //TODO : Mondir - This Line Must Be Hidden
                //Line2[0].Visible = true;
                //Line2[2].Visible = true;
                //Line2[1].Visible = true;
            }
            else
            {
                cmdDetailPrestV2.Visible = false;
                chkCompteurObli.Visible = false;
                cmdStatutHisto.Visible = false;
                chkUrgence.Visible = false;
                Label185.Visible = false;
                txtDateVisite.Visible = false;
                Label195.Visible = false;
                txtheureDebutP.Visible = false;
                groupBox9.Text = "P2";
                //TODO : Mondir - This Line Must Be Shown
                //Line2[0].Visible = false;
                //Line2[2].Visible = false;
                //Line2[1].Visible = false;
            }
            if (!string.IsNullOrEmpty(General.sEtatAvisDePassage))
            {
                cmd0.Visible = true;
            }
            else
            {
                cmd0.Visible = false;
            }
            ModMain.bActivate = false;


        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtLibDispatch_TextChanged(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtLibDispatch_TextChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            if (General.IsNumeric(txtLibDispatch.Text))
                txtDispatcheur.Text = txtLibDispatch.Text;
            else
            {
                using (var tmpModAdo = new ModAdo())
                    if (txtDispatcheur.Text != "")
                        txtDispatcheur.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Matricule FROM Personnel WHERE Personnel.Nom LIKE '" + StdSQLchaine.gFr_DoublerQuote(txtLibDispatch.Text) + "%'");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDispatcheur_AfterCloseUp(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtDispatcheur_AfterCloseUp() - Enter In The Function");
            //===> Fin Modif Mondir
            txtDispatcheur_ClickEvent(sender, e);
        }

        private void cmd0_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmd0_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                var cmd = sender as Button;
                int index = Convert.ToInt32(cmd.Tag);
                string sSQL;
                if (index == 0)
                {
                    frmAVS frm = new frmAVS();
                    frm.txtNoIntervention.Text = txtNoIntervention.Text;
                    frm.txtCode1.Text = txtCode1.Text;
                    frm.txtCodeImmeuble.Text = txtCodeImmeuble.Text;
                    frm.ShowDialog();
                }
                else if (index == 1)
                {
                    frmEncaissementInter frme = new frmEncaissementInter();
                    frme.txtNoIntervention.Text = txtNoIntervention.Text;
                    frme.txtCodeimmeuble.Text = txtCodeImmeuble.Text;
                    frme.ShowDialog();
                }
                else if (index == 2)
                {
                    sSQL = "SELECT CheminOS From GestionStandard WHERE  NumFicheStandard = " + General.nz(txtNumFicheStandard.Text, 0);
                    using (var tmpAdo = new ModAdo())
                        sSQL = tmpAdo.fc_ADOlibelle(sSQL);

                    if (!string.IsNullOrEmpty(sSQL))
                    {
                        ModuleAPI.Ouvrir(sSQL);
                    }
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        private void ComboTYMO_ID_AfterCloseUp(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"ComboTYMO_ID_AfterCloseUp() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                if (ComboTYMO_ID.ActiveRow != null && !string.IsNullOrEmpty(ComboTYMO_ID.ActiveRow.Cells["Heure"].Value.ToString())
                    && General.IsDate(ComboTYMO_ID.ActiveRow.Cells["Heure"].Value.ToString()))
                {
                    //'txtHeurePrevue = TimeValue(.Columns("Heure").value)
                    //'txtHeurePrevue = Hour(.Columns("Heure").value) & ":" & Minute(.Columns("Heure").value)
                    txtHeurePrevue.Text = Convert.ToDateTime(ComboTYMO_ID.ActiveRow.Cells["Heure"].Value).ToString("HH:mm");
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ComboTYMO_ID_CloseUp");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label50_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"Label50_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            bool bcontrole = false;
            if (fc_sauver(ref bcontrole))
            {
                if (txtNoIntervention.Text != "")
                {
                    //stocke la position de la fiche intervention
                    ModParametre.fc_SaveParamPosition(Name, Variable.cUserIntervention, txtNoIntervention.Text);
                    //stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(Name, Variable.cUserDocImmeuble, txtCodeImmeuble.Text);
                }
                View.Theme.Theme.Navigate(typeof(UserDocImmeuble));
            }
        }

        private void lblGoContrat_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"lblGoContrat_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            bool bcontrole = false;

            if (fc_sauver(ref bcontrole) && txtCodeContrat.Text != "")
            {
                if (txtNoIntervention.Text != "")
                {
                    //stocke la position de la fiche d'appel
                    ModParametre.fc_SaveParamPosition(Name, Variable.cUserIntervention, txtNoIntervention.Text);
                    //stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(Name, Variable.cUserDocFicheContrat, txtCodeContrat.Text);
                }
                View.Theme.Theme.Navigate(typeof(UserDocFicheContrat));
            }
        }

        private void Dtpick0_CloseUp(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"Dtpick0_CloseUp() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                txtDateRealise.Text = Dtpick0.Value.ToString(General.FormatDateSansHeureSQL);
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        private void txtDateRealise_Validating(object sender, CancelEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtDateRealise_Validating() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                if (!General.IsDate(txtDateRealise.Text) && txtDateRealise.Text != "")
                {
                    CustomMessageBox.Show(General.cMSGDate, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    return;
                }
                else
                {
                    if (General.IsDate(txtDateRealise.Text))
                        Dtpick0.Value = Convert.ToDateTime(txtDateRealise.Text);
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        private void txtNoIntervention_TextChanged(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtNoIntervention_TextChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            //===> Mondir le 26.03.2021, Line below commented, look at the diff of Navigate, and changed by Navigate(0)
            Navigate(0);
            //txtJobNumber.Text = GetInterventionPosition(Convert.ToInt32(txtNoIntervention.Text)).ToString();
        }

        private void txtDateRealise_TextChanged(object sender, EventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtDateRealise_TextChanged() - Enter In The Function");
            //===> Fin Modif Mondir
            if (string.IsNullOrEmpty(txtDateRealise.Text))
            {
                Dtpick0.Value = DateTime.Now;
            }
        }

        private void txtHeurePrevue_Leave(object sender, EventArgs e)
        {
            //if(txtHeurePrevue.Text.Trim() != "  :  ".Trim())
            //  {
            //      var isHeure = txtHeurePrevue.Text.Substring(0, 2);
            //      var isMin = txtHeurePrevue.Text.Substring(3);
            //      if(Convert.ToInt32(isHeure) >24 || Convert.ToInt32(isMin) > 60)
            //      {
            //         CustomMessageBox.Show("l'heure prévue est invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //          txtHeurePrevue.Focus();
            //      }
            //  }
        }

        private void txtHeurePrevue_Validating(object sender, CancelEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtHeurePrevue_Validating() - Enter In The Function");
            //===> Fin Modif Mondir
            if (txtHeurePrevue.Text.Trim() != "  :  ".Trim() && !General.IsDate(txtHeurePrevue.Text))
            {
                CustomMessageBox.Show("l'heure prévue saisie  est invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtHeurePrevue.Focus();
                return;

            }
        }

        private void txtheureDebutP_Validating(object sender, CancelEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtheureDebutP_Validating() - Enter In The Function");
            //===> Fin Modif Mondir
            if (txtheureDebutP.Text.Trim() != "  :  ".Trim() && !General.IsDate(txtheureDebutP.Text))
            {
                CustomMessageBox.Show("l'heure visite saisie est invalide", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                txtheureDebutP.Focus();
                return;

            }
        }

        private void txtDuree_Validating(object sender, CancelEventArgs e)
        {
            //===> Mondir 04.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtDuree_Validating() - Enter In The Function");
            //===> Fin Modif Mondir
            int nbDemiHeure = 0;

            if (!General.IsDate(txtDuree.Text) && !string.IsNullOrEmpty(txtDuree.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisie de date valide", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                txtDuree.Focus();
            }
            else
            {
                if (!string.IsNullOrEmpty(txtDuree.Text) && txtDuree.Text.Trim() != "  :  :  ".Trim())
                {
                    if (!Regex.IsMatch(txtDuree.Text, "\\d\\d:\\d\\d:\\d\\d"))
                        return;
                    if (General.nz((txtDebourse.Text), "0").ToString() == "0" || txtDebourse.Text == "0.00" && General.IsDate(txtDuree.Text))
                    {
                        //Découpage en demi-heure : 1800 secondes
                        if ((((Convert.ToDateTime(txtDuree.Text).Hour * 3600) + (Convert.ToDateTime(txtDuree.Text).Minute * 60) + Convert.ToDateTime(txtDuree.Text).Second) % 1800) > 0)
                        {
                            nbDemiHeure = Convert.ToInt32(((Convert.ToDateTime(txtDuree.Text).Hour * 3600) + (Convert.ToDateTime(txtDuree.Text).Minute * 60) + Convert.ToDateTime(txtDuree.Text).Second) / 1800) + 1;
                        }
                        else
                        {
                            nbDemiHeure = Convert.ToInt32(((Convert.ToDateTime(txtDuree.Text).Hour * 3600) + (Convert.ToDateTime(txtDuree.Text).Minute * 60) + Convert.ToDateTime(txtDuree.Text).Second) / 1800);
                        }
                        //Taux horaire moyen : 26 €
                        //Déplacement moyen : 26 €
                        txtDebourse.Text = Convert.ToString(nbDemiHeure * (26 / 4) + 26);
                    }
                }
            }
        }

        /// <summary>
        /// Mondir le 15.02.2021 to fix https://groupe-dt.mantishub.io/view.php?id=2256
        /// </summary>
        private void checkContrat()
        {
            //===> Get Active contrats
            var dt = ModContrat.fc_GetActiveContract(txtCodeImmeuble.Text);
            //===> If there is no active contrats
            if (dt.Rows.Count == 0)
            {

                var query = $"UPDATE IMMEUBLE SET P1 = 0, P2 = 0, P3 = 0, P4 = 0 WHERE CodeImmeuble = '{txtCodeImmeuble.Text}'";
                General.Execute(query);

                //===> Mondir le 31.05.2021, ony if there is only at least one contrat but ended ! https://groupe-dt.mantishub.io/view.php?id=2445
                if (ModContrat.AllContractsCount(txtCodeImmeuble.Text) != 0)
                {

                    //===> Mondir le 02.06.2021 https://groupe-dt.mantishub.io/view.php?id=2445#c6088
                    var dateMax = "";
                    using (var tmpModAdo = new ModAdo())
                    {
                        dateMax = tmpModAdo.fc_ADOlibelle($"SELECT MAX(DateFin) FROM Contrat WHERE CodeImmeuble = '{txtCodeImmeuble.Text}'");
                    }
                    //===> Fin Modif Mondir

                    //===> Mondir le 01.06.2021, moved this line down https://groupe-dt.mantishub.io/view.php?id=2445#c6081
                    //lblContratResilie.Visible = true;
                    //===> Mondir le 02.06.2021, added this codition below https://groupe-dt.mantishub.io/view.php?id=2445#c6088
                    if (!dateMax.IsDate())
                    {
                        lblContratResilie.Text = "Attention : Contrat résilié";
                    }
                    else
                    {
                        lblContratResilie.Text = $"Attention : Contrat résilié le {dateMax.ToDate().ToString("dd/MM/yyyy")}";
                    }
                    //===> Fin Modif Mondir
                }
                //===> Mondir le 01.06.2021, added this else if https://groupe-dt.mantishub.io/view.php?id=2445#c6081
                else if (ModContrat.AllContractsCount(txtCodeImmeuble.Text) == 0)
                {
                    lblContratResilie.Text = "Immeuble sans contrat";
                }

                //===> Mondir le 01.06.2021 https://groupe-dt.mantishub.io/view.php?id=2445#c6081
                lblContratResilie.Visible = true;
            }
            else
            {
                lblContratResilie.Visible = false;
            }

            //if (Check41.Checked || Check42.Checked || Check43.Checked || Check44.Checked || Check21.Checked)
            //{


            //===> Mondir le 11.10.2021, commented the line below
            //using (var tmpModAdo = new ModAdo())
            //{
            //    var query = $@"SELECT COUNT(*) AS 'T',SUM(CASE WHEN Resiliee = 1 THEN 1 ELSE 0 END) AS 'R', CodeImmeuble FROM Contrat
            //                WHERE CodeImmeuble = '{txtCodeImmeuble.Text}'
            //                GROUP BY CodeImmeuble";
            //    var dt = tmpModAdo.fc_OpenRecordSet(query);
            //    if (dt != null && dt.Rows.Count > 0)
            //    {
            //        if (dt.Rows[0]["T"].ToInt() == dt.Rows[0]["R"].ToInt())
            //        {
            //            Check41.Checked = false;
            //            Check42.Checked = false;
            //            Check43.Checked = false;
            //            Check44.Checked = false;

            //            //===> Mondir le 29.04.2021, https://groupe-dt.mantishub.io/view.php?id=2420
            //            Check21.Checked = true;
            //            //===> Fin Modif Mondir

            //            query =
            //                $"UPDATE IMMEUBLE SET P1 = 0, P2 = 0, P3 = 0, P4 = 0 WHERE CodeImmeuble = '{txtCodeImmeuble.Text}'";
            //            General.Execute(query);
            //        }
            //    }
            //}
            //}

            //===> Mondir le 02.04.2021, afficher le label contrat reselie
            //var dt2 = ModContrat.fc_GetActiveContract(txtCodeImmeuble.Text);
            //if (dt2 == null || dt2.Rows.Count == 0)
            //{
            //    lblContratResilie.Visible = true;
            //}
            //else
            //{
            //    lblContratResilie.Visible = false;

            //    //===> Mondir le 23.04.2021, https://groupe-dt.mantishub.io/view.php?id=2419
            //    //===> Mondir le 10.05.2021, moved this code here
            //    using (var tmpModAdo = new ModAdo())
            //    {
            //        //===> Check P1
            //        var query = $@"SELECT Contrat.NumContrat, Contrat.Avenant, FacArticle.CodeArticle, FacArticle.COT_Code FROM Contrat
            //                JOIN FacArticle ON FacArticle.CodeArticle = Contrat.CodeArticle
            //                WHERE Contrat.CodeImmeuble = '{txtCodeImmeuble.Text}' AND FacArticle.COT_Code = 'P1'";
            //        var dt = tmpModAdo.fc_OpenRecordSet(query);
            //        Check41.Checked = dt.Rows.Count > 0;

            //        //===> Check P2
            //        query = $@"SELECT Contrat.NumContrat, Contrat.Avenant, FacArticle.CodeArticle, FacArticle.COT_Code FROM Contrat
            //                JOIN FacArticle ON FacArticle.CodeArticle = Contrat.CodeArticle
            //                WHERE Contrat.CodeImmeuble = '{txtCodeImmeuble.Text}' AND FacArticle.COT_Code = 'P2'";
            //        dt = tmpModAdo.fc_OpenRecordSet(query);
            //        Check42.Checked = dt.Rows.Count > 0;

            //        //===> Check P3
            //        query = $@"SELECT Contrat.NumContrat, Contrat.Avenant, FacArticle.CodeArticle, FacArticle.COT_Code FROM Contrat
            //                JOIN FacArticle ON FacArticle.CodeArticle = Contrat.CodeArticle
            //                WHERE Contrat.CodeImmeuble = '{txtCodeImmeuble.Text}' AND FacArticle.COT_Code = 'P3'";
            //        dt = tmpModAdo.fc_OpenRecordSet(query);
            //        Check43.Checked = dt.Rows.Count > 0;

            //        //===> Check P4
            //        query = $@"SELECT Contrat.NumContrat, Contrat.Avenant, FacArticle.CodeArticle, FacArticle.COT_Code FROM Contrat
            //                JOIN FacArticle ON FacArticle.CodeArticle = Contrat.CodeArticle
            //                WHERE Contrat.CodeImmeuble = '{txtCodeImmeuble.Text}' AND FacArticle.COT_Code = 'P4'";
            //        dt = tmpModAdo.fc_OpenRecordSet(query);
            //        Check44.Checked = dt.Rows.Count > 0;

            //        query = $"UPDATE IMMEUBLE SET P1 = {(Check41.Checked ? "1" : "0")}, P2 = {(Check42.Checked ? "1" : "0")}, P3 = {(Check43.Checked ? "1" : "0")}, P4 = {(Check44.Checked ? "1" : "0")} WHERE CodeImmeuble = '{txtCodeImmeuble.Text}'";
            //        General.Execute(query);
            //    }
            //    //===> Fin Modif Mondir
            //}
            //===> Fin Modif Monsdir



        }

    }




}
