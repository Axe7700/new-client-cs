﻿using Axe_interDT.Shared;
using Axe_interDT.Views.Facture_Mannuel;
using Axe_interDT.Views.SharedViews;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Data;
using System.Windows.Forms;

namespace Axe_interDT.Views.Intervention.Forms
{
    public partial class frmFacture : Form
    {
        public frmFacture()
        {
            InitializeComponent();
        }

        private void frmFacture_Load(object sender, EventArgs e)
        {
            string sSQL;

            //===> Ahmed : Maj 02.09.2020 pour integrer les modifcations V02.0.9.2020V2
            //'=== modif du 02/09/2020, controle que ces factures temporaire
            //'=== existe dans la table FactureManuelleEnTete.

            //'=== garde un historique.
            sSQL = "INSERT INTO InterventionMajTemp"
                + " (Nofacture, Statut, Nointervention, Codeimmeuble, MajLe, MajPar) "
                + " SELECT        Intervention.NoFacture, Intervention.CodeEtat, Intervention.NoIntervention "
                + " , Intervention.Codeimmeuble, '" + DateTime.Now + "' as MajLe,'" + General.fncUserName() + "' as MajPar"
                + " FROM            Intervention LEFT OUTER JOIN"
                + " FactureManuelleEnTete ON Intervention.NoFacture = FactureManuelleEnTete.NoFacture "
                + " WHERE        (Intervention.NumFicheStandard = " + General.nz(txtNumFicheStandard.Text, 0) + ") "
                + " AND (Intervention.NoFacture LIKE N'XX%') "
                + " AND (FactureManuelleEnTete.NoFacture IS NULL)";

            General.Execute(sSQL);

            sSQL = "UPDATE       Intervention"
                   + " Set NoFacture = Null "
                   + " FROM            Intervention LEFT OUTER JOIN "
                   + " FactureManuelleEnTete ON Intervention.NoFacture = FactureManuelleEnTete.NoFacture"
                   + " WHERE (Intervention.NumFicheStandard = " + General.nz(txtNumFicheStandard.Text, 0) + ") "
                   + " AND (Intervention.NoFacture LIKE N'XX%') "
                   + " AND (FactureManuelleEnTete.NoFacture IS NULL)";
            General.Execute(sSQL);

            sSQL = "SELECT        NoFacture From Intervention "
                 + " WHERE        NumFicheStandard = " + General.nz(txtNumFicheStandard.Text, 0);

            sSQL = sSQL + "  AND ( NoFacture LIKE N'XX%')";
            using (var tmpAdo = new ModAdo())
                sSQL = tmpAdo.fc_ADOlibelle(sSQL);

            cmdFactTemp.Visible = !string.IsNullOrEmpty(sSQL);

            //===> Mondir le 30.04.2021 https://groupe-dt.mantishub.io/view.php?id=2409
            cmdEditFact.Visible = !string.IsNullOrEmpty(sSQL);
            //===> Fin Modif Mondir

            //'If lblNodevis <> "" Then
            //'    lblNodevis.Visible = True
            //'Else
            //'    lblNodevis.Visible = False
        }

        private void cmdCreerFac_Click(object sender, EventArgs e)
        {
            string sSQL = null;

            if (!Optintervention.Checked && !OptDevis.Checked)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez préciser le type de facture sur devis ou intervention.", "Facture",
                   MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (Optintervention.Checked == true)
            {
                if (optDefinitive.Checked == true)
                {
                    if (General.UCase(txtstatut.Text) == General.UCase("05") || General.UCase(txtstatut.Text) == General.UCase("ZZ") || General.UCase(txtstatut.Text) == General.UCase("P3"))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas facturer une intervention avec les statuts 05,ZZ et P3 (facture existante).", "Facture",
                                          MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (General.UCase(txtstatut.Text) != General.UCase("E"))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour établir une facture définitive vous devez d'abord changer le code état de l'intervention en E" + "\n" + "(édité pour facturation) ou dupliquer l'intervention avec le statut E.", "Facture",
                                          MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else if (OptAcompte.Checked)
                {
                    if (General.UCase(txtstatut.Text) == General.UCase("05") || General.UCase(txtstatut.Text) == General.UCase("ZZ") || General.UCase(txtstatut.Text) == General.UCase("P3"))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas facturer une intervention avec les statuts 05,ZZ et P3 (facture existante).", "Facture",
                                          MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (General.UCase(txtstatut.Text) == General.UCase("T"))
                    {
                        //'=== controle si facture existante.
                        //'sSQL = "SELECT        Intervention.NoFacture " _
                        //        & " FROM            GestionStandard INNER JOIN " _
                        //        & " Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard " _
                        //        & " WHERE        (Intervention.NoFacture IS NOT NULL AND Intervention.NoFacture <> '')" _
                        //        & "  AND GestionStandard.NumFicheStandard = " & txtNumficheStandard
                        //'sSQL = fc_ADOlibelle(sSQL)
                        //'If sSQL <> "" Then
                        //'        MsgBox "Vous ne pouvez pas facturer une intervention avec le statut T (facture existante).", vbInformation, ""
                        //'        Exit Sub
                        //'End If
                    }
                    else if (General.UCase(txtstatut.Text) != General.UCase("TS"))
                    {
                        //'=== autorise.
                    }
                    else if (General.UCase(txtstatut.Text) != General.UCase("E"))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour établir une facture définitive vous devez d'abord changer le code état de l'intervention en E" + "\n" + "(édité pour facturation) ou dupliquer l'intervention avec le statut E.", "Code état",
                                           MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                Variable.lFactInterOuDevis = 1;
            }
            if (OptDevis.Checked)
            {
                if (optDefinitive.Checked == true)
                {
                    if (General.UCase(txtstatut.Text) == General.UCase("05") || General.UCase(txtstatut.Text) == General.UCase("ZZ"))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas facturer une intervention sur devis avec le statut 05 et ZZ.", "Facture",
                                          MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (General.UCase(txtstatut.Text) != General.UCase("D"))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour établir une facture définitive sur devis vous devez d'abord changer le code état de l'intervention en D" + "\n" + "(devis) ou dupliquer l'intervention avec le statut D.", "Code état",
                                          MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }
                else if (OptAcompte.Checked)
                {
                    if (General.UCase(txtstatut.Text) == General.UCase("05") || General.UCase(txtstatut.Text) == General.UCase("ZZ"))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas facturer une intervention sur devis avec le statut 05 et ZZ.", "Facture",
                                          MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    else if (General.UCase(txtstatut.Text) == General.UCase("T"))
                    {
                        //'=== controle si facture existante.
                        //'sSQL = "SELECT        Intervention.NoFacture " _
                        //        & " FROM            GestionStandard INNER JOIN " _
                        //        & " Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard " _
                        //        & " WHERE        (Intervention.NoFacture IS NOT NULL AND Intervention.NoFacture <> '')" _
                        //        & "  AND GestionStandard.NumFicheStandard = " & txtNumficheStandard
                        //'sSQL = fc_ADOlibelle(sSQL)
                        //'If sSQL <> "" Then
                        //'        MsgBox "Vous ne pouvez pas facturer une intervention avec le statut T (facture existante)."
                        //'        Exit Sub
                        //'End If
                    }

                }
                Variable.lFactInterOuDevis = 2;
            }
            if (Optintervention.Checked)
            {
                if (!OptAcompte.Checked && !optDefinitive.Checked)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez préciser s'il s'agit d'une facture d'acompte ou d'une facture définitive.", "Erreur",
                                          MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (OptAcompte.Checked)
                {
                    //'=== Facture interv accompte
                    Variable.lDefOuAcompteOuGarantie = 1;

                }
                else if (optDefinitive.Checked)
                {
                    //'=== Facture interv definitive.
                    Variable.lDefOuAcompteOuGarantie = 2;
                }
            }
            if (OptDevis.Checked)
            {
                if (!OptAcompte.Checked && !optDefinitive.Checked && !optGarantie.Checked)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez préciser s'il s'agit d'une facture d'acompte, d'une facture définitive ou d'une retenue de garantie.", "Erreur",
                                          MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (OptAcompte.Checked)
                {
                    //'=== Facture interv accompte
                    Variable.lDefOuAcompteOuGarantie = 1;

                }
                else if (optDefinitive.Checked)
                {
                    //'=== Facture interv definitive.
                    Variable.lDefOuAcompteOuGarantie = 2;
                }
                else if (optGarantie.Checked)
                {
                    //'=== Facture interv definitive.
                    Variable.lDefOuAcompteOuGarantie = 3;
                }
            }
            Variable.lFactAVoir = 2;
            this.Close();
        }

        private void cmdVisualiser_Click(object sender, EventArgs e)
        {
            fc_Visu(true);
            Variable.lFactAVoir = 1;
        }

        private void cmdFactTemp_Click(object sender, EventArgs e)
        {
            fc_Visu(false);
            Variable.lFactAVoir = 1;
        }

        private void Optintervention_CheckedChanged(object sender, EventArgs e)
        {
            optGarantie.Visible = false;
        }

        private void OptDevis_CheckedChanged(object sender, EventArgs e)
        {
            optGarantie.Visible = true;
        }

        private void fc_Visu(bool breel)
        {
            string sSQL;
            DataTable rs = new DataTable();
            ModAdo rsAdo = new ModAdo();
            DataTable adotemp = new DataTable();
            ModAdo rsAdotemp = new ModAdo();
            string sCrystalOld = "";
            string sCrystal = "";
            ReportDocument Report = new ReportDocument();

            try
            {
                sSQL = "SELECT        NoFacture From Intervention "
                        + " WHERE        NumFicheStandard = " + General.nz(txtNumFicheStandard.Text, 0);

                if (breel == true)
                {
                    sSQL = sSQL + "  AND (NOT (NoFacture LIKE N'XX%'))";
                }
                else
                {
                    sSQL = sSQL + "  AND ( NoFacture LIKE N'XX%')";
                }
                rs = rsAdo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune facture en cours");
                    rs.Dispose();
                    rs = null;
                    //===> Ahmed :Modif 02.09.2020 pour integrer V02.09.2020V2
                    return;
                    //end modification
                }
                else
                {
                    foreach (DataRow r in rs.Rows)
                    {
                        sSQL = "SELECT DateFacture"
                               + " From FactureManuelleEntete"
                               + " WHERE NoFacture ='" + r["NoFacture"] + "'";

                        adotemp = rsAdotemp.fc_OpenRecordSet(sSQL);
                        if (adotemp.Rows.Count > 0)
                        {
                            if (Convert.ToDateTime(adotemp.Rows[0]["DateFacture"]) < Convert.ToDateTime("15/06/2001"))
                            {
                                if (string.IsNullOrEmpty(sCrystalOld))
                                {
                                    sCrystalOld = "{FactureManuelleEntete.NoFacture}='" + r["NoFacture"] + "'";
                                }
                                else
                                {
                                    sCrystalOld = sCrystalOld + " or {FactureManuelleEntete.NoFacture}='" + r["NoFacture"] + "'";
                                }
                            }
                            else
                            {
                                if (string.IsNullOrEmpty(sCrystal))
                                {
                                    sCrystal = "{FactureManuelleEntete.NoFacture}='" + r["NoFacture"] + "'";
                                }
                                else
                                {
                                    sCrystal = sCrystal + " or {FactureManuelleEntete.NoFacture}='" + r["NoFacture"] + "'";
                                }
                            }

                        }
                        //rsAdotemp.Dispose();
                        //rsAdotemp = null;
                    }
                }
                //Mondir 26.06.2020 : rs.Dispose() to rs?.Dispose() ===> Null Exception
                rs?.Dispose();
                rs = null;

                if (!string.IsNullOrEmpty(sCrystalOld))
                {
                    Report.Load(General.ETATOLDFACTUREMANUEL);
                    CrystalReportFormView cr = new CrystalReportFormView(Report, sCrystalOld);
                    cr.Show();

                }
                if (!string.IsNullOrEmpty(sCrystal) && !string.IsNullOrEmpty(sCrystalOld))
                {

                }
                if (!string.IsNullOrEmpty(sCrystal))
                {
                    Report.Load(General.ETATNEWFACTUREMANUEL);
                    CrystalReportFormView cr = new CrystalReportFormView(Report, sCrystal);
                    cr.Show();

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " fc_Visu ");
            }
        }

        /// <summary>
        /// Mondir le 30.04.2021 https://groupe-dt.mantishub.io/view.php?id=2409
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdEditFact_Click(object sender, EventArgs e)
        {
            try
            {
                var req = $@"SELECT TOP 1 Intervention.NoFacture,  GestionStandard.NoDevis FROM Intervention
                            JOIN GestionStandard ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard
                            WHERE GestionStandard.NumFicheStandard = '{General.nz(txtNumFicheStandard.Text, 0)}' AND Intervention.NoFacture LIKE N'XX%'";

                using (var tmpModAdo = new ModAdo())
                {
                    var dt = tmpModAdo.fc_OpenRecordSet(req);
                    if (dt.Rows.Count > 0)
                    {
                        var control = View.Theme.Theme.Navigate(typeof(UserDocFacManuel)) as UserDocFacManuel;

                        var noFacture = dt.Rows[0]["NoFacture"].ToString();

                        if (!dt.Rows[0]["NoDevis"].ToString().IsNullOrEmpty())
                        {
                            control.optDevis.Checked = true;
                        }
                        else
                        {
                            control.optIntervention.Checked = true;
                        }

                        control.cmdValidOpt_Click(null, null);
                        control.fc_load(dt.Rows[0]["NoFacture"].ToString());

                        var sSQL = "SELECT  TOP (1) NumFicheStandard From Intervention "
                                    + " WHERE  NoFacture = '" + General.nz(noFacture, 0) + "'";

                        using (var tmpModAdo2 = new ModAdo())
                            sSQL = General.nz(tmpModAdo.fc_ADOlibelle(sSQL), "0").ToString();

                        control.fc_Synthese(Convert.ToInt32(sSQL));
                    }
                }

                Close();
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }
    }
}
