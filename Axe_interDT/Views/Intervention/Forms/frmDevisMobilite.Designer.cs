﻿namespace Axe_interDT.Views.Intervention.Forms
{
    partial class frmDevisMobilite
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.GridDCM = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.label4 = new System.Windows.Forms.Label();
            this.ssGridDEM = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtNoInter = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.txtIntervenant = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCodeimmeuble = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDCM)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridDEM)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.GridDCM);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.ssGridDEM);
            this.groupBox6.Controls.Add(this.txtNoInter);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.txtIntervenant);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.txtCodeimmeuble);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox6.Location = new System.Drawing.Point(12, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(932, 567);
            this.groupBox6.TabIndex = 411;
            this.groupBox6.TabStop = false;
            // 
            // GridDCM
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridDCM.DisplayLayout.Appearance = appearance1;
            this.GridDCM.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridDCM.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridDCM.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridDCM.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridDCM.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridDCM.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridDCM.DisplayLayout.MaxColScrollRegions = 1;
            this.GridDCM.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridDCM.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridDCM.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridDCM.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridDCM.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridDCM.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridDCM.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridDCM.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridDCM.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridDCM.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridDCM.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridDCM.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridDCM.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridDCM.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridDCM.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridDCM.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridDCM.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridDCM.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridDCM.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridDCM.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridDCM.Location = new System.Drawing.Point(6, 320);
            this.GridDCM.Name = "GridDCM";
            this.GridDCM.Size = new System.Drawing.Size(920, 241);
            this.GridDCM.TabIndex = 510;
            this.GridDCM.Text = "ultraGrid1";
            this.GridDCM.AfterCellActivate += new System.EventHandler(this.GridDCM_AfterCellActivate);
            this.GridDCM.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridDCM_InitializeLayout);
            this.GridDCM.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.GridDCM_InitializeRow);
            this.GridDCM.AfterRowsDeleted += new System.EventHandler(this.GridDCM_AfterRowsDeleted);
            this.GridDCM.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.GridDCM_AfterRowUpdate);
            this.GridDCM.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridDCM_BeforeRowsDeleted);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label4.Location = new System.Drawing.Point(387, 293);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(126, 24);
            this.label4.TabIndex = 509;
            this.label4.Text = "Devis Mobilité";
            // 
            // ssGridDEM
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridDEM.DisplayLayout.Appearance = appearance13;
            this.ssGridDEM.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridDEM.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridDEM.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridDEM.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ssGridDEM.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridDEM.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ssGridDEM.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridDEM.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridDEM.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridDEM.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ssGridDEM.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridDEM.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridDEM.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridDEM.DisplayLayout.Override.CellAppearance = appearance20;
            this.ssGridDEM.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridDEM.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridDEM.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ssGridDEM.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ssGridDEM.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridDEM.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ssGridDEM.DisplayLayout.Override.RowAppearance = appearance23;
            this.ssGridDEM.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridDEM.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridDEM.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ssGridDEM.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridDEM.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridDEM.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssGridDEM.Location = new System.Drawing.Point(9, 142);
            this.ssGridDEM.Name = "ssGridDEM";
            this.ssGridDEM.Size = new System.Drawing.Size(917, 148);
            this.ssGridDEM.TabIndex = 508;
            this.ssGridDEM.Text = "ultraGrid1";
            this.ssGridDEM.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridDEM_InitializeLayout);
            this.ssGridDEM.AfterRowsDeleted += new System.EventHandler(this.ssGridDEM_AfterRowsDeleted);
            this.ssGridDEM.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ssGridDEM_AfterRowUpdate);
            this.ssGridDEM.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssGridDEM_BeforeRowsDeleted);
            // 
            // txtNoInter
            // 
            this.txtNoInter.AccAcceptNumbersOnly = false;
            this.txtNoInter.AccAllowComma = false;
            this.txtNoInter.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoInter.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoInter.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoInter.AccHidenValue = "";
            this.txtNoInter.AccNotAllowedChars = null;
            this.txtNoInter.AccReadOnly = false;
            this.txtNoInter.AccReadOnlyAllowDelete = false;
            this.txtNoInter.AccRequired = false;
            this.txtNoInter.BackColor = System.Drawing.Color.White;
            this.txtNoInter.CustomBackColor = System.Drawing.Color.White;
            this.txtNoInter.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNoInter.ForeColor = System.Drawing.Color.Blue;
            this.txtNoInter.Location = new System.Drawing.Point(153, 106);
            this.txtNoInter.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoInter.MaxLength = 32767;
            this.txtNoInter.Multiline = false;
            this.txtNoInter.Name = "txtNoInter";
            this.txtNoInter.ReadOnly = true;
            this.txtNoInter.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoInter.Size = new System.Drawing.Size(136, 27);
            this.txtNoInter.TabIndex = 507;
            this.txtNoInter.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoInter.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(6, 106);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(117, 19);
            this.label3.TabIndex = 506;
            this.label3.Text = "No Intervention";
            // 
            // txtIntervenant
            // 
            this.txtIntervenant.AccAcceptNumbersOnly = false;
            this.txtIntervenant.AccAllowComma = false;
            this.txtIntervenant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntervenant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntervenant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntervenant.AccHidenValue = "";
            this.txtIntervenant.AccNotAllowedChars = null;
            this.txtIntervenant.AccReadOnly = false;
            this.txtIntervenant.AccReadOnlyAllowDelete = false;
            this.txtIntervenant.AccRequired = false;
            this.txtIntervenant.BackColor = System.Drawing.Color.White;
            this.txtIntervenant.CustomBackColor = System.Drawing.Color.White;
            this.txtIntervenant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIntervenant.ForeColor = System.Drawing.Color.Blue;
            this.txtIntervenant.Location = new System.Drawing.Point(153, 75);
            this.txtIntervenant.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntervenant.MaxLength = 32767;
            this.txtIntervenant.Multiline = false;
            this.txtIntervenant.Name = "txtIntervenant";
            this.txtIntervenant.ReadOnly = true;
            this.txtIntervenant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntervenant.Size = new System.Drawing.Size(260, 27);
            this.txtIntervenant.TabIndex = 505;
            this.txtIntervenant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntervenant.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(6, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(90, 19);
            this.label2.TabIndex = 504;
            this.label2.Text = "Intervenant";
            // 
            // txtCodeimmeuble
            // 
            this.txtCodeimmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeimmeuble.AccAllowComma = false;
            this.txtCodeimmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeimmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeimmeuble.AccHidenValue = "";
            this.txtCodeimmeuble.AccNotAllowedChars = null;
            this.txtCodeimmeuble.AccReadOnly = false;
            this.txtCodeimmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeimmeuble.AccRequired = false;
            this.txtCodeimmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeimmeuble.ForeColor = System.Drawing.Color.Blue;
            this.txtCodeimmeuble.Location = new System.Drawing.Point(153, 44);
            this.txtCodeimmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeimmeuble.MaxLength = 32767;
            this.txtCodeimmeuble.Multiline = false;
            this.txtCodeimmeuble.Name = "txtCodeimmeuble";
            this.txtCodeimmeuble.ReadOnly = true;
            this.txtCodeimmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeimmeuble.Size = new System.Drawing.Size(260, 27);
            this.txtCodeimmeuble.TabIndex = 503;
            this.txtCodeimmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeimmeuble.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(6, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 19);
            this.label1.TabIndex = 502;
            this.label1.Text = "Code immeuble";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label33.Location = new System.Drawing.Point(387, 18);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(126, 24);
            this.label33.TabIndex = 384;
            this.label33.Text = "Devis Mobilité";
            // 
            // frmDevisMobilite
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(956, 591);
            this.Controls.Add(this.groupBox6);
            this.MaximumSize = new System.Drawing.Size(972, 630);
            this.MinimumSize = new System.Drawing.Size(972, 630);
            this.Name = "frmDevisMobilite";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Devis Mobilité";
            this.Load += new System.EventHandler(this.frmDevisMobilite_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDCM)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridDEM)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtNoInter;
        private System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 txtIntervenant;
        private System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtCodeimmeuble;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label4;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssGridDEM;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridDCM;
    }
}