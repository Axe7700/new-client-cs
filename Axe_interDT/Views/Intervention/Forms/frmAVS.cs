﻿using Axe_interDT.Shared;
using Axe_interDT.Views.Devis.Forms;
using System;
using System.Data;
using System.IO;
using System.Windows.Forms;

namespace Axe_interDT.Views.Intervention.Forms
{
    public partial class frmAVS : Form
    {
        public frmAVS()
        {
            InitializeComponent();
        }

        private void cmdImprime_Click(object sender, EventArgs e)
        {
            fc_EtatAvisDePassage(false);
        }

        private void cmdMail_Click(object sender, EventArgs e)
        {
            fc_EtatAvisDePassage(true);
        }

        private void fc_EtatAvisDePassage(bool EnvoieMail)
        {
            string MonDossier;
            string Msg = null;
            string FichierCrystalName;
            string OrigineAppel;
            int i;
            int FIN;
            int Debut;
            string sCodeGestionnaire;
            string sReturn;
            DataTable rsDoc = new DataTable();
            string sSQL;
            DateTime dtNow;

            var tmp = new ModAdo();
            try
            {
                MonDossier = General.DossierIntervention + txtNoIntervention.Text + "\\";
                //===> Mondir le 06.05.2021, https://groupe-dt.mantishub.io/view.php?id=2430, ajout du MonDossier.DirectoryExists()
                if (MonDossier.DirectoryExists())
                {
                    var Contenu = Directory.GetFiles(MonDossier, "0");
                }
                ModCourrier.FichierAttache = "";

                sSQL = "SELECT      NoIntervention From Intervention_2 WHERE NoIntervention =" + txtNoIntervention.Text;

                sSQL = tmp.fc_ADOlibelle(sSQL);

                if (string.IsNullOrEmpty(sSQL))
                {
                    sSQL = " INSERT INTO INtervention_2 (Nointervention, CodeImmeuble, CreeLe, CreePar, AvisImprimeCpt, AvisMailCpt)";

                    sSQL = sSQL + " VALUES (" + txtNoIntervention + ",'" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "',"
                              + "'" + DateTime.Now + "','" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "',0,0 )";
                    General.Execute(sSQL);
                }


                if (EnvoieMail == false)
                {
                    FichierCrystalName = devModeCrystal.fc_ExportCrystalSansFormule("{Intervention.NoIntervention} =" + txtNoIntervention.Text, General.gsRpt + General.sEtatAvisDePassage);
                    ModuleAPI.Ouvrir(FichierCrystalName);

                    sSQL = "UPDATE    Intervention_2 Set AvisImprimeCpt = AvisImprimeCpt + 1, MajLe ='" + DateTime.Now + "', MajPar ='" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'"
                                        + " WHERE NoIntervention = " + txtNoIntervention.Text;
                    General.Execute(sSQL);


                    sSQL = "INSERT INTO AVPH_AvisDePassageHisto"
                                + " (CodeImmeuble, NoIntervention, AVPH_EnvoyeLe, AVPH_EnvoyePar, AVPH_TypeEnvoi)"
                                + " VALUES     ('" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'," + txtNoIntervention.Text + ",'" + DateTime.Now + "', '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'"
                                + " , 'Impression')";
                    General.Execute(sSQL);
                }

                FichierCrystalName = devModeCrystal.fc_ExportCrystalSansFormule("{Intervention.NoIntervention} =" + txtNoIntervention.Text, General.gsRpt + General.sEtatAvisDePassage);

                if (string.IsNullOrEmpty(ModCourrier.FichierAttache))
                {
                    ModCourrier.FichierAttache = FichierCrystalName;
                }
                else
                {
                    ModCourrier.FichierAttache = ModCourrier.FichierAttache + "\t" + FichierCrystalName;
                }
                if (Dossier.fc_ControleDossier(General.DossierIntervention + txtNoIntervention.Text) == false)
                {
                    Dossier.fc_CreateDossier(General.DossierIntervention + txtNoIntervention.Text);
                }

                ModMain.bActivate = true;
                dtNow = DateTime.Now;

                sSQL = "UPDATE    Intervention_2 Set AvisMailCpt = AvisMailCpt + 1, MajLe ='" + dtNow + "', MajPar = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'"
                     + " WHERE NoIntervention = " + txtNoIntervention.Text;
                General.Execute(sSQL);

                sSQL = "INSERT INTO AVPH_AvisDePassageHisto"
                       + " (CodeImmeuble, NoIntervention, AVPH_EnvoyeLe, AVPH_EnvoyePar, AVPH_TypeEnvoi)"
                       + " VALUES     ('" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'," + txtNoIntervention + ",'" + dtNow + "', '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'"
                       + " , 'Mail')";
                General.Execute(sSQL);


                ModCourrier.MessageMail = "";
                frmMail frm = new frmMail();
                frm.txtMessage.Text = "";
                frm.txtMessage.Text = Msg;
                frm.txtDocuments.Text = ModCourrier.FichierAttache;
                frm.txtDossierSauvegarde.Text = "";
                frm.txtDossierSauvegarde.Text = General.DossierIntervention + txtNoIntervention.Text + "\\Mail\\";

                sCodeGestionnaire = tmp.fc_ADOlibelle("SELECT CodeGestionnaire from Immeuble"
                         + " Where CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'");

                frm.SSOleDBcmbA.Text = tmp.fc_ADOlibelle("SELECT Gestionnaire.Email_GES"
                                + " FROM Gestionnaire "
                                + " WHERE  code1 ='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'"
                                + " AND CodeGestinnaire='" + StdSQLchaine.gFr_DoublerQuote(sCodeGestionnaire) + "'");
                frm.ShowDialog();

                ModCourrier.FichierAttache = "";
                frm.Close();

                if (EnvoieMail == true)
                {
                    //   ' If InitialiseEtOuvreOutlook = True Then
                    //'   If CreateMail(AdresseMail, list, ObjetMail, Msg, FichierAttache, , DossierIntervention & txtNoIntervention.Text & "\Mail\") = True Then
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Message Envoyé", "Mail intervention n°" + txtNoIntervention.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //'  End If
                    //'  End If
                    File.Delete(FichierCrystalName);
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdImpression_Click");
            }

        }
    }
}
