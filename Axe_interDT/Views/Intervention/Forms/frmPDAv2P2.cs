﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Intervention.Forms
{
    public partial class frmPDAv2P2 : Form
    {
        DataTable rsOpP2 = null;
        ModAdo modAdorsOpP2 = null;

        DataTable rsOpTaches = null;
        ModAdo modAdorsOpTaches = null;

        DataTable rsDoc = null;
        ModAdo modAdorsDoc = null;

        Color cBlanc = Color.White;
        Color cRose = Color.FromArgb(255, 192, 192);

        public frmPDAv2P2()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadDetInter()
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;

            DataTable rsDoc = default(DataTable);
            ModAdo modAdorsDoc = null;

            try
            {
                sSQL = "SELECT    Intervention.NumFicheStandard, Intervention.Selection,  Intervention.CodeEtatPDA, " +
                    "  Intervention.NoIntervention AS ID, Intervention.CodeImmeuble,  " + "  Intervention.CodeEtat, Intervention.Intervenant, Personnel.Nom, Personnel.Prenom, " +
                    "  Intervention.DatePrevue,  " + "  Intervention.CompteurObli,  " + "  Intervention.DateRealise,  Intervention.Cop_NoAuto," +
                    "  Intervention.CreeLe, Intervention.CreePar, Intervention.ModifierLe, Intervention.ModifierPar," +
                    "  Intervention.EnvoyeLe, Intervention.EnvoyePar, Intervention.NoIntervention, Intervention.HeureDebutPDA, " +
                    "  Intervention.HeureFinPDA, Intervention.DureePDA ," + "  Intervention.HeureDebut, Intervention.HeureFin, Intervention.Duree," +
                    "  Intervention.NomSignataire, HeureSup , DevisEtablir,EnqueteQualite, Intervention.InfoImmeuble, Intervention.RecuSurPdaLe, " +
                    "  Intervention.LuSurPdaLe, Intervention.CodeEtat,  Intervention.PRI_Code, Intervention.HeurePrevue, Intervention.ErrGpsHZDebut " +
                    "  , Intervention.ErrGpsHZFin, DateHeureCopilot, ArrivePrevue , RAS ";

                sSQL = sSQL + " FROM         Intervention INNER JOIN " + " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble LEFT JOIN " + " Personnel ON Intervention.Intervenant = Personnel.Matricule  " + "";
                sSQL = sSQL + " WHERE  Intervention.NoIntervention ='" + txtNoInter.Text + "'";
                //sSQL = sSQL & " WHERE  Intervention.NumFicheStandard ='" & txtNumFicheStandard & "'"

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count == 0)
                {
                    return;
                }

                chkErrGpsHZDebut.CheckState = (CheckState)Convert.ToInt32(General.nz(rs.Rows[0]["ErrGpsHZDebut"], 0));
                chkErrGpsHZFin.CheckState = (CheckState)Convert.ToInt32(General.nz(rs.Rows[0]["ErrGpsHZFin"], 0));

                txtDateHeureCopilot.Text = rs.Rows[0]["DateHeureCopilot"] + "";

                if (General.IsDate(rs.Rows[0]["ArrivePrevue"]))
                {
                    txtArrivePrevue.Text = Convert.ToString(Convert.ToDateTime(rs.Rows[0]["ArrivePrevue"]).TimeOfDay);
                }
                else
                {
                    txtArrivePrevue.Text = rs.Rows[0]["ArrivePrevue"] + "";
                }

                txtNumFicheStandard.Text = rs.Rows[0]["Numfichestandard"] + "";
                txtNointervention.Text = rs.Rows[0]["Id"] + "";
                txtNoInter.Text = rs.Rows[0]["Id"] + "";
                cmbStatutPDA.Text = rs.Rows[0]["CodeEtatPDA"] + "";
                txtCodeimmeuble.Text = rs.Rows[0]["CodeImmeuble"] + "";
                //txtPDAB_Libelle = rs!PDAB_Libelle & ""
                txtIntervenant.Text = rs.Rows[0]["Nom"] + " " + rs.Rows[0]["Prenom"];

                using (var tmpModAdo = new ModAdo())
                    txtPRI_Code.Text = tmpModAdo.fc_ADOlibelle("SELECT PRI_Libelle From PRI_PRIME WHERE PRI_Code = '" + rs.Rows[0]["PRI_Code"] + "" + "'");


                if (General.IsDate(rs.Rows[0]["DatePrevue"]))
                {
                    txtDatePrevue.Text = Convert.ToString(Convert.ToDateTime(rs.Rows[0]["DatePrevue"]).ToShortDateString());
                }
                else
                {
                    txtDatePrevue.Text = rs.Rows[0]["DatePrevue"] + "";
                }

                if (General.IsDate(rs.Rows[0]["DateRealise"]))
                {
                    txtDateRealise.Text = Convert.ToString(Convert.ToDateTime(rs.Rows[0]["DateRealise"]).ToShortDateString());
                }
                else
                {
                    txtDateRealise.Text = rs.Rows[0]["DateRealise"] + "";
                }


                if (General.IsDate(rs.Rows[0]["HeurePrevue"]))
                {
                    txtHeurePrevue.Text = Convert.ToString(Convert.ToDateTime(rs.Rows[0]["HeurePrevue"]).TimeOfDay);
                }
                else
                {
                    txtHeurePrevue.Text = rs.Rows[0]["HeurePrevue"] + "";
                }

                if (General.IsDate(rs.Rows[0]["HeureDebut"]))
                {
                    txtHeureDebut.Text = Convert.ToString(Convert.ToDateTime(rs.Rows[0]["HeureDebut"]).TimeOfDay);
                }
                else
                {
                    txtHeureDebut.Text = rs.Rows[0]["HeureDebut"] + "";
                }


                if (General.IsDate(rs.Rows[0]["HeureFin"]))
                {
                    txtHeureFin.Text = Convert.ToString(Convert.ToDateTime(rs.Rows[0]["HeureFin"]).TimeOfDay);
                }
                else
                {
                    txtHeureFin.Text = rs.Rows[0]["HeureFin"] + "";
                }

                if (General.IsDate(rs.Rows[0]["Duree"]))
                {
                    txtDuree.Text = Convert.ToString(Convert.ToDateTime(rs.Rows[0]["Duree"]).TimeOfDay);
                }
                else
                {
                    txtDuree.Text = rs.Rows[0]["Duree"] + "";
                }

                if (General.IsDate(rs.Rows[0]["HeureDebutPDA"]))
                {
                    txtHeureDebutPDA.Text = Convert.ToDateTime(rs.Rows[0]["HeureDebutPDA"]).TimeOfDay + "";
                }
                else
                {
                    txtHeureDebutPDA.Text = rs.Rows[0]["HeureDebutPDA"] + "";
                }

                if (General.IsDate(rs.Rows[0]["HeureFinPDA"]))
                {
                    txtHeureFinPDA.Text = Convert.ToDateTime(rs.Rows[0]["HeureFinPDA"]).TimeOfDay + "";
                }
                else
                {
                    txtHeureDebutPDA.Text = rs.Rows[0]["HeureFinPDA"] + "";
                }



                txtDureePDA.Text = rs.Rows[0]["DureePDA"] + "";
                txtIntervenant.Text = rs.Rows[0]["Nom"] + " " + rs.Rows[0]["Prenom"] + "";
                txtNomSignataire.Text = rs.Rows[0]["NomSignataire"] + "";
                txtInfoImmeuble.Text = rs.Rows[0]["infoImmeuble"] + "";
                chkHeureSup.CheckState = (CheckState)Convert.ToInt32(General.nz(rs.Rows[0]["HeureSup"], 0));
                chkDevisEtablir.CheckState = (CheckState)Convert.ToInt32(General.nz(rs.Rows[0]["DevisEtablir"], 0));

                chkRAS.CheckState = (CheckState)Convert.ToInt32(General.nz(rs.Rows[0]["RAS"], 0));

                txtCodeEtat.Text = rs.Rows[0]["CodeEtat"] + "";

                if (rs.Rows[0]["EnqueteQualite"] == DBNull.Value)
                {
                    txtEnqueteQualite.Text = "Non renseigné";
                }
                else if (Convert.ToInt32(General.nz(rs.Rows[0]["EnqueteQualite"], 0)) == 0)
                {
                    txtEnqueteQualite.Text = "Non remplie par le client";
                }
                else if (Convert.ToInt32(General.nz(rs.Rows[0]["EnqueteQualite"], 0)) == 1)
                {
                    txtEnqueteQualite.Text = "mécontent";
                }
                else if (Convert.ToInt32(General.nz(rs.Rows[0]["EnqueteQualite"], 0)) == 2)
                {
                    txtEnqueteQualite.Text = "sans avis";
                }
                else if (Convert.ToInt32(General.nz(rs.Rows[0]["EnqueteQualite"], 0)) == 3)
                {
                    txtEnqueteQualite.Text = "satisfait";
                }
                else if (Convert.ToInt32(General.nz(rs.Rows[0]["EnqueteQualite"], 0)) == 4)
                {
                    txtEnqueteQualite.Text = "très satisfait";
                }
                else
                {
                    txtEnqueteQualite.Text = "Non renseigné";
                }

                txtRecuSurPdaLe.Text = rs.Rows[0]["RecuSurPdaLe"] + "";
                txtLuSurPdaLe.Text = rs.Rows[0]["LuSurPdaLe"] + "";

                //txtCommSignataire = rs!CommSignataire & ""
                //chkStockVehicule = nz(rs!StockVehicule, 0)
                //chkTrxUrgent = nz(rs!TrxUrgent, 0)
                //txtCommTxUrgent = rs!CommTxUrgent & ""
                //chkDevisEtablir = nz(rs!DevisEtablir, 0)
                //txtCommDevis = rs!CommDevis & ""
                //txtAnomalie = rs!Anomalie & ""
                //txtCommSurBagdeInvalide = rs!CommMotifScan & ""

                //txtCommentairePDA = rs!CommentairePDA & ""

                //If nz(rs!ID_MotifScan, "") <> "" Then
                //    sSQL = fc_ADOlibelle(" SELECT   MotifSan  FROM MotifScan  where  ID_MotifScan =" & nz(rs!ID_MotifScan, 0))
                //    txtLibID_MotifScan = sSQL
                //End If

                modAdors.Close();

                fc_LoadDoc(Convert.ToInt32(General.nz(txtNoInter.Text, 0)));//tested

                optOperation.Checked = true;
                //fc_ssGRIDTachesAnnuel nz(txtNoIntervention, 0)

                fc_ssGRIDTachesAnnuel(Convert.ToInt32(General.nz(txtNumFicheStandard.Text, 0)));

                var _with1 = ssGridTachesAnnuel;
                _with1.Text = modP2.cLIbNiv2;
                //.Columns("CAI_Libelle").Caption = clibNiv1
                //.Columns("EQM_CODE").Caption = cLibNv2Tier

                var _with2 = ssOpeTaches;
                _with2.Text = modP2.cLibNiv3 + "(simulation)";


                //fc_ssGRIDTachesAnnuel nz(txtNointervention, 0)

                fc_ssGRIDTachesAnnuel(Convert.ToInt32(General.nz(txtNumFicheStandard.Text, 0)));

                txtHeureDebut.BackColor = cBlanc;
                txtHeureDebutPDA.BackColor = cBlanc;
                txtHeureFin.BackColor = cBlanc;
                txtHeureFinPDA.BackColor = cBlanc;

                if (General.IsDate(txtHeureDebut.Text) && General.IsDate(txtHeureDebutPDA.Text))//tesed
                {

                    var dateDiff = (Convert.ToDateTime(txtHeureDebutPDA.Text).TimeOfDay - Convert.ToDateTime(txtHeureDebut.Text).TimeOfDay).TotalMinutes;
                    if (System.Math.Abs(dateDiff) > 2)
                    {

                        txtHeureDebut.BackColor = cRose;
                        //txtHeureDebutPDA.BackColor = cRose

                    }

                }

                if (General.IsDate(txtHeureFin.Text) && General.IsDate(txtHeureFinPDA.Text))//tested
                {
                    var dateDiff = (Convert.ToDateTime(txtHeureFinPDA.Text).TimeOfDay - Convert.ToDateTime(txtHeureFin.Text).TimeOfDay).TotalMinutes;
                    if (System.Math.Abs(dateDiff) > 2)
                    {
                        txtHeureFin.BackColor = cRose;
                        //txtHeuinPDA.BackColor = cRose

                    }

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadDoc");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lNoIntervention"></param>
        private void fc_LoadDoc(int lNoIntervention)
        {
            string sSQL = null;

            try
            {
                sSQL = "SELECT     TypeFichier, Chemin, NomFichier, CreeLe, CreePar" + " From InterventionDoc " + " Where Nointervention = " + lNoIntervention
                   + "  ORDER BY TypeFichier";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                modAdorsDoc = new ModAdo();
                rsDoc = modAdorsDoc.fc_OpenRecordSet(sSQL);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridDoc.DataSource = rsDoc;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadDoc");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lnumfichestandard"></param>
        private void fc_ssGRIDTachesAnnuel(int lnumfichestandard)
        {
            string sSQL = null;

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
                if (optOperation.Checked == true)//Tested
                {

                    ssGridTachesAnnuel.Visible = true;
                    ssOpeTaches.Visible = false;

                    sSQL = "SELECT     CodeImmeuble, PDAB_Libelle AS Localisation, NoInterventionVisite,INT_Realisee, OPR_ID, '' as libOPR, LibStatut,  GAI_LIbelle AS Equipements, " + " CAI_Libelle AS Composant, EQM_Code AS Operation," + " EQM_Libelle, " + " CTR_Libelle, MOY_Libelle, ANO_Libelle, OPE_Libelle," + " TPP_Code AS Periodicite, NoSemaine, Duree " + " From OperationP2 " + " WHERE NumFicheStandard = " + lnumfichestandard + " ORDER BY Localisation, Equipements, Composant, Operation";

                    modAdorsOpP2 = new ModAdo();
                    rsOpP2 = modAdorsOpP2.fc_OpenRecordSet(sSQL);


                    ssGridTachesAnnuel.DataSource = rsOpP2;
                    ssGridTachesAnnuel.UpdateData();
                }
                else if (optTaches.Checked == true)
                {

                    ssGridTachesAnnuel.Visible = false;
                    ssOpeTaches.Visible = true;

                    //sSQL = "SELECT     OperationP2.DatePrevue, OperationP2.EQM_code, " _
                    //& " TIP2_TachesInterP2.TIP2_Designation1, " _
                    //& " TIP2_TachesInterP2.TIP2_NoLIgne, TIP2_TachesInterP2.Realise, " _
                    //& " TIP2_TachesInterP2.DateRealise," _
                    //& " OperationP2.NoIntervention , OperationP2.NoInterventionVisite" _
                    //& " FROM OperationP2 INNER JOIN" _
                    //& " TIP2_TachesInterP2 " _
                    //& " ON OperationP2.NoIntervention = TIP2_TachesInterP2.NoInterventionOp  WHERE NoInterventionVisite =" & LNointervention _
                    //& " ORDER BY OperationP2.DatePrevue, OperationP2.EQM_code, TIP2_TachesInterP2.TIP2_NoLIgne"

                    sSQL = "SELECT     OperationP2.DatePrevue, OperationP2.EQM_code, " + " TIP2_TachesInterP2.TIP2_Designation1, " + " TIP2_TachesInterP2.TIP2_NoLIgne, TIP2_TachesInterP2.Realise, " + " TIP2_TachesInterP2.DateRealise," + " OperationP2.NoIntervention , OperationP2.NoInterventionVisite" + " FROM OperationP2 INNER JOIN" + " TIP2_TachesInterP2 " + " ON OperationP2.NoIntervention = TIP2_TachesInterP2.NoInterventionOp  WHERE NumFicheStandard =" + lnumfichestandard + " ORDER BY OperationP2.DatePrevue, OperationP2.EQM_code, TIP2_TachesInterP2.TIP2_NoLIgne";

                    modAdorsOpTaches = new ModAdo();
                    rsOpTaches = modAdorsOpTaches.fc_OpenRecordSet(sSQL);

                    ssOpeTaches.DataSource = rsOpTaches;

                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_ssGRIDTachesAnnuel;");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCompteur_Click(object sender, EventArgs e)
        {
            frmCompteurInter frmCompteurInter = new frmCompteurInter();
            frmCompteurInter.txtNointervention.Text = txtNointervention.Text;
            frmCompteurInter.ShowDialog();

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDevis_Click(object sender, EventArgs e)
        {
            frmDevisMobilite frmDevisMobilite = new frmDevisMobilite();
            frmDevisMobilite.txtNoInter.Text = txtNoInter.Text;
            frmDevisMobilite.ShowDialog();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            frmGeolocInter frmGeolocInter = new frmGeolocInter();
            frmGeolocInter.txtNoIntervention.Text = txtNoInter.Text;
            frmGeolocInter.ShowDialog();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmPDAv2P2_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);
            this.MinimumSize = new Size(935, 750);
            this.MaximumSize = new Size(935, 750);
            string sSQL = null;

            try
            {
                //cmbStatutPDA.RemoveAll
                //cmbStatutPDA.AddItem cAnnulee
                //cmbStatutPDA.AddItem cDemandeAnnulation
                //cmbStatutPDA.AddItem cAnnulationRefuse
                //cmbStatutPDA.AddItem cEnvoye
                //cmbStatutPDA.AddItem cErreurTransmis
                //cmbStatutPDA.AddItem cNonEnvoye
                //cmbStatutPDA.AddItem cREcu
                //cmbStatutPDA.AddItem cDebut
                //cmbStatutPDA.AddItem cFin
                //cmbStatutPDA.AddItem cPasFait

                sSQL = "SELECT   AUT_Formulaire, AUT_Objet, AUT_Nom " + " From AUT_Autorisation " + " WHERE   AUT_Formulaire = 'geocodage'" +
                    " AND AUT_Objet = 'geocodage'" + " AND AUT_Nom = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'";

                using (var tmpModAdo = new ModAdo())
                    sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                if (string.IsNullOrEmpty(sSQL))
                {

                    Command1.Visible = false;
                }
                else
                {
                    Command1.Visible = true;
                }

                fc_LoadDetInter();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Form_Load");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridDoc_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {

        }

        private void optOperation_CheckedChanged(object sender, EventArgs e)
        {
            if (optOperation.Checked)
            {
                //fc_ssGRIDTachesAnnuel nz(txtNoIntervention, 0)
                fc_ssGRIDTachesAnnuel(Convert.ToInt32(General.nz(txtNumFicheStandard.Text, 0)));

            }
        }

        private void optTaches_CheckedChanged(object sender, EventArgs e)
        {
            if (optTaches.Checked)
            {
                // fc_ssGRIDTachesAnnuel nz(txtNoIntervention, 0)
                fc_ssGRIDTachesAnnuel(Convert.ToInt32(General.nz(txtNumFicheStandard.Text, 0)));

            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridTachesAnnuel_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                string sSQL = null;

                sSQL = "SELECT     OPR_Libelle From OPR_OperationP2Realise WHERE OPR_ID = " + General.nz((e.Row.Cells["OPR_ID"].Text), 0);

                using (var tmpModAdo = new ModAdo())
                    sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                e.Row.Cells["libOPR"].Value = sSQL;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ":");
            }
        }

        private void ssOpeTaches_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ssOpeTaches.DisplayLayout.Bands[0].Columns["EQM_CODE"].Header.Caption = modP2.cLibNv2Tier;
            ssOpeTaches.DisplayLayout.Bands[0].Columns["TIP2_Designation1"].Header.Caption = modP2.cLibNiv3;

        }

        private void GridDoc_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridDoc.DisplayLayout.Bands[0].Columns["Chemin"].Width = 250;
            GridDoc.DisplayLayout.Bands[0].Columns["TypeFichier"].Header.Caption = "Type Fichier";
            GridDoc.DisplayLayout.Bands[0].Columns["Chemin"].Header.Caption = "Chemin";
            GridDoc.DisplayLayout.Bands[0].Columns["NomFichier"].Hidden = true;
            GridDoc.DisplayLayout.Bands[0].Columns["CreeLe"].Hidden = true;
            GridDoc.DisplayLayout.Bands[0].Columns["CreePar"].Hidden = true;
            for (int i = 0; i < GridDoc.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                GridDoc.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }

        }

        private void ssGridTachesAnnuel_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["Operation"].Hidden = true;
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["NoInterventionVisite"].Hidden = true;
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["INT_Realisee"].Hidden = true;
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["OPR_ID"].Hidden = true;
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["LibStatut"].Header.Caption = "Statut";
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["EQM_Libelle"].Hidden = true;
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["libOPR"].Header.Caption = "Niveau de validation";
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["CTR_Libelle"].Header.Caption = "Controle";
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["MOY_Libelle"].Header.Caption = "Moyen";
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["ANO_Libelle"].Header.Caption = "Anomalie";
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["OPE_Libelle"].Header.Caption = "Opération";
            ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns["NoSemaine"].Header.Caption = "N° Semaine";
            for (int i = 0; i < ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                ssGridTachesAnnuel.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }
        }

        private void GridDoc_ClickCell(object sender, Infragistics.Win.UltraWinGrid.ClickCellEventArgs e)
        {
            try
            {
                var _with5 = GridDoc;
                if (GridDoc.ActiveRow == null) return;
                //if (GridDoc.ActiveCell != null && GridDoc.ActiveCell.Column.Key.ToUpper() == "chemin".ToUpper())
                //{
                if (!string.IsNullOrEmpty(_with5.ActiveRow.Cells["Chemin"].Text))
                {
                    //ModuleAPI.Ouvrir(General.DossierIntervention + _with5.ActiveRow.Cells["Chemin"].Text);

                }

                //}


            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";FileIntervention_DblClick");
            }
        }

        private void GridDoc_DoubleClickRow_1(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                if (GridDoc.ActiveRow != null)
                {
                    if (!string.IsNullOrEmpty(GridDoc.ActiveRow.Cells["Chemin"].Text))
                    {
                        //ModuleAPI.Ouvrir(General.DossierIntervention + GridDoc.ActiveRow.Cells["Chemin"].Text);
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";FileIntervention_DblClick");
            }
        }

        private void GridDoc_Click(object sender, EventArgs e)
        {
            try
            {
                if (GridDoc.ActiveRow != null)
                {
                    if (!string.IsNullOrEmpty(GridDoc.ActiveRow.Cells["Chemin"].Text))
                    {
                        ModuleAPI.Ouvrir(General.DossierIntervention + GridDoc.ActiveRow.Cells["Chemin"].Text);
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";FileIntervention_DblClick");
            }
        }
        
        private void GridDoc_AfterRowActivate(object sender, EventArgs e)
        {
           
        }
    }
}
