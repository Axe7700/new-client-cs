﻿namespace Axe_interDT.Views.Intervention.Forms
{
    partial class frmCompteurInter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label33 = new System.Windows.Forms.Label();
            this.GridRel = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtNointervention = new iTalk.iTalk_TextBox_Small2();
            ((System.ComponentModel.ISupportInitialize)(this.GridRel)).BeginInit();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label33.Location = new System.Drawing.Point(401, 9);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(167, 20);
            this.label33.TabIndex = 384;
            this.label33.Text = "Relevés de compteurs";
            // 
            // GridRel
            // 
            this.GridRel.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridRel.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridRel.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridRel.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.GridRel.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridRel.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridRel.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.GridRel.DisplayLayout.UseFixedHeaders = true;
            this.GridRel.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridRel.Location = new System.Drawing.Point(12, 43);
            this.GridRel.Name = "GridRel";
            this.GridRel.Size = new System.Drawing.Size(989, 512);
            this.GridRel.TabIndex = 572;
            this.GridRel.Text = "Résultats ";
            this.GridRel.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridRel_InitializeLayout);
            this.GridRel.AfterExitEditMode += new System.EventHandler(this.GridRel_AfterExitEditMode);
            this.GridRel.AfterRowsDeleted += new System.EventHandler(this.GridRel_AfterRowsDeleted);
            this.GridRel.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.GridRel_AfterRowUpdate);
            this.GridRel.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridRel_BeforeRowsDeleted);
            // 
            // txtNointervention
            // 
            this.txtNointervention.AccAcceptNumbersOnly = false;
            this.txtNointervention.AccAllowComma = false;
            this.txtNointervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNointervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNointervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNointervention.AccHidenValue = "";
            this.txtNointervention.AccNotAllowedChars = null;
            this.txtNointervention.AccReadOnly = false;
            this.txtNointervention.AccReadOnlyAllowDelete = false;
            this.txtNointervention.AccRequired = false;
            this.txtNointervention.BackColor = System.Drawing.Color.White;
            this.txtNointervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNointervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNointervention.ForeColor = System.Drawing.Color.Black;
            this.txtNointervention.Location = new System.Drawing.Point(683, 7);
            this.txtNointervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNointervention.MaxLength = 32767;
            this.txtNointervention.Multiline = false;
            this.txtNointervention.Name = "txtNointervention";
            this.txtNointervention.ReadOnly = false;
            this.txtNointervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNointervention.Size = new System.Drawing.Size(113, 27);
            this.txtNointervention.TabIndex = 573;
            this.txtNointervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNointervention.UseSystemPasswordChar = false;
            this.txtNointervention.Visible = false;
            // 
            // frmCompteurInter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1013, 567);
            this.Controls.Add(this.txtNointervention);
            this.Controls.Add(this.GridRel);
            this.Controls.Add(this.label33);
            this.MaximumSize = new System.Drawing.Size(1029, 606);
            this.MinimumSize = new System.Drawing.Size(1029, 606);
            this.Name = "frmCompteurInter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Relevés de compteurs";
            this.Load += new System.EventHandler(this.frmCompteurInter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridRel)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label33;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridRel;
        public iTalk.iTalk_TextBox_Small2 txtNointervention;
    }
}