﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Intervention.Forms
{
    public partial class frmHistoMessage : Form
    {
        DataTable rsINMT = null;
        ModAdo modAdorsINMT = null;
        public frmHistoMessage()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadINMT()
        {
            string sSQL = null;

            try
            {
                sSQL = "SELECT     INMT_NoAuto, Nointervention, INMT_Ordre, Codeimmeuble," + " INMT_DateEnvoie, INMT_DateMaj, INMT_StatutInter, INMT_StatutTomtom, " + 
                    " INMT_LibelleStatut , INMT_Texte, INMT_Type" + " From INMT_MsgTomtom" + " Where Nointervention =" + General.nz(txtNointervention.Text, 0) +
                    " ORDER BY INMT_DateMaj DESC";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                modAdorsINMT = new ModAdo();
                rsINMT = modAdorsINMT.fc_OpenRecordSet(sSQL);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridINMT.DataSource = rsINMT;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadINMT");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmHistoMessage_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_LoadINMT();
        }

        private void GridINMT_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            modAdorsINMT.Update();
        }

        private void GridINMT_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdorsINMT.Update();
        }

        private void GridINMT_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                e.Cancel = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridINMT_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            for(int i=0;i < GridINMT.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                GridINMT.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }
            GridINMT.DisplayLayout.Bands[0].Columns["INMT_NoAuto"].Hidden = true;
            GridINMT.DisplayLayout.Bands[0].Columns["INMT_Ordre"].Header.Caption = "Ordre";
            GridINMT.DisplayLayout.Bands[0].Columns["INMT_DateEnvoie"].Header.Caption = "DateEnvoie";
            GridINMT.DisplayLayout.Bands[0].Columns["INMT_DateMaj"].Header.Caption = "Date de mise à jour";
            GridINMT.DisplayLayout.Bands[0].Columns["INMT_StatutInter"].Header.Caption = "Statut";
            GridINMT.DisplayLayout.Bands[0].Columns["INMT_StatutTomtom"].Header.Caption = "Statut Tomtom";
            GridINMT.DisplayLayout.Bands[0].Columns["INMT_LibelleStatut"].Hidden = true;
            GridINMT.DisplayLayout.Bands[0].Columns["INMT_Texte"].Header.Caption = "Texte";
            GridINMT.DisplayLayout.Bands[0].Columns["INMT_Type"].Header.Caption = "Type message";

        }

      
    }
}
