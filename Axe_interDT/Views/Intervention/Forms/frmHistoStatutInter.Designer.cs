﻿namespace Axe_interDT.Views.Intervention.Forms
{
    partial class frmHistoStatutInter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.label33 = new System.Windows.Forms.Label();
            this.optStatut = new System.Windows.Forms.RadioButton();
            this.OptLog = new System.Windows.Forms.RadioButton();
            this.GridPdaLog = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ssGridHSI = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtNointervention = new iTalk.iTalk_TextBox_Small2();
            ((System.ComponentModel.ISupportInitialize)(this.GridPdaLog)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridHSI)).BeginInit();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(11, 11);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(117, 19);
            this.label33.TabIndex = 502;
            this.label33.Text = "No intervention";
            // 
            // optStatut
            // 
            this.optStatut.AutoSize = true;
            this.optStatut.Checked = true;
            this.optStatut.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optStatut.Location = new System.Drawing.Point(372, 12);
            this.optStatut.Name = "optStatut";
            this.optStatut.Size = new System.Drawing.Size(109, 23);
            this.optStatut.TabIndex = 538;
            this.optStatut.TabStop = true;
            this.optStatut.Text = "Histo Statut";
            this.optStatut.UseVisualStyleBackColor = true;
            this.optStatut.CheckedChanged += new System.EventHandler(this.optStatut_CheckedChanged);
            // 
            // OptLog
            // 
            this.OptLog.AutoSize = true;
            this.OptLog.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.OptLog.Location = new System.Drawing.Point(514, 12);
            this.OptLog.Name = "OptLog";
            this.OptLog.Size = new System.Drawing.Size(144, 23);
            this.OptLog.TabIndex = 539;
            this.OptLog.Text = "Log smart phone";
            this.OptLog.UseVisualStyleBackColor = true;
            this.OptLog.CheckedChanged += new System.EventHandler(this.OptLog_CheckedChanged);
            // 
            // GridPdaLog
            // 
            this.GridPdaLog.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridPdaLog.DisplayLayout.Appearance = appearance1;
            this.GridPdaLog.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridPdaLog.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridPdaLog.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridPdaLog.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridPdaLog.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridPdaLog.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridPdaLog.DisplayLayout.MaxColScrollRegions = 1;
            this.GridPdaLog.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridPdaLog.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridPdaLog.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridPdaLog.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridPdaLog.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridPdaLog.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridPdaLog.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridPdaLog.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridPdaLog.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridPdaLog.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridPdaLog.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridPdaLog.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridPdaLog.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridPdaLog.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridPdaLog.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridPdaLog.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridPdaLog.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridPdaLog.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridPdaLog.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridPdaLog.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridPdaLog.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridPdaLog.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridPdaLog.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridPdaLog.Location = new System.Drawing.Point(14, 41);
            this.GridPdaLog.Name = "GridPdaLog";
            this.GridPdaLog.Size = new System.Drawing.Size(1292, 595);
            this.GridPdaLog.TabIndex = 540;
            this.GridPdaLog.Text = "ultraGrid1";
            this.GridPdaLog.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridPdaLog_InitializeLayout);
            this.GridPdaLog.AfterRowsDeleted += new System.EventHandler(this.GridPdaLog_AfterRowsDeleted);
            this.GridPdaLog.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.GridPdaLog_AfterRowUpdate);
            this.GridPdaLog.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridPdaLog_BeforeRowsDeleted);
            // 
            // ssGridHSI
            // 
            this.ssGridHSI.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridHSI.DisplayLayout.Appearance = appearance13;
            this.ssGridHSI.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridHSI.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridHSI.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridHSI.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ssGridHSI.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridHSI.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ssGridHSI.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridHSI.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridHSI.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridHSI.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ssGridHSI.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssGridHSI.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridHSI.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridHSI.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridHSI.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridHSI.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridHSI.DisplayLayout.Override.CellAppearance = appearance20;
            this.ssGridHSI.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridHSI.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridHSI.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ssGridHSI.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ssGridHSI.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridHSI.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ssGridHSI.DisplayLayout.Override.RowAppearance = appearance23;
            this.ssGridHSI.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridHSI.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridHSI.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ssGridHSI.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridHSI.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridHSI.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssGridHSI.Location = new System.Drawing.Point(14, 43);
            this.ssGridHSI.Name = "ssGridHSI";
            this.ssGridHSI.Size = new System.Drawing.Size(1292, 593);
            this.ssGridHSI.TabIndex = 541;
            this.ssGridHSI.Text = "ultraGrid1";
            this.ssGridHSI.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridHSI_InitializeLayout);
            this.ssGridHSI.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssGridHSI_InitializeRow);
            this.ssGridHSI.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ssGridHSI_AfterRowUpdate);
            this.ssGridHSI.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.ssGridHSI_Error);
            // 
            // txtNointervention
            // 
            this.txtNointervention.AccAcceptNumbersOnly = false;
            this.txtNointervention.AccAllowComma = false;
            this.txtNointervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNointervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNointervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNointervention.AccHidenValue = "";
            this.txtNointervention.AccNotAllowedChars = null;
            this.txtNointervention.AccReadOnly = false;
            this.txtNointervention.AccReadOnlyAllowDelete = false;
            this.txtNointervention.AccRequired = false;
            this.txtNointervention.BackColor = System.Drawing.Color.White;
            this.txtNointervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNointervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNointervention.ForeColor = System.Drawing.Color.Blue;
            this.txtNointervention.Location = new System.Drawing.Point(133, 11);
            this.txtNointervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNointervention.MaxLength = 32767;
            this.txtNointervention.Multiline = false;
            this.txtNointervention.Name = "txtNointervention";
            this.txtNointervention.ReadOnly = true;
            this.txtNointervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNointervention.Size = new System.Drawing.Size(207, 27);
            this.txtNointervention.TabIndex = 503;
            this.txtNointervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNointervention.UseSystemPasswordChar = false;
            // 
            // frmHistoStatutInter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1313, 641);
            this.Controls.Add(this.ssGridHSI);
            this.Controls.Add(this.GridPdaLog);
            this.Controls.Add(this.OptLog);
            this.Controls.Add(this.optStatut);
            this.Controls.Add(this.txtNointervention);
            this.Controls.Add(this.label33);
            this.MinimumSize = new System.Drawing.Size(1203, 661);
            this.Name = "frmHistoStatutInter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Historique Statut";
            this.Load += new System.EventHandler(this.frmHistoStatutInter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridPdaLog)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridHSI)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public iTalk.iTalk_TextBox_Small2 txtNointervention;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.RadioButton optStatut;
        private System.Windows.Forms.RadioButton OptLog;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridPdaLog;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssGridHSI;
    }
}