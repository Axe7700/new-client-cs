﻿namespace Axe_interDT.Views.Intervention.Forms
{
    partial class frmHistoMessage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.GridINMT = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtNointervention = new iTalk.iTalk_TextBox_Small2();
            ((System.ComponentModel.ISupportInitialize)(this.GridINMT)).BeginInit();
            this.SuspendLayout();
            // 
            // GridINMT
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridINMT.DisplayLayout.Appearance = appearance1;
            this.GridINMT.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridINMT.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridINMT.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridINMT.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridINMT.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridINMT.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridINMT.DisplayLayout.MaxColScrollRegions = 1;
            this.GridINMT.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridINMT.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridINMT.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridINMT.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridINMT.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridINMT.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridINMT.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridINMT.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridINMT.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridINMT.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridINMT.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridINMT.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridINMT.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridINMT.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridINMT.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridINMT.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridINMT.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridINMT.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridINMT.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridINMT.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridINMT.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridINMT.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridINMT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridINMT.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridINMT.Location = new System.Drawing.Point(0, 0);
            this.GridINMT.Name = "GridINMT";
            this.GridINMT.Size = new System.Drawing.Size(748, 503);
            this.GridINMT.TabIndex = 412;
            this.GridINMT.Text = "ultraGrid1";
            this.GridINMT.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridINMT_InitializeLayout);
            this.GridINMT.AfterRowsDeleted += new System.EventHandler(this.GridINMT_AfterRowsDeleted);
            this.GridINMT.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.GridINMT_AfterRowUpdate);
            this.GridINMT.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridINMT_BeforeRowsDeleted);
            // 
            // txtNointervention
            // 
            this.txtNointervention.AccAcceptNumbersOnly = false;
            this.txtNointervention.AccAllowComma = false;
            this.txtNointervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNointervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNointervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNointervention.AccHidenValue = "";
            this.txtNointervention.AccNotAllowedChars = null;
            this.txtNointervention.AccReadOnly = false;
            this.txtNointervention.AccReadOnlyAllowDelete = false;
            this.txtNointervention.AccRequired = false;
            this.txtNointervention.BackColor = System.Drawing.Color.White;
            this.txtNointervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNointervention.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNointervention.ForeColor = System.Drawing.Color.Black;
            this.txtNointervention.Location = new System.Drawing.Point(307, 240);
            this.txtNointervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNointervention.MaxLength = 32767;
            this.txtNointervention.Multiline = false;
            this.txtNointervention.Name = "txtNointervention";
            this.txtNointervention.ReadOnly = false;
            this.txtNointervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNointervention.Size = new System.Drawing.Size(135, 27);
            this.txtNointervention.TabIndex = 502;
            this.txtNointervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNointervention.UseSystemPasswordChar = false;
            this.txtNointervention.Visible = false;
            // 
            // frmHistoMessage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(748, 503);
            this.Controls.Add(this.txtNointervention);
            this.Controls.Add(this.GridINMT);
            this.MaximizeBox = false;
            this.Name = "frmHistoMessage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Historique des messages envoyés";
            this.Load += new System.EventHandler(this.frmHistoMessage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridINMT)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid GridINMT;
        public iTalk.iTalk_TextBox_Small2 txtNointervention;
    }
}