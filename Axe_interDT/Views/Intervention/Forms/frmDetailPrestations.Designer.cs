﻿namespace Axe_interDT.Views.Intervention.Forms
{
    partial class frmDetailPrestations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.txtMois = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.txtNoIntervention = new iTalk.iTalk_TextBox_Small2();
            this.ssGridPrest = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridPrest)).BeginInit();
            this.SuspendLayout();
            // 
            // txtMois
            // 
            this.txtMois.AccAcceptNumbersOnly = false;
            this.txtMois.AccAllowComma = false;
            this.txtMois.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMois.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMois.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMois.AccHidenValue = "";
            this.txtMois.AccNotAllowedChars = null;
            this.txtMois.AccReadOnly = false;
            this.txtMois.AccReadOnlyAllowDelete = false;
            this.txtMois.AccRequired = false;
            this.txtMois.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.txtMois.BackColor = System.Drawing.Color.White;
            this.txtMois.CustomBackColor = System.Drawing.Color.White;
            this.txtMois.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtMois.ForeColor = System.Drawing.Color.Blue;
            this.txtMois.Location = new System.Drawing.Point(302, 4);
            this.txtMois.Margin = new System.Windows.Forms.Padding(2);
            this.txtMois.MaxLength = 32767;
            this.txtMois.Multiline = false;
            this.txtMois.Name = "txtMois";
            this.txtMois.ReadOnly = true;
            this.txtMois.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMois.Size = new System.Drawing.Size(156, 27);
            this.txtMois.TabIndex = 503;
            this.txtMois.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMois.UseSystemPasswordChar = false;
            // 
            // label33
            // 
            this.label33.Anchor = System.Windows.Forms.AnchorStyles.Top;
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(252, 12);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(45, 19);
            this.label33.TabIndex = 502;
            this.label33.Text = "MOIS";
            // 
            // txtNoIntervention
            // 
            this.txtNoIntervention.AccAcceptNumbersOnly = false;
            this.txtNoIntervention.AccAllowComma = false;
            this.txtNoIntervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoIntervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoIntervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoIntervention.AccHidenValue = "";
            this.txtNoIntervention.AccNotAllowedChars = null;
            this.txtNoIntervention.AccReadOnly = false;
            this.txtNoIntervention.AccReadOnlyAllowDelete = false;
            this.txtNoIntervention.AccRequired = false;
            this.txtNoIntervention.BackColor = System.Drawing.Color.White;
            this.txtNoIntervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNoIntervention.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNoIntervention.ForeColor = System.Drawing.Color.Black;
            this.txtNoIntervention.Location = new System.Drawing.Point(538, 4);
            this.txtNoIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoIntervention.MaxLength = 32767;
            this.txtNoIntervention.Multiline = false;
            this.txtNoIntervention.Name = "txtNoIntervention";
            this.txtNoIntervention.ReadOnly = false;
            this.txtNoIntervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoIntervention.Size = new System.Drawing.Size(156, 27);
            this.txtNoIntervention.TabIndex = 504;
            this.txtNoIntervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoIntervention.UseSystemPasswordChar = false;
            this.txtNoIntervention.Visible = false;
            // 
            // ssGridPrest
            // 
            this.ssGridPrest.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridPrest.DisplayLayout.Appearance = appearance1;
            this.ssGridPrest.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridPrest.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridPrest.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridPrest.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ssGridPrest.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridPrest.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ssGridPrest.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridPrest.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridPrest.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridPrest.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ssGridPrest.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssGridPrest.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridPrest.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridPrest.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridPrest.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridPrest.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridPrest.DisplayLayout.Override.CellAppearance = appearance8;
            this.ssGridPrest.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridPrest.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridPrest.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ssGridPrest.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ssGridPrest.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridPrest.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ssGridPrest.DisplayLayout.Override.RowAppearance = appearance11;
            this.ssGridPrest.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridPrest.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridPrest.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ssGridPrest.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridPrest.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridPrest.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssGridPrest.Location = new System.Drawing.Point(12, 36);
            this.ssGridPrest.Name = "ssGridPrest";
            this.ssGridPrest.Size = new System.Drawing.Size(751, 339);
            this.ssGridPrest.TabIndex = 505;
            this.ssGridPrest.Text = "ultraGrid1";
            this.ssGridPrest.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridPrest_InitializeLayout);
            this.ssGridPrest.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssGridPrest_InitializeRow);
            this.ssGridPrest.AfterRowsDeleted += new System.EventHandler(this.ssGridPrest_AfterRowsDeleted);
            this.ssGridPrest.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ssGridPrest_AfterRowUpdate);
            this.ssGridPrest.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssGridPrest_BeforeRowsDeleted);
            // 
            // frmDetailPrestations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(775, 387);
            this.Controls.Add(this.ssGridPrest);
            this.Controls.Add(this.txtNoIntervention);
            this.Controls.Add(this.txtMois);
            this.Controls.Add(this.label33);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(791, 426);
            this.MinimumSize = new System.Drawing.Size(791, 426);
            this.Name = "frmDetailPrestations";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Détail prestation";
            this.Load += new System.EventHandler(this.frmDetailPrestations_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ssGridPrest)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public iTalk.iTalk_TextBox_Small2 txtMois;
        private System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtNoIntervention;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssGridPrest;
    }
}