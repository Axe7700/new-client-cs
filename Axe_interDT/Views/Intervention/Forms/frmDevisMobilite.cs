﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Intervention.Forms
{
    public partial class frmDevisMobilite : Form
    {
        private DataTable rsDME = null;
        ModAdo modAdorsDME = null;
        private DataTable rsDCM = null;
        ModAdo modAdorsDCM = null;
        public frmDevisMobilite()
        {
            InitializeComponent();
        }

        private void fc_DevisMobilite()
        {
            string sSQL = null;
            DataTable rs = default(DataTable);



            try
            {
                sSQL = "";
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_DevisMobilite");
            }
        }
        private void frmDevisMobilite_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_LoadEnTete(Convert.ToInt32(txtNoInter.Text));
        }


        private void fc_LoadEnTete(int lNoIntervention)
        {
            string sSQL = null;


            try
            {
                sSQL = "SELECT     DEM_ID, NoIntervention, DEM_DateCreeLe, DEM_CreePar, CodeImmeuble, CodeParticulier," +
                    " Matricule, REP_CODE, DEM_HT, DEM_TauxTVA, " + " DEM_MtTva , DEM_TTC, DEM_FlagEmail, DEM_Email " + " From DEM_DevisEnTeteMobile " +
                    " WHERE    NoIntervention = " + lNoIntervention;


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                modAdorsDME = new ModAdo();
                rsDME = modAdorsDME.fc_OpenRecordSet(sSQL);

                if (rsDME.Rows.Count > 0)
                {

                    txtNoInter.Text = rsDME.Rows[0]["NoIntervention"] + "";
                    txtCodeimmeuble.Text = rsDME.Rows[0]["CodeImmeuble"] + "";
                    txtIntervenant.Text = rsDME.Rows[0]["Matricule"] + "";
                }
                else
                {
                    txtNoInter.Text = "";
                    txtCodeimmeuble.Text = "";
                    txtIntervenant.Text = "";
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                ssGridDEM.DataSource = rsDME;
                if (rsDME.Rows.Count > 0)
                {

                    fc_LoadCorps(Convert.ToInt32(General.nz((ssGridDEM.Rows[0].Cells["DEM_ID"].Value.ToString()), 0)));
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadEnTete");
            }

        }

        private void fc_LoadCorps(int lDEM_ID)
        {
            string sSQL = null;

            try
            {
                sSQL = "SELECT     DCM_ID, Nointervention, DEM_ID, CodeArticle, '' as designation, DCM_Qte, DCM_PxUnitaire, DCM_HT" + " From DCM_DevisCorpsMobilite " + " WHERE   DEM_ID = " + lDEM_ID;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                modAdorsDCM = new ModAdo();
                rsDCM = modAdorsDCM.fc_OpenRecordSet(sSQL);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridDCM.DataSource = rsDCM;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadCorps");
            }

        }

        private void GridDCM_AfterCellActivate(object sender, EventArgs e)
        {
            fc_LoadCorps(Convert.ToInt32(General.nz((ssGridDEM.ActiveRow.Cells["DEM_ID"].Value.ToString()), 0)));
        }

        private void GridDCM_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            modAdorsDCM.Update();
        }

        private void GridDCM_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdorsDCM.Update();
        }

        private void GridDCM_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                e.Cancel = true;
        }

        private void ssGridDEM_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            modAdorsDME.Update();
        }

        private void ssGridDEM_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdorsDME.Update();
        }

        private void ssGridDEM_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                e.Cancel = true;
        }

        private void ssGridDEM_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

            ssGridDEM.DisplayLayout.Bands[0].Columns["DEM_ID"].Hidden = true;
            ssGridDEM.DisplayLayout.Bands[0].Columns["NoIntervention"].Hidden = true;
            ssGridDEM.DisplayLayout.Bands[0].Columns["DEM_DateCreeLe"].Header.Caption = "CreeLe";
            ssGridDEM.DisplayLayout.Bands[0].Columns["DEM_CreePar"].Hidden = true;
            ssGridDEM.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Hidden = true;
            ssGridDEM.DisplayLayout.Bands[0].Columns["CodeParticulier"].Header.Caption = "Code Particulier";

            ssGridDEM.DisplayLayout.Bands[0].Columns["REP_CODE"].Header.Caption = "Code réglement";
            ssGridDEM.DisplayLayout.Bands[0].Columns["DEM_HT"].Header.Caption = "HT";
            ssGridDEM.DisplayLayout.Bands[0].Columns["DEM_TauxTVA"].Header.Caption = "Taux TVA";
            ssGridDEM.DisplayLayout.Bands[0].Columns["DEM_MtTva"].Header.Caption = "Mt Tva";
            ssGridDEM.DisplayLayout.Bands[0].Columns["DEM_TTC"].Header.Caption = "TTC";
            ssGridDEM.DisplayLayout.Bands[0].Columns["DEM_FlagEmail"].Hidden = true;
            ssGridDEM.DisplayLayout.Bands[0].Columns["DEM_Email"].Header.Caption = "Email";

        }

        private void GridDCM_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ssGridDEM.DisplayLayout.Bands[0].Columns["DEM_ID"].Hidden = true;
            ssGridDEM.DisplayLayout.Bands[0].Columns["NoIntervention"].Hidden = true;
            GridDCM.DisplayLayout.Bands[0].Columns["DCM_Qte"].Header.Caption = "Qte";
            GridDCM.DisplayLayout.Bands[0].Columns["DCM_PxUnitaire"].Header.Caption = "Px Unitaire";
            GridDCM.DisplayLayout.Bands[0].Columns["DCM_HT"].Header.Caption = "HT";
        }

        private void GridDCM_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            string sSQL = "";

            try
            {
                if (e.Row.Cells["CodeArticle"].Text != "")
                {
                    sSQL = "SELECT     Designation1 From FacArticle WHERE     CodeArticle = '" + StdSQLchaine.gFr_DoublerQuote(e.Row.Cells["CodeArticle"].Text) + "'";
                    using (var tmpModAdo = new ModAdo())
                        e.Row.Cells["designation"].Value = tmpModAdo.fc_ADOlibelle(sSQL);
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }
    }
}
