﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Intervention.Forms
{
    public partial class frmEncaissementInter : Form
    {
        public frmEncaissementInter()
        {
            InitializeComponent();
        }
        DataTable rsEnc = new DataTable();
        ModAdo modAdorsEnc = new ModAdo();
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lnoInterevntion"></param>
        private void fc_loadEncaissement(int lnoInterevntion)
        {
            string sSQL;
            try
            {
                sSQL = "SELECT     id, NoIntervention, Codeimmeuble, DateEncaissement, Montant ,"
                      + "  CodeReglement, Reference, Nom, CodeTypeEnc, CompteTiers, ValideREX, NomRex, DateValideRex, ValideCompta, NomCompta, " 
                      + " DateValideCompta "
                      + " From InterEncaissement "
                      + " WHERE     NoIntervention = " + lnoInterevntion;
                rsEnc = modAdorsEnc.fc_OpenRecordSet(sSQL, GridEnc,"id",null);
                GridEnc.DataSource = rsEnc;
                GridEnc.UpdateData();

                sSQL = "SELECT     Code, Libelle, CodeSAGE"
                       + " From ModeReglement ORDER BY Code";
                sheridan.InitialiseCombo(DropCodeReglement,  sSQL, "Code", true);
                GridEnc.DisplayLayout.Bands[0].Columns["CodeReglement"].ValueList = DropCodeReglement;

                sSQL = "SELECT     CodeTypeEnc as Code, LibelleTypeEnc as Libelle "
                      + " From TypeEncaissement ORDER BY CodeTypeEnc";
                sheridan.InitialiseCombo(DropTypeEnc, sSQL, "Code", true);
                GridEnc.DisplayLayout.Bands[0].Columns["CodeTypeEnc"].ValueList = DropTypeEnc;

                sSQL = "SELECT     CT_Num, CT_Intitule" 
                     + " From F_COMPTET ORDER BY CT_Num";
               
                SAGE.fc_OpenConnSage ();               
                sheridan.InitialiseCombo(DropCptTiers, sSQL, "CT_Num", true,SAGE.adoSage);
                GridEnc.DisplayLayout.Bands[0].Columns["CompteTiers"].ValueList = DropCptTiers;               
                SAGE.fc_CloseConnSage();
                
            }
            catch(Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_loadEncaissement");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            GridEnc.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmEncaissementInter_VisibleChanged(object sender, EventArgs e)
        {
            fc_loadEncaissement(Convert.ToInt32(txtNoIntervention.Text));
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnc_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnc_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            modAdorsEnc.Update();
            if (GridEnc.Rows.Count >0)
            {
                General.Execute("UPDATE    Intervention Set Encaissement = 1  WHERE     NoIntervention = " + General.nz(txtNoIntervention.Text, 0));
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnc_AfterRowsDeleted(object sender, EventArgs e)
        {
            
            modAdorsEnc.Update();
            if (GridEnc.Rows.Count == 0)
            {
                General.Execute("UPDATE    Intervention Set Encaissement = 0 WHERE     NoIntervention = " + General.nz(txtNoIntervention.Text, 0));
            }
        }

    
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnc_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridEnc.DisplayLayout.Bands[0].Columns["id"].Hidden = true;
            GridEnc.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Hidden = true;
            GridEnc.DisplayLayout.Bands[0].Columns["NoIntervention"].Hidden = true;
            GridEnc.DisplayLayout.Bands[0].Columns["ValideREX"].Hidden = true; 
            GridEnc.DisplayLayout.Bands[0].Columns["ValideCompta"].Hidden = true;

            GridEnc.DisplayLayout.Bands[0].Columns["NomCompta"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            GridEnc.DisplayLayout.Bands[0].Columns["NomCompta"].CellAppearance.ForeColor = Color.Blue;
            
            GridEnc.DisplayLayout.Bands[0].Columns["DateValideCompta"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            GridEnc.DisplayLayout.Bands[0].Columns["DateValideCompta"].CellAppearance.ForeColor = Color.Blue;

            GridEnc.DisplayLayout.Bands[0].Columns["NomRex"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            GridEnc.DisplayLayout.Bands[0].Columns["NomRex"].CellAppearance.ForeColor = Color.Blue;

            GridEnc.DisplayLayout.Bands[0].Columns["DateValideRex"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            GridEnc.DisplayLayout.Bands[0].Columns["DateValideRex"].CellAppearance.ForeColor = Color.Blue;

            GridEnc.DisplayLayout.Bands[0].Columns["CompteTiers"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.AllowEdit;
            GridEnc.DisplayLayout.Bands[0].Columns["CodeTypeEnc"].Header.Caption = "Code Type Encaissement";
            GridEnc.DisplayLayout.Bands[0].Columns["CompteTiers"].Header.Caption = "Compte Tiers(particulier)";
            GridEnc.DisplayLayout.Bands[0].Columns["Nom"].Header.Caption = "Nom Emetteur";
            GridEnc.DisplayLayout.Bands[0].Columns["NomCompta"].Header.Caption = "Nom Comptable";
            GridEnc.DisplayLayout.Bands[0].Columns["DateValideCompta"].Header.Caption = "Date Validation Comptable";
            GridEnc.DisplayLayout.Bands[0].Columns["NomRex"].Header.Caption = "Nom Resp.Eploit";
            GridEnc.DisplayLayout.Bands[0].Columns["DateValideRex"].Header.Caption = "Date Validation Resp.Eploit";
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridEnc_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {
          
            if (string.IsNullOrEmpty(e.Row.Cells["NoIntervention"].Text))
            {
               e.Row.Cells["NoIntervention"].Value = txtNoIntervention.Text;
            }
            if (string.IsNullOrEmpty(GridEnc.ActiveRow.Cells["Codeimmeuble"].Text))
            {
                e.Row.Cells["Codeimmeuble"].Value = txtCodeimmeuble.Text;
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdValideRex_Click(object sender, EventArgs e)
        {
            string sSQL = null;
            try
            {
                GridEnc.UpdateData();
                sSQL = "Pour valider un encaissement, vous devez vous positionner sur une ligne de la grille.";
                if (GridEnc.ActiveRow == null)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sSQL, "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (GridEnc.ActiveRow != null && string.IsNullOrEmpty(GridEnc.ActiveRow.Cells["id"].Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sSQL, "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                sSQL = "Voulez-vous valider l'encaissement d'un montant de " + GridEnc.ActiveRow.Cells["Montant"].Text + " Euros ?";
                DialogResult dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sSQL, "Validation annulée", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dg == DialogResult.No)
                {
                    return;
                }
                sSQL = "UPDATE    InterEncaissement"
                         + " SET              ValideREX = 1, NomRex ='" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "', "
                         + " DateValideRex = '" + DateTime.Now + "'"
                         + " WHERE     id = " + GridEnc.ActiveRow.Cells["id"].Value;
                int xx = General.Execute(sSQL);
                fc_loadEncaissement(Convert.ToInt32(txtNoIntervention.Text));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmdValideRex_Click");
                
            }
           
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdValideCompta_Click(object sender, EventArgs e)
        {
            string sSQL = null;
            try
            {
                GridEnc.UpdateData();
               
                sSQL = "Pour valider un encaissement, vous devez vous positionner sur une ligne de la grille.";
                if (GridEnc.ActiveRow == null)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sSQL, "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (GridEnc.ActiveRow!= null && string.IsNullOrEmpty(GridEnc.ActiveRow.Cells["id"].Text))
                {                    
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sSQL, "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                sSQL = "Voulez-vous valider l'encaissement d'un montant de " + GridEnc.ActiveRow.Cells["Montant"].Text + " Euros ?";
                DialogResult dg = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sSQL, "Validation annulée", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question);
                if (dg == DialogResult.No)
                {
                    return;
                }
                sSQL = "UPDATE    InterEncaissement" 
                    + " SET  ValideCompta = 1, NomCompta ='" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "', " 
                    + " DateValideCompta = '" + DateTime.Now + "'"
                         + " WHERE     id = " + GridEnc.ActiveRow.Cells["id"].Value;
                int xx = General.Execute(sSQL);
                fc_loadEncaissement(Convert.ToInt32(txtNoIntervention.Text));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmdValideCompta");

            }
        }

        private void GridEnc_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            if (e.ErrorType == Infragistics.Win.UltraWinGrid.ErrorType.Data)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie de données incorrectes dans la colonne " + e.DataErrorInfo.Cell.Column.Header.Caption);
                e.Cancel = true;
            }
        }

        private void frmEncaissementInter_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);
            fc_loadEncaissement(Convert.ToInt32(txtNoIntervention.Text));
        }
    }
}
