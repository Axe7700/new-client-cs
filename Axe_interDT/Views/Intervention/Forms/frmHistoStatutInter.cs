﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Intervention.Forms
{
    public partial class frmHistoStatutInter : Form
    {
        DataTable rsHSI = null;
        ModAdo modAdorsHSI = null;

        DataTable rsPDA = null;
        ModAdo modAdorsPDA = null;
        public frmHistoStatutInter()
        {
            InitializeComponent();
        }


        /// <summary>
        /// tested
        /// </summary>
        private void fc_Log()
        {
            try
            {
                if (optStatut.Checked == true)//tested
                {
                    ssGridHSI.Visible = true;
                    GridPdaLog.Visible = false;
                    fc_LoadHSI();

                }
                else if (OptLog.Checked == true)//tested
                {
                    ssGridHSI.Visible = false;
                    GridPdaLog.Visible = true;
                    fc_LogPDA();

                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                StdSQLchaine.gFr_DoublerQuote(this.Name + ";fc_Log");
            }
        }

        //========================================================'
        //=======PROCEDURE A COPIER POUR UNE GRILLE SHERIDAN======'
        //=======EN MODE UBOUND==================================='
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadHSI()
        {
            string sSQL = null;


            try
            {
                sSQL = "SELECT     HSI_Naouto, HSI_Date, HSI_ModifiePar," + " Nointervention, Codeimmeuble, Fiche, HSI_ancienStatut, HSI_NouveauStatut, HSI_DateRealisee,HSI_HeurePrevue" +
                    " , HSI_CompteurObli, HSI_Urgent, HSI_DatePrevue, HSI_VisiteEntretien " + " From HSI_HistoStatutInter" + " Where Nointervention = " +
                   General.nz(txtNointervention.Text, 0);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                modAdorsHSI = new ModAdo();
                rsHSI = modAdorsHSI.fc_OpenRecordSet(sSQL);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                ssGridHSI.DataSource = rsHSI;
                ssGridHSI.UpdateData();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadHSI");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LogPDA()
        {
            string sSQL = null;

            try
            {
                //sSQL = "SELECT      Noauto, NomFichier, dateHeure, TypeInter, Statut, NoIntervention, Codeimmeuble, DateHeureDebutSaisie, DateHeureDebutReel, " _
                //& " DateHeureFinSaisie, DateHeureFinReel, CodePrime, infoImmeuble, RAS, Drapeau, HeureSup, demandeDevis, EnqueteQualite, DebutLongi, DebutLat, " _
                //& " FinLongi, FinLat, Matricule, Signataire, NoAstreinte, InfoComplementaire, CodeQualif, TypeEnvoie, TypeOperation, DatePrevue, dateCreation, CodeEtat, " _
                //& " CodeArticle, LibArticle, Urgence, TelePhone, InfoStat, InfoPrestation, SaisieCompteur, CommentaireCorrective, CommentaireSurEvent, DateHeureEvent, " _
                //& " DateHeureEventSQL , TypeEvent, LongitudeEvent, LattitudeEvent, EnvoyePar, TempCopilot " _
                //& " From PDA_LOG " _
                //& " WHERE     NoIntervention = '" & txtNointervention & "' " _
                //& " ORDER BY Noauto DESC"

                sSQL = "SELECT      Noauto, NomFichier, dateHeure, TypeInter, Statut, NoIntervention, Codeimmeuble, DateHeureDebutSaisie, DateHeureDebutReel, " +
                    " DateHeureFinSaisie, DateHeureFinReel, CodePrime, infoImmeuble, RAS, Drapeau, HeureSup, demandeDevis, EnqueteQualite, DebutLongi, DebutLat, " +
                    " FinLongi, FinLat, Matricule, Signataire, NoAstreinte, InfoComplementaire, CodeQualif, TypeEnvoie, TypeOperation, DatePrevue, dateCreation, CodeEtat, " +
                    " CodeArticle, LibArticle, Urgence, TelePhone, InfoStat, InfoPrestation, SaisieCompteur, CommentaireCorrective, CommentaireSurEvent, DateHeureEvent, " +
                    "  TypeEvent, LongitudeEvent, LattitudeEvent, EnvoyePar, TempCopilot " + " From PDA_LOG " + " WHERE     NoIntervention = '" + txtNointervention.Text + "' " +
                    " ORDER BY Noauto DESC";

                modAdorsPDA = new ModAdo();
                rsPDA = modAdorsPDA.fc_OpenRecordSet(sSQL);

                GridPdaLog.DataSource = rsPDA;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LogPDA");
            }
        }

        private void ssGridHSI_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {

        }

        private void GridPdaLog_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            modAdorsPDA.Update();
        }

        private void GridPdaLog_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdorsPDA.Update();
        }

        private void GridPdaLog_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                e.Cancel = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void OptLog_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (OptLog.Checked)
            {
                fc_Log();
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optStatut_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optStatut.Checked)
            {
                fc_Log();
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridHSI_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                if (e.Row.Cells["HSI_CompteurObli"].Value == DBNull.Value || e.Row.Cells["HSI_CompteurObli"].Value.ToString() == "0")
                {
                    e.Row.Cells["HSI_CompteurObli"].Value = false;
                }
                if (e.Row.Cells["HSI_Urgent"].Value == DBNull.Value || e.Row.Cells["HSI_CompteurObli"].Value.ToString() == "0")
                {
                    e.Row.Cells["HSI_Urgent"].Value = false;
                }               
                if (General.IsDate(e.Row.Cells["HSI_HeurePrevue"].Value))
                {
                    e.Row.Cells["HSI_HeurePrevue"].Value = Convert.ToDateTime(e.Row.Cells["HSI_HeurePrevue"].Value).ToString("HH:mm");
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssGridHSI_RowLoaded");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmHistoStatutInter_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_Log();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridHSI_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            for (int i = 0; i < ssGridHSI.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                ssGridHSI.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }

            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_Naouto"].Hidden = true;
            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_Date"].Header.Caption = "Date";
            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_Date"].Format = "dd/MM/yyyy HH:mm:ss";

            //====================> Code Commenté par Mondir, Pour enregister le With des colonnes
            //    ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_Date"].Width = 200;

            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_ModifiePar"].Header.Caption = "Modifié Par";
            ssGridHSI.DisplayLayout.Bands[0].Columns["Nointervention"].Hidden = true;
            ssGridHSI.DisplayLayout.Bands[0].Columns["Codeimmeuble"].Hidden = true;
            ssGridHSI.DisplayLayout.Bands[0].Columns["Fiche"].Header.Caption = "Origine";
            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_ancienStatut"].Header.Caption = "Ancien Statut";

            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_NouveauStatut"].Header.Caption = "Nouveau Statut";
            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_DateRealisee"].Header.Caption = "Date Realisee";

            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_HeurePrevue"].Header.Caption = "Heure Prevue";
            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_HeurePrevue"].Format = "HH:mm";

            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_CompteurObli"].Header.Caption = "Compteur Obligatoire";

            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_Urgent"].Header.Caption = "Urgent";
            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_DatePrevue"].Hidden = true;
            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_VisiteEntretien"].Hidden = true;

            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_Urgent"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridHSI.DisplayLayout.Bands[0].Columns["HSI_CompteurObli"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridPdaLog_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridPdaLog.DisplayLayout.Bands[0].Columns["Noauto"].Header.Caption = "ID";

            for (int i = 0; i < GridPdaLog.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                GridPdaLog.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }
        }

        private void ssGridHSI_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            if (ssGridHSI.ActiveCell == null) return;
            if (e.ErrorType == Infragistics.Win.UltraWinGrid.ErrorType.Data)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Données incorrectes dans la colonne " + ssGridHSI.ActiveCell.Column.Header.Caption);
                e.Cancel = true;
            }
        }
    }
}
