﻿namespace Axe_interDT.Views.Intervention.Forms
{
    partial class frmEncaissementInter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            this.GridEnc = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.cmdValideRex = new System.Windows.Forms.Button();
            this.cmdValideCompta = new System.Windows.Forms.Button();
            this.DropCodeReglement = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.DropTypeEnc = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.DropCptTiers = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtNoIntervention = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeimmeuble = new iTalk.iTalk_TextBox_Small2();
            ((System.ComponentModel.ISupportInitialize)(this.GridEnc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DropCodeReglement)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DropTypeEnc)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DropCptTiers)).BeginInit();
            this.SuspendLayout();
            // 
            // GridEnc
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridEnc.DisplayLayout.Appearance = appearance1;
            this.GridEnc.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridEnc.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridEnc.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridEnc.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridEnc.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridEnc.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridEnc.DisplayLayout.MaxColScrollRegions = 1;
            this.GridEnc.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridEnc.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridEnc.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridEnc.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.GridEnc.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.GridEnc.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.GridEnc.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridEnc.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridEnc.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridEnc.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridEnc.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridEnc.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridEnc.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridEnc.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridEnc.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridEnc.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridEnc.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridEnc.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridEnc.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridEnc.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridEnc.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridEnc.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridEnc.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridEnc.Location = new System.Drawing.Point(12, 104);
            this.GridEnc.Name = "GridEnc";
            this.GridEnc.Size = new System.Drawing.Size(893, 456);
            this.GridEnc.TabIndex = 513;
            this.GridEnc.Text = "ultraGrid1";
            this.GridEnc.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridEnc_InitializeLayout);
            this.GridEnc.AfterRowsDeleted += new System.EventHandler(this.GridEnc_AfterRowsDeleted);
            this.GridEnc.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.GridEnc_AfterRowUpdate);
            this.GridEnc.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.GridEnc_BeforeRowUpdate);
            this.GridEnc.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridEnc_BeforeRowsDeleted);
            this.GridEnc.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.GridEnc_Error);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(8, 72);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(113, 19);
            this.label2.TabIndex = 511;
            this.label2.Text = "N° intervention";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(8, 44);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 19);
            this.label1.TabIndex = 509;
            this.label1.Text = "Code immeuble";
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(836, 44);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 514;
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // cmdValideRex
            // 
            this.cmdValideRex.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdValideRex.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdValideRex.FlatAppearance.BorderSize = 0;
            this.cmdValideRex.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdValideRex.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdValideRex.ForeColor = System.Drawing.Color.White;
            this.cmdValideRex.Location = new System.Drawing.Point(446, 44);
            this.cmdValideRex.Margin = new System.Windows.Forms.Padding(2);
            this.cmdValideRex.Name = "cmdValideRex";
            this.cmdValideRex.Size = new System.Drawing.Size(191, 35);
            this.cmdValideRex.TabIndex = 515;
            this.cmdValideRex.Text = "Validation Service Comptabilité";
            this.cmdValideRex.UseVisualStyleBackColor = false;
            this.cmdValideRex.Click += new System.EventHandler(this.cmdValideCompta_Click);
            // 
            // cmdValideCompta
            // 
            this.cmdValideCompta.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdValideCompta.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdValideCompta.FlatAppearance.BorderSize = 0;
            this.cmdValideCompta.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdValideCompta.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdValideCompta.ForeColor = System.Drawing.Color.White;
            this.cmdValideCompta.Location = new System.Drawing.Point(641, 44);
            this.cmdValideCompta.Margin = new System.Windows.Forms.Padding(2);
            this.cmdValideCompta.Name = "cmdValideCompta";
            this.cmdValideCompta.Size = new System.Drawing.Size(191, 35);
            this.cmdValideCompta.TabIndex = 516;
            this.cmdValideCompta.Text = "Validation Responsable Exploit";
            this.cmdValideCompta.UseVisualStyleBackColor = false;
            this.cmdValideCompta.Click += new System.EventHandler(this.cmdValideRex_Click);
            // 
            // DropCodeReglement
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.DropCodeReglement.DisplayLayout.Appearance = appearance13;
            this.DropCodeReglement.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.DropCodeReglement.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.DropCodeReglement.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.DropCodeReglement.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.DropCodeReglement.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.DropCodeReglement.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.DropCodeReglement.DisplayLayout.MaxColScrollRegions = 1;
            this.DropCodeReglement.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.DropCodeReglement.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.DropCodeReglement.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.DropCodeReglement.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.DropCodeReglement.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.DropCodeReglement.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.DropCodeReglement.DisplayLayout.Override.CellAppearance = appearance20;
            this.DropCodeReglement.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.DropCodeReglement.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.DropCodeReglement.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.DropCodeReglement.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.DropCodeReglement.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.DropCodeReglement.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.DropCodeReglement.DisplayLayout.Override.RowAppearance = appearance23;
            this.DropCodeReglement.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.DropCodeReglement.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.DropCodeReglement.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.DropCodeReglement.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.DropCodeReglement.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.DropCodeReglement.Location = new System.Drawing.Point(215, 195);
            this.DropCodeReglement.Name = "DropCodeReglement";
            this.DropCodeReglement.Size = new System.Drawing.Size(186, 22);
            this.DropCodeReglement.TabIndex = 517;
            this.DropCodeReglement.Visible = false;
            // 
            // DropTypeEnc
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.DropTypeEnc.DisplayLayout.Appearance = appearance25;
            this.DropTypeEnc.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.DropTypeEnc.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.DropTypeEnc.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.DropTypeEnc.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.DropTypeEnc.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.DropTypeEnc.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.DropTypeEnc.DisplayLayout.MaxColScrollRegions = 1;
            this.DropTypeEnc.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.DropTypeEnc.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.DropTypeEnc.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.DropTypeEnc.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.DropTypeEnc.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.DropTypeEnc.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.DropTypeEnc.DisplayLayout.Override.CellAppearance = appearance32;
            this.DropTypeEnc.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.DropTypeEnc.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.DropTypeEnc.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.DropTypeEnc.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.DropTypeEnc.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.DropTypeEnc.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.DropTypeEnc.DisplayLayout.Override.RowAppearance = appearance35;
            this.DropTypeEnc.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.DropTypeEnc.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.DropTypeEnc.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.DropTypeEnc.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.DropTypeEnc.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.DropTypeEnc.Location = new System.Drawing.Point(215, 236);
            this.DropTypeEnc.Name = "DropTypeEnc";
            this.DropTypeEnc.Size = new System.Drawing.Size(186, 22);
            this.DropTypeEnc.TabIndex = 518;
            this.DropTypeEnc.Visible = false;
            // 
            // DropCptTiers
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.DropCptTiers.DisplayLayout.Appearance = appearance37;
            this.DropCptTiers.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.DropCptTiers.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.DropCptTiers.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.DropCptTiers.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.DropCptTiers.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.DropCptTiers.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.DropCptTiers.DisplayLayout.MaxColScrollRegions = 1;
            this.DropCptTiers.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.DropCptTiers.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.DropCptTiers.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.DropCptTiers.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.DropCptTiers.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.DropCptTiers.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.DropCptTiers.DisplayLayout.Override.CellAppearance = appearance44;
            this.DropCptTiers.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.DropCptTiers.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.DropCptTiers.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.DropCptTiers.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.DropCptTiers.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.DropCptTiers.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.DropCptTiers.DisplayLayout.Override.RowAppearance = appearance47;
            this.DropCptTiers.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.DropCptTiers.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.DropCptTiers.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.DropCptTiers.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.DropCptTiers.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.DropCptTiers.Location = new System.Drawing.Point(215, 274);
            this.DropCptTiers.Name = "DropCptTiers";
            this.DropCptTiers.Size = new System.Drawing.Size(186, 22);
            this.DropCptTiers.TabIndex = 519;
            this.DropCptTiers.Visible = false;
            // 
            // txtNoIntervention
            // 
            this.txtNoIntervention.AccAcceptNumbersOnly = false;
            this.txtNoIntervention.AccAllowComma = false;
            this.txtNoIntervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoIntervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoIntervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoIntervention.AccHidenValue = "";
            this.txtNoIntervention.AccNotAllowedChars = null;
            this.txtNoIntervention.AccReadOnly = false;
            this.txtNoIntervention.AccReadOnlyAllowDelete = false;
            this.txtNoIntervention.AccRequired = false;
            this.txtNoIntervention.BackColor = System.Drawing.Color.White;
            this.txtNoIntervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNoIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNoIntervention.ForeColor = System.Drawing.Color.Blue;
            this.txtNoIntervention.Location = new System.Drawing.Point(135, 72);
            this.txtNoIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoIntervention.MaxLength = 32767;
            this.txtNoIntervention.Multiline = false;
            this.txtNoIntervention.Name = "txtNoIntervention";
            this.txtNoIntervention.ReadOnly = true;
            this.txtNoIntervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoIntervention.Size = new System.Drawing.Size(249, 27);
            this.txtNoIntervention.TabIndex = 512;
            this.txtNoIntervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoIntervention.UseSystemPasswordChar = false;
            // 
            // txtCodeimmeuble
            // 
            this.txtCodeimmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeimmeuble.AccAllowComma = false;
            this.txtCodeimmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeimmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeimmeuble.AccHidenValue = "";
            this.txtCodeimmeuble.AccNotAllowedChars = null;
            this.txtCodeimmeuble.AccReadOnly = false;
            this.txtCodeimmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeimmeuble.AccRequired = false;
            this.txtCodeimmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeimmeuble.ForeColor = System.Drawing.Color.Blue;
            this.txtCodeimmeuble.Location = new System.Drawing.Point(135, 40);
            this.txtCodeimmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeimmeuble.MaxLength = 32767;
            this.txtCodeimmeuble.Multiline = false;
            this.txtCodeimmeuble.Name = "txtCodeimmeuble";
            this.txtCodeimmeuble.ReadOnly = true;
            this.txtCodeimmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeimmeuble.Size = new System.Drawing.Size(249, 27);
            this.txtCodeimmeuble.TabIndex = 510;
            this.txtCodeimmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeimmeuble.UseSystemPasswordChar = false;
            // 
            // frmEncaissementInter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(917, 572);
            this.Controls.Add(this.DropCptTiers);
            this.Controls.Add(this.DropTypeEnc);
            this.Controls.Add(this.DropCodeReglement);
            this.Controls.Add(this.cmdValideCompta);
            this.Controls.Add(this.cmdValideRex);
            this.Controls.Add(this.CmdSauver);
            this.Controls.Add(this.GridEnc);
            this.Controls.Add(this.txtNoIntervention);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtCodeimmeuble);
            this.Controls.Add(this.label1);
            this.MaximumSize = new System.Drawing.Size(933, 611);
            this.MinimumSize = new System.Drawing.Size(933, 611);
            this.Name = "frmEncaissementInter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmEncaissementInter";
            this.Load += new System.EventHandler(this.frmEncaissementInter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridEnc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DropCodeReglement)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DropTypeEnc)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DropCptTiers)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private Infragistics.Win.UltraWinGrid.UltraGrid GridEnc;
        public iTalk.iTalk_TextBox_Small2 txtNoIntervention;
        private System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtCodeimmeuble;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button CmdSauver;
        public System.Windows.Forms.Button cmdValideRex;
        public System.Windows.Forms.Button cmdValideCompta;
        public Infragistics.Win.UltraWinGrid.UltraCombo DropCodeReglement;
        public Infragistics.Win.UltraWinGrid.UltraCombo DropTypeEnc;
        public Infragistics.Win.UltraWinGrid.UltraCombo DropCptTiers;
    }
}