﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Intervention.Forms
{
    public partial class frmDetInterPDA : Form
    {
        public frmDetInterPDA()
        {
            InitializeComponent();
        }

       
        /// <summary>
        /// tested
        /// </summary>
        private void fc_Load()
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;


            try
            {
                sSQL = "SELECT     NumFicheStandard, NoIntervention, CodeImmeuble, DateSaisie, DatePrevue, DateRealise, HeureDebut, HeureFin, Duree, HeureDebutPDA, " +
                    " HeureFinPDA , DureePDA, HeurePrevue " + " From Intervention " + " WHERE NoIntervention = " + General.nz(txtNointervention.Text, 0);

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                txtDateSaisie.Text = rs.Rows[0]["DateSaisie"] + "";
                txtDatePrevue.Text = rs.Rows[0]["DatePrevue"] + "";
                txtDateRealise.Text = rs.Rows[0]["DateRealise"] + "";


                if (rs.Rows[0]["HeureDebut"] == DBNull.Value)
                {
                    txtHeureDebut.Text = "";
                }
                else
                {
                    txtHeureDebut.Text = Convert.ToString(Convert.ToDateTime(rs.Rows[0]["HeureDebut"]).TimeOfDay);
                }

                if (rs.Rows[0]["HeureFin"] == DBNull.Value)
                {
                    txtHeureFin.Text = "";
                }
                else
                {
                    txtHeureFin.Text = Convert.ToString(Convert.ToDateTime(rs.Rows[0]["HeureFin"]).TimeOfDay);
                }

                txtDuree.Text = rs.Rows[0]["Duree"] + "";

                if (rs.Rows[0]["HeureDebutPDA"] == DBNull.Value)
                {
                    txtHeureDebutPDA.Text = "";
                }
                else
                {
                    txtHeureDebutPDA.Text = Convert.ToString(Convert.ToDateTime(rs.Rows[0]["HeureDebutPDA"]).TimeOfDay);
                }

                if (rs.Rows[0]["HeureFinPDA"] == DBNull.Value)
                {
                    txtHeureFinPDA.Text = "";
                }
                else
                {
                    txtHeureFinPDA.Text = Convert.ToString(Convert.ToDateTime(rs.Rows[0]["HeureFinPDA"]).TimeOfDay);
                }

                txtDureePDA.Text = rs.Rows[0]["DureePDA"] + "";

                if (rs.Rows[0]["HeurePrevue"] == DBNull.Value)
                {
                    txtHeurePrevue.Text = "";
                }
                else
                {
                    txtHeurePrevue.Text = Convert.ToString(Convert.ToDateTime(rs.Rows[0]["HeurePrevue"]).TimeOfDay);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Load");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void frmDetInterPDA_Load(object sender, EventArgs e)
        {
            fc_Load();
        }
    }
}
