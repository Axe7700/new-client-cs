﻿namespace Axe_interDT.Views.Intervention.Forms
{
    partial class frmAVS
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.txtNoIntervention = new iTalk.iTalk_TextBox_Small2();
            this.txtCode1 = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.cmdMail = new System.Windows.Forms.Button();
            this.cmdImprime = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtNoIntervention
            // 
            this.txtNoIntervention.AccAcceptNumbersOnly = false;
            this.txtNoIntervention.AccAllowComma = false;
            this.txtNoIntervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoIntervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoIntervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoIntervention.AccHidenValue = "";
            this.txtNoIntervention.AccNotAllowedChars = null;
            this.txtNoIntervention.AccReadOnly = false;
            this.txtNoIntervention.AccReadOnlyAllowDelete = false;
            this.txtNoIntervention.AccRequired = false;
            this.txtNoIntervention.BackColor = System.Drawing.Color.White;
            this.txtNoIntervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNoIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNoIntervention.ForeColor = System.Drawing.Color.Black;
            this.txtNoIntervention.Location = new System.Drawing.Point(11, 4);
            this.txtNoIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoIntervention.MaxLength = 32767;
            this.txtNoIntervention.Multiline = false;
            this.txtNoIntervention.Name = "txtNoIntervention";
            this.txtNoIntervention.ReadOnly = false;
            this.txtNoIntervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoIntervention.Size = new System.Drawing.Size(135, 27);
            this.txtNoIntervention.TabIndex = 502;
            this.txtNoIntervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoIntervention.UseSystemPasswordChar = false;
            this.txtNoIntervention.Visible = false;
            // 
            // txtCode1
            // 
            this.txtCode1.AccAcceptNumbersOnly = false;
            this.txtCode1.AccAllowComma = false;
            this.txtCode1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCode1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCode1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCode1.AccHidenValue = "";
            this.txtCode1.AccNotAllowedChars = null;
            this.txtCode1.AccReadOnly = false;
            this.txtCode1.AccReadOnlyAllowDelete = false;
            this.txtCode1.AccRequired = false;
            this.txtCode1.BackColor = System.Drawing.Color.White;
            this.txtCode1.CustomBackColor = System.Drawing.Color.White;
            this.txtCode1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCode1.ForeColor = System.Drawing.Color.Black;
            this.txtCode1.Location = new System.Drawing.Point(185, 4);
            this.txtCode1.Margin = new System.Windows.Forms.Padding(2);
            this.txtCode1.MaxLength = 32767;
            this.txtCode1.Multiline = false;
            this.txtCode1.Name = "txtCode1";
            this.txtCode1.ReadOnly = false;
            this.txtCode1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCode1.Size = new System.Drawing.Size(135, 27);
            this.txtCode1.TabIndex = 503;
            this.txtCode1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCode1.UseSystemPasswordChar = false;
            this.txtCode1.Visible = false;
            // 
            // txtCodeImmeuble
            // 
            this.txtCodeImmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeImmeuble.AccAllowComma = false;
            this.txtCodeImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeuble.AccHidenValue = "";
            this.txtCodeImmeuble.AccNotAllowedChars = null;
            this.txtCodeImmeuble.AccReadOnly = false;
            this.txtCodeImmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeuble.AccRequired = false;
            this.txtCodeImmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeImmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeImmeuble.Location = new System.Drawing.Point(90, 74);
            this.txtCodeImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeuble.MaxLength = 32767;
            this.txtCodeImmeuble.Multiline = false;
            this.txtCodeImmeuble.Name = "txtCodeImmeuble";
            this.txtCodeImmeuble.ReadOnly = false;
            this.txtCodeImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeuble.Size = new System.Drawing.Size(135, 27);
            this.txtCodeImmeuble.TabIndex = 504;
            this.txtCodeImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeuble.UseSystemPasswordChar = false;
            this.txtCodeImmeuble.Visible = false;
            // 
            // cmdMail
            // 
            this.cmdMail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdMail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMail.FlatAppearance.BorderSize = 0;
            this.cmdMail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMail.ForeColor = System.Drawing.Color.White;
            this.cmdMail.Image = global::Axe_interDT.Properties.Resources.Send_Arrow_24x24;
            this.cmdMail.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdMail.Location = new System.Drawing.Point(185, 35);
            this.cmdMail.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMail.Name = "cmdMail";
            this.cmdMail.Size = new System.Drawing.Size(135, 35);
            this.cmdMail.TabIndex = 506;
            this.cmdMail.Text = "        Envoyer par Mail";
            this.cmdMail.UseVisualStyleBackColor = false;
            this.cmdMail.Click += new System.EventHandler(this.cmdMail_Click);
            // 
            // cmdImprime
            // 
            this.cmdImprime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdImprime.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdImprime.FlatAppearance.BorderSize = 0;
            this.cmdImprime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImprime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdImprime.ForeColor = System.Drawing.Color.White;
            this.cmdImprime.Image = global::Axe_interDT.Properties.Resources.Printer_16x16;
            this.cmdImprime.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdImprime.Location = new System.Drawing.Point(11, 35);
            this.cmdImprime.Margin = new System.Windows.Forms.Padding(2);
            this.cmdImprime.Name = "cmdImprime";
            this.cmdImprime.Size = new System.Drawing.Size(135, 35);
            this.cmdImprime.TabIndex = 505;
            this.cmdImprime.Text = "     Imprimer";
            this.cmdImprime.UseVisualStyleBackColor = false;
            this.cmdImprime.Click += new System.EventHandler(this.cmdImprime_Click);
            // 
            // frmAVS
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(327, 103);
            this.Controls.Add(this.cmdMail);
            this.Controls.Add(this.cmdImprime);
            this.Controls.Add(this.txtCodeImmeuble);
            this.Controls.Add(this.txtCode1);
            this.Controls.Add(this.txtNoIntervention);
            this.MaximumSize = new System.Drawing.Size(343, 142);
            this.MinimumSize = new System.Drawing.Size(343, 142);
            this.Name = "frmAVS";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmAVS";
            this.ResumeLayout(false);

        }

        #endregion

        public iTalk.iTalk_TextBox_Small2 txtNoIntervention;
        public iTalk.iTalk_TextBox_Small2 txtCode1;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeuble;
        public System.Windows.Forms.Button cmdMail;
        public System.Windows.Forms.Button cmdImprime;
    }
}