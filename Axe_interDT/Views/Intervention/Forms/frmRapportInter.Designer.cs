﻿namespace Axe_interDT.Views.Intervention.Forms
{
    partial class frmRapportInter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.GridDocuments = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtNoIntervention = new iTalk.iTalk_TextBox_Small2();
            this.cmdIntegrer = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            ((System.ComponentModel.ISupportInitialize)(this.GridDocuments)).BeginInit();
            this.SuspendLayout();
            // 
            // GridDocuments
            // 
            this.GridDocuments.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridDocuments.DisplayLayout.Appearance = appearance1;
            this.GridDocuments.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridDocuments.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridDocuments.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridDocuments.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridDocuments.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridDocuments.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridDocuments.DisplayLayout.MaxColScrollRegions = 1;
            this.GridDocuments.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridDocuments.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridDocuments.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridDocuments.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridDocuments.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridDocuments.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridDocuments.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridDocuments.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridDocuments.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridDocuments.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridDocuments.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridDocuments.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridDocuments.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridDocuments.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridDocuments.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridDocuments.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridDocuments.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridDocuments.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridDocuments.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridDocuments.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridDocuments.Location = new System.Drawing.Point(12, 49);
            this.GridDocuments.Name = "GridDocuments";
            this.GridDocuments.Size = new System.Drawing.Size(648, 364);
            this.GridDocuments.TabIndex = 412;
            this.GridDocuments.Text = "ultraGrid1";
            this.GridDocuments.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.GridDocuments_ClickCellButton);
            this.GridDocuments.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.GridDocuments_DoubleClickRow);
            // 
            // txtNoIntervention
            // 
            this.txtNoIntervention.AccAcceptNumbersOnly = false;
            this.txtNoIntervention.AccAllowComma = false;
            this.txtNoIntervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoIntervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoIntervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoIntervention.AccHidenValue = "";
            this.txtNoIntervention.AccNotAllowedChars = null;
            this.txtNoIntervention.AccReadOnly = false;
            this.txtNoIntervention.AccReadOnlyAllowDelete = false;
            this.txtNoIntervention.AccRequired = false;
            this.txtNoIntervention.BackColor = System.Drawing.Color.White;
            this.txtNoIntervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNoIntervention.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNoIntervention.ForeColor = System.Drawing.Color.Black;
            this.txtNoIntervention.Location = new System.Drawing.Point(402, 16);
            this.txtNoIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoIntervention.MaxLength = 32767;
            this.txtNoIntervention.Multiline = false;
            this.txtNoIntervention.Name = "txtNoIntervention";
            this.txtNoIntervention.ReadOnly = false;
            this.txtNoIntervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoIntervention.Size = new System.Drawing.Size(137, 27);
            this.txtNoIntervention.TabIndex = 504;
            this.txtNoIntervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoIntervention.UseSystemPasswordChar = false;
            this.txtNoIntervention.Visible = false;
            // 
            // cmdIntegrer
            // 
            this.cmdIntegrer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdIntegrer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdIntegrer.FlatAppearance.BorderSize = 0;
            this.cmdIntegrer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdIntegrer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdIntegrer.ForeColor = System.Drawing.Color.White;
            this.cmdIntegrer.Image = global::Axe_interDT.Properties.Resources.Edit_16x16;
            this.cmdIntegrer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdIntegrer.Location = new System.Drawing.Point(543, 9);
            this.cmdIntegrer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdIntegrer.Name = "cmdIntegrer";
            this.cmdIntegrer.Size = new System.Drawing.Size(117, 35);
            this.cmdIntegrer.TabIndex = 503;
            this.cmdIntegrer.Text = "   Integrer";
            this.cmdIntegrer.UseVisualStyleBackColor = false;
            this.cmdIntegrer.Click += new System.EventHandler(this.cmdIntegrer_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(9, 16);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(388, 16);
            this.label33.TabIndex = 502;
            this.label33.Text = "Sélectionnez les images à intégrer dans le rapport d\'intervention";
            // 
            // frmRapportInter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(672, 424);
            this.Controls.Add(this.txtNoIntervention);
            this.Controls.Add(this.cmdIntegrer);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.GridDocuments);
            this.MaximumSize = new System.Drawing.Size(688, 463);
            this.MinimumSize = new System.Drawing.Size(688, 463);
            this.Name = "frmRapportInter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmRapportInter";
            this.Load += new System.EventHandler(this.frmRapportInter_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridDocuments)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public Infragistics.Win.UltraWinGrid.UltraGrid GridDocuments;
        public iTalk.iTalk_TextBox_Small2 txtNoIntervention;
        public System.Windows.Forms.Button cmdIntegrer;
        public System.Windows.Forms.Label label33;
    }
}