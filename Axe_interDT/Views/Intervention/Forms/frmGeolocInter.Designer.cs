﻿namespace Axe_interDT.Views.Intervention.Forms
{
    partial class frmGeolocInter
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.txtNoIntervention = new iTalk.iTalk_TextBox_Small2();
            this.label14 = new System.Windows.Forms.Label();
            this.txtDistFin = new iTalk.iTalk_TextBox_Small2();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDistDeb = new iTalk.iTalk_TextBox_Small2();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.txtGPS_LongFinHZ = new iTalk.iTalk_TextBox_Small2();
            this.label9 = new System.Windows.Forms.Label();
            this.txtGPS_LatFinbHZ = new iTalk.iTalk_TextBox_Small2();
            this.label10 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.txtGPS_LongiDebHZ = new iTalk.iTalk_TextBox_Small2();
            this.txtGPS_LatDebHZ = new iTalk.iTalk_TextBox_Small2();
            this.txtGPS_LongiFin = new iTalk.iTalk_TextBox_Small2();
            this.label6 = new System.Windows.Forms.Label();
            this.txtGPS_LattFin = new iTalk.iTalk_TextBox_Small2();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtGPS_Longi = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.txtGPS_Latt = new iTalk.iTalk_TextBox_Small2();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtGPS_LattCopilot = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.txtGPS_LongiCopilot = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.txtNoIntervention);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.txtDistFin);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.txtDistDeb);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.txtGPS_LongFinHZ);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.txtGPS_LatFinbHZ);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.txtGPS_LongiDebHZ);
            this.groupBox6.Controls.Add(this.txtGPS_LatDebHZ);
            this.groupBox6.Controls.Add(this.txtGPS_LongiFin);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.txtGPS_LattFin);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.txtGPS_Longi);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.txtGPS_Latt);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.txtGPS_LattCopilot);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.txtGPS_LongiCopilot);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox6.Location = new System.Drawing.Point(12, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(591, 377);
            this.groupBox6.TabIndex = 411;
            this.groupBox6.TabStop = false;
            // 
            // txtNoIntervention
            // 
            this.txtNoIntervention.AccAcceptNumbersOnly = false;
            this.txtNoIntervention.AccAllowComma = false;
            this.txtNoIntervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoIntervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoIntervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoIntervention.AccHidenValue = "";
            this.txtNoIntervention.AccNotAllowedChars = null;
            this.txtNoIntervention.AccReadOnly = false;
            this.txtNoIntervention.AccReadOnlyAllowDelete = false;
            this.txtNoIntervention.AccRequired = false;
            this.txtNoIntervention.BackColor = System.Drawing.Color.White;
            this.txtNoIntervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNoIntervention.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNoIntervention.ForeColor = System.Drawing.Color.Black;
            this.txtNoIntervention.Location = new System.Drawing.Point(428, 20);
            this.txtNoIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoIntervention.MaxLength = 32767;
            this.txtNoIntervention.Multiline = false;
            this.txtNoIntervention.Name = "txtNoIntervention";
            this.txtNoIntervention.ReadOnly = false;
            this.txtNoIntervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoIntervention.Size = new System.Drawing.Size(135, 27);
            this.txtNoIntervention.TabIndex = 530;
            this.txtNoIntervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoIntervention.UseSystemPasswordChar = false;
            this.txtNoIntervention.Visible = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label14.Location = new System.Drawing.Point(6, 343);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(116, 19);
            this.label14.TabIndex = 529;
            this.label14.Text = "Distance en KM";
            // 
            // txtDistFin
            // 
            this.txtDistFin.AccAcceptNumbersOnly = false;
            this.txtDistFin.AccAllowComma = false;
            this.txtDistFin.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDistFin.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDistFin.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDistFin.AccHidenValue = "";
            this.txtDistFin.AccNotAllowedChars = null;
            this.txtDistFin.AccReadOnly = false;
            this.txtDistFin.AccReadOnlyAllowDelete = false;
            this.txtDistFin.AccRequired = false;
            this.txtDistFin.BackColor = System.Drawing.Color.White;
            this.txtDistFin.CustomBackColor = System.Drawing.Color.White;
            this.txtDistFin.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDistFin.ForeColor = System.Drawing.Color.Blue;
            this.txtDistFin.Location = new System.Drawing.Point(443, 345);
            this.txtDistFin.Margin = new System.Windows.Forms.Padding(2);
            this.txtDistFin.MaxLength = 32767;
            this.txtDistFin.Multiline = false;
            this.txtDistFin.Name = "txtDistFin";
            this.txtDistFin.ReadOnly = false;
            this.txtDistFin.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDistFin.Size = new System.Drawing.Size(135, 27);
            this.txtDistFin.TabIndex = 528;
            this.txtDistFin.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDistFin.UseSystemPasswordChar = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label15.Location = new System.Drawing.Point(322, 345);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(116, 19);
            this.label15.TabIndex = 527;
            this.label15.Text = "Distance en KM";
            // 
            // txtDistDeb
            // 
            this.txtDistDeb.AccAcceptNumbersOnly = false;
            this.txtDistDeb.AccAllowComma = false;
            this.txtDistDeb.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDistDeb.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDistDeb.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDistDeb.AccHidenValue = "";
            this.txtDistDeb.AccNotAllowedChars = null;
            this.txtDistDeb.AccReadOnly = false;
            this.txtDistDeb.AccReadOnlyAllowDelete = false;
            this.txtDistDeb.AccRequired = false;
            this.txtDistDeb.BackColor = System.Drawing.Color.White;
            this.txtDistDeb.CustomBackColor = System.Drawing.Color.White;
            this.txtDistDeb.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDistDeb.ForeColor = System.Drawing.Color.Blue;
            this.txtDistDeb.Location = new System.Drawing.Point(122, 345);
            this.txtDistDeb.Margin = new System.Windows.Forms.Padding(2);
            this.txtDistDeb.MaxLength = 32767;
            this.txtDistDeb.Multiline = false;
            this.txtDistDeb.Name = "txtDistDeb";
            this.txtDistDeb.ReadOnly = false;
            this.txtDistDeb.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDistDeb.Size = new System.Drawing.Size(135, 27);
            this.txtDistDeb.TabIndex = 526;
            this.txtDistDeb.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDistDeb.UseSystemPasswordChar = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label12.Location = new System.Drawing.Point(6, 312);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(80, 19);
            this.label12.TabIndex = 525;
            this.label12.Text = "Longitude";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label13.Location = new System.Drawing.Point(6, 275);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(67, 19);
            this.label13.TabIndex = 524;
            this.label13.Text = "Latitude";
            // 
            // txtGPS_LongFinHZ
            // 
            this.txtGPS_LongFinHZ.AccAcceptNumbersOnly = false;
            this.txtGPS_LongFinHZ.AccAllowComma = false;
            this.txtGPS_LongFinHZ.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGPS_LongFinHZ.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGPS_LongFinHZ.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGPS_LongFinHZ.AccHidenValue = "";
            this.txtGPS_LongFinHZ.AccNotAllowedChars = null;
            this.txtGPS_LongFinHZ.AccReadOnly = false;
            this.txtGPS_LongFinHZ.AccReadOnlyAllowDelete = false;
            this.txtGPS_LongFinHZ.AccRequired = false;
            this.txtGPS_LongFinHZ.BackColor = System.Drawing.Color.White;
            this.txtGPS_LongFinHZ.CustomBackColor = System.Drawing.Color.White;
            this.txtGPS_LongFinHZ.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtGPS_LongFinHZ.ForeColor = System.Drawing.Color.Blue;
            this.txtGPS_LongFinHZ.Location = new System.Drawing.Point(443, 314);
            this.txtGPS_LongFinHZ.Margin = new System.Windows.Forms.Padding(2);
            this.txtGPS_LongFinHZ.MaxLength = 32767;
            this.txtGPS_LongFinHZ.Multiline = false;
            this.txtGPS_LongFinHZ.Name = "txtGPS_LongFinHZ";
            this.txtGPS_LongFinHZ.ReadOnly = false;
            this.txtGPS_LongFinHZ.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGPS_LongFinHZ.Size = new System.Drawing.Size(135, 27);
            this.txtGPS_LongFinHZ.TabIndex = 523;
            this.txtGPS_LongFinHZ.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGPS_LongFinHZ.UseSystemPasswordChar = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label9.Location = new System.Drawing.Point(322, 314);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(80, 19);
            this.label9.TabIndex = 522;
            this.label9.Text = "Longitude";
            // 
            // txtGPS_LatFinbHZ
            // 
            this.txtGPS_LatFinbHZ.AccAcceptNumbersOnly = false;
            this.txtGPS_LatFinbHZ.AccAllowComma = false;
            this.txtGPS_LatFinbHZ.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGPS_LatFinbHZ.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGPS_LatFinbHZ.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGPS_LatFinbHZ.AccHidenValue = "";
            this.txtGPS_LatFinbHZ.AccNotAllowedChars = null;
            this.txtGPS_LatFinbHZ.AccReadOnly = false;
            this.txtGPS_LatFinbHZ.AccReadOnlyAllowDelete = false;
            this.txtGPS_LatFinbHZ.AccRequired = false;
            this.txtGPS_LatFinbHZ.BackColor = System.Drawing.Color.White;
            this.txtGPS_LatFinbHZ.CustomBackColor = System.Drawing.Color.White;
            this.txtGPS_LatFinbHZ.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtGPS_LatFinbHZ.ForeColor = System.Drawing.Color.Blue;
            this.txtGPS_LatFinbHZ.Location = new System.Drawing.Point(443, 279);
            this.txtGPS_LatFinbHZ.Margin = new System.Windows.Forms.Padding(2);
            this.txtGPS_LatFinbHZ.MaxLength = 32767;
            this.txtGPS_LatFinbHZ.Multiline = false;
            this.txtGPS_LatFinbHZ.Name = "txtGPS_LatFinbHZ";
            this.txtGPS_LatFinbHZ.ReadOnly = false;
            this.txtGPS_LatFinbHZ.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGPS_LatFinbHZ.Size = new System.Drawing.Size(135, 27);
            this.txtGPS_LatFinbHZ.TabIndex = 521;
            this.txtGPS_LatFinbHZ.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGPS_LatFinbHZ.UseSystemPasswordChar = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label10.Location = new System.Drawing.Point(322, 278);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(67, 19);
            this.label10.TabIndex = 520;
            this.label10.Text = "Latitude";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label11.Location = new System.Drawing.Point(239, 249);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(123, 24);
            this.label11.TabIndex = 519;
            this.label11.Text = "HORS ZONE";
            // 
            // txtGPS_LongiDebHZ
            // 
            this.txtGPS_LongiDebHZ.AccAcceptNumbersOnly = false;
            this.txtGPS_LongiDebHZ.AccAllowComma = false;
            this.txtGPS_LongiDebHZ.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGPS_LongiDebHZ.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGPS_LongiDebHZ.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGPS_LongiDebHZ.AccHidenValue = "";
            this.txtGPS_LongiDebHZ.AccNotAllowedChars = null;
            this.txtGPS_LongiDebHZ.AccReadOnly = false;
            this.txtGPS_LongiDebHZ.AccReadOnlyAllowDelete = false;
            this.txtGPS_LongiDebHZ.AccRequired = false;
            this.txtGPS_LongiDebHZ.BackColor = System.Drawing.Color.White;
            this.txtGPS_LongiDebHZ.CustomBackColor = System.Drawing.Color.White;
            this.txtGPS_LongiDebHZ.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtGPS_LongiDebHZ.ForeColor = System.Drawing.Color.Blue;
            this.txtGPS_LongiDebHZ.Location = new System.Drawing.Point(122, 314);
            this.txtGPS_LongiDebHZ.Margin = new System.Windows.Forms.Padding(2);
            this.txtGPS_LongiDebHZ.MaxLength = 32767;
            this.txtGPS_LongiDebHZ.Multiline = false;
            this.txtGPS_LongiDebHZ.Name = "txtGPS_LongiDebHZ";
            this.txtGPS_LongiDebHZ.ReadOnly = false;
            this.txtGPS_LongiDebHZ.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGPS_LongiDebHZ.Size = new System.Drawing.Size(135, 27);
            this.txtGPS_LongiDebHZ.TabIndex = 518;
            this.txtGPS_LongiDebHZ.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGPS_LongiDebHZ.UseSystemPasswordChar = false;
            // 
            // txtGPS_LatDebHZ
            // 
            this.txtGPS_LatDebHZ.AccAcceptNumbersOnly = false;
            this.txtGPS_LatDebHZ.AccAllowComma = false;
            this.txtGPS_LatDebHZ.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGPS_LatDebHZ.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGPS_LatDebHZ.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGPS_LatDebHZ.AccHidenValue = "";
            this.txtGPS_LatDebHZ.AccNotAllowedChars = null;
            this.txtGPS_LatDebHZ.AccReadOnly = false;
            this.txtGPS_LatDebHZ.AccReadOnlyAllowDelete = false;
            this.txtGPS_LatDebHZ.AccRequired = false;
            this.txtGPS_LatDebHZ.BackColor = System.Drawing.Color.White;
            this.txtGPS_LatDebHZ.CustomBackColor = System.Drawing.Color.White;
            this.txtGPS_LatDebHZ.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtGPS_LatDebHZ.ForeColor = System.Drawing.Color.Blue;
            this.txtGPS_LatDebHZ.Location = new System.Drawing.Point(122, 278);
            this.txtGPS_LatDebHZ.Margin = new System.Windows.Forms.Padding(2);
            this.txtGPS_LatDebHZ.MaxLength = 32767;
            this.txtGPS_LatDebHZ.Multiline = false;
            this.txtGPS_LatDebHZ.Name = "txtGPS_LatDebHZ";
            this.txtGPS_LatDebHZ.ReadOnly = false;
            this.txtGPS_LatDebHZ.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGPS_LatDebHZ.Size = new System.Drawing.Size(135, 27);
            this.txtGPS_LatDebHZ.TabIndex = 517;
            this.txtGPS_LatDebHZ.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGPS_LatDebHZ.UseSystemPasswordChar = false;
            // 
            // txtGPS_LongiFin
            // 
            this.txtGPS_LongiFin.AccAcceptNumbersOnly = false;
            this.txtGPS_LongiFin.AccAllowComma = false;
            this.txtGPS_LongiFin.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGPS_LongiFin.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGPS_LongiFin.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGPS_LongiFin.AccHidenValue = "";
            this.txtGPS_LongiFin.AccNotAllowedChars = null;
            this.txtGPS_LongiFin.AccReadOnly = false;
            this.txtGPS_LongiFin.AccReadOnlyAllowDelete = false;
            this.txtGPS_LongiFin.AccRequired = false;
            this.txtGPS_LongiFin.BackColor = System.Drawing.Color.White;
            this.txtGPS_LongiFin.CustomBackColor = System.Drawing.Color.White;
            this.txtGPS_LongiFin.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtGPS_LongiFin.ForeColor = System.Drawing.Color.Blue;
            this.txtGPS_LongiFin.Location = new System.Drawing.Point(443, 187);
            this.txtGPS_LongiFin.Margin = new System.Windows.Forms.Padding(2);
            this.txtGPS_LongiFin.MaxLength = 32767;
            this.txtGPS_LongiFin.Multiline = false;
            this.txtGPS_LongiFin.Name = "txtGPS_LongiFin";
            this.txtGPS_LongiFin.ReadOnly = false;
            this.txtGPS_LongiFin.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGPS_LongiFin.Size = new System.Drawing.Size(135, 27);
            this.txtGPS_LongiFin.TabIndex = 515;
            this.txtGPS_LongiFin.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGPS_LongiFin.UseSystemPasswordChar = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label6.Location = new System.Drawing.Point(358, 187);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(80, 19);
            this.label6.TabIndex = 514;
            this.label6.Text = "Longitude";
            // 
            // txtGPS_LattFin
            // 
            this.txtGPS_LattFin.AccAcceptNumbersOnly = false;
            this.txtGPS_LattFin.AccAllowComma = false;
            this.txtGPS_LattFin.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGPS_LattFin.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGPS_LattFin.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGPS_LattFin.AccHidenValue = "";
            this.txtGPS_LattFin.AccNotAllowedChars = null;
            this.txtGPS_LattFin.AccReadOnly = false;
            this.txtGPS_LattFin.AccReadOnlyAllowDelete = false;
            this.txtGPS_LattFin.AccRequired = false;
            this.txtGPS_LattFin.BackColor = System.Drawing.Color.White;
            this.txtGPS_LattFin.CustomBackColor = System.Drawing.Color.White;
            this.txtGPS_LattFin.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtGPS_LattFin.ForeColor = System.Drawing.Color.Blue;
            this.txtGPS_LattFin.Location = new System.Drawing.Point(443, 156);
            this.txtGPS_LattFin.Margin = new System.Windows.Forms.Padding(2);
            this.txtGPS_LattFin.MaxLength = 32767;
            this.txtGPS_LattFin.Multiline = false;
            this.txtGPS_LattFin.Name = "txtGPS_LattFin";
            this.txtGPS_LattFin.ReadOnly = false;
            this.txtGPS_LattFin.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGPS_LattFin.Size = new System.Drawing.Size(135, 27);
            this.txtGPS_LattFin.TabIndex = 513;
            this.txtGPS_LattFin.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGPS_LattFin.UseSystemPasswordChar = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label7.Location = new System.Drawing.Point(358, 155);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 19);
            this.label7.TabIndex = 512;
            this.label7.Text = "Latitude";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label8.Location = new System.Drawing.Point(385, 120);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(186, 24);
            this.label8.TabIndex = 511;
            this.label8.Text = "FIN INTERVENTION";
            // 
            // txtGPS_Longi
            // 
            this.txtGPS_Longi.AccAcceptNumbersOnly = false;
            this.txtGPS_Longi.AccAllowComma = false;
            this.txtGPS_Longi.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGPS_Longi.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGPS_Longi.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGPS_Longi.AccHidenValue = "";
            this.txtGPS_Longi.AccNotAllowedChars = null;
            this.txtGPS_Longi.AccReadOnly = false;
            this.txtGPS_Longi.AccReadOnlyAllowDelete = false;
            this.txtGPS_Longi.AccRequired = false;
            this.txtGPS_Longi.BackColor = System.Drawing.Color.White;
            this.txtGPS_Longi.CustomBackColor = System.Drawing.Color.White;
            this.txtGPS_Longi.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtGPS_Longi.ForeColor = System.Drawing.Color.Blue;
            this.txtGPS_Longi.Location = new System.Drawing.Point(122, 186);
            this.txtGPS_Longi.Margin = new System.Windows.Forms.Padding(2);
            this.txtGPS_Longi.MaxLength = 32767;
            this.txtGPS_Longi.Multiline = false;
            this.txtGPS_Longi.Name = "txtGPS_Longi";
            this.txtGPS_Longi.ReadOnly = false;
            this.txtGPS_Longi.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGPS_Longi.Size = new System.Drawing.Size(135, 27);
            this.txtGPS_Longi.TabIndex = 510;
            this.txtGPS_Longi.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGPS_Longi.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(13, 186);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(80, 19);
            this.label3.TabIndex = 509;
            this.label3.Text = "Longitude";
            // 
            // txtGPS_Latt
            // 
            this.txtGPS_Latt.AccAcceptNumbersOnly = false;
            this.txtGPS_Latt.AccAllowComma = false;
            this.txtGPS_Latt.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGPS_Latt.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGPS_Latt.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGPS_Latt.AccHidenValue = "";
            this.txtGPS_Latt.AccNotAllowedChars = null;
            this.txtGPS_Latt.AccReadOnly = false;
            this.txtGPS_Latt.AccReadOnlyAllowDelete = false;
            this.txtGPS_Latt.AccRequired = false;
            this.txtGPS_Latt.BackColor = System.Drawing.Color.White;
            this.txtGPS_Latt.CustomBackColor = System.Drawing.Color.White;
            this.txtGPS_Latt.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtGPS_Latt.ForeColor = System.Drawing.Color.Blue;
            this.txtGPS_Latt.Location = new System.Drawing.Point(122, 155);
            this.txtGPS_Latt.Margin = new System.Windows.Forms.Padding(2);
            this.txtGPS_Latt.MaxLength = 32767;
            this.txtGPS_Latt.Multiline = false;
            this.txtGPS_Latt.Name = "txtGPS_Latt";
            this.txtGPS_Latt.ReadOnly = false;
            this.txtGPS_Latt.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGPS_Latt.Size = new System.Drawing.Size(135, 27);
            this.txtGPS_Latt.TabIndex = 508;
            this.txtGPS_Latt.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGPS_Latt.UseSystemPasswordChar = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label4.Location = new System.Drawing.Point(13, 154);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(67, 19);
            this.label4.TabIndex = 507;
            this.label4.Text = "Latitude";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label5.Location = new System.Drawing.Point(30, 119);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(219, 24);
            this.label5.TabIndex = 506;
            this.label5.Text = "DEBUT INTERVENTION";
            // 
            // txtGPS_LattCopilot
            // 
            this.txtGPS_LattCopilot.AccAcceptNumbersOnly = false;
            this.txtGPS_LattCopilot.AccAllowComma = false;
            this.txtGPS_LattCopilot.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGPS_LattCopilot.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGPS_LattCopilot.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGPS_LattCopilot.AccHidenValue = "";
            this.txtGPS_LattCopilot.AccNotAllowedChars = null;
            this.txtGPS_LattCopilot.AccReadOnly = false;
            this.txtGPS_LattCopilot.AccReadOnlyAllowDelete = false;
            this.txtGPS_LattCopilot.AccRequired = false;
            this.txtGPS_LattCopilot.BackColor = System.Drawing.Color.White;
            this.txtGPS_LattCopilot.CustomBackColor = System.Drawing.Color.White;
            this.txtGPS_LattCopilot.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtGPS_LattCopilot.ForeColor = System.Drawing.Color.Black;
            this.txtGPS_LattCopilot.Location = new System.Drawing.Point(443, 65);
            this.txtGPS_LattCopilot.Margin = new System.Windows.Forms.Padding(2);
            this.txtGPS_LattCopilot.MaxLength = 32767;
            this.txtGPS_LattCopilot.Multiline = false;
            this.txtGPS_LattCopilot.Name = "txtGPS_LattCopilot";
            this.txtGPS_LattCopilot.ReadOnly = false;
            this.txtGPS_LattCopilot.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGPS_LattCopilot.Size = new System.Drawing.Size(135, 27);
            this.txtGPS_LattCopilot.TabIndex = 505;
            this.txtGPS_LattCopilot.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGPS_LattCopilot.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(358, 66);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(67, 19);
            this.label2.TabIndex = 504;
            this.label2.Text = "Latitude";
            // 
            // txtGPS_LongiCopilot
            // 
            this.txtGPS_LongiCopilot.AccAcceptNumbersOnly = false;
            this.txtGPS_LongiCopilot.AccAllowComma = false;
            this.txtGPS_LongiCopilot.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGPS_LongiCopilot.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGPS_LongiCopilot.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGPS_LongiCopilot.AccHidenValue = "";
            this.txtGPS_LongiCopilot.AccNotAllowedChars = null;
            this.txtGPS_LongiCopilot.AccReadOnly = false;
            this.txtGPS_LongiCopilot.AccReadOnlyAllowDelete = false;
            this.txtGPS_LongiCopilot.AccRequired = false;
            this.txtGPS_LongiCopilot.BackColor = System.Drawing.Color.White;
            this.txtGPS_LongiCopilot.CustomBackColor = System.Drawing.Color.White;
            this.txtGPS_LongiCopilot.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtGPS_LongiCopilot.ForeColor = System.Drawing.Color.Blue;
            this.txtGPS_LongiCopilot.Location = new System.Drawing.Point(122, 64);
            this.txtGPS_LongiCopilot.Margin = new System.Windows.Forms.Padding(2);
            this.txtGPS_LongiCopilot.MaxLength = 32767;
            this.txtGPS_LongiCopilot.Multiline = false;
            this.txtGPS_LongiCopilot.Name = "txtGPS_LongiCopilot";
            this.txtGPS_LongiCopilot.ReadOnly = false;
            this.txtGPS_LongiCopilot.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGPS_LongiCopilot.Size = new System.Drawing.Size(135, 27);
            this.txtGPS_LongiCopilot.TabIndex = 503;
            this.txtGPS_LongiCopilot.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGPS_LongiCopilot.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(16, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 19);
            this.label1.TabIndex = 502;
            this.label1.Text = "Latitude";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 14F);
            this.label33.Location = new System.Drawing.Point(229, 28);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(164, 24);
            this.label33.TabIndex = 384;
            this.label33.Text = "COPILOT ACTIVE";
            // 
            // frmGeolocInter
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(612, 401);
            this.Controls.Add(this.groupBox6);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(628, 440);
            this.MinimumSize = new System.Drawing.Size(628, 440);
            this.Name = "frmGeolocInter";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Géolocalisation de l\'intervention";
            this.Load += new System.EventHandler(this.frmGeolocInter_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label14;
        public iTalk.iTalk_TextBox_Small2 txtDistFin;
        private System.Windows.Forms.Label label15;
        public iTalk.iTalk_TextBox_Small2 txtDistDeb;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        public iTalk.iTalk_TextBox_Small2 txtGPS_LongFinHZ;
        private System.Windows.Forms.Label label9;
        public iTalk.iTalk_TextBox_Small2 txtGPS_LatFinbHZ;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Label label11;
        public iTalk.iTalk_TextBox_Small2 txtGPS_LongiDebHZ;
        public iTalk.iTalk_TextBox_Small2 txtGPS_LatDebHZ;
        public iTalk.iTalk_TextBox_Small2 txtGPS_LongiFin;
        private System.Windows.Forms.Label label6;
        public iTalk.iTalk_TextBox_Small2 txtGPS_LattFin;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        public iTalk.iTalk_TextBox_Small2 txtGPS_Longi;
        private System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 txtGPS_Latt;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        public iTalk.iTalk_TextBox_Small2 txtGPS_LattCopilot;
        private System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtGPS_LongiCopilot;
        private System.Windows.Forms.Label label1;
        public iTalk.iTalk_TextBox_Small2 txtNoIntervention;
    }
}