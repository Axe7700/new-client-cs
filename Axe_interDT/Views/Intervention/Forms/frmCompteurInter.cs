﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Intervention.Forms
{
    public partial class frmCompteurInter : Form
    {
        DataTable rsRel = null;
        ModAdo modAdorsRel = null;
        public frmCompteurInter()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmCompteurInter_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_LoadReleve();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadReleve()
        {
            string sSQL = null;

            try
            {
                sSQL = "SELECT       INT_Releve.NoAuto,INT_Releve.Etat, Intervention.NoIntervention AS [No Intervention],"
                    + " Personnel.Prenom + ' ' + Personnel.Nom AS Intervenant, Immeuble.CodeImmeuble, " 
                    + " INT_Releve.Libel_App AS Compteur, INT_Releve.PeriodeAu AS [Date relevé],"
                    +" INT_Releve.Evenement AS Evènement,  INT_Releve.LibelleEvent AS Désignation,"
                    +" INT_Releve.IndexDebut AS [Index début], INT_Releve.Livraison,"
                    +" INT_Releve.SaisieJauge AS Jauge," 
                    + " INT_Releve.SaisiePige AS Pige, INT_Releve.IndexFin AS [Index fin],"
                    +" INT_Releve.Anc_IndexDebut AS [Ancien Index début], "
                    + " INT_Releve.Anc_IndexFin AS [Ancien Index fin], INT_Releve.Consommation AS Conso,"
                    +" INT_Releve.dPeriodeDu AS [Période du], " 
                    + " INT_Releve.dPeriodeAu AS [Période au],"
                    + "INT_Releve.Observations AS [Observation relevé], Imm_Appareils.OrigineP1, " 
                    + " P1_TypeCpt.Livraison AS chkLivr, INT_Releve.Periodedu AS ReleveDu, "
                    +"INT_Releve.NumAppareil, INT_Releve.DiametreCuve AS [Diamètre cuve], " 
                    + " INT_Releve.Chauffage ";

                sSQL = sSQL + " FROM         INT_Releve INNER JOIN" 
                    + " Intervention ON INT_Releve.NoIntervention = Intervention.NoIntervention INNER JOIN "
                    + " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble LEFT OUTER JOIN "
                    + " FacArticle ON Intervention.Article = FacArticle.CodeArticle LEFT OUTER JOIN "
                    + " Imm_Appareils ON INT_Releve.NumAppareil = Imm_Appareils.NumAppareil LEFT OUTER JOIN " 
                    + " P1_TypeCpt ON Imm_Appareils.Type_Compteur = P1_TypeCpt.CODE LEFT OUTER JOIN " 
                    + " Personnel ON Intervention.Intervenant = Personnel.Matricule";

                sSQL = sSQL + " WHERE     Intervention.NoIntervention  = " + General.nz(txtNointervention.Text, 0) + "" 
                    + " ORDER BY Immeuble.CodeImmeuble, [Date relevé]";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                modAdorsRel = new ModAdo();
                rsRel = modAdorsRel.fc_OpenRecordSet(sSQL);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridRel.DataSource = rsRel;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadReleve");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridRel_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            GridRel.UpdateData();
           // modAdorsRel.Update();
        }

        private void GridRel_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdorsRel.Update();
        }

        private void GridRel_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                e.Cancel = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridRel_AfterExitEditMode(object sender, EventArgs e)
        {

            if (GridRel.ActiveRow.Cells["Etat"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET Etat= '{GridRel.ActiveRow.Cells["Etat"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Compteur"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET Libel_App= '{GridRel.ActiveRow.Cells["Compteur"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");


            if (GridRel.ActiveRow.Cells["Date relevé"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET PeriodeAu= '{GridRel.ActiveRow.Cells["Date relevé"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Evènement"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET Evenement= '{GridRel.ActiveRow.Cells["Evènement"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Désignation"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET LibelleEvent= '{GridRel.ActiveRow.Cells["Désignation"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Index début"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET IndexDebut = '{GridRel.ActiveRow.Cells["Index début"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Livraison"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET Livraison= '{GridRel.ActiveRow.Cells["Livraison"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Jauge"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET SaisieJauge= '{GridRel.ActiveRow.Cells["Jauge"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Pige"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET SaisiePige= '{GridRel.ActiveRow.Cells["Pige"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Index fin"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET IndexFin = '{GridRel.ActiveRow.Cells["Index fin"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Ancien Index début"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET Anc_IndexDebut= '{GridRel.ActiveRow.Cells["Ancien Index début"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Ancien Index fin"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET Anc_IndexFin= '{GridRel.ActiveRow.Cells["Ancien Index fin"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Conso"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET Consommation = '{GridRel.ActiveRow.Cells["Conso"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Période du"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET dPeriodeDu= '{GridRel.ActiveRow.Cells["Période du"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Période au"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET dPeriodeAu = '{GridRel.ActiveRow.Cells["Période au"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Observation relevé"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET Observations= '{GridRel.ActiveRow.Cells["Observation relevé"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["ReleveDu"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET Periodedu = '{GridRel.ActiveRow.Cells["ReleveDu"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");
            
            if (GridRel.ActiveRow.Cells["chkLivr"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET Livraison = '{GridRel.ActiveRow.Cells["chkLivr"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["NumAppareil"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET NumAppareil = '{GridRel.ActiveRow.Cells["NumAppareil"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Diamètre cuve"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET DiametreCuve = '{GridRel.ActiveRow.Cells["Diamètre cuve"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

            if (GridRel.ActiveRow.Cells["Chauffage"].DataChanged)//tested
                General.Execute($"UPDATE INT_Releve SET Chauffage= '{GridRel.ActiveRow.Cells["Chauffage"].Value.ToString()}'" +
                    $" WHERE  NoAuto={GridRel.ActiveRow.Cells["NoAuto"].Value.ToString()} and  NoIntervention  = '{GridRel.ActiveRow.Cells["No Intervention"].Value.ToString()}'");

         
           

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridRel_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridRel.DisplayLayout.Bands[0].Columns["NoAuto"].Hidden = true;
            GridRel.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Hidden = true;
            GridRel.DisplayLayout.Bands[0].Columns["No Intervention"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            GridRel.DisplayLayout.Bands[0].Columns["Intervenant"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
        
    }
    }
}
