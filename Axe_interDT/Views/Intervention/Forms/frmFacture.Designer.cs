﻿namespace Axe_interDT.Views.Intervention.Forms
{
    partial class frmFacture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdVisualiser = new System.Windows.Forms.Button();
            this.cmdFactTemp = new System.Windows.Forms.Button();
            this.txtNumFicheStandard = new iTalk.iTalk_TextBox_Small2();
            this.txtstatut = new iTalk.iTalk_TextBox_Small2();
            this.cmdCreerFac = new System.Windows.Forms.Button();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.optGarantie = new System.Windows.Forms.RadioButton();
            this.optDefinitive = new System.Windows.Forms.RadioButton();
            this.OptAcompte = new System.Windows.Forms.RadioButton();
            this.OptDevis = new System.Windows.Forms.RadioButton();
            this.Optintervention = new System.Windows.Forms.RadioButton();
            this.txtAcompte = new iTalk.iTalk_TextBox_Small2();
            this.cmdCreeAvoir = new System.Windows.Forms.Button();
            this.lblNodevis = new iTalk.iTalk_TextBox_Small2();
            this.cmdEditFact = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdVisualiser
            // 
            this.cmdVisualiser.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdVisualiser.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdVisualiser.FlatAppearance.BorderSize = 0;
            this.cmdVisualiser.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdVisualiser.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdVisualiser.ForeColor = System.Drawing.Color.White;
            this.cmdVisualiser.Image = global::Axe_interDT.Properties.Resources.Eye_24x24;
            this.cmdVisualiser.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdVisualiser.Location = new System.Drawing.Point(41, 52);
            this.cmdVisualiser.Margin = new System.Windows.Forms.Padding(2);
            this.cmdVisualiser.Name = "cmdVisualiser";
            this.cmdVisualiser.Size = new System.Drawing.Size(264, 40);
            this.cmdVisualiser.TabIndex = 579;
            this.cmdVisualiser.Tag = "";
            this.cmdVisualiser.Text = "Visualiser les factures ";
            this.cmdVisualiser.UseVisualStyleBackColor = false;
            this.cmdVisualiser.Click += new System.EventHandler(this.cmdVisualiser_Click);
            // 
            // cmdFactTemp
            // 
            this.cmdFactTemp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFactTemp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdFactTemp.FlatAppearance.BorderSize = 0;
            this.cmdFactTemp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFactTemp.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdFactTemp.ForeColor = System.Drawing.Color.White;
            this.cmdFactTemp.Image = global::Axe_interDT.Properties.Resources.Eye_24x24;
            this.cmdFactTemp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdFactTemp.Location = new System.Drawing.Point(41, 99);
            this.cmdFactTemp.Margin = new System.Windows.Forms.Padding(2);
            this.cmdFactTemp.Name = "cmdFactTemp";
            this.cmdFactTemp.Size = new System.Drawing.Size(264, 40);
            this.cmdFactTemp.TabIndex = 579;
            this.cmdFactTemp.Tag = "";
            this.cmdFactTemp.Text = "Visualiser les projets de facture";
            this.cmdFactTemp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdFactTemp.UseVisualStyleBackColor = false;
            this.cmdFactTemp.Click += new System.EventHandler(this.cmdFactTemp_Click);
            // 
            // txtNumFicheStandard
            // 
            this.txtNumFicheStandard.AccAcceptNumbersOnly = false;
            this.txtNumFicheStandard.AccAllowComma = false;
            this.txtNumFicheStandard.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNumFicheStandard.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNumFicheStandard.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNumFicheStandard.AccHidenValue = "";
            this.txtNumFicheStandard.AccNotAllowedChars = null;
            this.txtNumFicheStandard.AccReadOnly = false;
            this.txtNumFicheStandard.AccReadOnlyAllowDelete = false;
            this.txtNumFicheStandard.AccRequired = false;
            this.txtNumFicheStandard.BackColor = System.Drawing.Color.White;
            this.txtNumFicheStandard.CustomBackColor = System.Drawing.Color.White;
            this.txtNumFicheStandard.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtNumFicheStandard.ForeColor = System.Drawing.Color.Blue;
            this.txtNumFicheStandard.Location = new System.Drawing.Point(30, 11);
            this.txtNumFicheStandard.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumFicheStandard.MaxLength = 20;
            this.txtNumFicheStandard.Multiline = false;
            this.txtNumFicheStandard.Name = "txtNumFicheStandard";
            this.txtNumFicheStandard.ReadOnly = false;
            this.txtNumFicheStandard.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNumFicheStandard.Size = new System.Drawing.Size(94, 27);
            this.txtNumFicheStandard.TabIndex = 580;
            this.txtNumFicheStandard.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNumFicheStandard.UseSystemPasswordChar = false;
            this.txtNumFicheStandard.Visible = false;
            // 
            // txtstatut
            // 
            this.txtstatut.AccAcceptNumbersOnly = false;
            this.txtstatut.AccAllowComma = false;
            this.txtstatut.AccBackgroundColor = System.Drawing.Color.White;
            this.txtstatut.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtstatut.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtstatut.AccHidenValue = "";
            this.txtstatut.AccNotAllowedChars = null;
            this.txtstatut.AccReadOnly = false;
            this.txtstatut.AccReadOnlyAllowDelete = false;
            this.txtstatut.AccRequired = false;
            this.txtstatut.BackColor = System.Drawing.Color.White;
            this.txtstatut.CustomBackColor = System.Drawing.Color.White;
            this.txtstatut.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtstatut.ForeColor = System.Drawing.Color.Blue;
            this.txtstatut.Location = new System.Drawing.Point(128, 11);
            this.txtstatut.Margin = new System.Windows.Forms.Padding(2);
            this.txtstatut.MaxLength = 20;
            this.txtstatut.Multiline = false;
            this.txtstatut.Name = "txtstatut";
            this.txtstatut.ReadOnly = false;
            this.txtstatut.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtstatut.Size = new System.Drawing.Size(94, 27);
            this.txtstatut.TabIndex = 580;
            this.txtstatut.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtstatut.UseSystemPasswordChar = false;
            this.txtstatut.Visible = false;
            // 
            // cmdCreerFac
            // 
            this.cmdCreerFac.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdCreerFac.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCreerFac.FlatAppearance.BorderSize = 0;
            this.cmdCreerFac.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCreerFac.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCreerFac.ForeColor = System.Drawing.Color.White;
            this.cmdCreerFac.Image = global::Axe_interDT.Properties.Resources.Add_File_24x24;
            this.cmdCreerFac.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdCreerFac.Location = new System.Drawing.Point(64, 9);
            this.cmdCreerFac.Margin = new System.Windows.Forms.Padding(2);
            this.cmdCreerFac.Name = "cmdCreerFac";
            this.cmdCreerFac.Size = new System.Drawing.Size(264, 40);
            this.cmdCreerFac.TabIndex = 579;
            this.cmdCreerFac.Tag = "";
            this.cmdCreerFac.Text = "Créer une facture";
            this.cmdCreerFac.UseVisualStyleBackColor = false;
            this.cmdCreerFac.Click += new System.EventHandler(this.cmdCreerFac_Click);
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.ultraGroupBox2);
            this.ultraGroupBox1.Controls.Add(this.OptDevis);
            this.ultraGroupBox1.Controls.Add(this.Optintervention);
            this.ultraGroupBox1.Controls.Add(this.cmdCreerFac);
            this.ultraGroupBox1.Location = new System.Drawing.Point(342, 43);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(345, 212);
            this.ultraGroupBox1.TabIndex = 581;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.optGarantie);
            this.ultraGroupBox2.Controls.Add(this.optDefinitive);
            this.ultraGroupBox2.Controls.Add(this.OptAcompte);
            this.ultraGroupBox2.Location = new System.Drawing.Point(64, 94);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(264, 110);
            this.ultraGroupBox2.TabIndex = 581;
            // 
            // optGarantie
            // 
            this.optGarantie.AutoSize = true;
            this.optGarantie.Enabled = false;
            this.optGarantie.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optGarantie.Location = new System.Drawing.Point(16, 74);
            this.optGarantie.Name = "optGarantie";
            this.optGarantie.Size = new System.Drawing.Size(168, 23);
            this.optGarantie.TabIndex = 580;
            this.optGarantie.TabStop = true;
            this.optGarantie.Text = "Retenue de garantie";
            this.optGarantie.UseVisualStyleBackColor = true;
            this.optGarantie.Visible = false;
            // 
            // optDefinitive
            // 
            this.optDefinitive.AutoSize = true;
            this.optDefinitive.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optDefinitive.Location = new System.Drawing.Point(16, 45);
            this.optDefinitive.Name = "optDefinitive";
            this.optDefinitive.Size = new System.Drawing.Size(93, 23);
            this.optDefinitive.TabIndex = 580;
            this.optDefinitive.TabStop = true;
            this.optDefinitive.Text = "Définitive";
            this.optDefinitive.UseVisualStyleBackColor = true;
            // 
            // OptAcompte
            // 
            this.OptAcompte.AutoSize = true;
            this.OptAcompte.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptAcompte.Location = new System.Drawing.Point(16, 16);
            this.OptAcompte.Name = "OptAcompte";
            this.OptAcompte.Size = new System.Drawing.Size(160, 23);
            this.OptAcompte.TabIndex = 580;
            this.OptAcompte.TabStop = true;
            this.OptAcompte.Text = "Acompte/ Situation";
            this.OptAcompte.UseVisualStyleBackColor = true;
            // 
            // OptDevis
            // 
            this.OptDevis.AutoSize = true;
            this.OptDevis.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptDevis.Location = new System.Drawing.Point(209, 65);
            this.OptDevis.Name = "OptDevis";
            this.OptDevis.Size = new System.Drawing.Size(64, 23);
            this.OptDevis.TabIndex = 580;
            this.OptDevis.TabStop = true;
            this.OptDevis.Text = "Devis";
            this.OptDevis.UseVisualStyleBackColor = true;
            this.OptDevis.CheckedChanged += new System.EventHandler(this.OptDevis_CheckedChanged);
            // 
            // Optintervention
            // 
            this.Optintervention.AutoSize = true;
            this.Optintervention.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Optintervention.Location = new System.Drawing.Point(64, 65);
            this.Optintervention.Name = "Optintervention";
            this.Optintervention.Size = new System.Drawing.Size(112, 23);
            this.Optintervention.TabIndex = 580;
            this.Optintervention.TabStop = true;
            this.Optintervention.Text = "Intervention";
            this.Optintervention.UseVisualStyleBackColor = true;
            this.Optintervention.CheckedChanged += new System.EventHandler(this.Optintervention_CheckedChanged);
            // 
            // txtAcompte
            // 
            this.txtAcompte.AccAcceptNumbersOnly = false;
            this.txtAcompte.AccAllowComma = false;
            this.txtAcompte.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAcompte.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAcompte.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtAcompte.AccHidenValue = "";
            this.txtAcompte.AccNotAllowedChars = null;
            this.txtAcompte.AccReadOnly = false;
            this.txtAcompte.AccReadOnlyAllowDelete = false;
            this.txtAcompte.AccRequired = false;
            this.txtAcompte.BackColor = System.Drawing.Color.White;
            this.txtAcompte.CustomBackColor = System.Drawing.Color.White;
            this.txtAcompte.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtAcompte.ForeColor = System.Drawing.Color.Blue;
            this.txtAcompte.Location = new System.Drawing.Point(226, 11);
            this.txtAcompte.Margin = new System.Windows.Forms.Padding(2);
            this.txtAcompte.MaxLength = 20;
            this.txtAcompte.Multiline = false;
            this.txtAcompte.Name = "txtAcompte";
            this.txtAcompte.ReadOnly = false;
            this.txtAcompte.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAcompte.Size = new System.Drawing.Size(94, 27);
            this.txtAcompte.TabIndex = 580;
            this.txtAcompte.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAcompte.UseSystemPasswordChar = false;
            this.txtAcompte.Visible = false;
            // 
            // cmdCreeAvoir
            // 
            this.cmdCreeAvoir.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdCreeAvoir.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCreeAvoir.FlatAppearance.BorderSize = 0;
            this.cmdCreeAvoir.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCreeAvoir.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCreeAvoir.ForeColor = System.Drawing.Color.White;
            this.cmdCreeAvoir.Image = global::Axe_interDT.Properties.Resources.Add_File_24x24;
            this.cmdCreeAvoir.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdCreeAvoir.Location = new System.Drawing.Point(41, 207);
            this.cmdCreeAvoir.Margin = new System.Windows.Forms.Padding(2);
            this.cmdCreeAvoir.Name = "cmdCreeAvoir";
            this.cmdCreeAvoir.Size = new System.Drawing.Size(264, 40);
            this.cmdCreeAvoir.TabIndex = 579;
            this.cmdCreeAvoir.Tag = "";
            this.cmdCreeAvoir.Text = "Créer une facture";
            this.cmdCreeAvoir.UseVisualStyleBackColor = false;
            this.cmdCreeAvoir.Visible = false;
            this.cmdCreeAvoir.Click += new System.EventHandler(this.cmdCreerFac_Click);
            // 
            // lblNodevis
            // 
            this.lblNodevis.AccAcceptNumbersOnly = false;
            this.lblNodevis.AccAllowComma = false;
            this.lblNodevis.AccBackgroundColor = System.Drawing.Color.White;
            this.lblNodevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblNodevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblNodevis.AccHidenValue = "";
            this.lblNodevis.AccNotAllowedChars = null;
            this.lblNodevis.AccReadOnly = false;
            this.lblNodevis.AccReadOnlyAllowDelete = false;
            this.lblNodevis.AccRequired = false;
            this.lblNodevis.BackColor = System.Drawing.Color.White;
            this.lblNodevis.CustomBackColor = System.Drawing.Color.White;
            this.lblNodevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblNodevis.ForeColor = System.Drawing.Color.Blue;
            this.lblNodevis.Location = new System.Drawing.Point(342, 11);
            this.lblNodevis.Margin = new System.Windows.Forms.Padding(2);
            this.lblNodevis.MaxLength = 20;
            this.lblNodevis.Multiline = false;
            this.lblNodevis.Name = "lblNodevis";
            this.lblNodevis.ReadOnly = false;
            this.lblNodevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblNodevis.Size = new System.Drawing.Size(345, 27);
            this.lblNodevis.TabIndex = 582;
            this.lblNodevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblNodevis.UseSystemPasswordChar = false;
            // 
            // cmdEditFact
            // 
            this.cmdEditFact.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdEditFact.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdEditFact.FlatAppearance.BorderSize = 0;
            this.cmdEditFact.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdEditFact.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdEditFact.ForeColor = System.Drawing.Color.White;
            this.cmdEditFact.Image = global::Axe_interDT.Properties.Resources.Edit_16x16;
            this.cmdEditFact.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdEditFact.Location = new System.Drawing.Point(41, 144);
            this.cmdEditFact.Margin = new System.Windows.Forms.Padding(2);
            this.cmdEditFact.Name = "cmdEditFact";
            this.cmdEditFact.Size = new System.Drawing.Size(264, 40);
            this.cmdEditFact.TabIndex = 583;
            this.cmdEditFact.Tag = "";
            this.cmdEditFact.Text = "Modifier la facture en cours";
            this.cmdEditFact.UseVisualStyleBackColor = false;
            this.cmdEditFact.Visible = false;
            this.cmdEditFact.Click += new System.EventHandler(this.cmdEditFact_Click);
            // 
            // frmFacture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(705, 268);
            this.Controls.Add(this.cmdEditFact);
            this.Controls.Add(this.lblNodevis);
            this.Controls.Add(this.ultraGroupBox1);
            this.Controls.Add(this.txtAcompte);
            this.Controls.Add(this.txtstatut);
            this.Controls.Add(this.cmdCreeAvoir);
            this.Controls.Add(this.txtNumFicheStandard);
            this.Controls.Add(this.cmdFactTemp);
            this.Controls.Add(this.cmdVisualiser);
            this.MaximumSize = new System.Drawing.Size(721, 307);
            this.MinimumSize = new System.Drawing.Size(721, 307);
            this.Name = "frmFacture";
            this.Text = "Facture";
            this.Load += new System.EventHandler(this.frmFacture_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Button cmdVisualiser;
        public System.Windows.Forms.Button cmdFactTemp;
        public iTalk.iTalk_TextBox_Small2 txtNumFicheStandard;
        public iTalk.iTalk_TextBox_Small2 txtstatut;
        public System.Windows.Forms.Button cmdCreerFac;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        public System.Windows.Forms.RadioButton optGarantie;
        public System.Windows.Forms.RadioButton optDefinitive;
        public System.Windows.Forms.RadioButton OptAcompte;
        public System.Windows.Forms.RadioButton OptDevis;
        public System.Windows.Forms.RadioButton Optintervention;
        public iTalk.iTalk_TextBox_Small2 txtAcompte;
        public System.Windows.Forms.Button cmdCreeAvoir;
        public iTalk.iTalk_TextBox_Small2 lblNodevis;
        public System.Windows.Forms.Button cmdEditFact;
    }
}