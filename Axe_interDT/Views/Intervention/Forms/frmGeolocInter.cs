﻿using Axe_interDT.Shared;
using System;
using System.Data;
using System.Windows.Forms;

namespace Axe_interDT.Views.Intervention.Forms
{
    public partial class frmGeolocInter : Form
    {
        public frmGeolocInter()
        {
            InitializeComponent();
        }

        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadDetInter()
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            DataTable rsDoc = default(DataTable);
            ModAdo modAdorsDoc = null;
            string sCodeImmeuble = null;
            double dbLatImm = 0;
            double dbLonImm = 0;
            double dbLatDbHZ = 0;
            double dbLonDbHZ = 0;
            double dbLatFinHZ = 0;
            double dbLonFinHZ = 0;

            try
            {
                sSQL = "SELECT NoIntervention, GPS_Longi, GPS_Latt, GPS_LongiFin, GPS_LattFin, GPS_LongiDebHZ, GPS_LatDebHZ, GPS_LongFinHZ, " +
                    " GPS_LatFinbHZ, GPS_LongiCopilot, GPS_LattCopilot, codeimmeuble " + " From Intervention " + " WHERE NoIntervention = " + txtNoIntervention.Text;

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count == 0)
                {
                    return;
                }

                txtGPS_Longi.Text = rs.Rows[0]["GPS_Longi"] + "";
                txtGPS_Latt.Text = rs.Rows[0]["GPS_Latt"] + "";
                txtGPS_LongiFin.Text = rs.Rows[0]["GPS_LongiFin"] + "";
                txtGPS_LattFin.Text = rs.Rows[0]["GPS_LattFin"] + "";

                txtGPS_LongiDebHZ.Text = rs.Rows[0]["GPS_LongiDebHZ"] + "";
                txtGPS_LatDebHZ.Text = rs.Rows[0]["GPS_LatDebHZ"] + "";
                txtGPS_LongFinHZ.Text = rs.Rows[0]["GPS_LongFinHZ"] + "";
                txtGPS_LatFinbHZ.Text = rs.Rows[0]["GPS_LatFinbHZ"] + "";
                txtGPS_LattCopilot.Text = rs.Rows[0]["GPS_LattCopilot"] + "";
                txtGPS_LongiCopilot.Text = rs.Rows[0]["GPS_LongiCopilot"] + "";

                sCodeImmeuble = rs.Rows[0]["CodeImmeuble"] + "";

                modAdors.Close();

                sSQL = "SELECT     Latitude, Longitude" + " From Immeuble WHERE  CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {

                    dbLatImm = Convert.ToDouble(General.nz(rs.Rows[0]["latitude"], 0));
                    dbLonImm = Convert.ToDouble(General.nz(rs.Rows[0]["longitude"], 0));
                    if (General.IsNumeric(txtGPS_LatDebHZ.Text) && General.IsNumeric(txtGPS_LongiDebHZ.Text))//tested
                    {
                        dbLatDbHZ = Convert.ToDouble(General.nz(txtGPS_LatDebHZ.Text, 0));
                        dbLonDbHZ = Convert.ToDouble(General.nz(txtGPS_LongiDebHZ.Text, 0));
                        txtDistDeb.Text = Convert.ToString(General.FncArrondir(ModGPS.fc_DistanceGPS(dbLatImm, dbLonImm, dbLatDbHZ, dbLonDbHZ, "K"), 3));
                    }

                    if (General.IsNumeric(txtGPS_LatFinbHZ.Text) && General.IsNumeric(txtGPS_LongFinHZ.Text))//tested
                    {
                        dbLatFinHZ = Convert.ToDouble(General.nz(txtGPS_LatFinbHZ.Text, 0));
                        dbLonFinHZ = Convert.ToDouble(General.nz(txtGPS_LongFinHZ.Text, 0));
                        txtDistFin.Text = Convert.ToString(General.FncArrondir(ModGPS.fc_DistanceGPS(dbLatImm, dbLonImm, dbLatFinHZ, dbLonFinHZ, "K"), 3));
                    }

                }

                modAdors.Close();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";frmGeolocInter");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmGeolocInter_Load(object sender, EventArgs e)
        {
            fc_LoadDetInter();
        }
    }
}
