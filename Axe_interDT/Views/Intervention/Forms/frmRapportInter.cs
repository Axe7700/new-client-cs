﻿using Axe_interDT.Shared;
using Axe_interDT.Views.SharedViews;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Intervention.Forms
{
    public partial class frmRapportInter : Form
    {
        public frmRapportInter()
        {
            InitializeComponent();
        }

        private void fc_LoadImages()
        {
            string sSQL;
            DataTable rs = new DataTable();
            ModAdo rsAdo = new ModAdo();
            DataTable rsDataSource = new DataTable();
            try
            {
                sSQL = "SELECT     TypeFichier, Chemin, ID "
                       + " From InterventionDoc "
                       + " Where Nointervention = " + General.nz(txtNoIntervention.Text, 0)
                       + " ORDER BY TypeFichier";

                rs = rsAdo.fc_OpenRecordSet(sSQL);
                rsDataSource.Columns.Add("Attacher");
                rsDataSource.Columns.Add("Type Fichier");
                rsDataSource.Columns.Add("Document");
                rsDataSource.Columns.Add("ID");
                GridDocuments.DataSource = rsDataSource;

                if (rs.Rows.Count > 0)
                {
                    var newRow = rsDataSource.NewRow();
                    newRow["Attacher"] = "1";
                    newRow["Type Fichier"] = rs.Rows[0]["TypeFichier"];
                    newRow["Document"] =General.DossierIntervention + rs.Rows[0]["Chemin"]+"";
                    newRow["ID"] = General.nz(rs.Rows[0]["ID"],0);
                    rsDataSource.Rows.Add(newRow.ItemArray);
                    GridDocuments.DataSource = rsDataSource;
                }
                rs.Dispose();
                rs = null;
            }
            catch(Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadImages()");
            }



        }

        private void cmdIntegrer_Click(object sender, EventArgs e)
        {
            string sSQL;
            DataTable rs = new DataTable();
            ModAdo rsAdo = new ModAdo();
            DataTable rsDoc = new DataTable();
            ModAdo rsDocAdo = new ModAdo();
            int i;
            string sPath;
            string sFileRapport;
            FileStream stm;
            sSQL = "DELETE FROM INP_InterDocPrint WHERE INP_UserName = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'";
            General.Execute(sSQL);

            sSQL = "SELECT     INP_ID, INP_UserName, Nointervention, ID_DOC" 
                   + " From INP_InterDocPrint " 
                   + " WHERE INP_ID  = 0";

            rs = rsAdo.fc_OpenRecordSet(sSQL);
            sSQL = "SELECT     ID, Chemin, Images, TypeFichier From InterventionDoc Where Nointervention = " + txtNoIntervention.Text;

            rsDoc = rsDocAdo.fc_OpenRecordSet(sSQL);

            GridDocuments.UpdateData();
            for (i = 0; i < GridDocuments.Rows.Count; i++)
            {
                if(GridDocuments.Rows[i].Cells[0].Text=="1" || GridDocuments.Rows[i].Cells[0].Text == "-1")
                {
                    if (rsDoc.Rows.Count > 0)
                    {
                        foreach(DataRow rsDocRow in rsDoc.Rows)
                        {
                            if (rsDocRow["ID"] == GridDocuments.Rows[i].Cells["ID"].Value)
                            {
                                //'Debug.Print rsDoc!Id
                                // '=== enregistre le fichier dans la base de données.
                                sPath = General.DossierIntervention + rsDocRow["Chemin"];

                                //'sPath = "C:\temp\image\coucou.png"
                                if (Dossier.fc_ControleFichier(sPath)==true)
                                {
                                    //stm.Type = adTypeBinary 'Const adTypeBinary = 1
                                    //stm.Open
                                    //stm.LoadFromFile sPath
                                    //rsDoc!Images = stm.Read

                                    stm = File.Open(sPath, FileMode.CreateNew);
                                    rsDocRow["Images"] = stm.ReadByte();
                                    rsDocAdo.Update();
                                    stm.Close();
                                    //Debug.Print("convertie");
                                }
                                // '=== ajoute les fichiers à intégrer dans l'état.
                                var row=rs.NewRow();

                                row["INP_UserName"] = General.fncUserName();
                                row["NoIntervention"] = txtNoIntervention.Text;
                                row["ID_DOC"] = rsDocRow["ID_DOC"];
                                rs.Rows.Add(row.ItemArray);
                                rsAdo.Update();
                            }
                        }
                    }
                }
            }

            rsDoc.Dispose();
            rsDoc = null;
            rs.Dispose();
            rs = null;

            General.sFileRapportPhoto = fc_ExportRapport("{Intervention.NoIntervention} ="+ txtNoIntervention.Text
                                         + " AND {INP_InterDocPrint.INP_UserName} ='" + General.fncUserName() + "'", 
                                         General.gsRpt + General.RAPPORTINTERVENTIONV2, "");

            ModuleAPI.Ouvrir(General.sFileRapportPhoto);

        }

        private string fc_ExportRapport(string sSelectionFomula,string sReport,string sSelectSubReport)
        {
            int i;
            int j;

            ReportDocument Report = new ReportDocument();
            string sNomFic;
            int intDeroule;
            string[] tabParameterName;
            string sNomRep;
            bool bRange;// 'Indique qu'une plage de données a été passée en paramètre
            
            Report.Load(sReport);
            bRange = false;
            string functionReturnValue = null;

            try
            {
                sNomFic = "Expt_" + General.Left(General.fncUserName().Replace(" ", ""), 10) + "_" + DateTime.Now.ToString("yymmdd_hhmmss") + ".pdf";

                sNomRep = Environment.GetFolderPath(Environment.SpecialFolder.ApplicationData) + "\\" + Application.ProductName + "\\Export\\";

                intDeroule = 1;
                Dossier.fc_ControleAnCreateDossier(sNomRep);

                intDeroule = 2;
                Dossier.fc_NettoieDossier(sNomRep, "*.pdf");

                if (Dossier.fc_ControleFichier(sNomRep + sNomFic) == true)
                {
                    intDeroule = 22;
                    //'MsgBox intDeroule
                    File.Delete(sNomRep + sNomFic);
                }

                intDeroule = 3;

                Application.DoEvents();

                Report.RecordSelectionFormula = sSelectionFomula;

                intDeroule = 5;

                //Report.ExportOptions.FormatType = crEFTPortableDocFormat;
                intDeroule = 6;

                //Report.ExportOptions.PDFExportAllPages = True;

                intDeroule = 7;

                //Report.ExportOptions.DestinationType = crEDTDiskFile;
                intDeroule = 8;

                //Report.ExportOptions.DiskFileName = sNomRep & sNomFic;
                intDeroule = 9;

                Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, sNomRep + sNomFic);//exporter le crsytlreport en pdf

                //Report.Export False;
                intDeroule = 10;

                //'Set subRpt = Report.OpenSubreport("Photo")
                //'subRpt.RecordSelectionFormula = ""

                functionReturnValue = sNomRep + sNomFic;

                intDeroule = 11;
                return functionReturnValue;
            }
            catch(Exception ex)
            {
                Program.SaveException(ex);
                functionReturnValue = "";
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return functionReturnValue;
            }

        }

        private void fc_stokeimage(int lNoIntervention)
        {
            string sSQL;
            DataTable rs = new DataTable();
            ModAdo rsAdo = new ModAdo();

            string sPath;

            FileStream stm;
            //'---- AJOUTER LE CHAMPS IMAGE DANS LA TABLE InterventionDoc. -------------
            try
            {
                sSQL = "SELECT      Chemin, Images, TypeFichier From InterventionDoc Where Nointervention = " + lNoIntervention;
                rs = rsAdo.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {
                    foreach(DataRow rsDocRow in rs.Rows)
                    {
                        sPath = General.DossierIntervention + rsDocRow["Chemin"];

                        if (Dossier.fc_ControleFichier(sPath) == true)
                        {
                            //stm.Type = adTypeBinary
                            //stm.Open
                            //stm.LoadFromFile sPath
                            //rs!Images = stm.Read

                            stm = File.Open(sPath, FileMode.CreateNew);
                            rsDocRow["Images"] = stm.ReadByte();

                            rsAdo.Update();
                            stm.Close();
 
                        }
                    }
                }
                rs.Dispose();
                rs = null;
                stm = null;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_stokeimage");
            }

        }

        private void frmRapportInter_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_LoadImages();
        }

        private void GridDocuments_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            string sDesPathFile;
            string sFilter= "Documents (*.doc;*.txt;*rtf;*.xls)|*.doc;*.txt;*rtf;*.xls";
            sFilter = sFilter + "|Images (*.gif;*.jpg;*.jpeg;*.bmp;*.pcx)|*.gif;*.jpg;*.jpeg;*.bmp;*.pcx";

            OpenFileDialog openFileDialog1 = new OpenFileDialog();      
            openFileDialog1.Filter = sFilter;          
            openFileDialog1.ShowReadOnly = true;
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {
                GridDocuments.ActiveRow.Cells[0].Value = "1";
                GridDocuments.ActiveRow.Cells[0].Value = openFileDialog1.FileName;
            }

        }

        private void GridDocuments_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            if (!string.IsNullOrEmpty(GridDocuments.ActiveRow.Cells[1].Text))
            {
                ModuleAPI.Ouvrir(GridDocuments.ActiveRow.Cells[1].Text);
            }
        }
    }
}
