﻿namespace Axe_interDT.Views.Intervention.Forms
{
    partial class frmPDAv2P2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.FraP2 = new System.Windows.Forms.GroupBox();
            this.ssGridTachesAnnuel = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmdDevis = new System.Windows.Forms.Button();
            this.cmdCompteur = new System.Windows.Forms.Button();
            this.label22 = new System.Windows.Forms.Label();
            this.GridDoc = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtInfoImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.label21 = new System.Windows.Forms.Label();
            this.chkHeureSup = new System.Windows.Forms.CheckBox();
            this.chkRAS = new System.Windows.Forms.CheckBox();
            this.chkDevisEtablir = new System.Windows.Forms.CheckBox();
            this.txtEnqueteQualite = new iTalk.iTalk_TextBox_Small2();
            this.label18 = new System.Windows.Forms.Label();
            this.txtNomSignataire = new iTalk.iTalk_TextBox_Small2();
            this.label19 = new System.Windows.Forms.Label();
            this.txtPRI_Code = new iTalk.iTalk_TextBox_Small2();
            this.label20 = new System.Windows.Forms.Label();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.Command1 = new System.Windows.Forms.Button();
            this.txtDureePDA = new iTalk.iTalk_TextBox_Small2();
            this.label17 = new System.Windows.Forms.Label();
            this.txtHeureFinPDA = new iTalk.iTalk_TextBox_Small2();
            this.label11 = new System.Windows.Forms.Label();
            this.txtHeureDebutPDA = new iTalk.iTalk_TextBox_Small2();
            this.label12 = new System.Windows.Forms.Label();
            this.txtArrivePrevue = new iTalk.iTalk_TextBox_Small2();
            this.label13 = new System.Windows.Forms.Label();
            this.txtDateHeureCopilot = new iTalk.iTalk_TextBox_Small2();
            this.label14 = new System.Windows.Forms.Label();
            this.txtLuSurPdaLe = new iTalk.iTalk_TextBox_Small2();
            this.label15 = new System.Windows.Forms.Label();
            this.txtRecuSurPdaLe = new iTalk.iTalk_TextBox_Small2();
            this.label16 = new System.Windows.Forms.Label();
            this.txtDuree = new iTalk.iTalk_TextBox_Small2();
            this.label10 = new System.Windows.Forms.Label();
            this.txtHeureFin = new iTalk.iTalk_TextBox_Small2();
            this.label5 = new System.Windows.Forms.Label();
            this.txtHeureDebut = new iTalk.iTalk_TextBox_Small2();
            this.label6 = new System.Windows.Forms.Label();
            this.txtDateRealise = new iTalk.iTalk_TextBox_Small2();
            this.label7 = new System.Windows.Forms.Label();
            this.txtHeurePrevue = new iTalk.iTalk_TextBox_Small2();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDatePrevue = new iTalk.iTalk_TextBox_Small2();
            this.label9 = new System.Windows.Forms.Label();
            this.groupBox2 = new System.Windows.Forms.GroupBox();
            this.chkErrGpsHZFin = new System.Windows.Forms.CheckBox();
            this.chkErrGpsHZDebut = new System.Windows.Forms.CheckBox();
            this.cmbStatutPDA = new iTalk.iTalk_TextBox_Small2();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCodeEtat = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.txtNoInter = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.txtIntervenant = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCodeimmeuble = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.optOperation = new System.Windows.Forms.RadioButton();
            this.ssOpeTaches = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.optTaches = new System.Windows.Forms.RadioButton();
            this.txtNointervention = new iTalk.iTalk_TextBox_Small2();
            this.txtNumFicheStandard = new iTalk.iTalk_TextBox_Small2();
            this.FraP2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridTachesAnnuel)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDoc)).BeginInit();
            this.groupBox3.SuspendLayout();
            this.groupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssOpeTaches)).BeginInit();
            this.SuspendLayout();
            // 
            // FraP2
            // 
            this.FraP2.BackColor = System.Drawing.Color.Transparent;
            this.FraP2.Controls.Add(this.ssGridTachesAnnuel);
            this.FraP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.FraP2.Location = new System.Drawing.Point(12, 557);
            this.FraP2.Name = "FraP2";
            this.FraP2.Size = new System.Drawing.Size(901, 136);
            this.FraP2.TabIndex = 573;
            this.FraP2.TabStop = false;
            // 
            // ssGridTachesAnnuel
            // 
            this.ssGridTachesAnnuel.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssGridTachesAnnuel.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridTachesAnnuel.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridTachesAnnuel.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridTachesAnnuel.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridTachesAnnuel.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ssGridTachesAnnuel.DisplayLayout.UseFixedHeaders = true;
            this.ssGridTachesAnnuel.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssGridTachesAnnuel.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssGridTachesAnnuel.Location = new System.Drawing.Point(3, 18);
            this.ssGridTachesAnnuel.Name = "ssGridTachesAnnuel";
            this.ssGridTachesAnnuel.Size = new System.Drawing.Size(895, 115);
            this.ssGridTachesAnnuel.TabIndex = 572;
            this.ssGridTachesAnnuel.Text = "Visite preventive";
            this.ssGridTachesAnnuel.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridTachesAnnuel_InitializeLayout);
            this.ssGridTachesAnnuel.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssGridTachesAnnuel_InitializeRow);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.cmdDevis);
            this.groupBox1.Controls.Add(this.cmdCompteur);
            this.groupBox1.Controls.Add(this.label22);
            this.groupBox1.Controls.Add(this.GridDoc);
            this.groupBox1.Controls.Add(this.txtInfoImmeuble);
            this.groupBox1.Controls.Add(this.label21);
            this.groupBox1.Controls.Add(this.chkHeureSup);
            this.groupBox1.Controls.Add(this.chkRAS);
            this.groupBox1.Controls.Add(this.chkDevisEtablir);
            this.groupBox1.Controls.Add(this.txtEnqueteQualite);
            this.groupBox1.Controls.Add(this.label18);
            this.groupBox1.Controls.Add(this.txtNomSignataire);
            this.groupBox1.Controls.Add(this.label19);
            this.groupBox1.Controls.Add(this.txtPRI_Code);
            this.groupBox1.Controls.Add(this.label20);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox1.Location = new System.Drawing.Point(12, 270);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(901, 281);
            this.groupBox1.TabIndex = 412;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "RAPPORT";
            // 
            // cmdDevis
            // 
            this.cmdDevis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdDevis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdDevis.FlatAppearance.BorderSize = 0;
            this.cmdDevis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.cmdDevis.ForeColor = System.Drawing.Color.White;
            this.cmdDevis.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdDevis.Location = new System.Drawing.Point(265, 239);
            this.cmdDevis.Margin = new System.Windows.Forms.Padding(2);
            this.cmdDevis.Name = "cmdDevis";
            this.cmdDevis.Size = new System.Drawing.Size(135, 36);
            this.cmdDevis.TabIndex = 578;
            this.cmdDevis.Text = "Devis";
            this.cmdDevis.UseVisualStyleBackColor = false;
            this.cmdDevis.Click += new System.EventHandler(this.cmdDevis_Click);
            // 
            // cmdCompteur
            // 
            this.cmdCompteur.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdCompteur.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCompteur.FlatAppearance.BorderSize = 0;
            this.cmdCompteur.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCompteur.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.cmdCompteur.ForeColor = System.Drawing.Color.White;
            this.cmdCompteur.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdCompteur.Location = new System.Drawing.Point(126, 239);
            this.cmdCompteur.Margin = new System.Windows.Forms.Padding(2);
            this.cmdCompteur.Name = "cmdCompteur";
            this.cmdCompteur.Size = new System.Drawing.Size(135, 36);
            this.cmdCompteur.TabIndex = 538;
            this.cmdCompteur.Text = "Relevés de compteurs";
            this.cmdCompteur.UseVisualStyleBackColor = false;
            this.cmdCompteur.Click += new System.EventHandler(this.cmdCompteur_Click);
            // 
            // label22
            // 
            this.label22.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(419, 18);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(474, 19);
            this.label22.TabIndex = 577;
            this.label22.Text = "Fichiers associés à l\'intervention";
            this.label22.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // GridDoc
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridDoc.DisplayLayout.Appearance = appearance1;
            this.GridDoc.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridDoc.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridDoc.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridDoc.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridDoc.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridDoc.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridDoc.DisplayLayout.MaxColScrollRegions = 1;
            this.GridDoc.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridDoc.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridDoc.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridDoc.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridDoc.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridDoc.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridDoc.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridDoc.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridDoc.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridDoc.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridDoc.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridDoc.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridDoc.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridDoc.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridDoc.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridDoc.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridDoc.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridDoc.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridDoc.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridDoc.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridDoc.Location = new System.Drawing.Point(416, 44);
            this.GridDoc.Name = "GridDoc";
            this.GridDoc.Size = new System.Drawing.Size(477, 216);
            this.GridDoc.TabIndex = 576;
            this.GridDoc.Text = "ultraGrid1";
            this.GridDoc.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridDoc_InitializeLayout);
            this.GridDoc.AfterRowActivate += new System.EventHandler(this.GridDoc_AfterRowActivate);
            this.GridDoc.ClickCell += new Infragistics.Win.UltraWinGrid.ClickCellEventHandler(this.GridDoc_ClickCell);
            this.GridDoc.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.GridDoc_DoubleClickRow_1);
            this.GridDoc.Click += new System.EventHandler(this.GridDoc_Click);
            // 
            // txtInfoImmeuble
            // 
            this.txtInfoImmeuble.AccAcceptNumbersOnly = false;
            this.txtInfoImmeuble.AccAllowComma = false;
            this.txtInfoImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtInfoImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtInfoImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtInfoImmeuble.AccHidenValue = "";
            this.txtInfoImmeuble.AccNotAllowedChars = null;
            this.txtInfoImmeuble.AccReadOnly = false;
            this.txtInfoImmeuble.AccReadOnlyAllowDelete = false;
            this.txtInfoImmeuble.AccRequired = false;
            this.txtInfoImmeuble.BackColor = System.Drawing.Color.White;
            this.txtInfoImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtInfoImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtInfoImmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtInfoImmeuble.Location = new System.Drawing.Point(126, 165);
            this.txtInfoImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtInfoImmeuble.MaxLength = 32767;
            this.txtInfoImmeuble.Multiline = true;
            this.txtInfoImmeuble.Name = "txtInfoImmeuble";
            this.txtInfoImmeuble.ReadOnly = true;
            this.txtInfoImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtInfoImmeuble.Size = new System.Drawing.Size(278, 70);
            this.txtInfoImmeuble.TabIndex = 575;
            this.txtInfoImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtInfoImmeuble.UseSystemPasswordChar = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label21.Location = new System.Drawing.Point(7, 163);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(108, 19);
            this.label21.TabIndex = 574;
            this.label21.Text = "Info Immeuble";
            // 
            // chkHeureSup
            // 
            this.chkHeureSup.AutoSize = true;
            this.chkHeureSup.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkHeureSup.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkHeureSup.Location = new System.Drawing.Point(7, 139);
            this.chkHeureSup.Name = "chkHeureSup";
            this.chkHeureSup.Size = new System.Drawing.Size(156, 23);
            this.chkHeureSup.TabIndex = 573;
            this.chkHeureSup.Text = "H. supplémentaire";
            this.chkHeureSup.UseVisualStyleBackColor = true;
            // 
            // chkRAS
            // 
            this.chkRAS.AutoSize = true;
            this.chkRAS.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkRAS.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkRAS.Location = new System.Drawing.Point(169, 112);
            this.chkRAS.Name = "chkRAS";
            this.chkRAS.Size = new System.Drawing.Size(53, 23);
            this.chkRAS.TabIndex = 572;
            this.chkRAS.Text = "RAS";
            this.chkRAS.UseVisualStyleBackColor = true;
            // 
            // chkDevisEtablir
            // 
            this.chkDevisEtablir.AutoSize = true;
            this.chkDevisEtablir.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDevisEtablir.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkDevisEtablir.Location = new System.Drawing.Point(7, 112);
            this.chkDevisEtablir.Name = "chkDevisEtablir";
            this.chkDevisEtablir.Size = new System.Drawing.Size(155, 23);
            this.chkDevisEtablir.TabIndex = 571;
            this.chkDevisEtablir.Text = "Demande de devis";
            this.chkDevisEtablir.UseVisualStyleBackColor = true;
            // 
            // txtEnqueteQualite
            // 
            this.txtEnqueteQualite.AccAcceptNumbersOnly = false;
            this.txtEnqueteQualite.AccAllowComma = false;
            this.txtEnqueteQualite.AccBackgroundColor = System.Drawing.Color.White;
            this.txtEnqueteQualite.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtEnqueteQualite.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtEnqueteQualite.AccHidenValue = "";
            this.txtEnqueteQualite.AccNotAllowedChars = null;
            this.txtEnqueteQualite.AccReadOnly = false;
            this.txtEnqueteQualite.AccReadOnlyAllowDelete = false;
            this.txtEnqueteQualite.AccRequired = false;
            this.txtEnqueteQualite.BackColor = System.Drawing.Color.White;
            this.txtEnqueteQualite.CustomBackColor = System.Drawing.Color.White;
            this.txtEnqueteQualite.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtEnqueteQualite.ForeColor = System.Drawing.Color.Black;
            this.txtEnqueteQualite.Location = new System.Drawing.Point(137, 80);
            this.txtEnqueteQualite.Margin = new System.Windows.Forms.Padding(2);
            this.txtEnqueteQualite.MaxLength = 32767;
            this.txtEnqueteQualite.Multiline = false;
            this.txtEnqueteQualite.Name = "txtEnqueteQualite";
            this.txtEnqueteQualite.ReadOnly = true;
            this.txtEnqueteQualite.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtEnqueteQualite.Size = new System.Drawing.Size(274, 27);
            this.txtEnqueteQualite.TabIndex = 513;
            this.txtEnqueteQualite.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtEnqueteQualite.UseSystemPasswordChar = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label18.Location = new System.Drawing.Point(9, 80);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(120, 19);
            this.label18.TabIndex = 512;
            this.label18.Text = "Enquête qualité";
            // 
            // txtNomSignataire
            // 
            this.txtNomSignataire.AccAcceptNumbersOnly = false;
            this.txtNomSignataire.AccAllowComma = false;
            this.txtNomSignataire.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNomSignataire.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNomSignataire.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNomSignataire.AccHidenValue = "";
            this.txtNomSignataire.AccNotAllowedChars = null;
            this.txtNomSignataire.AccReadOnly = false;
            this.txtNomSignataire.AccReadOnlyAllowDelete = false;
            this.txtNomSignataire.AccRequired = false;
            this.txtNomSignataire.BackColor = System.Drawing.Color.White;
            this.txtNomSignataire.CustomBackColor = System.Drawing.Color.White;
            this.txtNomSignataire.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNomSignataire.ForeColor = System.Drawing.Color.Black;
            this.txtNomSignataire.Location = new System.Drawing.Point(137, 49);
            this.txtNomSignataire.Margin = new System.Windows.Forms.Padding(2);
            this.txtNomSignataire.MaxLength = 32767;
            this.txtNomSignataire.Multiline = false;
            this.txtNomSignataire.Name = "txtNomSignataire";
            this.txtNomSignataire.ReadOnly = true;
            this.txtNomSignataire.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNomSignataire.Size = new System.Drawing.Size(274, 27);
            this.txtNomSignataire.TabIndex = 511;
            this.txtNomSignataire.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNomSignataire.UseSystemPasswordChar = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label19.Location = new System.Drawing.Point(6, 49);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(134, 19);
            this.label19.TabIndex = 510;
            this.label19.Text = "Nom du signataire";
            // 
            // txtPRI_Code
            // 
            this.txtPRI_Code.AccAcceptNumbersOnly = false;
            this.txtPRI_Code.AccAllowComma = false;
            this.txtPRI_Code.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRI_Code.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRI_Code.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtPRI_Code.AccHidenValue = "";
            this.txtPRI_Code.AccNotAllowedChars = null;
            this.txtPRI_Code.AccReadOnly = false;
            this.txtPRI_Code.AccReadOnlyAllowDelete = false;
            this.txtPRI_Code.AccRequired = false;
            this.txtPRI_Code.BackColor = System.Drawing.Color.White;
            this.txtPRI_Code.CustomBackColor = System.Drawing.Color.White;
            this.txtPRI_Code.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtPRI_Code.ForeColor = System.Drawing.Color.Black;
            this.txtPRI_Code.Location = new System.Drawing.Point(137, 18);
            this.txtPRI_Code.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRI_Code.MaxLength = 32767;
            this.txtPRI_Code.Multiline = false;
            this.txtPRI_Code.Name = "txtPRI_Code";
            this.txtPRI_Code.ReadOnly = true;
            this.txtPRI_Code.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRI_Code.Size = new System.Drawing.Size(274, 27);
            this.txtPRI_Code.TabIndex = 509;
            this.txtPRI_Code.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRI_Code.UseSystemPasswordChar = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label20.Location = new System.Drawing.Point(6, 18);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(86, 19);
            this.label20.TabIndex = 508;
            this.label20.Text = "Type prime";
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.Command1);
            this.groupBox3.Controls.Add(this.txtDureePDA);
            this.groupBox3.Controls.Add(this.label17);
            this.groupBox3.Controls.Add(this.txtHeureFinPDA);
            this.groupBox3.Controls.Add(this.label11);
            this.groupBox3.Controls.Add(this.txtHeureDebutPDA);
            this.groupBox3.Controls.Add(this.label12);
            this.groupBox3.Controls.Add(this.txtArrivePrevue);
            this.groupBox3.Controls.Add(this.label13);
            this.groupBox3.Controls.Add(this.txtDateHeureCopilot);
            this.groupBox3.Controls.Add(this.label14);
            this.groupBox3.Controls.Add(this.txtLuSurPdaLe);
            this.groupBox3.Controls.Add(this.label15);
            this.groupBox3.Controls.Add(this.txtRecuSurPdaLe);
            this.groupBox3.Controls.Add(this.label16);
            this.groupBox3.Controls.Add(this.txtDuree);
            this.groupBox3.Controls.Add(this.label10);
            this.groupBox3.Controls.Add(this.txtHeureFin);
            this.groupBox3.Controls.Add(this.label5);
            this.groupBox3.Controls.Add(this.txtHeureDebut);
            this.groupBox3.Controls.Add(this.label6);
            this.groupBox3.Controls.Add(this.txtDateRealise);
            this.groupBox3.Controls.Add(this.label7);
            this.groupBox3.Controls.Add(this.txtHeurePrevue);
            this.groupBox3.Controls.Add(this.label8);
            this.groupBox3.Controls.Add(this.txtDatePrevue);
            this.groupBox3.Controls.Add(this.label9);
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox3.Location = new System.Drawing.Point(275, 17);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(638, 247);
            this.groupBox3.TabIndex = 412;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "HORODATAGE";
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Microsoft Sans Serif", 8F);
            this.Command1.ForeColor = System.Drawing.Color.White;
            this.Command1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command1.Location = new System.Drawing.Point(9, 209);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(285, 21);
            this.Command1.TabIndex = 413;
            this.Command1.Text = "Données brutes";
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // txtDureePDA
            // 
            this.txtDureePDA.AccAcceptNumbersOnly = false;
            this.txtDureePDA.AccAllowComma = false;
            this.txtDureePDA.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDureePDA.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDureePDA.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDureePDA.AccHidenValue = "";
            this.txtDureePDA.AccNotAllowedChars = null;
            this.txtDureePDA.AccReadOnly = false;
            this.txtDureePDA.AccReadOnlyAllowDelete = false;
            this.txtDureePDA.AccRequired = false;
            this.txtDureePDA.BackColor = System.Drawing.Color.White;
            this.txtDureePDA.CustomBackColor = System.Drawing.Color.White;
            this.txtDureePDA.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDureePDA.ForeColor = System.Drawing.Color.Black;
            this.txtDureePDA.Location = new System.Drawing.Point(450, 203);
            this.txtDureePDA.Margin = new System.Windows.Forms.Padding(2);
            this.txtDureePDA.MaxLength = 32767;
            this.txtDureePDA.Multiline = false;
            this.txtDureePDA.Name = "txtDureePDA";
            this.txtDureePDA.ReadOnly = true;
            this.txtDureePDA.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDureePDA.Size = new System.Drawing.Size(180, 27);
            this.txtDureePDA.TabIndex = 537;
            this.txtDureePDA.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDureePDA.UseSystemPasswordChar = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label17.Location = new System.Drawing.Point(298, 205);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 19);
            this.label17.TabIndex = 536;
            this.label17.Text = "Durée PDA";
            // 
            // txtHeureFinPDA
            // 
            this.txtHeureFinPDA.AccAcceptNumbersOnly = false;
            this.txtHeureFinPDA.AccAllowComma = false;
            this.txtHeureFinPDA.AccBackgroundColor = System.Drawing.Color.White;
            this.txtHeureFinPDA.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtHeureFinPDA.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtHeureFinPDA.AccHidenValue = "";
            this.txtHeureFinPDA.AccNotAllowedChars = null;
            this.txtHeureFinPDA.AccReadOnly = false;
            this.txtHeureFinPDA.AccReadOnlyAllowDelete = false;
            this.txtHeureFinPDA.AccRequired = false;
            this.txtHeureFinPDA.BackColor = System.Drawing.Color.White;
            this.txtHeureFinPDA.CustomBackColor = System.Drawing.Color.White;
            this.txtHeureFinPDA.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtHeureFinPDA.ForeColor = System.Drawing.Color.Black;
            this.txtHeureFinPDA.Location = new System.Drawing.Point(450, 172);
            this.txtHeureFinPDA.Margin = new System.Windows.Forms.Padding(2);
            this.txtHeureFinPDA.MaxLength = 32767;
            this.txtHeureFinPDA.Multiline = false;
            this.txtHeureFinPDA.Name = "txtHeureFinPDA";
            this.txtHeureFinPDA.ReadOnly = true;
            this.txtHeureFinPDA.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtHeureFinPDA.Size = new System.Drawing.Size(180, 27);
            this.txtHeureFinPDA.TabIndex = 535;
            this.txtHeureFinPDA.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtHeureFinPDA.UseSystemPasswordChar = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label11.Location = new System.Drawing.Point(298, 173);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(108, 19);
            this.label11.TabIndex = 534;
            this.label11.Text = "Heure Fin PDA";
            // 
            // txtHeureDebutPDA
            // 
            this.txtHeureDebutPDA.AccAcceptNumbersOnly = false;
            this.txtHeureDebutPDA.AccAllowComma = false;
            this.txtHeureDebutPDA.AccBackgroundColor = System.Drawing.Color.White;
            this.txtHeureDebutPDA.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtHeureDebutPDA.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtHeureDebutPDA.AccHidenValue = "";
            this.txtHeureDebutPDA.AccNotAllowedChars = null;
            this.txtHeureDebutPDA.AccReadOnly = false;
            this.txtHeureDebutPDA.AccReadOnlyAllowDelete = false;
            this.txtHeureDebutPDA.AccRequired = false;
            this.txtHeureDebutPDA.BackColor = System.Drawing.Color.White;
            this.txtHeureDebutPDA.CustomBackColor = System.Drawing.Color.White;
            this.txtHeureDebutPDA.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtHeureDebutPDA.ForeColor = System.Drawing.Color.Black;
            this.txtHeureDebutPDA.Location = new System.Drawing.Point(450, 141);
            this.txtHeureDebutPDA.Margin = new System.Windows.Forms.Padding(2);
            this.txtHeureDebutPDA.MaxLength = 32767;
            this.txtHeureDebutPDA.Multiline = false;
            this.txtHeureDebutPDA.Name = "txtHeureDebutPDA";
            this.txtHeureDebutPDA.ReadOnly = true;
            this.txtHeureDebutPDA.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtHeureDebutPDA.Size = new System.Drawing.Size(180, 27);
            this.txtHeureDebutPDA.TabIndex = 533;
            this.txtHeureDebutPDA.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtHeureDebutPDA.UseSystemPasswordChar = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label12.Location = new System.Drawing.Point(298, 142);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(130, 19);
            this.label12.TabIndex = 532;
            this.label12.Text = "Heure début PDA";
            // 
            // txtArrivePrevue
            // 
            this.txtArrivePrevue.AccAcceptNumbersOnly = false;
            this.txtArrivePrevue.AccAllowComma = false;
            this.txtArrivePrevue.AccBackgroundColor = System.Drawing.Color.White;
            this.txtArrivePrevue.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtArrivePrevue.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtArrivePrevue.AccHidenValue = "";
            this.txtArrivePrevue.AccNotAllowedChars = null;
            this.txtArrivePrevue.AccReadOnly = false;
            this.txtArrivePrevue.AccReadOnlyAllowDelete = false;
            this.txtArrivePrevue.AccRequired = false;
            this.txtArrivePrevue.BackColor = System.Drawing.Color.White;
            this.txtArrivePrevue.CustomBackColor = System.Drawing.Color.White;
            this.txtArrivePrevue.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtArrivePrevue.ForeColor = System.Drawing.Color.Black;
            this.txtArrivePrevue.Location = new System.Drawing.Point(450, 110);
            this.txtArrivePrevue.Margin = new System.Windows.Forms.Padding(2);
            this.txtArrivePrevue.MaxLength = 32767;
            this.txtArrivePrevue.Multiline = false;
            this.txtArrivePrevue.Name = "txtArrivePrevue";
            this.txtArrivePrevue.ReadOnly = true;
            this.txtArrivePrevue.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtArrivePrevue.Size = new System.Drawing.Size(180, 27);
            this.txtArrivePrevue.TabIndex = 531;
            this.txtArrivePrevue.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtArrivePrevue.UseSystemPasswordChar = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label13.Location = new System.Drawing.Point(298, 111);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(176, 19);
            this.label13.TabIndex = 530;
            this.label13.Text = "Heure d\'arrivée estimée";
            // 
            // txtDateHeureCopilot
            // 
            this.txtDateHeureCopilot.AccAcceptNumbersOnly = false;
            this.txtDateHeureCopilot.AccAllowComma = false;
            this.txtDateHeureCopilot.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateHeureCopilot.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateHeureCopilot.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateHeureCopilot.AccHidenValue = "";
            this.txtDateHeureCopilot.AccNotAllowedChars = null;
            this.txtDateHeureCopilot.AccReadOnly = false;
            this.txtDateHeureCopilot.AccReadOnlyAllowDelete = false;
            this.txtDateHeureCopilot.AccRequired = false;
            this.txtDateHeureCopilot.BackColor = System.Drawing.Color.White;
            this.txtDateHeureCopilot.CustomBackColor = System.Drawing.Color.White;
            this.txtDateHeureCopilot.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDateHeureCopilot.ForeColor = System.Drawing.Color.Black;
            this.txtDateHeureCopilot.Location = new System.Drawing.Point(450, 79);
            this.txtDateHeureCopilot.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateHeureCopilot.MaxLength = 32767;
            this.txtDateHeureCopilot.Multiline = false;
            this.txtDateHeureCopilot.Name = "txtDateHeureCopilot";
            this.txtDateHeureCopilot.ReadOnly = true;
            this.txtDateHeureCopilot.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateHeureCopilot.Size = new System.Drawing.Size(180, 27);
            this.txtDateHeureCopilot.TabIndex = 529;
            this.txtDateHeureCopilot.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateHeureCopilot.UseSystemPasswordChar = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label14.Location = new System.Drawing.Point(298, 80);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(149, 19);
            this.label14.TabIndex = 528;
            this.label14.Text = "Démarrer à (copilot)";
            // 
            // txtLuSurPdaLe
            // 
            this.txtLuSurPdaLe.AccAcceptNumbersOnly = false;
            this.txtLuSurPdaLe.AccAllowComma = false;
            this.txtLuSurPdaLe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLuSurPdaLe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLuSurPdaLe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLuSurPdaLe.AccHidenValue = "";
            this.txtLuSurPdaLe.AccNotAllowedChars = null;
            this.txtLuSurPdaLe.AccReadOnly = false;
            this.txtLuSurPdaLe.AccReadOnlyAllowDelete = false;
            this.txtLuSurPdaLe.AccRequired = false;
            this.txtLuSurPdaLe.BackColor = System.Drawing.Color.White;
            this.txtLuSurPdaLe.CustomBackColor = System.Drawing.Color.White;
            this.txtLuSurPdaLe.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtLuSurPdaLe.ForeColor = System.Drawing.Color.Black;
            this.txtLuSurPdaLe.Location = new System.Drawing.Point(450, 48);
            this.txtLuSurPdaLe.Margin = new System.Windows.Forms.Padding(2);
            this.txtLuSurPdaLe.MaxLength = 32767;
            this.txtLuSurPdaLe.Multiline = false;
            this.txtLuSurPdaLe.Name = "txtLuSurPdaLe";
            this.txtLuSurPdaLe.ReadOnly = true;
            this.txtLuSurPdaLe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLuSurPdaLe.Size = new System.Drawing.Size(180, 27);
            this.txtLuSurPdaLe.TabIndex = 527;
            this.txtLuSurPdaLe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLuSurPdaLe.UseSystemPasswordChar = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label15.Location = new System.Drawing.Point(298, 49);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(46, 19);
            this.label15.TabIndex = 526;
            this.label15.Text = "Lu Le";
            // 
            // txtRecuSurPdaLe
            // 
            this.txtRecuSurPdaLe.AccAcceptNumbersOnly = false;
            this.txtRecuSurPdaLe.AccAllowComma = false;
            this.txtRecuSurPdaLe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRecuSurPdaLe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRecuSurPdaLe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRecuSurPdaLe.AccHidenValue = "";
            this.txtRecuSurPdaLe.AccNotAllowedChars = null;
            this.txtRecuSurPdaLe.AccReadOnly = false;
            this.txtRecuSurPdaLe.AccReadOnlyAllowDelete = false;
            this.txtRecuSurPdaLe.AccRequired = false;
            this.txtRecuSurPdaLe.BackColor = System.Drawing.Color.White;
            this.txtRecuSurPdaLe.CustomBackColor = System.Drawing.Color.White;
            this.txtRecuSurPdaLe.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtRecuSurPdaLe.ForeColor = System.Drawing.Color.Black;
            this.txtRecuSurPdaLe.Location = new System.Drawing.Point(450, 17);
            this.txtRecuSurPdaLe.Margin = new System.Windows.Forms.Padding(2);
            this.txtRecuSurPdaLe.MaxLength = 32767;
            this.txtRecuSurPdaLe.Multiline = false;
            this.txtRecuSurPdaLe.Name = "txtRecuSurPdaLe";
            this.txtRecuSurPdaLe.ReadOnly = true;
            this.txtRecuSurPdaLe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRecuSurPdaLe.Size = new System.Drawing.Size(180, 27);
            this.txtRecuSurPdaLe.TabIndex = 525;
            this.txtRecuSurPdaLe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRecuSurPdaLe.UseSystemPasswordChar = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label16.Location = new System.Drawing.Point(298, 23);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(59, 19);
            this.label16.TabIndex = 524;
            this.label16.Text = "Reçu le";
            // 
            // txtDuree
            // 
            this.txtDuree.AccAcceptNumbersOnly = false;
            this.txtDuree.AccAllowComma = false;
            this.txtDuree.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDuree.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDuree.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDuree.AccHidenValue = "";
            this.txtDuree.AccNotAllowedChars = null;
            this.txtDuree.AccReadOnly = false;
            this.txtDuree.AccReadOnlyAllowDelete = false;
            this.txtDuree.AccRequired = false;
            this.txtDuree.BackColor = System.Drawing.Color.White;
            this.txtDuree.CustomBackColor = System.Drawing.Color.White;
            this.txtDuree.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDuree.ForeColor = System.Drawing.Color.Black;
            this.txtDuree.Location = new System.Drawing.Point(114, 173);
            this.txtDuree.Margin = new System.Windows.Forms.Padding(2);
            this.txtDuree.MaxLength = 32767;
            this.txtDuree.Multiline = false;
            this.txtDuree.Name = "txtDuree";
            this.txtDuree.ReadOnly = true;
            this.txtDuree.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDuree.Size = new System.Drawing.Size(180, 27);
            this.txtDuree.TabIndex = 523;
            this.txtDuree.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDuree.UseSystemPasswordChar = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label10.Location = new System.Drawing.Point(5, 174);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 19);
            this.label10.TabIndex = 522;
            this.label10.Text = "Durée";
            // 
            // txtHeureFin
            // 
            this.txtHeureFin.AccAcceptNumbersOnly = false;
            this.txtHeureFin.AccAllowComma = false;
            this.txtHeureFin.AccBackgroundColor = System.Drawing.Color.White;
            this.txtHeureFin.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtHeureFin.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtHeureFin.AccHidenValue = "";
            this.txtHeureFin.AccNotAllowedChars = null;
            this.txtHeureFin.AccReadOnly = false;
            this.txtHeureFin.AccReadOnlyAllowDelete = false;
            this.txtHeureFin.AccRequired = false;
            this.txtHeureFin.BackColor = System.Drawing.Color.White;
            this.txtHeureFin.CustomBackColor = System.Drawing.Color.White;
            this.txtHeureFin.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtHeureFin.ForeColor = System.Drawing.Color.Black;
            this.txtHeureFin.Location = new System.Drawing.Point(114, 142);
            this.txtHeureFin.Margin = new System.Windows.Forms.Padding(2);
            this.txtHeureFin.MaxLength = 32767;
            this.txtHeureFin.Multiline = false;
            this.txtHeureFin.Name = "txtHeureFin";
            this.txtHeureFin.ReadOnly = true;
            this.txtHeureFin.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtHeureFin.Size = new System.Drawing.Size(180, 27);
            this.txtHeureFin.TabIndex = 521;
            this.txtHeureFin.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtHeureFin.UseSystemPasswordChar = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label5.Location = new System.Drawing.Point(5, 143);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(76, 19);
            this.label5.TabIndex = 520;
            this.label5.Text = "Heure Fin";
            // 
            // txtHeureDebut
            // 
            this.txtHeureDebut.AccAcceptNumbersOnly = false;
            this.txtHeureDebut.AccAllowComma = false;
            this.txtHeureDebut.AccBackgroundColor = System.Drawing.Color.White;
            this.txtHeureDebut.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtHeureDebut.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtHeureDebut.AccHidenValue = "";
            this.txtHeureDebut.AccNotAllowedChars = null;
            this.txtHeureDebut.AccReadOnly = false;
            this.txtHeureDebut.AccReadOnlyAllowDelete = false;
            this.txtHeureDebut.AccRequired = false;
            this.txtHeureDebut.BackColor = System.Drawing.Color.White;
            this.txtHeureDebut.CustomBackColor = System.Drawing.Color.White;
            this.txtHeureDebut.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtHeureDebut.ForeColor = System.Drawing.Color.Black;
            this.txtHeureDebut.Location = new System.Drawing.Point(114, 111);
            this.txtHeureDebut.Margin = new System.Windows.Forms.Padding(2);
            this.txtHeureDebut.MaxLength = 32767;
            this.txtHeureDebut.Multiline = false;
            this.txtHeureDebut.Name = "txtHeureDebut";
            this.txtHeureDebut.ReadOnly = true;
            this.txtHeureDebut.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtHeureDebut.Size = new System.Drawing.Size(180, 27);
            this.txtHeureDebut.TabIndex = 519;
            this.txtHeureDebut.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtHeureDebut.UseSystemPasswordChar = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label6.Location = new System.Drawing.Point(5, 112);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(98, 19);
            this.label6.TabIndex = 518;
            this.label6.Text = "Heure début";
            // 
            // txtDateRealise
            // 
            this.txtDateRealise.AccAcceptNumbersOnly = false;
            this.txtDateRealise.AccAllowComma = false;
            this.txtDateRealise.AccBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDateRealise.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateRealise.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateRealise.AccHidenValue = "";
            this.txtDateRealise.AccNotAllowedChars = null;
            this.txtDateRealise.AccReadOnly = false;
            this.txtDateRealise.AccReadOnlyAllowDelete = false;
            this.txtDateRealise.AccRequired = false;
            this.txtDateRealise.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDateRealise.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDateRealise.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDateRealise.ForeColor = System.Drawing.Color.Black;
            this.txtDateRealise.Location = new System.Drawing.Point(114, 80);
            this.txtDateRealise.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateRealise.MaxLength = 32767;
            this.txtDateRealise.Multiline = false;
            this.txtDateRealise.Name = "txtDateRealise";
            this.txtDateRealise.ReadOnly = true;
            this.txtDateRealise.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateRealise.Size = new System.Drawing.Size(180, 27);
            this.txtDateRealise.TabIndex = 517;
            this.txtDateRealise.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateRealise.UseSystemPasswordChar = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label7.Location = new System.Drawing.Point(5, 81);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(103, 19);
            this.label7.TabIndex = 516;
            this.label7.Text = "Date Réalisée";
            // 
            // txtHeurePrevue
            // 
            this.txtHeurePrevue.AccAcceptNumbersOnly = false;
            this.txtHeurePrevue.AccAllowComma = false;
            this.txtHeurePrevue.AccBackgroundColor = System.Drawing.Color.White;
            this.txtHeurePrevue.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtHeurePrevue.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtHeurePrevue.AccHidenValue = "";
            this.txtHeurePrevue.AccNotAllowedChars = null;
            this.txtHeurePrevue.AccReadOnly = false;
            this.txtHeurePrevue.AccReadOnlyAllowDelete = false;
            this.txtHeurePrevue.AccRequired = false;
            this.txtHeurePrevue.BackColor = System.Drawing.Color.White;
            this.txtHeurePrevue.CustomBackColor = System.Drawing.Color.White;
            this.txtHeurePrevue.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtHeurePrevue.ForeColor = System.Drawing.Color.Black;
            this.txtHeurePrevue.Location = new System.Drawing.Point(114, 49);
            this.txtHeurePrevue.Margin = new System.Windows.Forms.Padding(2);
            this.txtHeurePrevue.MaxLength = 32767;
            this.txtHeurePrevue.Multiline = false;
            this.txtHeurePrevue.Name = "txtHeurePrevue";
            this.txtHeurePrevue.ReadOnly = true;
            this.txtHeurePrevue.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtHeurePrevue.Size = new System.Drawing.Size(180, 27);
            this.txtHeurePrevue.TabIndex = 515;
            this.txtHeurePrevue.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtHeurePrevue.UseSystemPasswordChar = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label8.Location = new System.Drawing.Point(5, 50);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(105, 19);
            this.label8.TabIndex = 514;
            this.label8.Text = "Heure prévue";
            // 
            // txtDatePrevue
            // 
            this.txtDatePrevue.AccAcceptNumbersOnly = false;
            this.txtDatePrevue.AccAllowComma = false;
            this.txtDatePrevue.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDatePrevue.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDatePrevue.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDatePrevue.AccHidenValue = "";
            this.txtDatePrevue.AccNotAllowedChars = null;
            this.txtDatePrevue.AccReadOnly = false;
            this.txtDatePrevue.AccReadOnlyAllowDelete = false;
            this.txtDatePrevue.AccRequired = false;
            this.txtDatePrevue.BackColor = System.Drawing.Color.White;
            this.txtDatePrevue.CustomBackColor = System.Drawing.Color.White;
            this.txtDatePrevue.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDatePrevue.ForeColor = System.Drawing.Color.Black;
            this.txtDatePrevue.Location = new System.Drawing.Point(114, 18);
            this.txtDatePrevue.Margin = new System.Windows.Forms.Padding(2);
            this.txtDatePrevue.MaxLength = 32767;
            this.txtDatePrevue.Multiline = false;
            this.txtDatePrevue.Name = "txtDatePrevue";
            this.txtDatePrevue.ReadOnly = true;
            this.txtDatePrevue.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDatePrevue.Size = new System.Drawing.Size(180, 27);
            this.txtDatePrevue.TabIndex = 513;
            this.txtDatePrevue.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDatePrevue.UseSystemPasswordChar = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label9.Location = new System.Drawing.Point(5, 19);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(95, 19);
            this.label9.TabIndex = 512;
            this.label9.Text = "Date prévue";
            // 
            // groupBox2
            // 
            this.groupBox2.BackColor = System.Drawing.Color.Transparent;
            this.groupBox2.Controls.Add(this.chkErrGpsHZFin);
            this.groupBox2.Controls.Add(this.chkErrGpsHZDebut);
            this.groupBox2.Controls.Add(this.cmbStatutPDA);
            this.groupBox2.Controls.Add(this.label4);
            this.groupBox2.Controls.Add(this.txtCodeEtat);
            this.groupBox2.Controls.Add(this.label3);
            this.groupBox2.Controls.Add(this.txtNoInter);
            this.groupBox2.Controls.Add(this.label2);
            this.groupBox2.Controls.Add(this.txtIntervenant);
            this.groupBox2.Controls.Add(this.label1);
            this.groupBox2.Controls.Add(this.txtCodeimmeuble);
            this.groupBox2.Controls.Add(this.label33);
            this.groupBox2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox2.Location = new System.Drawing.Point(12, 12);
            this.groupBox2.Name = "groupBox2";
            this.groupBox2.Size = new System.Drawing.Size(257, 252);
            this.groupBox2.TabIndex = 411;
            this.groupBox2.TabStop = false;
            this.groupBox2.Text = "IDENTIFICATION";
            // 
            // chkErrGpsHZFin
            // 
            this.chkErrGpsHZFin.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkErrGpsHZFin.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkErrGpsHZFin.Location = new System.Drawing.Point(9, 204);
            this.chkErrGpsHZFin.Name = "chkErrGpsHZFin";
            this.chkErrGpsHZFin.Size = new System.Drawing.Size(213, 21);
            this.chkErrGpsHZFin.TabIndex = 571;
            this.chkErrGpsHZFin.Text = "Anomalie GPS fin hors zone";
            this.chkErrGpsHZFin.UseVisualStyleBackColor = true;
            // 
            // chkErrGpsHZDebut
            // 
            this.chkErrGpsHZDebut.AutoSize = true;
            this.chkErrGpsHZDebut.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkErrGpsHZDebut.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkErrGpsHZDebut.Location = new System.Drawing.Point(9, 177);
            this.chkErrGpsHZDebut.Name = "chkErrGpsHZDebut";
            this.chkErrGpsHZDebut.Size = new System.Drawing.Size(238, 23);
            this.chkErrGpsHZDebut.TabIndex = 570;
            this.chkErrGpsHZDebut.Text = "Anomalie GPS début hors zone";
            this.chkErrGpsHZDebut.UseVisualStyleBackColor = true;
            // 
            // cmbStatutPDA
            // 
            this.cmbStatutPDA.AccAcceptNumbersOnly = false;
            this.cmbStatutPDA.AccAllowComma = false;
            this.cmbStatutPDA.AccBackgroundColor = System.Drawing.Color.White;
            this.cmbStatutPDA.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.cmbStatutPDA.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.cmbStatutPDA.AccHidenValue = "";
            this.cmbStatutPDA.AccNotAllowedChars = null;
            this.cmbStatutPDA.AccReadOnly = false;
            this.cmbStatutPDA.AccReadOnlyAllowDelete = false;
            this.cmbStatutPDA.AccRequired = false;
            this.cmbStatutPDA.BackColor = System.Drawing.Color.White;
            this.cmbStatutPDA.CustomBackColor = System.Drawing.Color.White;
            this.cmbStatutPDA.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbStatutPDA.ForeColor = System.Drawing.Color.Black;
            this.cmbStatutPDA.Location = new System.Drawing.Point(123, 144);
            this.cmbStatutPDA.Margin = new System.Windows.Forms.Padding(2);
            this.cmbStatutPDA.MaxLength = 32767;
            this.cmbStatutPDA.Multiline = false;
            this.cmbStatutPDA.Name = "cmbStatutPDA";
            this.cmbStatutPDA.ReadOnly = true;
            this.cmbStatutPDA.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.cmbStatutPDA.Size = new System.Drawing.Size(124, 27);
            this.cmbStatutPDA.TabIndex = 511;
            this.cmbStatutPDA.TextAlignment = Infragistics.Win.HAlign.Left;
            this.cmbStatutPDA.UseSystemPasswordChar = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label4.Location = new System.Drawing.Point(1, 145);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(84, 19);
            this.label4.TabIndex = 510;
            this.label4.Text = "Statut PDA";
            // 
            // txtCodeEtat
            // 
            this.txtCodeEtat.AccAcceptNumbersOnly = false;
            this.txtCodeEtat.AccAllowComma = false;
            this.txtCodeEtat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeEtat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeEtat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeEtat.AccHidenValue = "";
            this.txtCodeEtat.AccNotAllowedChars = null;
            this.txtCodeEtat.AccReadOnly = false;
            this.txtCodeEtat.AccReadOnlyAllowDelete = false;
            this.txtCodeEtat.AccRequired = false;
            this.txtCodeEtat.BackColor = System.Drawing.Color.White;
            this.txtCodeEtat.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeEtat.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeEtat.ForeColor = System.Drawing.Color.Black;
            this.txtCodeEtat.Location = new System.Drawing.Point(123, 113);
            this.txtCodeEtat.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeEtat.MaxLength = 32767;
            this.txtCodeEtat.Multiline = false;
            this.txtCodeEtat.Name = "txtCodeEtat";
            this.txtCodeEtat.ReadOnly = true;
            this.txtCodeEtat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeEtat.Size = new System.Drawing.Size(124, 27);
            this.txtCodeEtat.TabIndex = 509;
            this.txtCodeEtat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeEtat.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(1, 114);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(81, 19);
            this.label3.TabIndex = 508;
            this.label3.Text = "Code état ";
            // 
            // txtNoInter
            // 
            this.txtNoInter.AccAcceptNumbersOnly = false;
            this.txtNoInter.AccAllowComma = false;
            this.txtNoInter.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoInter.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoInter.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoInter.AccHidenValue = "";
            this.txtNoInter.AccNotAllowedChars = null;
            this.txtNoInter.AccReadOnly = false;
            this.txtNoInter.AccReadOnlyAllowDelete = false;
            this.txtNoInter.AccRequired = false;
            this.txtNoInter.BackColor = System.Drawing.Color.White;
            this.txtNoInter.CustomBackColor = System.Drawing.Color.White;
            this.txtNoInter.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNoInter.ForeColor = System.Drawing.Color.Black;
            this.txtNoInter.Location = new System.Drawing.Point(123, 82);
            this.txtNoInter.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoInter.MaxLength = 32767;
            this.txtNoInter.Multiline = false;
            this.txtNoInter.Name = "txtNoInter";
            this.txtNoInter.ReadOnly = true;
            this.txtNoInter.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoInter.Size = new System.Drawing.Size(124, 27);
            this.txtNoInter.TabIndex = 507;
            this.txtNoInter.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoInter.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(1, 83);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 19);
            this.label2.TabIndex = 506;
            this.label2.Text = "No Intervention";
            // 
            // txtIntervenant
            // 
            this.txtIntervenant.AccAcceptNumbersOnly = false;
            this.txtIntervenant.AccAllowComma = false;
            this.txtIntervenant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntervenant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntervenant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntervenant.AccHidenValue = "";
            this.txtIntervenant.AccNotAllowedChars = null;
            this.txtIntervenant.AccReadOnly = false;
            this.txtIntervenant.AccReadOnlyAllowDelete = false;
            this.txtIntervenant.AccRequired = false;
            this.txtIntervenant.BackColor = System.Drawing.Color.White;
            this.txtIntervenant.CustomBackColor = System.Drawing.Color.White;
            this.txtIntervenant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIntervenant.ForeColor = System.Drawing.Color.Black;
            this.txtIntervenant.Location = new System.Drawing.Point(123, 51);
            this.txtIntervenant.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntervenant.MaxLength = 32767;
            this.txtIntervenant.Multiline = false;
            this.txtIntervenant.Name = "txtIntervenant";
            this.txtIntervenant.ReadOnly = true;
            this.txtIntervenant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntervenant.Size = new System.Drawing.Size(124, 27);
            this.txtIntervenant.TabIndex = 505;
            this.txtIntervenant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntervenant.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(1, 52);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(90, 19);
            this.label1.TabIndex = 504;
            this.label1.Text = "Intervenant";
            // 
            // txtCodeimmeuble
            // 
            this.txtCodeimmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeimmeuble.AccAllowComma = false;
            this.txtCodeimmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeimmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeimmeuble.AccHidenValue = "";
            this.txtCodeimmeuble.AccNotAllowedChars = null;
            this.txtCodeimmeuble.AccReadOnly = false;
            this.txtCodeimmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeimmeuble.AccRequired = false;
            this.txtCodeimmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeimmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeimmeuble.Location = new System.Drawing.Point(123, 20);
            this.txtCodeimmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeimmeuble.MaxLength = 32767;
            this.txtCodeimmeuble.Multiline = false;
            this.txtCodeimmeuble.Name = "txtCodeimmeuble";
            this.txtCodeimmeuble.ReadOnly = true;
            this.txtCodeimmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeimmeuble.Size = new System.Drawing.Size(124, 27);
            this.txtCodeimmeuble.TabIndex = 503;
            this.txtCodeimmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeimmeuble.UseSystemPasswordChar = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(1, 21);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(118, 19);
            this.label33.TabIndex = 502;
            this.label33.Text = "Code immeuble";
            // 
            // optOperation
            // 
            this.optOperation.AutoSize = true;
            this.optOperation.Checked = true;
            this.optOperation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.optOperation.Location = new System.Drawing.Point(995, 146);
            this.optOperation.Name = "optOperation";
            this.optOperation.Size = new System.Drawing.Size(85, 20);
            this.optOperation.TabIndex = 538;
            this.optOperation.TabStop = true;
            this.optOperation.Text = "Opération";
            this.optOperation.UseVisualStyleBackColor = true;
            this.optOperation.CheckedChanged += new System.EventHandler(this.optOperation_CheckedChanged);
            // 
            // ssOpeTaches
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssOpeTaches.DisplayLayout.Appearance = appearance13;
            this.ssOpeTaches.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssOpeTaches.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ssOpeTaches.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssOpeTaches.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ssOpeTaches.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssOpeTaches.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ssOpeTaches.DisplayLayout.MaxColScrollRegions = 1;
            this.ssOpeTaches.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssOpeTaches.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssOpeTaches.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ssOpeTaches.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssOpeTaches.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ssOpeTaches.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssOpeTaches.DisplayLayout.Override.CellAppearance = appearance20;
            this.ssOpeTaches.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssOpeTaches.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ssOpeTaches.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ssOpeTaches.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ssOpeTaches.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssOpeTaches.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ssOpeTaches.DisplayLayout.Override.RowAppearance = appearance23;
            this.ssOpeTaches.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssOpeTaches.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssOpeTaches.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ssOpeTaches.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssOpeTaches.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssOpeTaches.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.ssOpeTaches.Location = new System.Drawing.Point(1012, 172);
            this.ssOpeTaches.Name = "ssOpeTaches";
            this.ssOpeTaches.Size = new System.Drawing.Size(160, 134);
            this.ssOpeTaches.TabIndex = 539;
            this.ssOpeTaches.Text = "ultraGrid1";
            this.ssOpeTaches.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssOpeTaches_InitializeLayout);
            // 
            // optTaches
            // 
            this.optTaches.AutoSize = true;
            this.optTaches.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.optTaches.Location = new System.Drawing.Point(963, 330);
            this.optTaches.Name = "optTaches";
            this.optTaches.Size = new System.Drawing.Size(159, 20);
            this.optTaches.TabIndex = 540;
            this.optTaches.Text = "Nature des opérations";
            this.optTaches.UseVisualStyleBackColor = true;
            this.optTaches.CheckedChanged += new System.EventHandler(this.optTaches_CheckedChanged);
            // 
            // txtNointervention
            // 
            this.txtNointervention.AccAcceptNumbersOnly = false;
            this.txtNointervention.AccAllowComma = false;
            this.txtNointervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNointervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNointervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNointervention.AccHidenValue = "";
            this.txtNointervention.AccNotAllowedChars = null;
            this.txtNointervention.AccReadOnly = false;
            this.txtNointervention.AccReadOnlyAllowDelete = false;
            this.txtNointervention.AccRequired = false;
            this.txtNointervention.BackColor = System.Drawing.Color.White;
            this.txtNointervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNointervention.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNointervention.ForeColor = System.Drawing.Color.Black;
            this.txtNointervention.Location = new System.Drawing.Point(995, 123);
            this.txtNointervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNointervention.MaxLength = 32767;
            this.txtNointervention.Multiline = false;
            this.txtNointervention.Name = "txtNointervention";
            this.txtNointervention.ReadOnly = false;
            this.txtNointervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNointervention.Size = new System.Drawing.Size(113, 27);
            this.txtNointervention.TabIndex = 527;
            this.txtNointervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNointervention.UseSystemPasswordChar = false;
            // 
            // txtNumFicheStandard
            // 
            this.txtNumFicheStandard.AccAcceptNumbersOnly = false;
            this.txtNumFicheStandard.AccAllowComma = false;
            this.txtNumFicheStandard.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNumFicheStandard.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNumFicheStandard.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNumFicheStandard.AccHidenValue = "";
            this.txtNumFicheStandard.AccNotAllowedChars = null;
            this.txtNumFicheStandard.AccReadOnly = false;
            this.txtNumFicheStandard.AccReadOnlyAllowDelete = false;
            this.txtNumFicheStandard.AccRequired = false;
            this.txtNumFicheStandard.BackColor = System.Drawing.Color.White;
            this.txtNumFicheStandard.CustomBackColor = System.Drawing.Color.White;
            this.txtNumFicheStandard.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNumFicheStandard.ForeColor = System.Drawing.Color.Black;
            this.txtNumFicheStandard.Location = new System.Drawing.Point(995, 97);
            this.txtNumFicheStandard.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumFicheStandard.MaxLength = 32767;
            this.txtNumFicheStandard.Multiline = false;
            this.txtNumFicheStandard.Name = "txtNumFicheStandard";
            this.txtNumFicheStandard.ReadOnly = false;
            this.txtNumFicheStandard.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNumFicheStandard.Size = new System.Drawing.Size(113, 27);
            this.txtNumFicheStandard.TabIndex = 526;
            this.txtNumFicheStandard.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNumFicheStandard.UseSystemPasswordChar = false;
            // 
            // frmPDAv2P2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(919, 711);
            this.Controls.Add(this.FraP2);
            this.Controls.Add(this.optTaches);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.ssOpeTaches);
            this.Controls.Add(this.groupBox2);
            this.Controls.Add(this.groupBox3);
            this.Controls.Add(this.optOperation);
            this.Controls.Add(this.txtNointervention);
            this.Controls.Add(this.txtNumFicheStandard);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedDialog;
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(935, 750);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(935, 726);
            this.Name = "frmPDAv2P2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Rapport PDA";
            this.Load += new System.EventHandler(this.frmPDAv2P2_Load);
            this.FraP2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssGridTachesAnnuel)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDoc)).EndInit();
            this.groupBox3.ResumeLayout(false);
            this.groupBox3.PerformLayout();
            this.groupBox2.ResumeLayout(false);
            this.groupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssOpeTaches)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.GroupBox groupBox1;
        public iTalk.iTalk_TextBox_Small2 txtCodeimmeuble;
        private System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 cmbStatutPDA;
        private System.Windows.Forms.Label label4;
        public iTalk.iTalk_TextBox_Small2 txtCodeEtat;
        private System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 txtNoInter;
        private System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtIntervenant;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.CheckBox chkErrGpsHZDebut;
        private System.Windows.Forms.CheckBox chkErrGpsHZFin;
        private System.Windows.Forms.GroupBox groupBox3;
        public iTalk.iTalk_TextBox_Small2 txtDureePDA;
        private System.Windows.Forms.Label label17;
        public iTalk.iTalk_TextBox_Small2 txtHeureFinPDA;
        private System.Windows.Forms.Label label11;
        public iTalk.iTalk_TextBox_Small2 txtHeureDebutPDA;
        private System.Windows.Forms.Label label12;
        public iTalk.iTalk_TextBox_Small2 txtArrivePrevue;
        private System.Windows.Forms.Label label13;
        public iTalk.iTalk_TextBox_Small2 txtDateHeureCopilot;
        private System.Windows.Forms.Label label14;
        public iTalk.iTalk_TextBox_Small2 txtLuSurPdaLe;
        private System.Windows.Forms.Label label15;
        public iTalk.iTalk_TextBox_Small2 txtRecuSurPdaLe;
        private System.Windows.Forms.Label label16;
        public iTalk.iTalk_TextBox_Small2 txtDuree;
        private System.Windows.Forms.Label label10;
        public iTalk.iTalk_TextBox_Small2 txtHeureFin;
        private System.Windows.Forms.Label label5;
        public iTalk.iTalk_TextBox_Small2 txtHeureDebut;
        private System.Windows.Forms.Label label6;
        public iTalk.iTalk_TextBox_Small2 txtDateRealise;
        private System.Windows.Forms.Label label7;
        public iTalk.iTalk_TextBox_Small2 txtHeurePrevue;
        private System.Windows.Forms.Label label8;
        public iTalk.iTalk_TextBox_Small2 txtDatePrevue;
        private System.Windows.Forms.Label label9;
        public System.Windows.Forms.Button Command1;
        public iTalk.iTalk_TextBox_Small2 txtInfoImmeuble;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.CheckBox chkHeureSup;
        private System.Windows.Forms.CheckBox chkRAS;
        private System.Windows.Forms.CheckBox chkDevisEtablir;
        public iTalk.iTalk_TextBox_Small2 txtEnqueteQualite;
        private System.Windows.Forms.Label label18;
        public iTalk.iTalk_TextBox_Small2 txtNomSignataire;
        private System.Windows.Forms.Label label19;
        public iTalk.iTalk_TextBox_Small2 txtPRI_Code;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.Button cmdDevis;
        public System.Windows.Forms.Button cmdCompteur;
        private System.Windows.Forms.Label label22;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridDoc;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssGridTachesAnnuel;
        public iTalk.iTalk_TextBox_Small2 txtNumFicheStandard;
        public iTalk.iTalk_TextBox_Small2 txtNointervention;
        private System.Windows.Forms.RadioButton optOperation;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssOpeTaches;
        private System.Windows.Forms.RadioButton optTaches;
        public System.Windows.Forms.GroupBox FraP2;
    }
}