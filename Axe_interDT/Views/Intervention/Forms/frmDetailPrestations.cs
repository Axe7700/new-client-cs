﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Intervention.Forms
{
    public partial class frmDetailPrestations : Form
    {
        DataTable rsPrest = null;
        ModAdo modAdorsPrest = null;
        public frmDetailPrestations()
        {
            InitializeComponent();
        }
/// <summary>
/// tested
/// </summary>
        private void fc_LoadPrest()
        {
            string sSQL = null;

            try
            {
                sSQL = "SELECT     Nointervention, CodeArticle, Designation, Commentaire, Intervenant, AFaire" + " From InterP2Complement " +
                    " WHERE     Nointervention = " + General.nz(txtNoIntervention.Text, 0) + " and aFaire = 1";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                modAdorsPrest = new ModAdo();
                rsPrest = modAdorsPrest.fc_OpenRecordSet(sSQL);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                ssGridPrest.DataSource = rsPrest;
                ssGridPrest.UpdateData();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadPrest");
            }

        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmDetailPrestations_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_LoadPrest();

        }
        private void ssGridPrest_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            modAdorsPrest.Update();
        }

        private void ssGridPrest_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdorsPrest.Update();
        }

        private void ssGridPrest_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                e.Cancel = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridPrest_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
             for(int i = 0; i < ssGridPrest.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                ssGridPrest.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }
            ssGridPrest.DisplayLayout.Bands[0].Columns["AFaire"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridPrest.DisplayLayout.Bands[0].Columns["Designation"].Width = 100;
            ssGridPrest.DisplayLayout.Bands[0].Columns["Commentaire"].Width = 100;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridPrest_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            if (e.Row.Cells["AFaire"].Value == DBNull.Value || e.Row.Cells["AFaire"].Value.ToString()=="0")
            {
                e.Row.Cells["AFaire"].Value = false;
            }

        }
    }
}
