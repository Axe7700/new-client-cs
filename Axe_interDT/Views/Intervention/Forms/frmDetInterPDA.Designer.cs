﻿namespace Axe_interDT.Views.Intervention.Forms
{
    partial class frmDetInterPDA
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label33 = new System.Windows.Forms.Label();
            this.txtDateSaisie = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.txtDatePrevue = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.txtHeurePrevue = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDateRealise = new iTalk.iTalk_TextBox_Small2();
            this.label4 = new System.Windows.Forms.Label();
            this.txtHeureDebut = new iTalk.iTalk_TextBox_Small2();
            this.label5 = new System.Windows.Forms.Label();
            this.txtHeureDebutPDA = new iTalk.iTalk_TextBox_Small2();
            this.label6 = new System.Windows.Forms.Label();
            this.txtHeureFinPDA = new iTalk.iTalk_TextBox_Small2();
            this.label7 = new System.Windows.Forms.Label();
            this.txtHeureFin = new iTalk.iTalk_TextBox_Small2();
            this.label8 = new System.Windows.Forms.Label();
            this.txtDureePDA = new iTalk.iTalk_TextBox_Small2();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDuree = new iTalk.iTalk_TextBox_Small2();
            this.label10 = new System.Windows.Forms.Label();
            this.txtNointervention = new iTalk.iTalk_TextBox_Small2();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label33.Location = new System.Drawing.Point(202, 9);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(154, 20);
            this.label33.TabIndex = 384;
            this.label33.Text = "Info complémentaire";
            // 
            // txtDateSaisie
            // 
            this.txtDateSaisie.AccAcceptNumbersOnly = false;
            this.txtDateSaisie.AccAllowComma = false;
            this.txtDateSaisie.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateSaisie.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateSaisie.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateSaisie.AccHidenValue = "";
            this.txtDateSaisie.AccNotAllowedChars = null;
            this.txtDateSaisie.AccReadOnly = false;
            this.txtDateSaisie.AccReadOnlyAllowDelete = false;
            this.txtDateSaisie.AccRequired = false;
            this.txtDateSaisie.BackColor = System.Drawing.Color.White;
            this.txtDateSaisie.CustomBackColor = System.Drawing.Color.White;
            this.txtDateSaisie.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDateSaisie.ForeColor = System.Drawing.Color.Blue;
            this.txtDateSaisie.Location = new System.Drawing.Point(115, 30);
            this.txtDateSaisie.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateSaisie.MaxLength = 32767;
            this.txtDateSaisie.Multiline = false;
            this.txtDateSaisie.Name = "txtDateSaisie";
            this.txtDateSaisie.ReadOnly = true;
            this.txtDateSaisie.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateSaisie.Size = new System.Drawing.Size(135, 27);
            this.txtDateSaisie.TabIndex = 503;
            this.txtDateSaisie.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateSaisie.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(7, 29);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(83, 19);
            this.label1.TabIndex = 502;
            this.label1.Text = "Date saisie";
            // 
            // txtDatePrevue
            // 
            this.txtDatePrevue.AccAcceptNumbersOnly = false;
            this.txtDatePrevue.AccAllowComma = false;
            this.txtDatePrevue.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDatePrevue.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDatePrevue.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDatePrevue.AccHidenValue = "";
            this.txtDatePrevue.AccNotAllowedChars = null;
            this.txtDatePrevue.AccReadOnly = false;
            this.txtDatePrevue.AccReadOnlyAllowDelete = false;
            this.txtDatePrevue.AccRequired = false;
            this.txtDatePrevue.BackColor = System.Drawing.Color.White;
            this.txtDatePrevue.CustomBackColor = System.Drawing.Color.White;
            this.txtDatePrevue.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDatePrevue.ForeColor = System.Drawing.Color.Blue;
            this.txtDatePrevue.Location = new System.Drawing.Point(115, 61);
            this.txtDatePrevue.Margin = new System.Windows.Forms.Padding(2);
            this.txtDatePrevue.MaxLength = 32767;
            this.txtDatePrevue.Multiline = false;
            this.txtDatePrevue.Name = "txtDatePrevue";
            this.txtDatePrevue.ReadOnly = true;
            this.txtDatePrevue.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDatePrevue.Size = new System.Drawing.Size(135, 27);
            this.txtDatePrevue.TabIndex = 505;
            this.txtDatePrevue.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDatePrevue.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(7, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 19);
            this.label2.TabIndex = 504;
            this.label2.Text = "Date Prévue";
            // 
            // txtHeurePrevue
            // 
            this.txtHeurePrevue.AccAcceptNumbersOnly = false;
            this.txtHeurePrevue.AccAllowComma = false;
            this.txtHeurePrevue.AccBackgroundColor = System.Drawing.Color.White;
            this.txtHeurePrevue.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtHeurePrevue.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtHeurePrevue.AccHidenValue = "";
            this.txtHeurePrevue.AccNotAllowedChars = null;
            this.txtHeurePrevue.AccReadOnly = false;
            this.txtHeurePrevue.AccReadOnlyAllowDelete = false;
            this.txtHeurePrevue.AccRequired = false;
            this.txtHeurePrevue.BackColor = System.Drawing.Color.White;
            this.txtHeurePrevue.CustomBackColor = System.Drawing.Color.White;
            this.txtHeurePrevue.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtHeurePrevue.ForeColor = System.Drawing.Color.Blue;
            this.txtHeurePrevue.Location = new System.Drawing.Point(115, 92);
            this.txtHeurePrevue.Margin = new System.Windows.Forms.Padding(2);
            this.txtHeurePrevue.MaxLength = 32767;
            this.txtHeurePrevue.Multiline = false;
            this.txtHeurePrevue.Name = "txtHeurePrevue";
            this.txtHeurePrevue.ReadOnly = true;
            this.txtHeurePrevue.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtHeurePrevue.Size = new System.Drawing.Size(135, 27);
            this.txtHeurePrevue.TabIndex = 507;
            this.txtHeurePrevue.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtHeurePrevue.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(7, 91);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(105, 19);
            this.label3.TabIndex = 506;
            this.label3.Text = "Heure Prévue";
            // 
            // txtDateRealise
            // 
            this.txtDateRealise.AccAcceptNumbersOnly = false;
            this.txtDateRealise.AccAllowComma = false;
            this.txtDateRealise.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateRealise.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateRealise.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateRealise.AccHidenValue = "";
            this.txtDateRealise.AccNotAllowedChars = null;
            this.txtDateRealise.AccReadOnly = false;
            this.txtDateRealise.AccReadOnlyAllowDelete = false;
            this.txtDateRealise.AccRequired = false;
            this.txtDateRealise.BackColor = System.Drawing.Color.White;
            this.txtDateRealise.CustomBackColor = System.Drawing.Color.White;
            this.txtDateRealise.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDateRealise.ForeColor = System.Drawing.Color.Blue;
            this.txtDateRealise.Location = new System.Drawing.Point(115, 123);
            this.txtDateRealise.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateRealise.MaxLength = 32767;
            this.txtDateRealise.Multiline = false;
            this.txtDateRealise.Name = "txtDateRealise";
            this.txtDateRealise.ReadOnly = true;
            this.txtDateRealise.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateRealise.Size = new System.Drawing.Size(135, 27);
            this.txtDateRealise.TabIndex = 509;
            this.txtDateRealise.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateRealise.UseSystemPasswordChar = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label4.Location = new System.Drawing.Point(7, 122);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(103, 19);
            this.label4.TabIndex = 508;
            this.label4.Text = "Date Réalisée";
            // 
            // txtHeureDebut
            // 
            this.txtHeureDebut.AccAcceptNumbersOnly = false;
            this.txtHeureDebut.AccAllowComma = false;
            this.txtHeureDebut.AccBackgroundColor = System.Drawing.Color.White;
            this.txtHeureDebut.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtHeureDebut.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtHeureDebut.AccHidenValue = "";
            this.txtHeureDebut.AccNotAllowedChars = null;
            this.txtHeureDebut.AccReadOnly = false;
            this.txtHeureDebut.AccReadOnlyAllowDelete = false;
            this.txtHeureDebut.AccRequired = false;
            this.txtHeureDebut.BackColor = System.Drawing.Color.White;
            this.txtHeureDebut.CustomBackColor = System.Drawing.Color.White;
            this.txtHeureDebut.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtHeureDebut.ForeColor = System.Drawing.Color.Blue;
            this.txtHeureDebut.Location = new System.Drawing.Point(115, 176);
            this.txtHeureDebut.Margin = new System.Windows.Forms.Padding(2);
            this.txtHeureDebut.MaxLength = 32767;
            this.txtHeureDebut.Multiline = false;
            this.txtHeureDebut.Name = "txtHeureDebut";
            this.txtHeureDebut.ReadOnly = true;
            this.txtHeureDebut.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtHeureDebut.Size = new System.Drawing.Size(135, 27);
            this.txtHeureDebut.TabIndex = 511;
            this.txtHeureDebut.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtHeureDebut.UseSystemPasswordChar = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label5.Location = new System.Drawing.Point(7, 175);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(100, 19);
            this.label5.TabIndex = 510;
            this.label5.Text = "Heure Début";
            // 
            // txtHeureDebutPDA
            // 
            this.txtHeureDebutPDA.AccAcceptNumbersOnly = false;
            this.txtHeureDebutPDA.AccAllowComma = false;
            this.txtHeureDebutPDA.AccBackgroundColor = System.Drawing.Color.White;
            this.txtHeureDebutPDA.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtHeureDebutPDA.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtHeureDebutPDA.AccHidenValue = "";
            this.txtHeureDebutPDA.AccNotAllowedChars = null;
            this.txtHeureDebutPDA.AccReadOnly = false;
            this.txtHeureDebutPDA.AccReadOnlyAllowDelete = false;
            this.txtHeureDebutPDA.AccRequired = false;
            this.txtHeureDebutPDA.BackColor = System.Drawing.Color.White;
            this.txtHeureDebutPDA.CustomBackColor = System.Drawing.Color.White;
            this.txtHeureDebutPDA.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtHeureDebutPDA.ForeColor = System.Drawing.Color.Blue;
            this.txtHeureDebutPDA.Location = new System.Drawing.Point(450, 175);
            this.txtHeureDebutPDA.Margin = new System.Windows.Forms.Padding(2);
            this.txtHeureDebutPDA.MaxLength = 32767;
            this.txtHeureDebutPDA.Multiline = false;
            this.txtHeureDebutPDA.Name = "txtHeureDebutPDA";
            this.txtHeureDebutPDA.ReadOnly = true;
            this.txtHeureDebutPDA.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtHeureDebutPDA.Size = new System.Drawing.Size(135, 27);
            this.txtHeureDebutPDA.TabIndex = 513;
            this.txtHeureDebutPDA.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtHeureDebutPDA.UseSystemPasswordChar = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label6.Location = new System.Drawing.Point(256, 174);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(139, 19);
            this.label6.TabIndex = 512;
            this.label6.Text = "Heure Début auto.";
            // 
            // txtHeureFinPDA
            // 
            this.txtHeureFinPDA.AccAcceptNumbersOnly = false;
            this.txtHeureFinPDA.AccAllowComma = false;
            this.txtHeureFinPDA.AccBackgroundColor = System.Drawing.Color.White;
            this.txtHeureFinPDA.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtHeureFinPDA.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtHeureFinPDA.AccHidenValue = "";
            this.txtHeureFinPDA.AccNotAllowedChars = null;
            this.txtHeureFinPDA.AccReadOnly = false;
            this.txtHeureFinPDA.AccReadOnlyAllowDelete = false;
            this.txtHeureFinPDA.AccRequired = false;
            this.txtHeureFinPDA.BackColor = System.Drawing.Color.White;
            this.txtHeureFinPDA.CustomBackColor = System.Drawing.Color.White;
            this.txtHeureFinPDA.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtHeureFinPDA.ForeColor = System.Drawing.Color.Blue;
            this.txtHeureFinPDA.Location = new System.Drawing.Point(450, 206);
            this.txtHeureFinPDA.Margin = new System.Windows.Forms.Padding(2);
            this.txtHeureFinPDA.MaxLength = 32767;
            this.txtHeureFinPDA.Multiline = false;
            this.txtHeureFinPDA.Name = "txtHeureFinPDA";
            this.txtHeureFinPDA.ReadOnly = true;
            this.txtHeureFinPDA.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtHeureFinPDA.Size = new System.Drawing.Size(135, 27);
            this.txtHeureFinPDA.TabIndex = 517;
            this.txtHeureFinPDA.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtHeureFinPDA.UseSystemPasswordChar = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label7.Location = new System.Drawing.Point(256, 205);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(115, 19);
            this.label7.TabIndex = 516;
            this.label7.Text = "Heure Fin auto.";
            // 
            // txtHeureFin
            // 
            this.txtHeureFin.AccAcceptNumbersOnly = false;
            this.txtHeureFin.AccAllowComma = false;
            this.txtHeureFin.AccBackgroundColor = System.Drawing.Color.White;
            this.txtHeureFin.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtHeureFin.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtHeureFin.AccHidenValue = "";
            this.txtHeureFin.AccNotAllowedChars = null;
            this.txtHeureFin.AccReadOnly = false;
            this.txtHeureFin.AccReadOnlyAllowDelete = false;
            this.txtHeureFin.AccRequired = false;
            this.txtHeureFin.BackColor = System.Drawing.Color.White;
            this.txtHeureFin.CustomBackColor = System.Drawing.Color.White;
            this.txtHeureFin.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtHeureFin.ForeColor = System.Drawing.Color.Blue;
            this.txtHeureFin.Location = new System.Drawing.Point(115, 207);
            this.txtHeureFin.Margin = new System.Windows.Forms.Padding(2);
            this.txtHeureFin.MaxLength = 32767;
            this.txtHeureFin.Multiline = false;
            this.txtHeureFin.Name = "txtHeureFin";
            this.txtHeureFin.ReadOnly = true;
            this.txtHeureFin.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtHeureFin.Size = new System.Drawing.Size(135, 27);
            this.txtHeureFin.TabIndex = 515;
            this.txtHeureFin.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtHeureFin.UseSystemPasswordChar = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label8.Location = new System.Drawing.Point(7, 206);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(76, 19);
            this.label8.TabIndex = 514;
            this.label8.Text = "Heure Fin";
            // 
            // txtDureePDA
            // 
            this.txtDureePDA.AccAcceptNumbersOnly = false;
            this.txtDureePDA.AccAllowComma = false;
            this.txtDureePDA.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDureePDA.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDureePDA.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDureePDA.AccHidenValue = "";
            this.txtDureePDA.AccNotAllowedChars = null;
            this.txtDureePDA.AccReadOnly = false;
            this.txtDureePDA.AccReadOnlyAllowDelete = false;
            this.txtDureePDA.AccRequired = false;
            this.txtDureePDA.BackColor = System.Drawing.Color.White;
            this.txtDureePDA.CustomBackColor = System.Drawing.Color.White;
            this.txtDureePDA.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDureePDA.ForeColor = System.Drawing.Color.Blue;
            this.txtDureePDA.Location = new System.Drawing.Point(450, 237);
            this.txtDureePDA.Margin = new System.Windows.Forms.Padding(2);
            this.txtDureePDA.MaxLength = 32767;
            this.txtDureePDA.Multiline = false;
            this.txtDureePDA.Name = "txtDureePDA";
            this.txtDureePDA.ReadOnly = true;
            this.txtDureePDA.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDureePDA.Size = new System.Drawing.Size(135, 27);
            this.txtDureePDA.TabIndex = 521;
            this.txtDureePDA.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDureePDA.UseSystemPasswordChar = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label9.Location = new System.Drawing.Point(256, 236);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(187, 19);
            this.label9.TabIndex = 520;
            this.label9.Text = "Text Text TextDurée auto";
            // 
            // txtDuree
            // 
            this.txtDuree.AccAcceptNumbersOnly = false;
            this.txtDuree.AccAllowComma = false;
            this.txtDuree.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDuree.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDuree.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDuree.AccHidenValue = "";
            this.txtDuree.AccNotAllowedChars = null;
            this.txtDuree.AccReadOnly = false;
            this.txtDuree.AccReadOnlyAllowDelete = false;
            this.txtDuree.AccRequired = false;
            this.txtDuree.BackColor = System.Drawing.Color.White;
            this.txtDuree.CustomBackColor = System.Drawing.Color.White;
            this.txtDuree.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDuree.ForeColor = System.Drawing.Color.Blue;
            this.txtDuree.Location = new System.Drawing.Point(115, 238);
            this.txtDuree.Margin = new System.Windows.Forms.Padding(2);
            this.txtDuree.MaxLength = 32767;
            this.txtDuree.Multiline = false;
            this.txtDuree.Name = "txtDuree";
            this.txtDuree.ReadOnly = true;
            this.txtDuree.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDuree.Size = new System.Drawing.Size(135, 27);
            this.txtDuree.TabIndex = 519;
            this.txtDuree.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDuree.UseSystemPasswordChar = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label10.Location = new System.Drawing.Point(7, 237);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(53, 19);
            this.label10.TabIndex = 518;
            this.label10.Text = "Durée";
            // 
            // txtNointervention
            // 
            this.txtNointervention.AccAcceptNumbersOnly = false;
            this.txtNointervention.AccAllowComma = false;
            this.txtNointervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNointervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNointervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNointervention.AccHidenValue = "";
            this.txtNointervention.AccNotAllowedChars = null;
            this.txtNointervention.AccReadOnly = false;
            this.txtNointervention.AccReadOnlyAllowDelete = false;
            this.txtNointervention.AccRequired = false;
            this.txtNointervention.BackColor = System.Drawing.Color.White;
            this.txtNointervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNointervention.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNointervention.ForeColor = System.Drawing.Color.Black;
            this.txtNointervention.Location = new System.Drawing.Point(364, 21);
            this.txtNointervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNointervention.MaxLength = 32767;
            this.txtNointervention.Multiline = false;
            this.txtNointervention.Name = "txtNointervention";
            this.txtNointervention.ReadOnly = true;
            this.txtNointervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNointervention.Size = new System.Drawing.Size(135, 27);
            this.txtNointervention.TabIndex = 522;
            this.txtNointervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNointervention.UseSystemPasswordChar = false;
            this.txtNointervention.Visible = false;
            // 
            // frmDetInterPDA
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(596, 275);
            this.Controls.Add(this.txtNointervention);
            this.Controls.Add(this.txtDureePDA);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtDuree);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.txtHeureFinPDA);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtHeureFin);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtHeureDebutPDA);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.txtHeureDebut);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtDateRealise);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtHeurePrevue);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.txtDatePrevue);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtDateSaisie);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label33);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(612, 314);
            this.MinimumSize = new System.Drawing.Size(612, 314);
            this.Name = "frmDetInterPDA";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Informations complémentaire";
            this.Load += new System.EventHandler(this.frmDetInterPDA_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtDateSaisie;
        private System.Windows.Forms.Label label1;
        public iTalk.iTalk_TextBox_Small2 txtDatePrevue;
        private System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtHeurePrevue;
        private System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 txtDateRealise;
        private System.Windows.Forms.Label label4;
        public iTalk.iTalk_TextBox_Small2 txtHeureDebut;
        private System.Windows.Forms.Label label5;
        public iTalk.iTalk_TextBox_Small2 txtHeureDebutPDA;
        private System.Windows.Forms.Label label6;
        public iTalk.iTalk_TextBox_Small2 txtHeureFinPDA;
        private System.Windows.Forms.Label label7;
        public iTalk.iTalk_TextBox_Small2 txtHeureFin;
        private System.Windows.Forms.Label label8;
        public iTalk.iTalk_TextBox_Small2 txtDureePDA;
        private System.Windows.Forms.Label label9;
        public iTalk.iTalk_TextBox_Small2 txtDuree;
        private System.Windows.Forms.Label label10;
        public iTalk.iTalk_TextBox_Small2 txtNointervention;
    }
}