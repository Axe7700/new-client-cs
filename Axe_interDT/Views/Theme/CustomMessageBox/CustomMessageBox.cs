﻿using Axe_interDT.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraMessageBox;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Theme.CustomMessageBox
{
    public class CustomMessageBox
    {
        private static UltraMessageBoxManager messageBoxManager;

        private static UltraMessageBoxManager GetBoxManager()
        {
            if (messageBoxManager == null)
            {
                messageBoxManager = new UltraMessageBoxManager();
                setStyleOfBoxManager();
            }
            return messageBoxManager;
        }

        private static void setStyleOfBoxManager()
        {

            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            appearance1.BackColor = System.Drawing.Color.LightGray;
            appearance1.BackColor2 = System.Drawing.Color.LightGray;
            appearance1.BorderColor = System.Drawing.Color.Black;
            appearance1.BorderColor2 = System.Drawing.Color.Black;
            appearance1.BorderColor3DBase = System.Drawing.Color.Black;
            appearance1.FontData.BoldAsString = "True";
            appearance1.FontData.ItalicAsString = "False";
            appearance1.FontData.SizeInPoints = 10.5F;
            appearance1.ForeColor = System.Drawing.Color.Black;
            appearance1.ForeColorDisabled = System.Drawing.Color.Black;
            messageBoxManager.Appearance = appearance1;
            messageBoxManager.ButtonAlignment = Infragistics.Win.HAlign.Right;
            messageBoxManager.MinimumWidth = 500;
            //boxManager.ContainingControl = this;
            messageBoxManager.Style = Infragistics.Win.UltraMessageBox.MessageBoxStyle.Standard;
        }

        public static DialogResult Show(string message, string title, MessageBoxButtons buttons, MessageBoxIcon icon, MessageBoxDefaultButton defaultButton = MessageBoxDefaultButton.Button1,bool isSmall=true)
        {
            try
            {
                var info = getMessageBoxInfo(message, title);
                info.Buttons = buttons;
                info.Icon = icon;
                info.DefaultButton = defaultButton;
                info.ShowHelpButton = DefaultableBoolean.False;
                if (!isSmall)
                    info.MinimumWidth = 700;
                //UltraMessageBoxInfo info = new UltraMessageBoxInfo(MessageBoxStyle.Default, Owner, message, title, buttons, icon, defaultButton, DefaultableBoolean.False);
                return GetBoxManager().ShowMessageBox(info);
            }catch(Exception ex)
            {
                Erreurs.gFr_debug(ex, "Show");
                return DialogResult.Cancel;
            }
        }

        private static UltraMessageBoxInfo getMessageBoxInfo(string message, string caption)
        {
            UltraMessageBoxInfo info = new UltraMessageBoxInfo();
            info.Style = MessageBoxStyle.Default;
            info.Text = message;
            if (View.Theme.Theme.MainForm != null)
                info.Owner = View.Theme.Theme.MainForm;
            //info.Owner = owner;
            info.Caption = caption;
            return info;
        }

        public static DialogResult Show(string message, string caption)
        {
            return GetBoxManager().ShowMessageBox(getMessageBoxInfo(message, caption));
        }

        public static void Show(string message)
        {
            GetBoxManager().ShowMessageBox(getMessageBoxInfo(message, ""));
        }

        public static DialogResult Show(string message, string caption, MessageBoxButtons buttons)
        {
            var info = getMessageBoxInfo(message, caption);
            info.Buttons = buttons;
            return GetBoxManager().ShowMessageBox(info);
        }

        public static DialogResult Show(string message, string caption, MessageBoxIcon icon)
        {
            var info = getMessageBoxInfo(message, caption);
            info.Icon = icon;
            return GetBoxManager().ShowMessageBox(info);
        }
    }
}
