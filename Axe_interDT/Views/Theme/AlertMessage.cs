﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Theme
{
    public partial class AlertMessage : Form
    {
        public object Success { get; }

        public enum EnumType { Success, Warning }
        public AlertMessage(EnumType type)
        {
            InitializeComponent();
            if (type.ToString().ToUpper() == EnumType.Success.ToString().ToUpper())
            {
                panelSucces.Visible = true;
                PanelWarning.Visible = false;
            }
            else if(type.ToString().ToUpper() == EnumType.Warning.ToString().ToUpper())
            {
                panelSucces.Visible = false;
                PanelWarning.Visible = true;
            }
        }

        private void AlertMessage_Load(object sender, EventArgs e)
        {
            this.Top =  30;
            this.Left = Screen.PrimaryScreen.Bounds.Width - this.Width -4;
        }

        private void AlertMessage_VisibleChanged(object sender, EventArgs e)
        {
            Timer T = new Timer { Interval = (int)5000 };
            T.Start();
            T.Tick += (se, ev) =>
            {
                Visible = false;
                T.Stop();
            };
        }
    }
}
