﻿using Axe_interDT.Properties;
using Axe_interDT.Shared;
using Newtonsoft.Json.Linq;
using System;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace Axe_interDT.Views.Theme
{
    public partial class frmWhatsNew : Form
    {
        public frmWhatsNew()
        {
            InitializeComponent();
        }

        private void frmWhatsNew_Load(object sender, EventArgs e)
        {
            string json = General._ExecutionMode == General.ExecutionMode.Dev ? @"C:\DelsotalEtThibault-Intranet\Axe_interDT\Resources\projet_info.json"
            : @"\\azdt-fic-01\AXECIEL\PACKAGES\INTRANET\Axe_interDT_Prod\projet_info.json";

            if (!File.Exists(json))
                return;

            StreamReader sr = new StreamReader(json, Encoding.GetEncoding("iso-8859-1"));
            var lines = sr.ReadToEnd();
            var jsonObj = JObject.Parse(lines);
            var version = jsonObj["Version"];
            var versionDate = jsonObj["VersionDate"];
            var commit = jsonObj["Commit"];
            var newFeatures = jsonObj["MailNewFeatures"];
            var fixedBugs = jsonObj["FixedBugs"];

            labelVersionNumber.Text = "N° Version : V" + version + "." + commit;
            labelVersionDate.Text = "Date de la version : " + versionDate;

            foreach (var feature in newFeatures.Where(ele => ele["Show"]?.ToString() == "true"))
            {
                //1710; 39
                var panel = new FlowLayoutPanel();
                panel.Size = new Size(1710, 39);
                //Adding the Functionalities
                //Adding func picturebox
                panel.Controls.Add(new PictureBox
                {
                    Size = new Size(32, 32),
                    SizeMode = PictureBoxSizeMode.StretchImage,
                    Image = Resources.functionality
                });
                //Adding function label
                panel.Controls.Add(new Label
                {
                    Font = new Font("Ubuntu", 14),
                    Margin = new Padding(3, 7, 3, 0),
                    Text = $"Fonctionnalité",
                    AutoSize = true
                });
                panel.Controls.Add(new Label
                {
                    Font = new Font("Ubuntu", 14),
                    Margin = new Padding(3, 7, 3, 0),
                    Text = feature["FeatureName"]?.ToString(),
                    AutoSize = true,
                    ForeColor = Color.FromArgb(66, 133, 244)
                });
                panel.Controls.Add(new Label
                {
                    Font = new Font("Ubuntu", 14),
                    Margin = new Padding(3, 7, 3, 0),
                    Text = "Demandé par",
                    AutoSize = true
                });
                panel.Controls.Add(new Label
                {
                    Font = new Font("Ubuntu", 14),
                    Margin = new Padding(3, 7, 3, 0),
                    Text = feature["RequestedBy"]?.ToString(),
                    AutoSize = true,
                    ForeColor = Color.FromArgb(66, 133, 244)
                });
                //Adding the flow laeyout panel
                flowLayoutPanel1.Controls.Add(panel);
            }

            //Adding the bugs
            foreach (var bug in fixedBugs.Where(ele => ele["Show"]?.ToString() == "true"))
            {
                //1710; 39
                var panel = new FlowLayoutPanel();
                panel.Size = new Size(1710, 39);

                //Adding func picturebox
                panel.Controls.Add(new PictureBox
                {
                    Size = new Size(32, 32),
                    SizeMode = PictureBoxSizeMode.StretchImage,
                    Image = Resources.bug
                });
                //Adding function label
                panel.Controls.Add(new Label
                {
                    Font = new Font("Ubuntu", 14),
                    Margin = new Padding(3, 7, 3, 0),
                    Text = $"Bug corrigé",
                    AutoSize = true
                });
                panel.Controls.Add(new Label
                {
                    Font = new Font("Ubuntu", 14),
                    Margin = new Padding(3, 7, 3, 0),
                    Text = $"#{bug["BugId"]} {bug["BugName"]}",
                    AutoSize = true,
                    ForeColor = Color.FromArgb(234, 67, 53)
                });
                panel.Controls.Add(new Label
                {
                    Font = new Font("Ubuntu", 14),
                    Margin = new Padding(3, 7, 3, 0),
                    Text = $"Signalé par",
                    AutoSize = true
                });
                panel.Controls.Add(new Label
                {
                    Font = new Font("Ubuntu", 14),
                    Margin = new Padding(3, 7, 3, 0),
                    Text = $"{bug["ReportedBy"]}",
                    AutoSize = true,
                    ForeColor = Color.FromArgb(234, 67, 53)
                });


                //Adding the flow laeyout panel
                flowLayoutPanel1.Controls.Add(panel);
            }
        }

        private void button15_Click(object sender, EventArgs e)
        {
            if (Views.Theme.CustomMessageBox.CustomMessageBox.Show("Une mise à jour est disponible, Cliquez sur \"OUI\" pour relancer le programme", "Nouvelle Version", MessageBoxButtons.YesNo, MessageBoxIcon.Information, MessageBoxDefaultButton.Button2) == DialogResult.Yes)
            {
                if (General._ExecutionMode == General.ExecutionMode.Dev)
                {
                    if (General.fncUserName().ToLower().Contains("mondir"))
                    {
                        ModuleAPI.Ouvrir(@"C:\Data\Donnee\delostal\Axe_interDT.exe.bat");
                        Application.Exit();
                        return;
                    }

                    if (File.Exists(@"C:\Users\messi\Desktop\bat.exe.bat"))
                    {
                        ModuleAPI.Ouvrir(@"C:\Users\messi\Desktop\bat.exe.bat");
                        Application.Exit();
                    }
                }
                else
                {
                    if (File.Exists(UserDocFichePrincipal.AxeInterDt_Bat_File_Path))
                    {
                        ModuleAPI.Ouvrir(UserDocFichePrincipal.AxeInterDt_Bat_File_Path);
                        Application.Exit();
                    }
                }
            }
        }
    }
}
