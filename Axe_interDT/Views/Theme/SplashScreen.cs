﻿using Axe_interDT.Shared;
using System;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;

namespace Axe_interDT.Views.Theme
{
    public partial class SplashScreen : Form
    {
        int i = 6;

        public SplashScreen()
        {
            InitializeComponent();

            //===> Mondir le 08.01.2021 Ticket #2159
            switch (General._Company)
            {
                case General.Company.LONG:
                    labelCompany.Text = "LONG Chauffage";
                    LabelVersion.Location = new Point(285, 75);
                    break;
                case General.Company.VF:
                    labelCompany.Text = "Van Froid";
                    LabelVersion.Location = new Point(285, 75);
                    break;
            }

            labelCompany.Visible = true;
            LabelVersion.Visible = true;
            //===> Fin Modif Mondir
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            /// if block added by mohammed
            if (View.Theme.Theme.killSplashScreen)
            {
                timer1.Stop();
                this.Hide();
                return;
            }
            int MaxWith = Width;
            int StepWith = MaxWith / View.Theme.Theme.ControlToLoadOnLunch.Count;
            i++;
            if (i >= 7)
            {
                i = 0;
                if (View.Theme.Theme.IsLoading < View.Theme.Theme.ControlToLoadOnLunch.Count - 1)
                    LabelLoading.Text = $"Chargement du module {View.Theme.Theme.ControlToLoadOnLunch.ElementAt(View.Theme.Theme.IsLoading).Key} .";
                LabelProgressBar.Width = StepWith * (View.Theme.Theme.IsLoading + 1);
            }
            LabelLoading.Text += ".";

        }

        private void SplashScreen_Load(object sender, EventArgs e)
        {
            Activate();
            LabelVersion.Text = $"V{General.Version}";
            timer1.Start();
        }

        private void LabelClose_Click(object sender, EventArgs e)
        {
            Environment.Exit(0);
        }
    }
}
