﻿
using Axe_interDT.Shared;
using Axe_interDT.Views;
using Axe_interDT.Views.Analytique;
using Axe_interDT.Views.Analytique.AnalytiqueTravaux;
using Axe_interDT.Views.Analytique.ExportImmeubleAstreinte;
using Axe_interDT.Views.Appel.Mail;
using Axe_interDT.Views.BaseDeDonnees.Gerant;
using Axe_interDT.Views.BaseDeDonnees.Immeuble;
using Axe_interDT.Views.BaseDeDonnees.Immeuble.Fiche_Mode_Reglement;
using Axe_interDT.Views.BaseDeDonnees.Immeuble.FicheLocalisation;
using Axe_interDT.Views.BaseDeDonnees.Immeuble.Intervenant_Par_Immeuble;
using Axe_interDT.Views.BaseDeDonnees.ParametreGMAO;
using Axe_interDT.Views.Commande.UserPreCommande;
using Axe_interDT.Views.Contrat;
using Axe_interDT.Views.Contrat.FicheGMAO;
using Axe_interDT.Views.Contrat.TriContrat;
using Axe_interDT.Views.Devis;
using Axe_interDT.Views.Devis.Historique_Devis;
using Axe_interDT.Views.DispatchDevis;
using Axe_interDT.Views.DispatchIntervention;
using Axe_interDT.Views.DispatchIntervention.ControleDesHeures;
using Axe_interDT.Views.DispatchIntervention.HistoriqueIntervention;
using Axe_interDT.Views.Facturations.VisualiserUneFacture;
using Axe_interDT.Views.Facture_Mannuel;
using Axe_interDT.Views.FicheGestionAppel;
using Axe_interDT.Views.FicheVehicule;
using Axe_interDT.Views.FicheVisiteEntretient;
using Axe_interDT.Views.Fournisseur.HistoriqueDesBonsDeCommande;
using Axe_interDT.Views.Fournisseurs.Fournisseur;
using Axe_interDT.Views.Fournisseurs.HistoriqueFactureFournisseur;
using Axe_interDT.Views.Fournisseurs.ListingDesArticlesCommandes;
using Axe_interDT.Views.Fournisseurs.RetourDesBondeCommande;
using Axe_interDT.Views.Intervention;
using Axe_interDT.Views.Les_Articles;
using Axe_interDT.Views.Parametrages;
using Axe_interDT.Views.Parametrages.ChargePersonnel;
using Axe_interDT.Views.Planning;
using Axe_interDT.Views.PorteFeuilleContrat;
using Axe_interDT.Views.ReleveDesCompteurs;
using Axe_interDT.Views.Theme;
using Axe_interDT.Views.Theme.CustomContectMenu;
using Axe_interDT.Views.Theme.Menu;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinSchedule;
using Microsoft.Win32;
using System;
using System.Collections.Generic;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Threading;
using System.Windows.Forms;

namespace Axe_interDT.View.Theme
{
    static class Theme
    {

        #region Variables

        public static string guid { get; set; } = "";
        public static string DocumentFolderPath { get; } = Environment.GetFolderPath(Environment.SpecialFolder.MyDocuments);
        public static Alert alert { get; set; }
        public static string Axe_interDTFolder { get; } = $"{DocumentFolderPath}\\Axe_interDT";
        public static string LogFolder { get; } = $"{Axe_interDTFolder}\\Log";
        public static string ThemeFolder { get; } = $"{Axe_interDTFolder}\\Theme";
        public static string LogoFolder { get; } = $"{Axe_interDTFolder}\\Logo";
        //===> Mondir le 09.11.2020, demande de Xavier par telephone
        public static string InfoFolder { get; } = $"{Axe_interDTFolder}\\Info";
        public static string InfoFile { get; } = $"{InfoFolder}\\Info.txt";
        //===> Fin Modif Mondir
        //===> Mondir le 30.04.2021, Create excel folder https://groupe-dt.mantishub.io/view.php?id=2423
        public static string ExcelFolder { get; } = $"{Axe_interDTFolder}\\Excel";
        //===> Fin Modif Mondir

        public static UserDocFichePrincipal MainForm { get; set; } //  This Is The Main Form, GenWin
        public static bool NextIsNull { get; set; }
        public static bool PreviousIsNull { get; set; }
        private static bool NeedsToDlete;
        private static List<Type> ListType = new List<Type>();
        private static int Navigation;
        public static Views.Theme.Menu.MainMenu MainMenu { get; set; }
        public static Dictionary<string, Type> ControlToLoadOnLunch { get; } =
            new Dictionary<string, Type>() { { "Client", typeof(UserDocClient) }, { "Immeuble", typeof(UserDocImmeuble) },
                { "Groupe", typeof(Views.BaseDeDonnees.Groupe.UserDocGroupe) }/*, { "Devis", typeof(UserDocDevis) }*/ , { "Home", typeof(Home) } };
        /// <summary>
        /// This Prop is a global prop to set the back color for all controls
        /// </summary>
        public static Color ControlsBackColor { get; } = Color.FromArgb(192, 192, 192);

        //=============> Mondir 04.06.2020 : UserDocImprimeFacture Ajouté demande de Frédéric, Donner le droit pour Sabrina Ramos
        //============> Mondir 10.11.2020 : demande de Rachid de supprimer UserDocImprimeFacture en prod
        public static Type[] AutorisedControls { get; set; } = { typeof(Views.Appel.UserDocStandard), typeof(UserDocClient), typeof(Views.BaseDeDonnees.Groupe.UserDocGroupe),
            typeof(UserDocReglement),typeof(UserDocLocalisation),typeof(UserDocImmeuble),typeof(UserPreCommande),
            typeof(UserDocHistoDevis),typeof(UserDocDevis),typeof(UserDispatchDevis),typeof(UserDocCtrlHeurePers),
            typeof(UserDocHistoriqueInterv),typeof(UserDispatch),typeof(UserDocVehicule),typeof(UserIntervention),
            typeof(UserDocFournisseur), typeof(UserBCLivraison), typeof(UserDocArticle), typeof(UserDocParamtre),
            typeof(UserDocCharge), typeof(UserDocPlanningCodeJock), typeof(UserDocRelTech), typeof(UserDocFournisseur),
            typeof(UserReceptMarchandise), typeof(UserBCLivraison), typeof(UserListeArticle), typeof(UserHistoReadSoft), typeof(UserDocSageScan),
            typeof(UserDocAnalytique2), typeof(UserPorteFeuilleContrat), typeof(UserDocAnalyTravaux), typeof(UserDocContratAnalytique),
            typeof(UserAnalyseContrat), typeof(UserDocAnalyse),typeof(UserDocMail),typeof(UserDocPrintFacture), typeof(UserDocFacManuel), typeof(UserDocFicheContrat),
            typeof(Home), typeof(UserDocExportImmAstreinte), typeof(UserDocP2V2), typeof(UserDocTriContat),
            //===> Mondir le 24.12.2020, added ContextMenuChoixDeSociete to Prod
            typeof(UserIntervP2V2), typeof(UserDocSuiviRelevesV2), typeof(UserDocIntervenantParImmeuble), typeof(ContextMenuChoixDeSociete),
            typeof(UserDocGestionAppel), typeof(UserDocParametreV2)
        };
        public static int IsLoading { get; set; } = 0;
        public static Thread SplashScreenThread { get; set; }
        public static bool ActivateSplashScreen { get; set; } = true;
        public static bool killSplashScreen { get; set; } = false;
        public static Control ControlAffiche;
        //==============> Added By Mondir
        //if BugFound the red alert will be hidden after 20sec, even there is Green altert after red one,
        //coz sometimes we have a Red alert then a green alert, so the Green one will override the Red one
        //ex : buttun_save()
        //{
        //      CallingASaveMethod() // This this method we have a bug that will show a red alert
        //      ShowGreenAlert() // This will show the green alert, The result that we will not see the red alert that happend before
        //}
        public static bool BugFound { get; set; }

        /// ====> Mondir le 02.07.2020, Added this property to alert the user
        /// ====> If true, is means that we have a new version and we need to alert the user that there is a new version,
        /// ====> If true, Th header will have only this text = 'Une Mise à jour d'intranet est disponible, Veuillez relancer l'application' and will not be able to change it when navigating
        public static bool NewVersionFound { get; set; }
        /// ====> Fin Modif Mondir
        ///

        //===> Mondir le 24.07.2020, Muti soc coleurs
        //===> Mondir le 27.07.2020, Not accepted by Xavier
        //public static Color LongMainColor = Color.FromArgb(211, 51, 22);
        //public static Color LongMainColor = Color.Black;
        //public static Color DTMainColor = Color.FromArgb(32, 58, 69);

        #endregion

        #region App Directories
        public static void CreateApplicationFolders()
        {
            CreateLogFolder();
            CreateThemeFolder();
            CreateLogoFolder();
            //===> Mondir le 09.11.2020, demande de Xavier par telephone
            CreateInfoFolder();
            //===> Fin Modif Mondir
            //===> Mondir le 30.04.2021, Create excel folder https://groupe-dt.mantishub.io/view.php?id=2423
            CreateExcelFolder();
            //===> Fin Modif Mondir
        }

        /// <summary>
        /// Mondir le 30.04.2021, Create excel folder https://groupe-dt.mantishub.io/view.php?id=2423
        /// </summary>
        private static void CreateExcelFolder()
        {
            if (!Directory.Exists(ExcelFolder))
                Directory.CreateDirectory(ExcelFolder);
        }

        /// <summary>
        ///  //===> Mondir le 09.11.2020, demande de Xavier par telephone
        /// 
        /// </summary>
        private static void CreateInfoFolder()
        {
            if (!Directory.Exists(InfoFolder))
                Directory.CreateDirectory(InfoFolder);
        }
        private static void CreateLogoFolder()
        {
            if (!Directory.Exists(LogoFolder))
                Directory.CreateDirectory(LogoFolder);
        }
        private static void CreateLogFolder()
        {
            if (!Directory.Exists(LogFolder))
                Directory.CreateDirectory(LogFolder);
        }
        private static void CreateThemeFolder()
        {
            if (!Directory.Exists(ThemeFolder))
                Directory.CreateDirectory(ThemeFolder);
        }
        #endregion

        #region Main functions (Navigate)
        public static Control Navigate(Type TypeToShow, bool AddTypeToNavigationList = true)
        {
            Control TmpControl = null;

            try
            {
                if (General._ExecutionMode == General.ExecutionMode.Prod)
                    //if (General.fncUserName().ToUpper() != "FL")
                    if (!AutorisedControls.Contains(TypeToShow))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas le droit", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        return null;
                    }

                if (NeedsToDlete)
                {
                    for (int i = ListType.Count - 1; i > Navigation; i--)
                        ListType.RemoveAt(i);
                    NeedsToDlete = false;
                }

                MainForm.PanelMainControl.Controls.Cast<Control>().ToList().ForEach(ele => ele.Visible = false); // Hide All Controls

                if (AddTypeToNavigationList || TypeToShow == typeof(Home))
                {
                    if (ListType.Count == 0)
                        ListType.Add(TypeToShow);
                    else
                    {

                        if (ListType.Last() != TypeToShow)
                            ListType.Add(TypeToShow);
                    }
                    Navigation = ListType.Count - 1;
                }

                TmpControl =
                    MainForm.PanelMainControl.Controls.Cast<Control>().FirstOrDefault(ele => ele.GetType() == TypeToShow);

                sendMessageToLog($"Navigating To : {TypeToShow}");

                if (TmpControl == null) // Create The Control
                {
                    ///=============> code added by mohammed
                    Control Control;
                    if (TypeToShow == typeof(ContextMenuChoixDeSociete))
                    {
                        Control = MainForm.ContextMenuChoixDeSociete;
                    }
                    else
                    {
                        Control = (Control)Activator.CreateInstance(TypeToShow);
                    }
                    //Control.Width = 1521;
                    //Control.Height = 859;
                    //Control.Size = new System.Drawing.Size(1521, 859);
                    Control.Dock = DockStyle.Fill;
                    Control.BackColor = ControlsBackColor;

                    TmpControl = Control;

                    GenericFonctionalityOnControls(TmpControl, TmpControl.Name);

                    Control.Visible = false;
                    MainForm.PanelMainControl.Controls.Add(Control);
                    ControlAffiche = Control;
                    Control.BringToFront();
                    Control.Show();
                    Control.Visible = true;
                }
                else
                {
                    ControlAffiche = TmpControl;
                    // TmpControl.BringToFront();
                    TmpControl.Focus();
                    TmpControl.Visible = true;

                }

                ChangeNavigationStatus();
                if (AddTypeToNavigationList)
                    ChangeMainFormHeaderLabel(TmpControl);
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
            return TmpControl;
        }
        #endregion

        public static void sendMessageToLog(string msg)
        {
            if (ControlAffiche != null)
            {
                var controlOnTop = ControlAffiche?.GetType();

                Program.log.Info($"GUID : {guid};Control On Top : {controlOnTop};{msg}");
            }
        }

        #region Theme
        public static void CopyTheme(bool force = false)
        {
            if (!Directory.Exists(ThemeFolder))
                Directory.CreateDirectory(ThemeFolder);

            //TODO : Mondir - To check
            //if (!File.Exists($@"{ThemeFolder}\AeroV2.isl") || force)
            //Try catch added by Mondir, to fix a bug caused by notificationIcon_Click in UserDocFichePrincipal
            File.WriteAllBytes($@"{ThemeFolder}\AeroV2.isl", Properties.Resources.AeroV2);
            //try
            //{

            //}
            //finally
            //{

            //}
        }

        private static void ChangeNavigationStatus(Control C = null)
        {
            if (C is Home)
                MainMenu.ShowHomeMenu("UserDocFichePrincipal");
            if (ListType.Count == 1 && ListType[0] == typeof(Home))
            {
                MainForm.buttonNext.Enabled = false;
                NextIsNull = true;
                PreviousIsNull = true;
                return;
            }
            if (ListType.Count == 0)
            {
                NextIsNull = true;
                MainForm.buttonNext.Enabled = false;
            }
            else
            {
                NextIsNull = false;
                MainForm.buttonNext.Enabled = true;
            }
            if (Navigation == 0)
            {
                PreviousIsNull = true;
                MainForm.buttonBack.Enabled = false;
            }
            else
            {
                PreviousIsNull = false;
                MainForm.buttonBack.Enabled = true;
            }
            if (Navigation == ListType.Count - 1)
            {
                NextIsNull = true;
                MainForm.buttonNext.Enabled = false;
            }
        }
        public static void ChangeMainFormHeaderLabel(Control C)
        {
            //===> Mondir le 02.07.2020, look at variable declaration
            if (NewVersionFound)
                return;
            //===> Fin Modif Mondir

            if (C != null && C.Tag != null)
                MainForm.LabelHeader.Text = C.Tag.ToString();
            else
                MainForm.LabelHeader.Text = "";
        }
        public static void Previous()
        {
            Navigation--;
            if (Navigation < 0)
                Navigation = 0;
            NeedsToDlete = true;
            MainForm.PanelMainControl.Controls.Cast<Control>().ToList().ForEach(ele => ele.Visible = false); // Hide All Controls
            var TmpControl =
                MainForm.PanelMainControl.Controls.Cast<Control>().First(ele => ele.GetType() == ListType[Navigation]);
            sendMessageToLog($"Navigating To : {TmpControl}");
            ControlAffiche = TmpControl;
            TmpControl.Visible = true;
            TmpControl.BringToFront();
            ChangeNavigationStatus(TmpControl);
            ChangeMainFormHeaderLabel(TmpControl);
        }
        public static void Next()
        {
            NeedsToDlete = false;
            Navigation++;
            if (Navigation > ListType.Count - 1)
                Navigation = ListType.Count - 1;
            MainForm.PanelMainControl.Controls.Cast<Control>().ToList().ForEach(ele => ele.Visible = false); // Hide All Controls
            var TmpControl =
                MainForm.PanelMainControl.Controls.Cast<Control>().First(ele => ele.GetType() == ListType[Navigation]);
            sendMessageToLog($"Navigating To : {TmpControl}");
            ControlAffiche = TmpControl;
            TmpControl.Visible = true;
            TmpControl.BringToFront();
            ChangeNavigationStatus(TmpControl);
            ChangeMainFormHeaderLabel(TmpControl);
        }
        public static void SetDateForCalendar(UltraCalendarCombo calendarCombo, iTalk.iTalk_TextBox_Small2 textBox)
        {
            ///Function added by mohammed
            DateTime date = DateTime.Now;
            if (!string.IsNullOrEmpty(textBox.Text) && General.IsDate(textBox.Text))
                date = Convert.ToDateTime(textBox.Text);

            var calendarInfo = calendarCombo.CalendarInfo;

            calendarInfo.SelectedDateRanges.Clear();

            calendarCombo.Value = date;

            calendarInfo.ActivateDay(date);

            if (calendarInfo.ActiveDay != null)
                calendarInfo.ActiveDay.Selected = true;

            calendarCombo.FirstMonth = calendarCombo.CalendarInfo.GetMonth(date);
        }

        public static void AfficheAlertSucces()
        {
            //See the decalaration of BugFound to understand
            if (BugFound)
                return;

            if (MainForm != null)
            {
                if (alert == null)
                {
                    alert = new Alert();
                }
                alert.MessageType = Alert.EnumMessageType.Success;
                alert.Message = "l'enregistrement de données a été effectué avec succes";
                alert.Dock = DockStyle.Fill;

                MainForm.panelAlert.Controls.Add(alert);
                MainForm.panelAlert.Visible = true;
                alert.Visible = true;
            }

        }
        public static void AfficheAlertDanger()
        {
            if (MainForm != null)
            {
                BugFound = true;
                //affichage d'un message pour informer l'utilisateur qu'il y a un beug
                if (alert == null)
                {
                    alert = new Alert();
                }
                alert.MessageType = Alert.EnumMessageType.Danger;
                alert.Message = "une erreur s'est produite lors du traitement !";
                alert.Dock = DockStyle.Fill;
                alert.HideAfter = Alert.EnumHideAfter.TwentySeconds;

                MainForm.panelAlert.Controls.Add(alert);
                MainForm.panelAlert.Visible = true;
                alert.Visible = true;
            }
        }

        #endregion

        #region Traitement sur tous les controles
        public static void recursiveLoopOnFrms(Form form)
        {
            foreach (Control C in form.Controls)
                GenericFonctionalityOnFrms(C, form.Name);
        }
        public static void GenericFonctionalityOnControls(Control C, string parentName)
        {
            foreach (Control CC in C.Controls)
            {
                if (CC is UltraGrid)
                {
                    setFoncionalityOnGrid(CC, parentName);
                }

                // La partie de TabIndeex
                if (CC is iTalk.iTalk_TextBox_Small2 == false && CC is UltraCombo == false)
                    CC.TabStop = true;



                if (CC.HasChildren)
                    GenericFonctionalityOnControls(CC, parentName);
            }
        }
        public static void GenericFonctionalityOnFrms(Control C, string parentName)
        {
            if (C is UltraGrid)
            {
                setFoncionalityOnGrid(C, parentName);
            }
            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                {
                    // La partie de TabIndeex
                    if (CC is iTalk.iTalk_TextBox_Small2 == false && CC is UltraCombo == false)
                        CC.TabStop = true;

                    //if (CC.HasChildren)
                    GenericFonctionalityOnFrms(CC, parentName);
                }
        }
        #endregion

        #region Traitement sur les grilles
        private static void setFoncionalityOnGrid(Control C, string parentName)
        {
            //var tmpControl = CC as UltraGrid;
            //tmpControl.DisplayLayout.Override.RowSizing = RowSizing.AutoFree;

            foreach (var Col in (C as UltraGrid).DisplayLayout.Bands[0].Columns)
                Col.CellMultiLine = DefaultableBoolean.True;

            var ultraGrid = C as UltraGrid;
            ultraGrid.Tag = parentName;


            ultraGrid.InitializeLayout += CC_InitializeLayout;

            ultraGrid.KeyDown += CC_KeyDown;

            PreventChangingValueByScroll.InitGrid(ultraGrid);

            //Enable Scroll on cell Edit see: https://groupe-dt.mantishub.io/view.php?id=1339
            ultraGrid.ControlAdded += CC_ControlAdded;

            //================== This Event is to save the column with and position ... for every column and for every grid on the application
            ultraGrid.AfterColPosChanged += (se, ev) =>
            {
                foreach (UltraGridColumn col in ultraGrid.DisplayLayout.Bands[0].Columns)
                {
                    General.saveInReg(parentName, ultraGrid.Name + "_" + col.Key + "_Width", col.Width.ToString());
                    General.saveInReg(parentName, ultraGrid.Name + "_" + col.Key + "_VisiblePosition", col.Header.VisiblePosition.ToString());
                }
            };

            //================= This Event is to save row Height
            ultraGrid.AfterRowResize += (se, ev) =>
            {
                General.saveInReg(parentName, ultraGrid.Name + "_RowHeight", ev.Row.Height.ToString());
            };
        }
        private static void CC_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //return;

            var grid = sender as UltraGrid;

            //===> Mondir le 12.02.2021 https://groupe-dt.mantishub.io/view.php?id=2329#c5795
            var tag = grid.Tag?.ToString().ToLower();
            var name = grid.Name.ToLower();

            //===> Commented line by Mondir
            //if (grid.Tag != null && grid.Tag.ToString().ToLower() == ("UserDocDevis").ToLower() && grid.Name.ToLower() == ("SSDBGridCorps").ToLower())
            if ((tag == "UserDocDevis".ToLower() && name == "SSDBGridCorps".ToLower()) || (tag == "UserDocFacManuel".ToLower() && name == "SSOleDBGrid1".ToLower()))
            {
                //ne fait rien 
            }
            else
            {
                grid.DisplayLayout.DefaultSelectedBackColor = Color.Empty;
                grid.DisplayLayout.DefaultSelectedForeColor = Color.Empty;
                grid.DisplayLayout.Override.ResetActiveRowAppearance();
                grid.DisplayLayout.Override.ResetSelectedRowAppearance();
                grid.DisplayLayout.Override.SelectedAppearancesEnabled = DefaultableBoolean.False;
                grid.DisplayLayout.Override.ResetActiveRowCellAppearance();
                //grid.DisplayLayout.Override.ResetActiveCellAppearance();
                //grid.DisplayLayout.Override.ResetActiveCellAppearance();

            }
            foreach (UltraGridColumn col in grid.DisplayLayout.Bands[0].Columns)
            {
                //==================== get the column with from registry
                if (grid.Tag != null)
                {
                    //This line is added to stope AfterColPosChanged from firring
                    grid.EventManager.AllEventsEnabled = false;

                    var width = General.getFrmReg(grid.Tag.ToString(), grid.Name + "_" + col.Key + "_Width", "-1");
                    if (width != "-1")
                        col.Width = Math.Abs(Convert.ToInt32(width));

                    var visibePosition = General.getFrmReg(grid.Tag.ToString(), grid.Name + "_" + col.Key + "_VisiblePosition", "-1");
                    if (visibePosition != "-1")
                        col.Header.VisiblePosition = Convert.ToInt32(visibePosition);

                    var rowHeight = General.getFrmReg(grid.Tag.ToString(), grid.Name + "_RowHeight", "-1");
                    if (rowHeight != "-1")
                        grid.DisplayLayout.Override.DefaultRowHeight = Convert.ToInt32(rowHeight);

                    grid.EventManager.AllEventsEnabled = true;
                }

                //==============> Look at : https://groupe-dt.mantishub.io/view.php?id=1116
                //if (grid.Tag != null && grid.Tag.ToString().ToUpper() == "UserDocFacManuel".ToUpper() && col.Key.ToUpper() == "AnaActivite".ToUpper())
                //    return;

                //if (col.ValueList != null)
                //    col.CellClickAction = CellClickAction.CellSelect;

                ///Added by mohammed to solve the error http://193.248.37.214:30002/issues/465.
                if (col.DataType == typeof(string) && col.ValueList == null)// if added by mohammed to add cellMultiline just for columns datatype of string. Also remove multiline for columns that contains combobox
                {
                    col.CellMultiLine = DefaultableBoolean.True;
                }

            }
        }
        private static void CC_KeyDown(object sender, KeyEventArgs e)
        {
            var grid = (UltraGrid)sender;
            var activeCell = grid.ActiveCell;


            if (activeCell == null || activeCell.IsInEditMode == false)
            {
                return;
            }

            var editor = activeCell.EditorResolved;

            if (activeCell.StyleResolved == Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox)
            {
                switch (e.KeyCode)
                {
                    case Keys.Left:

                        grid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                        e.Handled = true;
                        e.SuppressKeyPress = true;
                        grid.PerformAction(UltraGridAction.PrevCellByTab);
                        grid.PerformAction(UltraGridAction.EnterEditMode);

                        break;
                    case Keys.Right:


                        grid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                        e.Handled = true;
                        e.SuppressKeyPress = true;
                        grid.PerformAction(UltraGridAction.NextCellByTab);
                        grid.PerformAction(UltraGridAction.EnterEditMode);

                        break;
                    case Keys.Down:

                        grid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                        e.Handled = true;
                        e.SuppressKeyPress = true;
                        grid.PerformAction(UltraGridAction.BelowCell);
                        grid.PerformAction(UltraGridAction.EnterEditMode);


                        break;
                    case Keys.Up:

                        grid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                        e.Handled = true;
                        e.SuppressKeyPress = true;
                        grid.PerformAction(UltraGridAction.AboveCell);
                        grid.PerformAction(UltraGridAction.EnterEditMode);


                        break;

                }
                return;
            }
            else if (editor.SupportsSelectableText == true)
            {
                switch (e.KeyCode)
                {
                    case Keys.Left:
                        if (editor.SelectionStart == 0)
                        {
                            grid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                            e.Handled = true;
                            e.SuppressKeyPress = true;
                            grid.PerformAction(UltraGridAction.PrevCellByTab);
                            grid.PerformAction(UltraGridAction.EnterEditMode);
                        }
                        break;
                    case Keys.Right:
                        if (editor.SelectionLength == editor.TextLength || editor.SelectionStart == editor.TextLength)
                        {
                            grid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                            e.Handled = true;
                            e.SuppressKeyPress = true;
                            grid.PerformAction(UltraGridAction.NextCellByTab);
                            grid.PerformAction(UltraGridAction.EnterEditMode);

                        }
                        break;
                    case Keys.Down:

                        if (editor.IsDroppedDown == false)
                        {
                            if (editor.SelectionLength == editor.TextLength || editor.SelectionStart == editor.TextLength)
                            {
                                grid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                                e.Handled = true;
                                e.SuppressKeyPress = true;
                                grid.PerformAction(UltraGridAction.BelowCell);
                                grid.PerformAction(UltraGridAction.EnterEditMode);
                            }
                        }
                        break;
                    case Keys.Up:
                        if (editor.IsDroppedDown == false)
                        {
                            if (editor.SelectionLength == editor.TextLength || editor.SelectionStart == editor.TextLength)
                            {
                                grid.PerformAction(UltraGridAction.ExitEditMode, false, false);
                                e.Handled = true;
                                e.SuppressKeyPress = true;
                                grid.PerformAction(UltraGridAction.AboveCell);
                                grid.PerformAction(UltraGridAction.EnterEditMode);
                            }
                        }
                        break;

                }
            }
        }
        private static void CC_ControlAdded(object sender, ControlEventArgs e)
        {
            //Ahmed: This function was used to fix Grids Scroll issue When the cells are in edit mode for more see : https://groupe-dt.mantishub.io/view.php?id=1339

            if (e.Control.GetType() == typeof(EmbeddableTextBoxWithUIPermissions))
            {
                EmbeddableTextBoxWithUIPermissions textbox = (EmbeddableTextBoxWithUIPermissions)e.Control as EmbeddableTextBoxWithUIPermissions;
                textbox.MouseWheel += new MouseEventHandler((object se, MouseEventArgs ev) =>
                {
                    var grid = (UltraGrid)sender;

                    if (ev.Delta < 0)
                        grid.ActiveRowScrollRegion.Scroll(Infragistics.Win.UltraWinGrid.RowScrollAction.LineDown);
                    else grid.ActiveRowScrollRegion.Scroll(Infragistics.Win.UltraWinGrid.RowScrollAction.LineUp);

                });
            }
        }

        public static void ChangeCompany()
        {



            //===> Mondir le 23.07.2020, 
            var societe = "S1";
            if (Registry.GetValue($@"HKEY_CURRENT_USER\SOFTWARE\{General.cFrMultiSoc}\Param", "Societe", "")?.ToString() == "S6")
                societe = "S6";
            //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
            else if (Registry.GetValue($@"HKEY_CURRENT_USER\SOFTWARE\{General.cFrMultiSoc}\Param", "Societe", "")?.ToString() == "S7")
                societe = "S7";
            //===> Fin Modif Mondir

            if (societe == "S1")
            {
                Theme.MainForm.Text = $"Delostal & Thibault V{General.Version}.{General.Commit} - {General.VersionDate} - {General._ExecutionMode}";
                Theme.MainForm.imgLogoSociete.Image = Properties.Resources.logodt_295__53;
            }
            else if (societe == "S6")
            {
                Theme.MainForm.Text = $"LONG V{General.Version}.{General.Commit} - {General.VersionDate} - {General._ExecutionMode}";
                //Theme.MainForm.tableLayoutPanel10.BackColor = LongMainColor;
                //Theme.MainForm.imgLogoSociete.BackColor = LongMainColor;
                //Theme.MainForm.tableLayoutPanel2.BackColor = LongMainColor;
                //Theme.MainForm.tableLayoutPanel10.BackColor = LongMainColor;
                //Theme.MainForm.LabelHeader.BackColor = LongMainColor;
                Theme.MainForm.imgLogoSociete.Image = Properties.Resources.logo_long_finale;
            }
            else if (societe == "S7")
            {
                Theme.MainForm.Text = $"Van Froid V{General.Version}.{General.Commit} - {General.VersionDate} - {General._ExecutionMode}";
                Theme.MainForm.imgLogoSociete.Image = Properties.Resources.logo_vf;
            }
        }

        #endregion


    }
}