﻿namespace Axe_interDT.Views.Theme
{
    partial class SplashScreen
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.labelCompany = new System.Windows.Forms.Label();
            this.LabelVersion = new System.Windows.Forms.Label();
            this.LabelProgressBar = new System.Windows.Forms.Label();
            this.LabelLoading = new System.Windows.Forms.Label();
            this.LabelClose = new System.Windows.Forms.Label();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.labs = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // labelCompany
            // 
            this.labelCompany.Font = new System.Drawing.Font("Ubuntu", 15.75F);
            this.labelCompany.ForeColor = System.Drawing.Color.White;
            this.labelCompany.Location = new System.Drawing.Point(3, 52);
            this.labelCompany.Name = "labelCompany";
            this.labelCompany.Size = new System.Drawing.Size(405, 26);
            this.labelCompany.TabIndex = 0;
            this.labelCompany.Text = "Delostal && Thibault";
            this.labelCompany.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.labelCompany.Visible = false;
            // 
            // LabelVersion
            // 
            this.LabelVersion.AutoSize = true;
            this.LabelVersion.Font = new System.Drawing.Font("Ubuntu", 8F);
            this.LabelVersion.ForeColor = System.Drawing.Color.White;
            this.LabelVersion.Location = new System.Drawing.Point(295, 75);
            this.LabelVersion.Name = "LabelVersion";
            this.LabelVersion.Size = new System.Drawing.Size(30, 16);
            this.LabelVersion.TabIndex = 1;
            this.LabelVersion.Text = "V0.0";
            this.LabelVersion.Visible = false;
            // 
            // LabelProgressBar
            // 
            this.LabelProgressBar.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.LabelProgressBar.Location = new System.Drawing.Point(0, 131);
            this.LabelProgressBar.Name = "LabelProgressBar";
            this.LabelProgressBar.Size = new System.Drawing.Size(5, 15);
            this.LabelProgressBar.TabIndex = 2;
            // 
            // LabelLoading
            // 
            this.LabelLoading.AutoSize = true;
            this.LabelLoading.Font = new System.Drawing.Font("Ubuntu", 8F);
            this.LabelLoading.ForeColor = System.Drawing.Color.White;
            this.LabelLoading.Location = new System.Drawing.Point(123, 102);
            this.LabelLoading.Name = "LabelLoading";
            this.LabelLoading.Size = new System.Drawing.Size(156, 16);
            this.LabelLoading.TabIndex = 3;
            this.LabelLoading.Text = "Chargement du module xxx ...";
            // 
            // LabelClose
            // 
            this.LabelClose.AutoSize = true;
            this.LabelClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LabelClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F);
            this.LabelClose.ForeColor = System.Drawing.Color.White;
            this.LabelClose.Location = new System.Drawing.Point(378, 9);
            this.LabelClose.Name = "LabelClose";
            this.LabelClose.Size = new System.Drawing.Size(17, 17);
            this.LabelClose.TabIndex = 4;
            this.LabelClose.Text = "X";
            this.LabelClose.Click += new System.EventHandler(this.LabelClose_Click);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // labs
            // 
            this.labs.BackColor = System.Drawing.Color.White;
            this.labs.Location = new System.Drawing.Point(0, 131);
            this.labs.Name = "labs";
            this.labs.Size = new System.Drawing.Size(408, 15);
            this.labs.TabIndex = 5;
            // 
            // SplashScreen
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.ClientSize = new System.Drawing.Size(408, 170);
            this.Controls.Add(this.LabelProgressBar);
            this.Controls.Add(this.labs);
            this.Controls.Add(this.LabelClose);
            this.Controls.Add(this.LabelLoading);
            this.Controls.Add(this.LabelVersion);
            this.Controls.Add(this.labelCompany);
            this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
            this.Name = "SplashScreen";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
            this.Text = "SplashScreen";
            this.Load += new System.EventHandler(this.SplashScreen_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label labelCompany;
        private System.Windows.Forms.Label LabelVersion;
        private System.Windows.Forms.Label LabelProgressBar;
        private System.Windows.Forms.Label LabelLoading;
        private System.Windows.Forms.Label LabelClose;
        private System.Windows.Forms.Timer timer1;
        private System.Windows.Forms.Label labs;
    }
}