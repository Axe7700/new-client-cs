﻿using Axe_interDT.Shared;
using System;
using System.Diagnostics;
using System.IO;
using System.Windows.Forms;

namespace Axe_interDT.Views.Theme
{
    public partial class Info : UserControl
    {
        public enum InfoFormat
        {
            Rounded,
            Squar
        }
        public string Link { get; set; }
        private InfoFormat _Format;

        public InfoFormat Format
        {
            get { return _Format; }
            set
            {
                if (value == InfoFormat.Squar)
                    BackgroundImage = Properties.Resources.info_1_squar;
                else
                    BackgroundImage = Properties.Resources.info_1;
                _Format = value;
            }
        }

        public Info()
        {
            InitializeComponent();
        }

        private void timerChangeBack_Tick(object sender, EventArgs e)
        {
            //===> Mondir le 09.11.2020, demande de Xavier par telephone
            //BackgroundImage = swap ? Properties.Resources.info_1 : Properties.Resources.info_2;
            //swap = !swap;

            //For this type, no need to change status
            if (_Format == InfoFormat.Squar)
                return;

            //Check if we need to show the info button
            if (!General.showInfo)
                return;

            //Check if the info button already seen
            if (View.Theme.Theme.InfoFile.FileExists(true) &&
                File.ReadAllText(View.Theme.Theme.InfoFile).Contains($"V{General.Version}"))
                return;

            BackgroundImage = Properties.Resources.info_2;

            timerChangeBack.Stop();
        }

        private void Info_Load(object sender, EventArgs e)
        {
            //===> Mondir le 09.11.2020, demande de Xavier par telephone
            timerChangeBack.Start();
        }

        private void Info_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(Link))
                throw new Exception("Please add a value to Link property");

            Process.Start(Link);

            //If the format is squar, no need to change image
            if (_Format == InfoFormat.Squar)
                return;

            //===> Mondir le 09.11.2020, demande de Xavier par telephone
            BackgroundImage = Properties.Resources.info_1;
            File.AppendAllText(View.Theme.Theme.InfoFile, $"V{General.Version}{Environment.NewLine}");
            //===> Fin Modif Mondir
        }
    }
}
