﻿using Axe_interDT.Shared;
using Axe_interDT.View.Theme;
using Microsoft.VisualBasic;
using System;
using System.Data;
using System.Data.SqlClient;
using System.Windows.Forms;

namespace Axe_interDT.Views.Theme.CustomContectMenu
{
    public partial class ContextMenuChoixDeSociete : UserControl
    {
        public string soledb = "";

        public string NouvBase = "";
        public string NomServeur = "";
        public string NomBase = "";
        public string ChoixSociete = "";

        public string sColor = "";

        public string BaseS1 = "";
        public string ServeurS1 = "";

        public string BaseS2 = "";
        public string ServeurS2 = "";

        public string BaseS3 = "";
        public string ServeurS3 = "";

        public string BaseS4 = "";
        public string ServeurS4 = "";

        public string BaseS5 = "";
        public string ServeurS5 = "";

        public string BaseS6 = "";
        public string ServeurS6 = "";

        //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
        public string BaseS7 = "";
        public string ServeurS7 = "";
        //===> Fin Modif Mondir

        public ContextMenuChoixDeSociete()
        {
            InitializeComponent();
        }

        private async void cmdValider_Click(object sender, EventArgs e)
        {
            //===> Mondir le 23.07.2020, force the user to use the avariable database
            //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
            if (General._ExecutionMode == General.ExecutionMode.Test)
            {
                if (txtNomBase.Text.ToUpper() != "AXECIEL_DELOSTAL_PDA" &&
                    txtNomBase.Text.ToUpper() != "AXECIEL_LONG_PDA" &&
                    txtNomBase.Text.ToUpper() != "AXECIEL_VanFroid_PDA".ToUpper())
                {
                    CustomMessageBox.CustomMessageBox.Show("Les valeurs possible :\n- AXECIEL_DELOSTAL_PDA\n- AXECIEL_LONG_PDA", "Erreur",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            else if (General._ExecutionMode == General.ExecutionMode.Prod)
            {
                //====> Mondir 27.07.2020, Desable in Prod
                //====> Mondir le 28.12.2020, activer en prod
                //CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas modifier la base de données", "Erreur",
                //    MessageBoxButtons.OK, MessageBoxIcon.Error);
                //return;
                //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
                if (txtNomBase.Text.ToUpper() != "AXECIEL_DELOSTAL" &&
                    txtNomBase.Text.ToUpper() != "AXECIEL_LONG" &&
                    txtNomBase.Text.ToUpper() != "AXECIEL_VanFroid".ToUpper())
                {
                    CustomMessageBox.CustomMessageBox.Show("Les valeurs possible :\n- AXECIEL_DELOSTAL\n- AXECIEL_LONG", "Erreur",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
            }
            Cursor = Cursors.WaitCursor;

            //if (txtNomServeur.Text == "")
            //{
            //    CustomMessageBox.CustomMessageBox.Show("svp veuillez choisi un nom de serveur", "information");
            //    return;
            //}
            //if (txtNomBase.Text == "")
            //{
            //    CustomMessageBox.CustomMessageBox.Show("svp veuillez choisi un nom de base de donnes", "information");
            //    return;
            //}

            soledb = $"SERVER={txtNomServeur.Text};DATABASE={txtNomBase.Text};INTEGRATED SECURITY=TRUE;";

            ///===============> testing if the server and database are 100% correct
            try
            {
                var sqlConnection = new SqlConnection(soledb);

                Cursor = Cursors.WaitCursor;
                View.Theme.Theme.MainForm.Enabled = false;
                if (sqlConnection.State != ConnectionState.Open)
                    await sqlConnection.OpenAsync();
                View.Theme.Theme.MainForm.Enabled = true;
                Cursor = Cursors.Arrow;
                sqlConnection.Close();
                sqlConnection.Dispose();
            }
            catch (Exception ex)
            {
                Cursor = Cursors.Arrow;
                //une erreur liée au reseau -2146232060
                View.Theme.Theme.MainForm.Enabled = true;
                if (ex.Message.Contains("Une erreur liée au réseau ou spécifique à l'instance s'est produite lors de l'établissement d'une connexion à SQL Server"))
                {
                    CustomMessageBox.CustomMessageBox.Show("Ce serveur SQL n'existe pas ou son accès est refusé.", "Access au serveur " + txtNomServeur.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else if (ex.Message.Contains("Impossible d'ouvrir la base de données"))
                {
                    CustomMessageBox.CustomMessageBox.Show("Impossible d'ouvrir la base de données demandée (" + txtNomBase.Text + ")", "Access à la base " + txtNomBase.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                return;
            }
            finally
            {
                View.Theme.Theme.MainForm.Enabled = true;
            }
            General.saveInReg(General.cRegInter + "\\App", General.cLastConnection, soledb, false);

            var AssemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;

            //====> Mondir le 23.07.2020, change changed
            //if (txtNomBase.Text.ToUpper() == General.cNameLong.ToUpper())
            //    General.cFrNomApp = General.cRegInterLong;
            //else if (txtNomBase.Text.ToUpper() == General.cNameGDP.ToUpper())
            //    General.cFrNomApp = General.cRegInterGdp;
            //else if (AssemblyName.ToUpper() == General.cProgIntranetV3PDA.ToUpper())
            //{
            //    if (txtNomBase.Text.ToUpper() == "AXECIEL_DELOSTAL_PDA")
            //        General.cFrNomApp = "InterlibPDA";
            //    else if (txtNomBase.Text.ToUpper() == "AXECIEL_LONG_PDA")
            //        General.cFrNomApp = "InterlibPDAlong";
            //}
            //else
            //    General.cFrNomApp = General.cRegInter;
            if (txtNomBase.Text.ToUpper() == General.cNameLong.ToUpper())
            {
                General.cFrNomApp = $"Axe_interLONG";
                General.cRegInter = $"Axe_interLONG";
            }
            else if (txtNomBase.Text.ToUpper() == General.cNameVF.ToUpper())
            {
                General.cFrNomApp = $"Axe_interVF";
                General.cRegInter = $"Axe_interVF";
            }
            else if (txtNomBase.Text.ToUpper() == General.cNameGDP.ToUpper())
            {
                General.cFrNomApp = General.cRegInterGdp;
            }
            else if (General._ExecutionMode == General.ExecutionMode.Test)
            {
                if (txtNomBase.Text.ToUpper() == "AXECIEL_DELOSTAL_PDA")
                {
                    General.cFrNomApp = $"Axe_interDT_Test";
                    General.cRegInter = $"Axe_interDT_Test";
                }
                else if (txtNomBase.Text.ToUpper() == "AXECIEL_LONG_PDA")
                {
                    General.cFrNomApp = $"Axe_interLONG_Test";
                    General.cRegInter = $"Axe_interLONG_Test";
                }
                //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
                else if (txtNomBase.Text.ToUpper() == "AXECIEL_VanFroid_PDA".ToUpper())
                {
                    General.cFrNomApp = $"Axe_interVF_Test";
                    General.cRegInter = $"Axe_interVF_Test";
                }
                //===> Fin Modif Mondir
            }
            else
                General.cFrNomApp = General.cRegInter;
            //====> Fin Modif Mondir

            if (StockeLienMultiSoc())
            {
                //=== modif rachid du 9 decembre 2009.
                General.saveInReg(General.cRegInter + "\\App", General.DefaultSource, soledb, false);
                General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrNomServer, txtNomServeur.Text, false);
                General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrNomBase, txtNomBase.Text, false);
                General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrSociete, ChoixSociete, false);

                switch (ChoixSociete)
                {
                    case General.S1:
                        General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrBaseS1, txtNomBase.Text, false);
                        General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrServeurS1, txtNomServeur.Text, false);
                        BaseS1 = txtNomBase.Text;
                        ServeurS1 = txtNomServeur.Text;
                        break;
                    case General.S2:
                        General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrBaseS2, txtNomBase.Text, false);
                        General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrServeurS2, txtNomServeur.Text, false);
                        BaseS2 = txtNomBase.Text;
                        ServeurS2 = txtNomServeur.Text;
                        break;
                    case General.S3:
                        General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrBaseS3, txtNomBase.Text, false);
                        General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrServeurS3, txtNomServeur.Text, false);
                        BaseS3 = txtNomBase.Text;
                        ServeurS3 = txtNomServeur.Text;
                        break;
                    case General.S4:
                        General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrBaseS4, txtNomBase.Text, false);
                        General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrServeurS4, txtNomServeur.Text, false);
                        BaseS4 = txtNomBase.Text;
                        ServeurS4 = txtNomServeur.Text;
                        break;
                    case General.S5:
                        General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrBaseS5, txtNomBase.Text, false);
                        General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrServeurS5, txtNomServeur.Text, false);
                        BaseS5 = txtNomBase.Text;
                        ServeurS5 = txtNomServeur.Text;
                        break;
                    case General.S6:
                        General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrBaseS6, txtNomBase.Text, false);
                        General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrServeurS6, txtNomServeur.Text, false);
                        BaseS6 = txtNomBase.Text;
                        ServeurS6 = txtNomServeur.Text;
                        break;
                    //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
                    case General.S7:
                        General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrBaseS7, txtNomBase.Text, false);
                        General.saveInReg(General.cFrMultiSoc + "\\Param", General.cFrServeurS7, txtNomServeur.Text, false);
                        BaseS6 = txtNomBase.Text;
                        ServeurS6 = txtNomServeur.Text;
                        break;
                    //===> Fin Modif Mondir
                    default:
                        break;
                }

                General.lngInitRegitre = 0;


                General.StockeLien();

                //if (!string.IsNullOrEmpty(General.LogoSociete))
                //{
                //    View.Theme.Theme.MainForm.imgLogoSociete.Visible = true;
                //    //View.Theme.Theme.MainForm.imgLogoSociete.Image = Image.FromFile(General.LogoSociete);
                //    Application.DoEvents();
                //}
                //else
                //{
                //    View.Theme.Theme.MainForm.imgLogoSociete.Visible = false;
                //}

                //if (!string.IsNullOrEmpty(General.NomSousSociete))
                //{
                //    View.Theme.Theme.MainForm.Text = $"{General.NomSousSociete} V{Application.ProductVersion}";
                //    Application.DoEvents();
                //}

                //===> Mondir le 23.07.2020, 
                Axe_interDT.View.Theme.Theme.ChangeCompany();

                //this.Hide();
                //cmdQuitter.PerformClick();
                //===> Mondir le 06.11.2020, better navigate to home
                View.Theme.Theme.Navigate(typeof(Home));

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Bienvenue chez " + General.NomSousSociete + ".", "Changement de société", MessageBoxButtons.OK, MessageBoxIcon.Information);

            }

            cmdQuitter.PerformClick();

            if (General._ExecutionMode == General.ExecutionMode.Dev)
                if (General.getFrmReg(General.cFrNomApp + "\\App", "PosteDev", "", false) == "1" && (General.adocnn.DataSource.ToUpper() == "(LOCAL)" || General.adocnn.DataSource.ToUpper() == "."))
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("LOCAL");
                else
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("NOT LOCAL");

            Cursor = Cursors.Default;
        }


        public bool StockeLienMultiSoc()
        {
            bool StockeLienMultiSoc = false;
            int i = 0;

            reConnect:

            try
            {
                General.adocnn = new System.Data.SqlClient.SqlConnection(soledb);

                if (General.adocnn.State != ConnectionState.Open)
                    General.adocnn.Open();

                var tmpModAdo = new ModAdo();

                if (General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test)
                    General.rstmp = tmpModAdo.fc_OpenRecordSet("SELECT code,Adresse FROM lienV2");
                else
                    General.rstmp = tmpModAdo.fc_OpenRecordSet("SELECT code,Adresse FROM lien");
                if (General.getFrmReg(General.cFrNomApp + "\\App", "Dev", "null", false) != "1")
                    if (General.rstmp.Rows.Count > 0)
                        foreach (DataRow row in General.rstmp.Rows)
                            General.saveInReg(General.cFrNomApp + "\\App", row["Code"].ToString(), row["Adresse"].ToString(), false);

                tmpModAdo.Close();
                if (General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test)
                    General.rstmp = tmpModAdo.fc_OpenRecordSet("SELECT NoMaj FROM InitRegitreV2");
                else
                    General.rstmp = tmpModAdo.fc_OpenRecordSet("SELECT NoMaj FROM InitRegitre");
                if (General.rstmp.Rows.Count > 0)
                    General.saveInReg(General.cFrNomApp + "\\App", "NoMaj", General.rstmp.Rows[0]["NoMaj"].ToString(), false);

                tmpModAdo.Close();

                tmpModAdo.Dispose();

                fc_StockeVirgule();

                if (i != 0)
                    fc_InitMultiSoc();

                StockeLienMultiSoc = true;
                return StockeLienMultiSoc;

            }
            catch (Exception ex)
            {
                if (i < 2)
                {
                    if (ex.Message.Contains("Une erreur liée au réseau ou spécifique à l'instance s'est produite lors de l'établissement d'une connexion à SQL Server"))
                    {
                        CustomMessageBox.CustomMessageBox.Show("Ce serveur SQL n'existe pas ou son accès est refusé.", "Access au serveur " + txtNomServeur.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    else if (ex.Message.Contains("Impossible d'ouvrir la base de données"))
                    {
                        CustomMessageBox.CustomMessageBox.Show("Impossible d'ouvrir la base de données demandée (" + txtNomBase.Text + ")", "Access à la base " + txtNomBase.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                    soledb = General.getFrmReg(General.cFrNomApp + "\\App", "Source_OLEDB", "null", false);
                    i++;
                    goto reConnect;
                }
                else
                {
                    CustomMessageBox.CustomMessageBox.Show(ex.Message, "Multi Société", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }


            }
            return StockeLienMultiSoc;
        }

        public void fc_InitMultiSoc()
        {
            if (General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrNomServer, "null", false) == "")
            {
                General.saveInReg(General.cFrMultiSoc + "\\" + "Param", General.cFrNomServer, General.DefaultServer, false);
                NomServeur = General.DefaultServer;
            }
            else
            {
                NomServeur = General.getFrmReg(General.cFrMultiSoc + "\\Param", General.cFrNomServer, "", false);
            }

            txtNomServeur.Text = NomServeur;

            if (General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrNomBase, "null", false) == "")
            {
                General.saveInReg(General.cFrMultiSoc + "\\" + "Param", General.cFrNomBase, BaseS3, false);
                NomBase = BaseS3;
            }
            else
            {
                NomBase = General.getFrmReg(General.cFrMultiSoc + "\\Param", General.cFrNomBase, "", false);
            }

            txtNomBase.Text = NomBase;
            NouvBase = NomBase;

            if (General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrSociete, "null", false) == "")
            {
                //General.saveInReg(General.cFrMultiSoc + "\\" + "Param", General.cFrSociete, General.S1, false);
                General.saveInReg(General.cFrMultiSoc + "\\" + "Param", General.cFrSociete, General.S3, false);
                //ChoixSociete = General.S1;
                ChoixSociete = General.S3;
            }
            else
            {
                ChoixSociete = General.getFrmReg(General.cFrMultiSoc + "\\Param", General.cFrSociete, "", false);
            }
            //from here 
            OptS1.Text = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", "NomS1", "Delostal & Thibault", false);
            OptS2.Text = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", "NomS2", "PEZANT", false);
            OptS3.Text = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", "NomS3", "ECCEP", false);
            OptS4.Text = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", "NomS4", "AMMANN", false);
            OptS5.Text = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", "NomS5", "GAZDEPARIS", false);
            OptS6.Text = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", "NomS6", "LONG", false);
            //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
            OptS7.Text = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", "NomS7", "Van Froid", false);
            //===> Fin Modif Mondir


            var AssemblyName = System.Reflection.Assembly.GetExecutingAssembly().GetName().Name;
            if (AssemblyName.ToUpper() == General.cProgIntranetV3PDA.ToUpper())
            {
                BaseS1 = "AXECIEL_DELOSTAL_PDA";
                ServeurS1 = "DT-SQL-01";
            }
            else
            {
                BaseS1 = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrBaseS1, "", false);
                ServeurS1 = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrServeurS1, "", false);
            }

            BaseS2 = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrBaseS2, "", false);
            ServeurS2 = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrServeurS2, "", false);
            BaseS3 = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrBaseS3, "", false);
            ServeurS3 = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrServeurS3, "", false);
            BaseS4 = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrBaseS4, "", false);
            ServeurS4 = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrServeurS4, "", false);
            BaseS5 = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrBaseS5, "", false);
            ServeurS5 = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrServeurS5, "", false);

            //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
            BaseS7 = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrBaseS7, "", false);
            ServeurS7 = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrServeurS7, "", false);
            //===> Fin Modif Mondir

            if (AssemblyName.ToUpper() == General.cProgIntranetV3PDA.ToUpper())
            {
                BaseS6 = "AXECIEL_LONG_PDA";
                ServeurS6 = "DT-SQL-01";
            }
            else
            {
                BaseS6 = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrBaseS6, General.nomBaseS6, false);
                ServeurS6 = General.getFrmReg(General.cFrMultiSoc + "\\" + "Param", General.cFrServeurS6, General.DefaultServer, false);
            }

            switch (ChoixSociete)
            {
                case General.S1:
                    OptS1.Checked = true;
                    txtNomBase.Text = BaseS1;
                    txtNomServeur.Text = ServeurS1;
                    break;
                case General.S2:
                    OptS2.Checked = true;
                    txtNomBase.Text = BaseS2;
                    txtNomServeur.Text = ServeurS2;
                    break;
                case General.S3:
                    OptS3.Checked = true;
                    txtNomBase.Text = BaseS3;
                    txtNomServeur.Text = ServeurS3;
                    break;
                case General.S4:
                    OptS4.Checked = true;
                    txtNomBase.Text = BaseS4;
                    txtNomServeur.Text = ServeurS4;
                    break;
                case General.S5:
                    OptS5.Checked = true;
                    txtNomBase.Text = BaseS5;
                    txtNomServeur.Text = ServeurS5;
                    break;
                case General.S6:
                    OptS6.Checked = true;
                    txtNomBase.Text = BaseS6;
                    txtNomServeur.Text = ServeurS6;
                    break;
                //===> Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
                case General.S7:
                    OptS7.Checked = true;
                    txtNomBase.Text = BaseS7;
                    txtNomServeur.Text = ServeurS7;
                    break;
                //===> Fin Mondir
                default:
                    break;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_StockeVirgule()
        {
            string strRegistre = "";
            int sNoFile = 0;
            string strUserName = "";

            try
            {
                strUserName = General.fncUserName();

                strRegistre = "Windows Registry Editor Version 5.00" + Environment.NewLine;
                strRegistre = strRegistre + Environment.NewLine;
                strRegistre = strRegistre + @"[HKEY_CURRENT_USER\Control Panel\International]" + Environment.NewLine;
                strRegistre = strRegistre + @"""iCountry""=""33""" + Environment.NewLine;
                strRegistre = strRegistre + @"""sCountry""=""France""" + Environment.NewLine;
                strRegistre = strRegistre + @"""sCurrency""=""€""" + Environment.NewLine;
                strRegistre = strRegistre + @"""sDate""=""/""" + Environment.NewLine;
                strRegistre = strRegistre + @"""sDecimal""="".""" + Environment.NewLine;
                strRegistre = strRegistre + @"""sLanguage""=""FRA""" + Environment.NewLine;
                strRegistre = strRegistre + @"""sList""="";""" + Environment.NewLine;
                strRegistre = strRegistre + @"""sLongDate""=""dddd d MMMM yyyy""" + Environment.NewLine;
                strRegistre = strRegistre + @"""sShortDate""=""dd/MM/yyyy""" + Environment.NewLine;
                strRegistre = strRegistre + @"""sThousand""="" """ + Environment.NewLine;
                strRegistre = strRegistre + @"""sTime""="":""" + Environment.NewLine;
                strRegistre = strRegistre + @"""sTimeFormat""=""HH:mm:ss""" + Environment.NewLine;
                strRegistre = strRegistre + @"""iTimePrefix""=""0""" + Environment.NewLine;
                strRegistre = strRegistre + @"""sMonDecimalSep""="".""" + Environment.NewLine;
                strRegistre = strRegistre + @"""sMonThousandSep""="" """ + Environment.NewLine;
                strRegistre = strRegistre + @"""iNegNumber""=""1""" + Environment.NewLine;

                if (!Dossier.fc_ControleDossier("C:\\temp"))
                    Dossier.fc_CreateDossier("C:\\temp");

                if (!Dossier.fc_ControleDossier("C:\\temp\\temp"))
                    Dossier.fc_CreateDossier("C:\\temp\\temp");
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptS1_Click(object sender, EventArgs e)
        {
            txtNomBase.Text = BaseS1;
            txtNomServeur.Text = ServeurS1;
            NouvBase = BaseS1;
            ChoixSociete = General.S1;

            if (string.IsNullOrEmpty(txtNomServeur.Text))
            {
                txtNomServeur.Text = General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test ? "DT-SQL-01" : ".";
            }

            //===> Mondir le 24.12.2020, remplir les infos automatiquement de la base de donnée
            if (General._ExecutionMode == General.ExecutionMode.Prod && txtNomBase.Text != "AXECIEL_DELOSTAL")
            {
                txtNomBase.Text = "AXECIEL_DELOSTAL";
            }
            else if (General._ExecutionMode == General.ExecutionMode.Test && txtNomBase.Text != "AXECIEL_DELOSTAL_PDA")
            {
                txtNomBase.Text = "AXECIEL_DELOSTAL_PDA";
            }
        }

        private void OptS1_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                OptS1.Checked = true;
                OptS1.Text = Interaction.InputBox("Veuillez saisir le nom de la société", "Nom de société", OptS1.Text);
                General.saveInReg(General.cFrMultiSoc + "\\Param", "NomS1", OptS1.Text, false);
            }
        }

        private void OptS4_Click(object sender, EventArgs e)
        {
            txtNomBase.Text = BaseS4;
            txtNomServeur.Text = ServeurS4;
            NouvBase = BaseS4;
            ChoixSociete = General.S4;
        }

        private void OptS4_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                OptS4.Checked = true;
                OptS4.Text = Interaction.InputBox("Veuillez saisir le nom de la société", "Nom de société", OptS4.Text);
                General.saveInReg(General.cFrMultiSoc + "\\Param", "NomS4", OptS4.Text, false);
            }
        }

        private void OptS5_Click(object sender, EventArgs e)
        {
            txtNomBase.Text = BaseS5;
            txtNomServeur.Text = ServeurS5;
            NouvBase = BaseS5;
            ChoixSociete = General.S5;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptS6_Click(object sender, EventArgs e)
        {
            txtNomBase.Text = BaseS6;
            txtNomServeur.Text = ServeurS6;
            NouvBase = BaseS6;
            ChoixSociete = General.S6;

            if (string.IsNullOrEmpty(txtNomServeur.Text))
            {
                txtNomServeur.Text = General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test ? "DT-SQL-01" : ".";
            }

            //===> Mondir le 24.12.2020, remplir les infos automatiquement de la base de donnée
            if (General._ExecutionMode == General.ExecutionMode.Prod && txtNomBase.Text != "AXECIEL_LONG")
            {
                txtNomBase.Text = "AXECIEL_LONG";
            }
            else if (General._ExecutionMode == General.ExecutionMode.Test && txtNomBase.Text != "AXECIEL_LONG_PDA")
            {
                txtNomBase.Text = "AXECIEL_LONG_PDA";
            }

        }

        private void OptS4_CheckedChanged(object sender)
        {

        }

        private void cmdQuitter_Click(object sender, EventArgs e)
        {
            //===> Mondir le 06.11.2020, better to navigate to home
            //this.Hide();
            View.Theme.Theme.Navigate(typeof(Home));
        }

        private void OptS2_Click(object sender, EventArgs e)
        {
            txtNomBase.Text = BaseS2;
            txtNomServeur.Text = ServeurS2;
            NouvBase = BaseS2;
            ChoixSociete = General.S2;
        }

        private void OptS2_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                OptS2.Checked = true;
                OptS2.Text = Interaction.InputBox("Veuillez saisir le nom de la société", "Nom de société", OptS2.Text);
                General.saveInReg(General.cFrMultiSoc + "\\Param", "NomS2", OptS2.Text, false);
            }
        }

        private void OptS3_Click(object sender, EventArgs e)
        {
            txtNomBase.Text = BaseS3;
            txtNomServeur.Text = ServeurS3;
            NouvBase = BaseS3;
            ChoixSociete = General.S3;

            //===> Mondir le 24.12.2020, remplir les infos automatiquement de la base de donnée
            if (General._ExecutionMode == General.ExecutionMode.Prod && txtNomBase.Text != "AXECIEL_ECCEP")
            {
                txtNomBase.Text = "AXECIEL_ECCEP";
            }
            else if (General._ExecutionMode == General.ExecutionMode.Test)
            {
                txtNomBase.Text = "";
            }
        }

        private void OptS3_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                OptS3.Checked = true;
                OptS3.Text = Interaction.InputBox("Veuillez saisir le nom de la société", "Nom de société", OptS3.Text);
                General.saveInReg(General.cFrMultiSoc + "\\Param", "NomS3", OptS3.Text, false);
            }
        }

        private void OptS5_MouseUp(object sender, MouseEventArgs e)
        {
            //if (e.Button == MouseButtons.Right)
            //{
            //    OptS5.Checked = true;
            //    OptS5.Text = Interaction.InputBox("Veuillez saisir le nom de la société", "Nom de société", OptS5.Text);
            //    General.saveInReg(General.cFrMultiSoc + "\\Param", "NomS5", OptS5.Text, false);
            //}
        }

        /// <summary>
        /// Mondir le 20.05.2021, ajout du societe VanFroid ==> VF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptS7_Click(object sender, EventArgs e)
        {
            txtNomBase.Text = BaseS7;
            txtNomServeur.Text = ServeurS7;
            NouvBase = BaseS7;
            ChoixSociete = General.S7;

            if (string.IsNullOrEmpty(txtNomServeur.Text))
            {
                txtNomServeur.Text = General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test ? "DT-SQL-01" : ".";
            }

            //===> Mondir le 24.12.2020, remplir les infos automatiquement de la base de donnée
            if (General._ExecutionMode == General.ExecutionMode.Prod && txtNomBase.Text != "AXECIEL_VanFroid".ToUpper())
            {
                txtNomBase.Text = "AXECIEL_VanFroid";
            }
            else if (General._ExecutionMode == General.ExecutionMode.Test && txtNomBase.Text != "AXECIEL_VanFroid_PDA".ToUpper())
            {
                txtNomBase.Text = "AXECIEL_VanFroid_PDA";
            }
        }

        /// <summary>
        /// Mondir le 21.05.2021, ajout du societe VanFroid ==> VF
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void OptS7_MouseUp(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                OptS7.Checked = true;
                OptS7.Text = Interaction.InputBox("Veuillez saisir le nom de la société", "Nom de société", OptS7.Text);
                General.saveInReg(General.cFrMultiSoc + "\\Param", "NomS7", OptS7.Text, false);
            }
        }
    }
}
