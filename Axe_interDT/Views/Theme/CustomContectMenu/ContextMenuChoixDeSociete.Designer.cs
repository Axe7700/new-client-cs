﻿namespace Axe_interDT.Views.Theme.CustomContectMenu
{
    partial class ContextMenuChoixDeSociete
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.ultraTouchProvider2 = new Infragistics.Win.Touch.UltraTouchProvider(this.components);
            this.iTalk_GroupBox2 = new iTalk.iTalk_GroupBox();
            this.iTalk_GroupBox1 = new iTalk.iTalk_GroupBox();
            this.OptS7 = new System.Windows.Forms.RadioButton();
            this.OptS6 = new System.Windows.Forms.RadioButton();
            this.OptS1 = new System.Windows.Forms.RadioButton();
            this.label33 = new System.Windows.Forms.Label();
            this.txtNomServeur = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.txtNomBase = new iTalk.iTalk_TextBox_Small2();
            this.cmdQuitter = new System.Windows.Forms.Button();
            this.cmdValider = new System.Windows.Forms.Button();
            this.OptS2 = new System.Windows.Forms.RadioButton();
            this.OptS5 = new System.Windows.Forms.RadioButton();
            this.OptS4 = new System.Windows.Forms.RadioButton();
            this.OptS3 = new System.Windows.Forms.RadioButton();
            ((System.ComponentModel.ISupportInitialize)(this.ultraTouchProvider2)).BeginInit();
            this.iTalk_GroupBox2.SuspendLayout();
            this.iTalk_GroupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTouchProvider2
            // 
            this.ultraTouchProvider2.ContainingControl = this;
            // 
            // iTalk_GroupBox2
            // 
            this.iTalk_GroupBox2.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.iTalk_GroupBox2.BackColor = System.Drawing.Color.Transparent;
            this.iTalk_GroupBox2.Controls.Add(this.iTalk_GroupBox1);
            this.iTalk_GroupBox2.Controls.Add(this.label33);
            this.iTalk_GroupBox2.Controls.Add(this.txtNomServeur);
            this.iTalk_GroupBox2.Controls.Add(this.label1);
            this.iTalk_GroupBox2.Controls.Add(this.txtNomBase);
            this.iTalk_GroupBox2.Controls.Add(this.cmdQuitter);
            this.iTalk_GroupBox2.Controls.Add(this.cmdValider);
            this.iTalk_GroupBox2.Location = new System.Drawing.Point(361, 222);
            this.iTalk_GroupBox2.MinimumSize = new System.Drawing.Size(136, 50);
            this.iTalk_GroupBox2.Name = "iTalk_GroupBox2";
            this.iTalk_GroupBox2.Padding = new System.Windows.Forms.Padding(5, 28, 5, 5);
            this.iTalk_GroupBox2.Size = new System.Drawing.Size(518, 253);
            this.iTalk_GroupBox2.TabIndex = 512;
            this.iTalk_GroupBox2.Text = "Choix de Société";
            // 
            // iTalk_GroupBox1
            // 
            this.iTalk_GroupBox1.BackColor = System.Drawing.Color.Transparent;
            this.iTalk_GroupBox1.Controls.Add(this.OptS7);
            this.iTalk_GroupBox1.Controls.Add(this.OptS6);
            this.iTalk_GroupBox1.Controls.Add(this.OptS1);
            this.iTalk_GroupBox1.Location = new System.Drawing.Point(8, 31);
            this.iTalk_GroupBox1.MinimumSize = new System.Drawing.Size(136, 50);
            this.iTalk_GroupBox1.Name = "iTalk_GroupBox1";
            this.iTalk_GroupBox1.Padding = new System.Windows.Forms.Padding(5, 28, 5, 5);
            this.iTalk_GroupBox1.Size = new System.Drawing.Size(171, 212);
            this.iTalk_GroupBox1.TabIndex = 0;
            this.iTalk_GroupBox1.Text = "Société";
            // 
            // OptS7
            // 
            this.OptS7.AutoSize = true;
            this.OptS7.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptS7.Location = new System.Drawing.Point(8, 89);
            this.OptS7.Name = "OptS7";
            this.OptS7.Size = new System.Drawing.Size(91, 23);
            this.OptS7.TabIndex = 539;
            this.OptS7.TabStop = true;
            this.OptS7.Text = "Van Froid";
            this.OptS7.UseVisualStyleBackColor = true;
            this.OptS7.Click += new System.EventHandler(this.OptS7_Click);
            this.OptS7.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OptS7_MouseUp);
            // 
            // OptS6
            // 
            this.OptS6.AutoSize = true;
            this.OptS6.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptS6.Location = new System.Drawing.Point(8, 60);
            this.OptS6.Name = "OptS6";
            this.OptS6.Size = new System.Drawing.Size(67, 23);
            this.OptS6.TabIndex = 538;
            this.OptS6.TabStop = true;
            this.OptS6.Text = "LONG";
            this.OptS6.UseVisualStyleBackColor = true;
            this.OptS6.Click += new System.EventHandler(this.OptS6_Click);
            // 
            // OptS1
            // 
            this.OptS1.AutoSize = true;
            this.OptS1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptS1.Location = new System.Drawing.Point(8, 31);
            this.OptS1.Name = "OptS1";
            this.OptS1.Size = new System.Drawing.Size(148, 23);
            this.OptS1.TabIndex = 538;
            this.OptS1.TabStop = true;
            this.OptS1.Text = "Delostal & Thibault";
            this.OptS1.UseVisualStyleBackColor = true;
            this.OptS1.Click += new System.EventHandler(this.OptS1_Click);
            this.OptS1.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OptS1_MouseUp);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.label33.Location = new System.Drawing.Point(184, 67);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(103, 17);
            this.label33.TabIndex = 502;
            this.label33.Text = "Nom du serveur";
            // 
            // txtNomServeur
            // 
            this.txtNomServeur.AccAcceptNumbersOnly = false;
            this.txtNomServeur.AccAllowComma = false;
            this.txtNomServeur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNomServeur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNomServeur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNomServeur.AccHidenValue = "";
            this.txtNomServeur.AccNotAllowedChars = null;
            this.txtNomServeur.AccReadOnly = false;
            this.txtNomServeur.AccReadOnlyAllowDelete = false;
            this.txtNomServeur.AccRequired = false;
            this.txtNomServeur.BackColor = System.Drawing.Color.White;
            this.txtNomServeur.CustomBackColor = System.Drawing.Color.White;
            this.txtNomServeur.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomServeur.ForeColor = System.Drawing.Color.Black;
            this.txtNomServeur.Location = new System.Drawing.Point(293, 62);
            this.txtNomServeur.Margin = new System.Windows.Forms.Padding(2);
            this.txtNomServeur.MaxLength = 32767;
            this.txtNomServeur.Multiline = false;
            this.txtNomServeur.Name = "txtNomServeur";
            this.txtNomServeur.ReadOnly = false;
            this.txtNomServeur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNomServeur.Size = new System.Drawing.Size(217, 27);
            this.txtNomServeur.TabIndex = 503;
            this.txtNomServeur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNomServeur.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.label1.Location = new System.Drawing.Point(184, 93);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 17);
            this.label1.TabIndex = 504;
            this.label1.Text = "Nom de la base";
            // 
            // txtNomBase
            // 
            this.txtNomBase.AccAcceptNumbersOnly = false;
            this.txtNomBase.AccAllowComma = false;
            this.txtNomBase.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNomBase.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNomBase.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNomBase.AccHidenValue = "";
            this.txtNomBase.AccNotAllowedChars = null;
            this.txtNomBase.AccReadOnly = false;
            this.txtNomBase.AccReadOnlyAllowDelete = false;
            this.txtNomBase.AccRequired = false;
            this.txtNomBase.BackColor = System.Drawing.Color.White;
            this.txtNomBase.CustomBackColor = System.Drawing.Color.White;
            this.txtNomBase.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNomBase.ForeColor = System.Drawing.Color.Black;
            this.txtNomBase.Location = new System.Drawing.Point(292, 93);
            this.txtNomBase.Margin = new System.Windows.Forms.Padding(2);
            this.txtNomBase.MaxLength = 32767;
            this.txtNomBase.Multiline = false;
            this.txtNomBase.Name = "txtNomBase";
            this.txtNomBase.ReadOnly = false;
            this.txtNomBase.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNomBase.Size = new System.Drawing.Size(217, 27);
            this.txtNomBase.TabIndex = 505;
            this.txtNomBase.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNomBase.UseSystemPasswordChar = false;
            // 
            // cmdQuitter
            // 
            this.cmdQuitter.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cmdQuitter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdQuitter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdQuitter.FlatAppearance.BorderSize = 0;
            this.cmdQuitter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdQuitter.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdQuitter.ForeColor = System.Drawing.Color.White;
            this.cmdQuitter.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.cmdQuitter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdQuitter.Location = new System.Drawing.Point(335, 214);
            this.cmdQuitter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdQuitter.Name = "cmdQuitter";
            this.cmdQuitter.Size = new System.Drawing.Size(85, 30);
            this.cmdQuitter.TabIndex = 507;
            this.cmdQuitter.Text = "      &Annuler";
            this.cmdQuitter.UseVisualStyleBackColor = false;
            this.cmdQuitter.Click += new System.EventHandler(this.cmdQuitter_Click);
            // 
            // cmdValider
            // 
            this.cmdValider.Anchor = System.Windows.Forms.AnchorStyles.Bottom;
            this.cmdValider.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdValider.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdValider.FlatAppearance.BorderSize = 0;
            this.cmdValider.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdValider.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdValider.ForeColor = System.Drawing.Color.White;
            this.cmdValider.Image = global::Axe_interDT.Properties.Resources.Save_16x16;
            this.cmdValider.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdValider.Location = new System.Drawing.Point(424, 214);
            this.cmdValider.Margin = new System.Windows.Forms.Padding(2);
            this.cmdValider.Name = "cmdValider";
            this.cmdValider.Size = new System.Drawing.Size(85, 30);
            this.cmdValider.TabIndex = 506;
            this.cmdValider.Text = "      &Valider";
            this.cmdValider.UseVisualStyleBackColor = false;
            this.cmdValider.Click += new System.EventHandler(this.cmdValider_Click);
            // 
            // OptS2
            // 
            this.OptS2.AutoSize = true;
            this.OptS2.Enabled = false;
            this.OptS2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptS2.Location = new System.Drawing.Point(230, 414);
            this.OptS2.Name = "OptS2";
            this.OptS2.Size = new System.Drawing.Size(83, 23);
            this.OptS2.TabIndex = 538;
            this.OptS2.TabStop = true;
            this.OptS2.Text = "PEZANT";
            this.OptS2.UseVisualStyleBackColor = true;
            this.OptS2.Visible = false;
            this.OptS2.Click += new System.EventHandler(this.OptS2_Click);
            this.OptS2.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OptS2_MouseUp);
            // 
            // OptS5
            // 
            this.OptS5.AutoSize = true;
            this.OptS5.Enabled = false;
            this.OptS5.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptS5.Location = new System.Drawing.Point(230, 390);
            this.OptS5.Name = "OptS5";
            this.OptS5.Size = new System.Drawing.Size(111, 23);
            this.OptS5.TabIndex = 538;
            this.OptS5.TabStop = true;
            this.OptS5.Text = "Gaz De Paris";
            this.OptS5.UseVisualStyleBackColor = true;
            this.OptS5.Visible = false;
            this.OptS5.Click += new System.EventHandler(this.OptS5_Click);
            this.OptS5.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OptS5_MouseUp);
            // 
            // OptS4
            // 
            this.OptS4.AutoSize = true;
            this.OptS4.Enabled = false;
            this.OptS4.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptS4.Location = new System.Drawing.Point(230, 365);
            this.OptS4.Name = "OptS4";
            this.OptS4.Size = new System.Drawing.Size(93, 23);
            this.OptS4.TabIndex = 538;
            this.OptS4.TabStop = true;
            this.OptS4.Text = "AMMANN";
            this.OptS4.UseVisualStyleBackColor = true;
            this.OptS4.Visible = false;
            this.OptS4.Click += new System.EventHandler(this.OptS4_Click);
            this.OptS4.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OptS4_MouseUp);
            // 
            // OptS3
            // 
            this.OptS3.AutoSize = true;
            this.OptS3.Enabled = false;
            this.OptS3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.OptS3.Location = new System.Drawing.Point(230, 339);
            this.OptS3.Name = "OptS3";
            this.OptS3.Size = new System.Drawing.Size(74, 23);
            this.OptS3.TabIndex = 538;
            this.OptS3.TabStop = true;
            this.OptS3.Text = "ECCEP";
            this.OptS3.UseVisualStyleBackColor = true;
            this.OptS3.Visible = false;
            this.OptS3.Click += new System.EventHandler(this.OptS3_Click);
            this.OptS3.MouseUp += new System.Windows.Forms.MouseEventHandler(this.OptS3_MouseUp);
            // 
            // ContextMenuChoixDeSociete
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(239)))), ((int)(((byte)(239)))), ((int)(((byte)(239)))));
            this.Controls.Add(this.OptS2);
            this.Controls.Add(this.iTalk_GroupBox2);
            this.Controls.Add(this.OptS5);
            this.Controls.Add(this.OptS3);
            this.Controls.Add(this.OptS4);
            this.MinimumSize = new System.Drawing.Size(543, 125);
            this.Name = "ContextMenuChoixDeSociete";
            this.Size = new System.Drawing.Size(1308, 725);
            ((System.ComponentModel.ISupportInitialize)(this.ultraTouchProvider2)).EndInit();
            this.iTalk_GroupBox2.ResumeLayout(false);
            this.iTalk_GroupBox2.PerformLayout();
            this.iTalk_GroupBox1.ResumeLayout(false);
            this.iTalk_GroupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private iTalk.iTalk_GroupBox iTalk_GroupBox1;
        public iTalk.iTalk_TextBox_Small2 txtNomServeur;
        public System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtNomBase;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button cmdValider;
        public System.Windows.Forms.Button cmdQuitter;
        public System.Windows.Forms.RadioButton OptS2;
        public System.Windows.Forms.RadioButton OptS5;
        public System.Windows.Forms.RadioButton OptS4;
        public System.Windows.Forms.RadioButton OptS3;
        public System.Windows.Forms.RadioButton OptS6;
        public System.Windows.Forms.RadioButton OptS1;
        private iTalk.iTalk_GroupBox iTalk_GroupBox2;
        private Infragistics.Win.Touch.UltraTouchProvider ultraTouchProvider2;
        public System.Windows.Forms.RadioButton OptS7;
    }
}
