﻿using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Reflection;
using System.Windows.Forms;
using Axe_interDT.View.SharedViews;
using Excel = Microsoft.Office.Interop.Excel;
namespace Axe_interDT.View.Theme
{
    public partial class ThemeControls : UserControl
    {
        public ThemeControls()
        {
            InitializeComponent();
            //_RowColChange -> _AfterExitEditMode
            //_RowLoaded -> _InitializeRow
            //_BeforeColUpdate -> _BeforeCellUpdate
            //_BeforeUpdate -> _BeforeRowUpdate

            // Error
            //if (e.ErrorText.ToUpper() == "Data type conversion error.".ToUpper())
            //{
            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie de données incorrectes dans la colonne " + e.DataErrorInfo.Cell.Column.Header.Caption);
            //    e.Cancel = true;
            //}

            // _BeforeDelete
            //e.DisplayPromptMsg = false;
            //if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
            //    MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
            //    e.Cancel = true;

            //string[,] temp = new string[cNbColonne, 0];
            //Array.Copy(tabCmd, temp, Math.Min(tabCmd.Length, temp.Length));
            //tabCmd = temp;

        }
        // Grid Events //
        //_BeforeUpdate
        private void ssGridCopro_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {

        }
        private void ssGridCOMRBis_Error(object sender, ErrorEventArgs e)
        {

        }
        private void ssGridCOMRBis_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {

        }
        // End Grid Events //
        private void ThemeControls_Load(object sender, EventArgs e)
        {
            try
            {
                string requete = "";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") {Text = ""};
                fg.SetValues(new Dictionary<string, string> {{"column", iTalk_TextBox_Small21.Text}});
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    iTalk_TextBox_Small21.Text = fg.ugResultat.ActiveRow.Cells["Code SousAdresse"].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        iTalk_TextBox_Small21.Text = fg.ugResultat.ActiveRow.Cells["Code SousAdresse"].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }
        private void fc_ExportSite()
        {
            Excel.Application oXL;
            Excel.Workbook oWB;
            Excel.Worksheet oSheet;
            Excel.Range oRng;
            Excel.Range oResizeRange;
            DataTable rsExport = null;
            SqlDataAdapter SDArsExport = null;
            SqlCommandBuilder SCBrsExport = null;
            //DataColumn oField;
            int i;
            int j;
            int lLigne;
            string sMatricule = "";
            const int cCol = 0;
            bool bok;
            try
            {
                Cursor = Cursors.WaitCursor;
                //' Start Excel and get Application object.
                oXL = new Excel.Application();
                oXL.Visible = false;
                //' Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;
                rsExport = new DataTable();
                //SDArsExport = new SqlDataAdapter(SDArsAXT.SelectCommand.CommandText, GetConnection.conn);
                SDArsExport.Fill(rsExport);
                if (rsExport.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour ces critères.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                //  '== affiche les noms des champs.
                i = 1;
                foreach (DataColumn oField in rsExport.Columns)
                {
                    oSheet.Cells[1, i].VerticalAlignment = Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Cells[1, i].value = oField.ColumnName;
                    i++;
                }

                Cursor = Cursors.WaitCursor;
                // ' Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Interior.ColorIndex = 15;
                // 'formate la ligne en gras.
                oResizeRange.Font.Bold = true;
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Excel.XlBorderWeight.xlThin;
                oResizeRange.Borders.Weight = 1;
                lLigne = 3;
                Cursor = Cursors.WaitCursor;
                foreach (DataRow rsExportRow in rsExport.Rows)
                {
                    j = 1;
                    foreach (DataColumn rsExportColumn in rsExport.Columns)
                    {
                        oSheet.Cells[lLigne, j + cCol].value = rsExportRow[rsExportColumn.ColumnName];
                        j++;
                    }
                    lLigne++;
                }
                // 'formate la ligne en gras.
                oResizeRange.Font.Bold = true;
                oXL.Visible = true;
                oXL.UserControl = true;
                oRng = null;
                oSheet = null;
                oWB = null;
                oXL = null;
                Cursor = Cursors.Default;
                rsExport = null;
            }
            catch { }
        }
     
    }
}
