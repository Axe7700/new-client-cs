﻿using Axe_interDT.Properties;
using Axe_interDT.Shared;
using System;
using System.Diagnostics;
using System.Drawing;
using System.Reflection;
using System.Security;
using System.Security.Permissions;
using System.Windows.Automation;
using System.Windows.Forms;

namespace Axe_interDT.Views.Theme
{
    public static class FormUtils
    {
        public static void SetDefaultIcon()
        {
            //var icon = Icon.ExtractAssociatedIcon(EntryAssemblyInfo.ExecutablePath);
            //var icon = new Icon("Resources/Intranet.ico");
            //=========> Code commenté par Mondir, pas besoin de la class EntryAssemblyInfo
            //===> Mondir le 25.05.2021, commanted to add JF
            //var icon = General._Company == General.Company.DT ? Resources.Intranet : Resources.LONG_ICO;
            Icon icon = null;
            switch (General._Company)
            {
                case General.Company.DT:
                    icon = Resources.Intranet;
                    break;
                case General.Company.LONG:
                    icon = Resources.LONG_ICO;
                    break;
                case General.Company.VF:
                    icon = Resources.VF_LOGO1;
                    break;
            }
            typeof(Form)
                .GetField("defaultIcon", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Static)
                .SetValue(null, icon);
        }

        private static int x = 0;

        public static void OnCloseEvent()
        {
            Automation.AddAutomationEventHandler(WindowPattern.WindowOpenedEvent, AutomationElement.RootElement, TreeScope.Subtree, (sender, args) =>
            {
                foreach (Form form in Application.OpenForms)
                {
                    form.Invoke(new Action(() =>
                    {
                        form.Text = "aaaaaaaa";
                    }));
                    
                }
                //var element = sender as AutomationElement;
                //if(element == null)
                //{
                //    return;
                //}

                //var NativeHandle = element.Current.NativeWindowHandle;
                //var ProcessId = element.Current.ProcessId;

                //var name = element.Current.Name;

                //return;

                //if (ProcessId != Process.GetCurrentProcess().Id)
                //{
                //    return;
                //}

                //Form form = null;

                //f.Invoke(new MethodInvoker(() =>
                //{

                //}));

                //try
                //{
                //    form = Application.OpenForms.Cast<Form>()
                //        .FirstOrDefault(ff => ff.Handle == (IntPtr) NativeHandle);

                //    if (form != null)
                //    {
                //        form.Text = (x++).ToString();
                //    }
                //}
                //catch(Exception ex)
                //{

                //}

            });
        }
    }

    public static class EntryAssemblyInfo
    {
        private static string _executablePath;

        public static string ExecutablePath
        {
            get
            {
                if (_executablePath == null)
                {
                    PermissionSet permissionSets = new PermissionSet(PermissionState.None);
                    permissionSets.AddPermission(new FileIOPermission(PermissionState.Unrestricted));
                    permissionSets.AddPermission(new SecurityPermission(SecurityPermissionFlag.UnmanagedCode));
                    permissionSets.Assert();

                    string uriString = null;
                    var entryAssembly = Assembly.GetEntryAssembly();

                    if (entryAssembly == null)
                        uriString = Process.GetCurrentProcess().MainModule.FileName;
                    else
                        uriString = entryAssembly.CodeBase;

                    PermissionSet.RevertAssert();

                    if (string.IsNullOrWhiteSpace(uriString))
                        throw new Exception("Can not Get EntryAssembly or Process MainModule FileName");
                    else
                    {
                        var uri = new Uri(uriString);
                        if (uri.IsFile)
                            _executablePath = string.Concat(uri.LocalPath, Uri.UnescapeDataString(uri.Fragment));
                        else
                            _executablePath = uri.ToString();
                    }
                }

                return _executablePath;
            }
        }
    }
}
