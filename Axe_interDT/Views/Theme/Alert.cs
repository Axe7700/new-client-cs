﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Theme
{
    public partial class Alert : UserControl
    {
        private Image CloseButtonMouseHover = Properties.Resources.AlertGreenCloseMouseHover;
        private Image CloseButtonMouseLeave = Properties.Resources.AlertGreenCloseMouseLeave;
        public EnumHideAfter HideAfter { get; set; } = EnumHideAfter.TwoSeconds;
        public enum EnumHideAfter
        {
            OneSecond = 1000, TwoSeconds = 2000, ThreeSeconds = 3000,
            FourSeconds = 4000, FiveSeconds = 5000, SixSeconds = 6000, SevenSeconds = 7000, TwentySeconds = 20000
        }
        public enum EnumMessageType { Success, Danger }

        private EnumMessageType _MessageType = EnumMessageType.Success;
        private ContentAlignment _MessageContentAlignment = ContentAlignment.MiddleCenter;

        public EnumMessageType MessageType
        {
            get { return _MessageType; }
            set
            {
                _MessageType = value;
                if (_MessageType == EnumMessageType.Success)
                {
                    CloseButtonMouseHover = Properties.Resources.AlertGreenCloseMouseHover;
                    CloseButtonMouseLeave = Properties.Resources.AlertGreenCloseMouseLeave;
                    ButtonClose.Image = CloseButtonMouseLeave;
                    panel9.BackgroundImage = Properties.Resources.AlertGreenRight;
                    panel10.BackgroundImage = Properties.Resources.AlertGreenRightBottom;
                    panel11.BackgroundImage = Properties.Resources.AlertGreenRightTop;
                    panel8.BackgroundImage = Properties.Resources.AlertGreenLeft;
                    panel7.BackgroundImage = Properties.Resources.AlertGreenLeftBottom;
                    panel6.BackgroundImage = Properties.Resources.AlertGreenLeftTop;
                    LabelMessage.ForeColor = Color.FromArgb(60, 118, 61);
                    panel5.BackColor = Color.FromArgb(214, 233, 198);
                    panel4.BackColor = Color.FromArgb(214, 233, 198);
                    panel3.BackColor = Color.FromArgb(223, 240, 216);
                }
                else
                {
                    CloseButtonMouseHover = Properties.Resources.AlertDangerCloseMouseHover;
                    CloseButtonMouseLeave = Properties.Resources.AlertDangerCloseMouseLeave;
                    ButtonClose.Image = CloseButtonMouseLeave;
                    panel9.BackgroundImage = Properties.Resources.AlertDangerRight;
                    panel10.BackgroundImage = Properties.Resources.AlertDangerRightBottom;
                    panel11.BackgroundImage = Properties.Resources.AlertDangerRightTop;
                    panel8.BackgroundImage = Properties.Resources.AlertDangerLeft;
                    panel7.BackgroundImage = Properties.Resources.AlertDangerLeftBottom;
                    panel6.BackgroundImage = Properties.Resources.AlertDangerLeftTop;
                    LabelMessage.ForeColor = Color.FromArgb(169, 68, 100);
                    panel5.BackColor = Color.FromArgb(235, 204, 209);
                    panel4.BackColor = Color.FromArgb(235, 204, 209);
                    panel3.BackColor = Color.FromArgb(242, 222, 222);
                }
            }
        }
        public ContentAlignment MessageContentAlignment
        {
            get { return _MessageContentAlignment; }
            set
            {
                _MessageContentAlignment = value;
                LabelMessage.TextAlign = _MessageContentAlignment;
            }
        }
        public string Message
        {
            get { return LabelMessage.Text; }
            set
            {
                LabelMessage.Text = value;
                //using (Graphics g = CreateGraphics())
                //{
                //    LabelMessage.Top = 1;
                //    SizeF size = g.MeasureString(LabelMessage.Text, LabelMessage.Font);
                //    LabelMessage.Height = (int)Math.Ceiling(size.Height) + 52;
                //    Size = new Size(Width, LabelMessage.Height + 2);
                //}
            }
        }
        public Alert()
        {
            InitializeComponent();            
        }

        private void ButtonClose_MouseHover(object sender, EventArgs e)
            => ButtonClose.Image = CloseButtonMouseHover;

        private void ButtonClose_MouseLeave(object sender, EventArgs e)
            => ButtonClose.Image = CloseButtonMouseLeave;

        private void ButtonClose_Click(object sender, EventArgs e)
        {
            //Parent.Visible = false;
            Visible = false;
        }

        private void Alert_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;

            if (Parent != null && Parent.Visible)
            {                
                    Timer T = new Timer { Interval = (int)HideAfter };
                    T.Start();
                    T.Tick += (se, ev) =>
                    {
                        //Parent.Visible = false;
                        Visible = false;
                        View.Theme.Theme.BugFound = false;
                        T.Stop();                    
                    };
             
            }
        }

       
    }
}
