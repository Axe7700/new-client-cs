﻿namespace Axe_interDT.View.Theme
{
    partial class ThemeControls
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab13 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(ThemeControls));
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton1 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton2 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.SSTab1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.Grids = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.button19 = new System.Windows.Forms.Button();
            this.Combo1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.radioButton1 = new System.Windows.Forms.RadioButton();
            this.label20 = new System.Windows.Forms.Label();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label19 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label18 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label17 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.checkBox1 = new System.Windows.Forms.CheckBox();
            this.ugResultat = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ultraFormattedLinkLabel1 = new Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel();
            this.ultraFlowLayoutManager1 = new Infragistics.Win.Misc.UltraFlowLayoutManager(this.components);
            this.axCalendarControl1 = new AxXtremeCalendarControl.AxCalendarControl();
            this.axPlanning1 = new AxPLANNINGLib.AxPlanning();
            this.iTalk_TextBox_Small22 = new iTalk.iTalk_TextBox_Small2();
            this.iTalk_RichTextBox1 = new iTalk.iTalk_RichTextBox();
            this.iTalk_TextBox_Small21 = new iTalk.iTalk_TextBox_Small2();
            this.ultraCalendarCombo2 = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.ultraCalendarCombo1 = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.CmdAffUtil = new System.Windows.Forms.Button();
            this.cmdDesAffUtil = new System.Windows.Forms.Button();
            this.button27 = new System.Windows.Forms.Button();
            this.button26 = new System.Windows.Forms.Button();
            this.button25 = new System.Windows.Forms.Button();
            this.button24 = new System.Windows.Forms.Button();
            this.button23 = new System.Windows.Forms.Button();
            this.button22 = new System.Windows.Forms.Button();
            this.button20 = new System.Windows.Forms.Button();
            this.button21 = new System.Windows.Forms.Button();
            this.cmdPrecedent = new System.Windows.Forms.Button();
            this.cmdSuivant = new System.Windows.Forms.Button();
            this.cmdDernier = new System.Windows.Forms.Button();
            this.cmdPremier = new System.Windows.Forms.Button();
            this.button18 = new System.Windows.Forms.Button();
            this.button17 = new System.Windows.Forms.Button();
            this.button16 = new System.Windows.Forms.Button();
            this.button15 = new System.Windows.Forms.Button();
            this.button14 = new System.Windows.Forms.Button();
            this.button13 = new System.Windows.Forms.Button();
            this.button12 = new System.Windows.Forms.Button();
            this.button11 = new System.Windows.Forms.Button();
            this.button10 = new System.Windows.Forms.Button();
            this.button9 = new System.Windows.Forms.Button();
            this.button8 = new System.Windows.Forms.Button();
            this.button7 = new System.Windows.Forms.Button();
            this.Cmd17 = new System.Windows.Forms.Button();
            this.Cmd18 = new System.Windows.Forms.Button();
            this.button6 = new System.Windows.Forms.Button();
            this.button5 = new System.Windows.Forms.Button();
            this.button4 = new System.Windows.Forms.Button();
            this.button3 = new System.Windows.Forms.Button();
            this.CmdEditer = new System.Windows.Forms.Button();
            this.Command1 = new System.Windows.Forms.Button();
            this.button2 = new System.Windows.Forms.Button();
            this.button1 = new System.Windows.Forms.Button();
            this.cmdOK = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.cmdVisu = new System.Windows.Forms.Button();
            this.cmdMail = new System.Windows.Forms.Button();
            this.cmdFax = new System.Windows.Forms.Button();
            this.cmdLettre = new System.Windows.Forms.Button();
            this.cmdImprimer = new System.Windows.Forms.Button();
            this.cmdRechercheArticle = new System.Windows.Forms.Button();
            this.cmdRecherche = new System.Windows.Forms.Button();
            this.cmdMAJ = new System.Windows.Forms.Button();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.CmdHistoDevis = new System.Windows.Forms.Button();
            this.cmdMAJE = new System.Windows.Forms.Button();
            this.cmdSupprimer = new System.Windows.Forms.Button();
            this.CmdMenuDevis = new System.Windows.Forms.Button();
            this.axWinsock1 = new AxMSWinsockLib.AxWinsock();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).BeginInit();
            this.SSTab1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grids)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.Combo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugResultat)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraFlowLayoutManager1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axCalendarControl1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axPlanning1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarCombo2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarCombo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.axWinsock1)).BeginInit();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 21);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(347, 104);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(32, 80);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(204, 16);
            this.label1.TabIndex = 394;
            this.label1.Text = "System.Windows.Forms.TextBox";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(51, 123);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(175, 16);
            this.label2.TabIndex = 395;
            this.label2.Text = "iTalk.iTalk_TextBox_Small2";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(51, 42);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(92, 16);
            this.label33.TabIndex = 383;
            this.label33.Text = "Text Text Text";
            // 
            // SSTab1
            // 
            this.SSTab1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.SSTab1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl1);
            this.SSTab1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.SSTab1.Location = new System.Drawing.Point(577, 367);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.SSTab1.Size = new System.Drawing.Size(349, 126);
            this.SSTab1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Excel;
            this.SSTab1.TabIndex = 409;
            ultraTab13.TabPage = this.ultraTabPageControl1;
            ultraTab13.Text = "abc";
            this.SSTab1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab13});
            this.SSTab1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(347, 104);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox6.Location = new System.Drawing.Point(630, 289);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(208, 67);
            this.groupBox6.TabIndex = 410;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "abc";
            // 
            // Grids
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.Grids.DisplayLayout.Appearance = appearance1;
            this.Grids.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.Grids.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.Grids.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.Grids.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.Grids.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.Grids.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.Grids.DisplayLayout.MaxColScrollRegions = 1;
            this.Grids.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Grids.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.Grids.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.Grids.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.Grids.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.Grids.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.Grids.DisplayLayout.Override.CellAppearance = appearance8;
            this.Grids.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.Grids.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.Grids.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.Grids.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.Grids.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.Grids.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.Grids.DisplayLayout.Override.RowAppearance = appearance11;
            this.Grids.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.Grids.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Grids.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.Grids.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.Grids.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.Grids.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.Grids.Location = new System.Drawing.Point(336, 42);
            this.Grids.Name = "Grids";
            this.Grids.Size = new System.Drawing.Size(160, 134);
            this.Grids.TabIndex = 411;
            this.Grids.Text = "ultraGrid1";
            // 
            // button19
            // 
            this.button19.Location = new System.Drawing.Point(358, 13);
            this.button19.Name = "button19";
            this.button19.Size = new System.Drawing.Size(75, 23);
            this.button19.TabIndex = 500;
            // 
            // Combo1
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.Combo1.DisplayLayout.Appearance = appearance13;
            this.Combo1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.Combo1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.Combo1.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.Combo1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.Combo1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.Combo1.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.Combo1.DisplayLayout.MaxColScrollRegions = 1;
            this.Combo1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Combo1.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.Combo1.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.Combo1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.Combo1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.Combo1.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.Combo1.DisplayLayout.Override.CellAppearance = appearance20;
            this.Combo1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.Combo1.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.Combo1.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.Combo1.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.Combo1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.Combo1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.Combo1.DisplayLayout.Override.RowAppearance = appearance23;
            this.Combo1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Combo1.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.Combo1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.Combo1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.Combo1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.Combo1.Location = new System.Drawing.Point(105, 154);
            this.Combo1.Name = "Combo1";
            this.Combo1.Size = new System.Drawing.Size(186, 22);
            this.Combo1.TabIndex = 502;
            // 
            // radioButton1
            // 
            this.radioButton1.AutoSize = true;
            this.radioButton1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.radioButton1.Location = new System.Drawing.Point(402, 357);
            this.radioButton1.Name = "radioButton1";
            this.radioButton1.Size = new System.Drawing.Size(101, 20);
            this.radioButton1.TabIndex = 537;
            this.radioButton1.TabStop = true;
            this.radioButton1.Text = "radioButton1";
            this.radioButton1.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(487, 546);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(128, 13);
            this.label20.TabIndex = 564;
            this.label20.Text = "FACTURE NON VERIFIEE";
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.Gray;
            this.panel4.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel4.Location = new System.Drawing.Point(428, 547);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(55, 15);
            this.panel4.TabIndex = 563;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(337, 546);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 13);
            this.label19.TabIndex = 562;
            this.label19.Text = "BON A PAYER";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Location = new System.Drawing.Point(278, 547);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(55, 15);
            this.panel3.TabIndex = 561;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(211, 546);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(55, 13);
            this.label18.TabIndex = 560;
            this.label18.Text = "REFUSEE";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(152, 547);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(55, 15);
            this.panel2.TabIndex = 559;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(75, 546);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(62, 13);
            this.label17.TabIndex = 558;
            this.label17.Text = "A VALIDER";
            // 
            // panel1
            // 
            this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(216)))), ((int)(((byte)(181)))));
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(16, 547);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(55, 15);
            this.panel1.TabIndex = 557;
            // 
            // checkBox1
            // 
            this.checkBox1.AutoSize = true;
            this.checkBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.checkBox1.Location = new System.Drawing.Point(402, 331);
            this.checkBox1.Name = "checkBox1";
            this.checkBox1.Size = new System.Drawing.Size(93, 20);
            this.checkBox1.TabIndex = 569;
            this.checkBox1.Text = "checkBox1";
            this.checkBox1.UseVisualStyleBackColor = true;
            // 
            // ugResultat
            // 
            this.ugResultat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ugResultat.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ugResultat.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ugResultat.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ugResultat.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ugResultat.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ugResultat.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ugResultat.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ugResultat.DisplayLayout.UseFixedHeaders = true;
            this.ugResultat.Location = new System.Drawing.Point(646, 6);
            this.ugResultat.Name = "ugResultat";
            this.ugResultat.Size = new System.Drawing.Size(178, 188);
            this.ugResultat.TabIndex = 571;
            this.ugResultat.Text = "Résultat";
            // 
            // ultraFormattedLinkLabel1
            // 
            this.ultraFormattedLinkLabel1.Location = new System.Drawing.Point(164, 85);
            this.ultraFormattedLinkLabel1.Name = "ultraFormattedLinkLabel1";
            this.ultraFormattedLinkLabel1.Size = new System.Drawing.Size(130, 23);
            this.ultraFormattedLinkLabel1.TabIndex = 576;
            this.ultraFormattedLinkLabel1.TabStop = true;
            this.ultraFormattedLinkLabel1.Value = "ultraFormattedLinkLabel1";
            // 
            // axCalendarControl1
            // 
            this.axCalendarControl1.Location = new System.Drawing.Point(879, 37);
            this.axCalendarControl1.Name = "axCalendarControl1";
            this.axCalendarControl1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axCalendarControl1.OcxState")));
            this.axCalendarControl1.Size = new System.Drawing.Size(95, 88);
            this.axCalendarControl1.TabIndex = 581;
            // 
            // axPlanning1
            // 
            this.axPlanning1.Enabled = true;
            this.axPlanning1.Location = new System.Drawing.Point(692, 526);
            this.axPlanning1.Name = "axPlanning1";
            this.axPlanning1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axPlanning1.OcxState")));
            this.axPlanning1.Size = new System.Drawing.Size(333, 138);
            this.axPlanning1.TabIndex = 580;
            // 
            // iTalk_TextBox_Small22
            // 
            this.iTalk_TextBox_Small22.AccAcceptNumbersOnly = false;
            this.iTalk_TextBox_Small22.AccAllowComma = false;
            this.iTalk_TextBox_Small22.AccBackgroundColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small22.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.iTalk_TextBox_Small22.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.iTalk_TextBox_Small22.AccHidenValue = "";
            this.iTalk_TextBox_Small22.AccNotAllowedChars = null;
            this.iTalk_TextBox_Small22.AccReadOnly = false;
            this.iTalk_TextBox_Small22.AccReadOnlyAllowDelete = false;
            this.iTalk_TextBox_Small22.AccRequired = false;
            this.iTalk_TextBox_Small22.BackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small22.CustomBackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small22.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.iTalk_TextBox_Small22.ForeColor = System.Drawing.Color.Black;
            this.iTalk_TextBox_Small22.Location = new System.Drawing.Point(164, 616);
            this.iTalk_TextBox_Small22.Margin = new System.Windows.Forms.Padding(2);
            this.iTalk_TextBox_Small22.MaxLength = 32767;
            this.iTalk_TextBox_Small22.Multiline = false;
            this.iTalk_TextBox_Small22.Name = "iTalk_TextBox_Small22";
            this.iTalk_TextBox_Small22.ReadOnly = false;
            this.iTalk_TextBox_Small22.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.iTalk_TextBox_Small22.Size = new System.Drawing.Size(135, 27);
            this.iTalk_TextBox_Small22.TabIndex = 582;
            this.iTalk_TextBox_Small22.Text = "iTalk_TextBox_Small22";
            this.iTalk_TextBox_Small22.TextAlignment = Infragistics.Win.HAlign.Left;
            this.iTalk_TextBox_Small22.UseSystemPasswordChar = false;
            // 
            // iTalk_RichTextBox1
            // 
            this.iTalk_RichTextBox1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.iTalk_RichTextBox1.AutoWordSelection = false;
            this.iTalk_RichTextBox1.BackColor = System.Drawing.Color.Transparent;
            this.iTalk_RichTextBox1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iTalk_RichTextBox1.ForeColor = System.Drawing.Color.Black;
            this.iTalk_RichTextBox1.Location = new System.Drawing.Point(863, 166);
            this.iTalk_RichTextBox1.Name = "iTalk_RichTextBox1";
            this.iTalk_RichTextBox1.ReadOnly = false;
            this.iTalk_RichTextBox1.Size = new System.Drawing.Size(150, 100);
            this.iTalk_RichTextBox1.TabIndex = 568;
            this.iTalk_RichTextBox1.WordWrap = true;
            // 
            // iTalk_TextBox_Small21
            // 
            this.iTalk_TextBox_Small21.AccAcceptNumbersOnly = false;
            this.iTalk_TextBox_Small21.AccAllowComma = false;
            this.iTalk_TextBox_Small21.AccBackgroundColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.iTalk_TextBox_Small21.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.iTalk_TextBox_Small21.AccHidenValue = "";
            this.iTalk_TextBox_Small21.AccNotAllowedChars = null;
            this.iTalk_TextBox_Small21.AccReadOnly = false;
            this.iTalk_TextBox_Small21.AccReadOnlyAllowDelete = false;
            this.iTalk_TextBox_Small21.AccRequired = false;
            this.iTalk_TextBox_Small21.BackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.CustomBackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iTalk_TextBox_Small21.ForeColor = System.Drawing.Color.Black;
            this.iTalk_TextBox_Small21.Location = new System.Drawing.Point(148, 30);
            this.iTalk_TextBox_Small21.Margin = new System.Windows.Forms.Padding(2);
            this.iTalk_TextBox_Small21.MaxLength = 32767;
            this.iTalk_TextBox_Small21.Multiline = false;
            this.iTalk_TextBox_Small21.Name = "iTalk_TextBox_Small21";
            this.iTalk_TextBox_Small21.ReadOnly = false;
            this.iTalk_TextBox_Small21.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.iTalk_TextBox_Small21.Size = new System.Drawing.Size(135, 27);
            this.iTalk_TextBox_Small21.TabIndex = 501;
            this.iTalk_TextBox_Small21.TextAlignment = Infragistics.Win.HAlign.Left;
            this.iTalk_TextBox_Small21.UseSystemPasswordChar = false;
            // 
            // ultraCalendarCombo2
            // 
            appearance25.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance25.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance25.FontData.Name = "Ubuntu";
            appearance25.FontData.SizeInPoints = 11F;
            this.ultraCalendarCombo2.Appearance = appearance25;
            this.ultraCalendarCombo2.AutoSize = false;
            this.ultraCalendarCombo2.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraCalendarCombo2.BorderStyleCalendar = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraCalendarCombo2.BorderStyleDay = Infragistics.Win.UIElementBorderStyle.Rounded4;
            appearance26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance26.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance26.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance26.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance26.ForeColor = System.Drawing.Color.White;
            appearance26.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraCalendarCombo2.DateButtonAppearance = appearance26;
            appearance27.BackColor = System.Drawing.Color.LightGray;
            appearance27.ForeColor = System.Drawing.Color.Black;
            this.ultraCalendarCombo2.DateButtonAreaAppearance = appearance27;
            this.ultraCalendarCombo2.DateButtonAreaVisible = false;
            this.ultraCalendarCombo2.DateButtons.Add(dateButton1);
            this.ultraCalendarCombo2.DayOfWeekCaptionStyle = Infragistics.Win.UltraWinSchedule.DayOfWeekCaptionStyle.ShortDescription;
            appearance28.BackColor = System.Drawing.Color.White;
            appearance28.FontData.SizeInPoints = 13F;
            this.ultraCalendarCombo2.DropDownAppearance = appearance28;
            this.ultraCalendarCombo2.Location = new System.Drawing.Point(670, 239);
            this.ultraCalendarCombo2.Margin = new System.Windows.Forms.Padding(0);
            this.ultraCalendarCombo2.MinimumDaySize = new System.Drawing.Size(1, 1);
            this.ultraCalendarCombo2.MinimumSize = new System.Drawing.Size(121, 27);
            this.ultraCalendarCombo2.MonthOrientation = Infragistics.Win.UltraWinSchedule.MonthOrientation.DownThenAcross;
            appearance29.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ultraCalendarCombo2.MonthPopupAppearance = appearance29;
            this.ultraCalendarCombo2.Name = "ultraCalendarCombo2";
            this.ultraCalendarCombo2.NonAutoSizeHeight = 27;
            appearance30.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance30.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance30.ForeColor = System.Drawing.Color.White;
            this.ultraCalendarCombo2.ScrollButtonAppearance = appearance30;
            this.ultraCalendarCombo2.Size = new System.Drawing.Size(121, 27);
            this.ultraCalendarCombo2.TabIndex = 585;
            this.ultraCalendarCombo2.WeekNumbersVisible = true;
            appearance31.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance31.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance31.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance31.BackHatchStyle = Infragistics.Win.BackHatchStyle.None;
            appearance31.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance31.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance31.ForeColor = System.Drawing.Color.White;
            appearance31.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraCalendarCombo2.YearScrollButtonAppearance = appearance31;
            this.ultraCalendarCombo2.YearScrollButtonsVisible = Infragistics.Win.DefaultableBoolean.True;
            // 
            // ultraCalendarCombo1
            // 
            appearance32.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance32.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance32.FontData.Name = "Ubuntu";
            appearance32.FontData.SizeInPoints = 11F;
            this.ultraCalendarCombo1.Appearance = appearance32;
            this.ultraCalendarCombo1.AutoSize = false;
            this.ultraCalendarCombo1.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraCalendarCombo1.BorderStyleCalendar = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraCalendarCombo1.BorderStyleDay = Infragistics.Win.UIElementBorderStyle.Rounded4;
            appearance33.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance33.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance33.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance33.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance33.ForeColor = System.Drawing.Color.White;
            appearance33.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraCalendarCombo1.DateButtonAppearance = appearance33;
            appearance34.BackColor = System.Drawing.Color.LightGray;
            appearance34.ForeColor = System.Drawing.Color.Black;
            this.ultraCalendarCombo1.DateButtonAreaAppearance = appearance34;
            this.ultraCalendarCombo1.DateButtonAreaVisible = false;
            this.ultraCalendarCombo1.DateButtons.Add(dateButton2);
            this.ultraCalendarCombo1.DayOfWeekCaptionStyle = Infragistics.Win.UltraWinSchedule.DayOfWeekCaptionStyle.ShortDescription;
            appearance35.BackColor = System.Drawing.Color.White;
            appearance35.FontData.SizeInPoints = 13F;
            this.ultraCalendarCombo1.DropDownAppearance = appearance35;
            this.ultraCalendarCombo1.Location = new System.Drawing.Point(495, 342);
            this.ultraCalendarCombo1.MinimumDaySize = new System.Drawing.Size(1, 1);
            this.ultraCalendarCombo1.MinimumSize = new System.Drawing.Size(121, 27);
            this.ultraCalendarCombo1.MonthOrientation = Infragistics.Win.UltraWinSchedule.MonthOrientation.DownThenAcross;
            appearance36.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ultraCalendarCombo1.MonthPopupAppearance = appearance36;
            this.ultraCalendarCombo1.Name = "ultraCalendarCombo1";
            this.ultraCalendarCombo1.NonAutoSizeHeight = 27;
            appearance37.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance37.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance37.ForeColor = System.Drawing.Color.White;
            this.ultraCalendarCombo1.ScrollButtonAppearance = appearance37;
            this.ultraCalendarCombo1.Size = new System.Drawing.Size(121, 27);
            this.ultraCalendarCombo1.TabIndex = 586;
            this.ultraCalendarCombo1.WeekNumbersVisible = true;
            appearance38.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance38.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance38.BackHatchStyle = Infragistics.Win.BackHatchStyle.None;
            appearance38.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance38.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance38.ForeColor = System.Drawing.Color.White;
            appearance38.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraCalendarCombo1.YearScrollButtonAppearance = appearance38;
            this.ultraCalendarCombo1.YearScrollButtonsVisible = Infragistics.Win.DefaultableBoolean.True;
            // 
            // CmdAffUtil
            // 
            this.CmdAffUtil.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdAffUtil.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdAffUtil.FlatAppearance.BorderSize = 0;
            this.CmdAffUtil.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdAffUtil.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdAffUtil.Image = ((System.Drawing.Image)(resources.GetObject("CmdAffUtil.Image")));
            this.CmdAffUtil.Location = new System.Drawing.Point(528, 59);
            this.CmdAffUtil.Margin = new System.Windows.Forms.Padding(2);
            this.CmdAffUtil.Name = "CmdAffUtil";
            this.CmdAffUtil.Size = new System.Drawing.Size(75, 38);
            this.CmdAffUtil.TabIndex = 579;
            this.CmdAffUtil.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CmdAffUtil.UseVisualStyleBackColor = false;
            // 
            // cmdDesAffUtil
            // 
            this.cmdDesAffUtil.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdDesAffUtil.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdDesAffUtil.FlatAppearance.BorderSize = 0;
            this.cmdDesAffUtil.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDesAffUtil.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdDesAffUtil.Image = ((System.Drawing.Image)(resources.GetObject("cmdDesAffUtil.Image")));
            this.cmdDesAffUtil.Location = new System.Drawing.Point(528, 123);
            this.cmdDesAffUtil.Margin = new System.Windows.Forms.Padding(2);
            this.cmdDesAffUtil.Name = "cmdDesAffUtil";
            this.cmdDesAffUtil.Size = new System.Drawing.Size(75, 40);
            this.cmdDesAffUtil.TabIndex = 578;
            this.cmdDesAffUtil.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdDesAffUtil.UseVisualStyleBackColor = false;
            // 
            // button27
            // 
            this.button27.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(156)))), ((int)(((byte)(100)))), ((int)(((byte)(109)))));
            this.button27.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button27.FlatAppearance.BorderSize = 0;
            this.button27.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button27.Image = global::Axe_interDT.Properties.Resources.audio_24x24;
            this.button27.Location = new System.Drawing.Point(335, 277);
            this.button27.Margin = new System.Windows.Forms.Padding(2);
            this.button27.Name = "button27";
            this.button27.Size = new System.Drawing.Size(60, 35);
            this.button27.TabIndex = 577;
            this.button27.UseVisualStyleBackColor = false;
            // 
            // button26
            // 
            this.button26.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(159)))), ((int)(((byte)(118)))));
            this.button26.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button26.FlatAppearance.BorderSize = 0;
            this.button26.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button26.Image = global::Axe_interDT.Properties.Resources.folder_24x24;
            this.button26.Location = new System.Drawing.Point(272, 277);
            this.button26.Margin = new System.Windows.Forms.Padding(2);
            this.button26.Name = "button26";
            this.button26.Size = new System.Drawing.Size(60, 35);
            this.button26.TabIndex = 575;
            this.button26.UseVisualStyleBackColor = false;
            // 
            // button25
            // 
            this.button25.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(108)))), ((int)(((byte)(99)))));
            this.button25.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button25.FlatAppearance.BorderSize = 0;
            this.button25.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button25.ForeColor = System.Drawing.Color.White;
            this.button25.Image = global::Axe_interDT.Properties.Resources.cancel_24;
            this.button25.Location = new System.Drawing.Point(208, 277);
            this.button25.Margin = new System.Windows.Forms.Padding(2);
            this.button25.Name = "button25";
            this.button25.Size = new System.Drawing.Size(60, 35);
            this.button25.TabIndex = 574;
            this.button25.Text = "   Annuler";
            this.button25.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button25.UseVisualStyleBackColor = false;
            // 
            // button24
            // 
            this.button24.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(121)))), ((int)(((byte)(196)))));
            this.button24.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button24.FlatAppearance.BorderSize = 0;
            this.button24.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button24.ForeColor = System.Drawing.Color.White;
            this.button24.Image = global::Axe_interDT.Properties.Resources.Buy_16x16;
            this.button24.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button24.Location = new System.Drawing.Point(375, 459);
            this.button24.Margin = new System.Windows.Forms.Padding(2);
            this.button24.Name = "button24";
            this.button24.Size = new System.Drawing.Size(85, 35);
            this.button24.TabIndex = 573;
            this.button24.Text = "      Caddy";
            this.button24.UseVisualStyleBackColor = false;
            // 
            // button23
            // 
            this.button23.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(204)))));
            this.button23.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button23.FlatAppearance.BorderSize = 0;
            this.button23.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button23.ForeColor = System.Drawing.Color.White;
            this.button23.Image = global::Axe_interDT.Properties.Resources.Help_16x16;
            this.button23.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button23.Location = new System.Drawing.Point(283, 498);
            this.button23.Margin = new System.Windows.Forms.Padding(2);
            this.button23.Name = "button23";
            this.button23.Size = new System.Drawing.Size(85, 35);
            this.button23.TabIndex = 572;
            this.button23.Text = "      Aide";
            this.button23.UseVisualStyleBackColor = false;
            // 
            // button22
            // 
            this.button22.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(159)))), ((int)(((byte)(118)))));
            this.button22.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button22.FlatAppearance.BorderSize = 0;
            this.button22.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button22.ForeColor = System.Drawing.Color.White;
            this.button22.Image = global::Axe_interDT.Properties.Resources.Eye_16x16;
            this.button22.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button22.Location = new System.Drawing.Point(194, 498);
            this.button22.Margin = new System.Windows.Forms.Padding(2);
            this.button22.Name = "button22";
            this.button22.Size = new System.Drawing.Size(85, 35);
            this.button22.TabIndex = 570;
            this.button22.Text = "     Visu";
            this.button22.UseVisualStyleBackColor = false;
            // 
            // button20
            // 
            this.button20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(90)))), ((int)(((byte)(173)))), ((int)(((byte)(163)))));
            this.button20.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button20.FlatAppearance.BorderSize = 0;
            this.button20.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button20.ForeColor = System.Drawing.Color.White;
            this.button20.Image = global::Axe_interDT.Properties.Resources.Control_16X16;
            this.button20.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button20.Location = new System.Drawing.Point(105, 498);
            this.button20.Margin = new System.Windows.Forms.Padding(2);
            this.button20.Name = "button20";
            this.button20.Size = new System.Drawing.Size(85, 35);
            this.button20.TabIndex = 567;
            this.button20.Text = "    Controle";
            this.button20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button20.UseVisualStyleBackColor = false;
            // 
            // button21
            // 
            this.button21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(108)))), ((int)(((byte)(99)))));
            this.button21.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button21.FlatAppearance.BorderSize = 0;
            this.button21.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button21.ForeColor = System.Drawing.Color.White;
            this.button21.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.button21.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button21.Location = new System.Drawing.Point(16, 498);
            this.button21.Margin = new System.Windows.Forms.Padding(2);
            this.button21.Name = "button21";
            this.button21.Size = new System.Drawing.Size(85, 35);
            this.button21.TabIndex = 566;
            this.button21.Text = "   Annuler";
            this.button21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button21.UseVisualStyleBackColor = false;
            // 
            // cmdPrecedent
            // 
            this.cmdPrecedent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(162)))), ((int)(((byte)(211)))));
            this.cmdPrecedent.FlatAppearance.BorderSize = 0;
            this.cmdPrecedent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPrecedent.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdPrecedent.Image = global::Axe_interDT.Properties.Resources.previous_16;
            this.cmdPrecedent.Location = new System.Drawing.Point(118, 289);
            this.cmdPrecedent.Name = "cmdPrecedent";
            this.cmdPrecedent.Size = new System.Drawing.Size(25, 23);
            this.cmdPrecedent.TabIndex = 499;
            this.cmdPrecedent.UseVisualStyleBackColor = false;
            // 
            // cmdSuivant
            // 
            this.cmdSuivant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(162)))), ((int)(((byte)(211)))));
            this.cmdSuivant.FlatAppearance.BorderSize = 0;
            this.cmdSuivant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSuivant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdSuivant.Image = ((System.Drawing.Image)(resources.GetObject("cmdSuivant.Image")));
            this.cmdSuivant.Location = new System.Drawing.Point(144, 289);
            this.cmdSuivant.Name = "cmdSuivant";
            this.cmdSuivant.Size = new System.Drawing.Size(25, 23);
            this.cmdSuivant.TabIndex = 498;
            this.cmdSuivant.UseVisualStyleBackColor = false;
            // 
            // cmdDernier
            // 
            this.cmdDernier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(162)))), ((int)(((byte)(211)))));
            this.cmdDernier.FlatAppearance.BorderSize = 0;
            this.cmdDernier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDernier.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdDernier.Image = global::Axe_interDT.Properties.Resources.last_16;
            this.cmdDernier.Location = new System.Drawing.Point(170, 289);
            this.cmdDernier.Name = "cmdDernier";
            this.cmdDernier.Size = new System.Drawing.Size(25, 23);
            this.cmdDernier.TabIndex = 497;
            this.cmdDernier.UseVisualStyleBackColor = false;
            // 
            // cmdPremier
            // 
            this.cmdPremier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(162)))), ((int)(((byte)(211)))));
            this.cmdPremier.FlatAppearance.BorderSize = 0;
            this.cmdPremier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPremier.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdPremier.Image = global::Axe_interDT.Properties.Resources.first_16;
            this.cmdPremier.Location = new System.Drawing.Point(92, 289);
            this.cmdPremier.Name = "cmdPremier";
            this.cmdPremier.Size = new System.Drawing.Size(25, 23);
            this.cmdPremier.TabIndex = 496;
            this.cmdPremier.UseVisualStyleBackColor = false;
            // 
            // button18
            // 
            this.button18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(177)))), ((int)(((byte)(185)))));
            this.button18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button18.FlatAppearance.BorderSize = 0;
            this.button18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button18.Image = global::Axe_interDT.Properties.Resources.Right_Arrow_24x24;
            this.button18.Location = new System.Drawing.Point(16, 277);
            this.button18.Margin = new System.Windows.Forms.Padding(2);
            this.button18.Name = "button18";
            this.button18.Size = new System.Drawing.Size(60, 35);
            this.button18.TabIndex = 404;
            this.button18.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button18.UseVisualStyleBackColor = false;
            // 
            // button17
            // 
            this.button17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.button17.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button17.FlatAppearance.BorderSize = 0;
            this.button17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button17.ForeColor = System.Drawing.Color.White;
            this.button17.Image = global::Axe_interDT.Properties.Resources.Copy_16x16;
            this.button17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button17.Location = new System.Drawing.Point(283, 458);
            this.button17.Margin = new System.Windows.Forms.Padding(2);
            this.button17.Name = "button17";
            this.button17.Size = new System.Drawing.Size(85, 35);
            this.button17.TabIndex = 403;
            this.button17.Text = "       Copier";
            this.button17.UseVisualStyleBackColor = false;
            // 
            // button16
            // 
            this.button16.BackColor = System.Drawing.Color.Silver;
            this.button16.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button16.FlatAppearance.BorderSize = 0;
            this.button16.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button16.ForeColor = System.Drawing.Color.White;
            this.button16.Image = global::Axe_interDT.Properties.Resources.Eraser_16x16;
            this.button16.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button16.Location = new System.Drawing.Point(194, 458);
            this.button16.Margin = new System.Windows.Forms.Padding(2);
            this.button16.Name = "button16";
            this.button16.Size = new System.Drawing.Size(85, 35);
            this.button16.TabIndex = 402;
            this.button16.Text = "   Clean";
            this.button16.UseVisualStyleBackColor = false;
            // 
            // button15
            // 
            this.button15.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(204)))));
            this.button15.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button15.FlatAppearance.BorderSize = 0;
            this.button15.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button15.ForeColor = System.Drawing.Color.White;
            this.button15.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.button15.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button15.Location = new System.Drawing.Point(105, 458);
            this.button15.Margin = new System.Windows.Forms.Padding(2);
            this.button15.Name = "button15";
            this.button15.Size = new System.Drawing.Size(85, 35);
            this.button15.TabIndex = 401;
            this.button15.Text = "     init.";
            this.button15.UseVisualStyleBackColor = false;
            // 
            // button14
            // 
            this.button14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.button14.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button14.FlatAppearance.BorderSize = 0;
            this.button14.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button14.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button14.ForeColor = System.Drawing.Color.White;
            this.button14.Image = global::Axe_interDT.Properties.Resources.Send_Arrow_24x24;
            this.button14.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button14.Location = new System.Drawing.Point(16, 459);
            this.button14.Margin = new System.Windows.Forms.Padding(2);
            this.button14.Name = "button14";
            this.button14.Size = new System.Drawing.Size(85, 35);
            this.button14.TabIndex = 400;
            this.button14.Text = "        Envoyer";
            this.button14.UseVisualStyleBackColor = false;
            // 
            // button13
            // 
            this.button13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(54)))), ((int)(((byte)(122)))));
            this.button13.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button13.FlatAppearance.BorderSize = 0;
            this.button13.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button13.ForeColor = System.Drawing.Color.White;
            this.button13.Image = global::Axe_interDT.Properties.Resources.Trash_16x16;
            this.button13.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button13.Location = new System.Drawing.Point(283, 419);
            this.button13.Margin = new System.Windows.Forms.Padding(2);
            this.button13.Name = "button13";
            this.button13.Size = new System.Drawing.Size(85, 35);
            this.button13.TabIndex = 399;
            this.button13.Text = "    Supp.";
            this.button13.UseVisualStyleBackColor = false;
            // 
            // button12
            // 
            this.button12.BackColor = System.Drawing.Color.Silver;
            this.button12.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button12.FlatAppearance.BorderSize = 0;
            this.button12.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button12.Image = global::Axe_interDT.Properties.Resources.Stop_24x24;
            this.button12.Location = new System.Drawing.Point(528, 238);
            this.button12.Margin = new System.Windows.Forms.Padding(2);
            this.button12.Name = "button12";
            this.button12.Size = new System.Drawing.Size(60, 35);
            this.button12.TabIndex = 398;
            this.button12.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button12.UseVisualStyleBackColor = false;
            // 
            // button11
            // 
            this.button11.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(175)))), ((int)(((byte)(121)))), ((int)(((byte)(196)))));
            this.button11.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button11.FlatAppearance.BorderSize = 0;
            this.button11.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button11.ForeColor = System.Drawing.Color.White;
            this.button11.Image = global::Axe_interDT.Properties.Resources.Calculator_16x16;
            this.button11.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button11.Location = new System.Drawing.Point(194, 419);
            this.button11.Margin = new System.Windows.Forms.Padding(2);
            this.button11.Name = "button11";
            this.button11.Size = new System.Drawing.Size(85, 35);
            this.button11.TabIndex = 397;
            this.button11.Text = "      Calculer";
            this.button11.UseVisualStyleBackColor = false;
            // 
            // button10
            // 
            this.button10.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(178)))), ((int)(((byte)(67)))));
            this.button10.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button10.FlatAppearance.BorderSize = 0;
            this.button10.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button10.ForeColor = System.Drawing.Color.White;
            this.button10.Image = global::Axe_interDT.Properties.Resources.Save_16x16;
            this.button10.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button10.Location = new System.Drawing.Point(105, 420);
            this.button10.Margin = new System.Windows.Forms.Padding(2);
            this.button10.Name = "button10";
            this.button10.Size = new System.Drawing.Size(85, 35);
            this.button10.TabIndex = 396;
            this.button10.Text = "      Sauv.";
            this.button10.UseVisualStyleBackColor = false;
            // 
            // button9
            // 
            this.button9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(152)))), ((int)(((byte)(187)))));
            this.button9.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button9.FlatAppearance.BorderSize = 0;
            this.button9.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button9.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.999999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button9.ForeColor = System.Drawing.Color.White;
            this.button9.Image = global::Axe_interDT.Properties.Resources.Search_16x16;
            this.button9.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button9.Location = new System.Drawing.Point(16, 420);
            this.button9.Margin = new System.Windows.Forms.Padding(2);
            this.button9.Name = "button9";
            this.button9.Size = new System.Drawing.Size(85, 35);
            this.button9.TabIndex = 393;
            this.button9.Text = "     Chercher";
            this.button9.UseVisualStyleBackColor = false;
            // 
            // button8
            // 
            this.button8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(72)))), ((int)(((byte)(131)))));
            this.button8.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button8.FlatAppearance.BorderSize = 0;
            this.button8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button8.ForeColor = System.Drawing.Color.White;
            this.button8.Image = global::Axe_interDT.Properties.Resources.Excel_16x16;
            this.button8.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button8.Location = new System.Drawing.Point(283, 381);
            this.button8.Margin = new System.Windows.Forms.Padding(2);
            this.button8.Name = "button8";
            this.button8.Size = new System.Drawing.Size(85, 35);
            this.button8.TabIndex = 392;
            this.button8.Text = "       Excel";
            this.button8.UseVisualStyleBackColor = false;
            // 
            // button7
            // 
            this.button7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(204)))));
            this.button7.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button7.FlatAppearance.BorderSize = 0;
            this.button7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button7.ForeColor = System.Drawing.Color.White;
            this.button7.Image = global::Axe_interDT.Properties.Resources.Printer_16x16;
            this.button7.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button7.Location = new System.Drawing.Point(194, 381);
            this.button7.Margin = new System.Windows.Forms.Padding(2);
            this.button7.Name = "button7";
            this.button7.Size = new System.Drawing.Size(85, 35);
            this.button7.TabIndex = 391;
            this.button7.Text = "      Impr.";
            this.button7.UseVisualStyleBackColor = false;
            // 
            // Cmd17
            // 
            this.Cmd17.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(201)))), ((int)(((byte)(175)))));
            this.Cmd17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Cmd17.FlatAppearance.BorderSize = 0;
            this.Cmd17.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cmd17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Cmd17.ForeColor = System.Drawing.Color.White;
            this.Cmd17.Image = global::Axe_interDT.Properties.Resources.Ok_16x16;
            this.Cmd17.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Cmd17.Location = new System.Drawing.Point(105, 381);
            this.Cmd17.Margin = new System.Windows.Forms.Padding(2);
            this.Cmd17.Name = "Cmd17";
            this.Cmd17.Size = new System.Drawing.Size(85, 35);
            this.Cmd17.TabIndex = 390;
            this.Cmd17.Text = "     Valider";
            this.Cmd17.UseVisualStyleBackColor = false;
            // 
            // Cmd18
            // 
            this.Cmd18.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(108)))), ((int)(((byte)(99)))));
            this.Cmd18.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Cmd18.FlatAppearance.BorderSize = 0;
            this.Cmd18.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cmd18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Cmd18.ForeColor = System.Drawing.Color.White;
            this.Cmd18.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.Cmd18.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Cmd18.Location = new System.Drawing.Point(16, 381);
            this.Cmd18.Margin = new System.Windows.Forms.Padding(2);
            this.Cmd18.Name = "Cmd18";
            this.Cmd18.Size = new System.Drawing.Size(85, 35);
            this.Cmd18.TabIndex = 389;
            this.Cmd18.Text = "Annuler";
            this.Cmd18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Cmd18.UseVisualStyleBackColor = false;
            // 
            // button6
            // 
            this.button6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(95)))), ((int)(((byte)(97)))));
            this.button6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button6.FlatAppearance.BorderSize = 0;
            this.button6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button6.ForeColor = System.Drawing.Color.White;
            this.button6.Image = global::Axe_interDT.Properties.Resources.Edit_16x16;
            this.button6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button6.Location = new System.Drawing.Point(283, 342);
            this.button6.Margin = new System.Windows.Forms.Padding(2);
            this.button6.Name = "button6";
            this.button6.Size = new System.Drawing.Size(85, 35);
            this.button6.TabIndex = 387;
            this.button6.Text = "      Modifier";
            this.button6.UseVisualStyleBackColor = false;
            // 
            // button5
            // 
            this.button5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(108)))), ((int)(((byte)(99)))));
            this.button5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button5.FlatAppearance.BorderSize = 0;
            this.button5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button5.ForeColor = System.Drawing.Color.White;
            this.button5.Image = global::Axe_interDT.Properties.Resources.Add_File_16x16;
            this.button5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button5.Location = new System.Drawing.Point(16, 342);
            this.button5.Margin = new System.Windows.Forms.Padding(2);
            this.button5.Name = "button5";
            this.button5.Size = new System.Drawing.Size(85, 35);
            this.button5.TabIndex = 386;
            this.button5.Text = "Ajouter";
            this.button5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button5.UseVisualStyleBackColor = false;
            // 
            // button4
            // 
            this.button4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(152)))), ((int)(((byte)(187)))));
            this.button4.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.button4.FlatAppearance.BorderSize = 0;
            this.button4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button4.ForeColor = System.Drawing.Color.White;
            this.button4.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.button4.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button4.Location = new System.Drawing.Point(194, 342);
            this.button4.Margin = new System.Windows.Forms.Padding(2);
            this.button4.Name = "button4";
            this.button4.Size = new System.Drawing.Size(85, 35);
            this.button4.TabIndex = 385;
            this.button4.Text = "       Supp.";
            this.button4.UseVisualStyleBackColor = false;
            // 
            // button3
            // 
            this.button3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(71)))), ((int)(((byte)(201)))), ((int)(((byte)(175)))));
            this.button3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button3.FlatAppearance.BorderSize = 0;
            this.button3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.button3.ForeColor = System.Drawing.Color.White;
            this.button3.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.button3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.button3.Location = new System.Drawing.Point(105, 342);
            this.button3.Margin = new System.Windows.Forms.Padding(2);
            this.button3.Name = "button3";
            this.button3.Size = new System.Drawing.Size(85, 35);
            this.button3.TabIndex = 384;
            this.button3.Text = "Annuler";
            this.button3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.button3.UseVisualStyleBackColor = false;
            // 
            // CmdEditer
            // 
            this.CmdEditer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(95)))), ((int)(((byte)(95)))), ((int)(((byte)(97)))));
            this.CmdEditer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdEditer.FlatAppearance.BorderSize = 0;
            this.CmdEditer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdEditer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdEditer.Image = global::Axe_interDT.Properties.Resources.Edit_32x32;
            this.CmdEditer.Location = new System.Drawing.Point(464, 238);
            this.CmdEditer.Margin = new System.Windows.Forms.Padding(2);
            this.CmdEditer.Name = "CmdEditer";
            this.CmdEditer.Size = new System.Drawing.Size(60, 35);
            this.CmdEditer.TabIndex = 382;
            this.CmdEditer.UseVisualStyleBackColor = false;
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(2)))), ((int)(((byte)(72)))), ((int)(((byte)(131)))));
            this.Command1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command1.Image = global::Axe_interDT.Properties.Resources.Excel_32x32;
            this.Command1.Location = new System.Drawing.Point(592, 199);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(60, 35);
            this.Command1.TabIndex = 381;
            this.Command1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Command1.UseVisualStyleBackColor = false;
            // 
            // button2
            // 
            this.button2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(204)))));
            this.button2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button2.FlatAppearance.BorderSize = 0;
            this.button2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button2.Image = global::Axe_interDT.Properties.Resources.Printer_24x24;
            this.button2.Location = new System.Drawing.Point(16, 238);
            this.button2.Margin = new System.Windows.Forms.Padding(2);
            this.button2.Name = "button2";
            this.button2.Size = new System.Drawing.Size(60, 35);
            this.button2.TabIndex = 378;
            this.button2.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button2.UseVisualStyleBackColor = false;
            // 
            // button1
            // 
            this.button1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(177)))), ((int)(((byte)(185)))));
            this.button1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.button1.FlatAppearance.BorderSize = 0;
            this.button1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.button1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.button1.Image = global::Axe_interDT.Properties.Resources.cancel_24;
            this.button1.Location = new System.Drawing.Point(80, 238);
            this.button1.Margin = new System.Windows.Forms.Padding(2);
            this.button1.Name = "button1";
            this.button1.Size = new System.Drawing.Size(60, 35);
            this.button1.TabIndex = 377;
            this.button1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.button1.UseVisualStyleBackColor = false;
            // 
            // cmdOK
            // 
            this.cmdOK.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(127)))), ((int)(((byte)(115)))), ((int)(((byte)(153)))));
            this.cmdOK.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdOK.FlatAppearance.BorderSize = 0;
            this.cmdOK.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdOK.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdOK.Image = global::Axe_interDT.Properties.Resources.Ok_24x24;
            this.cmdOK.Location = new System.Drawing.Point(464, 199);
            this.cmdOK.Margin = new System.Windows.Forms.Padding(2);
            this.cmdOK.Name = "cmdOK";
            this.cmdOK.Size = new System.Drawing.Size(60, 35);
            this.cmdOK.TabIndex = 371;
            this.cmdOK.UseVisualStyleBackColor = false;
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(204)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.cmdAnnuler.Location = new System.Drawing.Point(528, 199);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(60, 35);
            this.cmdAnnuler.TabIndex = 372;
            this.cmdAnnuler.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            // 
            // cmdVisu
            // 
            this.cmdVisu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(182)))), ((int)(((byte)(159)))), ((int)(((byte)(118)))));
            this.cmdVisu.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdVisu.FlatAppearance.BorderSize = 0;
            this.cmdVisu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdVisu.Image = global::Axe_interDT.Properties.Resources.Eye_24x24;
            this.cmdVisu.Location = new System.Drawing.Point(272, 238);
            this.cmdVisu.Margin = new System.Windows.Forms.Padding(2);
            this.cmdVisu.Name = "cmdVisu";
            this.cmdVisu.Size = new System.Drawing.Size(60, 35);
            this.cmdVisu.TabIndex = 370;
            this.cmdVisu.UseVisualStyleBackColor = false;
            // 
            // cmdMail
            // 
            this.cmdMail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(53)))), ((int)(((byte)(152)))), ((int)(((byte)(219)))));
            this.cmdMail.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdMail.FlatAppearance.BorderSize = 0;
            this.cmdMail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMail.Image = global::Axe_interDT.Properties.Resources.Email_24x24;
            this.cmdMail.Location = new System.Drawing.Point(144, 238);
            this.cmdMail.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMail.Name = "cmdMail";
            this.cmdMail.Size = new System.Drawing.Size(60, 35);
            this.cmdMail.TabIndex = 368;
            this.cmdMail.UseVisualStyleBackColor = false;
            // 
            // cmdFax
            // 
            this.cmdFax.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(154)))), ((int)(((byte)(98)))), ((int)(((byte)(97)))));
            this.cmdFax.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdFax.FlatAppearance.BorderSize = 0;
            this.cmdFax.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFax.Image = global::Axe_interDT.Properties.Resources.Fax_32x32;
            this.cmdFax.Location = new System.Drawing.Point(208, 238);
            this.cmdFax.Margin = new System.Windows.Forms.Padding(2);
            this.cmdFax.Name = "cmdFax";
            this.cmdFax.Size = new System.Drawing.Size(60, 35);
            this.cmdFax.TabIndex = 369;
            this.cmdFax.UseVisualStyleBackColor = false;
            // 
            // cmdLettre
            // 
            this.cmdLettre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(152)))), ((int)(((byte)(187)))));
            this.cmdLettre.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdLettre.FlatAppearance.BorderSize = 0;
            this.cmdLettre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdLettre.Image = global::Axe_interDT.Properties.Resources.new_post_24;
            this.cmdLettre.Location = new System.Drawing.Point(400, 238);
            this.cmdLettre.Margin = new System.Windows.Forms.Padding(2);
            this.cmdLettre.Name = "cmdLettre";
            this.cmdLettre.Size = new System.Drawing.Size(60, 35);
            this.cmdLettre.TabIndex = 366;
            this.cmdLettre.UseVisualStyleBackColor = false;
            // 
            // cmdImprimer
            // 
            this.cmdImprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(107)))), ((int)(((byte)(177)))), ((int)(((byte)(185)))));
            this.cmdImprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdImprimer.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdImprimer.FlatAppearance.BorderSize = 0;
            this.cmdImprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImprimer.Image = global::Axe_interDT.Properties.Resources.Printer_24x24;
            this.cmdImprimer.Location = new System.Drawing.Point(336, 238);
            this.cmdImprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdImprimer.Name = "cmdImprimer";
            this.cmdImprimer.Size = new System.Drawing.Size(60, 35);
            this.cmdImprimer.TabIndex = 367;
            this.cmdImprimer.UseVisualStyleBackColor = false;
            // 
            // cmdRechercheArticle
            // 
            this.cmdRechercheArticle.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(162)))), ((int)(((byte)(211)))));
            this.cmdRechercheArticle.FlatAppearance.BorderSize = 0;
            this.cmdRechercheArticle.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheArticle.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheArticle.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheArticle.Location = new System.Drawing.Point(593, 246);
            this.cmdRechercheArticle.Name = "cmdRechercheArticle";
            this.cmdRechercheArticle.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheArticle.TabIndex = 365;
            this.cmdRechercheArticle.UseVisualStyleBackColor = false;
            // 
            // cmdRecherche
            // 
            this.cmdRecherche.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(232)))), ((int)(((byte)(152)))), ((int)(((byte)(187)))));
            this.cmdRecherche.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdRecherche.FlatAppearance.BorderSize = 0;
            this.cmdRecherche.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRecherche.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRecherche.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmdRecherche.Location = new System.Drawing.Point(16, 199);
            this.cmdRecherche.Margin = new System.Windows.Forms.Padding(2);
            this.cmdRecherche.Name = "cmdRecherche";
            this.cmdRecherche.Size = new System.Drawing.Size(60, 35);
            this.cmdRecherche.TabIndex = 358;
            this.cmdRecherche.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdRecherche.UseVisualStyleBackColor = false;
            // 
            // cmdMAJ
            // 
            this.cmdMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(248)))), ((int)(((byte)(178)))), ((int)(((byte)(67)))));
            this.cmdMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJ.FlatAppearance.BorderSize = 0;
            this.cmdMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMAJ.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdMAJ.Location = new System.Drawing.Point(80, 199);
            this.cmdMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJ.Name = "cmdMAJ";
            this.cmdMAJ.Size = new System.Drawing.Size(60, 35);
            this.cmdMAJ.TabIndex = 359;
            this.cmdMAJ.UseVisualStyleBackColor = false;
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(242)))), ((int)(((byte)(108)))), ((int)(((byte)(99)))));
            this.cmdAjouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAjouter.Image = global::Axe_interDT.Properties.Resources.Add_File_24x24;
            this.cmdAjouter.Location = new System.Drawing.Point(144, 199);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(60, 35);
            this.cmdAjouter.TabIndex = 360;
            this.cmdAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdAjouter.UseVisualStyleBackColor = false;
            // 
            // CmdHistoDevis
            // 
            this.CmdHistoDevis.BackColor = System.Drawing.Color.Silver;
            this.CmdHistoDevis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdHistoDevis.FlatAppearance.BorderSize = 0;
            this.CmdHistoDevis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdHistoDevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdHistoDevis.Image = global::Axe_interDT.Properties.Resources.Clock_24x24;
            this.CmdHistoDevis.Location = new System.Drawing.Point(400, 199);
            this.CmdHistoDevis.Margin = new System.Windows.Forms.Padding(2);
            this.CmdHistoDevis.Name = "CmdHistoDevis";
            this.CmdHistoDevis.Size = new System.Drawing.Size(60, 35);
            this.CmdHistoDevis.TabIndex = 364;
            this.CmdHistoDevis.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CmdHistoDevis.UseVisualStyleBackColor = false;
            // 
            // cmdMAJE
            // 
            this.cmdMAJE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(130)))), ((int)(((byte)(216)))), ((int)(((byte)(181)))));
            this.cmdMAJE.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJE.FlatAppearance.BorderSize = 0;
            this.cmdMAJE.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJE.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMAJE.Image = global::Axe_interDT.Properties.Resources.Ok_24x24;
            this.cmdMAJE.Location = new System.Drawing.Point(208, 199);
            this.cmdMAJE.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJE.Name = "cmdMAJE";
            this.cmdMAJE.Size = new System.Drawing.Size(60, 35);
            this.cmdMAJE.TabIndex = 361;
            this.cmdMAJE.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdMAJE.UseVisualStyleBackColor = false;
            // 
            // cmdSupprimer
            // 
            this.cmdSupprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(54)))), ((int)(((byte)(122)))));
            this.cmdSupprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSupprimer.FlatAppearance.BorderSize = 0;
            this.cmdSupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSupprimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSupprimer.Image = global::Axe_interDT.Properties.Resources.Trash_24x24;
            this.cmdSupprimer.Location = new System.Drawing.Point(336, 199);
            this.cmdSupprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSupprimer.Name = "cmdSupprimer";
            this.cmdSupprimer.Size = new System.Drawing.Size(60, 35);
            this.cmdSupprimer.TabIndex = 363;
            this.cmdSupprimer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdSupprimer.UseVisualStyleBackColor = false;
            // 
            // CmdMenuDevis
            // 
            this.CmdMenuDevis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(168)))), ((int)(((byte)(120)))), ((int)(((byte)(120)))));
            this.CmdMenuDevis.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdMenuDevis.FlatAppearance.BorderSize = 0;
            this.CmdMenuDevis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdMenuDevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdMenuDevis.Image = global::Axe_interDT.Properties.Resources.Menu_24x24;
            this.CmdMenuDevis.Location = new System.Drawing.Point(272, 199);
            this.CmdMenuDevis.Margin = new System.Windows.Forms.Padding(2);
            this.CmdMenuDevis.Name = "CmdMenuDevis";
            this.CmdMenuDevis.Size = new System.Drawing.Size(60, 35);
            this.CmdMenuDevis.TabIndex = 362;
            this.CmdMenuDevis.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CmdMenuDevis.UseVisualStyleBackColor = false;
            // 
            // axWinsock1
            // 
            this.axWinsock1.Enabled = true;
            this.axWinsock1.Location = new System.Drawing.Point(480, 629);
            this.axWinsock1.Name = "axWinsock1";
            this.axWinsock1.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("axWinsock1.OcxState")));
            this.axWinsock1.Size = new System.Drawing.Size(28, 28);
            this.axWinsock1.TabIndex = 587;
            // 
            // ThemeControls
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.axWinsock1);
            this.Controls.Add(this.ultraCalendarCombo1);
            this.Controls.Add(this.ultraCalendarCombo2);
            this.Controls.Add(this.iTalk_TextBox_Small22);
            this.Controls.Add(this.axCalendarControl1);
            this.Controls.Add(this.axPlanning1);
            this.Controls.Add(this.CmdAffUtil);
            this.Controls.Add(this.cmdDesAffUtil);
            this.Controls.Add(this.button27);
            this.Controls.Add(this.ultraFormattedLinkLabel1);
            this.Controls.Add(this.button26);
            this.Controls.Add(this.button25);
            this.Controls.Add(this.button24);
            this.Controls.Add(this.button23);
            this.Controls.Add(this.ugResultat);
            this.Controls.Add(this.button22);
            this.Controls.Add(this.checkBox1);
            this.Controls.Add(this.iTalk_RichTextBox1);
            this.Controls.Add(this.button20);
            this.Controls.Add(this.button21);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.panel4);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.radioButton1);
            this.Controls.Add(this.Combo1);
            this.Controls.Add(this.cmdPrecedent);
            this.Controls.Add(this.cmdSuivant);
            this.Controls.Add(this.cmdDernier);
            this.Controls.Add(this.cmdPremier);
            this.Controls.Add(this.button19);
            this.Controls.Add(this.Grids);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.SSTab1);
            this.Controls.Add(this.iTalk_TextBox_Small21);
            this.Controls.Add(this.button18);
            this.Controls.Add(this.button17);
            this.Controls.Add(this.button16);
            this.Controls.Add(this.button15);
            this.Controls.Add(this.button14);
            this.Controls.Add(this.button13);
            this.Controls.Add(this.button12);
            this.Controls.Add(this.button11);
            this.Controls.Add(this.button10);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.button9);
            this.Controls.Add(this.button8);
            this.Controls.Add(this.button7);
            this.Controls.Add(this.Cmd17);
            this.Controls.Add(this.Cmd18);
            this.Controls.Add(this.button6);
            this.Controls.Add(this.button5);
            this.Controls.Add(this.button4);
            this.Controls.Add(this.button3);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.CmdEditer);
            this.Controls.Add(this.Command1);
            this.Controls.Add(this.button2);
            this.Controls.Add(this.button1);
            this.Controls.Add(this.cmdOK);
            this.Controls.Add(this.cmdAnnuler);
            this.Controls.Add(this.cmdVisu);
            this.Controls.Add(this.cmdMail);
            this.Controls.Add(this.cmdFax);
            this.Controls.Add(this.cmdLettre);
            this.Controls.Add(this.cmdImprimer);
            this.Controls.Add(this.cmdRechercheArticle);
            this.Controls.Add(this.cmdRecherche);
            this.Controls.Add(this.cmdMAJ);
            this.Controls.Add(this.cmdAjouter);
            this.Controls.Add(this.CmdHistoDevis);
            this.Controls.Add(this.cmdMAJE);
            this.Controls.Add(this.cmdSupprimer);
            this.Controls.Add(this.CmdMenuDevis);
            this.Name = "ThemeControls";
            this.Size = new System.Drawing.Size(1110, 710);
            this.Load += new System.EventHandler(this.ThemeControls_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).EndInit();
            this.SSTab1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Grids)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.Combo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ugResultat)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraFlowLayoutManager1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axCalendarControl1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axPlanning1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarCombo2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraCalendarCombo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.axWinsock1)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.Button cmdRecherche;
        public System.Windows.Forms.Button cmdMAJ;
        public System.Windows.Forms.Button cmdAjouter;
        public System.Windows.Forms.Button cmdMAJE;
        public System.Windows.Forms.Button CmdMenuDevis;
        public System.Windows.Forms.Button cmdSupprimer;
        public System.Windows.Forms.Button CmdHistoDevis;
        public System.Windows.Forms.Button cmdRechercheArticle;
        public System.Windows.Forms.Button cmdVisu;
        public System.Windows.Forms.Button cmdMail;
        public System.Windows.Forms.Button cmdFax;
        public System.Windows.Forms.Button cmdLettre;
        public System.Windows.Forms.Button cmdImprimer;
        public System.Windows.Forms.Button cmdOK;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button button1;
        public System.Windows.Forms.Button button2;
        public System.Windows.Forms.Button Command1;
        public System.Windows.Forms.Button CmdEditer;
        public System.Windows.Forms.Button button3;
        public System.Windows.Forms.Button button4;
        public System.Windows.Forms.Button button5;
        public System.Windows.Forms.Button button6;
        public System.Windows.Forms.Button Cmd17;
        public System.Windows.Forms.Button Cmd18;
        public System.Windows.Forms.Button button7;
        public System.Windows.Forms.Button button8;
        public System.Windows.Forms.Button button9;
        public System.Windows.Forms.Button button10;
        public System.Windows.Forms.Button button11;
        public System.Windows.Forms.Button button12;
        public System.Windows.Forms.Button button13;
        public System.Windows.Forms.Button button14;
        public System.Windows.Forms.Button button15;
        public System.Windows.Forms.Button button16;
        public System.Windows.Forms.Button button17;
        public System.Windows.Forms.Button button18;
        public iTalk.iTalk_TextBox_Small2 iTalk_TextBox_Small21;
        public Infragistics.Win.UltraWinTabControl.UltraTabControl SSTab1;
        public Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        public Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        public System.Windows.Forms.Button cmdPrecedent;
        public System.Windows.Forms.Button cmdSuivant;
        public System.Windows.Forms.Button cmdDernier;
        public System.Windows.Forms.Button cmdPremier;
        public System.Windows.Forms.Button button19;
        public System.Windows.Forms.Button button21;
        public System.Windows.Forms.Button button20;
        public System.Windows.Forms.Button button22;
        public Infragistics.Win.UltraWinGrid.UltraGrid ugResultat;
        public Infragistics.Win.UltraWinGrid.UltraCombo Combo1;
        public System.Windows.Forms.Button button23;
        public System.Windows.Forms.Button button24;
        public System.Windows.Forms.Button button25;
        public System.Windows.Forms.Button button26;
        public System.Windows.Forms.Button button27;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.GroupBox groupBox6;
        public Infragistics.Win.UltraWinGrid.UltraGrid Grids;
        public System.Windows.Forms.RadioButton radioButton1;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.Panel panel3;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Panel panel1;
        public iTalk.iTalk_RichTextBox iTalk_RichTextBox1;
        public System.Windows.Forms.CheckBox checkBox1;
        public Infragistics.Win.FormattedLinkLabel.UltraFormattedLinkLabel ultraFormattedLinkLabel1;
        private Infragistics.Win.Misc.UltraFlowLayoutManager ultraFlowLayoutManager1;
        public System.Windows.Forms.Button cmdDesAffUtil;
        public System.Windows.Forms.Button CmdAffUtil;
        private AxPLANNINGLib.AxPlanning axPlanning1;
        private AxXtremeCalendarControl.AxCalendarControl axCalendarControl1;
        private iTalk.iTalk_TextBox_Small2 iTalk_TextBox_Small22;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarCombo2;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraCalendarCombo1;
        private AxMSWinsockLib.AxWinsock axWinsock1;
    }
}
