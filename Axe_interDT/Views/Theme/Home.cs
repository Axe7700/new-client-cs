﻿using Axe_interDT.Shared;
using System;
using System.Drawing;
using System.Windows.Forms;

namespace Axe_interDT.View.Theme
{
    public partial class Home : UserControl
    {
        public Home()
        {
            InitializeComponent();
        }

        private void Home_Load(object sender, EventArgs e)
        {

        }

        private void Home_VisibleChanged(object sender, EventArgs e)
        {
            //===> Mondir le 26.05.2021, ajout du Van Froid
            if (General._Company == General.Company.DT)
                PictureBoxLogo.Image = new Bitmap(Properties.Resources.DT_LOGO);
            else if (General._Company == General.Company.LONG)
                PictureBoxLogo.Image = new Bitmap(Properties.Resources.LONG_LOGO);
            else if(General._Company == General.Company.VF)
                PictureBoxLogo.Image = new Bitmap(Properties.Resources.VF_LOGO);
        }
    }
}
