﻿namespace Axe_interDT.Views.Theme
{
    partial class Info
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.timerChangeBack = new System.Windows.Forms.Timer(this.components);
            this.SuspendLayout();
            // 
            // timerChangeBack
            // 
            this.timerChangeBack.Interval = 300;
            this.timerChangeBack.Tick += new System.EventHandler(this.timerChangeBack_Tick);
            // 
            // Info
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.BackgroundImage = global::Axe_interDT.Properties.Resources.info_1;
            this.BackgroundImageLayout = System.Windows.Forms.ImageLayout.Stretch;
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.DoubleBuffered = true;
            this.Name = "Info";
            this.Size = new System.Drawing.Size(32, 32);
            this.Load += new System.EventHandler(this.Info_Load);
            this.Click += new System.EventHandler(this.Info_Click);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Timer timerChangeBack;
    }
}
