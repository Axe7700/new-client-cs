using System.Drawing;
using System.Windows.Forms;

namespace iTalk
{
    public class iTalk_HeaderLabel : Label
    {
        public iTalk_HeaderLabel()
        {
            Font = new Font("Segoe UI", 25, FontStyle.Regular);
            ForeColor = Color.FromArgb(80, 80, 80);
            BackColor = Color.Transparent;
        }
    }
}