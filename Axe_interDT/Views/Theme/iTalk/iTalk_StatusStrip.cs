using System.Windows.Forms;

namespace iTalk
{
    public class iTalk_StatusStrip : StatusStrip
    {
        public iTalk_StatusStrip()
        {
            this.Renderer = new ControlRenderer();
            SizingGrip = false;
        }

        public new ControlRenderer Renderer
        {
            get { return (ControlRenderer)base.Renderer; }
            set { base.Renderer = value; }
        }
    }
}