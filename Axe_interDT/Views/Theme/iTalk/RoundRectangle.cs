﻿#region Imports

using System.Drawing;
using System.Drawing.Drawing2D;
using System.Reflection;
#endregion Importsft

namespace iTalk
{

    //
    #region RoundRect

    // [CREDIT][DO NOT REMOVE]
    //
    // This module was written by Aeonhack
    //
    // [CREDIT][DO NOT REMOVE]

    internal static class RoundRectangle
    {
        public static GraphicsPath RoundRect(Rectangle Rectangle, int Curve)
        {
            GraphicsPath GP = new GraphicsPath();
            int EndArcWidth = Curve * 2;
            GP.AddArc(new Rectangle(Rectangle.X, Rectangle.Y, EndArcWidth, EndArcWidth), -180, 90);
            GP.AddArc(new Rectangle(Rectangle.Width - EndArcWidth + Rectangle.X, Rectangle.Y, EndArcWidth, EndArcWidth), -90, 90);
            GP.AddArc(new Rectangle(Rectangle.Width - EndArcWidth + Rectangle.X, Rectangle.Height - EndArcWidth + Rectangle.Y, EndArcWidth, EndArcWidth), 0, 90);
            GP.AddArc(new Rectangle(Rectangle.X, Rectangle.Height - EndArcWidth + Rectangle.Y, EndArcWidth, EndArcWidth), 90, 90);
            GP.AddLine(new Point(Rectangle.X, Rectangle.Height - EndArcWidth + Rectangle.Y), new Point(Rectangle.X, Curve + Rectangle.Y));
            return GP;
        }

        public static GraphicsPath RoundRect(int X, int Y, int Width, int Height, int Curve)
        {
            Rectangle Rectangle = new Rectangle(X, Y, Width, Height);
            GraphicsPath GP = new GraphicsPath();
            int EndArcWidth = Curve * 2;
            GP.AddArc(new Rectangle(Rectangle.X, Rectangle.Y, EndArcWidth, EndArcWidth), -180, 90);
            GP.AddArc(new Rectangle(Rectangle.Width - EndArcWidth + Rectangle.X, Rectangle.Y, EndArcWidth, EndArcWidth), -90, 90);
            GP.AddArc(new Rectangle(Rectangle.Width - EndArcWidth + Rectangle.X, Rectangle.Height - EndArcWidth + Rectangle.Y, EndArcWidth, EndArcWidth), 0, 90);
            GP.AddArc(new Rectangle(Rectangle.X, Rectangle.Height - EndArcWidth + Rectangle.Y, EndArcWidth, EndArcWidth), 90, 90);
            GP.AddLine(new Point(Rectangle.X, Rectangle.Height - EndArcWidth + Rectangle.Y), new Point(Rectangle.X, Curve + Rectangle.Y));
            return GP;
        }
    }

    #endregion RoundRect

    #region Control Renderer

    #region Color Table

    #endregion Color Table

    #region Renderer

    #endregion Renderer

    #region Drawing

    #endregion Drawing

    #endregion Control Renderer

    #region ThemeContainer

    #endregion ThemeContainer

    #region ControlBox

    #endregion ControlBox

    #region Button 1

    #endregion Button 1

    #region Button 2

    #endregion Button 2

    #region Toggle Button

    #endregion Toggle Button

    #region Label

    #endregion Label

    #region Link Label

    #endregion Link Label

    #region Header Label

    #endregion Header Label

    #region Big TextBox

    #endregion Big TextBox

    #region Small TextBox

    #endregion Small TextBox

    #region Small TextBox

    #endregion Small TextBox

    #region RichTextBox

    #endregion RichTextBox

    #region NumericUpDown

    #endregion NumericUpDown

    #region Left Chat Bubble

    #endregion Left Chat Bubble

    #region Right Chat Bubble

    #endregion Right Chat Bubble

    #region Separator

    #endregion Separator

    #region Panel

    #endregion Panel

    #region GroupBox

    #endregion GroupBox

    #region CheckBox

    #endregion CheckBox

    #region RadioButton

    #endregion RadioButton

    #region Notification Number

    #endregion Notification Number

    #region ListView

    #endregion ListView

    #region ComboBox

    #endregion ComboBox

    #region Circular ProgressBar

    #endregion Circular ProgressBar

    #region Progress Indicator

    #endregion Progress Indicator

    #region TabControl

    #endregion TabControl

    #region TrackBar

    #endregion TrackBar

    #region MenuStrip

    #endregion MenuStrip

    #region ContextMenuStrip

    #endregion ContextMenuStrip

    #region StatusStrip

    #endregion StatusStrip

    #region Info Icon

    #endregion Info Icon

    #region Tick Icon

    #endregion Tick Icon
}