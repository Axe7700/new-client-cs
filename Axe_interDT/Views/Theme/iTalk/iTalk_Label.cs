using System.Drawing;
using System.Windows.Forms;

namespace iTalk
{
    public class iTalk_Label : Label
    {
        public bool IsInFormHeader { get; set; }
        public iTalk_Label()
        {
            Font = new Font("Segoe UI", 8);
            ForeColor = Color.FromArgb(142, 142, 142);
            BackColor = Color.Transparent;
        }
    }
}