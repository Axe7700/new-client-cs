using System.Windows.Forms;

namespace iTalk
{
    public class iTalk_ContextMenuStrip : ContextMenuStrip
    {
        public iTalk_ContextMenuStrip()
        {
            this.Renderer = new ControlRenderer();
        }

        public new ControlRenderer Renderer
        {
            get { return (ControlRenderer)base.Renderer; }
            set { base.Renderer = value; }
        }
    }
}