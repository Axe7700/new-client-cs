using System.Windows.Forms;

namespace iTalk
{
    public class iTalk_MenuStrip : MenuStrip
    {
        public iTalk_MenuStrip()
        {
            this.Renderer = new ControlRenderer();
        }

        public new ControlRenderer Renderer
        {
            get { return (ControlRenderer)base.Renderer; }
            set { base.Renderer = value; }
        }
    }
}