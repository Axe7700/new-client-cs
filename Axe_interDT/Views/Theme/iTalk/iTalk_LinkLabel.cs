using System.Drawing;
using System.Windows.Forms;

namespace iTalk
{
    internal class iTalk_LinkLabel : LinkLabel
    {
        public iTalk_LinkLabel()
        {
            Font = new Font("Segoe UI", 8, FontStyle.Regular);
            BackColor = Color.Transparent;
            LinkColor = Color.FromArgb(51, 153, 225);
            ActiveLinkColor = Color.FromArgb(0, 101, 202);
            VisitedLinkColor = Color.FromArgb(0, 101, 202);
            LinkBehavior = System.Windows.Forms.LinkBehavior.NeverUnderline;
        }
    }
}