using System.Drawing;
using System.Drawing.Drawing2D;
using System.Windows.Forms;

namespace iTalk
{
    public class iTalk_Panel : ContainerControl
    {
        private Color BorderColor = Color.FromArgb(180, 180, 180);

        public Color AccBorderColor
        {
            get { return BorderColor; }
            set { BorderColor = value; }
        }

        private GraphicsPath Shape;

        public iTalk_Panel()
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            SetStyle(ControlStyles.UserPaint, true);

            BackColor = Color.Transparent;
            this.Size = new Size(187, 117);
            Padding = new Padding(5, 5, 5, 5);
            DoubleBuffered = true;
        }

        protected override void OnResize(System.EventArgs e)
        {
            base.OnResize(e);
            
            Invalidate();

            Shape = new GraphicsPath();
            var _with1 = Shape;
            _with1.AddArc(0, 0, 10, 10, 180, 90);
            _with1.AddArc(Width - 11, 0, 10, 10, -90, 90);
            _with1.AddArc(Width - 11, Height - 11, 10, 10, 0, 90);
            _with1.AddArc(0, Height - 11, 10, 10, 90, 90);
            _with1.CloseAllFigures();
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {
            try
            {
                base.OnPaint(e);
                
                Bitmap B = new Bitmap(Width, Height);
                Graphics G = Graphics.FromImage(B);

                G.SmoothingMode = SmoothingMode.HighQuality;

                G.Clear(Color.Transparent);
                G.FillPath(Brushes.White, Shape); // Draw RTB background
                G.DrawPath(new Pen(BorderColor), Shape); // Draw border
                G.Dispose();
                e.Graphics.DrawImage((Image)B.Clone(), 0, 0);
                B.Dispose();
            }
            catch
            {
                OnPaint(e);
            }
        }
    }
}