using System;
using System.Runtime.InteropServices;
using System.Windows.Forms;

namespace iTalk
{
    internal class iTalk_Listview : ListView
    {
        [DllImport("uxtheme", CharSet = CharSet.Unicode)]
        public static extern int SetWindowTheme(IntPtr hWnd, string textSubAppName, string textSubIdList);

        public iTalk_Listview()
        {
            this.SetStyle(ControlStyles.OptimizedDoubleBuffer, true);
            this.DoubleBuffered = true;
            HeaderStyle = ColumnHeaderStyle.Nonclickable;
            BorderStyle = System.Windows.Forms.BorderStyle.None;
        }

        protected override void OnHandleCreated(EventArgs e)
        {
            iTalk_Listview.SetWindowTheme(this.Handle, "explorer", null);
            base.OnHandleCreated(e);
        }
    }
}