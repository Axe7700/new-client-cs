using Infragistics.Win.UltraWinEditors;
using System;
using System.ComponentModel;
using System.Drawing;
using System.Drawing.Drawing2D;
using System.Linq;
using System.Windows.Forms;

namespace iTalk
{
    [DefaultEvent("TextChanged")]
    public class iTalk_TextBox_Small2 : Control
    {
        #region Variables

        public UltraTextEditor iTalkTB = new UltraTextEditor() /*{ Height = 20 }*/;
        private GraphicsPath Shape;
        private int _maxchars = 32767;
        private bool _ReadOnly;
        private bool _Multiline;
        private Infragistics.Win.HAlign ALNType;
        private bool isPasswordMasked = false;
        private Pen P1 /*= new Pen(Color.FromArgb(32, 58, 69))*/;
        public SolidBrush B1;
        private ScrollBars scrollBar;

        private Color BackGroundColorText;
        private string HidenValue = "";
        private int[] NotAllowedChars;
        private bool AcceptNumbersOnly = false;
        private bool MyReadOnlyAllowDelete = false;
        private bool MyReadOnly = false;
        private bool Required = false;


        ///Added By Mohammed
        public override Color BackColor
        {
            get => base.BackColor;
            set
            {
                if (value == Color.Transparent)
                    value = Color.White;
                base.BackColor = value;
                iTalkTB.Appearance.BackColor = value;
                iTalkTB.Appearance.BackColor2 = value;
            }
        }

        protected override void OnSizeChanged(EventArgs e)
        {
            base.OnSizeChanged(e);
            if (iTalkTB != null)
                iTalkTB.Size = this.Size;
        }
        public Color AccBackGroundColorText
        {
            get { return BackGroundColorText; }
            set { BackGroundColorText = value; }
        }

        public Color AccBackgroundColor
        {
            get { return iTalkTB.BackColor; }
            set { iTalkTB.BackColor = value; }
            //set { iTalkTB.Appearance.BackColor = value; }
        }

        public Color CustomBackColor
        {
            get { return B1.Color; }
            set
            {
                if (value.Name == "Transparent")
                    value = Color.White;
                B1 = new SolidBrush(value);
                //iTalkTB.BackColor = value;
                //BackColor = value;
                //iTalkTB.Appearance.BackColor = value;
                Invalidate();
            }
        }
        public Color AccBorderColor
        {
            get { return P1.Color; }
            set
            {
                if (value.Name == "Transparent")
                    value = Color.White;
                P1 = new Pen(value);
                iTalkTB.Appearance.BorderColor = value;
                iTalkTB.Appearance.BorderColor2 = value;
                Invalidate();
            }
        }

        #endregion Variables
        public bool AccReadOnlyAllowDelete
        {
            get { return MyReadOnlyAllowDelete; }
            set { MyReadOnlyAllowDelete = value; }
        }
        public bool AccRequired
        {
            get { return Required; }
            set { Required = value; }
        }
        public bool AccReadOnly
        {
            get { return MyReadOnly; }
            set { MyReadOnly = value; }
        }
        public int[] AccNotAllowedChars
        {
            get { return NotAllowedChars; }
            set { NotAllowedChars = value; }
        }
        public bool AccAcceptNumbersOnly
        {
            get { return AcceptNumbersOnly; }
            set { AcceptNumbersOnly = value; }
        }



        #region Properties
        public string AccHidenValue
        {
            get { return HidenValue; }
            set { HidenValue = value; }
        }
        public Infragistics.Win.HAlign TextAlignment
        {
            get { return ALNType; }
            set
            {
                ALNType = value;
                Invalidate();
            }
        }

        public int MaxLength
        {
            get { return _maxchars; }
            set
            {
                _maxchars = value;
                iTalkTB.MaxLength = MaxLength;
                Invalidate();
            }
        }
        public ScrollBars ScrollBar
        {
            get { return scrollBar; }
            set
            {
                scrollBar = value;
                if (Multiline && iTalkTB != null)
                    iTalkTB.Scrollbars = ScrollBars.Both;
                Invalidate();
            }
        }
        public bool UseSystemPasswordChar
        {
            get { return isPasswordMasked; }
            set
            {
                //=================> Code Comment� par Mondir
                //iTalkTB.UseSystemPasswordChar = UseSystemPasswordChar;
                isPasswordMasked = value;
                Invalidate();
            }
        }

        public bool ReadOnly
        {
            get { return _ReadOnly; }
            set
            {
                _ReadOnly = value;
                if (iTalkTB != null)
                {
                    iTalkTB.ReadOnly = value;
                }
            }
        }

        public bool Multiline
        {
            get { return _Multiline; }
            set
            {
                _Multiline = value;
                if (iTalkTB != null)
                {
                    iTalkTB.Multiline = value;

                    if (value)
                    {
                        //iTalkTB.Height = Height - 10;///=====changed by mohammed
                        iTalkTB.Height = Height;
                    }
                    else
                    {
                        //Height = iTalkTB.Height + 10;///===== changed by mohammed
                        Height = iTalkTB.Height;
                    }
                }
            }
        }
        public bool AccAllowComma { get; set; } = false;
        #endregion Properties

        #region EventArgs

        protected override void OnTextChanged(System.EventArgs e)
        {
            base.OnTextChanged(e);
            iTalkTB.Text = Text;
            Invalidate();
        }

        private void OnBaseTextChanged(object s, EventArgs e)
        {
            Text = iTalkTB.Text;

            if (Text == "")
                AccHidenValue = "";
            base.OnTextChanged(e);
        }

        protected override void OnForeColorChanged(System.EventArgs e)
        {
            base.OnForeColorChanged(e);
            iTalkTB.ForeColor = ForeColor;
            Invalidate();
        }

        protected override void OnFontChanged(System.EventArgs e)
        {
            base.OnFontChanged(e);
            iTalkTB.Font = Font;
            OnResize(null);
        }

        private void _OnDoubleClick(object sender, EventArgs e)
        {
            base.OnDoubleClick(e);
        }

        protected override void OnPaintBackground(PaintEventArgs e)
        {
            base.OnPaintBackground(e);
        }

        ///=====> Added by mohammed
        protected override void OnBackColorChanged(EventArgs e)
        {
            base.OnBackColorChanged(e);
            //AccBackgroundColor = BackColor;
            Invalidate();
            Refresh();
        }
        private void _OnKeyDown(object Obj, KeyEventArgs e)
        {

            if (e.Control && e.KeyCode == Keys.A)
            {
                iTalkTB.SelectAll();
                e.SuppressKeyPress = true;
            }
            if (e.Control && e.KeyCode == Keys.C)
            {
                iTalkTB.Copy();
                e.SuppressKeyPress = true;
            }
            if (AccReadOnly)
                e.SuppressKeyPress = true;
            if (AccReadOnlyAllowDelete && e.KeyValue != 8)
                e.SuppressKeyPress = true;
            if (NotAllowedChars?.Length > 0
                && NotAllowedChars.AsEnumerable().Contains(e.KeyValue))
                e.SuppressKeyPress = true;
            if (AcceptNumbersOnly && (e.KeyValue < 96 || e.KeyValue > 105) && e.KeyValue != 8 &&
                (AccAllowComma || e.KeyValue != 110 || ((TextBox)Obj).Text == "" || ((TextBox)Obj).Text.Contains("."))
                && e.KeyCode != Keys.Left && e.KeyCode != Keys.Right)
                e.SuppressKeyPress = true;
            base.OnKeyDown(e);
        }
        public override string ToString() => iTalkTB.Text;
        protected override void OnResize(System.EventArgs e)
        {
            base.OnResize(e);
            Invalidate();
            if (_Multiline)
            {
                //iTalkTB.Height = Height - 5;///===========> commented by mohammed
                iTalkTB.Height = Height;
            }
            else
            {
                //Height = iTalkTB.Height + 5;///===========> commented by mohammed
                Height = iTalkTB.Height;
            }

            Shape = new GraphicsPath();
            var _with1 = Shape;
            //_with1.AddArc(0, 0, 8, 8, 180, 90);
            //_with1.AddArc(Width - 11, 0, 8, 8, -90, 90);
            //_with1.AddArc(Width - 11, Height - 11, 8, 8, 0, 90);
            //_with1.AddArc(0, Height - 11, 8, 8, 90, 90);
            //_with1.CloseAllFigures();
        }

        protected override void OnGotFocus(System.EventArgs e)
        {
            base.OnGotFocus(e);
            iTalkTB.Focus();
        }

        #endregion EventArgs

        public void AddTextBox()
        {
            var _TB = iTalkTB;

            ///=====SIZE OF THE CONTROL AND LOCATION
            _TB.AutoSize = false;
            //_TB.Size = new Size(Width - 10, 33);//==============> commented by mohammed
            _TB.Size = new Size(Width, 27);//===========> added by mohammed
            //_TB.Location = new Point(5, 1); //=============> commented by mohammed
            _TB.Location = new Point(0, 0);

            ///=====TEXT OF THE CONTROL
            _TB.Text = string.Empty;
            //_TB.BorderStyle = BorderStyle.None;

            ///=====DISPALY STYLE OF THE CONTROL
            //========> added by mohammed

            _TB.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Standard;
            _TB.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            _TB.TextRenderingMode = Infragistics.Win.TextRenderingMode.Default;
            _TB.Appearance.BackColor = BackColor;
            _TB.Appearance.BackColor2 = BackColor;
            iTalkTB.Appearance.BorderColor = Color.FromArgb(32, 58, 69);
            iTalkTB.Appearance.BorderColor2 = Color.FromArgb(32, 58, 69);
            ///=====COLORS OF THE CONTROL
            _TB.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
            _TB.Font = new Font("Ubuntu", 11.25F);
            _TB.UseAppStyling = true;

            //==============> Code Comment� par Mondir
            //_TB.UseSystemPasswordChar = UseSystemPasswordChar;
            //_TB.TextRenderingMode = Infragistics.Win.TextRenderingMode.GDI;
            //_TB.Multiline = false;

            ///=====EVENT OF THE CONTROL
            iTalkTB.KeyDown += _OnKeyDown;
            iTalkTB.TextChanged += OnBaseTextChanged;
            iTalkTB.KeyPress += _OnKeyPress;
            iTalkTB.KeyUp += _OnKeyUp;
            //iTalkTB.GotFocus += _GotFocus;
            //iTalkTB.LostFocus += _LostFocus;
            iTalkTB.DoubleClick += _OnDoubleClick;

            ///Added by mohammed to set custom style on the active or hover control
            iTalkTB.Enter += _OnEnterOrMouseHover;
            iTalkTB.Leave += _OnLeaveOrMouseLeave;
            iTalkTB.MouseEnter += _GotFocus;
            iTalkTB.MouseLeave += _LostFocus;
            iTalkTB.ShowOverflowIndicator = true;
            //iTalkTB.MouseHover += _OnEnterOrMouseHover;
            //iTalkTB.MouseLeave += _OnLeaveOrMouseLeave;
            //ForeColor = Color.FromArgb(55, 84, 96);
        }
        private void _OnEnterOrMouseHover(object sender, EventArgs e)
        {
            //base.OnEnter(e);
            AccBorderColor = Color.DeepSkyBlue;
            BackColor = BackColor;

        }
        private void _OnLeaveOrMouseLeave(object sender, EventArgs e)
        {
            //base.OnLeave(e);
            //if (iTalkTB.Focused)
            AccBorderColor = Color.FromArgb(32, 58, 69);
            BackColor = BackColor;

        }
        private void _GotFocus(object sender, EventArgs e)
        {
            // base.Focus();

            AccBorderColor = Color.DeepSkyBlue;
        }
        private void _LostFocus(object sender, EventArgs e)
        {
            // Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Test");
            if (!iTalkTB.Focused)//check if the editor is focused to not override the borderColor in event Enter
                AccBorderColor = Color.FromArgb(32, 58, 69);
        }

        private void _OnKeyPress(object Obj, KeyPressEventArgs e)
        {
            base.OnKeyPress(e);
        }

        private void _OnKeyUp(object Obj, KeyEventArgs e)
        {
            base.OnKeyUp(e);
        }
        public iTalk_TextBox_Small2()
        {
            SetStyle(ControlStyles.SupportsTransparentBackColor, true);
            SetStyle(ControlStyles.UserPaint, true);

            // BackColor = Color.Transparent;
            BackColor = Color.White;///=====DEFAULT COLOR
            ForeColor = Color.Black;

            AddTextBox();
            Controls.Add(iTalkTB);

            P1 = new Pen(Color.FromArgb(32, 58, 69)); // P1 = Border color
            B1 = new SolidBrush(Color.White); // B1 = Rect Background color


            Text = null;
            Font = new Font("Ubuntu", 11.25F);

            //Size = new Size(135, 33);//===> commented by mohammed
            Size = new Size(135, 27);
            Margin = new Padding(2);

            DoubleBuffered = true;
        }

        protected override void OnPaint(System.Windows.Forms.PaintEventArgs e)
        {

            base.OnPaint(e);
            //Bitmap B = new Bitmap(Width, Height);
            //Graphics G = Graphics.FromImage(B);

            //G.SmoothingMode = SmoothingMode.AntiAlias;

            var _TB = iTalkTB;

            ///=====>WIDTH OF THE CONTROL
            //_TB.Width = Width - 10;//commented by mohammed
            _TB.Width = Width;//added by mohammed


            ///=====DISPLAY STYLE  
            //=========> added by mohammed
            _TB.DisplayStyle = Infragistics.Win.EmbeddableElementDisplayStyle.Standard;
            _TB.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            ///=====CONTROL ALIGN   
            _TB.Appearance.TextHAlign = Infragistics.Win.HAlign.Left;
            _TB.TextRenderingMode = Infragistics.Win.TextRenderingMode.GDI;
            _TB.Appearance.TextHAlign = TextAlignment;
            _TB.RightToLeft = RightToLeft.No;
            _TB.Font = new Font("Ubuntu", 11.25F);

            _TB.Multiline = Multiline;
            _TB.UseAppStyling = true;
            if (Multiline)
                _TB.Scrollbars = ScrollBars.Both;
            //==============> Code Comment� par Mondir
            //_TB.UseSystemPasswordChar = UseSystemPasswordChar;


            //Invalidate();
            //Refresh();
            //=================> Code Comment� par Mondir
            //_TB.UseSystemPasswordChar = UseSystemPasswordChar;

            //_TB.UseAppStyling = false;

            //============> commented by mohammed
            //G.Clear(Color.Transparent);
            //G.FillPath(B1, Shape); // Draw background
            //G.DrawPath(P1, Shape); // Draw border 
            //e.Graphics.DrawImage((Image)B.Clone(), 0, 0);

            //G.Dispose();
            //B.Dispose();
        }
    }
}