﻿namespace Axe_interDT.Views.Theme.Menu
{
    partial class MainMenu
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.PanelItems = new System.Windows.Forms.FlowLayoutPanel();
            this.SuspendLayout();
            // 
            // PanelItems
            // 
            this.PanelItems.AutoScroll = true;
            this.PanelItems.BackColor = System.Drawing.Color.Transparent;
            this.PanelItems.Dock = System.Windows.Forms.DockStyle.Fill;
            this.PanelItems.Location = new System.Drawing.Point(0, 0);
            this.PanelItems.Name = "PanelItems";
            this.PanelItems.Size = new System.Drawing.Size(220, 220);
            this.PanelItems.TabIndex = 0;
            this.PanelItems.Paint += new System.Windows.Forms.PaintEventHandler(this.PanelItems_Paint);
            // 
            // MainMenu
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.PanelItems);
            this.Name = "MainMenu";
            this.Size = new System.Drawing.Size(220, 220);
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.FlowLayoutPanel PanelItems;
    }
}
