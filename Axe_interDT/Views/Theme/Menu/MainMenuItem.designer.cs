﻿namespace Axe_interDT.Views.Theme.Menu
{
    partial class MainMenuItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelItemName = new System.Windows.Forms.Label();
            this.PanelSubItems = new System.Windows.Forms.FlowLayoutPanel();
            this.PictureBoxItemStatus = new System.Windows.Forms.PictureBox();
            this.PictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxItemStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelItemName
            // 
            this.LabelItemName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelItemName.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LabelItemName.Font = new System.Drawing.Font("Ubuntu Light", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.LabelItemName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.LabelItemName.Location = new System.Drawing.Point(27, 14);
            this.LabelItemName.Name = "LabelItemName";
            this.LabelItemName.Size = new System.Drawing.Size(170, 23);
            this.LabelItemName.TabIndex = 2;
            this.LabelItemName.Text = "Item Name";
            this.LabelItemName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.LabelItemName.Click += new System.EventHandler(this.MainMenuItem_Click);
            this.LabelItemName.MouseLeave += new System.EventHandler(this.MainMenuItem_MouseLeave);
            this.LabelItemName.MouseHover += new System.EventHandler(this.MainMenuItem_MouseHover);
            // 
            // PanelSubItems
            // 
            this.PanelSubItems.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.PanelSubItems.BackColor = System.Drawing.Color.Transparent;
            this.PanelSubItems.Location = new System.Drawing.Point(10, 50);
            this.PanelSubItems.Name = "PanelSubItems";
            this.PanelSubItems.Size = new System.Drawing.Size(200, 0);
            this.PanelSubItems.TabIndex = 3;
            this.PanelSubItems.MouseLeave += new System.EventHandler(this.MainMenuItem_MouseLeave);
            this.PanelSubItems.MouseHover += new System.EventHandler(this.MainMenuItem_MouseHover);
            // 
            // PictureBoxItemStatus
            // 
            this.PictureBoxItemStatus.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.PictureBoxItemStatus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PictureBoxItemStatus.Image = global::Axe_interDT.Properties.Resources.arrow_right;
            this.PictureBoxItemStatus.Location = new System.Drawing.Point(200, 16);
            this.PictureBoxItemStatus.Name = "PictureBoxItemStatus";
            this.PictureBoxItemStatus.Size = new System.Drawing.Size(20, 20);
            this.PictureBoxItemStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxItemStatus.TabIndex = 4;
            this.PictureBoxItemStatus.TabStop = false;
            this.PictureBoxItemStatus.Visible = false;
            // 
            // PictureBox
            // 
            this.PictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PictureBox.Location = new System.Drawing.Point(1, 15);
            this.PictureBox.Name = "PictureBox";
            this.PictureBox.Size = new System.Drawing.Size(23, 23);
            this.PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox.TabIndex = 1;
            this.PictureBox.TabStop = false;
            this.PictureBox.Click += new System.EventHandler(this.MainMenuItem_Click);
            this.PictureBox.MouseLeave += new System.EventHandler(this.MainMenuItem_MouseLeave);
            this.PictureBox.MouseHover += new System.EventHandler(this.MainMenuItem_MouseHover);
            // 
            // MainMenuItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.PictureBoxItemStatus);
            this.Controls.Add(this.PanelSubItems);
            this.Controls.Add(this.LabelItemName);
            this.Controls.Add(this.PictureBox);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "MainMenuItem";
            this.Size = new System.Drawing.Size(220, 50);
            this.Click += new System.EventHandler(this.MainMenuItem_Click);
            this.MouseLeave += new System.EventHandler(this.MainMenuItem_MouseLeave);
            this.MouseHover += new System.EventHandler(this.MainMenuItem_MouseHover);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxItemStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox PictureBox;
        private System.Windows.Forms.FlowLayoutPanel PanelSubItems;
        public System.Windows.Forms.Label LabelItemName;
        private System.Windows.Forms.PictureBox PictureBoxItemStatus;
    }
}
