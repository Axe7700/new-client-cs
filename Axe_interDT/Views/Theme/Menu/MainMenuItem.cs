﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Drawing.Printing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Theme.Menu
{
    public partial class MainMenuItem : UserControl
    {
        public enum MainMenuItemtype
        {
            Commun,
            Normal,
            Separetor,
            CommunWebsite,
            BackButton
        }

        public enum MainMenuType
        {
            MenuCollapsed,
            MenuNotCollapsed
        }

        private Color HoverColor = Color.FromArgb(239, 239, 239);
        private Color NormalColor = Color.FromArgb(239, 239, 239);
        public ItemState State { get; set; } = ItemState.Closed;
        public MainMenuType _MainMenuType { get; set; } = MainMenuType.MenuCollapsed;
        public MainMenuSubItem[] SubItems { get; set; } = null;
        public MainMenuItemtype Type { get; set; } = MainMenuItemtype.Normal;
        public bool HiddenFromTheMenu { get; set; } = false;
        public EventHandler ClickEvent { get; set; }
        public string ItemName { get; set; }
        public enum ItemState
        {
            Closed,
            Opened
        }
        public MainMenuItem()
        {
            InitializeComponent();
        }

        public MainMenuItem(Image I, string ItemName, EventHandler ClickEvent = null, MainMenuItemtype Type = MainMenuItemtype.Normal, params MainMenuSubItem[] SubItems) : this()
        {
            if (Type == MainMenuItemtype.Separetor)
                Size = new Size(220, 25);
            PictureBox.Image = I;
            this.ItemName = ItemName;
            LabelItemName.Text = ItemName;
            this.SubItems = SubItems;
            this.Type = Type;
            if (Type == MainMenuItemtype.BackButton)
                Visible = false;
            this.ClickEvent = ClickEvent;
            if (SubItems != null)
            {
                if (SubItems.Length > 0)
                    PictureBoxItemStatus.Visible = true;

                foreach (Control C in SubItems)
                {
                    C.Anchor = AnchorStyles.Left | AnchorStyles.Top | AnchorStyles.Right;
                    PanelSubItems.Controls.Add(C);
                    C.Width = PanelSubItems.Width + 60;
                }
                PanelSubItems.Height = SubItems.Length * (SubItems.Length > 0 ? SubItems[0].Height : 0) + 50;
            }
        }

        private void MainMenuItem_MouseHover(object sender, EventArgs e)
        {
            BackColor = HoverColor;
        }

        private void MainMenuItem_MouseLeave(object sender, EventArgs e)
        {
            BackColor = NormalColor;
        }

        public void MainMenuItem_Click(object sender, EventArgs e)
        {
            if (ClickEvent != null)
                ClickEvent.Invoke(sender, e);

            if (SubItems == null || SubItems.Length == 0)
                return;

            if (State == ItemState.Closed)
            {
                PictureBoxItemStatus.Image = Properties.Resources.arrow_down;

                //Refresh();
                //Invalidate();

                //var InitSize = 50;
                //while (InitSize < 50 + SubItems.Length * (SubItems.Length > 0 ? SubItems[0].Height : 0))
                //{
                //    InitSize += 2;
                //    Height = InitSize;
                //    //Refresh();
                //}

                closeOppenedItems();

                Height = SubItems.Length * (SubItems.Length > 0 ? SubItems[0].Height : 0) + 50;

                State = ItemState.Opened;
            }
            else
            {
                PictureBoxItemStatus.Image = Properties.Resources.arrow_right;

                //Refresh();
                //Invalidate();

                //var InitSize = Height;
                //while (InitSize > 50)
                //{
                //    InitSize -= 2;
                //    Height = InitSize;
                //    //Refresh();
                //}

                Height = 50;

                State = ItemState.Closed;
            }
            //Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(Height.ToString());
        }

        private void closeOppenedItems()
        {
            var controls = Parent.Controls;
            foreach (MainMenuItem mainMenuItem in controls.Cast<Control>().Where(c => c.GetType() == typeof(MainMenuItem)))
                if (mainMenuItem != this && mainMenuItem.State == ItemState.Opened && mainMenuItem.SubItems != null && mainMenuItem.SubItems.Length > 0)
                    mainMenuItem.MainMenuItem_Click(mainMenuItem, new EventArgs());
        }
    }
}
