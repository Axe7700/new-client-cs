﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using static Axe_interDT.Views.Theme.Menu.MainMenuItem;
using Axe_interDT.Views.Theme.CustomContectMenu;

namespace Axe_interDT.Views.Theme.Menu.MenuCollapsed
{
    public partial class MainMenuItemCollapsed : UserControl
    {
        public ContextMenuSubItems ContextMenuSubItems;
        private PoperContainer PoperContainer;
        private ToolTip ToolTip = new ToolTip();
        public string ItemName { get; set; }
        public EventHandler ClickEvent { get; set; }
        public MainMenuSubItem[] SubItems { get; set; } = null;
        public MainMenuItemCollapsed()
        {
            InitializeComponent();



        }

        public MainMenuItemCollapsed(Image I, string ItemName, EventHandler ClickEvent = null, MainMenuItemtype Type = MainMenuItemtype.Normal, params MainMenuSubItem[] SubItems) : this()
        {
            if (Type == MainMenuItemtype.Separetor)
                Size = new Size(220, 25);
            PictureBox.Image = I;
            this.ItemName = ItemName;
            this.ClickEvent = ClickEvent;
            this.SubItems = SubItems;
            if (SubItems != null)
            {
                ContextMenuSubItems = new ContextMenuSubItems(SubItems);
                PoperContainer = new PoperContainer(ContextMenuSubItems);

                if (SubItems.Length > 0)
                    PictureBoxItemStatus.Visible = true;
            }
        }

        private void MainMenuItemCollapsed_MouseHover(object sender, EventArgs e)
        {
            ToolTip.SetToolTip(PictureBox, this.ItemName);
            ToolTip.SetToolTip(PictureBoxItemStatus, this.ItemName);
            ToolTip.SetToolTip(this, this.ItemName);
        }

        private void MainMenuItemCollapsed_Click(object sender, EventArgs e)
        {
            if (ClickEvent != null)
                ClickEvent.Invoke(sender, e);
            if (SubItems == null || SubItems.Length == 0)
                return;

            PoperContainer.Show(this, 70, -50);
        }
    }
}
