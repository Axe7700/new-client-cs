﻿namespace Axe_interDT.Views.Theme.Menu.MenuCollapsed
{
    partial class MainMenuItemCollapsed
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.PictureBoxItemStatus = new System.Windows.Forms.PictureBox();
            this.PictureBox = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxItemStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).BeginInit();
            this.SuspendLayout();
            // 
            // PictureBoxItemStatus
            // 
            this.PictureBoxItemStatus.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PictureBoxItemStatus.Image = global::Axe_interDT.Properties.Resources.arrow_right;
            this.PictureBoxItemStatus.Location = new System.Drawing.Point(30, 17);
            this.PictureBoxItemStatus.Name = "PictureBoxItemStatus";
            this.PictureBoxItemStatus.Size = new System.Drawing.Size(20, 20);
            this.PictureBoxItemStatus.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxItemStatus.TabIndex = 4;
            this.PictureBoxItemStatus.TabStop = false;
            this.PictureBoxItemStatus.Visible = false;
            this.PictureBoxItemStatus.Click += new System.EventHandler(this.MainMenuItemCollapsed_Click);
            this.PictureBoxItemStatus.MouseHover += new System.EventHandler(this.MainMenuItemCollapsed_MouseHover);
            // 
            // PictureBox
            // 
            this.PictureBox.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PictureBox.Location = new System.Drawing.Point(1, 15);
            this.PictureBox.Name = "PictureBox";
            this.PictureBox.Size = new System.Drawing.Size(23, 23);
            this.PictureBox.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBox.TabIndex = 1;
            this.PictureBox.TabStop = false;
            this.PictureBox.Click += new System.EventHandler(this.MainMenuItemCollapsed_Click);
            this.PictureBox.MouseHover += new System.EventHandler(this.MainMenuItemCollapsed_MouseHover);
            // 
            // MainMenuItemCollapsed
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.PictureBoxItemStatus);
            this.Controls.Add(this.PictureBox);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "MainMenuItemCollapsed";
            this.Size = new System.Drawing.Size(59, 50);
            this.Click += new System.EventHandler(this.MainMenuItemCollapsed_Click);
            this.MouseHover += new System.EventHandler(this.MainMenuItemCollapsed_MouseHover);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxItemStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBox)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox PictureBox;
        private System.Windows.Forms.PictureBox PictureBoxItemStatus;
    }
}
