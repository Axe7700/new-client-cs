﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Theme.Menu.MenuCollapsed
{
    public partial class ContextMenuSubItems : UserControl
    {
        public MainMenuSubItem[] SubItems { get; set; } = null;
        public ContextMenuSubItems()
        {
            InitializeComponent();
        }

        public ContextMenuSubItems(params MainMenuSubItem[] SubItems) :this()
        {
            this.SubItems = SubItems;

            PanelItems.Controls.AddRange(SubItems);

            this.Height = SubItems.Length * 24 + 10;
        }
    }
}
