﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Theme.Menu
{
    public partial class MainMenuSubItem : UserControl
    {
        public EventHandler EH { get; set; }
        public MainMenuSubItem()
        {
            InitializeComponent();
        }

        public MainMenuSubItem(string ItemName, EventHandler EH) : this()
        {
            LabelItemName.Text = ItemName;
            this.EH = EH;
            SetOnClickEvent(EH);
        }
        public void SetOnClickEvent(EventHandler EH)
        {
            Click += EH;
            LabelItemName.Click += EH;
            PictureBoxArrow.Click += EH;
        }
    }
}
