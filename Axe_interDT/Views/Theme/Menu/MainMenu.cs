﻿using Axe_interDT.Shared;
using Axe_interDT.Views.Analytique;
using Axe_interDT.Views.Analytique.AnalytiqueTravaux;
using Axe_interDT.Views.Analytique.ExportImmeubleAstreinte;
using Axe_interDT.Views.Appel;
using Axe_interDT.Views.BaseDeDonnees.Gerant;
using Axe_interDT.Views.BaseDeDonnees.Immeuble;
using Axe_interDT.Views.BaseDeDonnees.Immeuble.Intervenant_Par_Immeuble;
using Axe_interDT.Views.Contrat;
using Axe_interDT.Views.Contrat.FicheGMAO;
using Axe_interDT.Views.Contrat.TriContrat;
using Axe_interDT.Views.DispatchDevis;
using Axe_interDT.Views.DispatchIntervention;
using Axe_interDT.Views.Facturation;
using Axe_interDT.Views.Facturations.VisualiserUneFacture;
using Axe_interDT.Views.Facture_Mannuel;
using Axe_interDT.Views.FicheGestionAppel;
using Axe_interDT.Views.FicheVehicule;
using Axe_interDT.Views.FicheVisiteEntretient;
using Axe_interDT.Views.Fournisseur.HistoriqueDesBonsDeCommande;
using Axe_interDT.Views.Fournisseurs.Fournisseur;
using Axe_interDT.Views.Fournisseurs.HistoriqueFactureFournisseur;
using Axe_interDT.Views.Fournisseurs.ListingDesArticlesCommandes;
using Axe_interDT.Views.Fournisseurs.RetourDesBondeCommande;
using Axe_interDT.Views.Les_Articles;
using Axe_interDT.Views.Parametrages;
using Axe_interDT.Views.Planning;
using Axe_interDT.Views.PorteFeuilleContrat;
using Axe_interDT.Views.ReleveDesCompteurs;
using Axe_interDT.Views.Theme.CustomContectMenu;
using Axe_interDT.Views.Theme.Menu.MenuCollapsed;
using System;
using System.Windows.Forms;
using Outlook = Microsoft.Office.Interop.Outlook;

namespace Axe_interDT.Views.Theme.Menu
{
    public partial class MainMenu : UserControl
    {
        public MainMenuItem[] CommunMenu { get; set; }
        public MainMenuItemCollapsed[] CommunMenuCollapsed { get; set; }
        public static MainMenuItem[] HomeMenu = null;
        public static MainMenuItem[] UserDocClientMenu = null;
        public static MainMenuItem[] UserDocImmeubleMenu = null;
        public static MainMenuItem[] UserDocGroupeMenu = null;
        public static MainMenuItem[] UserDispatchDevisMenu = null;
        public static MainMenuItem[] UserDocParamtreMenu = null;
        public static MainMenuItem[] UserDispatchMenu = null;
        public static MainMenuItem[] UserDocHistoDevisMenu = null;
        public static MainMenuItem[] UserDocHistoriqueIntervMenu = null;
        public static MainMenuItem[] UserDocP2V2Menu = null;
        MainMenuItem[] UserDocTriContratMenu = null;
        MainMenuItem[] UserListeContratMenu = null;
        MainMenuItem[] UserDocHistoContratMenu = null;
        MainMenuItem[] UserReceptMarchandiseMenu = null;
        MainMenuItem[] UserListeArticleMenu = null;
        MainMenuItem[] UserDocPrintFactureMenu = null;
        MainMenuItem[] UserDocImprimeFactureMenu = null;
        MainMenuItem[] UserDocFactFournMenu = null;
        MainMenuItem[] UserDocFournisseurMenu = null;
        MainMenuItem[] UserDocFacManuelMenu = null;
        MainMenuItem[] UserHistoReadSoftMenu = null;
        MainMenuItem[] UserDocParametreV2Menu = null;
        MainMenuItem[] UserDocCtrlHeurePersMenu = null;
        MainMenuItem[] UserDocCorrespondanceMenu = null;
        MainMenuItem[] UserDocVehiculeMenu = null;
        public static MainMenuItem[] UserDocRelTechMenu = null;
        MainMenuItem[] UserDocGestionAppelMenu = null;


        public MainMenu()
        {
            InitializeComponent();
        }
        public void ShowHomeMenu(string What)
        {
            ///
            ///
            ///         Changed By Mondir at 14.06.2019 : Look at https://groupe-dt.mantishub.io/view.php?id=970 
            ///
            ///
            if (CommunMenu == null)
            {
                MainMenuItem.MainMenuType MainMenuType = MainMenuItem.MainMenuType.MenuNotCollapsed;

                //1- Set The Commun Menu

                CommunMenu = new MainMenuItem[12];
                CommunMenuCollapsed = new MainMenuItemCollapsed[12];

                //===> Mondir le 02.11.2020, demandé par Rachid, Activation du module Gestion des appel
                CommunMenu[0] = new MainMenuItem(Properties.Resources.add_folder_48, "Appel", null, MainMenuItem.MainMenuItemtype.Normal,
                     new MainMenuSubItem("Fiche Appel", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocStandard))),
                     new MainMenuSubItem("Gestion des appels", (se, ev) =>
                     {
                         if (General._ExecutionMode == General.ExecutionMode.Prod)
                         {
                             CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas le droit d'accéder à la fiche gestion des appels", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                             return;
                         }
                         View.Theme.Theme.Navigate(typeof(UserDocGestionAppel));
                     })
                    )
                //===> Fin Modif Mondir
                { _MainMenuType = MainMenuType };
                CommunMenu[1] = new MainMenuItem(Properties.Resources.group_48, "Gérants", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocClient))) { _MainMenuType = MainMenuType };
                CommunMenu[2] = new MainMenuItem(Properties.Resources.apartment_48, "Immeubles", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocImmeuble))) { _MainMenuType = MainMenuType };
                CommunMenu[3] = new MainMenuItem(Properties.Resources.today_48, "Planning", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocPlanningCodeJock))) { _MainMenuType = MainMenuType };
                CommunMenu[4] = new MainMenuItem(Properties.Resources.car_48_2, "Dispatching Intervention", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDispatch))) { _MainMenuType = MainMenuType };
                CommunMenu[5] = new MainMenuItem(Properties.Resources.edit_property_48, "Dispatching Devis", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDispatchDevis))) { _MainMenuType = MainMenuType };
                CommunMenu[6] = new MainMenuItem(Properties.Resources.tool_box_48, "GMAO", null, MainMenuItem.MainMenuItemtype.Normal,
                        new MainMenuSubItem("Fiche GMAO", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocP2V2))),
                        new MainMenuSubItem("Dispatch GMAO", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocTriContat))),
                        new MainMenuSubItem("Création des visites d'entretien", (se, ev) => View.Theme.Theme.Navigate(typeof(UserIntervP2V2))),
                        new MainMenuSubItem("Relevés des compteurs", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocSuiviRelevesV2)))
                    )
                { _MainMenuType = MainMenuType };
                CommunMenu[7] = new MainMenuItem(Properties.Resources.package_2_48, "Fournisseurs", null, MainMenuItem.MainMenuItemtype.Normal,
                        new MainMenuSubItem("Fournisseurs", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocFournisseur))),
                        new MainMenuSubItem("Retour des bons de commande", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserReceptMarchandise))),
                        new MainMenuSubItem("Historique des bons de commandes", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserBCLivraison))),
                        new MainMenuSubItem("Listing des articles commandés", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserListeArticle))),
                        new MainMenuSubItem("Historique des factures fournisseurs", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserHistoReadSoft)))
                    )
                { _MainMenuType = MainMenuType };
                CommunMenu[8] = new MainMenuItem(Properties.Resources.bill_48, "Facturation", null, MainMenuItem.MainMenuItemtype.Normal,
                        new MainMenuSubItem("Facturation contrat", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocFicheContrat))),
                        new MainMenuSubItem("Facturation manuelle", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocFacManuel))),
                        new MainMenuSubItem("Visualiser une facture", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocPrintFacture))),
                        new MainMenuSubItem("Facturation", (se, ev) =>
                        {
                            //===============> Mondir 04.06.2020 : Demande de Frédéric GERARD, donner les droits pour Sabrina Ramos
                            if (General._ExecutionMode != General.ExecutionMode.Prod || General.fncUserName().ToLower() == "sar")
                                Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocImprimeFacture));
                            else
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("En cours de dévéloppement", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                        })
                    )
                { _MainMenuType = MainMenuType };
                CommunMenu[9] = new MainMenuItem(Properties.Resources.combo_48, "Analytique", null, MainMenuItem.MainMenuItemtype.Normal,
                        new MainMenuSubItem("Portefeuille Contrat", (se, ev) =>
                        {

                            if (General._ExecutionMode == General.ExecutionMode.Prod)
                            {
                                var lDroit = ModAutorisation.fc_DroitDetail("UserPortefeuilleContrat");
                                if (lDroit == 0)
                                    return;
                            }

                            Axe_interDT.View.Theme.Theme.Navigate(typeof(UserPorteFeuilleContrat));
                        }),
                        new MainMenuSubItem("Analytique Travaux", (se, ev) =>
                        {
                            if (General._ExecutionMode == General.ExecutionMode.Prod)
                            {
                                var lDroit = ModAutorisation.fc_DroitDetail("UserDocAnalyTRavaux");
                                if (lDroit == 0)
                                    return;
                            }
                            Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocAnalyTravaux));
                        }),
                        new MainMenuSubItem("Analytique Contrat", (se, ev) =>
                        {
                            if (General._ExecutionMode == General.ExecutionMode.Prod)
                            {
                                var lDroit = ModAutorisation.fc_DroitDetail("UserDocContratAnalytique");
                                if (lDroit == 0)
                                    return;
                            }
                            Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocContratAnalytique));
                        }),
                        new MainMenuSubItem("Suivi contrat", (se, ev) =>
                        {
                            if (General._ExecutionMode == General.ExecutionMode.Prod)
                            {
                                var lDroit = ModAutorisation.fc_DroitDetail("UserAnalyseContrat");
                                if (lDroit == 0)
                                    return;
                            }
                            Axe_interDT.View.Theme.Theme.Navigate(typeof(UserAnalyseContrat));
                        }),
                        new MainMenuSubItem("Statistique", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocAnalyse))),
                        new MainMenuSubItem("Export immeuble astreinte", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocExportImmAstreinte))),
                        new MainMenuSubItem("Immeuble Par Intervenant", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocIntervenantParImmeuble)))


                   )
                { _MainMenuType = MainMenuType };
                CommunMenu[10] = new MainMenuItem(Properties.Resources.gear_48, "Administration", null, MainMenuItem.MainMenuItemtype.Normal,
                        new MainMenuSubItem("Paramétrages", (se, ev) =>
                        {
                            if (General._ExecutionMode == General.ExecutionMode.Prod)
                            {
                                if (General.fncUserName().ToUpper() != "rachid abbouchi".ToUpper())
                                {
                                    int lDroit;
                                    lDroit = ModAutorisation.fc_DroitDetail("UserDocParamtre");
                                    if (lDroit == 0)
                                    {
                                        return;
                                    }
                                }
                            }
                            Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocParamtre));
                        }),
                        new MainMenuSubItem("Les articles", (se, ev) =>
                        {
                            bool bok = true;
                            string sMdp;
                            if (General.sMDParticle == "1")
                            {
                                bok = false;
                                sMdp = Microsoft.VisualBasic.Interaction.InputBox("Veuillez saisir un mot de passe.", "Mot de passe");
                                if (sMdp != "DT2025")
                                {
                                    sMdp = Microsoft.VisualBasic.Interaction.InputBox("Mot de passe incorrect. Veuillez saisir un mot de passe.", "Mot de passe");
                                    if (sMdp != "DT2025")
                                    {
                                        sMdp = Microsoft.VisualBasic.Interaction.InputBox("Mot de passe incorrect. Veuillez saisir un mot de passe.", "Mot de passe");
                                        if (sMdp != "DT2025")
                                        {
                                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Navigation annulée, vous n'avez pas les autorisations nécessaires pour accéder à cette fiche.", "Navigation annulée", MessageBoxButtons.OK);
                                        }
                                        else
                                        {
                                            bok = true;
                                        }
                                    }
                                    else
                                    {
                                        bok = true;
                                    }
                                }
                                else
                                {
                                    bok = true;
                                }
                            }
                            if (bok == true)
                            {
                                Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocArticle));
                            }

                        }),
                        new MainMenuSubItem("Listing des intervenants", (se, ev) => Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("En cours de dévéloppement", "", MessageBoxButtons.OK, MessageBoxIcon.Warning)),
                        new MainMenuSubItem("Fiche véhicule", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocVehicule))),
                        new MainMenuSubItem("Choix Société", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(ContextMenuChoixDeSociete)))
                   )
                { _MainMenuType = MainMenuType };
                CommunMenu[11] = new MainMenuItem(Properties.Resources.bug_2_128, "Signaler un bug", (se, ev) => SendMail()) { _MainMenuType = MainMenuType };


                //CommunMenu[11] = new MainMenuItem(Properties.Resources.gear_48, "Choix Société", (se, ev) =>
                //{
                //    Axe_interDT.View.Theme.Theme.Navigate(typeof(ContextMenuChoixDeSociete));
                //}
                //)
                //{ _MainMenuType = MainMenuType };

                CommunMenuCollapsed[0] = new MainMenuItemCollapsed(Properties.Resources.add_folder_48, "Fiche Appel", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocStandard)), MainMenuItem.MainMenuItemtype.Normal, null
                    )
                { Visible = false };
                CommunMenuCollapsed[1] = new MainMenuItemCollapsed(Properties.Resources.group_48, "Gérants", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocClient))) { Visible = false };
                CommunMenuCollapsed[2] = new MainMenuItemCollapsed(Properties.Resources.apartment_48, "Immeubles", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocImmeuble))) { Visible = false };
                CommunMenuCollapsed[3] = new MainMenuItemCollapsed(Properties.Resources.today_48, "Planning", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocPlanningCodeJock))) { Visible = false };
                CommunMenuCollapsed[4] = new MainMenuItemCollapsed(Properties.Resources.car_48_2, "Dispatching Intervention", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDispatch))) { Visible = false };
                CommunMenuCollapsed[5] = new MainMenuItemCollapsed(Properties.Resources.edit_property_48, "Dispatching Devis", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDispatchDevis))) { Visible = false };
                CommunMenuCollapsed[6] = new MainMenuItemCollapsed(Properties.Resources.tool_box_48, "GMAO", null, MainMenuItem.MainMenuItemtype.Normal,
                        new MainMenuSubItem("Fiche GMAO", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocP2V2))),
                        new MainMenuSubItem("Dispatch GMAO", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocTriContat))),
                        new MainMenuSubItem("Création des visites d'entretien", (se, ev) => View.Theme.Theme.Navigate(typeof(UserIntervP2V2))),
                        new MainMenuSubItem("Relevés des compteurs", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocSuiviRelevesV2)))
                    )
                { Visible = false };
                CommunMenuCollapsed[7] = new MainMenuItemCollapsed(Properties.Resources.package_2_48, "Fournisseurs", null, MainMenuItem.MainMenuItemtype.Normal,
                       new MainMenuSubItem("Fournisseurs", (se, ev) => View.Theme.Theme.Navigate(typeof(UserDocFournisseur))),
                       new MainMenuSubItem("Retour des bons de commande", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserReceptMarchandise))),
                       new MainMenuSubItem("Historique des bons de commandes", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserBCLivraison))),
                       new MainMenuSubItem("Listing des articles commandés", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserListeArticle))),
                       new MainMenuSubItem("Historique des factures fournisseurs", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserHistoReadSoft)))
                   )
                { Visible = false };
                CommunMenuCollapsed[8] = new MainMenuItemCollapsed(Properties.Resources.bill_48, "Facturation", null, MainMenuItem.MainMenuItemtype.Normal,
                        new MainMenuSubItem("Facturation contrat", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocFicheContrat))),
                        new MainMenuSubItem("Facturation manuelle", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocFacManuel))),
                        new MainMenuSubItem("Visualiser une facture", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocPrintFacture))),
                        new MainMenuSubItem("Facturation", (se, ev) => Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("En cours de dévéloppement", "", MessageBoxButtons.OK, MessageBoxIcon.Warning))
                    )
                { Visible = false };
                CommunMenuCollapsed[9] = new MainMenuItemCollapsed(Properties.Resources.combo_48, "Analytique", null, MainMenuItem.MainMenuItemtype.Normal,
                        new MainMenuSubItem("Portefeuille Contrat", (se, ev) =>
                        {

                            if (General._ExecutionMode == General.ExecutionMode.Prod)
                            {
                                var lDroit = ModAutorisation.fc_DroitDetail("UserPortefeuilleContrat");
                                if (lDroit == 0)
                                    return;
                            }

                            Axe_interDT.View.Theme.Theme.Navigate(typeof(UserPorteFeuilleContrat));
                        }),
                        new MainMenuSubItem("Analytique Travaux", (se, ev) =>
                        {
                            if (General._ExecutionMode == General.ExecutionMode.Prod)
                            {
                                var lDroit = ModAutorisation.fc_DroitDetail("UserDocAnalyTRavaux");
                                if (lDroit == 0)
                                    return;
                            }
                            Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocAnalyTravaux));
                        }),
                        new MainMenuSubItem("Analytique Contrat", (se, ev) =>
                        {
                            if (General._ExecutionMode == General.ExecutionMode.Prod)
                            {
                                var lDroit = ModAutorisation.fc_DroitDetail("UserDocContratAnalytique");
                                if (lDroit == 0)
                                    return;
                            }
                            Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocContratAnalytique));
                        }),
                        new MainMenuSubItem("Suivi contrat", (se, ev) =>
                        {
                            if (General._ExecutionMode == General.ExecutionMode.Prod)
                            {
                                var lDroit = ModAutorisation.fc_DroitDetail("UserAnalyseContrat");
                                if (lDroit == 0)
                                    return;
                            }
                            Axe_interDT.View.Theme.Theme.Navigate(typeof(UserAnalyseContrat));
                        }),
                        new MainMenuSubItem("Statistique", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocAnalyse))),
                        new MainMenuSubItem("Export immeuble astreinte", (se, ev) =>
                        {
                            //if (General._ExecutionMode == General.ExecutionMode.Prod)
                            //{
                            //    var lDroit = ModAutorisation.fc_DroitDetail("UserAnalyseContrat");
                            //    if (lDroit == 0)
                            //        return;
                            //}
                            Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocExportImmAstreinte));
                        }),
                        new MainMenuSubItem("Immeuble Par Intervenant", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocIntervenantParImmeuble)))
                   )
                { Visible = false };
                CommunMenuCollapsed[10] = new MainMenuItemCollapsed(Properties.Resources.gear_48, "Administration", null, MainMenuItem.MainMenuItemtype.Normal,
                        new MainMenuSubItem("Paramétrages", (se, ev) =>
                        {
                            if (General._ExecutionMode == General.ExecutionMode.Prod)
                            {
                                if (General.fncUserName().ToUpper() != "rachid abbouchi".ToUpper())
                                {
                                    int lDroit;
                                    lDroit = ModAutorisation.fc_DroitDetail("UserDocParamtre");
                                    if (lDroit == 0)
                                    {
                                        return;
                                    }
                                }
                            }
                            Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocParamtre));
                        }),
                        new MainMenuSubItem("Les articles", (se, ev) =>
                        {
                            //===> Mondir le 12.03.2021, i saw this code in norma menu above, but the collapse menu havent this, so i added it
                            bool bok = true;
                            string sMdp;
                            if (General.sMDParticle == "1")
                            {
                                bok = false;
                                sMdp = Microsoft.VisualBasic.Interaction.InputBox("Veuillez saisir un mot de passe.", "Mot de passe");
                                if (sMdp != "DT2025")
                                {
                                    sMdp = Microsoft.VisualBasic.Interaction.InputBox("Mot de passe incorrect. Veuillez saisir un mot de passe.", "Mot de passe");
                                    if (sMdp != "DT2025")
                                    {
                                        sMdp = Microsoft.VisualBasic.Interaction.InputBox("Mot de passe incorrect. Veuillez saisir un mot de passe.", "Mot de passe");
                                        if (sMdp != "DT2025")
                                        {
                                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Navigation annulée, vous n'avez pas les autorisations nécessaires pour accéder à cette fiche.", "Navigation annulée", MessageBoxButtons.OK);
                                        }
                                        else
                                        {
                                            bok = true;
                                        }
                                    }
                                    else
                                    {
                                        bok = true;
                                    }
                                }
                                else
                                {
                                    bok = true;
                                }
                            }
                            if (bok == true)
                            {
                                Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocArticle));
                            }
                        }),
                        new MainMenuSubItem("Listing des intervenants", (se, ev) => Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("En cours de dévéloppement", "", MessageBoxButtons.OK, MessageBoxIcon.Warning)),
                        new MainMenuSubItem("Fiche véhicule", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocVehicule))),
                         new MainMenuSubItem("Choix Société", (se, ev) => Axe_interDT.View.Theme.Theme.Navigate(typeof(ContextMenuChoixDeSociete)))
                   )
                { Visible = false };
                CommunMenuCollapsed[11] = new MainMenuItemCollapsed(Properties.Resources.bug_2_128, "Signaler un bug", (se, ev) => SendMail()) { Visible = false };

                //CommunMenuCollapsed[11] = new MainMenuItemCollapsed(Properties.Resources.gear_48, "Choix Société", (se, ev) =>
                //{
                //    Axe_interDT.View.Theme.Theme.Navigate(typeof(ContextMenuChoixDeSociete));
                //})
                //{ Visible = false };

                //2- Show The Menu
                PanelItems.Visible = false;
                foreach (Control C in CommunMenu)
                {
                    C.Width = PanelItems.Width - 20;
                    PanelItems.Controls.Add(C);
                }
                foreach (Control C in CommunMenuCollapsed)
                {
                    C.Width = PanelItems.Width - 20;
                    PanelItems.Controls.Add(C);
                }
                PanelItems.Visible = true;
            }
        }

        private static void SendMail()
        {
            try
            {
                //===> Mondir le 21.05.2021, Commented For the moment
                //CustomMessageBox.CustomMessageBox.Show("La fenêtre Outlook s'ouvrira, veuillez répondre aux questions dans le corps de l'e-mail.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                //===> Fin Modif Mondir

                View.Theme.Theme.MainForm.Cursor = Cursors.WaitCursor;

                Outlook.Application outlookApp = new Outlook.Application();
                Outlook._MailItem oMailItem = (Outlook._MailItem)outlookApp.CreateItem(Outlook.OlItemType.olMailItem);
                Outlook.Inspector oInspector = oMailItem.GetInspector;

                // Recipient
                Outlook.Recipients oRecips = (Outlook.Recipients)oMailItem.Recipients;
                Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add("intranet2019@groupe-dt.fr");
                oRecip.Resolve();
                //foreach (String recipient in lstAllRecipients)
                //{
                //    Outlook.Recipient oRecip = (Outlook.Recipient)oRecips.Add(recipient);
                //    oRecip.Resolve();
                //}

                //Add CC
                //Outlook.Recipient oCCRecip = oRecips.Add("THIYAGARAJAN.DURAIRAJAN@testmail.com");
                //oCCRecip.Type = (int)Outlook.OlMailRecipientType.olCC;
                //oCCRecip.Resolve();

                //Add Subject
                //===> Mondir le 21.05.2021, Commented For the moment
                //var html = "<html><body style=\"font-size: 1.2em;\">";
                //html += "Bonjour, <br><br>";
                //html += "Veuillez utiliser les flèches pour indiquer les boutons / champs sur votre capture d'écran <br><br>";
                //html += $"<b>Utilisateur :</b> {General.fncUserName()}<br>";
                //html += $"<b>Base de données :</b> {(General._Company == General.Company.DT ? "DELOSTAL" : "LONG")}<br>";
                //html += "<b>Êtes-vous la seule personne à avoir ce bug ? :</b> <br>";
                //html += "<b>Numéro de téléphone :</b> <br>";
                //html += "<b>Module concerné (Devis, Facture Manuelle ...) ** :</b> <br>";
                //html += "<b>Information Clé (Code Immeuble, Numéro d'intervention ...) ** :</b> <br>";
                //html += "<b>Description ** :</b> <br><br>";
                //html += "Cordialement, <br><br>";
                //html += "<b style=\"color: red;\">** = Obligatoire</b>";
                //html += "</body></html>";
                //oMailItem.HTMLBody = html;
                //===> Fin Modif Mondir


                // body, bcc etc...

                //Display the mailbox
                oMailItem.Display();

                View.Theme.Theme.MainForm.Cursor = Cursors.Default;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        private void PanelItems_Paint(object sender, PaintEventArgs e)
        {

        }
    }
}
