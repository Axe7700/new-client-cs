﻿namespace Axe_interDT.Views.Theme.Menu
{
    partial class MainMenuSubItem
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.LabelItemName = new System.Windows.Forms.Label();
            this.PictureBoxArrow = new System.Windows.Forms.PictureBox();
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxArrow)).BeginInit();
            this.SuspendLayout();
            // 
            // LabelItemName
            // 
            this.LabelItemName.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.LabelItemName.Cursor = System.Windows.Forms.Cursors.Hand;
            this.LabelItemName.Font = new System.Drawing.Font("Ubuntu Light", 10F);
            this.LabelItemName.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.LabelItemName.Location = new System.Drawing.Point(20, 2);
            this.LabelItemName.Name = "LabelItemName";
            this.LabelItemName.Size = new System.Drawing.Size(200, 20);
            this.LabelItemName.TabIndex = 0;
            this.LabelItemName.Text = "label1";
            this.LabelItemName.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // PictureBoxArrow
            // 
            this.PictureBoxArrow.Cursor = System.Windows.Forms.Cursors.Hand;
            this.PictureBoxArrow.Image = global::Axe_interDT.Properties.Resources.arrow_36_16;
            this.PictureBoxArrow.Location = new System.Drawing.Point(-3, 4);
            this.PictureBoxArrow.Name = "PictureBoxArrow";
            this.PictureBoxArrow.Size = new System.Drawing.Size(14, 14);
            this.PictureBoxArrow.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.PictureBoxArrow.TabIndex = 1;
            this.PictureBoxArrow.TabStop = false;
            // 
            // MainMenuSubItem
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.Transparent;
            this.Controls.Add(this.PictureBoxArrow);
            this.Controls.Add(this.LabelItemName);
            this.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Margin = new System.Windows.Forms.Padding(0);
            this.Name = "MainMenuSubItem";
            this.Size = new System.Drawing.Size(215, 24);
            ((System.ComponentModel.ISupportInitialize)(this.PictureBoxArrow)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        private System.Windows.Forms.PictureBox PictureBoxArrow;
        public System.Windows.Forms.Label LabelItemName;
    }
}
