﻿using Axe_interDT.Shared;
using Axe_interDT.Shared.FilterOfGrids;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Analytique;
using Axe_interDT.Views.Appel;
using Axe_interDT.Views.BaseDeDonnees.Gerant;
using Axe_interDT.Views.BaseDeDonnees.Immeuble;
using Axe_interDT.Views.Contrat;
using Axe_interDT.Views.Devis.Forms;
using Axe_interDT.Views.Devis.Historique_Devis;
using Axe_interDT.Views.DispatchDevis;
using Axe_interDT.Views.Intervention;
using Axe_interDT.Views.SharedViews;
using Axe_interDT.Views.Theme.CustomMessageBox;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using iTalk;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Windows.Forms;
using BeforeExitEditModeEventArgs = Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs;
using Excel = Microsoft.Office.Interop.Excel;
using Word = Microsoft.Office.Interop.Word;

namespace Axe_interDT.Views.Devis
{
    public partial class UserDocDevis : UserControl
    {
        ////TODO : Mondir - Chnage The Icon Of The Controle : cmdDossierDevis
        ////TODO : Mondir - Chnage The Icon Of The Controle : cmdAction4
        ////TODO : Mondir - Chnage The Icon Of The Controle : cmdExporter - A Smalled Pic

        bool gbAjout;

        string gsCodeTVA;

        //Dim giMsg                As Long
        ///''''''''
        DataTable adotemp;

        private ModAdo modAdoadotemp;
        DataTable ado2Temp;
        private ModAdo modAdoado2Temp;
        DataTable ado3temp;
        private ModAdo modAdoado3temp;
        DataTable rsSousFamilleArticle;
        private ModAdo modAdorsSousFamilleArticle;
        DataTable rsSousFamilleArticleDetail;
        private ModAdo modAdorsSousFamilleArticleDetail;
        DataTable rsFacArticle;
        private ModAdo modAdorsFacArticle;
        DataTable rsOuvrageEntete;
        private ModAdo modAdorsOuvrageEntete;
        DataTable rsOuvrageCorps;
        private ModAdo modAdorsOuvrageCorps;
        DataTable rsFournitArticleDetail;
        private ModAdo modAdorsFournitArticleDetail;
        string[] tSqlCrystal;
        bool blnFournisGrille;
        object[] dblQte = new object[2];
        bool blnColQteModif;

        short iNumLigne;
        DialogResult iMsgb;

        int i;
        int longBonCom;

        string cMdpCeof = "AM2014";

        bool bInsertion;
        bool bSuppression;
        bool boolEntree;
        bool BOOLErreurCorps;
        bool BoolAffichemessageErrur;
        bool boolRecherche;
        bool blnModele;
        bool blnErrGridUpdate;

        string strOldValue;
        string[] strTABSage;
        string TauxTVADefaut;

        object vartemp;

        bool boolModif;
        bool boolDoublon;
        bool boolRenumLigne;

        string[] tabNoFacture;
        string[] tabMontantFact;

        const short cOngletPoste = 4;
        const short cOngletDivers = 6;
        const short cOngletArticle = 5;
        const short cOngletPlan = 7;

        const string cAgrandir = "Agrandir";
        const string cReduire = "Reduire";

        const string cStatutTermine = "T";
        const string sStatutP3 = "P3";

        //DataTable SSDBGridCorpsSource = null;

        DataTable Adodc5 = null;
        ModAdo modAdoAdodc5 = null;

        DataTable Adodc6 = null;
        ModAdo modAdoAdodc6 = null;

        DataTable Adodc7 = null;
        ModAdo modAdoAdodc7 = null;

        string FileImmeublePattern = "";

        public bool bAppliqueVersion;

        private ModDevis ModDevis = new ModDevis();
        public UserDocDevis()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool initialiseComboMail()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"initialiseComboMail() - Enter In The Function");
            //===> Fin Modif Mondir

            bool functionReturnValue = false;
            DataTable rsTmp1 = default(DataTable);
            ModAdo modAdorsTmp1 = new ModAdo();
            Redemption.AddressList ral = null;
            Redemption.AddressLists rals = null;
            string sDossier = null;
            try
            {
                Microsoft.Office.Interop.Outlook.Recipient myRecipient =
                default(Microsoft.Office.Interop.Outlook.Recipient);
                Microsoft.Office.Interop.Outlook.Items myItem = default(Microsoft.Office.Interop.Outlook.Items);
                Microsoft.Office.Interop.Outlook.AddressList myAddressList =
                    default(Microsoft.Office.Interop.Outlook.AddressList);
                Microsoft.Office.Interop.Outlook.AddressEntries myAddressEntries =
                    default(Microsoft.Office.Interop.Outlook.AddressEntries);
                Microsoft.Office.Interop.Outlook.AddressEntry myAddressEntry =
                    default(Microsoft.Office.Interop.Outlook.AddressEntry);
                int Index = 0;
                rals = new Redemption.AddressLists();

                string essai = null;
                short i = 0;
                int x = 0;


                functionReturnValue = false;
                cmdMail.Enabled = true;
                SSOleDBCmbMail.Enabled = true;

                var SSOleDBCmbMailSource = new DataTable();
                SSOleDBCmbMailSource.Columns.Add("Email");

                SSOleDBCmbMail.DataSource = null;


                rsTmp1 = modAdorsTmp1.fc_OpenRecordSet("SELECT Table1.eMail " + "FROM Table1 WHERE Table1.Code1 ='" +
                                                       StdSQLchaine.gFr_DoublerQuote(Text2.Text) + "'");
                if (rsTmp1.Rows.Count > 0)
                {
                    if (cmdMail.Enabled == true)
                    {
                        //'   initialiseComboMail
                    }
                    //SSOleDBCmbMail.DataSource = rsTmp1;
                    //SSOleDBCmbMail.Text = rsTmp1.Rows[0][0].ToString();
                    foreach (DataRow Row in rsTmp1.Rows)
                        SSOleDBCmbMailSource.Rows.Add(Row[0].ToString());
                }

                rsTmp1 = modAdorsTmp1.fc_OpenRecordSet("SELECT Gestionnaire.Email_GES " +
                                                       "FROM Gestionnaire WHERE Gestionnaire.Code1 ='" +
                                                       StdSQLchaine.gFr_DoublerQuote(Text2.Text) + "'");
                if (rsTmp1.Rows.Count > 0)
                {
                    if (cmdMail.Enabled == true)
                    {
                        //'   initialiseComboMail
                    }
                    //SSOleDBCmbMail.DataSource = rsTmp1;
                    foreach (DataRow Row in rsTmp1.Rows)
                        SSOleDBCmbMailSource.Rows.Add(Row[0].ToString());
                }

                if (General.sInitAdressListOutlook345_V2 == "1")
                {
                    ModCourrier.InitAdressListOutlook345_V2();
                    for (x = 0; x <= ModCourrier.sTabAdresseMail.Length - 1; x++)
                    {
                        SSOleDBCmbMailSource.Rows.Add(ModCourrier.sTabAdresseMail[x].sNom);
                    }
                    //SSOleDBCmbMail.DataSource = tmpDataTable;

                }
                else if (General.adocnn.Database.ToUpper() == General.cNameLong.ToUpper())
                {
                    ModCourrier.InitAdressListOutlook345();
                    for (x = 0; x <= ModCourrier.sTabAdresseMail.Length - 1; x++)
                    {
                        SSOleDBCmbMailSource.Rows.Add(ModCourrier.sTabAdresseMail[x].sNom);

                    }
                    //SSOleDBCmbMail.DataSource = tmpDataTable;
                }
                else
                {
                    ModCourrier.InitAdressListOutlook345();
                    for (x = 0; x <= ModCourrier.sTabAdresseMail.Length - 1; x++)
                    {
                        SSOleDBCmbMailSource.Rows.Add(ModCourrier.sTabAdresseMail[x].sNom);

                    }
                    //SSOleDBCmbMail.DataSource = tmpDataTable;
                    //                sDossier = gfr_liaison("Dossier1")
                    //                Set ral = rals(sDossier)
                    //                For Index = 1 To ral.AddressEntries.Count
                    //                    SSOleDBCmbMail.AddItem ral.AddressEntries(Index).Name
                    //                Next Index

                }
                rals = null;
                ral = null;

                SSOleDBCmbMail.DataSource = SSOleDBCmbMailSource;

                functionReturnValue = true;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                if (ex.GetType().FullName == "System.Runtime.InteropServices.COMException")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Version Outlook du système est incompatible avec l’application",
                                       "Initialisation d'Outlook",
                                       MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {


                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Impossible d'initialiser Outlook, l'envoie par mail sera impossible",
                        "Initialisation d'Outlook",
                        MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                cmdMail.Enabled = false;
                SSOleDBCmbMail.Enabled = false;
                functionReturnValue = false;
                rals = null;
                ral = null;
                //TODO: Check for tha appropriate Exception type
                if (ex.GetType().FullName == "TODO")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Impossible d'accéder au dossier " + sDossier, "Initialisation d'Outlook",
                    MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                return functionReturnValue;
            }
        }

        /// <summary>
        /// Tested : This Controle Is Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Affichage1_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Affichage1_CheckedChanged() - Enter In The Function - Affichage1.Checked = {Affichage1.Checked}");
            //===> Fin Modif Mondir

            try
            {
                switch ((int)Affichage1.CheckState)
                {
                    case 0:
                        //effacement des infos debourse
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["mohud"].Hidden = true;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["Foud"].Hidden = true;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["Stud"].Hidden = true;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["Mohd"].Hidden = true;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["Mod"].Hidden = true;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["Fod"].Hidden = true;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["Std"].Hidden = true;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["TotalDebours"].Hidden = true;
                        break;
                    case 1:
                        //affichage des infos debourse
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["mohud"].Hidden = false;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["Foud"].Hidden = false;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["Stud"].Hidden = false;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["Mohd"].Hidden = false;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["Mod"].Hidden = false;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["Fod"].Hidden = false;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["Std"].Hidden = false;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["TotalDebours"].Hidden = false;
                        break;
                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";Affichage1_Click");
            }
        }

        /// <summary>
        /// Tested : This Controle Is Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Affichage2_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Affichage2_CheckedChanged() - Enter In The Function - Affichage2.Checked = {Affichage2.Checked}");
            //===> Fin Modif Mondir

            try
            {
                switch ((int)Affichage2.CheckState)
                {
                    case 0:
                        //effacement des infos debourse
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["MO"].Hidden = true;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["FO"].Hidden = true;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["ST"].Hidden = true;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["TotalVente"].Hidden = true;
                        break;
                    case 1:
                        //affichage des infos debourse
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["MO"].Hidden = false;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["FO"].Hidden = false;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["ST"].Hidden = false;
                        SSDBGridCorps.DisplayLayout.Bands[0].Columns["TotalVente"].Hidden = false;
                        break;
                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";Affichage2_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCpteRendu_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"btnCpteRendu_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            if (valider() == true)
            {

                if (!string.IsNullOrEmpty(Text14.Text))
                {
                    // stocke la position de la fiche devis.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, Text11.Text);
                    General.saveInReg(Variable.cUserDocRelTech, "NewVar", Text14.Text);
                    General.saveInReg(Variable.cUserDocRelTech, "Origine", this.Name);
                }

                View.Theme.Theme.Navigate(typeof(UserDocRelTech));
            }
        }


        private bool checkIfDevisModified(DataRow Devis, string numeroDevis)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"checkIfDevisModified() - Enter In The Function - numeroDevis = {numeroDevis}");
            //===> Fin Modif Mondir

            //string str = "";

            using (var tmpModAdo = new ModAdo())
            {

                //=============> Fiixing https://groupe-dt.mantishub.io/view.php?id=1620
                var dt = tmpModAdo.fc_OpenRecordSet($"select * from DevisEnTete where NumeroDevis='{numeroDevis}'");

                if (dt.Rows.Count == 0)
                    return false;

                var DataRow = tmpModAdo.fc_OpenRecordSet($"select * from DevisEnTete where NumeroDevis='{numeroDevis}'").Rows[0];
                foreach (DataColumn col in DataRow.Table.Columns)
                {
                    //Pour certain nouveau devis la valeur de la colonne 'CoefMontantSouhaite' dans la base est  NULL alors quand on ouvre la fiche devis
                    //sur Intranet on aura la 1 comme valeur, donc la fonction va renvoyer true, donc la fiche Modifié
                    if (Devis[col.ColumnName] != DBNull.Value && DataRow[col.ColumnName] == DBNull.Value)
                        return true;

                    if (Devis[col.ColumnName] == DBNull.Value && DataRow[col.ColumnName] != DBNull.Value)
                        return true;

                    if (Devis[col.ColumnName] == DBNull.Value && DataRow[col.ColumnName] == DBNull.Value)
                        continue;

                    if (col.DataType == typeof(String) && Devis[col.ColumnName]?.ToString() != DataRow[col.ColumnName]?.ToString())
                        return true;
                    else if (col.DataType == typeof(DateTime) && Convert.ToDateTime(Devis[col.ColumnName]).ToString("yyyy/MM/dd HH:mm:ss") != Convert.ToDateTime(DataRow[col.ColumnName]).ToString("yyyy/MM/dd HH:mm:ss"))
                        return true;
                    else if (col.DataType == typeof(Int32) && Convert.ToInt32(Devis[col.ColumnName]) != Convert.ToInt32(DataRow[col.ColumnName]))
                        return true;
                    else if ((col.DataType == typeof(Decimal) || col.DataType == typeof(Double)) &&
                        (Math.Abs(Convert.ToDouble(Devis[col.ColumnName]) - Convert.ToDouble(DataRow[col.ColumnName])) != 0 && Math.Abs(Convert.ToDouble(Devis[col.ColumnName]) - Convert.ToDouble(DataRow[col.ColumnName])) < 0.0000001))
                        return true;
                    else if (col.DataType == typeof(Boolean) && Convert.ToBoolean(Devis[col.ColumnName]) != Convert.ToBoolean(DataRow[col.ColumnName]))
                        return true;

                    //if (Devis[col.ColumnName].ToString() != DataRow[col.ColumnName].ToString())
                    //    return true;
                }
            }
            return false;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool valider()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"valider() - Enter In The Function");
            //===> Fin Modif Mondir

            bool functionReturnValue = false;
            UltraGridRow book = null;
            int lRowNumber = -1;
            string sTexte = null;
            int lLen = 0;

            try
            {
                try
                {
                    functionReturnValue = false;

                    GridSousFamilleArticle.UpdateData();
                    gridSousFamilleArticleDetail.UpdateData();
                    ssEnTete.UpdateData();
                    SStypeReglement.UpdateData();
                    ssGridDefaut.UpdateData();


                    functionReturnValue = true;
                    System.DateTime dDate = default(System.DateTime);
                    //If Not IsDate(txtDateImpression) Then
                    //    MsgBox "Validation annulée " & vbCrLf & "Date Invalide", vbCritical, "Données eronnées"
                    //    valider = False
                    //    SSTab1.Tab = 3
                    //    txtDateImpression.SetFocus
                    //    Exit Function
                    //End If
                    functionReturnValue = true;
                    dDate = DateTime.Now;
                    cmdSupprimer.Enabled = true;
                    cmdAjouter.Enabled = true;
                    cmdRecherche.Enabled = true;
                    cmdVersion.Enabled = true;
                    cmdRechercherDtandard.Enabled = false;
                    //Text1(0).Enabled = False
                    Text11.Enabled = true;
                    Text322.Text = txtTitreDevis.Text;

                    //Modifié Par Mondir =================> https://groupe-dt.mantishub.io/view.php?id=1465
                    //Text124.Text = General.gsUtilisateur;
                    SSDBGridCorps.UpdateData();

                    ///================> Tested
                    if (SSDBGridCorps.DisplayLayout.RowScrollRegions[0].VisibleRows.Count > 0)
                        book = SSDBGridCorps.DisplayLayout.RowScrollRegions[0].VisibleRows[0].Row;
                    ///===================> Tested
                    if (SSDBGridCorps.ActiveRow != null)
                        lRowNumber = SSDBGridCorps.ActiveRow.Index;
                    prcCalculTotalAvantCoef(Text11.Text);

                    ///===================> Tested
                    if (modAdoadotemp == null)
                    {
                        modAdoadotemp = new ModAdo();
                    }

                    //===============> Tested
                    if (fc_CtrlErrAvanc() == true)
                    {
                        return functionReturnValue;
                    }

                    fc_MajReception(Text11.Text, Text14.Text, SSetat.Text);


                    General.SQL = "select * from DevisEnTete where NumeroDevis='" + Text11.Text + "'";
                    modAdoadotemp = new ModAdo();
                    adotemp = modAdoadotemp.fc_OpenRecordSet(General.SQL);


                    bool isModified = false;

                    //===============> Tested
                    if (adotemp.Rows.Count == 0)
                    {
                        var NewRow = adotemp.NewRow();
                        Text115.Text = General.gsUtilisateur;
                        NewRow["IntervenantCreat"] = Text115.Text;
                        adotemp.Rows.Add(NewRow);
                    }

                    adotemp.Rows[0]["NumeroDevis"] = Text11.Text;
                    adotemp.Rows[0]["CodeImmeuble"] = Text14.Text;
                    //=========================> Ligne Modifié par Mondir : https://groupe-dt.mantishub.io/view.php?id=1465
                    //adotemp.Rows[0]["IntervenantModif"] = Text124.Text;
                    adotemp.Rows[0]["Annee"] = General.Left(Text11.Text, 4);
                    adotemp.Rows[0]["Mois"] = General.Mid(Text11.Text, 5, 2);

                    if (General.IsDate(txtDateImpression.Text))
                    {
                        adotemp.Rows[0]["DateImpression"] = txtDateImpression.Text;
                    }
                    //.Fields("Ordre") = Mid(Text1(1), 7, 4)

                    sTexte = General.Mid(Text11.Text, 7);
                    ///==================> Tested
                    if (sTexte.Contains("/"))
                    {
                        lLen = sTexte.Length;
                        lLen = lLen - 2;
                        adotemp.Rows[0]["Ordre"] = General.Mid(Text11.Text, 7, lLen);

                    }
                    else
                    {
                        adotemp.Rows[0]["Ordre"] = General.Mid(Text11.Text, 7);
                    }


                    adotemp.Rows[0]["observations"] = StdSQLchaine.gFr_DoublerQuote(Text311.Text);
                    if (General.IsNumeric(Text310.Text))
                        adotemp.Rows[0]["ModifApresAccord"] = Text310.Text;
                    else
                        adotemp.Rows[0]["ModifApresAccord"] = DBNull.Value;
                    ///==================> Tested
                    if (gbAjout)
                    {
                        adotemp.Rows[0]["DateCreation"] = dDate;
                        Text122.Text = Convert.ToString(dDate);
                    }
                    adotemp.Rows[0]["NumFicheStandard"] = General.nz(Text10.Text, 0);

                    adotemp.Rows[0]["CodeBE"] = General.nz(cmbBe.Text, System.DBNull.Value);
                    //=== modif du 23 07 2019.
                    adotemp.Rows[0]["TYD_Code"] = General.nz(ssTYD_TypeDevis.Text, System.DBNull.Value);
                    //=== fin modif du 23 07 2019.
                    //'=== modif du 27 03 2020.
                    adotemp.Rows[0]["MetierID"] = General.nz(ssCombo1.Text, System.DBNull.Value);
                    // '=== fin modif du 17 03 2020.
                    adotemp.Rows[0]["RenovationChauff"] = Check45.CheckState == CheckState.Checked ? 1 : 0;
                    //If txtDeviseur = "" Then
                    //    giMsg = MsgBox("Un deviseur est obligatoire", vbOKOnly + vbExclamation, "Deviseur")
                    //    txtDeviseur.SetFocus
                    //    Exit Sub
                    //Else
                    adotemp.Rows[0]["CodeDeviseur"] = txtDeviseur.Text;

                    ///===================> En C# si txtRespTrav.Value == null alors dans la base on aura NULL, en VB6 on aura EMPTY STRING("")
                    ///====================> Voir le  https://groupe-dt.mantishub.io/view.php?id=1465 et checkIfDevisModified()
                    if (txtRespTrav.Value != null)
                        adotemp.Rows[0]["ResponsableTravaux"] = txtRespTrav.Value;
                    else
                        adotemp.Rows[0]["ResponsableTravaux"] = "";

                    //End If
                    adotemp.Rows[0][19] = Text9.Text == "" ? "0" : Text9.Text;
                    adotemp.Rows[0]["AdrEnvoi1"] = Text5.Text;
                    adotemp.Rows[0]["AdrEnvoi2"] = Text6.Text;
                    adotemp.Rows[0]["CPEnvoi"] = Text7.Text;
                    adotemp.Rows[0]["VilleEnvoi"] = Text8.Text;
                    adotemp.Rows[0]["NbreExemplaire"] = Convert.ToInt16(Text4.Text);

                    ///==================> Tested
                    if (DateAcceptation.Checked && General.IsDate(DateAcceptation.Text))
                    {
                        adotemp.Rows[0]["DateAcceptation"] = Convert.ToDateTime(DateAcceptation.Text)
                            .ToString(General.FormatDateSQL);
                    }
                    else
                    {
                        adotemp.Rows[0]["DateAcceptation"] = System.DBNull.Value;
                    }

                    adotemp.Rows[0]["CodeCopieA"] = General.nz(ssDbCopieA.Text, 0);
                    adotemp.Rows[0]["DEV_LIbelleClient"] = txtDEV_LIbelleClient.Text;

                    ///==================> Tested
                    if (General.IsNumeric(Text31.Text))
                    {
                        adotemp.Rows[0]["NbreDeplacement"] = Text31.Text;
                        //
                    }
                    else
                    {
                        adotemp.Rows[0]["NbreDeplacement"] = 0;
                    }

                    ///==================> Tested
                    if (General.IsNumeric(Text318.Text))
                    {
                        adotemp.Rows[0]["PrixDeplacement"] = Text318.Text;
                    }
                    else
                    {
                        adotemp.Rows[0]["PrixDeplacement"] = 0;
                    }
                    ///============> Tested
                    if (General.IsNumeric(Text39.Text))
                    {
                        adotemp.Rows[0]["TotalHT"] = Text39.Text;
                    }
                    else
                    {
                        adotemp.Rows[0]["TotalHT"] = 0;
                    }
                    ///============> Tested
                    if (General.IsNumeric(Text321.Text))
                    {
                        adotemp.Rows[0]["TotalTVA"] = Text321.Text;
                    }
                    else
                    {
                        adotemp.Rows[0]["TotalTVA"] = 0;
                    }
                    ///============> Tested
                    if (General.IsNumeric(Text323.Text))
                    {
                        adotemp.Rows[0]["TotalTTC"] = Text323.Text;
                    }
                    else
                    {
                        adotemp.Rows[0]["TotalTTC"] = 0;
                    }
                    ///============> Tested
                    if (General.IsNumeric(Text317.Text))
                    {
                        adotemp.Rows[0]["TotalDeplacement"] = Text317.Text;
                    }
                    else
                    {
                        adotemp.Rows[0]["TotalDeplacement"] = 0;
                    }
                    ///============> Tested
                    if (General.IsNumeric(Text38.Text))
                    {
                        adotemp.Rows[0]["kDivers"] = Text38.Text;
                    }
                    else
                    {
                        adotemp.Rows[0]["kDivers"] = 1;
                    }

                    adotemp.Rows[0]["TitreDevis"] = Text322.Text + "";

                    adotemp.Rows[0]["MontantSouhaite"] = General.nz(Text310.Text, System.DBNull.Value);
                    // 'Text3(10) = nz(.Fields("MontantSouhaite"), "")
                    //'Text3(20) = nz(.Fields("CoefMontantSouhaite"), "1")
                    adotemp.Rows[0]["CoefMontantSouhaite"] = General.nz(Text320.Text, 1);

                    adotemp.Rows[0]["AvctChantier"] = General.nz(txtDevisEntete9.Text, System.DBNull.Value);
                    adotemp.Rows[0]["AvctFacturation"] = General.nz(txtDevisEntete10.Text, System.DBNull.Value);
                    adotemp.Rows[0]["AvctReglement"] = General.nz(txtDevisEntete11.Text, System.DBNull.Value);
                    adotemp.Rows[0]["ConditionPaiement"] = General.nz(txtDevisEntete12.Text, System.DBNull.Value);
                    //=== modif du 13 02 2017, ajout des champs
                    adotemp.Rows[0]["ComSurRecept"] = General.nz(txtDevisEntete23.Text, System.DBNull.Value);
                    adotemp.Rows[0]["ComSurCloture"] = General.nz(txtDevisEntete24.Text, System.DBNull.Value);
                    adotemp.Rows[0]["Receptionne"] = General.nz(Check47.CheckState == CheckState.Checked ? 1 : 0, 0);
                    adotemp.Rows[0]["ClotureDevis"] = General.nz(Check48.CheckState == CheckState.Checked ? 1 : 0, 0);
                    ///============> Tested
                    //////=========================> Ajouter par mondir pcq si on fait pas un test sur Checked alors le test est toujour true
                    if (General.IsDate(DTPicker10.Text) && DTPicker10.Checked)
                    {
                        adotemp.Rows[0]["DateReception"] = DTPicker10.Text;
                    }
                    else
                    {
                        adotemp.Rows[0]["DateReception"] = System.DBNull.Value;
                    }
                    ///============> Tested
                    //////=========================> Ajouter par mondir pcq si on fait pas un test sur Checked alors le test est toujour true
                    if (General.IsDate(DTPicker11.Text) && DTPicker11.Checked)
                    {
                        adotemp.Rows[0]["ClotureLe"] = DTPicker11.Text;
                    }
                    else
                    {
                        adotemp.Rows[0]["ClotureLe"] = System.DBNull.Value;
                    }
                    adotemp.Rows[0]["ReceptionnePar"] = General.nz(txtDevisEntete26.Text, System.DBNull.Value);
                    //=== FIN
                    adotemp.Rows[0]["Situation"] = General.nz(txtDevisEntete13.Text, System.DBNull.Value);
                    adotemp.Rows[0]["Acompte"] = General.nz(txtDevisEntete14.Text, System.DBNull.Value);
                    adotemp.Rows[0]["DelaisReglmt"] = General.nz(txtDevisEntete15.Text, System.DBNull.Value);

                    //====
                    //=== modif du 23 07 2019.
                    adotemp.Rows[0]["CLoturerPar"] = General.nz(txtDevisEntete29.Text, System.DBNull.Value);
                    adotemp.Rows[0]["CodeReglement"] = General.nz(txtDevisEntete28.Text, System.DBNull.Value);

                    ///=========================> Ajouter par mondir pcq si on fait pas un test sur Checked alors le test est toujour true
                    if (DTPicker12.Checked)
                        adotemp.Rows[0]["AcompteRecuLe"] = General.nz(DTPicker12.Text, System.DBNull.Value);
                    else
                        adotemp.Rows[0]["AcompteRecuLe"] = System.DBNull.Value;

                    //=== fin modif du 23 07 2019.

                    adotemp.Rows[0]["CodeTitre"] = txtdesArticle.Text;
                    ///============> Tested
                    if (General.IsNumeric(SSOleDBCombo1.Text))
                    {
                        adotemp.Rows[0]["TypePaiement"] = SSOleDBCombo1.Text;

                    }
                    //.Fields("TextePaiement") = Left(gFr_DoublerQuote(SSOleDBCombo1Bis.Text), .Fields("TextePaiement").DefinedSize)

                    //adotemp.Rows[0]["TextePaiement"] = General.Left(SSOleDBCombo1Bis.Text,adotemp.Rows[0]["TextePaiement"].ToString().Length);
                    //updated by mohammed https://groupe-dt.mantishub.io/view.php?id=1633
                    adotemp.Rows[0]["TextePaiement"] = General.Left(SSOleDBCombo1Bis.Text, 785);

                    adotemp.Rows[0]["enteteDevis"] = SSOleDBCombo2.Text;
                    adotemp.Rows[0]["codeetat"] = SSetat.Text;
                    adotemp.Rows[0]["DEV_CoefMOdefaut"] = General.nz(Text315.Text, 1);
                    adotemp.Rows[0]["DEV_CoefFOdefaut1"] = General.nz(Text314.Text, 1);
                    adotemp.Rows[0]["DEV_CoefSTdefaut2"] = General.nz(Text313.Text, 1);
                    adotemp.Rows[0]["DEV_CoefPrixdefaut2"] = General.nz(Text316.Text, System.DBNull.Value);

                    //'=== modif du 17 09 2018.
                    adotemp.Rows[0]["TelPart"] = General.nz(txtDevisEntete25.Text, System.DBNull.Value);
                    adotemp.Rows[0]["EmailPart"] = General.nz(txtDevisEntete27.Text, System.DBNull.Value);
                    adotemp.Rows[0]["AffichePart"] = Check410.Checked;
                    //'=== fin modif 19 09 2018.

                    ///==================> Tested
                    ///==================> https://groupe-dt.mantishub.io/view.php?id=1465
                    ///==================> DateMAJ to DateTime.Now, Was Text121.Text
                    if (adotemp.Rows.Count > 0)
                        if (checkIfDevisModified(adotemp.Rows[0], Text11.Text))
                        {
                            adotemp.Rows[0]["DateMAJ"] = DateTime.Now.ToString(General.FormatDateSQL);
                            Text124.Text = General.gsUtilisateur;
                            adotemp.Rows[0]["IntervenantModif"] = Text124.Text;
                        }

                    //if (General.IsDate(Text121.Text))
                    //{
                    //    adotemp.Rows[0]["DateMAJ"] = DateTime.Now.ToString(General.FormatDateSQL);

                    //}
                    //else
                    //{
                    //    adotemp.Rows[0]["DateMaj"] = System.DBNull.Value;
                    //}

                    var xx = modAdoadotemp.Update();
                }
                catch (Exception e)
                {
                    Erreurs.gFr_debug(e, this.Name + ";valider_ErrGrille");
                    SSDBGridCorps.UpdateData();
                    functionReturnValue = false;
                    return functionReturnValue;
                }


                if (gbAjout)
                {
                    //   For i = 1 To .Tabs - 1
                    //       .TabEnabled(i) = True
                    //   Next
                }

                modAdoadotemp.Dispose();
                adotemp = null;
                gbAjout = false;

                //----validation du corps de devis
                ///==========> Tested
                if (BOOLErreurCorps == false)
                {
                    // adocnn.BeginTrans

                    ValidationCorps();
                    boolModif = false;
                    // adocnn.CommitTrans

                }
                else
                {
                    afficheMessage(Convert.ToString(3));
                }

                ////================> Tested
                if (book != null)
                    SSDBGridCorps.ActiveColScrollRegion.ScrollRowIntoView(book, SSDBGridCorps.ActiveRowScrollRegion);

                ////===============> Tested
                if (lRowNumber != -1)
                {
                    SSDBGridCorps.Rows[lRowNumber].Selected = true;
                }

                modSaveDevis.fc_SaveDevis(Text11.Text);
                try
                {
                    General.SQL = "UPDATE Immeuble " + " SET " + " Immeuble.Gardien = '" +
                                  StdSQLchaine.gFr_DoublerQuote(Text14X1.Text) +
                                  "', Immeuble.AdresseGardien = '" + StdSQLchaine.gFr_DoublerQuote(Text10x.Text) + "'," +
                                  " Immeuble.TelGardien = '" + StdSQLchaine.gFr_DoublerQuote(Text15.Text) +
                                  "'," + " Immeuble.FaxGardien = '" + StdSQLchaine.gFr_DoublerQuote(Text16.Text) +
                                  "', Immeuble.SpecifAcces = '" + StdSQLchaine.gFr_DoublerQuote(Text112.Text) +
                                  "'," + " Immeuble.CodeAcces1 = '" + StdSQLchaine.gFr_DoublerQuote(Text17.Text) +
                                  "'," + " Immeuble.CodeAcces2 = '" + StdSQLchaine.gFr_DoublerQuote(Text19.Text) +
                                  "', Immeuble.horaires = '" + StdSQLchaine.gFr_DoublerQuote(Text18.Text) + "'," +
                                  " Immeuble.BoiteAClef = " + (Checkboite.CheckState == CheckState.Checked ? 1 : 0) +
                                  "," +
                                  " Immeuble.SitBoiteAClef = '" + StdSQLchaine.gFr_DoublerQuote(SituaBoite.Text) +
                                  "'," + " Immeuble.Observations = '" + StdSQLchaine.gFr_DoublerQuote(Text118.Text)
                                  + "'," + " Immeuble.HorairesLoge = '" + StdSQLchaine.gFr_DoublerQuote(Text113.Text) +
                                  "', Immeuble.CodeTVA = '" + txtTRouTP.Text + "'," + "P1='" +
                                  (Check41.CheckState == CheckState.Checked ? 1 : 0) + "'," + "P2='" +
                                  (Check42.CheckState == CheckState.Checked ? 1 : 0) + "'," + "P3='" +
                                  (Check43.CheckState == CheckState.Checked ? 1 : 0) + "'," + "P4='" +
                                  (Check44.CheckState == CheckState.Checked ? 1 : 0) + "'" +
                                  " WHERE Immeuble.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(Text14.Text) + "'";

                    var xx = General.Execute(General.SQL);

                    General.SQL = "UPDATE Table1 " + " SET Table1.Observations = '" +
                                  StdSQLchaine.gFr_DoublerQuote(Text119.Text) + "'" + " WHERE table1.Code1 ='" +
                                  StdSQLchaine.gFr_DoublerQuote(Text2.Text) + "'";

                    xx = General.Execute(General.SQL);

                    General.SQL = "UPDATE TechReleve " + " SET TechReleve.Obs = '" +
                                  StdSQLchaine.gFr_DoublerQuote(Text120.Text) + " '" +
                                  " WHERE TechReleve.CodeImmeuble ='" +
                                  StdSQLchaine.gFr_DoublerQuote(Text14.Text) + "'";

                    xx = General.Execute(General.SQL);
                    functionReturnValue = true;
                    return functionReturnValue;

                }
                catch (Exception e)
                {
                    Erreurs.gFr_debug(e, this.Name + ";valider_ErrReq");
                    //MsgBox err.Description, vbCritical, err.Source
                    functionReturnValue = false;
                }
            }
            catch (Exception e)
            {
                functionReturnValue = false;
                Erreurs.gFr_debug(e, this.Name + ";valider");
                return functionReturnValue;
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNumDevis"></param>
        public void prcCalculTotalAvantCoef(string sNumDevis)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"prcCalculTotalAvantCoef() - Enter In The Function - sNumDevis = {sNumDevis}");
            //===> Fin Modif Mondir

            try
            {
                double iMOhd = 0;
                double cMOd = 0;
                double cFOd = 0;
                double cSTd = 0;
                double cTotd = 0;
                double cMO = 0;
                double cFO = 0;
                double cST = 0;
                double cTot = 0;

                double dbMoSimple = 0;
                double dbMoEquipe = 0;

                double dbSommeTVA = 0;
                double dbSommeTTC = 0;
                double dbSommeHT = 0;
                double dbTotdebours = 0;
                double dbCoefDev = 0;
                double dbTTCsansKfdivers = 0;

                double kMO = 0;
                double kFO = 0;
                double kST = 0;
                double kDeplace = 0;
                double kDivers = 0;
                double kMaj = 0;

                object VarBook = null;
                short i = 0;

                dbMoSimple = 0;
                dbMoEquipe = 0;

                //var _with99 = SSDBGridCorps;
                SSDBGridCorps.UpdateData();
                //-- si une erreur a été decelée dans la grille du corps du devis dans l'évenement
                //-- BeforeColUpdate et BeforeUpadte on sort de la fonction sinon risque de message
                //-- d'erreur à répétition.
                if (blnErrGridUpdate == true)
                {
                    return;
                }

                /////=======================> Tested
                for (i = 0; i <= SSDBGridCorps.Rows.Count - 1; i++)
                {
                    iMOhd = iMOhd + Convert.ToDouble(General.nz(SSDBGridCorps.Rows[i].Cells["Mohd"].Value.ToString(),
                                "0"));

                    //=== affine le détail de la main d'oeuvre.
                    if (General.nz(SSDBGridCorps.Rows[i].Cells["CodeMO"].Value.ToString(), "").ToString() == "S")
                    {
                        dbMoSimple = dbMoSimple +
                                     Convert.ToDouble(General.nz(SSDBGridCorps.Rows[i].Cells["Mohd"].Value.ToString(),
                                         "0"));
                    }
                    else if (General.nz(SSDBGridCorps.Rows[i].Cells["CodeMO"].Value.ToString(), "").ToString() == "E")
                    {
                        dbMoEquipe = dbMoEquipe +
                                     Convert.ToDouble(General.nz(SSDBGridCorps.Rows[i].Cells["Mohd"].Value.ToString(),
                                         "0"));
                    }

                    //==== Deboursé.
                    cMOd = cMOd + Convert.ToDouble(General.nz(SSDBGridCorps.Rows[i].Cells["MOd"].Value, "0"));
                    cFOd = cFOd + Convert.ToDouble(General.nz(SSDBGridCorps.Rows[i].Cells["Fod"].Value, "0"));
                    cSTd = cSTd + Convert.ToDouble(General.nz(SSDBGridCorps.Rows[i].Cells["Std"].Value, "0"));
                    cTotd = cTotd + Convert.ToDouble(General.nz(SSDBGridCorps.Rows[i].Cells["TotalDebours"].Value,
                                "0"));

                    //==== Total ht aprés application des coeficients Mo,St,Fo.
                    cMO = cMO + Convert.ToDouble(General.nz(SSDBGridCorps.Rows[i].Cells["Mo"].Value, "0"));
                    cFO = cFO + Convert.ToDouble(General.nz(SSDBGridCorps.Rows[i].Cells["Fo"].Value, "0"));
                    cST = cST + Convert.ToDouble(General.nz(SSDBGridCorps.Rows[i].Cells["st"].Value, "0"));
                    cTot = cTot + Convert.ToDouble(General.nz(SSDBGridCorps.Rows[i].Cells["TotalVente"].Value, "0"));
                }

                Text30.Text = Convert.ToString(iMOhd);



                //=== main d'oeuvre équipe, onglet avancement.
                txtDevisEntete4.Text = Convert.ToString(dbMoEquipe);
                //txtDevisEntete(4) = Format(DateAdd("s", dbMoEquipe * 3600, #12:00:00 AM#), "hh:mm:ss")
                //=== main d'oeuvre simple, onglet avancement.
                txtDevisEntete5.Text = Convert.ToString(dbMoSimple);
                //txtDevisEntete(5) = Format(DateAdd("s", dbMoSimple * 3600, #12:00:00 AM#), "hh:mm:ss")


                Text312.Text = Convert.ToString(cMOd);
                Text32.Text = Convert.ToString(cFOd);
                Text33.Text = Convert.ToString(cSTd);
                Text34.Text = Convert.ToString(cTotd);

                //============= calcul des coef.
                //=== applique le coef Mo a la Mo.
                ////==================================> Tested
                if (cMOd == 0)
                {
                    kMO = 0;
                }
                else
                {
                    kMO = cMO / cMOd;
                }
                Text35.Text = Convert.ToString(kMO);

                //=== applique le Coef Fourn à la fourniture.
                //=============================> Tested
                if (cFOd == 0)
                {
                    kFO = 0;
                }
                else
                {
                    kFO = cFO / cFOd;
                }
                Text36.Text = Convert.ToString(kFO);

                //=== applique le Coef Stt au total sst.
                //=============================> Tested
                if (cSTd == 0)
                {
                    kST = 0;
                }
                else
                {
                    kST = cST / cSTd;
                }
                Text37.Text = Convert.ToString(kST);

                //=== calcul du coefficient de deplacement.
                ///===============> Tested
                if (General.IsNumeric(Text317.Text))
                {
                    if (cTot == 0)
                    {
                        kDeplace = 0;
                    }
                    else
                    {
                        kDeplace = (cTot + Convert.ToDouble(Text317.Text)) / cTot;
                    }
                }
                else
                {
                    kDeplace = 1;
                }
                Text319.Text = Convert.ToString(kDeplace);

                //=== mémorise le coefficient divers.
                ///===============> Tested
                if (General.IsNumeric(Text38.Text))
                {
                    kDivers = Convert.ToDouble(Text38.Text);
                }
                else
                {
                    kDivers = 1;
                }
                Text38.Text = Convert.ToString(kDivers);

                //=== mémorise le coefficient obtenu à partir du montant souhaité.
                ///===============> Tested
                if (General.IsNumeric(Text320.Text))
                {
                    kMaj = Convert.ToDouble(Text320.Text);
                }
                else
                {
                    kMaj = 1;
                }
                Text320.Text = Convert.ToString(kMaj);


                //=== mise a jour des prix apres vente
                SSDBGridCorps.UpdateData();

                var manager = SSDBGridCorps.EventManager;
                manager.AllEventsEnabled = false;

                ///=================> Tested
                for (i = 0; i <= SSDBGridCorps.Rows.Count - 1; i++)
                {
                    SSDBGridCorps.Rows[i].Cells["NumeroLigne"].Value = i;
                    SSDBGridCorps.Rows[i].Cells["coef1"].Value = kDeplace;
                    SSDBGridCorps.Rows[i].Cells["coef2"].Value = kDivers;
                    SSDBGridCorps.Rows[i].Cells["coef3"].Value = kMaj;

                    SSDBGridCorps.Rows[i].Cells["TotalVenteApresCoef"].Value =
                        Convert.ToDouble(
                            Convert.ToDouble(
                                General.nz(SSDBGridCorps.Rows[i].Cells["TotalVente"].Value.ToString(), "0")) *
                            kDeplace * kDivers * kMaj);

                    SSDBGridCorps.UpdateData();

                    dbTTCsansKfdivers = dbTTCsansKfdivers +
                                        Convert.ToDouble(
                                            Convert.ToDouble(General.nz(
                                                SSDBGridCorps.Rows[i].Cells["TotalVente"].Value.ToString(), "0")) *
                                            kDeplace);
                    dbTotdebours = dbTotdebours +
                                   Convert.ToDouble(General.nz(SSDBGridCorps.Rows[i].Cells["TotalDebours"].Value, "0"));
                    dbSommeHT = dbSommeHT + Convert.ToDouble(
                                    General.nz(SSDBGridCorps.Rows[i].Cells["TotalVenteApresCoef"].Value, "0"));
                    if (!string.IsNullOrEmpty(SSDBGridCorps.Rows[i].Cells["CodeTva"].Value.ToString()) &&
                        !string.IsNullOrEmpty(SSDBGridCorps.Rows[i].Cells["Quantite"].Value.ToString()))
                    {
                        dbSommeTVA = dbSommeTVA +
                                     Convert.ToDouble(
                                         General.nz(SSDBGridCorps.Rows[i].Cells["TotalVenteApresCoef"].Value, "0")) *
                                     (Convert.ToDouble(General.nz(SSDBGridCorps.Rows[i].Cells["CodeTVA"].Value, "0")) /
                                      100);
                    }
                    else if (!string.IsNullOrEmpty(SSDBGridCorps.Rows[i].Cells["Quantite"].Value.ToString()))
                    {
                        //  afficheMessage 4
                    }
                    dbSommeTTC = dbSommeHT + dbSommeTVA;

                }
                SSDBGridCorps.UpdateData();

                manager.AllEventsEnabled = true;

                //=== mise à jour du ceof devis onglet avancement.
                ///==============> Tested
                if (dbTotdebours != 0)
                {
                    dbCoefDev = dbSommeHT / dbTotdebours;
                    txtDevisEntete6.Text = Convert.ToString(Math.Round(dbCoefDev, 2));
                }
                else
                {
                    txtDevisEntete6.Text = Convert.ToString(0);
                }

                //=== calcul de la remise accordée onglet avancement.
                ///==============> Tested
                if ((dbSommeHT != dbTTCsansKfdivers) && dbSommeHT != 0)
                {
                    //If dbTTCsansKfdivers > dbSommeHT Then
                    txtDevisEntete7.Text = Convert.ToString(Math.Round(dbTTCsansKfdivers - dbSommeHT, 2));
                    //Else
                    //    txtDevisEntete(7) = FncArrondir(dbSommeHT - dbTTCsansKfdivers, 2)
                    //End If
                }
                else
                {
                    txtDevisEntete7.Text = Convert.ToString(0);
                }

                //===> Mondir le 09.02.2021, added MidpointRounding.AwayFromZero, bug reported by Rachid
                Text39.Text = Convert.ToString(Math.Round(dbSommeHT, 2, MidpointRounding.AwayFromZero));
                Text321.Text = Convert.ToString(Math.Round(dbSommeTVA, 2, MidpointRounding.AwayFromZero));
                //Text323.Text = Convert.ToString(Math.Round(dbSommeTTC, 2, MidpointRounding.AwayFromZero));
                Text323.Text = General.FncArrondir(Text39.Text.ToDouble() + Text321.Text.ToDouble(), 2).ToString();
                //===> Fin Modif Mondir

                //    .MoveFirst
                //    For i = 0 To .Rows - 1
                //        dbSommeHT = dbSommeHT + Numeric(nz(.Columns("TotalVenteApresCoef").value, "0"))
                //        If .Columns("CodeTva").value <> "" And .Columns("Quantite").value <> "" Then
                //            dbSommeTVA = dbSommeTVA + Numeric(nz(.Columns("TotalVenteApresCoef").value, "0")) _
                //'                            * (nz(.Columns("CodeTVA").value, "0") / 100)
                //        ElseIf .Columns("Quantite").value <> "" Then
                //          '  afficheMessage 4
                //        End If
                //        dbSommeTTC = dbSommeHT + dbSommeTVA
                //        .MoveNext
                //    Next
                //    Text3(9) = CCur(FncArrondir(dbSommeHT, 2))
                //    Text3(21) = CCur(FncArrondir(dbSommeTVA, 2))
                //    Text3(23) = CCur(FncArrondir(dbSommeTTC, 2))

                ///==============> Tested
                if (fc_VerifDevis() == true)
                {
                    frmSynthese.Visible = true;
                }
                else
                {
                    frmSynthese.Visible = false;
                }

                int xx = General.Execute("UPDATE DevisEntete SET DevisEntete.TotalAvantCoef=" + General.nz(Text34.Text.Replace(",", "."), "0") +
                                ", DevisEntete.TotalHT=" +
                                General.nz(Text39.Text.Replace(",", "."), "0") + " WHERE DevisEntete.NumeroDevis='" + Text11.Text + "'");

                //ne pas oublier d'arrondir les TVA et TTC
                //r      Text3(9) = CCur(FncArrondir(rs(0), 2))
                //r     Text3(21) = CCur(FncArrondir(rs(1), 2))
                //r      Text3(23) = CCur(FncArrondir(rs(2), 2))

                //r     Text3(9) = 0
                //r      Text3(21) = 0
                //r      Text3(23) = 0
                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";prcCalculTotalAvantCoef");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool fc_VerifDevis()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_VerifDevis() - Enter In The Function");
            //===> Fin Modif Mondir

            bool functionReturnValue = false;
            DataTable rsDevis = default(DataTable);
            ModAdo modAdorsDevis = new ModAdo();
            string strSelect = null;
            string sNodevis = null;
            string sNumFicheStandard = null;
            bool Match = false;
            int lngHeure = 0;
            int lngMinute = 0;
            double lngSeconde = 0;
            System.DateTime sDuree = default(System.DateTime);
            string sDureeTemp = null;
            double sDureeDiff = 0;
            int i = 0;
            double dbTotFacture = 0;

            functionReturnValue = true;

            //===> Mondir le 06.11.2020, below line was commented
            txtCharges.Text = "0";

            //Récupère le ou les devis liés aux interventions sélectionnées
            strSelect = "SELECT DISTINCT GestionStandard.NoDevis,GestionStandard.NumFicheStandard " +
                        " FROM GestionStandard INNER JOIN Intervention ";
            strSelect = strSelect + " ON GestionStandard.NumFicheStandard=Intervention.NumFicheStandard ";
            strSelect = strSelect + " WHERE GestionStandard.NoDevis='" + Text11.Text + "'";
            rsDevis = modAdorsDevis.fc_OpenRecordSet(strSelect);
            txtDevisEntete20.Text = "0";

            ///===================> Tested
            if (rsDevis.Rows.Count > 0)
            {
                //Stock le numéro de devis et le numéro de la fiche d'appel
                sNodevis = rsDevis.Rows[0]["NoDevis"] + "";
                sNumFicheStandard = rsDevis.Rows[0]["Numfichestandard"] + "";
                txtDevisEntete20.Text = sNumFicheStandard;
            }
            else
            {
                modAdorsDevis.Dispose();
                functionReturnValue = false;
                return functionReturnValue;
            }


            //Calcul du montant total du devis
            modAdorsDevis = new ModAdo();
            rsDevis = modAdorsDevis.fc_OpenRecordSet("SELECT SUM(DevisDetail.TotalVenteApresCoef) AS TotalVente " +
                                                     " FROM DevisDetail WHERE DevisDetail.NumeroDevis='" +
                                                     sNodevis + "'" + " GROUP BY DevisDetail.NumeroDevis");

            ///===================> Tested
            if (rsDevis.Rows.Count > 0)
            {
                Label247.Text = Convert.ToDouble(General.nz(rsDevis.Rows[0]["TotalVente"], "0")) + " €";
            }
            else
            {
                Label247.Text = "0 €";
            }
            modAdorsDevis.Dispose();

            modAdorsDevis = new ModAdo();

            //Calcul du montant total des achats réalisés sur ce devis
            rsDevis = modAdorsDevis.fc_OpenRecordSet(
                "SELECT SUM(BCD_Detail.BCD_PrixHT * BCD_Detail.BCD_Quantite) AS TotalAchats " +
                " FROM BCD_Detail INNER JOIN BonDeCommande " +
                " ON BCD_Detail.BCD_Cle=BonDeCommande.NoBonDeCommande " + " WHERE BonDeCommande.NumFicheStandard=" +
                sNumFicheStandard +
                " AND NOT BCD_PrixHT IS NULL AND NOT BCD_Quantite IS NULL " +
                " GROUP BY BonDeCommande.NumFicheStandard,BCD_Detail.BCD_PrixHT, " + " BCD_Detail.BCD_Quantite");

            ///===================> Tested
            if (rsDevis.Rows.Count > 0)
            {
                lblTotalAchats.Text = "0";
                foreach (DataRow rsDevisRow in rsDevis.Rows)
                {
                    lblTotalAchats.Text = Convert.ToString(Convert.ToDouble(lblTotalAchats.Text) +
                                                           Convert.ToDouble(General.nz(rsDevisRow["TotalAchats"],
                                                               "0")));
                }
                txtDevisEntete16.Text = lblTotalAchats.Text;
                lblTotalAchats.Text = lblTotalAchats.Text + " €";
            }
            else
            {
                txtDevisEntete16.Text = "0";
                lblTotalAchats.Text = "0 €";
            }


            modAdorsDevis.Dispose();

            modAdorsDevis = new ModAdo();

            //Calcul du montant total des factures sur situation de ce devis
            tabNoFacture = new string[1];
            //Sert à afficher une info bulle
            tabMontantFact = new string[1];
            //Sert à afficher une info bulle

            strSelect = "";
            strSelect = "SELECT FactureManuellePied.MontantHT AS TotalSituations," +
                        " FactureManuelleEntete.NoFacture FROM FactureManuellePied";
            strSelect = strSelect + " INNER JOIN FactureManuelleEntete ON ";
            strSelect = strSelect + " FactureManuellePied.CleFactureManuelle=" +
                        " FactureManuelleEntete.CleFactureManuelle ";
            strSelect = strSelect + " INNER JOIN Intervention ON ";
            strSelect = strSelect + " FactureManuelleEntete.NoFacture=Intervention.NoFacture ";
            strSelect = strSelect + " WHERE Intervention.NumFicheStandard=" + General.nz(sNumFicheStandard, "0") + "";
            strSelect = strSelect + " AND NOT FactureManuellePied.MontantHT IS NULL ";
            strSelect = strSelect +
                        " GROUP BY FactureManuellePied.MontantHT,FactureManuelleEntete.NoFacture,Intervention.NumFicheStandard ";
            rsDevis = modAdorsDevis.fc_OpenRecordSet(strSelect);
            lblTotalSituations.Text = "0";
            i = 0;
            dbTotFacture = 0;

            ///==================> Tested
            if (rsDevis.Rows.Count > 0)
            {
                foreach (DataRow rsDevisRow in rsDevis.Rows)
                {
                    if (tabNoFacture.Length - 1 < i)
                    {
                        Array.Resize(ref tabNoFacture, i + 1);
                    }
                    if (tabMontantFact.Length - 1 < i)
                    {
                        Array.Resize(ref tabMontantFact, i + 1);
                    }
                    tabNoFacture[i] = rsDevisRow["NoFacture"] + "";
                    tabMontantFact[i] = Convert.ToString(rsDevisRow["TotalSituations"] + "");
                    lblTotalSituations.Text =
                        Convert.ToString(Convert.ToDouble(lblTotalSituations.Text) +
                                         Convert.ToDouble(General.nz(rsDevisRow["TotalSituations"], "0")));
                    dbTotFacture = dbTotFacture + Convert.ToDouble(General.nz(rsDevisRow["TotalSituations"], "0"));
                    i = i + 1;
                }
                txtTotalSituation.Text = lblTotalSituations.Text;
                lblTotalSituations.Text = lblTotalSituations.Text + " €";
                txtDevisEntete19.Text = Convert.ToString(dbTotFacture);

            }
            else
            {
                txtTotalSituation.Text = "0";
                lblTotalSituations.Text = "0 €";
                ToolTip1.SetToolTip(lblTotalSituations, "Aucune facture enregistrée");
                txtDevisEntete19.Text = "0";

            }
            modAdorsDevis.Dispose();

            modAdorsDevis = new ModAdo();
            strSelect = "";
            strSelect = strSelect + "SELECT Intervention.HeureDebut,Intervention.HeureFin,";
            strSelect = strSelect +
                        " Intervention.Duree,Intervention.Deplacement, Intervention.DateRealise, Intervention.Intervenant FROM Intervention ";
            strSelect = strSelect + " WHERE Intervention.NumFicheStandard=" + sNumFicheStandard;
            rsDevis = modAdorsDevis.fc_OpenRecordSet(strSelect);
            if (rsDevis.Rows.Count > 0)
            {
                sDuree = Convert.ToDateTime("01/01/1900 00:00:00");
                foreach (DataRow rsDevisRow in rsDevis.Rows)
                {
                    if (rsDevisRow["Duree"] != DBNull.Value)
                    {
                        if (Convert.ToDateTime(Convert.ToDateTime(rsDevisRow["Duree"]).ToString("HH:mm:ss")) >
                            Convert.ToDateTime("00:00:00"))
                        {
                            sDuree = sDuree.AddSeconds(Convert.ToDateTime(rsDevisRow["Duree"]).Second);
                            sDuree = sDuree.AddMinutes(Convert.ToDateTime(rsDevisRow["Duree"]).Minute);
                            sDuree = sDuree.AddHours(Convert.ToDateTime(rsDevisRow["Duree"]).Hour);

                            //txtCharges.Text = CStr(CDbl(nz(txtCharges.Text, "0")) + CDbl(fc_CalcDebourse(rsDevis!Duree, IIf(rsDevis!Deplacement & "" = "1", True, False))))

                            ///=============> Tested first
                            if (General.IsDate(rsDevisRow["DateRealise"].ToString()))
                            {
                                txtCharges.Text =
                                    Convert.ToString(Convert.ToDouble(General.nz(txtCharges.Text, "0")) +
                                                     Convert.ToDouble(General.fc_CalcDebourse(
                                                         Convert.ToDateTime(rsDevisRow["Duree"]),
                                                         rsDevisRow["Intervenant"].ToString(),
                                                         Convert.ToDateTime(rsDevisRow["DateRealise"]),
                                                         (rsDevisRow["Deplacement"] + "" == "1"))));
                            }
                            else
                            {
                                txtCharges.Text =
                                    Convert.ToString(Convert.ToDouble(General.nz(txtCharges.Text, "0")) +
                                                     Convert.ToDouble(General.fc_CalcDebourse(
                                                         Convert.ToDateTime(rsDevisRow["Duree"]),
                                                         rsDevisRow["Intervenant"].ToString(), General.cDefDateRealise,
                                                         (rsDevisRow["Deplacement"] + "" == "1" ? true : false))));
                            }
                        }
                        else if (rsDevisRow["HeureDebut"] != DBNull.Value && rsDevisRow["HeureFin"] != DBNull.Value)
                        {
                            sDureeTemp = General.fc_calcDuree(Convert.ToDateTime(rsDevisRow["HeureDebut"]),
                                Convert.ToDateTime(rsDevisRow["HeureFin"]));
                            sDureeTemp = Convert.ToDateTime(sDureeTemp).ToString("HH:mm:ss");
                            sDuree = sDuree.AddSeconds(Convert.ToDateTime(sDureeTemp).Second);
                            sDuree = sDuree.AddMinutes(Convert.ToDateTime(sDureeTemp).Minute);
                            sDuree = sDuree.AddHours(Convert.ToDateTime(sDureeTemp).Hour);
                            //txtCharges.Text = CStr(CDbl(nz(txtCharges.Text, "0")) + CDbl(fc_CalcDebourse(CDate(sDureeTemp), IIf(rsDevis!Deplacement & "" = "1", True, False))))
                            if (General.IsDate(rsDevisRow["DateRealise"].ToString()))
                            {
                                txtCharges.Text = Convert.ToString(Convert.ToDouble(General.nz(txtCharges.Text, "0")) +
                                                                   Convert.ToDouble(General.fc_CalcDebourse(
                                                                       Convert.ToDateTime(sDureeTemp),
                                                                       rsDevisRow["Intervenant"].ToString(),
                                                                       Convert.ToDateTime(rsDevisRow["DateRealise"]),
                                                                       (rsDevisRow["Deplacement"] + "" == "1"
                                                                           ? true
                                                                           : false))));
                            }
                            else
                            {
                                txtCharges.Text = Convert.ToString(Convert.ToDouble(General.nz(txtCharges.Text, "0")) +
                                                                   Convert.ToDouble(General.fc_CalcDebourse(
                                                                       Convert.ToDateTime(sDureeTemp),
                                                                       rsDevisRow["Intervenant"].ToString(),
                                                                       General.cDefDateRealise,
                                                                       (rsDevisRow["Deplacement"] + "" == "1"
                                                                           ? true
                                                                           : false))));
                            }

                        }
                    }
                }
                modAdorsDevis.Dispose();
                sDureeDiff = (sDuree - Convert.ToDateTime("01/01/1900 00:00:00")).TotalSeconds;
                if (sDureeDiff < 0)
                {
                    sDureeDiff = sDureeDiff * (-1);
                }
                lngHeure = Convert.ToInt32(sDureeDiff / 3600);

                if (lngHeure * 3600 > sDureeDiff)
                {
                    lngHeure = lngHeure - 1;
                }

                lngMinute = Convert.ToInt32((sDureeDiff - (lngHeure * 3600)) / 60);

                if (lngMinute > ((sDureeDiff - (lngHeure * 3600)) / 60))
                {
                    lngMinute = lngMinute - 1;
                }

                lngSeconde = (sDureeDiff - (lngHeure * 3600) - (lngMinute * 60));

                if (Convert.ToString(lngHeure).Length == 1)
                {
                    lblTotalHeures.Text = "0" + lngHeure + ":";
                }
                else
                {
                    lblTotalHeures.Text = lngHeure + ":";
                }

                if (Convert.ToString(lngMinute).Length == 1)
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + "0" + lngMinute + ":";
                }
                else
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + lngMinute + ":";
                }

                if (Convert.ToString(lngSeconde).Length == 1)
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + "0" + lngSeconde;
                }
                else
                {
                    lblTotalHeures.Text = lblTotalHeures.Text + lngSeconde;
                }

                txtDevisEntete18.Text = lblTotalHeures.Text;

            }
            else
            {
                lblTotalHeures.Text = "00:00:00";

                txtDevisEntete18.Text = lblTotalHeures.Text;

            }

            if (Convert.ToDouble(General.nz(lblTotalAchats.Text.Replace("€", ""), "0")) +
                Convert.ToDouble(General.nz(txtCharges.Text, "0")) != 0)
            {
                lblCoefActuel.Text = Convert.ToString(Convert.ToDouble(General.nz(txtTotalSituation.Text, "0")) /
                                                      (Convert.ToDouble(lblTotalAchats.Text.Replace("€", "").Replace(" ", "")) +
                                                       Convert.ToDouble(General.nz(txtCharges.Text, "0"))));
            }
            else
            {
                lblCoefActuel.Text = "0.00";
            }

            //===> Mondir le 06.11.2020, Fix round, i added ,2, to fix this ticket https://groupe-dt.mantishub.io/view.php?id=2000
            lblCoefActuel.Text = Convert.ToString(Math.Round(Convert.ToDouble(lblCoefActuel.Text), 2));

            rsDevis = null;
            return functionReturnValue;



        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool fc_CtrlErrAvanc()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_CtrlErrAvanc() - Enter In The Function");
            //===> Fin Modif Mondir

            bool functionReturnValue = false;
            //=== retourne true en cas d'erreur constatée.
            functionReturnValue = false;

            if (!string.IsNullOrEmpty(txtDevisEntete9.Text))
            {
                if (!General.IsNumeric(txtDevisEntete9.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur dans la case 'Avancement chantier' doit être de type numérique.",
                        "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    functionReturnValue = true;
                    return functionReturnValue;
                }
            }

            if (!string.IsNullOrEmpty(txtDevisEntete10.Text))
            {
                if (!General.IsNumeric(txtDevisEntete10.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur dans la case 'Avancement facturation' doit être de type numérique.",
                        "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    functionReturnValue = true;
                    return functionReturnValue;
                }
            }

            if (!string.IsNullOrEmpty(txtDevisEntete11.Text))
            {
                if (!General.IsNumeric(txtDevisEntete11.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur dans la case 'Avancement réglement' doit être de type numérique.",
                        "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    functionReturnValue = true;
                    return functionReturnValue;
                }
            }

            if (!string.IsNullOrEmpty(txtDevisEntete14.Text))
            {
                if (!General.IsNumeric(txtDevisEntete14.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La valeur dans la case 'Acompte' doit être de type numérique.",
                        "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    functionReturnValue = true;
                    return functionReturnValue;
                }
            }

            if (Check47.CheckState == CheckState.Checked && !General.IsDate(DTPicker10.Value.ToString()))
            {

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                    "Vous avez cliqué sur la case à cocher 'réceptionné', vous devez saisir une date de clôture.",
                    "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                functionReturnValue = true;
                return functionReturnValue;
            }

            if (Check48.CheckState == CheckState.Checked && DTPicker11.Value != null)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                    "Vous avez cliqué sur la case à cocher 'clôturé', vous devez saisir une date de clôture.",
                    "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                functionReturnValue = true;
                return functionReturnValue;
            }
            return functionReturnValue;

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNodevis"></param>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="sTatut"></param>
        private void fc_MajReception(string sNodevis, string sCodeImmeuble, string sTatut)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_MajReception() - Enter In The Function - sNodevis = {sNodevis} - sTatut = {sTatut}");
            //===> Fin Modif Mondir

            string sSQL = null;
            DataTable rs = default(DataTable);

            try
            {
                if (Check46.CheckState == CheckState.Unchecked && Check47.CheckState == CheckState.Checked)
                {
                    if (sTatut.ToUpper() != cStatutTermine.ToUpper() && sTatut.ToUpper() != sStatutP3.ToUpper())
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Vous ne pouvez pas réceptionner un devis tant qu'il n' pas le statut T (terminée) ou P3.",
                            "Opération anuulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Check47.CheckState = System.Windows.Forms.CheckState.Unchecked;
                        DTPicker10.Text = "";
                        DTPicker10.Checked = false;
                        return;
                    }
                    ModDiv.fc_AlerteMailDevis(sNodevis);
                    txtDevisEntete26.Text = General.fncUserName();
                    //txtDevisEntete(25) = Now
                    Check46.CheckState = System.Windows.Forms.CheckState.Checked;
                }
                else
                {
                    Check46.CheckState = Check47.CheckState;
                    if (Check47.CheckState == 0)
                    {
                        DTPicker10.Text = "";
                        DTPicker10.Checked = false;
                        txtDevisEntete26.Text = "";
                    }
                }

                if ((int)Check49.CheckState == 0 && (int)Check48.CheckState == 1)
                {
                    if (sTatut.ToUpper() != cStatutTermine.ToUpper() && sTatut.ToUpper() != sStatutP3.ToUpper())
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Vous ne pouvez pas clôturer un devis tant qu'il n' pas le statut T (terminée) ou P3.",
                            "Opération anuulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        Check48.CheckState = System.Windows.Forms.CheckState.Unchecked;
                        DTPicker11.Text = "";
                        DTPicker11.Checked = false;
                        return;
                    }
                    ModDiv.fc_AlerteMailClotureDevis(sNodevis, sCodeImmeuble);
                    txtDevisEntete29.Text = General.fncUserName();
                    //txtDevisEntete(25) = Now
                    Check49.CheckState = System.Windows.Forms.CheckState.Checked;
                }
                else
                {
                    Check49.CheckState = Check48.CheckState;
                    if (Check48.CheckState == 0)
                    {
                        DTPicker11.Text = "";
                        txtDevisEntete29.Text = "";
                    }
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_MajReception");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void ValidationCorps()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ValidationCorps() - Enter In The Function");
            //===> Fin Modif Mondir

            int j = 0;
            int x = 0;
            int Ni = 0;
            int Nj = 0;
            int Nk = 0;
            int Nss = 0;


            try
            {
                if (General.sValidCorpsDevisV2 == "1")
                {

                    ValidationCorpsV2();
                    return;
                }

                SSDBGridCorps.UpdateData();

                //===> Mondir le 11.02.2021, moved this commented lines to top, it is not deleting,
                //===> Found this while searching about a solution for https://groupe-dt.mantishub.io/view.php?id=2171
                //===> I replaced it with a query
                //===> What i found is LONG are not using ValidationCorpsV2() to validate, General.sValidCorpsDevisV2 is 0 in there Database
                //----si un corps éxiste déja alors on le suppime.
                //if (adotemp.Rows.Count > 0)
                //{
                //    for (int i = 0; i < adotemp.Rows.Count; i++)
                //    {
                //        adotemp.Rows.RemoveAt(0);
                //    }
                //}
                General.Execute($"DELETE FROM DevisDetail WHERE DevisDetail.NumeroDevis ='{Text11.Text}'");
                //===> Fin Modif Mondir

                adotemp = new DataTable();
                var modAdoadotemp = new ModAdo();

                General.SQL = "SELECT DevisDetail.* From DevisDetail Where DevisDetail.NumeroDevis ='" + Text11.Text +
                              "' order by numeroligne";

                adotemp = modAdoadotemp.fc_OpenRecordSet(General.SQL);

                //----Fournis la table du corps avec les nouveaux enregistrements.
                Ni = 0;
                string ok = null;
                for (i = 0; i <= SSDBGridCorps.Rows.Count - 1; i++)
                {
                    UltraGridRow GridRow = SSDBGridCorps.Rows[i];
                    var NewRow = adotemp.NewRow();
                    NewRow["numerodevis"] = Text11.Text;
                    NewRow["NumeroLigne"] = i;
                    for (j = 0; j <= SSDBGridCorps.DisplayLayout.Bands[0].Columns.Count - 1; j++)
                    {
                        var Col = SSDBGridCorps.DisplayLayout.Bands[0].Columns[j].Key;
                        for (x = 0; x <= adotemp.Columns.Count - 1; x++)
                        {
                            if (adotemp.Columns[x].ColumnName.ToUpper() == Col.ToUpper())
                            {
                                ok = adotemp.Columns[x].ColumnName;
                                if (ok.ToUpper() != "NumeroDevis".ToUpper() && ok.ToUpper() != "NumeroLigne".ToUpper())
                                {
                                    if (string.IsNullOrEmpty(SSDBGridCorps.Rows[i].Cells[j].Text))
                                    {
                                        NewRow[x] = DBNull.Value;
                                    }
                                    else
                                    {
                                        if (SSDBGridCorps.Rows[i].Cells[j].Text.ToUpper() == "True".ToUpper())
                                        {
                                            NewRow[x] = 1;
                                        }
                                        else if (SSDBGridCorps.Rows[i].Cells[j].Text.ToUpper() == "False".ToUpper())
                                        {
                                            NewRow[x] = 0;
                                        }
                                        else
                                        {
                                            NewRow[x] = SSDBGridCorps.Rows[i].Cells[j].Text;
                                        }

                                    }

                                }
                                break;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(GridRow.Cells["CodeFamille"].Text))
                    {
                        Ni = Ni + 1;
                        NewRow["NFamille"] = Ni;
                        NewRow["NumOrdre"] = Ni;
                        Nj = 0;
                        Nk = 0;
                        Nss = 0;
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(GridRow.Cells["CodeSousFamille"].Text))
                        {
                            if (string.IsNullOrEmpty(GridRow.Cells["CodeArticle"].Text) &&
                                !string.IsNullOrEmpty(GridRow.Cells["CodeSousArticle"].Text))
                            {
                                Nss = Nss + 1;
                                NewRow["NFamille"] = Ni;
                                NewRow["NSFamille"] = Nj;
                                NewRow["NArticle"] = Nk;
                                NewRow["Nsousarticle"] = Nss;
                                NewRow["NumOrdre"] = Ni + "." + Nj + "." + Nk + "." + Nss;
                            }
                            else
                            {
                                Nk = Nk + 1;
                                Nss = 0;
                                NewRow["NFamille"] = Ni;
                                NewRow["NSFamille"] = Nj;
                                NewRow["NArticle"] = Nk;
                                NewRow["NumOrdre"] = Ni + "." + Nj + "." + Nk;
                            }
                        }
                        else
                        {
                            Nj = Nj + 1;
                            NewRow["NFamille"] = Ni;
                            NewRow["NSFamille"] = Nj;
                            NewRow["NumOrdre"] = Ni + "." + Nj;
                            Nk = 0;
                        }
                    }
                    adotemp.Rows.Add(NewRow);

                    //===> Mondir le 11.02.2021, Was not saving https://groupe-dt.mantishub.io/view.php?id=2171
                    modAdoadotemp.Update();
                    //===> FIn Modif Mondir
                }

                adotemp = null;
                modAdoadotemp?.Dispose();
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";ValidationCorps");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void ValidationCorpsV2()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ValidationCorpsV2() - Enter In The Function");
            //===> Fin Modif Mondir

            int j = 0;
            int x = 0;
            int Ni = 0;
            int Nj = 0;
            int Nk = 0;
            int Nss = 0;
            SqlConnection adoCorps = default(SqlConnection);
            DataTable adoDev = default(DataTable);
            SqlDataAdapter SDAadoDev;
            SqlCommandBuilder SCBadoDev;
            string sError = null;
            string sLog = null;
            int i = 0;
            string sSQldel;

            adoCorps = new SqlConnection(General.adocnn.ConnectionString);
            adoCorps.Open();


            try
            {
                SSDBGridCorps.UpdateData();
                SSDBGridCorps.UpdateData();
                SSDBGridCorps.UpdateData();

                sLog = General.fncUserName().Replace(" ", "").Replace(".", "").Replace("-", "");

                SqlCommand CMD = new SqlCommand("BEGIN TRANSACTION Trans1" + sLog, adoCorps);
                CMD.ExecuteNonQuery();

                adoDev = new DataTable();

                //Set adoDev.ActiveConnection = adocnn
                //    adoDev.CursorType = adOpenKeyset
                //    adoDev.LockType = adLockOptimistic
                //    adoDev.Properties("Preserve On Commit") = True
                //    adoDev.Properties("Preserve On Abort") = True



                General.SQL = "SELECT * From DevisDetail Where DevisDetail.NumeroDevis ='" + Text11.Text +
                              "'";

                //SDAadoDev = new SqlDataAdapter(General.SQL, adoCorps);
                //SDAadoDev.Fill(adoDev);

                //----si un corps éxiste déja alors on le suppime.
                //Modifier Par Mondir ====================================> Not Working With SqlCommandBuilder
                var xx = General.Execute($"DELETE FROM DevisDetail WHERE DevisDetail.NumeroDevis ='{Text11.Text}'");
                //if (adoDev.Rows.Count > 0)
                //{
                //    for (i = 0; i < adoDev.Rows.Count; i++)
                //    {
                //        adoDev.Rows.RemoveAt(0);
                //    }
                //    SCBadoDev = new SqlCommandBuilder(SDAadoDev);
                //    var xx = SDAadoDev.Update(adoDev);
                //}

                //--
                SDAadoDev = new SqlDataAdapter(General.SQL, adoCorps);
                SDAadoDev.Fill(adoDev);
                if (adoDev.Rows.Count > 0)
                {
                    foreach (DataRow r in adoDev.Rows)
                    {
                        r.Delete();
                    }
                    //'=== trace les suppresions de devis.
                    string strDateImp;
                    if (General.IsDate(txtDateImpression.Text))
                    {
                        strDateImp = txtDateImpression.Text;
                    }
                    else
                    {
                        strDateImp = "NULL";
                    }
                    sSQldel = "INSERT INTO DELETED_CORPS_DEVIS"
                    + "(NumeroDevis, CodeEtat,   DateDeleted, DateImpression, CodeImmeuble, NomUtilisateur)"
                    + " VALUES     ('" + StdSQLchaine.gFr_DoublerQuote(Text11.Text) + "','" + SSetat.Text + "','" + DateTime.Now + "', "
                    + "'" + strDateImp + "'"
                    + ",'" + StdSQLchaine.gFr_DoublerQuote(Text14.Text) + "','" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "')";

                    int yy = General.Execute(sSQldel);
                }

                //----Fournis la table du corps avec les nouveaux enregistrements.
                Ni = 0;
                string ok = null;
                for (i = 0; i <= SSDBGridCorps.Rows.Count - 1; i++)
                {
                    var NewRow = adoDev.NewRow();
                    NewRow["numerodevis"] = Text11.Text;
                    NewRow["NumeroLigne"] = i;

                    sError = "";

                    ///===============> Tested
                    for (j = 0; j <= SSDBGridCorps.DisplayLayout.Bands[0].Columns.Count - 1; j++)

                    {
                        UltraGridColumn Col = SSDBGridCorps.DisplayLayout.Bands[0].Columns[j];
                        for (x = 0; x <= adoDev.Columns.Count - 1; x++)
                        {
                            if (adoDev.Columns[x].ColumnName.ToUpper() == Col.Key.ToUpper())
                            {
                                ok = adoDev.Columns[x].ColumnName;
                                if (ok.ToUpper() != "NumeroDevis".ToUpper() && ok.ToUpper() != "NumeroLigne".ToUpper())
                                {

                                    if (string.IsNullOrEmpty(sError))
                                    {
                                        sError = "N°Ligne " + i + " <==> ";
                                    }
                                    sError = sError + adoDev.Columns[x].ColumnName + " = " + adoDev.Columns[x] +
                                             " <==> ";

                                    if (string.IsNullOrEmpty(SSDBGridCorps.Rows[i].Cells[j].Text))
                                    {
                                        NewRow[x] = DBNull.Value;
                                    }
                                    else
                                    {
                                        if (SSDBGridCorps.Rows[i].Cells[j].Text.ToUpper() == "True".ToUpper())
                                        {
                                            NewRow[x] = 1;
                                        }
                                        else if (SSDBGridCorps.Rows[i].Cells[j].Text.ToUpper() == "False".ToUpper())
                                        {
                                            NewRow[x] = 0;
                                        }
                                        else
                                        {
                                            NewRow[x] = SSDBGridCorps.Rows[i].Cells[j].Text;
                                        }
                                    }

                                }
                                break;
                            }
                        }
                    }

                    if (!string.IsNullOrEmpty(SSDBGridCorps.Rows[i].Cells["CodeFamille"].Text))
                    {
                        Ni = Ni + 1;
                        NewRow["NFamille"] = Ni;
                        NewRow["NumOrdre"] = Ni;
                        Nj = 0;
                        Nk = 0;
                        Nss = 0;
                    }
                    else
                    {
                        ///===========> Tested
                        if (string.IsNullOrEmpty(SSDBGridCorps.Rows[i].Cells["CodeSousFamille"].Text))
                        {
                            if (string.IsNullOrEmpty(SSDBGridCorps.Rows[i].Cells["CodeArticle"].Text) &&
                                !string.IsNullOrEmpty(SSDBGridCorps.Rows[i].Cells["CodeSousArticle"].Text))
                            {
                                Nss = Nss + 1;
                                NewRow["NFamille"] = Ni;
                                NewRow["NSFamille"] = Nj;
                                NewRow["NArticle"] = Nk;
                                NewRow["Nsousarticle"] = Nss;
                                NewRow["NumOrdre"] = Ni + "." + Nj + "." + Nk + "." + Nss;
                            }
                            else
                            {
                                Nk = Nk + 1;
                                Nss = 0;
                                NewRow["NFamille"] = Ni;
                                NewRow["NSFamille"] = Nj;
                                NewRow["NArticle"] = Nk;
                                NewRow["NumOrdre"] = Ni + "." + Nj + "." + Nk;
                            }
                        }
                        else
                        {
                            Nj = Nj + 1;
                            NewRow["NFamille"] = Ni;
                            NewRow["NSFamille"] = Nj;
                            NewRow["NumOrdre"] = Ni + "." + Nj;
                            Nk = 0;
                        }
                    }
                    adoDev.Rows.Add(NewRow);

                    SCBadoDev = new SqlCommandBuilder(SDAadoDev);
                    var xxx = SDAadoDev.Update(adoDev);
                }

                adoDev = null;

                CMD = new SqlCommand("COMMIT TRANSACTION Trans1" + sLog, adoCorps);
                CMD.ExecuteNonQuery();

                adoCorps.Close();
                adoCorps = null;

                fc_HistoDevis(i, "");
            }
            catch (Exception e)
            {
                fc_HistoDevis(i, e.Message + " ==> " + sError);

                Erreurs.gFr_debug(e, this.Name + ";ValidationCorpsV2; " + sError, true);

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                    "Les dernières modifications apportées sur le corps du devis ne seront pas sauvegardées."
                    + "\n\n" + " Une erreur est survenue à la ligne " + i +
                    ", modifiez (ou supprimez) cette ligne avant de valider à" +
                    " nouveau sous peine de perdre vos dernières modifications.", "Erreur", MessageBoxButtons.OK,
                    MessageBoxIcon.Error);


                SqlCommand CMD = new SqlCommand("ROLLBACK TRANSACTION Trans1" + sLog, adoCorps);
                CMD.ExecuteNonQuery();

                if (adoCorps.State == ConnectionState.Open)
                {

                    adoCorps.Close();
                    adoCorps = null;
                }
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lNbLigne"></param>
        /// <param name="sErreur"></param>
        private void fc_HistoDevis(int lNbLigne, string sErreur)
        {
            string sSQL = null;

            try
            {
                sSQL = "DELETE FROM DevisHistoEnTete WHERE ValideLe <='" + DateTime.Now.AddYears(-1).ToString(General.FormatDateSQL) + "'";
                var xx = General.Execute(sSQL);

                sSQL = "INSERT INTO DevisHistoEnTete" +
                       " (NumeroDevis, CodeEtat, DateAcceptation, TotalHT, TotalTVA, TotalTTC, " +
                       " CodeImmeuble, DateImpression, ValidePar, ValideLe," +
                       " NbLigneCorps, ErreurValidation)";
                sSQL = sSQL + " VALUES (" + " '" + StdSQLchaine.gFr_DoublerQuote(Text11.Text) + "'" + " ,'" +
                       StdSQLchaine.gFr_DoublerQuote(SSetat.Text) + "'" + " ," +
                       (DateAcceptation.Value == null || !DateAcceptation.Checked ? "Null" : "'" + DateAcceptation.Value.ToString(General.FormatDateSQL) + "'") + " ," +
                       General.nz(Text39.Text, 0).ToString().Replace(",", ".") + "" + " ," + General.nz(Text321.Text, 0).ToString().Replace(",", ".") + "" + " ," +
                       General.nz(Text323.Text, 0).ToString().Replace(",", ".") + "" + " ,'" + StdSQLchaine.gFr_DoublerQuote(Text14.Text) + "'" +
                       " ," + (string.IsNullOrEmpty(txtDateImpression.Text)
                           ? "Null"
                           : "'" +
                             Convert.ToDateTime(txtDateImpression.Text).ToString(General.FormatDateSQL) + "'") + "" + " ,'" +
                       StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'" + " ,'" + DateTime.Now.ToString(General.FormatDateSQL) + "'" + " ," +
                       lNbLigne + "" + " ,'" +
                       StdSQLchaine.gFr_DoublerQuote(sErreur) + "'" + ")";

                xx = General.Execute(sSQL);
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_HistoDevis");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strChoix"></param>
        private void afficheMessage(string strChoix)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"afficheMessage() - Enter In The Function - strChoix = {strChoix}");
            //===> Fin Modif Mondir

            try
            {
                switch (strChoix)
                {
                    case "1":
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique", "Données erronées",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "2":
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Attention cet immeuble n'a pas de code TVA, allez dans la fiche immeuble pour le fournir",
                            "Code TVA manquant", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "3":
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Une erreur est survenue dans le corps du devis, la validation est annulée, Veuilez resaisir correctement le corps",
                            "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "4":
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "La ligne n° " + SSDBGridCorps.ActiveRow.Cells["NumeroLigne"].Value +
                            " dans le corps du devis n' a pas de taux de TVA", "Données éronnées", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        break;
                    case "5":
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Des erreurs se sont produites dans le corps de factures, la validation est annulée, verifiez le corps et validez de nouveau.",
                            "Données éronnées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "6":
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez insérer un taux de tva ", "Données éronnées", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        break;
                    case "7":
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                            "Une erreur est survenue dans le corps du devis, Veuilez resaisir correctement le corps pour pouvoir vous rendre vers les modéles de devis",
                            "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "8":
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Opération annulée, aucun devis en cours", "Opération annulée",
                            MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                    case "9":
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez insérer un taux de tva.", "Opération annulée", MessageBoxButtons.OK,
                            MessageBoxIcon.Error);
                        break;
                    case "10":
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pour créer une demande de prix, vous devez sélectionner des sousAricles",
                            "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        break;
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";afficheMessage");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnFacture_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"btnFacture_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            fc_FactureVisu();
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_FactureVisu()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_FactureVisu() - Enter In The Function");
            //===> Fin Modif Mondir

            string sCrystalOld = null;
            string sCrystal = null;

            try
            {
                if (tSqlCrystal == null)
                    return;
                i = tSqlCrystal == null ? 0 : tSqlCrystal.Length;


                modAdoadotemp = new ModAdo();

                for (i = 0; i <= tSqlCrystal.Length - 1; i++)
                {

                    adotemp = modAdoadotemp.fc_OpenRecordSet("SELECT DateFacture" + " From FactureManuelleEntete" +
                                                             " WHERE NoFacture ='" + tSqlCrystal[i] + "'");
                    if (Convert.ToDateTime(adotemp.Rows[0]["DateFacture"]) < Convert.ToDateTime("15/06/2001"))
                    {
                        if (string.IsNullOrEmpty(sCrystalOld))
                        {
                            sCrystalOld = "{FactureManuelleEntete.NoFacture}='" + tSqlCrystal[i] + "'";
                        }
                        else
                        {
                            sCrystalOld = sCrystalOld + " or {FactureManuelleEntete.NoFacture}='" + tSqlCrystal[i] +
                                          "'";
                        }

                    }
                    else
                    {
                        ///==============> Tested
                        if (string.IsNullOrEmpty(sCrystal))
                        {
                            sCrystal = "{FactureManuelleEntete.NoFacture}='" + tSqlCrystal[i] + "'";
                        }
                        else
                        {
                            sCrystal = sCrystal + " or {FactureManuelleEntete.NoFacture}='" + tSqlCrystal[i] + "'";
                        }
                    }
                    modAdoadotemp?.Dispose();
                }

                adotemp = null;

                if (!string.IsNullOrEmpty(sCrystalOld))
                {

                    ReportDocument crfact = new ReportDocument();
                    string SelectionFormula = "";
                    crfact.Load(General.ETATOLDFACTUREMANUEL);
                    SelectionFormula = sCrystalOld;
                    CrystalReportFormView ReportForm = new CrystalReportFormView(crfact, SelectionFormula);
                    ReportForm.Show();
                }

                ////================> Tested
                if (!string.IsNullOrEmpty(sCrystal))
                {

                    ReportDocument crfact = new ReportDocument();
                    string SelectionFormula = "";
                    crfact.Load(General.ETATNEWFACTUREMANUEL);
                    SelectionFormula = sCrystal;
                    CrystalReportFormView ReportForm = new CrystalReportFormView(crfact, SelectionFormula);
                    ReportForm.Show();

                }

            }
            catch (OutOfMemoryException ex)
            {
                Program.SaveException(ex);

                //== indice en dehors de la plage.
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("il n'y a pas de facture associée a cette intervention",
                    "Facture non trouvée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_FactureVisu");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Check44_CheckedChanged(object sender, EventArgs e)
        {
            string sSQL;
            var CheckBox = sender as CheckBox;
            int Index = Convert.ToInt32(CheckBox.Tag);

            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Check44_CheckedChanged() - Enter In The Function - Index = {Index}");
            //===> Fin Modif Mondir

            switch (Index)
            {
                case 7:
                    if ((int)Check47.CheckState == 0)
                    {
                        DTPicker10.Text = "";
                        DTPicker10.Checked = false;
                        txtDevisEntete26.Text = "";
                    }
                    else if ((int)Check47.CheckState == 1)
                    {
                        DTPicker10.Text = DateTime.Now.ToString();
                        DTPicker10.Checked = true;
                    }
                    break;

                case 8:
                    if ((int)Check48.CheckState == 0)
                    {
                        DTPicker11.Text = "";
                        DTPicker11.Checked = false;
                        txtDevisEntete29.Text = "";
                    }
                    else if ((int)Check48.CheckState == 1)
                    {
                        DTPicker11.Text = DateTime.Now.ToString();
                    }
                    break;

                case 10:
                    sSQL = "UPDATE  DevisEnTete Set AffichePart = " + (int)Check410.CheckState + " WHERE     (NumeroDevis = N'" + StdSQLchaine.gFr_DoublerQuote(Text11.Text) + "')";
                    General.Execute(sSQL);
                    break;
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void chkFO_CheckStateChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"chkFO_CheckStateChanged() - Enter In The Function - chkFO.Checked = {chkFO.Checked}");
            //===> Fin Modif Mondir

            fc_ChekCoef();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void chkMO_CheckStateChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"chkMO_CheckStateChanged() - Enter In The Function - chkMO.Checked = {chkMO.Checked}");
            //===> Fin Modif Mondir

            fc_ChekCoef();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void chkPrixH_CheckStateChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_VerifDevis() - Enter In The Function");
            //===> Fin Modif Mondir

            fc_ChekCoef();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void chkST_CheckStateChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"chkST_CheckStateChanged() - Enter In The Function - chkST.Checked = {chkST.Checked}");
            //===> Fin Modif Mondir

            fc_ChekCoef();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void chkTVA_CheckStateChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"chkTVA_CheckStateChanged() - Enter In The Function - chkTVA.Checked = {chkTVA.Checked}");
            //===> Fin Modif Mondir

            fc_ChekCoef();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="bInit"></param>
        private void fc_ChekCoef(bool bInit = false)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ChekCoef() - Enter In The Function - bInit = {bInit}");
            //===> Fin Modif Mondir

            //== init en noir
            chkMO.ForeColor = System.Drawing.Color.Black;
            chkFO.ForeColor = System.Drawing.Color.Black;
            chkST.ForeColor = System.Drawing.Color.Black;
            chkTVA.ForeColor = System.Drawing.Color.Black;
            chkPrixH.ForeColor = System.Drawing.Color.Black;

            ///===========> Tested
            if (bInit == true)
            {
                //== init à 0
                chkMO.CheckState = System.Windows.Forms.CheckState.Unchecked;
                chkFO.CheckState = System.Windows.Forms.CheckState.Unchecked;
                chkST.CheckState = System.Windows.Forms.CheckState.Unchecked;
                chkTVA.CheckState = System.Windows.Forms.CheckState.Unchecked;
                chkPrixH.CheckState = System.Windows.Forms.CheckState.Unchecked;

            }
            else
            {
                ////=====> Tested
                if ((int)chkMO.CheckState == 1)
                {
                    chkMO.Font = new Font(chkMO.Font.Name, chkMO.Font.Size, FontStyle.Underline);
                    chkMO.ForeColor = System.Drawing.Color.Blue;
                }
                else
                {
                    chkMO.Font = new Font(chkMO.Font.Name, chkMO.Font.Size, FontStyle.Regular);
                }
                ////=====> Tested
                if ((int)chkFO.CheckState == 1)
                {
                    chkFO.Font = new Font(chkFO.Font.Name, chkFO.Font.Size, FontStyle.Underline);
                    chkFO.ForeColor = System.Drawing.Color.Blue;
                }
                else
                {
                    chkFO.Font = new Font(chkFO.Font.Name, chkFO.Font.Size, FontStyle.Regular);
                }

                ////=====> Tested
                if ((int)chkST.CheckState == 1)
                {
                    chkST.Font = new Font(chkST.Font.Name, chkST.Font.Size, FontStyle.Underline);
                    chkST.ForeColor = System.Drawing.Color.Blue;
                }
                else
                {
                    chkST.Font = new Font(chkST.Font.Name, chkST.Font.Size, FontStyle.Regular);
                }

                ////=====> Tested
                if ((int)chkTVA.CheckState == 1)
                {
                    chkTVA.Font = new Font(chkTVA.Font.Name, chkTVA.Font.Size, FontStyle.Underline);
                    chkTVA.ForeColor = System.Drawing.Color.Blue;
                }
                else
                {
                    chkTVA.Font = new Font(chkTVA.Font.Name, chkTVA.Font.Size, FontStyle.Underline);
                }

                if ((int)chkPrixH.CheckState == 1)
                {
                    chkPrixH.Font = new Font(chkPrixH.Font.Name, chkPrixH.Font.Size, FontStyle.Underline);
                    chkPrixH.ForeColor = System.Drawing.Color.Blue;
                }
                ////=====> Tested
                else
                {
                    chkPrixH.Font = new Font(chkPrixH.Font.Name, chkPrixH.Font.Size, FontStyle.Regular);
                }

            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbBe_AfterCloseUp(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmbBe_AfterCloseUp() - Enter In The Function");
            //===> Fin Modif Mondir

            using (var tmpModAdo = new ModAdo())
                lblLibBE.Text = tmpModAdo.fc_ADOlibelle("SELECT Libelle FROM Be WHERE codeBe=" + General.nz(cmbBe.Text, 0));
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbBe_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmbBe_BeforeDropDown() - Enter In The Function");
            //===> Fin Modif Mondir

            General.sSQL = "SELECT CodeBE,Libelle " + " from BE ";
            // If cmbBE.Rows = 0 Then
            sheridan.InitialiseCombo(cmbBe, General.sSQL, "CodeBE", false);
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbBe_Validated(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmbBe_Validated() - Enter In The Function");
            //===> Fin Modif Mondir

            using (var tmpModAdo = new ModAdo())
                lblLibBE.Text = tmpModAdo.fc_ADOlibelle("SELECT Libelle FROM Be WHERE codeBe=" + General.nz(cmbBe.Text, 0));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbUnite_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmbUnite_BeforeDropDown() - Enter In The Function");
            //===> Fin Modif Mondir

            General.sSQL = "SELECT QTE_Unite.QTE_Libelle AS Unite " + " FROM QTE_Unite ORDER BY QTE_Unite.QTE_Libelle";
            if (cmbUnite.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(cmbUnite, General.sSQL, "Unite", true);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbUniteFournit_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmbUniteFournit_BeforeDropDown() - Enter In The Function");
            //===> Fin Modif Mondir

            if (General.sGecetV3 == "1")
            {
                General.sSQL = "SELECT     CODE_UNITE, DESIGNATION" + " From Unite ORDER BY CODE_UNITE";
            }
            else
            {
                General.sSQL = "SELECT DISTINCT Dtiprix.fouunite AS [Unité] " + " FROM Dtiprix ORDER BY Dtiprix.fouunite";

            }
            if (cmbUniteFournit.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(cmbUniteFournit, General.sSQL, "Unité", true, ModParametre.adoGecet);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbUniteSousArticle_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmbUniteSousArticle_BeforeDropDown() - Enter In The Function");
            //===> Fin Modif Mondir

            if (General.sGecetV3 == "1")
            {
                General.sSQL = "SELECT     CODE_UNITE, DESIGNATION" + " From Unite ORDER BY CODE_UNITE";
            }
            else
            {
                General.sSQL = "SELECT DISTINCT Dtiprix.fouunite AS [Unité] " + " FROM Dtiprix ORDER BY Dtiprix.fouunite";

            }
            if (cmbUniteSousArticle.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(cmbUniteSousArticle, General.sSQL, "Unité", true, ModParametre.adoGecet);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAction0_Click(object sender, EventArgs e)
        {
            var Button = sender as Button;
            int Index = Convert.ToInt32(Button.Tag);

            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAction0_Click() - Enter In The Function - Index = {Index}");
            //===> Fin Modif Mondir

            string sTypeAffichage = null;
            string sSQL = null;
            int i = 0;
            Word.Application oWord = null;
            int lOrigTop = 0;
            bool bInstanceWord = false;


            try
            {
                ///========================> Tested
                if (Index == 0 || Index == 1)
                {

                    if (fc_CtrlErrAvanc() == true)
                    {
                        return;
                    }

                    fc_MajReception(Text11.Text, Text14.Text, SSetat.Text);

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    //== Validation Devis
                    fc_ValideAvance(Text11.Text);

                    switch (Index)
                    {
                        case 0:
                            //== Export excel
                            fc_exportExcel();
                            break;



                    }

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                }
                ///================> Tested ===> Control Is Always Hidden
                else if (Index == 2)
                {


                    if (string.IsNullOrEmpty(Text11.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun devis en cours, vous devez vous positionner sur un devis.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }

                    //=== si le statut est en 04 ou A, interdire le mode d'affichage.
                    //    If SSetat = "04" Or SSetat = "A" Then
                    //        MsgBox "Le statut devis du devis est en " & SSetat _
                    //'            & ", vous ne pouvez pas changer le type d'affichage.", vbInformation, "Opération annulée"
                    //
                    //        Exit Sub
                    //    End If


                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention vous voulez changer le type d'affichage du devis en cours," + " vous risquez de perdre les données saisies dans le corps du devis.\n Voulez vous continuer ?", "Type d'affichage",
                        MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {

                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez vous confirmer le changement du type d'affichage (rappel : les données saisies dans" + " le corps du devis seront perdu).\n Voulez vous continuer ?",
                            "Type d'affichage", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {

                            return;
                        }
                        return;
                    }

                    sSQL = "UPDATE DevisEnTete set TypeDevis = 0 WHERE NumeroDevis ='" + Text11.Text + "'";

                    var xx = General.Execute(sSQL);

                    //===stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, Text11.Text);


                    //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGDEVIS);

                }
                else if (Index == 4)
                {
                    try
                    {
                        //Use a running word instance if possible
                        oWord = new Word.Application();

                        //Otherwise use a new instance
                        ////=======> Tested
                        if (oWord == null)
                        {
                            bInstanceWord = false;
                            oWord = oWord = new Word.Application();


                            if (oWord == null)
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Opération annulé, Word n'est pas installé dans votre systéme.", "Word inexistant", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }
                        }
                        else
                        {
                            bInstanceWord = true;
                        }


                        lOrigTop = oWord.Top;
                        //oWord.Visible = False

                        ///==============> Tested
                        if (SSTab1.SelectedTab.Index == 1 || SSTab1.SelectedTab.Index == 3)
                        {
                            if (SSDBGridCorps.Rows.Count > 0)
                            {
                                for (i = 0; i <= SSDBGridCorps.Rows.Count - 1; i++)
                                {
                                    var value = Correcteur.WSpellCheck(SSDBGridCorps.Rows[i].Cells["TexteLigne"].Text, true, ref oWord);
                                    if (!string.IsNullOrEmpty(value))
                                        SSDBGridCorps.Rows[i].Cells["TexteLigne"].Value = value;
                                    SSDBGridCorps.Rows[i].Update();
                                }
                            }
                        }

                        if (SSTab1.SelectedTab.Index == 3)
                        {
                            txtTitreDevis.Text = Correcteur.WSpellCheck(txtTitreDevis.Text, true, ref oWord);

                            SSOleDBCombo2.Text = Correcteur.WSpellCheck(SSOleDBCombo2.Text, true, ref oWord);

                            // txtRmq.Text = WSpellCheck(txtRmq.Text, True, oWord)

                            SSOleDBCombo1Bis.Text = Correcteur.WSpellCheck(SSOleDBCombo1Bis.Text, true, ref oWord);
                        }

                        //If sstab1.Tab = 8 Or sstab1.Tab = 3 Then
                        //    If GridOptions.Rows > 0 Then
                        //        GridOptions.MoveFirst
                        //        For i = 0 To GridOptions.Rows - 1
                        //            GridOptions.Columns("TexteLigne").Text = WSpellCheck(GridOptions.Columns("TexteLigne").Text, True, oWord)
                        //            GridOptions.MoveNext
                        //        Next
                        //    End If
                        //End If
                        //Debug.Print(bInstanceWord);
                        //if (bInstanceWord == false)
                        //{

                        oWord.Top = lOrigTop;
                        oWord.Quit();
                        oWord = null;
                        //}
                    }
                    catch (Exception exception)
                    {
                        Program.SaveException(exception);
                        throw;
                    }


                }
                /// Mohammed 24.06.2020 added rachid's Modifs
                else if (Index == 5)
                {
                    General.fc_CreateFact(true, true, "", Text11.Text, Text14.Text, this, 0, General.fc_GetNumStandardFromDevis(Text11.Text));
                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";cmdAction_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNodevis"></param>
        private void fc_ValideAvance(string sNodevis)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ValideAvance() - Enter In The Function - sNodevis = {sNodevis}");
            //===> Fin Modif Mondir

            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = new ModAdo();
            try
            {
                if (string.IsNullOrEmpty(sNodevis))
                {
                    return;
                }



                sSQL = "SELECT  NumeroDevis, AvctChantier, AvctFacturation, AvctReglement, " +
                    " ConditionPaiement, Situation, Acompte, DelaisReglmt, Receptionne, ComSurRecept, ClotureDevis, ComSurCloture, DateReception, ReceptionnePar, ClotureLe "
                    + " , CLoturerPar, CodeReglement, AcompteRecuLe"
                    + " FROM DevisEnTete " + " WHERE NumeroDevis ='" + StdSQLchaine.gFr_DoublerQuote(sNodevis) + "'";

                rs = modAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {
                    rs.Rows[0]["AvctChantier"] = General.nz(txtDevisEntete9.Text, System.DBNull.Value);
                    rs.Rows[0]["AvctFacturation"] = General.nz(txtDevisEntete10.Text, System.DBNull.Value);
                    rs.Rows[0]["AvctReglement"] = General.nz(txtDevisEntete11.Text, System.DBNull.Value);
                    rs.Rows[0]["ConditionPaiement"] = General.nz(txtDevisEntete12.Text, System.DBNull.Value);
                    rs.Rows[0]["Situation"] = General.nz(txtDevisEntete13.Text, System.DBNull.Value);
                    rs.Rows[0]["Acompte"] = General.nz(txtDevisEntete14.Text, System.DBNull.Value);
                    rs.Rows[0]["DelaisReglmt"] = General.nz(txtDevisEntete15.Text, System.DBNull.Value);
                    rs.Rows[0]["ComSurRecept"] = General.nz(txtDevisEntete23.Text, System.DBNull.Value);
                    rs.Rows[0]["ComSurCloture"] = General.nz(txtDevisEntete24.Text, System.DBNull.Value);
                    rs.Rows[0]["Receptionne"] = General.nz(Check47.CheckState == CheckState.Checked ? 1 : 0, 0);
                    rs.Rows[0]["ClotureDevis"] = General.nz(Check48.CheckState == CheckState.Checked ? 1 : 0, 0);
                    if (General.IsDate(DTPicker10.Text) && DTPicker10.Checked)
                    {
                        rs.Rows[0]["DateReception"] = DTPicker10.Text;
                    }
                    else
                    {
                        rs.Rows[0]["DateReception"] = System.DBNull.Value;
                    }

                    ///================> Ajouté par Mondir : le test sans DTPicker11.Checked est toujours vrai
                    if (General.IsDate(DTPicker11.Text) && DTPicker11.Checked)
                    {
                        rs.Rows[0]["ClotureLe"] = DTPicker11.Text;
                    }
                    else
                    {
                        rs.Rows[0]["ClotureLe"] = System.DBNull.Value;
                    }

                    rs.Rows[0]["ReceptionnePar"] = General.nz(txtDevisEntete26.Text, System.DBNull.Value);

                    //=== modif du 23 07 2019.
                    rs.Rows[0]["CLoturerPar"] = General.nz(txtDevisEntete29.Text, System.DBNull.Value);
                    rs.Rows[0]["CodeReglement"] = General.nz(txtDevisEntete28.Text, System.DBNull.Value);

                    ///================> Ajouté par Mondir : le test sans DTPicker10.Checked est toujours vrai
                    if (General.IsDate(DTPicker10.Value.ToString()) && DTPicker10.Checked)
                        rs.Rows[0]["AcompteRecuLe"] = DTPicker12.Value;
                    else
                        rs.Rows[0]["AcompteRecuLe"] = DBNull.Value;
                    //=== fin modif du 23 07 2019.

                }
                var xx = modAdors.Update();
                modAdors?.Dispose();
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_ValideAvance");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_exportExcel()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_exportExcel() - Enter In The Function");
            //===> Fin Modif Mondir

            Excel.Application oXL;
            Excel.Workbook oWB;
            Excel.Worksheet oSheet;
            Excel.Range oRng;
            Excel.Range oResizeRange;

            int i = 0;
            int j = 0;
            string sMatricule = null;
            int lLigne = 0;


            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                //=== Start Excel and get Application object.
                oXL = new Excel.Application();
                oXL.Visible = false;

                //=== Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;

                i = 1;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                // Starting at E1, fill headers for the number of columns selected.
                //Set oResizeRange = oSheet.Range( "A1",  "B1").(ColumnSize:=i - 1)
                //oResizeRange.Interior.ColorIndex = 15
                //formate la ligne en gras.
                //oResizeRange.Font.Bold = True

                //Set oResizeRange = oSheet.Range("A1", "B1").Resize(ColumnSize:=i - 1)
                //oResizeRange.Borders.Weight = xlThin

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                lLigne = 2;
                j = 1;

                oSheet.Cells[lLigne, 1].value = "Immeuble";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete0.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "N° Devis";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete1.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Montant  HT";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete2.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Fourniture HT";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete3.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Nbre heure d'équipe";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete4.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Nbre heure simple";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete5.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Coefficient Devis";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete6.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Remise accordée";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete7.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Coefficient Réalisé (Devis)";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete8.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Commercial";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete21.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Responsable chantier";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete22.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Avancement chantier";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete9.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Avancement facturation";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete10.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Avancement réglement";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete11.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Condition de paiement";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete12.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Situation";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete13.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Acompte";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete14.Text;
                lLigne = lLigne + 1;

                //=== modif du 23 07 2019.
                oSheet.Cells[lLigne, 1].value = "Acompte reçu le";
                oSheet.Cells[lLigne, 2].value = DTPicker12.Value;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Mode de réglement";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete28.Text;
                lLigne = lLigne + 1;

                //=== fin modif du 23 07 2019.

                oSheet.Cells[lLigne, 1].value = "Délais de réglement";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete15.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Bon de commande";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete16.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Achat réel";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete17.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Nbre heure réel";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete18.Text;
                lLigne = lLigne + 1;

                oSheet.Cells[lLigne, 1].value = "Facturation";
                oSheet.Cells[lLigne, 2].value = txtDevisEntete19.Text;
                lLigne = lLigne + 1;

                i = 2;

                oResizeRange = oSheet.Range["A1", "A" + lLigne].Resize[ColumnSize: i - 1];
                oResizeRange.Columns.AutoFit();
                oResizeRange.Font.Bold = true;
                oResizeRange = null;

                oResizeRange = oSheet.Range["B1", "B" + lLigne].Resize[ColumnSize: i - 1];
                oResizeRange.Columns.AutoFit();
                oResizeRange = null;

                //oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
                //formate la ligne en gras.
                //oResizeRange.Font.Bold = True

                oXL.Visible = true;
                oXL.UserControl = true;
                oRng = null;
                oSheet = null;
                oWB = null;
                oXL = null;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            }
            catch (Exception e)
            {
                Program.SaveException(e);

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message, "Error: " + e.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFax_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdFax_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            double dbTauxTva = 0;

            try
            {
                bool EnvoieMail = false;

                //=== date de création, modif du 1 1 2012.
                if (General.IsDate(Text122.Text))
                {
                    if (Convert.ToDateTime(Text122.Text) >= Convert.ToDateTime("01/01/2014"))
                    {
                        dbTauxTva = 10;

                    }
                    else if (Convert.ToDateTime(Text122.Text) > Convert.ToDateTime("01/01/2012"))
                    {
                        dbTauxTva = 7;

                    }
                    else if (General.IsNumeric(txtTaux.Text))
                    {
                        dbTauxTva = Convert.ToDouble(txtTaux.Text);
                    }
                    else
                    {
                        dbTauxTva = 5.5;

                    }
                }

                var xx = General.Execute("UPDATE DevisDetail SET CodeTva =" + dbTauxTva.ToString().Replace(",", ".") + " WHERE NumeroDevis ='" + Text11.Text + "'" + " and (codetva is null  or codetva = 0)");


                //adocnn.Execute "UPDATE DevisDetail SET CodeTva = 5.5 WHERE NumeroDevis ='" & Text1(1) & "'" _
                //& " and (codetva is null  or codetva = 0)"


                if (OptDebourse.Checked == true)
                {
                    prcImpression(General.strDTDebourse, 0, Convert.ToInt16(Text4.Text));
                }
                else if (OptDebourseAff.Checked == true)
                {
                    prcImpression(General.strDTDebourseAffaire, 1, Convert.ToInt16(Text4.Text), true);
                }
                else if (OPtRecap.Checked == true)
                {
                    prcImpression(General.strDTDevisRecap, 1, Convert.ToInt16(Text4.Text), true);
                }
                else
                {
                    General.tabParametresDevis = new General.ParamDevis[3];
                    //        prcImpression strDTDevis, 0, CInt(Text4)
                    if (OptDescriptif.Checked == true)
                    {
                        General.tabParametresDevis = new General.ParamDevis[3];
                        General.tabParametresDevis[0].sName = "Parametre";
                        General.tabParametresDevis[0].sValue = "Descriptif";
                        General.tabParametresDevis[1].sName = "MasqueSomme ";
                        General.tabParametresDevis[1].sValue = "M";
                    }
                    else if (optForfaitaireAff.Checked == true)
                    {
                        General.tabParametresDevis[0].sName = "Parametre";
                        General.tabParametresDevis[0].sValue = "";
                        General.tabParametresDevis[1].sName = "MasqueSomme ";
                        General.tabParametresDevis[1].sValue = "M";

                    }
                    else if (optForfaitairePos.Checked == true)
                    {
                        General.tabParametresDevis[0].sName = "Parametre";
                        General.tabParametresDevis[0].sValue = "";
                        General.tabParametresDevis[1].sName = "MasqueSomme ";
                        General.tabParametresDevis[1].sValue = "";

                    }
                    else if (optQuantitatif.Checked == true)
                    {
                        General.tabParametresDevis[0].sName = "Parametre";
                        General.tabParametresDevis[0].sValue = "";
                        General.tabParametresDevis[1].sName = "MasqueSomme ";
                        General.tabParametresDevis[1].sValue = "Q";

                    }
                    fc_FaxDevisCr9(General.gsRpt + "NewDtDevisSQL.rpt", Text11.Text, SAGE.SuppCar(txtFax.Text, " "), txtDestinataire.Text);
                    //fc_ShowDevis Text1(1).Text


                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";cmdVisu_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sFileReportV6"></param>
        /// <param name="sNodevis"></param>
        /// <param name="lNumber"></param>
        /// <param name="sdest"></param>
        private void fc_FaxDevisCr9(string sFileReportV6, string sNodevis, string lNumber, string sdest)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_FaxDevisCr9() - Enter In The Function - sFileReportV6 = {sFileReportV6} - sNodevis = {sNodevis} - sdest = {sdest}");
            //===> Fin Modif Mondir

            object RetVal = null;
            string sName = null;
            string sport = null;
            string sDriver = null;
            string MyValue = null;
            string Message = null;
            int i = 0;
            ReportDocument Report = null;

            try
            {
                Cursor.Current = Cursors.WaitCursor;

                Message = "Voulez vous envoyer un fax chez " + sdest + " au numéro inscrit ci-dessous ?";

                MyValue = Microsoft.VisualBasic.Interaction.InputBox(Message, "Envoyer un fax", lNumber);

                if (string.IsNullOrEmpty(MyValue))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Opération annulée", "Envoi de fax", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                //== procedure de controle de numéro de fax

                lNumber = MyValue;

                Report = new ReportDocument();
                Report.Load(sFileReportV6);

                for (i = 0; i <= General.tabParametresDevis.Length - 1; i++)
                {
                    if (General.tabParametresDevis[i].sName != null && General.tabParametresDevis[i].sValue != null)
                        Report.DataDefinition.FormulaFields[General.tabParametresDevis[i].sName].Text = "'" + General.tabParametresDevis[i].sValue + "'";
                }

                //== renseigne le numero et le nom du destinataire.
                Report.DataDefinition.FormulaFields["faxmaker"].Text = "'" + General.FAXMAKER_KEY + lNumber + "'";
                Report.DataDefinition.FormulaFields["destinataire"].Text = "'" + General.FAXMAKER_KEYDEST + sdest + "'";


                //Report.DataDefinition.ParameterFields[1].ApplyDefaultValues(new ParameterValues(new List<string>() { sNodevis }));
                Report.SetParameterValue("NoDevis", sNodevis);

                foreach (string x in System.Drawing.Printing.PrinterSettings.InstalledPrinters)
                {
                    if (x.Contains(General.NAMEFAXMAKER))
                    {
                        sName = x;
                        //sport = x.Port;
                        //sDriver = x.DriverName;
                        break;
                    }
                }

                Report.PrintOptions.PrinterName = sName;
                Report.PrintToPrinter(1, false, 0, 0);

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Fax envoyé. Consulter ultérieurement votre boite de messagerie," + "vous recevrez  un Email indiquant l'état d'envoi du fax.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);

                Cursor.Current = Cursors.Default;

                //    CRViewer91.EnableAnimationCtrl = False
                //    CRViewer91.EnableCloseButton = True
                //    CRViewer91.EnableDrillDown = True
                //    CRViewer91.EnableExportButton = True
                //    CRViewer91.EnableGroupTree = False
                //    CRViewer91.EnableHelpButton = False
                //    CRViewer91.EnableNavigationControls = True
                //    CRViewer91.EnablePopupMenu = True
                //    CRViewer91.EnablePrintButton = True
                //    CRViewer91.EnableProgressControl = False
                //    CRViewer91.EnableRefreshButton = False
                //    CRViewer91.EnableSearchControl = True
                //    CRViewer91.EnableSearchExpertButton = False
                //    CRViewer91.EnableSelectExpertButton = False
                //    CRViewer91.EnableStopButton = True
                //    CRViewer91.EnableToolbar = True
                //    CRViewer91.EnableZoomControl = True
                //
                //
                //    CRViewer91.ReportSource = Report
                //    CRViewer91.ViewReport
                //    'CRViewer91.PrintReport

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, "fc_PrintDevisCr9");
            }
        }

        /// <summary>
        /// Testeds
        /// </summary>
        /// <param name="sReport"></param>
        /// <param name="iDestination"></param>
        /// <param name="iNombre"></param>
        /// <param name="bFax"></param>
        /// <param name="dtDateCreationDev"></param>
        public void prcImpression(string sReport, short iDestination, short iNombre, bool bFax = false, System.DateTime? dtDateCreationDev = null)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"prcImpression() - Enter In The Function - sReport = {sReport} " +
                                        $"- iDestination = {iDestination} - iNombre = {iNombre} - bFax = {bFax} - dtDateCreationDev = {dtDateCreationDev}");
            //===> Fin Modif Mondir

            int result = 0;
            double x = 0;
            double Y = 0;
            double Sum1 = 0;
            double Sum2 = 0;
            //Dim somme As curenncy
            double Tot1 = 0;
            double Tot2 = 0;
            int i = 0;
            short k = 0;
            string j = null;
            Microsoft.Office.Interop.Outlook.MailItem objNewMail = default(Microsoft.Office.Interop.Outlook.MailItem);
            short NoConnection = 0;

            // Initialisation de la propriété "Destination" à 0 (ouverture fenêtre aperçu)
            ReportDocument CR1 = new ReportDocument();
            ReportDocument CR2 = new ReportDocument();
            ReportDocument CR3 = new ReportDocument();
            CrystalReportFormView CrystalForm;
            string SelectionFormulaCR1 = "";
            string SelectionFormulaCR2 = "";
            string SelectionFormulaCR3 = "";
            string WindowTitle = "";

            // Permettre la sélection de l'imprimante dans l'aperçu
            //CR1.WindowShowPrintSetupBtn = true;
            //CR2.WindowShowPrintSetupBtn = true;
            //CR3.WindowShowPrintSetupBtn = true;


            //    NoConnection = CR1.LogOnServer("PDSODBC.DLL", "LUDOVIC", "DELOSTAL", "Ludovic", "max")

            // Blocage sur le type de règlement si celui-ci n'est pas renseigné
            if (string.IsNullOrEmpty(SSOleDBCombo1Bis.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Type de reglement obligatoire");
                //SSOleDBCombo1.SetFocus
                return;
            }
            //  .LogOnServer "pdsodbc.dll", "DTMDB", "", "admin", ""

            try
            {
                // Stockage du type d'état dans les objets Crystal Report
                ///===========================> Tested
                if (OPtRecap.Checked)
                {
                    // If IsDate(sDateDevisV2) Then
                    //         If CDate(sDateDevisV2) <= dtDateCreationDev Then
                    //                 CR2.ReportFileName = gsRpt & strDTDevisRecapV2
                    //         Else
                    //                 CR2.ReportFileName = gsRpt & strDTDevisRecap
                    //         End If
                    // Else
                    CR2.Load(General.gsRpt + General.strDTDevisRecap);
                    // End If
                }
                else if (OptDebourse.Checked)
                {
                    CR3.Load(General.gsRpt + General.strDTDebourse);
                }
                else if (OptDebourseAff.Checked)
                {
                    CR3.Load(General.gsRpt + General.strDTDebourseAffaire);
                }
                else if (string.IsNullOrEmpty(CR1.FileName))
                {
                    CR1.Load(General.gsRpt + General.strDTDevis);
                }


                // Sélection du devis à afficher dans l'état sélectionné
                //=======================> Tested
                if (OPtRecap.Checked)
                {
                    SelectionFormulaCR2 = "{DevisEntete.NumeroDevis}='" + Text11.Text + "'";
                }
                else if (OptDebourse.Checked || OptDebourseAff.Checked)
                {
                    SelectionFormulaCR3 = "{DevisEntete.NumeroDevis}='" + Text11.Text + "'";
                }
                else
                {
                    SelectionFormulaCR1 = "{DevisEntete.NumeroDevis}='" + Text11.Text + "'";
                }

                if (!string.IsNullOrEmpty(CR1.FileName))
                {
                    CR1.DataDefinition.FormulaFields[1].Text = "";
                    CR1.DataDefinition.FormulaFields[2].Text = "";
                }

                if (OptDescriptif.Checked)
                {
                    WindowTitle = "Aperçu descriptif du devis : " + Text11.Text;

                    CR1.DataDefinition.FormulaFields[1].Text = "Parametre='Descriptif'";

                    //Affiche "M" dans le champ de formule de crystal report ce qui
                    //permet de masquer le champ de sous-total (texte et numérique)

                    CR1.DataDefinition.FormulaFields[2].Text = "MasqueSomme ='M'";
                }
                else if (optForfaitairePos.Checked)
                {
                    WindowTitle = "Aperçu forfaitaire par postition du devis : " + Text11.Text;

                    CR1.DataDefinition.FormulaFields[1].Text = "Parametre=''";

                    CR1.DataDefinition.FormulaFields[2].Text = "MasqueSomme =''";
                }
                else if (optQuantitatif.Checked)
                {
                    WindowTitle = "Aperçu quantitatif du devis : " + Text11.Text;

                    CR1.DataDefinition.FormulaFields[1].Text = "Parametre=''";

                    CR1.DataDefinition.FormulaFields[2].Text = "MasqueSomme ='Q'";

                    //Demande l'affichage des quantités
                }
                else if (optForfaitaireAff.Checked)
                {
                    WindowTitle = "Aperçu forfaitaire par affaire du devis : " + Text11.Text;
                    CR1.DataDefinition.FormulaFields[1].Text = "Parametre='ForfaitAffaire'";

                    //Affiche "M" dans le champ de formule de crystal report ce qui
                    //permet de masquer le champ de sous-total (texte et numérique)

                    CR1.DataDefinition.FormulaFields[2].Text = "MasqueSomme ='M'";
                }
                else if (OPtRecap.Checked)
                {
                    WindowTitle = "Aperçu récapitulatif du devis : " + Text11.Text;
                    if (bFax == true)
                    {
                        General.fc_sendFaxmaker(CR2, "", "", 0, txtFax.Text, txtDestinataire.Text, "", true);
                        return;
                    }
                }
                else if (OptDebourse.Checked)
                {
                    WindowTitle = "Aperçu déboursé simple du devis : " + Text11.Text;
                    for (i = 0; i <= SSDBGridCorps.Rows.Count - 1; i++)
                    {
                        var Row = SSDBGridCorps.Rows[i];
                        if (Row.Cells["CodeMO"].Value.ToString() == "E")
                        {
                            x = Convert.ToDouble(Row.Cells["PxHeured"].Value);
                        }
                        else if (Row.Cells["CodeMO"].Value.ToString() == "S")
                        {
                            Y = Convert.ToDouble(Row.Cells["PxHeured"].Value);
                        }
                        if (Convert.ToDouble(Row.Cells["PxHeured"].Value) != 0 || Row.Cells["PxHeured"].Value != null)
                        {
                            if (Convert.ToDouble(Row.Cells["PxHeured"].Value) == x)
                            {
                                Tot2 = Tot2 + Convert.ToDouble(Row.Cells["Mohud"].Value);
                            }
                            else if (Convert.ToDouble(Row.Cells["PxHeured"].Value) == Y)
                            {
                                Tot1 = Tot1 + Convert.ToDouble(Row.Cells["Mohud"].Value);
                            }
                        }
                    }
                    Sum2 = Tot2 * x;
                    Sum1 = Tot1 * Y;

                    CR3.DataDefinition.FormulaFields[0].Text = "Quant1 = " + Tot1;
                    CR3.DataDefinition.FormulaFields[1].Text = "Quant2 = " + Tot2;
                    CR3.DataDefinition.FormulaFields[2].Text = "Prix1 = " + Y;
                    CR3.DataDefinition.FormulaFields[3].Text = "Prix2 =" + x;
                    CR3.DataDefinition.FormulaFields[4].Text = "Somme1 =" + Sum1;
                    CR3.DataDefinition.FormulaFields[5].Text = "Somme2 =" + Sum2;
                }
                else if (OptDebourseAff.Checked)
                {
                    WindowTitle = "Aperçu déboursé par poste du devis : " + Text11.Text;
                    CR3.DataDefinition.FormulaFields[0].Text = "";
                    CR3.DataDefinition.FormulaFields[1].Text = "";
                    CR3.DataDefinition.FormulaFields[2].Text = "";
                    CR3.DataDefinition.FormulaFields[3].Text = "";
                    CR3.DataDefinition.FormulaFields[4].Text = "";
                    CR3.DataDefinition.FormulaFields[5].Text = "";
                    if (bFax == true)
                    {
                        General.fc_sendFaxmaker(CR3, "", "", 6, txtFax.Text, txtDestinataire.Text, "", true);
                        return;
                    }

                }

                if (ModCourrier.EnvoieMail == true)
                {
                    ModCourrier.FichierAttache = "";
                    if (OptDescriptif.Checked == true)
                    {
                        //TODO : Mondir - Look for commented 3 lines
                        //CR1.EMailMessage = ModuleMail.MessageMail;
                        //CR1.EMailSubject = ModuleMail.ObjetMail;
                        //CR1.EMailToList = SSOleDBCmbMail.Text;
                        ModCourrier.FichierAttache = "C:\\DevisDescriptif" + Text11.Text + ".doc";
                    }
                    else if (optForfaitairePos.Checked == true)
                    {
                        //TODO : Mondir - Look for commented 3 lines
                        //CR1.EMailMessage = MessageMail;
                        //CR1.EMailSubject = ObjetMail;
                        //CR1.EMailToList = SSOleDBCmbMail.Text;
                        //CR1.EMailCCList = ""
                        ModCourrier.FichierAttache = "C:\\DevisForfaitPos" + Text11.Text + ".doc";
                    }
                    else if (optQuantitatif.Checked == true)
                    {
                        //TODO : Mondir - Look for commented 3 lines
                        //CR1.EMailMessage = MessageMail;
                        //CR1.EMailSubject = ObjetMail;
                        //CR1.EMailToList = SSOleDBCmbMail.Text;
                        ModCourrier.FichierAttache = "C:\\DevisQuantitatif" + Text11.Text + ".doc";
                    }
                    else if (optForfaitaireAff.Checked == true)
                    {
                        //TODO : Mondir - Look for commented 3 lines
                        //CR1.EMailMessage = MessageMail;
                        //CR1.EMailSubject = ObjetMail;
                        //CR1.EMailToList = SSOleDBCmbMail.Text;
                        ModCourrier.FichierAttache = "C:\\DevisForfaitAff" + Text11.Text + ".doc";
                    }
                    else if (OPtRecap.Checked == true)
                    {
                        //TODO : Mondir - Look for commented 3 lines
                        //CR2.EMailMessage = MessageMail;
                        //CR2.EMailSubject = ObjetMail;
                        //CR2.EMailToList = SSOleDBCmbMail.Text;
                        ModCourrier.FichierAttache = "C:\\DevisRecap" + Text11.Text + ".doc";
                    }
                    else if (OptDebourse.Checked == true)
                    {
                        //TODO : Mondir - Look for commented 3 lines
                        //CR3.EMailMessage = MessageMail;
                        //CR3.EMailSubject = ObjetMail;
                        //CR3.EMailToList = SSOleDBCmbMail.Text;
                        ModCourrier.FichierAttache = "C:\\DevisDebourse" + Text11.Text + ".doc";
                    }
                    else if (OptDebourseAff.Checked == true)
                    {
                        //TODO : Mondir - Look for commented 3 lines
                        //CR3.EMailMessage = MessageMail;
                        //CR3.EMailSubject = ObjetMail;
                        //CR3.EMailToList = SSOleDBCmbMail.Text;
                        ModCourrier.FichierAttache = "C:\\DevisDebourseAff" + Text11.Text + ".doc";
                    }

                    // Création du fichier (devis) à envoyer
                    if (ModCourrier.InitialiseEtOuvreOutlook() == true)
                    {
                        if (OPtRecap.Checked && iDestination == 3)
                        {
                            //TODO : Mondir - Look for commented 3 lines
                            //CR2.PrintFileType = Crystal.PrintFileTypeConstants.crptWinWord;
                            //CR2.PrintFileName = FichierAttache;
                            //CR2.Destination = Crystal.DestinationConstants.crptToFile;
                        }
                        else if ((OptDebourse.Checked | OptDebourseAff.Checked) & iDestination == 3)
                        {
                            //TODO : Mondir - Look for commented 3 lines
                            //CR3.PrintFileType = Crystal.PrintFileTypeConstants.crptWinWord;
                            //CR3.PrintFileName = FichierAttache;
                            //CR3.Destination = Crystal.DestinationConstants.crptToFile;
                        }
                        else if (iDestination == 3)
                        {
                            //TODO : Mondir - Look for commented 3 lines
                            //CR1.PrintFileType = Crystal.PrintFileTypeConstants.crptWinWord;
                            //CR1.PrintFileName = FichierAttache;
                            //CR1.Destination = Crystal.DestinationConstants.crptToFile;
                        }
                        else
                        {
                            //TODO : Mondir - Look for commented line
                            //CR1.Destination = Crystal.DestinationConstants.crptToWindow;
                        }
                    }
                    else
                    {
                        return;
                    }
                }


                //== lance l'impression directement sur l'imprimante ou affiche à l'écran.
                if (OPtRecap.Checked && iDestination == 1)
                {
                    CR2.RecordSelectionFormula = SelectionFormulaCR2;
                    CR2.PrintToPrinter(1, false, 0, 0);
                }
                else if (OPtRecap.Checked && iDestination == 0)
                {
                    CrystalForm = new CrystalReportFormView(CR2, SelectionFormulaCR2);
                    CrystalForm.Show();
                }
                else if ((OptDebourse.Checked || OptDebourseAff.Checked) && iDestination == 1)
                {
                    CR3.RecordSelectionFormula = SelectionFormulaCR3;
                    CR3.PrintToPrinter(1, false, 0, 0);
                }
                else if ((OptDebourse.Checked || OptDebourseAff.Checked) && iDestination == 0)
                {
                    CrystalForm = new CrystalReportFormView(CR3, SelectionFormulaCR3);
                    CrystalForm.Show();
                }
                else if (iDestination == 1)
                {
                    //        CR1.Destination = crptToPrinter
                    CrystalForm = new CrystalReportFormView(CR1, SelectionFormulaCR1);
                    CrystalForm.Show();
                }
                else if (iDestination == 0)
                {
                    CrystalForm = new CrystalReportFormView(CR1, SelectionFormulaCR1);
                    CrystalForm.Show();
                }

                //== lance l'action demandée : crptToFile ou crptToPrinter ou crptToWindow
                //if (OPtRecap.Checked)
                //{
                //    CR2.Action = 1;
                //}
                //else if (OptDebourse.Checked | OptDebourseAff.Checked)
                //{
                //    CR3.Action = 1;
                //}
                //else
                //{
                //    CR1.Action = 1;
                //}

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";prcImpression;");
            }



            //    err.clear
            //    Resume Next

            //sauvegarde de l'impression au format WORD dans le sous repertoire DEVIS/DT
            //en 1 exemplaire.
            //Si il existe, le remplacer...

            //If iDestination = 0 Then
            //        .Destination = crptToFile
            //        en cours de test sauvegarder sous C
            //        .PrintFileName = gCheminDataDT & "\" & Text14 & "\Devis\Dt\" & Text1(1) & ".doc"
            //        .PrintFileName = "c:\" & Text1(1) & ".doc"
            //        .PrintFileType = crptWinWord
            //        On Error Resume Next
            //        .Action = 1
            //    End If

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdGestionEntete_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdGestionEntete_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            var frmEnteteDevis = new frmEnteteDevis();
            frmEnteteDevis.ShowDialog();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdGestionTypeReglement_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdGestionTypeReglement_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            var frmPiedDevis = new frmPiedDevis();
            frmPiedDevis.ShowDialog();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdHistorique_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdHistorique_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!string.IsNullOrEmpty(Text11.Text))
            {
                // stocke la position de la fiche Devis
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, Text11.Text);
                // stocke les paramétres pour la feuille de destination.
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocHistoDevis, Text14.Text);

                //TODO : Mondir - Must to develope PROGUserDocHistoDevis
                View.Theme.Theme.Navigate(typeof(UserDocHistoDevis));
                //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocHistoDevis);
            }
        }

        /// <summary>
        /// Mondir le 04.09.2020, passag en 04 après l'envoie par mail, https://groupe-dt.mantishub.io/view.php?id=1894
        /// </summary>
        private void MajTVA()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"MajTVA() - Enter In The Function");
            //===> Fin Modif Mondir

            double dbTauxTva = 0;

            //=== date de création, modif du 1 1 2012.
            if (General.IsDate(Text122.Text))
            {
                if (Convert.ToDateTime(Text122.Text) >= Convert.ToDateTime("01/01/2014"))
                {
                    dbTauxTva = 10;
                }
                else if (Convert.ToDateTime(Text122.Text) > Convert.ToDateTime("01/01/2012"))
                {
                    dbTauxTva = 7;

                }
                else if (General.IsNumeric(txtTaux.Text))
                {
                    dbTauxTva = Convert.ToDouble(txtTaux.Text);
                }
                else
                {
                    dbTauxTva = 5.5;

                }
            }

            //var xx = General.Execute("UPDATE DevisDetail SET CodeTva =" + dbTauxTva.ToString().Replace(",", ".") +
            //    " WHERE NumeroDevis ='" + Text11.Text + "'" + " and (codetva is null  or codetva = 0)");

            //adocnn.Execute "UPDATE DevisDetail SET codeTva = 5.5 WHERE NumeroDevis ='" & Text1(1) & "'" _
            //& " and (codetva is null  or codetva = 0)"

            //'=== modif du 28 03 2019, ne mets pas a jour la tva si 0.

            var xx = General.Execute("UPDATE DevisDetail SET CodeTva =" + dbTauxTva.ToString().Replace(",", ".") + " WHERE NumeroDevis ='" + Text11.Text + "'"
                                     + " and (codetva is null  )");
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdImprimer_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdImprimer_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            double dbTauxTva = 0;
            string sReturn = null;
            string strEtat = null;

            try
            {
                ModCourrier.EnvoieMail = false;
                if (string.IsNullOrEmpty(SSOleDBCombo1Bis.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Type de reglement obligatoire.");
                    SSTab1.SelectedTab = SSTab1.Tabs[3];
                    return;
                }

                // Mettre le pointeur de la souris en sablier
                Cursor.Current = Cursors.WaitCursor;

                ////==================> Tested
                if (string.IsNullOrEmpty(txtDateImpression.Text))
                {
                    txtDateImpression.Text = DateTime.Now.ToString(General.FormatDateSansHeureSQL);
                    strEtat = SSetat.Text;
                    SSetat.Text = "04";
                }

                ///=====================> Tested First
                if (valider() == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Impression annulée.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtDateImpression.Text = "";
                    SSetat.Text = strEtat;
                }
                else
                {

                    //===> Mondir le 04.09.2020, passag en 04 après l'envoie par mail, https://groupe-dt.mantishub.io/view.php?id=1894
                    MajTVA();
                    //===> Fin Modif Mondir

                    //'adocnn.Execute "UPDATE DevisDetail SET codeTva = 5.5 WHERE NumeroDevis ='" & Text1(1) & "'" _
                    // & " and (codetva is null  or codetva = 0)"


                    if (OptDebourse.Checked == true)
                    {
                        prcImpression(General.strDTDebourse, 0, Convert.ToInt16(Text4.Text));
                    }
                    ///=================> Tested
                    else if (OptDebourseAff.Checked == true)
                    {
                        prcImpression(General.strDTDebourseAffaire, 0, Convert.ToInt16(Text4.Text));
                    }
                    ///===================> Tested
                    else if (OPtRecap.Checked == true)
                    {
                        prcImpression(General.strDTDevisRecap, 0, Convert.ToInt16(Text4.Text));
                        //        prcImpression strDTDevis, 0, CInt(Text4)
                    }
                    else
                    {
                        General.tabParametresDevis = new Axe_interDT.Shared.General.ParamDevis[3];
                        ModCrystalPDF.tabFormulas = new Axe_interDT.Shared.ModCrystalPDF.tpFormulas[3];

                        //ReDim tabParametresDevis(2)

                        if (OptDescriptif.Checked == true)
                        {
                            General.tabParametresDevis = new General.ParamDevis[3];
                            General.tabParametresDevis[0].sName = "Parametre";
                            General.tabParametresDevis[0].sValue = "Descriptif";
                            General.tabParametresDevis[1].sName = "MasqueSomme ";
                            General.tabParametresDevis[1].sValue = "M";

                            if (General.sExportPdf == "1")
                            {
                                ModCrystalPDF.tabFormulas[0].sNom = "Parametre";
                                ModCrystalPDF.tabFormulas[0].sCritere = "Descriptif";
                                ModCrystalPDF.tabFormulas[0].sType = devModeCrystal.cExportChaine;

                                ModCrystalPDF.tabFormulas[1].sNom = "MasqueSomme ";
                                ModCrystalPDF.tabFormulas[1].sCritere = "M";
                                ModCrystalPDF.tabFormulas[1].sType = devModeCrystal.cExportChaine;

                            }

                        }
                        ///================> Tested
                        else if (optForfaitaireAff.Checked == true)
                        {
                            General.tabParametresDevis[0].sName = "Parametre";
                            General.tabParametresDevis[0].sValue = "";
                            General.tabParametresDevis[1].sName = "MasqueSomme ";
                            General.tabParametresDevis[1].sValue = "M";

                            if (General.sExportPdf == "1")
                            {
                                ModCrystalPDF.tabFormulas[0].sNom = "Parametre";
                                ModCrystalPDF.tabFormulas[0].sCritere = "";
                                ModCrystalPDF.tabFormulas[0].sType = devModeCrystal.cExportChaine;

                                ModCrystalPDF.tabFormulas[1].sNom = "MasqueSomme ";
                                ModCrystalPDF.tabFormulas[1].sCritere = "";
                                ModCrystalPDF.tabFormulas[1].sType = devModeCrystal.cExportChaine;

                            }


                        }
                        else if (optForfaitairePos.Checked == true)
                        {
                            General.tabParametresDevis[0].sName = "Parametre";
                            General.tabParametresDevis[0].sValue = "";
                            General.tabParametresDevis[1].sName = "MasqueSomme ";
                            General.tabParametresDevis[1].sValue = "";

                            if (General.sExportPdf == "1")
                            {
                                ModCrystalPDF.tabFormulas[0].sNom = "Parametre";
                                ModCrystalPDF.tabFormulas[0].sCritere = "";
                                ModCrystalPDF.tabFormulas[0].sType = devModeCrystal.cExportChaine;

                                ModCrystalPDF.tabFormulas[1].sNom = "MasqueSomme ";
                                ModCrystalPDF.tabFormulas[1].sCritere = "";
                                ModCrystalPDF.tabFormulas[1].sType = devModeCrystal.cExportChaine;

                            }

                        }
                        else if (optQuantitatif.Checked == true)
                        {
                            General.tabParametresDevis[0].sName = "Parametre";
                            General.tabParametresDevis[0].sValue = "";
                            General.tabParametresDevis[1].sName = "MasqueSomme ";
                            General.tabParametresDevis[1].sValue = "Q";

                            if (General.sExportPdf == "1")
                            {
                                ModCrystalPDF.tabFormulas[0].sNom = "Parametre";
                                ModCrystalPDF.tabFormulas[0].sCritere = "";
                                ModCrystalPDF.tabFormulas[0].sType = devModeCrystal.cExportChaine;

                                ModCrystalPDF.tabFormulas[1].sNom = "MasqueSomme ";
                                ModCrystalPDF.tabFormulas[1].sCritere = "Q";
                                ModCrystalPDF.tabFormulas[1].sType = devModeCrystal.cExportChaine;
                            }


                        }

                        sReturn = General.fc_ShowDevis(Text11.Text);

                        ////===============> Tested
                        if (General.sExportPdf == "1" && !string.IsNullOrEmpty(sReturn))
                        {

                            //ShellExecute hWnd, "print", sReturn, "", "", SW_SHOWNORMAL

                            ModuleAPI.Ouvrir(sReturn);
                            Cursor.Current = Cursors.Arrow;

                        }

                    }


                    if (OptDebourse.Checked == true)
                    {

                    }
                    ////==================> Tested
                    else if (OptDebourseAff.Checked == true)
                    {
                        //  ce rapport DTDebourseAffaireSQL-V9.rpt n'existe pas
                        if (!File.Exists(General.gsRpt + "DTDebourseAffaireSQL-V9.rpt"))
                            return;
                        fc_ExportDevis(Text11.Text, "DTDebourseAffaireSQL-V9.rpt", "", "", "Debourse", true);
                    }
                    else if (OPtRecap.Checked == true)
                    {

                    }
                    else if (OptDescriptif.Checked == true)
                    {
                        fc_ExportDevis(Text11.Text, General.cExportDev, "Descriptif", "M", "Descriptif", true);
                    }
                    else if (optForfaitairePos.Checked == true)
                    {
                        fc_ExportDevis(Text11.Text, General.cExportDev, "", "", "", true);
                    }
                    ///====================> Tested
                    else if (optQuantitatif.Checked == true)
                    {
                        fc_ExportDevis(Text11.Text, General.cExportDev, "", "Q", "Quantitatif", true);
                    }
                    ///=================> Tested
                    else if (optForfaitaireAff.Checked == true)
                    {
                        fc_ExportDevis(Text11.Text, General.cExportDev, "", "M", "ForfaitAffaire", true);
                    }

                    var xx = General.Execute("UPDATE DevisEntete SET DevisEntete.ExportPDF=1 WHERE DevisEntete.NumeroDevis='" + Text11.Text + "'");
                    if (SSetat.Text.ToUpper() != "A" && SSetat.Text.ToUpper() != "TS" && SSetat.Text.ToUpper() != "CA" && SSetat.Text.ToUpper() != "R" && SSetat.Text.ToUpper() != "T" && SSetat.Text.ToUpper() != "P3")
                    {
                        SSetat.Text = "04";
                    }
                    bloque();
                }

                // Mettre le pointeur de la souris en flêche (normal)
                Cursor.Current = Cursors.Default;
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";cmdImprimer_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNumeroDevis"></param>
        /// <param name="sNomReport"></param>
        /// <param name="sParametre"></param>
        /// <param name="sMasqueSomme"></param>
        /// <param name="sNomDevis"></param>
        /// <param name="blnInsert"></param>
        /// <returns></returns>
        public string fc_ExportDevis(string sNumeroDevis, string sNomReport = General.cExportDev, string sParametre = "", string sMasqueSomme = "", string sNomDevis = "", bool blnInsert = false)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ExportDevis() - Enter In The Function - sNumeroDevis = {sNumeroDevis} " +
                                        $"- sNomReport = {sNomReport} - sParametre = {sParametre} - sMasqueSomme = {sMasqueSomme} " +
                                        $"- sNomDevis = {sNomDevis} - blnInsert = {blnInsert}");
            //===> Fin Modif Mondir

            string functionReturnValue = null;


            string sCodeImmeuble = null;
            string sTitreDevis = null;
            int i = 0;
            object x = null;
            string sFiltreDevis1 = null;
            string strTop = null;
            string sSQLtx = null;
            string sFileDevis = null;
            string sCreateFile = null;

            DataTable rs = default(DataTable);
            ModAdo modAdors;
            ReportDocument Report = new ReportDocument();

            bool bSupFile = false;

            string dtCreateFile = null;

            int hFileRepondeur = 0;

            int hFile = 0;
            short Cont = 0;

            try
            {
                rs = new DataTable();
                modAdors = new ModAdo();


                General.SQL = "SELECT  DevisEntete.NumeroDevis,DevisEntete.CodeImmeuble,DevisEntete.TitreDevis, DevisEntete.DateImpression " + " FROM DevisEntete WHERE DevisEntete.NumeroDevis='" + sNumeroDevis + "'";
                rs = modAdors.fc_OpenRecordSet(General.SQL);

                if (rs.Rows.Count > 0)
                {
                    if (sNomReport.ToUpper() == General.cExportDev.ToUpper())
                    {
                        if (General.IsDate(rs.Rows[0]["DateImpression"].ToString()))
                        {
                            if (Convert.ToDateTime(rs.Rows[0]["DateImpression"]) >= General.dtdateDevExportV2)
                            {

                                sSQLtx = "SELECT     Personnel.SCH_Code" + " FROM  DevisEnTete INNER JOIN"
                                    + "  Immeuble ON DevisEnTete.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN "
                                    + " Personnel ON Immeuble.CodeCommercial = Personnel.Matricule"
                                    + " WHERE DevisEnTete.NumeroDevis = '" + sNumeroDevis + "'";
                                using (var tmpModAdo = new ModAdo())
                                    sSQLtx = tmpModAdo.fc_ADOlibelle(sSQLtx);

                                if (sSQLtx.Contains("CLIM"))
                                {
                                    if (General.adocnn.Database.ToUpper() == General.cNameDelostal.ToUpper())
                                    {
                                        if (File.Exists(General.gsRpt + General.cExportDevV2clim))
                                        {
                                            Report.Load(General.gsRpt + General.cExportDevV2clim);
                                        }
                                        else
                                        {
                                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce rapport n'existe pas", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return functionReturnValue;
                                        }
                                    }
                                    else
                                    {

                                        if (File.Exists(General.gsRpt + General.cExportDevV2))
                                        {
                                            Report.Load(General.gsRpt + General.cExportDevV2);
                                        }
                                        else
                                        {
                                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce rapport n'existe pas", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                            return functionReturnValue;
                                        }
                                    }
                                }
                                else
                                {

                                    if (File.Exists(General.gsRpt + General.cExportDevV2))
                                    {
                                        Report.Load(General.gsRpt + General.cExportDevV2);
                                    }
                                    else
                                    {
                                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce rapport n'existe pas", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                        return functionReturnValue;
                                    }
                                }
                            }
                            else
                            {

                                if (File.Exists(General.gsRpt + General.cExportDevV2))
                                {
                                    Report.Load(General.gsRpt + General.cExportDevV2);
                                }
                                else
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce rapport n'existe pas", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                    return functionReturnValue;
                                }
                            }
                            ///==================> Tested                           
                        }
                        else
                        {

                            if (File.Exists(General.gsRpt + General.cExportDevV2))
                            {
                                Report.Load(General.gsRpt + General.cExportDevV2);
                            }
                            else
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce rapport n'existe pas", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                                return functionReturnValue;
                            }
                        }
                    }
                    ///==============> Tested
                    else
                    {
                        if (File.Exists(General.gsRpt + sNomReport))
                        {
                            Report.Load(General.gsRpt + sNomReport);
                        }
                        else
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce rapport n'existe pas", "", MessageBoxButtons.OK, MessageBoxIcon.Warning);
                            return functionReturnValue;
                        }

                    }

                    sCodeImmeuble = rs.Rows[0]["CodeImmeuble"].ToString().Trim();
                    sNumeroDevis = rs.Rows[0]["numerodevis"].ToString().Replace("/", "-");
                    sTitreDevis = rs.Rows[0]["TitreDevis"] + "";

                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble);
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble + "\\Devis ");
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeImmeuble + "\\Devis\\out");

                    bSupFile = false;
                    sFileDevis = General.sCheminDossier + "\\" + sCodeImmeuble + "\\Devis\\out\\Devis" + sNomDevis + sNumeroDevis + ".pdf";

                    ///=================> Tested
                    if (Dossier.fc_ControleFichier(sFileDevis) == false)
                    {
                        Dossier.fc_SupprimeFichier(sFileDevis);
                        bSupFile = false;
                    }
                    else
                    {
                        //        hFileRepondeur = FindFirstFile(sFileDevis, WFD)
                        //       If hFileRepondeur <> INVALID_HANDLE_VALUE Then '==Fichier trouvé.
                        //
                        //            '=== controle la date fichier si celui ci est posterieur ou égale on supprime le fichier.
                        //            ' sFile = StripNulls(WFD.cFileName)
                        //
                        //            hFile = OpenFile(sFileDevis, filestruct, OF_READ Or OF_SHARE_DENY_NONE)
                        //            GetFileTime hFile, Ft1, Ft2, Ft3
                        //
                        //
                        //            FileTimeToLocalFileTime Ft3, Ft3
                        //            FileTimeToSystemTime Ft3, SysTime
                        //
                        //            sCreateFile = SysTime.wDay & "/" & SysTime.wMonth & "/" & SysTime.wYear
                        //            dtCreateFile = sCreateFile
                        //
                        //            If dtCreateFile >= dtdateDevExportV2 Then
                        //
                        //                Cont = FindClose(hFileRepondeur)
                        //                fc_SupprimeFichier (sFileDevis)
                        //                bSupFile = False
                        //            Else
                        bSupFile = true;
                        //            End If
                        //        End If
                        Dossier.fc_SupprimeFichier(sFileDevis);
                        bSupFile = false;
                    }


                    if (bSupFile == false)
                    {
                        ///================> Tested
                        if (sNomReport == "DTDevisSQL-V9.rpt")
                        {
                            Report.DataDefinition.FormulaFields["Parametre"].Text = "'" + sParametre + "'";
                            Report.DataDefinition.FormulaFields["MasqueSomme "].Text = "'" + sMasqueSomme + "'";
                        }

                        //TODO : Mondir - Check This Commented Line And Test With FicheAppl Number = 300249
                        //Report.ParameterFields[1].DefaultValues.Add(rs.Rows[0]["NumeroDevis"].ToString());

                        Report.SetParameterValue("NoDevis", rs.Rows[0]["NumeroDevis"].ToString());

                        //TODO : Mondir - Check This Commented Lines
                        Report.ExportOptions.ExportFormatType = ExportFormatType.PortableDocFormat;
                        Report.ExportOptions.ExportDestinationType = ExportDestinationType.DiskFile;
                        DiskFileDestinationOptions CrDiskFileDestinationOptions = new DiskFileDestinationOptions();
                        CrDiskFileDestinationOptions.DiskFileName = General.sCheminDossier + "\\" + sCodeImmeuble + "\\Devis\\out\\Devis" + sNomDevis + sNumeroDevis.Replace("/", "-") + ".pdf";
                        Report.ExportOptions.DestinationOptions = CrDiskFileDestinationOptions;

                        //TODO : Mondir - When Using A 64bit And Trying To Export A Report ---> Solution Is To Use 32Bit
                        Report.Export();


                        //Report.Application.LogOffServer "PDSODBC.DLL", "DTDELOSTAL"
                        if (blnInsert == true)
                        {
                            General.SQL = "INSERT INTO DOC_Documents (";
                            General.SQL = General.SQL + "DOC_Nat,";
                            General.SQL = General.SQL + "DOC_PhyPath,";
                            General.SQL = General.SQL + "DOC_VirPath,";
                            General.SQL = General.SQL + "DOC_DTPath,";
                            General.SQL = General.SQL + "DOC_Name,";
                            General.SQL = General.SQL + "DOC_DCreat,";
                            General.SQL = General.SQL + "DOC_DModif,";
                            General.SQL = General.SQL + "DOC_FType,";
                            General.SQL = General.SQL + "DOC_Imm,";
                            General.SQL = General.SQL + "DOC_InOut,";
                            General.SQL = General.SQL + "DOC_Sujet,";
                            General.SQL = General.SQL + "DOC_NoPiece";
                            General.SQL = General.SQL + ") VALUES (";
                            General.SQL = General.SQL + "'Devis',";

                            //SQL = SQL & "'" & "\\delostal-DC-01\DataDT\" & sCodeimmeuble & "\Devis\out\Devis" & sNomDevis & Replace(sNumeroDevis, "/", "-") & ".pdf',"
                            General.SQL = General.SQL + "'" + General.sCheminDossier + "\\" + sCodeImmeuble + "\\Devis\\out\\Devis" + sNomDevis + sNumeroDevis.Replace("/", "-") + ".pdf',";

                            General.SQL = General.SQL + "'../Data/" + sCodeImmeuble + "/Devis/out/Devis" + sNomDevis + sNumeroDevis.Replace("/", "-") + ".pdf',";
                            General.SQL = General.SQL + "'" + General.sCheminDossier + "\\" + sCodeImmeuble + "\\Devis\\out\\Devis" + sNomDevis + sNumeroDevis.Replace("/", "-") + ".pdf" + "',";
                            General.SQL = General.SQL + "'Devis" + sNomDevis + sNumeroDevis.Replace("/", "-") + ".pdf',";
                            General.SQL = General.SQL + "'" + DateTime.Now.ToString(General.FormatDateSQL) + "',";
                            General.SQL = General.SQL + "'" + DateTime.Now.ToString(General.FormatDateSQL) + "',";
                            General.SQL = General.SQL + "'PDF File',";
                            General.SQL = General.SQL + "'" + sCodeImmeuble + "',";
                            General.SQL = General.SQL + "'out',";
                            General.SQL = General.SQL + "'" + StdSQLchaine.gFr_DoublerQuote(sTitreDevis) + "',";
                            General.SQL = General.SQL + "'" + sNumeroDevis + ".pdf'";
                            //== modif du 24 janvier 2007
                            //General.SQL = General.SQL & "'" & sNomDevis & ".pdf'"
                            General.SQL = General.SQL + ")";
                            var xx = General.Execute(General.SQL);
                        }
                    }
                }
                functionReturnValue = General.sCheminDossier + "\\" + sCodeImmeuble + "\\Devis\\out\\Devis" + sNomDevis + sNumeroDevis.Replace("/", "-") + ".pdf";
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Program.SaveException(e);

                functionReturnValue = "";
                //    On Error GoTo 0
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message);
                return functionReturnValue;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void bloque()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"bloque() - Enter In The Function");
            //===> Fin Modif Mondir

            object ctl = null;
            short i = 0;
            string sSQLDroit = null;

            bloqueRec(this);

            txtFax.Enabled = true;
            txtDestinataire.Enabled = true;
            txtFax.ReadOnly = false;
            txtDestinataire.ReadOnly = false;
            SSOleDBCmbMail.Enabled = true;

            //== modif rachid du 22/02/2005.
            SSOleDBCombo1Bis.ReadOnly = false;
            SSOleDBCombo1Bis.Enabled = true;

            txtDevisEntete9.AccReadOnly = false;
            txtDevisEntete10.AccReadOnly = false;
            txtDevisEntete11.AccReadOnly = false;
            txtDevisEntete12.AccReadOnly = false;
            //=== modif du 13 02 2017, ajout des champs
            txtDevisEntete23.AccReadOnly = false;
            txtDevisEntete24.AccReadOnly = false;
            Check47.Enabled = true;
            DTPicker10.Enabled = true;
            DTPicker11.Enabled = true;
            Text31.AccReadOnly = true;
            Text320.AccReadOnly = true;
            Text38.AccReadOnly = true;
            //=== FIN

            txtDevisEntete13.AccReadOnly = false;
            txtDevisEntete14.AccReadOnly = false;
            txtDevisEntete15.AccReadOnly = false;
            Check45.Enabled = true;
            Check40.Enabled = true;

            //=== modif du 23 07 2019.
            txtDevisEntete29.AccReadOnly = false;
            txtDevisEntete28.AccReadOnly = false;
            DTPicker12.Enabled = true;
            //=== fin modif du 23 07 2019.

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="C"></param>
        public void bloqueRec(Control C)
        {

            foreach (Control CC in C.Controls)
            {
                //if (CC.HasChildren)

                bloqueRec(CC);

                //Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(CC.Name);

                ///==============> Tested
                if (CC is System.Windows.Forms.TextBox)
                {
                    var tmp = CC as TextBox;
                    tmp.ReadOnly = true;
                }

                //======> Tested
                if (CC is iTalk_TextBox_Small2)
                {
                    var tmp = CC as iTalk_TextBox_Small2;
                    tmp.ReadOnly = true;
                    tmp.AccReadOnly = true;
                }

                if (CC is UltraCombo)
                {
                    CC.Enabled = true;
                }

                if (CC is UltraGrid)
                {
                    if ((CC.Tag != null && !CC.Tag.ToString().Contains("NoLock")) || C.Tag == null)
                    {
                        var ctl = CC as UltraGrid;
                        //foreach (UltraGridColumn Col in ctl.DisplayLayout.Bands[0].Columns)
                        //    Col.CellActivation = Activation.ActivateOnly;
                        //for (i = 0; i <= (ctl.DisplayLayout.Bands[0].Columns.Count - 1); i += 1)
                        //{                            
                        //    ctl.DisplayLayout.Bands[0].Columns[i].CellActivation= Activation.ActivateOnly;
                        //}
                        ctl.DisplayLayout.Bands[0].Override.AllowUpdate = DefaultableBoolean.False;
                    }
                }

                if (CC is UltraDropDown)
                {
                    CC.Enabled = false;
                }

                if (CC is System.Windows.Forms.RadioButton)
                {
                    if (CC.Tag == null || string.IsNullOrEmpty(CC.Tag.ToString()))
                    {
                        CC.Enabled = false;
                    }
                }

                if (CC is System.Windows.Forms.CheckBox)
                {
                    CC.Enabled = false;
                }
            }

        }

        /// <summary>
        /// ===> Mondir le 04.09.2020, passag en 04 après l'envoie par mail, https://groupe-dt.mantishub.io/view.php?id=1894
        /// </summary>
        private void ErroWhileSending(string oldStatut)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ErroWhileSending() - Enter In The Function - oldStatut = {oldStatut}");
            //===> Fin Modif Mondir

            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Impression annulée.\nVotre e-mail n'a pas été envoyé.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
            txtDateImpression.Text = "";
            SSetat.Text = oldStatut;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMail_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdMail_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            string strDevisLocal = null;

            try
            {
                short i = 0;

                // Vérification de la présence d'un type de règlement
                ///===============> Tested
                if (string.IsNullOrEmpty(SSOleDBCombo1.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le type de règlement", "", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                }

                if (SSOleDBCmbMail.Rows.Count == 0)
                {
                    if (initialiseComboMail() == false)
                    {
                        return;
                    }
                }

                //===> Mondir le 04.09.2020, passag en 04 après l'envoie par mail, https://groupe-dt.mantishub.io/view.php?id=1894
                if (SSetat.Text != "04" && MessageBox.Show("Le devis passera en 04 automatiquement après l'envoi.\nVous-vous continuer ?", "Important", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.No)
                    return;

                var strEtat = "";
                if (string.IsNullOrEmpty(txtDateImpression.Text))
                {
                    txtDateImpression.Text = DateTime.Now.ToString(General.FormatDateSansHeureSQL);
                    strEtat = SSetat.Text;
                    SSetat.Text = "04";
                }

                if (valider() == false)
                {
                    ErroWhileSending(strEtat);
                    return;
                }
                MajTVA();
                //===> Fin Modif Mondir


                //Valide l'envoie du mail
                ModCourrier.EnvoieMail = true;

                //Efface les éléments de la liste des destinataires mis en copie
                //for (i = 0; i <= frmCopie.lstCopier.Items.Count - 1; i++)
                //{
                //    frmCopie.lstCopier.Items.RemoveAt((0));
                //}

                ModCourrier.list = "";

                //Ajoute les éléments de la liste des destinataires mis en copie
                //        SSOleDBCmbMail.MoveFirst
                //    For i = 0 To SSOleDBCmbMail.Rows - 1
                //        frmCopie.lstCopier.AddItem SSOleDBCmbMail.Columns("Contacts").value
                //        SSOleDBCmbMail.MoveNext
                //    Next i

                //Ajoute les éléments de la liste du destinataire
                //        SSOleDBCmbMail.MoveFirst
                //    For i = 0 To SSOleDBCmbMail.Rows - 1
                //        frmMail.SSOleDBcmbA.AddItem SSOleDBCmbMail.Columns("Contacts").value
                //        SSOleDBCmbMail.MoveNext
                //    Next i
                //
                //    frmMail.SSOleDBcmbA.Text = SSOleDBCmbMail.Text



                if (ModCourrier.EnvoieMail == true)
                {
                    if (OptDebourse.Checked == true)
                    {

                    }
                    else if (OptDebourseAff.Checked == true)
                    {
                        //DTDebourseAffaireSQL-V9.rpt
                        ModCourrier.FichierAttache = fc_ExportDevis(Text11.Text, "DTDebourseAffaireSQL-V9.rpt", "", "", "Debourse");
                    }
                    else if (OPtRecap.Checked == true)
                    {

                    }
                    ///=============> Tested
                    else if (OptDescriptif.Checked == true)
                    {
                        ModCourrier.FichierAttache = fc_ExportDevis(Text11.Text, General.cExportDev, "Descriptif", "M", "Descriptif");
                    }
                    else if (optForfaitairePos.Checked == true)
                    {
                        ModCourrier.FichierAttache = fc_ExportDevis(Text11.Text);
                    }
                    else if (optQuantitatif.Checked == true)
                    {
                        ModCourrier.FichierAttache = fc_ExportDevis(Text11.Text, General.cExportDev, "", "Q", "Quantitatif");
                    }
                    else if (optForfaitaireAff.Checked == true)
                    {
                        ModCourrier.FichierAttache = fc_ExportDevis(Text11.Text, General.cExportDev, "", "M", "ForfaitAffaire");
                    }
                }

                //===> Mondir le 04.09.2020, passag en 04 après l'envoie par mail, https://groupe-dt.mantishub.io/view.php?id=1894
                if (string.IsNullOrEmpty(ModCourrier.FichierAttache))
                {
                    ErroWhileSending(strEtat);
                    return;
                }
                //===> Fin Modif Mondir

                ModMain.bActivate = true;

                var frmMail = new frmMail();
                frmMail.txtDocuments.Text = ModCourrier.FichierAttache;
                frmMail.txtObjet.Text = txtTitreDevis.Text;
                frmMail.txtMessage.Text = fc_GetMessage(Text10.Text, txtAppelValid.Text, txtTitreDevis.Text);
                frmMail.ShowDialog();

                SSOleDBCmbMail.Text = frmMail.SSOleDBcmbA.Text;
                if (ModCourrier.EnvoieMail == true)
                {
                    if ((ModCourrier.MonExplorer != null))
                    {
                        ModCourrier.MonExplorer.Close();
                        ModCourrier.MonExplorer = null;
                    }
                }

                frmMail.Close();
                //Réinitialise l'envoi du mail

                if (ModCourrier.EnvoieMail == true)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Votre e-mail a bien été envoyé à l'adresse : " + SSOleDBCmbMail.Text, "Mail envoyé", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                else
                {
                    //===> Mondir le 04.09.2020, passag en 04 après l'envoie par mail, https://groupe-dt.mantishub.io/view.php?id=1894
                    ErroWhileSending(strEtat);
                    ModCourrier.EnvoieMail = false;
                    return;
                    //===> Fin Modif Mondir
                }

                ModCourrier.EnvoieMail = false;

                //===> Mondir le 04.09.2020, passag en 04 après l'envoie par mail, https://groupe-dt.mantishub.io/view.php?id=1894
                var xx = General.Execute("UPDATE DevisEntete SET DevisEntete.ExportPDF=1 WHERE DevisEntete.NumeroDevis='" + Text11.Text + "'");
                if (SSetat.Text.ToUpper() != "A" && SSetat.Text.ToUpper() != "TS" && SSetat.Text.ToUpper() != "CA" && SSetat.Text.ToUpper() != "R" && SSetat.Text.ToUpper() != "T" && SSetat.Text.ToUpper() != "P3")
                {
                    SSetat.Text = "04";
                }
                bloque();
                //===> Fin Modif Mondir
            }
            catch (Exception exception)
            {
                ModCourrier.EnvoieMail = false;
                Erreurs.gFr_debug(exception, this.Name + ";cmdMail_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNoAppelDem"></param>
        /// <param name="sNoAppelValid"></param>
        /// <param name="sTitreDevis"></param>
        /// <returns></returns>
        public string fc_GetMessage(string sNoAppelDem, string sNoAppelValid, string sTitreDevis)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_GetMessage() - Enter In The Function - sNoAppelDem = {sNoAppelDem} - sNoAppelValid = {sNoAppelValid} " +
                                        $"- sTitreDevis = {sTitreDevis}");
            //===> Fin Modif Mondir

            string functionReturnValue = null;
            DataTable rsAppel = default(DataTable);
            var modAdorsAppel = new ModAdo();
            string sqlSelect = null;
            string sMsg = null;

            sqlSelect = "SELECT GestionStandard.CodeImmeuble,GestionStandard.CodeOrigine1,GestionStandard.CodeOrigine2,GestionStandard.Societe,GestionStandard.Source,GestionStandard.IntervenantDT,GestionStandard.DateAjout,GestionStandard.QuiDevis,Immeuble.Adresse,Immeuble.Ville,Immeuble.CodePostal FROM GestionStandard ";
            sqlSelect = sqlSelect + " INNER JOIN Immeuble ON GestionStandard.CodeImmeuble=Immeuble.CodeImmeuble WHERE NumFicheStandard=" + sNoAppelDem;
            rsAppel = modAdorsAppel.fc_OpenRecordSet(sqlSelect);
            if (rsAppel.Rows.Count > 0)
            {
                ///===============> Tested First
                if (rsAppel.Rows[0]["CodeOrigine1"] + "" == Convert.ToString(General.cTel))
                    sMsg = "Selon votre appel téléphonique du " + Convert.ToDateTime(rsAppel.Rows[0]["DateAjout"]).ToString(General.FormatDateSansHeureSQL) + ",";
                else if (rsAppel.Rows[0]["CodeOrigine1"] + "" == Convert.ToString(General.cFax))
                {
                    sMsg = "Selon votre fax du " +
                           Convert.ToDateTime(rsAppel.Rows[0]["DateAjout"]).ToString(General.FormatDateSansHeureSQL) + ",";
                }
                else if (rsAppel.Rows[0]["CodeOrigine1"] + "" == Convert.ToString(General.cInternet))
                {
                    sMsg = "Selon votre message extranet du " +
                           Convert.ToDateTime(rsAppel.Rows[0]["DateAjout"]).ToString(General.FormatDateSansHeureSQL + ",");
                }
                else if (rsAppel.Rows[0]["CodeOrigine1"] + "" == Convert.ToString(General.cRepondeur))
                {
                    sMsg = "Selon votre message laissé sur notre répondeur du " +
                           Convert.ToDateTime(rsAppel.Rows[0]["DateAjout"]).ToString(General.FormatDateSansHeureSQL) + ",";
                }
                else if (rsAppel.Rows[0]["CodeOrigine1"] + "" == Convert.ToString(General.cCourrier))
                {
                    sMsg = "Selon votre courrier du " +
                          Convert.ToDateTime(rsAppel.Rows[0]["DateAjout"]).ToString(General.FormatDateSansHeureSQL) + ",";
                }
                else if (rsAppel.Rows[0]["CodeOrigine1"] + "" == Convert.ToString(General.cRadio))
                {
                    sMsg = "Selon votre appel téléphonique du " +
                           Convert.ToDateTime(rsAppel.Rows[0]["DateAjout"]).ToString(General.FormatDateSansHeureSQL) + ",";
                }
                else if (rsAppel.Rows[0]["CodeOrigine1"] + "" == Convert.ToString(General.cDirect))
                {
                    sMsg = "Selon nos observations du " +
                          Convert.ToDateTime(Text122.Text).ToString(General.FormatDateSansHeureSQL) + ",";
                }
                else if (rsAppel.Rows[0]["CodeOrigine1"] + "" == Convert.ToString(General.CEmail))
                {
                    sMsg = "Selon votre e-Mail du " +
                           Convert.ToDateTime(rsAppel.Rows[0]["DateAjout"]).ToString(General.FormatDateSansHeureSQL) + ",";
                }
                else
                {
                    sMsg = "Selon votre appel téléphonique du " +
                           rsAppel.Rows[0]["DateAjout"] +
                           ",";
                }
                sMsg = sMsg + " veuillez trouver ci-joint le devis concernant l'immeuble situé : " + "\r\n";
                sMsg = sMsg + "\r\n" + rsAppel.Rows[0]["Adresse"] + "\r\n";
                sMsg = sMsg + "\r\n" + rsAppel.Rows[0]["CodePostal"] + " " + rsAppel.Rows[0]["Ville"] + "\r\n";
                sMsg = sMsg + "\r\n";
                sMsg = sMsg + "Veuillez nous le retourner revêtu de votre signature.";
            }

            functionReturnValue = sMsg;

            modAdorsAppel?.Dispose();
            rsAppel = null;
            return functionReturnValue;

        }

        /// <summary>
        /// Controle Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMAJ2_Click(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMAJ_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdMAJ_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            string sSQL = null;
            string sCodeEtat = null;
            DataTable rsT = default(DataTable);
            ModAdo modAdorsT = null;
            int lRenovationChauff = 0;
            string sRespTravaux = "";
            string sMajDev = "";

            try
            {

                cmdMAJ.Enabled = false;
                ///===============> Tested
                if (string.IsNullOrEmpty(txtDeviseur.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le deviseur", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    txtDeviseur.Focus();
                    cmdMAJ.Enabled = true;
                    return;
                }

                ///==================> Tested
                if (string.IsNullOrEmpty(ssTYD_TypeDevis.Text) && string.IsNullOrEmpty(txtDateImpression.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez désormais renseigner le type de devis.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    ssTYD_TypeDevis.Focus();
                    cmdMAJ.Enabled = true;
                    return;
                }
                if (General.sEvolution == "1")
                {
                    if (string.IsNullOrEmpty(ssCombo1.Text) && string.IsNullOrEmpty(txtDateImpression.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez désormais renseigner le metier.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        SSTab1.SelectedTab = SSTab1.Tabs[0];
                        ssTYD_TypeDevis.Focus();
                        cmdMAJ.Enabled = true;
                        return;
                    }
                }
                ///===============> Tested
                if (string.IsNullOrEmpty(txtdesArticle.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez mettre un code pour le titre du devis.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    SSTab1.SelectedTab = SSTab1.Tabs[0];
                    txtdesArticle.Focus();
                    cmdMAJ.Enabled = true;
                    return;
                }

                //===> Mondir le 23.11.2020, regarder la signature de la fonction
                General.fc_HistoStatutDevis(Text11.Text, SSetat.Text, "UserDocDevis");
                //===> Fin Modif Mondir


                if (!string.IsNullOrEmpty(txtDateImpression.Text))
                {

                    //== Modif rachid si le devis a un statut en 04, on autorise uniquement la
                    //== modif du statut.
                    sSQL = "SELECT CodeEtat, RenovationChauff, ResponsableTravaux FROM DevisEnTete WHERE NumeroDevis='" + Text11.Text + "'";
                    modAdorsT = new ModAdo();
                    rsT = modAdorsT.fc_OpenRecordSet(sSQL);

                    sCodeEtat = rsT.Rows[0]["CodeEtat"] + "";
                    lRenovationChauff = Convert.ToInt32(General.nz(rsT.Rows[0]["RenovationChauff"].ToString(), 0));
                    sRespTravaux = rsT.Rows[0]["ResponsableTravaux"] + "";
                    //sCodeEtat = fc_ADOlibelle(sSQL)

                    ////=============> Tested First
                    if (sCodeEtat.ToUpper() == SSetat.Text.ToUpper())
                    {
                        if (lRenovationChauff != (int)Check45.CheckState || (sRespTravaux == "" && txtRespTrav.Text != ""))
                        {

                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce devis a été envoyé au client, la modification n'est pas autorisée." + "\n" + " Seul " + "la case à cocher 'Rénovation chaufferie' a été mise à jour." +
                                "et/ou le responsable travaux si celui-ci n'est pas alimenté", "Sauvegarde du devis " + Text11.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);

                            sMajDev = "UPDATE DevisEnTete SET RenovationChauff = " + ((int)Check45.CheckState);

                            if (string.IsNullOrEmpty(sRespTravaux))
                                sMajDev = sMajDev + ", ResponsableTravaux ='" + StdSQLchaine.gFr_DoublerQuote(txtRespTrav.Text) + "'";

                            sMajDev = sMajDev + " where NumeroDevis='" + Text11.Text + "'";

                            var xx = General.Execute(sMajDev);

                        }
                        ///==================> Tested
                        else
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce devis ne peut pas être enregistré, celui-ci a été transmis au client(imprimé)", "Sauvegarde du devis " + Text11.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        cmdMAJ.Enabled = true;
                        return;

                    }
                    else if (sCodeEtat.ToUpper() != SSetat.Text.ToUpper())
                    {
                        //== Controle si le nouveau statut est autorisé.
                        sSQL = "SELECT ApImprime FROM DevisCodeEtat WHERE Code='" + SSetat.Text + "'";
                        using (var tmpModAdo = new ModAdo())
                            sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                        ////===============> Tested First
                        if (sSQL == "1")
                        {

                            Text121.Text = Convert.ToString(DateTime.Now);
                            valider();
                            fc_CtrlSaisie(Text11.Text);
                        }
                        else
                        {
                            sSQL = "SELECT RenovationChauff FROM DevisEnTete WHERE NumeroDevis='" + Text11.Text + "'";
                            using (var tmpModAdo = new ModAdo())
                                sSQL = General.nz(tmpModAdo.fc_ADOlibelle(sSQL), "0").ToString();

                            if (Convert.ToInt32(sSQL) != (int)Check45.CheckState || (sRespTravaux == "" && txtRespTrav.Text != ""))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce devis a été envoyé au client, la modification n'est pas autorisée." + "\n" + " Seul " + "la case à cocher 'Rénovation chaufferie' a été mise à jour" +
                                    " et/ou le responsable travaux si celui-ci n'est pas alimenté.", "Sauvegarde du devis " + Text11.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);

                                sMajDev = "UPDATE DevisEnTete SET RenovationChauff = " + ((int)Check45.CheckState);

                                if (string.IsNullOrEmpty(sRespTravaux))
                                    sMajDev = sMajDev + ", ResponsableTravaux ='" + StdSQLchaine.gFr_DoublerQuote(txtRespTrav.Text) + "'";

                                sMajDev = sMajDev + " where NumeroDevis='" + Text11.Text + "'";

                                var xx = General.Execute(sMajDev);
                            }
                            else
                            {

                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("le statut " + SSetat.Text + " n'est pas autorisé, celui-ci a été transmis au client(imprimé)", "Sauvegarde du devis " + Text11.Text, MessageBoxButtons.OK, MessageBoxIcon.Information);
                                SSetat.Text = sCodeEtat;
                            }
                            cmdMAJ.Enabled = true;
                            return;
                        }

                    }
                }
                else
                {
                    Text121.Text = Convert.ToString(DateTime.Now);
                    valider();
                    fc_CtrlSaisie(Text11.Text);

                }

                //    If Not IsNull(txtDateImpression.Text) And Not txtDateImpression.Text = "" Then
                //        MsgBox "Ce devis ne peut pas être enregistré, car il est envoyé au client (imprimé)", vbInformation, "Sauvegarde du devis " & Text1(1).Text
                //        Exit Sub
                //    Else
                //    End If 



                View.Theme.Theme.AfficheAlertSucces();
                cmdMAJ.Enabled = true;

            }
            catch (Exception ex)
            {
                cmdMAJ.Enabled = true;
                Erreurs.gFr_debug(ex, this.Name + ";cmdMAJ_Click");

            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNodevis"></param>
        private void fc_CtrlSaisie(string sNodevis)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_CtrlSaisie() - Enter In The Function - sNodevis = {sNodevis}");
            //===> Fin Modif Mondir

            string sSQL = null;
            string sMessage = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;

            try
            {
                if (string.IsNullOrEmpty(sNodevis))
                {
                    return;
                }


                if (General.sCrtlDevis != "1")
                {
                    return;
                }

                sMessage = "";

                //sSQL = "SELECT     NumeroDevis, NumeroLigne, Quantite, MOhud, PxHeured, FOud, STud" _
                //& " From DevisDetail" _
                //& " WHERE     (NumeroDevis = '" & gFr_DoublerQuote(sNoDevis) & "') AND (Quantite IS NOT NULL AND Quantite > 0) AND (MOhud IS NULL OR " _
                //& " MOhud = 0) AND (FOud IS NULL OR FOud = 0) AND (STud IS NULL OR STud = 0)" _
                //& " or " _
                //& " (NumeroDevis = '" & gFr_DoublerQuote(sNoDevis) & "') AND (Quantite = 0 OR Quantite IS NULL) AND (MOhud IS NOT NULL AND MOhud > 0) " _
                //& " OR " _
                //& " (NumeroDevis = '" & gFr_DoublerQuote(sNoDevis) & "') AND (Quantite = 0 OR Quantite IS NULL) AND (FOud IS NOT NULL AND FOud > 0)" _
                //& " OR " _
                //& " (NumeroDevis = '" & gFr_DoublerQuote(sNoDevis) & "') AND (Quantite = 0 OR Quantite IS NULL) AND (STud IS NOT NULL AND STud > 0) order by NumeroLigne"

                sSQL = "SELECT     NumeroDevis, NumeroLigne, Quantite, MOhud, PxHeured, FOud, STud, TotalVenteApresCoef " + " From DevisDetail";

                sSQL = sSQL + " WHERE     (NumeroDevis = '" + StdSQLchaine.gFr_DoublerQuote(sNodevis) + "') AND (Quantite IS NOT NULL AND Quantite > 0) " + " AND (TotalVenteApresCoef IS NULL OR TotalVenteApresCoef = 0) ";

                sSQL = sSQL + " OR ";

                sSQL = sSQL + " (NumeroDevis = '" + StdSQLchaine.gFr_DoublerQuote(sNodevis) + "') AND   (TotalVenteApresCoef IS NULL OR" + " TotalVenteApresCoef = 0)   AND (MOhud IS NOT NULL AND MOhud > 0) ";
                sSQL = sSQL + " OR ";

                sSQL = sSQL + " (NumeroDevis = '" + StdSQLchaine.gFr_DoublerQuote(sNodevis) + "') AND   (TotalVenteApresCoef IS NULL OR" + " TotalVenteApresCoef = 0)   AND (FOud IS NOT NULL AND FOud > 0) ";

                sSQL = sSQL + " OR ";
                sSQL = sSQL + " (NumeroDevis = '" + StdSQLchaine.gFr_DoublerQuote(sNodevis) + "') AND   (TotalVenteApresCoef IS NULL OR" + " TotalVenteApresCoef = 0)   AND (STud IS NOT NULL AND STud > 0) ";

                sSQL = sSQL + " order by NumeroLigne ";

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                foreach (DataRow rsRow in rs.Rows)
                {

                    //===controle si une quantité a été saisie qu'aucun mentant(MO? ST OU ST) de ne lui est associé.
                    if (Convert.ToInt32(General.nz(rsRow["Quantite"], 0)) > 0 && Convert.ToInt32(General.nz(rsRow["TotalVenteApresCoef"], 0)) == 0)
                    {
                        sMessage = sMessage + "- Sur la ligne " + rsRow["NumeroLigne"] + " une quantité a été saisie alors que le total vente après coef est à 0.\n";

                        //=== controle si un un nombre d'heure de main d'œuvre a été saisi sans aucune quantité.
                    }
                    else if (Convert.ToInt32(General.nz(rsRow["TotalVenteApresCoef"], 0)) == 0 && Convert.ToInt32(General.nz(rsRow["Mohud"], 0)) > 0)
                    {
                        sMessage = sMessage + "- Sur la ligne " + rsRow["NumeroLigne"] + " un nombre d'heure de main d'œuvre a été saisi alors que le total vente après coef est à 0.\n";

                        //=== controle si un montant de fourniture a été saisi sans aucune quantité.
                    }
                    else if (Convert.ToInt32(General.nz(rsRow["TotalVenteApresCoef"], 0)) == 0 && Convert.ToInt32(General.nz(rsRow["Foud"], 0)) > 0)
                    {
                        sMessage = sMessage + "- Sur la ligne " + rsRow["NumeroLigne"] + " un montant de fourniture a été saisi alors que le total vente après coef est à 0.\n";

                        //=== controle si un montant de sous traitance a été saisi sans aucune quantité.
                    }
                    else if (Convert.ToInt32(General.nz(rsRow["TotalVenteApresCoef"], 0)) == 0 && Convert.ToInt32(General.nz(rsRow["Stud"], 0)) > 0)
                    {
                        sMessage = sMessage + "- Sur la ligne " + rsRow["NumeroLigne"] + " un montant de sous-traitance a été saisi alors que le total vente après coef est à 0.\n";

                    }
                }

                if (!string.IsNullOrEmpty(sMessage))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMessage, "Alerte", MessageBoxButtons.OK, MessageBoxIcon.Information);

                }

                modAdors?.Dispose();

                //Set rs = fc_OpenRecordSet(sSQL, 1)
                //
                //Do While rs.EOF = False
                //
                //        '===controle si une quantité a été saisie qu'aucun mentant(MO? ST OU ST) de ne lui est associé.
                //        If nz(rs!Quantite, 0) > 0 And nz(rs!Mohud, 0) = 0 And nz(rs!Foud, 0) = 0 And nz(rs!Stud, 0) = 0 Then
                //                sMessage = sMessage & "- Sur la ligne " & rs!NumeroLigne & " une quantité a été saisie sans aucun montant associé (MO, ST ou FO)" & vbCrLf
                //
                //        '=== controle si un un nombre d'heure de main d'œuvre a été saisi sans aucune quantité.
                //        ElseIf nz(rs!Quantite, 0) = 0 And nz(rs!Mohud, 0) > 0 Then
                //                sMessage = sMessage & "- Sur la ligne " & rs!NumeroLigne & " Sur la ligne un nombre d'heure de main d'œuvre a été saisi sans aucune quantité." & vbCrLf
                //
                //        '=== controle si un montant de fourniture a été saisi sans aucune quantité.
                //        ElseIf nz(rs!Quantite, 0) = 0 And nz(rs!Foud, 0) > 0 Then
                //                sMessage = sMessage & "- Sur la ligne " & rs!NumeroLigne & " Sur la ligne un montant de fourniture a été saisi sans aucune quantité." & vbCrLf
                //
                //        '=== controle si un montant de fourniture a été saisi sans aucune quantité.
                //        ElseIf nz(rs!Quantite, 0) = 0 And nz(rs!Stud, 0) > 0 Then
                //                sMessage = sMessage & "- Sur la ligne " & rs!NumeroLigne & " Sur la ligne un montant de sous-traitance a été saisi sans aucune quantité." & vbCrLf
                //
                //        End If
                //        rs.MoveNext
                //Loop
                //
                //If sMessage <> "" Then
                //    MsgBox sMessage, vbInformation, "Alerte"
                //
                //End If
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_CtrlSaisie");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdModeles_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdModeles_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {

                int i = 0;
                int nbCol = 0;

                // Vérification de l'état de la grille :
                // si elle est bloquée : pas d'insertion d'un modèle
                // si elle n'est pas bloquée : autoriser l'insertion du modèle
                //================> Tested
                for (i = 0; i <= (SSDBGridCorps.DisplayLayout.Bands[0].Columns.Count - 1); i += 1)
                {
                    if (SSDBGridCorps.DisplayLayout.Bands[0].Columns[i].CellActivation == Activation.ActivateOnly)
                    {
                        nbCol = nbCol + 1;
                    }
                }
                //si la grille possède autant de colonne vérouillée qu'elle possède de colonne
                // (la grille complète est vérouillée) alors annulation de l'insertion du modèle :
                // Le devis est terminé donc figé.
                ///=============> Tested
                if (!string.IsNullOrEmpty(txtDateImpression.Text))
                {
                    General.SelectModele = false;
                    var frmModele2 = new frmModele();
                    General.gNumeroDevis = txtNoDevis.Text;
                    frmModele2.cmdAppliquer.Visible = false;
                    frmModele2.cmdQuitter.Visible = true;
                    frmModele2.cmdAppliquer.Enabled = false;
                    frmModele2.ShowDialog();
                    frmModele2.cmdAppliquer.Visible = true;
                    frmModele2.cmdQuitter.Visible = false;
                    frmModele2.cmdAppliquer.Enabled = true;
                    //===> Mondir le 08.01.2021, pour corriger #2162 #2155
                    //===> dans ce cas, General.SelectModele est = à true, les coef de la grille vont etre changer dans la fonction fournisGrille(bool);
                    //===> test à faire pour trouvé le bug : dispatch devis => chercher 202009449 => ouvrir 202009449 => tab Corps => Bouton Model => rien faire dans la fentre => Button Appliquer =>
                    //===> Ficher Appel => rechercher la fiche appel du Devis 202012490 => puis naviguer au devis 202012490 => Coef passé à 0
                    General.SelectModele = false;
                    //===> Fin Modif Mondir
                    //    MsgBox "Impossible d'insérer un modèle :   " & Chr(10) & vbTab & "le devis est figé  ", vbCritical, "Insertion d'un modèle annulée"
                    return;
                }


                if (string.IsNullOrEmpty(Text11.Text))
                {
                    afficheMessage(Convert.ToString(8));
                    return;
                }

                if (string.IsNullOrEmpty(Text315.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le coefficient de main d'oeuvre", "Coefficient de Main d'oeuvre absent", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Text315.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(Text314.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le coefficient fournisseur", "Coefficient fournisseur absent", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Text314.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(Text313.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le coefficient sous-traitant", "Coefficient sous-traitant absent", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Text313.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(Text316.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le prix horaire de la main d'oeuvre", "Prix horaire de la main d'oeuvre absent", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Text316.Focus();
                    return;
                }

                //----Valide le modéle existant
                if (BOOLErreurCorps == false)
                {
                    //adocnn.BeginTrans

                    ValidationCorps();
                    //adocnn.CommitTrans
                    // ERROR: Not supported in C#: OnErrorStatement

                }
                else
                {
                    afficheMessage(Convert.ToString(7));
                    return;
                }

                General.gNumeroDevis = Text11.Text;

                // Sauvegarde des coefficients de la ligne en cours dans la grille du devis
                General.MO = Convert.ToDouble(General.nz(Text315.Text, "0"));
                General.FO = Convert.ToDouble(General.nz(Text314.Text, "0"));
                General.sT = Convert.ToDouble(General.nz(Text313.Text, "0"));
                General.PxMO = Convert.ToDouble(General.nz(Text316.Text, "0"));
                //=== modif du 31/12/2011, change le taux de tva.
                //TVADefaut = Numeric(nz(txtTaux.Text, "5.5"))
                ///=============> Tested
                if (General.IsDate(Text122.Text))
                {
                    if (Convert.ToDateTime(Text122.Text) >= Convert.ToDateTime("01/01/2014"))
                    {
                        General.TVADefaut = Convert.ToDouble(General.nz(txtTaux.Text, "10"));
                    }
                    else
                    {
                        General.TVADefaut = Convert.ToDouble(General.nz(txtTaux.Text, "7"));
                    }
                }
                else
                {
                    General.TVADefaut = Convert.ToDouble(General.nz(txtTaux.Text, "7"));
                }

                General.SelectModele = false;
                frmModele frmModele = new frmModele();
                frmModele.ShowDialog();
                //'fc_UnloadAllForms
                if (General.BoolAnnuler == false)
                {
                    blnModele = true;

                    FournisgrilleNoModele();
                    blnModele = false;
                    RenumeroterGrille();

                    // Réinitialisation de la variable de selection de modèle pour permettre
                    // la vérification de saisie dans la grille du corps (programme : BeforeUpdate)
                    General.SelectModele = false;
                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";cmdModeles_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void FournisgrilleNoModele()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"FournisgrilleNoModele() - Enter In The Function");
            //===> Fin Modif Mondir

            int i = 0;
            try
            {
                General.SQL = "";
                for (i = 0; i <= General.strModele.Length - 1; i++)
                {
                    if (string.IsNullOrEmpty(General.SQL))
                    {
                        General.SQL = " where ModeleDevis='" + General.strModele[i] + "'";
                    }
                    else
                    {
                        General.SQL = General.SQL + " or ModeleDevis ='" + General.strModele[i] + "'";
                    }
                }
                modAdoadotemp = new ModAdo();
                General.SQL = "SELECT DevisModelesDetail.* FROM DevisModelesDetail " + General.SQL + " ORDER BY ModeleDevis ASC, NumeroLigne ASC ";
                fournisGrille(true);
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";FournisgrilleNoModele");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Modele"></param>
        private void fournisGrille(bool Modele = false)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fournisGrille() - Enter In The Function - Modele = {Modele}");
            //===> Fin Modif Mondir

            bool bTarifActualise = false;
            double dbTarifActualise = 0;
            bool bRefreshFoud = false;

            try
            {
                //== modif rachid mettre a jour l' article gecet.
                if (General.bPrixGecetAActualiser == true)
                {

                    bTarifActualise = true;
                }
                else
                {
                    bTarifActualise = false;
                }

                // bPrixGecetAActualiser = False

                //.Open SQL, adocnn, adOpenForwardOnly, adLockReadOnly
                modAdoadotemp = new ModAdo();
                adotemp = modAdoadotemp.fc_OpenRecordSet(General.SQL);

                if (adotemp.Rows.Count > 0)
                {
                    blnFournisGrille = true;
                    //    SSDBGridCorps.Redraw = False

                    if (General.SelectModele == true)
                    {
                        Text316.Text = Convert.ToString(General.PxMO);
                        Text315.Text = Convert.ToString(General.MO);
                        Text314.Text = Convert.ToString(General.FO);
                        Text313.Text = Convert.ToString(General.sT);
                    }

                    foreach (DataRow adotempRow in adotemp.Rows)
                    {

                        bRefreshFoud = false;

                        //== flag servant à forcer l'actualisation des tarifs gecet.
                        if (bTarifActualise == true)
                        {
                            //== Ne pas actualiser les tarifs gecet si le code est égale à 80000,
                            //== ou si le CodeSousArticle est nulle.
                            if (General.nz(adotempRow["CodeSousArticle"], "").ToString() == "80000" ||
                                string.IsNullOrEmpty(General.nz(adotempRow["CodeSousArticle"], "").ToString()))
                            {
                                bRefreshFoud = false;
                            }
                            else
                            {
                                bRefreshFoud = true;
                            }
                        }

                        var ResultRow = (SSDBGridCorps.DataSource as DataTable).NewRow();

                        if (Modele == true)
                        {
                            ResultRow["numerodevis"] = DBNull.Value;
                        }
                        else
                        {
                            ResultRow["numerodevis"] = adotempRow["numerodevis"];
                        }

                        // IsNumeric
                        ResultRow["NumeroLigne"] = adotempRow["NumeroLigne"];
                        ResultRow["CodeFamille"] = StdSQLchaine.fc_CtrlCharDevis(adotempRow["CodeFamille"].ToString());
                        ResultRow["CodeSousFamille"] = StdSQLchaine.fc_CtrlCharDevis(adotempRow["CodeSousFamille"].ToString());
                        ResultRow["CodeArticle"] = StdSQLchaine.fc_CtrlCharDevis(adotempRow["CodeArticle"].ToString());
                        //SQL = SQL & "!" & !CodeSousArticle & "!;"
                        ResultRow["CodeSousArticle"] = StdSQLchaine.fc_CtrlCharDevis(adotempRow["CodeSousArticle"] + "");
                        ResultRow["NumOrdre"] = adotempRow["NumOrdre"];
                        ResultRow["TexteLigne"] = adotempRow["TexteLigne"];
                        ResultRow["Quantite"] = adotempRow["Quantite"];
                        ResultRow["Unite"] = adotempRow["Unite"];
                        ResultRow["Mohud"] = adotempRow["Mohud"];

                        if (adotempRow["Mohud"] == DBNull.Value || Convert.ToInt32(adotempRow["Mohud"]) == 0)
                        {
                            ResultRow["CodeMO"] = adotempRow["CodeMO"];
                        }
                        else
                        {
                            ResultRow["CodeMO"] = General.nz(adotempRow["CodeMO"], "S");
                        }

                        if (General.SelectModele == true)
                        {
                            ResultRow["PxHeured"] = General.PxMO;
                        }
                        else
                        {
                            ResultRow["PxHeured"] = adotempRow["PxHeured"];
                        }


                        if (bRefreshFoud == true)
                        {
                            dbTarifActualise = fc_TarifGecet(adotempRow["CodeSousArticle"] + "");
                            ResultRow["Foud"] = dbTarifActualise;
                        }
                        else
                        {
                            ResultRow["Foud"] = adotempRow["Foud"];
                        }

                        ResultRow["Stud"] = adotempRow["Stud"];

                        if (General.SelectModele == true)
                        {
                            ResultRow["MOhd"] = Convert.ToDouble(General.nz(adotempRow["Quantite"], "0")) * Convert.ToDouble(General.nz(adotempRow["Mohud"], "0"));
                        }
                        else
                        {
                            ResultRow["MOhd"] = adotempRow["MOhd"];
                        }

                        if (General.SelectModele == true)
                        {
                            ResultRow["Mod"] = Convert.ToDouble(General.nz(General.PxMO, "0")) * Convert.ToDouble(General.nz(adotempRow["Mohud"], "0")) * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"));
                        }
                        else
                        {
                            ResultRow["Mod"] = adotempRow["Mod"];
                        }

                        if (bRefreshFoud == true)
                        {
                            ResultRow["Fod"] = dbTarifActualise * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"));
                        }
                        else if (General.SelectModele == true)
                        {
                            ResultRow["Fod"] = Convert.ToDouble(General.nz(adotempRow["Foud"], "0")) * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"));
                        }
                        else
                        {
                            ResultRow["Fod"] = adotempRow["Fod"];
                        }

                        if (General.SelectModele == true)
                        {
                            ResultRow["Std"] = Convert.ToDouble(General.nz(adotempRow["Stud"], "0")) * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"));
                        }
                        else
                        {
                            ResultRow["Std"] = adotempRow["Std"];
                        }

                        if (bRefreshFoud == true)
                        {
                            ResultRow["TotalDebours"] = (Convert.ToDouble(General.nz(General.PxMO, "0")) * Convert.ToDouble(General.nz(adotempRow["Mohud"], "0")) * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"))) +
                                               dbTarifActualise * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0")) + Convert.ToDouble(General.nz(adotempRow["Stud"], "0")) * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"));
                        }
                        else if (General.SelectModele == true)
                        {
                            ResultRow["TotalDebours"] = (Convert.ToDouble(General.nz(General.PxMO, "0")) * Convert.ToDouble(General.nz(adotempRow["Mohud"], "0")) *
                                Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"))) +
                                               dbTarifActualise * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0")) + Convert.ToDouble(General.nz(adotempRow["Foud"], "0")) * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"))
                                               + Convert.ToDouble(General.nz(adotempRow["Stud"], "0")) * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"));
                        }
                        else
                        {
                            ResultRow["TotalDebours"] = adotempRow["TotalDebours"];
                        }

                        if (General.SelectModele == true)
                        {
                            ResultRow["kMO"] = General.MO;
                        }
                        else
                        {
                            ResultRow["kMO"] = adotempRow["kMO"];
                        }

                        if (General.SelectModele == true)
                        {
                            ResultRow["MO"] = Convert.ToDouble(General.nz(General.MO, "1")) * Convert.ToDouble(General.nz(General.PxMO, "0")) *
                                Convert.ToDouble(General.nz(adotempRow["Mohud"], "0")) * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"));
                        }
                        else
                        {
                            ResultRow["MO"] = adotempRow["MO"];
                        }

                        if (General.SelectModele == true)
                        {
                            ResultRow["kFO"] = General.FO;
                        }
                        else
                        {
                            ResultRow["kFO"] = adotempRow["kFO"];
                        }

                        if (bRefreshFoud == true)
                        {
                            ResultRow["FO"] = Convert.ToDouble(General.nz(General.FO, "1")) * dbTarifActualise * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"));
                        }
                        else if (General.SelectModele == true)
                        {
                            ResultRow["FO"] = Convert.ToDouble(General.nz(General.FO, "1")) * Convert.ToDouble(General.nz(adotempRow["Foud"], "0")) *
                                Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"));
                        }
                        else
                        {
                            ResultRow["FO"] = adotempRow["FO"];
                        }

                        if (General.SelectModele == true)
                        {
                            ResultRow["kST"] = General.sT;
                        }
                        else
                        {
                            ResultRow["kST"] = adotempRow["kST"];
                        }

                        if (General.SelectModele == true)
                        {
                            ResultRow["sT"] = Convert.ToDouble(General.nz(General.sT, "1")) * Convert.ToDouble(General.nz(adotempRow["Stud"], "0")) *
                                Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"));
                        }
                        else
                        {
                            ResultRow["sT"] = adotempRow["sT"];
                        }

                        if (bRefreshFoud == true)
                        {
                            ResultRow["TotalVente"] = (Convert.ToDouble(General.nz(General.MO, "1")) * Convert.ToDouble(General.nz(General.PxMO, "0")) *
                                Convert.ToDouble(General.nz(adotempRow["Mohud"], "0")) * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"))) +
                                (Convert.ToDouble(General.nz(General.FO, "1")) * dbTarifActualise * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"))) +
                                (Convert.ToDouble(General.nz(General.sT, "1")) * Convert.ToDouble(General.nz(adotempRow["Stud"], "0")) *
                                Convert.ToDouble(General.nz(adotempRow["Quantite"], "0")));
                        }
                        else if (General.SelectModele == true)
                        {
                            ResultRow["TotalVente"] = (Convert.ToDouble(General.nz(General.MO, "1")) * Convert.ToDouble(General.nz(General.PxMO, "0")) *
                                Convert.ToDouble(General.nz(adotempRow["Mohud"], "0")) * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"))) +
                                (Convert.ToDouble(General.nz(General.FO, "1")) * Convert.ToDouble(General.nz(adotempRow["Foud"], "0")) *
                                Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"))) + (Convert.ToDouble(General.nz(General.sT, "1")) *
                                Convert.ToDouble(General.nz(adotempRow["Stud"], "0")) *
                                Convert.ToDouble(General.nz(adotempRow["Quantite"], "0")));
                        }
                        else
                        {
                            ResultRow["TotalVente"] = adotempRow["TotalVente"];
                        }


                        if (blnModele == true && !string.IsNullOrEmpty(txtTaux.Text))
                        {
                            ResultRow["CodeTVA"] = txtTaux.Text;
                        }
                        else
                        {
                            ResultRow["CodeTVA"] = adotempRow["CodeTVA"];
                        }

                        ResultRow["Coef1"] = adotempRow["Coef1"];
                        ResultRow["Coef2"] = adotempRow["Coef2"];
                        ResultRow["Coef3"] = adotempRow["Coef3"];

                        if (bRefreshFoud == true)
                        {
                            ResultRow["TotalVenteApresCoef"] = (Convert.ToDouble(General.nz(General.MO, "1")) * Convert.ToDouble(General.nz(General.PxMO, "0")) *
                                Convert.ToDouble(General.nz(adotempRow["Mohud"], "0")) * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"))) +
                                (Convert.ToDouble(General.nz(General.FO, "1")) * dbTarifActualise * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"))) +
                                (Convert.ToDouble(General.nz(General.sT, "1")) * Convert.ToDouble(General.nz(adotempRow["Stud"], "0")) *
                                Convert.ToDouble(General.nz(adotempRow["Quantite"], "0")));
                        }
                        else if (General.SelectModele == true)
                        {
                            ResultRow["TotalVenteApresCoef"] = (Convert.ToDouble(General.nz(General.MO, "1")) * Convert.ToDouble(General.nz(General.PxMO, "0")) *
                                Convert.ToDouble(General.nz(adotempRow["Mohud"], "0")) * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"))) +
                                (Convert.ToDouble(General.nz(General.FO, "1")) * Convert.ToDouble(General.nz(adotempRow["Foud"], "0")) *
                                Convert.ToDouble(General.nz(adotempRow["Quantite"], "0"))) + (Convert.ToDouble(General.nz(General.sT, "1")) *
                                Convert.ToDouble(General.nz(adotempRow["Stud"], "0")) * Convert.ToDouble(General.nz(adotempRow["Quantite"], "0")));
                        }
                        else
                        {
                            ResultRow["TotalVenteApresCoef"] = adotempRow["TotalVenteApresCoef"];
                        }

                        ResultRow["Fournisseur"] = adotempRow["Fournisseur"];

                        ResultRow["SautPage"] = General.nz(adotempRow["SautPage"], DBNull.Value);

                        (SSDBGridCorps.DataSource as DataTable).Rows.Add(ResultRow);

                    }
                    //     SSDBGridCorps.Redraw = True
                    // 
                    // This Line Added By Mondir Coz !!! We Desabled The Event Of The Grid, So the Before Insert Row Event Wont Be Fired
                    // Bug : https://groupe-dt.mantishub.io/view.php?id=1639
                    // So I Added This Line Manualy
                    // It Is The Same Value In The  BeforeRowInsert Event
                    if (SSetat.Text == "00")
                        SSetat.Text = "03";
                }
                blnFournisGrille = false;

                modAdoadotemp?.Dispose();

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";Fournisgrille");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sChrono"></param>
        /// <returns></returns>
        private double fc_TarifGecet(string sChrono)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_TarifGecet() - Enter In The Function - sChrono = {sChrono}");
            //===> Fin Modif Mondir

            string sSQL = null;
            string sPrx = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = new ModAdo();
            double dbPrixDev = 0;
            double dbPrixGecet = 0;

            sSQL = "";

            if (General.IsNumeric(sChrono))
            {
                if (General.sGecetV3 == "1")
                {
                    sSQL = "SELECT    Article.DESIGNATION_ARTICLE AS Libelle, Article.CHRONO_INITIAL AS Chrono, "

                    + " ArticleTarif.PRX_DEVIS_NET_UD AS [prix net achat],  "

                    + " ArticleTarif.CODE_UNITE_DEVIS AS fouunite "

                    + " FROM         Article INNER JOIN "

                    + " ArticleTarif ON Article.CHRONO_INITIAL = ArticleTarif.CHRONO_INITIAL"

                    + " WHERE Article.CHRONO_INITIAL = '" + sChrono + "' "

                    + " ORDER BY [prix net achat] DESC";

                }
                else if (General.sGecetV2 == "1")
                {
                    sSQL = "SELECT    Article.DESIGNATION_ARTICLE AS Libelle, Article.CHRONO_INITIAL AS Chrono,"
                        + " ArticleTarif.PRX_DEVIS_NET_UD AS [prix net achat], "
                        + "  ArticleTarif.CODE_UNITE_DEVIS AS fouunite"
                        + " FROM Article INNER JOIN"
                        + "  ArticleTarif ON Article.CHRONO_INITIAL = ArticleTarif.CHRONO_INITIAL"
                        + " WHERE Article.CHRONO_INITIAL  = '" + sChrono + "' "
                        + " ORDER BY [prix net achat] DESC";

                }
                else if (General.sGecetV2 == "1")
                {
                    //sSQL = "SELECT     ART_Article.ART_Libelle AS Libelle, ART_Article.ART_Chrono AS Chrono," _
                    //& " ARTP_ArticlePrix.ARTP_PrixNetDevis AS [prix net achat], " _
                    //& " ARTP_ArticlePrix.ARTP_UniteFourn AS fouunite" _
                    //& " FROM ART_Article INNER JOIN" _
                    //& " ARTP_ArticlePrix ON ART_Article.ART_Chrono = ARTP_ArticlePrix.ART_Chrono" _
                    //& " WHERE ART_Article.ART_Chrono = '" & sChrono & "' " _
                    //& " ORDER BY [prix net achat] DESC"

                    //==== modif du 17 11 2014, changmenet de l'unité devis.
                    sSQL = "SELECT     ART_Article.ART_Libelle AS Libelle, ART_Article.ART_Chrono AS Chrono,"
                        + " ARTP_ArticlePrix.ARTP_PrixNetDevis AS [prix net achat], "
                        + " ARTP_ArticlePrix.ARTP_UniteDevis AS fouunite"
                        + " FROM ART_Article INNER JOIN"
                        + " ARTP_ArticlePrix ON ART_Article.ART_Chrono = ARTP_ArticlePrix.ART_Chrono"
                        + " WHERE ART_Article.ART_Chrono = '" + sChrono + "' " + " ORDER BY [prix net achat] DESC";

                }
                else
                {
                    sSQL = "SELECT [prix net achat] FROM DtiPrix WHERE Chrono=" + General.nz(sChrono, "0") + " ORDER BY [prix net achat] DESC";

                }

                rs = new DataTable();

                ModParametre.fc_OpenConnGecet();

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL, null, "", ModParametre.adoGecet);

                dbPrixDev = 0;
                dbPrixGecet = 0;

                foreach (DataRow rsRow in rs.Rows)
                {
                    sPrx = rsRow["prix net achat"].ToString().Trim();
                    sPrx = sPrx.Replace(",", ".");

                    if (General.IsNumeric(sPrx))
                    {
                        dbPrixGecet = Convert.ToDouble(sPrx);
                    }
                    else
                    {
                        dbPrixGecet = 0;
                    }

                    if (dbPrixDev < dbPrixGecet)
                    {
                        dbPrixDev = Convert.ToDouble(sPrx);
                    }

                }

                modAdors?.Dispose();

                ModParametre.fc_CloseConnGecet();
            }
            return dbPrixDev;

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void RenumeroterGrille()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"RenumeroterGrille() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                for (i = 0; i <= SSDBGridCorps.Rows.Count - 1; i++)
                {
                    SSDBGridCorps.Rows[i].Cells["Numeroligne"].Value = i;
                }
                SSDBGridCorps.UpdateData();
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";RenumeroterGrille");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRecherche_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRecherche_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                string requete = "SELECT " + "NumeroDevis as \"Numero devis\", CodeImmeuble as \"Immeuble\"," + " CodeEtat as \"CodeEtat\" , Codedeviseur as \"Deviseur\"," +
                                 " titreDevis as \"Titre\" From DevisEntete";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des devis" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    General.SQL = "SELECT * FROM DevisEntete WHERE NumeroDevis='" + fg.ugResultat.ActiveRow.Cells[1].Value + "'";
                    Text11.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    Text10_KeyPress(Text11, new System.Windows.Forms.KeyPressEventArgs((char)13));
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        General.SQL = "SELECT * FROM DevisEntete WHERE NumeroDevis='" + fg.ugResultat.ActiveRow.Cells[1].Value + "'";
                        Text11.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        Text10_KeyPress(Text11, new System.Windows.Forms.KeyPressEventArgs((char)13));
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";cmdRecherche_Click");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text10_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            var textBox = sender as iTalk_TextBox_Small2;
            int Index = Convert.ToInt32(textBox.Tag.ToString().Split('=')[1]);

            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Text10_KeyPress() - Enter In The Function - Index = {Index}");
            //===> Fin Modif Mondir

            try
            {
                string CodeGerant = null;
                bool sDossierIntervention = false;
                switch (Index)
                {
                    case 0:
                        if (KeyAscii == 13)
                        {
                            prcAfficheImmeuble(Text10.Text);
                            fc_RechercheContrat(Text14.Text);
                            cmdMAJ.Enabled = true;
                            // FournisgrilleNoDevis
                            (SSDBGridCorps.DataSource as DataTable).Rows.Clear();
                            using (var tmpModAdo = new ModAdo())
                            {
                                General.rstmp =
                                    tmpModAdo.fc_OpenRecordSet(
                                        "SELECT MO, FO, ST, PxHeure" + " FROM  DEC_DevisCoefDefaut");
                                if (General.rstmp.Rows.Count > 0)
                                {
                                    Text315.Text = General.nz(General.rstmp.Rows[0]["MO"], "1").ToString();
                                    Text314.Text = General.nz(General.rstmp.Rows[0]["FO"], "1").ToString();
                                    Text313.Text = General.nz(General.rstmp.Rows[0]["sT"], "1").ToString();
                                    Text316.Text = General.nz(General.rstmp.Rows[0]["PxHeure"], "49").ToString();
                                }
                                if (string.IsNullOrEmpty(txtDateImpression.Text))
                                {
                                    Debloque();
                                }
                                else
                                {
                                    bloque();
                                }
                            }

                        }
                        break;


                    case 1:

                        ////============================> Tested
                        if (KeyAscii == 13)
                        {
                            //initialise à blancs le devis.
                            prcRAZDevis(false);
                            //--affichage des donnees
                            modAdoadotemp = new ModAdo();
                            General.SQL = "select * from DevisEnTete where NumeroDevis='" + Text11.Text + "'";
                            adotemp = modAdoadotemp.fc_OpenRecordSet(General.SQL);

                            if (adotemp.Rows.Count > 0)
                            {
                                //ModDevis.RV = adotemp.Rows[0]["RV"].ToString();
                                fc_ChekCoef(true);
                                Text315.Text = adotemp.Rows[0]["DEV_CoefMOdefaut"].ToString();
                                Text314.Text = adotemp.Rows[0]["DEV_CoefFOdefaut1"] + "";
                                Text313.Text = adotemp.Rows[0]["DEV_CoefSTdefaut2"] + "";
                                Text316.Text = adotemp.Rows[0]["DEV_CoefPrixdefaut2"] + "";
                                Text11.Text = adotemp.Rows[0]["NumeroDevis"] + "";
                                txtNoDevis.Text = adotemp.Rows[0]["NumeroDevis"] + "";
                                Text10.Text = adotemp.Rows[0]["NumFicheStandard"] + "";
                                Text121.Text = adotemp.Rows[0]["DateMAJ"] + "";
                                Text122.Text = adotemp.Rows[0]["DateCreation"] + "";
                                SSetat.Text = adotemp.Rows[0]["codeetat"] + "";
                                Text31.Text = General.nz(adotemp.Rows[0]["NbreDeplacement"], "0").ToString();
                                Text318.Text = General.nz(adotemp.Rows[0]["PrixDeplacement"], "0").ToString();
                                Text39.Text = General.nz(adotemp.Rows[0]["TotalHT"], "0").ToString();
                                Text321.Text = General.nz(adotemp.Rows[0]["TotalTVA"], "0").ToString();
                                Text323.Text = General.nz(adotemp.Rows[0]["TotalTTC"], "0").ToString();
                                Text317.Text = General.nz(adotemp.Rows[0]["TotalDeplacement"], "0").ToString();
                                Text38.Text = General.nz(adotemp.Rows[0]["kDivers"], "1").ToString();

                                Text310.Text = General.nz(adotemp.Rows[0]["MontantSouhaite"], "").ToString();
                                Text320.Text = General.nz(adotemp.Rows[0]["CoefMontantSouhaite"], "1").ToString();

                                Text115.Text = General.nz(adotemp.Rows[0]["IntervenantCreat"], General.gsUtilisateur).ToString();
                                Text124.Text = adotemp.Rows[0]["IntervenantModif"] + "";
                                Text322.Text = adotemp.Rows[0][5] + "";
                                //titre du devis

                                txtDevisEntete9.Text = adotemp.Rows[0]["AvctChantier"] + "";
                                txtDevisEntete10.Text = adotemp.Rows[0]["AvctFacturation"] + "";
                                txtDevisEntete11.Text = adotemp.Rows[0]["AvctReglement"] + "";
                                txtDevisEntete12.Text = adotemp.Rows[0]["ConditionPaiement"] + "";
                                //=== modif du 13 02 2017, ajout des champs
                                txtDevisEntete23.Text = adotemp.Rows[0]["ComSurRecept"] + "";
                                txtDevisEntete24.Text = adotemp.Rows[0]["ComSurCloture"] + "";
                                Check47.Checked = General.nz(adotemp.Rows[0]["Receptionne"], 0).ToString() == "1";
                                Check46.Checked = General.nz(adotemp.Rows[0]["Receptionne"], 0).ToString() == "1";
                                Check48.Checked = General.nz(adotemp.Rows[0]["ClotureDevis"], 0).ToString() == "1";
                                Check49.Checked = General.nz(adotemp.Rows[0]["ClotureDevis"], 0).ToString() == "1";
                                //txtDevisEntete(25) = .Fields("DateReception") & ""
                                if (General.IsDate(adotemp.Rows[0]["DateReception"].ToString()))
                                {
                                    DTPicker10.Value = Convert.ToDateTime(adotemp.Rows[0]["DateReception"]);
                                    DTPicker10.Checked = true;
                                }
                                if (General.IsDate(adotemp.Rows[0]["ClotureLe"].ToString()))
                                    DTPicker11.Value = Convert.ToDateTime(adotemp.Rows[0]["ClotureLe"]);
                                txtDevisEntete26.Text = adotemp.Rows[0]["ReceptionnePar"] + "";

                                //=== FIN
                                txtDevisEntete13.Text = adotemp.Rows[0]["Situation"] + "";
                                txtDevisEntete14.Text = adotemp.Rows[0]["Acompte"] + "";
                                txtDevisEntete15.Text = adotemp.Rows[0]["DelaisReglmt"] + "";

                                //=== modif du 23 07 2019.
                                txtDevisEntete29.Text = adotemp.Rows[0]["CLoturerPar"] + "";
                                txtDevisEntete28.Text = adotemp.Rows[0]["CodeReglement"] + "";
                                fc_ModeReglement();
                                if (General.IsDate(adotemp.Rows[0]["AcompteRecuLe"]))
                                {
                                    // View Issue #320 on redmine
                                    // DTPicker12.Value = Convert.ToDateTime(General.IsDate(adotemp.Rows[0]["AcompteRecuLe"]));
                                    DTPicker12.Value = Convert.ToDateTime(adotemp.Rows[0]["AcompteRecuLe"]);
                                    DTPicker12.Checked = true;
                                }
                                else
                                    DTPicker12.Checked = false;
                                //'=== fin modif du 23 07 2019.


                                txtTitreDevis.Text = adotemp.Rows[0]["TitreDevis"] + "";
                                txtdesArticle.Text = adotemp.Rows[0]["CodeTitre"] + "";
                                TauxTVADefaut = adotemp.Rows[0]["CodeTVA"] + "";
                                txtTaux.Text = TauxTVADefaut;
                                Text5.Text = adotemp.Rows[0]["AdrEnvoi1"] + "";
                                Text6.Text = adotemp.Rows[0]["AdrEnvoi2"] + "";
                                Text7.Text = adotemp.Rows[0]["CPEnvoi"] + "";
                                Text8.Text = adotemp.Rows[0]["VilleEnvoi"] + "";
                                Text4.Text = adotemp.Rows[0]["NbreExemplaire"] + "";
                                txtDeviseur.Text = adotemp.Rows[0]["CodeDeviseur"] + "";
                                txtRespTrav.Value = adotemp.Rows[0]["ResponsableTravaux"] + "";
                                txtDEV_LIbelleClient.Text = adotemp.Rows[0]["DEV_LIbelleClient"] + "";
                                if (adotemp.Rows[0]["DossierDevis"].ToString() != "" && (adotemp.Rows[0]["DossierDevis"].ToString() == "1" || Convert.ToBoolean(adotemp.Rows[0]["DossierDevis"])))
                                {
                                    cmdDossierDevis.BackColor = ColorTranslator.FromOle(0xc0ffc0);
                                }
                                else
                                {
                                    cmdDossierDevis.BackColor = ColorTranslator.FromOle(0xffc0c0);
                                }
                                if (adotemp.Rows[0]["DateImpression"] != DBNull.Value)
                                {
                                    txtDateImpression.Text = adotemp.Rows[0]["DateImpression"].ToString();
                                    bloque();
                                }
                                else
                                {
                                    txtDateImpression.Text = "";
                                    Debloque();
                                }
                                if (adotemp.Rows[0]["typepaiement"].ToString() == "0" || adotemp.Rows[0]["typepaiement"] == DBNull.Value)
                                {
                                    SSOleDBCombo1.Text = "";
                                }
                                else
                                {
                                    SSOleDBCombo1.Text = adotemp.Rows[0]["typepaiement"] + "";
                                }
                                SSOleDBCombo2.Text = adotemp.Rows[0]["Entetedevis"] + "";
                                if (adotemp.Rows[0]["CodeCopieA"].ToString() == "0" || adotemp.Rows[0]["CodeCopieA"] == DBNull.Value)
                                {
                                    ssDbCopieA.Text = "";
                                }
                                else
                                {
                                    ssDbCopieA.Text = adotemp.Rows[0]["CodeCopieA"] + "";
                                }

                                DateAcceptation.Text = adotemp.Rows[0]["DateAcceptation"] + "";
                                //==modif rachid 20 janvier 2005
                                cmbBe.Text = adotemp.Rows[0]["CodeBe"] + "";

                                //=== modif du 23 07 2019.
                                ssTYD_TypeDevis.Text = adotemp.Rows[0]["TYD_Code"].ToString();
                                //=== fin modif du 23 07 2019.

                                //'=== modif du 27 03 2020.
                                ssCombo1.Text = adotemp.Rows[0]["MetierID"] + "";
                                Check45.CheckState = (CheckState)Convert.ToInt32(General.nz(adotemp.Rows[0]["RenovationChauff"].ToString(), 0));

                                if (!string.IsNullOrEmpty(General.nz(adotemp.Rows[0]["CodeBe"], "").ToString()))
                                    using (var tmpModAdo = new ModAdo())
                                        lblLibBE.Text = tmpModAdo.fc_ADOlibelle("SELECT Libelle FROM Be WHERE codeBe=" + adotemp.Rows[0]["CodeBe"]);
                                //.Fields(9) = ssDbCopieA.Value

                                cmdMAJ.Enabled = true;
                                cmdSupprimer.Enabled = true;
                                cmdVersion.Enabled = true;

                                //=== modif du 17 09 2018.
                                txtDevisEntete25.Text = adotemp.Rows[0]["TelPart"] + "";
                                txtDevisEntete27.Text = adotemp.Rows[0]["EmailPart"] + "";
                                SSOleDBCmbMail.Text = txtDevisEntete27.Text;
                                Check410.Checked = General.nz(adotemp.Rows[0]["AffichePart"], 0).ToString() == "1";
                                //'=== fin modif 19 09 2018.


                                //affichage des variables / fiche standard
                                if (!string.IsNullOrEmpty(SSOleDBCombo1.Text) && string.IsNullOrEmpty(General.nz(adotemp.Rows[0]["TextePaiement"], "").ToString()))
                                {
                                    modAdoadotemp?.Dispose();
                                    modAdoadotemp = new ModAdo();
                                    adotemp = modAdoadotemp.fc_OpenRecordSet("SELECT TexteReglement" + " From TypeReglementDevis" + " where CodeReglement=" +
                                        SSOleDBCombo1.Text);
                                    if (adotemp.Rows.Count > 0)
                                    {
                                        SSOleDBCombo1Bis.Text = adotemp.Rows[0]["TexteReglement"] + "";
                                    }
                                    modAdoadotemp?.Dispose();
                                }
                                else
                                {
                                    SSOleDBCombo1Bis.Text = adotemp.Rows[0]["TextePaiement"] + "";
                                    modAdoadotemp?.Dispose();
                                }

                                if (!string.IsNullOrEmpty(ssDbCopieA.Text))
                                {
                                    modAdoadotemp?.Dispose();
                                    modAdoadotemp = new ModAdo();
                                    adotemp = modAdoadotemp.fc_OpenRecordSet("SELECT TypeCopieA.Libelle" + " FROM TypeCopieA" + " where CodeCopieA=" + ssDbCopieA.Text);
                                    if (adotemp.Rows.Count > 0)
                                    {
                                        ssDbCopieABis.Text = adotemp.Rows[0]["Libelle"] + "";
                                    }
                                    modAdoadotemp?.Dispose();
                                }

                                adotemp = null;
                                if (!string.IsNullOrEmpty(TauxTVADefaut))
                                {
                                    boolRecherche = true;
                                }
                                // affiche les informations relatives à l 'immeuble
                                prcAfficheImmeuble(Text10.Text);

                                fc_RechercheContrat((Text14.Text));

                                fc_NoFacture(Text11);

                                boolRecherche = false;

                                // Text9 = ""
                                // Text9 = .Fields(19) & ""

                                FournisgrilleNoDevis();
                                using (var tmpModAdo = new ModAdo())
                                    txtAppelValid.Text = General.nz(tmpModAdo.fc_ADOlibelle("SELECT GestionStandard.NumFicheStandard FROM GestionStandard " +
                                                                                            "WHERE GestionStandard.NoDevis='" + Text11.Text + "'"), "").ToString();
                                ///======> Tested
                                if (!string.IsNullOrEmpty(txtAppelValid.Text))
                                {
                                    cmdAnalytique.BackColor = ColorTranslator.FromOle(0xc0ffc0);
                                }
                                else
                                {
                                    cmdAnalytique.BackColor = ColorTranslator.FromOle(0xffc0c0);
                                }


                            }
                        }
                        break;
                }

                using (var tmpModAdo = new ModAdo())
                    lblNoIntervention.Text = tmpModAdo.fc_ADOlibelle("SELECT Intervention.NoIntervention" + " FROM Intervention INNER JOIN GestionStandard ON" +
                        " Intervention.NumFicheStandard=GestionStandard.NumFicheStandard " + " Where GestionStandard.NoDevis ='" + Text11.Text + "'");

                using (var tmpModAdo = new ModAdo())
                    lblWave.Text = tmpModAdo.fc_ADOlibelle("SELECT Intervention.Wave" + " FROM Intervention " + " Where Intervention.NoIntervention='" + lblNoIntervention.Text + "'");

                using (var tmpModAdo = new ModAdo())
                    sDossierIntervention = Convert.ToBoolean(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Intervention.DossierIntervention " + " FROM Intervention WHERE Intervention.NoIntervention='" +
                                                                                                lblNoIntervention.Text + "'"), false));
                ///=======> Tested
                if (sDossierIntervention == false)
                {
                    imgNoDossierIntervention.Visible = true;
                    imgYesDossierIntervention.Visible = false;
                }
                else
                {
                    imgNoDossierIntervention.Visible = false;
                    imgYesDossierIntervention.Visible = true;
                }
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);

                Text9.Text = "";
                Text9.Text = "1";
                DateAcceptation.Text = "";
                DateAcceptation.Checked = false;
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNumeroFicheStandard"></param>
        public void prcAfficheImmeuble(string sNumeroFicheStandard)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"prcAfficheImmeuble() - Enter In The Function - sNumeroFicheStandard = {sNumeroFicheStandard}");
            //===> Fin Modif Mondir

            string sSQLParticulier;
            DataTable rsPart = new DataTable();
            ModAdo rsPartAdo = new ModAdo();
            try
            {
                string CodeGerant = null;

                //recherche des renseignements de la 1ere page
                General.SQL = "SELECT " + "GestionStandard.CodeImmeuble,GestionStandard.CodeOrigine2,Immeuble.CodeDepanneur, Immeuble.AngleRue, Immeuble.Gardien, " +
                    "Immeuble.AdresseGardien, Immeuble.TelGardien, Immeuble.FaxGardien, " + "Immeuble.SpecifAcces, Immeuble.CodeAcces1, Immeuble.CodeAcces2, " +
                    "Immeuble.horaires, Immeuble.BoiteAClef, Immeuble.SitBoiteAClef, " + "Immeuble.P3, Immeuble.Observations as ObsImmeuble , Table1.Nom as NomClient, " +
                    "Table1.Observations, Personnel.Nom as Commercial, Personnel_1.Nom as Depanneur , " + "TechReleve.Obs, Immeuble.HorairesLoge,Immeuble.Code1, " +
                    "Immeuble.CodeTVA,GestionStandard.CodeParticulier, " + " gestionstandard.source, Immeuble.Situation,P1,P2,P3,P4 , immeuble.ChaudCondensation " +
                    "FROM (((Table1 RIGHT JOIN (Immeuble RIGHT JOIN GestionStandard ON " + "Immeuble.CodeImmeuble = GestionStandard.CodeImmeuble) ON " +
                    "Table1.Code1 = Immeuble.Code1) LEFT JOIN Personnel ON " +
                    "Immeuble.CodeCommercial = Personnel.Matricule) " + "LEFT JOIN Personnel AS Personnel_1 ON Immeuble.CodeDepanneur = " +
                    "Personnel_1.Matricule) LEFT JOIN TechReleve ON " +
                    "Immeuble.CodeImmeuble = TechReleve.CodeImmeuble " + "WHERE (((GestionStandard.NumFicheStandard)=" + sNumeroFicheStandard + "))";


                modAdoadotemp = new ModAdo();
                adotemp = modAdoadotemp.fc_OpenRecordSet(General.SQL);

                //affichage des données
                if (adotemp.Rows.Count > 0)
                {
                    Text14.Text = adotemp.Rows[0]["codeimmeuble"] + "";
                    //            FileImmeuble.Pattern = "*.*"
                    DirectoryInfo d = new DirectoryInfo(General.sCheminDossier + "\\" + adotemp.Rows[0]["CodeImmeuble"] + "\\photos");//Assuming Test is your Folder
                    if (d.Exists)
                        foreach (FileInfo file in d.GetFiles())
                        {
                            FileImmeuble.Items.Add(file.FullName);
                        }
                    //Text1(10) = fc_ADOlibelle("SELECT TypeContrat.libelleContrat FROM TypeContrat WHERE TypeContrat.CodeContrat='" & adotemp("CodeContrat") & "'") & ""
                    Text2.Text = adotemp.Rows[0]["NomClient"] + "";
                    Text13.Text = adotemp.Rows[0]["Depanneur"] + "";
                    //
                    //Text1(2) = adotemp(24) & "" 'Personnel.Nom
                    Text12.Text = adotemp.Rows[0]["Commercial"] + "";
                    //Personnel.Nom
                    Text14X1.Text = adotemp.Rows[0]["Gardien"] + "";
                    Text10x.Text = adotemp.Rows[0]["AdresseGardien"] + "";
                    Text15.Text = adotemp.Rows[0]["TelGardien"] + "";
                    Text16.Text = adotemp.Rows[0]["FaxGardien"] + "";
                    Text112.Text = adotemp.Rows[0]["SpecifAcces"] + "";
                    Text113.Text = adotemp.Rows[0]["HorairesLoge"] + "";
                    Text17.Text = adotemp.Rows[0]["CodeAcces1"] + "";
                    Text19.Text = adotemp.Rows[0]["CodeAcces2"] + "";
                    Text18.Text = adotemp.Rows[0]["Horaires"] + "";
                    SituaBoite.Text = adotemp.Rows[0]["SitBoiteAClef"] + "";
                    Checkboite.CheckState = (CheckState)Convert.ToInt16(Convert.ToBoolean(adotemp.Rows[0]["BoiteAclef"]) == true ? 1 : 0);
                    //BoiteAclef
                    //Check1 = IIf(adotemp("P3") = True, 1, 0) & ""
                    Text118.Text = adotemp.Rows[0]["ObsImmeuble"] + "";
                    Text119.Text = adotemp.Rows[0]["Observations"] + "";
                    Text120.Text = adotemp.Rows[0]["Obs"] + "";
                    Text11x.Text = adotemp.Rows[0]["Code1"] + "";
                    CodeGerant = adotemp.Rows[0]["code1"].ToString();

                    Check40.CheckState = (CheckState)Convert.ToInt32(General.nz(adotemp.Rows[0]["ChaudCondensation"], 0));

                    this.Check41.CheckState = (CheckState)(Convert.ToBoolean(General.nz(adotemp.Rows[0]["P1"], "0")) == true || adotemp.Rows[0]["P1"].ToString() == "1" ? 1 : 0);
                    this.Check42.CheckState = (CheckState)(Convert.ToBoolean(General.nz(adotemp.Rows[0]["P2"], "0")) == true || adotemp.Rows[0]["P2"].ToString() == "1" ? 1 : 0);
                    this.Check43.CheckState = (CheckState)(Convert.ToBoolean(General.nz(adotemp.Rows[0]["P3"], "0")) == true || adotemp.Rows[0]["P3"].ToString() == "1" ? 1 : 0);
                    this.Check44.CheckState = (CheckState)(Convert.ToBoolean(General.nz(adotemp.Rows[0]["P4"], "0")) == true || adotemp.Rows[0]["P4"].ToString() == "1" ? 1 : 0);

                    if (!string.IsNullOrEmpty(adotemp.Rows[0]["Commercial"] + ""))
                    {
                        lblCommercial.Text = adotemp.Rows[0]["Commercial"] + "";
                    }
                    else
                    {
                        using (var tmpModAdo = new ModAdo())
                            lblCommercial.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel INNER JOIN Table1 ON Personnel.Matricule = Table1.commercial WHERE Table1.Code1='" + CodeGerant + "'");
                    }

                    using (var tmpModAdo = new ModAdo())
                        lblDepanneur.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel INNER JOIN Immeuble ON Personnel.Matricule = Immeuble.CodeDepanneur WHERE Immeuble.CodeImmeuble='" + Text14.Text + "'");
                    Text111.Text = adotemp.Rows[0]["CodeParticulier"] + "";
                    //GestionStandard.CodeParticulier
                    txtSituation.Text = adotemp.Rows[0]["Situation"] + "";
                    gsCodeTVA = adotemp.Rows[0]["CodeTVA"] + "";
                    //Code TVA de l'immeuble(TR ou TP)
                    if (string.IsNullOrEmpty(gsCodeTVA))
                    {
                        afficheMessage(Convert.ToString(2));
                    }
                    txtTRouTP.Text = gsCodeTVA;

                    //            If nz(adotemp!CodeOrigine2, "") = 12 Then 'Gérant
                    using (var tmpModAdo = new ModAdo())
                        txtFax.Text = tmpModAdo.fc_ADOlibelle("SELECT Fax FROM Table1 WHERE Nom='" + StdSQLchaine.gFr_DoublerQuote(Text2.Text) + "'");

                    using (var tmpModAdo = new ModAdo())
                        txtDestinataire.Text = tmpModAdo.fc_ADOlibelle("SELECT Gestionnaire.NomGestion FROM Gestionnaire INNER JOIN Immeuble ON Gestionnaire.CodeGestinnaire=Immeuble.CodeGestionnaire WHERE Immeuble.CodeImmeuble='" + Text14.Text + "'");

                    using (var tmpModAdo = new ModAdo())
                        txtDestinataire.Text = tmpModAdo.fc_ADOlibelle("SELECT     Gestionnaire.NomGestion FROM         Gestionnaire INNER JOIN " + " Immeuble ON Gestionnaire.CodeGestinnaire = Immeuble.CodeGestionnaire AND Gestionnaire.Code1 = Immeuble.Code1" + " WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(Text14.Text) + "'");

                    //            ElseIf nz(adotemp!CodeOrigine2, "") = 10 Then 'Copro
                    //                txtFax.Text = fc_ADOlibelle("SELECT Fax FROM ImmeublePart INNER JOIN GestionStandard ON ImmeublePart.CodeParticulier=GestionStandard.CodeParticulier WHERE GestionStandard.NumFicheStandard=" & Text1(0).Text & "")
                    //                txtDestinataire.Text = fc_ADOlibelle("SELECT Nom FROM ImmeublePart INNER JOIN GestionStandard ON ImmeublePart.CodeParticulier=GestionStandard.CodeParticulier WHERE GestionStandard.NumFicheStandard=" & Text1(0).Text & "")
                    //            ElseIf nz(adotemp!CodeOrigine2, "") = 13 Then 'Fournisseur
                    //
                    //            ElseIf nz(adotemp!CodeOrigine2, "") = 14 Then 'Gardien
                    //                txtFax.Text = fc_ADOlibelle("SELECT FaxGardien FROM Immeuble WHERE Immeuble.CodeImmeuble='" & Text14.Text & "'")
                    //                txtDestinataire.Text = fc_ADOlibelle("SELECT Gardien FROM Immeuble WHERE Immeuble.CodeImmeuble='" & Text14.Text & "'")
                    //
                    //            End If



                    //=== modif du 17 09 2018, gestion de l'adresse mail et du tel0

                    if (string.IsNullOrEmpty(txtDevisEntete25.Text) && string.IsNullOrEmpty(txtDevisEntete27.Text))
                    {
                        if (!string.IsNullOrEmpty(General.nz(adotemp.Rows[0]["CodeParticulier"], "").ToString()))
                        {
                            sSQLParticulier = "SELECT     Tel, eMail, TelPortable_IMP From ImmeublePart "
                            + " WHERE  CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(Text14.Text) + "'"
                            + " AND CodeParticulier = '" + StdSQLchaine.gFr_DoublerQuote(adotemp.Rows[0]["CodeParticulier"].ToString()) + "'";

                            rsPart = rsPartAdo.fc_OpenRecordSet(sSQLParticulier);

                            if (rsPart.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(rsPart.Rows[0]["TelPortable_IMP"].ToString()))
                                {
                                    txtDevisEntete25.Text = rsPart.Rows[0]["TelPortable_IMP"].ToString();
                                }
                                else
                                {
                                    txtDevisEntete25.Text = rsPart.Rows[0]["Tel"].ToString();
                                }
                                txtDevisEntete27.Text = rsPart.Rows[0]["Email"].ToString();
                                SSOleDBCmbMail.Text = txtDevisEntete27.Text;
                            }
                            rsPart.Dispose();
                            rsPart = null;
                        }
                        else
                        {
                            sSQLParticulier = " SELECT     Gestionnaire.CodeQualif_Qua, Gestionnaire.TelGestion, Gestionnaire.Email_GES, "
                               + " Immeuble.CodeImmeuble, Gestionnaire.Code1, Gestionnaire.TelPortable_GES "
                               + " FROM         Gestionnaire INNER JOIN "
                               + " Immeuble ON Gestionnaire.Code1 = Immeuble.Code1 AND Gestionnaire.CodeGestinnaire = Immeuble.CodeGestionnaire "
                               + " WHERE     (Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(Text14.Text) + "') AND (Gestionnaire.CodeQualif_Qua = 'part')";

                            rsPart = rsPartAdo.fc_OpenRecordSet(sSQLParticulier);

                            if (rsPart.Rows.Count > 0)
                            {
                                if (!string.IsNullOrEmpty(rsPart.Rows[0]["TelPortable_GES"].ToString()))
                                {
                                    txtDevisEntete25.Text = rsPart.Rows[0]["TelPortable_GES"].ToString();
                                }
                                else
                                {
                                    txtDevisEntete25.Text = rsPart.Rows[0]["TelGestion"].ToString();
                                }
                                txtDevisEntete27.Text = rsPart.Rows[0]["EMail_GES"].ToString();
                                SSOleDBCmbMail.Text = txtDevisEntete27.Text;
                            }
                            rsPart.Dispose();
                            rsPart = null;
                        }
                    }
                    //'=== fin modif du 17 09 2018.



                    //--recherche du particulier si il existe ou gerant par defaut.
                    //=== modif du 18 04 2014, si l'immeuble est equipé de chaudiere à condensation
                    //=== la tva passe à 5.5.
                    if (General.nz(adotemp.Rows[0]["ChaudCondensation"], 0).ToString() == "1")
                    {
                        Text9.Text = "";
                        Text9.Text = "1";
                        modAdoadotemp?.Dispose();
                        General.SQL = "select taux from typetauxtva where typetauxtva ='STR'";

                    }
                    else if (string.IsNullOrEmpty(adotemp.Rows[0]["CodeParticulier"].ToString()))
                    {
                        Text9.Text = "";
                        Text9.Text = "1";
                        modAdoadotemp?.Dispose();
                        General.SQL = "select taux from typetauxtva where typetauxtva ='" + txtTRouTP.Text + "'";
                    }
                    else
                    {
                        Text123.Text = adotemp.Rows[0]["Source"] + "";
                        modAdoadotemp?.Dispose();
                        Option2.Visible = true;
                        Option2_CheckedChanged(Option2, new EventArgs());
                        General.SQL = "select taux from typetauxtva where typetauxtva ='TR'";
                    }

                    //--recherche du taux de TVA
                    if (boolRecherche == false)
                    {
                        modAdoadotemp = new ModAdo();
                        adotemp = modAdoadotemp.fc_OpenRecordSet(General.SQL);
                        if (adotemp.Rows.Count > 0)
                        {
                            txtTaux.Text = adotemp.Rows[0]["Taux"] + "";
                            TauxTVADefaut = adotemp.Rows[0]["Taux"] + "";
                        }
                        modAdoadotemp?.Dispose();
                    }

                    //           '-- libelle du contrat Delostal.
                    //           SQL = "SELECT FacArticle.Designation1," _
                    //'           & " Contrat.Resiliee" _
                    //'           & "  FROM Contrat LEFT JOIN FacArticle ON" _
                    //'           & " Contrat.CodeArticle = FacArticle.CodeArticle" _
                    //'           & " Where Contrat.CodeImmeuble ='" & Text14 & "'" _
                    //'           & " ORDER BY Contrat.Resiliee DESC , Contrat.NumContrat DESC , Contrat.Avenant"
                    //            .Open SQL, adocnn, adOpenForwardOnly, adLockReadOnly
                    //            If Not (.BOF And .EOF) Then
                    //                If !Resiliee = True Then
                    //                    txtContrat.ForeColor = &HFF&
                    //                Else
                    //                    txtContrat.ForeColor = &H808080
                    //                End If
                    //                txtContrat = !Designation1 & ""
                    //            Else
                    //                txtContrat = ""
                    //            End If
                    modAdoadotemp?.Dispose();
                    // 2- récupération de l'adresse mail

                }

                adotemp = null;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";prcAfficheImmeuble");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Option2_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Option2_CheckedChanged() - Enter In The Function - Option2.Checked = {Option2.Checked}");
            //===> Fin Modif Mondir

            if (Option2.Checked)
            {
                Text5.Enabled = false;
                Text6.Enabled = false;
                Text7.Enabled = false;
                Text8.Enabled = false;
                Text9.Text = "";
                Text9.Text = "2";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        private void fc_RechercheContrat(string sCodeImmeuble)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_RechercheContrat() - Enter In The Function - sCodeImmeuble = {sCodeImmeuble}");
            //===> Fin Modif Mondir

            try
            {
                string LenNumContrat = null;

                //        LenNumContrat = fc_ADOlibelle("SELECT MAX(LEN(NUMCONTRAT)) AS MaxNum FROM Contrat WHERE CodeImmeuble='" & sCodeImmeuble & "'")

                //        sSQl = ""
                //        sSQl = "SELECT MAX(Contrat.NumContrat) AS MaxiNum, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1 " _
                //'                & " From Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle "
                //            sSQl = sSQl & " WHERE Contrat.CodeImmeuble like '" & sCodeImmeuble & "%' AND Avenant=0"
                //            sSQl = sSQl & " AND LEN(NumContrat)=" & nz(LenNumContrat, "''")
                //            sSQl = sSQl & " GROUP BY Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1"

                //===> Mondir le 31.08.2020, pour crrigé 
                //General.sSQL = "";
                //General.sSQL = "SELECT MAX(Contrat.DateEffet) as MaxiDate,Contrat.NumContrat AS MaxiNum, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1 " +
                //               " From Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle ";
                //General.sSQL = General.sSQL + " WHERE Contrat.CodeImmeuble like '" + sCodeImmeuble + "%' AND Avenant=0";
                //General.sSQL = General.sSQL + " GROUP BY Contrat.NumContrat, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1";
                General.sSQL = $@"SELECT MAX(Contrat.DateEffet) as MaxiDate,Contrat.NumContrat AS MaxiNum, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1, Contrat.Resiliee, Contrat.Avenant  From Contrat
                                LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle
                                WHERE Contrat.CodeImmeuble like '{sCodeImmeuble}%' --AND Avenant=0
                                GROUP BY Contrat.NumContrat, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1, Contrat.Resiliee, Contrat.Avenant
                                order by MAX(Contrat.NumContrat) DESC, Resiliee ASC";
                //Fin Modif Mondir

                using (var tmpModAdo = new ModAdo())
                {
                    General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);

                    if (General.rstmp.Rows.Count > 0)
                    {
                        txtContrat10.Text = General.rstmp.Rows[0]["Designation1"] + "";
                        txtCodeContrat.Text = General.rstmp.Rows[0]["MaxiNum"] + "";
                    }
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + "RechercheContrat");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void Debloque()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Debloque() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                DebloqueRec(this);

                //== modif rachid du 22/02/2005.
                SSOleDBCombo1Bis.ReadOnly = false;
                SSOleDBCombo1Bis.Enabled = true;

                Check40.Enabled = true;

                //Text310.AccReadOnly = true;
                Text310.AccReadOnly = false;
                Text310.BackColor = Color.White;
                Text320.AccReadOnly = true;
                Text38.AccReadOnly = true;
            }
            catch (Exception e)
            {
                Program.SaveException(e);

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message + "Fonction Debloque " + this.Name + " : " + e.Message + "\n");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="C"></param>
        private void DebloqueRec(Control C)
        {
            foreach (Control ctl_loopVariable in C.Controls)
            {
                //if (ctl_loopVariable.HasChildren)

                DebloqueRec(ctl_loopVariable);

                Control ctl = ctl_loopVariable;

                ///=======> Tested
                if (ctl is TextBox || ctl is iTalk_TextBox_Small2)
                {
                    if (ctl.Name == "txtDateImpression" || ctl.Name.Contains("Text3"))
                    {
                        if (ctl.Name.Contains("Text3"))
                        {
                            if (ctl.Tag != null)
                            {
                                var Index = Convert.ToInt32(ctl.Tag.ToString());
                                if (Index == 20 || Index == 17 || Index == 4 || Index == 3 ||
                                   Index == 2 || Index == 12 || Index == 0)
                                {
                                    var tmp = ctl as iTalk_TextBox_Small2;
                                    tmp.AccReadOnly = true;
                                    tmp.ReadOnly = true;
                                }
                                else
                                {
                                    var tmp = ctl as iTalk_TextBox_Small2;
                                    tmp.AccReadOnly = false;
                                    tmp.ReadOnly = false;
                                }
                            }
                        }
                        else
                        {
                            var tmp = ctl as iTalk_TextBox_Small2;
                            tmp.AccReadOnly = true;
                            tmp.ReadOnly = false;
                        }
                    }
                    else
                    {
                        if (ctl is iTalk_TextBox_Small2)
                        {
                            var tmp = ctl as iTalk_TextBox_Small2;
                            tmp.AccReadOnly = false;
                            tmp.ReadOnly = false;
                        }
                    }
                    //If ctl.Name = "SSOleDBCombo1Bis" Then
                    //    ctl.Locked = True
                    //End If
                }

                ///==================> Tested
                if (ctl is UltraCombo)
                {
                    ctl.Enabled = true;
                }

                ///==================> Tested
                if (ctl is UltraGrid)
                {
                    if (ctl.Name == this.SSDBGridCorps.Name)
                    {
                        var tmpControl = ctl as UltraGrid;
                        //for (i = 0; i <= (tmpControl.DisplayLayout.Bands[0].Columns.Count - 1); i += 1)
                        //{
                        //    tmpControl.DisplayLayout.Bands[0].Columns[i].CellActivation = Activation.AllowEdit;
                        //}
                        tmpControl.DisplayLayout.Bands[0].Override.AllowUpdate = DefaultableBoolean.True;
                        //Bloquage des colonnes initialement bloquées
                        tmpControl.DisplayLayout.Bands[0].Columns["Mohd"].CellActivation = Activation.ActivateOnly;
                        tmpControl.DisplayLayout.Bands[0].Columns["Mod"].CellActivation = Activation.ActivateOnly;
                        tmpControl.DisplayLayout.Bands[0].Columns["Fod"].CellActivation = Activation.ActivateOnly;
                        tmpControl.DisplayLayout.Bands[0].Columns["Std"].CellActivation = Activation.ActivateOnly;
                        tmpControl.DisplayLayout.Bands[0].Columns["TotalDebours"].CellActivation = Activation.ActivateOnly;
                        tmpControl.DisplayLayout.Bands[0].Columns["Mo"].CellActivation = Activation.ActivateOnly;
                        tmpControl.DisplayLayout.Bands[0].Columns["Fo"].CellActivation = Activation.ActivateOnly;
                        tmpControl.DisplayLayout.Bands[0].Columns["St"].CellActivation = Activation.ActivateOnly;
                        tmpControl.DisplayLayout.Bands[0].Columns["TotalVente"].CellActivation = Activation.ActivateOnly;
                    }
                }

                if (ctl is UltraDropDown || ctl is UltraCombo)
                {
                    ctl.Enabled = true;
                }

                ////================> Tested
                if (ctl is RadioButton || ctl is Button)
                {
                    ctl.Enabled = true;
                }

                ////================> Tested
                if (ctl is CheckBox)
                {
                    ctl.Enabled = true;
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="C"></param>
        /// <param name="ControlName"></param>
        /// <param name="ControlIndex"></param>
        /// <returns></returns>
        private Control FindControl(Control C, string ControlName, int ControlIndex)
        {
            if (C == null)
            {
                return null;
            }

            if (C.Tag is string && C.Name.Contains(ControlName) && ((string)C.Tag == ControlIndex.ToString() || C.Tag.ToString().Contains("ID=" + ControlIndex)))
            {
                return C;
            }

            return (from Control control in C.Controls
                    select FindControl(control, ControlName, ControlIndex)).FirstOrDefault(c => c != null);

            //if (C.Name.Contains(ControlName))
            //    if (C.Tag != null && (C.Tag.ToString() == ControlIndex.ToString() || C.Tag.ToString().Contains("ID=" + ControlIndex)))
            //    {
            //        Found = C;
            //        return;
            //    }

            //foreach (Control CC in C.Controls)
            //    if (CC.HasChildren)
            //        FindControl(CC, ControlName, ControlIndex, Found);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="bAll"></param>
        public void prcRAZDevis(bool bAll)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"prcRAZDevis() - Enter In The Function - bAll = {bAll}");
            //===> Fin Modif Mondir

            try
            {
                if (bAll)
                {
                    for (i = 0; i <= 23; i++)
                    {

                        if (i != 15)
                        {
                            if (i != 10)
                            {
                                var txt = FindControl(this, "Text1", i) as iTalk_TextBox_Small2;
                                if (txt != null)
                                    txt.Text = "";
                            }
                        }

                    }
                }
                else
                {
                    Text10.Text = "";
                    for (i = 2; i <= 23; i++)
                    {
                        if (i != 15)
                        {
                            if (i != 10)
                            {
                                var txt = FindControl(this, "Text1", i) as iTalk_TextBox_Small2;
                                if (txt != null)
                                    txt.Text = "";
                            }
                        }
                    }

                }

                for (i = 0; i <= 24; i++)
                {
                    if (i != 24)
                    {
                        //var txt = FindControl(this, "Text3", i) as iTalk_TextBox_Small2;
                        //if (txt != null)
                        //    txt.Text = "";
                    }
                }

                Text2.Text = "";
                Text10x.Text = "";
                Text14.Text = "";
                txtNoDevis.Text = "";
                SSOleDBCombo1.Text = "";
                SSOleDBCombo1Bis.Text = "";
                SSOleDBCombo2.Text = "";
                ssDbCopieA.Text = "";
                ssDbCopieABis.Text = "";
                SituaBoite.Text = "";
                Checkboite.CheckState = CheckState.Unchecked;
                //Check1 = 0
                this.Check41.CheckState = CheckState.Unchecked;
                this.Check42.CheckState = CheckState.Unchecked;
                this.Check43.CheckState = CheckState.Unchecked;
                this.Check44.CheckState = CheckState.Unchecked;
                txtdesArticle.Text = "";

                txtTitreDevis.Text = "";
                txtDateImpression.Text = "";
                Text5.Text = "";
                Text6.Text = "";

                txtDevisEntete0.Text = "";
                txtDevisEntete1.Text = "";
                txtDevisEntete2.Text = "";
                txtDevisEntete3.Text = "";
                txtDevisEntete4.Text = "";
                txtDevisEntete5.Text = "";
                txtDevisEntete6.Text = "";
                txtDevisEntete7.Text = "";
                txtDevisEntete8.Text = "";
                txtDevisEntete9.Text = "";
                txtDevisEntete10.Text = "";
                txtDevisEntete11.Text = "";
                txtDevisEntete12.Text = "";
                //=== modif du 13 02 2017, ajout des champs
                txtDevisEntete23.Text = "";
                Check47.CheckState = CheckState.Unchecked;
                Check46.CheckState = CheckState.Unchecked;
                Check48.CheckState = CheckState.Unchecked;
                Check49.CheckState = CheckState.Unchecked;
                txtDevisEntete24.Text = "";
                //txtDevisEntete(25) = ""
                DTPicker10.Text = "";
                DTPicker10.Checked = false;
                DTPicker11.Text = "";
                txtDevisEntete26.Text = "";
                //=== FIN
                txtDevisEntete13.Text = "";
                txtDevisEntete14.Text = "";
                txtDevisEntete15.Text = "";
                //=== modif du 23 07 2019.
                txtDevisEntete29.Text = "";
                txtDevisEntete28.Text = "";
                txtDevisEntete31.Text = "";
                DTPicker12.Checked = false;
                //=== fin modif du 23 07 2019.
                txtDevisEntete16.Text = "";
                txtDevisEntete17.Text = "";
                txtDevisEntete18.Text = "";
                txtDevisEntete19.Text = "";
                cmbBe.Text = "";
                //=== modif du 23 07 2019.
                ssTYD_TypeDevis.Text = "";
                ssCombo1.Text = "";
                ssCombo1.Value = "";
                //=== fin modif du 23 07 2019.

                lblLibBE.Text = "";
                Option2.Visible = false;

                DateAcceptation.Text = "";
                DateAcceptation.Checked = false;


                //'=== modif du 17 09 2018.
                txtDevisEntete25.Text = "";
                txtDevisEntete27.Text = "";
                Check410.Checked = false;
                SSOleDBCmbMail.Text = "";
                //'=== fin modif du 17 09 2018.
                txtRespTrav.Text = "";
                txtRespTrav.Value = "";

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";prcRAZDevis");
            }


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNodevis"></param>
        private void fc_NoFacture(object sNodevis)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_NoFacture() - Enter In The Function - sNodevis = {sNodevis}");
            //===> Fin Modif Mondir

            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            int lngNbFact = 0;
            try
            {
                sSQL = "SELECT  distinct  Intervention.NoFacture" + " FROM         GestionStandard INNER JOIN" + " Intervention ON GestionStandard.NumFicheStandard " + " = Intervention.NumFicheStandard" + " WHERE GestionStandard.NoDevis = '" + sNodevis + "' " + " AND ( Intervention.NoFacture IS not NULL " + " AND Intervention.NoFacture <> ''" + " AND NOT LEFT(NoFacture,2)='XX' )";

                lngNbFact = 0;
                txtNoFacture.Text = "";
                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);
                tSqlCrystal = null;
                foreach (DataRow rsRow in rs.Rows)
                {
                    txtNoFacture.Text = txtNoFacture.Text + rsRow["NoFacture"] + "" + "\n";
                    Array.Resize(ref tSqlCrystal, lngNbFact + 1);
                    tSqlCrystal[lngNbFact] = rsRow["NoFacture"] + "";
                    lngNbFact = lngNbFact + 1;
                }

                if (lngNbFact >= 1)
                {
                    btnFacture.BackColor = ColorTranslator.FromOle(0xc0ffc0);
                    if (lngNbFact > 1)
                    {
                        btnFacture.Text = lngNbFact + " Factures";
                    }
                    else
                    {
                        btnFacture.Text = "1 Facture";
                    }
                }
                else
                {
                    btnFacture.BackColor = ColorTranslator.FromOle(0xffc0c0);
                    btnFacture.Text = "Facture";
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);

                modAdors?.Dispose();
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        private void FournisgrilleNoDevis()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"FournisgrilleNoDevis() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                modAdoadotemp = new ModAdo();
                General.SQL = "SELECT DevisDetail.* From DevisDetail Where DevisDetail.NumeroDevis ='" + Text11.Text + "' order by numeroligne";
                (SSDBGridCorps.DataSource as DataTable).Rows.Clear();
                fournisGrille(false);
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";FournisgrilleNoDevis");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheArticle0_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRechercheArticle0_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                string requete = "SELECT FacArticle.CodeArticle as \"Code Article\" , FacArticle.Designation1 as \"Designation1\", FacArticle.Designation2 as \"Designation2\" " +
                                 ", CodeCategorieArticle as \"Catégorie article\" FROM FacArticle";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche du titre" };
                fg.SetValues(new Dictionary<string, string> { { "CodeArticle", txtdesArticle.Text }, { "CodeCategorieArticle", "D" } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtdesArticle.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    if (string.IsNullOrEmpty(txtTitreDevis.Text))
                    {
                        txtTitreDevis.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    }
                    Text322.Text = txtTitreDevis.Text;
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtdesArticle.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        if (string.IsNullOrEmpty(txtTitreDevis.Text))
                        {
                            txtTitreDevis.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        }
                        Text322.Text = txtTitreDevis.Text;
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheArticle_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheCodeArticle_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRechercheCodeArticle_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {

                var tmpControl = sender as Button;
                var index = Convert.ToInt32(tmpControl.Tag);

                if (index == 0)
                {
                    string requete = "SELECT FacArticle.CodeArticle as \"Code Article\" , FacArticle.Designation1 as \"Designation1\"," +
                 " FacArticle.Designation2 as \"Designation2\" FROM FacArticle";
                    string where_order = " CodeCategorieArticle = 'D'";
                    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche du titre" };
                    fg.SetValues(new Dictionary<string, string> { { "CodeArticle", txtFiltreArticle.Text } });
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        txtFiltreArticle.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtFiltreArticle_KeyPress(txtFiltreArticle, new System.Windows.Forms.KeyPressEventArgs((char)13));
                        fg.Dispose();
                        fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            txtFiltreArticle.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            txtFiltreArticle_KeyPress(txtFiltreArticle, new System.Windows.Forms.KeyPressEventArgs((char)13));
                            fg.Dispose();
                            fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();
                }
                else if (index == 1)
                {
                    string requete = "SELECT     Code as \"Code\", Libelle as \"Libelle\" FROM  ModeReglement";
                    string where_order = "";
                    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche du mode de réglement" };
                    fg.SetValues(new Dictionary<string, string> { { "CodeArticle", txtFiltreArticle.Text } });
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        txtDevisEntete28.Text = fg.ugResultat.ActiveRow.Cells["code"].Value.ToString();
                        txtDevisEntete31.Text = fg.ugResultat.ActiveRow.Cells["Libelle"].Value.ToString();
                        txtFiltreArticle_KeyPress(txtFiltreArticle, new System.Windows.Forms.KeyPressEventArgs((char)13));
                        fg.Dispose();
                        fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            txtDevisEntete28.Text = fg.ugResultat.ActiveRow.Cells["code"].Value.ToString();
                            txtDevisEntete31.Text = fg.ugResultat.ActiveRow.Cells["Libelle"].Value.ToString();
                            txtFiltreArticle_KeyPress(txtFiltreArticle, new System.Windows.Forms.KeyPressEventArgs((char)13));
                            fg.Dispose();
                            fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();
                }


            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheCodeArticle_Click");
            }
        }

        private void fc_ModeReglement()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ModeReglement() - Enter In The Function");
            //===> Fin Modif Mondir

            string sSQL = "";

            try
            {
                sSQL = "SELECT     Libelle From ModeReglement WHERE  Code = '" + StdSQLchaine.gFr_DoublerQuote(txtDevisEntete28.Text) + "'";
                using (var tmpModAdo = new ModAdo())
                    sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                if (string.IsNullOrEmpty(sSQL))
                    txtDevisEntete31.Text = "";
                else
                    txtDevisEntete31.Text = sSQL;


            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFiltreArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtFiltreArticle_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short sh = (short)e.KeyChar;
            if (sh == 13)
                sub_FiltreArticle();
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void sub_FiltreArticle()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"sub_FiltreArticle() - Enter In The Function");
            //===> Fin Modif Mondir

            string sqlWhere = null;
            General.sSQL = "SELECT FacArticle.CodeArticle, FacArticle.Designation1,";
            General.sSQL = General.sSQL + " FacArticle.PrixAchat, FacArticle.MOtps, FacArticle.FO,";
            General.sSQL = General.sSQL + " FacArticle.ST, FacArticle.CodeCategorieArticle,";
            General.sSQL = General.sSQL + " FacArticle.Designation2, FacArticle.Designation3,";
            General.sSQL = General.sSQL + " FacArticle.Designation4, FacArticle.Designation5,";
            General.sSQL = General.sSQL + " FacArticle.Designation6, FacArticle.Designation7,";
            General.sSQL = General.sSQL + " FacArticle.Designation8, FacArticle.Designation9,";
            General.sSQL = General.sSQL + " FacArticle.Designation10";
            General.sSQL = General.sSQL + " FROM FacArticle ";
            sqlWhere = "WHERE CodeCategorieArticle = 'D'";

            if (!string.IsNullOrEmpty(txtFiltreArticle.Text))
            {
                sqlWhere = sqlWhere + " AND FacArticle.CodeArticle LIKE '%" + txtFiltreArticle.Text.Replace("*", "%").Replace("'", "''") + "%'";
            }

            if (!string.IsNullOrEmpty(txtFiltreDesignationArticle.Text))
            {
                sqlWhere = sqlWhere + " AND FacArticle.Designation1 LIKE '%" + txtFiltreDesignationArticle.Text.Replace("*", "%").Replace("'", "''") + "%'";
            }

            General.sSQL = General.sSQL + sqlWhere + " order by CodeArticle";

            if (modAdorsFacArticle == null)
            {
                modAdorsFacArticle = new ModAdo();
            }
            rsFacArticle = modAdorsFacArticle.fc_OpenRecordSet(General.sSQL);

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            ssGridFacArticle.Visible = false;
            ssGridFacArticle.DataSource = rsFacArticle;
            ssGridFacArticle.Visible = true;
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheCodeArticleArticle_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRechercheCodeArticleArticle_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                string requete = "SELECT FacArticle.CodeArticle as \"Code Article\" , FacArticle.Designation1 as \"Designation1\", FacArticle.Designation2 as \"Designation2\" FROM FacArticle";
                string where_order = " CodeCategorieArticle = 'D'";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un article" };
                fg.SetValues(new Dictionary<string, string> { { "Designation1", txtFiltreDesignationArticle.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtFiltreDesignationArticle.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    txtFiltreDesignationArticle_KeyPress(txtFiltreDesignationArticle, new System.Windows.Forms.KeyPressEventArgs((char)13));
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtFiltreDesignationArticle.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        txtFiltreDesignationArticle_KeyPress(txtFiltreDesignationArticle, new System.Windows.Forms.KeyPressEventArgs((char)13));
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheCodeArticleArticle_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFiltreDesignationArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtFiltreDesignationArticle_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                sub_FiltreArticle();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheCodePoste1_Click(object sender, EventArgs e)
        {
            var btn = sender as Button;
            int Index = Convert.ToInt32(btn.Tag);

            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRechercheCodePoste1_Click() - Enter In The Function - Index = {Index}");
            //===> Fin Modif Mondir

            try
            {
                string requete = "SELECT  SousFamilleArticle.CodeSousFamille AS \"Code Poste\", SousFamilleArticle.IntituleSousFamille AS \"Intitule\" FROM SousFamilleArticle";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un poste" };
                if (Index == 1)
                {
                    fg.SetValues(new Dictionary<string, string> { { "CodeSousFamille", txtFiltrePoste.Text } });
                }
                else if (Index == 0)
                {
                    fg.SetValues(new Dictionary<string, string> { { "IntituleSousFamille", txtFiltreLibellePoste.Text } });
                }
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    if (Index == 0)
                    {
                        txtFiltrePoste.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    }
                    else if (Index == 1)
                    {
                        txtFiltreLibellePoste.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    }
                    sub_FiltrePoste();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        if (Index == 0)
                        {
                            txtFiltrePoste.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        }
                        else if (Index == 1)
                        {
                            txtFiltreLibellePoste.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        }
                        sub_FiltrePoste();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheCodePoste_Click Index:" + Index + ";");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void sub_FiltrePoste()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"sub_FiltrePoste() - Enter In The Function");
            //===> Fin Modif Mondir

            string sqlWhere = null;
            General.sSQL = "SELECT  SousFamilleArticle.CodeSousFamille, SousFamilleArticle.IntituleSousFamille";
            General.sSQL = General.sSQL + " FROM SousFamilleArticle ";
            sqlWhere = "";
            if (!string.IsNullOrEmpty(txtFiltrePoste.Text))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = "WHERE SousFamilleArticle.CodeSousFamille LIKE '%" + txtFiltrePoste.Text.Replace("*", "%").Replace("'", "''") + "%'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND SousFamilleArticle.CodeSousFamille LIKE '%" + txtFiltrePoste.Text.Replace("*", "%").Replace("'", "''") + "%'";
                }
            }

            if (!string.IsNullOrEmpty(txtFiltreLibellePoste.Text))
            {
                if (string.IsNullOrEmpty(sqlWhere))
                {
                    sqlWhere = "WHERE SousFamilleArticle.IntituleSousFamille LIKE '%" + txtFiltreLibellePoste.Text.Replace("*", "%").Replace("'", "''") + "%'";
                }
                else
                {
                    sqlWhere = sqlWhere + " AND SousFamilleArticle.IntituleSousFamille LIKE '%" + txtFiltreLibellePoste.Text.Replace("*", "%").Replace("'", "''") + "%'";
                }
            }


            General.sSQL = General.sSQL + sqlWhere + " ORDER BY SousFamilleArticle.CodeSousFamille";

            if (modAdorsSousFamilleArticle == null)
            {
                modAdorsSousFamilleArticle = new ModAdo();
            }
            rsSousFamilleArticle = modAdorsSousFamilleArticle.fc_OpenRecordSet(General.sSQL);

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            GridSousFamilleArticle.Visible = false;
            GridSousFamilleArticle.DataSource = rsSousFamilleArticle;
            GridSousFamilleArticle.Visible = true;
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

        }

        /// <summary>
        /// Control Is Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercherDtandard_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRechercherDtandard_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                string requete = "SELECT " + "NumFicheStandard as \"Numero de fiche:\", CodeImmeuble as \"Ref immeuble:\" From GestionStandard";
                string where_order = " DdeDeDevis = true ";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des fiches standard" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    General.SQL = "SELECT * FROM GestionStandard WHERE NumFicheStandard='" + fg.ugResultat.ActiveRow.Cells[1].Value + "'";
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        General.SQL = "SELECT * FROM GestionStandard WHERE NumFicheStandard='" + fg.ugResultat.ActiveRow.Cells[1].Value + "'";
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();

                Text10.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                Text10_KeyPress(Text10, new System.Windows.Forms.KeyPressEventArgs((char)0));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercherDtandard_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRetour_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRetour_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            if (valider() == true)
            {
                View.Theme.Theme.Navigate(typeof(UserDispatchDevis));
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdTypeREglement_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdTypeREglement_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                string requete = "SELECT CodeReglement as \"code\"," + "  LibelleReglement as \"libelle\",  TexteReglement as \"texte\"" + " From TypeReglementDevis";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un mode de règlement" };
                fg.ugResultat.InitializeLayout += (se, ev) =>
                {
                    fg.ugResultat.DisplayLayout.Bands[0].Columns[1].Hidden = true;
                };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    SSOleDBCombo1Bis.Text = fg.ugResultat.ActiveRow.Cells["texte"].Value.ToString();
                    SSOleDBCombo1.Text = fg.ugResultat.ActiveRow.Cells["code"].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        SSOleDBCombo1Bis.Text = fg.ugResultat.ActiveRow.Cells["texte"].Value.ToString();
                        SSOleDBCombo1.Text = fg.ugResultat.ActiveRow.Cells["code"].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdTypeREglement_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdVersion_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdVersion_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                object sNewNumDevis = null;
                int ExsitsNewNumDevis = 0;
                sNewNumDevis = General.fncNumDevis(true, Text11.Text, Text14.Text);
                //vérification si le numero devis a été déjà dupliqué  pour corrigier le bug https://groupe-dt.mantishub.io/view.php?id=1837
                using (var tmpAdo = new ModAdo())
                {
                    ExsitsNewNumDevis = Convert.ToInt32(General.nz(tmpAdo.fc_ADOlibelle("select count(NumeroDevis) from DevisEntete where   NumeroDevis = '" + sNewNumDevis + "'"), 0));
                }
                if (ExsitsNewNumDevis > 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce devis a déjà été dupliqué, si vous voulez le dupliquer à nouveau rendez vous sur le devis N° " + sNewNumDevis + " et cliquez sur Version +", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                var xx = General.Execute("DELETE FROM DevisEnteteTemp "
                    + " WHERE (NumeroDevis ='" + Text11.Text + "')");

                //recopie du devis avec la nouvelle version

                //recopie de l'entete

                xx = General.Execute("INSERT INTO DevisEnteteTemp "
                    + " SELECT DevisEntete.* FROM DevisEntete WHERE (NumeroDevis ='" + Text11.Text + "')");

                //recopie du corps

                xx = General.Execute("INSERT INTO DevisDetailTemp "
                    + " SELECT DevisDetail.* FROM DevisDetail WHERE (NumeroDevis ='" + Text11.Text + "')");

                //mise a jour du code immeuble dans la table temporaire

                xx = General.Execute("UPDATE DevisEnteteTemp SET NumeroDevis ='" + sNewNumDevis + "', "
                    + "Annee='" + DateTime.Now.Year + "', Mois='" + DateTime.Now.Month + "', DateCreation='"
                    + DateTime.Now.ToString(General.FormatDateSansHeureSQL) + " "
                    + DateTime.Now.ToString("HH:mm:ss") + "'"
                    + " ,ordre ='000' WHERE (NumeroDevis ='" + Text11.Text + "')");

                //copie temporaire vers entete

                xx = General.Execute("INSERT INTO DevisEntete SELECT DevisEnteteTemp.*"
                    + " FROM DevisEnteteTemp WHERE (NumeroDevis ='" + sNewNumDevis + "')");

                //copie temporaire vers corps

                xx = General.Execute("INSERT INTO DevisDetail SELECT DevisDetailTemp.*"
                    + " FROM DevisDetailTemp WHERE (NumeroDevis ='" + sNewNumDevis + "')");

                //effacement temporaire

                xx = General.Execute("DELETE FROM DevisEnteteTemp "
                    + " WHERE (NumeroDevis ='" + sNewNumDevis + "')");

                xx = General.Execute("UPDATE DevisEntete set DateImpression= null, CodeEtat='00' WHERE (NumeroDevis ='" + sNewNumDevis + "')");
                //affichage du nouveau devis
                Text11.Text = sNewNumDevis.ToString();
                txtNoDevis.Text = sNewNumDevis.ToString();
                SSetat.Value = "00";

                Debloque();
                txtDateImpression.Text = "";
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);

                modAdoadotemp?.Dispose();
                adotemp = modAdoadotemp.fc_OpenRecordSet("SELECT DevisEntete.NumeroDevis" +
                                                         " FROM DevisEntete where numerodevis='" + txtNoDevis.Text +
                                                         "'");
                if (adotemp.Rows.Count > 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il éxiste déja  une version plus récente", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                }
                modAdoadotemp?.Dispose();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdVideo_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdVideo_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            string sNameFile = null;
            int j = 0;
            string[] tName = null;
            string sCodeImmeuble = null;
            try
            {
                sCodeImmeuble = Text14.Text;

                frmAfficheFichier frmAfficheFichier = new frmAfficheFichier();

                frmAfficheFichier.LstFichier.Items.Clear();

                Video.fc_RecherheVideo(sCodeImmeuble, ref sNameFile, false, true);

                if (!string.IsNullOrEmpty(sNameFile))
                {
                    tName = sNameFile.Split(';');
                    for (j = 0; j < tName.Length - 1; j++)
                    {
                        frmAfficheFichier.LstFichier.Items.Add(General.sCheminDossier + "\\" + sCodeImmeuble + "\\Video\\" + tName[j]);


                    }
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le système n'a pas trouvé de vidéo pour l'immeuble " + sCodeImmeuble + ".", "Aucun résultat", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                frmAfficheFichier.lblTexte.Text = "Le système à trouvé " + j + " vidéo(s)." + "\n" + "Veuillez double cliquer sur un des fichier pour la visualiser.";
                frmAfficheFichier.ShowDialog();
                frmAfficheFichier.Close();
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";cmdVideo_Click");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdVisu_Click(object sender, EventArgs e)
        {
            //===> Mondir le 12.02.2021, pour corriger https://groupe-dt.mantishub.io/view.php?id=2238
            cmdVisu.Enabled = false;
            Cursor = Cursors.WaitCursor;
            //===> Fin Modif Mondir

            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdVisu_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            double dbTauxTva = 0;
            string sReturn = null;

            try
            {
                ModCourrier.EnvoieMail = false;

                //=== date de création.
                if (General.IsDate(Text122.Text))
                {
                    //
                    //If CDate(Text1(22)) >= #12/30/2013# Then
                    if (Convert.ToDateTime(Text122.Text) >= Convert.ToDateTime("01/01/2014"))
                    {
                        dbTauxTva = 10;
                    }
                    else if (Convert.ToDateTime(Text122.Text) > Convert.ToDateTime("01/01/2012"))
                    {
                        dbTauxTva = 7;

                    }
                    else if (General.IsNumeric(txtTaux.Text))
                    {
                        dbTauxTva = Convert.ToDouble(txtTaux.Text);
                    }
                    else
                    {
                        dbTauxTva = 5.5;

                    }
                }

                //adocnn.Execute "UPDATE DevisDetail SET CodeTva =" & dbTauxTva & " WHERE NumeroDevis ='" & Text1(1) & "'" _
                //& " and (codetva is null  or codetva = 0)"

                //=== ,tva en autoliquidation.
                int xx = General.Execute("UPDATE DevisDetail SET CodeTva =" + dbTauxTva + " WHERE NumeroDevis ='" + Text11.Text + "'" + " and (codetva is null)");


                //adocnn.Execute "UPDATE DevisDetail SET CodeTva = 5.5 WHERE NumeroDevis ='" & Text1(1) & "'" _
                //& " and (codetva is null  or codetva = 0)"


                if (OptDebourse.Checked == true)
                {
                    prcImpression(General.strDTDebourse, 0, Convert.ToInt16(Text4.Text));

                }
                else if (OptDebourseAff.Checked == true)
                {
                    prcImpression(General.strDTDebourseAffaire, 0, Convert.ToInt16(Text4.Text));

                }
                else if (OPtRecap.Checked == true)
                {
                    prcImpression(General.strDTDevisRecap, 0, Convert.ToInt16(Text4.Text), false, Convert.ToDateTime(Text122.Text));

                }
                else
                {
                    General.tabParametresDevis = new General.ParamDevis[3];
                    ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[3];


                    //        prcImpression strDTDevis, 0, CInt(Text4)
                    if (OptDescriptif.Checked == true)
                    {
                        General.tabParametresDevis = new General.ParamDevis[3];

                        General.tabParametresDevis[0].sName = "Parametre";
                        General.tabParametresDevis[0].sValue = "Descriptif";

                        General.tabParametresDevis[1].sName = "MasqueSomme ";
                        General.tabParametresDevis[1].sValue = "M";

                        if (General.sExportPdf == "1")
                        {
                            ModCrystalPDF.tabFormulas[0].sNom = "Parametre";
                            ModCrystalPDF.tabFormulas[0].sCritere = "Descriptif";
                            ModCrystalPDF.tabFormulas[0].sType = devModeCrystal.cExportChaine;

                            ModCrystalPDF.tabFormulas[1].sNom = "MasqueSomme ";
                            ModCrystalPDF.tabFormulas[1].sCritere = "M";
                            ModCrystalPDF.tabFormulas[1].sType = devModeCrystal.cExportChaine;

                        }

                    }
                    ////==================> Tested
                    else if (optForfaitaireAff.Checked == true)
                    {

                        General.tabParametresDevis[0].sName = "Parametre";
                        General.tabParametresDevis[0].sValue = "";

                        General.tabParametresDevis[1].sName = "MasqueSomme ";
                        General.tabParametresDevis[1].sValue = "M";

                        if (General.sExportPdf == "1")
                        {
                            ModCrystalPDF.tabFormulas[0].sNom = "Parametre";
                            ModCrystalPDF.tabFormulas[0].sCritere = "";
                            ModCrystalPDF.tabFormulas[0].sType = devModeCrystal.cExportChaine;

                            ModCrystalPDF.tabFormulas[1].sNom = "MasqueSomme ";
                            ModCrystalPDF.tabFormulas[1].sCritere = "M";
                            ModCrystalPDF.tabFormulas[1].sType = devModeCrystal.cExportChaine;
                        }

                    }
                    else if (optForfaitairePos.Checked == true)
                    {

                        General.tabParametresDevis[0].sName = "Parametre";
                        General.tabParametresDevis[0].sValue = "";

                        General.tabParametresDevis[1].sName = "MasqueSomme ";
                        General.tabParametresDevis[1].sValue = "";

                        if (General.sExportPdf == "1")
                        {
                            ModCrystalPDF.tabFormulas[0].sNom = "Parametre";
                            ModCrystalPDF.tabFormulas[0].sCritere = "";
                            ModCrystalPDF.tabFormulas[0].sType = devModeCrystal.cExportChaine;

                            ModCrystalPDF.tabFormulas[1].sNom = "MasqueSomme ";
                            ModCrystalPDF.tabFormulas[1].sCritere = "";
                            ModCrystalPDF.tabFormulas[1].sType = devModeCrystal.cExportChaine;

                        }

                    }
                    else if (optQuantitatif.Checked == true)
                    {

                        General.tabParametresDevis[0].sName = "Parametre";
                        General.tabParametresDevis[0].sValue = "";

                        General.tabParametresDevis[1].sName = "MasqueSomme ";
                        General.tabParametresDevis[1].sValue = "Q";

                        if (General.sExportPdf == "1")
                        {
                            ModCrystalPDF.tabFormulas[0].sNom = "Parametre";
                            ModCrystalPDF.tabFormulas[0].sCritere = "";
                            ModCrystalPDF.tabFormulas[0].sType = devModeCrystal.cExportChaine;

                            ModCrystalPDF.tabFormulas[1].sNom = "MasqueSomme ";
                            ModCrystalPDF.tabFormulas[1].sCritere = "Q";
                            ModCrystalPDF.tabFormulas[1].sType = devModeCrystal.cExportChaine;
                        }

                    }

                    // ===== A EFFACER.
                    //           Dim xx           As String
                    //           Dim rsxx         As adodb.Recordset
                    //
                    //           xx = "SELECT  50    NumeroDevis, CodeEtat, Annee" _
                    //'                & " From DevisEnTete " _
                    //'                & " WHERE     (CodeEtat = N'04') " _
                    //'                & " ORDER BY NumeroDevis DESC"
                    //
                    //           Set rsxx = fc_OpenRecordSet(xx)
                    //
                    //           Do While rsxx.EOF = False
                    //
                    //               fc_ShowDevis (Text1(1).Text)
                    //               rsxx.MoveNext
                    //           Loop
                    //           Exit Sub


                    sReturn = General.fc_ShowDevis(Text11.Text);

                    ///====================> Tested
                    if (General.sExportPdf == "1" && !string.IsNullOrEmpty(sReturn))
                    {

                        ModuleAPI.Ouvrir(sReturn);
                        Cursor.Current = Cursors.Arrow;

                    }

                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";cmdVisu_Click");
            }

            //===> Mondir le 12.02.2021, pour corriger https://groupe-dt.mantishub.io/view.php?id=2238
            Cursor = Cursors.Default;
            cmdVisu.Enabled = true;
            //===> Fin Modif Mondir
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdWave_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdWave_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            bool boolFolder = false;
            bool boolMp3 = false;
            string sMp3 = null;

            try
            {
                Cursor.Current = Cursors.Default;
                if (!string.IsNullOrEmpty(lblWave.Text))
                {
                    //== modifie l'extension wav en mp3 et ouvre le fichier si existant.
                    sMp3 = General.Mid(lblWave.Text, 1, lblWave.Text.Length - 3) + "mp3";
                    // sMp3 = txtWave
                    boolMp3 = File.Exists(sMp3);
                    if (boolMp3 == true)
                    {
                        ModuleAPI.Ouvrir(sMp3);
                        return;
                    }
                    else
                    {

                        boolFolder = File.Exists(lblWave.Text);
                        if (boolFolder == true)
                        {
                            ModuleAPI.Ouvrir(lblWave.Text);
                            return;
                        }
                    }
                    if (boolFolder == false && boolMp3 == false)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le fichier son a été déplacé ou supprimé.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    ModuleAPI.Ouvrir(sMp3);
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun rapport d'intervention vocal n'est associé a cette intervention", "Document non trouvé", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";cmdWave_Click");
            }
        }

        /// <summary>
        /// Controle is Allways Disabled
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DateAcceptation_Validating(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"DateAcceptation_Validating() - Enter In The Function");
            //===> Fin Modif Mondir

            string SqlAppelValid = null;

            try
            {
                if (!string.IsNullOrEmpty(DateAcceptation.Value.ToString()) && DateAcceptation.Checked)
                {
                    General.Execute("UPDATE gestionstandard set DateDevis ='" +
                                    DateAcceptation.Value.ToString(General.FormatDateSQL) + "'" + " where nodevis='" + Text11.Text + "'");
                }

                SqlAppelValid = "SELECT GestionStandard.NumFicheStandard FROM GestionStandard WHERE GestionStandard.NoDevis='" + Text11.Text + "'";

                using (var tmpModAdo = new ModAdo())
                    txtAppelValid.Text = tmpModAdo.fc_ADOlibelle(SqlAppelValid);
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";DateAcceptation_Validate");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileImmeuble_DoubleClick(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"FileImmeuble_DoubleClick() - Enter In The Function");
            //===> Fin Modif Mondir

            if (FileImmeuble.SelectedIndex != (-1))
            {
                ModuleAPI.Ouvrir(FileImmeuble.Items[FileImmeuble.SelectedIndex].ToString());
            }
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocDevis_VisibleChanged(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"UserDocDevis_VisibleChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!Visible)
            {
                RecVisibleChanged(this);

                General.tabParametresDevis = new General.ParamDevis[2];

                //=== modif rachid du 10 mai 2007
                ///Dim objcontrole    As Object
                ///Dim blnSaveGrid     As Boolean
                ///
                /// For Each objcontrole In Me.Controls
                ///    If TypeOf objcontrole Is SSOleDBGrid Then
                ///        If GetSetting(cFrNomApp, Me.Name, "SaveGrid", "0") = "0" Then
                ///            blnSaveGrid = True
                ///        End If
                ///        fc_SavDimensionGrille Me.Name, objcontrole
                ///        If blnSaveGrid = True Then
                ///            SaveSetting cFrNomApp, Me.Name, "SaveGrid", "0"
                ///        End If
                ///    End If
                /// Next
                /// If blnSaveGrid = True Then
                ///    SaveSetting cFrNomApp, Me.Name, "SaveGrid", "1"
                ///End If


                ModParametre.fc_CloseConnGecet();
                General.fc_UnloadAllForms();

                ModDevis.CommitTransaction();

            }
            else
            {
                //var adoCorps = new SqlConnection(General.adocnn.ConnectionString);
                //adoCorps.Open();

                //SqlTransaction t1 = adoCorps.BeginTransaction(IsolationLevel.Serializable, "");
                //t1.

                //SqlCommand CMD = new SqlCommand(@"SET TRANSACTION ISOLATION LEVEL SERIALIZABLE;
                //                                    BEGIN TRANSACTION
                //                                    SELECT * FROM DevisDetail WHERE NumeroDevis = '202011476'", adoCorps);
                //CMD.ExecuteNonQuery();

                //adoCorps.Close();


                //CMD = new SqlCommand("COMMIT TRANSACTION Trans1_MONDIR", adoCorps);
                //CMD.ExecuteNonQuery();

                View.Theme.Theme.MainForm.mainMenu1.ShowHomeMenu("UserDocClient");

                var tmpModAdoX = new ModAdo();
                //var DT = tmpModAdoX.fc_OpenRecordSet("SELECT * FROM DevisDetail WHERE NumeroDevis = '0'");
                var SSDBGridCorpsSource = new DataTable();
                SSDBGridCorpsSource.Columns.Add("numerodevis");
                SSDBGridCorpsSource.Columns.Add("NumeroLigne");
                SSDBGridCorpsSource.Columns.Add("CodeFamille");
                SSDBGridCorpsSource.Columns.Add("CodeSousFamille");
                SSDBGridCorpsSource.Columns.Add("CodeArticle");
                SSDBGridCorpsSource.Columns.Add("CodeSousArticle");
                SSDBGridCorpsSource.Columns.Add("NumOrdre");
                SSDBGridCorpsSource.Columns.Add("TexteLigne");
                SSDBGridCorpsSource.Columns.Add("Quantite");
                SSDBGridCorpsSource.Columns.Add("Unite");
                SSDBGridCorpsSource.Columns.Add("MOhud");
                SSDBGridCorpsSource.Columns.Add("CodeMO");
                SSDBGridCorpsSource.Columns.Add("PxHeured");
                SSDBGridCorpsSource.Columns.Add("FOud");
                SSDBGridCorpsSource.Columns.Add("STud");
                SSDBGridCorpsSource.Columns.Add("MOhd");
                SSDBGridCorpsSource.Columns.Add("MOd");
                SSDBGridCorpsSource.Columns.Add("FOd");
                SSDBGridCorpsSource.Columns.Add("STd");
                SSDBGridCorpsSource.Columns.Add("TotalDebours");
                SSDBGridCorpsSource.Columns.Add("kMO");
                SSDBGridCorpsSource.Columns.Add("MO");
                SSDBGridCorpsSource.Columns.Add("kFO");
                SSDBGridCorpsSource.Columns.Add("FO");
                SSDBGridCorpsSource.Columns.Add("kST");
                SSDBGridCorpsSource.Columns.Add("ST");
                SSDBGridCorpsSource.Columns.Add("TotalVente");
                SSDBGridCorpsSource.Columns.Add("Coef1");
                SSDBGridCorpsSource.Columns.Add("Coef2");
                SSDBGridCorpsSource.Columns.Add("Coef3");
                SSDBGridCorpsSource.Columns.Add("CodeTVA");
                SSDBGridCorpsSource.Columns.Add("TotalVenteApresCoef");
                SSDBGridCorpsSource.Columns.Add("Facturer");
                SSDBGridCorpsSource.Columns.Add("DateFacture");
                SSDBGridCorpsSource.Columns.Add("NFamille");
                SSDBGridCorpsSource.Columns.Add("NSFamille");
                SSDBGridCorpsSource.Columns.Add("NArticle");
                SSDBGridCorpsSource.Columns.Add("NSousArticle");
                SSDBGridCorpsSource.Columns.Add("Fournisseur");
                SSDBGridCorpsSource.Columns.Add("SautPage");
                SSDBGridCorps.DataSource = SSDBGridCorpsSource;
                InitColumnProps();

                try
                {
                    short i = 0;
                    string strNodevis = null;
                    string strAncProg = null;
                    string strAncVar = null;
                    string sSQLDroit = null;
                    object objcontrole = null;
                    int lnbF = 0;


                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;


                    FileImmeublePattern = "*.bmp;*.jpg;*.jpeg;*.pcx;*.tif";
                    //    sstab1.TabVisible(6) = False 'Divers
                    //    'sstab1.TabVisible(7) = False 'Dossier
                    //    sstab1.TabsPerRow = 7
                    //    sstab1.Tab = 0

                    fc_Affichage();

                    //--- code état 04 Termine
                    //SSDBGridCorps.StyleSets["Lock"].BackColor = 0xc0c0c0;
                    //SSDBGridCorps.StyleSets["Lock"].ForeColor = 0xffffff;


                    General.open_conn();

                    DateAcceptation.Enabled = false;
                    DateAcceptation.Checked = false;

                    //TODO : Mondir - This Condition For To Set The Company Logo
                    //if (!string.IsNullOrEmpty(General.LogoSociete) | !Information.IsDBNull(General.LogoSociete))
                    //{
                    //    imgLogoSociete.Image = System.Drawing.Image.FromFile(General.LogoSociete);
                    //    if (Information.IsNumeric(General.imgTop))
                    //    {
                    //        imgLogoSociete.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(General.imgTop);
                    //    }
                    //    if (Information.IsNumeric(General.imgLeft))
                    //    {
                    //        imgLogoSociete.Left = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsX(General.imgLeft);
                    //    }
                    //}

                    if (!string.IsNullOrEmpty(General.NomSousSociete))
                    {
                        //TODO : This Line To Set Company Name
                        //lblNomSociete.Text = General.NomSousSociete;
                        System.Windows.Forms.Application.DoEvents();
                    }
                    SSTab1.SelectedTab = SSTab1.Tabs[0];

                    General.gsUtilisateur = General.fncUserName();

                    // initialise l'envoie d'email.
                    //    If UCase(gsUtilisateur) <> "LUDOVIC" Then
                    //initialiseComboMail
                    //    End If



                    cmdRechercherDtandard.Enabled = false;
                    cmdVersion.Enabled = false;
                    cmdMAJ.Enabled = false;
                    cmdSupprimer.Enabled = false;
                    txtDeviseur.Text = "";
                    ssDbCopieA.Text = "";
                    General.NomImmeuble = "";
                    var _with117 = this;
                    //- positionne sur le dernier enregistrement.
                    if (ModParametre.fc_RecupParam(this.Name) == true)
                    {
                        // charge les enregistremants
                        // si Newvar contient FICHEAPPEL alors
                        if (ModParametre.sFicheAppelante == Variable.cUserDocStandard)
                        {
                            fc_ChargeEnregistrement(ModParametre.sNewVar, true);
                            //Faire apparaître un historique des devis pour l'immeuble
                            //à voir pour faire apparaître un devis pour le gerant
                            General.NomImmeuble = Text14.Text;
                            frmHistoDevis frmHistoDevis = new frmHistoDevis();
                            frmHistoDevis.ShowDialog();
                        }
                        else
                        {
                            fc_ChargeEnregistrement(ModParametre.sNewVar);
                        }

                    }

                    //==== controle si il existe un fichier dans le dossier video
                    //===============> Teested
                    if (!string.IsNullOrEmpty(Text14.Text))
                    {
                        string tmp = "";
                        lnbF = Video.fc_RecherheVideo(Text14.Text, ref tmp, true);
                        if (lnbF > 0)
                        {
                            cmdVideo.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0ffc0);
                        }
                        else
                        {
                            cmdVideo.BackColor = System.Drawing.ColorTranslator.FromOle(0xffc0c0);
                        }
                    }

                    if (General.fc_InitRegGrilles(Variable.cUserDocDevis) == true)
                    {
                        RecFunction(this);
                    }
                    else
                    {
                        RecFunction2(this);
                    }

                    var SSDBDropDown1Source = new DataTable();
                    SSDBDropDown1Source.Columns.Add("column 0");
                    SSDBDropDown1Source.Rows.Add("E");
                    SSDBDropDown1Source.Rows.Add("S");

                    SSDBDropDown1.DataSource = SSDBDropDown1Source;
                    SSDBDropDown1.ValueMember = "column 0";



                    cmbUnite.ValueMember = "Unite";
                    cmbUnite_BeforeDropDown(cmbUnite, null);

                    ModParametre.fc_OpenConnGecet();
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                    //TODO : Mondir - Check This Navigation Links
                    //lblNavigation[0].Text = General.sNomLien0;
                    //lblNavigation[1].Text = General.sNomLien1;
                    //lblNavigation[2].Text = General.sNomLien2;

                    if (General.sUseCoefGecet == "1")
                    {
                        cmdCoefDevis.Visible = true;
                    }
                    else
                    {
                        cmdCoefDevis.Visible = false;
                    }

                    //===  modif du 14 03 2017.
                    sSQLDroit = "SELECT     AUT_Nom" + " From AUT_Autorisation" + " WHERE  AUT_Formulaire = '" + Variable.cUserDocDevis + "'" +
                                " AND AUT_Objet = 'MajDEvCLoture' " + " AND AUT_Nom = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'";

                    using (var tmpModAdo = new ModAdo())
                        sSQLDroit = tmpModAdo.fc_ADOlibelle(sSQLDroit);
                    if (!string.IsNullOrEmpty(sSQLDroit))
                    {
                        Check48.Enabled = true;
                        DTPicker11.Enabled = true;
                        txtDevisEntete24.Enabled = true;
                    }
                    else
                    {
                        Check48.Enabled = false;
                        DTPicker11.Enabled = false;
                        txtDevisEntete24.Enabled = false;
                    }

                    ModMain.bActivate = false;

                    if (General.sEvolution == "1")
                    {
                        ssCombo1.Visible = true;
                    }
                    else
                    {
                        ssCombo1.Visible = false;
                    }

                    ModDevis.StartTransaction(Text11.Text, this);
                }
                catch (Exception ex)
                {
                    Erreurs.gFr_debug(ex, this.Name + ";Me_Show");
                }


            }
            // var cc = SSDBGridCorps.DisplayLayout.Bands[0].Columns[i].CellActivation;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="C"></param>
        private void RecVisibleChanged(Control C)
        {
            foreach (Control CC in C.Controls)
                if (CC.HasChildren)
                    RecVisibleChanged(CC);
            Control objcontrole = null;
            bool blnSaveGrid = false;

            foreach (Control objcontrole_loopVariable in C.Controls)
            {
                objcontrole = objcontrole_loopVariable;
                if (objcontrole is UltraGrid)
                {
                    if (General.getFrmReg(this.Name, "SaveGrid", "0") == "0")
                    {
                        blnSaveGrid = true;
                    }
                    if (blnSaveGrid == true)
                    {
                        General.saveInReg(this.Name, "SaveGrid", "0");
                    }
                }
            }
            if (blnSaveGrid == true)
            {
                General.saveInReg(this.Name, "SaveGrid", "1");
            }

            /////This Code Added By Mohammed
            //if (C is UltraGrid)
            //{
            //    sheridan.fc_SavDimensionGrille(this.Name, C as UltraGrid);
            //}
        }


        private void loopControl(Control C)
        {

            ///this function added by Mohammed
            foreach (Control c in C.Controls)
                loopControl(c);
            if (C is UltraGrid)
            {
                sheridan.fc_SavDimensionGrille(this.Name, C as UltraGrid);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocDevis_Load(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"UserDocDevis_Load() - Enter In The Function");
            //===> Fin Modif Mondir

            /////this event added by Mohammed
            //this.ParentForm.FormClosing += (se, ev) =>
            //{
            //    loopControl(this);
            //};

            SSTab1.Tabs[8].Visible = false;

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeChrono"></param>
        private void sub_RechFournit(string sCodeChrono)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"sub_RechFournit() - Enter In The Function - sCodeChrono = {sCodeChrono}");
            //===> Fin Modif Mondir

            DataTable rsChronoFournit;
            SqlDataAdapter SDArsChronoFournit = null;

            rsChronoFournit = new DataTable();
            SDArsChronoFournit = new SqlDataAdapter(
                "SELECT Dtiarti.Chrono, Dtiarti.Libelle, Dtiprix.fouunite FROM Dtiarti INNER JOIN DtiPrix ON" +
                " Dtiarti.Chrono=Dtiprix.Chrono WHERE Dtiarti.Chrono='" + sCodeChrono + "' ORDER BY [prix net achat]" +
                " DESC", ModParametre.adoGecet);
            SDArsChronoFournit.Fill(rsChronoFournit);
            ///====================> Tested First
            if (rsChronoFournit.Rows.Count > 0)
            {
                GridFournitArtDetail.ActiveRow.Cells["CodeChrono"].Value = rsChronoFournit.Rows[0]["Chrono"];
                GridFournitArtDetail.ActiveRow.Cells["Designation"].Value = rsChronoFournit.Rows[0]["Libelle"];
                GridFournitArtDetail.ActiveRow.Cells["Unite"].Value = rsChronoFournit.Rows[0]["fouunite"];
                GridFournitArtDetail.ActiveRow.Cells["Qte"].Value = "1";
            }
            else
            {

                string requete = "SELECT Dtiprix.Marque AS \"Marque\", Dtiprix.Fabref as \"Ref. Fabriquant\", Dtiprix.Fourref as \"Ref. Fournisseur\", dtiarti.libelle AS \"Designation\", " +
                                 " dtiarti.chrono as \"Chrono\" ," + " Dtiprix.Fournisseur as [Fournisseur], " + " Dtiprix.[Px tarif], Dtiprix.[Prix net achat]," +
                                 " Dtiprix.FouUnite AS \"Unité\", Dtiprix.Dapplication, Dtiprix.KFunite," + " Dtiprix.Statutci, Dtiprix.CoefVente" +
                                 " FROM dtiarti INNER JOIN Dtiprix ON dtiarti.chrono = Dtiprix.Chrono";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "", Connection: ModParametre.adoGecet) { Text = "Recherche des articles Gecet" };

                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    GridFournitArtDetail.ActiveRow.Cells["CodeChrono"].Value = fg.ugResultat.ActiveRow.Cells["Chrono"].Value;
                    GridFournitArtDetail.ActiveRow.Cells["Designation"].Value = fg.ugResultat.ActiveRow.Cells["Designation"].Value;
                    GridFournitArtDetail.ActiveRow.Cells["Unite"].Value = fg.ugResultat.ActiveRow.Cells["Unité"].Value;
                    GridFournitArtDetail.ActiveRow.Cells["Qte"].Value = "1";
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        GridFournitArtDetail.ActiveRow.Cells["CodeChrono"].Value = fg.ugResultat.ActiveRow.Cells["Chrono"].Value;
                        GridFournitArtDetail.ActiveRow.Cells["Designation"].Value = fg.ugResultat.ActiveRow.Cells["Designation"].Value;
                        GridFournitArtDetail.ActiveRow.Cells["Unite"].Value = fg.ugResultat.ActiveRow.Cells["Unité"].Value;
                        GridFournitArtDetail.ActiveRow.Cells["Qte"].Value = "1";
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFournitArtDetail_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"GridFournitArtDetail_BeforeRowsDeleted() - Enter In The Function");
            //===> Fin Modif Mondir

            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFournitArtDetail_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"GridFournitArtDetail_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            if (GridFournitArtDetail.ActiveCell == null)
                return;
            if (GridFournitArtDetail.ActiveCell.Column.Key.ToUpper() == "CodeChrono".ToUpper() && (int)e.KeyChar == 13)
            {
                //        boolEntree = True
                //            With RechercheMultiCritere
                //            .stxt = "SELECT Dtiprix.Marque AS [Marque], Dtiprix.Fabref as [Ref. Fabriquant], Dtiprix.Fourref as [Ref. Fournisseur], dtiarti.libelle AS [Designation], " _
                //'                & " dtiarti.chrono as [Chrono] ," _
                //'                & " Dtiprix.Fournisseur as [Fournisseur], " _
                //'                & " Dtiprix.[Px tarif], Dtiprix.[Prix net achat]," _
                //'                & " Dtiprix.FouUnite AS [Unité], Dtiprix.Dapplication, Dtiprix.KFunite," _
                //'                & " Dtiprix.Statutci, Dtiprix.CoefVente" _
                //'                & " FROM dtiarti INNER JOIN Dtiprix ON dtiarti.chrono = Dtiprix.Chrono"
                //            .txtConnectionString.Text = adoGecet.ConnectionString
                //            '.TotalChamps = 5
                //            .Caption = "Recherche des articles Gecet"
                //            .Show vbModal
                //            If nz(.LigneChoisie, "") <> "" Then
                //                GridFournitArtDetail.Columns("CodeChrono").value = .dbgrid1.Columns("Chrono").value
                //                GridFournitArtDetail.Columns("IntituleArticle").value = .dbgrid1.Columns("Designation").value
                //                GridFournitArtDetail.Columns("Unite").value = .dbgrid1.Columns("Unité").value
                //                GridFournitArtDetail.Columns("Quantite").value = "1"
                //            End If
                //            Unload RechercheMultiCritere
                //        End With


                //            With frmGecet
                //                adocnn.Execute "DELETE FROM PanierGecet WHERE Utilisateur='" & gsUtilisateur & "'"
                //                .txtCle.Text = txtNoDevis.Text
                //                .txtOrigine.Text = cUserDocDevis
                //                .txtType.Text = "3"
                //                .txtKFO.Text = Text3(14).Text
                //                .txtKMO.Text = Text3(15).Text
                //                .txtKST.Text = Text3(13).Text
                //                .txtPxHeure.Text = Text3(16).Text
                //                .txtTVA.Text = txtTaux.Text
                //                .txtNumLigne.Text = GridFournitArtDetail.Row
                //                .Show vbModal
                //                fc_StockFournitArtDetail
                //                Unload frmGecet
                //            End With

                if (General.sGecetV3 == "1")
                {
                    frmGecetV3 frmGecet3 = new frmGecetV3();
                    General.Execute("DELETE FROM PanierGecet WHERE Utilisateur='" + General.gsUtilisateur + "'");
                    frmGecet3.txtCle.Text = txtNoDevis.Text;
                    frmGecet3.txtOrigine.Text = Variable.cUserDocDevis;
                    frmGecet3.txtType.Text = "3";
                    frmGecet3.txtKFO.Text = Text314.Text;
                    frmGecet3.txtKMO.Text = Text315.Text;
                    frmGecet3.txtKST.Text = Text313.Text;
                    frmGecet3.txtPxHeure.Text = Text316.Text;
                    frmGecet3.txtTVA.Text = txtTaux.Text;
                    frmGecet3.ShowDialog();

                    fc_StockFournitArtDetail();
                    frmGecet3.Close();
                }
                else
                {
                    frmGecet2 frmGecet2 = new frmGecet2();
                    General.Execute("DELETE FROM PanierGecet WHERE Utilisateur='" + General.gsUtilisateur + "'");
                    frmGecet2.txtCle.Text = txtNoDevis.Text;
                    frmGecet2.txtOrigine.Text = Variable.cUserDocDevis;
                    frmGecet2.txtType.Text = "3";
                    frmGecet2.txtKFO.Text = Text314.Text;
                    frmGecet2.txtKMO.Text = Text315.Text;
                    frmGecet2.txtKST.Text = Text313.Text;
                    frmGecet2.txtPxHeure.Text = Text316.Text;
                    frmGecet2.txtTVA.Text = txtTaux.Text;
                    frmGecet2.ShowDialog();

                    fc_StockFournitArtDetail();
                    frmGecet2.Close();

                }

            }
            boolEntree = false;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private object fc_StockFournitArtDetail()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_StockFournitArtDetail() - Enter In The Function");
            //===> Fin Modif Mondir

            object functionReturnValue = null;
            string sqlInsert = null;
            string sqlValues = null;
            DataTable rsFournitArtDetail = null;
            ModAdo modAdorsFournitArtDetail = null;
            int lngMaxNoLigne = 0;

            try
            {
                modAdorsFournitArtDetail = new ModAdo();

                sqlInsert = "INSERT INTO DEV_FournitArticleDetail (";
                sqlInsert = sqlInsert + "CodeArticle,CodeChrono,Designation,Qte,NoLigne";
                sqlInsert = sqlInsert + ") VALUES (";

                rsFournitArtDetail = modAdorsFournitArtDetail.fc_OpenRecordSet("SELECT Code,Designation,Quantite FROM PanierGecet WHERE Utilisateur='" + General.gsUtilisateur + "' ORDER BY CleAuto");

                if (rsFournitArtDetail.Rows.Count > 0)
                {
                    using (var tmpModAdo = new ModAdo())
                        lngMaxNoLigne = Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT MAX(NoLigne) AS MaxNoLigne FROM DEV_FournitArticleDetail WHERE CodeArticle='" + ssGridFacArticle.ActiveRow.Cells["CodeArticle"].Value + "'"), "0"));
                    foreach (DataRow rsFournitArtDetailRow in rsFournitArtDetail.Rows)
                    {
                        lngMaxNoLigne = lngMaxNoLigne + 1;
                        sqlValues = "'" + ssGridFacArticle.ActiveRow.Cells["CodeArticle"].Value + "',";
                        sqlValues = sqlValues + "" + General.nz(rsFournitArtDetailRow["Code"], "0") + ",";
                        sqlValues = sqlValues + "'" + StdSQLchaine.gFr_DoublerQuote(rsFournitArtDetailRow["Designation"] + "") + "',";
                        sqlValues = sqlValues + "" + General.nz(rsFournitArtDetailRow["Quantite"], "0") + ",";
                        sqlValues = sqlValues + "" + lngMaxNoLigne + "";
                        sqlValues = sqlValues + ")";
                        var xx = General.Execute(sqlInsert + sqlValues);
                    }
                }
                modAdorsFournitArtDetail?.Dispose();
                fc_GridFournitArtDetail((ssGridFacArticle.ActiveRow.Cells["CodeArticle"].Value.ToString()));
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
            return functionReturnValue;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_GridFournitArtDetail(string sCode)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_GridFournitArtDetail() - Enter In The Function - sCode = {sCode}");
            //===> Fin Modif Mondir

            modAdorsFournitArticleDetail = new ModAdo();

            General.sSQL = "SELECT DEV_FournitArticleDetail.CodeArticle,";
            General.sSQL = General.sSQL + " DEV_FournitArticleDetail.CodeChrono, DEV_FournitArticleDetail.Designation, DEV_FournitArticleDetail.Qte, DEV_FournitArticleDetail.Unite, DEV_FournitArticleDetail.NoLigne ";
            General.sSQL = General.sSQL + " FROM DEV_FournitArticleDetail ";
            General.sSQL = General.sSQL + " WHERE DEV_FournitArticleDetail.CodeArticle='" + sCode + "'";
            General.sSQL = General.sSQL + " ORDER BY DEV_FournitArticleDetail.NoLigne";

            rsFournitArticleDetail = modAdorsFournitArticleDetail.fc_OpenRecordSet(General.sSQL);

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            GridFournitArtDetail.Visible = false;
            GridFournitArtDetail.DataSource = rsFournitArticleDetail;
            GridFournitArtDetail.Visible = true;
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridSousFamilleArticle_AfterRowUpdate(object sender, RowEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"GridSousFamilleArticle_AfterRowUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            int i = 0;

            //    If GridSousFamilleArticle.Columns("CodeSousFamille").value = "" Then
            //        If GridSousFamilleArticle.Rows > 0 Then
            //            GridSousFamilleArticle.MoveFirst
            //        End If
            //    End If

            if (string.IsNullOrEmpty(e.Row.Cells["CodeSousFamille"].Value.ToString()))
            {
                gridSousFamilleArticleDetail.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
            }
            else
            {
                gridSousFamilleArticleDetail.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
            }
            if (gridSousFamilleArticleDetail.ActiveRow == null ||
                e.Row.Cells["CodeSousFamille"].Value.ToString() != gridSousFamilleArticleDetail.ActiveRow.Cells["CodeSousFamille"].Value.ToString())
            {
                fc_GridSousFamilleArticle((GridSousFamilleArticle.ActiveRow.Cells["CodeSousFamille"].Value.ToString()));
                GridSousFamilleArticle.Focus();
            }
            GridSousFamilleArticle.Focus();

            var xx = modAdorsSousFamilleArticle.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_GridSousFamilleArticle(string sCode)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_GridSousFamilleArticle() - Enter In The Function - sCode = {sCode}");
            //===> Fin Modif Mondir

            General.sSQL = "SELECT SousFamilleArticleDetail.CodeSousFamille," + " SousFamilleArticleDetail.NumLigne, SousFamilleArticleDetail.CodeArticle," +
                " SousFamilleArticleDetail.CodeSousArticle," + " SousFamilleArticleDetail.IntituleArticle," + " SousFamilleArticleDetail.Quantite,SousFamilleArticleDetail.Unite, " +
                "SousFamilleArticleDetail.Prix" + " FROM SousFamilleArticleDetail " + " where CodeSousFamille='" + sCode + "'";
            General.sSQL = General.sSQL + " order by NumLigne";
            modAdorsSousFamilleArticleDetail = new ModAdo();
            rsSousFamilleArticleDetail = modAdorsSousFamilleArticleDetail.fc_OpenRecordSet(General.sSQL);

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            gridSousFamilleArticleDetail.Visible = false;
            gridSousFamilleArticleDetail.DataSource = rsSousFamilleArticleDetail;
            gridSousFamilleArticleDetail.Visible = true;
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeArticle"></param>
        private void sub_RechArticle(string sCodeArticle)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"sub_RechArticle() - Enter In The Function - sCodeArticle = {sCodeArticle}");
            //===> Fin Modif Mondir

            DataTable rsArt = null;
            ModAdo modAdorsArt = new ModAdo();

            rsArt = modAdorsArt.fc_OpenRecordSet("SELECT FacArticle.CodeArticle, FacArticle.Designation1 FROM FacArticle WHERE FacArticle.CodeArticle='" + sCodeArticle + "'");
            if (rsArt.Rows.Count > 0)
            {
                gridSousFamilleArticleDetail.ActiveRow.Cells["IntituleArticle"].Value = rsArt.Rows[0]["Designation1"] + "";
                gridSousFamilleArticleDetail.UpdateData();
                //sub_chargeFourniture_Article sCodeArticle
            }
            else
            {

                string requete = "SELECT FacArticle.CodeArticle as \"Article\", FacArticle.Designation1 as \"Designation\" from FacArticle";
                string where_order = " CodeCategorieArticle = 'D' ";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des articles" };
                fg.SetValues(new Dictionary<string, string> { { "CodeArticle", gridSousFamilleArticleDetail.ActiveRow.Cells["CodeArticle"].Value.ToString() } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    gridSousFamilleArticleDetail.ActiveRow.Cells["CodeArticle"].Value = fg.ugResultat.ActiveRow.Cells[1].Value;
                    gridSousFamilleArticleDetail.ActiveRow.Cells["IntituleArticle"].Value = fg.ugResultat.ActiveRow.Cells[2].Value;
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        gridSousFamilleArticleDetail.ActiveRow.Cells["CodeArticle"].Value = fg.ugResultat.ActiveRow.Cells[1].Value;
                        gridSousFamilleArticleDetail.ActiveRow.Cells["IntituleArticle"].Value = fg.ugResultat.ActiveRow.Cells[2].Value;
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            boolEntree = false;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridSousFamilleArticleDetail_AfterRowsDeleted(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"gridSousFamilleArticleDetail_AfterRowsDeleted() - Enter In The Function");
            //===> Fin Modif Mondir

            if (GridSousFamilleArticle.ActiveRow != null)
                fc_GridSousFamilleArticle(GridSousFamilleArticle.ActiveRow.Cells["CodeSousFamille"].Value.ToString());
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridSousFamilleArticleDetail_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"gridSousFamilleArticleDetail_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            if ((int)e.KeyChar == 13)
            {
                boolEntree = true;
                if (gridSousFamilleArticleDetail.ActiveCell.Column.Index == 2)
                {

                    string requete = "SELECT FacArticle.CodeArticle as \"Article\", FacArticle.Designation1 as \"Designation\" from FacArticle";
                    string where_order = "CodeCategorieArticle = 'D'";
                    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des articles" };
                    fg.SetValues(new Dictionary<string, string> { { "CodeArticle", gridSousFamilleArticleDetail.ActiveRow.Cells["CodeArticle"].Value.ToString() } });
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        gridSousFamilleArticleDetail.ActiveRow.Cells["CodeArticle"].Value = fg.ugResultat.ActiveRow.Cells[1].Value;
                        gridSousFamilleArticleDetail.ActiveRow.Cells["IntituleArticle"].Value = fg.ugResultat.ActiveRow.Cells[2].Value;
                        fg.Dispose();
                        fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            gridSousFamilleArticleDetail.ActiveRow.Cells["CodeArticle"].Value = fg.ugResultat.ActiveRow.Cells[1].Value;
                            gridSousFamilleArticleDetail.ActiveRow.Cells["IntituleArticle"].Value = fg.ugResultat.ActiveRow.Cells[2].Value;
                            fg.Dispose();
                            fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();

                }
                if (gridSousFamilleArticleDetail.ActiveCell.Column.Index == 3)
                {

                    string requete = "SELECT Dtiprix.Marque AS \"Marque\", Dtiprix.Fabref as \"Ref. Fabriquant\", Dtiprix.Fourref as \"Ref. Fournisseur\", dtiarti.libelle AS \"Designation\", " +
                        " dtiarti.chrono as \"Chrono\" ," + " Dtiprix.Fournisseur as \"Fournisseur\", " + " Dtiprix.\"Px tarif\", Dtiprix.\"Prix net achat\"," + " Dtiprix.FouUnite AS \"Unit\", " +
                        "Dtiprix.Dapplication, Dtiprix.KFunite," + " Dtiprix.Statutci, Dtiprix.CoefVente" + " FROM dtiarti INNER JOIN Dtiprix ON dtiarti.chrono = Dtiprix.Chrono";
                    string where_order = "";
                    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "", Connection: ModParametre.adoGecet) { Text = "Recherche des articles" };
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        gridSousFamilleArticleDetail.ActiveRow.Cells["CodeSousFamille"].Value = fg.ugResultat.ActiveRow.Cells["Chrono"].Value;
                        gridSousFamilleArticleDetail.ActiveRow.Cells["IntituleArticle"].Value = fg.ugResultat.ActiveRow.Cells["Designation"].Value;
                        gridSousFamilleArticleDetail.ActiveRow.Cells["Prix"].Value = Convert.ToDouble(General.nz(fg.ugResultat.ActiveRow.Cells["Prix net achat"].Value.ToString().Replace(",", ".").Replace(" ", ""), "0").ToString());
                        fg.Dispose();
                        fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            gridSousFamilleArticleDetail.ActiveRow.Cells["CodeSousFamille"].Value = fg.ugResultat.ActiveRow.Cells["Chrono"].Value;
                            gridSousFamilleArticleDetail.ActiveRow.Cells["IntituleArticle"].Value = fg.ugResultat.ActiveRow.Cells["Designation"].Value;
                            gridSousFamilleArticleDetail.ActiveRow.Cells["Prix"].Value = Convert.ToDouble(General.nz(fg.ugResultat.ActiveRow.Cells["Prix net achat"].Value.ToString().Replace(",", ".").Replace(" ", ""), "0"));
                            fg.Dispose();
                            fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();
                }
            }
            boolEntree = false;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void imgYesDossierIntervention_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"imgYesDossierIntervention_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;

                General.File1EnCours = General.getFrmReg("App", "File1EnCours", "");
                General.Drive1EnCours = General.getFrmReg("App", "Drive1EnCours", "");
                General.Dir1EnCours = General.getFrmReg("App", "Dir1EnCours", "");

                frmDossierIntervention frmDossierIntervention = new frmDossierIntervention();
                try
                {
                    if (string.IsNullOrEmpty(General.Drive1EnCours))
                    {
                        if (General._ExecutionMode == General.ExecutionMode.Dev)
                            frmDossierIntervention.Drive1.Drive = "C:\\";
                        else
                            frmDossierIntervention.Drive1.Drive = "W:\\";
                    }
                    else
                    {
                        frmDossierIntervention.Drive1.Drive = General.Drive1EnCours;
                    }
                }
                catch (Exception ex)
                {
                    Program.SaveException(ex);

                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Lecteur " + General.Drive1EnCours);
                }

                try
                {
                    //if (string.IsNullOrEmpty(General.Dir1EnCours))
                    //{
                    //    frmDossierIntervention.Dir1111.Tag = "C:\\";
                    //}
                    //else
                    //{
                    //    frmDossierIntervention.Dir1111.Tag = General.Dir1EnCours;
                    //}
                    if (string.IsNullOrEmpty(General.Dir1EnCours))
                    {
                        if (General._ExecutionMode == General.ExecutionMode.Dev)
                            frmDossierIntervention.Dir1.Path = "C:\\";
                        else
                            frmDossierIntervention.Dir1.Path = "W:\\";
                    }
                    else
                    {
                        frmDossierIntervention.Dir1.Path = General.Dir1EnCours;
                    }
                }
                catch (Exception ex)
                {
                    Program.SaveException(ex);

                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Dossier " + General.Dir1EnCours);
                }

                try
                {
                    if (string.IsNullOrEmpty(General.File1EnCours))
                    {
                        //frmDossierIntervention.File1.Tag = frmDossierIntervention.Dir1111.Tag;
                        frmDossierIntervention.File1.Path = frmDossierIntervention.Dir1.Path;
                    }
                    else
                    {
                        frmDossierIntervention.File1.Path = General.File1EnCours;
                    }
                }
                catch (Exception ex)
                {
                    Program.SaveException(ex);

                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Dossier " + General.File1EnCours);
                }

                frmDossierIntervention.FileIntervention.Path = General.DossierIntervention + lblNoIntervention.Text;
                frmDossierIntervention.File1.Pattern = "*.doc;*.xls;*.tif;*.pcx;*.htm;*.html;*.txt;*.bmp;*.gif;*.jpg;*.jpeg";
                frmDossierIntervention.lblIntervention.Text = " l'Intervention N° : " + lblNoIntervention.Text;
                frmDossierIntervention.Text = frmDossierIntervention.Text + " Intervention";

                frmDossierIntervention.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "cmdDossierIntervention_Click");
            }


        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label2147_Click(object sender, EventArgs e)
        {
            var Label = sender as Label;
            int Index = Convert.ToInt32(Label.Tag);

            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Label2147_Click() - Enter In The Function - Index = {Index}");
            //===> Fin Modif Mondir

            string sNoIntervention = null;

            try
            {
                if (Index == 47)
                {
                    if (string.IsNullOrEmpty(lblNoIntervention.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'y a pas d'intervention pour ce devis.", "Navigation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    //--recherche du No Intervention.
                    //        sSQl = "SELECT Intervention.NoIntervention" _
                    //'                & " FROM Intervention " _
                    //'                & " Where NoDevis ='" & Text1(1) & "'"
                    //        sNoIntervention = fc_ADOlibelle(sSQl)
                    //        If sNoIntervention = "" Then
                    //            MsgBox "Il n'y a pas d'intervention pour ce devis.", vbCritical, ""
                    //        Else
                    if (valider() == true)
                    {
                        if (!string.IsNullOrEmpty(Text11.Text))
                        {
                            // stocke la position de la fiche devis.
                            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, Text11.Text);
                            // stocke les paramétres pour la feuille de destination.
                            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, lblNoIntervention.Text);
                        }
                        View.Theme.Theme.Navigate(typeof(UserIntervention));
                    }
                    //        End If
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " : cmdVisuInter_Click Devis N°: " + Text11.Text + " ");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label34_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Label34_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            GridSousFamilleArticle.DeleteSelectedRows();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label35_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Label35_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            gridSousFamilleArticleDetail.DeleteSelectedRows();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNodevis"></param>
        private void fc_SyntheseDev(string sNodevis)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_SyntheseDev() - Enter In The Function - sNodevis = {sNodevis}");
            //===> Fin Modif Mondir

            int lnumfichestandard = 0;

            string sSQL = null;
            string sDep = null;

            DataTable rs = null;
            SqlDataAdapter SDArs = null;
            SqlCommand lcmd;

            double dbBonHT = 0;
            double dbFactFournHT = 0;
            double dbFactClt = 0;

            try
            {
                txtDevisEntete0.Text = Text14.Text;
                //=== code immeuble.
                txtDevisEntete1.Text = Text11.Text;
                //=== numéro de devis.
                txtDevisEntete2.Text = Text39.Text;
                //=== Montant HT
                txtDevisEntete3.Text = Text32.Text;
                //=== fourniture ht
                //txtDevisEntete(4) =           '=== Nbre heure équipes
                //txtDevisEntete(5) =           '=== Nbre heure simples
                //txtDevisEntete(6) =           '=== Coefficient devis
                //txtDevisEntete(7) =           '=== Remise accordée
                //txtDevisEntete(8) =           '=== coefficient réalisé
                txtDevisEntete21.Text = lblCommercial.Text;

                using (var tmpModAdo = new ModAdo())
                    sDep = tmpModAdo.fc_ADOlibelle("select CodeDepanneur FROM immeuble " + " WHERE CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(Text14.Text) + "'");

                if (!string.IsNullOrEmpty(sDep))
                {
                    using (var tmpModAdo = new ModAdo())
                        txtDevisEntete22.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel " + " WHERE Personnel.Initiales='" +
                            tmpModAdo.fc_ADOlibelle("SELECT Personnel.CRespExploit FROM Personnel " + " WHERE Personnel.Matricule='" + sDep + "'") + "'");
                }

                //    sSQL = "SELECT GestionStandard.NumFicheStandard " _
                //'        & " FROM GestionStandard " _
                //'        & " WHERE GestionStandard.NoDevis='" & sNoDevis & "'"

                lnumfichestandard = Convert.ToInt32(General.nz(txtDevisEntete20.Text, 0));

                if (lnumfichestandard == 0)
                {
                    return;
                }

                //=== Appel de la procedure stockée qui récupére toutes les lignes ht et quantité
                //=== du bon de commande.
                //    With lcmd
                //        Set .ActiveConnection = adocnn
                //        .CommandType = adCmdStoredProc
                //        .CommandText = "GetMntCoprsBcd"
                //        .Parameters.Append .CreateParameter("@lNumficheStandard", _
                //'                                                adInteger, adParamInput, _
                //'                                                , lNumFichestandard)
                //       .Execute
                //
                //       Set rs = .Execute
                //
                //    End With
                //    Set lcmd = Nothing
                //
                //    dbBonHT = 0
                //    Do While rs.EOF = False
                //
                //        dbBonHT = dbBonHT + nz(rs!BCD_Quantite, 0) * nz(rs!BCD_PrixHT, 0)
                //        rs.MoveNext
                //    Loop
                //
                //    rs.Close
                //    Set rs = Nothing

                //=== Appel de la procedure stockée qui récupére toutes les lignes ht
                //=== des factures fournisseurs.

                lcmd = new SqlCommand("GetTotHtFactFourn", General.adocnn);
                lcmd.CommandType = CommandType.StoredProcedure;
                lcmd.Parameters.AddWithValue("lNumficheStandard", lnumfichestandard);
                SDArs = new SqlDataAdapter(lcmd);
                rs = new DataTable();
                SDArs.Fill(rs);

                lcmd = null;

                dbFactFournHT = Convert.ToDouble(General.nz(rs.Rows[0]["TotHt"], 0));

                SDArs?.Dispose();
                rs?.Dispose();

                //    '=== Appel de la procedure stockée qui récupére toutes les lignes ht
                //    '=== des factures fournisseurs.
                //    With lcmd
                //        Set .ActiveConnection = adocnn
                //        .CommandType = adCmdStoredProc
                //        .CommandText = "GetTotFactClient"
                //        .Parameters.Append .CreateParameter("@lNumficheStandard", _
                //'                                                adInteger, adParamInput, _
                //'                                                , lNumFichestandard)
                //       .Execute
                //        Set rs = .Execute
                //
                //    End With
                //    Set lcmd = Nothing
                //
                //    dbFactClt = nz(rs!TotHt, 0)
                //
                //    rs.Close
                //    Set rs = Nothing
                //
                //    '=== Appel de la procedure stockée qui récupére les durées des interventions.
                //    With lcmd
                //        Set .ActiveConnection = adocnn
                //        .CommandType = adCmdStoredProc
                //        .CommandText = "GetDureeInterv"
                //        .Parameters.Append .CreateParameter("@lNumficheStandard", _
                //'                                                adInteger, adParamInput, _
                //'                                                , lNumFichestandard)
                //       .Execute
                //        Set rs = .Execute
                //
                //    End With
                //    Set lcmd = Nothing
                //
                //    Do While rs.EOF = False
                //
                //        rs.MoveNext
                //    Loop
                //'    dbFactClt = nz(rs!TotHt, 0)
                //
                //    rs.Close
                //    Set rs = Nothing


                //=== montant bon de commande Ht.
                txtDevisEntete17.Text = Convert.ToString(Math.Round(dbFactFournHT, 2));
                //    '=== montant facture fourniseur Ht.
                //    txtDevisEntete(17) = FncArrondir(dbFactFournHT, 2)
                //    '=== montant facturé ht.
                //    txtDevisEntete(19) = FncArrondir(dbFactClt, 2)
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_SyntheseDev;");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridSousFamilleArticle_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"GridSousFamilleArticle_BeforeExitEditMode() - Enter In The Function");
            //===> Fin Modif Mondir

            if (GridSousFamilleArticle.ActiveCell == null)
                return;
            if (GridSousFamilleArticle.ActiveCell.Column.Key.ToUpper() == "CodeSousFamille".ToUpper())
            {
                GridSousFamilleArticle.ActiveRow.Cells["CodeSousFamille"].Value = StdSQLchaine.fc_CtrlCharDevis(GridSousFamilleArticle.ActiveRow.Cells["CodeSousFamille"].Text);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridSousFamilleArticle_AfterExitEditMode(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label47_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Label47_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            ssGridFacArticle.DeleteSelectedRows();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Label44_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Label44_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            GridFournitArtDetail.DeleteSelectedRows();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblAppelValid_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"lblAppelValid_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            if (valider() == true)
            {
                if (!string.IsNullOrEmpty(txtAppelValid.Text))
                {
                    // ' stocke la position de la fiche devis.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, Text11.Text);
                    // stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocStandard, txtAppelValid.Text);
                }
                View.Theme.Theme.Navigate(typeof(UserDocStandard));
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblGoContrat_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"lblGoContrat_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            string sSQL = null;

            try
            {
                //    MsgBox "Fonctionnalité non installée", vbInformation, "Contrat"
                //    Exit Sub
                if (valider() == true)
                {

                    if (string.IsNullOrEmpty(txtContrat10.Text))
                    {

                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Navigation annulée, Il n'existe pas de contrat pour cet immeuble.");
                        return;
                    }

                    if (!string.IsNullOrEmpty(Text11.Text))
                    {
                        // stocke la position de la fiche devis.
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, Text11.Text);
                        // stocke les paramétres pour la feuille de destination.
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFicheContrat, txtCodeContrat.Text);
                    }

                    View.Theme.Theme.Navigate(typeof(UserDocFicheContrat));

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";lblGoContrat_Click");
            }


        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblTotalSituations_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"lblTotalSituations_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            int i = 0;
            string sPhrase = null;

            if (!string.IsNullOrEmpty(tabNoFacture[0]))
            {
                sPhrase = "Factures enregistrées :";
                for (i = 0; i <= tabNoFacture.Length - 1; i++)
                {
                    sPhrase = sPhrase + "\n";
                    sPhrase = sPhrase + "  " + tabNoFacture[i] + " : " + General.nz(tabMontantFact[i], "0") + " €";
                }
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sPhrase, "Factures sur situation", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LienGoImmeuble_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"LienGoImmeuble_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            if (valider() == true)
            {

                if (!string.IsNullOrEmpty(Text14.Text))
                {
                    // stocke la position de la fiche devis.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, Text11.Text);
                    // stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, Text14.Text);
                }
                Axe_interDT.View.Theme.Theme.Navigate(typeof(UserDocImmeuble));
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Option1_CheckedChanged(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Option1_CheckedChanged() - Enter In The Function - Option1.Checked = {Option1.Checked}");
            //===> Fin Modif Mondir

            if (Option1.Checked)
            {
                Text5.Enabled = false;
                Text6.Enabled = false;
                Text7.Enabled = false;
                Text8.Enabled = false;
                Text9.Text = "";
                Text9.Text = "1";
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option3_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Option3_CheckedChanged() - Enter In The Function - Option3.Checked = {Option3.Checked}");
            //===> Fin Modif Mondir

            if (Option3.Checked)
            {
                Text5.Enabled = true;
                Text6.Enabled = true;
                Text7.Enabled = true;
                Text8.Enabled = true;
                Text9.Text = "";
                Text9.Text = "3";
            }
        }

        /// <summary>
        /// TODO : Ask Rachid, This Controls Is Hidden Somewhere
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDbCopieA_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssDbCopieA_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                cmdCopieA_Click(cmdCopieA, new System.EventArgs());
            }
        }

        /// <summary>
        /// TODO : Ask Rachid, This Controls Is Hidden Somewhere
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCopieA_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdCopieA_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                string requete = "SELECT TypeCopieA.CodeCopieA as \"Code\", TypeCopieA.Libelle as \"Libelle\" FROM TypeCopieA";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des fiches standard" };
                fg.SetValues(new Dictionary<string, string> { { "CodeCopieA", ssDbCopieA.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    ssDbCopieA.Text = fg.ugResultat.ActiveRow.Cells["code"].Value.ToString();
                    ssDbCopieABis.Text = fg.ugResultat.ActiveRow.Cells["Libelle"].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        ssDbCopieA.Text = fg.ugResultat.ActiveRow.Cells["code"].Value.ToString();
                        ssDbCopieABis.Text = fg.ugResultat.ActiveRow.Cells["Libelle"].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdCopieA_Click");
            }
        }

        /// <summary>
        /// Controle Is Hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDemandeDePrix_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdDemandeDePrix_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            object vartemp = null;
            bool blnSelect = false;
            bool blnDemande = false;
            int lngCompte = 0;

            try
            {
                //==== Verifie si le corps de devis a été valider
                if (boolModif == true)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez d'abord valider le Devis", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                //==== création de demande de prix.
                //==== Pour creer celui ci l'uti. doit selectionner les enregistrements
                //==== avec code sousarticle dans la grille.
                blnDemande = false;
                longBonCom = 0;
                lngCompte = 1;
                Recom:

                //==== boucle pour déselectionner les enregistrements sans codeSousAticle.
                for (i = 0; i <= SSDBGridCorps.Selected.Rows.Count - 1; i++)
                {
                    if (string.IsNullOrEmpty(SSDBGridCorps.Selected.Rows[i].Cells["codesousarticle"].Value.ToString()))
                    {
                        SSDBGridCorps.Selected.Rows[i].Selected = false;
                        goto Recom;
                    }
                }

                if (SSDBGridCorps.Selected.Rows.Count != 0)
                {
                    //==== boucle sur tous les enregistrements séléctionnés.
                    for (i = 0; i <= SSDBGridCorps.Selected.Rows.Count - 1; i++)
                    {
                        if (!string.IsNullOrEmpty(SSDBGridCorps.Selected.Rows[i].Cells["codesousarticle"].Value.ToString()))
                        {

                            if (blnDemande == false)
                            {
                                //==== creation de la demande de prix.
                                DemandeDePrix();
                            }
                            blnDemande = true;
                            //==== création du corps de la demande.
                            if (adotemp == null)
                            {
                                General.SQL = "SELECT NoBonDeCommande," + " Noligne, Ref, Produit, ProduitPrix," + " ProduitQte, SStotal" + " FROM CommandeProduit";
                                modAdoadotemp = new ModAdo();
                                adotemp = modAdoadotemp.fc_OpenRecordSet(General.SQL);
                            }
                            var NewRow = adotemp.NewRow();
                            NewRow["NoBonDeCommande"] = longBonCom;
                            NewRow["NoLigne"] = lngCompte;
                            NewRow["Ref"] = SSDBGridCorps.Selected.Rows[i].Cells["codeSousArticle"].Value;
                            NewRow["Produit"] = SSDBGridCorps.Selected.Rows[i].Cells["Texteligne"].Value;
                            NewRow["ProduitPrix"] = SSDBGridCorps.Selected.Rows[i].Cells["Foud"].Value;
                            NewRow["ProduitQte"] = SSDBGridCorps.Selected.Rows[i].Cells["Quantite"].Value;
                            NewRow["SStotal"] = SSDBGridCorps.Selected.Rows[i].Cells["Fod"].Value;
                            modAdoadotemp.Update();
                            lngCompte = lngCompte + 1;
                        }
                    }
                    adotemp?.Dispose();
                    //05112002 prcLiaison PROGB0NCOMMANDE, Text1(1), CStr(longBonCom)
                    //05112002  fc_Navigue Me,  gsCheminPackage & PROGB0NCOMMANDE
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Fonction en construction.");

                    //==== si l'uti. n' a pas sélectionnée d'enregistrements.
                }
                else
                {
                    afficheMessage(Convert.ToString(10));
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdDemandeDePrix_Click");
            }
        }

        /// <summary>
        /// The Fonction Is Called From A Hidden Control
        /// </summary>
        private void DemandeDePrix()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"DemandeDePrix() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                modAdoadotemp = new ModAdo();
                //==== Verifie d'abord si il éxiste déja une demande de prix.
                General.SQL = "SELECT NoBonDeCommande, CodeImmeuble,NumeroDevis," + " datecommande,Acheteur, DemandeDePrix" + " From BonDeCommande" + " WHERE NumeroDevis='" + Text11.Text + "'";
                adotemp = modAdoadotemp.fc_OpenRecordSet(General.SQL);
                var NewRow = adotemp.NewRow();
                NewRow["numerodevis"] = Text11.Text;
                NewRow["CodeImmeuble"] = Text14.Text;
                NewRow["DateCommande"] = DateTime.Now.ToString("dd/MM/yyyy");
                NewRow["Acheteur"] = General.gsUtilisateur;
                NewRow["DemandeDePrix"] = true;
                adotemp.Rows.Add(NewRow);
                modAdoadotemp.Update();
                using (var tmpModAdo = new ModAdo())
                    longBonCom = Convert.ToInt32(tmpModAdo.fc_ADOlibelle("SELECT MAX(NoBonDeCommande) FROM BonDeCommande"));
                modAdoadotemp.Dispose();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";DemandeDePrix");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDossierDevis_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdDossierDevis_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;

                General.File1EnCours = General.getFrmReg("App", "File1EnCours", "");
                General.Drive1EnCours = General.getFrmReg("App", "Drive1EnCours", "");
                General.Dir1EnCours = General.getFrmReg("App", "Dir1EnCours", "");

                if (!string.IsNullOrEmpty(Text11.Text))
                {
                    if (Dossier.fc_ControleDossier(General.DossierDevis + Text11.Text) == false)
                    {
                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez-vous créer un dossier pour ce devis ?", "Création d'un dossier d'intervention", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                        {
                            Dossier.fc_CreateDossier(General.DossierDevis + Text11.Text);
                            cmdDossierDevis.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0ffc0);
                            var xx = General.Execute("UPDATE DevisEntete set DevisEntete.DossierDevis=1 WHERE DevisEntete.NumeroDevis='" + Text11.Text + "'");
                        }
                        else
                        {
                            return;
                        }
                    }

                    frmDossierIntervention frmDossierIntervention = new frmDossierIntervention();

                    try
                    {
                        //foreach (var drive in System.IO.DriveInfo.GetDrives())
                        //{
                        //    frmDossierIntervention.Drive1.Items.Add(drive.Name);
                        //}
                        if (string.IsNullOrEmpty(General.Drive1EnCours))
                        {
                            if (General._ExecutionMode == General.ExecutionMode.Dev)
                                frmDossierIntervention.Drive1.Drive = "C:\\";
                            else
                                frmDossierIntervention.Drive1.Drive = "W:\\";
                        }
                        else
                        {
                            frmDossierIntervention.Drive1.Drive = General.Drive1EnCours;
                        }
                    }
                    catch (Exception ex)
                    {
                        Program.SaveException(ex);

                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Lecteur " + General.Drive1EnCours);
                    }


                    try
                    {
                        //if (string.IsNullOrEmpty(General.Dir1EnCours))
                        //{
                        //    frmDossierIntervention.Dir1111.Tag = "C:\\";
                        //}
                        //else
                        //{
                        //    frmDossierIntervention.Dir1111.Tag = General.Dir1EnCours;
                        //}
                        if (string.IsNullOrEmpty(General.Dir1EnCours))
                        {
                            if (General._ExecutionMode == General.ExecutionMode.Dev)
                                frmDossierIntervention.Dir1.Path = "C:\\";
                            else
                                frmDossierIntervention.Dir1.Path = "W:\\";
                        }
                        else
                        {
                            frmDossierIntervention.Dir1.Path = General.Dir1EnCours;
                        }
                    }
                    catch (Exception ex)
                    {
                        Program.SaveException(ex);

                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Dossier " + General.Dir1EnCours);
                    }

                    try
                    {
                        if (string.IsNullOrEmpty(General.File1EnCours))
                        {
                            //frmDossierIntervention.File1.Tag = frmDossierIntervention.Dir1111.Tag;
                            frmDossierIntervention.File1.Path = frmDossierIntervention.Dir1.Path;
                        }
                        else
                        {
                            frmDossierIntervention.File1.Path = General.File1EnCours;
                        }
                    }
                    catch (Exception ex)
                    {
                        Program.SaveException(ex);

                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Dossier " + General.File1EnCours);
                    }
                    frmDossierIntervention.DirNoInter.Path = General.DossierDevis + Text11.Text;
                    frmDossierIntervention.FileIntervention.Path = General.DossierDevis + Text11.Text;
                    frmDossierIntervention.File1.Pattern = "*.doc;*.xls;*.tif;*.pcx;*.htm;*.html;*.txt;*.bmp;*.gif;*.jpg;*.jpeg";
                    frmDossierIntervention.lblIntervention.Text = frmDossierIntervention.lblIntervention.Text + " le Devis N° : " + Text11.Text;
                    frmDossierIntervention.Text = frmDossierIntervention.Text + " Devis";
                    frmDossierIntervention.ShowDialog();
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "Devis : cmdDossierDevis_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDbCopieA_Leave(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssDbCopieA_Leave() - Enter In The Function");
            //===> Fin Modif Mondir

            Text4.Text = Convert.ToString(Convert.ToInt16(Text4.Text) + 1);
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="ssdb"></param>
        /// <param name="ColIndex"></param>
        private void Integrer(UltraGrid ssdb, int ColIndex)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Integrer() - Enter In The Function - ssdb.Name = {ssdb.Name} - ColIndex = {ColIndex}");
            //===> Fin Modif Mondir

            try
            {
                var manager = ssdb.EventManager;

                string strTEMP = null;

                var _with49 = ssdb;

                if (_with49.ActiveRow == null)
                    return;

                //---Champs code famille
                //==================> Tested
                if (ColIndex == 2)
                {
                    strTEMP = _with49.ActiveRow.Cells["Codefamille"].Value.ToString();
                    if (_with49.ActiveRow.IsAddRow)
                    {
                        //_with49.UpdateData();
                        manager.AllEventsEnabled = false;
                        var RowIndex = _with49.ActiveRow.Index;
                        _with49.Rows[RowIndex].Delete(false);
                        InsertionFamille(strTEMP, ref RowIndex, true);
                        manager.AllEventsEnabled = true;
                    }
                    else
                    {
                        manager.AllEventsEnabled = false;
                        _with49.ActiveRow.Cells["Codefamille"].Value = strOldValue;
                        //_with49.UpdateData();
                        var tmp = _with49.ActiveRow.Index;
                        InsertionFamille(strTEMP, ref tmp, false);
                        manager.AllEventsEnabled = true;
                    }
                    return;
                }

                //--- Champs code sous famille - Poste
                //==================> Tested
                if (ColIndex == 3)
                {
                    strTEMP = _with49.ActiveRow.Cells["CodeSouSfamille"].Value.ToString();
                    if (_with49.ActiveRow.IsAddRow)
                    {
                        //_with49.UpdateData();
                        manager.AllEventsEnabled = false;
                        var RowIndex = _with49.ActiveRow.Index;
                        _with49.Rows[RowIndex].Delete(false);
                        InsertionSousFamille(strTEMP, ref RowIndex, true);
                        manager.AllEventsEnabled = true;
                    }
                    else
                    {
                        manager.AllEventsEnabled = false;
                        _with49.ActiveRow.Cells["CodeSouSfamille"].Value = strOldValue;
                        //_with49.UpdateData();
                        var tmp = _with49.ActiveRow.Index;
                        InsertionSousFamille(strTEMP, ref tmp, false);
                        manager.AllEventsEnabled = true;
                    }
                    return;
                }


                //----Champs code article
                //==================> Tested
                if (ColIndex == 4)
                {
                    strTEMP = _with49.ActiveRow.Cells["CodeArticle"].Value.ToString();
                    if (_with49.ActiveRow.IsAddRow)
                    {
                        //_with49.UpdateData();
                        manager.AllEventsEnabled = false;
                        var RowIndex = _with49.ActiveRow.Index;
                        _with49.Rows[RowIndex].Delete(false);
                        recherchearticle(strTEMP, ref RowIndex, true);
                        manager.AllEventsEnabled = true;
                    }
                    else
                    {
                        manager.AllEventsEnabled = false;
                        _with49.ActiveRow.Cells["codearticle"].Value = strOldValue;
                        //_with49.UpdateData();
                        var tmp = _with49.ActiveRow.Index;
                        recherchearticle(strTEMP, ref tmp, false);
                        manager.AllEventsEnabled = true;
                    }
                    return;
                }

                //----Champs code sous article
                if (ColIndex == 5)
                {
                    strTEMP = _with49.ActiveRow.Cells["codesousarticle"].Value.ToString();
                    if (_with49.ActiveRow.IsAddRow)
                    {
                        //.Update
                        // .RemoveItem .Rows - 1
                        // RechercheArticle strtemp, .AddItemRowIndex(.Bookmark), True
                        manager.AllEventsEnabled = false;
                        var RowIndex = _with49.ActiveRow.Index;
                        inserefourniture(ref RowIndex, true);
                        //_with49.Update();
                        manager.AllEventsEnabled = true;
                    }
                    else
                    {
                        //.Columns("codesousarticle").Value = strOldValue
                        //.Update
                        // RechercheArticle strtemp, .AddItemRowIndex(.Bookmark), False
                        manager.AllEventsEnabled = false;
                        var tmp = _with49.ActiveRow.Index;
                        inserefourniture(ref tmp, false);
                        //_with49.Update();
                        manager.AllEventsEnabled = true;
                    }
                    return;
                }

                _with49.Update();

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Integrer");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strCodeFamille"></param>
        /// <param name="Index"></param>
        /// <param name="boolAddrow"></param>
        private void InsertionFamille(string strCodeFamille, ref int Index, bool boolAddrow)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"InsertionFamille() - Enter In The Function - strCodeFamille = {strCodeFamille} - Index = {Index} - boolAddrow = {boolAddrow}");
            //===> Fin Modif Mondir

            try
            {
                modAdoadotemp = new ModAdo();

                var _with101 = adotemp;

                General.SQL = "SELECT IntituleFamille " + "From FamilleArticle " + "WHERE CodeFamille='" + strCodeFamille + "'";
                _with101 = modAdoadotemp.fc_OpenRecordSet(General.SQL);

                ////=======================> Tested First

                var SSDBGridCorpsSource = SSDBGridCorps.DataSource as DataTable;

                if (_with101.Rows.Count > 0)
                {
                    var newRow = SSDBGridCorpsSource.NewRow();

                    //General.SQL = PrepareLigne("CodeFamille", SSDBGridCorps, StdSQLchaine.fc_CtrlCharDevis(strCodeFamille), "");
                    newRow["CodeFamille"] = StdSQLchaine.fc_CtrlCharDevis(strCodeFamille);
                    //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps,  General.SQL);
                    newRow["Texteligne"] = _with101.Rows[0]["intitulefamille"] + "";
                    //General.SQL = PrepareLigne("pxHeured", SSDBGridCorps, Text316.Text, General.SQL);
                    newRow["pxHeured"] = Text316.Text;
                    //General.SQL = PrepareLigne("KMO", SSDBGridCorps, Text315.Text, General.SQL);
                    newRow["KMO"] = Text315.Text;
                    //General.SQL = PrepareLigne("KFO", SSDBGridCorps, Text314.Text, General.SQL);
                    newRow["KFO"] = Text314.Text;
                    //General.SQL = PrepareLigne("KST", SSDBGridCorps, Text313.Text, General.SQL);
                    newRow["KST"] = Text313.Text;
                    //InsereLigneInGrille(General.SQL, boolAddrow, Index);
                    if (boolAddrow)
                        SSDBGridCorpsSource.Rows.Add(newRow);
                    else
                        SSDBGridCorpsSource.Rows.InsertAt(newRow, Index);
                }
                else
                {
                    var newRow = SSDBGridCorpsSource.NewRow();
                    modAdoadotemp?.Dispose();
                    adotemp = null;
                    //General.SQL = PrepareLigne("CodeFamille", SSDBGridCorps, StdSQLchaine.fc_CtrlCharDevis(strCodeFamille), "");
                    newRow["CodeFamille"] = StdSQLchaine.fc_CtrlCharDevis(strCodeFamille);
                    //InsereLigneInGrille(General.SQL, boolAddrow, Index);
                    if (boolAddrow)
                        SSDBGridCorpsSource.Rows.Add(newRow);
                    else
                        SSDBGridCorpsSource.Rows.InsertAt(newRow, Index);
                    return;
                }
                modAdoadotemp?.Dispose();
                General.SQL = "SELECT  FamilleArticle.IntituleFamille," + " FamilleArticleDetail.Quantite, FamilleArticleDetail.NumLigne," + " FamilleArticleDetail.CodeSousFamille" + " FROM FamilleArticle INNER JOIN FamilleArticleDetail" + " ON FamilleArticle.CodeFamille = FamilleArticleDetail.CodeFamille" + " WHERE FamilleArticleDetail.CodeFamille='" + strCodeFamille + "'" + " ORDER BY FamilleArticleDetail.NumLigne";

                modAdoadotemp = new ModAdo();
                _with101 = modAdoadotemp.fc_OpenRecordSet(General.SQL);
                foreach (DataRow Row in _with101.Rows)
                {
                    Index++;
                    InsertionSousFamille(Row["CodeSousFamille"].ToString(), ref Index, boolAddrow, Convert.ToInt32(Row["Quantite"]));
                }
                modAdoadotemp?.Dispose();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";InsertionFamille");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="row"></param>
        private void setCellsToNull(DataRow row)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"setCellsToNull() - Enter In The Function");
            //===> Fin Modif Mondir

            for (var i = 0; i < row.ItemArray.Length; i++)
                if (row[i].ToString() == "" || row[i].ToString() == "0")
                    row[i] = DBNull.Value;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strCodeSouFamille"></param>
        /// <param name="Index"></param>
        /// <param name="boolAddrow"></param>
        /// <param name="LongQuantite"></param>
        private void InsertionSousFamille(string strCodeSouFamille, ref int Index, bool boolAddrow, int LongQuantite = 1)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"InsertionSousFamille() - Enter In The Function - strCodeSouFamille = {strCodeSouFamille} " +
                                        $"- Index = {Index} - boolAddrow = {boolAddrow} - LongQuantite = {LongQuantite}");
            //===> Fin Modif Mondir

            DataTable rsBis = null;
            ModAdo modAdorsBis = null;
            int i = 0;

            try
            {
                General.SQL = "SELECT SousFamilleArticle.CodeSousFamille," + " SousFamilleArticle.IntituleSousFamille, FacArticle.CodeArticle," +
                    " FacArticle.Designation1,FacArticle.Designation2," + " FacArticle.Designation3, FacArticle.Designation4," +
                    " FacArticle.Designation5, FacArticle.Designation6," + " FacArticle.Designation7, FacArticle.Designation8," +
                    " FacArticle.Designation9, FacArticle.Designation10," + " FacArticle.MOtps, FacArticle.FO," +
                    " FacArticle.ST, SousFamilleArticleDetail.Quantite, SousFamilleArticleDetail.Unite AS UniteSousArt, " +
                    "SousFamilleArticleDetail.CodeSousArticle,SousFamilleArticleDetail.IntituleArticle,SousFamilleArticleDetail.Prix," +
                    " FacArticle.CG_Num, FacArticle.CG_Num2" + " FROM SousFamilleArticle LEFT JOIN (SousFamilleArticleDetail" +
                    " LEFT JOIN FacArticle ON SousFamilleArticleDetail.CodeArticle =" + " FacArticle.CodeArticle) ON SousFamilleArticle.CodeSousFamille =" +
                    " SousFamilleArticleDetail.CodeSousFamille" + " WHERE SousFamilleArticle.CodeSousFamille='" + strCodeSouFamille + "'";

                modAdorsBis = new ModAdo();
                var _with102 = rsBis;
                _with102 = modAdorsBis.fc_OpenRecordSet(General.SQL);
                if (_with102.Rows.Count > 0)
                {
                    var SSDBGridCorpsSource = SSDBGridCorps.DataSource as DataTable;

                    var NewRow = SSDBGridCorpsSource.NewRow();
                    NewRow["CodeSousFamille"] = _with102.Rows[0]["CodeSousFamille"];
                    NewRow["Texteligne"] = _with102.Rows[0]["IntituleSousFamille"];

                    //General.SQL = PrepareLigne("Codesousfamille", SSDBGridCorps, StdSQLchaine.fc_CtrlCharDevis(_with102.Rows[0]["CodeSousFamille"] + ""), "");
                    //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, General.nz(_with102.Rows[0]["IntituleSousFamille"], "").ToString(), General.SQL);
                    // modif rachid du 28 Novenbre 2002
                    //SQL = PrepareLigne("pxHeured", SSDBGridCorps, Text3(16), SQL)
                    //SQL = PrepareLigne("KMO", SSDBGridCorps, Text3(15), SQL)
                    //SQL = PrepareLigne("KFO", SSDBGridCorps, Text3(14), SQL)
                    //SQL = PrepareLigne("KST", SSDBGridCorps, Text3(13), SQL)
                    //General.SQL = PrepareLigne("pxHeured", SSDBGridCorps, "", General.SQL);
                    //General.SQL = PrepareLigne("KMO", SSDBGridCorps, "", General.SQL);
                    //General.SQL = PrepareLigne("KFO", SSDBGridCorps, "", General.SQL);
                    //General.SQL = PrepareLigne("KST", SSDBGridCorps, "", General.SQL);

                    NewRow["pxHeured"] = DBNull.Value;
                    NewRow["KMO"] = DBNull.Value;
                    NewRow["KFO"] = DBNull.Value;
                    NewRow["KST"] = DBNull.Value;

                    //setCellsToNull(NewRow);

                    //InsereLigneInGrille(General.SQL, boolAddrow, Index);

                    if (boolAddrow)
                    {
                        SSDBGridCorpsSource.Rows.Add(NewRow);
                        boolAddrow = false;
                    }
                    else
                    {
                        SSDBGridCorpsSource.Rows.InsertAt(NewRow, Index);
                    }
                    SSDBGridCorps.DataBind();

                    Index = Index + 1;
                    foreach (DataRow Row in _with102.Rows)
                    {
                        if (!General.IsNumeric(Row["Quantite"].ToString()) && Row["CodeArticle"] != DBNull.Value)
                        {
                            InsertionArticle(Row["CodeArticle"].ToString(), ref Index, boolAddrow, Row, LongQuantite * 1);
                        }
                        else if (Row["CodeArticle"] != DBNull.Value)
                        {
                            InsertionArticle(Row["CodeArticle"].ToString(), ref Index, boolAddrow, Row, LongQuantite * Convert.ToInt32(Row["Quantite"]));
                        }
                        else if (Row["CodeSousArticle"] != DBNull.Value)
                        {
                            InsertionArticle(General.nz(Row["CodeArticle"], "").ToString(), ref Index, boolAddrow, Row, LongQuantite * Convert.ToInt32(Row["Quantite"]));
                        }
                    }

                }
                modAdorsBis?.Dispose();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";InsertionSousFamille");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strCodeArticle"></param>
        /// <param name="Index"></param>
        /// <param name="boolAddrow"></param>
        private void recherchearticle(string strCodeArticle, ref int Index, bool boolAddrow)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"recherchearticle() - Enter In The Function - strCodeArticle = {strCodeArticle} " +
                                        $"- Index = {Index} - boolAddrow = {boolAddrow}");
            //===> Fin Modif Mondir

            try
            {
                modAdoadotemp = new ModAdo();
                adotemp = modAdoadotemp.fc_OpenRecordSet("SELECT FacArticle.CodeArticle, FacArticle.Designation1," + " FacArticle.Designation2, FacArticle.Designation3," + " FacArticle.Designation4, FacArticle.Designation5," + " FacArticle.Designation6, FacArticle.Designation7," + " FacArticle.Designation8, FacArticle.Designation9," + " FacArticle.Designation10, FacArticle.MOtps, FacArticle.FO," + " FacArticle.ST, Facarticle.CG_Num, FacArticle.CG_Num2, Facarticle.Unite AS UniteSousArt" + " From FacArticle" + " WHERE FacArticle.CodeArticle='" + strCodeArticle + "'");
                if (adotemp.Rows.Count > 0)
                {
                    InsertionArticle(strCodeArticle, ref Index, boolAddrow, adotemp.Rows[0]);
                }
                modAdoadotemp?.Dispose();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";RechercheArticle");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strCodeArticle"></param>
        /// <param name="Index"></param>
        /// <param name="boolAddrow"></param>
        /// <param name="rs"></param>
        /// <param name="Quantite"></param>
        private void InsertionArticle(string strCodeArticle, ref int Index, bool boolAddrow, DataRow rs, int Quantite = 1)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"InsertionArticle() - Enter In The Function - strCodeArticle = {strCodeArticle} " +
                                        $"- Index = {Index} - boolAddrow = {boolAddrow} - Quantite = {Quantite}");
            //===> Fin Modif Mondir

            object iMOhud = null;
            object iMOh = null;

            object cFOud = null;
            object cSTud = null;
            object cPXHeured = null;
            object cMOd = null;
            object cFOd = null;
            object cSTd = null;
            object cMO = null;
            object cFO = null;
            object cST = null;
            object cTotalDebours = null;
            object cTotalVente = null;
            short i = 0;
            bool boolTrouve = false;
            string strchamp = null;
            try
            {

                var SSDBGridCorpsSource = SSDBGridCorps.DataSource as DataTable;
                var NewRow = SSDBGridCorpsSource.NewRow();

                if (Quantite == 0)
                {
                    Quantite = 1;
                }

                var _with103 = rs;
                //General.SQL = PrepareLigne("CodeArticle", SSDBGridCorps, StdSQLchaine.fc_CtrlCharDevis(_with103["CodeArticle"] + ""), "");
                NewRow["CodeArticle"] = _with103["CodeArticle"];


                if (string.IsNullOrEmpty(General.nz(_with103["CodeArticle"], "").ToString()))
                {
                    //General.SQL = PrepareLigne("CodeSousArticle", SSDBGridCorps, General.nz(_with103["CodeSousArticle"], "").ToString(), General.SQL);
                    NewRow["CodeSousArticle"] = _with103["CodeSousArticle"];
                }

                for (i = 1; i <= 10; i++)
                {
                    strchamp = "Designation" + i;
                    if (!string.IsNullOrEmpty(_with103[strchamp].ToString()))
                    {
                        boolTrouve = true;
                        break;
                    }
                }

                if (boolTrouve == false)
                {
                    //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, General.nz(_with103["IntituleArticle"], "").ToString(), General.SQL);
                    NewRow["Texteligne"] = _with103["IntituleArticle"];
                }

                if (!string.IsNullOrEmpty(_with103["Designation1"].ToString()))
                {
                    //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, _with103["Designation1"].ToString(), General.SQL);
                    NewRow["Texteligne"] = _with103["Designation1"];
                }
                if (!string.IsNullOrEmpty(_with103["Designation2"].ToString()))
                {
                    //InsereLigneInGrille(General.SQL, boolAddrow, Index);
                    if (boolAddrow)
                        SSDBGridCorpsSource.Rows.Add(NewRow);
                    else
                        SSDBGridCorpsSource.Rows.InsertAt(NewRow, Index);
                    Index = Index + 1;
                    //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, _with103["Designation2"].ToString(), "");
                    NewRow = SSDBGridCorpsSource.NewRow();
                    NewRow["Texteligne"] = _with103["Designation2"].ToString();
                }

                if (!string.IsNullOrEmpty(_with103["Designation3"].ToString()))
                {
                    //InsereLigneInGrille(General.SQL, boolAddrow, Index);
                    if (boolAddrow)
                        SSDBGridCorpsSource.Rows.Add(NewRow);
                    else
                        SSDBGridCorpsSource.Rows.InsertAt(NewRow, Index);
                    Index = Index + 1;
                    //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, _with103["Designation3"].ToString(), "");
                    NewRow = SSDBGridCorpsSource.NewRow();
                    NewRow["Texteligne"] = _with103["Designation3"].ToString();
                }

                if (!string.IsNullOrEmpty(_with103["Designation4"].ToString()))
                {
                    //InsereLigneInGrille(General.SQL, boolAddrow, Index);
                    if (boolAddrow)
                        SSDBGridCorpsSource.Rows.Add(NewRow);
                    else
                        SSDBGridCorpsSource.Rows.InsertAt(NewRow, Index);
                    Index = Index + 1;
                    //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, _with103["Designation4"].ToString(), "");
                    NewRow = SSDBGridCorpsSource.NewRow();
                    NewRow["Texteligne"] = _with103["Designation4"].ToString();
                }
                if (!string.IsNullOrEmpty(_with103["Designation5"].ToString()))
                {
                    //InsereLigneInGrille(General.SQL, boolAddrow, Index);
                    if (boolAddrow)
                        SSDBGridCorpsSource.Rows.Add(NewRow);
                    else
                        SSDBGridCorpsSource.Rows.InsertAt(NewRow, Index);
                    Index = Index + 1;
                    //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, _with103["Designation5"].ToString(), "");
                    NewRow = SSDBGridCorpsSource.NewRow();
                    NewRow["Texteligne"] = _with103["Designation5"].ToString();
                }
                if (!string.IsNullOrEmpty(_with103["Designation6"].ToString()))
                {
                    //InsereLigneInGrille(General.SQL, boolAddrow, Index);
                    if (boolAddrow)
                        SSDBGridCorpsSource.Rows.Add(NewRow);
                    else
                        SSDBGridCorpsSource.Rows.InsertAt(NewRow, Index);
                    Index = Index + 1;
                    //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, _with103["Designation6"].ToString(), "");
                    NewRow = SSDBGridCorpsSource.NewRow();
                    NewRow["Texteligne"] = _with103["Designation6"].ToString();
                }
                if (!string.IsNullOrEmpty(_with103["Designation7"].ToString()))
                {
                    //InsereLigneInGrille(General.SQL, boolAddrow, Index);
                    if (boolAddrow)
                        SSDBGridCorpsSource.Rows.Add(NewRow);
                    else
                        SSDBGridCorpsSource.Rows.InsertAt(NewRow, Index);
                    Index = Index + 1;
                    //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, _with103["Designation7"].ToString(), "");
                    NewRow = SSDBGridCorpsSource.NewRow();
                    NewRow["Texteligne"] = _with103["Designation7"].ToString();
                }
                if (!string.IsNullOrEmpty(_with103["Designation8"].ToString()))
                {
                    //InsereLigneInGrille(General.SQL, boolAddrow, Index);
                    if (boolAddrow)
                        SSDBGridCorpsSource.Rows.Add(NewRow);
                    else
                        SSDBGridCorpsSource.Rows.InsertAt(NewRow, Index);
                    Index = Index + 1;
                    //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, _with103["Designation8"].ToString(), "");
                    NewRow = SSDBGridCorpsSource.NewRow();
                    NewRow["Texteligne"] = _with103["Designation8"].ToString();
                }
                if (!string.IsNullOrEmpty(_with103["Designation9"].ToString()))
                {
                    //InsereLigneInGrille(General.SQL, boolAddrow, Index);
                    if (boolAddrow)
                        SSDBGridCorpsSource.Rows.Add(NewRow);
                    else
                        SSDBGridCorpsSource.Rows.InsertAt(NewRow, Index);
                    Index = Index + 1;
                    //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, _with103["Designation9"].ToString(), "");
                    NewRow = SSDBGridCorpsSource.NewRow();
                    NewRow["Texteligne"] = _with103["Designation9"].ToString();
                }
                if (!string.IsNullOrEmpty(_with103["Designation10"].ToString()))
                {
                    //InsereLigneInGrille(General.SQL, boolAddrow, Index);
                    if (boolAddrow)
                        SSDBGridCorpsSource.Rows.Add(NewRow);
                    else
                        SSDBGridCorpsSource.Rows.InsertAt(NewRow, Index);
                    Index = Index + 1;
                    //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, _with103["Designation10"].ToString(), "");
                    NewRow = SSDBGridCorpsSource.NewRow();
                    NewRow["Texteligne"] = _with103["Designation10"].ToString();
                }

                //            SQL = PrepareLigne("Quantite", SSDBGridCorps, nz(!Quantite, "1"), SQL)

                if (Quantite != 0)
                {
                    //General.SQL = PrepareLigne("Quantite", SSDBGridCorps, Quantite.ToString(), General.SQL);
                    NewRow["Quantite"] = Quantite;
                }
                else
                {
                    //General.SQL = PrepareLigne("Quantite", SSDBGridCorps, "", General.SQL);
                    NewRow["Quantite"] = DBNull.Value;
                }

                if (boolTrouve == false)
                {
                    //General.SQL = PrepareLigne("Unite", SSDBGridCorps, _with103["UniteSousArt"] + "", General.SQL);
                    NewRow["Unite"] = _with103["UniteSousArt"];
                }

                if (rs["motps"] == DBNull.Value || Convert.ToInt32(rs["motps"]) == 0)
                {
                    iMOhud = DBNull.Value;
                }
                else
                {
                    iMOhud = rs["motps"];
                }
                //General.SQL = PrepareLigne("MOhud", SSDBGridCorps, iMOhud.ToString(), General.SQL);
                NewRow["MOhud"] = iMOhud;


                if (!string.IsNullOrEmpty(iMOhud.ToString()))
                {
                    //General.SQL = PrepareLigne("CODEMO", SSDBGridCorps, "S", General.SQL);
                    NewRow["CODEMO"] = "S";
                }
                if (!string.IsNullOrEmpty(iMOhud.ToString()))
                {
                    cPXHeured = Text316.Text;
                }
                else
                {
                    cPXHeured = DBNull.Value;
                }
                //General.SQL = PrepareLigne("PXHeured", SSDBGridCorps, cPXHeured.ToString(), General.SQL);
                NewRow["PXHeured"] = cPXHeured;


                if (rs["FO"] == DBNull.Value || Convert.ToInt32(rs["FO"]) == 0)
                {
                    cFOud = DBNull.Value;
                }
                else
                {
                    cFOud = Convert.ToDecimal(rs["FO"]);
                }

                if (boolTrouve == true)
                {
                    //General.SQL = PrepareLigne("FOud", SSDBGridCorps, cFOud.ToString(), General.SQL);
                    NewRow["FOud"] = cFOud;
                }
                else
                {
                    //General.SQL = PrepareLigne("FOud", SSDBGridCorps, General.nz(_with103["Prix"], "0").ToString(), General.SQL);
                    NewRow["FOud"] = General.nz(_with103["Prix"], "0");
                    cFOud = General.nz(_with103["Prix"], "0");
                }


                if (rs["sT"] == DBNull.Value || Convert.ToInt32(rs["sT"]) == 0)
                {
                    cSTud = DBNull.Value;
                }
                else
                {
                    cSTud = Convert.ToDecimal(rs["sT"]);
                }
                //General.SQL = PrepareLigne("STud", SSDBGridCorps, cSTud.ToString(), General.SQL);
                NewRow["STud"] = cSTud;

                if (!string.IsNullOrEmpty(iMOhud.ToString()) && Quantite != 0)
                {
                    iMOh = Convert.ToDouble(iMOhud) * Quantite;
                }
                else
                {
                    iMOh = DBNull.Value;
                }
                //General.SQL = PrepareLigne("MOhd", SSDBGridCorps, iMOh.ToString(), General.SQL);
                NewRow["STud"] = iMOh;

                if (!string.IsNullOrEmpty(iMOhud.ToString()) && Quantite != 0)
                {
                    cMOd = Convert.ToDouble(iMOhud) * Convert.ToDouble(cPXHeured) * Quantite;
                }
                else
                {
                    cMOd = DBNull.Value;
                }
                //General.SQL = PrepareLigne("MOd", SSDBGridCorps, cMOd.ToString(), General.SQL);

                NewRow["MOd"] = cMOd;

                if (!string.IsNullOrEmpty(cFOud.ToString()) && Quantite != 0)
                {
                    cFOd = Convert.ToDouble(cFOud) * Quantite;
                }
                else
                {
                    cFOd = DBNull.Value;
                }
                //General.SQL = PrepareLigne("FOd", SSDBGridCorps, cFOd.ToString(), General.SQL);
                NewRow["FOd"] = cFOd;

                if (!string.IsNullOrEmpty(cSTud.ToString()) && Quantite != 0)
                {
                    cSTd = Convert.ToDouble(cSTud) * Quantite;
                }
                else
                {
                    cSTd = DBNull.Value;
                }
                //General.SQL = PrepareLigne("STd", SSDBGridCorps, cSTd.ToString(), General.SQL);
                NewRow["STd"] = cSTd;

                if (!string.IsNullOrEmpty(cMOd.ToString()) || !string.IsNullOrEmpty(cFOd.ToString()) || !string.IsNullOrEmpty(cSTd.ToString()))
                {
                    cTotalDebours = General.nz(cMOd, "0").ToString() + General.nz(cFOd, "0") + General.nz(cSTd, "0");
                }
                else
                {
                    cTotalDebours = DBNull.Value;
                }
                //General.SQL = PrepareLigne("TotalDebours", SSDBGridCorps, cTotalDebours.ToString(), General.SQL);
                NewRow["TotalDebours"] = cTotalDebours;

                if (!string.IsNullOrEmpty(iMOhud.ToString()))
                {
                    //General.SQL = PrepareLigne("KMO", SSDBGridCorps, Text315.ToString(), General.SQL);
                    NewRow["KMO"] = Text315.Text;
                }
                else
                {
                    //General.SQL = PrepareLigne("KMO", SSDBGridCorps, Text315.ToString(), General.SQL);
                    NewRow["KMO"] = Text315.Text;
                }
                if (!string.IsNullOrEmpty(iMOhud.ToString()))
                {
                    cMO = Convert.ToDouble(Text315.Text) * Convert.ToDouble(cMOd);
                }
                else
                {
                    cMO = DBNull.Value;
                }
                //General.SQL = PrepareLigne("MO", SSDBGridCorps, cMO.ToString(), General.SQL);
                NewRow["MO"] = cMO;

                if (!string.IsNullOrEmpty(cFOud.ToString()))
                {
                    //General.SQL = PrepareLigne("KFO", SSDBGridCorps, Text314.Text, General.SQL);
                    NewRow["KFO"] = Text314.Text;
                }
                else
                {
                    //General.SQL = PrepareLigne("KFO", SSDBGridCorps, Text314.Text, General.SQL);
                    NewRow["KFO"] = Text314.Text;
                }
                if (!string.IsNullOrEmpty(cFOud.ToString()))
                {
                    cFO = Convert.ToDouble(Text314.Text) * Convert.ToDouble(cFOd);
                }
                else
                {
                    cFO = DBNull.Value;
                }
                //General.SQL = PrepareLigne("FO", SSDBGridCorps, cFO.ToString(), General.SQL);
                NewRow["FO"] = cFO;
                if (!string.IsNullOrEmpty(cSTud.ToString()))
                {
                    //General.SQL = PrepareLigne("KST", SSDBGridCorps, Text313.Text, General.SQL);
                    NewRow["KST"] = Text313.Text;
                }
                else
                {
                    //General.SQL = PrepareLigne("KST", SSDBGridCorps, Text313.Text, General.SQL);
                    NewRow["KST"] = Text313.Text;
                }
                if (cST != null && !string.IsNullOrEmpty(cST.ToString()))
                {
                    cST = Convert.ToDouble(Text313.Text) * Convert.ToDouble(cSTd);
                }
                else
                {
                    cST = DBNull.Value;
                }
                //General.SQL = PrepareLigne("ST", SSDBGridCorps, cST.ToString(), General.SQL);
                NewRow["ST"] = cST;
                if (!string.IsNullOrEmpty(cMO.ToString()) || !string.IsNullOrEmpty(cFO.ToString()) || !string.IsNullOrEmpty(cST.ToString()))
                {
                    cTotalVente = Convert.ToDouble(General.nz(cMO, "0")) + Convert.ToDouble(General.nz(cFO, "0")) + Convert.ToDouble(General.nz(cST, "0"));
                }
                else
                {
                    cTotalVente = DBNull.Value;
                }

                //General.SQL = PrepareLigne("TotalVente", SSDBGridCorps, cTotalVente.ToString(), General.SQL);
                NewRow["TotalVente"] = cTotalVente;


                //General.SQL = PrepareLigne("CodeTVA", SSDBGridCorps, TauxTVADefaut, General.SQL);

                NewRow["CodeTVA"] = TauxTVADefaut;



                //    If IsNull(rs2("codetva")) Then
                //        sCodeTVA = Text3(24)    'gsCodeTVA
                //    Else
                //        sCodeTVA = fncLibelleTVA(rs2("codetva"))
                //    End If
                //InsereLigneInGrille(General.SQL, boolAddrow, Index);

                if (boolAddrow)
                    SSDBGridCorpsSource.Rows.Add(NewRow);
                else
                    SSDBGridCorpsSource.Rows.InsertAt(NewRow, Index);


                Index = Index + 1;
                if (General.sGecetV3 == "1")
                {
                    insereSousArticleV3(strCodeArticle, ref Index, boolAddrow, Quantite);
                }
                else
                {
                    insereSousArticle(strCodeArticle, ref Index, boolAddrow, Quantite);
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";InsertionArticle");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strCodeArticle"></param>
        /// <param name="Index"></param>
        /// <param name="boolAddrow"></param>
        /// <param name="Quantite"></param>
        private void insereSousArticle(string strCodeArticle, ref int Index, bool boolAddrow, int Quantite = 0)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"insereSousArticle() - Enter In The Function - strCodeArticle = {strCodeArticle} " +
                                        $"- Index = {Index} - boolAddrow = {boolAddrow} - Quantite = {Quantite}");
            //===> Fin Modif Mondir

            string sNomBaseProd = null;
            string sNomBaseGecet = null;
            double dblPrixApresCoef = 0;

            try
            {

                sNomBaseProd = "[" + General.adocnn.Database + "].dbo.";
                sNomBaseGecet = "[" + ModParametre.adoGecet.Database + "].dbo.";

                ado3temp = new DataTable();
                ModAdo modAdoado3temp = new ModAdo();
                //    SQL = "SELECT Sousarticle.Chrono, Sousarticle.Designation," _
                //'        & " Sousarticle.Quantite, PrixApresCoef" _
                //'        & " From Sousarticle" _
                //'        & " Where Sousarticle.CodeArticle ='" & strCodeArticle & "'" _
                //'        & " ORDER BY Sousarticle.NumLigne "

                //    SQL = "SELECT " & sNomBaseProd & "DEV_FournitArticleDetail.CodeChrono AS Chrono, " & sNomBaseProd & "DEV_FournitArticleDetail.Designation, " & sNomBaseProd & "DEV_FournitArticleDetail.Qte AS Quantite , "
                //    SQL = SQL & " " & sNomBaseGecet & "Dtiarti.Libelle AS LibelleGecet, " & sNomBaseGecet & "Dtiprix.[Prix net achat] AS PrixApresCoef, " & sNomBaseGecet & "Dtiprix.fouunite AS [Unite] "
                //    SQL = SQL & " FROM " & sNomBaseProd & "DEV_FournitArticleDetail "
                //    SQL = SQL & " LEFT OUTER JOIN " & sNomBaseGecet & "Dtiarti ON "
                //    SQL = SQL & sNomBaseProd & "DEV_FournitArticleDetail.CodeChrono=" & sNomBaseGecet & "Dtiarti.Chrono "
                //    SQL = SQL & " LEFT OUTER JOIN " & sNomBaseGecet & "Dtiprix ON "
                //    SQL = SQL & sNomBaseGecet & "Dtiarti.Chrono=" & sNomBaseGecet & "Dtiprix.Chrono "
                //    SQL = SQL & " WHERE " & sNomBaseProd & "DEV_FournitArticleDetail.CodeArticle='" & strCodeArticle & "' "
                //
                //    SQL = SQL & " AND (" & sNomBaseGecet & "Dtiprix.[Prix net achat] IN("
                //    SQL = SQL & "SELECT MAX(" & sNomBaseGecet & "Dtiprix.[Prix net achat]) AS MaxPrix"
                //    SQL = SQL & " FROM " & sNomBaseProd & "DEV_FournitArticleDetail "
                //    SQL = SQL & " LEFT OUTER JOIN " & sNomBaseGecet & "Dtiarti ON "
                //    SQL = SQL & sNomBaseProd & "DEV_FournitArticleDetail.CodeChrono=" & sNomBaseGecet & "Dtiarti.Chrono "
                //    SQL = SQL & " LEFT OUTER JOIN " & sNomBaseGecet & "Dtiprix ON "
                //    SQL = SQL & sNomBaseGecet & "Dtiarti.Chrono=" & sNomBaseGecet & "Dtiprix.Chrono "
                //    SQL = SQL & " WHERE " & sNomBaseProd & "DEV_FournitArticleDetail.CodeArticle='" & strCodeArticle & "' "
                //    SQL = SQL & " GROUP BY " & sNomBaseProd & "DEV_FournitArticleDetail.CodeChrono, " & sNomBaseProd & "DEV_FournitArticleDetail.Qte, "
                //    SQL = SQL & " " & sNomBaseGecet & "Dtiarti.Libelle) OR " & sNomBaseGecet & "Dtiprix.[Prix net achat] IS NULL)"
                //
                //    SQL = SQL & " GROUP BY " & sNomBaseProd & "DEV_FournitArticleDetail.CodeChrono, " & sNomBaseProd & "DEV_FournitArticleDetail.Designation, " & sNomBaseProd & "DEV_FournitArticleDetail.Qte, "
                //    SQL = SQL & " " & sNomBaseGecet & "Dtiarti.Libelle, " & sNomBaseGecet & "Dtiprix.[Prix net achat]," & sNomBaseGecet & "Dtiprix.fouunite, "
                //    SQL = SQL & sNomBaseProd & "DEV_FournitArticleDetail.NoLigne "
                //    SQL = SQL & " ORDER BY " & sNomBaseProd & "DEV_FournitArticleDetail.NoLigne "


                General.SQL = "SELECT " + sNomBaseProd + "DEV_FournitArticleDetail.CodeChrono AS Chrono, " + sNomBaseProd + "DEV_FournitArticleDetail.Designation, " + sNomBaseProd + "DEV_FournitArticleDetail.Qte AS Quantite , ";

                //SQL = SQL & " " & sNomBaseGecet & "ART_Article.ART_Libelle AS LibelleGecet, " _
                //& sNomBaseGecet & "ARTP_ArticlePrix.ARTP_PrixNetDevis AS PrixApresCoef, " _
                //& sNomBaseGecet & "ARTP_ArticlePrix.ARTP_UniteFourn AS [Unite] "
                //==== modif du 17 11 2014, changmenet de l'unité devis.
                General.SQL = General.SQL + " " + sNomBaseGecet + "ART_Article.ART_Libelle AS LibelleGecet, " + sNomBaseGecet + "ARTP_ArticlePrix.ARTP_PrixNetDevis AS PrixApresCoef, " + sNomBaseGecet + "ARTP_ArticlePrix.ARTP_UniteDevis AS [Unite] ";

                General.SQL = General.SQL + " FROM " + sNomBaseProd + "DEV_FournitArticleDetail ";
                General.SQL = General.SQL + " LEFT OUTER JOIN " + sNomBaseGecet + "ART_Article ON ";

                General.SQL = General.SQL + sNomBaseProd + "DEV_FournitArticleDetail.CodeChrono=" + sNomBaseGecet + "ART_Article.ART_Chrono ";

                General.SQL = General.SQL + " LEFT OUTER JOIN " + sNomBaseGecet + "ARTP_ArticlePrix ON ";

                General.SQL = General.SQL + sNomBaseGecet + "ART_Article.ART_Chrono=" + sNomBaseGecet + "ARTP_ArticlePrix.ART_Chrono ";

                General.SQL = General.SQL + " WHERE " + sNomBaseProd + "DEV_FournitArticleDetail.CodeArticle='" + StdSQLchaine.gFr_DoublerQuote(strCodeArticle) + "' ";

                General.SQL = General.SQL + " AND (" + sNomBaseGecet + "ARTP_ArticlePrix.ARTP_PrixNetDevis IN(";

                General.SQL = General.SQL + "SELECT MAX(" + sNomBaseGecet + "ARTP_ArticlePrix.ARTP_PrixNetDevis) AS MaxPrix";

                General.SQL = General.SQL + " FROM " + sNomBaseProd + "DEV_FournitArticleDetail ";

                General.SQL = General.SQL + " LEFT OUTER JOIN " + sNomBaseGecet + "ART_Article ON ";

                General.SQL = General.SQL + sNomBaseProd + "DEV_FournitArticleDetail.CodeChrono=" + sNomBaseGecet + "ART_Article.ART_Chrono ";

                General.SQL = General.SQL + " LEFT OUTER JOIN " + sNomBaseGecet + "ARTP_ArticlePrix ON ";

                General.SQL = General.SQL + sNomBaseGecet + "ART_Article.ART_Chrono=" + sNomBaseGecet + "ARTP_ArticlePrix.ART_Chrono ";

                General.SQL = General.SQL + " WHERE " + sNomBaseProd + "DEV_FournitArticleDetail.CodeArticle='" + strCodeArticle + "' ";

                General.SQL = General.SQL + " GROUP BY " + sNomBaseProd + "DEV_FournitArticleDetail.CodeChrono, " + sNomBaseProd + "DEV_FournitArticleDetail.Qte, ";

                General.SQL = General.SQL + " " + sNomBaseGecet + "ART_Article.ART_Libelle) OR " + sNomBaseGecet + "ARTP_ArticlePrix.ARTP_PrixNetDevis IS NULL)";

                General.SQL = General.SQL + " GROUP BY " + sNomBaseProd + "DEV_FournitArticleDetail.CodeChrono, " + sNomBaseProd + "DEV_FournitArticleDetail.Designation, " + sNomBaseProd + "DEV_FournitArticleDetail.Qte, ";

                //SQL = SQL & " " & sNomBaseGecet & "ART_Article.ART_Libelle, " & sNomBaseGecet & "ARTP_ArticlePrix.ARTP_PrixNetDevis," _
                //& sNomBaseGecet & "ARTP_ArticlePrix.ARTP_UniteFourn, "
                //==== modif du 17 11 2014, changmenet de l'unité devis.
                General.SQL = General.SQL + " " + sNomBaseGecet + "ART_Article.ART_Libelle, " + sNomBaseGecet + "ARTP_ArticlePrix.ARTP_PrixNetDevis," + sNomBaseGecet + "ARTP_ArticlePrix.ARTP_UniteDevis, ";

                General.SQL = General.SQL + sNomBaseProd + "DEV_FournitArticleDetail.NoLigne ";
                General.SQL = General.SQL + " ORDER BY " + sNomBaseProd + "DEV_FournitArticleDetail.NoLigne ";

                var _with104 = ado3temp;
                _with104 = modAdoado3temp.fc_OpenRecordSet(General.SQL);
                if (_with104.Rows.Count > 0)
                {
                    var SSDBGridCorpsSource = SSDBGridCorps.DataSource as DataTable;

                    foreach (DataRow Row in _with104.Rows)
                    {
                        var NewRow = SSDBGridCorpsSource.NewRow();

                        dblPrixApresCoef = Convert.ToDouble(General.nz(Row["PrixApresCoef"].ToString().Replace(",", ".").Replace(" ", ""), "0").ToString());
                        General.SQL = "";
                        //General.SQL = PrepareLigne("CodeSousArticle", SSDBGridCorps, StdSQLchaine.fc_CtrlCharDevis(Row["Chrono"] + ""), "");
                        NewRow["CodeSousArticle"] = StdSQLchaine.fc_CtrlCharDevis(Row["Chrono"] + "");


                        if (string.IsNullOrEmpty(General.nz(Row["LibelleGecet"], "").ToString()))
                        {
                            //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, Row["Designation"].ToString(), General.SQL);
                            NewRow["Texteligne"] = Row["Designation"] + "";
                        }
                        else
                        {
                            //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, Row["LibelleGecet"].ToString(), General.SQL);
                            NewRow["Texteligne"] = Row["LibelleGecet"] + "";
                        }
                        if (Row["Quantite"] != DBNull.Value)
                        {
                            //General.SQL = PrepareLigne("quantite", SSDBGridCorps, (Convert.ToDouble(Row["Quantite"]) * Quantite).ToString(), General.SQL);
                            NewRow["quantite"] = (Convert.ToDouble(Row["Quantite"]) * Quantite).ToString();
                        }
                        else
                        {
                            //General.SQL = PrepareLigne("quantite", SSDBGridCorps, (1 * Quantite).ToString(), General.SQL);
                            NewRow["quantite"] = (1 * Quantite).ToString();
                        }
                        //General.SQL = PrepareLigne("Unite", SSDBGridCorps, Row["Unite"].ToString(), General.SQL);
                        NewRow["Unite"] = Row["Unite"];
                        //General.SQL = PrepareLigne("pxHeured", SSDBGridCorps, Text316.Text, General.SQL);
                        NewRow["pxHeured"] = Text316.Text;
                        //General.SQL = PrepareLigne("FOud", SSDBGridCorps, dblPrixApresCoef.ToString(), General.SQL);
                        NewRow["FOud"] = dblPrixApresCoef;
                        //General.SQL = PrepareLigne("FOd", SSDBGridCorps, (dblPrixApresCoef * Convert.ToDouble(Row["Quantite"]) * Quantite).ToString(), General.SQL);
                        NewRow["FOd"] = dblPrixApresCoef * Convert.ToDouble(Row["Quantite"]) * Quantite;
                        //General.SQL = PrepareLigne("TotalDebours", SSDBGridCorps, (dblPrixApresCoef * Convert.ToDouble(Row["Quantite"]) * Quantite).ToString(), General.SQL);
                        NewRow["TotalDebours"] = dblPrixApresCoef * Convert.ToDouble(Row["Quantite"]) * Quantite;
                        //General.SQL = PrepareLigne("KMO", SSDBGridCorps, Text315.Text, General.SQL);
                        NewRow["KMO"] = Text315.Text;
                        //General.SQL = PrepareLigne("KFO", SSDBGridCorps, Text314.Text, General.SQL);
                        NewRow["KFO"] = Text314.Text;
                        //General.SQL = PrepareLigne("FO", SSDBGridCorps, (dblPrixApresCoef * Convert.ToDouble(Row["Quantite"]) * Convert.ToDouble(Text314.Text) * Quantite).ToString(), General.SQL);
                        NewRow["FO"] = dblPrixApresCoef * Convert.ToDouble(Row["Quantite"]) * Convert.ToDouble(Text314.Text) * Quantite;
                        //General.SQL = PrepareLigne("KST", SSDBGridCorps, Text313.Text, General.SQL);
                        NewRow["KST"] = Text313.Text;
                        //General.SQL = PrepareLigne("TotalVente", SSDBGridCorps, Convert.ToString(dblPrixApresCoef * Convert.ToDouble(Row["Quantite"]) * Convert.ToDouble(Text314.Text) * Quantite), General.SQL);
                        NewRow["TotalVente"] = dblPrixApresCoef * Convert.ToDouble(Row["Quantite"]) * Convert.ToDouble(Text314.Text) * Quantite;
                        //General.SQL = PrepareLigne("CodeTva", SSDBGridCorps, TauxTVADefaut, General.SQL);
                        NewRow["CodeTva"] = TauxTVADefaut;
                        //InsereLigneInGrille(General.SQL, boolAddrow, Index);
                        if (boolAddrow)
                            SSDBGridCorpsSource.Rows.Add(NewRow);
                        else
                            SSDBGridCorpsSource.Rows.InsertAt(NewRow, Index);
                        Index = Index + 1;
                    }
                }
                modAdoado3temp?.Dispose();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";insereSousArticle");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Index"></param>
        /// <param name="boolAddrow"></param>
        private void inserefourniture(ref int Index, bool boolAddrow)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"inserefourniture() - Enter In The Function - Index = {Index} - boolAddrow = {boolAddrow}");
            //===> Fin Modif Mondir

            try
            {
                double PrixNet = 0;

                SearchTemplate fg = new SearchTemplate();
                var _with105 = fg.ugResultat;

                var SSDBGridCorpsSource = SSDBGridCorps.DataSource as DataTable;
                var newRow = SSDBGridCorpsSource.NewRow();

                PrixNet = Convert.ToDouble(_with105.ActiveRow.Cells["prix net achat"].Value.ToString().Replace(",", "."));
                General.SQL = "";
                //General.SQL = PrepareLigne("codesousarticle", SSDBGridCorps, StdSQLchaine.fc_CtrlCharDevis(_with105.ActiveRow.Cells["chrono"].Value + ""), "");
                newRow["codesousarticle"] = StdSQLchaine.fc_CtrlCharDevis(_with105.ActiveRow.Cells["chrono"].Value + "");
                //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, (_with105.ActiveRow.Cells["designation"].Value.ToString()), General.SQL);
                newRow["Texteligne"] = _with105.ActiveRow.Cells["designation"].Value.ToString();
                //General.SQL = PrepareLigne("quantite", SSDBGridCorps, "1", General.SQL);
                newRow["quantite"] = 1;
                //General.SQL = PrepareLigne("pxHeured", SSDBGridCorps, Text316.Text, General.SQL);
                newRow["pxHeured"] = Text316.Text;
                //General.SQL = PrepareLigne("FOud", SSDBGridCorps, Convert.ToString(Math.Round(PrixNet * 1.1, 2)), General.SQL);
                newRow["FOud"] = Convert.ToString(Math.Round(PrixNet * 1.1, 2));
                //General.SQL = PrepareLigne("FOd", SSDBGridCorps, Convert.ToString(Math.Round(PrixNet * 1.1, 2)), General.SQL);
                newRow["FOd"] = Convert.ToString(Math.Round(PrixNet * 1.1, 2));
                //General.SQL = PrepareLigne("TotalDebours", SSDBGridCorps, Convert.ToString(Math.Round(PrixNet * 1.1, 2)), General.SQL);
                newRow["TotalDebours"] = Convert.ToString(Math.Round(PrixNet * 1.1, 2));
                //General.SQL = PrepareLigne("KMO", SSDBGridCorps, Text315.Text, General.SQL);
                newRow["KMO"] = Text315.Text;
                //General.SQL = PrepareLigne("KFO", SSDBGridCorps, Text314.Text, General.SQL);
                newRow["KFO"] = Text314.Text;
                //General.SQL = PrepareLigne("FO", SSDBGridCorps, Convert.ToString(Math.Round(PrixNet * Convert.ToDouble(Text314.Text) * 1.1, 2)), General.SQL);
                newRow["FO"] = Convert.ToString(Math.Round(PrixNet * Convert.ToDouble(Text314.Text) * 1.1, 2));
                //General.SQL = PrepareLigne("KST", SSDBGridCorps, Text313.Text, General.SQL);
                newRow["KST"] = Text313.Text;
                //General.SQL = PrepareLigne("TotalVente", SSDBGridCorps, Convert.ToString(Math.Round(PrixNet * Convert.ToDouble(Convert.ToDouble(Text314.Text) * 1.1), 2)), General.SQL);
                newRow["TotalVente"] = Convert.ToString(Math.Round(PrixNet * Convert.ToDouble(Convert.ToDouble(Text314.Text) * 1.1), 2));
                //General.SQL = PrepareLigne("CodeTva", SSDBGridCorps, TauxTVADefaut, General.SQL);
                newRow["CodeTva"] = TauxTVADefaut;
                //General.SQL = PrepareLigne("fournisseur", SSDBGridCorps, (_with105.ActiveRow.Cells["Fournisseur"].Value.ToString()), General.SQL);
                newRow["fournisseur"] = _with105.ActiveRow.Cells["Fournisseur"].Value.ToString();
                //InsereLigneInGrille(General.SQL, boolAddrow, Index);
                if (boolAddrow)
                    SSDBGridCorpsSource.Rows.Add(newRow);
                else
                    SSDBGridCorpsSource.Rows.InsertAt(newRow, Index);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";inserefourniture");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGridCorps_AfterCellUpdate(object sender, CellEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSDBGridCorps_AfterCellUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                var _with50 = SSDBGridCorps;
                if (SSDBGridCorps.ActiveCell == null) return;
                var Index = SSDBGridCorps.ActiveCell.Column.Index;

                if (boolEntree == false)
                {
                    //---Champs code famille
                    if (Index == 2)
                    {
                        Integrer(SSDBGridCorps, Index);
                        return;
                    }

                    //---Champs code sous famille (Poste)
                    if (Index == 3)
                    {
                        Integrer(SSDBGridCorps, Index);
                        return;
                    }

                    //---Champs code article
                    if (Index == 4)
                    {
                        Integrer(SSDBGridCorps, Index);
                        return;
                    }

                    if (Index == 5)
                    {
                        var manager = SSDBGridCorps.EventManager;
                        manager.AllEventsEnabled = false;
                        if (General.sGecetV2 == "1" || General.sGecetV3 == "1")
                        {
                            fc_StockFourGecet(_with50.ActiveRow.Cells["CodeSousArticle"].Text);

                        }
                        else
                        {

                            if (General.IsNumeric(_with50.ActiveRow.Cells["CodeSousArticle"].Text))
                            {
                                fc_StockFourGecet(_with50.ActiveRow.Cells["CodeSousArticle"].Text);
                            }
                            else if (!string.IsNullOrEmpty(_with50.ActiveRow.Cells["CodeSousArticle"].Text))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi le caractère '" +
                                    _with50.ActiveRow.Cells["CodeSousArticle"].Text +
                                    "' dans la colonne 'CodeSousArticle', celle-ci n'accepte que les codes numériques (code Chrono gecet).");

                            }
                        }
                        manager.AllEventsEnabled = true;
                    }

                }

                SSDBGridCorps.Rows.Refresh(RefreshRow.FireInitializeRow);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSDBGridCorps_AfterColUpdate");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGridCorps_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSDBGridCorps_BeforeExitEditMode() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                var _with51 = SSDBGridCorps;
                if (SSDBGridCorps.ActiveCell == null || SSDBGridCorps.ActiveRow == null)
                    return;

                if (SSDBGridCorps.ActiveCell.Column.Index == SSDBGridCorps.DisplayLayout.Bands[0].Columns["Quantite"].Index)
                {
                    if (!string.IsNullOrEmpty(SSDBGridCorps.ActiveCell.Text) && !General.IsNumeric(SSDBGridCorps.ActiveCell.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["Quantite"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["Quantite"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == SSDBGridCorps.DisplayLayout.Bands[0].Columns["Mohud"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["Mohud"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["Mohud"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["Mohud"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["Mohud"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == SSDBGridCorps.DisplayLayout.Bands[0].Columns["PxHeured"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["PxHeured"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["PxHeured"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["PxHeured"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["PxHeured"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == _with51.DisplayLayout.Bands[0].Columns["Mod"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["Mod"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["Mod"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["Mod"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["Mod"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == _with51.DisplayLayout.Bands[0].Columns["Foud"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["Foud"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["Foud"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["Foud"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["Foud"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == _with51.DisplayLayout.Bands[0].Columns["Fod"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["Fod"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["Fod"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["Fod"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["Fod"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        e.Cancel = true;
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == _with51.DisplayLayout.Bands[0].Columns["Stud"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["Stud"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["Stud"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["Stud"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["Stud"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == _with51.DisplayLayout.Bands[0].Columns["Std"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["Std"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["Std"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["Std"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["Std"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == _with51.DisplayLayout.Bands[0].Columns["TotalDebours"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["TotalDebours"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["TotalDebours"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["TotalDebours"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["TotalDebours"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == _with51.DisplayLayout.Bands[0].Columns["Kmo"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["Kmo"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["Kmo"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["Kmo"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["Kmo"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == _with51.DisplayLayout.Bands[0].Columns["MO"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["MO"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["MO"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["MO"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["MO"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == _with51.DisplayLayout.Bands[0].Columns["Kfo"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["Kfo"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["Kfo"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["Kfo"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["Kfo"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == _with51.DisplayLayout.Bands[0].Columns["FO"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["FO"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["FO"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["FO"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["FO"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == _with51.DisplayLayout.Bands[0].Columns["Kst"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["Kst"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["Kst"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["Kst"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["Kst"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == _with51.DisplayLayout.Bands[0].Columns["ST"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["ST"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["ST"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["ST"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["ST"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == _with51.DisplayLayout.Bands[0].Columns["TotalVente"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["TotalVente"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["TotalVente"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["TotalVente"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["TotalVente"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }
                else if (SSDBGridCorps.ActiveCell.Column.Index == _with51.DisplayLayout.Bands[0].Columns["CodeTVA"].Index)
                {
                    if (!string.IsNullOrEmpty(_with51.ActiveRow.Cells["CodeTVA"].Text) && !General.IsNumeric(_with51.ActiveRow.Cells["CodeTVA"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi dans la colonne '" + _with51.ActiveRow.Cells["CodeTVA"].Column.Header.Caption + "' une donnée non numérique (" + _with51.ActiveRow.Cells["CodeTVA"].Text + ")," + " veuillez d'abord modifier cette valeur avant de valider.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        SSDBGridCorps.ActiveCell.CancelUpdate();
                        //e.Cancel = true;
                        return;
                    }
                }

                //=== fin modif du 07 03 2018.
                var manager = SSDBGridCorps.EventManager;
                if (!string.IsNullOrEmpty(SSDBGridCorps.ActiveRow.Cells["Mohud"].Text) && SSDBGridCorps.ActiveRow.Cells["Mohud"].Text != "0")
                {
                    manager.AllEventsEnabled = false;
                    if (string.IsNullOrEmpty(SSDBGridCorps.ActiveRow.Cells["Quantite"].Text))
                    {
                        SSDBGridCorps.ActiveRow.Cells["Quantite"].Value = 1;
                    }
                    if (string.IsNullOrEmpty(SSDBGridCorps.ActiveRow.Cells["pxHeured"].Text))
                    {
                        SSDBGridCorps.ActiveRow.Cells["pxHeured"].Value = Text316.Text;
                    }
                    if (string.IsNullOrEmpty(SSDBGridCorps.ActiveRow.Cells["KMO"].Text))
                    {
                        SSDBGridCorps.ActiveRow.Cells["KMO"].Value = Text315.Text;
                    }
                    if (string.IsNullOrEmpty(SSDBGridCorps.ActiveRow.Cells["CodeMO"].Text))
                    {
                        SSDBGridCorps.ActiveRow.Cells["CodeMO"].Value = "S";
                    }
                    manager.AllEventsEnabled = true;
                }
                else
                {
                    if (SSDBGridCorps.ActiveRow.Cells[3].Value == DBNull.Value)
                    {
                        manager.AllEventsEnabled = false;
                        SSDBGridCorps.ActiveRow.Cells["pxHeured"].Value = 0;
                        SSDBGridCorps.ActiveRow.Cells["KMO"].Value = 0;
                        SSDBGridCorps.ActiveRow.Cells["CodeMO"].Value = DBNull.Value;
                        manager.AllEventsEnabled = true;
                    }
                }

                //    If ColIndex = 9 Then
                //        SSDBGridCorps.Columns(ColIndex].Value = SSDBGridCorps.Columns(ColIndex].Text
                //    End If
                //    With SSDBGridCorps
                //    If ColIndex = 12 Then
                //        If .Columns("Mohud"].Text <> "" And .Columns("Mohud"].Text <> "0" Then
                //            MsgBox "Vous ne pouvez pas insérer de prix en fourniture : " && vbCrLf _
                //'                    && "Cette ligne contient de la main d'oeuvre", vbInformation, "Insertion de fourniture annulée"
                //            'Cancel = True
                //            .Columns(12].Text = OldValue
                //            Exit Sub
                //        End If
                //    ElseIf ColIndex = 13 Then
                //        If .Columns("Mohud"].Text <> "" And .Columns("Mohud"].Text <> "0" Then
                //            MsgBox "Vous ne pouvez pas insérer de prix en sous-traitance : " && vbCrLf _
                //'                    && "Cette ligne contient de la main d'oeuvre", vbInformation, "Insertion de fourniture annulée"
                //            'Cancel = True
                //            .Columns(13].Text = OldValue
                //            Exit Sub
                //        End If
                //    ElseIf ColIndex = 9 Then
                //        If .Columns("Foud"].Text <> "" Or .Columns("Stud"].Text <> "" Then
                //            MsgBox "Vous ne pouvez pas insérer de main d'oeuvre : " && vbCrLf _
                //'                    && "Cette ligne contient des fournitures", vbInformation, "Insertion de fourniture annulée"
                //            'Cancel = True
                //            .Columns(9].Text = OldValue
                //            Exit Sub
                //        End If
                //    End If
                //    End With

                //mise à jour des colonnes pour les coefficients
                var _with52 = SSDBGridCorps;
                manager.AllEventsEnabled = false;
                if (_with52.ActiveRow.Cells["Foud"].Text != "0" && !string.IsNullOrEmpty(_with52.ActiveRow.Cells["Foud"].Text))
                {
                    if (Text314.Text != "")
                        _with52.ActiveRow.Cells["KFO"].Value = Convert.ToDouble(Text314.Text);
                }
                else
                {
                    _with52.ActiveRow.Cells["KFO"].Value = DBNull.Value;
                }
                if (_with52.ActiveRow.Cells["stud"].Text != "0" && !string.IsNullOrEmpty(_with52.ActiveRow.Cells["stud"].Text))
                {
                    if (Text313.Text != "")
                        _with52.ActiveRow.Cells["KST"].Value = Convert.ToDouble(Text313.Text);
                }
                else
                {
                    _with52.ActiveRow.Cells["KST"].Value = DBNull.Value;
                }

                //=== modif du 28 avril 2014 enleve les espaces.
                if (SSDBGridCorps.ActiveCell.Column.Index == SSDBGridCorps.DisplayLayout.Bands[0].Columns["Quantite"].Index)
                {
                    SSDBGridCorps.ActiveRow.Cells["Quantite"].Value = SSDBGridCorps.ActiveRow.Cells["Quantite"].Text;

                }
                manager.AllEventsEnabled = true;

                if (SSDBGridCorps.ActiveCell.Column.Index > 6)
                {
                    if (string.IsNullOrEmpty(SSDBGridCorps.ActiveRow.Cells["Quantite"].Text) && SSDBGridCorps.ActiveRow.Cells["Quantite"].Text == "0")
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez d'abord saisir une quantité dans la ligne " + SSDBGridCorps.ActiveRow.Cells["NumeroLigne"].Text);
                    }
                    else
                    {
                        if (!General.IsNumeric(SSDBGridCorps.ActiveRow.Cells["Quantite"].Value.ToString()) && !string.IsNullOrEmpty(SSDBGridCorps.ActiveRow.Cells["Quantite"].Text))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La colonne Quantité doit être de type numérique.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                    }
                    if (SSDBGridCorps.ActiveRow.Cells[3].Value == DBNull.Value)
                        prcCalculDetail();
                }

                manager.AllEventsEnabled = false;
                if (string.IsNullOrEmpty(SSDBGridCorps.ActiveRow.Cells["codetva"].Value.ToString())
                    && SSDBGridCorps.ActiveRow.Cells["quantite"].Value != DBNull.Value
                    && General.IsNumeric(SSDBGridCorps.ActiveRow.Cells["quantite"].Value.ToString())
                    && Convert.ToInt32(Convert.ToDouble(SSDBGridCorps.ActiveRow.Cells["quantite"].Value)) != 0)
                {
                    if (SSDBGridCorps.ActiveRow.Cells[3].Value == DBNull.Value)
                        SSDBGridCorps.ActiveRow.Cells["codetva"].Value = Convert.ToDouble(txtTaux.Text);
                }
                bInsertion = false;

                SSDBGridCorps.ActiveRow.Cells["CodeMO"].Value = SSDBGridCorps.ActiveRow.Cells["CodeMO"].Text.ToString().ToUpper();
                manager.AllEventsEnabled = true;

                SSDBGridCorps.Rows.Refresh(RefreshRow.FireInitializeRow);
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";SSDBGridCorps_BeforeColUpdate");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        public void prcCalculDetail()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"prcCalculDetail() - Enter In The Function");
            //===> Fin Modif Mondir

            int i = 0;
            var manager = SSDBGridCorps.EventManager;
            manager.AllEventsEnabled = false;

            try
            {
                var _with98 = SSDBGridCorps;
                if (!string.IsNullOrEmpty(_with98.ActiveRow.Cells["Quantite"].Text))
                {
                    _with98.ActiveRow.Cells["MOhd"].Value = Convert.ToString(Convert.ToDouble(_with98.ActiveRow.Cells["Quantite"].Text) * Convert.ToDouble(General.nz((_with98.ActiveRow.Cells["Mohud"].Text), "0")));
                    _with98.ActiveRow.Cells["Mod"].Value = Convert.ToDouble(_with98.ActiveRow.Cells["Mohd"].Text) * Convert.ToDouble(General.nz((_with98.ActiveRow.Cells["PxHeured"].Text), "0"));
                    _with98.ActiveRow.Cells["Fod"].Value = Convert.ToDouble(_with98.ActiveRow.Cells["Quantite"].Text) * Convert.ToDouble(General.nz((_with98.ActiveRow.Cells["Foud"].Text), "0"));
                    _with98.ActiveRow.Cells["Std"].Value = Convert.ToDouble(_with98.ActiveRow.Cells["Quantite"].Text) * Convert.ToDouble(General.nz((_with98.ActiveRow.Cells["Stud"].Text), "0"));
                    _with98.ActiveRow.Cells["TotalDebours"].Value = Convert.ToString(Convert.ToDouble(General.nz((_with98.ActiveRow.Cells["MOd"].Text), "0")) + Convert.ToDouble(General.nz((_with98.ActiveRow.Cells["Fod"].Text), "0")) + Convert.ToDouble(General.nz((_with98.ActiveRow.Cells["Std"].Text), "0")));
                    _with98.ActiveRow.Cells["MO"].Value = Convert.ToDouble(_with98.ActiveRow.Cells["Mod"].Text) * Convert.ToDouble(General.nz((_with98.ActiveRow.Cells["Kmo"].Text), "0"));
                    _with98.ActiveRow.Cells["FO"].Value = Convert.ToDouble(_with98.ActiveRow.Cells["Fod"].Text) * Convert.ToDouble(General.nz((_with98.ActiveRow.Cells["Kfo"].Text), "0"));
                    _with98.ActiveRow.Cells["ST"].Value = Convert.ToDouble(_with98.ActiveRow.Cells["Std"].Text) * Convert.ToDouble(General.nz((_with98.ActiveRow.Cells["Kst"].Text), "0"));
                    _with98.ActiveRow.Cells["TotalVente"].Value = Convert.ToString(Convert.ToDouble(General.nz((_with98.ActiveRow.Cells["MO"].Text), "0")) + Convert.ToDouble(General.nz((_with98.ActiveRow.Cells["FO"].Text), "0")) + Convert.ToDouble(General.nz((_with98.ActiveRow.Cells["ST"].Text), "0")));
                }
                else
                {
                    for (i = _with98.ActiveRow.Cells["quantite"].Column.Index; i <= _with98.DisplayLayout.Bands[0].Columns.Count - 2; i++)
                    {
                        if (_with98.ActiveRow.Cells[i].Column.DataType == typeof(string))
                        {
                            if (_with98.ActiveRow.Cells[i].Column.Key.ToLower() != "codetva")
                                _with98.ActiveRow.Cells[i].Value = DBNull.Value;
                        }
                        else
                        {
                            _with98.ActiveRow.Cells[i].Value = 0;
                        }
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";prcCalculDetail");
            }

            manager.AllEventsEnabled = true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGridCorps_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSDBGridCorps_BeforeRowsDeleted() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!string.IsNullOrEmpty(txtDateImpression.Text))
            {
                e.Cancel = true;
            }
            else
            {
                e.DisplayPromptMsg = false;
                iMsgb = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                if (iMsgb == DialogResult.No)
                {
                    e.Cancel = true;
                }
                else
                {
                    //suppression de la ligne avec renumerotation des lignes suivantes
                    bSuppression = true;
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGridCorps_BeforeRowInsert(object sender, BeforeRowInsertEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSDBGridCorps_BeforeRowInsert() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                if (blnFournisGrille == false)
                {
                    if (!General.IsNumeric(Text315.Text))
                    {
                        afficheMessage(Convert.ToString(1));
                        Text315.Focus();
                        e.Cancel = true;
                    }
                    else if (!General.IsNumeric(Text314.Text))
                    {
                        afficheMessage(Convert.ToString(1));
                        Text314.Focus();
                        e.Cancel = true;
                    }
                    else if (!General.IsNumeric(Text313.Text))
                    {
                        afficheMessage(Convert.ToString(1));
                        Text313.Focus();
                        e.Cancel = true;
                    }
                    else if (!General.IsNumeric(Text316.Text))
                    {
                        afficheMessage(Convert.ToString(1));
                        Text316.Focus();
                        e.Cancel = true;
                    }
                    else if (!General.IsNumeric(txtTaux.Text))
                    {
                        afficheMessage(Convert.ToString(9));
                        txtTaux.Focus();
                        e.Cancel = true;
                    }
                }
                //If txtDeviseur = "" Then
                //    giMsg = MsgBox("Un deviseur est obligatoire", vbOKOnly + vbExclamation, "Deviseur")
                //    SSTab1.Tab = 0
                //    txtDeviseur.SetFocus
                //    Cancel = True
                //End If
                if (SSetat.Text == "00")
                {
                    SSetat.Text = "03";
                }

                SSDBGridCorps.Rows.Refresh(RefreshRow.FireInitializeRow);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSDBGridCorps_BeforeInsert");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGridCorps_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSDBGridCorps_BeforeRowUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                e.Cancel = false;
                blnErrGridUpdate = false;
                //    Debug.Print "Before Update : " & SSDBGridCorps.Columns("Quantite").value
                //Pas de vérification si chargement d'un modèle
                if (General.SelectModele == false)
                {
                    if (e.Row != null && !string.IsNullOrEmpty(e.Row.Cells["Mohud"].Text) && e.Row.Cells["Mohud"].Text != "0")
                    {
                        if (e.Row.Cells["CodeMO"].Value.ToString() != "E" && e.Row.Cells["CodeMO"].Value.ToString() != "S")
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le code de main d'oeuvre (CodeMO) dans la ligne n°" + e.Row.Cells["NumeroLigne"].Text, "Renseignements incomplets", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Cancel = true;
                            blnErrGridUpdate = true;
                            return;
                        }
                    }
                }

                //---boolean indiquant qu'aucune erreur n'est survenue dans le corps.
                BOOLErreurCorps = false;
                if (e.Row != null &&
                    string.IsNullOrEmpty(e.Row.Cells["codetva"].Value.ToString())
                    && e.Row.Cells["quantite"].Value.ToString() != "0" &&
                    !string.IsNullOrEmpty(e.Row.Cells["quantite"].Value.ToString()))
                {
                    afficheMessage(Convert.ToString(6));
                    e.Cancel = true;
                    blnErrGridUpdate = true;
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSDBGridCorps_BeforeUpdate");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void InitColumnProps()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"InitColumnProps() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {

                //var SSDBGridCorpsSource = new DataTable();
                //SSDBGridCorpsSource.Columns.Add("numerodevis");
                //SSDBGridCorpsSource.Columns.Add("NumeroLigne");
                //SSDBGridCorpsSource.Columns.Add("CodeFamille");
                //SSDBGridCorpsSource.Columns.Add("CodeSousFamille");
                //SSDBGridCorpsSource.Columns.Add("CodeArticle");
                //SSDBGridCorpsSource.Columns.Add("CodeSousArticle");
                //SSDBGridCorpsSource.Columns.Add("NumOrdre");
                //SSDBGridCorpsSource.Columns.Add("TexteLigne");
                //SSDBGridCorpsSource.Columns.Add("Quantite");
                //SSDBGridCorpsSource.Columns.Add("Unite");
                //SSDBGridCorpsSource.Columns.Add("MOhud");
                //SSDBGridCorpsSource.Columns.Add("CodeMO");
                //SSDBGridCorpsSource.Columns.Add("PxHeured");
                //SSDBGridCorpsSource.Columns.Add("FOud");
                //SSDBGridCorpsSource.Columns.Add("STud");
                //SSDBGridCorpsSource.Columns.Add("MOhd");
                //SSDBGridCorpsSource.Columns.Add("MOd");
                //SSDBGridCorpsSource.Columns.Add("FOd");
                //SSDBGridCorpsSource.Columns.Add("STd");
                //SSDBGridCorpsSource.Columns.Add("TotalDebours");
                //SSDBGridCorpsSource.Columns.Add("kMO");
                //SSDBGridCorpsSource.Columns.Add("MO");
                //SSDBGridCorpsSource.Columns.Add("kFO");
                //SSDBGridCorpsSource.Columns.Add("FO");
                //SSDBGridCorpsSource.Columns.Add("kST");
                //SSDBGridCorpsSource.Columns.Add("ST");
                //SSDBGridCorpsSource.Columns.Add("TotalVente");
                //SSDBGridCorpsSource.Columns.Add("Coef1");
                //SSDBGridCorpsSource.Columns.Add("Coef2");
                //SSDBGridCorpsSource.Columns.Add("Coef3");
                //SSDBGridCorpsSource.Columns.Add("CodeTVA");
                //SSDBGridCorpsSource.Columns.Add("TotalVenteApresCoef");
                //SSDBGridCorpsSource.Columns.Add("Facturer");
                //SSDBGridCorpsSource.Columns.Add("DateFacture");
                //SSDBGridCorpsSource.Columns.Add("NFamille");
                //SSDBGridCorpsSource.Columns.Add("NSFamille");
                //SSDBGridCorpsSource.Columns.Add("NArticle");
                //SSDBGridCorpsSource.Columns.Add("NSousArticle");
                //SSDBGridCorpsSource.Columns.Add("Fournisseur");
                //SSDBGridCorpsSource.Columns.Add("SautPage");

                //SSDBGridCorps.DataSource = SSDBGridCorpsSource;

                var _with55 = SSDBGridCorps;
                _with55.DisplayLayout.Bands[0].Columns["NumeroDevis"].Hidden = true;
                _with55.DisplayLayout.Bands[0].Columns["NumeroLigne"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["PXHeured"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["KMO"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["KST"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["coef1"].Hidden = true;
                _with55.DisplayLayout.Bands[0].Columns["coef2"].Hidden = true;
                _with55.DisplayLayout.Bands[0].Columns["coef3"].Hidden = true;
                _with55.DisplayLayout.Bands[0].Columns["TotalVenteApresCoef"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["NumOrdre"].Hidden = true;

                // Affichage des colonnes souhaitées par les deviseurs DELOSTAL
                _with55.DisplayLayout.Bands[0].Columns["TexteLigne"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["Fournisseur"].Hidden = true;
                _with55.DisplayLayout.Bands[0].Columns["KFO"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["Quantite"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["Unite"].Hidden = false;

                // Masquage des colonnes suivant les recommandations DELOSTAL
                _with55.DisplayLayout.Bands[0].Columns["CodeMO"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["Mohd"].Hidden = true;
                _with55.DisplayLayout.Bands[0].Columns["Mod"].Hidden = true;
                _with55.DisplayLayout.Bands[0].Columns["Fod"].Hidden = true;
                _with55.DisplayLayout.Bands[0].Columns["Std"].Hidden = true;
                _with55.DisplayLayout.Bands[0].Columns["TotalDebours"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["Mo"].Hidden = true;
                _with55.DisplayLayout.Bands[0].Columns["Fo"].Hidden = true;
                _with55.DisplayLayout.Bands[0].Columns["St"].Hidden = true;
                _with55.DisplayLayout.Bands[0].Columns["TotalVente"].Hidden = true;
                _with55.DisplayLayout.Bands[0].Columns["CodeFamille"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["CodeFamille"].MaxLength = 50;
                _with55.DisplayLayout.Bands[0].Columns["CodeSousArticle"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["CodeSousArticle"].MaxLength = 50;
                _with55.DisplayLayout.Bands[0].Columns["CodeSousFamille"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["CodeSousFamille"].MaxLength = 50;
                _with55.DisplayLayout.Bands[0].Columns["CodeArticle"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["CodeArticle"].MaxLength = 50;
                _with55.DisplayLayout.Bands[0].Columns["Mohud"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["Foud"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["Stud"].Hidden = false;
                _with55.DisplayLayout.Bands[0].Columns["Mohd"].CellActivation = Activation.ActivateOnly;
                _with55.DisplayLayout.Bands[0].Columns["Mohd"].CellAppearance.BackColor = ColorTranslator.FromOle(0xe0e0e0);
                _with55.DisplayLayout.Bands[0].Columns["Mod"].CellActivation = Activation.ActivateOnly;
                _with55.DisplayLayout.Bands[0].Columns["Mod"].CellAppearance.BackColor = ColorTranslator.FromOle(0xe0e0e0);
                _with55.DisplayLayout.Bands[0].Columns["Fod"].CellActivation = Activation.ActivateOnly;
                _with55.DisplayLayout.Bands[0].Columns["Fod"].CellAppearance.BackColor = ColorTranslator.FromOle(0xe0e0e0);
                _with55.DisplayLayout.Bands[0].Columns["Std"].CellActivation = Activation.ActivateOnly;
                _with55.DisplayLayout.Bands[0].Columns["Std"].CellAppearance.BackColor = ColorTranslator.FromOle(0xe0e0e0);
                _with55.DisplayLayout.Bands[0].Columns["TotalDebours"].CellActivation = Activation.ActivateOnly;
                _with55.DisplayLayout.Bands[0].Columns["TotalDebours"].CellAppearance.BackColor = ColorTranslator.FromOle(0xe0e0e0);
                _with55.DisplayLayout.Bands[0].Columns["Mo"].CellActivation = Activation.ActivateOnly;
                _with55.DisplayLayout.Bands[0].Columns["Mo"].CellAppearance.BackColor = ColorTranslator.FromOle(0xe0e0e0);
                _with55.DisplayLayout.Bands[0].Columns["Fo"].CellActivation = Activation.ActivateOnly;
                _with55.DisplayLayout.Bands[0].Columns["Fo"].CellAppearance.BackColor = ColorTranslator.FromOle(0xe0e0e0);
                _with55.DisplayLayout.Bands[0].Columns["St"].CellActivation = Activation.ActivateOnly;
                _with55.DisplayLayout.Bands[0].Columns["St"].CellAppearance.BackColor = ColorTranslator.FromOle(0xe0e0e0);
                _with55.DisplayLayout.Bands[0].Columns["TotalVente"].CellActivation = Activation.ActivateOnly;
                _with55.DisplayLayout.Bands[0].Columns["TotalVente"].CellAppearance.BackColor = ColorTranslator.FromOle(0xe0e0e0);
                _with55.DisplayLayout.Bands[0].Columns["TotalVenteApresCoef"].CellAppearance.BackColor = ColorTranslator.FromOle(0xe0e0e0);
                _with55.DisplayLayout.Bands[0].Columns["TotalVenteApresCoef"].CellActivation = Activation.ActivateOnly;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSDBGridCorps_InitColumnProps");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGridCorps_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSDBGridCorps_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir

            // e.Layout.Override.CellClickAction = CellClickAction.Edit;
            SSDBGridCorps.EventManager.AllEventsEnabled = false;
            SSDBGridCorps.DisplayLayout.Bands[0].Columns["CodeMO"].ValueList = SSDBDropDown1;
            SSDBGridCorps.DisplayLayout.Bands[0].Columns["Unite"].ValueList = cmbUnite;

            e.Layout.Bands[0].Columns["TexteLigne"].Width = 400;
            e.Layout.Bands[0].Columns["NumOrdre"].Hidden = true;
            e.Layout.Bands[0].Columns["NumeroDevis"].Header.Caption = "Numero Devis";
            e.Layout.Bands[0].Columns["NumeroLigne"].Header.Caption = "N°Ligne";
            e.Layout.Bands[0].Columns["CodeSousFamille"].Header.Caption = "Poste";
            e.Layout.Bands[0].Columns["CodeSousFamille"].MaxLength = 50;
            e.Layout.Bands[0].Columns["CodeSousFamille"].Header.Appearance.ForeColor = Color.Red;
            e.Layout.Bands[0].Columns["CodeArticle"].Header.Caption = "Code Article";
            e.Layout.Bands[0].Columns["CodeArticle"].Header.Appearance.ForeColor = Color.Red;
            e.Layout.Bands[0].Columns["CodeArticle"].MaxLength = 50;
            e.Layout.Bands[0].Columns["TexteLigne"].Header.Caption = "Texte Ligne";
            e.Layout.Bands[0].Columns["TexteLigne"].CellMultiLine = Infragistics.Win.DefaultableBoolean.True;

            e.Layout.Bands[0].Columns["Quantite"].Header.Caption = "Qté";
            e.Layout.Bands[0].Columns["CodeMO"].Header.Caption = "S/Equipe";
            e.Layout.Bands[0].Columns["PxHeured"].Header.Caption = "Px Heure";
            e.Layout.Bands[0].Columns["TotalDebours"].Header.Caption = "Total Debours";
            e.Layout.Bands[0].Columns["TotalDebours"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["TotalVente"].Header.Caption = "Total Vente";
            e.Layout.Bands[0].Columns["TotalVente"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["CodeTVA"].Header.Caption = "Code TVA";
            e.Layout.Bands[0].Columns["TotalVenteApresCoef"].Header.Caption = "Total Vente Apres Coef";
            e.Layout.Bands[0].Columns["TotalVenteApresCoef"].CellAppearance.BackColor = Color.Gray;
            e.Layout.Bands[0].Columns["SautPage"].Header.Caption = "Saut";
            e.Layout.Bands[0].Columns["SautPage"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            e.Layout.Bands[0].Columns["Facturer"].Hidden = true;
            e.Layout.Bands[0].Columns["DateFacture"].Hidden = true;
            e.Layout.Bands[0].Columns["NFamille"].Hidden = true;
            e.Layout.Bands[0].Columns["NSFamille"].Hidden = true;
            e.Layout.Bands[0].Columns["NArticle"].Hidden = true;
            e.Layout.Bands[0].Columns["NSousArticle"].Hidden = true;
            SSDBGridCorps.EventManager.AllEventsEnabled = true;
            e.Layout.Bands[0].Columns["SautPage"].Editor.DataFilter = new BooleanColumnDataFilter();

            //===> Mondir le 03.09.2020, SautPage is useless, asked Rachid : https://groupe-dt.mantishub.io/view.php?id=1851
            //===> Mondir le 16.09.2021, Remis l'option Saut de Page https://groupe-dt.mantishub.io/view.php?id=2259
            //e.Layout.Bands[0].Columns["SautPage"].Hidden = true;
            e.Layout.Bands[0].Columns["SautPage"].DefaultCellValue = false;
            //Fin Modif Mondir

            //e.Layout.Bands[0].Columns["NumeroDevis"].Header.VisiblePosition = 1;
            //e.Layout.Bands[0].Columns["NumeroLigne"].Header.VisiblePosition = 2;
            //e.Layout.Bands[0].Columns["CodeFamille"].Header.VisiblePosition = 3;
            //e.Layout.Bands[0].Columns["CodeSousFamille"].Header.VisiblePosition = 4;
            //e.Layout.Bands[0].Columns["CodeArticle"].Header.VisiblePosition = 5;
            //e.Layout.Bands[0].Columns["CodeSousArticle"].Header.VisiblePosition = 6;
            //e.Layout.Bands[0].Columns["TexteLigne"].Header.VisiblePosition = 7;
            //e.Layout.Bands[0].Columns["Quantite"].Header.VisiblePosition = 8;
            //e.Layout.Bands[0].Columns["Unite"].Header.VisiblePosition = 9;
            //e.Layout.Bands[0].Columns["Mohud"].Header.VisiblePosition = 10;
            e.Layout.Bands[0].Columns["Mohud"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };

            //e.Layout.Bands[0].Columns["CodeMO"].Header.VisiblePosition =11;
            //e.Layout.Bands[0].Columns["PxHeured"].Header.VisiblePosition = 12;
            e.Layout.Bands[0].Columns["PxHeured"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };

            //e.Layout.Bands[0].Columns["Foud"].Header.VisiblePosition = 13;
            e.Layout.Bands[0].Columns["Foud"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["Stud"].Header.VisiblePosition = 14;
            e.Layout.Bands[0].Columns["Stud"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["Mohd"].Header.VisiblePosition = 15;
            e.Layout.Bands[0].Columns["Mohd"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["Mod"].Header.VisiblePosition = 16;
            e.Layout.Bands[0].Columns["Mod"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["Fod"].Header.VisiblePosition = 17;
            e.Layout.Bands[0].Columns["Fod"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["Std"].Header.VisiblePosition = 18;

            e.Layout.Bands[0].Columns["Std"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["TotalDebours"].Header.VisiblePosition = 19;
            e.Layout.Bands[0].Columns["TotalDebours"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["KMO"].Header.VisiblePosition = 20;
            e.Layout.Bands[0].Columns["KMO"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["MO"].Header.VisiblePosition = 21;
            e.Layout.Bands[0].Columns["MO"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["KFO"].Header.VisiblePosition = 22;
            e.Layout.Bands[0].Columns["KFO"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["FO"].Header.VisiblePosition = 23;
            e.Layout.Bands[0].Columns["FO"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["KST"].Header.VisiblePosition = 24;
            e.Layout.Bands[0].Columns["KST"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["ST"].Header.VisiblePosition = 25;
            e.Layout.Bands[0].Columns["ST"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["TotalVente"].Header.VisiblePosition = 26;
            e.Layout.Bands[0].Columns["TotalVente"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            e.Layout.Bands[0].Columns["CodeTVA"].Header.VisiblePosition = 27;
            e.Layout.Bands[0].Columns["CodeTVA"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["Coef1"].Header.VisiblePosition = 28;
            e.Layout.Bands[0].Columns["Coef1"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["Coef2"].Header.VisiblePosition = 29;
            e.Layout.Bands[0].Columns["Coef2"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["Coef3"].Header.VisiblePosition = 30;
            e.Layout.Bands[0].Columns["Coef3"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["TotalVenteApresCoef"].Header.VisiblePosition = 31;
            e.Layout.Bands[0].Columns["TotalVenteApresCoef"].Editor = new EditorWithText()
            {
                DataFilter = new StringColumnDataFilter()
            };
            //e.Layout.Bands[0].Columns["Fournisseur"].Header.VisiblePosition = 32;
            //e.Layout.Bands[0].Columns["SautPage"].Header.VisiblePosition = 33;


            //e.Layout.Bands[0].Columns["Quantite"].Header.VisiblePosition = e.Layout.Bands[0].Columns["TexteLigne"].Header.VisiblePosition + 1;
            //e.Layout.Bands[0].Columns["Unite"].Header.VisiblePosition = e.Layout.Bands[0].Columns["Quantite"].Header.VisiblePosition + 1;
            //e.Layout.Bands[0].Columns["Mohud"].Header.VisiblePosition = e.Layout.Bands[0].Columns["Unite"].Header.VisiblePosition + 1;
            //e.Layout.Bands[0].Columns["CodeMO"].Header.VisiblePosition = e.Layout.Bands[0].Columns["Mohud"].Header.VisiblePosition + 1;
            //e.Layout.Bands[0].Columns["PxHeured"].Header.VisiblePosition = e.Layout.Bands[0].Columns["CodeMO"].Header.VisiblePosition + 1;
            //e.Layout.Bands[0].Columns["CodeTVA"].Header.VisiblePosition = e.Layout.Bands[0].Columns["kST"].Header.VisiblePosition + 1;
            //e.Layout.Bands[0].Columns["SautPage"].Header.VisiblePosition = e.Layout.Bands[0].Columns["TotalVenteApresCoef"].Header.VisiblePosition + 1;

            e.Layout.Override.HeaderClickAction = HeaderClickAction.Select;

            //foreach (UltraGridColumn col in e.Layout.Bands[0].Columns)
            //    col.CellMultiLine = Infragistics.Win.DefaultableBoolean.True;


        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGridCorps_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSDBGridCorps_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {

                if (SSDBGridCorps.ActiveCell != null && SSDBGridCorps.ActiveCell.Column.Index >= 0)
                {
                    //if (SSDBGridCorps.ActiveRow.Cells[SSDBGridCorps.ActiveCell.Column.Index].Activation== Activation.ActivateOnly)
                    //{
                    //    return;
                    //}
                    if (SSDBGridCorps.DisplayLayout.Bands[0].Override.AllowUpdate == DefaultableBoolean.False)
                    {
                        return;
                    }
                }
                if (string.IsNullOrEmpty(Text315.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le coefficient de main d'oeuvre", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Text315.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(Text314.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le coefficient fournisseur", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Text314.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(Text313.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le coefficient sous traitant", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Text313.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(Text316.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le prix horaire de la main d'oeuvre", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    Text316.Focus();
                    return;
                }

                if (string.IsNullOrEmpty(txtTaux.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez renseigner le taux de TVA", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    txtTaux.Focus();
                    return;
                }

                if ((short)e.KeyChar == 13)
                {
                    boolEntree = true;
                    if (SSDBGridCorps.ActiveCell != null)
                    {
                        if (SSDBGridCorps.ActiveCell.Column.Index == 1)
                        {
                            if ((short)e.KeyChar == 13)
                            {
                                var DataSource = SSDBGridCorps.DataSource as DataTable;
                                if (SSDBGridCorps.ActiveRow.IsAddRow)
                                {
                                    DataSource.Rows.Add(DataSource.NewRow());
                                }
                                else
                                {
                                    DataSource.Rows.InsertAt(DataSource.NewRow(), SSDBGridCorps.ActiveRow.Index);
                                }
                                //SSDBGridCorps.DataBind();
                            }
                        }
                        else if (SSDBGridCorps.ActiveCell.Column.Index == 2)
                        {

                            string requete = "SELECT FamilleArticle.CodeFamille as \"Code Famille\", FamilleArticle.IntituleFamille as \"intitule Famille\" FROM FamilleArticle";
                            string where_order = "";
                            SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des articles" };
                            fg.SetValues(new Dictionary<string, string> { { "CodeFamille", SSDBGridCorps.ActiveRow.Cells["CodeFamille"].Text } });
                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                strOldValue = SSDBGridCorps.ActiveRow.Cells["CodeFamille"].Value.ToString();
                                SSDBGridCorps.ActiveRow.Cells["CodeFamille"].Value = fg.ugResultat.ActiveRow.Cells["code Famille"].Value;
                                Integrer(SSDBGridCorps, SSDBGridCorps.ActiveCell.Column.Index);
                                fg.Dispose();
                                fg.Close();
                            };

                            fg.ugResultat.KeyDown += (se, ev) =>
                            {

                                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                {
                                    strOldValue = SSDBGridCorps.ActiveRow.Cells["CodeFamille"].Value.ToString();
                                    SSDBGridCorps.ActiveRow.Cells["CodeFamille"].Value = fg.ugResultat.ActiveRow.Cells["code Famille"].Value;
                                    Integrer(SSDBGridCorps, SSDBGridCorps.ActiveCell.Column.Index);
                                    fg.Dispose();
                                    fg.Close();
                                }
                            };
                            fg.StartPosition = FormStartPosition.CenterParent;
                            fg.ShowDialog();
                            //Poste
                        }
                        ///)
                        else if (SSDBGridCorps.ActiveCell.Column.Index == 3)
                        {
                            //             With RechercheMultiCritere
                            //                .stxt = "SELECT SousFamilleArticle.CodeSousFamille as [Code sous Famille], SousFamilleArticle.IntituleSousFamille as [Intitule sous Famille] FROM SousFamilleArticle"
                            //                .Caption = "Recherche des articles"
                            //                .Move Screen.Width - .Width, (Screen.Height - 15 * Screen.TwipsPerPixelY) - .Height
                            //                .Champs0 = SSDBGridCorps.Columns("CodeSousFamille").value
                            //                .Show vbModal
                            //                If nz(.LigneChoisie, "") <> "" Then
                            //                      strOldValue = SSDBGridCorps.Columns("CodeSousFamille").value
                            //                      SSDBGridCorps.Columns("CodeSousFamille").value = .dbgrid1.Columns("Code Sous Famille").value
                            //                      Integrer SSDBGridCorps, SSDBGridCorps.col
                            //                End If
                            //                Unload RechercheMultiCritere
                            //            End With
                            frmGecet frmGecet = new frmGecet();
                            var _with57 = frmGecet;
                            General.Execute("DELETE FROM PanierGecet WHERE Utilisateur='" + General.gsUtilisateur + "'");
                            _with57.txtCle.Text = txtNoDevis.Text;
                            _with57.txtOrigine.Text = Variable.cUserDocDevis;
                            _with57.txtType.Text = "2";
                            _with57.txtKFO.Text = Text314.Text;
                            _with57.txtKMO.Text = Text315.Text;
                            _with57.txtKST.Text = Text313.Text;
                            _with57.txtPxHeure.Text = Text316.Text;
                            _with57.txtTVA.Text = txtTaux.Text;
                            _with57.ShowDialog();
                            fc_StockPanierPoste();
                            frmGecet.Close();
                            frmGecet.Close();

                            //Article
                        }
                        else if (SSDBGridCorps.ActiveCell.Column.Index == 4)
                        {
                            //            With RechercheMultiCritere
                            //                .stxt = "SELECT FacArticle.CodeArticle as [Code article], FacArticle.Designation1 as [Designation] From FacArticle"
                            //                .Caption = "Recherche des articles"
                            //                 .txtWhere = " WHERE CodeCategorieArticle = 'D'"
                            //                .Move Screen.Width - .Width, (Screen.Height - 15 * Screen.TwipsPerPixelY) - .Height
                            //                .Champs0 = SSDBGridCorps.Columns("CodeArticle").value
                            //                .Show vbModal
                            //                If nz(.LigneChoisie, "") <> "" Then
                            //                    strOldValue = SSDBGridCorps.Columns("CodeArticle").value
                            //                    SSDBGridCorps.Columns("CodeArticle").value = .dbgrid1.Columns("Code article").value
                            //                    Integrer SSDBGridCorps, SSDBGridCorps.col
                            //                End If
                            //                Unload RechercheMultiCritere
                            //            End With

                            frmGecet frmGecet = new frmGecet();
                            var _with58 = frmGecet;
                            General.Execute("DELETE FROM PanierGecet WHERE Utilisateur='" + General.gsUtilisateur + "'");
                            _with58.txtCle.Text = txtNoDevis.Text;
                            _with58.txtOrigine.Text = Variable.cUserDocDevis;
                            _with58.txtType.Text = "1";
                            _with58.txtKFO.Text = Text314.Text;
                            _with58.txtKMO.Text = Text315.Text;
                            _with58.txtKST.Text = Text313.Text;
                            _with58.txtPxHeure.Text = Text31.Text;
                            _with58.txtTVA.Text = txtTaux.Text;
                            _with58.ShowDialog();
                            fc_StockPanierArticle();
                            frmGecet.Close();

                        }
                        else if (SSDBGridCorps.ActiveCell.Column.Index == SSDBGridCorps.ActiveRow.Cells["codesousarticle"].Column.Index)
                        {
                            if (!string.IsNullOrEmpty(SSDBGridCorps.ActiveRow.Cells["codesousarticle"].Value.ToString().Replace("\r", "").Replace("\n", "")))
                            {

                                if (General.sGecetV3 == "1")
                                {
                                    FrmFournture FrmFournture = new FrmFournture();
                                    FrmFournture.txtrequete.Text = "SELECT     Article.CHRONO_INITIAL AS Chrono,"
                                     + " Article.DESIGNATION_ARTICLE AS Libelle,"
                                     + " ArticleTarif.NOM_FOURNISSEUR AS fournisseur,"
                                     + " ArticleTarif.PRX_DEVIS_NET_UD AS [prix net achat], "
                                     + " ArticleTarif.IDGEC_LIGNE_PRIX AS NoAuto "
                                     + " FROM         Article INNER JOIN "
                                     + " ArticleTarif ON Article.CHRONO_INITIAL = ArticleTarif.CHRONO_INITIAL"
                                     + " WHERE  Article.CHRONO_INITIAL = '" + SSDBGridCorps.ActiveRow.Cells["codesousarticle"].Value + "'";

                                    ModParametre.fc_OpenConnGecet();

                                    FrmFournture.txtConnectionString.Text = ModParametre.adoGecet.ConnectionString;
                                    FrmFournture.ShowDialog();

                                    if (!string.IsNullOrEmpty(FrmFournture.txtChoix.Text))
                                    {
                                        SSDBGridCorps.ActiveRow.Cells["fournisseur"].Value = FrmFournture.txtChoix.Text;
                                        SSDBGridCorps.UpdateData();
                                    }
                                    FrmFournture.Close();
                                }
                                else if (General.sGecetV2 == "1")
                                {
                                    FrmFournture FrmFournture = new FrmFournture();
                                    FrmFournture.txtrequete.Text = "SELECT     ARTP_ArticlePrix.ART_Chrono AS Chrono,"
                                                    + " ART_Article.ART_Libelle AS Libelle, "
                                                    + " ARTP_ArticlePrix.ARTP_Nofourn AS fournisseur ,"
                                                    + " ARTP_ArticlePrix.ARTP_PrixNetDevis AS [prix net achat], "
                                                    + " ARTP_ArticlePrix.ARTP_NoAuto AS NoAuto"
                                                    + " FROM         ARTP_ArticlePrix INNER JOIN"
                                                    + " ART_Article "
                                                    + " ON ARTP_ArticlePrix.ART_Chrono = ART_Article.ART_Chrono"
                                                    + " WHERE  ARTP_ArticlePrix.ART_Chrono = '" + SSDBGridCorps.ActiveRow.Cells["codesousarticle"].Value + "'";



                                    ModParametre.fc_OpenConnGecet();

                                    FrmFournture.txtConnectionString.Text = ModParametre.adoGecet.ConnectionString;
                                    FrmFournture.ShowDialog();

                                    if (!string.IsNullOrEmpty(FrmFournture.txtChoix.Text))
                                    {
                                        SSDBGridCorps.ActiveRow.Cells["fournisseur"].Value = FrmFournture.txtChoix.Text;
                                        SSDBGridCorps.UpdateData();
                                    }
                                    FrmFournture.Close();
                                }
                                else
                                {

                                    FrmFournture FrmFournture = new FrmFournture();

                                    FrmFournture.txtrequete.Text = "SELECT Dtiprix.Chrono, Dtiarti.Libelle,"
                                                    + " Dtiprix.fournisseur,"
                                                    + " Dtiprix.[prix net achat],Dtiprix.NoAuto "
                                                    + " FROM Dtiprix INNER JOIN Dtiarti ON"
                                                    + " Dtiprix.Chrono = Dtiarti.Chrono"
                                                    + " where dtiprix.chrono=" + SSDBGridCorps.ActiveRow.Cells["codesousarticle"].Value;

                                    FrmFournture.txtConnectionString.Text = ModParametre.adoGecet.ConnectionString;
                                    FrmFournture.ShowDialog();
                                    if (!string.IsNullOrEmpty(FrmFournture.txtChoix.Text))
                                    {
                                        SSDBGridCorps.ActiveRow.Cells["fournisseur"].Value = FrmFournture.txtChoix.Text;
                                        SSDBGridCorps.UpdateData();
                                    }
                                    FrmFournture.Close();
                                }
                            }
                            else if (string.IsNullOrEmpty(SSDBGridCorps.ActiveRow.Cells["codesousarticle"].Value.ToString().Replace("\r", "").Replace("\n", "")))
                            {
                                //                  With RechercheMultiCritere
                                //                    .stxt = "SELECT Dtiprix.Marque AS [Marque], Dtiprix.Fabref as [Ref. Fabriquant], Dtiprix.Fourref as [Ref. Fournisseur], dtiarti.libelle AS [Designation], " _
                                //'                        & " dtiarti.chrono as [Chrono] ," _
                                //'                        & " Dtiprix.Fournisseur as [Fournisseur], " _
                                //'                        & " Dtiprix.[Px tarif], Dtiprix.[Prix net achat]," _
                                //'                        & " Dtiprix.FouUnite AS [Unité], Dtiprix.Dapplication, Dtiprix.KFunite," _
                                //'                        & " Dtiprix.Statutci, Dtiprix.CoefVente" _
                                //'                        & " FROM dtiarti INNER JOIN Dtiprix ON dtiarti.chrono = Dtiprix.Chrono"
                                //                    .txtConnectionString.Text = adoGecet.ConnectionString
                                //                    '.TotalChamps = 5
                                //                    .Caption = "Recherche des articles Gecet"
                                //                    .Move Screen.Width - .Width, (Screen.Height - 15 * Screen.TwipsPerPixelY) - .Height
                                //                    .Show vbModal
                                //                    If nz(.LigneChoisie, "") <> "" Then
                                //                        strOldValue = SSDBGridCorps.Columns("CodeArticle").value
                                //                        'SSDBGridCorps.Columns("CodeArticle").value = .dbgrid1.Columns("Ref. Fournisseur").value
                                //                        Integrer SSDBGridCorps, SSDBGridCorps.col
                                //                    End If
                                //                    Unload RechercheMultiCritere

                                //                End With
                                //=============> TODO : Mondir - must test this Conditoin
                                if (General.sGecetV3 == "1")
                                {
                                    frmGecetV3 frmGecet3 = new frmGecetV3();
                                    var _with59 = frmGecet3;
                                    General.Execute("DELETE FROM PanierGecet WHERE Utilisateur='" + General.gsUtilisateur + "'");
                                    _with59.txtCle.Text = txtNoDevis.Text;
                                    _with59.txtOrigine.Text = Variable.cUserDocDevis;
                                    _with59.txtType.Text = "0";
                                    _with59.txtKFO.Text = Text314.Text;
                                    _with59.txtKMO.Text = Text315.Text;
                                    _with59.txtKST.Text = Text313.Text;
                                    _with59.txtPxHeure.Text = Text316.Text;
                                    _with59.txtTVA.Text = txtTaux.Text;
                                    _with59.ShowDialog();
                                    fc_StockPanierGecet();
                                    frmGecet3.Close();
                                }
                                else if (General.sGecetV2 == "1")
                                {
                                    frmGecet2 frmGecet2 = new frmGecet2();
                                    var _with59 = frmGecet2;
                                    General.Execute("DELETE FROM PanierGecet WHERE Utilisateur='" + General.gsUtilisateur + "'");
                                    _with59.txtCle.Text = txtNoDevis.Text;
                                    _with59.txtOrigine.Text = Variable.cUserDocDevis;
                                    _with59.txtType.Text = "0";
                                    _with59.txtKFO.Text = Text314.Text;
                                    _with59.txtKMO.Text = Text315.Text;
                                    _with59.txtKST.Text = Text313.Text;
                                    _with59.txtPxHeure.Text = Text316.Text;
                                    _with59.txtTVA.Text = txtTaux.Text;
                                    _with59.ShowDialog();
                                    fc_StockPanierGecet();
                                    frmGecet2.Close();
                                }
                                else
                                {

                                    frmGecet frmGecet = new frmGecet();
                                    var _with60 = frmGecet;
                                    General.Execute("DELETE FROM PanierGecet WHERE Utilisateur='" + General.gsUtilisateur + "'");
                                    _with60.txtCle.Text = txtNoDevis.Text;
                                    _with60.txtOrigine.Text = Variable.cUserDocDevis;
                                    _with60.txtType.Text = "0";
                                    _with60.txtKFO.Text = Text314.Text;
                                    _with60.txtKMO.Text = Text315.Text;
                                    _with60.txtKST.Text = Text313.Text;
                                    _with60.txtPxHeure.Text = Text316.Text;
                                    _with60.txtTVA.Text = txtTaux.Text;
                                    _with60.ShowDialog();
                                    fc_StockPanierGecet();
                                    frmGecet.Close();
                                }
                            }
                        }

                        SSDBGridCorps.Rows.Refresh(RefreshRow.FireInitializeRow);
                    }
                    boolEntree = false;
                }

                //TODO : Mondir - Look On VB6
                //if (!string.IsNullOrEmpty(SSDBGridCorps.ActiveRow.Cells["CodeSousFamille"].Text) && SSDBGridCorps.ActiveCell.Column.Index >= SSDBGridCorps.ActiveRow.Cells["Quantite"].Column.Index)
                //{

                //}

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSDBGridCorps_KeyPress");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_StockPanierPoste()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_StockPanierPoste() - Enter In The Function");
            //===> Fin Modif Mondir

            var manager = SSDBGridCorps.EventManager;
            DataTable rsPanier = default(DataTable);
            ModAdo modAdorsPanier = null;
            string sAddItem = null;

            modAdorsPanier = new ModAdo();
            rsPanier = modAdorsPanier.fc_OpenRecordSet("SELECT * FROM PanierGecet WHERE Utilisateur='" + General.gsUtilisateur + "' ORDER BY CleAuto DESC");

            manager.AllEventsEnabled = false;

            if (rsPanier.Rows.Count > 0)
            {
                var RowIndex = SSDBGridCorps.ActiveRow.Index;
                bool turn = false;
                foreach (DataRow rsPanierRow in rsPanier.Rows)
                {
                    if (turn)
                        SSDBGridCorps.ActiveRow = SSDBGridCorps.Rows[RowIndex - 1];
                    else
                        SSDBGridCorps.ActiveRow = SSDBGridCorps.Rows[RowIndex];

                    strOldValue = SSDBGridCorps.ActiveRow.Cells["CodeSousFamille"].Value.ToString();
                    SSDBGridCorps.ActiveRow.Cells["CodeSousFamille"].Value = rsPanierRow["Code"] + "";
                    if (SSDBGridCorps.ActiveRow.IsAddRow)
                    {
                        SSDBGridCorps.UpdateData();
                        //RowIndex = SSDBGridCorps.Rows.Count - 1;
                        SSDBGridCorps.Rows[RowIndex].Delete(false);
                        InsertionSousFamille(rsPanierRow["Code"] + "", ref RowIndex, true, Convert.ToInt32(General.nz(rsPanierRow["Quantite"], "1")));
                        turn = true;
                        //RowIndex++;
                    }
                    else
                    {
                        SSDBGridCorps.ActiveRow.Cells["CodeSouSfamille"].Value = strOldValue;
                        SSDBGridCorps.UpdateData();
                        InsertionSousFamille(rsPanierRow["Code"] + "", ref RowIndex, false, Convert.ToInt32(General.nz(rsPanierRow["Quantite"], "1")));
                        turn = true;
                        //RowIndex++;
                    }
                }
            }

            manager.AllEventsEnabled = true;

            modAdorsPanier?.Dispose();
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_StockPanierArticle()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_StockPanierArticle() - Enter In The Function");
            //===> Fin Modif Mondir

            DataTable rsPanier = default(DataTable);
            ModAdo modAdorsPanier = null;
            DataTable rsArticle = default(DataTable);
            ModAdo modAdorsArticle = null;
            string sAddItem = null;
            var manager = SSDBGridCorps.EventManager;

            modAdorsPanier = new ModAdo();
            modAdorsArticle = new ModAdo();

            rsPanier = modAdorsPanier.fc_OpenRecordSet("SELECT * FROM PanierGecet WHERE Utilisateur='" + General.gsUtilisateur + "' ORDER BY CleAuto DESC");

            manager.AllEventsEnabled = false;

            if (rsPanier.Rows.Count > 0)
            {

                var RowIndex = SSDBGridCorps.ActiveRow.Index;
                bool turn = false;

                foreach (DataRow rsPanierRow in rsPanier.Rows)
                {
                    rsArticle = modAdorsArticle.fc_OpenRecordSet("SELECT * FROM FacArticle WHERE CodeArticle='" + rsPanierRow["Code"] + "'");

                    if (turn)
                        SSDBGridCorps.ActiveRow = SSDBGridCorps.Rows[RowIndex - 1];
                    else
                        SSDBGridCorps.ActiveRow = SSDBGridCorps.Rows[RowIndex];

                    strOldValue = SSDBGridCorps.ActiveRow.Cells["CodeArticle"].Value.ToString();
                    SSDBGridCorps.ActiveRow.Cells["CodeArticle"].Value = rsPanierRow["Code"] + "";
                    if (SSDBGridCorps.ActiveRow.IsAddRow)
                    {
                        SSDBGridCorps.UpdateData();
                        SSDBGridCorps.Rows[RowIndex].Delete(false);
                        InsertionArticle(rsPanierRow["Code"] + "", ref RowIndex, true, rsArticle.Rows[0], Convert.ToInt32(General.nz(rsPanierRow["Quantite"], "1")));
                        turn = true;
                    }
                    else
                    {
                        SSDBGridCorps.ActiveRow.Cells["CodeArticle"].Value = strOldValue;
                        SSDBGridCorps.UpdateData();
                        InsertionArticle(rsPanierRow["Code"] + "", ref RowIndex, false, rsArticle.Rows[0], Convert.ToInt32(General.nz(rsPanierRow["Quantite"], "1")));
                        turn = true;
                    }
                }
                rsArticle = null;
            }

            manager.AllEventsEnabled = true;

            modAdorsPanier?.Dispose();
        }

        /// <summary>
        /// Tested
        /// Modfis de la version 26.07.2020 de VB6 Ajouté par Mondir
        /// </summary>
        private void fc_StockPanierGecet()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_StockPanierGecet() - Enter In The Function");
            //===> Fin Modif Mondir

            DataTable rsPanier = default(DataTable);
            ModAdo modAdorsPanier = null;
            string sAddItem = null;

            ///Mondir 24.06.2020, Modifs de la version 26.06.2020 envoyé par Rachid
            bool baddRow;

            if (SSDBGridCorps.ActiveRow != null && SSDBGridCorps.ActiveRow.IsAddRow)
                baddRow = true;
            else
                baddRow = false;
            ///Fi modif Mondir

            try
            {
                modAdorsPanier = new ModAdo();
                rsPanier = new DataTable();
                rsPanier = modAdorsPanier.fc_OpenRecordSet("SELECT * FROM PanierGecet WHERE Utilisateur='" + General.gsUtilisateur + "' ORDER BY CleAuto DESC");

                var SSDBGridCorpsSource = SSDBGridCorps.DataSource as DataTable;

                var manager = SSDBGridCorps.EventManager;
                manager.AllEventsEnabled = false;

                if (rsPanier.Rows.Count > 0)
                {
                    foreach (DataRow rsPanierRow in rsPanier.Rows)
                    {
                        var NewRow = SSDBGridCorpsSource.NewRow();

                        NewRow["NumeroDevis"] = DBNull.Value;
                        //NumeroDevis
                        NewRow["NumeroLigne"] = DBNull.Value;
                        //NumeroLigne
                        NewRow["CodeFamille"] = DBNull.Value;
                        //CodeFamille
                        NewRow["CodeSousFamille"] = DBNull.Value;
                        //CodeSousFamille
                        NewRow["CodeArticle"] = DBNull.Value;
                        //CodeArticle
                        if (General.sGecetV3 == "1")
                        {
                            if (!string.IsNullOrEmpty(General.nz(rsPanierRow["idBDD2"], "").ToString()))
                            {
                                //=== modif du 17 09 2019, article provenant de la base de donnée 2.
                                // NewRow["CodeSousArticle"] = General.cArt80000;//'CodeSousArticle
                                NewRow["CodeSousArticle"] = General.cArtiNonQualifie + General.nz(rsPanierRow["idBDD2"], 0);//'CodeSousArticle
                            }
                            else
                            {
                                using (var tmpModAdo = new ModAdo())
                                    NewRow["CodeSousArticle"] = tmpModAdo.fc_ADOlibelle("SELECT  CHRONO_INITIAL From ArticleTarif WHERE  IDGEC_LIGNE_PRIX =" + General.nz(rsPanierRow["Code"], "0") + "", false, ModParametre.adoGecet);
                            }

                        }
                        else if (General.sGecetV2 == "1")
                        {
                            using (var tmpModAdo = new ModAdo())
                                NewRow["CodeSousArticle"] = tmpModAdo.fc_ADOlibelle("SELECT ART_Chrono FROM ARTP_ArticlePrix WHERE ARTP_NoAuto=" + General.nz(rsPanierRow["Code"], "0") + "", false, ModParametre.adoGecet);
                        }
                        else
                        {
                            using (var tmpModAdo = new ModAdo())
                                NewRow["CodeSousArticle"] = tmpModAdo.fc_ADOlibelle("SELECT Chrono FROM DtiPrix WHERE NoAuto=" + General.nz(rsPanierRow["Code"], "0") + "", false, ModParametre.adoGecet);
                            //CodeSousArticle
                        }

                        NewRow["NumOrdre"] = "";
                        //NumOrdre
                        NewRow["TexteLigne"] = rsPanierRow["Designation"];
                        //TexteLigne
                        NewRow["Quantite"] = rsPanierRow["Quantite"];
                        //Quantite
                        if (General.sGecetV3 == "1")
                        {

                            if (!string.IsNullOrEmpty(General.nz(rsPanierRow["idBDD2"], "").ToString()))
                            {
                                //=== modif du 17 09 2019, article provenant de la base de donnée 2.
                                using (var tmpModAdo = new ModAdo())
                                    NewRow["Unite"] = General.Left(tmpModAdo.fc_ADOlibelle("SELECT     UNITE From BDD2 Where IDGEC_DONNEES_NON_QUALIFIEES = " + General.nz(rsPanierRow["idBDD2"], "0") + "", false, ModParametre.adoGecet), 3); //'Unite
                                                                                                                                                                                                                                                // NewRow["Unite"] = tmpModAdo.fc_ADOlibelle("SELECT     UNITE From BDD2 Where IDGEC_DONNEES_NON_QUALIFIEES = " + General.nz(rsPanierRow["idBDD2"], "0") + "", false, ModParametre.adoGecet); //'Unite

                            }
                            else
                            {
                                using (var tmpModAdo = new ModAdo())
                                    NewRow["Unite"] = tmpModAdo.fc_ADOlibelle("SELECT ArticleTarif.CODE_UNITE_DEVIS FROM  ArticleTarif WHERE IDGEC_LIGNE_PRIX =" + General.nz(rsPanierRow["Code"], "0") + "", false, ModParametre.adoGecet);
                            }
                        }
                        else if (General.sGecetV2 == "1")
                        {
                            //sAdditem = sAdditem & "!" & fc_ADOlibelle("SELECT ARTP_UniteFourn FROM  ARTP_ArticlePrix WHERE ARTP_NoAuto=" & nz(!Code, "0") & "", , adoGecet) & "!;" 'Unite
                            //=== modif du 17 11 2014 changement de l'unité vers unité devis.
                            using (var tmpModAdo = new ModAdo())
                                NewRow["Unite"] = tmpModAdo.fc_ADOlibelle("SELECT ARTP_UniteDevis FROM  ARTP_ArticlePrix WHERE ARTP_NoAuto=" + General.nz(rsPanierRow["Code"], "0") + "", false, ModParametre.adoGecet);
                            //Unite

                        }
                        else
                        {
                            using (var tmpModAdo = new ModAdo())
                                NewRow["Unite"] = tmpModAdo.fc_ADOlibelle("SELECT fouunite FROM DtiPrix WHERE NoAuto=" + General.nz(rsPanierRow["Code"], "0") + "", false, ModParametre.adoGecet);
                            //Unite
                        }
                        NewRow["Mohud"] = DBNull.Value;
                        //Mohud
                        NewRow["CodeMO"] = DBNull.Value;
                        //CodeMO
                        NewRow["PxHeured"] = DBNull.Value;
                        //PxHeured
                        if (General.sUseCoefGecet == "1")
                        {
                            NewRow["Foud"] = Convert.ToDouble(rsPanierRow["PxVenteFOSC"]) * General.dbGenCoefGecet;
                            //Foud
                        }
                        else
                        {
                            NewRow["Foud"] = Convert.ToDouble(rsPanierRow["PxVenteFOSC"]);
                            //Foud
                        }
                        NewRow["Stud"] = DBNull.Value;
                        //Stud
                        NewRow["Mohd"] = DBNull.Value;
                        //Mohd
                        NewRow["Mod"] = DBNull.Value;
                        //Mod
                        if (General.sUseCoefGecet == "1")
                        {
                            NewRow["Fod"] = Convert.ToString(Convert.ToDouble(General.nz(rsPanierRow["Quantite"], 0)) * Convert.ToDouble(General.nz(rsPanierRow["PxVenteFOSC"], 0)) * General.dbGenCoefGecet);
                            //Fod
                        }
                        else
                        {
                            NewRow["Fod"] = Convert.ToString(Convert.ToDouble(General.nz(rsPanierRow["Quantite"], 0)) * Convert.ToDouble(General.nz(rsPanierRow["PxVenteFOSC"], 0)));
                            //Fod
                        }
                        NewRow["Std"] = DBNull.Value;
                        //Std
                        if (General.sUseCoefGecet == "1")
                        {
                            NewRow["TotalDebours"] = Convert.ToString(Convert.ToDouble(General.nz(rsPanierRow["Quantite"], 0)) * Convert.ToDouble(General.nz(rsPanierRow["PxVenteFOSC"], 0)) * General.dbGenCoefGecet);
                            //TotalDebours
                        }
                        else
                        {
                            NewRow["TotalDebours"] = Convert.ToString(Convert.ToDouble(General.nz(rsPanierRow["Quantite"], 0)) * Convert.ToDouble(General.nz(rsPanierRow["PxVenteFOSC"], 0)));
                            //TotalDebours
                        }
                        NewRow["KMO"] = DBNull.Value;
                        //KMO
                        NewRow["MO"] = DBNull.Value;
                        //MO
                        NewRow["KFO"] = rsPanierRow["kFO"];
                        //KFO
                        if (General.sUseCoefGecet == "1")
                        {
                            NewRow["FO"] = Convert.ToString(Convert.ToDouble(General.nz(rsPanierRow["kFO"], 0)) * Convert.ToDouble(General.nz(rsPanierRow["Quantite"], 0)) * Convert.ToDouble(General.nz(rsPanierRow["PxVenteFOSC"], 0)) * General.dbGenCoefGecet);
                            //FO
                        }
                        else
                        {
                            NewRow["FO"] = Convert.ToString(Convert.ToDouble(General.nz(rsPanierRow["kFO"], 0)) * Convert.ToDouble(General.nz(rsPanierRow["Quantite"], 0)) * Convert.ToDouble(General.nz(rsPanierRow["PxVenteFOSC"], 0)));
                            //FO
                        }
                        NewRow["KST"] = DBNull.Value;
                        //KST
                        NewRow["ST"] = DBNull.Value;
                        //ST
                        if (General.sUseCoefGecet == "1")
                        {
                            NewRow["TotalVente"] = Convert.ToString(Convert.ToDouble(General.nz(rsPanierRow["kFO"], 0)) * Convert.ToDouble(General.nz(rsPanierRow["Quantite"], 0)) * Convert.ToDouble(General.nz(rsPanierRow["PxVenteFOSC"], 0)) * General.dbGenCoefGecet);
                            //TotalVente
                        }
                        else
                        {
                            NewRow["TotalVente"] = Convert.ToString(Convert.ToDouble(General.nz(rsPanierRow["kFO"], 0)) * Convert.ToDouble(General.nz(rsPanierRow["Quantite"], 0)) * Convert.ToDouble(General.nz(rsPanierRow["PxVenteFOSC"], 0)));
                            //TotalVente
                        }
                        NewRow["CodeTVA"] = General.nz((txtTaux.Text), "5.5");
                        //CodeTVA
                        NewRow["Coef1"] = DBNull.Value;
                        //Coef1
                        NewRow["Coef2"] = DBNull.Value;
                        //Coef2
                        NewRow["Coef3"] = DBNull.Value;
                        //Coef3
                        NewRow["TotalVenteApresCoef"] = DBNull.Value;
                        //TotalVenteApresCoef
                        NewRow["Fournisseur"] = DBNull.Value;
                        //Fournisseur

                        ///Mondir 27.06.2020 Modifs de la version V26.06.2020 enoiyé par Rachid
                        if (baddRow)
                        {
                            SSDBGridCorpsSource.Rows.Add(NewRow);
                        }
                        else
                        {
                            SSDBGridCorpsSource.Rows.InsertAt(NewRow, SSDBGridCorps.ActiveRow.Index);
                        }
                        //Find Modif Mondir
                        //SSDBGridCorps.DataBind();
                        //SSDBGridCorps.UpdateData();
                    }
                }
                ///=====================> Added par Mondir to fix this bug https://groupe-dt.mantishub.io/view.php?id=1660
                else
                    SSDBGridCorps.ActiveRow.CancelUpdate();
                //modAdorsPanier?.Dispose();
                //SSDBGridCorps.UpdateData();.

                //SSDBGridCorpsSource.Rows[SSDBGridCorps.ActiveRow.Index - 1].Delete();
                //SSDBGridCorpsSource.PerformAction
                //Added By Mondir, The same thing happend on VB6, in VB6 we got out from the cell
                ///=====================> Added par Mondir to fix this bug https://groupe-dt.mantishub.io/view.php?id=1660
                SSDBGridCorps.PerformAction(UltraGridAction.ExitEditMode);

                manager.AllEventsEnabled = true;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }

        }

        //TODO : Mondir - Event To Be In The SubMenu
        //private void LabelGerant_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    short Index = LabelGerant.GetIndex(eventSender);
        //    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        //    ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocClient);
        //}

        //TODO : Mondir - Event To Be In The SubMenu
        //private void LabelImmeuble_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    short Index = LabelImmeuble.GetIndex(eventSender);
        //    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        //    ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocImmeuble);
        //}

        //TODO : Mondir - Event To Be In The SubMenu
        //private void LabelDispatchIntervention_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        //    ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDispatch);
        //}

        //TODO : Mondir - Event To Be In The SubMenu
        //private void LabelDispatchDevis_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    short Index = LabelDispatchDevis.GetIndex(eventSender);
        //    ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDispatchDevis);
        //}

        //TODO : Mondir - Event To Be In The SubMenu
        //private void LabelAppel_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    short Index = LabelAppel.GetIndex(eventSender);
        //    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        //    ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocStandard);
        //}

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeChrono"></param>
        /// <returns></returns>
        private object fc_StockFourGecet(string sCodeChrono)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_StockFourGecet() - Enter In The Function - sCodeChrono = {sCodeChrono}");
            //===> Fin Modif Mondir

            object functionReturnValue = null;
            DataTable rsPanier = default(DataTable);
            SqlDataAdapter SDArsPanier = null;
            string sAddItem = null;
            string stTemp = null;
            double dbTemp = 0;

            try
            {
                rsPanier = new DataTable();
                //sUseCoefGecet = "1"
                if (General.sGecetV3 == "1")
                {
                    SDArsPanier = new SqlDataAdapter("SELECT    Article.DESIGNATION_ARTICLE AS Libelle, Article.CHRONO_INITIAL AS Chrono,"
                        + " ArticleTarif.PRX_DEVIS_NET_UD AS [prix net achat], "
                        + " ArticleTarif.CODE_UNITE_DEVIS AS fouunite"
                        + " FROM         Article INNER JOIN"
                        + " ArticleTarif ON Article.CHRONO_INITIAL = ArticleTarif.CHRONO_INITIAL"
                        + "  WHERE Article.CHRONO_INITIAL = '" + sCodeChrono + "' " + " ORDER BY [prix net achat] DESC", ModParametre.adoGecet);
                    SDArsPanier.Fill(rsPanier);
                }
                else if (General.sGecetV2 == "1")
                {
                    //rsPanier.Open "SELECT     ART_Article.ART_Libelle AS Libelle, ART_Article.ART_Chrono AS Chrono," _
                    //& " ARTP_ArticlePrix.ARTP_PrixNetDevis AS [prix net achat], " _
                    //& " ARTP_ArticlePrix.ARTP_UniteFourn AS fouunite" _
                    //& " FROM ART_Article INNER JOIN" _
                    //& " ARTP_ArticlePrix ON ART_Article.ART_Chrono = ARTP_ArticlePrix.ART_Chrono" _
                    //& " WHERE ART_Article.ART_Chrono = '" & sCodeChrono & "' " _
                    //& " ORDER BY [prix net achat] DESC", adoGecet
                    //==== modif du 17 11 2014, changmenet de l'unité devis.
                    SDArsPanier = new SqlDataAdapter("SELECT     ART_Article.ART_Libelle AS Libelle, ART_Article.ART_Chrono AS Chrono,"
                        + " ARTP_ArticlePrix.ARTP_PrixNetDevis AS [prix net achat], "
                        + " ARTP_ArticlePrix.ARTP_UniteDevis AS fouunite"
                        + " FROM ART_Article INNER JOIN"
                        + " ARTP_ArticlePrix ON ART_Article.ART_Chrono = ARTP_ArticlePrix.ART_Chrono"
                        + " WHERE ART_Article.ART_Chrono = '" + sCodeChrono + "' " + " ORDER BY [prix net achat] DESC", ModParametre.adoGecet);
                    SDArsPanier.Fill(rsPanier);

                }
                else
                {

                    SDArsPanier = new SqlDataAdapter("SELECT Dtiarti.Libelle, DtiPrix.Chrono, DtiPrix.[prix net achat], DtiPrix.fouunite "
                        + " FROM DtiArti INNER JOIN Dtiprix on Dtiarti.Chrono=Dtiprix.Chrono "
                        + " WHERE Dtiprix.Chrono='" + sCodeChrono + "' ORDER BY [prix net achat] DESC", ModParametre.adoGecet);
                    SDArsPanier.Fill(rsPanier);
                }

                var SSDBGridCorpsSource = SSDBGridCorps.DataSource as DataTable;

                if (rsPanier.Rows.Count > 0)
                {
                    //        While Not rsPanier.EOF

                    var newRow = SSDBGridCorpsSource.NewRow();

                    var Row = rsPanier.Rows[0];
                    //var NewRow = SSDBGridCorps.DisplayLayout.Bands[0].AddNew();
                    newRow["NumeroDevis"] = DBNull.Value;
                    //NumeroDevis
                    newRow["NumeroLigne"] = DBNull.Value;
                    //NumeroLigne
                    newRow["CodeFamille"] = DBNull.Value;
                    //CodeFamille
                    newRow["CodeSousFamille"] = DBNull.Value;
                    //CodeSousFamille
                    newRow["CodeArticle"] = DBNull.Value;
                    //CodeArticle
                    newRow["CodeSousArticle"] = sCodeChrono;
                    //CodeSousArticle
                    newRow["NumOrdre"] = DBNull.Value;
                    //NumOrdre
                    newRow["TexteLigne"] = Row["Libelle"] + "";
                    //TexteLigne
                    newRow["Quantite"] = "1";
                    //Quantite
                    newRow["Unite"] = Row["fouunite"];
                    //Unite
                    newRow["Mohud"] = DBNull.Value;
                    //Mohud
                    newRow["CodeMO"] = DBNull.Value;
                    //CodeMO
                    newRow["PxHeured"] = DBNull.Value;
                    //PxHeured

                    if (General.sUseCoefGecet == "1")
                    {
                        stTemp = Convert.ToString(Convert.ToDouble(Row["prix net achat"].ToString().Replace(" ", "")));
                        if (General.IsNumeric(stTemp))
                        {
                            dbTemp = Convert.ToDouble(stTemp) * General.dbGenCoefGecet;
                            dbTemp = Math.Round(dbTemp);
                            stTemp = dbTemp.ToString();
                            stTemp = stTemp.Replace(" ", "");
                        }
                        newRow["Foud"] = stTemp;
                        //Foud
                    }
                    else
                    {
                        newRow["Foud"] = Convert.ToString(Convert.ToDouble(Row["prix net achat"].ToString().Replace(" ", "")));
                        //Foud
                    }

                    newRow["Stud"] = DBNull.Value;
                    //Stud
                    newRow["Mohd"] = DBNull.Value;
                    //Mohd
                    newRow["Mod"] = DBNull.Value;
                    //Mod

                    if (General.sUseCoefGecet == "1")
                    {
                        stTemp = Convert.ToString(Convert.ToDouble(Row["prix net achat"].ToString().Replace(" ", "")));
                        if (General.IsNumeric(stTemp))
                        {
                            dbTemp = Convert.ToDouble(stTemp) * General.dbGenCoefGecet;
                            dbTemp = Math.Round(dbTemp);
                            stTemp = Convert.ToString(dbTemp);
                            stTemp = stTemp.Replace(" ", "");
                        }
                        newRow["Fod"] = stTemp;
                        //Fod
                    }
                    else
                    {
                        newRow["Fod"] = Convert.ToString(Convert.ToDouble(Row["prix net achat"].ToString().Replace(" ", "")));
                        //Fod
                    }

                    newRow["Std"] = DBNull.Value;
                    //Std

                    if (General.sUseCoefGecet == "1")
                    {
                        stTemp = Convert.ToString(Convert.ToDouble(General.nz("1", 0)) * Convert.ToDouble(General.nz(Convert.ToString(Convert.ToDouble(Row["prix net achat"].ToString().Replace(" ", ""))), 0)));
                        if (General.IsNumeric(stTemp))
                        {
                            dbTemp = Convert.ToDouble(stTemp) * General.dbGenCoefGecet;
                            dbTemp = Math.Round(dbTemp);
                            stTemp = Convert.ToString(dbTemp);
                            stTemp = stTemp.Replace(" ", "");
                        }
                        newRow["TotalDebours"] = stTemp;
                        //TotalDebours
                    }
                    else
                    {
                        newRow["TotalDebours"] = Convert.ToString(Convert.ToDouble(General.nz("1", 0)) * Convert.ToDouble(General.nz(Convert.ToString(Convert.ToDouble(Row["prix net achat"].ToString().Replace(" ", ""))), 0)));
                        //TotalDebours
                    }
                    newRow["KMO"] = DBNull.Value;
                    //KMO
                    newRow["MO"] = DBNull.Value;
                    //MO
                    newRow["KFO"] = Text314.Text;
                    //KFO
                    if (General.sUseCoefGecet == "1")
                    {
                        stTemp = Convert.ToString(Convert.ToDouble(General.nz(Text314.Text, 0)) * Convert.ToDouble(General.nz("1", 0)) *
                            Convert.ToDouble(General.nz(Convert.ToString(Convert.ToDouble(Row["prix net achat"].ToString().Replace(" ", ""))), 0)));
                        if (General.IsNumeric(stTemp))
                        {
                            dbTemp = Convert.ToDouble(stTemp) * General.dbGenCoefGecet;
                            dbTemp = Math.Round(dbTemp);
                            stTemp = Convert.ToString(dbTemp);
                            stTemp = stTemp.Replace(" ", "");
                        }
                        newRow["FO"] = stTemp;
                        //FO
                    }
                    else
                    {
                        newRow["FO"] = Convert.ToString(Convert.ToDouble(General.nz(Text314.Text, 0)) * Convert.ToDouble(General.nz("1", 0)) *
                            Convert.ToDouble(General.nz(Convert.ToString(Convert.ToDouble(Row["prix net achat"].ToString().Replace(" ", ""))), 0)));
                        //FO
                    }

                    newRow["KST"] = DBNull.Value;
                    //KST
                    newRow["ST"] = DBNull.Value;
                    //ST

                    if (General.sUseCoefGecet == "1")
                    {
                        stTemp = Convert.ToString(Convert.ToDouble(General.nz(Text314.Text, 0)) * Convert.ToDouble(General.nz("1", 0)) * Convert.ToDouble(General.nz(Convert.ToString(Convert.ToDouble(Row["prix net achat"].ToString().Replace(" ", ""))), 0)));
                        if (General.IsNumeric(stTemp))
                        {
                            dbTemp = Convert.ToDouble(stTemp) * General.dbGenCoefGecet;
                            dbTemp = Math.Round(dbTemp);
                            stTemp = Convert.ToString(dbTemp);
                            stTemp = stTemp.Replace(" ", "");
                        }
                        newRow["TotalVente"] = stTemp;
                        //TotalVente
                    }
                    else
                    {
                        newRow["TotalVente"] = Convert.ToString(Convert.ToDouble(General.nz(Text314.Text, 0)) * Convert.ToDouble(General.nz("1", 0)) *
                            Convert.ToDouble(General.nz(Convert.ToString(Convert.ToDouble(Row["prix net achat"].ToString().Replace(" ", ""))), 0)));
                        //TotalVente
                    }
                    newRow["CodeTVA"] = General.nz((txtTaux.Text), "5.5");
                    //CodeTVA
                    newRow["Coef1"] = DBNull.Value;
                    //Coef1
                    newRow["Coef2"] = DBNull.Value;
                    //Coef2
                    newRow["Coef3"] = DBNull.Value;
                    //Coef3
                    newRow["TotalVenteApresCoef"] = DBNull.Value;
                    //TotalVenteApresCoef
                    newRow["Fournisseur"] = DBNull.Value;
                    //Fournisseur
                    if (SSDBGridCorps.ActiveRow.IsAddRow)
                    {
                        SSDBGridCorps.UpdateData();
                        SSDBGridCorps.Rows[SSDBGridCorps.Rows.Count - 1].Delete(false);
                        SSDBGridCorpsSource.Rows.Add(newRow);
                    }
                    else
                    {
                        SSDBGridCorps.ActiveCell.Value = strOldValue;
                        SSDBGridCorpsSource.Rows.InsertAt(newRow, SSDBGridCorps.ActiveRow.Index);
                    }
                    //SSDBGridCorps.DataBind();
                    //SSDBGridCorps.UpdateData();
                    //        Wend
                }
                rsPanier?.Dispose();
                SDArsPanier?.Dispose();
                return functionReturnValue;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " fc_StockFourGecet ");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGridCorps_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            //Program.SaveException(null, $"SSDBGridCorps_InitializeRow() - Enter In The Function");
            //===> Fin Modif Mondir

            short i = 0;

            try
            {
                e.Row.CellAppearance.BackColor = ColorTranslator.FromOle(0xffffff);

                foreach (UltraGridCell cell in e.Row.Cells)
                {
                    if (cell.Column.Key.ToUpper() == "TotalVenteApresCoef".ToUpper() || cell.Column.Key.ToUpper() == "TotalDebours".ToUpper() ||
                        cell.Column.Key.ToUpper() == "TotalVente".ToUpper())
                    {
                        cell.Appearance.BackColor = ColorTranslator.FromOle(0xc0c0c0);
                        cell.Appearance.BackColor2 = ColorTranslator.FromOle(0xc0c0c0);
                    }
                    else
                    {
                        cell.Appearance.BackColor = Color.FromArgb(255, 224, 192);
                        cell.Appearance.BackColor2 = Color.FromArgb(255, 224, 192);
                    }
                }

                //e.Row.CellAppearance.BackColor = Color.FromArgb(255, 224, 192);

                ///=============> Tested
                if ((e.Row.Cells["CodeFamille"].Value != null && !string.IsNullOrEmpty(e.Row.Cells["CodeFamille"].Value.ToString())) ||
                (e.Row.Cells["CodeSousFamille"].Value != null && !string.IsNullOrEmpty(e.Row.Cells["CodeSousFamille"].Value.ToString())))
                {
                    for (i = 8; i <= e.Row.Cells.Count - 2; i++)
                    {
                        e.Row.Cells[i].Appearance.BackColor = ColorTranslator.FromOle(0xc0c0c0);
                        e.Row.Cells[i].Appearance.ForeColor = ColorTranslator.FromOle(0xffffff);
                        e.Row.Cells[i].Activation = Activation.AllowEdit;
                    }
                }

                //----Color en jaune Les enregistrements avec le champs choix
                //----à oui.
                if (e.Row.Cells["Codesousarticle"].Value != null && !string.IsNullOrEmpty(e.Row.Cells["Codesousarticle"].Value.ToString()))
                {
                    e.Row.Cells["codesousarticle"].Appearance.BackColor = ColorTranslator.FromOle(0xffffff);
                    e.Row.Cells["texteligne"].Appearance.BackColor = ColorTranslator.FromOle(0xffffff);
                    e.Row.Cells[i].Activation = Activation.NoEdit;
                    // For i = 1 To .Groups(0).Columns.Count - 1                          ' SSOleDBGrid1.Columns(i).CellStyleSet "yesStyle"
                    //     .Groups(0).Columns(i).CellStyleSet "yesStyle", .Row
                    // Next i
                    //  Else
                    //     .Columns("codesousarticle").CellStyleSet "NoStyle", .Row
                    //     .Columns("Texteligne").CellStyleSet "NoStyle", .Row
                }
                e.Row.Cells["TotalVenteApresCoef"].Appearance.BackColor = Color.FromArgb(224, 224, 224);
                e.Row.Cells["TotalDebours"].Appearance.BackColor = Color.FromArgb(224, 224, 224);

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";SSDBGridCorps_RowLoaded");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGridCorps_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSDBGridCorps_Error() - Enter In The Function");
            //===> Fin Modif Mondir

            Type lDatatype;

            try
            {
                //SSDBGridCorps.ActiveCell.Value = DBNull.Value;
                e.Cancel = true;
                //if (e.ErrorType == ErrorType.Data)
                //{
                //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie de données incorrectes dans la colonne " + SSDBGridCorps.ActiveCell.Column.Header.Caption);
                //    e.Cancel = true;
                //}
                //TODO : Mondir - Check This Commented Lines, Must Show A Error Message With Entring Another Type Of Value
                //if (eventArgs.errCode == 5)
                //{
                //    Interaction.MsgBox("Il y a eu une erreur de conversion dans la colonne " + SSDBGridCorps.Columns[eventArgs.colIndex].Caption, MsgBoxStyle.Critical, "Erreur de conversion de donnée");
                //    BoolAffichemessageErrur = false;
                //}
                //else
                //{
                //    BoolAffichemessageErrur = true;
                //}

                //===================>TODO : Mondir - Start Commeting
                //BOOLErreurCorps = true;


                //var _with67 = SSDBGridCorps;

                //lDatatype = _with67.ActiveCell.Column.DataType;

                //if ((lDatatype == typeof(short) || lDatatype == typeof(int) || lDatatype == typeof(double) || lDatatype == typeof(DateTime)))
                //{
                //    _with67.ActiveCell.Value = null;
                //}
                ///=======================> Finished Commeting

                //    If ErrCode = 5 Then
                //        Cancel = True
                //        ErrString = "Erreur de saisie dans la colonne " & .Columns(Colindex).Caption
                //        MsgBox ErrString
                //    End If
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSDBGridCorps_UpdateError");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSetat_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSetat_BeforeDropDown() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                if (!string.IsNullOrEmpty(txtDateImpression.Text))
                {
                    sheridan.InitialiseCombo(SSetat, "SELECT code, libelle from DevisCodeEtat WHERE ApImprime=1", "code");
                }
                else
                {
                    sheridan.InitialiseCombo(SSetat, "SELECT code, libelle from DevisCodeEtat WHERE CodeEtatManuel=1", "code");

                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSetat_DropDown");
            }

        }

        //TODO : Mondir - Search About This Event And Add It
        //private void SSDBGridCorps_ValidationError(System.Object eventSender, AxSSDataWidgets_B_OLEDB._DSSDBGridEvents_ValidationErrorEvent eventArgs)
        //{
        //    Interaction.MsgBox("Données invalides : " + eventArgs.invalidText + Constants.vbCrLf + "Position :" + Strings.Chr(eventArgs.startPosition));
        //}

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridFacArticle_AfterRowUpdate(object sender, RowEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridFacArticle_AfterRowUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            int i = 0;

            //    If ssGridFacArticle.Columns("CodeArticle").value = "" Then
            //        If ssGridFacArticle.Rows > 0 Then
            //            ssGridFacArticle.MoveFirst
            //        End If
            //    End If

            if (string.IsNullOrEmpty(e.Row.Cells["CodeArticle"].Value.ToString()))
            {
                GridFournitArtDetail.DisplayLayout.Bands[0].Override.AllowAddNew = AllowAddNew.No;
            }
            else
            {
                GridFournitArtDetail.DisplayLayout.Bands[0].Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
            }
            if (GridFournitArtDetail.ActiveRow == null || e.Row.Cells["CodeArticle"].Value != GridFournitArtDetail.ActiveRow.Cells["CodeArticle"].Value)
            {
                fc_GridFournitArtDetail((e.Row.Cells["CodeArticle"].Value.ToString()));
            }

            var xx = modAdorsFacArticle.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridFacArticle_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridFacArticle_BeforeExitEditMode() - Enter In The Function");
            //===> Fin Modif Mondir

            var _with68 = ssGridFacArticle;
            if (_with68.ActiveCell.Column.Index == _with68.ActiveRow.Cells["CodeArticle"].Column.Index)
            {
                _with68.ActiveRow.Cells["CodeArticle"].Value = StdSQLchaine.fc_CtrlCharDevis(_with68.ActiveRow.Cells["CodeArticle"].Text);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridFacArticle_AfterCellActivate(object sender, EventArgs e)
        {

        }

        /// <summary>
        /// Tesed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridFacArticle_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridFacArticle_BeforeRowsDeleted() - Enter In The Function");
            //===> Fin Modif Mondir

            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
                var xx = General.Execute("DELETE FROM DEV_FournitArticleDetail WHERE CodeArticle='" + ssGridFacArticle.ActiveRow.Cells["CodeArticle"].Value + "'");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBCmbMail_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBCmbMail_BeforeDropDown() - Enter In The Function");
            //===> Fin Modif Mondir

            if (SSOleDBCmbMail.Rows.Count == 0)
            {
                initialiseComboMail();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void SSOleDBCombo1_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBCombo1_TextChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            boolModif = true;
        }

        /// <summary>
        /// Controle Is Hidden
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void SSOleDBCombo1_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSOleDBCombo1_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(eventArgs.KeyChar);
            try
            {
                if (KeyAscii == 13)
                {
                    cmdTypeREglement_Click(cmdTypeREglement, new System.EventArgs());
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSOleDBCombo1_KeyPress");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridSousFamilleArticleDetail_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"gridSousFamilleArticleDetail_BeforeRowsDeleted() - Enter In The Function");
            //===> Fin Modif Mondir

            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridSousFamilleArticle_BeforeRowsDeleted(object sender, BeforeRowsDeletedEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"GridSousFamilleArticle_BeforeRowsDeleted() - Enter In The Function");
            //===> Fin Modif Mondir

            int nbRecAff = 0;
            e.DisplayPromptMsg = false;

            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
            else
            {
                nbRecAff = General.Execute("DELETE FROM SousFamilleArticleDetail WHERE CodeSousFamille='" + GridSousFamilleArticle.ActiveRow.Cells["CodeSousFamille"].Text + "'");
            }
        }

        private void SSTab1_SelectedTabChanging(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangingEventArgs e)
        {

        }


        /// <summary>
        /// Tested
        /// </summary>
        private void FC_ChargePoste()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"FC_ChargePoste() - Enter In The Function");
            //===> Fin Modif Mondir

            //    If GridSousFamilleArticle.Rows = 0 Then
            General.sSQL = "SELECT  SousFamilleArticle.CodeSousFamille, SousFamilleArticle.IntituleSousFamille" + " FROM SousFamilleArticle" + " order by CodeSousFamille";
            rsSousFamilleArticle = new DataTable();
            modAdorsSousFamilleArticle = new ModAdo();
            rsSousFamilleArticle = modAdorsSousFamilleArticle.fc_OpenRecordSet(General.sSQL);
            //If rsSousFamilleArticle.EOF = False Then
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            GridSousFamilleArticle.Visible = false;
            GridSousFamilleArticle.DataSource = rsSousFamilleArticle;
            GridSousFamilleArticle.Visible = true;
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            if (rsSousFamilleArticle.Rows.Count > 0)
            {
                GridSousFamilleArticle.ActiveRow = GridSousFamilleArticle.Rows[0];
            }
            //End If
            //    End If
            cmbUniteSousArticle_BeforeDropDown(cmbUniteSousArticle, null);
            cmbUniteSousArticle.ValueMember = "Unité";


        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadFacArticle()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_LoadFacArticle() - Enter In The Function");
            //===> Fin Modif Mondir

            if (ssGridFacArticle.Rows.Count == 0)
            {
                General.sSQL = "SELECT FacArticle.CodeArticle, FacArticle.Designation1," + " FacArticle.PrixAchat, FacArticle.MOtps, FacArticle.FO," +
                    " FacArticle.ST, FacArticle.CodeCategorieArticle," + " FacArticle.Designation2, FacArticle.Designation3," + " FacArticle.Designation4, FacArticle.Designation5," +
                    " FacArticle.Designation6, FacArticle.Designation7," + " FacArticle.Designation8, FacArticle.Designation9," + " FacArticle.Designation10 " +
                    " FROM FacArticle WHERE CodeCategorieArticle = 'D'" + " ORDER BY CodeArticle";

                modAdorsFacArticle = new ModAdo();
                rsFacArticle = modAdorsFacArticle.fc_OpenRecordSet(General.sSQL);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                ssGridFacArticle.Visible = false;
                ssGridFacArticle.DataSource = rsFacArticle;
                ssGridFacArticle.Visible = true;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                if (rsFacArticle.Rows.Count > 0)
                {
                    ssGridFacArticle.ActiveRow = ssGridFacArticle.Rows[0];
                }

            }
            cmbUniteFournit_BeforeDropDown(cmbUniteFournit, null);
            cmbUniteFournit.ValueMember = "Unité";

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_OngletDivers()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_OngletDivers() - Enter In The Function");
            //===> Fin Modif Mondir

            //-- Alimente le table En téte de devis.
            var _with82 = Adodc6;
            General.sSQL = "SELECT * FROM TypeEnteteDevis ORDER BY LIBELLE";
            modAdoAdodc6 = new ModAdo();
            _with82 = modAdoAdodc6.fc_OpenRecordSet(General.sSQL, ssEnTete, "Code");
            ssEnTete.DataSource = _with82;

            //-- Alimente la table Réglement devis.
            var _with83 = Adodc7;
            General.sSQL = "SELECT * FROM TypeReglementDevis ORDER BY LibelleReglement";
            modAdoAdodc7 = new ModAdo();
            _with83 = modAdoAdodc7.fc_OpenRecordSet(General.sSQL, SStypeReglement, "CodeReglement");
            SStypeReglement.DataSource = _with83;

            var _with84 = Adodc5;
            modAdoAdodc5 = new ModAdo();
            General.sSQL = "SELECT Noauto, MO, FO, ST, PxHeure" + " FROM DEC_DevisCoefDefaut";
            _with84 = modAdoAdodc5.fc_OpenRecordSet(General.sSQL, ssGridDefaut, "Noauto");
            ssGridDefaut.DataSource = _with84;
        }

        /// <summary>
        /// TODO : Shows an error on VB6
        /// Mondir le 15.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssTYD_TypeDevis_AfterCloseUp(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssTYD_TypeDevis_AfterCloseUp() - Enter In The Function");
            //===> Fin Modif Mondir

            fc_TypeDevis();

            ///===> Mondir le 15.11.2020 pour ajouter les modfis de la version V12.11.2020
            fc_MajTypeDevis();
            //===> Fin Modif Mondir
        }

        /// <summary>
        /// TODO : Shows an error on VB6
        /// </summary>
        private void fc_TypeDevis()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_TypeDevis() - Enter In The Function");
            //===> Fin Modif Mondir

            string sSQL = null;

            if (!string.IsNullOrEmpty(ssTYD_TypeDevis.Text))
            {
                sSQL = "SELECT TYD_Libelle FROM TYD_TypeDevis" + " where TYD_Code ='" + ssTYD_TypeDevis.Text + "'";
                using (var tmpModAdo = new ModAdo())
                    lblTypeDevis.Text = tmpModAdo.fc_ADOlibelle(sSQL);
            }
            else
            {
                ssTYD_TypeDevis.Text = "";
            }

        }

        /// <summary>
        /// TODO : Mondir - This Fonctions Show An Error ON VB6, The table TYD_TypeDevis Not found
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssTYD_TypeDevis_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssTYD_TypeDevis_BeforeDropDown() - Enter In The Function");
            //===> Fin Modif Mondir

            string sSQL = null;

            sSQL = "SELECT TYD_Code, TYD_Libelle FROM TYD_TypeDevis";
            sheridan.InitialiseCombo(ssTYD_TypeDevis, sSQL, "TYD_Code");
        }

        /// <summary>
        /// TODO : Shows an error on VB6
        /// </summary>
        private void ssTYD_TypeDevis_Validated(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssTYD_TypeDevis_Validated() - Enter In The Function");
            //===> Fin Modif Mondir

            fc_TypeDevis();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Text3_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            var txt = eventSender as iTalk_TextBox_Small2;
            int Index = Convert.ToInt32(txt.Tag);

            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Text3_TextChanged() - Enter In The Function - Index = {Index}");
            //===> Fin Modif Mondir

            //Coef MO et Tarif MO
            if (Index == 16 || Index == 15)
            {
                if (General.IsNumeric(Text315.Text) && General.IsNumeric(Text316.Text))
                {
                    lblPrixVente.Text = Convert.ToString(Convert.ToDouble(Text315.Text) * Convert.ToDouble(Text316.Text)) + " €";
                }
                else
                {
                    lblPrixVente.Text = "0.00 €";
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Text3_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)(eventArgs.KeyChar);
            var textbox = eventSender as iTalk_TextBox_Small2;
            int Index = Convert.ToInt32(textbox.Tag);

            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Text3_KeyPress() - Enter In The Function - Index = {Index}");
            //===> Fin Modif Mondir

            double kMaj = 0;

            try
            {
                if (KeyAscii == 13)
                {
                    switch (Index)
                    {
                        case 8:
                            if (General.IsNumeric(Text38.Text))
                            {
                                General.giMsg = (short)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous allez appliquer un coefficient de " + Text38.Text + " sur l'ensemble du devis", "Modification du devis après accord", MessageBoxButtons.YesNo, MessageBoxIcon.Error);
                                if (General.giMsg == 6)
                                {
                                    prcCalculTotalAvantCoef((Text11.Text));
                                }
                                else
                                {
                                    Text38.Text = "1";
                                }
                            }
                            else
                            {
                                afficheMessage(Convert.ToString(1));
                                Text38.Text = "";
                                Text38.Focus();
                            }
                            break;

                        case 10:
                            if (General.IsNumeric(Text310.Text))
                            {
                                kMaj = Convert.ToDouble(Text310.Text) / (Convert.ToDouble(Text39.Text) / Convert.ToDouble(Text320.Text));
                                Text320.Text = Convert.ToString(kMaj);
                                General.giMsg = (short)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous allez appliquer un coefficient de " + kMaj + " sur l'ensemble du devis", "Modification du devis après accord", MessageBoxButtons.YesNo, MessageBoxIcon.Information);
                                if (General.giMsg == 6)
                                {
                                    prcCalculTotalAvantCoef((Text11.Text));
                                }
                                else
                                {
                                    Text320.Text = "1";
                                    Text310.Text = "";
                                }
                            }
                            else
                            {
                                if (Text310.Text == "")
                                {
                                    Text320.Text = 1 + "";
                                    prcCalculTotalAvantCoef((Text11.Text));
                                }
                                else
                                {
                                    afficheMessage(Convert.ToString(1));
                                    Text310.Text = "";
                                    Text310.Focus();
                                }

                            }
                            break;
                        case 18:
                        case 1:
                            if (General.IsNumeric(Text31.Text) && General.IsNumeric(Text318.Text))
                            {
                                Text317.Text = Convert.ToString(Convert.ToDouble(Text31.Text) * Convert.ToDouble(Text318.Text));
                                prcCalculTotalAvantCoef((Text11.Text));
                            }
                            else if (!General.IsNumeric(Text31.Text) && !string.IsNullOrEmpty(Text31.Text))
                            {
                                afficheMessage(Convert.ToString(1));
                                Text31.Focus();
                            }
                            else if (!General.IsNumeric(Text318.Text) && !string.IsNullOrEmpty(Text318.Text))
                            {
                                afficheMessage(Convert.ToString(1));
                                Text318.Focus();
                            }
                            break;
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Text3_KeyPress");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Text3_Leave(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Text3_Leave() - Enter In The Function");
            //===> Fin Modif Mondir

            txtTitreDevis.Text = Text322.Text;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text4_Leave(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Text4_Leave() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!General.IsNumeric(Text4.Text))
            {
                var _with87 = Text4;
                _with87.Text = Convert.ToString(1);
                _with87.Focus();
                return;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text9_TextChanged(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Text9_TextChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                ado3temp = new DataTable();
                ModAdo modAdoado3temp = null;
                switch (Text9.Text)
                {

                    case "1":
                        //recherche de l'adresse du gerant
                        var _with88 = ado3temp;
                        if (string.IsNullOrEmpty(Text5.Text) && string.IsNullOrEmpty(Text6.Text))
                        {
                            modAdoado3temp = new ModAdo();
                            _with88 = modAdoado3temp.fc_OpenRecordSet("SELECT Nom, Adresse1, CodePostal, Ville, eMail FROM Table1" + " WHERE (Code1 ='" + Text11x.Text + "')");
                            if (_with88.Rows.Count > 0)
                            {
                                Text5.Text = _with88.Rows[0][0] + "";
                                Text6.Text = _with88.Rows[0][1] + "";
                                Text7.Text = _with88.Rows[0][2] + "";
                                Text8.Text = _with88.Rows[0][3] + "";
                                initialiseComboMail();
                                var Source = SSOleDBCmbMail.DataSource as DataTable;
                                Source?.Rows.Add(_with88.Rows[0][4].ToString());
                                SSOleDBCmbMail.DataSource = Source;
                                SSOleDBCmbMail.Text = _with88.Rows[0][4] + "";
                            }
                            modAdoado3temp?.Dispose();
                        }

                        Text5.Enabled = false;
                        Text6.Enabled = false;
                        Text7.Enabled = false;
                        Text8.Enabled = false;
                        //SSOleDBCmbMail.Enabled = False
                        Option1.Checked = true;
                        break;

                    case "2":
                        //recherche de l'adresse du proprietaire
                        modAdoado3temp = new ModAdo();
                        var _with89 = ado3temp;
                        _with89 = modAdoado3temp.fc_OpenRecordSet("SELECT Nom, Adresse, CodePostal, Ville, CodeTVA, eMail FROM" + " ImmeublePart WHERE (CodeImmeuble ='" + Text14.Text + "') AND" + " (CodeParticulier ='" + Text111.Text + "')");
                        if (_with89.Rows.Count > 0)
                        {
                            Text5.Text = _with89.Rows[0][0] + "";
                            Text6.Text = _with89.Rows[0][1] + "";
                            Text7.Text = _with89.Rows[0][2] + "";
                            Text8.Text = _with89.Rows[0][3] + "";
                            initialiseComboMail();
                            var Source = SSOleDBCmbMail.DataSource as DataTable;
                            Source.Rows.Add(_with89.Rows[0][5].ToString());
                            SSOleDBCmbMail.DataSource = Source;
                            SSOleDBCmbMail.Text = _with89.Rows[0][5] + "";
                        }
                        modAdoado3temp?.Dispose();
                        Text5.Enabled = false;
                        Text6.Enabled = false;
                        Text7.Enabled = false;
                        Text8.Enabled = false;
                        //SSOleDBCmbMail.Enabled = False
                        Option2.Checked = true;
                        break;

                    case "3":
                        //efface les adresses
                        Text5.Text = "";
                        Text6.Text = "";
                        Text7.Text = "";
                        Text8.Text = "";
                        SSOleDBCmbMail.DataSource = null;
                        Text5.Enabled = true;
                        Text6.Enabled = true;
                        Text7.Enabled = true;
                        Text8.Enabled = true;
                        SSOleDBCmbMail.Enabled = true;
                        Option3.Checked = true;
                        break;
                }
                modAdoado3temp?.Dispose();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Text9_Change");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtdesArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtdesArticle_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13 && string.IsNullOrEmpty(txtdesArticle.Text))
            {
                cmdRechercheArticle0_Click(cmdRechercheArticle0, new System.EventArgs());
            }
            else if (KeyAscii == 13)
            {
                KeyAscii = 0;
                txtTitreDevis.Text = RechDesArticle(txtdesArticle.Text);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strV"></param>
        /// <returns></returns>
        private string RechDesArticle(string strV)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"RechDesArticle() - Enter In The Function - strV = {strV}");
            //===> Fin Modif Mondir

            string functionReturnValue = null;
            try
            {
                adotemp = new DataTable();
                ModAdo modAdoadotemp = null;
                var _with114 = adotemp;
                General.SQL = "SELECT  FacArticle.Designation1  FROM FacArticle where FacArticle.CodeArticle='" + strV + "'";
                modAdoadotemp = new ModAdo();
                _with114 = modAdoadotemp.fc_OpenRecordSet(General.SQL);
                if (_with114.Rows.Count > 0)
                {
                    functionReturnValue = _with114.Rows[0]["Designation1"] + "";
                }
                else
                {
                    functionReturnValue = "";
                }
                adotemp = null;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";RechDesArticle");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtDeviseur_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtDeviseur_BeforeDropDown() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                short i = 0;
                string req = null;

                req = "SELECT Personnel.Matricule,Personnel.Nom," + "Qualification.Qualification " + "FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif ";

                if (General.nbCodeDeviseur > 0)
                {
                    req = req + " WHERE (";
                    for (i = 1; i <= (General.nbCodeDeviseur); i++)
                    {
                        if (i < (General.nbCodeDeviseur))
                        {
                            req = req + "Personnel.CodeQualif='" + General.CodeQualifDeviseur[i] + "' OR ";
                        }
                        else
                        {
                            req = req + "Personnel.CodeQualif='" + General.CodeQualifDeviseur[i] + "'";
                        }
                    }
                    req = req + ")";
                }

                if (string.IsNullOrEmpty(req))
                {

                    req = " WHERE (Personnel.NonActif is null or Personnel.NonActif = 0) ";
                }
                else
                {
                    req = req + " AND (Personnel.NonActif is null or Personnel.NonActif = 0)";
                }

                req = req + " ORDER BY Personnel.Matricule ";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                //If txtDeviseur.Rows = 0 Then
                sheridan.InitialiseCombo(txtDeviseur, req, "Matricule");
                //End If
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtDeviseur_DropDown");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtFiltreLibellePoste_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtFiltreLibellePoste_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(eventArgs.KeyChar);
            if (KeyAscii == 13)
            {
                sub_FiltrePoste();
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtFiltrePoste_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtFiltrePoste_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            short KeyAscii = (short)(eventArgs.KeyChar);
            if (KeyAscii == 13)
            {
                sub_FiltrePoste();
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtRespTrav_TextChanged(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtRespTrav_TextChanged() - Enter In The Function");
            //===> Fin Modif Mondir

            string sSQL = null;

            if (!string.IsNullOrEmpty(Text11.Text))
            {
                if (!string.IsNullOrEmpty(txtRespTrav.Text))
                {
                    using (var tmpModAdo = new ModAdo())
                        sSQL = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + txtRespTrav.Text + "'");
                    if (string.IsNullOrEmpty(sSQL))
                    {

                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce responsable travaux n'existe pas.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        return;
                    }

                    sSQL = "UPDATE DevisEnTete set ResponsableTravaux ='" + StdSQLchaine.gFr_DoublerQuote(txtRespTrav.Text) + "'" + " WHERE NumeroDevis ='" + StdSQLchaine.gFr_DoublerQuote(Text11.Text) + "'";
                    var xx = General.Execute(sSQL);
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtRespTrav_AfterCloseUp(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtRespTrav_AfterCloseUp() - Enter In The Function");
            //===> Fin Modif Mondir

            string sSQL = null;

            if (!string.IsNullOrEmpty(Text11.Text))
            {
                if (!string.IsNullOrEmpty(txtRespTrav.Text))
                {
                    using (var tmpModAdo = new ModAdo())
                        sSQL = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + txtRespTrav.Text + "'");
                    if (string.IsNullOrEmpty(sSQL))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce responsable travaux n'existe pas.", "Erreur de saisie", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        return;
                    }

                    sSQL = "UPDATE DevisEnTete set ResponsableTravaux ='" + StdSQLchaine.gFr_DoublerQuote(txtRespTrav.Text) + "'" + " WHERE NumeroDevis ='" + StdSQLchaine.gFr_DoublerQuote(Text11.Text) + "'";
                    var xx = General.Execute(sSQL);
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtRespTrav_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtRespTrav_BeforeDropDown() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                short i = 0;
                string req = null;

                req = "SELECT Personnel.Matricule,Personnel.Nom," + "Qualification.Qualification " + "FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif ";

                if (General.nbCodeRespTrav > 0)
                {
                    if (General.CodeQualifRespTrav.Length > 0)
                    {
                        if (!(General.CodeQualifRespTrav[1] == "*"))
                        {
                            req = req + " WHERE (";
                            for (i = 1; i <= General.CodeQualifRespTrav.Length - 1; i++)
                            {
                                if (i < General.CodeQualifRespTrav.Length - 1)
                                {
                                    req = req + "Personnel.CodeQualif='" + General.CodeQualifRespTrav[i] + "' OR ";
                                }
                                else
                                {
                                    req = req + "Personnel.CodeQualif='" + General.CodeQualifRespTrav[i] + "'";
                                }
                            }
                            req = req + ")";
                        }
                    }
                }

                if (string.IsNullOrEmpty(req))
                {

                    req = " WHERE Personnel.NonActif is null or Personnel.NonActif = 0";
                }
                else
                {
                    req = req + " AND (Personnel.NonActif is null or Personnel.NonActif = 0)";
                }

                req = req + " ORDER BY Personnel.Matricule ";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                //If txtRespTrav.Rows = 0 Then
                sheridan.InitialiseCombo(txtRespTrav, req, "Matricule");
                //End If
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtRespTrav_DropDown;");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTaux_Validating(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtTaux_Validating() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                var _with113 = SSDBGridCorps;
                if (General.IsNumeric(txtTaux.Text))
                {
                    TauxTVADefaut = txtTaux.Text;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtTaux_Validate");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtTitreDevis_Leave(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"txtTitreDevis_Leave() - Enter In The Function");
            //===> Fin Modif Mondir

            Text322.Text = txtTitreDevis.Text;
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Affichage()
        {
            //Dim sTypeAffichage          As String
            //
            //sTypeAffichage = GetSetting(cFrNomApp, cUserDocDevis, "TypeAffichage", "1")
            //
            //'=== affichage = 1 pour l'affichage du devis en mode normal.
            //'=== affichage = 2 pour l'affichage du devis en mode synthése.
            //
            //'=== cache de maniére permanente l'onglet 6(Divers).
            //SSTab1.TabVisible(6) = False
            //
            //SSTab1.TabsPerRow = 7
            //SSTab1.Tab = 0
            //
            //If sTypeAffichage = "1" Then
            //    SSTab1.TabVisible(0) = True
            //    SSTab1.TabVisible(1) = True
            //    SSTab1.TabVisible(2) = True
            //    SSTab1.TabVisible(3) = True
            //    SSTab1.TabVisible(4) = True
            //    SSTab1.TabVisible(5) = True
            //    SSTab1.TabVisible(7) = True
            //
            //    SSTab1.TabVisible(8) = False
            //
            //ElseIf sTypeAffichage = "2" Then
            //    SSTab1.TabVisible(0) = False
            //    SSTab1.TabVisible(1) = False
            //    SSTab1.TabVisible(2) = False
            //    SSTab1.TabVisible(3) = False
            //    SSTab1.TabVisible(4) = False
            //    SSTab1.TabVisible(5) = False
            //    SSTab1.TabVisible(7) = False
            //
            //    SSTab1.TabVisible(8) = True
            //End If



        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        /// <param name="blFisheStandard"></param>
        private void fc_ChargeEnregistrement(string sCode, bool blFisheStandard = false)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"fc_ChargeEnregistrement() - Enter In The Function - sCode = {sCode} - blFisheStandard = {blFisheStandard}");
            //===> Fin Modif Mondir

            string sCodeImmeuble = null;
            string sCodeDevis = null;
            string sContrat = null;
            bool sDossierIntervention = false;
            string sDateMaxi = null;

            try
            {
                sCodeDevis = General.getFrmReg(Variable.cUserDocDevis, "CodeDevis", "");

                General.sSQL = "";

                ///==============> Tested
                if (!string.IsNullOrEmpty(sCodeDevis))
                {
                    General.sSQL = "SELECT Numerodevis FROM DevisEntete where NumeroDevis ='" + sCodeDevis + "' order by NumeroDevis desc";
                }
                else
                {

                    if (blFisheStandard == true)
                    {
                        General.sSQL = "SELECT Numerodevis FROM DevisEntete where numfichestandard ='" + sCode + "' order by NumeroDevis desc";
                    }
                    else
                    {
                        General.sSQL = "SELECT Numerodevis FROM DevisEntete where NumeroDevis ='" + sCode + "'";
                    }

                }

                using (var tmpModAdo = new ModAdo())
                {
                    General.rstmp = tmpModAdo.fc_OpenRecordSet(General.sSQL);
                    if (General.rstmp.Rows.Count == 0)
                    {
                        //txtSelCodeimmeuble = fc_ADOlibelle("select codeimmeuble from gestionstandard where numfichestandard=" & sCode)
                        txtSelCodeimmeuble.Text = General.getFrmReg(Variable.cUserDocDevis, "Immeuble", "");
                        using (var tmpModAdo2 = new ModAdo())
                        {
                            sDateMaxi = tmpModAdo2.fc_ADOlibelle("SELECT MAX(Contrat.DateEffet) FROM Contrat WHERE Contrat.CodeImmeuble='" + txtSelCodeimmeuble.Text + "'");
                            sContrat = tmpModAdo2.fc_ADOlibelle("SELECT Contrat.NumContrat,Contrat.Avenant FROM Contrat WHERE Contrat.CodeImmeuble='" + txtSelCodeimmeuble.Text + "' AND Resiliee=0 AND DateEffet='" + sDateMaxi + "' ORDER BY Avenant ASC ");
                        }
                        //=== Vérification d'un contrat associé à l'immeuble avant la création du devis
                        if (string.IsNullOrEmpty(sContrat))
                        {
                            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention, il n'y a pas de contrat associé à l'immeuble." + "\n" + "Voulez-vous continuer à créer un devis pour cet immeuble ?", "Immeuble sans contrat", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                            {
                                if (ModMain.lLien >= 0)
                                {
                                    if (ModMain.lLien != 0)
                                        ModMain.lLien = ModMain.lLien - 1;
                                    //TODO : Mondir - Check This Line
                                    //ModMain.fc_Navigue(ref this, ref ModMain.stabLien[ModMain.lLien], ref true);
                                }
                                return;
                            }
                        }

                        using (var tmpModAdo2 = new ModAdo())
                            if (Convert.ToBoolean(tmpModAdo2.fc_ADOlibelle("SELECT P3 FROM Immeuble WHERE CodeImmeuble='" + txtSelCodeimmeuble.Text + "'")) == true)
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention, cet imeuble est soumis à un contrat de type P3.", "Devis sur immeuble P3", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            }

                        cmdAjouter_Click(cmdAjouter, new System.EventArgs());
                        Text10.Text = sCode;
                        //==No de fiche standard
                        Text10_KeyPress(Text10, new System.Windows.Forms.KeyPressEventArgs((char)13));
                        SSetat.Text = "00";
                        //== Code etat

                        Text121.Text = Convert.ToString(DateTime.Now);
                        valider();
                        //cmdMAJ_Click

                    }
                    ///==================> Tested
                    else
                    {
                        Text11.Text = General.rstmp.Rows[0]["numerodevis"] + "";
                        txtNoDevis.Text = Text11.Text;
                        Text10_KeyPress(Text11, new System.Windows.Forms.KeyPressEventArgs((char)(13)));
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "; fc_ChargeEnregistrement; ");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdAjouter_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAjouter_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {

                prcRAZDevis((true));
                gbAjout = true;

                cmdRecherche.Enabled = false;
                cmdAjouter.Enabled = false;
                cmdSupprimer.Enabled = false;
                cmdMAJ.Enabled = false;
                cmdVersion.Enabled = false;
                cmdRechercherDtandard.Enabled = true;

                var _with13 = Text11;
                _with13.Text = General.fncNumDevis(false, "", txtSelCodeimmeuble.Text);
                //.Enabled = False
                txtNoDevis.Text = _with13.Text;

                //Text1(0).Enabled = True
                Text115.Text = General.gsUtilisateur;
                var _with14 = SSTab1;
                _with14.SelectedTab = _with14.Tabs[0];
                //For i = 1 To .Tabs - 1
                //    .TabEnabled(i) = False
                //Next
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAjouter_Click");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="C"></param>
        private void RecFunction(Control C)
        {
            foreach (Control objcontrole_loopVariable in C.Controls)
            {
                if (objcontrole_loopVariable.HasChildren)
                    RecFunction(objcontrole_loopVariable);

                if (objcontrole_loopVariable is UltraGrid)
                {
                    sheridan.fc_DeleteRegGrille(Variable.cUserDocDevis, objcontrole_loopVariable as UltraGrid, false, true);
                    //sheridan.fc_LoadDimensionGrille(this.Name, objcontrole_loopVariable as UltraGrid);
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="C"></param>
        private void RecFunction2(Control C)
        {
            foreach (Control objcontrole_loopVariable in C.Controls)
            {
                if (objcontrole_loopVariable.HasChildren)
                    RecFunction2(objcontrole_loopVariable);

                if (objcontrole_loopVariable is UltraGrid)
                {
                    sheridan.fc_LoadDimensionGrille(this.Name, objcontrole_loopVariable as UltraGrid);
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCoefDevis_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdCoefDevis_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            //===> Mondir le 12.03.2021 changer le mot de pass, demanadé oar Fréd
            //sT = Microsoft.VisualBasic.Interaction.InputBox("Veuillez saisir le mot de passe", "Mot de passe");

            var aut = ModAutorisation.fc_DroitDetail("UserDocDevis-cmdCoefDevis", 2);
            if (aut == 0)
                return;
            //===> Fin Modif Mondir

            frmCoefDevis2 frmCoefDevis2 = new frmCoefDevis2();
            frmCoefDevis2.ShowDialog();

            //===> Mondir le 12.03.2021, code bellow is commented, code above is used, Demandé par Fréd
            //var sMdp = Microsoft.VisualBasic.Interaction.InputBox("Mot de passe incorrect. Veuillez saisir un mot de passe.", "Mot de passe");
            //if (sMdp != cMdpCeof)
            //{
            //    sMdp = Microsoft.VisualBasic.Interaction.InputBox("Mot de passe incorrect. Veuillez saisir un mot de passe.", "Mot de passe");
            //    if (sMdp != cMdpCeof)
            //    {
            //        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Navigation annulée, vous n'avez pas les autorisations nécessaires pour accéder à cette fiche.", "Navigation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
            //    }
            //    else
            //    {
            //        //TODO : Mondir - Must Develope frmCoefDevis2
            //        frmCoefDevis2 frmCoefDevis2 = new frmCoefDevis2();
            //        frmCoefDevis2.ShowDialog();
            //    }
            //}
            //else
            //{
            //    //TODO : Mondir - Must Develope frmCoefDevis2
            //    frmCoefDevis2 frmCoefDevis2 = new frmCoefDevis2();
            //    frmCoefDevis2.ShowDialog();
            //}
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFournitArtDetail_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"GridFournitArtDetail_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir

            GridFournitArtDetail.DisplayLayout.Bands[0].Columns["Unite"].ValueList = cmbUniteFournit;
            e.Layout.Bands[0].Columns["CodeArticle"].Hidden = true;
            e.Layout.Bands[0].Columns["NoLigne"].Hidden = true;
            e.Layout.Bands[0].Columns["CodeChrono"].Header.Caption = "Code Chrono";
            e.Layout.Bands[0].Columns["CodeChrono"].MaxLength = 15;
            e.Layout.Bands[0].Columns["Designation"].Header.Caption = "Intitulé";
            e.Layout.Bands[0].Columns["Designation"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Qte"].Header.Caption = "Quantite";
            e.Layout.Bands[0].Columns["Unite"].MaxLength = 3;
            //e.Layout.Bands[0].Columns["NumLigne"].Header.Caption = "N°Ligne";
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridFacArticle_AfterRowActivate(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridFacArticle_AfterRowActivate() - Enter In The Function");
            //===> Fin Modif Mondir

            int i = 0;

            //    If ssGridFacArticle.Columns("CodeArticle").value = "" Then
            //        If ssGridFacArticle.Rows > 0 Then
            //            ssGridFacArticle.MoveFirst
            //        End If
            //    End If

            if (string.IsNullOrEmpty(ssGridFacArticle.ActiveRow.Cells["CodeArticle"].Value.ToString()))
            {
                GridFournitArtDetail.DisplayLayout.Bands[0].Override.AllowAddNew = AllowAddNew.No;
            }
            else
            {
                GridFournitArtDetail.DisplayLayout.Bands[0].Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
            }


            if (GridFournitArtDetail.ActiveRow == null || ssGridFacArticle.ActiveRow.Cells["CodeArticle"].Value != GridFournitArtDetail.ActiveRow.Cells["CodeArticle"].Value)
            {
                fc_GridFournitArtDetail((ssGridFacArticle.ActiveRow.Cells["CodeArticle"].Value.ToString()));
                ssGridFacArticle.Focus();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridSousFamilleArticleDetail_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"gridSousFamilleArticleDetail_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir

            gridSousFamilleArticleDetail.DisplayLayout.Bands[0].Columns["Unite"].ValueList = cmbUniteSousArticle;
            e.Layout.Bands[0].Columns["CodeSousFamille"].Hidden = true;
            e.Layout.Bands[0].Columns["CodeSousFamille"].Header.Caption = "Code poste";
            e.Layout.Bands[0].Columns["CodeSousFamille"].MaxLength = 50;
            e.Layout.Bands[0].Columns["NumLigne"].Header.Caption = "N°Ligne";
            e.Layout.Bands[0].Columns["CodeArticle"].Header.Caption = "Code Article";
            e.Layout.Bands[0].Columns["CodeArticle"].MaxLength = 50;
            e.Layout.Bands[0].Columns["CodeSousArticle"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Unite"].MaxLength = 3;
            e.Layout.Bands[0].Columns["IntituleArticle"].Header.Caption = "Intitulé";
            e.Layout.Bands[0].Columns["IntituleArticle"].MaxLength = 255;
        }

        private void GridFournitArtDetail_BeforeExitEditMode(object sender, BeforeExitEditModeEventArgs e)
        {

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFournitArtDetail_AfterExitEditMode(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"GridFournitArtDetail_AfterExitEditMode() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                if (boolEntree == false && GridFournitArtDetail.ActiveRow.Cells["CodeChrono"].Value.ToString() != "80000")
                {
                    //---Champs code famille
                    if (GridFournitArtDetail.ActiveCell.Column.Key.ToUpper() == "CodeChrono".ToUpper())
                    {
                        if (GridFournitArtDetail.ActiveCell.DataChanged)
                            sub_RechFournit(GridFournitArtDetail.ActiveCell.Value.ToString());
                    }
                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";gridSousFamilleArticleDetail_AfterColUpdate");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridSousFamilleArticleDetail_AfterExitEditMode(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"gridSousFamilleArticleDetail_AfterExitEditMode() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                if (boolEntree == false)
                {
                    //---Champs code famille
                    if (gridSousFamilleArticleDetail.ActiveCell.Column.Key.ToUpper() == "CodeArticle".ToUpper())
                    {
                        if (gridSousFamilleArticleDetail.ActiveCell.DataChanged)
                            sub_RechArticle(gridSousFamilleArticleDetail.ActiveRow.Cells["CodeArticle"].Value.ToString());
                        return;
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";gridSousFamilleArticleDetail_AfterColUpdate");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridSousFamilleArticle_AfterRowActivate(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"GridSousFamilleArticle_AfterRowActivate() - Enter In The Function");
            //===> Fin Modif Mondir

            int i = 0;
            if (string.IsNullOrEmpty(GridSousFamilleArticle.ActiveRow.Cells["CodeSousFamille"].Value.ToString()))
            {
                gridSousFamilleArticleDetail.DisplayLayout.Override.AllowAddNew = AllowAddNew.No;
            }
            else
            {
                gridSousFamilleArticleDetail.DisplayLayout.Override.AllowAddNew = AllowAddNew.TemplateOnBottom;
            }
            if (gridSousFamilleArticleDetail.ActiveRow == null ||
                GridSousFamilleArticle.ActiveRow.Cells["CodeSousFamille"].Value != gridSousFamilleArticleDetail.ActiveRow.Cells["CodeSousFamille"].Value)
            {
                fc_GridSousFamilleArticle(GridSousFamilleArticle.ActiveRow.Cells["CodeSousFamille"].Value.ToString());
            }
            GridSousFamilleArticle.Focus();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSTab1_SelectedTabChanged(object sender, Infragistics.Win.UltraWinTabControl.SelectedTabChangedEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSTab1_SelectedTabChanged() - Enter In The Function - e.Tab.Index = {e.Tab.Index}");
            //===> Fin Modif Mondir

            try
            {
                var _with81 = this;
                switch (e.Tab.Index)
                {

                    case 1:
                        break;
                    //Text1_KeyPress (1), (13)
                    case 2:
                        //calcul du total
                        prcCalculTotalAvantCoef((Text11.Text));
                        break;
                    case 3:
                        break;

                    ///=================> Tested
                    case cOngletPoste:
                        FC_ChargePoste();
                        break;
                    ///================> Tested
                    case cOngletDivers:
                        fc_OngletDivers();
                        break;
                    ////==============> Tested
                    case cOngletArticle:
                        fc_LoadFacArticle();
                        break;
                    ////=================> Tested
                    case cOngletPlan:

                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                        prcCalculTotalAvantCoef(Text11.Text);

                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                        fc_SyntheseDev(Text11.Text);

                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                        break;
                }

                //---valide les onglets precedents
                switch (SSTab1.SelectedTab?.Index)
                {
                    case cOngletPoste:
                        _with81.GridSousFamilleArticle.UpdateData();
                        _with81.gridSousFamilleArticleDetail.UpdateData();
                        break;
                    case cOngletDivers:
                        _with81.ssEnTete.UpdateData();
                        _with81.SStypeReglement.UpdateData();
                        _with81.ssGridDefaut.UpdateData();
                        break;
                    case cOngletArticle:
                        _with81.ssGridFacArticle.UpdateData();
                        break;
                }
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";SSTab1_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGridCorps_BeforeCellUpdate(object sender, BeforeCellUpdateEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSDBGridCorps_BeforeCellUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            //    If ColIndex = SSDBGridCorps.Columns("Quantite").Position Then
            //        Debug.Print "BeforeColUpdate : " && SSDBGridCorps.Columns("Quantite").value
            //        blnColQteModif = True
            //        dblQte(0) = SSDBGridCorps.Row
            //        dblQte(1) = CDbl(nz(SSDBGridCorps.Columns("Quantite").value, "1"))
            //    Else
            //        blnColQteModif = False
            //    End If


            //=== modif du 07 03 2018.
            //===controle si les données sont de type numérique.
            //if (SSDBGridCorps.ActiveCell == null || SSDBGridCorps.ActiveCell.DataChanged)
            //    return;
            if (SSDBGridCorps.ActiveCell == null) return;
            if (SSDBGridCorps.ActiveCell.Column.Index == 2 || SSDBGridCorps.ActiveCell.Column.Index == 3 || SSDBGridCorps.ActiveCell.Column.Index == 4 || SSDBGridCorps.ActiveCell.Column.Index == 5)
            {
                strOldValue = SSDBGridCorps.ActiveCell.Value.ToString();
                return;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGridCorps_DoubleClickCell(object sender, DoubleClickCellEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSDBGridCorps_DoubleClickCell() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                var _with54 = SSDBGridCorps;
                var originalRow = _with54.ActiveRow.Index;

                var DataSource = _with54.DataSource as DataTable;
                if (e.Cell.Column.Index == 1)
                {
                    if (_with54.ActiveRow.IsAddRow)
                    {
                        DataSource.Rows.Add(DataSource.NewRow());
                    }
                    else
                    {
                        // _with54.ActiveRow.Index+1 to fix issue #1147 on mantishub,Add new line after the current row 
                        DataSource.Rows.InsertAt(DataSource.NewRow(), _with54.ActiveRow.Index);
                    }
                    _with54.DataBind();

                    SSDBGridCorps.Rows.Refresh(RefreshRow.FireInitializeRow);

                    SSDBGridCorps.Rows[originalRow].Activate();
                    SSDBGridCorps.ActiveRow.Cells[1].Activate();
                    SSDBGridCorps.PerformAction(Infragistics.Win.UltraWinGrid.UltraGridAction.EnterEditMode);

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSDBGridCorps_DblClick");
            }
        }

        private void GridSousFamilleArticle_BeforeCellUpdate(object sender, BeforeCellUpdateEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"GridSousFamilleArticle_BeforeCellUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            //General.sSQL = "SELECT  SousFamilleArticle.CodeSousFamille, SousFamilleArticle.IntituleSousFamille";
            //General.sSQL = General.sSQL + " FROM SousFamilleArticle ";

            if (e.Cell.Column.Key.ToUpper() != "CodeSousFamille".ToUpper())
                return;

            if (e.Cell.DataChanged)
            {
                if (e.Cell.OriginalValue.ToString() != e.Cell.Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var Count = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM SousFamilleArticle WHERE CodeSousFamille = '{e.Cell.Text}'"));
                        if (Count != 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà.", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Cell.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridSousFamilleArticleDetail_AfterRowUpdate(object sender, RowEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"gridSousFamilleArticleDetail_AfterRowUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            var xx = modAdorsSousFamilleArticleDetail.Update();
        }

        private void gridSousFamilleArticleDetail_BeforeCellUpdate(object sender, BeforeCellUpdateEventArgs e)
        {
            //General.sSQL = "SELECT SousFamilleArticleDetail.CodeSousFamille," + " SousFamilleArticleDetail.NumLigne, SousFamilleArticleDetail.CodeArticle," +
            //   " SousFamilleArticleDetail.CodeSousArticle," + " SousFamilleArticleDetail.IntituleArticle," + " SousFamilleArticleDetail.Quantite,SousFamilleArticleDetail.Unite, " +
            //   "SousFamilleArticleDetail.Prix" + " FROM SousFamilleArticleDetail " + " where CodeSousFamille='" + sCode + "'";
            //General.sSQL = General.sSQL + " order by NumLigne";


        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridSousFamilleArticleDetail_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"gridSousFamilleArticleDetail_BeforeRowUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            if (e.Row.Cells["CodeSousFamille"].Value == DBNull.Value)
            {
                var manager = gridSousFamilleArticleDetail.EventManager;
                manager.AllEventsEnabled = false;
                e.Row.Cells["CodeSousFamille"].Value = GridSousFamilleArticle.ActiveRow.Cells["CodeSousFamille"].Value;
                manager.AllEventsEnabled = true;
            }

            if (e.Row.Cells["NumLigne"].DataChanged || e.Row.Cells["CodeArticle"].DataChanged)
            {
                if (e.Row.Cells["NumLigne"].OriginalValue.ToString() != e.Row.Cells["NumLigne"].Text || e.Row.Cells["CodeArticle"].OriginalValue.ToString() != e.Row.Cells["CodeArticle"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var Count = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM SousFamilleArticleDetail" +
                            $" WHERE NumLigne = '{e.Row.Cells["NumLigne"].Text}' AND CodeArticle = '{e.Row.Cells["CodeArticle"].Text}'"));
                        if (Count != 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà.", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void gridSousFamilleArticleDetail_AfterRowInsert(object sender, RowEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"gridSousFamilleArticleDetail_AfterRowInsert() - Enter In The Function");
            //===> Fin Modif Mondir

            if (e.Row.IsAddRow)
                e.Row.Cells["NumLigne"].Value = gridSousFamilleArticleDetail.Rows.Count == 0 ? 1 : gridSousFamilleArticleDetail.Rows.Count + 1;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridFacArticle_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridFacArticle_BeforeRowUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            //General.sSQL = "SELECT FacArticle.CodeArticle, FacArticle.Designation1,";
            //General.sSQL = General.sSQL + " FacArticle.PrixAchat, FacArticle.MOtps, FacArticle.FO,";
            //General.sSQL = General.sSQL + " FacArticle.ST, FacArticle.CodeCategorieArticle,";
            //General.sSQL = General.sSQL + " FacArticle.Designation2, FacArticle.Designation3,";
            //General.sSQL = General.sSQL + " FacArticle.Designation4, FacArticle.Designation5,";
            //General.sSQL = General.sSQL + " FacArticle.Designation6, FacArticle.Designation7,";
            //General.sSQL = General.sSQL + " FacArticle.Designation8, FacArticle.Designation9,";

            if (e.Row.Cells["CodeArticle"].DataChanged)
            {
                if (e.Row.Cells["CodeArticle"].OriginalValue.ToString() != e.Row.Cells["CodeArticle"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var Count = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM FacArticle" +
                            $" WHERE CodeArticle = '{e.Row.Cells["CodeArticle"].Text}'"));
                        if (Count != 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà.", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }


        private void cmdConcurrent_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdConcurrent_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            frmConccurents frm = new frmConccurents();
            frm.txtNoDevis.Text = Text11.Text;//ajouter par Salma
            frm.ShowDialog();
        }

        private void cmdExporter_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdExporter_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            frmExport frm = new frmExport();

            if (Dossier.fc_ControleDossier("C:\\temp") == false)
            {
                Dossier.fc_CreateDossier("C:\\temp");
            }

            frm.txtDossier.Text = "C:\\temp\\";
            frm.txtNomFic.Text = "Devis-" + txtNoDevis.Text.Replace("/", "-");
            frm.txtNoDevis.Text = txtNoDevis.Text;

            if (OptDescriptif.Checked)
            {
                frm.txtParametre.Text = "Descriptif";
                frm.txtMasqueSomme.Text = "M";
            }
            else if (optForfaitairePos.Checked)
            {
                frm.txtParametre.Text = "";
                frm.txtMasqueSomme.Text = "";
            }
            else if (optQuantitatif.Checked)
            {
                frm.txtParametre.Text = "";
                frm.txtMasqueSomme.Text = "Q";
            }
            else if (optForfaitaireAff.Checked)
            {
                //   '.txtParametre.Text = "ForfaitAffaire"
                //'=== modif du 21 11 2012.
                frm.txtParametre.Text = "";
                frm.txtMasqueSomme.Text = "M";

            }

            frm.ShowDialog();

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFournitArtDetail_BeforeRowUpdate(object sender, CancelableRowEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"GridFournitArtDetail_BeforeRowUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            //General.sSQL = "SELECT DEV_FournitArticleDetail.CodeArticle,";
            //General.sSQL = General.sSQL + " DEV_FournitArticleDetail.CodeChrono, DEV_FournitArticleDetail.Designation, DEV_FournitArticleDetail.Qte, DEV_FournitArticleDetail.Unite, DEV_FournitArticleDetail.NoLigne ";
            //General.sSQL = General.sSQL + " FROM DEV_FournitArticleDetail ";
            //General.sSQL = General.sSQL + " WHERE DEV_FournitArticleDetail.CodeArticle='" + sCode + "'";
            //General.sSQL = General.sSQL + " ORDER BY DEV_FournitArticleDetail.NoLigne";

            if (e.Row.Cells["CodeChrono"].Value == DBNull.Value)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Veuillez renseigner la colonne Code Chrono", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
                e.Row.CancelUpdate();
                return;
            }

            if (e.Row.Cells["CodeArticle"].Value == DBNull.Value)
            {
                var manager = GridFournitArtDetail.EventManager;
                manager.AllEventsEnabled = false;
                e.Row.Cells["CodeArticle"].Value = ssGridFacArticle.ActiveRow.Cells["CodeArticle"].Value;
                manager.AllEventsEnabled = true;
            }

            if (e.Row.Cells["CodeChrono"].DataChanged || e.Row.Cells["CodeArticle"].DataChanged)
            {
                if (e.Row.Cells["CodeChrono"].OriginalValue.ToString() != e.Row.Cells["CodeChrono"].Text || e.Row.Cells["CodeArticle"].OriginalValue.ToString() != e.Row.Cells["CodeArticle"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var Count = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM DEV_FournitArticleDetail WHERE CodeChrono = '{e.Row.Cells["CodeChrono"].Text}' " +
                            $"AND  CodeArticle = '{e.Row.Cells["CodeArticle"].Text}'"));
                        if (Count != 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà.", "Données érronées", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridFournitArtDetail_AfterRowUpdate(object sender, RowEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"GridFournitArtDetail_AfterRowUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            var xx = modAdorsFournitArticleDetail.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssEnTete_AfterRowUpdate(object sender, RowEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssEnTete_AfterRowUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            var xx = modAdoAdodc6.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SStypeReglement_AfterRowUpdate(object sender, RowEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SStypeReglement_AfterRowUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            var xx = modAdoAdodc7.Update();
        }

        private void ssEnTete_AfterRowsDeleted(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssEnTete_AfterRowsDeleted() - Enter In The Function");
            //===> Fin Modif Mondir

            var xx = modAdoAdodc6.Update();
        }

        private void ssGridDefaut_AfterRowUpdate(object sender, RowEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridDefaut_AfterRowUpdate() - Enter In The Function");
            //===> Fin Modif Mondir

            var xx = modAdoAdodc5.Update();
        }

        private void GridSousFamilleArticle_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"GridSousFamilleArticle_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir

            //SELECT  SousFamilleArticle.CodeSousFamille, SousFamilleArticle.IntituleSousFamille"
            e.Layout.Bands[0].Columns["CodeSousFamille"].Header.Caption = "Code";
            e.Layout.Bands[0].Columns["CodeSousFamille"].MaxLength = 20;
            e.Layout.Bands[0].Columns["IntituleSousFamille"].Header.Caption = "Libelle";
            e.Layout.Bands[0].Columns["IntituleSousFamille"].MaxLength = 50;
        }

        private void ssGridFacArticle_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridFacArticle_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir

            e.Layout.Bands[0].Columns["CodeArticle"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Designation1"].MaxLength = 350;
            e.Layout.Bands[0].Columns["CodeCategorieArticle"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Designation2"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Designation3"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Designation4"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Designation5"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Designation6"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Designation7"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Designation8"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Designation9"].MaxLength = 50;
            e.Layout.Bands[0].Columns["Designation10"].MaxLength = 50;
            e.Layout.Bands[0].Columns["PrixAchat"].Hidden = true;
            e.Layout.Bands[0].Columns["CodeCategorieArticle"].Hidden = true;
        }

        private void ssEnTete_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssEnTete_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir

            e.Layout.Bands[0].Columns["Code"].Hidden = true;
            e.Layout.Bands[0].Columns["Utilisateur"].Hidden = true;
            e.Layout.Bands[0].Columns["Libelle"].Header.Caption = "Code";
            e.Layout.Bands[0].Columns["Libelle"].MaxLength = 50;
        }

        private void SStypeReglement_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SStypeReglement_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir

            e.Layout.Bands[0].Columns["CodeReglement"].Hidden = true;
            e.Layout.Bands[0].Columns["LibelleReglement"].Header.Caption = "Code";
            e.Layout.Bands[0].Columns["LibelleReglement"].MaxLength = 50;
            e.Layout.Bands[0].Columns["TexteReglement"].Header.Caption = "Texte";
        }

        private void ssGridDefaut_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssGridDefaut_InitializeLayout() - Enter In The Function");
            //===> Fin Modif Mondir

            e.Layout.Bands[0].Columns["noauto"].Hidden = true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnalytique_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAnalytique_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!string.IsNullOrEmpty(txtAppelValid.Text))
            {
                // stocke la position de la fiche devis.
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, Text11.Text);
                General.saveInReg(Variable.cUserDocAnalytique2, "NewVar", txtAppelValid.Text);
                General.saveInReg(Variable.cUserDocAnalytique2, "CodeImmeuble", "");
                General.saveInReg(Variable.cUserDocAnalytique2, "Gerant", "");
                View.Theme.Theme.Navigate(typeof(UserDocAnalytique2));
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Navigation annulée, aucune intervention n'a été réalisée", "Navigation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LienGoFicheAppel_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"LienGoFicheAppel_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            if (valider())
            {
                if (Text10.Text != "")
                {
                    //stocke la position de la fiche devis.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, Text11.Text);
                    //stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocStandard, Text10.Text);
                }
                View.Theme.Theme.Navigate(typeof(UserDocStandard));
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LienGoGerant_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"LienGoGerant_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            if (valider())
            {
                if (Text11.Text != "")
                {
                    //stocke la position de la fiche devis.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, Text11.Text);
                    //stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, Text11x.Text);
                }
                View.Theme.Theme.Navigate(typeof(UserDocClient));
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAppliqueCoef_Click_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAppliqueCoef_Click_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            long nbRecAff = 0;
            int i;
            double dblOldValue;

            string sqlUpdate = "";
            string sqlWhere = "";
            string sqlValue = "";

            try
            {
                if (chkMO.Checked == false && chkFO.Checked == false && chkST.Checked == false && chkTVA.Checked == false && chkPrixH.Checked == false)
                {
                    CustomMessageBox.Show("Vous n'avez pas indiquer le(s) coefficient(s) à mettre à jour.", "Mise à jour coefficient", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    chkMO.ForeColor = ColorTranslator.FromOle(0xFF);
                    chkFO.ForeColor = ColorTranslator.FromOle(0xFF);
                    chkST.ForeColor = ColorTranslator.FromOle(0xFF);
                    chkTVA.ForeColor = ColorTranslator.FromOle(0xFF);
                    chkPrixH.ForeColor = ColorTranslator.FromOle(0xFF);
                    return;
                }
                chkMO.ForeColor = ColorTranslator.FromOle(0x0000012);
                chkFO.ForeColor = ColorTranslator.FromOle(0x0000012);
                chkST.ForeColor = ColorTranslator.FromOle(0x0000012);
                chkTVA.ForeColor = ColorTranslator.FromOle(0x0000012);
                chkPrixH.ForeColor = ColorTranslator.FromOle(0x0000012);
                //'Si l'utilisateur n'enregistre pas le devis : pas de changement de coef.
                //'Si une erreur est survenue lors de la validation du devis : pas de changement de coef.

                if (CustomMessageBox.Show("Souhaitez-vous enregistrer le devis ?", "Appliquer les coefficients", MessageBoxButtons.YesNo, MessageBoxIcon.Question) != DialogResult.Yes)
                {
                    return;
                }
                // Adding this block to solve issue #1088 on mantishub
                else
                {
                    if (valider() == false)
                    {
                        CustomMessageBox.Show("Validation intérrompue : annulation du changement de coefficient", "Appliquer les coefficients", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                }

                if (SSDBGridCorps.Rows.Count > 0)
                {
                    SSDBGridCorps.UpdateData();

                    sqlUpdate = "UPDATE DevisDetail SET ";
                    sqlWhere = " WHERE DevisDetail.NumeroDevis='" + Text11.Text + "'";

                    if (chkMO.Checked)
                    {
                        sqlValue = " MO=Mod*" + General.nz(Text315.Text, "1").ToString().Replace(",", ".") + " , KMO=" + Text315.Text + " ";
                        var xx = General.Execute(sqlUpdate + sqlValue + sqlWhere + " AND Mohud>0 ");
                        xx = General.Execute(sqlUpdate + "TotalDebours=(COALESCE((Mod+Fod+STd),(Mod+Fod),(Mod+STd),Mod,Fod,STd)) " + sqlWhere + " AND Mohud>0 ");
                        xx = General.Execute(sqlUpdate + " TotalVente=(COALESCE((Mo+FO+ST),(Mo+FO),(MO+ST),(FO+ST),MO,FO,ST)) " + sqlWhere + " AND Mohud>0 ");
                        xx = General.Execute(sqlUpdate + " TotalVenteApresCoef=(COALESCE(((MO*Kmo)+(FO*Kfo)+(ST*Kst)),((MO*Kmo)+(FO*Kfo)),((MO*Kmo)+(ST*Kst)),(MO*Kmo),(FO*Kfo),(ST*Kst))) " + sqlWhere + " AND Mohud>0 ");
                    }

                    if (chkFO.Checked)
                    {
                        sqlValue = " FO=Fod*" + General.nz(Text314.Text, "1").ToString().Replace(",", ".") + ", KFO=" + General.nz(Text314.Text, "1").ToString().Replace(",", ".") + " ";
                        var xx = General.Execute(sqlUpdate + sqlValue + sqlWhere + " AND FOud>0 ");
                        xx = General.Execute(sqlUpdate + "TotalDebours=(COALESCE((Mod+Fod+STd),(Mod+Fod),(Mod+STd),Mod,Fod,STd)) " + sqlWhere + " AND FOud>0 ");
                        xx = General.Execute(sqlUpdate + " TotalVente=(COALESCE((Mo+FO+ST),(Mo+FO),(MO+ST),(FO+ST),MO,FO,ST)) " + sqlWhere + " AND FOud>0 ");
                        xx = General.Execute(sqlUpdate + " TotalVenteApresCoef=(COALESCE(((MO*Kmo)+(FO*Kfo)+(ST*Kst)),((MO*Kmo)+(FO*Kfo)),((MO*Kmo)+(ST*Kst)),(MO*Kmo),(FO*Kfo),(ST*Kst))) "
                            + sqlWhere + " AND Foud>0 ");
                    }

                    if (chkST.Checked)
                    {
                        sqlValue = " ST=Std*" + General.nz(Text313.Text, "1").ToString().Replace(",", ".") + ", KST=" + General.nz(Text313.Text, "1").ToString().Replace(",", ".") + " ";
                        var xx = General.Execute(sqlUpdate + sqlValue + sqlWhere + " AND STud>0 ");
                        xx = General.Execute(sqlUpdate + "TotalDebours=(COALESCE((Mod+Fod+STd),(Mod+Fod),(Mod+STd),Mod,Fod,STd)) " + sqlWhere + " AND STud>0 ");
                        xx = General.Execute(sqlUpdate + " TotalVente=(COALESCE((Mo+FO+ST),(Mo+FO),(MO+ST),(FO+ST),MO,FO,ST)) " + sqlWhere + " AND STud>0 ");
                        xx = General.Execute(sqlUpdate + " TotalVenteApresCoef=(COALESCE(((MO*Kmo)+(FO*Kfo)+(ST*Kst)),((MO*Kmo)+(FO*Kfo)),((MO*Kmo)+(ST*Kst)),(MO*Kmo),(FO*Kfo),(ST*Kst))) " + sqlWhere + " AND STud>0 ");
                    }

                    if (chkTVA.Checked)
                    {
                        txtTaux.Text = txtTaux.Text.Replace(",", ".");
                        Application.DoEvents();

                        //=== modif du 31/12/2011.
                        if (!General.IsNumeric(txtTaux.Text))
                        {
                            CustomMessageBox.Show("Vous devez saisir un taux valide dans le champ Taux de TVA par défaut");
                            return;
                        }

                        sqlValue = " CodeTVA=" + txtTaux.Text;
                        var xx = General.Execute(sqlUpdate + sqlValue + sqlWhere + " AND NOT CodeTVA IS NULL ");
                    }

                    if (chkPrixH.Checked)
                    {
                        sqlValue = " PxHeured =" + General.nz(Text316.Text, 0).ToString().Replace(",", ".");
                        var xx = General.Execute(sqlUpdate + sqlValue + sqlWhere + " AND Mohud>0 ");
                        //        'adocnn.Execute sqlUpdate & sqlValue & sqlWhere & " AND Mohud>0 ", nbRecAff
                        xx = General.Execute(sqlUpdate + " Mod=COALESCE((Mohd * PxHeured ),(Mohd * PxHeured )) " + sqlWhere + " AND Mohud>0 ");
                        xx = General.Execute(sqlUpdate + " Mo=COALESCE((Mod * kmo ),(Mod * kmo )) " + sqlWhere + " AND Mohud>0 ");
                        xx = General.Execute(sqlUpdate + " TotalDebours=(COALESCE((Mod+Fod+STd),(Mod+Fod),(Mod+STd),Mod,Fod,STd)) " + sqlWhere + " AND Mohud>0 ");
                        xx = General.Execute(sqlUpdate + " TotalVente=(COALESCE((Mo+FO+ST),(Mo+FO),(MO+ST),(FO+ST),MO,FO,ST)) " + sqlWhere + " AND Mohud>0 ");
                    }



                    FournisgrilleNoDevis();
                    valider();
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdEnteteDevis_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdEnteteDevis_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                string requete = "SELECT TypeEnteteDevis.Libelle AS \"Libelle\", TypeEnteteDevis.Texte AS \"Texte\", " +
                    " TypeEnteteDevis.Code AS \"Code\" " +
                    "FROM TypeEnteteDevis";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'une entête de devis" };
                fg.ugResultat.InitializeLayout += (se, ev) =>
                {
                    ev.Layout.Bands[0].Columns[0].Hidden = true;
                };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    SSOleDBCombo2.Text = fg.ugResultat.ActiveRow.Cells["texte"].Value.ToString();
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        SSOleDBCombo2.Text = fg.ugResultat.ActiveRow.Cells["texte"].Value.ToString();
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";cmdRecherche_Click");
            }
        }

        private void SSDBGridCorps_KeyDown(object sender, KeyEventArgs e)
        {
            //if (SSDBGridCorps.ActiveCell == null)
            //    return;
            ////  ' perform action needed to move cursor 

            //switch (e.KeyData)
            //{
            //    case Keys.Up:

            //        SSDBGridCorps.PerformAction(UltraGridAction.ExitEditMode, false, false);
            //        SSDBGridCorps.PerformAction(UltraGridAction.AboveCell, false, false);
            //        e.Handled = true;
            //        SSDBGridCorps.PerformAction(UltraGridAction.EnterEditMode, false, false);
            //        break;
            //    case Keys.Down:
            //        SSDBGridCorps.PerformAction(UltraGridAction.ExitEditMode, false, false);
            //        SSDBGridCorps.PerformAction(UltraGridAction.BelowCell, false, false);
            //        e.Handled = true;
            //        SSDBGridCorps.PerformAction(UltraGridAction.EnterEditMode, false, false);

            //        break;

            //    case Keys.Right:
            //        SSDBGridCorps.PerformAction(UltraGridAction.ExitEditMode, false, false);
            //        SSDBGridCorps.PerformAction(UltraGridAction.NextCellByTab, false, false);
            //        e.Handled = true;
            //        SSDBGridCorps.PerformAction(UltraGridAction.EnterEditMode, false, false);
            //        break;

            //    case Keys.Left:
            //        SSDBGridCorps.PerformAction(UltraGridAction.ExitEditMode, false, false);
            //        SSDBGridCorps.PerformAction(UltraGridAction.PrevCellByTab, false, false);
            //        e.Handled = true;
            //        SSDBGridCorps.PerformAction(UltraGridAction.EnterEditMode, false, false);
            //        break;               
            //    default:
            //        Console.WriteLine("Default case");
            //        break;
            //}


            //if (e.KeyData == Keys.Left || e.KeyData == Keys.Right)
            //{
            //    //var currentIndex = SSDBGridCorps.ActiveCell.Column.Header.VisiblePosition;

            //    if (e.KeyData == Keys.Left)
            //    {
            //        KeyLeft();
            //    }
            //    else if (e.KeyData == Keys.Right)
            //    {
            //        KeyRight();
            //    }

            //    //foreach (var cell in SSDBGridCorps.ActiveRow.Cells)
            //    //    if (cell.Column.Header.VisiblePosition == currentIndex /*&& cell.Column.Hidden == false*/)
            //    //    {
            //    //        SSDBGridCorps.ActiveCell = cell;
            //    //        break;
            //    //    }

            //    e.Handled = true;
            //}
        }

        private const int KEYEVENTF_EXTENDEDKEY = 1;
        private const int KEYEVENTF_KEYUP = 2;

        [DllImport("user32.dll")] static extern uint keybd_event(byte bVk, byte bScan, int dwFlags, int dwExtraInfo);
        public static void KeyLeft()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"KeyLeft() - Enter In The Function");
            //===> Fin Modif Mondir

            keybd_event((byte)Keys.ShiftKey, 0, 0, 0);
            keybd_event((byte)Keys.Tab, 0, 0, 0);
            keybd_event((byte)Keys.ShiftKey, 0, KEYEVENTF_KEYUP, 0);
            keybd_event((byte)Keys.Tab, 0, KEYEVENTF_KEYUP, 0);
        }

        public static void KeyRight()
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"KeyRight() - Enter In The Function");
            //===> Fin Modif Mondir

            keybd_event((byte)Keys.Tab, 0, 0, 0);
            keybd_event((byte)Keys.Tab, 0, KEYEVENTF_KEYUP, 0);
        }

        private void cmdRechercheCodeArticleArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdRechercheCodeArticleArticle_KeyPress() - Enter In The Function");
            //===> Fin Modif Mondir

            try
            {
                var tmpControl = sender as Button;
                var index = Convert.ToInt32(tmpControl.Tag);

                if (index == 0)
                {

                }
                else if (index == 1)
                {
                    if ((int)e.KeyChar == 13)
                    {
                        cmdRechercheCodeArticle_Click(cmdRechercheCodeArticle1, null);
                    }
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// tester
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdRestaurer_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"CmdRestaurer_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            string sSQL = "";
            // 'sSQL = "SELECT ApImprime FROM DevisCodeEtat WHERE Code='" & SSetat & "'"
            //'
            //'sSQL = fc_ADOlibelle(sSQL)
            //'
            //'If sSQL = "1" Then
            //'
            //'Else

            //===> Mondir le 15.01.2021 pour ajouter les modifs de la version V12.01.2021
            if (General.fncUserName().ToUpper() == "axeciel-user".ToUpper() ||
                General.fncUserName().ToUpper() == "axeciel-user2".ToUpper() ||
                General.fncUserName().ToUpper() == "mondir".ToUpper())
            {

            }
            else
            {
                if (!string.IsNullOrEmpty(txtDateImpression.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(
                        "le statut " + SSetat.Text + " n'est pas autorisé, celui-ci a été transmis au client(imprimé)",
                        "Sauvegarde du devis " + Text11.Text,
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
            }
            //===> Fin Modif Mondir

            bAppliqueVersion = false;
            FrmRestaureDevis frm = new FrmRestaureDevis(this);
            frm.txtNodevis.Text = Text11.Text;
            frm.txtCodeImmeuble.Text = Text14.Text;
            frm.ShowDialog();

            if (bAppliqueVersion == true)
            {
                FournisgrilleNoDevis();
            }
        }
        private void insereSousArticleV3(string strCodeArticle, ref int Index, bool boolAddrow, int Quantite)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"insereSousArticleV3() - Enter In The Function - strCodeArticle = {strCodeArticle} " +
                                        $"Index = {Index} - boolAddrow = {boolAddrow} - Quantite = {Quantite}");
            //===> Fin Modif Mondir

            string sNomBaseProd = null;
            string sNomBaseGecet = null;
            double dblPrixApresCoef = 0;
            try
            {
                sNomBaseProd = "[" + General.adocnn.Database + "].dbo.";
                sNomBaseGecet = "[" + ModParametre.adoGecet.Database + "].dbo.";

                ado3temp = new DataTable();

                General.SQL = "SELECT " + sNomBaseProd + "DEV_FournitArticleDetail.CodeChrono AS Chrono, "
                        + sNomBaseProd + "DEV_FournitArticleDetail.Designation, " + sNomBaseProd
                        + "DEV_FournitArticleDetail.Qte AS Quantite , ";


                //'==== modif du 17 11 2014, changmenet de l'unité devis.
                General.SQL = General.SQL + " " + sNomBaseGecet + "Article.DESIGNATION_ARTICLE AS LibelleGecet, "
                          + sNomBaseGecet + "ArticleTarif.PRX_DEVIS_NET_UD AS PrixApresCoef, "
                          + sNomBaseGecet + "ArticleTarif.CODE_UNITE_DEVIS AS [Unite] ";


                General.SQL = General.SQL + " FROM " + sNomBaseProd + "DEV_FournitArticleDetail ";
                General.SQL = General.SQL + " LEFT OUTER JOIN " + sNomBaseGecet + "Article ON ";


                General.SQL = General.SQL + sNomBaseProd + "DEV_FournitArticleDetail.CodeChrono=" + sNomBaseGecet + "Article.CHRONO_INITIAL ";


                General.SQL = General.SQL + " LEFT OUTER JOIN " + sNomBaseGecet + "ArticleTarif ON ";


                General.SQL = General.SQL + sNomBaseGecet + "Article.CHRONO_INITIAL=" + sNomBaseGecet + "ArticleTarif.CHRONO_INITIAL ";


                General.SQL = General.SQL + " WHERE " + sNomBaseProd + "DEV_FournitArticleDetail.CodeArticle='" + StdSQLchaine.gFr_DoublerQuote(strCodeArticle) + "' ";


                General.SQL = General.SQL + " AND (" + sNomBaseGecet + "ArticleTarif.PRX_DEVIS_NET_UD IN(";


                General.SQL = General.SQL + "SELECT MAX(" + sNomBaseGecet + "ArticleTarif.PRX_DEVIS_NET_UD) AS MaxPrix";


                General.SQL = General.SQL + " FROM " + sNomBaseProd + "DEV_FournitArticleDetail ";


                General.SQL = General.SQL + " LEFT OUTER JOIN " + sNomBaseGecet + "Article ON ";


                General.SQL = General.SQL + sNomBaseProd + "DEV_FournitArticleDetail.CodeChrono=" + sNomBaseGecet + "Article.CHRONO_INITIAL ";


                General.SQL = General.SQL + " LEFT OUTER JOIN " + sNomBaseGecet + "ArticleTarif ON ";


                General.SQL = General.SQL + sNomBaseGecet + "Article.CHRONO_INITIAL=" + sNomBaseGecet + "ArticleTarif.CHRONO_INITIAL ";


                General.SQL = General.SQL + " WHERE " + sNomBaseProd + "DEV_FournitArticleDetail.CodeArticle='" + strCodeArticle + "' ";


                General.SQL = General.SQL + " GROUP BY " + sNomBaseProd + "DEV_FournitArticleDetail.CodeChrono, " + sNomBaseProd
                        + "DEV_FournitArticleDetail.Qte, ";


                General.SQL = General.SQL + " " + sNomBaseGecet + "Article.DESIGNATION_ARTICLE) OR " + sNomBaseGecet + "ArticleTarif.PRX_DEVIS_NET_UD IS NULL)";


                General.SQL = General.SQL + " GROUP BY " + sNomBaseProd + "DEV_FournitArticleDetail.CodeChrono, " + sNomBaseProd
                    + "DEV_FournitArticleDetail.Designation, "
                    + sNomBaseProd + "DEV_FournitArticleDetail.Qte, ";



                // '==== modif du 17 11 2014, changmenet de l'unité devis.
                General.SQL = General.SQL + " " + sNomBaseGecet + "Article.DESIGNATION_ARTICLE, " + sNomBaseGecet + "ArticleTarif.PRX_DEVIS_NET_UD,"
                    + sNomBaseGecet + "ArticleTarif.CODE_UNITE_DEVIS, ";


                General.SQL = General.SQL + sNomBaseProd + "DEV_FournitArticleDetail.NoLigne ";
                General.SQL = General.SQL + " ORDER BY " + sNomBaseProd + "DEV_FournitArticleDetail.NoLigne ";
                modAdoado3temp = new ModAdo();
                ado3temp = modAdoado3temp.fc_OpenRecordSet(General.SQL);
                if (ado3temp.Rows.Count > 0)
                {
                    var SSDBGridCorpsSource = SSDBGridCorps.DataSource as DataTable;

                    foreach (DataRow Row in ado3temp.Rows)
                    {
                        var NewRow = SSDBGridCorpsSource.NewRow();

                        dblPrixApresCoef = Convert.ToDouble(General.nz(Row["PrixApresCoef"].ToString().Replace(",", ".").Replace(" ", ""), "0").ToString());
                        General.SQL = "";
                        //General.SQL = PrepareLigne("CodeSousArticle", SSDBGridCorps, StdSQLchaine.fc_CtrlCharDevis(Row["Chrono"] + ""), "");
                        NewRow["CodeSousArticle"] = StdSQLchaine.fc_CtrlCharDevis(Row["Chrono"] + "");

                        if (string.IsNullOrEmpty(General.nz(Row["LibelleGecet"], "").ToString()))
                        {
                            //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, Row["Designation"].ToString(), General.SQL);
                            NewRow["Texteligne"] = Row["Designation"] + "";
                        }
                        else
                        {
                            //General.SQL = PrepareLigne("Texteligne", SSDBGridCorps, Row["LibelleGecet"].ToString(), General.SQL);
                            NewRow["Texteligne"] = Row["LibelleGecet"] + "";
                        }

                        if (Row["Quantite"] != DBNull.Value)
                        {
                            //General.SQL = PrepareLigne("quantite", SSDBGridCorps, (Convert.ToDouble(Row["Quantite"]) * Quantite).ToString(), General.SQL);
                            NewRow["quantite"] = (Convert.ToDouble(Row["Quantite"]) * Quantite).ToString();
                        }
                        else
                        {
                            //General.SQL = PrepareLigne("quantite", SSDBGridCorps, (1 * Quantite).ToString(), General.SQL);
                            NewRow["quantite"] = (1 * Quantite).ToString();
                        }

                        //General.SQL = PrepareLigne("Unite", SSDBGridCorps, Row["Unite"].ToString(), General.SQL);
                        NewRow["Unite"] = Row["Unite"];
                        //General.SQL = PrepareLigne("pxHeured", SSDBGridCorps, Text316.Text, General.SQL);
                        NewRow["pxHeured"] = Text316.Text;
                        //General.SQL = PrepareLigne("FOud", SSDBGridCorps, dblPrixApresCoef.ToString(), General.SQL);
                        NewRow["FOud"] = dblPrixApresCoef;
                        //General.SQL = PrepareLigne("FOd", SSDBGridCorps, (dblPrixApresCoef * Convert.ToDouble(Row["Quantite"]) * Quantite).ToString(), General.SQL);
                        //=============> Mondir 01.04.2020 : Added this condition to check if the value is null we pass 0 else we convert its value
                        //                                   To Fix this bug : https://groupe-dt.mantishub.io/view.php?id=1750       
                        NewRow["FOd"] = dblPrixApresCoef * Convert.ToDouble(Row["Quantite"] != DBNull.Value ? Row["Quantite"] : 0) * Quantite;
                        //General.SQL = PrepareLigne("TotalDebours", SSDBGridCorps, (dblPrixApresCoef * Convert.ToDouble(Row["Quantite"]) * Quantite).ToString(), General.SQL);
                        //=============> Mondir 01.04.2020 : Added this condition to check if the value is null we pass 0 else we convert its value
                        //                                   To Fix this bug : https://groupe-dt.mantishub.io/view.php?id=1750     
                        NewRow["TotalDebours"] = dblPrixApresCoef * Convert.ToDouble(Row["Quantite"] != DBNull.Value ? Row["Quantite"] : 0) * Quantite;
                        //General.SQL = PrepareLigne("KMO", SSDBGridCorps, Text315.Text, General.SQL);
                        NewRow["KMO"] = Text315.Text;
                        //General.SQL = PrepareLigne("KFO", SSDBGridCorps, Text314.Text, General.SQL);
                        NewRow["KFO"] = Text314.Text;
                        //General.SQL = PrepareLigne("FO", SSDBGridCorps, (dblPrixApresCoef * Convert.ToDouble(Row["Quantite"]) * Convert.ToDouble(Text314.Text) * Quantite).ToString(), General.SQL);
                        //=============> Mondir 01.04.2020 : Added this condition to check if the value is null we pass 0 else we convert its value
                        //                                   To Fix this bug : https://groupe-dt.mantishub.io/view.php?id=1750     
                        NewRow["FO"] = dblPrixApresCoef * Convert.ToDouble(Row["Quantite"] != DBNull.Value ? Row["Quantite"] : 0) * Convert.ToDouble(Text314.Text) * Quantite;
                        //General.SQL = PrepareLigne("KST", SSDBGridCorps, Text313.Text, General.SQL);
                        NewRow["KST"] = Text313.Text;
                        //General.SQL = PrepareLigne("TotalVente", SSDBGridCorps, Convert.ToString(dblPrixApresCoef * Convert.ToDouble(Row["Quantite"]) * Convert.ToDouble(Text314.Text) * Quantite), General.SQL);
                        //=============> Mondir 01.04.2020 : Added this condition to check if the value is null we pass 0 else we convert its value
                        //                                   To Fix this bug : https://groupe-dt.mantishub.io/view.php?id=1750     
                        NewRow["TotalVente"] = dblPrixApresCoef * Convert.ToDouble(Row["Quantite"] != DBNull.Value ? Row["Quantite"] : 0) * Convert.ToDouble(Text314.Text) * Quantite;
                        //General.SQL = PrepareLigne("CodeTva", SSDBGridCorps, TauxTVADefaut, General.SQL);
                        NewRow["CodeTva"] = TauxTVADefaut;
                        //InsereLigneInGrille(General.SQL, boolAddrow, Index);
                        if (boolAddrow)
                            SSDBGridCorpsSource.Rows.Add(NewRow);
                        else
                            SSDBGridCorpsSource.Rows.InsertAt(NewRow, Index);
                        Index = Index + 1;
                    }
                }
                modAdoado3temp?.Dispose();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, ";insereSousArticle");
            }

        }

        private void label94_Click(object sender, EventArgs e)
        {

            var label = sender as Label;
            int index = Convert.ToInt32(label.Tag);

            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"label94_Click() - Enter In The Function - index = {index}");
            //===> Fin Modif Mondir

            if (index == 6)
            {
                ssGridFacArticle.DeleteSelectedRows();
            }
            else if (index == 7)
            {
                GridFournitArtDetail.DeleteSelectedRows();
            }
        }

        private void ssCombo1_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"ssCombo1_BeforeDropDown() - Enter In The Function");
            //===> Fin Modif Mondir

            string sSQL = "SELECT        MetierID, LibelleMetier"
                        + " From Metier ORDER BY MetierID";
            sheridan.InitialiseCombo(ssCombo1, sSQL, "MetierID");

        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text314_Validating(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Text314_Validating() - Enter In The Function");
            //===> Fin Modif Mondir

            // log DT-2020.06.19  Product Version : 0.9.6.6 - 17/06/2020 - Prod#
            //==> System.ArgumentException|Message : Le format de la chaîne d'entrée est incorrect.           
            //le champs DEV_CoefMOdefaut de la table DevisEntete est de type float 
            ////donc pour éviter ce bug, j'ai ajouter cette vérification avant de valider 

            if (!string.IsNullOrEmpty(Text314.Text) && !General.IsNumeric(Text314.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique", "Données erronées",
                          MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text314.Focus();
                return;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text315_Validating(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Text315_Validating() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!string.IsNullOrEmpty(Text315.Text) && !General.IsNumeric(Text315.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique", "Données erronées",
                          MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text315.Focus();
                return;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text313_Validating(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Text313_Validating() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!string.IsNullOrEmpty(Text313.Text) && !General.IsNumeric(Text313.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique", "Données erronées",
                          MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text313.Focus();
                return;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Text316_Validating(object sender, CancelEventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"Text316_Validating() - Enter In The Function");
            //===> Fin Modif Mondir

            if (!string.IsNullOrEmpty(Text316.Text) && !General.IsNumeric(Text316.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique", "Données erronées",
                          MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text316.Focus();
                return;
            }
        }

        /// <summary>
        /// Mondir le 01.07.2020, bouton demandé par Rachid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMaj2_Click_1(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdMaj2_Click_1() - Enter In The Function");
            //===> Fin Modif Mondir

            cmdMAJ_Click(cmdMAJ, new System.EventArgs());
        }

        /// <summary>
        /// Mondir le 01.07.2020, demandé par Rachid
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAgrandir_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdAgrandir_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            if (cmdAgrandir.Text.ToUpper() == cAgrandir.ToUpper())
            {
                groupBox22.Visible = false;
                groupBox24.Visible = false;
                groupBox21.Dock = DockStyle.Fill;
                flowLayoutPanel1.Visible = false;
                tableLayoutPanel103.RowStyles[0].Height = 0;
                if (!View.Theme.Theme.MainForm.menuCollapsed)
                    View.Theme.Theme.MainForm.PictureBoxSettings_Click(sender, e);
                cmdAgrandir.Text = cReduire;
            }
            else
            {
                groupBox21.Dock = DockStyle.None;
                groupBox22.Visible = true;
                groupBox24.Visible = true;
                flowLayoutPanel1.Visible = true;
                tableLayoutPanel103.RowStyles[0].Height = 40;
                if (View.Theme.Theme.MainForm.menuCollapsed)
                    View.Theme.Theme.MainForm.PictureBoxSettings_Click(sender, e);
                cmdAgrandir.Text = cAgrandir;
            }
        }

        private void SSetat_ValueChanged(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"SSetat_ValueChanged() - Enter In The Function - NoDevis = {Text11.Text} - New Value = {SSetat.Text}");
            //===> Fin Modif Mondir

        }

        private void tableLayoutPanel45_Paint(object sender, PaintEventArgs e)
        {

        }

        private void cmdStatutHisto_Click(object sender, EventArgs e)
        {
            //===> Mondir 20.11.2020, Ajout du LOG
            Program.SaveException(null, $"cmdStatutHisto_Click() - Enter In The Function");
            //===> Fin Modif Mondir

            var frmHistoStatutDevis = new frmHistoStatutDevis(Text11.Text);
            frmHistoStatutDevis.Show();
        }

        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        private void ssTYD_TypeDevis_ValueChanged(object sender, EventArgs e)
        {
            fc_MajTypeDevis();
        }

        //===> Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        private void fc_MajTypeDevis()
        {
            string sSQL = "";
            try
            {
                if (!string.IsNullOrEmpty(Text11.Text))
                {
                    if (!string.IsNullOrEmpty(ssTYD_TypeDevis.Text))
                    {
                        sSQL = "SELECT AUT_Nom From AUT_Autorisation "
                               + " WHERE    AUT_Nom = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'"
                               + " AND AUT_Formulaire = '" + StdSQLchaine.gFr_DoublerQuote(Name) + "'"
                               + " AND AUT_Objet = 'MajTypeDevis'";

                        using (var tmpModAdo = new ModAdo())
                            sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                        if (string.IsNullOrEmpty(sSQL))
                            return;

                        //===Controle si le type de devis existe.
                        sSQL = "SELECT        TYD_Code"
                               + " From TYD_TypeDevis WHERE   TYD_Code = '" +
                               StdSQLchaine.gFr_DoublerQuote(ssTYD_TypeDevis.Text) + "'";

                        using (var tmpModAdo = new ModAdo())
                            sSQL = tmpModAdo.fc_ADOlibelle(sSQL);

                        if (string.IsNullOrEmpty(sSQL))
                        {
                            CustomMessageBox.Show("le type de devis " + ssTYD_TypeDevis.Text + " n'existe pas."
                                + Environment.NewLine + "Veuillez le sélectionner dans la liste déroulante.", "Type de devis incorrect.", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        }
                        else
                        {
                            sSQL = "UPDATE DevisEnTete set TYD_Code ='" +
                                   StdSQLchaine.gFr_DoublerQuote(ssTYD_TypeDevis.Text) + "'" + " WHERE NumeroDevis ='" + StdSQLchaine.gFr_DoublerQuote(Text11.Text) + "'";
                            General.Execute(sSQL);
                        }
                    }
                }
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        private void tableLayoutPanel45_Paint_1(object sender, PaintEventArgs e)
        {

        }



        private void txtDeviseur_Validating(object sender, CancelEventArgs e)
        {

        }
    }
}