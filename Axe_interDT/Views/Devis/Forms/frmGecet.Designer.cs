﻿using Infragistics.Win.UltraWinGrid;

namespace Axe_interDT.Views.Devis.Forms
{
    partial class frmGecet
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.optEtouOU = new System.Windows.Forms.RadioButton();
            this.optEspace = new System.Windows.Forms.RadioButton();
            this.panel1 = new System.Windows.Forms.Panel();
            this.OptEt = new System.Windows.Forms.RadioButton();
            this.OptOu = new System.Windows.Forms.RadioButton();
            this.txtType = new iTalk.iTalk_TextBox_Small2();
            this.txtPxHeure = new iTalk.iTalk_TextBox_Small2();
            this.txtTVA = new iTalk.iTalk_TextBox_Small2();
            this.txtKST = new iTalk.iTalk_TextBox_Small2();
            this.txtKMO = new iTalk.iTalk_TextBox_Small2();
            this.txtKFO = new iTalk.iTalk_TextBox_Small2();
            this.txtCle = new iTalk.iTalk_TextBox_Small2();
            this.txtNumLigne = new iTalk.iTalk_TextBox_Small2();
            this.txtOrigine = new iTalk.iTalk_TextBox_Small2();
            this.Label11 = new System.Windows.Forms.Label();
            this.Label12 = new System.Windows.Forms.Label();
            this.Label13 = new System.Windows.Forms.Label();
            this.Label16 = new System.Windows.Forms.Label();
            this.Label15 = new System.Windows.Forms.Label();
            this.Label14 = new System.Windows.Forms.Label();
            this.Label10 = new System.Windows.Forms.Label();
            this.cmbFourn = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.Check1 = new System.Windows.Forms.CheckBox();
            this.chkTriPrix = new System.Windows.Forms.CheckBox();
            this.Check2 = new System.Windows.Forms.CheckBox();
            this.GridGecet = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cmdAppliqueClose = new System.Windows.Forms.Button();
            this.lblTotal = new System.Windows.Forms.Label();
            this.CmdPanier = new System.Windows.Forms.Button();
            this.cmdHelp = new System.Windows.Forms.Button();
            this.cmdClean = new System.Windows.Forms.Button();
            this.cmdRechercher = new System.Windows.Forms.Button();
            this.cmdAppliquer = new System.Windows.Forms.Button();
            this.txtMarque = new iTalk.iTalk_TextBox_Small2();
            this.txtRefFab = new iTalk.iTalk_TextBox_Small2();
            this.txtRefFourn = new iTalk.iTalk_TextBox_Small2();
            this.txtLibelle = new iTalk.iTalk_TextBox_Small2();
            this.txtFamille = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeChrono = new iTalk.iTalk_TextBox_Small2();
            this.Frame1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.panel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFourn)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridGecet)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.Transparent;
            this.Frame1.Controls.Add(this.panel2);
            this.Frame1.Controls.Add(this.panel1);
            this.Frame1.Controls.Add(this.txtType);
            this.Frame1.Controls.Add(this.txtPxHeure);
            this.Frame1.Controls.Add(this.txtTVA);
            this.Frame1.Controls.Add(this.txtKST);
            this.Frame1.Controls.Add(this.txtKMO);
            this.Frame1.Controls.Add(this.txtKFO);
            this.Frame1.Controls.Add(this.txtCle);
            this.Frame1.Controls.Add(this.txtNumLigne);
            this.Frame1.Controls.Add(this.txtOrigine);
            this.Frame1.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.Frame1.Location = new System.Drawing.Point(12, 12);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(949, 78);
            this.Frame1.TabIndex = 411;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "Sélectionnez la manière dont vous voulez faire vos recherches.";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.optEtouOU);
            this.panel2.Controls.Add(this.optEspace);
            this.panel2.Location = new System.Drawing.Point(6, 17);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(598, 58);
            this.panel2.TabIndex = 590;
            // 
            // optEtouOU
            // 
            this.optEtouOU.AutoSize = true;
            this.optEtouOU.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optEtouOU.Location = new System.Drawing.Point(10, 7);
            this.optEtouOU.Name = "optEtouOU";
            this.optEtouOU.Size = new System.Drawing.Size(257, 23);
            this.optEtouOU.TabIndex = 538;
            this.optEtouOU.Text = "Saisir ET ou OU entre chaque mot";
            this.optEtouOU.UseVisualStyleBackColor = true;
            this.optEtouOU.CheckedChanged += new System.EventHandler(this.optEtouOU_CheckedChanged);
            // 
            // optEspace
            // 
            this.optEspace.AutoSize = true;
            this.optEspace.Checked = true;
            this.optEspace.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.optEspace.Location = new System.Drawing.Point(10, 31);
            this.optEspace.Name = "optEspace";
            this.optEspace.Size = new System.Drawing.Size(588, 23);
            this.optEspace.TabIndex = 539;
            this.optEspace.TabStop = true;
            this.optEspace.Text = "Utiliser la touche espace, celle ci équivaut à ET ou OU selon votre choix ci aprè" +
    "s =>";
            this.optEspace.UseVisualStyleBackColor = true;
            this.optEspace.CheckedChanged += new System.EventHandler(this.optEspace_CheckedChanged);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.OptEt);
            this.panel1.Controls.Add(this.OptOu);
            this.panel1.Location = new System.Drawing.Point(610, 45);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(98, 30);
            this.panel1.TabIndex = 589;
            // 
            // OptEt
            // 
            this.OptEt.AutoSize = true;
            this.OptEt.Checked = true;
            this.OptEt.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.OptEt.Location = new System.Drawing.Point(4, 6);
            this.OptEt.Name = "OptEt";
            this.OptEt.Size = new System.Drawing.Size(40, 21);
            this.OptEt.TabIndex = 540;
            this.OptEt.TabStop = true;
            this.OptEt.Text = "ET";
            this.OptEt.UseVisualStyleBackColor = true;
            this.OptEt.CheckedChanged += new System.EventHandler(this.OptEt_CheckedChanged);
            // 
            // OptOu
            // 
            this.OptOu.AutoSize = true;
            this.OptOu.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.OptOu.Location = new System.Drawing.Point(50, 6);
            this.OptOu.Name = "OptOu";
            this.OptOu.Size = new System.Drawing.Size(45, 21);
            this.OptOu.TabIndex = 541;
            this.OptOu.Text = "OU";
            this.OptOu.UseVisualStyleBackColor = true;
            this.OptOu.CheckedChanged += new System.EventHandler(this.OptOu_CheckedChanged);
            // 
            // txtType
            // 
            this.txtType.AccAcceptNumbersOnly = false;
            this.txtType.AccAllowComma = false;
            this.txtType.AccBackgroundColor = System.Drawing.Color.White;
            this.txtType.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtType.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtType.AccHidenValue = "";
            this.txtType.AccNotAllowedChars = null;
            this.txtType.AccReadOnly = false;
            this.txtType.AccReadOnlyAllowDelete = false;
            this.txtType.AccRequired = false;
            this.txtType.BackColor = System.Drawing.Color.White;
            this.txtType.CustomBackColor = System.Drawing.Color.White;
            this.txtType.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtType.ForeColor = System.Drawing.Color.Black;
            this.txtType.Location = new System.Drawing.Point(794, 48);
            this.txtType.Margin = new System.Windows.Forms.Padding(2);
            this.txtType.MaxLength = 32767;
            this.txtType.Multiline = false;
            this.txtType.Name = "txtType";
            this.txtType.ReadOnly = false;
            this.txtType.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtType.Size = new System.Drawing.Size(24, 27);
            this.txtType.TabIndex = 588;
            this.txtType.Text = "0";
            this.txtType.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtType.UseSystemPasswordChar = false;
            this.txtType.Visible = false;
            // 
            // txtPxHeure
            // 
            this.txtPxHeure.AccAcceptNumbersOnly = false;
            this.txtPxHeure.AccAllowComma = false;
            this.txtPxHeure.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPxHeure.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPxHeure.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPxHeure.AccHidenValue = "";
            this.txtPxHeure.AccNotAllowedChars = null;
            this.txtPxHeure.AccReadOnly = false;
            this.txtPxHeure.AccReadOnlyAllowDelete = false;
            this.txtPxHeure.AccRequired = false;
            this.txtPxHeure.BackColor = System.Drawing.Color.White;
            this.txtPxHeure.CustomBackColor = System.Drawing.Color.White;
            this.txtPxHeure.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPxHeure.ForeColor = System.Drawing.Color.Black;
            this.txtPxHeure.Location = new System.Drawing.Point(766, 48);
            this.txtPxHeure.Margin = new System.Windows.Forms.Padding(2);
            this.txtPxHeure.MaxLength = 32767;
            this.txtPxHeure.Multiline = false;
            this.txtPxHeure.Name = "txtPxHeure";
            this.txtPxHeure.ReadOnly = false;
            this.txtPxHeure.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPxHeure.Size = new System.Drawing.Size(24, 27);
            this.txtPxHeure.TabIndex = 587;
            this.txtPxHeure.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPxHeure.UseSystemPasswordChar = false;
            this.txtPxHeure.Visible = false;
            // 
            // txtTVA
            // 
            this.txtTVA.AccAcceptNumbersOnly = false;
            this.txtTVA.AccAllowComma = false;
            this.txtTVA.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTVA.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTVA.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtTVA.AccHidenValue = "";
            this.txtTVA.AccNotAllowedChars = null;
            this.txtTVA.AccReadOnly = false;
            this.txtTVA.AccReadOnlyAllowDelete = false;
            this.txtTVA.AccRequired = false;
            this.txtTVA.BackColor = System.Drawing.Color.White;
            this.txtTVA.CustomBackColor = System.Drawing.Color.White;
            this.txtTVA.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtTVA.ForeColor = System.Drawing.Color.Black;
            this.txtTVA.Location = new System.Drawing.Point(738, 48);
            this.txtTVA.Margin = new System.Windows.Forms.Padding(2);
            this.txtTVA.MaxLength = 32767;
            this.txtTVA.Multiline = false;
            this.txtTVA.Name = "txtTVA";
            this.txtTVA.ReadOnly = false;
            this.txtTVA.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTVA.Size = new System.Drawing.Size(24, 27);
            this.txtTVA.TabIndex = 586;
            this.txtTVA.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTVA.UseSystemPasswordChar = false;
            this.txtTVA.Visible = false;
            // 
            // txtKST
            // 
            this.txtKST.AccAcceptNumbersOnly = false;
            this.txtKST.AccAllowComma = false;
            this.txtKST.AccBackgroundColor = System.Drawing.Color.White;
            this.txtKST.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtKST.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtKST.AccHidenValue = "";
            this.txtKST.AccNotAllowedChars = null;
            this.txtKST.AccReadOnly = false;
            this.txtKST.AccReadOnlyAllowDelete = false;
            this.txtKST.AccRequired = false;
            this.txtKST.BackColor = System.Drawing.Color.White;
            this.txtKST.CustomBackColor = System.Drawing.Color.White;
            this.txtKST.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtKST.ForeColor = System.Drawing.Color.Black;
            this.txtKST.Location = new System.Drawing.Point(850, 16);
            this.txtKST.Margin = new System.Windows.Forms.Padding(2);
            this.txtKST.MaxLength = 32767;
            this.txtKST.Multiline = false;
            this.txtKST.Name = "txtKST";
            this.txtKST.ReadOnly = false;
            this.txtKST.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtKST.Size = new System.Drawing.Size(24, 27);
            this.txtKST.TabIndex = 585;
            this.txtKST.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtKST.UseSystemPasswordChar = false;
            this.txtKST.Visible = false;
            // 
            // txtKMO
            // 
            this.txtKMO.AccAcceptNumbersOnly = false;
            this.txtKMO.AccAllowComma = false;
            this.txtKMO.AccBackgroundColor = System.Drawing.Color.White;
            this.txtKMO.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtKMO.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtKMO.AccHidenValue = "";
            this.txtKMO.AccNotAllowedChars = null;
            this.txtKMO.AccReadOnly = false;
            this.txtKMO.AccReadOnlyAllowDelete = false;
            this.txtKMO.AccRequired = false;
            this.txtKMO.BackColor = System.Drawing.Color.White;
            this.txtKMO.CustomBackColor = System.Drawing.Color.White;
            this.txtKMO.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtKMO.ForeColor = System.Drawing.Color.Black;
            this.txtKMO.Location = new System.Drawing.Point(822, 16);
            this.txtKMO.Margin = new System.Windows.Forms.Padding(2);
            this.txtKMO.MaxLength = 32767;
            this.txtKMO.Multiline = false;
            this.txtKMO.Name = "txtKMO";
            this.txtKMO.ReadOnly = false;
            this.txtKMO.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtKMO.Size = new System.Drawing.Size(24, 27);
            this.txtKMO.TabIndex = 584;
            this.txtKMO.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtKMO.UseSystemPasswordChar = false;
            this.txtKMO.Visible = false;
            // 
            // txtKFO
            // 
            this.txtKFO.AccAcceptNumbersOnly = false;
            this.txtKFO.AccAllowComma = false;
            this.txtKFO.AccBackgroundColor = System.Drawing.Color.White;
            this.txtKFO.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtKFO.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtKFO.AccHidenValue = "";
            this.txtKFO.AccNotAllowedChars = null;
            this.txtKFO.AccReadOnly = false;
            this.txtKFO.AccReadOnlyAllowDelete = false;
            this.txtKFO.AccRequired = false;
            this.txtKFO.BackColor = System.Drawing.Color.White;
            this.txtKFO.CustomBackColor = System.Drawing.Color.White;
            this.txtKFO.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtKFO.ForeColor = System.Drawing.Color.Black;
            this.txtKFO.Location = new System.Drawing.Point(794, 16);
            this.txtKFO.Margin = new System.Windows.Forms.Padding(2);
            this.txtKFO.MaxLength = 32767;
            this.txtKFO.Multiline = false;
            this.txtKFO.Name = "txtKFO";
            this.txtKFO.ReadOnly = false;
            this.txtKFO.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtKFO.Size = new System.Drawing.Size(24, 27);
            this.txtKFO.TabIndex = 583;
            this.txtKFO.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtKFO.UseSystemPasswordChar = false;
            this.txtKFO.Visible = false;
            // 
            // txtCle
            // 
            this.txtCle.AccAcceptNumbersOnly = false;
            this.txtCle.AccAllowComma = false;
            this.txtCle.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCle.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCle.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCle.AccHidenValue = "";
            this.txtCle.AccNotAllowedChars = null;
            this.txtCle.AccReadOnly = false;
            this.txtCle.AccReadOnlyAllowDelete = false;
            this.txtCle.AccRequired = false;
            this.txtCle.BackColor = System.Drawing.Color.White;
            this.txtCle.CustomBackColor = System.Drawing.Color.White;
            this.txtCle.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCle.ForeColor = System.Drawing.Color.Black;
            this.txtCle.Location = new System.Drawing.Point(732, 16);
            this.txtCle.Margin = new System.Windows.Forms.Padding(2);
            this.txtCle.MaxLength = 32767;
            this.txtCle.Multiline = false;
            this.txtCle.Name = "txtCle";
            this.txtCle.ReadOnly = false;
            this.txtCle.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCle.Size = new System.Drawing.Size(24, 27);
            this.txtCle.TabIndex = 582;
            this.txtCle.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCle.UseSystemPasswordChar = false;
            this.txtCle.Visible = false;
            // 
            // txtNumLigne
            // 
            this.txtNumLigne.AccAcceptNumbersOnly = false;
            this.txtNumLigne.AccAllowComma = false;
            this.txtNumLigne.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNumLigne.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNumLigne.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtNumLigne.AccHidenValue = "";
            this.txtNumLigne.AccNotAllowedChars = null;
            this.txtNumLigne.AccReadOnly = false;
            this.txtNumLigne.AccReadOnlyAllowDelete = false;
            this.txtNumLigne.AccRequired = false;
            this.txtNumLigne.BackColor = System.Drawing.Color.White;
            this.txtNumLigne.CustomBackColor = System.Drawing.Color.White;
            this.txtNumLigne.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNumLigne.ForeColor = System.Drawing.Color.Black;
            this.txtNumLigne.Location = new System.Drawing.Point(704, 16);
            this.txtNumLigne.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumLigne.MaxLength = 32767;
            this.txtNumLigne.Multiline = false;
            this.txtNumLigne.Name = "txtNumLigne";
            this.txtNumLigne.ReadOnly = false;
            this.txtNumLigne.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNumLigne.Size = new System.Drawing.Size(24, 27);
            this.txtNumLigne.TabIndex = 581;
            this.txtNumLigne.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNumLigne.UseSystemPasswordChar = false;
            this.txtNumLigne.Visible = false;
            // 
            // txtOrigine
            // 
            this.txtOrigine.AccAcceptNumbersOnly = false;
            this.txtOrigine.AccAllowComma = false;
            this.txtOrigine.AccBackgroundColor = System.Drawing.Color.White;
            this.txtOrigine.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtOrigine.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtOrigine.AccHidenValue = "";
            this.txtOrigine.AccNotAllowedChars = null;
            this.txtOrigine.AccReadOnly = false;
            this.txtOrigine.AccReadOnlyAllowDelete = false;
            this.txtOrigine.AccRequired = false;
            this.txtOrigine.BackColor = System.Drawing.Color.White;
            this.txtOrigine.CustomBackColor = System.Drawing.Color.White;
            this.txtOrigine.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtOrigine.ForeColor = System.Drawing.Color.Black;
            this.txtOrigine.Location = new System.Drawing.Point(676, 16);
            this.txtOrigine.Margin = new System.Windows.Forms.Padding(2);
            this.txtOrigine.MaxLength = 32767;
            this.txtOrigine.Multiline = false;
            this.txtOrigine.Name = "txtOrigine";
            this.txtOrigine.ReadOnly = false;
            this.txtOrigine.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtOrigine.Size = new System.Drawing.Size(24, 27);
            this.txtOrigine.TabIndex = 580;
            this.txtOrigine.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtOrigine.UseSystemPasswordChar = false;
            this.txtOrigine.Visible = false;
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label11.Location = new System.Drawing.Point(12, 96);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(68, 19);
            this.Label11.TabIndex = 412;
            this.Label11.Tag = "1";
            this.Label11.Text = "Chrono :";
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.Location = new System.Drawing.Point(198, 96);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(64, 19);
            this.Label12.TabIndex = 503;
            this.Label12.Text = "Famille :";
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label13.Location = new System.Drawing.Point(420, 96);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(62, 19);
            this.Label13.TabIndex = 505;
            this.Label13.Text = "Libellé :";
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label16.Location = new System.Drawing.Point(420, 127);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(70, 19);
            this.Label16.TabIndex = 513;
            this.Label16.Text = "Marque :";
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label15.Location = new System.Drawing.Point(198, 127);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(108, 19);
            this.Label15.TabIndex = 511;
            this.Label15.Text = "Réf. fabricant :";
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label14.Location = new System.Drawing.Point(8, 132);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(84, 19);
            this.Label14.TabIndex = 509;
            this.Label14.Text = "Réf. fourn :";
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label10.Location = new System.Drawing.Point(784, 128);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(98, 19);
            this.Label10.TabIndex = 515;
            this.Label10.Text = "Fournisseur :";
            // 
            // cmbFourn
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbFourn.DisplayLayout.Appearance = appearance1;
            this.cmbFourn.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbFourn.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbFourn.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbFourn.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cmbFourn.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbFourn.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cmbFourn.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbFourn.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbFourn.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbFourn.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cmbFourn.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbFourn.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFourn.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbFourn.DisplayLayout.Override.CellAppearance = appearance8;
            this.cmbFourn.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbFourn.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbFourn.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cmbFourn.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cmbFourn.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbFourn.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cmbFourn.DisplayLayout.Override.RowAppearance = appearance11;
            this.cmbFourn.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbFourn.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cmbFourn.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbFourn.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbFourn.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbFourn.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFourn.Location = new System.Drawing.Point(888, 127);
            this.cmbFourn.MinimumSize = new System.Drawing.Size(115, 27);
            this.cmbFourn.Name = "cmbFourn";
            this.cmbFourn.Size = new System.Drawing.Size(115, 27);
            this.cmbFourn.TabIndex = 516;
            this.cmbFourn.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbFourn_BeforeDropDown);
            this.cmbFourn.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbFourn_KeyPress);
            // 
            // Check1
            // 
            this.Check1.AutoSize = true;
            this.Check1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check1.Location = new System.Drawing.Point(12, 154);
            this.Check1.Name = "Check1";
            this.Check1.Size = new System.Drawing.Size(246, 23);
            this.Check1.TabIndex = 570;
            this.Check1.Text = "Seuls les articles du fournisseur";
            this.Check1.UseVisualStyleBackColor = true;
            this.Check1.Visible = false;
            // 
            // chkTriPrix
            // 
            this.chkTriPrix.AutoSize = true;
            this.chkTriPrix.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.chkTriPrix.Location = new System.Drawing.Point(15, 181);
            this.chkTriPrix.Name = "chkTriPrix";
            this.chkTriPrix.Size = new System.Drawing.Size(159, 21);
            this.chkTriPrix.TabIndex = 571;
            this.chkTriPrix.Text = "Tri décroissant du prix";
            this.chkTriPrix.UseVisualStyleBackColor = true;
            this.chkTriPrix.Visible = false;
            // 
            // Check2
            // 
            this.Check2.AutoSize = true;
            this.Check2.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.Check2.Location = new System.Drawing.Point(201, 181);
            this.Check2.Name = "Check2";
            this.Check2.Size = new System.Drawing.Size(227, 21);
            this.Check2.TabIndex = 572;
            this.Check2.Text = "Seuls les articles dans nos stocks";
            this.Check2.UseVisualStyleBackColor = true;
            this.Check2.Visible = false;
            // 
            // GridGecet
            // 
            this.GridGecet.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridGecet.DisplayLayout.Appearance = appearance13;
            this.GridGecet.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridGecet.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.GridGecet.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridGecet.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.GridGecet.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridGecet.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.GridGecet.DisplayLayout.MaxColScrollRegions = 1;
            this.GridGecet.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridGecet.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridGecet.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.GridGecet.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridGecet.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.GridGecet.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridGecet.DisplayLayout.Override.CellAppearance = appearance20;
            this.GridGecet.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridGecet.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.GridGecet.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.GridGecet.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.GridGecet.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridGecet.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.GridGecet.DisplayLayout.Override.RowAppearance = appearance23;
            this.GridGecet.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridGecet.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridGecet.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.GridGecet.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridGecet.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridGecet.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GridGecet.Location = new System.Drawing.Point(15, 208);
            this.GridGecet.Name = "GridGecet";
            this.GridGecet.Size = new System.Drawing.Size(1080, 419);
            this.GridGecet.TabIndex = 573;
            this.GridGecet.Text = "ultraGrid1";
            this.GridGecet.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridGecet_InitializeLayout);
            this.GridGecet.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.GridGecet_InitializeRow);
            this.GridGecet.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.GridGecet_ClickCellButton);
            this.GridGecet.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.GridGecet_BeforeExitEditMode);
            // 
            // cmdAppliqueClose
            // 
            this.cmdAppliqueClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAppliqueClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAppliqueClose.FlatAppearance.BorderSize = 0;
            this.cmdAppliqueClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAppliqueClose.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAppliqueClose.ForeColor = System.Drawing.Color.White;
            this.cmdAppliqueClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAppliqueClose.Location = new System.Drawing.Point(942, 165);
            this.cmdAppliqueClose.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAppliqueClose.Name = "cmdAppliqueClose";
            this.cmdAppliqueClose.Size = new System.Drawing.Size(154, 38);
            this.cmdAppliqueClose.TabIndex = 574;
            this.cmdAppliqueClose.Text = "Appliquer et fermer";
            this.cmdAppliqueClose.UseVisualStyleBackColor = false;
            this.cmdAppliqueClose.Click += new System.EventHandler(this.cmdAppliqueClose_Click);
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(-3, 182);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(217, 17);
            this.lblTotal.TabIndex = 580;
            // 
            // CmdPanier
            // 
            this.CmdPanier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdPanier.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdPanier.FlatAppearance.BorderSize = 0;
            this.CmdPanier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdPanier.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdPanier.ForeColor = System.Drawing.Color.White;
            this.CmdPanier.Image = global::Axe_interDT.Properties.Resources.Buy_16x16;
            this.CmdPanier.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdPanier.Location = new System.Drawing.Point(519, 164);
            this.CmdPanier.Margin = new System.Windows.Forms.Padding(2);
            this.CmdPanier.Name = "CmdPanier";
            this.CmdPanier.Size = new System.Drawing.Size(85, 39);
            this.CmdPanier.TabIndex = 579;
            this.CmdPanier.Text = "      Caddy";
            this.CmdPanier.UseVisualStyleBackColor = false;
            this.CmdPanier.Click += new System.EventHandler(this.CmdPanier_Click);
            // 
            // cmdHelp
            // 
            this.cmdHelp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdHelp.FlatAppearance.BorderSize = 0;
            this.cmdHelp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdHelp.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdHelp.ForeColor = System.Drawing.Color.White;
            this.cmdHelp.Image = global::Axe_interDT.Properties.Resources.Help_16x16;
            this.cmdHelp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdHelp.Location = new System.Drawing.Point(430, 164);
            this.cmdHelp.Margin = new System.Windows.Forms.Padding(2);
            this.cmdHelp.Name = "cmdHelp";
            this.cmdHelp.Size = new System.Drawing.Size(85, 39);
            this.cmdHelp.TabIndex = 578;
            this.cmdHelp.Text = "      Aide";
            this.cmdHelp.UseVisualStyleBackColor = false;
            this.cmdHelp.Click += new System.EventHandler(this.cmdHelp_Click);
            // 
            // cmdClean
            // 
            this.cmdClean.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdClean.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdClean.FlatAppearance.BorderSize = 0;
            this.cmdClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClean.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdClean.ForeColor = System.Drawing.Color.White;
            this.cmdClean.Image = global::Axe_interDT.Properties.Resources.Eraser_16x16;
            this.cmdClean.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdClean.Location = new System.Drawing.Point(609, 164);
            this.cmdClean.Margin = new System.Windows.Forms.Padding(2);
            this.cmdClean.Name = "cmdClean";
            this.cmdClean.Size = new System.Drawing.Size(85, 39);
            this.cmdClean.TabIndex = 577;
            this.cmdClean.Text = "   Clean";
            this.cmdClean.UseVisualStyleBackColor = false;
            this.cmdClean.Click += new System.EventHandler(this.cmdClean_Click);
            // 
            // cmdRechercher
            // 
            this.cmdRechercher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercher.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdRechercher.FlatAppearance.BorderSize = 0;
            this.cmdRechercher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercher.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRechercher.ForeColor = System.Drawing.Color.White;
            this.cmdRechercher.Image = global::Axe_interDT.Properties.Resources.Search_16x16;
            this.cmdRechercher.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdRechercher.Location = new System.Drawing.Point(698, 164);
            this.cmdRechercher.Margin = new System.Windows.Forms.Padding(2);
            this.cmdRechercher.Name = "cmdRechercher";
            this.cmdRechercher.Size = new System.Drawing.Size(124, 39);
            this.cmdRechercher.TabIndex = 576;
            this.cmdRechercher.Text = "     Rechercher";
            this.cmdRechercher.UseVisualStyleBackColor = false;
            this.cmdRechercher.Click += new System.EventHandler(this.cmdRechercher_Click);
            // 
            // cmdAppliquer
            // 
            this.cmdAppliquer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAppliquer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAppliquer.FlatAppearance.BorderSize = 0;
            this.cmdAppliquer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAppliquer.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAppliquer.ForeColor = System.Drawing.Color.White;
            this.cmdAppliquer.Image = global::Axe_interDT.Properties.Resources.Save_16x16;
            this.cmdAppliquer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAppliquer.Location = new System.Drawing.Point(826, 165);
            this.cmdAppliquer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAppliquer.Name = "cmdAppliquer";
            this.cmdAppliquer.Size = new System.Drawing.Size(112, 38);
            this.cmdAppliquer.TabIndex = 575;
            this.cmdAppliquer.Text = "      Appliquer";
            this.cmdAppliquer.UseVisualStyleBackColor = false;
            this.cmdAppliquer.Click += new System.EventHandler(this.cmdAppliquer_Click);
            // 
            // txtMarque
            // 
            this.txtMarque.AccAcceptNumbersOnly = false;
            this.txtMarque.AccAllowComma = false;
            this.txtMarque.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMarque.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMarque.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtMarque.AccHidenValue = "";
            this.txtMarque.AccNotAllowedChars = null;
            this.txtMarque.AccReadOnly = false;
            this.txtMarque.AccReadOnlyAllowDelete = false;
            this.txtMarque.AccRequired = false;
            this.txtMarque.BackColor = System.Drawing.Color.White;
            this.txtMarque.CustomBackColor = System.Drawing.Color.White;
            this.txtMarque.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarque.ForeColor = System.Drawing.Color.Black;
            this.txtMarque.Location = new System.Drawing.Point(482, 127);
            this.txtMarque.Margin = new System.Windows.Forms.Padding(2);
            this.txtMarque.MaxLength = 32767;
            this.txtMarque.Multiline = false;
            this.txtMarque.Name = "txtMarque";
            this.txtMarque.ReadOnly = false;
            this.txtMarque.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMarque.Size = new System.Drawing.Size(297, 27);
            this.txtMarque.TabIndex = 514;
            this.txtMarque.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMarque.UseSystemPasswordChar = false;
            this.txtMarque.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMarque_KeyPress);
            // 
            // txtRefFab
            // 
            this.txtRefFab.AccAcceptNumbersOnly = false;
            this.txtRefFab.AccAllowComma = false;
            this.txtRefFab.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRefFab.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRefFab.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtRefFab.AccHidenValue = "";
            this.txtRefFab.AccNotAllowedChars = null;
            this.txtRefFab.AccReadOnly = false;
            this.txtRefFab.AccReadOnlyAllowDelete = false;
            this.txtRefFab.AccRequired = false;
            this.txtRefFab.BackColor = System.Drawing.Color.White;
            this.txtRefFab.CustomBackColor = System.Drawing.Color.White;
            this.txtRefFab.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefFab.ForeColor = System.Drawing.Color.Black;
            this.txtRefFab.Location = new System.Drawing.Point(303, 127);
            this.txtRefFab.Margin = new System.Windows.Forms.Padding(2);
            this.txtRefFab.MaxLength = 32767;
            this.txtRefFab.Multiline = false;
            this.txtRefFab.Name = "txtRefFab";
            this.txtRefFab.ReadOnly = false;
            this.txtRefFab.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRefFab.Size = new System.Drawing.Size(115, 27);
            this.txtRefFab.TabIndex = 512;
            this.txtRefFab.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRefFab.UseSystemPasswordChar = false;
            this.txtRefFab.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRefFab_KeyPress);
            // 
            // txtRefFourn
            // 
            this.txtRefFourn.AccAcceptNumbersOnly = false;
            this.txtRefFourn.AccAllowComma = false;
            this.txtRefFourn.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRefFourn.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRefFourn.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtRefFourn.AccHidenValue = "";
            this.txtRefFourn.AccNotAllowedChars = null;
            this.txtRefFourn.AccReadOnly = false;
            this.txtRefFourn.AccReadOnlyAllowDelete = false;
            this.txtRefFourn.AccRequired = false;
            this.txtRefFourn.BackColor = System.Drawing.Color.White;
            this.txtRefFourn.CustomBackColor = System.Drawing.Color.White;
            this.txtRefFourn.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefFourn.ForeColor = System.Drawing.Color.Black;
            this.txtRefFourn.Location = new System.Drawing.Point(89, 127);
            this.txtRefFourn.Margin = new System.Windows.Forms.Padding(2);
            this.txtRefFourn.MaxLength = 32767;
            this.txtRefFourn.Multiline = false;
            this.txtRefFourn.Name = "txtRefFourn";
            this.txtRefFourn.ReadOnly = false;
            this.txtRefFourn.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRefFourn.Size = new System.Drawing.Size(107, 27);
            this.txtRefFourn.TabIndex = 510;
            this.txtRefFourn.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRefFourn.UseSystemPasswordChar = false;
            this.txtRefFourn.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRefFourn_KeyPress);
            // 
            // txtLibelle
            // 
            this.txtLibelle.AccAcceptNumbersOnly = false;
            this.txtLibelle.AccAllowComma = false;
            this.txtLibelle.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibelle.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibelle.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLibelle.AccHidenValue = "";
            this.txtLibelle.AccNotAllowedChars = null;
            this.txtLibelle.AccReadOnly = false;
            this.txtLibelle.AccReadOnlyAllowDelete = false;
            this.txtLibelle.AccRequired = false;
            this.txtLibelle.BackColor = System.Drawing.Color.White;
            this.txtLibelle.CustomBackColor = System.Drawing.Color.White;
            this.txtLibelle.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLibelle.ForeColor = System.Drawing.Color.Black;
            this.txtLibelle.Location = new System.Drawing.Point(482, 96);
            this.txtLibelle.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelle.MaxLength = 32767;
            this.txtLibelle.Multiline = false;
            this.txtLibelle.Name = "txtLibelle";
            this.txtLibelle.ReadOnly = false;
            this.txtLibelle.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibelle.Size = new System.Drawing.Size(521, 27);
            this.txtLibelle.TabIndex = 506;
            this.txtLibelle.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibelle.UseSystemPasswordChar = false;
            this.txtLibelle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLibelle_KeyPress);
            // 
            // txtFamille
            // 
            this.txtFamille.AccAcceptNumbersOnly = false;
            this.txtFamille.AccAllowComma = false;
            this.txtFamille.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFamille.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFamille.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtFamille.AccHidenValue = "";
            this.txtFamille.AccNotAllowedChars = null;
            this.txtFamille.AccReadOnly = false;
            this.txtFamille.AccReadOnlyAllowDelete = false;
            this.txtFamille.AccRequired = false;
            this.txtFamille.BackColor = System.Drawing.Color.White;
            this.txtFamille.CustomBackColor = System.Drawing.Color.White;
            this.txtFamille.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFamille.ForeColor = System.Drawing.Color.Black;
            this.txtFamille.Location = new System.Drawing.Point(303, 96);
            this.txtFamille.Margin = new System.Windows.Forms.Padding(2);
            this.txtFamille.MaxLength = 32767;
            this.txtFamille.Multiline = false;
            this.txtFamille.Name = "txtFamille";
            this.txtFamille.ReadOnly = false;
            this.txtFamille.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFamille.Size = new System.Drawing.Size(115, 27);
            this.txtFamille.TabIndex = 504;
            this.txtFamille.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFamille.UseSystemPasswordChar = false;
            this.txtFamille.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFamille_KeyPress);
            // 
            // txtCodeChrono
            // 
            this.txtCodeChrono.AccAcceptNumbersOnly = false;
            this.txtCodeChrono.AccAllowComma = false;
            this.txtCodeChrono.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeChrono.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeChrono.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCodeChrono.AccHidenValue = "";
            this.txtCodeChrono.AccNotAllowedChars = null;
            this.txtCodeChrono.AccReadOnly = false;
            this.txtCodeChrono.AccReadOnlyAllowDelete = false;
            this.txtCodeChrono.AccRequired = false;
            this.txtCodeChrono.BackColor = System.Drawing.Color.White;
            this.txtCodeChrono.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeChrono.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodeChrono.ForeColor = System.Drawing.Color.Black;
            this.txtCodeChrono.Location = new System.Drawing.Point(89, 96);
            this.txtCodeChrono.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeChrono.MaxLength = 32767;
            this.txtCodeChrono.Multiline = false;
            this.txtCodeChrono.Name = "txtCodeChrono";
            this.txtCodeChrono.ReadOnly = false;
            this.txtCodeChrono.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeChrono.Size = new System.Drawing.Size(107, 27);
            this.txtCodeChrono.TabIndex = 502;
            this.txtCodeChrono.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeChrono.UseSystemPasswordChar = false;
            this.txtCodeChrono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeChrono_KeyPress);
            // 
            // frmGecet
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1107, 639);
            this.Controls.Add(this.lblTotal);
            this.Controls.Add(this.CmdPanier);
            this.Controls.Add(this.cmdHelp);
            this.Controls.Add(this.cmdClean);
            this.Controls.Add(this.cmdRechercher);
            this.Controls.Add(this.cmdAppliquer);
            this.Controls.Add(this.cmdAppliqueClose);
            this.Controls.Add(this.GridGecet);
            this.Controls.Add(this.Check2);
            this.Controls.Add(this.chkTriPrix);
            this.Controls.Add(this.Check1);
            this.Controls.Add(this.cmbFourn);
            this.Controls.Add(this.Label10);
            this.Controls.Add(this.txtMarque);
            this.Controls.Add(this.Label16);
            this.Controls.Add(this.txtRefFab);
            this.Controls.Add(this.Label15);
            this.Controls.Add(this.txtRefFourn);
            this.Controls.Add(this.Label14);
            this.Controls.Add(this.txtLibelle);
            this.Controls.Add(this.Label13);
            this.Controls.Add(this.txtFamille);
            this.Controls.Add(this.Label12);
            this.Controls.Add(this.txtCodeChrono);
            this.Controls.Add(this.Label11);
            this.Controls.Add(this.Frame1);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(915, 509);
            this.Name = "frmGecet";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Recherche d\'articles Gecet";
            this.Activated += new System.EventHandler(this.frmGecet_Activated);
            this.Load += new System.EventHandler(this.frmGecet_Load);
            this.Frame1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFourn)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridGecet)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public iTalk.iTalk_TextBox_Small2 txtCodeChrono;
        public iTalk.iTalk_TextBox_Small2 txtFamille;
        public iTalk.iTalk_TextBox_Small2 txtLibelle;
        public iTalk.iTalk_TextBox_Small2 txtMarque;
        public iTalk.iTalk_TextBox_Small2 txtRefFab;
        public iTalk.iTalk_TextBox_Small2 txtRefFourn;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbFourn;
        public System.Windows.Forms.Button cmdAppliqueClose;
        public System.Windows.Forms.Button cmdAppliquer;
        public System.Windows.Forms.Button cmdRechercher;
        public System.Windows.Forms.Button cmdClean;
        public System.Windows.Forms.Button cmdHelp;
        public System.Windows.Forms.Button CmdPanier;
        public iTalk.iTalk_TextBox_Small2 txtType;
        public iTalk.iTalk_TextBox_Small2 txtPxHeure;
        public iTalk.iTalk_TextBox_Small2 txtTVA;
        public iTalk.iTalk_TextBox_Small2 txtKST;
        public iTalk.iTalk_TextBox_Small2 txtKMO;
        public iTalk.iTalk_TextBox_Small2 txtKFO;
        public iTalk.iTalk_TextBox_Small2 txtCle;
        public iTalk.iTalk_TextBox_Small2 txtNumLigne;
        public iTalk.iTalk_TextBox_Small2 txtOrigine;
        public System.Windows.Forms.GroupBox Frame1;
        public System.Windows.Forms.RadioButton optEspace;
        public System.Windows.Forms.RadioButton optEtouOU;
        public System.Windows.Forms.RadioButton OptOu;
        public System.Windows.Forms.RadioButton OptEt;
        public System.Windows.Forms.Label Label11;
        public System.Windows.Forms.Label Label12;
        public System.Windows.Forms.Label Label13;
        public System.Windows.Forms.Label Label16;
        public System.Windows.Forms.Label Label15;
        public System.Windows.Forms.Label Label14;
        public System.Windows.Forms.Label Label10;
        public System.Windows.Forms.CheckBox Check1;
        public System.Windows.Forms.CheckBox chkTriPrix;
        public System.Windows.Forms.CheckBox Check2;
        public System.Windows.Forms.Label lblTotal;
        public UltraGrid GridGecet;
        public System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.Panel panel2;
    }
}