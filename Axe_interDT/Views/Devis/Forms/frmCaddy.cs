﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmCaddy : Form
    {
        private DataTable rsCaddy;
        private ModAdo modAdorsCaddy;
        public frmCaddy()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmCaddy_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            modAdorsCaddy = new ModAdo();
            rsCaddy = modAdorsCaddy.fc_OpenRecordSet("SELECT CleAuto,Code,Designation,Quantite FROM PanierGecet WHERE Utilisateur='" + General.gsUtilisateur + "'");
            GridCaddy.DataSource = rsCaddy;
            GridCaddy.DisplayLayout.Bands[0].Columns["CleAuto"].Hidden = true;
            GridCaddy.DisplayLayout.Bands[0].Columns["Designation"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            GridCaddy.DisplayLayout.Bands[0].Columns["Code"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
        }

        private void GridCaddy_Leave(object sender, EventArgs e)
        {
            GridCaddy.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCaddy_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo,MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCaddy_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            modAdorsCaddy.Update();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCaddy_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdorsCaddy.Update();
        }
    }
}
