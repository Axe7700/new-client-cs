﻿namespace Axe_interDT.Views.Devis.Forms
{
    partial class frmGecetV3
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.panel3 = new System.Windows.Forms.Panel();
            this.optEtouOU = new System.Windows.Forms.RadioButton();
            this.optEspace = new System.Windows.Forms.RadioButton();
            this.txtType = new iTalk.iTalk_TextBox_Small2();
            this.txtPxHeure = new iTalk.iTalk_TextBox_Small2();
            this.txtTVA = new iTalk.iTalk_TextBox_Small2();
            this.txtKST = new iTalk.iTalk_TextBox_Small2();
            this.txtKMO = new iTalk.iTalk_TextBox_Small2();
            this.txtKFO = new iTalk.iTalk_TextBox_Small2();
            this.txtCle = new iTalk.iTalk_TextBox_Small2();
            this.txtNumLigne = new iTalk.iTalk_TextBox_Small2();
            this.txtOrigine = new iTalk.iTalk_TextBox_Small2();
            this.panel1 = new System.Windows.Forms.Panel();
            this.OptEt = new System.Windows.Forms.RadioButton();
            this.OptOu = new System.Windows.Forms.RadioButton();
            this.panel2 = new System.Windows.Forms.Panel();
            this.optBaseQualifie = new System.Windows.Forms.RadioButton();
            this.optBaseNonQualifie = new System.Windows.Forms.RadioButton();
            this.FraNonQualifie = new System.Windows.Forms.GroupBox();
            this.txtPreFixe = new iTalk.iTalk_TextBox_Small2();
            this.CmdPanierNQ = new System.Windows.Forms.Button();
            this.chkTriPrixNQ = new System.Windows.Forms.CheckBox();
            this.cmdHelpNQ = new System.Windows.Forms.Button();
            this.cmbFournNQ = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cmdAppliqueCloseNQ = new System.Windows.Forms.Button();
            this.Label10 = new System.Windows.Forms.Label();
            this.cmdCleanNQ = new System.Windows.Forms.Button();
            this.txtRefFournNQ = new iTalk.iTalk_TextBox_Small2();
            this.cmdAppliquerNQ = new System.Windows.Forms.Button();
            this.Label14 = new System.Windows.Forms.Label();
            this.cmdRechercheNonQualif = new System.Windows.Forms.Button();
            this.txtLibelleNQ = new iTalk.iTalk_TextBox_Small2();
            this.Label13 = new System.Windows.Forms.Label();
            this.Label11 = new System.Windows.Forms.Label();
            this.txtFamilleNQ = new iTalk.iTalk_TextBox_Small2();
            this.txtIDGEC_DONNEES_NON_QUALIFIEES = new iTalk.iTalk_TextBox_Small2();
            this.Label12 = new System.Windows.Forms.Label();
            this.FraQualifie = new System.Windows.Forms.GroupBox();
            this.lblTotal = new System.Windows.Forms.Label();
            this.CmdPanier = new System.Windows.Forms.Button();
            this.cmdHelp = new System.Windows.Forms.Button();
            this.cmdClean = new System.Windows.Forms.Button();
            this.cmdRechercher = new System.Windows.Forms.Button();
            this.cmdAppliquer = new System.Windows.Forms.Button();
            this.cmdAppliqueClose = new System.Windows.Forms.Button();
            this.Check2 = new System.Windows.Forms.CheckBox();
            this.chkTriPrix = new System.Windows.Forms.CheckBox();
            this.Check1 = new System.Windows.Forms.CheckBox();
            this.cmbFourn = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label1 = new System.Windows.Forms.Label();
            this.txtMarque = new iTalk.iTalk_TextBox_Small2();
            this.Label16 = new System.Windows.Forms.Label();
            this.txtRefFab = new iTalk.iTalk_TextBox_Small2();
            this.Label15 = new System.Windows.Forms.Label();
            this.txtRefFourn = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.txtSynonyme = new iTalk.iTalk_TextBox_Small2();
            this.Label17 = new System.Windows.Forms.Label();
            this.txtLibelle = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.txtFamille = new iTalk.iTalk_TextBox_Small2();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCodeChrono = new iTalk.iTalk_TextBox_Small2();
            this.label5 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.GridGecetNonQualif = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.GridGecet = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.Frame1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.FraNonQualifie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFournNQ)).BeginInit();
            this.FraQualifie.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFourn)).BeginInit();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridGecetNonQualif)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridGecet)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.Transparent;
            this.Frame1.Controls.Add(this.panel3);
            this.Frame1.Controls.Add(this.txtType);
            this.Frame1.Controls.Add(this.txtPxHeure);
            this.Frame1.Controls.Add(this.txtTVA);
            this.Frame1.Controls.Add(this.txtKST);
            this.Frame1.Controls.Add(this.txtKMO);
            this.Frame1.Controls.Add(this.txtKFO);
            this.Frame1.Controls.Add(this.txtCle);
            this.Frame1.Controls.Add(this.txtNumLigne);
            this.Frame1.Controls.Add(this.txtOrigine);
            this.Frame1.Controls.Add(this.panel1);
            this.Frame1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Frame1.Location = new System.Drawing.Point(2, 5);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(992, 78);
            this.Frame1.TabIndex = 412;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "Sélectionnez la manière dont vous voulez faire vos recherches.";
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.optEtouOU);
            this.panel3.Controls.Add(this.optEspace);
            this.panel3.Location = new System.Drawing.Point(10, 21);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(590, 51);
            this.panel3.TabIndex = 599;
            // 
            // optEtouOU
            // 
            this.optEtouOU.AutoSize = true;
            this.optEtouOU.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optEtouOU.Location = new System.Drawing.Point(3, 3);
            this.optEtouOU.Name = "optEtouOU";
            this.optEtouOU.Size = new System.Drawing.Size(257, 23);
            this.optEtouOU.TabIndex = 538;
            this.optEtouOU.Text = "Saisir ET ou OU entre chaque mot";
            this.optEtouOU.UseVisualStyleBackColor = true;
            this.optEtouOU.CheckedChanged += new System.EventHandler(this.optEtouOU_CheckedChanged);
            // 
            // optEspace
            // 
            this.optEspace.AutoSize = true;
            this.optEspace.Checked = true;
            this.optEspace.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optEspace.Location = new System.Drawing.Point(3, 25);
            this.optEspace.Name = "optEspace";
            this.optEspace.Size = new System.Drawing.Size(588, 23);
            this.optEspace.TabIndex = 539;
            this.optEspace.TabStop = true;
            this.optEspace.Text = "Utiliser la touche espace, celle ci équivaut à ET ou OU selon votre choix ci aprè" +
    "s =>";
            this.optEspace.UseVisualStyleBackColor = true;
            this.optEspace.CheckedChanged += new System.EventHandler(this.optEspace_CheckedChanged);
            // 
            // txtType
            // 
            this.txtType.AccAcceptNumbersOnly = false;
            this.txtType.AccAllowComma = false;
            this.txtType.AccBackgroundColor = System.Drawing.Color.White;
            this.txtType.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtType.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtType.AccHidenValue = "";
            this.txtType.AccNotAllowedChars = null;
            this.txtType.AccReadOnly = false;
            this.txtType.AccReadOnlyAllowDelete = false;
            this.txtType.AccRequired = false;
            this.txtType.BackColor = System.Drawing.Color.White;
            this.txtType.CustomBackColor = System.Drawing.Color.White;
            this.txtType.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtType.ForeColor = System.Drawing.Color.Black;
            this.txtType.Location = new System.Drawing.Point(811, 51);
            this.txtType.Margin = new System.Windows.Forms.Padding(2);
            this.txtType.MaxLength = 32767;
            this.txtType.Multiline = false;
            this.txtType.Name = "txtType";
            this.txtType.ReadOnly = false;
            this.txtType.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtType.Size = new System.Drawing.Size(24, 27);
            this.txtType.TabIndex = 598;
            this.txtType.Text = "0";
            this.txtType.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtType.UseSystemPasswordChar = false;
            this.txtType.Visible = false;
            // 
            // txtPxHeure
            // 
            this.txtPxHeure.AccAcceptNumbersOnly = false;
            this.txtPxHeure.AccAllowComma = false;
            this.txtPxHeure.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPxHeure.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPxHeure.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPxHeure.AccHidenValue = "";
            this.txtPxHeure.AccNotAllowedChars = null;
            this.txtPxHeure.AccReadOnly = false;
            this.txtPxHeure.AccReadOnlyAllowDelete = false;
            this.txtPxHeure.AccRequired = false;
            this.txtPxHeure.BackColor = System.Drawing.Color.White;
            this.txtPxHeure.CustomBackColor = System.Drawing.Color.White;
            this.txtPxHeure.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPxHeure.ForeColor = System.Drawing.Color.Black;
            this.txtPxHeure.Location = new System.Drawing.Point(783, 51);
            this.txtPxHeure.Margin = new System.Windows.Forms.Padding(2);
            this.txtPxHeure.MaxLength = 32767;
            this.txtPxHeure.Multiline = false;
            this.txtPxHeure.Name = "txtPxHeure";
            this.txtPxHeure.ReadOnly = false;
            this.txtPxHeure.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPxHeure.Size = new System.Drawing.Size(24, 27);
            this.txtPxHeure.TabIndex = 597;
            this.txtPxHeure.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPxHeure.UseSystemPasswordChar = false;
            this.txtPxHeure.Visible = false;
            // 
            // txtTVA
            // 
            this.txtTVA.AccAcceptNumbersOnly = false;
            this.txtTVA.AccAllowComma = false;
            this.txtTVA.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTVA.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTVA.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtTVA.AccHidenValue = "";
            this.txtTVA.AccNotAllowedChars = null;
            this.txtTVA.AccReadOnly = false;
            this.txtTVA.AccReadOnlyAllowDelete = false;
            this.txtTVA.AccRequired = false;
            this.txtTVA.BackColor = System.Drawing.Color.White;
            this.txtTVA.CustomBackColor = System.Drawing.Color.White;
            this.txtTVA.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtTVA.ForeColor = System.Drawing.Color.Black;
            this.txtTVA.Location = new System.Drawing.Point(755, 51);
            this.txtTVA.Margin = new System.Windows.Forms.Padding(2);
            this.txtTVA.MaxLength = 32767;
            this.txtTVA.Multiline = false;
            this.txtTVA.Name = "txtTVA";
            this.txtTVA.ReadOnly = false;
            this.txtTVA.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTVA.Size = new System.Drawing.Size(24, 27);
            this.txtTVA.TabIndex = 596;
            this.txtTVA.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTVA.UseSystemPasswordChar = false;
            this.txtTVA.Visible = false;
            // 
            // txtKST
            // 
            this.txtKST.AccAcceptNumbersOnly = false;
            this.txtKST.AccAllowComma = false;
            this.txtKST.AccBackgroundColor = System.Drawing.Color.White;
            this.txtKST.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtKST.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtKST.AccHidenValue = "";
            this.txtKST.AccNotAllowedChars = null;
            this.txtKST.AccReadOnly = false;
            this.txtKST.AccReadOnlyAllowDelete = false;
            this.txtKST.AccRequired = false;
            this.txtKST.BackColor = System.Drawing.Color.White;
            this.txtKST.CustomBackColor = System.Drawing.Color.White;
            this.txtKST.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtKST.ForeColor = System.Drawing.Color.Black;
            this.txtKST.Location = new System.Drawing.Point(867, 19);
            this.txtKST.Margin = new System.Windows.Forms.Padding(2);
            this.txtKST.MaxLength = 32767;
            this.txtKST.Multiline = false;
            this.txtKST.Name = "txtKST";
            this.txtKST.ReadOnly = false;
            this.txtKST.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtKST.Size = new System.Drawing.Size(24, 27);
            this.txtKST.TabIndex = 595;
            this.txtKST.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtKST.UseSystemPasswordChar = false;
            this.txtKST.Visible = false;
            // 
            // txtKMO
            // 
            this.txtKMO.AccAcceptNumbersOnly = false;
            this.txtKMO.AccAllowComma = false;
            this.txtKMO.AccBackgroundColor = System.Drawing.Color.White;
            this.txtKMO.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtKMO.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtKMO.AccHidenValue = "";
            this.txtKMO.AccNotAllowedChars = null;
            this.txtKMO.AccReadOnly = false;
            this.txtKMO.AccReadOnlyAllowDelete = false;
            this.txtKMO.AccRequired = false;
            this.txtKMO.BackColor = System.Drawing.Color.White;
            this.txtKMO.CustomBackColor = System.Drawing.Color.White;
            this.txtKMO.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtKMO.ForeColor = System.Drawing.Color.Black;
            this.txtKMO.Location = new System.Drawing.Point(839, 19);
            this.txtKMO.Margin = new System.Windows.Forms.Padding(2);
            this.txtKMO.MaxLength = 32767;
            this.txtKMO.Multiline = false;
            this.txtKMO.Name = "txtKMO";
            this.txtKMO.ReadOnly = false;
            this.txtKMO.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtKMO.Size = new System.Drawing.Size(24, 27);
            this.txtKMO.TabIndex = 594;
            this.txtKMO.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtKMO.UseSystemPasswordChar = false;
            this.txtKMO.Visible = false;
            // 
            // txtKFO
            // 
            this.txtKFO.AccAcceptNumbersOnly = false;
            this.txtKFO.AccAllowComma = false;
            this.txtKFO.AccBackgroundColor = System.Drawing.Color.White;
            this.txtKFO.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtKFO.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtKFO.AccHidenValue = "";
            this.txtKFO.AccNotAllowedChars = null;
            this.txtKFO.AccReadOnly = false;
            this.txtKFO.AccReadOnlyAllowDelete = false;
            this.txtKFO.AccRequired = false;
            this.txtKFO.BackColor = System.Drawing.Color.White;
            this.txtKFO.CustomBackColor = System.Drawing.Color.White;
            this.txtKFO.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtKFO.ForeColor = System.Drawing.Color.Black;
            this.txtKFO.Location = new System.Drawing.Point(811, 19);
            this.txtKFO.Margin = new System.Windows.Forms.Padding(2);
            this.txtKFO.MaxLength = 32767;
            this.txtKFO.Multiline = false;
            this.txtKFO.Name = "txtKFO";
            this.txtKFO.ReadOnly = false;
            this.txtKFO.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtKFO.Size = new System.Drawing.Size(24, 27);
            this.txtKFO.TabIndex = 593;
            this.txtKFO.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtKFO.UseSystemPasswordChar = false;
            this.txtKFO.Visible = false;
            // 
            // txtCle
            // 
            this.txtCle.AccAcceptNumbersOnly = false;
            this.txtCle.AccAllowComma = false;
            this.txtCle.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCle.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCle.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCle.AccHidenValue = "";
            this.txtCle.AccNotAllowedChars = null;
            this.txtCle.AccReadOnly = false;
            this.txtCle.AccReadOnlyAllowDelete = false;
            this.txtCle.AccRequired = false;
            this.txtCle.BackColor = System.Drawing.Color.White;
            this.txtCle.CustomBackColor = System.Drawing.Color.White;
            this.txtCle.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCle.ForeColor = System.Drawing.Color.Black;
            this.txtCle.Location = new System.Drawing.Point(749, 19);
            this.txtCle.Margin = new System.Windows.Forms.Padding(2);
            this.txtCle.MaxLength = 32767;
            this.txtCle.Multiline = false;
            this.txtCle.Name = "txtCle";
            this.txtCle.ReadOnly = false;
            this.txtCle.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCle.Size = new System.Drawing.Size(24, 27);
            this.txtCle.TabIndex = 592;
            this.txtCle.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCle.UseSystemPasswordChar = false;
            this.txtCle.Visible = false;
            // 
            // txtNumLigne
            // 
            this.txtNumLigne.AccAcceptNumbersOnly = false;
            this.txtNumLigne.AccAllowComma = false;
            this.txtNumLigne.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNumLigne.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNumLigne.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtNumLigne.AccHidenValue = "";
            this.txtNumLigne.AccNotAllowedChars = null;
            this.txtNumLigne.AccReadOnly = false;
            this.txtNumLigne.AccReadOnlyAllowDelete = false;
            this.txtNumLigne.AccRequired = false;
            this.txtNumLigne.BackColor = System.Drawing.Color.White;
            this.txtNumLigne.CustomBackColor = System.Drawing.Color.White;
            this.txtNumLigne.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNumLigne.ForeColor = System.Drawing.Color.Black;
            this.txtNumLigne.Location = new System.Drawing.Point(721, 19);
            this.txtNumLigne.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumLigne.MaxLength = 32767;
            this.txtNumLigne.Multiline = false;
            this.txtNumLigne.Name = "txtNumLigne";
            this.txtNumLigne.ReadOnly = false;
            this.txtNumLigne.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNumLigne.Size = new System.Drawing.Size(24, 27);
            this.txtNumLigne.TabIndex = 591;
            this.txtNumLigne.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNumLigne.UseSystemPasswordChar = false;
            this.txtNumLigne.Visible = false;
            // 
            // txtOrigine
            // 
            this.txtOrigine.AccAcceptNumbersOnly = false;
            this.txtOrigine.AccAllowComma = false;
            this.txtOrigine.AccBackgroundColor = System.Drawing.Color.White;
            this.txtOrigine.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtOrigine.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtOrigine.AccHidenValue = "";
            this.txtOrigine.AccNotAllowedChars = null;
            this.txtOrigine.AccReadOnly = false;
            this.txtOrigine.AccReadOnlyAllowDelete = false;
            this.txtOrigine.AccRequired = false;
            this.txtOrigine.BackColor = System.Drawing.Color.White;
            this.txtOrigine.CustomBackColor = System.Drawing.Color.White;
            this.txtOrigine.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtOrigine.ForeColor = System.Drawing.Color.Black;
            this.txtOrigine.Location = new System.Drawing.Point(693, 19);
            this.txtOrigine.Margin = new System.Windows.Forms.Padding(2);
            this.txtOrigine.MaxLength = 32767;
            this.txtOrigine.Multiline = false;
            this.txtOrigine.Name = "txtOrigine";
            this.txtOrigine.ReadOnly = false;
            this.txtOrigine.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtOrigine.Size = new System.Drawing.Size(24, 27);
            this.txtOrigine.TabIndex = 590;
            this.txtOrigine.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtOrigine.UseSystemPasswordChar = false;
            this.txtOrigine.Visible = false;
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.OptEt);
            this.panel1.Controls.Add(this.OptOu);
            this.panel1.Location = new System.Drawing.Point(604, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(97, 34);
            this.panel1.TabIndex = 589;
            // 
            // OptEt
            // 
            this.OptEt.AutoSize = true;
            this.OptEt.Checked = true;
            this.OptEt.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.OptEt.Location = new System.Drawing.Point(3, 10);
            this.OptEt.Name = "OptEt";
            this.OptEt.Size = new System.Drawing.Size(45, 23);
            this.OptEt.TabIndex = 540;
            this.OptEt.TabStop = true;
            this.OptEt.Text = "ET";
            this.OptEt.UseVisualStyleBackColor = true;
            this.OptEt.CheckedChanged += new System.EventHandler(this.OptEt_CheckedChanged);
            // 
            // OptOu
            // 
            this.OptOu.AutoSize = true;
            this.OptOu.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.OptOu.Location = new System.Drawing.Point(49, 10);
            this.OptOu.Name = "OptOu";
            this.OptOu.Size = new System.Drawing.Size(49, 23);
            this.OptOu.TabIndex = 541;
            this.OptOu.Text = "OU";
            this.OptOu.UseVisualStyleBackColor = true;
            this.OptOu.CheckedChanged += new System.EventHandler(this.OptOu_CheckedChanged);
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.optBaseQualifie);
            this.panel2.Controls.Add(this.optBaseNonQualifie);
            this.panel2.Location = new System.Drawing.Point(264, 80);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(439, 34);
            this.panel2.TabIndex = 590;
            // 
            // optBaseQualifie
            // 
            this.optBaseQualifie.AutoSize = true;
            this.optBaseQualifie.Checked = true;
            this.optBaseQualifie.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optBaseQualifie.Location = new System.Drawing.Point(3, 10);
            this.optBaseQualifie.Name = "optBaseQualifie";
            this.optBaseQualifie.Size = new System.Drawing.Size(174, 23);
            this.optBaseQualifie.TabIndex = 540;
            this.optBaseQualifie.TabStop = true;
            this.optBaseQualifie.Text = "Base GECET qualifiée";
            this.optBaseQualifie.UseVisualStyleBackColor = true;
            this.optBaseQualifie.CheckedChanged += new System.EventHandler(this.optBaseQualifie_CheckedChanged);
            // 
            // optBaseNonQualifie
            // 
            this.optBaseNonQualifie.AutoSize = true;
            this.optBaseNonQualifie.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optBaseNonQualifie.Location = new System.Drawing.Point(200, 10);
            this.optBaseNonQualifie.Name = "optBaseNonQualifie";
            this.optBaseNonQualifie.Size = new System.Drawing.Size(204, 23);
            this.optBaseNonQualifie.TabIndex = 541;
            this.optBaseNonQualifie.Text = "Base GECET non qualifiée";
            this.optBaseNonQualifie.UseVisualStyleBackColor = true;
            this.optBaseNonQualifie.CheckedChanged += new System.EventHandler(this.optBaseNonQualifie_CheckedChanged);
            // 
            // FraNonQualifie
            // 
            this.FraNonQualifie.BackColor = System.Drawing.Color.Transparent;
            this.FraNonQualifie.Controls.Add(this.txtPreFixe);
            this.FraNonQualifie.Controls.Add(this.CmdPanierNQ);
            this.FraNonQualifie.Controls.Add(this.chkTriPrixNQ);
            this.FraNonQualifie.Controls.Add(this.cmdHelpNQ);
            this.FraNonQualifie.Controls.Add(this.cmbFournNQ);
            this.FraNonQualifie.Controls.Add(this.cmdAppliqueCloseNQ);
            this.FraNonQualifie.Controls.Add(this.Label10);
            this.FraNonQualifie.Controls.Add(this.cmdCleanNQ);
            this.FraNonQualifie.Controls.Add(this.txtRefFournNQ);
            this.FraNonQualifie.Controls.Add(this.cmdAppliquerNQ);
            this.FraNonQualifie.Controls.Add(this.Label14);
            this.FraNonQualifie.Controls.Add(this.cmdRechercheNonQualif);
            this.FraNonQualifie.Controls.Add(this.txtLibelleNQ);
            this.FraNonQualifie.Controls.Add(this.Label13);
            this.FraNonQualifie.Controls.Add(this.Label11);
            this.FraNonQualifie.Controls.Add(this.txtFamilleNQ);
            this.FraNonQualifie.Controls.Add(this.txtIDGEC_DONNEES_NON_QUALIFIEES);
            this.FraNonQualifie.Controls.Add(this.Label12);
            this.FraNonQualifie.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.FraNonQualifie.Location = new System.Drawing.Point(2, 115);
            this.FraNonQualifie.Name = "FraNonQualifie";
            this.FraNonQualifie.Size = new System.Drawing.Size(1116, 142);
            this.FraNonQualifie.TabIndex = 591;
            this.FraNonQualifie.TabStop = false;
            // 
            // txtPreFixe
            // 
            this.txtPreFixe.AccAcceptNumbersOnly = false;
            this.txtPreFixe.AccAllowComma = false;
            this.txtPreFixe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPreFixe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPreFixe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPreFixe.AccHidenValue = "";
            this.txtPreFixe.AccNotAllowedChars = null;
            this.txtPreFixe.AccReadOnly = false;
            this.txtPreFixe.AccReadOnlyAllowDelete = false;
            this.txtPreFixe.AccRequired = false;
            this.txtPreFixe.BackColor = System.Drawing.Color.White;
            this.txtPreFixe.CustomBackColor = System.Drawing.Color.White;
            this.txtPreFixe.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPreFixe.ForeColor = System.Drawing.Color.Black;
            this.txtPreFixe.Location = new System.Drawing.Point(85, 20);
            this.txtPreFixe.Margin = new System.Windows.Forms.Padding(2);
            this.txtPreFixe.MaxLength = 32767;
            this.txtPreFixe.Multiline = false;
            this.txtPreFixe.Name = "txtPreFixe";
            this.txtPreFixe.ReadOnly = false;
            this.txtPreFixe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPreFixe.Size = new System.Drawing.Size(53, 27);
            this.txtPreFixe.TabIndex = 610;
            this.txtPreFixe.Text = "BD2_";
            this.txtPreFixe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPreFixe.UseSystemPasswordChar = false;
            // 
            // CmdPanierNQ
            // 
            this.CmdPanierNQ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdPanierNQ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdPanierNQ.FlatAppearance.BorderSize = 0;
            this.CmdPanierNQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdPanierNQ.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.CmdPanierNQ.ForeColor = System.Drawing.Color.White;
            this.CmdPanierNQ.Image = global::Axe_interDT.Properties.Resources.Buy_16x16;
            this.CmdPanierNQ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdPanierNQ.Location = new System.Drawing.Point(560, 88);
            this.CmdPanierNQ.Margin = new System.Windows.Forms.Padding(2);
            this.CmdPanierNQ.Name = "CmdPanierNQ";
            this.CmdPanierNQ.Size = new System.Drawing.Size(85, 39);
            this.CmdPanierNQ.TabIndex = 609;
            this.CmdPanierNQ.Text = "      Caddy";
            this.CmdPanierNQ.UseVisualStyleBackColor = false;
            this.CmdPanierNQ.Click += new System.EventHandler(this.CmdPanierNQ_Click);
            // 
            // chkTriPrixNQ
            // 
            this.chkTriPrixNQ.AutoSize = true;
            this.chkTriPrixNQ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.chkTriPrixNQ.Location = new System.Drawing.Point(6, 88);
            this.chkTriPrixNQ.Name = "chkTriPrixNQ";
            this.chkTriPrixNQ.Size = new System.Drawing.Size(158, 20);
            this.chkTriPrixNQ.TabIndex = 602;
            this.chkTriPrixNQ.Text = "Tri décroissant du prix";
            this.chkTriPrixNQ.UseVisualStyleBackColor = true;
            this.chkTriPrixNQ.Visible = false;
            // 
            // cmdHelpNQ
            // 
            this.cmdHelpNQ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdHelpNQ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdHelpNQ.FlatAppearance.BorderSize = 0;
            this.cmdHelpNQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdHelpNQ.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdHelpNQ.ForeColor = System.Drawing.Color.White;
            this.cmdHelpNQ.Image = global::Axe_interDT.Properties.Resources.Help_16x16;
            this.cmdHelpNQ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdHelpNQ.Location = new System.Drawing.Point(471, 87);
            this.cmdHelpNQ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdHelpNQ.Name = "cmdHelpNQ";
            this.cmdHelpNQ.Size = new System.Drawing.Size(85, 39);
            this.cmdHelpNQ.TabIndex = 608;
            this.cmdHelpNQ.Text = "      Aide";
            this.cmdHelpNQ.UseVisualStyleBackColor = false;
            this.cmdHelpNQ.Click += new System.EventHandler(this.cmdHelpNQ_Click);
            // 
            // cmbFournNQ
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbFournNQ.DisplayLayout.Appearance = appearance1;
            this.cmbFournNQ.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbFournNQ.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbFournNQ.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbFournNQ.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cmbFournNQ.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbFournNQ.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cmbFournNQ.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbFournNQ.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbFournNQ.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbFournNQ.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cmbFournNQ.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbFournNQ.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFournNQ.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbFournNQ.DisplayLayout.Override.CellAppearance = appearance8;
            this.cmbFournNQ.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbFournNQ.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbFournNQ.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cmbFournNQ.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cmbFournNQ.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbFournNQ.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cmbFournNQ.DisplayLayout.Override.RowAppearance = appearance11;
            this.cmbFournNQ.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbFournNQ.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cmbFournNQ.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbFournNQ.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbFournNQ.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbFournNQ.Location = new System.Drawing.Point(334, 49);
            this.cmbFournNQ.Name = "cmbFournNQ";
            this.cmbFournNQ.Size = new System.Drawing.Size(115, 25);
            this.cmbFournNQ.TabIndex = 601;
            this.cmbFournNQ.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbFournNQ_BeforeDropDown);
            this.cmbFournNQ.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbFournNQ_KeyPress);
            // 
            // cmdAppliqueCloseNQ
            // 
            this.cmdAppliqueCloseNQ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAppliqueCloseNQ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAppliqueCloseNQ.FlatAppearance.BorderSize = 0;
            this.cmdAppliqueCloseNQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAppliqueCloseNQ.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdAppliqueCloseNQ.ForeColor = System.Drawing.Color.White;
            this.cmdAppliqueCloseNQ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAppliqueCloseNQ.Location = new System.Drawing.Point(962, 88);
            this.cmdAppliqueCloseNQ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAppliqueCloseNQ.Name = "cmdAppliqueCloseNQ";
            this.cmdAppliqueCloseNQ.Size = new System.Drawing.Size(149, 38);
            this.cmdAppliqueCloseNQ.TabIndex = 604;
            this.cmdAppliqueCloseNQ.Text = "Appliquer et fermer";
            this.cmdAppliqueCloseNQ.UseVisualStyleBackColor = false;
            this.cmdAppliqueCloseNQ.Click += new System.EventHandler(this.cmdAppliqueCloseNQ_Click);
            // 
            // Label10
            // 
            this.Label10.AutoSize = true;
            this.Label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label10.Location = new System.Drawing.Point(244, 55);
            this.Label10.Name = "Label10";
            this.Label10.Size = new System.Drawing.Size(84, 16);
            this.Label10.TabIndex = 600;
            this.Label10.Text = "Fournisseur :";
            // 
            // cmdCleanNQ
            // 
            this.cmdCleanNQ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdCleanNQ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCleanNQ.FlatAppearance.BorderSize = 0;
            this.cmdCleanNQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCleanNQ.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdCleanNQ.ForeColor = System.Drawing.Color.White;
            this.cmdCleanNQ.Image = global::Axe_interDT.Properties.Resources.Eraser_16x16;
            this.cmdCleanNQ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdCleanNQ.Location = new System.Drawing.Point(650, 88);
            this.cmdCleanNQ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdCleanNQ.Name = "cmdCleanNQ";
            this.cmdCleanNQ.Size = new System.Drawing.Size(85, 39);
            this.cmdCleanNQ.TabIndex = 607;
            this.cmdCleanNQ.Text = "   Clean";
            this.cmdCleanNQ.UseVisualStyleBackColor = false;
            this.cmdCleanNQ.Click += new System.EventHandler(this.cmdCleanNQ_Click);
            // 
            // txtRefFournNQ
            // 
            this.txtRefFournNQ.AccAcceptNumbersOnly = false;
            this.txtRefFournNQ.AccAllowComma = false;
            this.txtRefFournNQ.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRefFournNQ.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRefFournNQ.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtRefFournNQ.AccHidenValue = "";
            this.txtRefFournNQ.AccNotAllowedChars = null;
            this.txtRefFournNQ.AccReadOnly = false;
            this.txtRefFournNQ.AccReadOnlyAllowDelete = false;
            this.txtRefFournNQ.AccRequired = false;
            this.txtRefFournNQ.BackColor = System.Drawing.Color.White;
            this.txtRefFournNQ.CustomBackColor = System.Drawing.Color.White;
            this.txtRefFournNQ.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtRefFournNQ.ForeColor = System.Drawing.Color.Black;
            this.txtRefFournNQ.Location = new System.Drawing.Point(85, 49);
            this.txtRefFournNQ.Margin = new System.Windows.Forms.Padding(2);
            this.txtRefFournNQ.MaxLength = 32767;
            this.txtRefFournNQ.Multiline = false;
            this.txtRefFournNQ.Name = "txtRefFournNQ";
            this.txtRefFournNQ.ReadOnly = false;
            this.txtRefFournNQ.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRefFournNQ.Size = new System.Drawing.Size(154, 27);
            this.txtRefFournNQ.TabIndex = 599;
            this.txtRefFournNQ.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRefFournNQ.UseSystemPasswordChar = false;
            this.txtRefFournNQ.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRefFournNQ_KeyPress);
            // 
            // cmdAppliquerNQ
            // 
            this.cmdAppliquerNQ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAppliquerNQ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAppliquerNQ.FlatAppearance.BorderSize = 0;
            this.cmdAppliquerNQ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAppliquerNQ.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdAppliquerNQ.ForeColor = System.Drawing.Color.White;
            this.cmdAppliquerNQ.Image = global::Axe_interDT.Properties.Resources.Save_16x16;
            this.cmdAppliquerNQ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAppliquerNQ.Location = new System.Drawing.Point(857, 88);
            this.cmdAppliquerNQ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAppliquerNQ.Name = "cmdAppliquerNQ";
            this.cmdAppliquerNQ.Size = new System.Drawing.Size(101, 38);
            this.cmdAppliquerNQ.TabIndex = 605;
            this.cmdAppliquerNQ.Text = "      Appliquer";
            this.cmdAppliquerNQ.UseVisualStyleBackColor = false;
            this.cmdAppliquerNQ.Click += new System.EventHandler(this.cmdAppliquerNQ_Click);
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label14.Location = new System.Drawing.Point(8, 55);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(70, 16);
            this.Label14.TabIndex = 598;
            this.Label14.Text = "Réf. fourn :";
            // 
            // cmdRechercheNonQualif
            // 
            this.cmdRechercheNonQualif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheNonQualif.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdRechercheNonQualif.FlatAppearance.BorderSize = 0;
            this.cmdRechercheNonQualif.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheNonQualif.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdRechercheNonQualif.ForeColor = System.Drawing.Color.White;
            this.cmdRechercheNonQualif.Image = global::Axe_interDT.Properties.Resources.Search_16x16;
            this.cmdRechercheNonQualif.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdRechercheNonQualif.Location = new System.Drawing.Point(739, 88);
            this.cmdRechercheNonQualif.Margin = new System.Windows.Forms.Padding(2);
            this.cmdRechercheNonQualif.Name = "cmdRechercheNonQualif";
            this.cmdRechercheNonQualif.Size = new System.Drawing.Size(114, 39);
            this.cmdRechercheNonQualif.TabIndex = 606;
            this.cmdRechercheNonQualif.Text = "     Rechercher";
            this.cmdRechercheNonQualif.UseVisualStyleBackColor = false;
            this.cmdRechercheNonQualif.Click += new System.EventHandler(this.cmdRechercheNonQualif_Click);
            // 
            // txtLibelleNQ
            // 
            this.txtLibelleNQ.AccAcceptNumbersOnly = false;
            this.txtLibelleNQ.AccAllowComma = false;
            this.txtLibelleNQ.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibelleNQ.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibelleNQ.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLibelleNQ.AccHidenValue = "";
            this.txtLibelleNQ.AccNotAllowedChars = null;
            this.txtLibelleNQ.AccReadOnly = false;
            this.txtLibelleNQ.AccReadOnlyAllowDelete = false;
            this.txtLibelleNQ.AccRequired = false;
            this.txtLibelleNQ.BackColor = System.Drawing.Color.White;
            this.txtLibelleNQ.CustomBackColor = System.Drawing.Color.White;
            this.txtLibelleNQ.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLibelleNQ.ForeColor = System.Drawing.Color.Black;
            this.txtLibelleNQ.Location = new System.Drawing.Point(551, 20);
            this.txtLibelleNQ.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelleNQ.MaxLength = 32767;
            this.txtLibelleNQ.Multiline = false;
            this.txtLibelleNQ.Name = "txtLibelleNQ";
            this.txtLibelleNQ.ReadOnly = false;
            this.txtLibelleNQ.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibelleNQ.Size = new System.Drawing.Size(297, 27);
            this.txtLibelleNQ.TabIndex = 597;
            this.txtLibelleNQ.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibelleNQ.UseSystemPasswordChar = false;
            this.txtLibelleNQ.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLibelleNQ_KeyPress);
            // 
            // Label13
            // 
            this.Label13.AutoSize = true;
            this.Label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label13.Location = new System.Drawing.Point(469, 20);
            this.Label13.Name = "Label13";
            this.Label13.Size = new System.Drawing.Size(54, 16);
            this.Label13.TabIndex = 596;
            this.Label13.Text = "Libellé :";
            // 
            // Label11
            // 
            this.Label11.AutoSize = true;
            this.Label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label11.Location = new System.Drawing.Point(8, 20);
            this.Label11.Name = "Label11";
            this.Label11.Size = new System.Drawing.Size(24, 16);
            this.Label11.TabIndex = 592;
            this.Label11.Tag = "1";
            this.Label11.Text = "ID:";
            // 
            // txtFamilleNQ
            // 
            this.txtFamilleNQ.AccAcceptNumbersOnly = false;
            this.txtFamilleNQ.AccAllowComma = false;
            this.txtFamilleNQ.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFamilleNQ.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFamilleNQ.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtFamilleNQ.AccHidenValue = "";
            this.txtFamilleNQ.AccNotAllowedChars = null;
            this.txtFamilleNQ.AccReadOnly = false;
            this.txtFamilleNQ.AccReadOnlyAllowDelete = false;
            this.txtFamilleNQ.AccRequired = false;
            this.txtFamilleNQ.BackColor = System.Drawing.Color.White;
            this.txtFamilleNQ.CustomBackColor = System.Drawing.Color.White;
            this.txtFamilleNQ.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtFamilleNQ.ForeColor = System.Drawing.Color.Black;
            this.txtFamilleNQ.Location = new System.Drawing.Point(334, 20);
            this.txtFamilleNQ.Margin = new System.Windows.Forms.Padding(2);
            this.txtFamilleNQ.MaxLength = 32767;
            this.txtFamilleNQ.Multiline = false;
            this.txtFamilleNQ.Name = "txtFamilleNQ";
            this.txtFamilleNQ.ReadOnly = false;
            this.txtFamilleNQ.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFamilleNQ.Size = new System.Drawing.Size(115, 27);
            this.txtFamilleNQ.TabIndex = 595;
            this.txtFamilleNQ.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFamilleNQ.UseSystemPasswordChar = false;
            this.txtFamilleNQ.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFamilleNQ_KeyPress);
            // 
            // txtIDGEC_DONNEES_NON_QUALIFIEES
            // 
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.AccAcceptNumbersOnly = false;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.AccAllowComma = false;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.AccHidenValue = "";
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.AccNotAllowedChars = null;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.AccReadOnly = false;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.AccReadOnlyAllowDelete = false;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.AccRequired = false;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.BackColor = System.Drawing.Color.White;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.CustomBackColor = System.Drawing.Color.White;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.ForeColor = System.Drawing.Color.Black;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.Location = new System.Drawing.Point(142, 20);
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.Margin = new System.Windows.Forms.Padding(2);
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.MaxLength = 32767;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.Multiline = false;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.Name = "txtIDGEC_DONNEES_NON_QUALIFIEES";
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.ReadOnly = false;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.Size = new System.Drawing.Size(97, 27);
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.TabIndex = 593;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.UseSystemPasswordChar = false;
            this.txtIDGEC_DONNEES_NON_QUALIFIEES.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIDGEC_DONNEES_NON_QUALIFIEES_KeyPress);
            // 
            // Label12
            // 
            this.Label12.AutoSize = true;
            this.Label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label12.Location = new System.Drawing.Point(244, 20);
            this.Label12.Name = "Label12";
            this.Label12.Size = new System.Drawing.Size(58, 16);
            this.Label12.TabIndex = 594;
            this.Label12.Text = "Famille :";
            // 
            // FraQualifie
            // 
            this.FraQualifie.BackColor = System.Drawing.Color.Transparent;
            this.FraQualifie.Controls.Add(this.lblTotal);
            this.FraQualifie.Controls.Add(this.CmdPanier);
            this.FraQualifie.Controls.Add(this.cmdHelp);
            this.FraQualifie.Controls.Add(this.cmdClean);
            this.FraQualifie.Controls.Add(this.cmdRechercher);
            this.FraQualifie.Controls.Add(this.cmdAppliquer);
            this.FraQualifie.Controls.Add(this.cmdAppliqueClose);
            this.FraQualifie.Controls.Add(this.Check2);
            this.FraQualifie.Controls.Add(this.chkTriPrix);
            this.FraQualifie.Controls.Add(this.Check1);
            this.FraQualifie.Controls.Add(this.cmbFourn);
            this.FraQualifie.Controls.Add(this.label1);
            this.FraQualifie.Controls.Add(this.txtMarque);
            this.FraQualifie.Controls.Add(this.Label16);
            this.FraQualifie.Controls.Add(this.txtRefFab);
            this.FraQualifie.Controls.Add(this.Label15);
            this.FraQualifie.Controls.Add(this.txtRefFourn);
            this.FraQualifie.Controls.Add(this.label2);
            this.FraQualifie.Controls.Add(this.txtSynonyme);
            this.FraQualifie.Controls.Add(this.Label17);
            this.FraQualifie.Controls.Add(this.txtLibelle);
            this.FraQualifie.Controls.Add(this.label3);
            this.FraQualifie.Controls.Add(this.txtFamille);
            this.FraQualifie.Controls.Add(this.label4);
            this.FraQualifie.Controls.Add(this.txtCodeChrono);
            this.FraQualifie.Controls.Add(this.label5);
            this.FraQualifie.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FraQualifie.Location = new System.Drawing.Point(2, 115);
            this.FraQualifie.Name = "FraQualifie";
            this.FraQualifie.Size = new System.Drawing.Size(1116, 144);
            this.FraQualifie.TabIndex = 592;
            this.FraQualifie.TabStop = false;
            // 
            // lblTotal
            // 
            this.lblTotal.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTotal.Location = new System.Drawing.Point(227, 90);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(217, 17);
            this.lblTotal.TabIndex = 611;
            // 
            // CmdPanier
            // 
            this.CmdPanier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdPanier.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdPanier.FlatAppearance.BorderSize = 0;
            this.CmdPanier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdPanier.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdPanier.ForeColor = System.Drawing.Color.White;
            this.CmdPanier.Image = global::Axe_interDT.Properties.Resources.Buy_16x16;
            this.CmdPanier.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdPanier.Location = new System.Drawing.Point(560, 92);
            this.CmdPanier.Margin = new System.Windows.Forms.Padding(2);
            this.CmdPanier.Name = "CmdPanier";
            this.CmdPanier.Size = new System.Drawing.Size(85, 39);
            this.CmdPanier.TabIndex = 610;
            this.CmdPanier.Text = "      Caddy";
            this.CmdPanier.UseVisualStyleBackColor = false;
            this.CmdPanier.Click += new System.EventHandler(this.CmdPanier_Click);
            // 
            // cmdHelp
            // 
            this.cmdHelp.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdHelp.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdHelp.FlatAppearance.BorderSize = 0;
            this.cmdHelp.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdHelp.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdHelp.ForeColor = System.Drawing.Color.White;
            this.cmdHelp.Image = global::Axe_interDT.Properties.Resources.Help_16x16;
            this.cmdHelp.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdHelp.Location = new System.Drawing.Point(471, 92);
            this.cmdHelp.Margin = new System.Windows.Forms.Padding(2);
            this.cmdHelp.Name = "cmdHelp";
            this.cmdHelp.Size = new System.Drawing.Size(85, 39);
            this.cmdHelp.TabIndex = 609;
            this.cmdHelp.Text = "      Aide";
            this.cmdHelp.UseVisualStyleBackColor = false;
            this.cmdHelp.Click += new System.EventHandler(this.cmdHelp_Click);
            // 
            // cmdClean
            // 
            this.cmdClean.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdClean.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdClean.FlatAppearance.BorderSize = 0;
            this.cmdClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClean.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdClean.ForeColor = System.Drawing.Color.White;
            this.cmdClean.Image = global::Axe_interDT.Properties.Resources.Eraser_16x16;
            this.cmdClean.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdClean.Location = new System.Drawing.Point(649, 92);
            this.cmdClean.Margin = new System.Windows.Forms.Padding(2);
            this.cmdClean.Name = "cmdClean";
            this.cmdClean.Size = new System.Drawing.Size(79, 39);
            this.cmdClean.TabIndex = 608;
            this.cmdClean.Text = "   Clean";
            this.cmdClean.UseVisualStyleBackColor = false;
            this.cmdClean.Click += new System.EventHandler(this.cmdClean_Click);
            // 
            // cmdRechercher
            // 
            this.cmdRechercher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercher.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdRechercher.FlatAppearance.BorderSize = 0;
            this.cmdRechercher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercher.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRechercher.ForeColor = System.Drawing.Color.White;
            this.cmdRechercher.Image = global::Axe_interDT.Properties.Resources.Search_16x16;
            this.cmdRechercher.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdRechercher.Location = new System.Drawing.Point(732, 92);
            this.cmdRechercher.Margin = new System.Windows.Forms.Padding(2);
            this.cmdRechercher.Name = "cmdRechercher";
            this.cmdRechercher.Size = new System.Drawing.Size(114, 39);
            this.cmdRechercher.TabIndex = 607;
            this.cmdRechercher.Text = "Rechercher";
            this.cmdRechercher.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdRechercher.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdRechercher.UseVisualStyleBackColor = false;
            this.cmdRechercher.Click += new System.EventHandler(this.cmdRechercher_Click);
            // 
            // cmdAppliquer
            // 
            this.cmdAppliquer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAppliquer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAppliquer.FlatAppearance.BorderSize = 0;
            this.cmdAppliquer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAppliquer.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAppliquer.ForeColor = System.Drawing.Color.White;
            this.cmdAppliquer.Image = global::Axe_interDT.Properties.Resources.Save_16x16;
            this.cmdAppliquer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAppliquer.Location = new System.Drawing.Point(850, 92);
            this.cmdAppliquer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAppliquer.Name = "cmdAppliquer";
            this.cmdAppliquer.Size = new System.Drawing.Size(101, 38);
            this.cmdAppliquer.TabIndex = 606;
            this.cmdAppliquer.Text = " Appliquer";
            this.cmdAppliquer.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAppliquer.TextImageRelation = System.Windows.Forms.TextImageRelation.ImageBeforeText;
            this.cmdAppliquer.UseVisualStyleBackColor = false;
            this.cmdAppliquer.Click += new System.EventHandler(this.cmdAppliquer_Click);
            // 
            // cmdAppliqueClose
            // 
            this.cmdAppliqueClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAppliqueClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAppliqueClose.FlatAppearance.BorderSize = 0;
            this.cmdAppliqueClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAppliqueClose.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAppliqueClose.ForeColor = System.Drawing.Color.White;
            this.cmdAppliqueClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAppliqueClose.Location = new System.Drawing.Point(955, 92);
            this.cmdAppliqueClose.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAppliqueClose.Name = "cmdAppliqueClose";
            this.cmdAppliqueClose.Size = new System.Drawing.Size(152, 38);
            this.cmdAppliqueClose.TabIndex = 605;
            this.cmdAppliqueClose.Text = "Appliquer et fermer";
            this.cmdAppliqueClose.UseVisualStyleBackColor = false;
            this.cmdAppliqueClose.Click += new System.EventHandler(this.cmdAppliqueClose_Click);
            // 
            // Check2
            // 
            this.Check2.AutoSize = true;
            this.Check2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check2.Location = new System.Drawing.Point(185, 115);
            this.Check2.Name = "Check2";
            this.Check2.Size = new System.Drawing.Size(254, 23);
            this.Check2.TabIndex = 604;
            this.Check2.Text = "Seuls les articles dans nos stocks";
            this.Check2.UseVisualStyleBackColor = true;
            this.Check2.Visible = false;
            // 
            // chkTriPrix
            // 
            this.chkTriPrix.AutoSize = true;
            this.chkTriPrix.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkTriPrix.Location = new System.Drawing.Point(6, 116);
            this.chkTriPrix.Name = "chkTriPrix";
            this.chkTriPrix.Size = new System.Drawing.Size(179, 23);
            this.chkTriPrix.TabIndex = 603;
            this.chkTriPrix.Text = "Tri décroissant du prix";
            this.chkTriPrix.UseVisualStyleBackColor = true;
            this.chkTriPrix.Visible = false;
            // 
            // Check1
            // 
            this.Check1.AutoSize = true;
            this.Check1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check1.Location = new System.Drawing.Point(6, 90);
            this.Check1.Name = "Check1";
            this.Check1.Size = new System.Drawing.Size(246, 23);
            this.Check1.TabIndex = 587;
            this.Check1.Text = "Seuls les articles du fournisseur";
            this.Check1.UseVisualStyleBackColor = true;
            this.Check1.Visible = false;
            // 
            // cmbFourn
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbFourn.DisplayLayout.Appearance = appearance13;
            this.cmbFourn.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbFourn.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbFourn.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbFourn.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.cmbFourn.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbFourn.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.cmbFourn.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbFourn.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbFourn.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbFourn.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.cmbFourn.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbFourn.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.cmbFourn.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbFourn.DisplayLayout.Override.CellAppearance = appearance20;
            this.cmbFourn.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbFourn.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbFourn.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.cmbFourn.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.cmbFourn.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbFourn.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.cmbFourn.DisplayLayout.Override.RowAppearance = appearance23;
            this.cmbFourn.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbFourn.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.cmbFourn.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbFourn.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbFourn.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbFourn.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbFourn.Location = new System.Drawing.Point(992, 55);
            this.cmbFourn.Name = "cmbFourn";
            this.cmbFourn.Size = new System.Drawing.Size(115, 27);
            this.cmbFourn.TabIndex = 586;
            this.cmbFourn.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbFourn_BeforeDropDown);
            this.cmbFourn.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbFourn_KeyPress);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(891, 63);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(98, 19);
            this.label1.TabIndex = 585;
            this.label1.Text = "Fournisseur :";
            // 
            // txtMarque
            // 
            this.txtMarque.AccAcceptNumbersOnly = false;
            this.txtMarque.AccAllowComma = false;
            this.txtMarque.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMarque.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMarque.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtMarque.AccHidenValue = "";
            this.txtMarque.AccNotAllowedChars = null;
            this.txtMarque.AccReadOnly = false;
            this.txtMarque.AccReadOnlyAllowDelete = false;
            this.txtMarque.AccRequired = false;
            this.txtMarque.BackColor = System.Drawing.Color.White;
            this.txtMarque.CustomBackColor = System.Drawing.Color.White;
            this.txtMarque.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtMarque.ForeColor = System.Drawing.Color.Black;
            this.txtMarque.Location = new System.Drawing.Point(475, 53);
            this.txtMarque.Margin = new System.Windows.Forms.Padding(2);
            this.txtMarque.MaxLength = 32767;
            this.txtMarque.Multiline = false;
            this.txtMarque.Name = "txtMarque";
            this.txtMarque.ReadOnly = false;
            this.txtMarque.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMarque.Size = new System.Drawing.Size(307, 27);
            this.txtMarque.TabIndex = 584;
            this.txtMarque.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMarque.UseSystemPasswordChar = false;
            this.txtMarque.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtMarque_KeyPress);
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label16.Location = new System.Drawing.Point(403, 59);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(70, 19);
            this.Label16.TabIndex = 583;
            this.Label16.Text = "Marque :";
            // 
            // txtRefFab
            // 
            this.txtRefFab.AccAcceptNumbersOnly = false;
            this.txtRefFab.AccAllowComma = false;
            this.txtRefFab.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRefFab.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRefFab.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtRefFab.AccHidenValue = "";
            this.txtRefFab.AccNotAllowedChars = null;
            this.txtRefFab.AccReadOnly = false;
            this.txtRefFab.AccReadOnlyAllowDelete = false;
            this.txtRefFab.AccRequired = false;
            this.txtRefFab.BackColor = System.Drawing.Color.White;
            this.txtRefFab.CustomBackColor = System.Drawing.Color.White;
            this.txtRefFab.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtRefFab.ForeColor = System.Drawing.Color.Black;
            this.txtRefFab.Location = new System.Drawing.Point(287, 53);
            this.txtRefFab.Margin = new System.Windows.Forms.Padding(2);
            this.txtRefFab.MaxLength = 32767;
            this.txtRefFab.Multiline = false;
            this.txtRefFab.Name = "txtRefFab";
            this.txtRefFab.ReadOnly = false;
            this.txtRefFab.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRefFab.Size = new System.Drawing.Size(115, 27);
            this.txtRefFab.TabIndex = 582;
            this.txtRefFab.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRefFab.UseSystemPasswordChar = false;
            this.txtRefFab.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRefFab_KeyPress);
            // 
            // Label15
            // 
            this.Label15.AutoSize = true;
            this.Label15.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label15.Location = new System.Drawing.Point(187, 59);
            this.Label15.Name = "Label15";
            this.Label15.Size = new System.Drawing.Size(108, 19);
            this.Label15.TabIndex = 581;
            this.Label15.Text = "Réf. fabricant :";
            // 
            // txtRefFourn
            // 
            this.txtRefFourn.AccAcceptNumbersOnly = false;
            this.txtRefFourn.AccAllowComma = false;
            this.txtRefFourn.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRefFourn.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRefFourn.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtRefFourn.AccHidenValue = "";
            this.txtRefFourn.AccNotAllowedChars = null;
            this.txtRefFourn.AccReadOnly = false;
            this.txtRefFourn.AccReadOnlyAllowDelete = false;
            this.txtRefFourn.AccRequired = false;
            this.txtRefFourn.BackColor = System.Drawing.Color.White;
            this.txtRefFourn.CustomBackColor = System.Drawing.Color.White;
            this.txtRefFourn.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtRefFourn.ForeColor = System.Drawing.Color.Black;
            this.txtRefFourn.Location = new System.Drawing.Point(75, 53);
            this.txtRefFourn.Margin = new System.Windows.Forms.Padding(2);
            this.txtRefFourn.MaxLength = 32767;
            this.txtRefFourn.Multiline = false;
            this.txtRefFourn.Name = "txtRefFourn";
            this.txtRefFourn.ReadOnly = false;
            this.txtRefFourn.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRefFourn.Size = new System.Drawing.Size(107, 27);
            this.txtRefFourn.TabIndex = 580;
            this.txtRefFourn.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRefFourn.UseSystemPasswordChar = false;
            this.txtRefFourn.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRefFourn_KeyPress);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(-1, 59);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 19);
            this.label2.TabIndex = 579;
            this.label2.Text = "Réf. fourn :";
            // 
            // txtSynonyme
            // 
            this.txtSynonyme.AccAcceptNumbersOnly = false;
            this.txtSynonyme.AccAllowComma = false;
            this.txtSynonyme.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSynonyme.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSynonyme.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtSynonyme.AccHidenValue = "";
            this.txtSynonyme.AccNotAllowedChars = null;
            this.txtSynonyme.AccReadOnly = false;
            this.txtSynonyme.AccReadOnlyAllowDelete = false;
            this.txtSynonyme.AccRequired = false;
            this.txtSynonyme.BackColor = System.Drawing.Color.White;
            this.txtSynonyme.CustomBackColor = System.Drawing.Color.White;
            this.txtSynonyme.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtSynonyme.ForeColor = System.Drawing.Color.Black;
            this.txtSynonyme.Location = new System.Drawing.Point(992, 20);
            this.txtSynonyme.Margin = new System.Windows.Forms.Padding(2);
            this.txtSynonyme.MaxLength = 32767;
            this.txtSynonyme.Multiline = false;
            this.txtSynonyme.Name = "txtSynonyme";
            this.txtSynonyme.ReadOnly = false;
            this.txtSynonyme.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSynonyme.Size = new System.Drawing.Size(115, 27);
            this.txtSynonyme.TabIndex = 578;
            this.txtSynonyme.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSynonyme.UseSystemPasswordChar = false;
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label17.Location = new System.Drawing.Point(897, 24);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(80, 19);
            this.Label17.TabIndex = 577;
            this.Label17.Text = "Synonyme";
            // 
            // txtLibelle
            // 
            this.txtLibelle.AccAcceptNumbersOnly = false;
            this.txtLibelle.AccAllowComma = false;
            this.txtLibelle.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibelle.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibelle.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLibelle.AccHidenValue = "";
            this.txtLibelle.AccNotAllowedChars = null;
            this.txtLibelle.AccReadOnly = false;
            this.txtLibelle.AccReadOnlyAllowDelete = false;
            this.txtLibelle.AccRequired = false;
            this.txtLibelle.BackColor = System.Drawing.Color.White;
            this.txtLibelle.CustomBackColor = System.Drawing.Color.White;
            this.txtLibelle.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtLibelle.ForeColor = System.Drawing.Color.Black;
            this.txtLibelle.Location = new System.Drawing.Point(475, 20);
            this.txtLibelle.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelle.MaxLength = 32767;
            this.txtLibelle.Multiline = false;
            this.txtLibelle.Name = "txtLibelle";
            this.txtLibelle.ReadOnly = false;
            this.txtLibelle.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibelle.Size = new System.Drawing.Size(307, 27);
            this.txtLibelle.TabIndex = 576;
            this.txtLibelle.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibelle.UseSystemPasswordChar = false;
            this.txtLibelle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLibelle_KeyPress);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(408, 22);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(62, 19);
            this.label3.TabIndex = 575;
            this.label3.Text = "Libellé :";
            // 
            // txtFamille
            // 
            this.txtFamille.AccAcceptNumbersOnly = false;
            this.txtFamille.AccAllowComma = false;
            this.txtFamille.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFamille.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFamille.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtFamille.AccHidenValue = "";
            this.txtFamille.AccNotAllowedChars = null;
            this.txtFamille.AccReadOnly = false;
            this.txtFamille.AccReadOnlyAllowDelete = false;
            this.txtFamille.AccRequired = false;
            this.txtFamille.BackColor = System.Drawing.Color.White;
            this.txtFamille.CustomBackColor = System.Drawing.Color.White;
            this.txtFamille.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtFamille.ForeColor = System.Drawing.Color.Black;
            this.txtFamille.Location = new System.Drawing.Point(287, 20);
            this.txtFamille.Margin = new System.Windows.Forms.Padding(2);
            this.txtFamille.MaxLength = 32767;
            this.txtFamille.Multiline = false;
            this.txtFamille.Name = "txtFamille";
            this.txtFamille.ReadOnly = false;
            this.txtFamille.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFamille.Size = new System.Drawing.Size(116, 27);
            this.txtFamille.TabIndex = 574;
            this.txtFamille.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFamille.UseSystemPasswordChar = false;
            this.txtFamille.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFamille_KeyPress);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(187, 22);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(64, 19);
            this.label4.TabIndex = 573;
            this.label4.Text = "Famille :";
            // 
            // txtCodeChrono
            // 
            this.txtCodeChrono.AccAcceptNumbersOnly = false;
            this.txtCodeChrono.AccAllowComma = false;
            this.txtCodeChrono.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeChrono.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeChrono.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCodeChrono.AccHidenValue = "";
            this.txtCodeChrono.AccNotAllowedChars = null;
            this.txtCodeChrono.AccReadOnly = false;
            this.txtCodeChrono.AccReadOnlyAllowDelete = false;
            this.txtCodeChrono.AccRequired = false;
            this.txtCodeChrono.BackColor = System.Drawing.Color.White;
            this.txtCodeChrono.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeChrono.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCodeChrono.ForeColor = System.Drawing.Color.Black;
            this.txtCodeChrono.Location = new System.Drawing.Point(75, 22);
            this.txtCodeChrono.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeChrono.MaxLength = 32767;
            this.txtCodeChrono.Multiline = false;
            this.txtCodeChrono.Name = "txtCodeChrono";
            this.txtCodeChrono.ReadOnly = false;
            this.txtCodeChrono.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeChrono.Size = new System.Drawing.Size(107, 27);
            this.txtCodeChrono.TabIndex = 572;
            this.txtCodeChrono.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeChrono.UseSystemPasswordChar = false;
            this.txtCodeChrono.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeChrono_KeyPress);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(2, 28);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(68, 19);
            this.label5.TabIndex = 571;
            this.label5.Tag = "1";
            this.label5.Text = "Chrono :";
            // 
            // groupBox6
            // 
            this.groupBox6.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.GridGecetNonQualif);
            this.groupBox6.Controls.Add(this.GridGecet);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox6.Location = new System.Drawing.Point(2, 271);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1119, 368);
            this.groupBox6.TabIndex = 593;
            this.groupBox6.TabStop = false;
            // 
            // GridGecetNonQualif
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridGecetNonQualif.DisplayLayout.Appearance = appearance25;
            this.GridGecetNonQualif.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridGecetNonQualif.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.GridGecetNonQualif.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridGecetNonQualif.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.GridGecetNonQualif.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridGecetNonQualif.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.GridGecetNonQualif.DisplayLayout.MaxColScrollRegions = 1;
            this.GridGecetNonQualif.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridGecetNonQualif.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridGecetNonQualif.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.GridGecetNonQualif.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridGecetNonQualif.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.GridGecetNonQualif.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridGecetNonQualif.DisplayLayout.Override.CellAppearance = appearance32;
            this.GridGecetNonQualif.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridGecetNonQualif.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.GridGecetNonQualif.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.GridGecetNonQualif.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.GridGecetNonQualif.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridGecetNonQualif.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.GridGecetNonQualif.DisplayLayout.Override.RowAppearance = appearance35;
            this.GridGecetNonQualif.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridGecetNonQualif.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridGecetNonQualif.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.GridGecetNonQualif.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridGecetNonQualif.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridGecetNonQualif.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridGecetNonQualif.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GridGecetNonQualif.Location = new System.Drawing.Point(3, 18);
            this.GridGecetNonQualif.Name = "GridGecetNonQualif";
            this.GridGecetNonQualif.Size = new System.Drawing.Size(1113, 347);
            this.GridGecetNonQualif.TabIndex = 575;
            this.GridGecetNonQualif.Text = "ultraGrid1";
            this.GridGecetNonQualif.Visible = false;
            this.GridGecetNonQualif.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridGecetNonQualif_InitializeLayout);
            this.GridGecetNonQualif.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.GridGecetNonQualif_BeforeExitEditMode);
            this.GridGecetNonQualif.BeforeCellUpdate += new Infragistics.Win.UltraWinGrid.BeforeCellUpdateEventHandler(this.GridGecetNonQualif_BeforeCellUpdate);
            // 
            // GridGecet
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridGecet.DisplayLayout.Appearance = appearance37;
            this.GridGecet.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridGecet.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.GridGecet.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridGecet.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.GridGecet.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridGecet.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.GridGecet.DisplayLayout.MaxColScrollRegions = 1;
            this.GridGecet.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridGecet.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridGecet.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.GridGecet.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridGecet.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.GridGecet.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridGecet.DisplayLayout.Override.CellAppearance = appearance44;
            this.GridGecet.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridGecet.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.GridGecet.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.GridGecet.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.GridGecet.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridGecet.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.GridGecet.DisplayLayout.Override.RowAppearance = appearance47;
            this.GridGecet.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridGecet.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridGecet.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.GridGecet.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridGecet.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridGecet.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridGecet.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GridGecet.Location = new System.Drawing.Point(3, 18);
            this.GridGecet.Name = "GridGecet";
            this.GridGecet.Size = new System.Drawing.Size(1113, 347);
            this.GridGecet.TabIndex = 574;
            this.GridGecet.Text = "ultraGrid1";
            this.GridGecet.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridGecet_InitializeLayout);
            this.GridGecet.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.GridGecet_ClickCellButton);
            this.GridGecet.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.GridGecet_BeforeExitEditMode);
            // 
            // frmGecetV3
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1126, 639);
            this.Controls.Add(this.FraQualifie);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.FraNonQualifie);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.Frame1);
            this.Name = "frmGecetV3";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Recherche d\'articles Gecet V3";
            this.Activated += new System.EventHandler(this.frmGecetV3_Activated);
            this.Load += new System.EventHandler(this.frmGecetV3_Load);
            this.Frame1.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.panel1.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            this.FraNonQualifie.ResumeLayout(false);
            this.FraNonQualifie.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFournNQ)).EndInit();
            this.FraQualifie.ResumeLayout(false);
            this.FraQualifie.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbFourn)).EndInit();
            this.groupBox6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridGecetNonQualif)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridGecet)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.GroupBox Frame1;
        private System.Windows.Forms.Panel panel1;
        public System.Windows.Forms.RadioButton OptEt;
        public System.Windows.Forms.RadioButton OptOu;
        public System.Windows.Forms.RadioButton optEspace;
        public System.Windows.Forms.RadioButton optEtouOU;
        private System.Windows.Forms.Panel panel2;
        public System.Windows.Forms.RadioButton optBaseQualifie;
        public System.Windows.Forms.RadioButton optBaseNonQualifie;
        public System.Windows.Forms.GroupBox FraNonQualifie;
        public System.Windows.Forms.Button CmdPanierNQ;
        public System.Windows.Forms.CheckBox chkTriPrixNQ;
        public System.Windows.Forms.Button cmdHelpNQ;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbFournNQ;
        public System.Windows.Forms.Button cmdAppliqueCloseNQ;
        public System.Windows.Forms.Label Label10;
        public System.Windows.Forms.Button cmdCleanNQ;
        public iTalk.iTalk_TextBox_Small2 txtRefFournNQ;
        public System.Windows.Forms.Button cmdAppliquerNQ;
        public System.Windows.Forms.Label Label14;
        public System.Windows.Forms.Button cmdRechercheNonQualif;
        public iTalk.iTalk_TextBox_Small2 txtLibelleNQ;
        public System.Windows.Forms.Label Label13;
        public System.Windows.Forms.Label Label11;
        public iTalk.iTalk_TextBox_Small2 txtFamilleNQ;
        public iTalk.iTalk_TextBox_Small2 txtIDGEC_DONNEES_NON_QUALIFIEES;
        public System.Windows.Forms.Label Label12;
        public System.Windows.Forms.GroupBox FraQualifie;
        public System.Windows.Forms.CheckBox Check2;
        public System.Windows.Forms.CheckBox chkTriPrix;
        public System.Windows.Forms.CheckBox Check1;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbFourn;
        public System.Windows.Forms.Label label1;
        public iTalk.iTalk_TextBox_Small2 txtMarque;
        public System.Windows.Forms.Label Label16;
        public iTalk.iTalk_TextBox_Small2 txtRefFab;
        public System.Windows.Forms.Label Label15;
        public iTalk.iTalk_TextBox_Small2 txtRefFourn;
        public System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtSynonyme;
        public System.Windows.Forms.Label Label17;
        public iTalk.iTalk_TextBox_Small2 txtLibelle;
        public System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 txtFamille;
        public System.Windows.Forms.Label label4;
        public iTalk.iTalk_TextBox_Small2 txtCodeChrono;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Button CmdPanier;
        public System.Windows.Forms.Button cmdHelp;
        public System.Windows.Forms.Button cmdClean;
        public System.Windows.Forms.Button cmdRechercher;
        public System.Windows.Forms.Button cmdAppliquer;
        public System.Windows.Forms.Button cmdAppliqueClose;
        public iTalk.iTalk_TextBox_Small2 txtType;
        public iTalk.iTalk_TextBox_Small2 txtPxHeure;
        public iTalk.iTalk_TextBox_Small2 txtTVA;
        public iTalk.iTalk_TextBox_Small2 txtKST;
        public iTalk.iTalk_TextBox_Small2 txtKMO;
        public iTalk.iTalk_TextBox_Small2 txtKFO;
        public iTalk.iTalk_TextBox_Small2 txtCle;
        public iTalk.iTalk_TextBox_Small2 txtNumLigne;
        public iTalk.iTalk_TextBox_Small2 txtOrigine;
        public System.Windows.Forms.GroupBox groupBox6;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridGecet;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridGecetNonQualif;
        public System.Windows.Forms.Label lblTotal;
        private System.Windows.Forms.Panel panel3;
        public iTalk.iTalk_TextBox_Small2 txtPreFixe;
    }
}