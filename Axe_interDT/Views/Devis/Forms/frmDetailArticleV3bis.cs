﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmDetailArticleV3bis : Form
    {
        DataTable rsUrl = null;
        SqlDataAdapter SDArsUrl = null;
        SqlCommandBuilder SCBrsUrl = null;
        public DataTable rsCond = null;
        public SqlDataAdapter SDArsCond = null;
        public SqlCommandBuilder SCBrsCond = null;
        public bool bLoadVS = false;

        public frmDetailArticleV3bis()
        {
            InitializeComponent();
        }
        public void fc_LoadArticle()
        {
            string sSQL = null;
            DataTable rs = null;
            SqlDataAdapter SDArs = null;

            rs = new DataTable();

            try
            {

                ModParametre.fc_OpenConnGecet();

                sSQL = "SELECT     CHRONO_INITIAL, ID_FAMILLE, LIBELLE_FAMILLE, LIBELLE_SOUS_FAMILLE, TYPE, COMPOSANT, FAMILLE_DLB, FAMILLE_ATOLL, "
                         + " REGROUPEMENT_ATOLL, DESIGNATION_ARTICLE, LIBELLE_TECHNIQUE, LIBELLE_DEVIS, SYNONYMES, ANCIENNE_REFERENCE, "
                         + " DATE_FIN_ACTIVITE, UNITE_CONDITIONNEMENT, CODE_UNITE_CONDITIONNEMENT, LIBELLE_UNITE_CONDITIONNEMENT, UNITE_BASE, "
                         + " CODE_UNITE_BASE, LIBELLE_UNITE_BASE, UNCS_SUR_UV, UNITE_DEVIS, CODE_UNITE_DEVIS, LIBELLE_UNITE_DEVIS, IMAGE, "
                         + " LIEN_URL_TECHNIQUE, LIEN_FICHE_SECURITE, CHRONO_INITIAL_ARTICLE_REMPLACEMENT, LONGUEUR, HAUTEUR, VOLUME, LARGEUR, "
                         + " SURFACE , POIDS_NET, CORPS_D_ETAT, NATURE, SPECIAL_BBATH, SUPPRIME_CHEZ_FABRIQUANT, sTatut "
                         + "  from Article "
                         + " WHERE     (CHRONO_INITIAL = '" + txtCHRONO_INITIAL.Text + "')";

                SDArs = new SqlDataAdapter(sSQL, ModParametre.adoGecet);
                SDArs.Fill(rs);

                fc_Load_Conditionnement(General.nz(txtCHRONO_INITIAL.Text, 0).ToString());

                if (rs.Rows.Count == 0)
                {
                    rs.Dispose();
                    rs = null;
                    return;

                }
                txtCHRONO_INITIAL.Text = rs.Rows[0]["CHRONO_INITIAL"] + "";
                txtID_FAMILLE.Text = rs.Rows[0]["ID_FAMILLE"] + "";
                txtLIBELLE_FAMILLE.Text = rs.Rows[0]["LIBELLE_FAMILLE"] + "";
                txtLIBELLE_SOUS_FAMILLE.Text = rs.Rows[0]["LIBELLE_SOUS_FAMILLE "] + "";
                txtTYPE.Text = rs.Rows[0]["Type"] + "";
                txtCOMPOSANT.Text = rs.Rows[0]["COMPOSANT "] + "";
                //'txtFAMILLE_DLB
                txtFAMILLE_ATOLL.Text = rs.Rows[0]["FAMILLE_ATOLL"] + "";
                txtREGROUPEMENT_ATOLL.Text = rs.Rows[0]["REGROUPEMENT_ATOLL"] + "";
                txtDESIGNATION_ARTICLE.Text = rs.Rows[0]["DESIGNATION_ARTICLE"] + "";
                txtLIBELLE_TECHNIQUE.Text = rs.Rows[0]["LIBELLE_TECHNIQUE"] + "";
                txtLIBELLE_DEVIS.Text = rs.Rows[0]["LIBELLE_DEVIS"] + "";
                txtNATURE.Text = rs.Rows[0]["SYNONYMES"] + "";
                txtANCIENNE_REFERENCE.Text = rs.Rows[0]["ANCIENNE_REFERENCE"] + "";
                txtDATE_FIN_ACTIVITE.Text = rs.Rows[0]["DATE_FIN_ACTIVITE "] + "";
                txtUNITE_CONDITIONNEMENT.Text = rs.Rows[0]["UNITE_CONDITIONNEMENT"] + "";
                txtCODE_UNITE_CONDITIONNEMENT.Text = rs.Rows[0]["CODE_UNITE_CONDITIONNEMENT"] + "";
                txtLIBELLE_UNITE_CONDITIONNEMENT.Text = rs.Rows[0]["LIBELLE_UNITE_CONDITIONNEMENT"] + "";
                txtUNITE_BASE.Text = rs.Rows[0]["UNITE_BASE"] + "";
                txtUNITE_DEVIS.Text = rs.Rows[0]["CODE_UNITE_BASE"] + "";
                txtLIBELLE_UNITE_BASE.Text = rs.Rows[0]["LIBELLE_UNITE_BASE"] + "";
                txtUNCS_SUR_UV.Text = rs.Rows[0]["UNCS_SUR_UV"] + "";
                txtUNITE_DEVIS.Text = rs.Rows[0]["UNITE_DEVIS"] + "";
                txtCODE_UNITE_DEVIS.Text = rs.Rows[0]["CODE_UNITE_DEVIS"] + "";
                txtLIBELLE_UNITE_DEVIS.Text = rs.Rows[0]["LIBELLE_UNITE_DEVIS"] + "";
                txtIMAGE.Text = rs.Rows[0]["Image"] + "";
                txtLIEN_URL_TECHNIQUE.Text = rs.Rows[0]["LIEN_URL_TECHNIQUE"] + "";
                txtLIEN_FICHE_SECURITE.Text = rs.Rows[0]["LIEN_FICHE_SECURITE"] + "";
                txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.Text = rs.Rows[0]["CHRONO_INITIAL_ARTICLE_REMPLACEMENT"] + "";
                txtLONGUEUR.Text = rs.Rows[0]["LONGUEUR"] + "";
                txtHAUTEUR.Text = rs.Rows[0]["HAUTEUR"] + "";
                txtVOLUME.Text = rs.Rows[0]["Volume"] + "";
                txtLARGEUR.Text = rs.Rows[0]["LARGEUR"] + "";
                txtSURFACE.Text = rs.Rows[0]["SURFACE"] + "";
                txtPOIDS_NET.Text = rs.Rows[0]["POIDS_NET"] + "";
                txtCORPS_D_ETAT.Text = rs.Rows[0]["CORPS_D_ETAT"] + "";
                txtNATURE.Text = rs.Rows[0]["NATURE"] + "";
                txtSPECIAL_BBATH.Text = rs.Rows[0]["SPECIAL_BBATH"] + "";
                txtSUPPRIME_CHEZ_FABRIQUANT.Text = rs.Rows[0]["SUPPRIME_CHEZ_FABRIQUANT"] + "";
                txtsTatut.Text = rs.Rows[0]["sTatut"] + "";

                rs.Dispose();

                sSQL = "SELECT     IDGEC_LIGNE_PRIX, CODE_FOURNISSEUR_ADHERENT, NOM_FOURNISSEUR, CHRONO_INITIAL, DESIGNATION_ARTICLE, LIBELLE_TECHNIQUE, "
                        + " DATE_APPLICATION, LIBELLE_MARQUE, LIBELLE_STATUT_CONTROLE, REF_FABRICANT, REF_FOURNISSEUR, REF_HISTORIQUE, "
                        + " PRIX_NET_DETAILS, PRIX_PUBLIC_DETAILS, CODE_UNITE_VALEUR, LIBELLE_UNITE_VALEUR, QTE_UNITE_VALEUR, CODE_COND_STOCK, "
                        + " LIBELLE_COND_STOCK, QTE_COND_STOCK, CODE_UNITE_STOCK, LIBELLE_UNITE_STOCK, QTE_UNITE_STOCK, CODE_UNITE_DEVIS, "
                        + " LIBELLE_UNITE_DEVIS, QTE_UNITE_DEVIS, CODE_UNITE_METIER, LIBELLE_UNITE_METIER, QTE_UNITE_METIER, CODE_COND_FOURNISSEUR, "
                        + " LIBELLE_COND_FOURNISSEUR, QTE_COND_FOURNISSEUR, QTE_MIN_CMD_UCNF, NB_UC_DANS_UV, CODE_UNITE_FOURNISSEUR, "
                        + " LIBELLE_UNITE_FOURNISSEUR, QTE_UNITE_FOURNISSEUR, QTE_MIN_CMD_UC, PRIX_GECET, LIBELLE_UNITE_PRIX_GECET, "
                        + " PRIX_GECET_UNITE, LIBELLE_UNITE_PRIX_GECET_UNITE, PRIX_FABRICANT, LIBELLE_UNITE_PRIX_FABRICANT, PRIX_DISTRIBUTEUR, "
                        + " LIBELLE_UNITE_PRIX_DISTRIBUTEUR, PRIX_COND_FOUR, LIBELLE_COND_PRIX_COND_FOURNISSEUR, PRIX_STOCK, "
                        + " LIBELLE_UNITE_PRIX_STOCK, PRIX_COND_STOCK, LIBELLE_COND_PRIX_COND_STOCK, TAXE, PRX_DEVIS_NET_UD, "
                        + " LIBELLE_UNITE_PRIX_DEVIS, PRIX_DEVIS_MAJORE, LIBELLE_UNITE_PRIX_DEVIS_MAJORE, UNCF_SUR_UC, UD_SUR_US, US_SUR_UC,"
                        + " US_SUR_UV , PRIX_DONNE_PAR, DATE_CREATION "
                        + " FROM         ArticleTarif WHERE IDGEC_LIGNE_PRIX = " + txtIDGEC_LIGNE_PRIX;

                SDArs = new SqlDataAdapter(sSQL, ModParametre.adoGecet);
                SDArs.Fill(rs);

                if (rs.Rows.Count > 0)
                {
                    txtIDGEC_LIGNE_PRIX.Text = rs.Rows[0]["IDGEC_LIGNE_PRIX"] + "";
                    txtCODE_FOURNISSEUR_ADHERENT.Text = rs.Rows[0]["CODE_FOURNISSEUR_ADHERENT"] + "";
                    txtNOM_FOURNISSEUR.Text = rs.Rows[0]["NOM_FOURNISSEUR"] + "";
                    txtCHRONO_INITIAL_2.Text = rs.Rows[0]["CHRONO_INITIAL"] + "";
                    txtDESIGNATION_ARTICLE_2.Text = rs.Rows[0]["DESIGNATION_ARTICLE"] + "";
                    txtLIBELLE_TECHNIQUE_2.Text = rs.Rows[0]["LIBELLE_TECHNIQUE"] + "";
                    txtDATE_APPLICATION.Text = rs.Rows[0]["DATE_APPLICATION"] + "";
                    txtLIBELLE_MARQUE.Text = rs.Rows[0]["LIBELLE_MARQUE"] + "";
                    txtCODE_UNITE_VALEUR.Text = rs.Rows[0]["LIBELLE_STATUT_CONTROLE"] + "";
                    txtREF_FABRICANT.Text = rs.Rows[0]["REF_FABRICANT"] + "";
                    txtREF_FOURNISSEUR.Text = rs.Rows[0]["REF_FOURNISSEUR"] + "";
                    txtREF_HISTORIQUE.Text = rs.Rows[0]["REF_HISTORIQUE"] + "";
                    txtPRIX_NET_DETAILS.Text = rs.Rows[0]["PRIX_NET_DETAILS"] + "";
                    txtPRIX_PUBLIC_DETAILS.Text = rs.Rows[0]["PRIX_PUBLIC_DETAILS"] + "";
                    txtCODE_UNITE_VALEUR.Text = rs.Rows[0]["CODE_UNITE_VALEUR"] + "";
                    txtLIBELLE_UNITE_VALEUR.Text = rs.Rows[0]["LIBELLE_UNITE_VALEUR"] + "";
                    txtQTE_UNITE_VALEUR.Text = rs.Rows[0]["QTE_UNITE_VALEUR"] + "";
                    txtCODE_COND_STOCK.Text = rs.Rows[0]["CODE_COND_STOCK"] + "";
                    txtLIBELLE_COND_STOCK.Text = rs.Rows[0]["LIBELLE_COND_STOCK"] + "";
                    txtQTE_COND_STOCK.Text = rs.Rows[0]["QTE_COND_STOCK"] + "";
                    txtCODE_UNITE_STOCK.Text = rs.Rows[0]["CODE_UNITE_STOCK"] + "";
                    txtLIBELLE_UNITE_STOCK.Text = rs.Rows[0]["LIBELLE_UNITE_STOCK"] + "";
                    txtQTE_UNITE_STOCK.Text = rs.Rows[0]["QTE_UNITE_STOCK"] + "";
                    txtCODE_UNITE_DEVIS_2.Text = rs.Rows[0]["CODE_UNITE_DEVIS"] + "";
                    txtLIBELLE_UNITE_DEVIS_2.Text = rs.Rows[0]["LIBELLE_UNITE_DEVIS"] + "";
                    txtQTE_UNITE_DEVIS.Text = rs.Rows[0]["QTE_UNITE_DEVIS"] + "";
                    txtCODE_UNITE_METIER.Text = rs.Rows[0]["CODE_UNITE_METIER"] + "";
                    txtLIBELLE_UNITE_METIER.Text = rs.Rows[0]["LIBELLE_UNITE_METIER"] + "";
                    txtQTE_UNITE_METIER.Text = rs.Rows[0]["QTE_UNITE_METIER"] + "";
                    txtCODE_COND_FOURNISSEUR.Text = rs.Rows[0]["CODE_COND_FOURNISSEUR"] + "";
                    txtLIBELLE_COND_FOURNISSEUR.Text = rs.Rows[0]["LIBELLE_COND_FOURNISSEUR"] + "";
                    txtQTE_COND_FOURNISSEUR.Text = rs.Rows[0]["QTE_COND_FOURNISSEUR"] + "";
                    txtQTE_MIN_CMD_UCNF.Text = rs.Rows[0]["QTE_MIN_CMD_UCNF"] + "";
                    txtNB_UC_DANS_UV.Text = rs.Rows[0]["NB_UC_DANS_UV"] + "";
                    txtCODE_UNITE_FOURNISSEUR.Text = rs.Rows[0]["CODE_UNITE_FOURNISSEUR"] + "";
                    txtLIBELLE_UNITE_FOURNISSEUR.Text = rs.Rows[0]["LIBELLE_UNITE_FOURNISSEUR"] + "";
                    txtQTE_UNITE_FOURNISSEUR.Text = rs.Rows[0]["QTE_UNITE_FOURNISSEUR"] + "";
                    txtQTE_MIN_CMD_UC.Text = rs.Rows[0]["QTE_MIN_CMD_UC"] + "";
                    txtPRIX_GECET.Text = rs.Rows[0]["PRIX_GECET"] + "";
                    txtLIBELLE_UNITE_PRIX_GECET.Text = rs.Rows[0]["LIBELLE_UNITE_PRIX_GECET"] + "";
                    txtPRIX_GECET_UNITE.Text = rs.Rows[0]["PRIX_GECET_UNITE"] + "";
                    txtLIBELLE_UNITE_PRIX_GECET_UNITE.Text = rs.Rows[0]["LIBELLE_UNITE_PRIX_GECET_UNITE"] + "";
                    txtPRIX_FABRICANT.Text = rs.Rows[0]["PRIX_FABRICANT"] + "";
                    txtLIBELLE_UNITE_PRIX_FABRICANT.Text = rs.Rows[0]["LIBELLE_UNITE_PRIX_FABRICANT"] + "";
                    txtPRIX_DISTRIBUTEUR.Text = rs.Rows[0]["PRIX_DISTRIBUTEUR"] + "";
                    txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.Text = rs.Rows[0]["LIBELLE_UNITE_PRIX_DISTRIBUTEUR"] + "";
                    txtPRIX_COND_FOUR_2.Text = rs.Rows[0]["PRIX_COND_FOUR"] + "";
                    txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.Text = rs.Rows[0]["LIBELLE_COND_PRIX_COND_FOURNISSEUR"] + "";
                    txtPRIX_STOCK.Text = rs.Rows[0]["PRIX_STOCK"] + "";
                    txtLIBELLE_UNITE_PRIX_STOCK.Text = rs.Rows[0]["LIBELLE_UNITE_PRIX_STOCK"] + "";
                    txtPRIX_COND_STOCK.Text = rs.Rows[0]["PRIX_COND_STOCK"] + "";
                    txtLIBELLE_COND_PRIX_COND_STOCK.Text = rs.Rows[0]["LIBELLE_COND_PRIX_COND_STOCK"] + "";
                    txtTAXE.Text = rs.Rows[0]["TAXE"] + "";
                    txtPRX_DEVIS_NET_UD.Text = rs.Rows[0]["PRX_DEVIS_NET_UD"] + "";
                    TXTLIBELLE_UNITE_PRIX_DEVIS.Text = rs.Rows[0]["LIBELLE_UNITE_PRIX_DEVIS"] + "";
                    txtPRIX_DEVIS_MAJORE.Text = rs.Rows[0]["PRIX_DEVIS_MAJORE"] + "";
                    txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.Text = rs.Rows[0]["LIBELLE_UNITE_PRIX_DEVIS_MAJORE"] + "";
                    txtUNCF_SUR_UC.Text = rs.Rows[0]["UNCF_SUR_UC"] + "";
                    TXTUD_SUR_US.Text = rs.Rows[0]["UD_SUR_US"] + "";
                    txtUS_SUR_UC.Text = rs.Rows[0]["US_SUR_UC"] + "";
                    txtUS_SUR_UV.Text = rs.Rows[0]["US_SUR_UV"] + "";
                    txtPRIX_DONNE_PAR.Text = rs.Rows[0]["PRIX_DONNE_PAR"] + "";
                    txtDATE_CREATION.Text = rs.Rows[0]["DATE_CREATION"] + "";
                }
                rs.Dispose();
                rs = null;
                ModParametre.fc_CloseConnGecet();

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, ";fc_loadArticle");
            }
        }

        private void frmDetailArticleV3bis_Load(object sender, EventArgs e)
        {
            try
            {
                View.Theme.Theme.recursiveLoopOnFrms(this);
                GridCond.Visible = false;              
                //'fc_LoadArticle
                bLoadVS = true;
                bLoadVS = false;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Form_Activate");
            }
        }

        private void txtLIEN_FICHE_SECURITE_Click(object sender, EventArgs e)
        {
            try
            {
                ModuleAPI.Ouvrir(txtLIEN_FICHE_SECURITE.Text);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtLIEN_FICHE_SECURITE_Click");
            }
        }

        private void txtLIEN_URL_TECHNIQUE_Click(object sender, EventArgs e)
        {
            try
            {
                ModuleAPI.Ouvrir(txtLIEN_URL_TECHNIQUE.Text);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtLIEN_URL_TECHNIQUE_Click");
            }
        }
        public void fc_Load_Conditionnement(string sChrono)
        {
            string sSQL = null;

            try
            {


                sSQL = "SELECT     IDGEC_CONDITIONNEMENT, CHRONO_ARTICLE, LIBELLE_ARTICLE, CODE_UNITE, DESIGNATION, QTE_PAR_UNITE"
                       + " From Conditionnement "
                       + " WHERE  CHRONO_ARTICLE = '" + sChrono + "'";


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                rsCond = new DataTable();
                SDArsCond = new SqlDataAdapter(sSQL, ModParametre.adoGecet);
                SDArsCond.Fill(rsCond);

                GridCond.DataSource = rsCond;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, ";fc_Load_Conditionnement");
            }
        }

        private void GridCond_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }

        private void GridCond_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridCond.DisplayLayout.Bands[0].Columns["CODE_UNITE"].Header.Caption = "Unité";
            GridCond.DisplayLayout.Bands[0].Columns["QTE_PAR_UNITE"].Header.Caption = "Qte/Unité";
            GridCond.DisplayLayout.Bands[0].Columns["IDGEC_CONDITIONNEMENT"].Hidden = true;
            GridCond.DisplayLayout.Bands[0].Columns["CHRONO_ARTICLE"].Hidden = true;
            GridCond.DisplayLayout.Bands[0].Columns["LIBELLE_ARTICLE"].Hidden = true;
            GridCond.DisplayLayout.Bands[0].Columns["DESIGNATION"].Hidden = true;
        }

        private void txtLIBELLE_TECHNIQUE_2_DoubleClick(object sender, EventArgs e)
        {
            try
            {
                ModuleAPI.Ouvrir(txtLIBELLE_TECHNIQUE_2.Text);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "; txtLIBELLE_TECHNIQUE_2_DoubleClick");
            }
        }
    }
}
