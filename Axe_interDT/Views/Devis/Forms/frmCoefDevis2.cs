﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmCoefDevis2 : Form
    {
        public frmCoefDevis2()
        {
            InitializeComponent();
        }
        string sSQL = null;
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// testd
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMAJ_Click(object sender, EventArgs e)
        {
            fc_MajCoef();
        }
        /// <summary>
        /// TEESTED
        /// </summary>
        private void fc_MajCoef()
        {
            try
            {
                if (!General.IsNumeric(txtCoefFO.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Valeur incorrecte, le coefficient doit être de type numérique.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                General.dbGenCoefGecet = Convert.ToDouble(txtCoefFO.Text);
                sSQL = "UPDATE    Lien Set adresse = " + General.dbGenCoefGecet + "WHERE   Code = 'CoefGECET'";
                General.Execute(sSQL);

            }
            catch (Exception ex) { Program.SaveException(ex); }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_Coef()
        {
            try
            {
                if (General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test)
                    sSQL = "SELECT  adresse From LienV2 WHERE Code = 'CoefGECET'";
                else
                    sSQL = "SELECT  adresse From Lien WHERE Code = 'CoefGECET'";

                using (var tmpModAdo = new ModAdo())
                    txtCoefFO.Text = tmpModAdo.fc_ADOlibelle(sSQL);
            }
            catch (Exception e) { Program.SaveException(e); }
        }

        private void frmCoefDevis2_Activated(object sender, EventArgs e)
        {
            fc_Coef();
        }
    }
}
