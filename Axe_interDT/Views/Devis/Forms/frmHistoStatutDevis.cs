﻿using Axe_interDT.Shared;
using System.Windows.Forms;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmHistoStatutDevis : Form
    {
        ModAdo histoModAdo = new ModAdo();
        public string numeroDevis { get; set; }

        public frmHistoStatutDevis()
        {
            InitializeComponent();
        }

        public frmHistoStatutDevis(string numeroDevis) : this()
        {
            this.numeroDevis = numeroDevis;
        }

        private void frmHistoStatutDevis_Activated(object sender, System.EventArgs e)
        {
            //===> Mondir le 25.06.2021 https://groupe-dt.mantishub.io/view.php?id=2508 added Date column [Date]
            GridHisto.DataSource = histoModAdo.fc_OpenRecordSet($"SELECT OldStatut as 'Ancien statut', NewStatut as 'Nouveau statut', [User] AS 'Utilisateur', Origine, [Date] FROM DevisHistoStatut WHERE NumeroDevis = '{numeroDevis}'");
        }

        private void GridHisto_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            //===> Mondir le 25.06.2021 https://groupe-dt.mantishub.io/view.php?id=2508 added Date column
            e.Layout.Bands[0].Columns["Date"].Format = "dd/MM/yyyy HH:mm:ss";
        }

        private void GridHisto_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            if (e.Row.Cells["Origine"].Value == null)
                return;

            if (e.Row.Cells["Origine"].Value.ToString().ToUpper() == "UserDocDevis".ToUpper())
                e.Row.Cells["Origine"].Value = "Fiche Devis";
            else if (e.Row.Cells["Origine"].Value.ToString().ToUpper() == "UserDocStandard".ToUpper())
                e.Row.Cells["Origine"].Value = "Fiche Appel";

        }
    }
}
