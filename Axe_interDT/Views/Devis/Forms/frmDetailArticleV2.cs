﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmDetailArticleV2 : Form
    {
        DataTable rsUrl = null;
        SqlDataAdapter SDArsUrl = null;
        SqlCommandBuilder SCBrsUrl = null;
        DataTable rsEquiv = null;
        SqlDataAdapter SDArsEquiv = null;
        SqlCommandBuilder SCBrsEquiv = null;
        public frmDetailArticleV2()
        {
            InitializeComponent();
        }
        private void fc_clearArt()
        {

            txtART_Famille.Text = "";
            txtART_SousFam.Text = "";
            txtART_Type.Text = "";
            txtART_Composant.Text = "";
            txtART_Libelle.Text = "";
            txtART_LibelleTechnique.Text = "";
            txtART_Synonyme.Text = "";
            txtART_PoidsTheo.Text = "";
            txtART_UnitPoidsTheo.Text = "";
            txtART_poids.Text = "";
            txtART_Longueur.Text = "";
            txtART_Largeur.Text = "";
            txtART_Surface.Text = "";
            txtART_UcnsParUs.Text = "";
            txtART_UnitsStock.Text = "";
            txtART_CorpEtat.Text = "";
            txtART_Nature.Text = "";
            txtART_FamAtoll.Text = "";
            txtART_EtatDiffusion.Text = "";
            txtART_Remplacement.Text = "";
            txtART_KfusParAncUs.Text = "";
            txtART_KfUdParAncUd.Text = "";
            txtART_ConnetEf.Text = "";
            txtART_ConnetEC.Text = "";
            txtART_Evacuation.Text = "";
            txtART_RaccordGaz.Text = "";
            txtART_DebitEau.Text = "";
            txtART_DebitGaz.Text = "";
            txtART_UniteDev.Text = "";
            txtART_dateFinActivite.Text = "";
            txtART_Marque.Text = "";
            txtART_Gamme.Text = "";
            txtART_LibelleDevServ.Text = "";
            txtART_familleGroupe.Text = "";
            txtART_Tva.Text = "";
            txtART_RubriqueAnaly.Text = "";
            txtART_CategorieOutil.Text = "";
            txtART_StatutVie.Text = "";
            txtART_SupprimeFab.Text = "";
            txtART_TypoAtoll.Text = "";
            txtART_NbLien.Text = "";

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sART_Chrono"></param>
        private void fc_AfficheArticle(string sART_Chrono)
        {
            string sSQL = null;
            DataTable rs = null;
            SqlDataAdapter SDArs = null;

            //SSTab1.TabVisible(0) = True
            //fraArticle.Visible = True
            //SSTab1.TabVisible(1) = False
            //fraPrix.Visible = False


            rs = new DataTable();

            sSQL = "SELECT     ART_Article.*" + " FROM         ART_Article" + " WHERE ART_Chrono ='" + sART_Chrono + "'";
            SDArs = new SqlDataAdapter(sSQL, ModParametre.adoGecet);
            SDArs.Fill(rs);

            if (rs.Rows.Count == 0)
            {
                fc_clearArt();
                rs=null;
                SDArs?.Dispose();
                return;
            }

            txtART_Chrono.Text = rs.Rows[0]["ART_Chrono"] + "";
            txtART_Famille.Text = rs.Rows[0]["ART_Famille"] + "";
            txtART_SousFam.Text = rs.Rows[0]["ART_SousFam"] + "";
            txtART_Type.Text = rs.Rows[0]["ART_Type"] + "";
            txtART_Composant.Text = rs.Rows[0]["ART_Composant"] + "";
            txtART_Libelle.Text = rs.Rows[0]["ART_Libelle"] + "";
            txtART_LibelleTechnique.Text = rs.Rows[0]["ART_LibelleTechnique"] + "";
            txtART_Synonyme.Text = rs.Rows[0]["ART_Synonyme"] + "";
            txtART_PoidsTheo.Text = rs.Rows[0]["ART_PoidsTheo"] + "";
            txtART_UnitPoidsTheo.Text = rs.Rows[0]["ART_UnitPoidsTheo"] + "";
            txtART_poids.Text = rs.Rows[0]["ART_poids"] + "";
            txtART_Longueur.Text = rs.Rows[0]["ART_Longueur"] + "";
            txtART_Largeur.Text = rs.Rows[0]["ART_Largeur"] + "";
            txtART_Surface.Text = rs.Rows[0]["ART_Surface"] + "";
            txtART_UcnsParUs.Text = rs.Rows[0]["ART_UcnsParUs"] + "";
            txtART_UnitsStock.Text = rs.Rows[0]["ART_UnitsStock"] + "";
            txtART_CorpEtat.Text = rs.Rows[0]["ART_CorpEtat"] + "";
            txtART_Nature.Text = rs.Rows[0]["ART_Nature"] + "";
            txtART_FamAtoll.Text = rs.Rows[0]["ART_FamAtoll"] + "";
            txtART_EtatDiffusion.Text = rs.Rows[0]["ART_EtatDiffusion"] + "";
            txtART_Remplacement.Text = rs.Rows[0]["ART_Remplacement"] + "";
            txtART_KfusParAncUs.Text = rs.Rows[0]["ART_KfusParAncUs"] + "";
            txtART_KfUdParAncUd.Text = rs.Rows[0]["ART_KfUdParAncUd"] + "";
            txtART_ConnetEf.Text = rs.Rows[0]["ART_ConnetEf"] + "";
            txtART_ConnetEC.Text = rs.Rows[0]["ART_ConnetEC"] + "";
            txtART_Evacuation.Text = rs.Rows[0]["ART_Evacuation"] + "";
            txtART_RaccordGaz.Text = rs.Rows[0]["ART_RaccordGaz"] + "";
            txtART_DebitEau.Text = rs.Rows[0]["ART_DebitEau"] + "";
            txtART_DebitGaz.Text = rs.Rows[0]["ART_DebitGaz"] + "";
            txtART_UniteDev.Text = rs.Rows[0]["ART_UniteDev"] + "";
            txtART_dateFinActivite.Text = rs.Rows[0]["ART_dateFinActivite"] + "";
            txtART_Marque.Text = rs.Rows[0]["ART_Marque"] + "";
            txtART_Gamme.Text = rs.Rows[0]["ART_Gamme"] + "";
            txtART_LibelleDevServ.Text = rs.Rows[0]["ART_LibelleDevServ"] + "";
            txtART_familleGroupe.Text = rs.Rows[0]["ART_familleGroupe"] + "";
            txtART_Tva.Text = rs.Rows[0]["ART_Tva"] + "";
            txtART_RubriqueAnaly.Text = rs.Rows[0]["ART_RubriqueAnaly"] + "";
            txtART_CategorieOutil.Text = rs.Rows[0]["ART_CategorieOutil"] + "";
            txtART_StatutVie.Text = rs.Rows[0]["ART_StatutVie"] + "";
            txtART_SupprimeFab.Text = rs.Rows[0]["ART_SupprimeFab"] + "";
            txtART_TypoAtoll.Text = rs.Rows[0]["ART_TypoAtoll"] + "";
            txtART_NbLien.Text = rs.Rows[0]["ART_NbLien"] + "";

            rs = null;
            SDArs?.Dispose();
         

        }
        private void fc_clearPrix()
        {

            txtART_Chrono.Text = "";
            txtARTP_Nofourn.Text = "";
            txtARTP_Nomfourn.Text = "";
            txtARTP_Marque.Text = "";
            txtARTP_RefFab.Text = "";
            txtARTP_RefFourn.Text = "";
            txtARTP_RefInfo.Text = "";
            txtARTP_RefHisto.Text = "";
            txtARTP_Devise.Text = "";
            txtARTP_Statut.Text = "";
            txtARTP_DelaiLivr.Text = "";
            txtARTP_QteMinUCNF.Text = "";
            txtARTP_QteMinUC.Text = "";
            txtARTP_CoefVente.Text = "";
            txtARTP_UniteValeur.Text = "";
            txtARTP_UnitCondFourn.Text = "";
            txtARTP_UniteFourn.Text = "";
            txtARTP_UniteDevis.Text = "";
            txtARTP_UcnfParUc.Text = "";
            txtARTP_UvParUc.Text = "";
            txtARTP_UdParUs.Text = "";
            txtARTP_UsParUc.Text = "";
            txtARTP_DateAplication.Text = "";
            txtARTP_PrixTarif.Text = "";
            txtARTP_PrixPublic.Text = "";
            txtARTP_PrixMulti.Text = "";
            txtARTP_PrixNetStock.Text = "";
            txtARTP_StatutPrixNet.Text = "";
            txtARTP_PrixNetCond.Text = "";
            txtARTP_LibelleCond.Text = "";
            txtARTP_PrixNet.Text = "";
            txtARTP_PrixNetDevis.Text = "";
            txtARTP_PrixNetDepan.Text = "";
            txtARTP_RemisePourCent.Text = "";
            txtARTP_RemiseIndic.Text = "";
            txtARTP_MajorDevis.Text = "";
            txtARTP_fournPref.Text = "";
            txtARTP_EcoTaxe.Text = "";
            txtARTP_PrixDevServ.Text = "";
            txtARTP_UsParUv.Text = "";
            txtARTP_PrixDonnePar.Text = "";
            txtARTP_DatePrix.Text = "";

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="lARTP_Noauto"></param>
        private void fc_AffichePrx(int lARTP_Noauto)
        {
            string sSQL = null;
            DataTable rs = null;
            SqlDataAdapter SDArs = null;

            //SSTab1.TabVisible(1) = True
            //SSTab1.TabVisible(0) = False
            //fraArticle.Visible = False

            //fraPrix.Visible = True


            sSQL = "SELECT     ARTP_ArticlePrix.*" + " FROM         ARTP_ArticlePrix" + " WHERE ARTP_NoAuto =" + lARTP_Noauto;

            rs = new DataTable();
            SDArs = new SqlDataAdapter(sSQL, ModParametre.adoGecet);
            SDArs.Fill(rs);

            if (rs.Rows.Count == 0)
            {
                fc_clearPrix();
                SDArs?.Dispose();
                rs =null;               
                return;
            }

            txtART_ChronoPrix.Text = rs.Rows[0]["ART_Chrono"] + "";
            txtARTP_Nofourn.Text = rs.Rows[0]["ARTP_Nofourn"] + "";
            txtARTP_Nomfourn.Text = rs.Rows[0]["ARTP_Nomfourn"] + "";
            txtARTP_Marque.Text = rs.Rows[0]["ARTP_Marque"] + "";
            txtARTP_RefFab.Text = rs.Rows[0]["ARTP_RefFab"] + "";
            txtARTP_RefFourn.Text = rs.Rows[0]["ARTP_RefFourn"] + "";
            txtARTP_RefInfo.Text = rs.Rows[0]["ARTP_RefInfo"] + "";
            txtARTP_RefHisto.Text = rs.Rows[0]["ARTP_RefHisto"] + "";
            txtARTP_Devise.Text = rs.Rows[0]["ARTP_Devise"] + "";
            txtARTP_Statut.Text = rs.Rows[0]["ARTP_Statut"] + "";
            txtARTP_DelaiLivr.Text = rs.Rows[0]["ARTP_DelaiLivr"] + "";
            txtARTP_QteMinUCNF.Text = rs.Rows[0]["ARTP_QteMinUCNF"] + "";
            txtARTP_QteMinUC.Text = rs.Rows[0]["ARTP_QteMinUC"] + "";
            txtARTP_CoefVente.Text = rs.Rows[0]["ARTP_CoefVente"] + "";
            txtARTP_UniteValeur.Text = rs.Rows[0]["ARTP_UniteValeur"] + "";
            txtARTP_UnitCondFourn.Text = rs.Rows[0]["ARTP_UnitCondFourn"] + "";
            txtARTP_UniteFourn.Text = rs.Rows[0]["ARTP_UniteFourn"] + "";
            txtARTP_UniteDevis.Text = rs.Rows[0]["ARTP_UniteDevis"] + "";
            txtARTP_UcnfParUc.Text = rs.Rows[0]["ARTP_UcnfParUc"] + "";
            txtARTP_UvParUc.Text = rs.Rows[0]["ARTP_UvParUc"] + "";
            txtARTP_UdParUs.Text = rs.Rows[0]["ARTP_UdParUs"] + "";
            txtARTP_UsParUc.Text = rs.Rows[0]["ARTP_UsParUc"] + "";
            txtARTP_DateAplication.Text = rs.Rows[0]["ARTP_DateAplication"] + "";
            txtARTP_PrixTarif.Text = rs.Rows[0]["ARTP_PrixTarif"] + "";
            txtARTP_PrixPublic.Text = rs.Rows[0]["ARTP_PrixPublic"] + "";
            txtARTP_PrixMulti.Text = rs.Rows[0]["ARTP_PrixMulti"] + "";
            txtARTP_PrixNetStock.Text = rs.Rows[0]["ARTP_PrixNetStock"] + "";
            txtARTP_StatutPrixNet.Text = rs.Rows[0]["ARTP_StatutPrixNet"] + "";
            txtARTP_PrixNetCond.Text = rs.Rows[0]["ARTP_PrixNetCond"] + "";
            txtARTP_LibelleCond.Text = rs.Rows[0]["ARTP_LibelleCond"] + "";
            txtARTP_PrixNet.Text = rs.Rows[0]["ARTP_PrixNet"] + "";
            txtARTP_PrixNetDevis.Text = rs.Rows[0]["ARTP_PrixNetDevis"] + "";
            txtARTP_PrixNetDepan.Text = rs.Rows[0]["ARTP_PrixNetDepan"] + "";
            txtARTP_RemisePourCent.Text = rs.Rows[0]["ARTP_RemisePourCent"] + "";
            txtARTP_RemiseIndic.Text = rs.Rows[0]["ARTP_RemiseIndic"] + "";
            txtARTP_MajorDevis.Text = rs.Rows[0]["ARTP_MajorDevis"] + "";
            txtARTP_fournPref.Text = rs.Rows[0]["ARTP_fournPref"] + "";
            txtARTP_EcoTaxe.Text = rs.Rows[0]["ARTP_EcoTaxe"] + "";
            txtARTP_PrixDevServ.Text = rs.Rows[0]["ARTP_PrixDevServ"] + "";
            txtARTP_UsParUv.Text = rs.Rows[0]["ARTP_UsParUv"] + "";
            txtARTP_PrixDonnePar.Text = rs.Rows[0]["ARTP_PrixDonnePar"] + "";
            txtARTP_DatePrix.Text = rs.Rows[0]["ARTP_DatePrix"] + "";
          
            rs = null;
            SDArs?.Dispose();
           

        }

       
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadURL()
        {
            string sSQL = null;

            try
            {
                sSQL = "SELECT     ART_Chrono, ARTU_NoLigne, ARTU_Table," + " ARTU_Description, ARTU_Famille, ARTU_NoFourn, ARTU_NomFourn," + " ARTU_Type, ARTU_URL, " 
                    + " ARTU_DateCreation , ARTU_creePar, ARTU_Commentaire, ARTU_NoAuto" + " FROM         ARTU_URLTemp";

                sSQL = sSQL + " WHERE (ART_Chrono ='" + txtART_ChronoPrix.Text + "'" + " and ARTU_NomFourn ='" + StdSQLchaine.gFr_DoublerQuote(txtARTP_Nomfourn.Text) + "') " 
                    + " or  (ART_Chrono ='" + txtART_ChronoPrix.Text + "'" + " and ( ARTU_NomFourn  is null or ARTU_NomFourn = ''))";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                rsUrl = new DataTable();
                SDArsUrl = new SqlDataAdapter(sSQL, ModParametre.adoGecet);
                SDArsUrl.Fill(rsUrl);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                ssGridURL.DataSource = rsUrl;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridURL_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                ModuleAPI.Ouvrir(ssGridURL.ActiveRow.Cells["ARTU_URL"].Text);
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
            }
        }

        private void ssGridURL_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            if (e.ErrorType == Infragistics.Win.UltraWinGrid.ErrorType.Data)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie de données incorrectes dans la colonne " + ssGridURL?.ActiveCell?.Column.Header.Caption);
                e.Cancel = true;
            }
        }

        private void ssGridURL_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadEquiv()
        {
            string sSQL = null;

            try
            {
                //sSQL = "SELECT     ARTE_Chrono, ARTE_ChronoEquiv" _
                //& " FROM         ARTE_ArticleEquivTemp"

                sSQL = "SELECT     ARTE_ArticleEquivTemp.ARTE_Chrono, ARTE_ArticleEquivTemp.ARTE_ChronoEquiv, " + " ART_Article.ART_Libelle"
                    + " FROM         ARTE_ArticleEquivTemp INNER JOIN" + " ART_Article ON ARTE_ArticleEquivTemp.ARTE_Chrono = ART_Article.ART_Chrono";

                sSQL = sSQL + " WHERE ARTE_Chrono ='" + txtART_Chrono.Text + "'";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                rsEquiv = new DataTable();
                SDArsEquiv = new SqlDataAdapter(sSQL, ModParametre.adoGecet);
                SDArsEquiv.Fill(rsEquiv);

                ssGridEquiv.DataSource = rsEquiv;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
            }
        }

        private void ssGridEquiv_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            if (e.ErrorType == Infragistics.Win.UltraWinGrid.ErrorType.Data)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisie de données incorrectes dans la colonne " + ssGridEquiv.ActiveCell.Column.Header.Caption);
                e.Cancel = true;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridURL_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ssGridURL.DisplayLayout.Bands[0].Columns["ART_Chrono"].Hidden = true;
            ssGridURL.DisplayLayout.Bands[0].Columns["ARTU_NoLigne"].Hidden = true;
            ssGridURL.DisplayLayout.Bands[0].Columns["ARTU_Table"].Header.Caption = "Table";
            ssGridURL.DisplayLayout.Bands[0].Columns["ARTU_Description"].Header.Caption = "Description";
            ssGridURL.DisplayLayout.Bands[0].Columns["ARTU_Famille"].Hidden = true;
            ssGridURL.DisplayLayout.Bands[0].Columns["ARTU_NoFourn"].Hidden = true;
            ssGridURL.DisplayLayout.Bands[0].Columns["ARTU_NomFourn"].Hidden = true;
            ssGridURL.DisplayLayout.Bands[0].Columns["ARTU_Type"].Header.Caption = "Type";
            ssGridURL.DisplayLayout.Bands[0].Columns["ARTU_URL"].Header.Caption = "URL";
            ssGridURL.DisplayLayout.Bands[0].Columns["ARTU_DateCreation"].Hidden = true;
            ssGridURL.DisplayLayout.Bands[0].Columns["ARTU_creePar"].Hidden = true;
            ssGridURL.DisplayLayout.Bands[0].Columns["ARTU_Commentaire"].Hidden = true;
            ssGridURL.DisplayLayout.Bands[0].Columns["ARTU_NoAuto"].Hidden = true;
            for(int i=0;i < ssGridURL.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                ssGridURL.DisplayLayout.Bands[0].Columns[i].CellActivation= Infragistics.Win.UltraWinGrid.Activation.NoEdit; 
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridEquiv_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
          
            ssGridEquiv.DisplayLayout.Bands[0].Columns["ARTE_Chrono"].Hidden = true;
            ssGridEquiv.DisplayLayout.Bands[0].Columns["ARTE_ChronoEquiv"].Header.Caption = "Chrono Equivalent";
            ssGridEquiv.DisplayLayout.Bands[0].Columns["ART_Libelle"].Header.Caption = "Libelle";
            ssGridEquiv.DisplayLayout.Bands[0].Columns["ARTE_ChronoEquiv"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ssGridEquiv.DisplayLayout.Bands[0].Columns["ART_Libelle"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmDetailArticleV2_Load(object sender, EventArgs e)
        {     
            try
            {
                View.Theme.Theme.recursiveLoopOnFrms(this);

                ModParametre.fc_OpenConnGecet();
                fc_AfficheArticle(txtART_Chrono.Text);
                fc_AffichePrx(Convert.ToInt32(txtART_ChronoPrix.Text));
                fc_LoadURL();
                fc_LoadEquiv();
                // rsArtGecet.Close
                // Set rsArtGecet = Nothing
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Form_Activate");
            }
        }

        private void label21_Click(object sender, EventArgs e)
        {

        }
    }
}
