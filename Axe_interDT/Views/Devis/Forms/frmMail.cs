﻿using Axe_interDT.Shared;
using System;
using System.Data;
using System.Windows.Forms;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmMail : Form
    {
        public bool blnActivate;

        //===> Mondir le 12.02.2021, if true, frmMail opened from Commande, must show emails of Fournisseur https://groupe-dt.mantishub.io/view.php?id=2261
        public bool PreCommande { get; set; }
        //===> Fin Modif Mondir

        public frmMail()
        {
            InitializeComponent();
        }

        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            short i = 0;

            //for (i = 0; i <= (frmCopie.lstCopier.Items.Count - 1); i++)
            // {
            //   frmCopie.lstCopier.SetSelected(i, false);
            // }

            txtCopie.Text = "";
            txtObjet.Text = "";
            txtMessage.Text = "";
            ModCourrier.EnvoieMail = false;
            txtObjet.Focus();
            this.Visible = false;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCopie_Click(object sender, EventArgs e)
        {
            var frmCopie = new frmCopie();
            frmCopie.ShowDialog();
            frmCopie.Close();
            txtCopie.Text = ModCourrier.list;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdEnvoyer_Click(object sender, EventArgs e)
        {
            short i = 0;
            string stemp = null;
            // désélectionne les éléments de la liste à copier
            //For i = 0 To (frmCopie.lstCopier.ListCount - 1)
            //    frmCopie.lstCopier.Selected(i) = False
            //Next i

            try
            {
                stemp = "";

                if (optHaute.Checked == true)
                {
                    ModCourrier.ImportanceMail = "Haute";
                }
                else if (optNormale.Checked == true)
                {
                    ModCourrier.ImportanceMail = "Normale";
                }
                else
                {
                    ModCourrier.ImportanceMail = "Basse";
                }
                GridDocuments.UpdateData();

                ModCourrier.list = txtCopie.Text;
                ModCourrier.ObjetMail = txtObjet.Text;
                ModCourrier.MessageMail = txtMessage.Text;
                ModCourrier.AdresseMail_Renamed = SSOleDBcmbA.Text;

                txtObjet.Text = "";
                txtMessage.Text = "";
                txtCopie.Text = "";
                ModCourrier.EnvoieMail = true;

                ModCourrier.FichierAttache = "";


                for (i = 0; i <= GridDocuments.Rows.Count - 1; i++)
                {
                    if (Convert.ToBoolean(GridDocuments.Rows[i].Cells[0].Value))//tested
                    {
                        if (string.IsNullOrEmpty(ModCourrier.FichierAttache))
                        {
                            if (Dossier.fc_ControleFichier((GridDocuments.Rows[i].Cells[1].Text)) == false)
                            {
                                stemp = stemp + "    - " + GridDocuments.Rows[i].Cells[1].Text + "\n";
                            }
                            else
                            {
                                ModCourrier.FichierAttache = GridDocuments.Rows[i].Cells[1].Text;
                            }
                        }
                        else
                        {
                            if (Dossier.fc_ControleFichier((GridDocuments.Rows[i].Cells[1].Text)) == false)
                            {
                                stemp = stemp + "    - " + GridDocuments.Rows[i].Cells[1].Text + "\n";
                            }
                            else
                            {
                                ModCourrier.FichierAttache = ModCourrier.FichierAttache + "\t" + GridDocuments.Rows[i].Cells[1].Text;
                            }
                        }
                    }
                }
                if (!string.IsNullOrEmpty(stemp))
                {
                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Les fichiers ci-dessous ne sont pas disponible, voulez-vous continuer ?\n\n" + stemp, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        return;
                    }
                }
                if (General.sRDOmail == "1")
                {
                    ModCourrier.CreateMailRDO(ModCourrier.AdresseMail_Renamed, ModCourrier.list, ModCourrier.ObjetMail, ModCourrier.MessageMail, ModCourrier.FichierAttache, '\t', txtDossierSauvegarde.Text);
                }
                else
                {
                    ModCourrier.CreateMail(ModCourrier.AdresseMail_Renamed, ModCourrier.list, ModCourrier.ObjetMail, ModCourrier.MessageMail, ModCourrier.FichierAttache, "\t", txtDossierSauvegarde.Text);
                }

                ModCourrier.EnvoieMail = true;

                this.Visible = false;
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";cmdEnvoyer_Click");
            }
        }
        private bool InitCmbMail(string Dossier)
        {
            bool functionReturnValue = false;
            //    Dim myRecipient As Recipient
            //    Dim myItem As Items
            //    Dim myAddressList As AddressList
            //    Dim RedAddressList As Object
            //    Dim myAddressEntries As AddressEntries
            //    Dim myAddressEntry As AddressEntry
            int Index = 0;
            Redemption.AddressLists rals = new Redemption.AddressLists();
            Redemption.AddressList ral = null;
            string s = null;

            ral = rals.Item(Dossier);
            s = Convert.ToString(ral.AddressEntries.Count);

            try
            {
                var SSOleDBcmbASource = new DataTable();
                SSOleDBcmbASource.Columns.Add("Col");

                for (Index = 1; Index <= ral.AddressEntries.Count; Index++)
                {
                    SSOleDBcmbASource.Rows.Add(ral.AddressEntries[Index].Name + "\t" + ral.AddressEntries[Index].Name);
                }

                SSOleDBcmbA.DataSource = SSOleDBcmbASource;

                rals = null;
                ral = null;

                functionReturnValue = true;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                rals = null;
                ral = null;
                functionReturnValue = false;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.Message, "Dossier : " + Dossier, MessageBoxButtons.OK, MessageBoxIcon.Error);
                return functionReturnValue;
            }
        }

        private void frmMail_FormClosed(object sender, FormClosedEventArgs e)
        {
            blnActivate = false;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridDocuments_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            string sDesPathFile = null;
            string sFilter = null;
            sFilter = "Documents (*.doc;*.txt;*rtf;*.xls)|*.doc;*.txt;*rtf;*.xls";
            sFilter = sFilter + "|Images (*.gif;*.jpg;*.jpeg;*.bmp;*.pcx)|*.gif;*.jpg;*.jpeg;*.bmp;*.pcx";
            var CDOpen = new OpenFileDialog();
            CDOpen.Filter = sFilter;
            CDOpen.ShowDialog();
            if (!string.IsNullOrEmpty(CDOpen.FileName))
            {
                GridDocuments.ActiveRow.Cells[0].Value = true;
                GridDocuments.ActiveRow.Cells[1].Value = CDOpen.FileName;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridDocuments_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            if (!string.IsNullOrEmpty(GridDocuments.ActiveRow.Cells[1].Text))
            {
                ModuleAPI.Ouvrir(GridDocuments.ActiveRow.Cells[1].Text);
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridDocuments_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridDocuments.DisplayLayout.Bands[0].Columns["Document"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton;
            GridDocuments.DisplayLayout.Bands[0].Columns["Attacher"].Width = 50;
            GridDocuments.DisplayLayout.Bands[0].Columns["Document"].Width = 436;
        }

        private void frmMail_Load(object sender, EventArgs e)
        {
            int x = 0;
            DataTable adotemp = null;
            ModAdo modAdoadotemp = null;
            int i = 0;
            int FIN = 0;
            int Debut = 0;

            try
            {
                View.Theme.Theme.recursiveLoopOnFrms(this);
                //If blnActivate = True Then Exit Sub

                //If blnActivate = True Then
                if (ModMain.bActivate == true)
                {
                    txtObjet.Focus();

                    General.SQL = "Select UserInitial,UserEmail " + "From uemails " + "ORDER BY UserInitial ASC";

                    modAdoadotemp = new ModAdo();
                    adotemp = modAdoadotemp.fc_OpenRecordSet(General.SQL);

                    SSOleDBcmbA.DataSource = null;

                    var SSOleDBcmbASource = new DataTable();
                    SSOleDBcmbASource.Columns.Add("Contacts");

                    if (adotemp.Rows.Count > 0)
                    {
                        foreach (DataRow adotempRow in adotemp.Rows)
                        {
                            SSOleDBcmbASource.Rows.Add(adotempRow["UserInitial"] + "\n" + adotempRow["UserEmail"]);
                        }
                    }

                    //        For i = 1 To UBound(MesDossiers)
                    //                If InitCmbMail(MesDossiers(i)) = False Then Exit For
                    //        Next i


                    if (General.sRDOmail == "1")//tested
                    {
                        ModCourrier.InitAdressListOutlookRDO();
                    }
                    else
                    {
                        ModCourrier.InitAdressListOutlook();
                    }

                    //===> Mondir le 12.02.2021, look at the diff of PreCommande https://groupe-dt.mantishub.io/view.php?id=2261
                    if (PreCommande)
                    {
                        adotemp = modAdoadotemp.fc_OpenRecordSet("SELECT Email FROM fournisseurArticle WHERE Email != '' AND Email IS NOT NULL");
                        foreach (DataRow row in adotemp.Rows)
                        {
                            SSOleDBcmbASource.Rows.Add(row[0]);
                        }

                        adotemp = modAdoadotemp.fc_OpenRecordSet("SELECT Fns_Email FROM FNS_Agences WHERE Fns_Email != '' AND Fns_Email IS NOT NULL");
                        foreach (DataRow row in adotemp.Rows)
                        {
                            SSOleDBcmbASource.Rows.Add(row[0]);
                        }
                    }
                    //===> Fin Modif Mondir


                    if (SSOleDBcmbA.Rows.Count == 0)//tested
                    {
                        for (x = 0; x <= ModCourrier.sTabAdresseMail.Length - 1; x++)
                        {
                            SSOleDBcmbASource.Rows.Add(ModCourrier.sTabAdresseMail[x].sNom);
                        }
                        SSOleDBcmbA.DataSource = SSOleDBcmbASource;
                        SSOleDBcmbA.DisplayLayout.Bands[0].Columns[0].Width = 370;
                    }

                    var GridDocumentsSource = new DataTable();
                    GridDocumentsSource.Columns.Add("Attacher", typeof(bool));
                    GridDocumentsSource.Columns.Add("Document");
                    GridDocuments.DataSource = GridDocumentsSource;

                    if (!string.IsNullOrEmpty(txtDocuments.Text))
                    {

                        if (txtDocuments.Text != "")
                            foreach (var str in txtDocuments.Text.Split('\t'))
                            {
                                if (General.Right(str, 3).ToUpper() == "amr".ToUpper() || General.Right(str, 3).ToUpper() == "wav".ToUpper())
                                {
                                    GridDocumentsSource.Rows.Add(false, str);
                                }
                                else
                                {
                                    GridDocumentsSource.Rows.Add(true, str);
                                }
                            }

                        //if (txtDocuments.Text.IndexOf(Microsoft.VisualBasic.Constants.vbTab, 1) == -1)//modifier par salma
                        //{
                        //    if (General.Right(txtDocuments.Text, 3).ToUpper() == "amr".ToUpper() || General.Right(txtDocuments.Text, 3).ToUpper() == "wav".ToUpper())
                        //    {
                        //        GridDocumentsSource.Rows.Add(false, txtDocuments.Text);
                        //    }
                        //    else
                        //    {
                        //        GridDocumentsSource.Rows.Add(true, txtDocuments.Text);
                        //    }
                        //}
                        //else
                        //{

                        //    foreach(var str in txtDocuments.Text.Split('\t'))
                        //    {
                        //        if (General.Right(str, 3).ToUpper() == "amr".ToUpper() || General.Right(str, 3).ToUpper() == "wav".ToUpper())
                        //        {
                        //            GridDocumentsSource.Rows.Add(false, str);
                        //        }
                        //        else
                        //        {
                        //            GridDocumentsSource.Rows.Add(true, str);
                        //        }
                        //    }

                        //    //Debut = 0;
                        //    //FIN = 1;
                        //    //FIN = txtDocuments.Text.IndexOf(Microsoft.VisualBasic.Constants.vbTab, FIN);

                        //    //if (General.Right(General.Left(txtDocuments.Text, FIN - 1), 3).ToUpper() == "amr".ToUpper() || General.Right(General.Left(txtDocuments.Text, FIN - 1), 3).ToUpper() == "wav".ToUpper())
                        //    //{
                        //    //    GridDocumentsSource.Rows.Add(false, General.Left(txtDocuments.Text, FIN - 1));
                        //    //}
                        //    //else
                        //    //{
                        //    //    GridDocumentsSource.Rows.Add(true, General.Left(txtDocuments.Text, FIN - 1));
                        //    //}
                        //    //i = 1;
                        //    //do
                        //    //{
                        //    //    i = i + 1;
                        //    //    FIN = FIN + 1;
                        //    //    Debut = FIN;

                        //    //    FIN = txtDocuments.Text.IndexOf("\t", FIN);
                        //    //    if (FIN == 0)
                        //    //    {
                        //    //        if (General.Right(General.Mid(txtDocuments.Text, Debut, txtDocuments.Text.Length - Debut + 1), 3).ToUpper() == "amr".ToUpper() ||
                        //    //            General.Right(General.Mid(txtDocuments.Text, Debut, txtDocuments.Text.Length - Debut + 1), 3).ToUpper() == "wav".ToUpper())
                        //    //        {
                        //    //            GridDocumentsSource.Rows.Add(false, General.Mid(txtDocuments.Text, Debut, txtDocuments.Text.Length - Debut + 1));
                        //    //        }
                        //    //        else
                        //    //        {
                        //    //            GridDocumentsSource.Rows.Add(true, General.Mid(txtDocuments.Text, Debut, txtDocuments.Text.Length - Debut + 1));
                        //    //        }
                        //    //        break;
                        //    //    }
                        //    //    if (General.Right(General.Mid(txtDocuments.Text, Debut, FIN - Debut), 3).ToUpper() == "amr".ToUpper() || General.Right(General.Mid(txtDocuments.Text, Debut, FIN - Debut), 3).ToUpper() == "wav".ToUpper())
                        //    //    {
                        //    //        GridDocumentsSource.Rows.Add(false, General.Mid(txtDocuments.Text, Debut, FIN - Debut));
                        //    //    }
                        //    //    else
                        //    //    {
                        //    //        GridDocumentsSource.Rows.Add(true, General.Mid(txtDocuments.Text, Debut, FIN - Debut));
                        //    //    }
                        //    //} while (true);
                        //}
                    }

                    GridDocuments.DataSource = GridDocumentsSource;

                }
                Cursor.Current = Cursors.Arrow;

                ModMain.bActivate = false;
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";Form_Activate");
            }
            // blnActivate = False
        }
    }
}
