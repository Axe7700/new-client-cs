﻿namespace Axe_interDT.Views.Devis.Forms
{
    partial class frmAfficheFichier
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblTexte = new System.Windows.Forms.Label();
            this.LstFichier = new System.Windows.Forms.ListBox();
            this.txtCheminFichier = new System.Windows.Forms.TextBox();
            this.txtNbOccurence = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // lblTexte
            // 
            this.lblTexte.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTexte.Location = new System.Drawing.Point(12, 9);
            this.lblTexte.Name = "lblTexte";
            this.lblTexte.Size = new System.Drawing.Size(507, 71);
            this.lblTexte.TabIndex = 384;
            // 
            // LstFichier
            // 
            this.LstFichier.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.LstFichier.FormattingEnabled = true;
            this.LstFichier.ItemHeight = 19;
            this.LstFichier.Location = new System.Drawing.Point(15, 76);
            this.LstFichier.Name = "LstFichier";
            this.LstFichier.Size = new System.Drawing.Size(504, 156);
            this.LstFichier.TabIndex = 385;
            this.LstFichier.SelectedIndexChanged += new System.EventHandler(this.LstFichier_SelectedIndexChanged);
            // 
            // txtCheminFichier
            // 
            this.txtCheminFichier.Location = new System.Drawing.Point(53, 243);
            this.txtCheminFichier.Name = "txtCheminFichier";
            this.txtCheminFichier.Size = new System.Drawing.Size(100, 20);
            this.txtCheminFichier.TabIndex = 386;
            this.txtCheminFichier.Visible = false;
            // 
            // txtNbOccurence
            // 
            this.txtNbOccurence.Location = new System.Drawing.Point(203, 243);
            this.txtNbOccurence.Name = "txtNbOccurence";
            this.txtNbOccurence.Size = new System.Drawing.Size(100, 20);
            this.txtNbOccurence.TabIndex = 387;
            this.txtNbOccurence.Visible = false;
            // 
            // frmAfficheFichier
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(531, 273);
            this.Controls.Add(this.txtNbOccurence);
            this.Controls.Add(this.txtCheminFichier);
            this.Controls.Add(this.LstFichier);
            this.Controls.Add(this.lblTexte);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(547, 312);
            this.MinimumSize = new System.Drawing.Size(547, 312);
            this.Name = "frmAfficheFichier";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Factures Fournisseurs";
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion
        public System.Windows.Forms.ListBox LstFichier;
        public System.Windows.Forms.Label lblTexte;
        public System.Windows.Forms.TextBox txtCheminFichier;
        public System.Windows.Forms.TextBox txtNbOccurence;
    }
}