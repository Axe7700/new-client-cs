﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmGecet : Form
    {
        public DataTable rsFournPrix;
        double dblSelecteurWidth;
        double[] tabColPerCent;
        string[] tabArtSelect;
        string[] tabQteArt;
        bool blnActivate;

        const string cEsp = "ESPACE";
        const string cEtouOU = "ETouOU";

        DataTable GridGecetSource =null;

        public frmGecet()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbFourn_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sqlSelect = null;

            if (cmbFourn.Rows.Count == 0)
            {
                if (txtType.Text == "0")
                {
                    sqlSelect = "SELECT DISTINCT Dtiprix.Fournisseur FROM Dtiprix ORDER BY Dtiprix.Fournisseur ";
                }
                else
                {
                    sqlSelect = "SELECT DISTINCT Dtiprix.Fournisseur FROM Dtiprix ORDER BY Dtiprix.Fournisseur ";
                }
                sheridan.InitialiseCombo(cmbFourn, sqlSelect, "Fournisseur", false, ModParametre.adoGecet);
            }
        }

        private void cmbFourn_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == 13)
            {
                cmdRechercher_Click(cmdRechercher, new System.EventArgs());
            }
        }

        private void cmdAppliqueClose_Click(object sender, EventArgs e)
        {
            GridGecet.UpdateData();
            if (GridGecet.Rows.Count > 0)
            {
                if (txtOrigine.Text == Variable.cUserBCmdBody || txtOrigine.Text.ToUpper() == Variable.cUserPreCommande2.ToUpper())
                {
                    fc_MajBCmd((txtCle.Text), General.nz((txtNumLigne.Text), "0").ToString());//tested

                }
                else if (txtOrigine.Text == Variable.cUserDocDevis)
                {
                    fc_MajDevis((txtCle.Text), General.nz((txtNumLigne.Text), "0").ToString());

                }
                else if (txtOrigine.Text == Variable.cUserPreCommande)
                {
                    //==modif 20 05 2005 rachid ajout de commandes dans precommande
                    fc_LoadArtPreComm();//tested
                }

            }
            if (GridGecet.Rows.Count > 0)
            {
                GridGecet.DataSource = null;
            }
            tabArtSelect = new string[2];
            tabQteArt = new string[2];
            GridGecet.Visible = false;
            this.Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LoadArtPreComm()
        {
            int i = 0;
            int j = 0;

            try
            {
                if (General.tArtGecetPre != null)
                {
                    j = General.tArtGecetPre.Length;
                }
              else
                {
                    //== indice en dehors de la plage.
                    j = 0;
                }


                //tested
                for (i = 0; i <= tabArtSelect.Length - 1; i++)
                {

                    if (!string.IsNullOrEmpty(tabArtSelect[i]))
                    {
                        Array.Resize(ref General.tArtGecetPre, j + 1);

                        General.tArtGecetPre[j].sNoAuto = tabArtSelect[i];
                        if (General.IsNumeric(tabQteArt[i]))//tested
                        {
                            General.tArtGecetPre[j].Qte = Convert.ToDouble(tabQteArt[i]);
                        }
                        else
                        {
                            General.tArtGecetPre[j].Qte = 1;
                        }
                        j = j + 1;
                    }

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadArtPreComm ");
            }
        }

        private void cmdAppliquer_Click(object sender, EventArgs e)
        {
            GridGecet.UpdateData();
            if (GridGecet.Rows.Count > 0)
            {
                if (txtOrigine.Text.ToUpper() == Variable.cUserBCmdBody.ToUpper() || txtOrigine.Text.ToUpper() == Variable.cUserPreCommande2.ToUpper())
                {
                    fc_MajBCmd((txtCle.Text), General.nz((txtNumLigne.Text), "0").ToString());
                }
                else if (txtOrigine.Text == Variable.cUserDocDevis)//tested
                {
                    fc_MajDevis((txtCle.Text), General.nz((txtNumLigne.Text), "0").ToString());
                }
                else if (txtOrigine.Text == Variable.cUserPreCommande)
                {
                    //==modif 20 05 2005 rachid ajout de commandes dans precommande
                    fc_LoadArtPreComm();//tested
                }
            }
            if (GridGecet.Rows.Count > 0)
            {
                GridGecet.DataSource = null;
            }
            tabArtSelect = new string[2];
            tabQteArt = new string[2];
            GridGecet.Visible = false;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sNoBCmd"></param>
        /// <param name="sNoLigne"></param>
        private void fc_MajBCmd(string sNoBCmd, string sNoLigne = "0")
        {
            int i = 0;
            string sqlInsert = null;
            string sqlValues = null;
            DataTable rsDtiPrix = default(DataTable);
            SqlDataAdapter SDArsDtiPrix = null;

            sqlInsert = "INSERT INTO BCD_Detail (";
            sqlInsert = sqlInsert + "BCD_Cle,";
            sqlInsert = sqlInsert + "BCD_References,";
            sqlInsert = sqlInsert + "BCD_Designation,";
            sqlInsert = sqlInsert + "BCD_Quantite,";
            sqlInsert = sqlInsert + "BCD_PrixHT,";
            sqlInsert = sqlInsert + "BCD_Unite,";
            sqlInsert = sqlInsert + "BCD_NoLigne,";
            sqlInsert = sqlInsert + "BCD_Chrono,";
            sqlInsert = sqlInsert + "BCD_NoAutoGecet,";
            sqlInsert = sqlInsert + "BCD_Fournisseur";
            sqlInsert = sqlInsert + ") VALUES (";

            for (i = 0; i <= tabArtSelect.Length - 1; i++)
            {
                if (!string.IsNullOrEmpty(tabArtSelect[i]))
                {
                    rsDtiPrix = new DataTable();
                    SDArsDtiPrix = new SqlDataAdapter("SELECT DtiArti.Chrono,Dtiarti.Libelle,Dtiprix.Fournisseur,Dtiprix.Fourref,Dtiprix.Fouunite,Dtiprix.[prix net achat] FROM Dtiarti INNER JOIN Dtiprix ON DtiArti.Chrono=Dtiprix.Chrono WHERE Dtiprix.NoAuto=" + tabArtSelect[i], ModParametre.adoGecet);
                    SDArsDtiPrix.Fill(rsDtiPrix);
                    if (rsDtiPrix.Rows.Count > 0)
                    {
                        var _with1 = rsDtiPrix;
                        General.Execute("UPDATE BCD_Detail SET BCD_NoLigne=BCD_NoLigne+1 WHERE BCD_Cle=" + sNoBCmd + " AND BCD_NoLigne>=" + sNoLigne);
                        sqlValues = "" + txtCle.Text + ",";
                        sqlValues = sqlValues + "'" + StdSQLchaine.gFr_DoublerQuote(_with1.Rows[0]["fourref"] + "") + "',";
                        sqlValues = sqlValues + "'" + StdSQLchaine.gFr_DoublerQuote(_with1.Rows[0]["Libelle"] + "") + "',";
                        sqlValues = sqlValues + "'" + tabQteArt[i] + "',";
                        sqlValues = sqlValues + "'" + General.fc_FormatNumber((_with1.Rows[0]["prix net achat"].ToString())) + "',";
                        using (var tmpModAdo = new ModAdo())
                            sqlValues = sqlValues + "'" + tmpModAdo.fc_ADOlibelle("SELECT QTE_No FROM QTE_Unite WHERE QTE_Libelle='" + _with1.Rows[0]["fouunite"] + "'") + "',";
                        sqlValues = sqlValues + "'" + sNoLigne + "',";
                        sqlValues = sqlValues + "" + General.nz(_with1.Rows[0]["Chrono"], "0") + ",";
                        //Code chrono
                        sqlValues = sqlValues + "" + General.nz(tabArtSelect[i], "0") + ",";
                        //Numéro auto de l'article foutnisseur
                        sqlValues = sqlValues + "'" + StdSQLchaine.gFr_DoublerQuote(_with1.Rows[0]["Fournisseur"] + "") + "'";
                        sqlValues = sqlValues + ")";
                        sNoLigne = Convert.ToString(Convert.ToInt32(sNoLigne) + 1);
                        General.Execute(sqlInsert + sqlValues);
                    }
                    rsDtiPrix?.Dispose();
                    SDArsDtiPrix?.Dispose();
                }
            }
            if ((rsDtiPrix != null))
            {
                rsDtiPrix?.Dispose();
                SDArsDtiPrix?.Dispose();
            }
            txtNumLigne.Text = sNoLigne;
        }


        private void fc_MajDevis(string sNoDevis, string sNoLigne = "0")
        {
            int i = 0;
            string sqlInsert = null;
            string sqlValues = null;
            DataTable rsDtiPrix = default(DataTable);
            SqlDataAdapter SDArsDtiPrix = null;

            sqlInsert = "INSERT INTO PanierGecet (";
            sqlInsert = sqlInsert + "Utilisateur,";
            sqlInsert = sqlInsert + "Type,";
            //0 : fourniture Gecet, 1 : Article, 2 : Poste
            sqlInsert = sqlInsert + "Code,";
            sqlInsert = sqlInsert + "Designation,";
            sqlInsert = sqlInsert + "Quantite,";
            sqlInsert = sqlInsert + "KMO,";
            sqlInsert = sqlInsert + "KFO,";
            sqlInsert = sqlInsert + "KST,";
            sqlInsert = sqlInsert + "PxHeureSC,";
            sqlInsert = sqlInsert + "PxVenteFOSC,";
            sqlInsert = sqlInsert + "PxVenteSTSC,";
            sqlInsert = sqlInsert + "NoLigne";
            sqlInsert = sqlInsert + ") VALUES (";

            for (i = 0; i <= tabArtSelect.Length - 1; i++)
            {
                if (!string.IsNullOrEmpty(tabArtSelect[i]))
                {
                    rsDtiPrix = new DataTable();
                    //Fourniture GECET
                    if (txtType.Text == "0")//tested
                    {
                        SDArsDtiPrix = new SqlDataAdapter("SELECT Dtiarti.Libelle,Dtiprix.Fournisseur,Dtiprix.Fourref,Dtiprix.Fouunite,Dtiprix.[prix net achat] FROM Dtiarti INNER JOIN Dtiprix ON DtiArti.Chrono=Dtiprix.Chrono WHERE Dtiprix.NoAuto=" + tabArtSelect[i], ModParametre.adoGecet);
                        SDArsDtiPrix.Fill(rsDtiPrix);
                        //Article
                    }
                    else if (txtType.Text == "1")//tested
                    {
                        General.sSQL = "SELECT FacArticle.CodeArticle, FacArticle.Designation1," + " FacArticle.PrixAchat, FacArticle.MOtps, FacArticle.FO," + " FacArticle.ST " + " FROM FacArticle WHERE CodeCategorieArticle = 'D' AND CodeArticle='" + tabArtSelect[i] + "'" + " ORDER BY CodeArticle";
                        SDArsDtiPrix = new SqlDataAdapter(General.sSQL, General.adocnn);
                        SDArsDtiPrix.Fill(rsDtiPrix);
                        //Poste
                    }
                    else if (txtType.Text == "2")
                    {
                        General.sSQL = "SELECT SousFamilleArticle.CodeSousFamille, SousFamilleArticle.IntituleSousFamille " + " FROM SousFamilleArticle WHERE CodeSousFamille='" + tabArtSelect[i] + "'" + " ORDER BY CodeSousFamille";
                        SDArsDtiPrix = new SqlDataAdapter(General.sSQL, General.adocnn);
                        SDArsDtiPrix.Fill(rsDtiPrix);
                        //Fourniture GECET
                    }
                    else if (txtType.Text == "3")
                    {
                        SDArsDtiPrix = new SqlDataAdapter("SELECT Dtiarti.Libelle FROM Dtiarti WHERE DtiArti.Chrono=" + tabArtSelect[i], ModParametre.adoGecet);
                        SDArsDtiPrix.Fill(rsDtiPrix);
                    }

                    if (rsDtiPrix.Rows.Count > 0)
                    {
                        var _with2 = rsDtiPrix;
                        //Article Gecet
                        if (txtType.Text == "0")//tested
                        {
                            sqlValues = "'" + General.gsUtilisateur + "',";
                            //Utilisateur
                            sqlValues = sqlValues + "'0',";
                            //Type
                            sqlValues = sqlValues + "'" + tabArtSelect[i] + "',";
                            //Code
                            sqlValues = sqlValues + "'" + StdSQLchaine.gFr_DoublerQuote(_with2.Rows[0]["Libelle"] + "") + "',";
                            //Designation
                            sqlValues = sqlValues + "" + General.nz(tabQteArt[i], "0") + ",";
                            //Quantité
                            sqlValues = sqlValues + "" + General.nz((txtKMO.Text), "0") + ",";
                            //KMO
                            sqlValues = sqlValues + "" + General.nz((txtKFO.Text), "0") + ",";
                            //KFO
                            sqlValues = sqlValues + "" + General.nz((txtKST.Text), "0") + ",";
                            //KST
                            sqlValues = sqlValues + "0,";
                            //PxHeureSC
                            sqlValues = sqlValues + "" + General.nz(General.fc_FormatNumber((_with2.Rows[0]["prix net achat"].ToString())), "0") + ",";
                            //PxVenteFOSC
                            sqlValues = sqlValues + "0,";
                            //PxVenteSTSC
                            sqlValues = sqlValues + "" + General.nz(sNoLigne, "0") + "";
                            //NoLigne
                            sqlValues = sqlValues + ")";
                            sNoLigne = Convert.ToString(Convert.ToInt32(sNoLigne) + 1);
                            General.Execute(sqlInsert + sqlValues);
                        }
                        else if (txtType.Text == "1")//tested
                        {
                            sqlValues = "'" + General.gsUtilisateur + "',";
                            //Utilisateur
                            sqlValues = sqlValues + "'2',";
                            //Type
                            sqlValues = sqlValues + "'" + tabArtSelect[i] + "',";
                            //CodeArticle
                            sqlValues = sqlValues + "'" + StdSQLchaine.gFr_DoublerQuote(_with2.Rows[0]["Designation1"] + "") + "',";
                            //Designation
                            sqlValues = sqlValues + "" + General.nz(tabQteArt[i], "0") + ",";
                            //Quantité
                            sqlValues = sqlValues + "" + General.nz((txtKMO.Text), "0") + ",";
                            //KMO
                            sqlValues = sqlValues + "" + General.nz((txtKFO.Text), "0") + ",";
                            //KFO
                            sqlValues = sqlValues + "" + General.nz((txtKST.Text), "0") + ",";
                            //KST
                            sqlValues = sqlValues + "0,";
                            //PxHeureSC
                            sqlValues = sqlValues + "0,";
                            //PxVenteFOSC
                            sqlValues = sqlValues + "0,";
                            //PxVenteSTSC
                            sqlValues = sqlValues + "" + General.nz(sNoLigne, "0") + "";
                            //NoLigne
                            sqlValues = sqlValues + ")";
                            sNoLigne = Convert.ToString(Convert.ToInt32(sNoLigne) + 1);
                            General.Execute(sqlInsert + sqlValues);
                        }
                        else if (txtType.Text == "2")
                        {
                            sqlValues = "'" + General.gsUtilisateur + "',";
                            //Utilisateur
                            sqlValues = sqlValues + "'2',";
                            //Type
                            sqlValues = sqlValues + "'" + tabArtSelect[i].Replace(",",".") + "',";
                            //CodePoste
                            sqlValues = sqlValues + "'" + StdSQLchaine.gFr_DoublerQuote(_with2.Rows[0]["IntituleSousFamille"] + "").Replace(",", ".") + "',";
                            //Designation
                            sqlValues = sqlValues + "" + General.nz(tabQteArt[i].Replace(",", "."), "0") + ",";
                            //Quantité
                            sqlValues = sqlValues + "" + General.nz((txtKMO.Text).Replace(",", "."), "0") + ",";
                            //KMO
                            sqlValues = sqlValues + "" + General.nz((txtKFO.Text).Replace(",", "."), "0") + ",";
                            //KFO
                            sqlValues = sqlValues + "" + General.nz((txtKST.Text).Replace(",", "."), "0") + ",";
                            //KST
                            sqlValues = sqlValues + "0,";
                            //PxHeureSC
                            sqlValues = sqlValues + "0,";
                            //PxVenteFOSC
                            sqlValues = sqlValues + "0,";
                            //PxVenteSTSC
                            sqlValues = sqlValues + "" + General.nz(sNoLigne, "0") + "";
                            //NoLigne
                            sqlValues = sqlValues + ")";
                            sNoLigne = Convert.ToString(Convert.ToInt32(sNoLigne) + 1);
                            var xx = General.Execute(sqlInsert + sqlValues);
                            //Article Gecet
                        }
                        else if (txtType.Text == "3")
                        {
                            sqlValues = "'" + General.gsUtilisateur + "',";
                            //Utilisateur
                            sqlValues = sqlValues + "'0',";
                            //Type
                            sqlValues = sqlValues + "'" + tabArtSelect[i] + "',";
                            //Code
                            sqlValues = sqlValues + "'" + StdSQLchaine.gFr_DoublerQuote(_with2.Rows[0]["Libelle"] + "") + "',";
                            //Designation
                            sqlValues = sqlValues + "" + General.nz(tabQteArt[i], "0") + ",";
                            //Quantité
                            sqlValues = sqlValues + "" + General.nz((txtKMO.Text), "0") + ",";
                            //KMO
                            sqlValues = sqlValues + "" + General.nz((txtKFO.Text), "0") + ",";
                            //KFO
                            sqlValues = sqlValues + "" + General.nz((txtKST.Text), "0") + ",";
                            //KST
                            sqlValues = sqlValues + "0,";
                            //PxHeureSC
                            sqlValues = sqlValues + "0,";
                            //PxVenteFOSC
                            sqlValues = sqlValues + "0,";
                            //PxVenteSTSC
                            sqlValues = sqlValues + "" + General.nz(sNoLigne, "0") + "";
                            //NoLigne
                            sqlValues = sqlValues + ")";
                            sNoLigne = Convert.ToString(Convert.ToInt32(sNoLigne) + 1);
                            General.Execute(sqlInsert + sqlValues);

                        }
                    }
                    rsDtiPrix?.Dispose();
                }
            }
            rsDtiPrix?.Dispose();
            SDArsDtiPrix?.Dispose();
            txtNumLigne.Text = sNoLigne;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdClean_Click(object sender, EventArgs e)
        {
            sub_Clean();
        }
        /// <summary>
        /// tested
        /// </summary>
        private void sub_Clean()
        {
            txtCodeChrono.Text = "";
            txtFamille.Text = "";
            txtLibelle.Text = "";
            txtRefFourn.Text = "";
            txtRefFab.Text = "";
            txtMarque.Text = "";
            cmbFourn.Text = "";
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdHelp_Click(object sender, EventArgs e)
        {
            new frmHelp().ShowDialog();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdPanier_Click(object sender, EventArgs e)
        {
            new frmCaddy().ShowDialog();
        }
        /// <summary>
        /// tested dans le cas txtOrigine.Text == Variable.cUserDocDevis
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercher_Click(object sender, EventArgs e)
        {
            fc_SaveSettingPos();

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;
            if (txtOrigine.Text == Variable.cUserBCmdBody || txtOrigine.Text.ToUpper() == Variable.cUserPreCommande.ToUpper() || txtOrigine.Text.ToUpper() == Variable.cUserPreCommande2.ToUpper())
            {
                fc_ChercheGecet();
            }
            else if (txtOrigine.Text == Variable.cUserDocDevis | txtOrigine.Text == Variable.cUserDocFacManuel)
            {
                fc_ChercheDevis();

            }
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_SaveSettingPos()
        {
            if (optEtouOU.Checked == true)
            {
                General.saveInReg(this.Name, "TypeRecherche", cEtouOU);
            }
            else if (optEspace.Checked == true)
            {
                General.saveInReg(this.Name, "TypeRecherche", cEsp);
            }

            if (OptOu.Checked == true)
            {
                General.saveInReg(this.Name, "EspaceEtOu", "OR");
            }
            else if (OptEt.Checked == true)
            {
                General.saveInReg(this.Name, "EspaceEtOu", "AND");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <returns></returns>
        private object fc_ChercheGecet()
        {
            object functionReturnValue = null;
            string sqlSelect = null;
            string sqlWhere = null;


            DataTable rsChercher = default(DataTable);
            SqlDataAdapter SDArsChercher = null;
            DataTable rsDtiPrix = default(DataTable);
            SqlDataAdapter SDArsDtiPrix = null;
            string sqlWhereFo = null;
            int lngNbArtFo = 0;

            string sTypeRech = "";
            string sEspEtOu = "";

            try
            {
                if (optEtouOU.Checked == true)
                {
                    sTypeRech = cEtouOU;

                }
                else if (optEspace.Checked == true)
                {
                    sTypeRech = cEsp;
                    if (OptOu.Checked == true)
                    {
                        sEspEtOu = "OR";
                    }
                    else if (OptEt.Checked == true)
                    {
                        sEspEtOu = "AND";
                    }
                }



                if (GridGecet.Rows.Count > 0)
                {
                    GridGecet.DataSource = null;
                }

                tabArtSelect = new string[2];

                sqlSelect = "SELECT Dtiarti.Chrono,Dtiarti.Libelle,Dtiprix.fourref,Dtiprix.fouunite,Dtiprix.fournisseur,Dtiprix.[prix net achat],Dtiprix.Marque,DtiPrix.NoAuto ";
                sqlSelect = sqlSelect + " FROM Dtiarti INNER JOIN Dtiprix ON Dtiarti.Chrono=Dtiprix.Chrono ";

                if (!string.IsNullOrEmpty(txtCodeChrono.Text))//tested
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE Dtiarti.Chrono LIKE '" + txtCodeChrono.Text.Replace("*", "%") + "'";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND Dtiarti.Chrono='" + txtCodeChrono.Text.Replace("*", "%") + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtFamille.Text))
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE Dtiarti.sFam LIKE '" + txtFamille.Text.Replace("*", "%") + "'";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND Dtiarti.sFam LIKE '" + txtFamille.Text.Replace("*", "%") + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtLibelle.Text))//tested
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE (" + General.fc_ReturnWhere("Dtiarti.Libelle", (txtLibelle.Text), "1", sTypeRech, sEspEtOu) + ")";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND (" + General.fc_ReturnWhere("Dtiarti.Libelle", (txtLibelle.Text), "1", sTypeRech, sEspEtOu) + ")";
                    }
                }

                if (!string.IsNullOrEmpty(txtRefFourn.Text))
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE (" + General.fc_ReturnWhere("Dtiprix.fourref", (txtRefFourn.Text), "1", sTypeRech, sEspEtOu) + ")";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND (" + General.fc_ReturnWhere("Dtiprix.fourref", (txtRefFourn.Text), "1", sTypeRech, sEspEtOu) + ")";
                    }
                }

                if (!string.IsNullOrEmpty(txtRefFab.Text))
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE (" + General.fc_ReturnWhere("Dtiprix.fabref", (txtRefFab.Text), "1", sTypeRech, sEspEtOu) + ")";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND (" + General.fc_ReturnWhere("Dtiprix.fabref", (txtRefFab.Text), "1", sTypeRech, sEspEtOu) + ")";
                    }
                }

                if (!string.IsNullOrEmpty(txtMarque.Text))//tested
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE (" + General.fc_ReturnWhere("Dtiprix.Marque", (txtMarque.Text), "1", sTypeRech, sEspEtOu) + ")";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND (" + General.fc_ReturnWhere("Dtiprix.Marque", (txtMarque.Text), "1", sTypeRech, sEspEtOu) + ")";
                    }
                }

                if (!string.IsNullOrEmpty(cmbFourn.Text))//tested
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE Dtiprix.fournisseur  LIKE '" + cmbFourn.Text.Replace("*", "%") + "'";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND Dtiprix.fournisseur='" + cmbFourn.Text.Replace("*", "%") + "'";
                    }
                }

                sqlSelect = sqlSelect + sqlWhere + " ORDER BY Dtiarti.sFam,Dtiarti.Libelle,Dtiprix.[prix net achat] " + (chkTriPrix.CheckState == 0 ? "ASC" : "DESC");

                rsDtiPrix = new DataTable();
                rsChercher = new DataTable();
                SDArsChercher = new SqlDataAdapter(sqlSelect, ModParametre.adoGecet);
                SDArsChercher.Fill(rsChercher);

                if (rsChercher.Rows.Count > 1)
                {
                    if (rsChercher.Rows.Count > 100)
                    {
                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous allez afficher " + rsChercher.Rows.Count + " articles." + "\n" + "Ca risque de prendre du temps, souhaitez-vous continuer ?", "Nombre de données important", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            rsDtiPrix?.Dispose();
                            SDArsDtiPrix?.Dispose();
                            rsChercher = null;
                            GridGecet.Visible = false;
                            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                            return functionReturnValue;
                        }
                    }
                    lblTotal.Text = rsChercher.Rows.Count + " produits en réponse";
                }
                else if (rsChercher.Rows.Count == 1)
                {
                    lblTotal.Text = rsChercher.Rows.Count + " produit en réponse";
                }
                else
                {
                    lblTotal.Text = "Aucun produit en réponse";
                }
                System.Windows.Forms.Application.DoEvents();

                DataTable GridGecetSource = new DataTable();
                GridGecetSource.Columns.Add("Choix");
                GridGecetSource.Columns.Add("Qté");
                GridGecetSource.Columns.Add("Chrono");
                GridGecetSource.Columns.Add("Libelle");
                GridGecetSource.Columns.Add("Ref. fourn");
                GridGecetSource.Columns.Add("Unité Métier");
                GridGecetSource.Columns.Add("FournPrix");
                GridGecetSource.Columns.Add("Prix");
                GridGecetSource.Columns.Add("Nb Fourn");
                GridGecetSource.Columns.Add("Fabricant");
                GridGecetSource.Columns.Add("Caractéristiques");
                GridGecetSource.Columns.Add("CleAuto");
                GridGecet.DataSource = GridGecetSource;

                var newRow = GridGecetSource.NewRow();

                if (rsChercher.Rows.Count > 0)
                {
                    foreach (DataRow rsChercherRow in rsChercher.Rows)
                    {
                        sqlSelect = "";
                        sqlSelect = " SELECT DtiPrix.NoAuto FROM DtiPrix ";
                        sqlWhere = " WHERE Chrono='" + rsChercherRow["Chrono"] + "'";
                        sqlSelect = sqlSelect + sqlWhere + " ORDER BY Dtiprix.Chrono ";
                        rsDtiPrix = new DataTable();
                        SDArsDtiPrix = new SqlDataAdapter(sqlSelect, ModParametre.adoGecet);
                        SDArsDtiPrix.Fill(rsDtiPrix);
                        var _with4 = rsChercher;

                        newRow["Choix"] = "0";
                        newRow["Qté"] = "0";
                        newRow["Chrono"] = rsChercherRow["Chrono"];
                        newRow["Libelle"] = rsChercherRow["Libelle"];
                        newRow["Ref. fourn"] = rsChercherRow["fourref"];
                        newRow["Unité Métier"] = rsChercherRow["fouunite"];
                        newRow["FournPrix"] = rsChercherRow["Fournisseur"];
                        newRow["Prix"] = rsChercherRow["prix net achat"];
                        newRow["Nb Fourn"] = rsDtiPrix.Rows.Count;
                        newRow["Fabricant"] = rsChercherRow["Marque"];
                        newRow["Caractéristiques"] = "Afficher";
                        newRow["CleAuto"] = rsChercherRow["Noauto"];
                        /*  GridGecetSource.Rows.Add("0", "0", _with4.Rows[0]["Chrono"] + "", _with4.Rows[0]["Libelle"], _with4.Rows[0]["fourref"], _with4.Rows[0]["fouunite"],
                              _with4.Rows[0]["Fournisseur"], _with4.Rows[0]["prix net achat"], rsDtiPrix.Rows.Count, _with4.Rows[0]["Marque"] + "", "Afficher", _with4.Rows[0]["NoAuto"]);*/
                        GridGecetSource.Rows.Add(newRow.ItemArray);
                    GridGecet.DataSource = GridGecetSource;

                        rsDtiPrix?.Dispose();
                        SDArsDtiPrix?.Dispose();
                    }
                    GridGecet.Visible = true;
                }
                else
                {
                    GridGecet.Visible = false;
                }

                rsDtiPrix?.Dispose();
                SDArsDtiPrix?.Dispose();
                rsDtiPrix = null;
                rsChercher?.Dispose();
                SDArsChercher?.Dispose();
                rsChercher = null;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, " frmGecet fc_ChercheGecet ");
                return functionReturnValue;
            }
        }

        private object fc_ChercheArticleGecet()
        {
            object functionReturnValue = null;
            string sqlSelect = null;
            string sqlWhere = null;
            DataTable rsChercher = default(DataTable);
            SqlDataAdapter SDArsChercher = null;
            DataTable rsDtiPrix = default(DataTable);
            SqlDataAdapter SDArsDtiPrix = null;
            string sqlWhereFo = null;
            int lngNbArtFo = 0;

            string sTypeRech = "";
            string sEspEtOu = "";


            try
            {
                if (optEtouOU.Checked == true)
                {
                    sTypeRech = cEtouOU;

                }
                else if (optEspace.Checked == true)
                {
                    sTypeRech = cEsp;
                    if (OptOu.Checked == true)
                    {
                        sEspEtOu = "OR";
                    }
                    else if (OptEt.Checked == true)
                    {
                        sEspEtOu = "AND";
                    }
                }


                if (GridGecet.Rows.Count > 0)
                {
                    GridGecet.DataSource = null;
                }

                tabArtSelect = new string[2];

                sqlSelect = "SELECT Dtiarti.Chrono,Dtiarti.Libelle ";
                sqlSelect = sqlSelect + " FROM Dtiarti ";

                if (!string.IsNullOrEmpty(txtCodeChrono.Text))
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE Dtiarti.Chrono LIKE '" + txtCodeChrono.Text.Replace("*", "%") + "'";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND Dtiarti.Chrono='" + txtCodeChrono.Text.Replace("*", "%") + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtFamille.Text))
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE Dtiarti.sFam LIKE '" + txtFamille.Text.Replace("*", "%") + "'";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND Dtiarti.sFam LIKE '" + txtFamille.Text.Replace("*", "%") + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtLibelle.Text))
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE (" + General.fc_ReturnWhere("Dtiarti.Libelle", (txtLibelle.Text), "1", sTypeRech, sEspEtOu) + ")";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND (" + General.fc_ReturnWhere("Dtiarti.Libelle", (txtLibelle.Text), "1", sTypeRech, sEspEtOu) + ")";
                    }
                }

                sqlSelect = sqlSelect + sqlWhere + " ORDER BY Dtiarti.sFam,Dtiarti.Libelle ";

                rsDtiPrix = new DataTable();
                rsChercher = new DataTable();
                SDArsChercher = new SqlDataAdapter(sqlSelect, ModParametre.adoGecet);
                SDArsChercher.Fill(rsChercher);

                if (rsChercher.Rows.Count > 1)
                {
                    if (rsChercher.Rows.Count > 100)
                    {
                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous allez afficher " + rsChercher.Rows.Count + " articles." + "\n" + "Ca risque de prendre du temps, souhaitez-vous continuer ?", "Nombre de données important",
                            MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            rsDtiPrix?.Dispose();
                            SDArsDtiPrix?.Dispose();
                            rsChercher = null;
                            GridGecet.Visible = false;
                            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                            return functionReturnValue;
                        }
                    }
                    lblTotal.Text = rsChercher.Rows.Count + " produits en réponse";
                }
                else if (rsChercher.Rows.Count == 1)
                {
                    lblTotal.Text = rsChercher.Rows.Count + " produit en réponse";
                }
                else
                {
                    lblTotal.Text = "Aucun produit en réponse";
                }
                System.Windows.Forms.Application.DoEvents();

                if (rsChercher.Rows.Count > 0)
                {
                    foreach (DataRow rsChercherRow in rsChercher.Rows)
                    {
                        sqlSelect = "";
                        sqlSelect = " SELECT DtiPrix.NoAuto FROM DtiPrix ";
                        sqlWhere = " WHERE Chrono='" + rsChercherRow["Chrono"] + "'";
                        sqlSelect = sqlSelect + sqlWhere + " ORDER BY Dtiprix.Chrono ";
                        rsDtiPrix = new DataTable();
                        SDArsDtiPrix = new SqlDataAdapter(sqlSelect, ModParametre.adoGecet);
                        SDArsDtiPrix.Fill(rsDtiPrix);
                        var _with5 = rsChercher;
                        GridGecetSource.Rows.Add("0", "0", _with5.Rows[0]["Chrono"] + "", _with5.Rows[0]["Libelle"], "", "", "", "0", rsDtiPrix.Rows.Count, "" + "", "Afficher", _with5.Rows[0]["Chrono"]);

                        GridGecet.DataSource = GridGecetSource;

                        rsDtiPrix?.Dispose();
                        SDArsDtiPrix?.Dispose();
                    }
                    GridGecet.Visible = true;
                }
                else
                {
                    GridGecet.Visible = false;
                }

                rsDtiPrix?.Dispose();
                SDArsDtiPrix?.Dispose();
                rsChercher?.Dispose();
                SDArsChercher?.Dispose();
                rsChercher = null;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, " frmGecet fc_ChercheArticleGecet ");
                return functionReturnValue;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_ChercheDevis()
        {
            if (txtType.Text == "0")//tested
            {
                fc_ChercheGecet();
            }
            else if (txtType.Text == "1")//tested
            {
                fc_ChercheArticle();
            }
            else if (txtType.Text == "2")//tested
            {
                fc_CherchePoste();
            }
            else if (txtType.Text == "3")
            {
                fc_ChercheArticleGecet();
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <returns></returns>
        private object fc_ChercheArticle()
        {
            object functionReturnValue = null;
            object sEspEtO = null;
            string sqlSelect = null;
            string sqlWhere = null;
            DataTable rsChercher = default(DataTable);
            SqlDataAdapter SDArsChercher = null;
            string sqlWhereFo = null;
            int lngNbArtFo = 0;

            string sTypeRech = "";
            string sEspEtOu ="";

            GridGecetSource = new DataTable();
            GridGecetSource.Columns.Add("Choix");
            GridGecetSource.Columns.Add("Qté");
            GridGecetSource.Columns.Add("Chrono");
            GridGecetSource.Columns.Add("Libelle");
            GridGecetSource.Columns.Add("Ref. fourn");
            GridGecetSource.Columns.Add("Unité métier");
            GridGecetSource.Columns.Add("FournPrix");
            GridGecetSource.Columns.Add("Prix");
            GridGecetSource.Columns.Add("Nb Fourn");
            GridGecetSource.Columns.Add("Fabricant");
            GridGecetSource.Columns.Add("Caractéristiques");
            GridGecetSource.Columns.Add("CleAuto");
            GridGecet.DataSource = GridGecetSource;

            try
            {
                if (optEtouOU.Checked == true)
                {
                    sTypeRech = cEtouOU;

                }
                else if (optEspace.Checked == true)
                {
                    sTypeRech = cEsp;
                    if (OptOu.Checked == true)
                    {
                        sEspEtOu = "OR";
                    }
                    else if (OptEt.Checked == true)
                    {
                        sEspEtOu = "AND";
                    }
                }


                if (GridGecet.Rows.Count > 0)
                {
                    GridGecet.DataSource = null;
                }

                tabArtSelect = new string[2];

                sqlSelect = "SELECT * FROM FacArticle ";

                sqlWhere = " WHERE CodeCategorieArticle='D' ";

                if (!string.IsNullOrEmpty(txtCodeChrono.Text))//tested
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE FacArticle.CodeArticle LIKE '" + txtCodeChrono.Text.Replace("*", "%") + "'";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND FacArticle.CodeArticle LIKE '" + txtCodeChrono.Text.Replace("*", "%") + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtLibelle.Text))//tested
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE (" + General.fc_ReturnWhere("FacArticle.Designation1", (txtLibelle.Text), "1", sTypeRech, sEspEtOu) + ")";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND (" + General.fc_ReturnWhere("FacArticle.Designation1", (txtLibelle.Text), "1", sTypeRech, sEspEtOu.ToString()) + ")";
                    }
                }


                sqlSelect = sqlSelect + sqlWhere + " ORDER BY CodeArticle,Designation1 ";

                rsChercher = new DataTable();
                SDArsChercher = new SqlDataAdapter(sqlSelect, General.adocnn);
                SDArsChercher.Fill(rsChercher);

                if (rsChercher.Rows.Count > 1)//tested
                {
                    if (rsChercher.Rows.Count > 100)
                    {
                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous allez afficher " + rsChercher.Rows.Count + " articles." + "\n" + "Ca risque prendre du temps, souhaitez-vous continuer ?", "Nombre de données important", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            rsChercher?.Dispose();
                            SDArsChercher?.Dispose();
                            GridGecet.Visible = false;
                            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                            return functionReturnValue;
                        }
                    }
                    lblTotal.Text = rsChercher.Rows.Count + " produits en réponse";
                }
                else if (rsChercher.Rows.Count == 1)
                {
                    lblTotal.Text = rsChercher.Rows.Count + " produit en réponse";
                }
                else
                {
                    lblTotal.Text = "Aucun produit en réponse";
                }
                System.Windows.Forms.Application.DoEvents();

                var newRow = GridGecetSource.NewRow();

                if (rsChercher.Rows.Count > 0)
                {
                    foreach (DataRow rsChercherRow in rsChercher.Rows)
                    {
                        var _with6 = rsChercher;
                        newRow["Choix"] = "0";
                        newRow["Qté"] = "0";
                        newRow["Chrono"] = rsChercherRow["CodeArticle"] + "";
                        newRow["Libelle"] = rsChercherRow["Designation1"];
                        newRow["Ref. fourn"] = "";
                        newRow["Unité métier"] = "";
                        newRow["FournPrix"] = "";
                        newRow["Prix"] = "";
                        newRow["Nb Fourn"] = "";
                        newRow["Fabricant"] = "" + "";
                        newRow["Caractéristiques"] = "Afficher";
                        newRow["CleAuto"] = rsChercherRow["CodeArticle"];
                        //  GridGecetSource.Rows.Add("0", "0", _with6.Rows[0]["CodeArticle"] + "", _with6.Rows[0]["Designation1"], "", "", "", "", "", "" + "", "Afficher", _with6.Rows[0]["CodeArticle"]);
                        GridGecetSource.Rows.Add(newRow.ItemArray);
                        GridGecet.DataSource = GridGecetSource;
                    }
                    GridGecet.Visible = true;
                }
                else
                {
                    GridGecet.Visible = false;
                }

                rsChercher?.Dispose();
                SDArsChercher?.Dispose();
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, " frmGecet fc_ChercheArticle ");
                return functionReturnValue;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <returns></returns>
        private object fc_CherchePoste()
        {
            object functionReturnValue = null;
            string sqlSelect = null;
            string sqlWhere = null;
            DataTable rsChercher = default(DataTable);
            SqlDataAdapter SDArsChercher = null;
            string sqlWhereFo = null;
            int lngNbArtFo = 0;

            string sTypeRech = "";
            string sEspEtOu = "";
            GridGecetSource = new DataTable();
            GridGecetSource.Columns.Add("Choix");
            GridGecetSource.Columns.Add("Qté");
            GridGecetSource.Columns.Add("Chrono");
            GridGecetSource.Columns.Add("Libelle");
            GridGecetSource.Columns.Add("Ref. fourn");
            GridGecetSource.Columns.Add("Unité métier");
            GridGecetSource.Columns.Add("FournPrix");
            GridGecetSource.Columns.Add("Prix");
            GridGecetSource.Columns.Add("Nb Fourn");
            GridGecetSource.Columns.Add("Fabricant");
            GridGecetSource.Columns.Add("Caractéristiques");
            GridGecetSource.Columns.Add("CleAuto");
            GridGecet.DataSource = GridGecetSource;
        
            try
            {
                if (optEtouOU.Checked == true)
                {
                    sTypeRech = cEtouOU;

                }
                else if (optEspace.Checked == true)
                {
                    sTypeRech = cEsp;
                    if (OptOu.Checked == true)
                    {
                        sEspEtOu = "OR";
                    }
                    else if (OptEt.Checked == true)
                    {
                        sEspEtOu = "AND";
                    }
                }



                if (GridGecet.Rows.Count > 0)
                {
                    GridGecet.DataSource = null;
                }

                tabArtSelect = new string[2];

                sqlSelect = "SELECT * FROM SousFamilleArticle ";

                if (!string.IsNullOrEmpty(txtCodeChrono.Text))//tested
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE SousFamilleArticle.CodeSousFamille LIKE '" + txtCodeChrono.Text.Replace("*", "%") + "'";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND SousFamilleArticle.CodeSousFamille LIKE '" + txtCodeChrono.Text.Replace("*", "%") + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtLibelle.Text))//tested
                {
                    if (string.IsNullOrEmpty(sqlWhere))
                    {
                        sqlWhere = " WHERE (" + General.fc_ReturnWhere("SousFamilleArticle.IntituleSousFamille", (txtLibelle.Text), "1", sTypeRech, sEspEtOu) + ")";
                    }
                    else
                    {
                        sqlWhere = sqlWhere + " AND (" + General.fc_ReturnWhere("SousFamilleArticle.IntituleSousFamille", (txtLibelle.Text), "1", sTypeRech, sEspEtOu) + ")";
                    }
                }


                sqlSelect = sqlSelect + sqlWhere + " ORDER BY CodeSousFamille,IntituleSousFamille ";

                rsChercher = new DataTable();
                SDArsChercher = new SqlDataAdapter(sqlSelect, General.adocnn);
                SDArsChercher.Fill(rsChercher);

                if (rsChercher.Rows.Count > 1)//tested
                {
                    if (rsChercher.Rows.Count > 100)
                    {
                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous allez afficher " + rsChercher.Rows.Count + " articles." + "\n" + "Ca risque prendre du temps, souhaitez-vous continuer ?", "Nombre de données important", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            rsChercher?.Dispose();
                            SDArsChercher?.Dispose();
                            GridGecet.Visible = false;
                            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                            return functionReturnValue;
                        }
                    }
                    lblTotal.Text = rsChercher.Rows.Count + " produits en réponse";
                }
                else if (rsChercher.Rows.Count == 1)//tested
                {
                    lblTotal.Text = rsChercher.Rows.Count + " produit en réponse";
                }
                else
                {
                    lblTotal.Text = "Aucun produit en réponse";
                }
                System.Windows.Forms.Application.DoEvents();

                var newRow = GridGecetSource.NewRow();
           
                if (rsChercher.Rows.Count > 0)
                {
                    foreach (DataRow rsChercherRow in rsChercher.Rows)
                    {
                      
                        var _with7 = rsChercher;
                        newRow["Choix"] = "0";
                        newRow["Qté"] = "0";
                        newRow["Chrono"] = rsChercherRow["CodeSousFamille"] + "";
                        newRow["Libelle"] = rsChercherRow["IntituleSousFamille"];
                        newRow["Ref. fourn"] = "";
                        newRow["Unité métier"] = "";
                        newRow["FournPrix"] = "";
                        newRow["Prix"] = "";
                        newRow["Nb Fourn"] = ""; 
                         newRow["Fabricant"] = "" + "";
                        newRow["Caractéristiques"] = "Afficher";
                        newRow["CleAuto"] = rsChercherRow["CodeSousFamille"];
                      
                        //  GridGecetSource.Rows.Add("0", "0", rsChercherRow["CodeSousFamille"] + "", rsChercherRow["IntituleSousFamille"], "", "", "", "", "", "" + "", "Afficher", rsChercherRow["CodeSousFamille"]);

                        GridGecetSource.Rows.Add(newRow.ItemArray);
                     
                         GridGecet.DataSource = GridGecetSource;
                    }
                    GridGecet.Visible = true;
                    
                }
                else
                {
                    GridGecet.Visible = false;
                }

                rsChercher?.Dispose();
                SDArsChercher?.Dispose();
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, " frmGecet fc_CherchePoste ");
                return functionReturnValue;
            }
        }

        private void frmGecet_Activated(object sender, EventArgs e)
        {
            int i = 0;
            optEspace.Checked = true;
            txtCodeChrono.Select();
            txtCodeChrono.Focus();
            if (txtOrigine.Text == Variable.cUserDocDevis)
            {
                CmdPanier.Visible = true;
            }
            else
            {
                CmdPanier.Visible = false;
            }
         
            switch (txtType.Text)
            {
                case "0":
                    //Fourniture Gecet
                   this.Text = "Recherche de fournitures Gecet";
                    /*GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Width = 764;
                   GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Width = 3674;
                   GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Width = 555;
                   GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Width = 794;
                   GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Width = 690;
                   GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Width = 1814;
                   GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Width = 800;
                   GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Width = 525;
                   GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Width = 975;
                   GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Width = 1275;
                   GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Width = 1814;*/
                    break;
                case "1":
                    //Article

                    this.Text = "Recherche d'articles";
                    Label11.Text = "Code";
                    
                    /*GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Header.Caption = "Code";
                   GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Header.Caption = "Libellé";
                   GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Header.Caption = "Ref. fourn";
                   GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Header.Caption = "Unité métier";
                   GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Header.Caption = "Qté";
                   GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Header.Caption = "Liste Fournisseur";
                   GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Header.Caption = "Tarif";
                   GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Header.Caption = "Nb Fourn";
                   GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Header.Caption = "Fabricant";
                   GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Header.Caption = "Caractéristiques";
                   GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Header.Caption = "Code";

                   GridGecet.DisplayLayout.Bands[0].Columns["Choix"].Hidden = false;
                   GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Hidden = false;
                   GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Hidden = false;
                   GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Hidden = true;
                   GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Hidden = true;
                   GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Hidden = false;
                   GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Hidden = true;
                   GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Hidden = true;
                   GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Hidden = true;
                   GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Hidden = true;
                   GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Hidden = true;
                   GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Hidden = true;*/

                    Label10.Visible = false;
                    Label12.Visible = false;
                    Label14.Visible = false;
                    Label15.Visible = false;
                    Label16.Visible = false;

                    txtFamille.Visible = false;
                    txtRefFab.Visible = false;
                    txtRefFourn.Visible = false;
                    txtMarque.Visible = false;
                    cmbFourn.Visible = false;

                    /*GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Width = 1470;
                    GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Width = 8580;
                    GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Width = 720;
                    GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Width = 01;*/
                    break;


                case "2":
                    //Poste
                    this.Text = "Recherche de postes";
                    Label11.Text = "Code";
                   /* GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Header.Caption = "Code";
                    GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Header.Caption = "Libellé";
                    GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Header.Caption = "Ref. fourn";
                    GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Header.Caption = "Unité métier";
                    GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Header.Caption = "Qté";
                    GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Header.Caption = "Liste Fournisseur";
                    GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Header.Caption = "Tarif";
                    GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Header.Caption = "Nb Fourn";
                    GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Header.Caption = "Fabricant";
                    GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Header.Caption = "Caractéristiques";
                    GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Header.Caption = "Code";

                    GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Hidden = false;
                    GridGecet.DisplayLayout.Bands[0].Columns["Choix"].Hidden = false;
                    GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Hidden = false;
                    GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Hidden = false;
                    GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Hidden = true;*/

                    Label10.Visible = false;
                    Label12.Visible = false;
                    Label14.Visible = false;
                    Label15.Visible = false;
                    Label16.Visible = false;

                    txtFamille.Visible = false;
                    txtRefFab.Visible = false;
                    txtRefFourn.Visible = false;
                    txtFamille.Visible = false;
                    txtMarque.Visible = false;
                    cmbFourn.Visible = false;

                   /* GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Width = 1470;
                    GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Width = 8579;
                    GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Width = 720;*/
                    break;

                case "3":
                    //Articles Gecet (sans les fournisseurs)

                    this.Text = "Recherche d'articles Gecet";
                    Label11.Text = "Chrono";
                    /* GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Header.Caption = "Chrono";
                    GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Header.Caption = "Libellé";
                    GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Header.Caption = "Ref. fourn";
                    GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Header.Caption = "Unité métier";
                    GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Header.Caption = "Qté";
                    GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Header.Caption = "Liste Fournisseur";
                    GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Header.Caption = "Tarif";
                    GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Header.Caption = "Nb Fourn";
                    GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Header.Caption = "Fabricant";
                    GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Header.Caption = "Caractéristiques";
                    GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Header.Caption = "Code";

                    GridGecet.DisplayLayout.Bands[0].Columns["Choix"].Hidden = false;
                    GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Hidden = false;
                    GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Hidden = false;
                    GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Hidden = false;
                    GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Hidden = false;
                    GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Hidden = true;*/

                    //Label1(0).Hidden = true
                    //Label1(2).Hidden = false
                    //Label1(4).Hidden = true
                    //Label1(5).Hidden = true
                    //Label1(6).Hidden = true

                    txtFamille.Visible = true;
                    //txtRefFab.Hidden = true
                    //txtRefFourn.Hidden = true
                    //txtMarque.Hidden = true
                    //cmbFourn.Hidden = true

                  /*  GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Width = 1470;
                    GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Width = 8580;
                    GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Width = 720;
                    GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Width = 525;
                    GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Width = 01;*/
                    break;

                default:
                    /* GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Width = 764;
                    GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Width = 3674;
                    GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Width = 555;
                    GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Width = 794;
                    GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Width = 690;
                    GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Width = 1814;
                    GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Width = 530;
                    GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Width = 525;
                    GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Width = 975;
                    GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Width = 1275;
                    GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Width = 1814;*/
                    break;
            }
            //Utilisé pour l'évènement "Resize" de la Form
            tabColPerCent = new double[GridGecet.DisplayLayout.Bands[0].Columns.Count];

            blnActivate = true;
        }

        private void frmGecet_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            int i = 0;

            tabArtSelect = new string[2];
            tabQteArt = new string[2];

            string sTemp = null;


            try
            {
                ModParametre.fc_OpenConnGecet();

                sTemp = General.getFrmReg(this.Name, "TypeRecherche", cEsp);

                if (sTemp.ToUpper() == cEsp.ToUpper())
                {
                    optEspace.Checked = true;
                }
                else if (sTemp.ToUpper() == cEtouOU.ToUpper())
                {
                    optEtouOU.Checked = true;
                }
                else
                {
                    optEspace.Checked = true;
                }

                sTemp = General.getFrmReg(this.Name, "EspaceEtOu", "AND");

                if (sTemp.ToUpper() == "AND".ToUpper())
                {
                    OptEt.Checked = true;
                }
                else if (sTemp.ToUpper() == "OR".ToUpper())
                {
                    OptOu.Checked = true;
                }
                else
                {
                    OptEt.Checked = true;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Form_Load");
            }
        }

        private void GridGecet_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            int i = 0;
            bool Match = false;

            if (GridGecet.ActiveCell == null)
                return;

            if (GridGecet.ActiveCell.Column.Index == GridGecet.ActiveRow.Cells["Qté"].Column.Index)
            {
                if (!string.IsNullOrEmpty(GridGecet.ActiveRow.Cells["Qté"].Text))
                {
                    if (!General.IsNumeric(GridGecet.ActiveRow.Cells["Qté"].Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur numérique dans le champ Qté.", "Saisie des quantité annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        e.Cancel = true;
                    }
                    else
                    {
                        if (GridGecet.ActiveRow.Cells["Choix"].Text.ToString() == "False")
                        {
                            GridGecet.ActiveRow.Cells["Choix"].Value = "True";
                        }
                        for (i = 0; i <= tabArtSelect.Length-1; i++)
                        {
                            if (tabArtSelect[i] == GridGecet.ActiveRow.Cells["CleAuto"].Text)
                            {
                                Match = true;
                                if (GridGecet.ActiveRow.Cells["Qté"].Text == "0" || string.IsNullOrEmpty(GridGecet.ActiveRow.Cells["Qté"].Text))
                                {
                                    tabArtSelect[i] = "";
                                    tabQteArt[i] = "";
                                }
                                else
                                {
                                    tabArtSelect[i] = GridGecet.ActiveRow.Cells["CleAuto"].Text;
                                    tabQteArt[i] = GridGecet.ActiveRow.Cells["Qté"].Text;
                                }
                            }
                        }
                        if (Match == false)
                        {
                            Array.Resize(ref tabArtSelect, tabArtSelect.Length + 2);
                            Array.Resize(ref tabQteArt, tabQteArt.Length + 2);
                            tabArtSelect[tabArtSelect.Length-1] = GridGecet.ActiveRow.Cells["CleAuto"].Text;
                            tabQteArt[tabQteArt.Length-1] = GridGecet.ActiveRow.Cells["Qté"].Text;
                        }
                    }
                }
                else
                {
                    if (GridGecet.ActiveRow.Cells["Choix"].Text.ToString() == "-1")
                    {
                        GridGecet.ActiveRow.Cells["Choix"].Value = "0";
                    }
                    GridGecet.ActiveRow.Cells["Qté"].Value = "0";
                    for (i = 0; i <= tabArtSelect.Length - 1; i++)
                    {
                        if (tabArtSelect[i] == GridGecet.ActiveRow.Cells["CleAuto"].Text)
                        {
                            Match = true;
                            if (GridGecet.ActiveRow.Cells["Qté"].Text == "0" || string.IsNullOrEmpty(GridGecet.ActiveRow.Cells["Qté"].Text))
                            {
                                tabArtSelect[i] = "";
                                tabQteArt[i] = "";
                            }
                            else
                            {
                                tabArtSelect[i] = GridGecet.ActiveRow.Cells["CleAuto"].Text;
                                tabQteArt[i] = GridGecet.ActiveRow.Cells["Qté"].Text;
                            }
                        }
                    }
                    if (Match == false)
                    {
                        Array.Resize(ref tabArtSelect, tabArtSelect.Length + 2);
                        Array.Resize(ref tabQteArt, tabQteArt.Length + 2);
                        tabArtSelect[tabArtSelect.Length-1] = GridGecet.ActiveRow.Cells["CleAuto"].Text;
                        tabQteArt[tabQteArt.Length-1] = GridGecet.ActiveRow.Cells["Qté"].Text;
                    }
                }
            }
            else if (GridGecet.ActiveCell.Column.Index == GridGecet.ActiveRow.Cells["Choix"].Column.Index)
            {
                if (GridGecet.ActiveRow.Cells["Choix"].Text.ToString() == "True")
                {
                    if (General.nz((GridGecet.ActiveRow.Cells["Qté"].Text), "0").ToString() == "0")
                    {
                        GridGecet.ActiveRow.Cells["Qté"].Value = "1";
                    }
                }
                else
                {
                    GridGecet.ActiveRow.Cells["Qté"].Value = "0";
                }
                for (i = 0; i <= tabArtSelect.Length - 1; i++)
                {
                    if (tabArtSelect[i] == GridGecet.ActiveRow.Cells["CleAuto"].Text)
                    {
                        Match = true;
                        if (GridGecet.ActiveRow.Cells["Qté"].Text == "0" || string.IsNullOrEmpty(GridGecet.ActiveRow.Cells["Qté"].Text))
                        {
                            tabArtSelect[i] = "";
                            tabQteArt[i] = "";
                        }
                        else
                        {
                            tabArtSelect[i] = GridGecet.ActiveRow.Cells["CleAuto"].Text;
                            tabQteArt[i] = GridGecet.ActiveRow.Cells["Qté"].Text;
                        }
                    }
                }
                if (Match == false)
                {
                    Array.Resize(ref tabArtSelect, tabArtSelect.Length - 1 + 2);
                    Array.Resize(ref tabQteArt, tabQteArt.Length - 1 + 2);
                    tabArtSelect[tabArtSelect.Length - 1] = GridGecet.ActiveRow.Cells["CleAuto"].Text;
                    tabQteArt[tabQteArt.Length - 1] = GridGecet.ActiveRow.Cells["Qté"].Text;
                }
            }
        }

        private void GridGecet_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            frmDetailArticle frmDetailArticle = new frmDetailArticle();
            frmDetailArticle.txtNoAuto.Text = GridGecet.ActiveRow.Cells["CleAuto"].Text;
            frmDetailArticle.ShowDialog();
        }

        private void optEspace_CheckedChanged(object sender, EventArgs e)
        {
            if (optEspace.Checked)
            {
                fc_SaveSettingPos();
            }
        }
       
        private void OptEt_CheckedChanged(object sender, EventArgs e)
        {
            if (OptEt.Checked)
            {
                fc_SaveSettingPos();
            }
        }

        private void optEtouOU_CheckedChanged(object sender, EventArgs e)
        {
            if (optEtouOU.Checked)
            {
                fc_SaveSettingPos();
            }
        }

        private void OptOu_CheckedChanged(object sender, EventArgs e)
        {
            if (OptOu.Checked)
            {
                fc_SaveSettingPos();
            }
        }

        private void txtCodeChrono_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)(eventArgs.KeyChar);
            if (KeyAscii == 13)
            {
                cmdRechercher_Click(cmdRechercher, new System.EventArgs());
            }
        }

        private void txtFamille_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)(eventArgs.KeyChar);
            if (KeyAscii == 13)
            {
                cmdRechercher_Click(cmdRechercher, new System.EventArgs());
            }
        }
        private void txtLibelle_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)(eventArgs.KeyChar);
            if (KeyAscii == 13)
            {
                cmdRechercher_Click(cmdRechercher, new System.EventArgs());
            }
        }

        private void txtMarque_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)(eventArgs.KeyChar);
            if (KeyAscii == 13)
            {
                cmdRechercher_Click(cmdRechercher, new System.EventArgs());
            }
        }

        private void txtRefFab_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)(eventArgs.KeyChar);
            if (KeyAscii == 13)
            {
                cmdRechercher_Click(cmdRechercher, new System.EventArgs());
            }
        }

        private void txtRefFourn_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)(eventArgs.KeyChar);
            if (KeyAscii == 13)
            {
                cmdRechercher_Click(cmdRechercher, new System.EventArgs());
            }
        }

        private void GridGecet_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
             GridGecet.DisplayLayout.Bands[0].Columns["Choix"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].CellButtonAppearance.BackColor = Color.Gray;
            switch (txtType.Text)
            {
                case "0":
                    //Fourniture Gecet
                    this.Text = "Recherche de fournitures Gecet";
                    GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Width = 76;
                     GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Width = 367;
                     GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Width = 55;
                     GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Width = 79;
                     GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Width = 69;
                     GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Width = 181;
                     GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Width = 80;
                     GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Width = 52;
                     GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Width = 97;
                     GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Width = 127;
                     GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Width = 181;
                    break;
                case "1":
                    //Article

                    this.Text = "Recherche d'articles";
                    Label11.Text = "Code";
                    GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Header.Caption = "Code";
                     GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Header.Caption = "Libellé";
                     GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Header.Caption = "Ref. fourn";
                     GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Header.Caption = "Unité métier";
                     GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Header.Caption = "Qté";
                     GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Header.Caption = "Liste Fournisseur";
                     GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Header.Caption = "Tarif";
                     GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Header.Caption = "Nb Fourn";
                     GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Header.Caption = "Fabricant";
                     GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Header.Caption = "Caractéristiques";
                     GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Header.Caption = "Code";

                     GridGecet.DisplayLayout.Bands[0].Columns["Choix"].Hidden = false;
                     GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Hidden = false;
                     GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Hidden = false;
                     GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Hidden = true;
                     GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Hidden = true;
                     GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Hidden = false;
                     GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Hidden = true;
                     GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Hidden = true;
                     GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Hidden = true;
                     GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Hidden = true;
                     GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Hidden = true;
                     GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Hidden = true;

                    Label10.Visible = false;
                    Label12.Visible = false;
                    Label14.Visible = false;
                    Label15.Visible = false;
                    Label16.Visible = false;

                    txtFamille.Visible = false;
                    txtRefFab.Visible = false;
                    txtRefFourn.Visible = false;
                    txtMarque.Visible = false;
                    cmbFourn.Visible = false;

                     GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Width = 147;
                      GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Width = 850;
                      GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Width = 72;
                      GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Width = 01;
                      GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Width = 01;
                      GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Width = 01;
                      GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Width = 01;
                      GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Width = 01;
                      GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Width = 01;
                      GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Width = 01;
                      GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Width = 01;
                    break;


                case "2":
                    //Poste
                    this.Text = "Recherche de postes";
                    Label11.Text = "Code";
                    GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Header.Caption = "Code";
                     GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Header.Caption = "Libellé";
                     GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Header.Caption = "Ref. fourn";
                     GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Header.Caption = "Unité métier";
                     GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Header.Caption = "Qté";
                     GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Header.Caption = "Liste Fournisseur";
                     GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Header.Caption = "Tarif";
                     GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Header.Caption = "Nb Fourn";
                     GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Header.Caption = "Fabricant";
                     GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Header.Caption = "Caractéristiques";
                     GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Header.Caption = "Code";

                     GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Hidden = false;
                     GridGecet.DisplayLayout.Bands[0].Columns["Choix"].Hidden = false;
                     GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Hidden = false;
                     GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Hidden = true;
                     GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Hidden = true;
                     GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Hidden = false;
                     GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Hidden = true;
                     GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Hidden = true;
                     GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Hidden = true;
                     GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Hidden = true;
                     GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Hidden = true;
                     GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Hidden = true;

                    Label10.Visible = false;
                    Label12.Visible = false;
                    Label14.Visible = false;
                    Label15.Visible = false;
                    Label16.Visible = false;

                    txtFamille.Visible = false;
                    txtRefFab.Visible = false;
                    txtRefFourn.Visible = false;
                    txtFamille.Visible = false;
                    txtMarque.Visible = false;
                    cmbFourn.Visible = false;

                      GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Width = 147;
                      GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Width = 857;
                      GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Width = 72;
                    break;

                case "3":
                    //Articles Gecet (sans les fournisseurs)

                    this.Text = "Recherche d'articles Gecet";
                    Label11.Text = "Chrono";
                    GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Header.Caption = "Chrono";
                    GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Header.Caption = "Libellé";
                    GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Header.Caption = "Ref. fourn";
                    GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Header.Caption = "Unité métier";
                    GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Header.Caption = "Qté";
                    GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Header.Caption = "Liste Fournisseur";
                    GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Header.Caption = "Tarif";
                    GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Header.Caption = "Nb Fourn";
                    GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Header.Caption = "Fabricant";
                    GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Header.Caption = "Caractéristiques";
                    GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Header.Caption = "Code";

                    GridGecet.DisplayLayout.Bands[0].Columns["Choix"].Hidden = false;
                    GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Hidden = false;
                    GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Hidden = false;
                    GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Hidden = false;
                    GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Hidden = false;
                    GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Hidden = true;
                    GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Hidden = true;

                    //Label1(0).Hidden = true
                    //Label1(2).Hidden = false
                    //Label1(4).Hidden = true
                    //Label1(5).Hidden = true
                    //Label1(6).Hidden = true

                    txtFamille.Visible = true;
                    //txtRefFab.Hidden = true
                    //txtRefFourn.Hidden = true
                    //txtMarque.Hidden = true
                    //cmbFourn.Hidden = true

                    GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Width = 147;
                    GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Width = 85;
                    GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Width = 72;
                    GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Width = 52;
                    GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Width = 01;
                    GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Width = 01;
                    break;

                default:
                    GridGecet.DisplayLayout.Bands[0].Columns["Chrono"].Width = 76;
                    GridGecet.DisplayLayout.Bands[0].Columns["Libelle"].Width = 367;
                    GridGecet.DisplayLayout.Bands[0].Columns["Qté"].Width = 55;
                    GridGecet.DisplayLayout.Bands[0].Columns["Ref. fourn"].Width = 79;
                    GridGecet.DisplayLayout.Bands[0].Columns["Unité métier"].Width = 69;
                    GridGecet.DisplayLayout.Bands[0].Columns["FournPrix"].Width = 181;
                    GridGecet.DisplayLayout.Bands[0].Columns["Prix"].Width = 53;
                    GridGecet.DisplayLayout.Bands[0].Columns["Nb Fourn"].Width = 525;
                    GridGecet.DisplayLayout.Bands[0].Columns["Fabricant"].Width = 975;
                    GridGecet.DisplayLayout.Bands[0].Columns["Caractéristiques"].Width = 127;
                    GridGecet.DisplayLayout.Bands[0].Columns["CleAuto"].Width = 181;
                    break;
            }

        }

        private void GridGecet_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            if (e.Row.Cells["Choix"].Value.ToString()== "0") {

                e.Row.Cells["Choix"].Value = "False";
            }
            GridGecet.UpdateData();
        }

        
    }
}
