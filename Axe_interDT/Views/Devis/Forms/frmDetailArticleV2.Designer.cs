﻿namespace Axe_interDT.Views.Devis.Forms
{
    partial class frmDetailArticleV2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cmdFamille = new System.Windows.Forms.Button();
            this.ssGridEquiv = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtART_NbLien = new iTalk.iTalk_TextBox_Small2();
            this.label26 = new System.Windows.Forms.Label();
            this.txtART_TypoAtoll = new iTalk.iTalk_TextBox_Small2();
            this.label20 = new System.Windows.Forms.Label();
            this.txtART_SupprimeFab = new iTalk.iTalk_TextBox_Small2();
            this.label21 = new System.Windows.Forms.Label();
            this.txtART_StatutVie = new iTalk.iTalk_TextBox_Small2();
            this.label22 = new System.Windows.Forms.Label();
            this.txtART_Remplacement = new iTalk.iTalk_TextBox_Small2();
            this.label23 = new System.Windows.Forms.Label();
            this.txtART_Tva = new iTalk.iTalk_TextBox_Small2();
            this.label24 = new System.Windows.Forms.Label();
            this.txtART_familleGroupe = new iTalk.iTalk_TextBox_Small2();
            this.label25 = new System.Windows.Forms.Label();
            this.txtART_LibelleDevServ = new iTalk.iTalk_TextBox_Small2();
            this.label18 = new System.Windows.Forms.Label();
            this.Text39 = new iTalk.iTalk_TextBox_Small2();
            this.label19s = new System.Windows.Forms.Label();
            this.txtART_Gamme = new iTalk.iTalk_TextBox_Small2();
            this.label16 = new System.Windows.Forms.Label();
            this.txtART_Marque = new iTalk.iTalk_TextBox_Small2();
            this.label17 = new System.Windows.Forms.Label();
            this.txtART_CorpEtat = new iTalk.iTalk_TextBox_Small2();
            this.label14 = new System.Windows.Forms.Label();
            this.txtART_UnitsStock = new iTalk.iTalk_TextBox_Small2();
            this.label15 = new System.Windows.Forms.Label();
            this.txtART_Volume = new iTalk.iTalk_TextBox_Small2();
            this.label12 = new System.Windows.Forms.Label();
            this.txtART_UniteDev = new iTalk.iTalk_TextBox_Small2();
            this.label13 = new System.Windows.Forms.Label();
            this.txtART_hauteur = new iTalk.iTalk_TextBox_Small2();
            this.label10 = new System.Windows.Forms.Label();
            this.txtART_Surface = new iTalk.iTalk_TextBox_Small2();
            this.label11 = new System.Windows.Forms.Label();
            this.txtART_Largeur = new iTalk.iTalk_TextBox_Small2();
            this.label9 = new System.Windows.Forms.Label();
            this.txtART_Longueur = new iTalk.iTalk_TextBox_Small2();
            this.label8 = new System.Windows.Forms.Label();
            this.txtART_poids = new iTalk.iTalk_TextBox_Small2();
            this.label7 = new System.Windows.Forms.Label();
            this.txtART_UnitPoidsTheo = new iTalk.iTalk_TextBox_Small2();
            this.label6 = new System.Windows.Forms.Label();
            this.txtART_PoidsTheo = new iTalk.iTalk_TextBox_Small2();
            this.label5 = new System.Windows.Forms.Label();
            this.txtART_Synonyme = new iTalk.iTalk_TextBox_Small2();
            this.label4 = new System.Windows.Forms.Label();
            this.txtART_LibelleTechnique = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.txtART_Libelle = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.txtART_Famille = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.txtART_Chrono = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtARTP_EcoTaxe = new iTalk.iTalk_TextBox_Small2();
            this.label56 = new System.Windows.Forms.Label();
            this.txtARTP_MajorDevis = new iTalk.iTalk_TextBox_Small2();
            this.label55 = new System.Windows.Forms.Label();
            this.txtARTP_RemiseIndic = new iTalk.iTalk_TextBox_Small2();
            this.label48 = new System.Windows.Forms.Label();
            this.txtARTP_RemisePourCent = new iTalk.iTalk_TextBox_Small2();
            this.label49 = new System.Windows.Forms.Label();
            this.txtARTP_UniteDevis = new iTalk.iTalk_TextBox_Small2();
            this.label50 = new System.Windows.Forms.Label();
            this.txtARTP_PrixNetDevis = new iTalk.iTalk_TextBox_Small2();
            this.label51 = new System.Windows.Forms.Label();
            this.txtARTP_LibelleCond = new iTalk.iTalk_TextBox_Small2();
            this.label52 = new System.Windows.Forms.Label();
            this.txtARTP_PrixTarif = new iTalk.iTalk_TextBox_Small2();
            this.label53 = new System.Windows.Forms.Label();
            this.txtARTP_StatutPrixNet = new iTalk.iTalk_TextBox_Small2();
            this.label54 = new System.Windows.Forms.Label();
            this.txtARTP_PrixNetStock = new iTalk.iTalk_TextBox_Small2();
            this.label38 = new System.Windows.Forms.Label();
            this.txtARTP_PrixNet = new iTalk.iTalk_TextBox_Small2();
            this.label39 = new System.Windows.Forms.Label();
            this.txtARTP_PrixPublic = new iTalk.iTalk_TextBox_Small2();
            this.label40 = new System.Windows.Forms.Label();
            this.txtARTP_UniteFourn = new iTalk.iTalk_TextBox_Small2();
            this.label41 = new System.Windows.Forms.Label();
            this.txtARTP_QteMinUC = new iTalk.iTalk_TextBox_Small2();
            this.label42 = new System.Windows.Forms.Label();
            this.txtARTP_PrixMulti = new iTalk.iTalk_TextBox_Small2();
            this.label43 = new System.Windows.Forms.Label();
            this.txtARTP_UniteValeur = new iTalk.iTalk_TextBox_Small2();
            this.label44 = new System.Windows.Forms.Label();
            this.txtARTP_PrixNetCond = new iTalk.iTalk_TextBox_Small2();
            this.label45 = new System.Windows.Forms.Label();
            this.txtARTP_UnitCondFourn = new iTalk.iTalk_TextBox_Small2();
            this.label46 = new System.Windows.Forms.Label();
            this.txtARTP_QteMinUCNF = new iTalk.iTalk_TextBox_Small2();
            this.label47 = new System.Windows.Forms.Label();
            this.txtARTP_DateAplication = new iTalk.iTalk_TextBox_Small2();
            this.label27 = new System.Windows.Forms.Label();
            this.txtARTP_CoefVente = new iTalk.iTalk_TextBox_Small2();
            this.label28 = new System.Windows.Forms.Label();
            this.txtARTP_DelaiLivr = new iTalk.iTalk_TextBox_Small2();
            this.label29 = new System.Windows.Forms.Label();
            this.txtARTP_Statut = new iTalk.iTalk_TextBox_Small2();
            this.label30 = new System.Windows.Forms.Label();
            this.txtARTP_Devise = new iTalk.iTalk_TextBox_Small2();
            this.label31 = new System.Windows.Forms.Label();
            this.txtARTP_RefFourn = new iTalk.iTalk_TextBox_Small2();
            this.label32 = new System.Windows.Forms.Label();
            this.txtARTP_RefFab = new iTalk.iTalk_TextBox_Small2();
            this.label34 = new System.Windows.Forms.Label();
            this.txtARTP_Marque = new iTalk.iTalk_TextBox_Small2();
            this.label35 = new System.Windows.Forms.Label();
            this.txtARTP_Nomfourn = new iTalk.iTalk_TextBox_Small2();
            this.label36 = new System.Windows.Forms.Label();
            this.txtARTP_Nofourn = new iTalk.iTalk_TextBox_Small2();
            this.label37 = new System.Windows.Forms.Label();
            this.ssGridURL = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtART_UcnsParUs = new iTalk.iTalk_TextBox_Small2();
            this.txtART_DebitGaz = new iTalk.iTalk_TextBox_Small2();
            this.txtART_DebitEau = new iTalk.iTalk_TextBox_Small2();
            this.txtART_RaccordGaz = new iTalk.iTalk_TextBox_Small2();
            this.txtART_Evacuation = new iTalk.iTalk_TextBox_Small2();
            this.txtART_ConnetEC = new iTalk.iTalk_TextBox_Small2();
            this.txtART_ConnetEf = new iTalk.iTalk_TextBox_Small2();
            this.txtART_KfUdParAncUd = new iTalk.iTalk_TextBox_Small2();
            this.txtART_KfusParAncUs = new iTalk.iTalk_TextBox_Small2();
            this.txtART_Composant = new iTalk.iTalk_TextBox_Small2();
            this.txtART_Type = new iTalk.iTalk_TextBox_Small2();
            this.txtART_ChronoPrix = new iTalk.iTalk_TextBox_Small2();
            this.txtART_CondUnitStock = new iTalk.iTalk_TextBox_Small2();
            this.txtART_SousFam = new iTalk.iTalk_TextBox_Small2();
            this.txtARTP_DatePrix = new iTalk.iTalk_TextBox_Small2();
            this.txtARTP_PrixDonnePar = new iTalk.iTalk_TextBox_Small2();
            this.txtARTP_PrixDevServ = new iTalk.iTalk_TextBox_Small2();
            this.txtARTP_fournPref = new iTalk.iTalk_TextBox_Small2();
            this.txtARTP_PrixNetDepan = new iTalk.iTalk_TextBox_Small2();
            this.txtARTP_UsParUc = new iTalk.iTalk_TextBox_Small2();
            this.txtARTP_UdParUs = new iTalk.iTalk_TextBox_Small2();
            this.txtARTP_UvParUc = new iTalk.iTalk_TextBox_Small2();
            this.txtARTP_UcnfParUc = new iTalk.iTalk_TextBox_Small2();
            this.txtARTP_RefHisto = new iTalk.iTalk_TextBox_Small2();
            this.txtARTP_RefInfo = new iTalk.iTalk_TextBox_Small2();
            this.txtART_CategorieOutil = new iTalk.iTalk_TextBox_Small2();
            this.txtART_RubriqueAnaly = new iTalk.iTalk_TextBox_Small2();
            this.txtART_EtatDiffusion = new iTalk.iTalk_TextBox_Small2();
            this.txtART_FamAtoll = new iTalk.iTalk_TextBox_Small2();
            this.txtART_Nature = new iTalk.iTalk_TextBox_Small2();
            this.Label19 = new System.Windows.Forms.Label();
            this.txtART_dateFinActivite = new iTalk.iTalk_TextBox_Small2();
            this.txtARTP_UsParUv = new iTalk.iTalk_TextBox_Small2();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridEquiv)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridURL)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.cmdFamille);
            this.groupBox6.Controls.Add(this.ssGridEquiv);
            this.groupBox6.Controls.Add(this.txtART_NbLien);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.txtART_TypoAtoll);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.txtART_SupprimeFab);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.txtART_StatutVie);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Controls.Add(this.txtART_Remplacement);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.txtART_Tva);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.txtART_familleGroupe);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this.txtART_LibelleDevServ);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.Text39);
            this.groupBox6.Controls.Add(this.label19s);
            this.groupBox6.Controls.Add(this.txtART_Gamme);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.txtART_Marque);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Controls.Add(this.txtART_CorpEtat);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.txtART_UnitsStock);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.txtART_Volume);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.txtART_UniteDev);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.txtART_hauteur);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.txtART_Surface);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.txtART_Largeur);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.txtART_Longueur);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.txtART_poids);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.txtART_UnitPoidsTheo);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.txtART_PoidsTheo);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.txtART_Synonyme);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.txtART_LibelleTechnique);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.txtART_Libelle);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.txtART_Famille);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Controls.Add(this.txtART_Chrono);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox6.Location = new System.Drawing.Point(12, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1198, 394);
            this.groupBox6.TabIndex = 411;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "ARTICLE";
            // 
            // cmdFamille
            // 
            this.cmdFamille.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFamille.FlatAppearance.BorderSize = 0;
            this.cmdFamille.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFamille.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdFamille.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdFamille.Location = new System.Drawing.Point(361, 49);
            this.cmdFamille.Name = "cmdFamille";
            this.cmdFamille.Size = new System.Drawing.Size(25, 20);
            this.cmdFamille.TabIndex = 573;
            this.cmdFamille.UseVisualStyleBackColor = false;
            // 
            // ssGridEquiv
            // 
            this.ssGridEquiv.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ssGridEquiv.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ssGridEquiv.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssGridEquiv.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridEquiv.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridEquiv.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridEquiv.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridEquiv.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ssGridEquiv.DisplayLayout.UseFixedHeaders = true;
            this.ssGridEquiv.Location = new System.Drawing.Point(801, 247);
            this.ssGridEquiv.Name = "ssGridEquiv";
            this.ssGridEquiv.Size = new System.Drawing.Size(388, 135);
            this.ssGridEquiv.TabIndex = 572;
            this.ssGridEquiv.Text = "ARTICLE EQUIVALENT";
            this.ssGridEquiv.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridEquiv_InitializeLayout);
            this.ssGridEquiv.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.ssGridEquiv_Error);
            // 
            // txtART_NbLien
            // 
            this.txtART_NbLien.AccAcceptNumbersOnly = false;
            this.txtART_NbLien.AccAllowComma = false;
            this.txtART_NbLien.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_NbLien.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_NbLien.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_NbLien.AccHidenValue = "";
            this.txtART_NbLien.AccNotAllowedChars = null;
            this.txtART_NbLien.AccReadOnly = false;
            this.txtART_NbLien.AccReadOnlyAllowDelete = false;
            this.txtART_NbLien.AccRequired = false;
            this.txtART_NbLien.BackColor = System.Drawing.Color.White;
            this.txtART_NbLien.CustomBackColor = System.Drawing.Color.White;
            this.txtART_NbLien.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_NbLien.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_NbLien.Location = new System.Drawing.Point(1002, 209);
            this.txtART_NbLien.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_NbLien.MaxLength = 32767;
            this.txtART_NbLien.Multiline = false;
            this.txtART_NbLien.Name = "txtART_NbLien";
            this.txtART_NbLien.ReadOnly = false;
            this.txtART_NbLien.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_NbLien.Size = new System.Drawing.Size(187, 27);
            this.txtART_NbLien.TabIndex = 555;
            this.txtART_NbLien.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_NbLien.UseSystemPasswordChar = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(798, 210);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(107, 16);
            this.label26.TabIndex = 554;
            this.label26.Text = "Nombre de liens";
            // 
            // txtART_TypoAtoll
            // 
            this.txtART_TypoAtoll.AccAcceptNumbersOnly = false;
            this.txtART_TypoAtoll.AccAllowComma = false;
            this.txtART_TypoAtoll.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_TypoAtoll.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_TypoAtoll.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_TypoAtoll.AccHidenValue = "";
            this.txtART_TypoAtoll.AccNotAllowedChars = null;
            this.txtART_TypoAtoll.AccReadOnly = false;
            this.txtART_TypoAtoll.AccReadOnlyAllowDelete = false;
            this.txtART_TypoAtoll.AccRequired = false;
            this.txtART_TypoAtoll.BackColor = System.Drawing.Color.White;
            this.txtART_TypoAtoll.CustomBackColor = System.Drawing.Color.White;
            this.txtART_TypoAtoll.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_TypoAtoll.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_TypoAtoll.Location = new System.Drawing.Point(1002, 178);
            this.txtART_TypoAtoll.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_TypoAtoll.MaxLength = 32767;
            this.txtART_TypoAtoll.Multiline = false;
            this.txtART_TypoAtoll.Name = "txtART_TypoAtoll";
            this.txtART_TypoAtoll.ReadOnly = false;
            this.txtART_TypoAtoll.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_TypoAtoll.Size = new System.Drawing.Size(187, 27);
            this.txtART_TypoAtoll.TabIndex = 553;
            this.txtART_TypoAtoll.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_TypoAtoll.UseSystemPasswordChar = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(798, 179);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(99, 16);
            this.label20.TabIndex = 552;
            this.label20.Text = "Typologie Atoll";
            // 
            // txtART_SupprimeFab
            // 
            this.txtART_SupprimeFab.AccAcceptNumbersOnly = false;
            this.txtART_SupprimeFab.AccAllowComma = false;
            this.txtART_SupprimeFab.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_SupprimeFab.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_SupprimeFab.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_SupprimeFab.AccHidenValue = "";
            this.txtART_SupprimeFab.AccNotAllowedChars = null;
            this.txtART_SupprimeFab.AccReadOnly = false;
            this.txtART_SupprimeFab.AccReadOnlyAllowDelete = false;
            this.txtART_SupprimeFab.AccRequired = false;
            this.txtART_SupprimeFab.BackColor = System.Drawing.Color.White;
            this.txtART_SupprimeFab.CustomBackColor = System.Drawing.Color.White;
            this.txtART_SupprimeFab.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_SupprimeFab.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_SupprimeFab.Location = new System.Drawing.Point(1002, 147);
            this.txtART_SupprimeFab.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_SupprimeFab.MaxLength = 32767;
            this.txtART_SupprimeFab.Multiline = false;
            this.txtART_SupprimeFab.Name = "txtART_SupprimeFab";
            this.txtART_SupprimeFab.ReadOnly = false;
            this.txtART_SupprimeFab.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_SupprimeFab.Size = new System.Drawing.Size(187, 27);
            this.txtART_SupprimeFab.TabIndex = 551;
            this.txtART_SupprimeFab.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_SupprimeFab.UseSystemPasswordChar = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(798, 148);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(122, 16);
            this.label21.TabIndex = 550;
            this.label21.Text = "Sup chez Fabricant";
            this.label21.Click += new System.EventHandler(this.label21_Click);
            // 
            // txtART_StatutVie
            // 
            this.txtART_StatutVie.AccAcceptNumbersOnly = false;
            this.txtART_StatutVie.AccAllowComma = false;
            this.txtART_StatutVie.AccBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtART_StatutVie.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_StatutVie.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_StatutVie.AccHidenValue = "";
            this.txtART_StatutVie.AccNotAllowedChars = null;
            this.txtART_StatutVie.AccReadOnly = false;
            this.txtART_StatutVie.AccReadOnlyAllowDelete = false;
            this.txtART_StatutVie.AccRequired = false;
            this.txtART_StatutVie.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtART_StatutVie.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtART_StatutVie.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_StatutVie.ForeColor = System.Drawing.Color.Black;
            this.txtART_StatutVie.Location = new System.Drawing.Point(1002, 116);
            this.txtART_StatutVie.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_StatutVie.MaxLength = 32767;
            this.txtART_StatutVie.Multiline = false;
            this.txtART_StatutVie.Name = "txtART_StatutVie";
            this.txtART_StatutVie.ReadOnly = false;
            this.txtART_StatutVie.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_StatutVie.Size = new System.Drawing.Size(187, 27);
            this.txtART_StatutVie.TabIndex = 549;
            this.txtART_StatutVie.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_StatutVie.UseSystemPasswordChar = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(798, 117);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(198, 15);
            this.label22.TabIndex = 548;
            this.label22.Text = "Statut vie article (actif, inactif)";
            // 
            // txtART_Remplacement
            // 
            this.txtART_Remplacement.AccAcceptNumbersOnly = false;
            this.txtART_Remplacement.AccAllowComma = false;
            this.txtART_Remplacement.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Remplacement.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Remplacement.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Remplacement.AccHidenValue = "";
            this.txtART_Remplacement.AccNotAllowedChars = null;
            this.txtART_Remplacement.AccReadOnly = false;
            this.txtART_Remplacement.AccReadOnlyAllowDelete = false;
            this.txtART_Remplacement.AccRequired = false;
            this.txtART_Remplacement.BackColor = System.Drawing.Color.White;
            this.txtART_Remplacement.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Remplacement.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Remplacement.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_Remplacement.Location = new System.Drawing.Point(1002, 85);
            this.txtART_Remplacement.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Remplacement.MaxLength = 32767;
            this.txtART_Remplacement.Multiline = false;
            this.txtART_Remplacement.Name = "txtART_Remplacement";
            this.txtART_Remplacement.ReadOnly = false;
            this.txtART_Remplacement.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Remplacement.Size = new System.Drawing.Size(187, 27);
            this.txtART_Remplacement.TabIndex = 547;
            this.txtART_Remplacement.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Remplacement.UseSystemPasswordChar = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(798, 86);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(100, 16);
            this.label23.TabIndex = 546;
            this.label23.Text = "Remplacement";
            // 
            // txtART_Tva
            // 
            this.txtART_Tva.AccAcceptNumbersOnly = false;
            this.txtART_Tva.AccAllowComma = false;
            this.txtART_Tva.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Tva.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Tva.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Tva.AccHidenValue = "";
            this.txtART_Tva.AccNotAllowedChars = null;
            this.txtART_Tva.AccReadOnly = false;
            this.txtART_Tva.AccReadOnlyAllowDelete = false;
            this.txtART_Tva.AccRequired = false;
            this.txtART_Tva.BackColor = System.Drawing.Color.White;
            this.txtART_Tva.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Tva.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Tva.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_Tva.Location = new System.Drawing.Point(1002, 54);
            this.txtART_Tva.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Tva.MaxLength = 32767;
            this.txtART_Tva.Multiline = false;
            this.txtART_Tva.Name = "txtART_Tva";
            this.txtART_Tva.ReadOnly = false;
            this.txtART_Tva.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Tva.Size = new System.Drawing.Size(187, 27);
            this.txtART_Tva.TabIndex = 545;
            this.txtART_Tva.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Tva.UseSystemPasswordChar = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(798, 55);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(35, 16);
            this.label24.TabIndex = 544;
            this.label24.Text = "TVA";
            // 
            // txtART_familleGroupe
            // 
            this.txtART_familleGroupe.AccAcceptNumbersOnly = false;
            this.txtART_familleGroupe.AccAllowComma = false;
            this.txtART_familleGroupe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_familleGroupe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_familleGroupe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_familleGroupe.AccHidenValue = "";
            this.txtART_familleGroupe.AccNotAllowedChars = null;
            this.txtART_familleGroupe.AccReadOnly = false;
            this.txtART_familleGroupe.AccReadOnlyAllowDelete = false;
            this.txtART_familleGroupe.AccRequired = false;
            this.txtART_familleGroupe.BackColor = System.Drawing.Color.White;
            this.txtART_familleGroupe.CustomBackColor = System.Drawing.Color.White;
            this.txtART_familleGroupe.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_familleGroupe.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_familleGroupe.Location = new System.Drawing.Point(1002, 23);
            this.txtART_familleGroupe.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_familleGroupe.MaxLength = 32767;
            this.txtART_familleGroupe.Multiline = false;
            this.txtART_familleGroupe.Name = "txtART_familleGroupe";
            this.txtART_familleGroupe.ReadOnly = false;
            this.txtART_familleGroupe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_familleGroupe.Size = new System.Drawing.Size(187, 27);
            this.txtART_familleGroupe.TabIndex = 543;
            this.txtART_familleGroupe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_familleGroupe.UseSystemPasswordChar = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(798, 24);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(158, 16);
            this.label25.TabIndex = 542;
            this.label25.Text = "Famille de regroupement";
            // 
            // txtART_LibelleDevServ
            // 
            this.txtART_LibelleDevServ.AccAcceptNumbersOnly = false;
            this.txtART_LibelleDevServ.AccAllowComma = false;
            this.txtART_LibelleDevServ.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_LibelleDevServ.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_LibelleDevServ.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_LibelleDevServ.AccHidenValue = "";
            this.txtART_LibelleDevServ.AccNotAllowedChars = null;
            this.txtART_LibelleDevServ.AccReadOnly = false;
            this.txtART_LibelleDevServ.AccReadOnlyAllowDelete = false;
            this.txtART_LibelleDevServ.AccRequired = false;
            this.txtART_LibelleDevServ.BackColor = System.Drawing.Color.White;
            this.txtART_LibelleDevServ.CustomBackColor = System.Drawing.Color.White;
            this.txtART_LibelleDevServ.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_LibelleDevServ.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_LibelleDevServ.Location = new System.Drawing.Point(594, 362);
            this.txtART_LibelleDevServ.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_LibelleDevServ.MaxLength = 32767;
            this.txtART_LibelleDevServ.Multiline = false;
            this.txtART_LibelleDevServ.Name = "txtART_LibelleDevServ";
            this.txtART_LibelleDevServ.ReadOnly = false;
            this.txtART_LibelleDevServ.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_LibelleDevServ.Size = new System.Drawing.Size(187, 27);
            this.txtART_LibelleDevServ.TabIndex = 541;
            this.txtART_LibelleDevServ.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_LibelleDevServ.UseSystemPasswordChar = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(388, 363);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(142, 16);
            this.label18.TabIndex = 540;
            this.label18.Text = "Libellé Devis Services";
            // 
            // Text39
            // 
            this.Text39.AccAcceptNumbersOnly = false;
            this.Text39.AccAllowComma = false;
            this.Text39.AccBackgroundColor = System.Drawing.Color.White;
            this.Text39.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text39.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.Text39.AccHidenValue = "";
            this.Text39.AccNotAllowedChars = null;
            this.Text39.AccReadOnly = false;
            this.Text39.AccReadOnlyAllowDelete = false;
            this.Text39.AccRequired = false;
            this.Text39.BackColor = System.Drawing.Color.White;
            this.Text39.CustomBackColor = System.Drawing.Color.White;
            this.Text39.Font = new System.Drawing.Font("Tahoma", 10F);
            this.Text39.ForeColor = System.Drawing.Color.DodgerBlue;
            this.Text39.Location = new System.Drawing.Point(594, 331);
            this.Text39.Margin = new System.Windows.Forms.Padding(2);
            this.Text39.MaxLength = 32767;
            this.Text39.Multiline = false;
            this.Text39.Name = "Text39";
            this.Text39.ReadOnly = false;
            this.Text39.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text39.Size = new System.Drawing.Size(187, 27);
            this.Text39.TabIndex = 539;
            this.Text39.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text39.UseSystemPasswordChar = false;
            // 
            // label19s
            // 
            this.label19s.AutoSize = true;
            this.label19s.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19s.Location = new System.Drawing.Point(388, 332);
            this.label19s.Name = "label19s";
            this.label19s.Size = new System.Drawing.Size(93, 16);
            this.label19s.TabIndex = 538;
            this.label19s.Text = "Unité de stock";
            // 
            // txtART_Gamme
            // 
            this.txtART_Gamme.AccAcceptNumbersOnly = false;
            this.txtART_Gamme.AccAllowComma = false;
            this.txtART_Gamme.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Gamme.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Gamme.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Gamme.AccHidenValue = "";
            this.txtART_Gamme.AccNotAllowedChars = null;
            this.txtART_Gamme.AccReadOnly = false;
            this.txtART_Gamme.AccReadOnlyAllowDelete = false;
            this.txtART_Gamme.AccRequired = false;
            this.txtART_Gamme.BackColor = System.Drawing.Color.White;
            this.txtART_Gamme.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Gamme.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Gamme.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_Gamme.Location = new System.Drawing.Point(594, 300);
            this.txtART_Gamme.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Gamme.MaxLength = 32767;
            this.txtART_Gamme.Multiline = false;
            this.txtART_Gamme.Name = "txtART_Gamme";
            this.txtART_Gamme.ReadOnly = false;
            this.txtART_Gamme.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Gamme.Size = new System.Drawing.Size(187, 27);
            this.txtART_Gamme.TabIndex = 537;
            this.txtART_Gamme.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Gamme.UseSystemPasswordChar = false;
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(388, 301);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(56, 16);
            this.label16.TabIndex = 536;
            this.label16.Text = "Gamme";
            // 
            // txtART_Marque
            // 
            this.txtART_Marque.AccAcceptNumbersOnly = false;
            this.txtART_Marque.AccAllowComma = false;
            this.txtART_Marque.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Marque.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Marque.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Marque.AccHidenValue = "";
            this.txtART_Marque.AccNotAllowedChars = null;
            this.txtART_Marque.AccReadOnly = false;
            this.txtART_Marque.AccReadOnlyAllowDelete = false;
            this.txtART_Marque.AccRequired = false;
            this.txtART_Marque.BackColor = System.Drawing.Color.White;
            this.txtART_Marque.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Marque.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Marque.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_Marque.Location = new System.Drawing.Point(594, 270);
            this.txtART_Marque.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Marque.MaxLength = 32767;
            this.txtART_Marque.Multiline = false;
            this.txtART_Marque.Name = "txtART_Marque";
            this.txtART_Marque.ReadOnly = false;
            this.txtART_Marque.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Marque.Size = new System.Drawing.Size(187, 27);
            this.txtART_Marque.TabIndex = 535;
            this.txtART_Marque.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Marque.UseSystemPasswordChar = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(388, 271);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(54, 16);
            this.label17.TabIndex = 534;
            this.label17.Text = "Marque";
            // 
            // txtART_CorpEtat
            // 
            this.txtART_CorpEtat.AccAcceptNumbersOnly = false;
            this.txtART_CorpEtat.AccAllowComma = false;
            this.txtART_CorpEtat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_CorpEtat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_CorpEtat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_CorpEtat.AccHidenValue = "";
            this.txtART_CorpEtat.AccNotAllowedChars = null;
            this.txtART_CorpEtat.AccReadOnly = false;
            this.txtART_CorpEtat.AccReadOnlyAllowDelete = false;
            this.txtART_CorpEtat.AccRequired = false;
            this.txtART_CorpEtat.BackColor = System.Drawing.Color.White;
            this.txtART_CorpEtat.CustomBackColor = System.Drawing.Color.White;
            this.txtART_CorpEtat.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_CorpEtat.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_CorpEtat.Location = new System.Drawing.Point(594, 239);
            this.txtART_CorpEtat.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_CorpEtat.MaxLength = 32767;
            this.txtART_CorpEtat.Multiline = false;
            this.txtART_CorpEtat.Name = "txtART_CorpEtat";
            this.txtART_CorpEtat.ReadOnly = false;
            this.txtART_CorpEtat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_CorpEtat.Size = new System.Drawing.Size(187, 27);
            this.txtART_CorpEtat.TabIndex = 533;
            this.txtART_CorpEtat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_CorpEtat.UseSystemPasswordChar = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(388, 240);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(86, 16);
            this.label14.TabIndex = 532;
            this.label14.Text = "Corps état 20";
            // 
            // txtART_UnitsStock
            // 
            this.txtART_UnitsStock.AccAcceptNumbersOnly = false;
            this.txtART_UnitsStock.AccAllowComma = false;
            this.txtART_UnitsStock.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_UnitsStock.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_UnitsStock.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_UnitsStock.AccHidenValue = "";
            this.txtART_UnitsStock.AccNotAllowedChars = null;
            this.txtART_UnitsStock.AccReadOnly = false;
            this.txtART_UnitsStock.AccReadOnlyAllowDelete = false;
            this.txtART_UnitsStock.AccRequired = false;
            this.txtART_UnitsStock.BackColor = System.Drawing.Color.White;
            this.txtART_UnitsStock.CustomBackColor = System.Drawing.Color.White;
            this.txtART_UnitsStock.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_UnitsStock.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_UnitsStock.Location = new System.Drawing.Point(594, 208);
            this.txtART_UnitsStock.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_UnitsStock.MaxLength = 32767;
            this.txtART_UnitsStock.Multiline = false;
            this.txtART_UnitsStock.Name = "txtART_UnitsStock";
            this.txtART_UnitsStock.ReadOnly = false;
            this.txtART_UnitsStock.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_UnitsStock.Size = new System.Drawing.Size(187, 27);
            this.txtART_UnitsStock.TabIndex = 531;
            this.txtART_UnitsStock.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_UnitsStock.UseSystemPasswordChar = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(388, 209);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(93, 16);
            this.label15.TabIndex = 530;
            this.label15.Text = "Unité de stock";
            // 
            // txtART_Volume
            // 
            this.txtART_Volume.AccAcceptNumbersOnly = false;
            this.txtART_Volume.AccAllowComma = false;
            this.txtART_Volume.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Volume.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Volume.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Volume.AccHidenValue = "";
            this.txtART_Volume.AccNotAllowedChars = null;
            this.txtART_Volume.AccReadOnly = false;
            this.txtART_Volume.AccReadOnlyAllowDelete = false;
            this.txtART_Volume.AccRequired = false;
            this.txtART_Volume.BackColor = System.Drawing.Color.White;
            this.txtART_Volume.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Volume.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Volume.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_Volume.Location = new System.Drawing.Point(594, 177);
            this.txtART_Volume.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Volume.MaxLength = 32767;
            this.txtART_Volume.Multiline = false;
            this.txtART_Volume.Name = "txtART_Volume";
            this.txtART_Volume.ReadOnly = false;
            this.txtART_Volume.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Volume.Size = new System.Drawing.Size(187, 27);
            this.txtART_Volume.TabIndex = 529;
            this.txtART_Volume.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Volume.UseSystemPasswordChar = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(388, 178);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(54, 16);
            this.label12.TabIndex = 528;
            this.label12.Text = "Volume";
            // 
            // txtART_UniteDev
            // 
            this.txtART_UniteDev.AccAcceptNumbersOnly = false;
            this.txtART_UniteDev.AccAllowComma = false;
            this.txtART_UniteDev.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_UniteDev.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_UniteDev.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_UniteDev.AccHidenValue = "";
            this.txtART_UniteDev.AccNotAllowedChars = null;
            this.txtART_UniteDev.AccReadOnly = false;
            this.txtART_UniteDev.AccReadOnlyAllowDelete = false;
            this.txtART_UniteDev.AccRequired = false;
            this.txtART_UniteDev.BackColor = System.Drawing.Color.White;
            this.txtART_UniteDev.CustomBackColor = System.Drawing.Color.White;
            this.txtART_UniteDev.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_UniteDev.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_UniteDev.Location = new System.Drawing.Point(594, 146);
            this.txtART_UniteDev.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_UniteDev.MaxLength = 32767;
            this.txtART_UniteDev.Multiline = false;
            this.txtART_UniteDev.Name = "txtART_UniteDev";
            this.txtART_UniteDev.ReadOnly = false;
            this.txtART_UniteDev.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_UniteDev.Size = new System.Drawing.Size(187, 27);
            this.txtART_UniteDev.TabIndex = 527;
            this.txtART_UniteDev.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_UniteDev.UseSystemPasswordChar = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(388, 147);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(75, 16);
            this.label13.TabIndex = 526;
            this.label13.Text = "Unité devis";
            // 
            // txtART_hauteur
            // 
            this.txtART_hauteur.AccAcceptNumbersOnly = false;
            this.txtART_hauteur.AccAllowComma = false;
            this.txtART_hauteur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_hauteur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_hauteur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_hauteur.AccHidenValue = "";
            this.txtART_hauteur.AccNotAllowedChars = null;
            this.txtART_hauteur.AccReadOnly = false;
            this.txtART_hauteur.AccReadOnlyAllowDelete = false;
            this.txtART_hauteur.AccRequired = false;
            this.txtART_hauteur.BackColor = System.Drawing.Color.White;
            this.txtART_hauteur.CustomBackColor = System.Drawing.Color.White;
            this.txtART_hauteur.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_hauteur.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_hauteur.Location = new System.Drawing.Point(594, 115);
            this.txtART_hauteur.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_hauteur.MaxLength = 32767;
            this.txtART_hauteur.Multiline = false;
            this.txtART_hauteur.Name = "txtART_hauteur";
            this.txtART_hauteur.ReadOnly = false;
            this.txtART_hauteur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_hauteur.Size = new System.Drawing.Size(187, 27);
            this.txtART_hauteur.TabIndex = 525;
            this.txtART_hauteur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_hauteur.UseSystemPasswordChar = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(388, 116);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(55, 16);
            this.label10.TabIndex = 524;
            this.label10.Text = "Hauteur";
            // 
            // txtART_Surface
            // 
            this.txtART_Surface.AccAcceptNumbersOnly = false;
            this.txtART_Surface.AccAllowComma = false;
            this.txtART_Surface.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Surface.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Surface.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Surface.AccHidenValue = "";
            this.txtART_Surface.AccNotAllowedChars = null;
            this.txtART_Surface.AccReadOnly = false;
            this.txtART_Surface.AccReadOnlyAllowDelete = false;
            this.txtART_Surface.AccRequired = false;
            this.txtART_Surface.BackColor = System.Drawing.Color.White;
            this.txtART_Surface.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Surface.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Surface.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_Surface.Location = new System.Drawing.Point(594, 84);
            this.txtART_Surface.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Surface.MaxLength = 32767;
            this.txtART_Surface.Multiline = false;
            this.txtART_Surface.Name = "txtART_Surface";
            this.txtART_Surface.ReadOnly = false;
            this.txtART_Surface.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Surface.Size = new System.Drawing.Size(187, 27);
            this.txtART_Surface.TabIndex = 523;
            this.txtART_Surface.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Surface.UseSystemPasswordChar = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(388, 85);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(54, 16);
            this.label11.TabIndex = 522;
            this.label11.Text = "Surface";
            // 
            // txtART_Largeur
            // 
            this.txtART_Largeur.AccAcceptNumbersOnly = false;
            this.txtART_Largeur.AccAllowComma = false;
            this.txtART_Largeur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Largeur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Largeur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Largeur.AccHidenValue = "";
            this.txtART_Largeur.AccNotAllowedChars = null;
            this.txtART_Largeur.AccReadOnly = false;
            this.txtART_Largeur.AccReadOnlyAllowDelete = false;
            this.txtART_Largeur.AccRequired = false;
            this.txtART_Largeur.BackColor = System.Drawing.Color.White;
            this.txtART_Largeur.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Largeur.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Largeur.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_Largeur.Location = new System.Drawing.Point(594, 53);
            this.txtART_Largeur.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Largeur.MaxLength = 32767;
            this.txtART_Largeur.Multiline = false;
            this.txtART_Largeur.Name = "txtART_Largeur";
            this.txtART_Largeur.ReadOnly = false;
            this.txtART_Largeur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Largeur.Size = new System.Drawing.Size(187, 27);
            this.txtART_Largeur.TabIndex = 521;
            this.txtART_Largeur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Largeur.UseSystemPasswordChar = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(398, 54);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(54, 16);
            this.label9.TabIndex = 520;
            this.label9.Text = "Largeur";
            // 
            // txtART_Longueur
            // 
            this.txtART_Longueur.AccAcceptNumbersOnly = false;
            this.txtART_Longueur.AccAllowComma = false;
            this.txtART_Longueur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Longueur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Longueur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Longueur.AccHidenValue = "";
            this.txtART_Longueur.AccNotAllowedChars = null;
            this.txtART_Longueur.AccReadOnly = false;
            this.txtART_Longueur.AccReadOnlyAllowDelete = false;
            this.txtART_Longueur.AccRequired = false;
            this.txtART_Longueur.BackColor = System.Drawing.Color.White;
            this.txtART_Longueur.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Longueur.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Longueur.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_Longueur.Location = new System.Drawing.Point(594, 22);
            this.txtART_Longueur.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Longueur.MaxLength = 32767;
            this.txtART_Longueur.Multiline = false;
            this.txtART_Longueur.Name = "txtART_Longueur";
            this.txtART_Longueur.ReadOnly = false;
            this.txtART_Longueur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Longueur.Size = new System.Drawing.Size(187, 27);
            this.txtART_Longueur.TabIndex = 519;
            this.txtART_Longueur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Longueur.UseSystemPasswordChar = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(388, 23);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(64, 16);
            this.label8.TabIndex = 518;
            this.label8.Text = "Longueur";
            // 
            // txtART_poids
            // 
            this.txtART_poids.AccAcceptNumbersOnly = false;
            this.txtART_poids.AccAllowComma = false;
            this.txtART_poids.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_poids.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_poids.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_poids.AccHidenValue = "";
            this.txtART_poids.AccNotAllowedChars = null;
            this.txtART_poids.AccReadOnly = false;
            this.txtART_poids.AccReadOnlyAllowDelete = false;
            this.txtART_poids.AccRequired = false;
            this.txtART_poids.BackColor = System.Drawing.Color.White;
            this.txtART_poids.CustomBackColor = System.Drawing.Color.White;
            this.txtART_poids.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_poids.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_poids.Location = new System.Drawing.Point(168, 339);
            this.txtART_poids.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_poids.MaxLength = 32767;
            this.txtART_poids.Multiline = false;
            this.txtART_poids.Name = "txtART_poids";
            this.txtART_poids.ReadOnly = false;
            this.txtART_poids.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_poids.Size = new System.Drawing.Size(187, 27);
            this.txtART_poids.TabIndex = 517;
            this.txtART_poids.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_poids.UseSystemPasswordChar = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 339);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(43, 16);
            this.label7.TabIndex = 516;
            this.label7.Text = "Poids";
            // 
            // txtART_UnitPoidsTheo
            // 
            this.txtART_UnitPoidsTheo.AccAcceptNumbersOnly = false;
            this.txtART_UnitPoidsTheo.AccAllowComma = false;
            this.txtART_UnitPoidsTheo.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_UnitPoidsTheo.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_UnitPoidsTheo.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_UnitPoidsTheo.AccHidenValue = "";
            this.txtART_UnitPoidsTheo.AccNotAllowedChars = null;
            this.txtART_UnitPoidsTheo.AccReadOnly = false;
            this.txtART_UnitPoidsTheo.AccReadOnlyAllowDelete = false;
            this.txtART_UnitPoidsTheo.AccRequired = false;
            this.txtART_UnitPoidsTheo.BackColor = System.Drawing.Color.White;
            this.txtART_UnitPoidsTheo.CustomBackColor = System.Drawing.Color.White;
            this.txtART_UnitPoidsTheo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_UnitPoidsTheo.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_UnitPoidsTheo.Location = new System.Drawing.Point(168, 308);
            this.txtART_UnitPoidsTheo.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_UnitPoidsTheo.MaxLength = 32767;
            this.txtART_UnitPoidsTheo.Multiline = false;
            this.txtART_UnitPoidsTheo.Name = "txtART_UnitPoidsTheo";
            this.txtART_UnitPoidsTheo.ReadOnly = false;
            this.txtART_UnitPoidsTheo.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_UnitPoidsTheo.Size = new System.Drawing.Size(187, 27);
            this.txtART_UnitPoidsTheo.TabIndex = 515;
            this.txtART_UnitPoidsTheo.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_UnitPoidsTheo.UseSystemPasswordChar = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 309);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(135, 16);
            this.label6.TabIndex = 514;
            this.label6.Text = "Unité poids théorique";
            // 
            // txtART_PoidsTheo
            // 
            this.txtART_PoidsTheo.AccAcceptNumbersOnly = false;
            this.txtART_PoidsTheo.AccAllowComma = false;
            this.txtART_PoidsTheo.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_PoidsTheo.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_PoidsTheo.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_PoidsTheo.AccHidenValue = "";
            this.txtART_PoidsTheo.AccNotAllowedChars = null;
            this.txtART_PoidsTheo.AccReadOnly = false;
            this.txtART_PoidsTheo.AccReadOnlyAllowDelete = false;
            this.txtART_PoidsTheo.AccRequired = false;
            this.txtART_PoidsTheo.BackColor = System.Drawing.Color.White;
            this.txtART_PoidsTheo.CustomBackColor = System.Drawing.Color.White;
            this.txtART_PoidsTheo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_PoidsTheo.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_PoidsTheo.Location = new System.Drawing.Point(168, 277);
            this.txtART_PoidsTheo.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_PoidsTheo.MaxLength = 32767;
            this.txtART_PoidsTheo.Multiline = false;
            this.txtART_PoidsTheo.Name = "txtART_PoidsTheo";
            this.txtART_PoidsTheo.ReadOnly = false;
            this.txtART_PoidsTheo.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_PoidsTheo.Size = new System.Drawing.Size(187, 27);
            this.txtART_PoidsTheo.TabIndex = 513;
            this.txtART_PoidsTheo.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_PoidsTheo.UseSystemPasswordChar = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 278);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(102, 16);
            this.label5.TabIndex = 512;
            this.label5.Text = "Poids théorique";
            // 
            // txtART_Synonyme
            // 
            this.txtART_Synonyme.AccAcceptNumbersOnly = false;
            this.txtART_Synonyme.AccAllowComma = false;
            this.txtART_Synonyme.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Synonyme.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Synonyme.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Synonyme.AccHidenValue = "";
            this.txtART_Synonyme.AccNotAllowedChars = null;
            this.txtART_Synonyme.AccReadOnly = false;
            this.txtART_Synonyme.AccReadOnlyAllowDelete = false;
            this.txtART_Synonyme.AccRequired = false;
            this.txtART_Synonyme.BackColor = System.Drawing.Color.White;
            this.txtART_Synonyme.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Synonyme.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Synonyme.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_Synonyme.Location = new System.Drawing.Point(168, 246);
            this.txtART_Synonyme.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Synonyme.MaxLength = 32767;
            this.txtART_Synonyme.Multiline = false;
            this.txtART_Synonyme.Name = "txtART_Synonyme";
            this.txtART_Synonyme.ReadOnly = false;
            this.txtART_Synonyme.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Synonyme.Size = new System.Drawing.Size(187, 27);
            this.txtART_Synonyme.TabIndex = 511;
            this.txtART_Synonyme.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Synonyme.UseSystemPasswordChar = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 247);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(72, 16);
            this.label4.TabIndex = 510;
            this.label4.Text = "Synonyme";
            // 
            // txtART_LibelleTechnique
            // 
            this.txtART_LibelleTechnique.AccAcceptNumbersOnly = false;
            this.txtART_LibelleTechnique.AccAllowComma = false;
            this.txtART_LibelleTechnique.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_LibelleTechnique.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_LibelleTechnique.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_LibelleTechnique.AccHidenValue = "";
            this.txtART_LibelleTechnique.AccNotAllowedChars = null;
            this.txtART_LibelleTechnique.AccReadOnly = false;
            this.txtART_LibelleTechnique.AccReadOnlyAllowDelete = false;
            this.txtART_LibelleTechnique.AccRequired = false;
            this.txtART_LibelleTechnique.BackColor = System.Drawing.Color.White;
            this.txtART_LibelleTechnique.CustomBackColor = System.Drawing.Color.White;
            this.txtART_LibelleTechnique.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_LibelleTechnique.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_LibelleTechnique.Location = new System.Drawing.Point(171, 165);
            this.txtART_LibelleTechnique.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_LibelleTechnique.MaxLength = 32767;
            this.txtART_LibelleTechnique.Multiline = true;
            this.txtART_LibelleTechnique.Name = "txtART_LibelleTechnique";
            this.txtART_LibelleTechnique.ReadOnly = false;
            this.txtART_LibelleTechnique.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_LibelleTechnique.Size = new System.Drawing.Size(187, 77);
            this.txtART_LibelleTechnique.TabIndex = 509;
            this.txtART_LibelleTechnique.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_LibelleTechnique.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(6, 165);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(79, 16);
            this.label3.TabIndex = 508;
            this.label3.Text = "Libellé tech.";
            // 
            // txtART_Libelle
            // 
            this.txtART_Libelle.AccAcceptNumbersOnly = false;
            this.txtART_Libelle.AccAllowComma = false;
            this.txtART_Libelle.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Libelle.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Libelle.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Libelle.AccHidenValue = "";
            this.txtART_Libelle.AccNotAllowedChars = null;
            this.txtART_Libelle.AccReadOnly = false;
            this.txtART_Libelle.AccReadOnlyAllowDelete = false;
            this.txtART_Libelle.AccRequired = false;
            this.txtART_Libelle.BackColor = System.Drawing.Color.White;
            this.txtART_Libelle.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Libelle.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Libelle.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_Libelle.Location = new System.Drawing.Point(171, 84);
            this.txtART_Libelle.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Libelle.MaxLength = 32767;
            this.txtART_Libelle.Multiline = true;
            this.txtART_Libelle.Name = "txtART_Libelle";
            this.txtART_Libelle.ReadOnly = false;
            this.txtART_Libelle.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Libelle.Size = new System.Drawing.Size(187, 77);
            this.txtART_Libelle.TabIndex = 507;
            this.txtART_Libelle.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Libelle.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(6, 85);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(48, 16);
            this.label2.TabIndex = 506;
            this.label2.Text = "Libellé";
            // 
            // txtART_Famille
            // 
            this.txtART_Famille.AccAcceptNumbersOnly = false;
            this.txtART_Famille.AccAllowComma = false;
            this.txtART_Famille.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Famille.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Famille.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Famille.AccHidenValue = "";
            this.txtART_Famille.AccNotAllowedChars = null;
            this.txtART_Famille.AccReadOnly = false;
            this.txtART_Famille.AccReadOnlyAllowDelete = false;
            this.txtART_Famille.AccRequired = false;
            this.txtART_Famille.BackColor = System.Drawing.Color.White;
            this.txtART_Famille.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Famille.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Famille.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_Famille.Location = new System.Drawing.Point(171, 53);
            this.txtART_Famille.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Famille.MaxLength = 32767;
            this.txtART_Famille.Multiline = false;
            this.txtART_Famille.Name = "txtART_Famille";
            this.txtART_Famille.ReadOnly = false;
            this.txtART_Famille.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Famille.Size = new System.Drawing.Size(187, 27);
            this.txtART_Famille.TabIndex = 505;
            this.txtART_Famille.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Famille.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(6, 54);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(52, 16);
            this.label1.TabIndex = 504;
            this.label1.Text = "Famille";
            // 
            // txtART_Chrono
            // 
            this.txtART_Chrono.AccAcceptNumbersOnly = false;
            this.txtART_Chrono.AccAllowComma = false;
            this.txtART_Chrono.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Chrono.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Chrono.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Chrono.AccHidenValue = "";
            this.txtART_Chrono.AccNotAllowedChars = null;
            this.txtART_Chrono.AccReadOnly = false;
            this.txtART_Chrono.AccReadOnlyAllowDelete = false;
            this.txtART_Chrono.AccRequired = false;
            this.txtART_Chrono.BackColor = System.Drawing.Color.White;
            this.txtART_Chrono.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Chrono.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Chrono.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtART_Chrono.Location = new System.Drawing.Point(171, 22);
            this.txtART_Chrono.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Chrono.MaxLength = 32767;
            this.txtART_Chrono.Multiline = false;
            this.txtART_Chrono.Name = "txtART_Chrono";
            this.txtART_Chrono.ReadOnly = false;
            this.txtART_Chrono.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Chrono.Size = new System.Drawing.Size(187, 27);
            this.txtART_Chrono.TabIndex = 503;
            this.txtART_Chrono.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Chrono.UseSystemPasswordChar = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(6, 23);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(49, 16);
            this.label33.TabIndex = 502;
            this.label33.Text = "chrono";
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.txtARTP_EcoTaxe);
            this.groupBox1.Controls.Add(this.label56);
            this.groupBox1.Controls.Add(this.txtARTP_MajorDevis);
            this.groupBox1.Controls.Add(this.label55);
            this.groupBox1.Controls.Add(this.txtARTP_RemiseIndic);
            this.groupBox1.Controls.Add(this.label48);
            this.groupBox1.Controls.Add(this.txtARTP_RemisePourCent);
            this.groupBox1.Controls.Add(this.label49);
            this.groupBox1.Controls.Add(this.txtARTP_UniteDevis);
            this.groupBox1.Controls.Add(this.label50);
            this.groupBox1.Controls.Add(this.txtARTP_PrixNetDevis);
            this.groupBox1.Controls.Add(this.label51);
            this.groupBox1.Controls.Add(this.txtARTP_LibelleCond);
            this.groupBox1.Controls.Add(this.label52);
            this.groupBox1.Controls.Add(this.txtARTP_PrixTarif);
            this.groupBox1.Controls.Add(this.label53);
            this.groupBox1.Controls.Add(this.txtARTP_StatutPrixNet);
            this.groupBox1.Controls.Add(this.label54);
            this.groupBox1.Controls.Add(this.txtARTP_PrixNetStock);
            this.groupBox1.Controls.Add(this.label38);
            this.groupBox1.Controls.Add(this.txtARTP_PrixNet);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.txtARTP_PrixPublic);
            this.groupBox1.Controls.Add(this.label40);
            this.groupBox1.Controls.Add(this.txtARTP_UniteFourn);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this.txtARTP_QteMinUC);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.txtARTP_PrixMulti);
            this.groupBox1.Controls.Add(this.label43);
            this.groupBox1.Controls.Add(this.txtARTP_UniteValeur);
            this.groupBox1.Controls.Add(this.label44);
            this.groupBox1.Controls.Add(this.txtARTP_PrixNetCond);
            this.groupBox1.Controls.Add(this.label45);
            this.groupBox1.Controls.Add(this.txtARTP_UnitCondFourn);
            this.groupBox1.Controls.Add(this.label46);
            this.groupBox1.Controls.Add(this.txtARTP_QteMinUCNF);
            this.groupBox1.Controls.Add(this.label47);
            this.groupBox1.Controls.Add(this.txtARTP_DateAplication);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.txtARTP_CoefVente);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.txtARTP_DelaiLivr);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.txtARTP_Statut);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.txtARTP_Devise);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.txtARTP_RefFourn);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.txtARTP_RefFab);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.txtARTP_Marque);
            this.groupBox1.Controls.Add(this.label35);
            this.groupBox1.Controls.Add(this.txtARTP_Nomfourn);
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this.txtARTP_Nofourn);
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox1.Location = new System.Drawing.Point(9, 406);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1198, 336);
            this.groupBox1.TabIndex = 412;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TARIF";
            // 
            // txtARTP_EcoTaxe
            // 
            this.txtARTP_EcoTaxe.AccAcceptNumbersOnly = false;
            this.txtARTP_EcoTaxe.AccAllowComma = false;
            this.txtARTP_EcoTaxe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_EcoTaxe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_EcoTaxe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_EcoTaxe.AccHidenValue = "";
            this.txtARTP_EcoTaxe.AccNotAllowedChars = null;
            this.txtARTP_EcoTaxe.AccReadOnly = false;
            this.txtARTP_EcoTaxe.AccReadOnlyAllowDelete = false;
            this.txtARTP_EcoTaxe.AccRequired = false;
            this.txtARTP_EcoTaxe.BackColor = System.Drawing.Color.White;
            this.txtARTP_EcoTaxe.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_EcoTaxe.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_EcoTaxe.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_EcoTaxe.Location = new System.Drawing.Point(1002, 270);
            this.txtARTP_EcoTaxe.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_EcoTaxe.MaxLength = 32767;
            this.txtARTP_EcoTaxe.Multiline = false;
            this.txtARTP_EcoTaxe.Name = "txtARTP_EcoTaxe";
            this.txtARTP_EcoTaxe.ReadOnly = false;
            this.txtARTP_EcoTaxe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_EcoTaxe.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_EcoTaxe.TabIndex = 595;
            this.txtARTP_EcoTaxe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_EcoTaxe.UseSystemPasswordChar = false;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(798, 271);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(57, 16);
            this.label56.TabIndex = 594;
            this.label56.Text = "Ecotaxe";
            // 
            // txtARTP_MajorDevis
            // 
            this.txtARTP_MajorDevis.AccAcceptNumbersOnly = false;
            this.txtARTP_MajorDevis.AccAllowComma = false;
            this.txtARTP_MajorDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_MajorDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_MajorDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_MajorDevis.AccHidenValue = "";
            this.txtARTP_MajorDevis.AccNotAllowedChars = null;
            this.txtARTP_MajorDevis.AccReadOnly = false;
            this.txtARTP_MajorDevis.AccReadOnlyAllowDelete = false;
            this.txtARTP_MajorDevis.AccRequired = false;
            this.txtARTP_MajorDevis.BackColor = System.Drawing.Color.White;
            this.txtARTP_MajorDevis.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_MajorDevis.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_MajorDevis.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_MajorDevis.Location = new System.Drawing.Point(1002, 239);
            this.txtARTP_MajorDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_MajorDevis.MaxLength = 32767;
            this.txtARTP_MajorDevis.Multiline = false;
            this.txtARTP_MajorDevis.Name = "txtARTP_MajorDevis";
            this.txtARTP_MajorDevis.ReadOnly = false;
            this.txtARTP_MajorDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_MajorDevis.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_MajorDevis.TabIndex = 593;
            this.txtARTP_MajorDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_MajorDevis.UseSystemPasswordChar = false;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.Location = new System.Drawing.Point(798, 240);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(122, 16);
            this.label55.TabIndex = 592;
            this.label55.Text = "% Majoration devis";
            // 
            // txtARTP_RemiseIndic
            // 
            this.txtARTP_RemiseIndic.AccAcceptNumbersOnly = false;
            this.txtARTP_RemiseIndic.AccAllowComma = false;
            this.txtARTP_RemiseIndic.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_RemiseIndic.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_RemiseIndic.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_RemiseIndic.AccHidenValue = "";
            this.txtARTP_RemiseIndic.AccNotAllowedChars = null;
            this.txtARTP_RemiseIndic.AccReadOnly = false;
            this.txtARTP_RemiseIndic.AccReadOnlyAllowDelete = false;
            this.txtARTP_RemiseIndic.AccRequired = false;
            this.txtARTP_RemiseIndic.BackColor = System.Drawing.Color.White;
            this.txtARTP_RemiseIndic.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_RemiseIndic.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_RemiseIndic.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_RemiseIndic.Location = new System.Drawing.Point(1002, 208);
            this.txtARTP_RemiseIndic.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_RemiseIndic.MaxLength = 32767;
            this.txtARTP_RemiseIndic.Multiline = false;
            this.txtARTP_RemiseIndic.Name = "txtARTP_RemiseIndic";
            this.txtARTP_RemiseIndic.ReadOnly = false;
            this.txtARTP_RemiseIndic.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_RemiseIndic.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_RemiseIndic.TabIndex = 591;
            this.txtARTP_RemiseIndic.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_RemiseIndic.UseSystemPasswordChar = false;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(798, 209);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(115, 16);
            this.label48.TabIndex = 590;
            this.label48.Text = "Remise indicative";
            // 
            // txtARTP_RemisePourCent
            // 
            this.txtARTP_RemisePourCent.AccAcceptNumbersOnly = false;
            this.txtARTP_RemisePourCent.AccAllowComma = false;
            this.txtARTP_RemisePourCent.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_RemisePourCent.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_RemisePourCent.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_RemisePourCent.AccHidenValue = "";
            this.txtARTP_RemisePourCent.AccNotAllowedChars = null;
            this.txtARTP_RemisePourCent.AccReadOnly = false;
            this.txtARTP_RemisePourCent.AccReadOnlyAllowDelete = false;
            this.txtARTP_RemisePourCent.AccRequired = false;
            this.txtARTP_RemisePourCent.BackColor = System.Drawing.Color.White;
            this.txtARTP_RemisePourCent.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_RemisePourCent.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_RemisePourCent.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_RemisePourCent.Location = new System.Drawing.Point(1002, 177);
            this.txtARTP_RemisePourCent.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_RemisePourCent.MaxLength = 32767;
            this.txtARTP_RemisePourCent.Multiline = false;
            this.txtARTP_RemisePourCent.Name = "txtARTP_RemisePourCent";
            this.txtARTP_RemisePourCent.ReadOnly = false;
            this.txtARTP_RemisePourCent.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_RemisePourCent.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_RemisePourCent.TabIndex = 589;
            this.txtARTP_RemisePourCent.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_RemisePourCent.UseSystemPasswordChar = false;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(798, 178);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(64, 16);
            this.label49.TabIndex = 588;
            this.label49.Text = "% remise";
            // 
            // txtARTP_UniteDevis
            // 
            this.txtARTP_UniteDevis.AccAcceptNumbersOnly = false;
            this.txtARTP_UniteDevis.AccAllowComma = false;
            this.txtARTP_UniteDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_UniteDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_UniteDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_UniteDevis.AccHidenValue = "";
            this.txtARTP_UniteDevis.AccNotAllowedChars = null;
            this.txtARTP_UniteDevis.AccReadOnly = false;
            this.txtARTP_UniteDevis.AccReadOnlyAllowDelete = false;
            this.txtARTP_UniteDevis.AccRequired = false;
            this.txtARTP_UniteDevis.BackColor = System.Drawing.Color.White;
            this.txtARTP_UniteDevis.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_UniteDevis.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_UniteDevis.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_UniteDevis.Location = new System.Drawing.Point(1002, 115);
            this.txtARTP_UniteDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_UniteDevis.MaxLength = 32767;
            this.txtARTP_UniteDevis.Multiline = false;
            this.txtARTP_UniteDevis.Name = "txtARTP_UniteDevis";
            this.txtARTP_UniteDevis.ReadOnly = false;
            this.txtARTP_UniteDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_UniteDevis.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_UniteDevis.TabIndex = 587;
            this.txtARTP_UniteDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_UniteDevis.UseSystemPasswordChar = false;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(798, 148);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(137, 16);
            this.label50.TabIndex = 586;
            this.label50.Text = "Prix net devis (UD)";
            // 
            // txtARTP_PrixNetDevis
            // 
            this.txtARTP_PrixNetDevis.AccAcceptNumbersOnly = false;
            this.txtARTP_PrixNetDevis.AccAllowComma = false;
            this.txtARTP_PrixNetDevis.AccBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtARTP_PrixNetDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_PrixNetDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_PrixNetDevis.AccHidenValue = "";
            this.txtARTP_PrixNetDevis.AccNotAllowedChars = null;
            this.txtARTP_PrixNetDevis.AccReadOnly = false;
            this.txtARTP_PrixNetDevis.AccReadOnlyAllowDelete = false;
            this.txtARTP_PrixNetDevis.AccRequired = false;
            this.txtARTP_PrixNetDevis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtARTP_PrixNetDevis.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtARTP_PrixNetDevis.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_PrixNetDevis.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_PrixNetDevis.Location = new System.Drawing.Point(1002, 146);
            this.txtARTP_PrixNetDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_PrixNetDevis.MaxLength = 32767;
            this.txtARTP_PrixNetDevis.Multiline = false;
            this.txtARTP_PrixNetDevis.Name = "txtARTP_PrixNetDevis";
            this.txtARTP_PrixNetDevis.ReadOnly = false;
            this.txtARTP_PrixNetDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_PrixNetDevis.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_PrixNetDevis.TabIndex = 585;
            this.txtARTP_PrixNetDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_PrixNetDevis.UseSystemPasswordChar = false;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(798, 117);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(106, 16);
            this.label51.TabIndex = 584;
            this.label51.Text = "Unité devis (UD)";
            // 
            // txtARTP_LibelleCond
            // 
            this.txtARTP_LibelleCond.AccAcceptNumbersOnly = false;
            this.txtARTP_LibelleCond.AccAllowComma = false;
            this.txtARTP_LibelleCond.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_LibelleCond.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_LibelleCond.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_LibelleCond.AccHidenValue = "";
            this.txtARTP_LibelleCond.AccNotAllowedChars = null;
            this.txtARTP_LibelleCond.AccReadOnly = false;
            this.txtARTP_LibelleCond.AccReadOnlyAllowDelete = false;
            this.txtARTP_LibelleCond.AccRequired = false;
            this.txtARTP_LibelleCond.BackColor = System.Drawing.Color.White;
            this.txtARTP_LibelleCond.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_LibelleCond.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_LibelleCond.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_LibelleCond.Location = new System.Drawing.Point(1002, 84);
            this.txtARTP_LibelleCond.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_LibelleCond.MaxLength = 32767;
            this.txtARTP_LibelleCond.Multiline = false;
            this.txtARTP_LibelleCond.Name = "txtARTP_LibelleCond";
            this.txtARTP_LibelleCond.ReadOnly = false;
            this.txtARTP_LibelleCond.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_LibelleCond.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_LibelleCond.TabIndex = 583;
            this.txtARTP_LibelleCond.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_LibelleCond.UseSystemPasswordChar = false;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(798, 85);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(151, 16);
            this.label52.TabIndex = 582;
            this.label52.Text = "Libellé Conditionnement";
            // 
            // txtARTP_PrixTarif
            // 
            this.txtARTP_PrixTarif.AccAcceptNumbersOnly = false;
            this.txtARTP_PrixTarif.AccAllowComma = false;
            this.txtARTP_PrixTarif.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_PrixTarif.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_PrixTarif.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_PrixTarif.AccHidenValue = "";
            this.txtARTP_PrixTarif.AccNotAllowedChars = null;
            this.txtARTP_PrixTarif.AccReadOnly = false;
            this.txtARTP_PrixTarif.AccReadOnlyAllowDelete = false;
            this.txtARTP_PrixTarif.AccRequired = false;
            this.txtARTP_PrixTarif.BackColor = System.Drawing.Color.White;
            this.txtARTP_PrixTarif.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_PrixTarif.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_PrixTarif.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_PrixTarif.Location = new System.Drawing.Point(1002, 53);
            this.txtARTP_PrixTarif.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_PrixTarif.MaxLength = 32767;
            this.txtARTP_PrixTarif.Multiline = false;
            this.txtARTP_PrixTarif.Name = "txtARTP_PrixTarif";
            this.txtARTP_PrixTarif.ReadOnly = false;
            this.txtARTP_PrixTarif.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_PrixTarif.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_PrixTarif.TabIndex = 581;
            this.txtARTP_PrixTarif.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_PrixTarif.UseSystemPasswordChar = false;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(798, 54);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(33, 16);
            this.label53.TabIndex = 580;
            this.label53.Text = "Prix ";
            // 
            // txtARTP_StatutPrixNet
            // 
            this.txtARTP_StatutPrixNet.AccAcceptNumbersOnly = false;
            this.txtARTP_StatutPrixNet.AccAllowComma = false;
            this.txtARTP_StatutPrixNet.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_StatutPrixNet.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_StatutPrixNet.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_StatutPrixNet.AccHidenValue = "";
            this.txtARTP_StatutPrixNet.AccNotAllowedChars = null;
            this.txtARTP_StatutPrixNet.AccReadOnly = false;
            this.txtARTP_StatutPrixNet.AccReadOnlyAllowDelete = false;
            this.txtARTP_StatutPrixNet.AccRequired = false;
            this.txtARTP_StatutPrixNet.BackColor = System.Drawing.Color.White;
            this.txtARTP_StatutPrixNet.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_StatutPrixNet.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_StatutPrixNet.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_StatutPrixNet.Location = new System.Drawing.Point(1002, 21);
            this.txtARTP_StatutPrixNet.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_StatutPrixNet.MaxLength = 32767;
            this.txtARTP_StatutPrixNet.Multiline = false;
            this.txtARTP_StatutPrixNet.Name = "txtARTP_StatutPrixNet";
            this.txtARTP_StatutPrixNet.ReadOnly = false;
            this.txtARTP_StatutPrixNet.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_StatutPrixNet.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_StatutPrixNet.TabIndex = 579;
            this.txtARTP_StatutPrixNet.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_StatutPrixNet.UseSystemPasswordChar = false;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(798, 22);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(80, 16);
            this.label54.TabIndex = 578;
            this.label54.Text = "Flag prix net";
            // 
            // txtARTP_PrixNetStock
            // 
            this.txtARTP_PrixNetStock.AccAcceptNumbersOnly = false;
            this.txtARTP_PrixNetStock.AccAllowComma = false;
            this.txtARTP_PrixNetStock.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_PrixNetStock.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_PrixNetStock.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_PrixNetStock.AccHidenValue = "";
            this.txtARTP_PrixNetStock.AccNotAllowedChars = null;
            this.txtARTP_PrixNetStock.AccReadOnly = false;
            this.txtARTP_PrixNetStock.AccReadOnlyAllowDelete = false;
            this.txtARTP_PrixNetStock.AccRequired = false;
            this.txtARTP_PrixNetStock.BackColor = System.Drawing.Color.White;
            this.txtARTP_PrixNetStock.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_PrixNetStock.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_PrixNetStock.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_PrixNetStock.Location = new System.Drawing.Point(594, 303);
            this.txtARTP_PrixNetStock.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_PrixNetStock.MaxLength = 32767;
            this.txtARTP_PrixNetStock.Multiline = false;
            this.txtARTP_PrixNetStock.Name = "txtARTP_PrixNetStock";
            this.txtARTP_PrixNetStock.ReadOnly = false;
            this.txtARTP_PrixNetStock.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_PrixNetStock.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_PrixNetStock.TabIndex = 577;
            this.txtARTP_PrixNetStock.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_PrixNetStock.UseSystemPasswordChar = false;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(388, 303);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(119, 16);
            this.label38.TabIndex = 576;
            this.label38.Text = "Prix net stock (US) ";
            // 
            // txtARTP_PrixNet
            // 
            this.txtARTP_PrixNet.AccAcceptNumbersOnly = false;
            this.txtARTP_PrixNet.AccAllowComma = false;
            this.txtARTP_PrixNet.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_PrixNet.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_PrixNet.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_PrixNet.AccHidenValue = "";
            this.txtARTP_PrixNet.AccNotAllowedChars = null;
            this.txtARTP_PrixNet.AccReadOnly = false;
            this.txtARTP_PrixNet.AccReadOnlyAllowDelete = false;
            this.txtARTP_PrixNet.AccRequired = false;
            this.txtARTP_PrixNet.BackColor = System.Drawing.Color.White;
            this.txtARTP_PrixNet.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_PrixNet.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_PrixNet.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_PrixNet.Location = new System.Drawing.Point(594, 270);
            this.txtARTP_PrixNet.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_PrixNet.MaxLength = 32767;
            this.txtARTP_PrixNet.Multiline = false;
            this.txtARTP_PrixNet.Name = "txtARTP_PrixNet";
            this.txtARTP_PrixNet.ReadOnly = false;
            this.txtARTP_PrixNet.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_PrixNet.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_PrixNet.TabIndex = 575;
            this.txtARTP_PrixNet.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_PrixNet.UseSystemPasswordChar = false;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(388, 270);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(81, 16);
            this.label39.TabIndex = 574;
            this.label39.Text = "Prix net (UC)";
            // 
            // txtARTP_PrixPublic
            // 
            this.txtARTP_PrixPublic.AccAcceptNumbersOnly = false;
            this.txtARTP_PrixPublic.AccAllowComma = false;
            this.txtARTP_PrixPublic.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_PrixPublic.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_PrixPublic.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_PrixPublic.AccHidenValue = "";
            this.txtARTP_PrixPublic.AccNotAllowedChars = null;
            this.txtARTP_PrixPublic.AccReadOnly = false;
            this.txtARTP_PrixPublic.AccReadOnlyAllowDelete = false;
            this.txtARTP_PrixPublic.AccRequired = false;
            this.txtARTP_PrixPublic.BackColor = System.Drawing.Color.White;
            this.txtARTP_PrixPublic.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_PrixPublic.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_PrixPublic.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_PrixPublic.Location = new System.Drawing.Point(594, 239);
            this.txtARTP_PrixPublic.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_PrixPublic.MaxLength = 32767;
            this.txtARTP_PrixPublic.Multiline = false;
            this.txtARTP_PrixPublic.Name = "txtARTP_PrixPublic";
            this.txtARTP_PrixPublic.ReadOnly = false;
            this.txtARTP_PrixPublic.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_PrixPublic.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_PrixPublic.TabIndex = 573;
            this.txtARTP_PrixPublic.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_PrixPublic.UseSystemPasswordChar = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(388, 239);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(99, 16);
            this.label40.TabIndex = 572;
            this.label40.Text = "Prix public (UC)";
            // 
            // txtARTP_UniteFourn
            // 
            this.txtARTP_UniteFourn.AccAcceptNumbersOnly = false;
            this.txtARTP_UniteFourn.AccAllowComma = false;
            this.txtARTP_UniteFourn.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_UniteFourn.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_UniteFourn.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_UniteFourn.AccHidenValue = "";
            this.txtARTP_UniteFourn.AccNotAllowedChars = null;
            this.txtARTP_UniteFourn.AccReadOnly = false;
            this.txtARTP_UniteFourn.AccReadOnlyAllowDelete = false;
            this.txtARTP_UniteFourn.AccRequired = false;
            this.txtARTP_UniteFourn.BackColor = System.Drawing.Color.White;
            this.txtARTP_UniteFourn.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_UniteFourn.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_UniteFourn.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_UniteFourn.Location = new System.Drawing.Point(594, 208);
            this.txtARTP_UniteFourn.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_UniteFourn.MaxLength = 32767;
            this.txtARTP_UniteFourn.Multiline = false;
            this.txtARTP_UniteFourn.Name = "txtARTP_UniteFourn";
            this.txtARTP_UniteFourn.ReadOnly = false;
            this.txtARTP_UniteFourn.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_UniteFourn.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_UniteFourn.TabIndex = 571;
            this.txtARTP_UniteFourn.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_UniteFourn.UseSystemPasswordChar = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(388, 208);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(137, 16);
            this.label41.TabIndex = 570;
            this.label41.Text = "Unité fournisseur (UC)";
            // 
            // txtARTP_QteMinUC
            // 
            this.txtARTP_QteMinUC.AccAcceptNumbersOnly = false;
            this.txtARTP_QteMinUC.AccAllowComma = false;
            this.txtARTP_QteMinUC.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_QteMinUC.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_QteMinUC.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_QteMinUC.AccHidenValue = "";
            this.txtARTP_QteMinUC.AccNotAllowedChars = null;
            this.txtARTP_QteMinUC.AccReadOnly = false;
            this.txtARTP_QteMinUC.AccReadOnlyAllowDelete = false;
            this.txtARTP_QteMinUC.AccRequired = false;
            this.txtARTP_QteMinUC.BackColor = System.Drawing.Color.White;
            this.txtARTP_QteMinUC.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_QteMinUC.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_QteMinUC.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_QteMinUC.Location = new System.Drawing.Point(594, 177);
            this.txtARTP_QteMinUC.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_QteMinUC.MaxLength = 32767;
            this.txtARTP_QteMinUC.Multiline = false;
            this.txtARTP_QteMinUC.Name = "txtARTP_QteMinUC";
            this.txtARTP_QteMinUC.ReadOnly = false;
            this.txtARTP_QteMinUC.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_QteMinUC.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_QteMinUC.TabIndex = 569;
            this.txtARTP_QteMinUC.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_QteMinUC.UseSystemPasswordChar = false;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(388, 177);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(157, 16);
            this.label42.TabIndex = 568;
            this.label42.Text = "Qté mini commande (UC)";
            // 
            // txtARTP_PrixMulti
            // 
            this.txtARTP_PrixMulti.AccAcceptNumbersOnly = false;
            this.txtARTP_PrixMulti.AccAllowComma = false;
            this.txtARTP_PrixMulti.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_PrixMulti.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_PrixMulti.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_PrixMulti.AccHidenValue = "";
            this.txtARTP_PrixMulti.AccNotAllowedChars = null;
            this.txtARTP_PrixMulti.AccReadOnly = false;
            this.txtARTP_PrixMulti.AccReadOnlyAllowDelete = false;
            this.txtARTP_PrixMulti.AccRequired = false;
            this.txtARTP_PrixMulti.BackColor = System.Drawing.Color.White;
            this.txtARTP_PrixMulti.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_PrixMulti.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_PrixMulti.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_PrixMulti.Location = new System.Drawing.Point(594, 146);
            this.txtARTP_PrixMulti.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_PrixMulti.MaxLength = 32767;
            this.txtARTP_PrixMulti.Multiline = false;
            this.txtARTP_PrixMulti.Name = "txtARTP_PrixMulti";
            this.txtARTP_PrixMulti.ReadOnly = false;
            this.txtARTP_PrixMulti.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_PrixMulti.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_PrixMulti.TabIndex = 567;
            this.txtARTP_PrixMulti.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_PrixMulti.UseSystemPasswordChar = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(388, 146);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(90, 16);
            this.label43.TabIndex = 566;
            this.label43.Text = "Prix multi (UV)";
            // 
            // txtARTP_UniteValeur
            // 
            this.txtARTP_UniteValeur.AccAcceptNumbersOnly = false;
            this.txtARTP_UniteValeur.AccAllowComma = false;
            this.txtARTP_UniteValeur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_UniteValeur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_UniteValeur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_UniteValeur.AccHidenValue = "";
            this.txtARTP_UniteValeur.AccNotAllowedChars = null;
            this.txtARTP_UniteValeur.AccReadOnly = false;
            this.txtARTP_UniteValeur.AccReadOnlyAllowDelete = false;
            this.txtARTP_UniteValeur.AccRequired = false;
            this.txtARTP_UniteValeur.BackColor = System.Drawing.Color.White;
            this.txtARTP_UniteValeur.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_UniteValeur.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_UniteValeur.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_UniteValeur.Location = new System.Drawing.Point(594, 115);
            this.txtARTP_UniteValeur.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_UniteValeur.MaxLength = 32767;
            this.txtARTP_UniteValeur.Multiline = false;
            this.txtARTP_UniteValeur.Name = "txtARTP_UniteValeur";
            this.txtARTP_UniteValeur.ReadOnly = false;
            this.txtARTP_UniteValeur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_UniteValeur.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_UniteValeur.TabIndex = 565;
            this.txtARTP_UniteValeur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_UniteValeur.UseSystemPasswordChar = false;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(388, 115);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(109, 16);
            this.label44.TabIndex = 564;
            this.label44.Text = "Unité valeur (UV)";
            // 
            // txtARTP_PrixNetCond
            // 
            this.txtARTP_PrixNetCond.AccAcceptNumbersOnly = false;
            this.txtARTP_PrixNetCond.AccAllowComma = false;
            this.txtARTP_PrixNetCond.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_PrixNetCond.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_PrixNetCond.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_PrixNetCond.AccHidenValue = "";
            this.txtARTP_PrixNetCond.AccNotAllowedChars = null;
            this.txtARTP_PrixNetCond.AccReadOnly = false;
            this.txtARTP_PrixNetCond.AccReadOnlyAllowDelete = false;
            this.txtARTP_PrixNetCond.AccRequired = false;
            this.txtARTP_PrixNetCond.BackColor = System.Drawing.Color.White;
            this.txtARTP_PrixNetCond.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_PrixNetCond.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_PrixNetCond.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_PrixNetCond.Location = new System.Drawing.Point(594, 84);
            this.txtARTP_PrixNetCond.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_PrixNetCond.MaxLength = 32767;
            this.txtARTP_PrixNetCond.Multiline = false;
            this.txtARTP_PrixNetCond.Name = "txtARTP_PrixNetCond";
            this.txtARTP_PrixNetCond.ReadOnly = false;
            this.txtARTP_PrixNetCond.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_PrixNetCond.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_PrixNetCond.TabIndex = 563;
            this.txtARTP_PrixNetCond.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_PrixNetCond.UseSystemPasswordChar = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(388, 84);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(99, 16);
            this.label45.TabIndex = 562;
            this.label45.Text = "Prix net (UCNF)";
            // 
            // txtARTP_UnitCondFourn
            // 
            this.txtARTP_UnitCondFourn.AccAcceptNumbersOnly = false;
            this.txtARTP_UnitCondFourn.AccAllowComma = false;
            this.txtARTP_UnitCondFourn.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_UnitCondFourn.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_UnitCondFourn.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_UnitCondFourn.AccHidenValue = "";
            this.txtARTP_UnitCondFourn.AccNotAllowedChars = null;
            this.txtARTP_UnitCondFourn.AccReadOnly = false;
            this.txtARTP_UnitCondFourn.AccReadOnlyAllowDelete = false;
            this.txtARTP_UnitCondFourn.AccRequired = false;
            this.txtARTP_UnitCondFourn.BackColor = System.Drawing.Color.White;
            this.txtARTP_UnitCondFourn.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_UnitCondFourn.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_UnitCondFourn.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_UnitCondFourn.Location = new System.Drawing.Point(594, 53);
            this.txtARTP_UnitCondFourn.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_UnitCondFourn.MaxLength = 32767;
            this.txtARTP_UnitCondFourn.Multiline = false;
            this.txtARTP_UnitCondFourn.Name = "txtARTP_UnitCondFourn";
            this.txtARTP_UnitCondFourn.ReadOnly = false;
            this.txtARTP_UnitCondFourn.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_UnitCondFourn.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_UnitCondFourn.TabIndex = 561;
            this.txtARTP_UnitCondFourn.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_UnitCondFourn.UseSystemPasswordChar = false;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(388, 53);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(156, 16);
            this.label46.TabIndex = 560;
            this.label46.Text = "Unité Condi. four. (UCNF)";
            // 
            // txtARTP_QteMinUCNF
            // 
            this.txtARTP_QteMinUCNF.AccAcceptNumbersOnly = false;
            this.txtARTP_QteMinUCNF.AccAllowComma = false;
            this.txtARTP_QteMinUCNF.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_QteMinUCNF.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_QteMinUCNF.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_QteMinUCNF.AccHidenValue = "";
            this.txtARTP_QteMinUCNF.AccNotAllowedChars = null;
            this.txtARTP_QteMinUCNF.AccReadOnly = false;
            this.txtARTP_QteMinUCNF.AccReadOnlyAllowDelete = false;
            this.txtARTP_QteMinUCNF.AccRequired = false;
            this.txtARTP_QteMinUCNF.BackColor = System.Drawing.Color.White;
            this.txtARTP_QteMinUCNF.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_QteMinUCNF.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_QteMinUCNF.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_QteMinUCNF.Location = new System.Drawing.Point(594, 21);
            this.txtARTP_QteMinUCNF.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_QteMinUCNF.MaxLength = 32767;
            this.txtARTP_QteMinUCNF.Multiline = false;
            this.txtARTP_QteMinUCNF.Name = "txtARTP_QteMinUCNF";
            this.txtARTP_QteMinUCNF.ReadOnly = false;
            this.txtARTP_QteMinUCNF.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_QteMinUCNF.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_QteMinUCNF.TabIndex = 559;
            this.txtARTP_QteMinUCNF.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_QteMinUCNF.UseSystemPasswordChar = false;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(388, 21);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(200, 16);
            this.label47.TabIndex = 558;
            this.label47.Text = "Quantité min commande (UCNF)";
            // 
            // txtARTP_DateAplication
            // 
            this.txtARTP_DateAplication.AccAcceptNumbersOnly = false;
            this.txtARTP_DateAplication.AccAllowComma = false;
            this.txtARTP_DateAplication.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_DateAplication.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_DateAplication.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_DateAplication.AccHidenValue = "";
            this.txtARTP_DateAplication.AccNotAllowedChars = null;
            this.txtARTP_DateAplication.AccReadOnly = false;
            this.txtARTP_DateAplication.AccReadOnlyAllowDelete = false;
            this.txtARTP_DateAplication.AccRequired = false;
            this.txtARTP_DateAplication.BackColor = System.Drawing.Color.White;
            this.txtARTP_DateAplication.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_DateAplication.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_DateAplication.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_DateAplication.Location = new System.Drawing.Point(171, 303);
            this.txtARTP_DateAplication.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_DateAplication.MaxLength = 32767;
            this.txtARTP_DateAplication.Multiline = false;
            this.txtARTP_DateAplication.Name = "txtARTP_DateAplication";
            this.txtARTP_DateAplication.ReadOnly = false;
            this.txtARTP_DateAplication.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_DateAplication.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_DateAplication.TabIndex = 557;
            this.txtARTP_DateAplication.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_DateAplication.UseSystemPasswordChar = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(9, 303);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(106, 16);
            this.label27.TabIndex = 556;
            this.label27.Text = "Date application";
            // 
            // txtARTP_CoefVente
            // 
            this.txtARTP_CoefVente.AccAcceptNumbersOnly = false;
            this.txtARTP_CoefVente.AccAllowComma = false;
            this.txtARTP_CoefVente.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_CoefVente.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_CoefVente.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_CoefVente.AccHidenValue = "";
            this.txtARTP_CoefVente.AccNotAllowedChars = null;
            this.txtARTP_CoefVente.AccReadOnly = false;
            this.txtARTP_CoefVente.AccReadOnlyAllowDelete = false;
            this.txtARTP_CoefVente.AccRequired = false;
            this.txtARTP_CoefVente.BackColor = System.Drawing.Color.White;
            this.txtARTP_CoefVente.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_CoefVente.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_CoefVente.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_CoefVente.Location = new System.Drawing.Point(171, 270);
            this.txtARTP_CoefVente.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_CoefVente.MaxLength = 32767;
            this.txtARTP_CoefVente.Multiline = false;
            this.txtARTP_CoefVente.Name = "txtARTP_CoefVente";
            this.txtARTP_CoefVente.ReadOnly = false;
            this.txtARTP_CoefVente.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_CoefVente.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_CoefVente.TabIndex = 555;
            this.txtARTP_CoefVente.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_CoefVente.UseSystemPasswordChar = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(9, 270);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(150, 16);
            this.label28.TabIndex = 554;
            this.label28.Text = "Coef. Vente dépannage";
            // 
            // txtARTP_DelaiLivr
            // 
            this.txtARTP_DelaiLivr.AccAcceptNumbersOnly = false;
            this.txtARTP_DelaiLivr.AccAllowComma = false;
            this.txtARTP_DelaiLivr.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_DelaiLivr.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_DelaiLivr.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_DelaiLivr.AccHidenValue = "";
            this.txtARTP_DelaiLivr.AccNotAllowedChars = null;
            this.txtARTP_DelaiLivr.AccReadOnly = false;
            this.txtARTP_DelaiLivr.AccReadOnlyAllowDelete = false;
            this.txtARTP_DelaiLivr.AccRequired = false;
            this.txtARTP_DelaiLivr.BackColor = System.Drawing.Color.White;
            this.txtARTP_DelaiLivr.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_DelaiLivr.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_DelaiLivr.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_DelaiLivr.Location = new System.Drawing.Point(171, 239);
            this.txtARTP_DelaiLivr.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_DelaiLivr.MaxLength = 32767;
            this.txtARTP_DelaiLivr.Multiline = false;
            this.txtARTP_DelaiLivr.Name = "txtARTP_DelaiLivr";
            this.txtARTP_DelaiLivr.ReadOnly = false;
            this.txtARTP_DelaiLivr.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_DelaiLivr.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_DelaiLivr.TabIndex = 553;
            this.txtARTP_DelaiLivr.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_DelaiLivr.UseSystemPasswordChar = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(9, 239);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(112, 16);
            this.label29.TabIndex = 552;
            this.label29.Text = "Délai de livraison";
            // 
            // txtARTP_Statut
            // 
            this.txtARTP_Statut.AccAcceptNumbersOnly = false;
            this.txtARTP_Statut.AccAllowComma = false;
            this.txtARTP_Statut.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_Statut.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_Statut.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_Statut.AccHidenValue = "";
            this.txtARTP_Statut.AccNotAllowedChars = null;
            this.txtARTP_Statut.AccReadOnly = false;
            this.txtARTP_Statut.AccReadOnlyAllowDelete = false;
            this.txtARTP_Statut.AccRequired = false;
            this.txtARTP_Statut.BackColor = System.Drawing.Color.White;
            this.txtARTP_Statut.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_Statut.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_Statut.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_Statut.Location = new System.Drawing.Point(171, 208);
            this.txtARTP_Statut.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_Statut.MaxLength = 32767;
            this.txtARTP_Statut.Multiline = false;
            this.txtARTP_Statut.Name = "txtARTP_Statut";
            this.txtARTP_Statut.ReadOnly = false;
            this.txtARTP_Statut.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_Statut.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_Statut.TabIndex = 551;
            this.txtARTP_Statut.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_Statut.UseSystemPasswordChar = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(9, 208);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(142, 16);
            this.label30.TabIndex = 550;
            this.label30.Text = "Statut Controlé Indicatif";
            // 
            // txtARTP_Devise
            // 
            this.txtARTP_Devise.AccAcceptNumbersOnly = false;
            this.txtARTP_Devise.AccAllowComma = false;
            this.txtARTP_Devise.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_Devise.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_Devise.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_Devise.AccHidenValue = "";
            this.txtARTP_Devise.AccNotAllowedChars = null;
            this.txtARTP_Devise.AccReadOnly = false;
            this.txtARTP_Devise.AccReadOnlyAllowDelete = false;
            this.txtARTP_Devise.AccRequired = false;
            this.txtARTP_Devise.BackColor = System.Drawing.Color.White;
            this.txtARTP_Devise.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_Devise.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_Devise.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_Devise.Location = new System.Drawing.Point(171, 177);
            this.txtARTP_Devise.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_Devise.MaxLength = 32767;
            this.txtARTP_Devise.Multiline = false;
            this.txtARTP_Devise.Name = "txtARTP_Devise";
            this.txtARTP_Devise.ReadOnly = false;
            this.txtARTP_Devise.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_Devise.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_Devise.TabIndex = 549;
            this.txtARTP_Devise.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_Devise.UseSystemPasswordChar = false;
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(9, 177);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(51, 16);
            this.label31.TabIndex = 548;
            this.label31.Text = "Devise";
            // 
            // txtARTP_RefFourn
            // 
            this.txtARTP_RefFourn.AccAcceptNumbersOnly = false;
            this.txtARTP_RefFourn.AccAllowComma = false;
            this.txtARTP_RefFourn.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_RefFourn.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_RefFourn.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_RefFourn.AccHidenValue = "";
            this.txtARTP_RefFourn.AccNotAllowedChars = null;
            this.txtARTP_RefFourn.AccReadOnly = false;
            this.txtARTP_RefFourn.AccReadOnlyAllowDelete = false;
            this.txtARTP_RefFourn.AccRequired = false;
            this.txtARTP_RefFourn.BackColor = System.Drawing.Color.White;
            this.txtARTP_RefFourn.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_RefFourn.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_RefFourn.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_RefFourn.Location = new System.Drawing.Point(171, 146);
            this.txtARTP_RefFourn.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_RefFourn.MaxLength = 32767;
            this.txtARTP_RefFourn.Multiline = false;
            this.txtARTP_RefFourn.Name = "txtARTP_RefFourn";
            this.txtARTP_RefFourn.ReadOnly = false;
            this.txtARTP_RefFourn.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_RefFourn.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_RefFourn.TabIndex = 547;
            this.txtARTP_RefFourn.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_RefFourn.UseSystemPasswordChar = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(9, 146);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(97, 16);
            this.label32.TabIndex = 546;
            this.label32.Text = "Réf fournisseur";
            // 
            // txtARTP_RefFab
            // 
            this.txtARTP_RefFab.AccAcceptNumbersOnly = false;
            this.txtARTP_RefFab.AccAllowComma = false;
            this.txtARTP_RefFab.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_RefFab.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_RefFab.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_RefFab.AccHidenValue = "";
            this.txtARTP_RefFab.AccNotAllowedChars = null;
            this.txtARTP_RefFab.AccReadOnly = false;
            this.txtARTP_RefFab.AccReadOnlyAllowDelete = false;
            this.txtARTP_RefFab.AccRequired = false;
            this.txtARTP_RefFab.BackColor = System.Drawing.Color.White;
            this.txtARTP_RefFab.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_RefFab.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_RefFab.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_RefFab.Location = new System.Drawing.Point(171, 115);
            this.txtARTP_RefFab.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_RefFab.MaxLength = 32767;
            this.txtARTP_RefFab.Multiline = false;
            this.txtARTP_RefFab.Name = "txtARTP_RefFab";
            this.txtARTP_RefFab.ReadOnly = false;
            this.txtARTP_RefFab.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_RefFab.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_RefFab.TabIndex = 545;
            this.txtARTP_RefFab.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_RefFab.UseSystemPasswordChar = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(9, 115);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(125, 16);
            this.label34.TabIndex = 544;
            this.label34.Text = "Référence fabricant";
            // 
            // txtARTP_Marque
            // 
            this.txtARTP_Marque.AccAcceptNumbersOnly = false;
            this.txtARTP_Marque.AccAllowComma = false;
            this.txtARTP_Marque.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_Marque.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_Marque.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_Marque.AccHidenValue = "";
            this.txtARTP_Marque.AccNotAllowedChars = null;
            this.txtARTP_Marque.AccReadOnly = false;
            this.txtARTP_Marque.AccReadOnlyAllowDelete = false;
            this.txtARTP_Marque.AccRequired = false;
            this.txtARTP_Marque.BackColor = System.Drawing.Color.White;
            this.txtARTP_Marque.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_Marque.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_Marque.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_Marque.Location = new System.Drawing.Point(171, 84);
            this.txtARTP_Marque.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_Marque.MaxLength = 32767;
            this.txtARTP_Marque.Multiline = false;
            this.txtARTP_Marque.Name = "txtARTP_Marque";
            this.txtARTP_Marque.ReadOnly = false;
            this.txtARTP_Marque.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_Marque.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_Marque.TabIndex = 543;
            this.txtARTP_Marque.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_Marque.UseSystemPasswordChar = false;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(9, 84);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(54, 16);
            this.label35.TabIndex = 542;
            this.label35.Text = "Marque";
            // 
            // txtARTP_Nomfourn
            // 
            this.txtARTP_Nomfourn.AccAcceptNumbersOnly = false;
            this.txtARTP_Nomfourn.AccAllowComma = false;
            this.txtARTP_Nomfourn.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_Nomfourn.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_Nomfourn.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_Nomfourn.AccHidenValue = "";
            this.txtARTP_Nomfourn.AccNotAllowedChars = null;
            this.txtARTP_Nomfourn.AccReadOnly = false;
            this.txtARTP_Nomfourn.AccReadOnlyAllowDelete = false;
            this.txtARTP_Nomfourn.AccRequired = false;
            this.txtARTP_Nomfourn.BackColor = System.Drawing.Color.White;
            this.txtARTP_Nomfourn.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_Nomfourn.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_Nomfourn.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_Nomfourn.Location = new System.Drawing.Point(171, 53);
            this.txtARTP_Nomfourn.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_Nomfourn.MaxLength = 32767;
            this.txtARTP_Nomfourn.Multiline = false;
            this.txtARTP_Nomfourn.Name = "txtARTP_Nomfourn";
            this.txtARTP_Nomfourn.ReadOnly = false;
            this.txtARTP_Nomfourn.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_Nomfourn.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_Nomfourn.TabIndex = 541;
            this.txtARTP_Nomfourn.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_Nomfourn.UseSystemPasswordChar = false;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(9, 53);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(110, 16);
            this.label36.TabIndex = 540;
            this.label36.Text = "Nom Fournisseur";
            // 
            // txtARTP_Nofourn
            // 
            this.txtARTP_Nofourn.AccAcceptNumbersOnly = false;
            this.txtARTP_Nofourn.AccAllowComma = false;
            this.txtARTP_Nofourn.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_Nofourn.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_Nofourn.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_Nofourn.AccHidenValue = "";
            this.txtARTP_Nofourn.AccNotAllowedChars = null;
            this.txtARTP_Nofourn.AccReadOnly = false;
            this.txtARTP_Nofourn.AccReadOnlyAllowDelete = false;
            this.txtARTP_Nofourn.AccRequired = false;
            this.txtARTP_Nofourn.BackColor = System.Drawing.Color.White;
            this.txtARTP_Nofourn.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_Nofourn.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_Nofourn.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtARTP_Nofourn.Location = new System.Drawing.Point(171, 21);
            this.txtARTP_Nofourn.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_Nofourn.MaxLength = 32767;
            this.txtARTP_Nofourn.Multiline = false;
            this.txtARTP_Nofourn.Name = "txtARTP_Nofourn";
            this.txtARTP_Nofourn.ReadOnly = false;
            this.txtARTP_Nofourn.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_Nofourn.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_Nofourn.TabIndex = 539;
            this.txtARTP_Nofourn.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_Nofourn.UseSystemPasswordChar = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(9, 21);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(100, 16);
            this.label37.TabIndex = 538;
            this.label37.Text = "Réf. fournisseur";
            // 
            // ssGridURL
            // 
            this.ssGridURL.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.ssGridURL.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ssGridURL.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.ssGridURL.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridURL.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.ssGridURL.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridURL.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridURL.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ssGridURL.DisplayLayout.UseFixedHeaders = true;
            this.ssGridURL.Location = new System.Drawing.Point(12, 745);
            this.ssGridURL.Name = "ssGridURL";
            this.ssGridURL.Size = new System.Drawing.Size(1198, 104);
            this.ssGridURL.TabIndex = 573;
            this.ssGridURL.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridURL_InitializeLayout);
            this.ssGridURL.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.ssGridURL_BeforeRowsDeleted);
            this.ssGridURL.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.ssGridURL_Error);
            this.ssGridURL.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.ssGridURL_DoubleClickRow);
            // 
            // txtART_UcnsParUs
            // 
            this.txtART_UcnsParUs.AccAcceptNumbersOnly = false;
            this.txtART_UcnsParUs.AccAllowComma = false;
            this.txtART_UcnsParUs.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_UcnsParUs.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_UcnsParUs.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_UcnsParUs.AccHidenValue = "";
            this.txtART_UcnsParUs.AccNotAllowedChars = null;
            this.txtART_UcnsParUs.AccReadOnly = false;
            this.txtART_UcnsParUs.AccReadOnlyAllowDelete = false;
            this.txtART_UcnsParUs.AccRequired = false;
            this.txtART_UcnsParUs.BackColor = System.Drawing.Color.White;
            this.txtART_UcnsParUs.CustomBackColor = System.Drawing.Color.White;
            this.txtART_UcnsParUs.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_UcnsParUs.ForeColor = System.Drawing.Color.Black;
            this.txtART_UcnsParUs.Location = new System.Drawing.Point(1256, 374);
            this.txtART_UcnsParUs.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_UcnsParUs.MaxLength = 32767;
            this.txtART_UcnsParUs.Multiline = false;
            this.txtART_UcnsParUs.Name = "txtART_UcnsParUs";
            this.txtART_UcnsParUs.ReadOnly = false;
            this.txtART_UcnsParUs.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_UcnsParUs.Size = new System.Drawing.Size(187, 27);
            this.txtART_UcnsParUs.TabIndex = 585;
            this.txtART_UcnsParUs.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_UcnsParUs.UseSystemPasswordChar = false;
            // 
            // txtART_DebitGaz
            // 
            this.txtART_DebitGaz.AccAcceptNumbersOnly = false;
            this.txtART_DebitGaz.AccAllowComma = false;
            this.txtART_DebitGaz.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_DebitGaz.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_DebitGaz.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_DebitGaz.AccHidenValue = "";
            this.txtART_DebitGaz.AccNotAllowedChars = null;
            this.txtART_DebitGaz.AccReadOnly = false;
            this.txtART_DebitGaz.AccReadOnlyAllowDelete = false;
            this.txtART_DebitGaz.AccRequired = false;
            this.txtART_DebitGaz.BackColor = System.Drawing.Color.White;
            this.txtART_DebitGaz.CustomBackColor = System.Drawing.Color.White;
            this.txtART_DebitGaz.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_DebitGaz.ForeColor = System.Drawing.Color.Black;
            this.txtART_DebitGaz.Location = new System.Drawing.Point(1256, 347);
            this.txtART_DebitGaz.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_DebitGaz.MaxLength = 32767;
            this.txtART_DebitGaz.Multiline = false;
            this.txtART_DebitGaz.Name = "txtART_DebitGaz";
            this.txtART_DebitGaz.ReadOnly = false;
            this.txtART_DebitGaz.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_DebitGaz.Size = new System.Drawing.Size(187, 27);
            this.txtART_DebitGaz.TabIndex = 584;
            this.txtART_DebitGaz.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_DebitGaz.UseSystemPasswordChar = false;
            // 
            // txtART_DebitEau
            // 
            this.txtART_DebitEau.AccAcceptNumbersOnly = false;
            this.txtART_DebitEau.AccAllowComma = false;
            this.txtART_DebitEau.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_DebitEau.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_DebitEau.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_DebitEau.AccHidenValue = "";
            this.txtART_DebitEau.AccNotAllowedChars = null;
            this.txtART_DebitEau.AccReadOnly = false;
            this.txtART_DebitEau.AccReadOnlyAllowDelete = false;
            this.txtART_DebitEau.AccRequired = false;
            this.txtART_DebitEau.BackColor = System.Drawing.Color.White;
            this.txtART_DebitEau.CustomBackColor = System.Drawing.Color.White;
            this.txtART_DebitEau.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_DebitEau.ForeColor = System.Drawing.Color.Black;
            this.txtART_DebitEau.Location = new System.Drawing.Point(1256, 321);
            this.txtART_DebitEau.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_DebitEau.MaxLength = 32767;
            this.txtART_DebitEau.Multiline = false;
            this.txtART_DebitEau.Name = "txtART_DebitEau";
            this.txtART_DebitEau.ReadOnly = false;
            this.txtART_DebitEau.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_DebitEau.Size = new System.Drawing.Size(187, 27);
            this.txtART_DebitEau.TabIndex = 583;
            this.txtART_DebitEau.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_DebitEau.UseSystemPasswordChar = false;
            // 
            // txtART_RaccordGaz
            // 
            this.txtART_RaccordGaz.AccAcceptNumbersOnly = false;
            this.txtART_RaccordGaz.AccAllowComma = false;
            this.txtART_RaccordGaz.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_RaccordGaz.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_RaccordGaz.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_RaccordGaz.AccHidenValue = "";
            this.txtART_RaccordGaz.AccNotAllowedChars = null;
            this.txtART_RaccordGaz.AccReadOnly = false;
            this.txtART_RaccordGaz.AccReadOnlyAllowDelete = false;
            this.txtART_RaccordGaz.AccRequired = false;
            this.txtART_RaccordGaz.BackColor = System.Drawing.Color.White;
            this.txtART_RaccordGaz.CustomBackColor = System.Drawing.Color.White;
            this.txtART_RaccordGaz.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_RaccordGaz.ForeColor = System.Drawing.Color.Black;
            this.txtART_RaccordGaz.Location = new System.Drawing.Point(1256, 294);
            this.txtART_RaccordGaz.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_RaccordGaz.MaxLength = 32767;
            this.txtART_RaccordGaz.Multiline = false;
            this.txtART_RaccordGaz.Name = "txtART_RaccordGaz";
            this.txtART_RaccordGaz.ReadOnly = false;
            this.txtART_RaccordGaz.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_RaccordGaz.Size = new System.Drawing.Size(187, 27);
            this.txtART_RaccordGaz.TabIndex = 582;
            this.txtART_RaccordGaz.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_RaccordGaz.UseSystemPasswordChar = false;
            // 
            // txtART_Evacuation
            // 
            this.txtART_Evacuation.AccAcceptNumbersOnly = false;
            this.txtART_Evacuation.AccAllowComma = false;
            this.txtART_Evacuation.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Evacuation.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Evacuation.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Evacuation.AccHidenValue = "";
            this.txtART_Evacuation.AccNotAllowedChars = null;
            this.txtART_Evacuation.AccReadOnly = false;
            this.txtART_Evacuation.AccReadOnlyAllowDelete = false;
            this.txtART_Evacuation.AccRequired = false;
            this.txtART_Evacuation.BackColor = System.Drawing.Color.White;
            this.txtART_Evacuation.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Evacuation.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Evacuation.ForeColor = System.Drawing.Color.Black;
            this.txtART_Evacuation.Location = new System.Drawing.Point(1256, 268);
            this.txtART_Evacuation.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Evacuation.MaxLength = 32767;
            this.txtART_Evacuation.Multiline = false;
            this.txtART_Evacuation.Name = "txtART_Evacuation";
            this.txtART_Evacuation.ReadOnly = false;
            this.txtART_Evacuation.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Evacuation.Size = new System.Drawing.Size(187, 27);
            this.txtART_Evacuation.TabIndex = 581;
            this.txtART_Evacuation.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Evacuation.UseSystemPasswordChar = false;
            // 
            // txtART_ConnetEC
            // 
            this.txtART_ConnetEC.AccAcceptNumbersOnly = false;
            this.txtART_ConnetEC.AccAllowComma = false;
            this.txtART_ConnetEC.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_ConnetEC.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_ConnetEC.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_ConnetEC.AccHidenValue = "";
            this.txtART_ConnetEC.AccNotAllowedChars = null;
            this.txtART_ConnetEC.AccReadOnly = false;
            this.txtART_ConnetEC.AccReadOnlyAllowDelete = false;
            this.txtART_ConnetEC.AccRequired = false;
            this.txtART_ConnetEC.BackColor = System.Drawing.Color.White;
            this.txtART_ConnetEC.CustomBackColor = System.Drawing.Color.White;
            this.txtART_ConnetEC.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_ConnetEC.ForeColor = System.Drawing.Color.Black;
            this.txtART_ConnetEC.Location = new System.Drawing.Point(1256, 241);
            this.txtART_ConnetEC.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_ConnetEC.MaxLength = 32767;
            this.txtART_ConnetEC.Multiline = false;
            this.txtART_ConnetEC.Name = "txtART_ConnetEC";
            this.txtART_ConnetEC.ReadOnly = false;
            this.txtART_ConnetEC.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_ConnetEC.Size = new System.Drawing.Size(187, 27);
            this.txtART_ConnetEC.TabIndex = 580;
            this.txtART_ConnetEC.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_ConnetEC.UseSystemPasswordChar = false;
            // 
            // txtART_ConnetEf
            // 
            this.txtART_ConnetEf.AccAcceptNumbersOnly = false;
            this.txtART_ConnetEf.AccAllowComma = false;
            this.txtART_ConnetEf.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_ConnetEf.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_ConnetEf.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_ConnetEf.AccHidenValue = "";
            this.txtART_ConnetEf.AccNotAllowedChars = null;
            this.txtART_ConnetEf.AccReadOnly = false;
            this.txtART_ConnetEf.AccReadOnlyAllowDelete = false;
            this.txtART_ConnetEf.AccRequired = false;
            this.txtART_ConnetEf.BackColor = System.Drawing.Color.White;
            this.txtART_ConnetEf.CustomBackColor = System.Drawing.Color.White;
            this.txtART_ConnetEf.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_ConnetEf.ForeColor = System.Drawing.Color.Black;
            this.txtART_ConnetEf.Location = new System.Drawing.Point(1256, 215);
            this.txtART_ConnetEf.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_ConnetEf.MaxLength = 32767;
            this.txtART_ConnetEf.Multiline = false;
            this.txtART_ConnetEf.Name = "txtART_ConnetEf";
            this.txtART_ConnetEf.ReadOnly = false;
            this.txtART_ConnetEf.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_ConnetEf.Size = new System.Drawing.Size(187, 27);
            this.txtART_ConnetEf.TabIndex = 579;
            this.txtART_ConnetEf.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_ConnetEf.UseSystemPasswordChar = false;
            // 
            // txtART_KfUdParAncUd
            // 
            this.txtART_KfUdParAncUd.AccAcceptNumbersOnly = false;
            this.txtART_KfUdParAncUd.AccAllowComma = false;
            this.txtART_KfUdParAncUd.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_KfUdParAncUd.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_KfUdParAncUd.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_KfUdParAncUd.AccHidenValue = "";
            this.txtART_KfUdParAncUd.AccNotAllowedChars = null;
            this.txtART_KfUdParAncUd.AccReadOnly = false;
            this.txtART_KfUdParAncUd.AccReadOnlyAllowDelete = false;
            this.txtART_KfUdParAncUd.AccRequired = false;
            this.txtART_KfUdParAncUd.BackColor = System.Drawing.Color.White;
            this.txtART_KfUdParAncUd.CustomBackColor = System.Drawing.Color.White;
            this.txtART_KfUdParAncUd.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_KfUdParAncUd.ForeColor = System.Drawing.Color.Black;
            this.txtART_KfUdParAncUd.Location = new System.Drawing.Point(1256, 188);
            this.txtART_KfUdParAncUd.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_KfUdParAncUd.MaxLength = 32767;
            this.txtART_KfUdParAncUd.Multiline = false;
            this.txtART_KfUdParAncUd.Name = "txtART_KfUdParAncUd";
            this.txtART_KfUdParAncUd.ReadOnly = false;
            this.txtART_KfUdParAncUd.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_KfUdParAncUd.Size = new System.Drawing.Size(187, 27);
            this.txtART_KfUdParAncUd.TabIndex = 578;
            this.txtART_KfUdParAncUd.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_KfUdParAncUd.UseSystemPasswordChar = false;
            // 
            // txtART_KfusParAncUs
            // 
            this.txtART_KfusParAncUs.AccAcceptNumbersOnly = false;
            this.txtART_KfusParAncUs.AccAllowComma = false;
            this.txtART_KfusParAncUs.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_KfusParAncUs.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_KfusParAncUs.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_KfusParAncUs.AccHidenValue = "";
            this.txtART_KfusParAncUs.AccNotAllowedChars = null;
            this.txtART_KfusParAncUs.AccReadOnly = false;
            this.txtART_KfusParAncUs.AccReadOnlyAllowDelete = false;
            this.txtART_KfusParAncUs.AccRequired = false;
            this.txtART_KfusParAncUs.BackColor = System.Drawing.Color.White;
            this.txtART_KfusParAncUs.CustomBackColor = System.Drawing.Color.White;
            this.txtART_KfusParAncUs.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_KfusParAncUs.ForeColor = System.Drawing.Color.Black;
            this.txtART_KfusParAncUs.Location = new System.Drawing.Point(1256, 162);
            this.txtART_KfusParAncUs.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_KfusParAncUs.MaxLength = 32767;
            this.txtART_KfusParAncUs.Multiline = false;
            this.txtART_KfusParAncUs.Name = "txtART_KfusParAncUs";
            this.txtART_KfusParAncUs.ReadOnly = false;
            this.txtART_KfusParAncUs.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_KfusParAncUs.Size = new System.Drawing.Size(187, 27);
            this.txtART_KfusParAncUs.TabIndex = 577;
            this.txtART_KfusParAncUs.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_KfusParAncUs.UseSystemPasswordChar = false;
            // 
            // txtART_Composant
            // 
            this.txtART_Composant.AccAcceptNumbersOnly = false;
            this.txtART_Composant.AccAllowComma = false;
            this.txtART_Composant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Composant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Composant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Composant.AccHidenValue = "";
            this.txtART_Composant.AccNotAllowedChars = null;
            this.txtART_Composant.AccReadOnly = false;
            this.txtART_Composant.AccReadOnlyAllowDelete = false;
            this.txtART_Composant.AccRequired = false;
            this.txtART_Composant.BackColor = System.Drawing.Color.White;
            this.txtART_Composant.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Composant.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Composant.ForeColor = System.Drawing.Color.Black;
            this.txtART_Composant.Location = new System.Drawing.Point(1256, 135);
            this.txtART_Composant.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Composant.MaxLength = 32767;
            this.txtART_Composant.Multiline = false;
            this.txtART_Composant.Name = "txtART_Composant";
            this.txtART_Composant.ReadOnly = false;
            this.txtART_Composant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Composant.Size = new System.Drawing.Size(187, 27);
            this.txtART_Composant.TabIndex = 576;
            this.txtART_Composant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Composant.UseSystemPasswordChar = false;
            // 
            // txtART_Type
            // 
            this.txtART_Type.AccAcceptNumbersOnly = false;
            this.txtART_Type.AccAllowComma = false;
            this.txtART_Type.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Type.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Type.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Type.AccHidenValue = "";
            this.txtART_Type.AccNotAllowedChars = null;
            this.txtART_Type.AccReadOnly = false;
            this.txtART_Type.AccReadOnlyAllowDelete = false;
            this.txtART_Type.AccRequired = false;
            this.txtART_Type.BackColor = System.Drawing.Color.White;
            this.txtART_Type.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Type.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Type.ForeColor = System.Drawing.Color.Black;
            this.txtART_Type.Location = new System.Drawing.Point(1256, 109);
            this.txtART_Type.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Type.MaxLength = 32767;
            this.txtART_Type.Multiline = false;
            this.txtART_Type.Name = "txtART_Type";
            this.txtART_Type.ReadOnly = false;
            this.txtART_Type.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Type.Size = new System.Drawing.Size(187, 27);
            this.txtART_Type.TabIndex = 575;
            this.txtART_Type.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Type.UseSystemPasswordChar = false;
            // 
            // txtART_ChronoPrix
            // 
            this.txtART_ChronoPrix.AccAcceptNumbersOnly = false;
            this.txtART_ChronoPrix.AccAllowComma = false;
            this.txtART_ChronoPrix.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_ChronoPrix.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_ChronoPrix.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_ChronoPrix.AccHidenValue = "";
            this.txtART_ChronoPrix.AccNotAllowedChars = null;
            this.txtART_ChronoPrix.AccReadOnly = false;
            this.txtART_ChronoPrix.AccReadOnlyAllowDelete = false;
            this.txtART_ChronoPrix.AccRequired = false;
            this.txtART_ChronoPrix.BackColor = System.Drawing.Color.White;
            this.txtART_ChronoPrix.CustomBackColor = System.Drawing.Color.White;
            this.txtART_ChronoPrix.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_ChronoPrix.ForeColor = System.Drawing.Color.Black;
            this.txtART_ChronoPrix.Location = new System.Drawing.Point(1256, 82);
            this.txtART_ChronoPrix.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_ChronoPrix.MaxLength = 32767;
            this.txtART_ChronoPrix.Multiline = false;
            this.txtART_ChronoPrix.Name = "txtART_ChronoPrix";
            this.txtART_ChronoPrix.ReadOnly = false;
            this.txtART_ChronoPrix.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_ChronoPrix.Size = new System.Drawing.Size(187, 27);
            this.txtART_ChronoPrix.TabIndex = 574;
            this.txtART_ChronoPrix.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_ChronoPrix.UseSystemPasswordChar = false;
            // 
            // txtART_CondUnitStock
            // 
            this.txtART_CondUnitStock.AccAcceptNumbersOnly = false;
            this.txtART_CondUnitStock.AccAllowComma = false;
            this.txtART_CondUnitStock.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_CondUnitStock.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_CondUnitStock.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_CondUnitStock.AccHidenValue = "";
            this.txtART_CondUnitStock.AccNotAllowedChars = null;
            this.txtART_CondUnitStock.AccReadOnly = false;
            this.txtART_CondUnitStock.AccReadOnlyAllowDelete = false;
            this.txtART_CondUnitStock.AccRequired = false;
            this.txtART_CondUnitStock.BackColor = System.Drawing.Color.White;
            this.txtART_CondUnitStock.CustomBackColor = System.Drawing.Color.White;
            this.txtART_CondUnitStock.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_CondUnitStock.ForeColor = System.Drawing.Color.Black;
            this.txtART_CondUnitStock.Location = new System.Drawing.Point(1256, 400);
            this.txtART_CondUnitStock.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_CondUnitStock.MaxLength = 32767;
            this.txtART_CondUnitStock.Multiline = false;
            this.txtART_CondUnitStock.Name = "txtART_CondUnitStock";
            this.txtART_CondUnitStock.ReadOnly = false;
            this.txtART_CondUnitStock.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_CondUnitStock.Size = new System.Drawing.Size(187, 27);
            this.txtART_CondUnitStock.TabIndex = 586;
            this.txtART_CondUnitStock.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_CondUnitStock.UseSystemPasswordChar = false;
            // 
            // txtART_SousFam
            // 
            this.txtART_SousFam.AccAcceptNumbersOnly = false;
            this.txtART_SousFam.AccAllowComma = false;
            this.txtART_SousFam.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_SousFam.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_SousFam.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_SousFam.AccHidenValue = "";
            this.txtART_SousFam.AccNotAllowedChars = null;
            this.txtART_SousFam.AccReadOnly = false;
            this.txtART_SousFam.AccReadOnlyAllowDelete = false;
            this.txtART_SousFam.AccRequired = false;
            this.txtART_SousFam.BackColor = System.Drawing.Color.White;
            this.txtART_SousFam.CustomBackColor = System.Drawing.Color.White;
            this.txtART_SousFam.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_SousFam.ForeColor = System.Drawing.Color.Black;
            this.txtART_SousFam.Location = new System.Drawing.Point(1256, 506);
            this.txtART_SousFam.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_SousFam.MaxLength = 32767;
            this.txtART_SousFam.Multiline = false;
            this.txtART_SousFam.Name = "txtART_SousFam";
            this.txtART_SousFam.ReadOnly = false;
            this.txtART_SousFam.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_SousFam.Size = new System.Drawing.Size(187, 27);
            this.txtART_SousFam.TabIndex = 590;
            this.txtART_SousFam.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_SousFam.UseSystemPasswordChar = false;
            // 
            // txtARTP_DatePrix
            // 
            this.txtARTP_DatePrix.AccAcceptNumbersOnly = false;
            this.txtARTP_DatePrix.AccAllowComma = false;
            this.txtARTP_DatePrix.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_DatePrix.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_DatePrix.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_DatePrix.AccHidenValue = "";
            this.txtARTP_DatePrix.AccNotAllowedChars = null;
            this.txtARTP_DatePrix.AccReadOnly = false;
            this.txtARTP_DatePrix.AccReadOnlyAllowDelete = false;
            this.txtARTP_DatePrix.AccRequired = false;
            this.txtARTP_DatePrix.BackColor = System.Drawing.Color.White;
            this.txtARTP_DatePrix.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_DatePrix.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_DatePrix.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_DatePrix.Location = new System.Drawing.Point(1256, 479);
            this.txtARTP_DatePrix.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_DatePrix.MaxLength = 32767;
            this.txtARTP_DatePrix.Multiline = false;
            this.txtARTP_DatePrix.Name = "txtARTP_DatePrix";
            this.txtARTP_DatePrix.ReadOnly = false;
            this.txtARTP_DatePrix.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_DatePrix.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_DatePrix.TabIndex = 589;
            this.txtARTP_DatePrix.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_DatePrix.UseSystemPasswordChar = false;
            // 
            // txtARTP_PrixDonnePar
            // 
            this.txtARTP_PrixDonnePar.AccAcceptNumbersOnly = false;
            this.txtARTP_PrixDonnePar.AccAllowComma = false;
            this.txtARTP_PrixDonnePar.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_PrixDonnePar.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_PrixDonnePar.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_PrixDonnePar.AccHidenValue = "";
            this.txtARTP_PrixDonnePar.AccNotAllowedChars = null;
            this.txtARTP_PrixDonnePar.AccReadOnly = false;
            this.txtARTP_PrixDonnePar.AccReadOnlyAllowDelete = false;
            this.txtARTP_PrixDonnePar.AccRequired = false;
            this.txtARTP_PrixDonnePar.BackColor = System.Drawing.Color.White;
            this.txtARTP_PrixDonnePar.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_PrixDonnePar.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_PrixDonnePar.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_PrixDonnePar.Location = new System.Drawing.Point(1256, 453);
            this.txtARTP_PrixDonnePar.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_PrixDonnePar.MaxLength = 32767;
            this.txtARTP_PrixDonnePar.Multiline = false;
            this.txtARTP_PrixDonnePar.Name = "txtARTP_PrixDonnePar";
            this.txtARTP_PrixDonnePar.ReadOnly = false;
            this.txtARTP_PrixDonnePar.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_PrixDonnePar.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_PrixDonnePar.TabIndex = 588;
            this.txtARTP_PrixDonnePar.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_PrixDonnePar.UseSystemPasswordChar = false;
            // 
            // txtARTP_PrixDevServ
            // 
            this.txtARTP_PrixDevServ.AccAcceptNumbersOnly = false;
            this.txtARTP_PrixDevServ.AccAllowComma = false;
            this.txtARTP_PrixDevServ.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_PrixDevServ.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_PrixDevServ.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_PrixDevServ.AccHidenValue = "";
            this.txtARTP_PrixDevServ.AccNotAllowedChars = null;
            this.txtARTP_PrixDevServ.AccReadOnly = false;
            this.txtARTP_PrixDevServ.AccReadOnlyAllowDelete = false;
            this.txtARTP_PrixDevServ.AccRequired = false;
            this.txtARTP_PrixDevServ.BackColor = System.Drawing.Color.White;
            this.txtARTP_PrixDevServ.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_PrixDevServ.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_PrixDevServ.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_PrixDevServ.Location = new System.Drawing.Point(1256, 426);
            this.txtARTP_PrixDevServ.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_PrixDevServ.MaxLength = 32767;
            this.txtARTP_PrixDevServ.Multiline = false;
            this.txtARTP_PrixDevServ.Name = "txtARTP_PrixDevServ";
            this.txtARTP_PrixDevServ.ReadOnly = false;
            this.txtARTP_PrixDevServ.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_PrixDevServ.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_PrixDevServ.TabIndex = 587;
            this.txtARTP_PrixDevServ.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_PrixDevServ.UseSystemPasswordChar = false;
            // 
            // txtARTP_fournPref
            // 
            this.txtARTP_fournPref.AccAcceptNumbersOnly = false;
            this.txtARTP_fournPref.AccAllowComma = false;
            this.txtARTP_fournPref.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_fournPref.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_fournPref.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_fournPref.AccHidenValue = "";
            this.txtARTP_fournPref.AccNotAllowedChars = null;
            this.txtARTP_fournPref.AccReadOnly = false;
            this.txtARTP_fournPref.AccReadOnlyAllowDelete = false;
            this.txtARTP_fournPref.AccRequired = false;
            this.txtARTP_fournPref.BackColor = System.Drawing.Color.White;
            this.txtARTP_fournPref.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_fournPref.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_fournPref.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_fournPref.Location = new System.Drawing.Point(1490, 400);
            this.txtARTP_fournPref.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_fournPref.MaxLength = 32767;
            this.txtARTP_fournPref.Multiline = false;
            this.txtARTP_fournPref.Name = "txtARTP_fournPref";
            this.txtARTP_fournPref.ReadOnly = false;
            this.txtARTP_fournPref.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_fournPref.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_fournPref.TabIndex = 604;
            this.txtARTP_fournPref.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_fournPref.UseSystemPasswordChar = false;
            // 
            // txtARTP_PrixNetDepan
            // 
            this.txtARTP_PrixNetDepan.AccAcceptNumbersOnly = false;
            this.txtARTP_PrixNetDepan.AccAllowComma = false;
            this.txtARTP_PrixNetDepan.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_PrixNetDepan.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_PrixNetDepan.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_PrixNetDepan.AccHidenValue = "";
            this.txtARTP_PrixNetDepan.AccNotAllowedChars = null;
            this.txtARTP_PrixNetDepan.AccReadOnly = false;
            this.txtARTP_PrixNetDepan.AccReadOnlyAllowDelete = false;
            this.txtARTP_PrixNetDepan.AccRequired = false;
            this.txtARTP_PrixNetDepan.BackColor = System.Drawing.Color.White;
            this.txtARTP_PrixNetDepan.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_PrixNetDepan.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_PrixNetDepan.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_PrixNetDepan.Location = new System.Drawing.Point(1490, 374);
            this.txtARTP_PrixNetDepan.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_PrixNetDepan.MaxLength = 32767;
            this.txtARTP_PrixNetDepan.Multiline = false;
            this.txtARTP_PrixNetDepan.Name = "txtARTP_PrixNetDepan";
            this.txtARTP_PrixNetDepan.ReadOnly = false;
            this.txtARTP_PrixNetDepan.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_PrixNetDepan.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_PrixNetDepan.TabIndex = 603;
            this.txtARTP_PrixNetDepan.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_PrixNetDepan.UseSystemPasswordChar = false;
            // 
            // txtARTP_UsParUc
            // 
            this.txtARTP_UsParUc.AccAcceptNumbersOnly = false;
            this.txtARTP_UsParUc.AccAllowComma = false;
            this.txtARTP_UsParUc.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_UsParUc.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_UsParUc.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_UsParUc.AccHidenValue = "";
            this.txtARTP_UsParUc.AccNotAllowedChars = null;
            this.txtARTP_UsParUc.AccReadOnly = false;
            this.txtARTP_UsParUc.AccReadOnlyAllowDelete = false;
            this.txtARTP_UsParUc.AccRequired = false;
            this.txtARTP_UsParUc.BackColor = System.Drawing.Color.White;
            this.txtARTP_UsParUc.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_UsParUc.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_UsParUc.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_UsParUc.Location = new System.Drawing.Point(1490, 347);
            this.txtARTP_UsParUc.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_UsParUc.MaxLength = 32767;
            this.txtARTP_UsParUc.Multiline = false;
            this.txtARTP_UsParUc.Name = "txtARTP_UsParUc";
            this.txtARTP_UsParUc.ReadOnly = false;
            this.txtARTP_UsParUc.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_UsParUc.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_UsParUc.TabIndex = 602;
            this.txtARTP_UsParUc.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_UsParUc.UseSystemPasswordChar = false;
            // 
            // txtARTP_UdParUs
            // 
            this.txtARTP_UdParUs.AccAcceptNumbersOnly = false;
            this.txtARTP_UdParUs.AccAllowComma = false;
            this.txtARTP_UdParUs.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_UdParUs.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_UdParUs.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_UdParUs.AccHidenValue = "";
            this.txtARTP_UdParUs.AccNotAllowedChars = null;
            this.txtARTP_UdParUs.AccReadOnly = false;
            this.txtARTP_UdParUs.AccReadOnlyAllowDelete = false;
            this.txtARTP_UdParUs.AccRequired = false;
            this.txtARTP_UdParUs.BackColor = System.Drawing.Color.White;
            this.txtARTP_UdParUs.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_UdParUs.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_UdParUs.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_UdParUs.Location = new System.Drawing.Point(1490, 321);
            this.txtARTP_UdParUs.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_UdParUs.MaxLength = 32767;
            this.txtARTP_UdParUs.Multiline = false;
            this.txtARTP_UdParUs.Name = "txtARTP_UdParUs";
            this.txtARTP_UdParUs.ReadOnly = false;
            this.txtARTP_UdParUs.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_UdParUs.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_UdParUs.TabIndex = 601;
            this.txtARTP_UdParUs.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_UdParUs.UseSystemPasswordChar = false;
            // 
            // txtARTP_UvParUc
            // 
            this.txtARTP_UvParUc.AccAcceptNumbersOnly = false;
            this.txtARTP_UvParUc.AccAllowComma = false;
            this.txtARTP_UvParUc.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_UvParUc.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_UvParUc.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_UvParUc.AccHidenValue = "";
            this.txtARTP_UvParUc.AccNotAllowedChars = null;
            this.txtARTP_UvParUc.AccReadOnly = false;
            this.txtARTP_UvParUc.AccReadOnlyAllowDelete = false;
            this.txtARTP_UvParUc.AccRequired = false;
            this.txtARTP_UvParUc.BackColor = System.Drawing.Color.White;
            this.txtARTP_UvParUc.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_UvParUc.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_UvParUc.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_UvParUc.Location = new System.Drawing.Point(1490, 294);
            this.txtARTP_UvParUc.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_UvParUc.MaxLength = 32767;
            this.txtARTP_UvParUc.Multiline = false;
            this.txtARTP_UvParUc.Name = "txtARTP_UvParUc";
            this.txtARTP_UvParUc.ReadOnly = false;
            this.txtARTP_UvParUc.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_UvParUc.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_UvParUc.TabIndex = 600;
            this.txtARTP_UvParUc.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_UvParUc.UseSystemPasswordChar = false;
            // 
            // txtARTP_UcnfParUc
            // 
            this.txtARTP_UcnfParUc.AccAcceptNumbersOnly = false;
            this.txtARTP_UcnfParUc.AccAllowComma = false;
            this.txtARTP_UcnfParUc.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_UcnfParUc.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_UcnfParUc.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_UcnfParUc.AccHidenValue = "";
            this.txtARTP_UcnfParUc.AccNotAllowedChars = null;
            this.txtARTP_UcnfParUc.AccReadOnly = false;
            this.txtARTP_UcnfParUc.AccReadOnlyAllowDelete = false;
            this.txtARTP_UcnfParUc.AccRequired = false;
            this.txtARTP_UcnfParUc.BackColor = System.Drawing.Color.White;
            this.txtARTP_UcnfParUc.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_UcnfParUc.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_UcnfParUc.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_UcnfParUc.Location = new System.Drawing.Point(1490, 268);
            this.txtARTP_UcnfParUc.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_UcnfParUc.MaxLength = 32767;
            this.txtARTP_UcnfParUc.Multiline = false;
            this.txtARTP_UcnfParUc.Name = "txtARTP_UcnfParUc";
            this.txtARTP_UcnfParUc.ReadOnly = false;
            this.txtARTP_UcnfParUc.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_UcnfParUc.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_UcnfParUc.TabIndex = 599;
            this.txtARTP_UcnfParUc.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_UcnfParUc.UseSystemPasswordChar = false;
            // 
            // txtARTP_RefHisto
            // 
            this.txtARTP_RefHisto.AccAcceptNumbersOnly = false;
            this.txtARTP_RefHisto.AccAllowComma = false;
            this.txtARTP_RefHisto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_RefHisto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_RefHisto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_RefHisto.AccHidenValue = "";
            this.txtARTP_RefHisto.AccNotAllowedChars = null;
            this.txtARTP_RefHisto.AccReadOnly = false;
            this.txtARTP_RefHisto.AccReadOnlyAllowDelete = false;
            this.txtARTP_RefHisto.AccRequired = false;
            this.txtARTP_RefHisto.BackColor = System.Drawing.Color.White;
            this.txtARTP_RefHisto.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_RefHisto.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_RefHisto.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_RefHisto.Location = new System.Drawing.Point(1490, 241);
            this.txtARTP_RefHisto.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_RefHisto.MaxLength = 32767;
            this.txtARTP_RefHisto.Multiline = false;
            this.txtARTP_RefHisto.Name = "txtARTP_RefHisto";
            this.txtARTP_RefHisto.ReadOnly = false;
            this.txtARTP_RefHisto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_RefHisto.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_RefHisto.TabIndex = 598;
            this.txtARTP_RefHisto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_RefHisto.UseSystemPasswordChar = false;
            // 
            // txtARTP_RefInfo
            // 
            this.txtARTP_RefInfo.AccAcceptNumbersOnly = false;
            this.txtARTP_RefInfo.AccAllowComma = false;
            this.txtARTP_RefInfo.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_RefInfo.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_RefInfo.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_RefInfo.AccHidenValue = "";
            this.txtARTP_RefInfo.AccNotAllowedChars = null;
            this.txtARTP_RefInfo.AccReadOnly = false;
            this.txtARTP_RefInfo.AccReadOnlyAllowDelete = false;
            this.txtARTP_RefInfo.AccRequired = false;
            this.txtARTP_RefInfo.BackColor = System.Drawing.Color.White;
            this.txtARTP_RefInfo.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_RefInfo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_RefInfo.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_RefInfo.Location = new System.Drawing.Point(1490, 215);
            this.txtARTP_RefInfo.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_RefInfo.MaxLength = 32767;
            this.txtARTP_RefInfo.Multiline = false;
            this.txtARTP_RefInfo.Name = "txtARTP_RefInfo";
            this.txtARTP_RefInfo.ReadOnly = false;
            this.txtARTP_RefInfo.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_RefInfo.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_RefInfo.TabIndex = 597;
            this.txtARTP_RefInfo.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_RefInfo.UseSystemPasswordChar = false;
            // 
            // txtART_CategorieOutil
            // 
            this.txtART_CategorieOutil.AccAcceptNumbersOnly = false;
            this.txtART_CategorieOutil.AccAllowComma = false;
            this.txtART_CategorieOutil.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_CategorieOutil.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_CategorieOutil.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_CategorieOutil.AccHidenValue = "";
            this.txtART_CategorieOutil.AccNotAllowedChars = null;
            this.txtART_CategorieOutil.AccReadOnly = false;
            this.txtART_CategorieOutil.AccReadOnlyAllowDelete = false;
            this.txtART_CategorieOutil.AccRequired = false;
            this.txtART_CategorieOutil.BackColor = System.Drawing.Color.White;
            this.txtART_CategorieOutil.CustomBackColor = System.Drawing.Color.White;
            this.txtART_CategorieOutil.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_CategorieOutil.ForeColor = System.Drawing.Color.Black;
            this.txtART_CategorieOutil.Location = new System.Drawing.Point(1490, 188);
            this.txtART_CategorieOutil.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_CategorieOutil.MaxLength = 32767;
            this.txtART_CategorieOutil.Multiline = false;
            this.txtART_CategorieOutil.Name = "txtART_CategorieOutil";
            this.txtART_CategorieOutil.ReadOnly = false;
            this.txtART_CategorieOutil.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_CategorieOutil.Size = new System.Drawing.Size(187, 27);
            this.txtART_CategorieOutil.TabIndex = 596;
            this.txtART_CategorieOutil.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_CategorieOutil.UseSystemPasswordChar = false;
            // 
            // txtART_RubriqueAnaly
            // 
            this.txtART_RubriqueAnaly.AccAcceptNumbersOnly = false;
            this.txtART_RubriqueAnaly.AccAllowComma = false;
            this.txtART_RubriqueAnaly.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_RubriqueAnaly.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_RubriqueAnaly.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_RubriqueAnaly.AccHidenValue = "";
            this.txtART_RubriqueAnaly.AccNotAllowedChars = null;
            this.txtART_RubriqueAnaly.AccReadOnly = false;
            this.txtART_RubriqueAnaly.AccReadOnlyAllowDelete = false;
            this.txtART_RubriqueAnaly.AccRequired = false;
            this.txtART_RubriqueAnaly.BackColor = System.Drawing.Color.White;
            this.txtART_RubriqueAnaly.CustomBackColor = System.Drawing.Color.White;
            this.txtART_RubriqueAnaly.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_RubriqueAnaly.ForeColor = System.Drawing.Color.Black;
            this.txtART_RubriqueAnaly.Location = new System.Drawing.Point(1490, 162);
            this.txtART_RubriqueAnaly.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_RubriqueAnaly.MaxLength = 32767;
            this.txtART_RubriqueAnaly.Multiline = false;
            this.txtART_RubriqueAnaly.Name = "txtART_RubriqueAnaly";
            this.txtART_RubriqueAnaly.ReadOnly = false;
            this.txtART_RubriqueAnaly.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_RubriqueAnaly.Size = new System.Drawing.Size(187, 27);
            this.txtART_RubriqueAnaly.TabIndex = 595;
            this.txtART_RubriqueAnaly.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_RubriqueAnaly.UseSystemPasswordChar = false;
            // 
            // txtART_EtatDiffusion
            // 
            this.txtART_EtatDiffusion.AccAcceptNumbersOnly = false;
            this.txtART_EtatDiffusion.AccAllowComma = false;
            this.txtART_EtatDiffusion.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_EtatDiffusion.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_EtatDiffusion.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_EtatDiffusion.AccHidenValue = "";
            this.txtART_EtatDiffusion.AccNotAllowedChars = null;
            this.txtART_EtatDiffusion.AccReadOnly = false;
            this.txtART_EtatDiffusion.AccReadOnlyAllowDelete = false;
            this.txtART_EtatDiffusion.AccRequired = false;
            this.txtART_EtatDiffusion.BackColor = System.Drawing.Color.White;
            this.txtART_EtatDiffusion.CustomBackColor = System.Drawing.Color.White;
            this.txtART_EtatDiffusion.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_EtatDiffusion.ForeColor = System.Drawing.Color.Black;
            this.txtART_EtatDiffusion.Location = new System.Drawing.Point(1490, 135);
            this.txtART_EtatDiffusion.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_EtatDiffusion.MaxLength = 32767;
            this.txtART_EtatDiffusion.Multiline = false;
            this.txtART_EtatDiffusion.Name = "txtART_EtatDiffusion";
            this.txtART_EtatDiffusion.ReadOnly = false;
            this.txtART_EtatDiffusion.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_EtatDiffusion.Size = new System.Drawing.Size(187, 27);
            this.txtART_EtatDiffusion.TabIndex = 594;
            this.txtART_EtatDiffusion.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_EtatDiffusion.UseSystemPasswordChar = false;
            // 
            // txtART_FamAtoll
            // 
            this.txtART_FamAtoll.AccAcceptNumbersOnly = false;
            this.txtART_FamAtoll.AccAllowComma = false;
            this.txtART_FamAtoll.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_FamAtoll.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_FamAtoll.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_FamAtoll.AccHidenValue = "";
            this.txtART_FamAtoll.AccNotAllowedChars = null;
            this.txtART_FamAtoll.AccReadOnly = false;
            this.txtART_FamAtoll.AccReadOnlyAllowDelete = false;
            this.txtART_FamAtoll.AccRequired = false;
            this.txtART_FamAtoll.BackColor = System.Drawing.Color.White;
            this.txtART_FamAtoll.CustomBackColor = System.Drawing.Color.White;
            this.txtART_FamAtoll.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_FamAtoll.ForeColor = System.Drawing.Color.Black;
            this.txtART_FamAtoll.Location = new System.Drawing.Point(1490, 109);
            this.txtART_FamAtoll.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_FamAtoll.MaxLength = 32767;
            this.txtART_FamAtoll.Multiline = false;
            this.txtART_FamAtoll.Name = "txtART_FamAtoll";
            this.txtART_FamAtoll.ReadOnly = false;
            this.txtART_FamAtoll.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_FamAtoll.Size = new System.Drawing.Size(187, 27);
            this.txtART_FamAtoll.TabIndex = 593;
            this.txtART_FamAtoll.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_FamAtoll.UseSystemPasswordChar = false;
            // 
            // txtART_Nature
            // 
            this.txtART_Nature.AccAcceptNumbersOnly = false;
            this.txtART_Nature.AccAllowComma = false;
            this.txtART_Nature.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Nature.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Nature.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_Nature.AccHidenValue = "";
            this.txtART_Nature.AccNotAllowedChars = null;
            this.txtART_Nature.AccReadOnly = false;
            this.txtART_Nature.AccReadOnlyAllowDelete = false;
            this.txtART_Nature.AccRequired = false;
            this.txtART_Nature.BackColor = System.Drawing.Color.White;
            this.txtART_Nature.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Nature.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Nature.ForeColor = System.Drawing.Color.Black;
            this.txtART_Nature.Location = new System.Drawing.Point(1490, 82);
            this.txtART_Nature.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Nature.MaxLength = 32767;
            this.txtART_Nature.Multiline = false;
            this.txtART_Nature.Name = "txtART_Nature";
            this.txtART_Nature.ReadOnly = false;
            this.txtART_Nature.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Nature.Size = new System.Drawing.Size(187, 27);
            this.txtART_Nature.TabIndex = 592;
            this.txtART_Nature.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Nature.UseSystemPasswordChar = false;
            // 
            // Label19
            // 
            this.Label19.AutoSize = true;
            this.Label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label19.Location = new System.Drawing.Point(1534, 472);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(33, 16);
            this.Label19.TabIndex = 605;
            this.Label19.Text = "Prix ";
            // 
            // txtART_dateFinActivite
            // 
            this.txtART_dateFinActivite.AccAcceptNumbersOnly = false;
            this.txtART_dateFinActivite.AccAllowComma = false;
            this.txtART_dateFinActivite.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_dateFinActivite.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_dateFinActivite.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtART_dateFinActivite.AccHidenValue = "";
            this.txtART_dateFinActivite.AccNotAllowedChars = null;
            this.txtART_dateFinActivite.AccReadOnly = false;
            this.txtART_dateFinActivite.AccReadOnlyAllowDelete = false;
            this.txtART_dateFinActivite.AccRequired = false;
            this.txtART_dateFinActivite.BackColor = System.Drawing.Color.White;
            this.txtART_dateFinActivite.CustomBackColor = System.Drawing.Color.White;
            this.txtART_dateFinActivite.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_dateFinActivite.ForeColor = System.Drawing.Color.Black;
            this.txtART_dateFinActivite.Location = new System.Drawing.Point(1256, 532);
            this.txtART_dateFinActivite.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_dateFinActivite.MaxLength = 32767;
            this.txtART_dateFinActivite.Multiline = false;
            this.txtART_dateFinActivite.Name = "txtART_dateFinActivite";
            this.txtART_dateFinActivite.ReadOnly = false;
            this.txtART_dateFinActivite.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_dateFinActivite.Size = new System.Drawing.Size(187, 27);
            this.txtART_dateFinActivite.TabIndex = 606;
            this.txtART_dateFinActivite.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_dateFinActivite.UseSystemPasswordChar = false;
            // 
            // txtARTP_UsParUv
            // 
            this.txtARTP_UsParUv.AccAcceptNumbersOnly = false;
            this.txtARTP_UsParUv.AccAllowComma = false;
            this.txtARTP_UsParUv.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_UsParUv.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_UsParUv.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtARTP_UsParUv.AccHidenValue = "";
            this.txtARTP_UsParUv.AccNotAllowedChars = null;
            this.txtARTP_UsParUv.AccReadOnly = false;
            this.txtARTP_UsParUv.AccReadOnlyAllowDelete = false;
            this.txtARTP_UsParUv.AccRequired = false;
            this.txtARTP_UsParUv.BackColor = System.Drawing.Color.White;
            this.txtARTP_UsParUv.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_UsParUv.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_UsParUv.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_UsParUv.Location = new System.Drawing.Point(1256, 558);
            this.txtARTP_UsParUv.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_UsParUv.MaxLength = 32767;
            this.txtARTP_UsParUv.Multiline = false;
            this.txtARTP_UsParUv.Name = "txtARTP_UsParUv";
            this.txtARTP_UsParUv.ReadOnly = false;
            this.txtARTP_UsParUv.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_UsParUv.Size = new System.Drawing.Size(187, 27);
            this.txtARTP_UsParUv.TabIndex = 607;
            this.txtARTP_UsParUv.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_UsParUv.UseSystemPasswordChar = false;
            // 
            // frmDetailArticleV2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1219, 850);
            this.Controls.Add(this.txtARTP_UsParUv);
            this.Controls.Add(this.txtART_dateFinActivite);
            this.Controls.Add(this.Label19);
            this.Controls.Add(this.txtARTP_fournPref);
            this.Controls.Add(this.txtARTP_PrixNetDepan);
            this.Controls.Add(this.txtARTP_UsParUc);
            this.Controls.Add(this.txtARTP_UdParUs);
            this.Controls.Add(this.txtARTP_UvParUc);
            this.Controls.Add(this.txtARTP_UcnfParUc);
            this.Controls.Add(this.txtARTP_RefHisto);
            this.Controls.Add(this.txtARTP_RefInfo);
            this.Controls.Add(this.txtART_CategorieOutil);
            this.Controls.Add(this.txtART_RubriqueAnaly);
            this.Controls.Add(this.txtART_EtatDiffusion);
            this.Controls.Add(this.txtART_FamAtoll);
            this.Controls.Add(this.txtART_Nature);
            this.Controls.Add(this.txtART_SousFam);
            this.Controls.Add(this.txtARTP_DatePrix);
            this.Controls.Add(this.txtARTP_PrixDonnePar);
            this.Controls.Add(this.txtARTP_PrixDevServ);
            this.Controls.Add(this.txtART_CondUnitStock);
            this.Controls.Add(this.txtART_UcnsParUs);
            this.Controls.Add(this.txtART_DebitGaz);
            this.Controls.Add(this.txtART_DebitEau);
            this.Controls.Add(this.txtART_RaccordGaz);
            this.Controls.Add(this.txtART_Evacuation);
            this.Controls.Add(this.txtART_ConnetEC);
            this.Controls.Add(this.txtART_ConnetEf);
            this.Controls.Add(this.txtART_KfUdParAncUd);
            this.Controls.Add(this.txtART_KfusParAncUs);
            this.Controls.Add(this.txtART_Composant);
            this.Controls.Add(this.txtART_Type);
            this.Controls.Add(this.txtART_ChronoPrix);
            this.Controls.Add(this.ssGridURL);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox6);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(1235, 900);
            this.MinimumSize = new System.Drawing.Size(1235, 726);
            this.Name = "frmDetailArticleV2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Fiche signalétique d\'un article";
            this.Load += new System.EventHandler(this.frmDetailArticleV2_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridEquiv)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridURL)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox6;
        public iTalk.iTalk_TextBox_Small2 txtART_LibelleDevServ;
        private System.Windows.Forms.Label label18;
        public iTalk.iTalk_TextBox_Small2 Text39;
        private System.Windows.Forms.Label label19s;
        public iTalk.iTalk_TextBox_Small2 txtART_Gamme;
        private System.Windows.Forms.Label label16;
        public iTalk.iTalk_TextBox_Small2 txtART_Marque;
        private System.Windows.Forms.Label label17;
        public iTalk.iTalk_TextBox_Small2 txtART_CorpEtat;
        private System.Windows.Forms.Label label14;
        public iTalk.iTalk_TextBox_Small2 txtART_UnitsStock;
        private System.Windows.Forms.Label label15;
        public iTalk.iTalk_TextBox_Small2 txtART_Volume;
        private System.Windows.Forms.Label label12;
        public iTalk.iTalk_TextBox_Small2 txtART_UniteDev;
        private System.Windows.Forms.Label label13;
        public iTalk.iTalk_TextBox_Small2 txtART_hauteur;
        private System.Windows.Forms.Label label10;
        public iTalk.iTalk_TextBox_Small2 txtART_Surface;
        private System.Windows.Forms.Label label11;
        public iTalk.iTalk_TextBox_Small2 txtART_Largeur;
        private System.Windows.Forms.Label label9;
        public iTalk.iTalk_TextBox_Small2 txtART_Longueur;
        private System.Windows.Forms.Label label8;
        public iTalk.iTalk_TextBox_Small2 txtART_poids;
        private System.Windows.Forms.Label label7;
        public iTalk.iTalk_TextBox_Small2 txtART_UnitPoidsTheo;
        private System.Windows.Forms.Label label6;
        public iTalk.iTalk_TextBox_Small2 txtART_PoidsTheo;
        private System.Windows.Forms.Label label5;
        public iTalk.iTalk_TextBox_Small2 txtART_Synonyme;
        private System.Windows.Forms.Label label4;
        public iTalk.iTalk_TextBox_Small2 txtART_LibelleTechnique;
        private System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 txtART_Libelle;
        private System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtART_Famille;
        private System.Windows.Forms.Label label1;
        public iTalk.iTalk_TextBox_Small2 txtART_Chrono;
        private System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtART_TypoAtoll;
        private System.Windows.Forms.Label label20;
        public iTalk.iTalk_TextBox_Small2 txtART_SupprimeFab;
        private System.Windows.Forms.Label label21;
        public iTalk.iTalk_TextBox_Small2 txtART_StatutVie;
        private System.Windows.Forms.Label label22;
        public iTalk.iTalk_TextBox_Small2 txtART_Remplacement;
        private System.Windows.Forms.Label label23;
        public iTalk.iTalk_TextBox_Small2 txtART_Tva;
        private System.Windows.Forms.Label label24;
        public iTalk.iTalk_TextBox_Small2 txtART_familleGroupe;
        private System.Windows.Forms.Label label25;
        public iTalk.iTalk_TextBox_Small2 txtART_NbLien;
        private System.Windows.Forms.Label label26;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssGridEquiv;
        private System.Windows.Forms.GroupBox groupBox1;
        public iTalk.iTalk_TextBox_Small2 txtARTP_DateAplication;
        private System.Windows.Forms.Label label27;
        public iTalk.iTalk_TextBox_Small2 txtARTP_CoefVente;
        private System.Windows.Forms.Label label28;
        public iTalk.iTalk_TextBox_Small2 txtARTP_DelaiLivr;
        private System.Windows.Forms.Label label29;
        public iTalk.iTalk_TextBox_Small2 txtARTP_Statut;
        private System.Windows.Forms.Label label30;
        public iTalk.iTalk_TextBox_Small2 txtARTP_Devise;
        private System.Windows.Forms.Label label31;
        public iTalk.iTalk_TextBox_Small2 txtARTP_RefFourn;
        private System.Windows.Forms.Label label32;
        public iTalk.iTalk_TextBox_Small2 txtARTP_RefFab;
        private System.Windows.Forms.Label label34;
        public iTalk.iTalk_TextBox_Small2 txtARTP_Marque;
        private System.Windows.Forms.Label label35;
        public iTalk.iTalk_TextBox_Small2 txtARTP_Nomfourn;
        private System.Windows.Forms.Label label36;
        public iTalk.iTalk_TextBox_Small2 txtARTP_Nofourn;
        private System.Windows.Forms.Label label37;
        public iTalk.iTalk_TextBox_Small2 txtARTP_EcoTaxe;
        private System.Windows.Forms.Label label56;
        public iTalk.iTalk_TextBox_Small2 txtARTP_MajorDevis;
        private System.Windows.Forms.Label label55;
        public iTalk.iTalk_TextBox_Small2 txtARTP_RemiseIndic;
        private System.Windows.Forms.Label label48;
        public iTalk.iTalk_TextBox_Small2 txtARTP_RemisePourCent;
        private System.Windows.Forms.Label label49;
        public iTalk.iTalk_TextBox_Small2 txtARTP_UniteDevis;
        private System.Windows.Forms.Label label50;
        public iTalk.iTalk_TextBox_Small2 txtARTP_PrixNetDevis;
        private System.Windows.Forms.Label label51;
        public iTalk.iTalk_TextBox_Small2 txtARTP_LibelleCond;
        private System.Windows.Forms.Label label52;
        public iTalk.iTalk_TextBox_Small2 txtARTP_PrixTarif;
        private System.Windows.Forms.Label label53;
        public iTalk.iTalk_TextBox_Small2 txtARTP_StatutPrixNet;
        private System.Windows.Forms.Label label54;
        public iTalk.iTalk_TextBox_Small2 txtARTP_PrixNetStock;
        private System.Windows.Forms.Label label38;
        public iTalk.iTalk_TextBox_Small2 txtARTP_PrixNet;
        private System.Windows.Forms.Label label39;
        public iTalk.iTalk_TextBox_Small2 txtARTP_PrixPublic;
        private System.Windows.Forms.Label label40;
        public iTalk.iTalk_TextBox_Small2 txtARTP_UniteFourn;
        private System.Windows.Forms.Label label41;
        public iTalk.iTalk_TextBox_Small2 txtARTP_QteMinUC;
        private System.Windows.Forms.Label label42;
        public iTalk.iTalk_TextBox_Small2 txtARTP_PrixMulti;
        private System.Windows.Forms.Label label43;
        public iTalk.iTalk_TextBox_Small2 txtARTP_UniteValeur;
        private System.Windows.Forms.Label label44;
        public iTalk.iTalk_TextBox_Small2 txtARTP_PrixNetCond;
        private System.Windows.Forms.Label label45;
        public iTalk.iTalk_TextBox_Small2 txtARTP_UnitCondFourn;
        private System.Windows.Forms.Label label46;
        public iTalk.iTalk_TextBox_Small2 txtARTP_QteMinUCNF;
        private System.Windows.Forms.Label label47;
        public Infragistics.Win.UltraWinGrid.UltraGrid ssGridURL;
        public iTalk.iTalk_TextBox_Small2 txtART_UcnsParUs;
        public iTalk.iTalk_TextBox_Small2 txtART_DebitGaz;
        public iTalk.iTalk_TextBox_Small2 txtART_DebitEau;
        public iTalk.iTalk_TextBox_Small2 txtART_RaccordGaz;
        public iTalk.iTalk_TextBox_Small2 txtART_Evacuation;
        public iTalk.iTalk_TextBox_Small2 txtART_ConnetEC;
        public iTalk.iTalk_TextBox_Small2 txtART_ConnetEf;
        public iTalk.iTalk_TextBox_Small2 txtART_KfUdParAncUd;
        public iTalk.iTalk_TextBox_Small2 txtART_KfusParAncUs;
        public iTalk.iTalk_TextBox_Small2 txtART_Composant;
        public iTalk.iTalk_TextBox_Small2 txtART_Type;
        public iTalk.iTalk_TextBox_Small2 txtART_ChronoPrix;
        public iTalk.iTalk_TextBox_Small2 txtART_CondUnitStock;
        public iTalk.iTalk_TextBox_Small2 txtART_SousFam;
        public iTalk.iTalk_TextBox_Small2 txtARTP_DatePrix;
        public iTalk.iTalk_TextBox_Small2 txtARTP_PrixDonnePar;
        public iTalk.iTalk_TextBox_Small2 txtARTP_PrixDevServ;
        public iTalk.iTalk_TextBox_Small2 txtARTP_fournPref;
        public iTalk.iTalk_TextBox_Small2 txtARTP_PrixNetDepan;
        public iTalk.iTalk_TextBox_Small2 txtARTP_UsParUc;
        public iTalk.iTalk_TextBox_Small2 txtARTP_UdParUs;
        public iTalk.iTalk_TextBox_Small2 txtARTP_UvParUc;
        public iTalk.iTalk_TextBox_Small2 txtARTP_UcnfParUc;
        public iTalk.iTalk_TextBox_Small2 txtARTP_RefHisto;
        public iTalk.iTalk_TextBox_Small2 txtARTP_RefInfo;
        public iTalk.iTalk_TextBox_Small2 txtART_CategorieOutil;
        public iTalk.iTalk_TextBox_Small2 txtART_RubriqueAnaly;
        public iTalk.iTalk_TextBox_Small2 txtART_EtatDiffusion;
        public iTalk.iTalk_TextBox_Small2 txtART_FamAtoll;
        public iTalk.iTalk_TextBox_Small2 txtART_Nature;
        public System.Windows.Forms.Button cmdFamille;
        private System.Windows.Forms.Label Label19;
        public iTalk.iTalk_TextBox_Small2 txtART_dateFinActivite;
        public iTalk.iTalk_TextBox_Small2 txtARTP_UsParUv;
    }
}