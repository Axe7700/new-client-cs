﻿namespace Axe_interDT.Views.Devis.Forms
{
    partial class FrmRestaureDevis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.TreeView1 = new Infragistics.Win.UltraWinTree.UltraTree();
            this.GridDevis = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cmdRestaurer = new System.Windows.Forms.Button();
            this.Command2 = new System.Windows.Forms.Button();
            this.txtNbLigne = new iTalk.iTalk_TextBox_Small2();
            this.txtTotalHT = new iTalk.iTalk_TextBox_Small2();
            this.txtDateDevis = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.txtNodevis = new iTalk.iTalk_TextBox_Small2();
            ((System.ComponentModel.ISupportInitialize)(this.TreeView1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDevis)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(617, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "Apres chaque validation du devis, une copie de celui-ci sera présente dans ce for" +
    "mulaire.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(1004, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Sur le côté gauche se trouve tous les devis sauvegardés et horodaté. Sélectionnez" +
    " le devis que vous désirez et cliquer sur le bouton \" Restaurer \".";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(12, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(125, 19);
            this.label3.TabIndex = 2;
            this.label3.Text = "Numéro de devis";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(12, 101);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(35, 13);
            this.label4.TabIndex = 2;
            this.label4.Text = "label3";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(12, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 19);
            this.label5.TabIndex = 2;
            this.label5.Text = "Code immeuble";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(287, 142);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(67, 19);
            this.label6.TabIndex = 2;
            this.label6.Text = "Devis du";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(519, 142);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(67, 19);
            this.label7.TabIndex = 2;
            this.label7.Text = "Total HT";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(742, 142);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(94, 19);
            this.label8.TabIndex = 2;
            this.label8.Text = "Nb de lignes";
            // 
            // TreeView1
            // 
            this.TreeView1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.TreeView1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.TreeView1.Location = new System.Drawing.Point(6, 136);
            this.TreeView1.Name = "TreeView1";
            this.TreeView1.Size = new System.Drawing.Size(264, 528);
            this.TreeView1.TabIndex = 503;
            this.TreeView1.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.TreeView1_AfterSelect);
            // 
            // GridDevis
            // 
            this.GridDevis.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.GridDevis.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridDevis.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridDevis.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridDevis.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridDevis.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridDevis.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.GridDevis.DisplayLayout.UseFixedHeaders = true;
            this.GridDevis.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GridDevis.Location = new System.Drawing.Point(276, 168);
            this.GridDevis.Name = "GridDevis";
            this.GridDevis.Size = new System.Drawing.Size(991, 496);
            this.GridDevis.TabIndex = 572;
            this.GridDevis.Text = "Résultat";
            this.GridDevis.AfterRowsDeleted += new System.EventHandler(this.GridDevis_AfterRowsDeleted);
            this.GridDevis.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.GridDevis_BeforeRowUpdate);
            this.GridDevis.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridDevis_BeforeRowsDeleted);
            // 
            // cmdRestaurer
            // 
            this.cmdRestaurer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdRestaurer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdRestaurer.FlatAppearance.BorderSize = 0;
            this.cmdRestaurer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRestaurer.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRestaurer.ForeColor = System.Drawing.Color.White;
            this.cmdRestaurer.Location = new System.Drawing.Point(1058, 9);
            this.cmdRestaurer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdRestaurer.Name = "cmdRestaurer";
            this.cmdRestaurer.Size = new System.Drawing.Size(98, 35);
            this.cmdRestaurer.TabIndex = 573;
            this.cmdRestaurer.Text = "Restaurer";
            this.cmdRestaurer.UseVisualStyleBackColor = false;
            this.cmdRestaurer.Click += new System.EventHandler(this.cmdRestaurer_Click);
            // 
            // Command2
            // 
            this.Command2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command2.FlatAppearance.BorderSize = 0;
            this.Command2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command2.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command2.ForeColor = System.Drawing.Color.White;
            this.Command2.Location = new System.Drawing.Point(1170, 9);
            this.Command2.Margin = new System.Windows.Forms.Padding(2);
            this.Command2.Name = "Command2";
            this.Command2.Size = new System.Drawing.Size(98, 35);
            this.Command2.TabIndex = 573;
            this.Command2.Text = "Annuler";
            this.Command2.UseVisualStyleBackColor = false;
            this.Command2.Click += new System.EventHandler(this.Command2_Click);
            // 
            // txtNbLigne
            // 
            this.txtNbLigne.AccAcceptNumbersOnly = false;
            this.txtNbLigne.AccAllowComma = false;
            this.txtNbLigne.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNbLigne.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNbLigne.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNbLigne.AccHidenValue = "";
            this.txtNbLigne.AccNotAllowedChars = null;
            this.txtNbLigne.AccReadOnly = false;
            this.txtNbLigne.AccReadOnlyAllowDelete = false;
            this.txtNbLigne.AccRequired = false;
            this.txtNbLigne.BackColor = System.Drawing.Color.White;
            this.txtNbLigne.CustomBackColor = System.Drawing.Color.White;
            this.txtNbLigne.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNbLigne.ForeColor = System.Drawing.Color.Black;
            this.txtNbLigne.Location = new System.Drawing.Point(841, 137);
            this.txtNbLigne.Margin = new System.Windows.Forms.Padding(2);
            this.txtNbLigne.MaxLength = 32767;
            this.txtNbLigne.Multiline = false;
            this.txtNbLigne.Name = "txtNbLigne";
            this.txtNbLigne.ReadOnly = false;
            this.txtNbLigne.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNbLigne.Size = new System.Drawing.Size(102, 27);
            this.txtNbLigne.TabIndex = 502;
            this.txtNbLigne.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNbLigne.UseSystemPasswordChar = false;
            // 
            // txtTotalHT
            // 
            this.txtTotalHT.AccAcceptNumbersOnly = false;
            this.txtTotalHT.AccAllowComma = false;
            this.txtTotalHT.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTotalHT.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTotalHT.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTotalHT.AccHidenValue = "";
            this.txtTotalHT.AccNotAllowedChars = null;
            this.txtTotalHT.AccReadOnly = false;
            this.txtTotalHT.AccReadOnlyAllowDelete = false;
            this.txtTotalHT.AccRequired = false;
            this.txtTotalHT.BackColor = System.Drawing.Color.White;
            this.txtTotalHT.CustomBackColor = System.Drawing.Color.White;
            this.txtTotalHT.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtTotalHT.ForeColor = System.Drawing.Color.Black;
            this.txtTotalHT.Location = new System.Drawing.Point(597, 136);
            this.txtTotalHT.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotalHT.MaxLength = 32767;
            this.txtTotalHT.Multiline = false;
            this.txtTotalHT.Name = "txtTotalHT";
            this.txtTotalHT.ReadOnly = false;
            this.txtTotalHT.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTotalHT.Size = new System.Drawing.Size(140, 27);
            this.txtTotalHT.TabIndex = 502;
            this.txtTotalHT.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTotalHT.UseSystemPasswordChar = false;
            // 
            // txtDateDevis
            // 
            this.txtDateDevis.AccAcceptNumbersOnly = false;
            this.txtDateDevis.AccAllowComma = false;
            this.txtDateDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateDevis.AccHidenValue = "";
            this.txtDateDevis.AccNotAllowedChars = null;
            this.txtDateDevis.AccReadOnly = false;
            this.txtDateDevis.AccReadOnlyAllowDelete = false;
            this.txtDateDevis.AccRequired = false;
            this.txtDateDevis.BackColor = System.Drawing.Color.White;
            this.txtDateDevis.CustomBackColor = System.Drawing.Color.White;
            this.txtDateDevis.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtDateDevis.ForeColor = System.Drawing.Color.Black;
            this.txtDateDevis.Location = new System.Drawing.Point(368, 137);
            this.txtDateDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateDevis.MaxLength = 32767;
            this.txtDateDevis.Multiline = false;
            this.txtDateDevis.Name = "txtDateDevis";
            this.txtDateDevis.ReadOnly = false;
            this.txtDateDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateDevis.Size = new System.Drawing.Size(146, 27);
            this.txtDateDevis.TabIndex = 502;
            this.txtDateDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateDevis.UseSystemPasswordChar = false;
            // 
            // txtCodeImmeuble
            // 
            this.txtCodeImmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeImmeuble.AccAllowComma = false;
            this.txtCodeImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeuble.AccHidenValue = "";
            this.txtCodeImmeuble.AccNotAllowedChars = null;
            this.txtCodeImmeuble.AccReadOnly = false;
            this.txtCodeImmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeuble.AccRequired = false;
            this.txtCodeImmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtCodeImmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeImmeuble.Location = new System.Drawing.Point(152, 101);
            this.txtCodeImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeuble.MaxLength = 32767;
            this.txtCodeImmeuble.Multiline = false;
            this.txtCodeImmeuble.Name = "txtCodeImmeuble";
            this.txtCodeImmeuble.ReadOnly = false;
            this.txtCodeImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeuble.Size = new System.Drawing.Size(216, 27);
            this.txtCodeImmeuble.TabIndex = 502;
            this.txtCodeImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeuble.UseSystemPasswordChar = false;
            // 
            // txtNodevis
            // 
            this.txtNodevis.AccAcceptNumbersOnly = false;
            this.txtNodevis.AccAllowComma = false;
            this.txtNodevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNodevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNodevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNodevis.AccHidenValue = "";
            this.txtNodevis.AccNotAllowedChars = null;
            this.txtNodevis.AccReadOnly = false;
            this.txtNodevis.AccReadOnlyAllowDelete = false;
            this.txtNodevis.AccRequired = false;
            this.txtNodevis.BackColor = System.Drawing.Color.White;
            this.txtNodevis.CustomBackColor = System.Drawing.Color.White;
            this.txtNodevis.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtNodevis.ForeColor = System.Drawing.Color.Black;
            this.txtNodevis.Location = new System.Drawing.Point(152, 59);
            this.txtNodevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtNodevis.MaxLength = 32767;
            this.txtNodevis.Multiline = false;
            this.txtNodevis.Name = "txtNodevis";
            this.txtNodevis.ReadOnly = false;
            this.txtNodevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNodevis.Size = new System.Drawing.Size(216, 27);
            this.txtNodevis.TabIndex = 502;
            this.txtNodevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNodevis.UseSystemPasswordChar = false;
            // 
            // FrmRestaureDevis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1279, 676);
            this.Controls.Add(this.Command2);
            this.Controls.Add(this.cmdRestaurer);
            this.Controls.Add(this.GridDevis);
            this.Controls.Add(this.TreeView1);
            this.Controls.Add(this.txtNbLigne);
            this.Controls.Add(this.txtTotalHT);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.txtDateDevis);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.txtCodeImmeuble);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtNodevis);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Name = "FrmRestaureDevis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmRestaureDevis";
            this.Load += new System.EventHandler(this.FrmRestaureDevis_Load);
            ((System.ComponentModel.ISupportInitialize)(this.TreeView1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDevis)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        public iTalk.iTalk_TextBox_Small2 txtNodevis;
        private System.Windows.Forms.Label label5;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeuble;
        private System.Windows.Forms.Label label6;
        public iTalk.iTalk_TextBox_Small2 txtDateDevis;
        private System.Windows.Forms.Label label7;
        public iTalk.iTalk_TextBox_Small2 txtTotalHT;
        private System.Windows.Forms.Label label8;
        public iTalk.iTalk_TextBox_Small2 txtNbLigne;
        private Infragistics.Win.UltraWinTree.UltraTree TreeView1;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridDevis;
        public System.Windows.Forms.Button cmdRestaurer;
        public System.Windows.Forms.Button Command2;
    }
}