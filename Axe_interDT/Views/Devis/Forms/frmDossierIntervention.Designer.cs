﻿namespace Axe_interDT.Views.Devis.Forms
{
    partial class frmDossierIntervention
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmDossierIntervention));
            this.lblIntervention = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.DirNoInter1111 = new System.Windows.Forms.TreeView();
            this.Dir1111 = new System.Windows.Forms.TreeView();
            this.cmdRecup = new System.Windows.Forms.Button();
            this.cmdRendre = new System.Windows.Forms.Button();
            this.cmdToutRendre = new System.Windows.Forms.Button();
            this.cmdToutRecup = new System.Windows.Forms.Button();
            this.cmdQuitter = new System.Windows.Forms.Button();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Dir1 = new Microsoft.VisualBasic.Compatibility.VB6.DirListBox();
            this.panel2 = new System.Windows.Forms.Panel();
            this.DirNoInter = new Microsoft.VisualBasic.Compatibility.VB6.DirListBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.File1 = new Microsoft.VisualBasic.Compatibility.VB6.FileListBox();
            this.FileIntervention = new Microsoft.VisualBasic.Compatibility.VB6.FileListBox();
            this.Drive1 = new Microsoft.VisualBasic.Compatibility.VB6.DriveListBox();
            this.panel1.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // lblIntervention
            // 
            this.lblIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblIntervention.Location = new System.Drawing.Point(3, 0);
            this.lblIntervention.Name = "lblIntervention";
            this.lblIntervention.Size = new System.Drawing.Size(242, 17);
            this.lblIntervention.TabIndex = 384;
            this.lblIntervention.Text = "Fichiers presents pour ";
            this.lblIntervention.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(307, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(138, 19);
            this.label1.TabIndex = 385;
            this.label1.Text = "Fichiers à déplacer";
            // 
            // DirNoInter1111
            // 
            this.DirNoInter1111.Location = new System.Drawing.Point(27, 64);
            this.DirNoInter1111.Name = "DirNoInter1111";
            this.DirNoInter1111.Size = new System.Drawing.Size(148, 41);
            this.DirNoInter1111.TabIndex = 504;
            this.DirNoInter1111.Visible = false;
            this.DirNoInter1111.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.DirNoInter_AfterSelect);
            // 
            // Dir1111
            // 
            this.Dir1111.Location = new System.Drawing.Point(259, 39);
            this.Dir1111.Name = "Dir1111";
            this.Dir1111.Size = new System.Drawing.Size(131, 97);
            this.Dir1111.TabIndex = 505;
            this.Dir1111.Visible = false;
            this.Dir1111.AfterSelect += new System.Windows.Forms.TreeViewEventHandler(this.Dir1_AfterSelect);
            // 
            // cmdRecup
            // 
            this.cmdRecup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRecup.FlatAppearance.BorderSize = 0;
            this.cmdRecup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRecup.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRecup.Image = global::Axe_interDT.Properties.Resources.previous_16;
            this.cmdRecup.Location = new System.Drawing.Point(8, 48);
            this.cmdRecup.Name = "cmdRecup";
            this.cmdRecup.Size = new System.Drawing.Size(25, 23);
            this.cmdRecup.TabIndex = 511;
            this.cmdRecup.UseVisualStyleBackColor = false;
            this.cmdRecup.Click += new System.EventHandler(this.cmdRecup_Click);
            // 
            // cmdRendre
            // 
            this.cmdRendre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRendre.FlatAppearance.BorderSize = 0;
            this.cmdRendre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRendre.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRendre.Image = ((System.Drawing.Image)(resources.GetObject("cmdRendre.Image")));
            this.cmdRendre.Location = new System.Drawing.Point(8, 135);
            this.cmdRendre.Name = "cmdRendre";
            this.cmdRendre.Size = new System.Drawing.Size(25, 23);
            this.cmdRendre.TabIndex = 510;
            this.cmdRendre.UseVisualStyleBackColor = false;
            this.cmdRendre.Click += new System.EventHandler(this.cmdRendre_Click);
            // 
            // cmdToutRendre
            // 
            this.cmdToutRendre.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdToutRendre.FlatAppearance.BorderSize = 0;
            this.cmdToutRendre.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdToutRendre.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdToutRendre.Image = global::Axe_interDT.Properties.Resources.last_16;
            this.cmdToutRendre.Location = new System.Drawing.Point(8, 106);
            this.cmdToutRendre.Name = "cmdToutRendre";
            this.cmdToutRendre.Size = new System.Drawing.Size(25, 23);
            this.cmdToutRendre.TabIndex = 509;
            this.cmdToutRendre.UseVisualStyleBackColor = false;
            this.cmdToutRendre.Click += new System.EventHandler(this.cmdToutRendre_Click);
            // 
            // cmdToutRecup
            // 
            this.cmdToutRecup.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdToutRecup.FlatAppearance.BorderSize = 0;
            this.cmdToutRecup.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdToutRecup.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdToutRecup.Image = global::Axe_interDT.Properties.Resources.first_16;
            this.cmdToutRecup.Location = new System.Drawing.Point(8, 77);
            this.cmdToutRecup.Name = "cmdToutRecup";
            this.cmdToutRecup.Size = new System.Drawing.Size(25, 23);
            this.cmdToutRecup.TabIndex = 508;
            this.cmdToutRecup.UseVisualStyleBackColor = false;
            this.cmdToutRecup.Click += new System.EventHandler(this.cmdToutRecup_Click);
            // 
            // cmdQuitter
            // 
            this.cmdQuitter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdQuitter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdQuitter.FlatAppearance.BorderSize = 0;
            this.cmdQuitter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdQuitter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdQuitter.ForeColor = System.Drawing.Color.White;
            this.cmdQuitter.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.cmdQuitter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdQuitter.Location = new System.Drawing.Point(306, 584);
            this.cmdQuitter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdQuitter.Name = "cmdQuitter";
            this.cmdQuitter.Size = new System.Drawing.Size(85, 30);
            this.cmdQuitter.TabIndex = 567;
            this.cmdQuitter.Text = "   Quitter";
            this.cmdQuitter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdQuitter.UseVisualStyleBackColor = false;
            this.cmdQuitter.Click += new System.EventHandler(this.cmdQuitter_Click);
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Controls.Add(this.Dir1);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(307, 57);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(242, 258);
            this.panel1.TabIndex = 570;
            // 
            // Dir1
            // 
            this.Dir1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Dir1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Dir1.FormattingEnabled = true;
            this.Dir1.IntegralHeight = false;
            this.Dir1.Location = new System.Drawing.Point(0, 0);
            this.Dir1.Name = "Dir1";
            this.Dir1.Size = new System.Drawing.Size(240, 256);
            this.Dir1.TabIndex = 570;
            this.Dir1.Change += new System.EventHandler(this.Dir1_Change);
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Controls.Add(this.DirNoInter);
            this.panel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel2.Location = new System.Drawing.Point(3, 57);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(242, 258);
            this.panel2.TabIndex = 571;
            // 
            // DirNoInter
            // 
            this.DirNoInter.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DirNoInter.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.DirNoInter.FormattingEnabled = true;
            this.DirNoInter.IntegralHeight = false;
            this.DirNoInter.Location = new System.Drawing.Point(0, 0);
            this.DirNoInter.Name = "DirNoInter";
            this.DirNoInter.Size = new System.Drawing.Size(240, 256);
            this.DirNoInter.TabIndex = 570;
            this.DirNoInter.Change += new System.EventHandler(this.DirNoInter_Change);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 3;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lblIntervention, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.panel1, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.cmdQuitter, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.panel3, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.File1, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.FileIntervention, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.Drive1, 2, 1);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 23F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(552, 616);
            this.tableLayoutPanel1.TabIndex = 572;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.cmdRecup);
            this.panel3.Controls.Add(this.cmdToutRecup);
            this.panel3.Controls.Add(this.cmdToutRendre);
            this.panel3.Controls.Add(this.cmdRendre);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(251, 57);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(50, 258);
            this.panel3.TabIndex = 572;
            // 
            // File1
            // 
            this.File1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.File1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.File1.FormattingEnabled = true;
            this.File1.Location = new System.Drawing.Point(307, 321);
            this.File1.Name = "File1";
            this.File1.Pattern = "*.*";
            this.File1.Size = new System.Drawing.Size(242, 251);
            this.File1.TabIndex = 573;
            this.File1.DoubleClick += new System.EventHandler(this.File1_DoubleClick);
            // 
            // FileIntervention
            // 
            this.FileIntervention.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FileIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.FileIntervention.FormattingEnabled = true;
            this.FileIntervention.Location = new System.Drawing.Point(3, 321);
            this.FileIntervention.Name = "FileIntervention";
            this.FileIntervention.Pattern = "*.*";
            this.FileIntervention.Size = new System.Drawing.Size(242, 251);
            this.FileIntervention.TabIndex = 574;
            this.FileIntervention.DoubleClick += new System.EventHandler(this.FileIntervention_DoubleClick);
            // 
            // Drive1
            // 
            this.Drive1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Drive1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Drive1.FormattingEnabled = true;
            this.Drive1.Location = new System.Drawing.Point(307, 33);
            this.Drive1.Margin = new System.Windows.Forms.Padding(3, 2, 3, 3);
            this.Drive1.Name = "Drive1";
            this.Drive1.Size = new System.Drawing.Size(242, 26);
            this.Drive1.TabIndex = 575;
            this.Drive1.SelectedIndexChanged += new System.EventHandler(this.Drive1_SelectedIndexChanged_1);
            // 
            // frmDossierIntervention
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(552, 616);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.DirNoInter1111);
            this.Controls.Add(this.Dir1111);
            this.Name = "frmDossierIntervention";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Dossier";
            this.Load += new System.EventHandler(this.frmDossierIntervention_Load);
            this.panel1.ResumeLayout(false);
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Button cmdRecup;
        public System.Windows.Forms.Button cmdRendre;
        public System.Windows.Forms.Button cmdToutRendre;
        public System.Windows.Forms.Button cmdToutRecup;
        public System.Windows.Forms.Button cmdQuitter;
        public System.Windows.Forms.Label lblIntervention;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.TreeView DirNoInter1111;
        public System.Windows.Forms.TreeView Dir1111;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.Panel panel3;
        public Microsoft.VisualBasic.Compatibility.VB6.DirListBox Dir1;
        public Microsoft.VisualBasic.Compatibility.VB6.DirListBox DirNoInter;
        public Microsoft.VisualBasic.Compatibility.VB6.FileListBox File1;
        public Microsoft.VisualBasic.Compatibility.VB6.FileListBox FileIntervention;
        public Microsoft.VisualBasic.Compatibility.VB6.DriveListBox Drive1;
    }
}