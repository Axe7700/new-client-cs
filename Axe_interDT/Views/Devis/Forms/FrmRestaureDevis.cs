﻿using Axe_interDT.Shared;
using Axe_interDT.Views.Theme.CustomMessageBox;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class FrmRestaureDevis : Form
    {
        UserDocDevis UserDocDevis = null;
        public FrmRestaureDevis(UserDocDevis userDocDevis = null)
        {
            InitializeComponent();
            UserDocDevis = userDocDevis;
        }
        DataTable rsDev;
        const string cNiv1 = "A";
        SqlConnection adoBUp;
        ModAdo ModAdoGrid;

        /// <summary>
        /// TESTED
        /// </summary>
        public void fc_LoadTreev()
        {
            string sSQL;
            string sCle;
            DataTable rs;
            string sDBup;
            string[] sTab = new string[1];
            DateTime dtDate;
            string sNodevis;
            string oNode;
            bool bexist = false;
            try
            {
                sDBup = "AXECIEL_BACKUP";
                string provider;
                if (General._ExecutionMode == General.ExecutionMode.Prod || General._ExecutionMode == General.ExecutionMode.Test)
                    provider = "Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=" + sDBup + ";Data Source=" + General.DefaultServer;
                else
                    provider = "Integrated Security=SSPI;Persist Security Info=False;Initial Catalog=" + sDBup + ";Data Source=.";

                adoBUp = new SqlConnection(provider);
                if (adoBUp.State != ConnectionState.Open)
                    adoBUp.Open();
                sSQL = "SELECT     BaseDeDonnee, Utilisateur, DateSave, NumeroDevis From DevisEnTete  WHERE     BaseDeDonnee = '" + General.adocnn.Database + "'  AND  NumeroDevis = '" + StdSQLchaine.gFr_DoublerQuote(txtNodevis.Text) + "' ORDER BY DateSave DESC";
                rs = new DataTable();
                ModAdo ado = new ModAdo();
                rs = ado.fc_OpenRecordSet(sSQL,null,"",adoBUp);

                if (TreeView1.Nodes.Count > 0)
                    TreeView1.Nodes.Clear();
                if (rs.Rows.Count == 0)
                {
                    fc_LoadDetailDevis(txtNodevis.Text, Convert.ToDateTime("00:00:00"));
                }
                foreach (DataRow dr in rs.Rows)
                {
                    sCle = cNiv1 + "@" + dr["BaseDeDonnee"] + "@" + dr["Utilisateur"] + "@" + dr["DateSave"] + "@" + dr["numerodevis"];
                    // '=== Ajout du premier niveau de hiérarchie des localisations.
                    UltraTreeNode node = new UltraTreeNode() { Key = sCle, Text = dr["DateSave"]+"" };
                    node.Override.NodeAppearance.Image = Properties.Resources.check_mark_2_16;
                    TreeView1.Nodes.Add(node);
                    bexist = true;
                }
                ModAdo.fc_CloseRecordset(rs);
                rs = null;
                if (bexist)
                {
                    TreeView1.Nodes[0].Selected = true;
                    //TreeView1.Nodes[0].Override.NodeAppearance.Image= Properties.Resources.arrow_bottom;
                    TreeView1_AfterSelect(this, new SelectEventArgs(TreeView1.SelectedNodes));
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, Name + "; fc_LoadTreev");
            }
        }
        private void FrmRestaureDevis_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);
            fc_LoadTreev();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRestaurer_Click(object sender, EventArgs e)
        {
            UltraTreeNode oNode;
            bool bselect;
            string sID;
            DateTime dtDate = default(DateTime);
            string sNodevis = "";

            bselect = false;
            foreach (var node in TreeView1.Nodes)
            {
                if (node.Selected)
                {
                    if (General.UCase(General.Left(node.Key, 1)) == General.UCase(cNiv1))
                    {
                        sID = node.Key;
                        bselect = true;
                        var sTab = node.Key.Split('@');
                        sNodevis = sTab[4];
                        if (General.IsDate(sTab[3]))
                            dtDate = Convert.ToDateTime(sTab[3]);
                    }
                }
            }
            if (bselect == false)
            {
                CustomMessageBox.Show("Vous devez sélectionner un devis.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            else
            {
                if (CustomMessageBox.Show("Voulez-vous restaurer le devis " + sNodevis + " à la date du " + dtDate + "?", "Restauration", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                    return;
                else
                {
                    fc_RestaureDevis(sNodevis, dtDate);
                    if (UserDocDevis != null)
                        UserDocDevis.bAppliqueVersion = true;
                    this.Close();
                }
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sNodevis"></param>
        /// <param name="dbDate"></param>
        public void fc_RestaureDevis(string sNodevis, DateTime dbDate)
        {
            string sSQL, sDB, sDBup, sUser;
            DateTime dtDate;
            SqlConnection oConn;
            try
            {
                sDB = General.adocnn.Database;
                sUser = General.fncUserName();
                dtDate = DateTime.Now;
                sDBup = "AXECIEL_BACKUP";
                sSQL = "DELETE  FROM DevisDetail where NumeroDevis ='" + sNodevis + "'";
                General.Execute(sSQL);


                sSQL = "INSERT INTO " + sDB + ".dbo.DevisDetail " + " (" + sDB + ".dbo.DevisDetail.NumeroDevis, " + sDB + ".dbo.DevisDetail.NumeroLigne, " + sDB + ".dbo.DevisDetail.CodeFamille, " + sDB + ".dbo.DevisDetail.CodeSousFamille, " + sDB + ".dbo.DevisDetail.CodeArticle, " + sDB + ".dbo.DevisDetail.CodeSousArticle, " + sDB + ".dbo.DevisDetail.NumOrdre, " + " " + sDB + ".dbo.DevisDetail.TexteLigne, " + sDB + ".dbo.DevisDetail.Quantite, " + sDB + ".dbo.DevisDetail.MOhud, " + sDB + ".dbo.DevisDetail.PxHeured, " + sDB + ".dbo.DevisDetail.FOud, " + sDB + ".dbo.DevisDetail.STud, " + sDB + ".dbo.DevisDetail.MOhd, " + sDB + ".dbo.DevisDetail.MOd, " + sDB + ".dbo.DevisDetail.FOd, " + sDB + ".dbo.DevisDetail.STd, " + sDB + ".dbo.DevisDetail.TotalDebours, " + sDB + ".dbo.DevisDetail.kMO, " + sDB + ".dbo.DevisDetail.MO, " + sDB + ".dbo.DevisDetail.kFO, " + sDB + ".dbo.DevisDetail.FO, " + sDB + ".dbo.DevisDetail.kST, " + sDB + ".dbo.DevisDetail.ST, " + sDB + ".dbo.DevisDetail.TotalVente, " + sDB + ".dbo.DevisDetail.Coef1, " + sDB + ".dbo.DevisDetail.Coef2, " + " " + sDB + ".dbo.DevisDetail.Coef3, " + sDB + ".dbo.DevisDetail.TotalVenteApresCoef, " + sDB + ".dbo.DevisDetail.CodeTVA, " + sDB + ".dbo.DevisDetail.Facturer, " + sDB + ".dbo.DevisDetail.DateFacture, " + sDB + ".dbo.DevisDetail.NFamille, " + sDB + ".dbo.DevisDetail.NSFamille, " + sDB + ".dbo.DevisDetail.NArticle, " + sDB + ".dbo.DevisDetail.NSousArticle, " + sDB + ".dbo.DevisDetail.Fournisseur, " + sDB + ".dbo.DevisDetail.CodeMO, " + sDB + ".dbo.DevisDetail.Unite, " + sDB + ".dbo.DevisDetail.SautPage ) ";


                sSQL = sSQL + " SELECT      "
                    + sDBup + ".dbo.DevisDetail.NumeroDevis, " + sDBup + ".dbo.DevisDetail.NumeroLigne, " + sDBup + ".dbo.DevisDetail.CodeFamille, " + sDBup + ".dbo.DevisDetail.CodeSousFamille, " + sDBup + ".dbo.DevisDetail.CodeArticle, " + sDBup + ".dbo.DevisDetail.CodeSousArticle, " + sDBup + ".dbo.DevisDetail.NumOrdre, "
                    + " " + sDBup + ".dbo.DevisDetail.TexteLigne, " + sDBup + ".dbo.DevisDetail.Quantite, " + sDBup + ".dbo.DevisDetail.MOhud, " + sDBup + ".dbo.DevisDetail.PxHeured, " + sDBup + ".dbo.DevisDetail.FOud, " + sDBup + ".dbo.DevisDetail.STud, "
                    + sDBup + ".dbo.DevisDetail.MOhd, " + sDBup + ".dbo.DevisDetail.MOd, " + sDBup + ".dbo.DevisDetail.FOd, " + sDBup + ".dbo.DevisDetail.STd, " + sDBup + ".dbo.DevisDetail.TotalDebours, " + sDBup + ".dbo.DevisDetail.kMO, "
                    + sDBup + ".dbo.DevisDetail.MO, " + sDBup + ".dbo.DevisDetail.kFO, " + sDBup + ".dbo.DevisDetail.FO, " + sDBup + ".dbo.DevisDetail.kST, " + sDBup + ".dbo.DevisDetail.ST, " + sDBup + ".dbo.DevisDetail.TotalVente, "
                    + sDBup + ".dbo.DevisDetail.Coef1, " + sDBup + ".dbo.DevisDetail.Coef2, "
                    + " " + sDBup + ".dbo.DevisDetail.Coef3, " + sDBup + ".dbo.DevisDetail.TotalVenteApresCoef, " + sDBup + ".dbo.DevisDetail.CodeTVA, " + sDBup + ".dbo.DevisDetail.Facturer, " + sDBup + ".dbo.DevisDetail.DateFacture, "
                    + sDBup + ".dbo.DevisDetail.NFamille, " + sDBup + ".dbo.DevisDetail.NSFamille, " + sDBup + ".dbo.DevisDetail.NArticle, " + sDBup + ".dbo.DevisDetail.NSousArticle, " + sDBup + ".dbo.DevisDetail.Fournisseur, "
                    + sDBup + ".dbo.DevisDetail.CodeMO, " + sDBup + ".dbo.DevisDetail.Unite, "
                    + " " + sDBup + ".dbo.DevisDetail.SautPage "
                    + " FROM         " + sDBup + ".dbo.DevisDetail WHERE " + sDBup + ".dbo.DevisDetail.NumeroDevis ='" + sNodevis + "'"
                    + " AND " + sDBup + ".dbo.DevisDetail.BaseDeDonnee ='" + General.adocnn.Database + "'"
                    + " AND " + sDBup + ".dbo.DevisDetail.DateSave ='" + dbDate + "'";
                General.Execute(sSQL);
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "ModSaveDEvis;fc_SaveDevis");
            }
        }

        private void Command2_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void TreeView1_AfterSelect(object sender, SelectEventArgs e)
        {
            DateTime dtDate = default(DateTime);
            string sNodevis;
            var node = TreeView1.ActiveNode;
            if (node != null)
            {
                var sTab = node.Key.Split('@');
                if (sTab.Length >= 4)
                {
                    sNodevis = sTab[4];
                    if (General.IsDate(sTab[3]))
                    {
                        dtDate = Convert.ToDateTime(sTab[3]);
                        txtDateDevis.Text = sTab[3];
                    }
                    fc_LoadDetailDevis(sNodevis, dtDate);
                }
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sNodevis"></param>
        /// <param name="dtDate"></param>
        public void fc_LoadDetailDevis(string sNodevis, DateTime dtDate)
        {
            string sSQL, sCle, sDBup;
            DataTable rs;

            try
            {
                sSQL = "SELECT     TotalHT FROM         DevisEnTete  DevisEnTete  WHERE   BaseDeDonnee = '" + General.adocnn.Database + "' AND DateSave ='" + dtDate + "' AND NumeroDevis = '" + sNodevis + "'";

                rsDev = new DataTable();
                var Modado = new ModAdo();
                rsDev = Modado.fc_OpenRecordSet(sSQL,null,"",adoBUp);
                if (rsDev.Rows.Count > 0)
                    txtTotalHT.Text = General.nz(rsDev.Rows[0]["TotalHT"], 0) + "";
                else
                    txtTotalHT.Text = "0";
                ModAdo.fc_CloseRecordset(rsDev);
                rsDev = null;
                Modado.Close();
                sSQL = "SELECT     NumeroDevis, NumeroLigne, CodeFamille, CodeSousFamille, CodeArticle, CodeSousArticle, NumOrdre, " + " TexteLigne, Quantite, MOhud, PxHeured, FOud, " + " STud, MOhd, MOd, FOd, STd, TotalDebours, kMO, MO, kFO, FO, kST, ST, TotalVente, " + " Coef1, Coef2, Coef3, TotalVenteApresCoef, CodeTVA, Facturer, " + " DateFacture , NFamille, NSFamille, NArticle, NSousArticle, Fournisseur, CodeMO, Unite, SautPage " + " FROM         DevisDetail" + " WHERE   BaseDeDonnee = '" + General.adocnn.Database + "' AND DateSave ='" + dtDate + "' AND NumeroDevis = '" + sNodevis + "' order by NumeroLigne";
                this.Cursor = Cursors.WaitCursor;
                ModAdoGrid = new ModAdo();
                rsDev = new DataTable();

                rsDev = ModAdoGrid.fc_OpenRecordSet(sSQL,null,"",adoBUp);
                GridDevis.DataSource = rsDev;
                this.Cursor = Cursors.Arrow;
                txtNbLigne.Text = rsDev.Rows.Count + "";
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, Name + ";fc_LoadDetailDevis");
            }
        }

        private void GridDevis_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {
            if (e.Row != null)
            {
                var sql = "select count(*) from devisDetail where NumeroDevis='" + e.Row.Cells["NumeroDevis"].Value + "' and NumeroLigne='" + e.Row.Cells["NumeroLigne"].Value + "'";
                var ado = new ModAdo();
                var count = ado.fc_ADOlibelle(sql);
                if (Convert.ToInt32(count) > 0)
                {
                    CustomMessageBox.Show("Cet enregistrement exist deja");
                    e.Row.CancelUpdate();
                    e.Cancel = true;
                    ado.Close();
                    return;
                }
                ado.Close();

                ModAdoGrid.Update();
            }
        }

        private void GridDevis_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;

        }

        private void GridDevis_AfterRowsDeleted(object sender, EventArgs e)
        {
            ModAdoGrid.Update();
        }
    }
}
