﻿namespace Axe_interDT.Views.Devis.Forms
{
    partial class frmModele
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.cmdAppliquer = new System.Windows.Forms.Button();
            this.Option1 = new System.Windows.Forms.RadioButton();
            this.Label43 = new System.Windows.Forms.Label();
            this.Label435 = new System.Windows.Forms.Label();
            this.SSDBGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.SSOleDBGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.Label1 = new System.Windows.Forms.Label();
            this.Frame10 = new System.Windows.Forms.GroupBox();
            this.Label2142 = new System.Windows.Forms.Label();
            this.Label2127 = new System.Windows.Forms.Label();
            this.Label2130 = new System.Windows.Forms.Label();
            this.Label2128 = new System.Windows.Forms.Label();
            this.Label21 = new System.Windows.Forms.Label();
            this.cmdSauvegarder = new System.Windows.Forms.Button();
            this.cmdFermer = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.cmdRechercheModele0 = new System.Windows.Forms.Button();
            this.cmdRechercheModele1 = new System.Windows.Forms.Button();
            this.chkTarifActualiser = new System.Windows.Forms.CheckBox();
            this.fraActualiseGecet = new System.Windows.Forms.Panel();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.cmdQuitter = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.ultraGroupBox4 = new Infragistics.Win.Misc.UltraGroupBox();
            this.txtFiltreModele = new iTalk.iTalk_TextBox_Small2();
            this.txtFiltreLibelleModele = new iTalk.iTalk_TextBox_Small2();
            this.Text1 = new iTalk.iTalk_RichTextBox();
            this.txtTaux = new iTalk.iTalk_TextBox_Small2();
            this.Text316 = new iTalk.iTalk_TextBox_Small2();
            this.Text313 = new iTalk.iTalk_TextBox_Small2();
            this.Text315 = new iTalk.iTalk_TextBox_Small2();
            this.Text314 = new iTalk.iTalk_TextBox_Small2();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSDBGrid1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).BeginInit();
            this.Frame10.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).BeginInit();
            this.ultraGroupBox4.SuspendLayout();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.groupBox1.Controls.Add(this.Text1);
            this.groupBox1.Location = new System.Drawing.Point(255, 33);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(886, 89);
            this.groupBox1.TabIndex = 8;
            this.groupBox1.TabStop = false;
            // 
            // cmdAppliquer
            // 
            this.cmdAppliquer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAppliquer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAppliquer.FlatAppearance.BorderSize = 0;
            this.cmdAppliquer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAppliquer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdAppliquer.ForeColor = System.Drawing.Color.White;
            this.cmdAppliquer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAppliquer.Location = new System.Drawing.Point(130, 0);
            this.cmdAppliquer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAppliquer.Name = "cmdAppliquer";
            this.cmdAppliquer.Size = new System.Drawing.Size(85, 35);
            this.cmdAppliquer.TabIndex = 401;
            this.cmdAppliquer.Text = "Sélection";
            this.cmdAppliquer.UseVisualStyleBackColor = false;
            this.cmdAppliquer.Click += new System.EventHandler(this.cmdAppliquer_Click);
            // 
            // Option1
            // 
            this.Option1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Option1.AutoSize = true;
            this.Option1.Checked = true;
            this.Option1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Option1.Location = new System.Drawing.Point(119, 56);
            this.Option1.Name = "Option1";
            this.Option1.Size = new System.Drawing.Size(126, 23);
            this.Option1.TabIndex = 10;
            this.Option1.TabStop = true;
            this.Option1.Text = "Modele global";
            this.Option1.UseVisualStyleBackColor = true;
            // 
            // Label43
            // 
            this.Label43.AutoSize = true;
            this.Label43.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label43.Location = new System.Drawing.Point(3, 2);
            this.Label43.Name = "Label43";
            this.Label43.Size = new System.Drawing.Size(167, 19);
            this.Label43.TabIndex = 9;
            this.Label43.Text = "Filtre libelle du modele";
            // 
            // Label435
            // 
            this.Label435.AutoSize = true;
            this.Label435.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label435.Location = new System.Drawing.Point(2, 2);
            this.Label435.Name = "Label435";
            this.Label435.Size = new System.Drawing.Size(108, 19);
            this.Label435.TabIndex = 10;
            this.Label435.Text = "Filtre modele :";
            // 
            // SSDBGrid1
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSDBGrid1.DisplayLayout.Appearance = appearance1;
            this.SSDBGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.SSDBGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.SSDBGrid1.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSDBGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.SSDBGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSDBGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.SSDBGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.SSDBGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSDBGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSDBGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.SSDBGrid1.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.SSDBGrid1.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.SSDBGrid1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.SSDBGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSDBGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.SSDBGrid1.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSDBGrid1.DisplayLayout.Override.CellAppearance = appearance8;
            this.SSDBGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSDBGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.SSDBGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.SSDBGrid1.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.SSDBGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSDBGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.SSDBGrid1.DisplayLayout.Override.RowAppearance = appearance11;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSDBGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.SSDBGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSDBGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSDBGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSDBGrid1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSDBGrid1.Location = new System.Drawing.Point(3, 3);
            this.SSDBGrid1.Name = "SSDBGrid1";
            this.SSDBGrid1.Size = new System.Drawing.Size(634, 457);
            this.SSDBGrid1.TabIndex = 11;
            this.SSDBGrid1.Text = "Liste des modèles";
            this.SSDBGrid1.AfterCellActivate += new System.EventHandler(this.SSDBGrid1_AfterCellActivate);
            this.SSDBGrid1.AfterRowActivate += new System.EventHandler(this.SSDBGrid1_AfterRowActivate);
            this.SSDBGrid1.AfterRowsDeleted += new System.EventHandler(this.SSDBGrid1_AfterRowsDeleted);
            this.SSDBGrid1.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.SSDBGrid1_AfterRowUpdate);
            this.SSDBGrid1.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.SSDBGrid1_BeforeRowUpdate);
            // 
            // SSOleDBGrid1
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBGrid1.DisplayLayout.Appearance = appearance13;
            this.SSOleDBGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.SSOleDBGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBGrid1.DisplayLayout.Override.CellAppearance = appearance20;
            this.SSOleDBGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBGrid1.DisplayLayout.Override.RowAppearance = appearance23;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.SSOleDBGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSOleDBGrid1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSOleDBGrid1.Location = new System.Drawing.Point(643, 3);
            this.SSOleDBGrid1.Name = "SSOleDBGrid1";
            this.SSOleDBGrid1.Size = new System.Drawing.Size(635, 457);
            this.SSOleDBGrid1.TabIndex = 12;
            this.SSOleDBGrid1.Text = "Détail du modèle sélectionné";
            this.SSOleDBGrid1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSOleDBGrid1_InitializeLayout);
            this.SSOleDBGrid1.AfterRowsDeleted += new System.EventHandler(this.SSOleDBGrid1_AfterRowsDeleted);
            this.SSOleDBGrid1.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.SSOleDBGrid1_AfterRowUpdate);
            // 
            // Label1
            // 
            this.Label1.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Label1.Location = new System.Drawing.Point(3, 7);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(57, 16);
            this.Label1.TabIndex = 13;
            this.Label1.Text = "Devis n°";
            // 
            // Frame10
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.Frame10, 2);
            this.Frame10.Controls.Add(this.txtTaux);
            this.Frame10.Controls.Add(this.Text316);
            this.Frame10.Controls.Add(this.Text313);
            this.Frame10.Controls.Add(this.Text315);
            this.Frame10.Controls.Add(this.Text314);
            this.Frame10.Controls.Add(this.Label2142);
            this.Frame10.Controls.Add(this.Label2127);
            this.Frame10.Controls.Add(this.Label2130);
            this.Frame10.Controls.Add(this.Label2128);
            this.Frame10.Controls.Add(this.Label21);
            this.Frame10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Frame10.Location = new System.Drawing.Point(3, 466);
            this.Frame10.Name = "Frame10";
            this.Frame10.Size = new System.Drawing.Size(1275, 51);
            this.Frame10.TabIndex = 14;
            this.Frame10.TabStop = false;
            this.Frame10.Text = "Cofficients";
            // 
            // Label2142
            // 
            this.Label2142.AutoSize = true;
            this.Label2142.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label2142.Location = new System.Drawing.Point(493, 19);
            this.Label2142.Name = "Label2142";
            this.Label2142.Size = new System.Drawing.Size(75, 19);
            this.Label2142.TabIndex = 4;
            this.Label2142.Text = "Taux TVA:";
            // 
            // Label2127
            // 
            this.Label2127.AutoSize = true;
            this.Label2127.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label2127.Location = new System.Drawing.Point(205, 16);
            this.Label2127.Name = "Label2127";
            this.Label2127.Size = new System.Drawing.Size(29, 19);
            this.Label2127.TabIndex = 3;
            this.Label2127.Text = "ST:";
            // 
            // Label2130
            // 
            this.Label2130.AutoSize = true;
            this.Label2130.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label2130.Location = new System.Drawing.Point(302, 16);
            this.Label2130.Name = "Label2130";
            this.Label2130.Size = new System.Drawing.Size(112, 19);
            this.Label2130.TabIndex = 2;
            this.Label2130.Text = "Prix heure MO:";
            // 
            // Label2128
            // 
            this.Label2128.AutoSize = true;
            this.Label2128.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label2128.Location = new System.Drawing.Point(101, 17);
            this.Label2128.Name = "Label2128";
            this.Label2128.Size = new System.Drawing.Size(33, 19);
            this.Label2128.TabIndex = 1;
            this.Label2128.Text = "FO:";
            // 
            // Label21
            // 
            this.Label21.AutoSize = true;
            this.Label21.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label21.Location = new System.Drawing.Point(1, 18);
            this.Label21.Name = "Label21";
            this.Label21.Size = new System.Drawing.Size(38, 19);
            this.Label21.TabIndex = 0;
            this.Label21.Text = "MO:";
            // 
            // cmdSauvegarder
            // 
            this.cmdSauvegarder.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSauvegarder.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSauvegarder.FlatAppearance.BorderSize = 0;
            this.cmdSauvegarder.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSauvegarder.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdSauvegarder.ForeColor = System.Drawing.Color.White;
            this.cmdSauvegarder.Image = global::Axe_interDT.Properties.Resources.Save_16x16;
            this.cmdSauvegarder.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdSauvegarder.Location = new System.Drawing.Point(2, 0);
            this.cmdSauvegarder.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSauvegarder.Name = "cmdSauvegarder";
            this.cmdSauvegarder.Size = new System.Drawing.Size(119, 35);
            this.cmdSauvegarder.TabIndex = 397;
            this.cmdSauvegarder.Text = "      Sauvegarder";
            this.cmdSauvegarder.UseVisualStyleBackColor = false;
            this.cmdSauvegarder.Click += new System.EventHandler(this.cmdSauvegarder_Click);
            // 
            // cmdFermer
            // 
            this.cmdFermer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFermer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdFermer.FlatAppearance.BorderSize = 0;
            this.cmdFermer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFermer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdFermer.ForeColor = System.Drawing.Color.White;
            this.cmdFermer.Image = global::Axe_interDT.Properties.Resources.Ok_16x16;
            this.cmdFermer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdFermer.Location = new System.Drawing.Point(21, 2);
            this.cmdFermer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdFermer.Name = "cmdFermer";
            this.cmdFermer.Size = new System.Drawing.Size(100, 35);
            this.cmdFermer.TabIndex = 398;
            this.cmdFermer.Text = "      Appliquer";
            this.cmdFermer.UseVisualStyleBackColor = false;
            this.cmdFermer.Click += new System.EventHandler(this.cmdFermer_Click);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdAnnuler.ForeColor = System.Drawing.Color.White;
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.cmdAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAnnuler.Location = new System.Drawing.Point(21, 41);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(100, 35);
            this.cmdAnnuler.TabIndex = 399;
            this.cmdAnnuler.Text = "       Annuler";
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // cmdRechercheModele0
            // 
            this.cmdRechercheModele0.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdRechercheModele0.FlatAppearance.BorderSize = 0;
            this.cmdRechercheModele0.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheModele0.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheModele0.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheModele0.Location = new System.Drawing.Point(217, 3);
            this.cmdRechercheModele0.Name = "cmdRechercheModele0";
            this.cmdRechercheModele0.Size = new System.Drawing.Size(28, 20);
            this.cmdRechercheModele0.TabIndex = 401;
            this.cmdRechercheModele0.Tag = "0";
            this.cmdRechercheModele0.UseVisualStyleBackColor = false;
            this.cmdRechercheModele0.Click += new System.EventHandler(this.cmdRechercheModele_Click);
            // 
            // cmdRechercheModele1
            // 
            this.cmdRechercheModele1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdRechercheModele1.FlatAppearance.BorderSize = 0;
            this.cmdRechercheModele1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheModele1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheModele1.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheModele1.Location = new System.Drawing.Point(283, 3);
            this.cmdRechercheModele1.Name = "cmdRechercheModele1";
            this.cmdRechercheModele1.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheModele1.TabIndex = 402;
            this.cmdRechercheModele1.Tag = "1";
            this.cmdRechercheModele1.UseVisualStyleBackColor = false;
            this.cmdRechercheModele1.Click += new System.EventHandler(this.cmdRechercheModele_Click);
            // 
            // chkTarifActualiser
            // 
            this.chkTarifActualiser.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.chkTarifActualiser.AutoSize = true;
            this.tableLayoutPanel2.SetColumnSpan(this.chkTarifActualiser, 2);
            this.chkTarifActualiser.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.chkTarifActualiser.Location = new System.Drawing.Point(1086, 4);
            this.chkTarifActualiser.Name = "chkTarifActualiser";
            this.chkTarifActualiser.Size = new System.Drawing.Size(186, 21);
            this.chkTarifActualiser.TabIndex = 570;
            this.chkTarifActualiser.Text = "Actualiser les tarifs Gecet";
            this.chkTarifActualiser.UseVisualStyleBackColor = true;
            // 
            // fraActualiseGecet
            // 
            this.fraActualiseGecet.Location = new System.Drawing.Point(457, 9);
            this.fraActualiseGecet.Name = "fraActualiseGecet";
            this.fraActualiseGecet.Size = new System.Drawing.Size(200, 23);
            this.fraActualiseGecet.TabIndex = 571;
            // 
            // Timer1
            // 
            this.Timer1.Interval = 1000;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // cmdQuitter
            // 
            this.cmdQuitter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdQuitter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdQuitter.FlatAppearance.BorderSize = 0;
            this.cmdQuitter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdQuitter.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdQuitter.ForeColor = System.Drawing.Color.White;
            this.cmdQuitter.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.cmdQuitter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdQuitter.Location = new System.Drawing.Point(21, 12);
            this.cmdQuitter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdQuitter.Name = "cmdQuitter";
            this.cmdQuitter.Size = new System.Drawing.Size(100, 25);
            this.cmdQuitter.TabIndex = 572;
            this.cmdQuitter.Text = "        Quitter";
            this.cmdQuitter.UseVisualStyleBackColor = false;
            this.cmdQuitter.Visible = false;
            this.cmdQuitter.Click += new System.EventHandler(this.cmdFermer_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.SSDBGrid1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.SSOleDBGrid1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Frame10, 0, 1);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(16, 181);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1281, 520);
            this.tableLayoutPanel1.TabIndex = 573;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 252F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 131F));
            this.tableLayoutPanel2.Controls.Add(this.chkTarifActualiser, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.Label1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.ultraGroupBox1, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.ultraGroupBox2, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.ultraGroupBox3, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.groupBox1, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.ultraGroupBox4, 0, 1);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(19, 12);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 95F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1275, 166);
            this.tableLayoutPanel2.TabIndex = 574;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.ultraGroupBox1.Controls.Add(this.cmdFermer);
            this.ultraGroupBox1.Controls.Add(this.cmdAnnuler);
            this.ultraGroupBox1.Controls.Add(this.cmdQuitter);
            this.ultraGroupBox1.Location = new System.Drawing.Point(1147, 33);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(125, 89);
            this.ultraGroupBox1.TabIndex = 571;
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.ultraGroupBox2.Controls.Add(this.Label435);
            this.ultraGroupBox2.Controls.Add(this.txtFiltreModele);
            this.ultraGroupBox2.Controls.Add(this.cmdRechercheModele0);
            this.ultraGroupBox2.Location = new System.Drawing.Point(3, 128);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(246, 32);
            this.ultraGroupBox2.TabIndex = 572;
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.tableLayoutPanel2.SetColumnSpan(this.ultraGroupBox3, 2);
            this.ultraGroupBox3.Controls.Add(this.Label43);
            this.ultraGroupBox3.Controls.Add(this.txtFiltreLibelleModele);
            this.ultraGroupBox3.Controls.Add(this.cmdRechercheModele1);
            this.ultraGroupBox3.Location = new System.Drawing.Point(255, 128);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(377, 35);
            this.ultraGroupBox3.TabIndex = 572;
            // 
            // ultraGroupBox4
            // 
            this.ultraGroupBox4.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.ultraGroupBox4.Controls.Add(this.cmdAppliquer);
            this.ultraGroupBox4.Controls.Add(this.Option1);
            this.ultraGroupBox4.Controls.Add(this.cmdSauvegarder);
            this.ultraGroupBox4.Location = new System.Drawing.Point(3, 33);
            this.ultraGroupBox4.Name = "ultraGroupBox4";
            this.ultraGroupBox4.Size = new System.Drawing.Size(245, 89);
            this.ultraGroupBox4.TabIndex = 573;
            // 
            // txtFiltreModele
            // 
            this.txtFiltreModele.AccAcceptNumbersOnly = false;
            this.txtFiltreModele.AccAllowComma = false;
            this.txtFiltreModele.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFiltreModele.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFiltreModele.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtFiltreModele.AccHidenValue = "";
            this.txtFiltreModele.AccNotAllowedChars = null;
            this.txtFiltreModele.AccReadOnly = false;
            this.txtFiltreModele.AccReadOnlyAllowDelete = false;
            this.txtFiltreModele.AccRequired = false;
            this.txtFiltreModele.BackColor = System.Drawing.Color.White;
            this.txtFiltreModele.CustomBackColor = System.Drawing.Color.White;
            this.txtFiltreModele.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFiltreModele.ForeColor = System.Drawing.Color.Black;
            this.txtFiltreModele.Location = new System.Drawing.Point(112, 3);
            this.txtFiltreModele.Margin = new System.Windows.Forms.Padding(2);
            this.txtFiltreModele.MaxLength = 32767;
            this.txtFiltreModele.Multiline = false;
            this.txtFiltreModele.Name = "txtFiltreModele";
            this.txtFiltreModele.ReadOnly = false;
            this.txtFiltreModele.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFiltreModele.Size = new System.Drawing.Size(100, 27);
            this.txtFiltreModele.TabIndex = 6;
            this.txtFiltreModele.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFiltreModele.UseSystemPasswordChar = false;
            this.txtFiltreModele.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFiltreModele_KeyPress);
            // 
            // txtFiltreLibelleModele
            // 
            this.txtFiltreLibelleModele.AccAcceptNumbersOnly = false;
            this.txtFiltreLibelleModele.AccAllowComma = false;
            this.txtFiltreLibelleModele.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFiltreLibelleModele.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFiltreLibelleModele.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtFiltreLibelleModele.AccHidenValue = "";
            this.txtFiltreLibelleModele.AccNotAllowedChars = null;
            this.txtFiltreLibelleModele.AccReadOnly = false;
            this.txtFiltreLibelleModele.AccReadOnlyAllowDelete = false;
            this.txtFiltreLibelleModele.AccRequired = false;
            this.txtFiltreLibelleModele.BackColor = System.Drawing.Color.White;
            this.txtFiltreLibelleModele.CustomBackColor = System.Drawing.Color.White;
            this.txtFiltreLibelleModele.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFiltreLibelleModele.ForeColor = System.Drawing.Color.Black;
            this.txtFiltreLibelleModele.Location = new System.Drawing.Point(178, 2);
            this.txtFiltreLibelleModele.Margin = new System.Windows.Forms.Padding(2);
            this.txtFiltreLibelleModele.MaxLength = 32767;
            this.txtFiltreLibelleModele.Multiline = false;
            this.txtFiltreLibelleModele.Name = "txtFiltreLibelleModele";
            this.txtFiltreLibelleModele.ReadOnly = false;
            this.txtFiltreLibelleModele.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFiltreLibelleModele.Size = new System.Drawing.Size(100, 27);
            this.txtFiltreLibelleModele.TabIndex = 5;
            this.txtFiltreLibelleModele.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFiltreLibelleModele.UseSystemPasswordChar = false;
            this.txtFiltreLibelleModele.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtFiltreLibelleModele_KeyPress);
            // 
            // Text1
            // 
            this.Text1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.Text1.AutoWordSelection = false;
            this.Text1.BackColor = System.Drawing.Color.Transparent;
            this.Text1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Text1.ForeColor = System.Drawing.Color.Black;
            this.Text1.Location = new System.Drawing.Point(6, 12);
            this.Text1.Name = "Text1";
            this.Text1.ReadOnly = false;
            this.Text1.Size = new System.Drawing.Size(877, 71);
            this.Text1.TabIndex = 401;
            this.Text1.WordWrap = true;
            // 
            // txtTaux
            // 
            this.txtTaux.AccAcceptNumbersOnly = false;
            this.txtTaux.AccAllowComma = false;
            this.txtTaux.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTaux.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTaux.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtTaux.AccHidenValue = "";
            this.txtTaux.AccNotAllowedChars = null;
            this.txtTaux.AccReadOnly = false;
            this.txtTaux.AccReadOnlyAllowDelete = false;
            this.txtTaux.AccRequired = false;
            this.txtTaux.BackColor = System.Drawing.Color.White;
            this.txtTaux.CustomBackColor = System.Drawing.Color.White;
            this.txtTaux.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTaux.ForeColor = System.Drawing.Color.Black;
            this.txtTaux.Location = new System.Drawing.Point(566, 19);
            this.txtTaux.Margin = new System.Windows.Forms.Padding(2);
            this.txtTaux.MaxLength = 32767;
            this.txtTaux.Multiline = false;
            this.txtTaux.Name = "txtTaux";
            this.txtTaux.ReadOnly = false;
            this.txtTaux.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTaux.Size = new System.Drawing.Size(62, 27);
            this.txtTaux.TabIndex = 9;
            this.txtTaux.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTaux.UseSystemPasswordChar = false;
            // 
            // Text316
            // 
            this.Text316.AccAcceptNumbersOnly = false;
            this.Text316.AccAllowComma = false;
            this.Text316.AccBackgroundColor = System.Drawing.Color.White;
            this.Text316.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text316.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.Text316.AccHidenValue = "";
            this.Text316.AccNotAllowedChars = null;
            this.Text316.AccReadOnly = false;
            this.Text316.AccReadOnlyAllowDelete = false;
            this.Text316.AccRequired = false;
            this.Text316.BackColor = System.Drawing.Color.White;
            this.Text316.CustomBackColor = System.Drawing.Color.White;
            this.Text316.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Text316.ForeColor = System.Drawing.Color.Black;
            this.Text316.Location = new System.Drawing.Point(417, 17);
            this.Text316.Margin = new System.Windows.Forms.Padding(2);
            this.Text316.MaxLength = 32767;
            this.Text316.Multiline = false;
            this.Text316.Name = "Text316";
            this.Text316.ReadOnly = false;
            this.Text316.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text316.Size = new System.Drawing.Size(59, 27);
            this.Text316.TabIndex = 8;
            this.Text316.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text316.UseSystemPasswordChar = false;
            // 
            // Text313
            // 
            this.Text313.AccAcceptNumbersOnly = false;
            this.Text313.AccAllowComma = false;
            this.Text313.AccBackgroundColor = System.Drawing.Color.White;
            this.Text313.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text313.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.Text313.AccHidenValue = "";
            this.Text313.AccNotAllowedChars = null;
            this.Text313.AccReadOnly = false;
            this.Text313.AccReadOnlyAllowDelete = false;
            this.Text313.AccRequired = false;
            this.Text313.BackColor = System.Drawing.Color.White;
            this.Text313.CustomBackColor = System.Drawing.Color.White;
            this.Text313.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Text313.ForeColor = System.Drawing.Color.Black;
            this.Text313.Location = new System.Drawing.Point(237, 17);
            this.Text313.Margin = new System.Windows.Forms.Padding(2);
            this.Text313.MaxLength = 32767;
            this.Text313.Multiline = false;
            this.Text313.Name = "Text313";
            this.Text313.ReadOnly = false;
            this.Text313.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text313.Size = new System.Drawing.Size(59, 27);
            this.Text313.TabIndex = 7;
            this.Text313.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text313.UseSystemPasswordChar = false;
            // 
            // Text315
            // 
            this.Text315.AccAcceptNumbersOnly = false;
            this.Text315.AccAllowComma = false;
            this.Text315.AccBackgroundColor = System.Drawing.Color.White;
            this.Text315.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text315.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.Text315.AccHidenValue = "";
            this.Text315.AccNotAllowedChars = null;
            this.Text315.AccReadOnly = false;
            this.Text315.AccReadOnlyAllowDelete = false;
            this.Text315.AccRequired = false;
            this.Text315.BackColor = System.Drawing.Color.White;
            this.Text315.CustomBackColor = System.Drawing.Color.White;
            this.Text315.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Text315.ForeColor = System.Drawing.Color.Black;
            this.Text315.Location = new System.Drawing.Point(35, 17);
            this.Text315.Margin = new System.Windows.Forms.Padding(2);
            this.Text315.MaxLength = 32767;
            this.Text315.Multiline = false;
            this.Text315.Name = "Text315";
            this.Text315.ReadOnly = false;
            this.Text315.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text315.Size = new System.Drawing.Size(59, 27);
            this.Text315.TabIndex = 6;
            this.Text315.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text315.UseSystemPasswordChar = false;
            // 
            // Text314
            // 
            this.Text314.AccAcceptNumbersOnly = false;
            this.Text314.AccAllowComma = false;
            this.Text314.AccBackgroundColor = System.Drawing.Color.White;
            this.Text314.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text314.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.Text314.AccHidenValue = "";
            this.Text314.AccNotAllowedChars = null;
            this.Text314.AccReadOnly = false;
            this.Text314.AccReadOnlyAllowDelete = false;
            this.Text314.AccRequired = false;
            this.Text314.BackColor = System.Drawing.Color.White;
            this.Text314.CustomBackColor = System.Drawing.Color.White;
            this.Text314.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Text314.ForeColor = System.Drawing.Color.Black;
            this.Text314.Location = new System.Drawing.Point(133, 17);
            this.Text314.Margin = new System.Windows.Forms.Padding(2);
            this.Text314.MaxLength = 32767;
            this.Text314.Multiline = false;
            this.Text314.Name = "Text314";
            this.Text314.ReadOnly = false;
            this.Text314.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text314.Size = new System.Drawing.Size(59, 27);
            this.Text314.TabIndex = 5;
            this.Text314.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text314.UseSystemPasswordChar = false;
            // 
            // frmModele
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1309, 713);
            this.Controls.Add(this.tableLayoutPanel2);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.fraActualiseGecet);
            this.MaximizeBox = false;
            this.MinimumSize = new System.Drawing.Size(685, 512);
            this.Name = "frmModele";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Modèle de devis";
            this.Load += new System.EventHandler(this.frmModele_Load);
            this.groupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.SSDBGrid1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).EndInit();
            this.Frame10.ResumeLayout(false);
            this.Frame10.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ultraGroupBox3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox4)).EndInit();
            this.ultraGroupBox4.ResumeLayout(false);
            this.ultraGroupBox4.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion
        private iTalk.iTalk_TextBox_Small2 txtFiltreLibelleModele;
        private iTalk.iTalk_TextBox_Small2 txtFiltreModele;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton Option1;
        private System.Windows.Forms.Label Label43;
        private System.Windows.Forms.Label Label435;
        private Infragistics.Win.UltraWinGrid.UltraGrid SSDBGrid1;
        private Infragistics.Win.UltraWinGrid.UltraGrid SSOleDBGrid1;
        private System.Windows.Forms.Label Label1;
        private System.Windows.Forms.GroupBox Frame10;
        private iTalk.iTalk_TextBox_Small2 txtTaux;
        private iTalk.iTalk_TextBox_Small2 Text316;
        private iTalk.iTalk_TextBox_Small2 Text313;
        private iTalk.iTalk_TextBox_Small2 Text315;
        private iTalk.iTalk_TextBox_Small2 Text314;
        private System.Windows.Forms.Label Label2142;
        private System.Windows.Forms.Label Label2127;
        private System.Windows.Forms.Label Label2130;
        private System.Windows.Forms.Label Label2128;
        private System.Windows.Forms.Label Label21;
        public System.Windows.Forms.Button cmdSauvegarder;
        public System.Windows.Forms.Button cmdFermer;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button cmdAppliquer;
        private iTalk.iTalk_RichTextBox Text1;
        public System.Windows.Forms.Button cmdRechercheModele0;
        public System.Windows.Forms.Button cmdRechercheModele1;
        private System.Windows.Forms.CheckBox chkTarifActualiser;
        private System.Windows.Forms.Panel fraActualiseGecet;
        private System.Windows.Forms.Timer Timer1;
        public System.Windows.Forms.Button cmdQuitter;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox4;
    }
}