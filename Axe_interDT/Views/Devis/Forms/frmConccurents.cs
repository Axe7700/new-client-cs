﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmConccurents : Form
    {
        public frmConccurents()
        {
            InitializeComponent();
        }

        private ModAdo ModAdo;
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmConccurents_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            string sSQL = "SELECT CNC_Code, CNC_Libelle FROM CNC_Conccurent";
            if (ssDropconcurrent.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(ssDropconcurrent, sSQL, "CNC_Code", true);
            }
            fc_Load();
        }
        /// <summary>
        /// Tested
        /// </summary>
        public void fc_Load()
        {

            string sSQL = "SELECT  DEC_NoAuto,DEC_Code, DEC_Libelle,NumeroDevis"
                + " FROM DEC_DevisConccurent";

            var rsCNC = new DataTable();
            ModAdo = new ModAdo();
            rsCNC = ModAdo.fc_OpenRecordSet(sSQL);
            ssgCNC_Conccurent.DataSource = rsCNC;
            ssgCNC_Conccurent.UpdateData();
          
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssgCNC_Conccurent_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            string sCode;
            if (ssgCNC_Conccurent.ActiveRow.Cells["DEC_Code"].Column.Index == ssgCNC_Conccurent.ActiveCell.Column.Index)
            {
                sCode = General.fc_FormatCode(ssgCNC_Conccurent.ActiveRow.Cells["DEC_Code"].Text + "");
                if (string.IsNullOrEmpty(sCode))
                    e.Cancel = true;
                else
                    ssgCNC_Conccurent.ActiveRow.Cells["DEC_Code"].Value = sCode;
            }
            if (string.IsNullOrEmpty(ssgCNC_Conccurent.ActiveRow.Cells["NumeroDevis"].Text.ToString()))
            {
                ssgCNC_Conccurent.ActiveRow.Cells["NumeroDevis"].Value = txtNoDevis.Text;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssgCNC_Conccurent_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {

            ModAdo.Update();
            fc_Load();
        
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssgCNC_Conccurent_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            ssgCNC_Conccurent.DisplayLayout.Bands[0].Columns["DEC_Code"].Header.Caption = "Code";
            ssgCNC_Conccurent.DisplayLayout.Bands[0].Columns["DEC_Libelle"].Header.Caption = "Libelle";
            ssgCNC_Conccurent.DisplayLayout.Bands[0].Columns["DEC_Code"].ValueList = ssDropconcurrent;
            ssgCNC_Conccurent.DisplayLayout.Bands[0].Columns["NumeroDevis"].Hidden = true;
            ssgCNC_Conccurent.DisplayLayout.Bands[0].Columns["DEC_NoAuto"].Hidden = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssgCNC_Conccurent_AfterRowsDeleted(object sender, EventArgs e)
        {
            ModAdo.Update();
            fc_Load();
           
        }

     
    }
}
