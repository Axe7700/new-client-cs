﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmCopie : Form
    {
        public frmCopie()
        {
            InitializeComponent();
        }

        private void btnValider_Click(object sender, EventArgs e)
        {
            int i = 0;
            int j = 0;
            int nbSelect = 0;

            ModCourrier.list = "";

            for (i = 0; i <= lstCopier.Items.Count - 1; i++)
            {
                if (lstCopier.GetSelected(i))
                {
                    j = j + 1;
                }
            }

            for (i = 0; i <= lstCopier.Items.Count - 1; i++)
            {
                if (lstCopier.GetSelected(i))
                {
                    nbSelect = nbSelect + 1;
                    if (nbSelect < j)
                    {
                        ModCourrier.list = ModCourrier.list + lstCopier.Items[i] + ";";
                    }
                    else
                    {
                        ModCourrier.list = ModCourrier.list + lstCopier.Items[i] + ";";
                    }
                }
            }

            this.Close();
        }

        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            Close();
        }
        public bool initListeOutlook(string Dossier)
        {
            bool functionReturnValue = false;

            short Index = 0;
            Redemption.AddressLists rals = new Redemption.AddressLists();
            Redemption.AddressList ral = null;
            string s = null;

            try
            {
                ral = rals.Item(Dossier);
                s = Convert.ToString(ral.AddressEntries.Count);

                for (Index = 1; Index <= ral.AddressEntries.Count; Index++)
                {
                    lstCopier.Items.Add(ral.AddressEntries[Index].Name);
                }

                rals = null;
                ral = null;


                functionReturnValue = true;
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Program.SaveException(e);

                rals = null;
                ral = null;
                functionReturnValue = false;

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Impossible de charger les adresses du dossier : " + Dossier, "Adresses du dossier " + Dossier + " inaccessible", MessageBoxButtons.OK, MessageBoxIcon.Error);
                return functionReturnValue;
            }
        }


        public object initListeCopie()
        {
            int x = 0;

            short i = 0;
            //===Efface les éléments de la liste des destinataires mis en copie.
            for (i = 0; i <= this.lstCopier.Items.Count - 1; i++)
            {
                this.lstCopier.Items.RemoveAt((0));
            }

            ModCourrier.list = "";

            //SQL = "Select UserInitial,UserEmail " _
            //'    & "From uemails " _
            //'    & "ORDER BY UserInitial ASC"
            //
            //Set adotemp = New ADODB.Recordset
            //adotemp.Open SQL, adocnn
            //
            //If Not (adotemp.EOF) Then
            //    adotemp.MoveFirst
            //    While Not adotemp.EOF
            //        lstCopier.AddItem adotemp!UserEmail
            //        adotemp.MoveNext
            //    Wend
            //    adotemp.Close
            //End If

            //For i = 1 To UBound(MesDossiers)
            //    If initListeOutlook(MesDossiers(i)) = False Then Exit For
            //Next i



            //If sRDOmail = "1" Then
            //    InitAdressListOutlookRDO
            //Else
            //    InitAdressListOutlook
            //End If

            if (lstCopier.Items.Count == 0)
            {
                for (x = 0; x <= ModCourrier.sTabAdresseMail.Length - 1; x++)
                {
                    lstCopier.Items.Add(ModCourrier.sTabAdresseMail[x].sNom);
                }
            }
            return null;
        }

        private void frmCopie_Load(object sender, EventArgs e)
        {
            initListeCopie();
        }
    }
}
