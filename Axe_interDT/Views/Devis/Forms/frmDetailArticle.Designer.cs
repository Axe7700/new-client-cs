﻿namespace Axe_interDT.Views.Devis.Forms
{
    partial class frmDetailArticle
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.label6 = new System.Windows.Forms.Label();
            this.lblChrono = new System.Windows.Forms.Label();
            this.lblLibelle = new System.Windows.Forms.Label();
            this.lblFamille = new System.Windows.Forms.Label();
            this.lblFournisseur0 = new System.Windows.Forms.Label();
            this.lblRefFourn = new System.Windows.Forms.Label();
            this.lblMarque = new System.Windows.Forms.Label();
            this.lblRefFab = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.lblDateMAJ = new System.Windows.Forms.Label();
            this.lblFournisseur1 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.lblFraisPort = new System.Windows.Forms.Label();
            this.lblDelaiLiv = new System.Windows.Forms.Label();
            this.lblContact = new System.Windows.Forms.Label();
            this.lblAgence = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.label24 = new System.Windows.Forms.Label();
            this.label25 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.lblCorpsEtat = new System.Windows.Forms.Label();
            this.lblUnite = new System.Windows.Forms.Label();
            this.lblNbUnite = new System.Windows.Forms.Label();
            this.lblConditionnement = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label20 = new System.Windows.Forms.Label();
            this.lblCodeParent = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.lblPrixVente = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.lblPrixStock = new System.Windows.Forms.Label();
            this.lblPrixPublic = new System.Windows.Forms.Label();
            this.lblPrixDevis = new System.Windows.Forms.Label();
            this.lblPrixAchat = new System.Windows.Forms.Label();
            this.label28 = new System.Windows.Forms.Label();
            this.label29 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.cmdFermer = new System.Windows.Forms.Button();
            this.txtNoAuto = new System.Windows.Forms.TextBox();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(10, 45);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(53, 17);
            this.label33.TabIndex = 384;
            this.label33.Text = "Chrono";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(10, 65);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(50, 17);
            this.label1.TabIndex = 385;
            this.label1.Text = "Libellé";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 101);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(51, 17);
            this.label2.TabIndex = 386;
            this.label2.Text = "Famille";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(10, 121);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(77, 17);
            this.label3.TabIndex = 387;
            this.label3.Text = "Fournisseur";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(10, 141);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(66, 17);
            this.label4.TabIndex = 388;
            this.label4.Text = "Réf. fourn";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(10, 161);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(54, 17);
            this.label5.TabIndex = 389;
            this.label5.Text = "Marque";
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(10, 181);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(55, 17);
            this.label6.TabIndex = 390;
            this.label6.Text = "Réf. Fab";
            // 
            // lblChrono
            // 
            this.lblChrono.AutoSize = true;
            this.lblChrono.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblChrono.ForeColor = System.Drawing.Color.Blue;
            this.lblChrono.Location = new System.Drawing.Point(129, 45);
            this.lblChrono.Name = "lblChrono";
            this.lblChrono.Size = new System.Drawing.Size(69, 17);
            this.lblChrono.TabIndex = 391;
            this.lblChrono.Text = "lblChrono";
            // 
            // lblLibelle
            // 
            this.lblLibelle.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLibelle.ForeColor = System.Drawing.Color.Blue;
            this.lblLibelle.Location = new System.Drawing.Point(129, 65);
            this.lblLibelle.Name = "lblLibelle";
            this.lblLibelle.Size = new System.Drawing.Size(280, 33);
            this.lblLibelle.TabIndex = 392;
            this.lblLibelle.Text = "lblLibelle";
            // 
            // lblFamille
            // 
            this.lblFamille.AutoSize = true;
            this.lblFamille.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFamille.ForeColor = System.Drawing.Color.Blue;
            this.lblFamille.Location = new System.Drawing.Point(129, 101);
            this.lblFamille.Name = "lblFamille";
            this.lblFamille.Size = new System.Drawing.Size(67, 17);
            this.lblFamille.TabIndex = 393;
            this.lblFamille.Text = "lblFamille";
            // 
            // lblFournisseur0
            // 
            this.lblFournisseur0.AutoSize = true;
            this.lblFournisseur0.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFournisseur0.ForeColor = System.Drawing.Color.Blue;
            this.lblFournisseur0.Location = new System.Drawing.Point(129, 121);
            this.lblFournisseur0.Name = "lblFournisseur0";
            this.lblFournisseur0.Size = new System.Drawing.Size(93, 17);
            this.lblFournisseur0.TabIndex = 394;
            this.lblFournisseur0.Text = "lblFournisseur";
            // 
            // lblRefFourn
            // 
            this.lblRefFourn.AutoSize = true;
            this.lblRefFourn.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRefFourn.ForeColor = System.Drawing.Color.Blue;
            this.lblRefFourn.Location = new System.Drawing.Point(129, 141);
            this.lblRefFourn.Name = "lblRefFourn";
            this.lblRefFourn.Size = new System.Drawing.Size(79, 17);
            this.lblRefFourn.TabIndex = 395;
            this.lblRefFourn.Text = "lblRefFourn";
            // 
            // lblMarque
            // 
            this.lblMarque.AutoSize = true;
            this.lblMarque.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblMarque.ForeColor = System.Drawing.Color.Blue;
            this.lblMarque.Location = new System.Drawing.Point(129, 161);
            this.lblMarque.Name = "lblMarque";
            this.lblMarque.Size = new System.Drawing.Size(70, 17);
            this.lblMarque.TabIndex = 396;
            this.lblMarque.Text = "lblMarque";
            // 
            // lblRefFab
            // 
            this.lblRefFab.AutoSize = true;
            this.lblRefFab.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblRefFab.ForeColor = System.Drawing.Color.Blue;
            this.lblRefFab.Location = new System.Drawing.Point(129, 181);
            this.lblRefFab.Name = "lblRefFab";
            this.lblRefFab.Size = new System.Drawing.Size(66, 17);
            this.lblRefFab.TabIndex = 397;
            this.lblRefFab.Text = "lblRefFab";
            // 
            // panel1
            // 
            this.panel1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel1.Location = new System.Drawing.Point(1, 201);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(450, 1);
            this.panel1.TabIndex = 398;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(8, 9);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(142, 26);
            this.label7.TabIndex = 399;
            this.label7.Text = "Identification";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(195, 18);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(124, 17);
            this.label8.TabIndex = 400;
            this.label8.Text = "Date de mise à jour";
            // 
            // lblDateMAJ
            // 
            this.lblDateMAJ.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateMAJ.ForeColor = System.Drawing.Color.Blue;
            this.lblDateMAJ.Location = new System.Drawing.Point(325, 19);
            this.lblDateMAJ.Name = "lblDateMAJ";
            this.lblDateMAJ.Size = new System.Drawing.Size(96, 17);
            this.lblDateMAJ.TabIndex = 401;
            this.lblDateMAJ.Text = "lblDateMAJ";
            // 
            // lblFournisseur1
            // 
            this.lblFournisseur1.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFournisseur1.ForeColor = System.Drawing.Color.Blue;
            this.lblFournisseur1.Location = new System.Drawing.Point(248, 220);
            this.lblFournisseur1.Name = "lblFournisseur1";
            this.lblFournisseur1.Size = new System.Drawing.Size(96, 17);
            this.lblFournisseur1.TabIndex = 419;
            this.lblFournisseur1.Text = "lblFournisseur";
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Ubuntu", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(8, 211);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(212, 26);
            this.label11.TabIndex = 417;
            this.label11.Text = "Données Fournisseur";
            // 
            // panel2
            // 
            this.panel2.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel2.Location = new System.Drawing.Point(1, 327);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(450, 1);
            this.panel2.TabIndex = 416;
            // 
            // lblFraisPort
            // 
            this.lblFraisPort.AutoSize = true;
            this.lblFraisPort.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFraisPort.ForeColor = System.Drawing.Color.Blue;
            this.lblFraisPort.Location = new System.Drawing.Point(129, 307);
            this.lblFraisPort.Name = "lblFraisPort";
            this.lblFraisPort.Size = new System.Drawing.Size(79, 17);
            this.lblFraisPort.TabIndex = 412;
            this.lblFraisPort.Text = "lblFraisPort";
            // 
            // lblDelaiLiv
            // 
            this.lblDelaiLiv.AutoSize = true;
            this.lblDelaiLiv.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDelaiLiv.ForeColor = System.Drawing.Color.Blue;
            this.lblDelaiLiv.Location = new System.Drawing.Point(129, 287);
            this.lblDelaiLiv.Name = "lblDelaiLiv";
            this.lblDelaiLiv.Size = new System.Drawing.Size(72, 17);
            this.lblDelaiLiv.TabIndex = 411;
            this.lblDelaiLiv.Text = "lblDelaiLiv";
            // 
            // lblContact
            // 
            this.lblContact.AutoSize = true;
            this.lblContact.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblContact.ForeColor = System.Drawing.Color.Blue;
            this.lblContact.Location = new System.Drawing.Point(129, 267);
            this.lblContact.Name = "lblContact";
            this.lblContact.Size = new System.Drawing.Size(74, 17);
            this.lblContact.TabIndex = 410;
            this.lblContact.Text = "lblContact";
            // 
            // lblAgence
            // 
            this.lblAgence.AutoSize = true;
            this.lblAgence.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblAgence.ForeColor = System.Drawing.Color.Blue;
            this.lblAgence.Location = new System.Drawing.Point(129, 247);
            this.lblAgence.Name = "lblAgence";
            this.lblAgence.Size = new System.Drawing.Size(71, 17);
            this.lblAgence.TabIndex = 409;
            this.lblAgence.Text = "lblAgence";
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(10, 307);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(85, 17);
            this.label22.TabIndex = 405;
            this.label22.Text = "Frais de Port";
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(10, 287);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(62, 17);
            this.label23.TabIndex = 404;
            this.label23.Text = "Délai Liv.";
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(10, 267);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(58, 17);
            this.label24.TabIndex = 403;
            this.label24.Text = "Contact";
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(10, 247);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(55, 17);
            this.label25.TabIndex = 402;
            this.label25.Text = "Agence";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Ubuntu", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(8, 342);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(100, 26);
            this.label10.TabIndex = 429;
            this.label10.Text = "Stockage";
            // 
            // panel3
            // 
            this.panel3.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.panel3.Location = new System.Drawing.Point(1, 478);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(450, 1);
            this.panel3.TabIndex = 428;
            // 
            // lblCorpsEtat
            // 
            this.lblCorpsEtat.AutoSize = true;
            this.lblCorpsEtat.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCorpsEtat.ForeColor = System.Drawing.Color.Blue;
            this.lblCorpsEtat.Location = new System.Drawing.Point(129, 438);
            this.lblCorpsEtat.Name = "lblCorpsEtat";
            this.lblCorpsEtat.Size = new System.Drawing.Size(86, 17);
            this.lblCorpsEtat.TabIndex = 427;
            this.lblCorpsEtat.Text = "lblCorpsEtat";
            // 
            // lblUnite
            // 
            this.lblUnite.AutoSize = true;
            this.lblUnite.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUnite.ForeColor = System.Drawing.Color.Blue;
            this.lblUnite.Location = new System.Drawing.Point(129, 418);
            this.lblUnite.Name = "lblUnite";
            this.lblUnite.Size = new System.Drawing.Size(57, 17);
            this.lblUnite.TabIndex = 426;
            this.lblUnite.Text = "lblUnite";
            // 
            // lblNbUnite
            // 
            this.lblNbUnite.AutoSize = true;
            this.lblNbUnite.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNbUnite.ForeColor = System.Drawing.Color.Blue;
            this.lblNbUnite.Location = new System.Drawing.Point(129, 398);
            this.lblNbUnite.Name = "lblNbUnite";
            this.lblNbUnite.Size = new System.Drawing.Size(74, 17);
            this.lblNbUnite.TabIndex = 425;
            this.lblNbUnite.Text = "lblNbUnite";
            // 
            // lblConditionnement
            // 
            this.lblConditionnement.AutoSize = true;
            this.lblConditionnement.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblConditionnement.ForeColor = System.Drawing.Color.Blue;
            this.lblConditionnement.Location = new System.Drawing.Point(129, 378);
            this.lblConditionnement.Name = "lblConditionnement";
            this.lblConditionnement.Size = new System.Drawing.Size(130, 17);
            this.lblConditionnement.TabIndex = 424;
            this.lblConditionnement.Text = "lblConditionnement";
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(10, 438);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(85, 17);
            this.label17.TabIndex = 423;
            this.label17.Text = "Corps d\'état";
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(10, 418);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(41, 17);
            this.label18.TabIndex = 422;
            this.label18.Text = "Unité";
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(10, 398);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(76, 17);
            this.label19.TabIndex = 421;
            this.label19.Text = "Nb à l\'unité";
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(10, 378);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(114, 17);
            this.label20.TabIndex = 420;
            this.label20.Text = "Conditionnement";
            // 
            // lblCodeParent
            // 
            this.lblCodeParent.AutoSize = true;
            this.lblCodeParent.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodeParent.ForeColor = System.Drawing.Color.Blue;
            this.lblCodeParent.Location = new System.Drawing.Point(131, 458);
            this.lblCodeParent.Name = "lblCodeParent";
            this.lblCodeParent.Size = new System.Drawing.Size(98, 17);
            this.lblCodeParent.TabIndex = 431;
            this.lblCodeParent.Text = "lblCodeParent";
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(10, 458);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(85, 17);
            this.label12.TabIndex = 430;
            this.label12.Text = "Code parent";
            // 
            // lblPrixVente
            // 
            this.lblPrixVente.AutoSize = true;
            this.lblPrixVente.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrixVente.ForeColor = System.Drawing.Color.Blue;
            this.lblPrixVente.Location = new System.Drawing.Point(133, 611);
            this.lblPrixVente.Name = "lblPrixVente";
            this.lblPrixVente.Size = new System.Drawing.Size(84, 17);
            this.lblPrixVente.TabIndex = 443;
            this.lblPrixVente.Text = "lblPrixVente";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(14, 611);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(89, 17);
            this.label13.TabIndex = 442;
            this.label13.Text = "Prix de vente";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Ubuntu", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(12, 495);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(50, 26);
            this.label14.TabIndex = 441;
            this.label14.Text = "Prix";
            // 
            // lblPrixStock
            // 
            this.lblPrixStock.AutoSize = true;
            this.lblPrixStock.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrixStock.ForeColor = System.Drawing.Color.Blue;
            this.lblPrixStock.Location = new System.Drawing.Point(133, 591);
            this.lblPrixStock.Name = "lblPrixStock";
            this.lblPrixStock.Size = new System.Drawing.Size(82, 17);
            this.lblPrixStock.TabIndex = 439;
            this.lblPrixStock.Text = "lblPrixStock";
            // 
            // lblPrixPublic
            // 
            this.lblPrixPublic.AutoSize = true;
            this.lblPrixPublic.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrixPublic.ForeColor = System.Drawing.Color.Blue;
            this.lblPrixPublic.Location = new System.Drawing.Point(133, 571);
            this.lblPrixPublic.Name = "lblPrixPublic";
            this.lblPrixPublic.Size = new System.Drawing.Size(84, 17);
            this.lblPrixPublic.TabIndex = 438;
            this.lblPrixPublic.Text = "lblPrixPublic";
            // 
            // lblPrixDevis
            // 
            this.lblPrixDevis.AutoSize = true;
            this.lblPrixDevis.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrixDevis.ForeColor = System.Drawing.Color.Blue;
            this.lblPrixDevis.Location = new System.Drawing.Point(133, 551);
            this.lblPrixDevis.Name = "lblPrixDevis";
            this.lblPrixDevis.Size = new System.Drawing.Size(80, 17);
            this.lblPrixDevis.TabIndex = 437;
            this.lblPrixDevis.Text = "lblPrixDevis";
            // 
            // lblPrixAchat
            // 
            this.lblPrixAchat.AutoSize = true;
            this.lblPrixAchat.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblPrixAchat.ForeColor = System.Drawing.Color.Blue;
            this.lblPrixAchat.Location = new System.Drawing.Point(133, 531);
            this.lblPrixAchat.Name = "lblPrixAchat";
            this.lblPrixAchat.Size = new System.Drawing.Size(84, 17);
            this.lblPrixAchat.TabIndex = 436;
            this.lblPrixAchat.Text = "lblPrixAchat";
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(14, 591);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(87, 17);
            this.label28.TabIndex = 435;
            this.label28.Text = "Prix de stock";
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(14, 571);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(71, 17);
            this.label29.TabIndex = 434;
            this.label29.Text = "Prix public";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(14, 551);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(66, 17);
            this.label30.TabIndex = 433;
            this.label30.Text = "Prix devis";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(14, 531);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(80, 17);
            this.label31.TabIndex = 432;
            this.label31.Text = "Prix d\'achat";
            // 
            // cmdFermer
            // 
            this.cmdFermer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFermer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdFermer.FlatAppearance.BorderSize = 0;
            this.cmdFermer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFermer.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdFermer.ForeColor = System.Drawing.Color.White;
            this.cmdFermer.Image = global::Axe_interDT.Properties.Resources.Ok_16x16;
            this.cmdFermer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdFermer.Location = new System.Drawing.Point(163, 635);
            this.cmdFermer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdFermer.Name = "cmdFermer";
            this.cmdFermer.Size = new System.Drawing.Size(85, 35);
            this.cmdFermer.TabIndex = 444;
            this.cmdFermer.Text = "     Fermer";
            this.cmdFermer.UseVisualStyleBackColor = false;
            this.cmdFermer.Click += new System.EventHandler(this.cmdFermer_Click);
            // 
            // txtNoAuto
            // 
            this.txtNoAuto.Location = new System.Drawing.Point(17, 650);
            this.txtNoAuto.Name = "txtNoAuto";
            this.txtNoAuto.Size = new System.Drawing.Size(100, 20);
            this.txtNoAuto.TabIndex = 445;
            this.txtNoAuto.Visible = false;
            // 
            // frmDetailArticle
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(421, 678);
            this.Controls.Add(this.txtNoAuto);
            this.Controls.Add(this.cmdFermer);
            this.Controls.Add(this.lblPrixVente);
            this.Controls.Add(this.label13);
            this.Controls.Add(this.label14);
            this.Controls.Add(this.lblPrixStock);
            this.Controls.Add(this.lblPrixPublic);
            this.Controls.Add(this.lblPrixDevis);
            this.Controls.Add(this.lblPrixAchat);
            this.Controls.Add(this.label28);
            this.Controls.Add(this.label29);
            this.Controls.Add(this.label30);
            this.Controls.Add(this.label31);
            this.Controls.Add(this.lblCodeParent);
            this.Controls.Add(this.label12);
            this.Controls.Add(this.label10);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.lblCorpsEtat);
            this.Controls.Add(this.lblUnite);
            this.Controls.Add(this.lblNbUnite);
            this.Controls.Add(this.lblConditionnement);
            this.Controls.Add(this.label17);
            this.Controls.Add(this.label18);
            this.Controls.Add(this.label19);
            this.Controls.Add(this.label20);
            this.Controls.Add(this.lblFournisseur1);
            this.Controls.Add(this.label11);
            this.Controls.Add(this.panel2);
            this.Controls.Add(this.lblFraisPort);
            this.Controls.Add(this.lblDelaiLiv);
            this.Controls.Add(this.lblContact);
            this.Controls.Add(this.lblAgence);
            this.Controls.Add(this.label22);
            this.Controls.Add(this.label23);
            this.Controls.Add(this.label24);
            this.Controls.Add(this.label25);
            this.Controls.Add(this.lblDateMAJ);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.lblRefFab);
            this.Controls.Add(this.lblMarque);
            this.Controls.Add(this.lblRefFourn);
            this.Controls.Add(this.lblFournisseur0);
            this.Controls.Add(this.lblFamille);
            this.Controls.Add(this.lblLibelle);
            this.Controls.Add(this.lblChrono);
            this.Controls.Add(this.label6);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label33);
            this.MaximizeBox = false;
            this.Name = "frmDetailArticle";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Fiche signalétique d\'un article";
            this.Activated += new System.EventHandler(this.frmDetailArticle_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.Label lblChrono;
        private System.Windows.Forms.Label lblLibelle;
        private System.Windows.Forms.Label lblFamille;
        private System.Windows.Forms.Label lblFournisseur0;
        private System.Windows.Forms.Label lblRefFourn;
        private System.Windows.Forms.Label lblMarque;
        private System.Windows.Forms.Label lblRefFab;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.Label lblDateMAJ;
        private System.Windows.Forms.Label lblFournisseur1;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label lblFraisPort;
        private System.Windows.Forms.Label lblDelaiLiv;
        private System.Windows.Forms.Label lblContact;
        private System.Windows.Forms.Label lblAgence;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblCorpsEtat;
        private System.Windows.Forms.Label lblUnite;
        private System.Windows.Forms.Label lblNbUnite;
        private System.Windows.Forms.Label lblConditionnement;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.Label lblCodeParent;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label lblPrixVente;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label lblPrixStock;
        private System.Windows.Forms.Label lblPrixPublic;
        private System.Windows.Forms.Label lblPrixDevis;
        private System.Windows.Forms.Label lblPrixAchat;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.Label label30;
        private System.Windows.Forms.Label label31;
        public System.Windows.Forms.Button cmdFermer;
        public System.Windows.Forms.TextBox txtNoAuto;
    }
}