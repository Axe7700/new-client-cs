﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmDetailArticle : Form
    {
        public frmDetailArticle()
        {
            InitializeComponent();
        }

        private void cmdFermer_Click(object sender, EventArgs e)
        {
            this.Close();
        }
/// <summary>
/// tested
/// </summary>
/// <param name="sender"></param>
/// <param name="e"></param>
        private void frmDetailArticle_Activated(object sender, EventArgs e)
        {
            DataTable rsArtGecet = default(DataTable);
            SqlDataAdapter SDArsArtGecet = null;

            ModParametre.fc_OpenConnGecet();
            rsArtGecet = new DataTable();
            SDArsArtGecet = new SqlDataAdapter("SELECT Dtiarti.*,Dtiprix.* FROM Dtiarti INNER JOIN Dtiprix ON Dtiarti.Chrono=Dtiprix.Chrono WHERE Dtiprix.NoAuto=" + txtNoAuto.Text, ModParametre.adoGecet);
            SDArsArtGecet.Fill(rsArtGecet);
            if (rsArtGecet.Rows.Count > 0)
            {
                var _with1 = rsArtGecet;
                lblDateMAJ.Text = _with1.Rows[0]["Dapplication"] + "";
                lblChrono.Text = _with1.Rows[0]["Chrono"] + "";
                lblLibelle.Text = _with1.Rows[0]["Libelle"] + "";
                lblFamille.Text = _with1.Rows[0]["sFam"] + "";
                lblFournisseur1.Text = _with1.Rows[0]["Fournisseur"] + "";
                lblFournisseur0.Text = _with1.Rows[0]["Fournisseur"] + "";
                lblRefFourn.Text = _with1.Rows[0]["fourref"] + "";
                lblMarque.Text = _with1.Rows[0]["Marque"] + "";
                lblRefFab.Text = _with1.Rows[0]["fabref"] + "";
                lblAgence.Text = "";
                lblContact.Text = "";
                lblDelaiLiv.Text = "";
                lblFraisPort.Text = "";
                lblConditionnement.Text = "";
                lblNbUnite.Text = "";
                lblUnite.Text = _with1.Rows[0]["fouunite"] + "";
                lblCorpsEtat.Text = "";
                lblCodeParent.Text = "";
                lblPrixAchat.Text = _with1.Rows[0]["prix net achat"] + "";
                lblPrixDevis.Text = "";
                lblPrixPublic.Text = "";
                lblPrixStock.Text = "";
                lblPrixVente.Text = "";
            }
            rsArtGecet?.Dispose();
            SDArsArtGecet?.Dispose();
        }
    }
}
