﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using System;
using System.Collections.Generic;
using System.Data;
using System.Windows.Forms;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmModele : Form
    {
        private DataTable data1 = new DataTable();
        private ModAdo modAdodata1 = new ModAdo();
        private DataTable Data2 = new DataTable();
        private ModAdo modAdoData2 = new ModAdo();

        //TODO ==> Ask Mondir about properties of grid (added ,delete and modif) 
        //in vb6 when I added a ligne in grid (detail du modele séléctionné) I have an error
        public frmModele()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            General.BoolAnnuler = true;
            this.Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmModele_Load(object sender, EventArgs e)
        {
            try
            {
                View.Theme.Theme.recursiveLoopOnFrms(this);

                Text1.Text = "";
                Label1.Text = Label1.Text + " " + General.gNumeroDevis;
                General.BoolAnnuler = false;
                General.strModele = new string[1];
                //With Label2

                //  For i = 0 To .Count - 1
                //      .Item(i).Visible = False
                //      .Item(i).Caption = ""
                //      '.Item(i).BackColor = H800000000F
                //      .Item(i).Left = 60
                //      .Item(i).Top = 360
                //      .Item(i).Height = 7035
                //     .Item(i).Width = 10335
                //      .Item(i).ZOrder (1)
                //  Next
                //End With

                General.SQL = "SELECT * FROM DevisModeles order by ModeleDevis";

                modAdodata1 = new ModAdo();
                data1 = modAdodata1.fc_OpenRecordSet(General.SQL);
                SSDBGrid1.DataSource = data1;

                if (SSDBGrid1.Rows.Count == 0)
                    return;

                General.SQL = "SELECT * FROM DevisModelesDetail " + "WHERE ModeleDevis='" + SSDBGrid1.Rows[0].Cells[0].Value + "' " + "ORDER BY NumeroLigne";

                modAdoData2 = new ModAdo();
                Data2 = modAdoData2.fc_OpenRecordSet(General.SQL);
                SSOleDBGrid1.DataSource = Data2;

                Text315.Text = Convert.ToString(General.MO);
                Text314.Text = Convert.ToString(General.FO);
                Text313.Text = Convert.ToString(General.sT);
                Text316.Text = Convert.ToString(General.PxMO);
                txtTaux.Text = Convert.ToString(General.TVADefaut);

                General.sTarifGecetVisible = General.gfr_liaison("TarifGecetVisible");

                General.sTarifGecetValeurDefaut = General.gfr_liaison("TarifGecetValeurDefaut");

                General.bPrixGecetAActualiser = false;

                if (General.sTarifGecetVisible == "1")
                {
                    fraActualiseGecet.Visible = true;
                    if (General.sTarifGecetValeurDefaut == "1")
                    {
                        chkTarifActualiser.CheckState = CheckState.Checked;

                    }
                    else
                    {
                        chkTarifActualiser.CheckState = CheckState.Unchecked;
                    }
                }
                else
                {
                    fraActualiseGecet.Visible = false;
                }

            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, "frmModele ; Form_Load");
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFermer_Click(object sender, EventArgs e)
        {

            General.SelectModele = true;
            // Récupétration des nouveaux coefficients afin de l'appliquer au modèle sélectionné
            General.MO = Convert.ToDouble(Text315.Text);
            General.FO = Convert.ToDouble(Text314.Text);
            General.sT = Convert.ToDouble(Text313.Text);
            General.PxMO = Convert.ToDouble(Text316.Text);
            General.TVADefaut = Convert.ToDouble(txtTaux.Text);

            if (General.sTarifGecetVisible == "1" && chkTarifActualiser.CheckState == CheckState.Unchecked)
            {
                General.bPrixGecetAActualiser = false;
            }
            else if (General.sTarifGecetVisible == "1" && (int)chkTarifActualiser.CheckState == 1)
            {
                General.bPrixGecetAActualiser = true;
            }

            this.Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAppliquer_Click(object sender, EventArgs e)
        {

            //---stocke les numeros de devis pour pouvoir les inserer dans
            //---le corps du devis en cours.


            if (SSDBGrid1.ActiveRow == null)
                return;

            if (General.strModele.Length > 0)
            {
                Array.Resize(ref General.strModele, General.strModele.Length + 2);
                General.strModele[General.strModele.Length - 1] = SSDBGrid1.ActiveRow.Cells[0].Value.ToString();
                Text1.Text = Text1.Text + "\n" + SSDBGrid1.ActiveRow.Cells[0].Value;
            }
            else
            {
                General.strModele[General.strModele.Length - 1] = SSDBGrid1.ActiveRow.Cells[0].Value.ToString();
                Text1.Text = SSDBGrid1.ActiveRow.Cells[0].Value.ToString();
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSauvegarder_Click(object sender, EventArgs e)
        {
            string sNomModèles = "";
            string sLibellé = "";
            try
            {
                sNomModèles = Microsoft.VisualBasic.Interaction.InputBox("Nom du modèle (obligatoire)", "Modèle");
                sLibellé = Microsoft.VisualBasic.Interaction.InputBox("Libellé du modèle", "Modèle");
                Cursor.Current = Cursors.WaitCursor;
                if (sNomModèles != "")
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM DevisModeles WHERE ModeleDevis= '{sNomModèles}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce Nom du modèle éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }
                    }
                    String SQL = "INSERT INTO DevisModeles (ModeleDevis,Libelle) VALUES ('" + sNomModèles + "','" + sLibellé + "')";
                    General.Execute(SQL);
                    SQL = "INSERT INTO DevisModelesDetail (CodeArticle, CodeFamille,"
                 + " CodeSousFamille, CodeTVA, Coef1, Coef2, Coef3, DateFacture, Facturer,"
                 + " FO, FOd, FOud, kFO, kMO, kST, MO, MOd, MOhd, MOhud, NArticle, NFamille,"
                 + " NSFamille, NumeroLigne, NumOrdre, PxHeured, Quantite, ST, STd, STud,"
                 + " TexteLigne, TotalDebours, TotalVente, ModeleDevis,codesousarticle, fournisseur)"
                 + " SELECT CodeArticle,"
                 + " CodeFamille, CodeSousFamille, CodeTVA, Coef1, Coef2, Coef3, DateFacture,"
                 + " Facturer, FO, FOd, FOud, kFO, kMO, kST, MO, [DevisDetail].[MOd], MOhd, MOhud, NArticle,"
                 + " NFamille, NSFamille, NumeroLigne, NumOrdre, PxHeured, Quantite, ST, STd,"
                 + " STud, TexteLigne, TotalDebours, TotalVente,'" + sNomModèles + "' AS Expr1 "
                 + " , codesousarticle, fournisseur"
                 + " FROM DevisDetail WHERE (NumeroDevis ='" + General.gNumeroDevis + "')";
                    General.Execute(SQL);

                    Timer1.Enabled = true;
                }
                Cursor.Current = Cursors.Arrow;


            }
            catch (Exception ex)
            {

                Program.SaveException(ex);

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);


            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheModele_Click(object sender, EventArgs e)
        {
            var Btn = sender as Button;
            var Index = Convert.ToInt32(Btn.Tag);
            try
            {

                string requete = "SELECT  DevisModeles.ModeleDevis AS \"Modele\", DevisModeles.Libelle AS \"Libelle\" FROM DevisModeles";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un modele de devis" };
                if (Index == 1)
                    fg.SetValues(new Dictionary<string, string> { { "ModeleDevis", txtFiltreModele.Text } });
                else if (Index == 0)
                    fg.SetValues(new Dictionary<string, string> { { "Libelle", txtFiltreLibelleModele.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    if (Index == 0)//tested
                    {
                        txtFiltreModele.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        modAdodata1 = new ModAdo();
                        data1 = modAdodata1.fc_OpenRecordSet("SELECT * FROM DevisModeles WHERE ModeleDevis LIKE '%" + txtFiltreModele.Text + "%' order by ModeleDevis");
                        SSDBGrid1.DataSource = data1;

                        if (SSDBGrid1.Rows.Count == 0)
                            return;

                        modAdoData2 = new ModAdo();
                        Data2 = modAdoData2.fc_OpenRecordSet("SELECT * FROM DevisModelesDetail " + "WHERE ModeleDevis='" + SSDBGrid1.Rows[0].Cells[0].Value + "' " + "ORDER BY NumeroLigne");
                        SSOleDBGrid1.DataSource = Data2;


                    }
                    else if (Index == 1)//tested
                    {
                        txtFiltreLibelleModele.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        modAdodata1 = new ModAdo();

                        data1 = modAdodata1.fc_OpenRecordSet("SELECT * FROM DevisModeles WHERE Libelle LIKE '%" + txtFiltreLibelleModele.Text + "%' order by ModeleDevis");
                        SSDBGrid1.DataSource = data1;

                        if (SSDBGrid1.Rows.Count == 0)
                            return;

                        modAdoData2 = new ModAdo();
                        Data2 = modAdoData2.fc_OpenRecordSet("SELECT * FROM DevisModelesDetail " + "WHERE ModeleDevis='" + SSDBGrid1.Rows[0].Cells[0].Value + "' " + "ORDER BY NumeroLigne");
                        SSOleDBGrid1.DataSource = Data2;

                    }
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {

                        if (Index == 0)
                        {
                            txtFiltreModele.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            modAdodata1 = new ModAdo();
                            data1 = modAdodata1.fc_OpenRecordSet("SELECT * FROM DevisModeles WHERE ModeleDevis LIKE '%" + txtFiltreModele.Text + "%' order by ModeleDevis");
                            SSDBGrid1.DataSource = data1;

                            if (SSDBGrid1.Rows.Count == 0)
                                return;

                            modAdoData2 = new ModAdo();
                            Data2 = modAdoData2.fc_OpenRecordSet("SELECT * FROM DevisModelesDetail " + "WHERE ModeleDevis='" + SSDBGrid1.Rows[0].Cells[0].Value + "' " + "ORDER BY NumeroLigne");
                            SSOleDBGrid1.DataSource = Data2;


                        }
                        else if (Index == 1)
                        {
                            txtFiltreLibelleModele.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                            modAdodata1 = new ModAdo();

                            data1 = modAdodata1.fc_OpenRecordSet("SELECT * FROM DevisModeles WHERE Libelle LIKE '%" + txtFiltreLibelleModele.Text + "%' order by ModeleDevis");
                            SSDBGrid1.DataSource = data1;

                            if (SSDBGrid1.Rows.Count == 0)
                                return;

                            modAdoData2 = new ModAdo();
                            Data2 = modAdoData2.fc_OpenRecordSet("SELECT * FROM DevisModelesDetail " + "WHERE ModeleDevis='" + SSDBGrid1.Rows[0].Cells[0].Value + "' " + "ORDER BY NumeroLigne");
                            SSOleDBGrid1.DataSource = Data2;


                        }
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, " frmModele cmdRechercheCodePoste_Click Index:" + Index + ";");
            }
        }
        //SSDBGrid1_RowColChange
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGrid1_AfterCellActivate(object sender, EventArgs e)
        {
            //try
            //{
            //    String SQL = "SELECT * FROM DevisModelesDetail " + "WHERE ModeleDevis='" + SSDBGrid1.ActiveRow.Cells[0].Value + "' " + "ORDER BY NumeroLigne";


            //    Data2 = modAdoData2.fc_OpenRecordSet(SQL);


            //    SSOleDBGrid1.DataSource = Data2;
            //}
            //catch (Exception exception)
            //{
            //    Erreurs.gFr_debug(exception, "frmModele ; SSDBGrid1_RowColChange");
            //}
        }

        private void SSOleDBGrid1_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["ModeleDevis"].Hidden = false;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["CodeMO"].Hidden = false;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["CodeSousFamille"].Header.Caption = "Poste";
            //SSOleDBGrid1.DisplayLayout.Bands[0].Columns["CodeArticle"].Header.Caption = "Poste";
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["TexteLigne"].Hidden = false;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Quantite"].Hidden = false;


            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["NumeroLigne"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["CodeFamille"].Hidden = true;

            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["CodeSousArticle"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["NumOrdre"].Hidden = true;

            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["MOhud"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["PxHeured"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["FOud"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["STud"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["MOhd"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["MOd"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["FOd"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["STd"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["TotalDebours"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["kMO"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["MO"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["kFO"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["FO"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["kST"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["ST"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["TotalVente"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Coef1"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Coef2"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Coef3"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["TotalVenteApresCoef"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["CodeTVA"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Facturer"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["DateFacture"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["NFamille"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["NSFamille"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["NArticle"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Fournisseur"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Unite"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["SautPage"].Hidden = true;
            //  SSOleDBGrid1.DisplayLayout.Bands[0].Columns["PrixUnitaireCal"].Hidden = true;
            //   SSOleDBGrid1.DisplayLayout.Bands[0].Columns["QuantiteCal"].Hidden = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(modAdodata1?.SDArsAdo.SelectCommand.CommandText))
                {
                    modAdodata1 = new ModAdo();
                    data1 = modAdodata1.fc_OpenRecordSet("SELECT * FROM DevisModeles WHERE ModeleDevis LIKE '%" + txtFiltreModele.Text + "%' order by ModeleDevis");
                    SSDBGrid1.DataSource = data1;
                    Timer1.Enabled = false;
                }
                Cursor.Current = Cursors.Default;
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, "frmModele ; Timer1_Timer");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFiltreLibelleModele_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                data1 = modAdodata1.fc_OpenRecordSet("SELECT * FROM DevisModeles WHERE Libelle LIKE '%" + txtFiltreLibelleModele.Text + "%' order by ModeleDevis");
                SSDBGrid1.DataSource = data1;
                if (SSDBGrid1.Rows.Count == 0)
                    return;

                modAdoData2 = new ModAdo();
                Data2 = modAdoData2.fc_OpenRecordSet("SELECT * FROM DevisModelesDetail " + "WHERE ModeleDevis='" + SSDBGrid1.Rows[0].Cells[0].Value + "' " + "ORDER BY NumeroLigne");
                SSOleDBGrid1.DataSource = Data2;
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtFiltreModele_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)(e.KeyChar);
            try
            {
                if (KeyAscii == 13)
                {
                    //            Data1.Recordset.Close
                    //            Data1.ConnectionString = ""
                    //            Data1.ConnectionString = adocnn.ConnectionString
                    data1 = modAdodata1.fc_OpenRecordSet("SELECT * FROM DevisModeles WHERE ModeleDevis LIKE '%" + txtFiltreModele.Text + "%' order by ModeleDevis");
                    SSDBGrid1.DataSource = data1;
                    if (SSDBGrid1.Rows.Count > 0)
                        SSDBGrid1.Rows[SSDBGrid1.Rows.Count - 1].Selected = true;

                }
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, this.Name + ";txtFiltreModele_KeyPress");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGrid1_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            modAdodata1.Update();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGrid1_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdodata1.Update();
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdoData2.Update();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            modAdoData2.Update();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSDBGrid1_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {
            if (e.Row.Cells["ModeleDevis"].DataChanged)
            {
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM DevisModeles  WHERE ModeleDevis= '{e.Row.Cells["ModeleDevis"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void SSDBGrid1_AfterRowActivate(object sender, EventArgs e)
        {
            if (SSDBGrid1.ActiveRow == null) return;
            try
            {
                String SQL = "SELECT * FROM DevisModelesDetail " + "WHERE ModeleDevis='" + SSDBGrid1.ActiveRow.Cells[0].Value + "' " + "ORDER BY NumeroLigne";


                Data2 = modAdoData2.fc_OpenRecordSet(SQL);


                SSOleDBGrid1.DataSource = Data2;
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, "frmModele ; SSDBGrid1_RowColChange");
            }
        }
    }

}
