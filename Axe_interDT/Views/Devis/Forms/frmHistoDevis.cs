﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmHistoDevis : Form
    {
        public frmHistoDevis()
        {
            InitializeComponent();
        }

        private void Command1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void frmHistoDevis_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            FourniGrilleDevisHisto();
        }
        /// <summary>
        /// tested
        /// </summary>
        public void FourniGrilleDevisHisto()
        {

            DataTable rs2 = default(DataTable);
            ModAdo modAdors2 = null;
            string sql2 = null;

            SSOleDBGridDevisHisto.DataSource = null;

            sql2 = "SELECT DevisEntete.CodeEtat, DevisEntete.TitreDevis, DevisEntete.Observations, " 
                + "DevisCodeEtat.Code, DevisCodeEtat.Libelle, DevisEntete.CodeDeviseur, Personnel.Nom " 
                + "FROM DevisEntete LEFT JOIN " 
                + "DevisCodeEtat ON DevisEntete.CodeEtat = DevisCodeEtat.Code LEFT JOIN Personnel ON DevisEntete.CodeDeviseur = Personnel.Matricule "
                + "WHERE (DevisEntete.CodeImmeuble = '" + General.NomImmeuble + "') " + "ORDER BY DevisEntete.DateCreation DESC";
            modAdors2 = new ModAdo();

            // Permet de compter ou de limiter le nombre d'enregistrements
            rs2 = modAdors2.fc_OpenRecordSet(sql2);

            SSOleDBGridDevisHisto.DataSource = rs2;


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGridDevisHisto_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns["CodeEtat"].Hidden = true;
            SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns["Code"].Hidden = true;
            SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns["CodeDeviseur"].Hidden = true;
            SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns["Libelle"].Header.Caption = "Etat";
            SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns["TitreDevis"].Header.Caption = "Titre";
            SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns["Nom"].Header.Caption = "Deviseur";
            for(int i=0;i< SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns.Count - 1; i++)
            {
                SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }
        }
    }
}
