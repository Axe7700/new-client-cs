﻿namespace Axe_interDT.Views.Devis.Forms
{
    partial class frmMail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SSOleDBcmbA = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.GridDocuments = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cmdCopie = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.optBasse = new System.Windows.Forms.RadioButton();
            this.optNormale = new System.Windows.Forms.RadioButton();
            this.optHaute = new System.Windows.Forms.RadioButton();
            this.cmdEnvoyer = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.txtMessage = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCopie = new iTalk.iTalk_TextBox_Small2();
            this.txtObjet = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDocuments = new iTalk.iTalk_TextBox_Small2();
            this.txtDossierSauvegarde = new iTalk.iTalk_TextBox_Small2();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBcmbA)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDocuments)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 15F);
            this.label33.Location = new System.Drawing.Point(3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(822, 30);
            this.label33.TabIndex = 384;
            this.label33.Text = "Message";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(3, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(101, 19);
            this.label1.TabIndex = 385;
            this.label1.Text = "Destinataire :";
            // 
            // SSOleDBcmbA
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBcmbA.DisplayLayout.Appearance = appearance1;
            this.SSOleDBcmbA.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBcmbA.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBcmbA.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBcmbA.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.SSOleDBcmbA.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBcmbA.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.SSOleDBcmbA.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBcmbA.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBcmbA.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBcmbA.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.SSOleDBcmbA.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBcmbA.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBcmbA.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBcmbA.DisplayLayout.Override.CellAppearance = appearance8;
            this.SSOleDBcmbA.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBcmbA.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBcmbA.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.SSOleDBcmbA.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.SSOleDBcmbA.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBcmbA.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBcmbA.DisplayLayout.Override.RowAppearance = appearance11;
            this.SSOleDBcmbA.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBcmbA.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.SSOleDBcmbA.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBcmbA.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBcmbA.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBcmbA.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSOleDBcmbA.Location = new System.Drawing.Point(123, 3);
            this.SSOleDBcmbA.Name = "SSOleDBcmbA";
            this.SSOleDBcmbA.Size = new System.Drawing.Size(231, 27);
            this.SSOleDBcmbA.TabIndex = 503;
            // 
            // GridDocuments
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridDocuments.DisplayLayout.Appearance = appearance13;
            this.GridDocuments.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridDocuments.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.GridDocuments.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridDocuments.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.GridDocuments.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridDocuments.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.GridDocuments.DisplayLayout.MaxColScrollRegions = 1;
            this.GridDocuments.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridDocuments.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridDocuments.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.GridDocuments.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.GridDocuments.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.GridDocuments.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.GridDocuments.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridDocuments.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.GridDocuments.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridDocuments.DisplayLayout.Override.CellAppearance = appearance20;
            this.GridDocuments.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridDocuments.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.GridDocuments.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.GridDocuments.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.GridDocuments.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridDocuments.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.GridDocuments.DisplayLayout.Override.RowAppearance = appearance23;
            this.GridDocuments.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridDocuments.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridDocuments.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.GridDocuments.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridDocuments.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridDocuments.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridDocuments.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridDocuments.Location = new System.Drawing.Point(123, 3);
            this.GridDocuments.Name = "GridDocuments";
            this.GridDocuments.Size = new System.Drawing.Size(702, 104);
            this.GridDocuments.TabIndex = 508;
            this.GridDocuments.Text = "ultraGrid1";
            this.GridDocuments.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridDocuments_InitializeLayout);
            this.GridDocuments.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.GridDocuments_ClickCellButton);
            this.GridDocuments.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.GridDocuments_DoubleClickRow);
            // 
            // cmdCopie
            // 
            this.cmdCopie.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdCopie.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCopie.Dock = System.Windows.Forms.DockStyle.Right;
            this.cmdCopie.FlatAppearance.BorderSize = 0;
            this.cmdCopie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCopie.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.cmdCopie.ForeColor = System.Drawing.Color.White;
            this.cmdCopie.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdCopie.Location = new System.Drawing.Point(83, 32);
            this.cmdCopie.Margin = new System.Windows.Forms.Padding(2);
            this.cmdCopie.Name = "cmdCopie";
            this.cmdCopie.Size = new System.Drawing.Size(35, 26);
            this.cmdCopie.TabIndex = 513;
            this.cmdCopie.Text = "CC";
            this.cmdCopie.UseVisualStyleBackColor = false;
            this.cmdCopie.Click += new System.EventHandler(this.cmdCopie_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(3, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(55, 19);
            this.label2.TabIndex = 514;
            this.label2.Text = "Objet :";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(3, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(78, 19);
            this.label3.TabIndex = 515;
            this.label3.Text = "Message :";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label4.Location = new System.Drawing.Point(3, 0);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(93, 38);
            this.label4.TabIndex = 516;
            this.label4.Text = "Documents joints";
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.optBasse);
            this.groupBox6.Controls.Add(this.optNormale);
            this.groupBox6.Controls.Add(this.optHaute);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox6.Location = new System.Drawing.Point(731, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(94, 98);
            this.groupBox6.TabIndex = 517;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Importance";
            // 
            // optBasse
            // 
            this.optBasse.AutoSize = true;
            this.optBasse.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optBasse.Location = new System.Drawing.Point(6, 71);
            this.optBasse.Name = "optBasse";
            this.optBasse.Size = new System.Drawing.Size(68, 23);
            this.optBasse.TabIndex = 540;
            this.optBasse.Text = "Basse";
            this.optBasse.UseVisualStyleBackColor = true;
            // 
            // optNormale
            // 
            this.optNormale.AutoSize = true;
            this.optNormale.Checked = true;
            this.optNormale.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optNormale.Location = new System.Drawing.Point(6, 48);
            this.optNormale.Name = "optNormale";
            this.optNormale.Size = new System.Drawing.Size(87, 23);
            this.optNormale.TabIndex = 539;
            this.optNormale.TabStop = true;
            this.optNormale.Text = "Normale";
            this.optNormale.UseVisualStyleBackColor = true;
            // 
            // optHaute
            // 
            this.optHaute.AutoSize = true;
            this.optHaute.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optHaute.Location = new System.Drawing.Point(6, 24);
            this.optHaute.Name = "optHaute";
            this.optHaute.Size = new System.Drawing.Size(70, 23);
            this.optHaute.TabIndex = 538;
            this.optHaute.Text = "Haute";
            this.optHaute.UseVisualStyleBackColor = true;
            // 
            // cmdEnvoyer
            // 
            this.cmdEnvoyer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdEnvoyer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdEnvoyer.FlatAppearance.BorderSize = 0;
            this.cmdEnvoyer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdEnvoyer.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.cmdEnvoyer.ForeColor = System.Drawing.Color.White;
            this.cmdEnvoyer.Image = global::Axe_interDT.Properties.Resources.Send_Arrow_24x24;
            this.cmdEnvoyer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdEnvoyer.Location = new System.Drawing.Point(135, 2);
            this.cmdEnvoyer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdEnvoyer.Name = "cmdEnvoyer";
            this.cmdEnvoyer.Size = new System.Drawing.Size(124, 35);
            this.cmdEnvoyer.TabIndex = 510;
            this.cmdEnvoyer.Text = "        Envoyer";
            this.cmdEnvoyer.UseVisualStyleBackColor = false;
            this.cmdEnvoyer.Click += new System.EventHandler(this.cmdEnvoyer_Click);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.cmdAnnuler.ForeColor = System.Drawing.Color.White;
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_16x16;
            this.cmdAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAnnuler.Location = new System.Drawing.Point(2, 2);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(120, 35);
            this.cmdAnnuler.TabIndex = 509;
            this.cmdAnnuler.Text = "     Annuler";
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel5, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel6, 0, 4);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 108F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(828, 557);
            this.tableLayoutPanel1.TabIndex = 518;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.GridDocuments, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.label4, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 396);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 110F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(828, 110);
            this.tableLayoutPanel5.TabIndex = 520;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.txtMessage, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 0);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 138);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 1;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 258F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 258F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(828, 258);
            this.tableLayoutPanel4.TabIndex = 519;
            // 
            // txtMessage
            // 
            this.txtMessage.AccAcceptNumbersOnly = false;
            this.txtMessage.AccAllowComma = false;
            this.txtMessage.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMessage.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMessage.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtMessage.AccHidenValue = "";
            this.txtMessage.AccNotAllowedChars = null;
            this.txtMessage.AccReadOnly = false;
            this.txtMessage.AccReadOnlyAllowDelete = false;
            this.txtMessage.AccRequired = false;
            this.txtMessage.BackColor = System.Drawing.Color.White;
            this.txtMessage.CustomBackColor = System.Drawing.Color.White;
            this.txtMessage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMessage.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtMessage.ForeColor = System.Drawing.Color.Black;
            this.txtMessage.Location = new System.Drawing.Point(122, 2);
            this.txtMessage.Margin = new System.Windows.Forms.Padding(2);
            this.txtMessage.MaxLength = 32767;
            this.txtMessage.Multiline = true;
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.ReadOnly = false;
            this.txtMessage.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMessage.Size = new System.Drawing.Size(704, 254);
            this.txtMessage.TabIndex = 507;
            this.txtMessage.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMessage.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 100F));
            this.tableLayoutPanel2.Controls.Add(this.groupBox6, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 30);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(828, 108);
            this.tableLayoutPanel2.TabIndex = 385;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.SSOleDBcmbA, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtCopie, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.cmdCopie, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtObjet, 1, 2);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 3;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(728, 108);
            this.tableLayoutPanel3.TabIndex = 518;
            // 
            // txtCopie
            // 
            this.txtCopie.AccAcceptNumbersOnly = false;
            this.txtCopie.AccAllowComma = false;
            this.txtCopie.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCopie.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCopie.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCopie.AccHidenValue = "";
            this.txtCopie.AccNotAllowedChars = null;
            this.txtCopie.AccReadOnly = false;
            this.txtCopie.AccReadOnlyAllowDelete = false;
            this.txtCopie.AccRequired = false;
            this.txtCopie.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCopie.BackColor = System.Drawing.Color.White;
            this.txtCopie.CustomBackColor = System.Drawing.Color.White;
            this.txtCopie.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCopie.ForeColor = System.Drawing.Color.Black;
            this.txtCopie.Location = new System.Drawing.Point(122, 32);
            this.txtCopie.Margin = new System.Windows.Forms.Padding(2);
            this.txtCopie.MaxLength = 32767;
            this.txtCopie.Multiline = false;
            this.txtCopie.Name = "txtCopie";
            this.txtCopie.ReadOnly = true;
            this.txtCopie.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCopie.Size = new System.Drawing.Size(604, 27);
            this.txtCopie.TabIndex = 504;
            this.txtCopie.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCopie.UseSystemPasswordChar = false;
            // 
            // txtObjet
            // 
            this.txtObjet.AccAcceptNumbersOnly = false;
            this.txtObjet.AccAllowComma = false;
            this.txtObjet.AccBackgroundColor = System.Drawing.Color.White;
            this.txtObjet.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtObjet.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtObjet.AccHidenValue = "";
            this.txtObjet.AccNotAllowedChars = null;
            this.txtObjet.AccReadOnly = false;
            this.txtObjet.AccReadOnlyAllowDelete = false;
            this.txtObjet.AccRequired = false;
            this.txtObjet.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.txtObjet.BackColor = System.Drawing.Color.White;
            this.txtObjet.CustomBackColor = System.Drawing.Color.White;
            this.txtObjet.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtObjet.ForeColor = System.Drawing.Color.Black;
            this.txtObjet.Location = new System.Drawing.Point(122, 62);
            this.txtObjet.Margin = new System.Windows.Forms.Padding(2);
            this.txtObjet.MaxLength = 32767;
            this.txtObjet.Multiline = false;
            this.txtObjet.Name = "txtObjet";
            this.txtObjet.ReadOnly = false;
            this.txtObjet.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtObjet.Size = new System.Drawing.Size(604, 27);
            this.txtObjet.TabIndex = 505;
            this.txtObjet.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtObjet.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.cmdEnvoyer, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.cmdAnnuler, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Right;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(559, 509);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(266, 45);
            this.tableLayoutPanel6.TabIndex = 521;
            // 
            // txtDocuments
            // 
            this.txtDocuments.AccAcceptNumbersOnly = false;
            this.txtDocuments.AccAllowComma = false;
            this.txtDocuments.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDocuments.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDocuments.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtDocuments.AccHidenValue = "";
            this.txtDocuments.AccNotAllowedChars = null;
            this.txtDocuments.AccReadOnly = false;
            this.txtDocuments.AccReadOnlyAllowDelete = false;
            this.txtDocuments.AccRequired = false;
            this.txtDocuments.BackColor = System.Drawing.Color.White;
            this.txtDocuments.CustomBackColor = System.Drawing.Color.White;
            this.txtDocuments.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDocuments.ForeColor = System.Drawing.Color.Black;
            this.txtDocuments.Location = new System.Drawing.Point(85, 581);
            this.txtDocuments.Margin = new System.Windows.Forms.Padding(2);
            this.txtDocuments.MaxLength = 32767;
            this.txtDocuments.Multiline = false;
            this.txtDocuments.Name = "txtDocuments";
            this.txtDocuments.ReadOnly = false;
            this.txtDocuments.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDocuments.Size = new System.Drawing.Size(186, 27);
            this.txtDocuments.TabIndex = 512;
            this.txtDocuments.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDocuments.UseSystemPasswordChar = false;
            this.txtDocuments.Visible = false;
            // 
            // txtDossierSauvegarde
            // 
            this.txtDossierSauvegarde.AccAcceptNumbersOnly = false;
            this.txtDossierSauvegarde.AccAllowComma = false;
            this.txtDossierSauvegarde.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDossierSauvegarde.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDossierSauvegarde.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtDossierSauvegarde.AccHidenValue = "";
            this.txtDossierSauvegarde.AccNotAllowedChars = null;
            this.txtDossierSauvegarde.AccReadOnly = false;
            this.txtDossierSauvegarde.AccReadOnlyAllowDelete = false;
            this.txtDossierSauvegarde.AccRequired = false;
            this.txtDossierSauvegarde.BackColor = System.Drawing.Color.White;
            this.txtDossierSauvegarde.CustomBackColor = System.Drawing.Color.White;
            this.txtDossierSauvegarde.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDossierSauvegarde.ForeColor = System.Drawing.Color.Black;
            this.txtDossierSauvegarde.Location = new System.Drawing.Point(24, 581);
            this.txtDossierSauvegarde.Margin = new System.Windows.Forms.Padding(2);
            this.txtDossierSauvegarde.MaxLength = 32767;
            this.txtDossierSauvegarde.Multiline = false;
            this.txtDossierSauvegarde.Name = "txtDossierSauvegarde";
            this.txtDossierSauvegarde.ReadOnly = false;
            this.txtDossierSauvegarde.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDossierSauvegarde.Size = new System.Drawing.Size(57, 27);
            this.txtDossierSauvegarde.TabIndex = 511;
            this.txtDossierSauvegarde.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDossierSauvegarde.UseSystemPasswordChar = false;
            this.txtDossierSauvegarde.Visible = false;
            // 
            // frmMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(828, 557);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.txtDocuments);
            this.Controls.Add(this.txtDossierSauvegarde);
            this.Name = "frmMail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Envoi d\'un Mail";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmMail_FormClosed);
            this.Load += new System.EventHandler(this.frmMail_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBcmbA)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.GridDocuments)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label1;
        public iTalk.iTalk_TextBox_Small2 txtCopie;
        public iTalk.iTalk_TextBox_Small2 txtObjet;
        public iTalk.iTalk_TextBox_Small2 txtMessage;
        private Infragistics.Win.UltraWinGrid.UltraGrid GridDocuments;
        public System.Windows.Forms.Button cmdEnvoyer;
        public System.Windows.Forms.Button cmdAnnuler;
        public iTalk.iTalk_TextBox_Small2 txtDossierSauvegarde;
        public iTalk.iTalk_TextBox_Small2 txtDocuments;
        public System.Windows.Forms.Button cmdCopie;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton optBasse;
        private System.Windows.Forms.RadioButton optNormale;
        private System.Windows.Forms.RadioButton optHaute;
        public Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBcmbA;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
    }
}