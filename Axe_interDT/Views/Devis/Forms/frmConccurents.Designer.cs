﻿namespace Axe_interDT.Views.Devis.Forms
{
    partial class frmConccurents
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.ssgCNC_Conccurent = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.ssDropconcurrent = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtNoDevis = new iTalk.iTalk_TextBox_Small2();
            ((System.ComponentModel.ISupportInitialize)(this.ssgCNC_Conccurent)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropconcurrent)).BeginInit();
            this.SuspendLayout();
            // 
            // ssgCNC_Conccurent
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssgCNC_Conccurent.DisplayLayout.Appearance = appearance1;
            this.ssgCNC_Conccurent.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.ssgCNC_Conccurent.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssgCNC_Conccurent.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ssgCNC_Conccurent.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssgCNC_Conccurent.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ssgCNC_Conccurent.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssgCNC_Conccurent.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ssgCNC_Conccurent.DisplayLayout.MaxColScrollRegions = 1;
            this.ssgCNC_Conccurent.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssgCNC_Conccurent.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssgCNC_Conccurent.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ssgCNC_Conccurent.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.ssgCNC_Conccurent.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.ssgCNC_Conccurent.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.ssgCNC_Conccurent.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssgCNC_Conccurent.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ssgCNC_Conccurent.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssgCNC_Conccurent.DisplayLayout.Override.CellAppearance = appearance8;
            this.ssgCNC_Conccurent.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssgCNC_Conccurent.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ssgCNC_Conccurent.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ssgCNC_Conccurent.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ssgCNC_Conccurent.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssgCNC_Conccurent.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ssgCNC_Conccurent.DisplayLayout.Override.RowAppearance = appearance11;
            this.ssgCNC_Conccurent.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssgCNC_Conccurent.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssgCNC_Conccurent.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ssgCNC_Conccurent.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssgCNC_Conccurent.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssgCNC_Conccurent.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssgCNC_Conccurent.Location = new System.Drawing.Point(12, 12);
            this.ssgCNC_Conccurent.Name = "ssgCNC_Conccurent";
            this.ssgCNC_Conccurent.Size = new System.Drawing.Size(351, 376);
            this.ssgCNC_Conccurent.TabIndex = 412;
            this.ssgCNC_Conccurent.Text = "ssgCNC_Conccurent";
            this.ssgCNC_Conccurent.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssgCNC_Conccurent_InitializeLayout);
            this.ssgCNC_Conccurent.AfterRowsDeleted += new System.EventHandler(this.ssgCNC_Conccurent_AfterRowsDeleted);
            this.ssgCNC_Conccurent.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ssgCNC_Conccurent_AfterRowUpdate);
            this.ssgCNC_Conccurent.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.ssgCNC_Conccurent_BeforeExitEditMode);
            // 
            // ssDropconcurrent
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssDropconcurrent.DisplayLayout.Appearance = appearance13;
            this.ssDropconcurrent.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssDropconcurrent.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropconcurrent.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropconcurrent.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ssDropconcurrent.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropconcurrent.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ssDropconcurrent.DisplayLayout.MaxColScrollRegions = 1;
            this.ssDropconcurrent.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssDropconcurrent.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssDropconcurrent.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ssDropconcurrent.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssDropconcurrent.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ssDropconcurrent.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssDropconcurrent.DisplayLayout.Override.CellAppearance = appearance20;
            this.ssDropconcurrent.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssDropconcurrent.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropconcurrent.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ssDropconcurrent.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ssDropconcurrent.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssDropconcurrent.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ssDropconcurrent.DisplayLayout.Override.RowAppearance = appearance23;
            this.ssDropconcurrent.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssDropconcurrent.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ssDropconcurrent.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssDropconcurrent.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssDropconcurrent.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ssDropconcurrent.Location = new System.Drawing.Point(118, 189);
            this.ssDropconcurrent.Name = "ssDropconcurrent";
            this.ssDropconcurrent.Size = new System.Drawing.Size(186, 22);
            this.ssDropconcurrent.TabIndex = 503;
            this.ssDropconcurrent.Visible = false;
            // 
            // txtNoDevis
            // 
            this.txtNoDevis.AccAcceptNumbersOnly = false;
            this.txtNoDevis.AccAllowComma = false;
            this.txtNoDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoDevis.AccHidenValue = "";
            this.txtNoDevis.AccNotAllowedChars = null;
            this.txtNoDevis.AccReadOnly = false;
            this.txtNoDevis.AccReadOnlyAllowDelete = false;
            this.txtNoDevis.AccRequired = false;
            this.txtNoDevis.BackColor = System.Drawing.Color.White;
            this.txtNoDevis.CustomBackColor = System.Drawing.Color.White;
            this.txtNoDevis.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNoDevis.ForeColor = System.Drawing.Color.Black;
            this.txtNoDevis.Location = new System.Drawing.Point(188, 0);
            this.txtNoDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoDevis.MaxLength = 32767;
            this.txtNoDevis.Multiline = false;
            this.txtNoDevis.Name = "txtNoDevis";
            this.txtNoDevis.ReadOnly = false;
            this.txtNoDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoDevis.Size = new System.Drawing.Size(135, 27);
            this.txtNoDevis.TabIndex = 504;
            this.txtNoDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoDevis.UseSystemPasswordChar = false;
            this.txtNoDevis.Visible = false;
            // 
            // frmConccurents
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(370, 400);
            this.Controls.Add(this.txtNoDevis);
            this.Controls.Add(this.ssDropconcurrent);
            this.Controls.Add(this.ssgCNC_Conccurent);
            this.Name = "frmConccurents";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmConccurents";
            this.Load += new System.EventHandler(this.frmConccurents_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ssgCNC_Conccurent)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropconcurrent)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public Infragistics.Win.UltraWinGrid.UltraGrid ssgCNC_Conccurent;
        public Infragistics.Win.UltraWinGrid.UltraCombo ssDropconcurrent;
        public iTalk.iTalk_TextBox_Small2 txtNoDevis;
    }
}