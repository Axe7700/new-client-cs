﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmEnteteDevis : Form
    {
        public frmEnteteDevis()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            DataTable rsEntete = default(DataTable);
            ModAdo modAdorsEntete;

            if (string.IsNullOrEmpty(txtLibelle.Text) && string.IsNullOrEmpty(txtTexte.Text) && !string.IsNullOrEmpty(txtCode.Text))
            {
                return;
            }

            txtCode.Text = "";
            txtLibelle.Text = "";
            txtTexte.Text = "";
            lblUtilisateur.Text = "";

            modAdorsEntete = new ModAdo();
            rsEntete = modAdorsEntete.fc_OpenRecordSet("SELECT * FROM TypeEnteteDevis WHERE Code=0");
            //    rsEntete.Close
            var NewRow = rsEntete.NewRow();
            NewRow["Utilisateur"] = General.gsUtilisateur;
            rsEntete.Rows.Add(NewRow);
            lblUtilisateur.Text = General.gsUtilisateur;
            modAdorsEntete.Update();
            using (var tmpModAdo = new ModAdo())
                txtCode.Text = tmpModAdo.fc_ADOlibelle("SELECT MAX(Code) FROM TypeEnteteDevis");
            modAdorsEntete?.Dispose();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLibelle.Text) && string.IsNullOrEmpty(txtTexte.Text) && !string.IsNullOrEmpty(txtCode.Text))
            {
                General.Execute("DELETE FROM TypeEnteteDevis WHERE Code=" + txtCode.Text);
                cmdDernier_Click(cmdDernier, new System.EventArgs());
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDernier_Click(object sender, EventArgs e)
        {
            DataTable rsEntete = default(DataTable);
            var modAdorsEntete = new ModAdo();


            rsEntete = modAdorsEntete.fc_OpenRecordSet("SELECT TOP 1 TypeEnteteDevis.* FROM TypeEnteteDevis ORDER BY Code DESC");

            if (rsEntete.Rows.Count > 0)
            {
                txtCode.Text = rsEntete.Rows[0]["Code"] + "";
                txtLibelle.Text = rsEntete.Rows[0]["Libelle"] + "";
                txtTexte.Text = rsEntete.Rows[0]["Texte"] + "";
                lblUtilisateur.Text = rsEntete.Rows[0]["Utilisateur"] + "";
            }
            else
            {
                txtCode.Text = "";
                txtLibelle.Text = "";
                txtTexte.Text = "";
                lblUtilisateur.Text = "";
            }

            modAdorsEntete?.Dispose();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdPrecedent_Click(object sender, EventArgs e)
        {
            DataTable rsEntete = default(DataTable);
            var modAdorsEntete = new ModAdo();


            rsEntete = modAdorsEntete.fc_OpenRecordSet("SELECT TypeEnteteDevis.* FROM TypeEnteteDevis WHERE Code<'" + txtCode.Text + "' ORDER BY Code DESC");

            if (rsEntete.Rows.Count > 0)
            {
                txtCode.Text = rsEntete.Rows[0]["Code"] + "";
                txtLibelle.Text = rsEntete.Rows[0]["Libelle"] + "";
                txtTexte.Text = rsEntete.Rows[0]["Texte"] + "";
                lblUtilisateur.Text = rsEntete.Rows[0]["Utilisateur"] + "";
            }

            modAdorsEntete?.Dispose();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdPremier_Click(object sender, EventArgs e)
        {
            DataTable rsEntete = default(DataTable);
            var modAdorsEntete = new ModAdo();


            rsEntete = modAdorsEntete.fc_OpenRecordSet("SELECT TOP 1 TypeEnteteDevis.* FROM TypeEnteteDevis ORDER BY Code ASC");

            if (rsEntete.Rows.Count > 0)
            {
                txtCode.Text = rsEntete.Rows[0]["Code"] + "";
                txtLibelle.Text = rsEntete.Rows[0]["Libelle"] + "";
                txtTexte.Text = rsEntete.Rows[0]["Texte"] + "";
                lblUtilisateur.Text = rsEntete.Rows[0]["Utilisateur"] + "";
            }
            else
            {
                txtCode.Text = "";
                txtLibelle.Text = "";
                txtTexte.Text = "";
                lblUtilisateur.Text = "";
            }

            modAdorsEntete?.Dispose();
        }

        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            try
            {
                string requete = "SELECT TypeEnteteDevis.Libelle AS [Libellé],TypeEnteteDevis.Texte AS [Texte], " + "TypeEnteteDevis.Code AS [Code], TypeEnteteDevis.Utilisateur  " +
                                 " FROM TypeEnteteDevis"; ;
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des Entêtes de devis" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    this.txtTexte.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    this.txtLibelle.Text = fg.ugResultat.ActiveRow.Cells[0].Value.ToString();
                    this.txtCode.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    this.lblUtilisateur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        this.txtTexte.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        this.txtLibelle.Text = fg.ugResultat.ActiveRow.Cells[0].Value.ToString();
                        this.txtCode.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        this.lblUtilisateur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, "frmEnteteDevis " + ";cmdEnteteDevis_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            string sqlSave = null;
            DataTable rsEntete = default(DataTable);
            var modAdorsEntete = new ModAdo();

            try
            {
                if (string.IsNullOrEmpty(txtCode.Text))
                {
                    rsEntete = modAdorsEntete.fc_OpenRecordSet("SELECT TOP 1 * FROM TypeEnteteDevis ");
                    var NewRow = rsEntete.NewRow();
                    NewRow["Utilisateur"] = General.gsUtilisateur;
                    lblUtilisateur.Text = General.gsUtilisateur;
                    modAdorsEntete.Update();
                    using (var tmpModAdo = new ModAdo())
                        txtCode.Text = tmpModAdo.fc_ADOlibelle("SELEXT MAX(Code) FROM TypeEnteteDevis");
                    modAdorsEntete?.Dispose();
                }

                sqlSave = "UPDATE TypeEnteteDevis SET Texte='" + StdSQLchaine.gFr_DoublerQuote(txtTexte.Text) + "', Libelle='" + StdSQLchaine.gFr_DoublerQuote(txtLibelle.Text) + "' ";
                sqlSave = sqlSave + " WHERE Code=" + txtCode.Text;

                General.Execute(sqlSave);
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, "frmEnteteDevis " + " cmdSauver_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSuivant_Click(object sender, EventArgs e)
        {
            DataTable rsEntete = default(DataTable);
            var modAdorsEntete = new ModAdo();

            rsEntete = modAdorsEntete.fc_OpenRecordSet("SELECT TypeEnteteDevis.* FROM TypeEnteteDevis WHERE Code>'" + txtCode.Text + "' ORDER BY Code ASC");

            if (rsEntete.Rows.Count > 0)
            {
                txtCode.Text = rsEntete.Rows[0]["Code"] + "";
                txtLibelle.Text = rsEntete.Rows[0]["Libelle"] + "";
                txtTexte.Text = rsEntete.Rows[0]["Texte"] + "";
                lblUtilisateur.Text = rsEntete.Rows[0]["Utilisateur"] + "";
            }

            modAdorsEntete?.Dispose();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSupprimer_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCode.Text))
            {
                return;
            }
            if (lblUtilisateur.Text == General.gsUtilisateur)
            {
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez-vous vraiment supprimer cette entete ?", "Suppression d'une entête", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    General.Execute("DELETE FROM TypeEnteteDevis WHERE Code=" + txtCode.Text);
                    cmdPremier_Click(cmdPremier, new System.EventArgs());
                }
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas supprimer une entête créée par une autre personne.", "Suppression non autorisée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmEnteteDevis_Activated(object sender, EventArgs e)
        {
            DataTable rsEntete = default(DataTable);
            var modAdorsEntete = new ModAdo();

            rsEntete = modAdorsEntete.fc_OpenRecordSet("SELECT * FROM TypeEnteteDevis WHERE Utilisateur='" + General.gsUtilisateur + "'");

            if (rsEntete.Rows.Count > 0)
            {
                txtCode.Text = rsEntete.Rows[0]["Code"] + "";
                txtLibelle.Text = rsEntete.Rows[0]["Libelle"] + "";
                txtTexte.Text = rsEntete.Rows[0]["Texte"] + "";
                lblUtilisateur.Text = rsEntete.Rows[0]["Utilisateur"] + "";
            }

            modAdorsEntete?.Dispose();
        }
    }
}
