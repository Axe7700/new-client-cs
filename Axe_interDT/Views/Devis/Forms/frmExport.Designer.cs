﻿namespace Axe_interDT.Views.Devis.Forms
{
    partial class frmExport
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.optExportWord = new System.Windows.Forms.RadioButton();
            this.optExportExcel = new System.Windows.Forms.RadioButton();
            this.optExportAcrobat = new System.Windows.Forms.RadioButton();
            this.label33 = new System.Windows.Forms.Label();
            this.cmdExport = new System.Windows.Forms.Button();
            this.cmdRep = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.txtNomFichierSortie = new System.Windows.Forms.RichTextBox();
            this.txtDossier = new iTalk.iTalk_TextBox_Small2();
            this.txtNomFic = new iTalk.iTalk_TextBox_Small2();
            this.txtNoDevis = new iTalk.iTalk_TextBox_Small2();
            this.txtParametre = new iTalk.iTalk_TextBox_Small2();
            this.txtMasqueSomme = new iTalk.iTalk_TextBox_Small2();
            this.SuspendLayout();
            // 
            // optExportWord
            // 
            this.optExportWord.AutoSize = true;
            this.optExportWord.Checked = true;
            this.optExportWord.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optExportWord.Location = new System.Drawing.Point(37, 48);
            this.optExportWord.Name = "optExportWord";
            this.optExportWord.Size = new System.Drawing.Size(134, 23);
            this.optExportWord.TabIndex = 538;
            this.optExportWord.TabStop = true;
            this.optExportWord.Text = "Microsoft Word";
            this.optExportWord.UseVisualStyleBackColor = true;
            this.optExportWord.CheckedChanged += new System.EventHandler(this.optExportWord_CheckedChanged);
            // 
            // optExportExcel
            // 
            this.optExportExcel.AutoSize = true;
            this.optExportExcel.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optExportExcel.Location = new System.Drawing.Point(37, 74);
            this.optExportExcel.Name = "optExportExcel";
            this.optExportExcel.Size = new System.Drawing.Size(135, 23);
            this.optExportExcel.TabIndex = 538;
            this.optExportExcel.Text = "Microsoft Excel";
            this.optExportExcel.UseVisualStyleBackColor = true;
            this.optExportExcel.CheckedChanged += new System.EventHandler(this.optExportExcel_CheckedChanged);
            // 
            // optExportAcrobat
            // 
            this.optExportAcrobat.AutoSize = true;
            this.optExportAcrobat.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optExportAcrobat.Location = new System.Drawing.Point(37, 100);
            this.optExportAcrobat.Name = "optExportAcrobat";
            this.optExportAcrobat.Size = new System.Drawing.Size(134, 23);
            this.optExportAcrobat.TabIndex = 538;
            this.optExportAcrobat.Text = "Acrobat Reader";
            this.optExportAcrobat.UseVisualStyleBackColor = true;
            this.optExportAcrobat.CheckedChanged += new System.EventHandler(this.optExportAcrobat_CheckedChanged);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(34, 132);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(147, 16);
            this.label33.TabIndex = 540;
            this.label33.Text = "Nom du fichier exporté :";
            // 
            // cmdExport
            // 
            this.cmdExport.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdExport.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExport.FlatAppearance.BorderSize = 0;
            this.cmdExport.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExport.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExport.Image = global::Axe_interDT.Properties.Resources.Excel_32x32;
            this.cmdExport.Location = new System.Drawing.Point(179, 238);
            this.cmdExport.Margin = new System.Windows.Forms.Padding(0, 5, 0, 0);
            this.cmdExport.Name = "cmdExport";
            this.cmdExport.Size = new System.Drawing.Size(60, 40);
            this.cmdExport.TabIndex = 542;
            this.cmdExport.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdExport.UseVisualStyleBackColor = false;
            this.cmdExport.Click += new System.EventHandler(this.cmdExport_Click);
            // 
            // cmdRep
            // 
            this.cmdRep.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdRep.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdRep.FlatAppearance.BorderSize = 0;
            this.cmdRep.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRep.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdRep.ForeColor = System.Drawing.Color.White;
            this.cmdRep.ImageAlign = System.Drawing.ContentAlignment.TopLeft;
            this.cmdRep.Location = new System.Drawing.Point(291, 179);
            this.cmdRep.Margin = new System.Windows.Forms.Padding(2);
            this.cmdRep.Name = "cmdRep";
            this.cmdRep.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.cmdRep.Size = new System.Drawing.Size(60, 40);
            this.cmdRep.TabIndex = 541;
            this.cmdRep.Text = "...";
            this.cmdRep.UseVisualStyleBackColor = false;
            this.cmdRep.Click += new System.EventHandler(this.cmdRep_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(34, 19);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(167, 16);
            this.label1.TabIndex = 543;
            this.label1.Text = "Choisir le type d\'export";
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.cmdAnnuler.Location = new System.Drawing.Point(93, 238);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(60, 40);
            this.cmdAnnuler.TabIndex = 544;
            this.cmdAnnuler.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // txtNomFichierSortie
            // 
            this.txtNomFichierSortie.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNomFichierSortie.Location = new System.Drawing.Point(35, 160);
            this.txtNomFichierSortie.Name = "txtNomFichierSortie";
            this.txtNomFichierSortie.Size = new System.Drawing.Size(251, 59);
            this.txtNomFichierSortie.TabIndex = 545;
            this.txtNomFichierSortie.Text = "";
            // 
            // txtDossier
            // 
            this.txtDossier.AccAcceptNumbersOnly = false;
            this.txtDossier.AccAllowComma = false;
            this.txtDossier.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDossier.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDossier.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDossier.AccHidenValue = "";
            this.txtDossier.AccNotAllowedChars = null;
            this.txtDossier.AccReadOnly = false;
            this.txtDossier.AccReadOnlyAllowDelete = false;
            this.txtDossier.AccRequired = false;
            this.txtDossier.BackColor = System.Drawing.Color.White;
            this.txtDossier.CustomBackColor = System.Drawing.Color.White;
            this.txtDossier.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDossier.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtDossier.Location = new System.Drawing.Point(179, 48);
            this.txtDossier.Margin = new System.Windows.Forms.Padding(2);
            this.txtDossier.MaxLength = 32767;
            this.txtDossier.Multiline = false;
            this.txtDossier.Name = "txtDossier";
            this.txtDossier.ReadOnly = false;
            this.txtDossier.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDossier.Size = new System.Drawing.Size(78, 27);
            this.txtDossier.TabIndex = 582;
            this.txtDossier.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDossier.UseSystemPasswordChar = false;
            this.txtDossier.Visible = false;
            // 
            // txtNomFic
            // 
            this.txtNomFic.AccAcceptNumbersOnly = false;
            this.txtNomFic.AccAllowComma = false;
            this.txtNomFic.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNomFic.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNomFic.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNomFic.AccHidenValue = "";
            this.txtNomFic.AccNotAllowedChars = null;
            this.txtNomFic.AccReadOnly = false;
            this.txtNomFic.AccReadOnlyAllowDelete = false;
            this.txtNomFic.AccRequired = false;
            this.txtNomFic.BackColor = System.Drawing.Color.White;
            this.txtNomFic.CustomBackColor = System.Drawing.Color.White;
            this.txtNomFic.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNomFic.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtNomFic.Location = new System.Drawing.Point(179, 79);
            this.txtNomFic.Margin = new System.Windows.Forms.Padding(2);
            this.txtNomFic.MaxLength = 32767;
            this.txtNomFic.Multiline = false;
            this.txtNomFic.Name = "txtNomFic";
            this.txtNomFic.ReadOnly = false;
            this.txtNomFic.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNomFic.Size = new System.Drawing.Size(78, 27);
            this.txtNomFic.TabIndex = 583;
            this.txtNomFic.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNomFic.UseSystemPasswordChar = false;
            this.txtNomFic.Visible = false;
            // 
            // txtNoDevis
            // 
            this.txtNoDevis.AccAcceptNumbersOnly = false;
            this.txtNoDevis.AccAllowComma = false;
            this.txtNoDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoDevis.AccHidenValue = "";
            this.txtNoDevis.AccNotAllowedChars = null;
            this.txtNoDevis.AccReadOnly = false;
            this.txtNoDevis.AccReadOnlyAllowDelete = false;
            this.txtNoDevis.AccRequired = false;
            this.txtNoDevis.BackColor = System.Drawing.Color.White;
            this.txtNoDevis.CustomBackColor = System.Drawing.Color.White;
            this.txtNoDevis.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNoDevis.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtNoDevis.Location = new System.Drawing.Point(179, 108);
            this.txtNoDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoDevis.MaxLength = 32767;
            this.txtNoDevis.Multiline = false;
            this.txtNoDevis.Name = "txtNoDevis";
            this.txtNoDevis.ReadOnly = false;
            this.txtNoDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoDevis.Size = new System.Drawing.Size(78, 27);
            this.txtNoDevis.TabIndex = 584;
            this.txtNoDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoDevis.UseSystemPasswordChar = false;
            this.txtNoDevis.Visible = false;
            // 
            // txtParametre
            // 
            this.txtParametre.AccAcceptNumbersOnly = false;
            this.txtParametre.AccAllowComma = false;
            this.txtParametre.AccBackgroundColor = System.Drawing.Color.White;
            this.txtParametre.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtParametre.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtParametre.AccHidenValue = "";
            this.txtParametre.AccNotAllowedChars = null;
            this.txtParametre.AccReadOnly = false;
            this.txtParametre.AccReadOnlyAllowDelete = false;
            this.txtParametre.AccRequired = false;
            this.txtParametre.BackColor = System.Drawing.Color.White;
            this.txtParametre.CustomBackColor = System.Drawing.Color.White;
            this.txtParametre.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtParametre.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtParametre.Location = new System.Drawing.Point(273, 48);
            this.txtParametre.Margin = new System.Windows.Forms.Padding(2);
            this.txtParametre.MaxLength = 32767;
            this.txtParametre.Multiline = false;
            this.txtParametre.Name = "txtParametre";
            this.txtParametre.ReadOnly = false;
            this.txtParametre.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtParametre.Size = new System.Drawing.Size(78, 27);
            this.txtParametre.TabIndex = 585;
            this.txtParametre.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtParametre.UseSystemPasswordChar = false;
            this.txtParametre.Visible = false;
            // 
            // txtMasqueSomme
            // 
            this.txtMasqueSomme.AccAcceptNumbersOnly = false;
            this.txtMasqueSomme.AccAllowComma = false;
            this.txtMasqueSomme.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMasqueSomme.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMasqueSomme.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMasqueSomme.AccHidenValue = "";
            this.txtMasqueSomme.AccNotAllowedChars = null;
            this.txtMasqueSomme.AccReadOnly = false;
            this.txtMasqueSomme.AccReadOnlyAllowDelete = false;
            this.txtMasqueSomme.AccRequired = false;
            this.txtMasqueSomme.BackColor = System.Drawing.Color.White;
            this.txtMasqueSomme.CustomBackColor = System.Drawing.Color.White;
            this.txtMasqueSomme.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtMasqueSomme.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtMasqueSomme.Location = new System.Drawing.Point(273, 79);
            this.txtMasqueSomme.Margin = new System.Windows.Forms.Padding(2);
            this.txtMasqueSomme.MaxLength = 32767;
            this.txtMasqueSomme.Multiline = false;
            this.txtMasqueSomme.Name = "txtMasqueSomme";
            this.txtMasqueSomme.ReadOnly = false;
            this.txtMasqueSomme.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMasqueSomme.Size = new System.Drawing.Size(78, 27);
            this.txtMasqueSomme.TabIndex = 586;
            this.txtMasqueSomme.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMasqueSomme.UseSystemPasswordChar = false;
            this.txtMasqueSomme.Visible = false;
            // 
            // frmExport
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(363, 302);
            this.Controls.Add(this.txtMasqueSomme);
            this.Controls.Add(this.txtParametre);
            this.Controls.Add(this.txtNoDevis);
            this.Controls.Add(this.txtNomFic);
            this.Controls.Add(this.txtDossier);
            this.Controls.Add(this.txtNomFichierSortie);
            this.Controls.Add(this.cmdAnnuler);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.cmdExport);
            this.Controls.Add(this.cmdRep);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.optExportAcrobat);
            this.Controls.Add(this.optExportExcel);
            this.Controls.Add(this.optExportWord);
            this.Name = "frmExport";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmExport";
            this.Load += new System.EventHandler(this.frmExport_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.RadioButton optExportWord;
        public System.Windows.Forms.RadioButton optExportExcel;
        public System.Windows.Forms.RadioButton optExportAcrobat;
    
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Button cmdExport;
        public System.Windows.Forms.Button cmdRep;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.RichTextBox txtNomFichierSortie;
        public iTalk.iTalk_TextBox_Small2 txtDossier;
        public iTalk.iTalk_TextBox_Small2 txtNomFic;
        public iTalk.iTalk_TextBox_Small2 txtNoDevis;
        public iTalk.iTalk_TextBox_Small2 txtParametre;
        public iTalk.iTalk_TextBox_Small2 txtMasqueSomme;
    }
}