﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Diagnostics;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Reflection;using System.Windows.Forms;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmAfficheFichier : Form
    {
        public frmAfficheFichier()
        {
             InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void LstFichier_SelectedIndexChanged(object sender, EventArgs e)
        {
            try
            {
                string text = LstFichier.GetItemText(LstFichier.SelectedItem);
                Process.Start(text);
            }
            catch(Exception ex) {

                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le fichier spécifié est introuvable", "", MessageBoxButtons.OK, MessageBoxIcon.Error);

            }
        }

       
    }
}
