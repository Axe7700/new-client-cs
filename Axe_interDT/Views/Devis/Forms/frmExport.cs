﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Diagnostics;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Runtime.InteropServices;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using Microsoft.VisualBasic;
namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmExport : Form
    {
        public frmExport()
        {
            InitializeComponent();
        }
        const string crEFTPortableDocFormat = "31";
        const string crEFTExcel97 = "36";
        const string crEFTWordForWindows = "14";
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            this.Visible = false;
        }

        private void cmdExport_Click(object sender, EventArgs e)
        {
            if (optExportAcrobat.Checked == true)
            {
                fc_Export((txtNomFichierSortie.Text), (txtDossier.Text), "DTDevisSQL-V9.rpt", (txtParametre.Text), (txtMasqueSomme.Text), (txtNoDevis.Text), false, "", crEFTPortableDocFormat);
            }
            else if (optExportExcel.Checked == true)
            {
                fc_Export((txtNomFichierSortie.Text), (txtDossier.Text), "DTDevisSQL-V9.rpt", (txtParametre.Text), (txtMasqueSomme.Text), (txtNoDevis.Text), false, "", crEFTExcel97);
            }
            else if (optExportWord.Checked == true)
            {
                fc_Export((txtNomFichierSortie.Text), (txtDossier.Text), "DTDevisSQL-V9.rpt", (txtParametre.Text), (txtMasqueSomme.Text), (txtNoDevis.Text), false, "", crEFTWordForWindows);
            }
            this.Visible = false;
        }
        private void testing(string file)
        {
            //Process[] Processes;
            //Processes = Process.GetProcessesByName("EXCEL");
            //foreach (var proc in Processes)
            //{
            //    var hand = proc.Handle;
            //    if (!proc.HasExited)
            //    {
            //        var t = proc.Modules.Cast<ProcessModule>().Where(b => b.FileName.ToLower() == file.ToLower());
            //        var a = proc.Modules.Cast<ProcessModule>().ToList().RemoveAll(ta => { return ta.FileName.ToLower() == file.ToLower(); });
            //    }
            //}
            //Correcteur.closeWindowOfExcel(file);
            CloseExcelWorkbook(file);
        }
        private void CloseExcelWorkbook(string workbookName, string option = "Excel")
        {
            try
            {
                ///This function close opened file (excel, word or pdf).
                if (option.ToLower() == "excel")
                {
                    ///This code used to close excel file to ignore exception we can't access this file because it's used by onether process.
                    Process[] plist = Process.GetProcessesByName("Excel", ".");
                    if (plist.Length == 0)
                        return;

                    Object obj = Marshal.GetActiveObject("Excel.Application");

                    Microsoft.Office.Interop.Excel.Application excelAppl = (Microsoft.Office.Interop.Excel.Application)obj;

                    Microsoft.Office.Interop.Excel.Workbooks workbooks = excelAppl.Workbooks;
                    foreach (Microsoft.Office.Interop.Excel.Workbook wkbk in workbooks)
                    {
                        if (wkbk.Name == workbookName)
                            wkbk.Close();
                    }
                    GC.Collect();
                    GC.WaitForPendingFinalizers();
                    if (workbooks != null)
                        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbooks);
                    System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excelAppl);
                    GC.Collect();
                }
                else if (option.ToLower() == "word")
                {
                    ///This code used to close word file to ignore exception we can't access this file because it's used by onether process.
                    Process[] plist = Process.GetProcessesByName("WinWord");
                    if (plist.Length == 0)
                        return;
                    if (plist.Length > 1)
                    {
                        foreach (var proc in plist)
                        {
                            if (proc.MainWindowTitle.ToLower() == workbookName.ToLower() + "  -  Mode de compatibilité - Word".ToLower())
                                proc.Kill();
                        }
                    }
                    else
                    {
                        Object obj = Marshal.GetActiveObject("Word.Application");
                        if (obj == null)
                            return;
                        Microsoft.Office.Interop.Word.Application excelAppl = (Microsoft.Office.Interop.Word.Application)obj;

                        var workbooks = excelAppl.Documents;
                        foreach (Microsoft.Office.Interop.Word.Document wkbk in workbooks)
                        {
                            if (wkbk.Name == workbookName)
                                wkbk.Close();
                        }
                        //dispose
                        //workbooks.Close(); //this would close all workbooks
                        GC.Collect();
                        GC.WaitForPendingFinalizers();
                        if (workbooks != null)
                            System.Runtime.InteropServices.Marshal.FinalReleaseComObject(workbooks);
                        //excelAppl.Quit(); //would close the excel application
                        System.Runtime.InteropServices.Marshal.FinalReleaseComObject(excelAppl);
                        GC.Collect();
                    }

                }

                else if(option.ToLower() == "pdf")
                {
                    Correcteur.closeWindowOfExcel(workbookName);//if pdf file opened in Edge explorer we close the opened pdf file.
                    Correcteur.closeWindowOfExcel(workbookName+ " - Adobe Acrobat Reader DC", "AcrobatSDIWindow");//if the pdf file opened in acrobat reader ,we close opened pdf file, but in case adobe acrobat i have a problem, this operation needs a confirmation to close this opened file. 
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                MessageBox.Show(ex.Message);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNomFic"></param>
        /// <param name="sNomRep"></param>
        /// <param name="sNomReport"></param>
        /// <param name="sParametre"></param>
        /// <param name="sMasqueSomme"></param>
        /// <param name="sNomDevis"></param>
        /// <param name="blnInsert"></param>
        /// <param name="sExtention"></param>
        /// <param name="sTypeExport"></param>
        /// <returns></returns>
        public string fc_Export(string sNomFic, string sNomRep, string sNomReport = "DTDevisSQL-V9.rpt", string sParametre = "", string sMasqueSomme = "", string sNomDevis = "", bool blnInsert = false, string sExtention = "pdf", string sTypeExport = null)
        {
            string functionReturnValue = null;
            object fc_ExportDevis = null;

            string sCodeImmeuble = null;
            string sTitreDevis = null;
            int i = 0;
            object x = null;
            string sFiltreDevis1 = null;
            string strTop = null;
            DataTable rs = default(DataTable);
            ReportDocument Report = null;
            object RetVal = null;

            try
            {
                rs = new DataTable();
                General.SQL = "SELECT  DevisEntete.NumeroDevis,DevisEntete.CodeImmeuble,DevisEntete.TitreDevis FROM DevisEntete WHERE DevisEntete.NumeroDevis='" + sNomDevis + "'";
                var Modado = new ModAdo();
                rs = Modado.fc_OpenRecordSet(General.SQL);

                //rs.Open(General.SQL, General.adocnn, ADODB.CursorTypeEnum.adOpenStatic, ADODB.LockTypeEnum.adLockOptimistic);

                if (rs.Rows.Count > 0)
                {

                    Report = new ReportDocument();
                    Report.Load(General.gsRpt + sNomReport);


                    sNomFic = General.Replace(sNomFic + "", "/", "-");

                    if (Dossier.fc_ControleFichier(sNomRep + sNomFic) == true)
                    {
                        try
                        {
                            var file = File.Open(sNomRep + sNomFic, FileMode.Open);
                            file.Close();
                            file.Dispose();
                        }
                        catch (Exception ex)
                        {
                            if (sTypeExport == "31")
                                CloseExcelWorkbook(sNomFic, "pdf");
                            else if (sTypeExport == "36")
                                CloseExcelWorkbook(sNomFic, "excel");
                            else if (sTypeExport == "14")
                                CloseExcelWorkbook(sNomFic, "word");
                        }
                        if (sTypeExport == "31")
                            CloseExcelWorkbook(sNomFic, "pdf");
                        File.Delete(sNomRep + sNomFic);
                    }
                    if (sNomReport == "DTDevisSQL-V9.rpt")
                    {
                        Report.DataDefinition.FormulaFields["Parametre"].Text = "'" + sParametre + "'";
                        Report.DataDefinition.FormulaFields["MasqueSomme "].Text = "'" + sMasqueSomme + "'";
                    }

                    //Report.ParameterFields[1].DefaultValues.Add(rs.Rows[0]["NumeroDevis"]);
                    //Report.ParameterFields["NoDevis"].CurrentValues.Add(rs.Rows[0]["NumeroDevis"].ToString());
                    Report.SetParameterValue("NoDevis", rs.Rows[0]["NumeroDevis"].ToString());


                    if (sTypeExport == "31") //exporter en pdf
                    {
                        Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, sNomRep + sNomFic);
                    }
                    else if (sTypeExport == "36") //exporter en Excel
                    {
                        Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.Excel, sNomRep + sNomFic);
                    }
                    else if (sTypeExport == "14")//exporter en Word
                    {
                        Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.WordForWindows, sNomRep + sNomFic);
                    }

                }

                fc_ExportDevis = sNomRep + sNomFic;
                ModuleAPI.Ouvrir(sNomRep + sNomFic);
                Modado.Close();
                rs = null;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
                return functionReturnValue;
            }



        }
        
    

       
        /// <summary>
        /// Tetsed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optExportWord_CheckedChanged(object sender, EventArgs e)
        {
            if (txtNomFichierSortie.Text != "")
            {
                txtNomFichierSortie.Text = General.Left(txtNomFichierSortie.Text, General.Len(txtNomFichierSortie.Text) - 3) + "doc";
            }
            else
            {
                if (txtNomFic.Text != "")
                {
                    txtNomFichierSortie.Text = txtNomFic.Text + ".doc";
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optExportExcel_CheckedChanged(object sender, EventArgs e)
        {
            if (txtNomFichierSortie.Text != "")
            {
                txtNomFichierSortie.Text = General.Left(txtNomFichierSortie.Text, General.Len(txtNomFichierSortie.Text) - 3) + "xls";
            }
            else
            {
                if (txtNomFic.Text != "")
                {
                    txtNomFichierSortie.Text = txtNomFic.Text + ".xls";
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optExportAcrobat_CheckedChanged(object sender, EventArgs e)
        {
            if (txtNomFichierSortie.Text != "")
            {
                txtNomFichierSortie.Text = General.Left(txtNomFichierSortie.Text, General.Len(txtNomFichierSortie.Text) - 3) + "pdf";
            }
            else
            {
                if (txtNomFic.Text != "")
                {
                    txtNomFichierSortie.Text = txtNomFic.Text + ".pdf";
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmExport_Load(object sender, EventArgs e)
        {
            if (txtNomFic.Text != "")
            {
                if (optExportAcrobat.Checked)
                {
                    txtNomFichierSortie.Text = txtNomFic.Text + ".pdf";
                }
                else if (optExportExcel.Checked)
                {
                    txtNomFichierSortie.Text = txtNomFic.Text + ".xls";
                }
                else
                {
                    txtNomFichierSortie.Text = txtNomFic.Text + ".doc";
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRep_Click(object sender, EventArgs e)
        {
            OpenFileDialog openFileDialog1 = new OpenFileDialog();
            openFileDialog1.InitialDirectory = @"C:\";
            openFileDialog1.CheckFileExists = true;
            openFileDialog1.CheckPathExists = true;

            openFileDialog1.FilterIndex = 2;
            openFileDialog1.RestoreDirectory = true;
            openFileDialog1.ReadOnlyChecked = true;
            openFileDialog1.ShowReadOnly = true;


            if (optExportAcrobat.Checked)
            {
                openFileDialog1.Filter = "Pdf Files|*.pdf";
            }
            else if (optExportExcel.Checked)
            {
                openFileDialog1.Filter = "Excel |*.xlsx";
            }
            else if (optExportWord.Checked)
            {
                openFileDialog1.Filter = "Word Documents|*.doc";
            }
            if (openFileDialog1.ShowDialog() == DialogResult.OK)
            {

                txtNomFichierSortie.Text = openFileDialog1.FileName;
                txtDossier.Text = General.Left(openFileDialog1.FileName, General.Len(openFileDialog1.FileName) - General.Len(openFileDialog1.FileName));

            }
        }
    }
}
