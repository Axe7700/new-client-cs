﻿namespace Axe_interDT.Views.Devis.Forms
{
    partial class frmEnteteDevis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmEnteteDevis));
            this.txtTexte = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.lblUtilisateur = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtLibelle = new iTalk.iTalk_TextBox_Small2();
            this.txtCode = new iTalk.iTalk_TextBox_Small2();
            this.label4 = new System.Windows.Forms.Label();
            this.CmdRechercher = new System.Windows.Forms.Button();
            this.cmdSupprimer = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.cmdPrecedent = new System.Windows.Forms.Button();
            this.cmdSuivant = new System.Windows.Forms.Button();
            this.cmdDernier = new System.Windows.Forms.Button();
            this.cmdPremier = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtTexte
            // 
            this.txtTexte.AccAcceptNumbersOnly = false;
            this.txtTexte.AccAllowComma = false;
            this.txtTexte.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTexte.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTexte.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtTexte.AccHidenValue = "";
            this.txtTexte.AccNotAllowedChars = null;
            this.txtTexte.AccReadOnly = false;
            this.txtTexte.AccReadOnlyAllowDelete = false;
            this.txtTexte.AccRequired = false;
            this.txtTexte.BackColor = System.Drawing.Color.White;
            this.txtTexte.CustomBackColor = System.Drawing.Color.White;
            this.txtTexte.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTexte.ForeColor = System.Drawing.Color.Black;
            this.txtTexte.Location = new System.Drawing.Point(148, 114);
            this.txtTexte.Margin = new System.Windows.Forms.Padding(2);
            this.txtTexte.MaxLength = 32767;
            this.txtTexte.Multiline = true;
            this.txtTexte.Name = "txtTexte";
            this.txtTexte.ReadOnly = false;
            this.txtTexte.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTexte.Size = new System.Drawing.Size(545, 247);
            this.txtTexte.TabIndex = 503;
            this.txtTexte.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTexte.UseSystemPasswordChar = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(12, 54);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(144, 19);
            this.label33.TabIndex = 502;
            this.label33.Text = "Nom de l\'utilisateur";
            // 
            // lblUtilisateur
            // 
            this.lblUtilisateur.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblUtilisateur.Location = new System.Drawing.Point(151, 54);
            this.lblUtilisateur.Name = "lblUtilisateur";
            this.lblUtilisateur.Size = new System.Drawing.Size(211, 17);
            this.lblUtilisateur.TabIndex = 504;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(12, 114);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(129, 19);
            this.label2.TabIndex = 505;
            this.label2.Text = "Texte de l\'entête";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(12, 82);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(135, 19);
            this.label3.TabIndex = 506;
            this.label3.Text = "Libellé de l\'entête";
            // 
            // txtLibelle
            // 
            this.txtLibelle.AccAcceptNumbersOnly = false;
            this.txtLibelle.AccAllowComma = false;
            this.txtLibelle.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibelle.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibelle.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLibelle.AccHidenValue = "";
            this.txtLibelle.AccNotAllowedChars = null;
            this.txtLibelle.AccReadOnly = false;
            this.txtLibelle.AccReadOnlyAllowDelete = false;
            this.txtLibelle.AccRequired = false;
            this.txtLibelle.BackColor = System.Drawing.Color.White;
            this.txtLibelle.CustomBackColor = System.Drawing.Color.White;
            this.txtLibelle.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtLibelle.ForeColor = System.Drawing.Color.Black;
            this.txtLibelle.Location = new System.Drawing.Point(148, 82);
            this.txtLibelle.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibelle.MaxLength = 32767;
            this.txtLibelle.Multiline = false;
            this.txtLibelle.Name = "txtLibelle";
            this.txtLibelle.ReadOnly = false;
            this.txtLibelle.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibelle.Size = new System.Drawing.Size(545, 27);
            this.txtLibelle.TabIndex = 507;
            this.txtLibelle.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibelle.UseSystemPasswordChar = false;
            // 
            // txtCode
            // 
            this.txtCode.AccAcceptNumbersOnly = false;
            this.txtCode.AccAllowComma = false;
            this.txtCode.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCode.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCode.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCode.AccHidenValue = "";
            this.txtCode.AccNotAllowedChars = null;
            this.txtCode.AccReadOnly = false;
            this.txtCode.AccReadOnlyAllowDelete = false;
            this.txtCode.AccRequired = false;
            this.txtCode.BackColor = System.Drawing.Color.White;
            this.txtCode.CustomBackColor = System.Drawing.Color.White;
            this.txtCode.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCode.ForeColor = System.Drawing.Color.Black;
            this.txtCode.Location = new System.Drawing.Point(27, 181);
            this.txtCode.Margin = new System.Windows.Forms.Padding(2);
            this.txtCode.MaxLength = 32767;
            this.txtCode.Multiline = false;
            this.txtCode.Name = "txtCode";
            this.txtCode.ReadOnly = false;
            this.txtCode.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCode.Size = new System.Drawing.Size(54, 27);
            this.txtCode.TabIndex = 508;
            this.txtCode.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCode.UseSystemPasswordChar = false;
            this.txtCode.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 16F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(12, 9);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(169, 26);
            this.label4.TabIndex = 509;
            this.label4.Text = "Entête de Devis";
            // 
            // CmdRechercher
            // 
            this.CmdRechercher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdRechercher.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdRechercher.FlatAppearance.BorderSize = 0;
            this.CmdRechercher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdRechercher.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdRechercher.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.CmdRechercher.Location = new System.Drawing.Point(269, 6);
            this.CmdRechercher.Margin = new System.Windows.Forms.Padding(2);
            this.CmdRechercher.Name = "CmdRechercher";
            this.CmdRechercher.Size = new System.Drawing.Size(60, 35);
            this.CmdRechercher.TabIndex = 510;
            this.CmdRechercher.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.CmdRechercher.UseVisualStyleBackColor = false;
            this.CmdRechercher.Visible = false;
            this.CmdRechercher.Click += new System.EventHandler(this.CmdRechercher_Click);
            // 
            // cmdSupprimer
            // 
            this.cmdSupprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdSupprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSupprimer.FlatAppearance.BorderSize = 0;
            this.cmdSupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSupprimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSupprimer.Image = global::Axe_interDT.Properties.Resources.Trash_24x24;
            this.cmdSupprimer.Location = new System.Drawing.Point(333, 6);
            this.cmdSupprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSupprimer.Name = "cmdSupprimer";
            this.cmdSupprimer.Size = new System.Drawing.Size(60, 35);
            this.cmdSupprimer.TabIndex = 511;
            this.cmdSupprimer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdSupprimer.UseVisualStyleBackColor = false;
            this.cmdSupprimer.Click += new System.EventHandler(this.cmdSupprimer_Click);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.cmdAnnuler.Location = new System.Drawing.Point(397, 6);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(60, 35);
            this.cmdAnnuler.TabIndex = 512;
            this.cmdAnnuler.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAjouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAjouter.Image = global::Axe_interDT.Properties.Resources.Add_File_24x24;
            this.cmdAjouter.Location = new System.Drawing.Point(461, 6);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(60, 35);
            this.cmdAjouter.TabIndex = 513;
            this.cmdAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdAjouter.UseVisualStyleBackColor = false;
            this.cmdAjouter.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(525, 7);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 514;
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // cmdPrecedent
            // 
            this.cmdPrecedent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdPrecedent.FlatAppearance.BorderSize = 0;
            this.cmdPrecedent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPrecedent.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdPrecedent.Image = global::Axe_interDT.Properties.Resources.previous_16;
            this.cmdPrecedent.Location = new System.Drawing.Point(616, 12);
            this.cmdPrecedent.Margin = new System.Windows.Forms.Padding(0);
            this.cmdPrecedent.Name = "cmdPrecedent";
            this.cmdPrecedent.Size = new System.Drawing.Size(25, 23);
            this.cmdPrecedent.TabIndex = 518;
            this.cmdPrecedent.UseVisualStyleBackColor = false;
            this.cmdPrecedent.Click += new System.EventHandler(this.cmdPrecedent_Click);
            // 
            // cmdSuivant
            // 
            this.cmdSuivant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSuivant.FlatAppearance.BorderSize = 0;
            this.cmdSuivant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSuivant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdSuivant.Image = ((System.Drawing.Image)(resources.GetObject("cmdSuivant.Image")));
            this.cmdSuivant.Location = new System.Drawing.Point(641, 12);
            this.cmdSuivant.Margin = new System.Windows.Forms.Padding(0);
            this.cmdSuivant.Name = "cmdSuivant";
            this.cmdSuivant.Size = new System.Drawing.Size(25, 23);
            this.cmdSuivant.TabIndex = 517;
            this.cmdSuivant.UseVisualStyleBackColor = false;
            this.cmdSuivant.Click += new System.EventHandler(this.cmdSuivant_Click);
            // 
            // cmdDernier
            // 
            this.cmdDernier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdDernier.FlatAppearance.BorderSize = 0;
            this.cmdDernier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDernier.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdDernier.Image = global::Axe_interDT.Properties.Resources.last_16;
            this.cmdDernier.Location = new System.Drawing.Point(666, 12);
            this.cmdDernier.Margin = new System.Windows.Forms.Padding(0);
            this.cmdDernier.Name = "cmdDernier";
            this.cmdDernier.Size = new System.Drawing.Size(25, 23);
            this.cmdDernier.TabIndex = 516;
            this.cmdDernier.UseVisualStyleBackColor = false;
            this.cmdDernier.Click += new System.EventHandler(this.cmdDernier_Click);
            // 
            // cmdPremier
            // 
            this.cmdPremier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdPremier.FlatAppearance.BorderSize = 0;
            this.cmdPremier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPremier.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdPremier.Image = global::Axe_interDT.Properties.Resources.first_16;
            this.cmdPremier.Location = new System.Drawing.Point(591, 12);
            this.cmdPremier.Margin = new System.Windows.Forms.Padding(0);
            this.cmdPremier.Name = "cmdPremier";
            this.cmdPremier.Size = new System.Drawing.Size(25, 23);
            this.cmdPremier.TabIndex = 515;
            this.cmdPremier.UseVisualStyleBackColor = false;
            this.cmdPremier.Click += new System.EventHandler(this.cmdPremier_Click);
            // 
            // frmEnteteDevis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(704, 366);
            this.Controls.Add(this.cmdPrecedent);
            this.Controls.Add(this.cmdSuivant);
            this.Controls.Add(this.cmdDernier);
            this.Controls.Add(this.cmdPremier);
            this.Controls.Add(this.CmdSauver);
            this.Controls.Add(this.cmdAjouter);
            this.Controls.Add(this.cmdAnnuler);
            this.Controls.Add(this.cmdSupprimer);
            this.Controls.Add(this.CmdRechercher);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtCode);
            this.Controls.Add(this.txtLibelle);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.lblUtilisateur);
            this.Controls.Add(this.txtTexte);
            this.Controls.Add(this.label33);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(720, 405);
            this.MinimumSize = new System.Drawing.Size(720, 405);
            this.Name = "frmEnteteDevis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Gestion des entête de devis";
            this.Activated += new System.EventHandler(this.frmEnteteDevis_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public iTalk.iTalk_TextBox_Small2 txtTexte;
        private System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label lblUtilisateur;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 txtLibelle;
        public iTalk.iTalk_TextBox_Small2 txtCode;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Button CmdRechercher;
        public System.Windows.Forms.Button cmdSupprimer;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button cmdAjouter;
        public System.Windows.Forms.Button CmdSauver;
        public System.Windows.Forms.Button cmdPrecedent;
        public System.Windows.Forms.Button cmdSuivant;
        public System.Windows.Forms.Button cmdDernier;
        public System.Windows.Forms.Button cmdPremier;
    }
}