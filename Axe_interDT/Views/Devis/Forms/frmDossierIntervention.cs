﻿using Axe_interDT.Shared;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmDossierIntervention : Form
    {
        public string File1Pattern = "";
        public frmDossierIntervention()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdQuitter_Click(object sender, EventArgs e)
        {
            General.saveInReg("App", "File1EnCours", File1.Path.ToString());
            General.saveInReg("App", "Drive1EnCours", Drive1.Text);
            General.saveInReg("App", "Dir1EnCours", Dir1.Path.ToString());

            this.Visible = false;
            Close();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRecup_Click(object sender, EventArgs e)
        {
            if (File1.SelectedIndex != (-1))
            {
                //Dossier.fc_DeplaceFichier(File1.Tag.ToString() + "\\" + File1.Items[File1.SelectedIndex], FileIntervention.Tag.ToString() + "\\" + File1.Items[File1.SelectedIndex]);
                Dossier.fc_DeplaceFichier(File1.Path.ToString() + "\\" + File1.Items[File1.SelectedIndex], FileIntervention.Path.ToString() + "\\" + File1.Items[File1.SelectedIndex]);
                File1.Refresh();
                FileIntervention.Refresh();
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRendre_Click(object sender, EventArgs e)
        {
            if (FileIntervention.SelectedIndex != (-1))
            {
                //Dossier.fc_DeplaceFichier(FileIntervention.Tag + "\\" + FileIntervention.Items[FileIntervention.SelectedIndex], File1.Tag + "\\" + FileIntervention.Items[FileIntervention.SelectedIndex]);
                Dossier.fc_DeplaceFichier(FileIntervention.Path + "\\" + FileIntervention.Items[FileIntervention.SelectedIndex], File1.Path + "\\" + FileIntervention.Items[FileIntervention.SelectedIndex]);
                //General.RefreshListBox(File1);
                //General.RefreshListBox(FileIntervention);
                File1.Refresh();
                FileIntervention.Refresh();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdToutRecup_Click(object sender, EventArgs e)
        {
            int i = 0;

            var tmp = File1.Items.Count - 1;

            for (i = 0; i <= tmp; i++)
            {
                //Dossier.fc_DeplaceFichier(File1.Tag + "\\" + File1.Items[0], FileIntervention.Tag + "\\" + File1.Items[0]);
                Dossier.fc_DeplaceFichier(File1.Path + "\\" + File1.Items[0], FileIntervention.Path + "\\" + File1.Items[0]);
                //File1.Items.RemoveAt(0);
                File1.Refresh();
                FileIntervention.Refresh();
            }
            //General.RefreshListBox(File1);
            //General.RefreshListBox(FileIntervention);
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdToutRendre_Click(object sender, EventArgs e)
        {
            int i = 0;

            var tmp = FileIntervention.Items.Count - 1;

            for (i = 0; i <= tmp; i++)
            {
                //Dossier.fc_DeplaceFichier(FileIntervention.Tag + "\\" + FileIntervention.Items[0], File1.Tag + "\\" + FileIntervention.Items[0]);
                Dossier.fc_DeplaceFichier(FileIntervention.Path + "\\" + FileIntervention.Items[0], File1.Path + "\\" + FileIntervention.Items[0]);
                //FileIntervention.Items.RemoveAt(0);
                File1.Refresh();
                FileIntervention.Refresh();
            }
            //General.RefreshListBox(File1);
            //General.RefreshListBox(FileIntervention);
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Dir1_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //try
            //{
            //    File1.Tag = Dir1.SelectedNode.Tag;
            //    General.RefreshListBox(File1);
            //}
            //catch (Exception ex)
            //{
            //    Program.SaveException(ex);
            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
            //}
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DirNoInter_AfterSelect(object sender, TreeViewEventArgs e)
        {
            //try
            //{
            //    FileIntervention.Tag = DirNoInter.SelectedNode.Tag;
            //    General.RefreshListBox(FileIntervention);
            //}
            //catch (Exception ex)
            //{
            //    Program.SaveException(ex);
            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
            //}
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Drive1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //try
            //{
            //    if (Drive1.Text.Contains("C:") && Drive1.Text.Contains("c:"))
            //    {
            //        Dir1111.Tag = Drive1.Text;
            //        Dir1.Tag = Drive1.Text;
            //    }
            //    else
            //    {
            //        Dir1111.Tag = "C:\\";
            //        Dir1.Tag = "C:\\";
            //    }
            //    //Dir1111.Tag = Drive1.Text;
            //    Dir1.Tag = Drive1.Text;
            //    File1.Tag = Drive1.Text;
            //    Dir1.Nodes.Clear();
            //    if (Dir1.Tag != null && Directory.Exists(Dir1.Tag.ToString()))
            //        fillTreeView(Dir1.Tag.ToString(), Dir1, File1);
            //}
            //catch (Exception ex)
            //{
            //    Program.SaveException(ex);
            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
            //}
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void File1_DoubleClick(object sender, EventArgs e)
        {
            if (File1.SelectedIndex != (-1))
            {
                ModuleAPI.Ouvrir(File1.Path + "\\" + File1.Items[File1.SelectedIndex]);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileIntervention_DoubleClick(object sender, EventArgs e)
        {
            if (FileIntervention.SelectedIndex != (-1))
            {
                ModuleAPI.Ouvrir(FileIntervention.Path + "\\" + FileIntervention.Items[FileIntervention.SelectedIndex]);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmDossierIntervention_Load(object sender, EventArgs e)
        {
            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            if (Convert.ToDouble(General.AfficheRetour) == 0)
            {
                cmdRendre.Visible = false;
                cmdToutRendre.Visible = false;
            }
            else
            {
                cmdRendre.Visible = true;
                cmdToutRendre.Visible = true;
            }
            //if (Dir1.Tag != null && Directory.Exists(Dir1.Tag.ToString()))
            //    fillTreeView(Dir1.Tag.ToString(), Dir1, File1);

            //var fileInfo = new DirectoryInfo(DirNoInter.Tag.ToString());
            //if (fileInfo.Exists)
            //{
            //    //string pattern = ".exe;.dll;.sys;.ocx;.oca;.chi;.propdesc;.ins;.dat;.mshc;.cab;.targets;.xml;.prg;.dbf;.app;.rif;.fon;.bmp;.msk;.mem";

            //    string pattern = ".doc;.xls;.tif;.pcx;.htm;.html;.txt;.bmp;.gif;.jpg;.png;.jpeg;.pdf;.msg;.docx;.accdb;.bmp;.contact;.pptx;.pub;.xlsx;.ppt;.mp3;.mp4;.wav.;.amr";
            //    var extensionsPattern = pattern.Split(';');

            //    int filesCount = fileInfo.GetFiles().Where(d => extensionsPattern.Contains(d.Extension)).Count();

            //    int DirectoriesCount = fileInfo.GetDirectories().Length;

            //    string text = fileInfo.Name + " ( " + DirectoriesCount + " dossiers / " + filesCount + " fichiers )";

            //    UltraTreeNode firstChildNode = new UltraTreeNode(fileInfo.FullName, text) { Tag = fileInfo.FullName };

            //    firstChildNode.Override.NodeAppearance.Image = Properties.Resources.folder_opened16;

            //    firstChildNode.Expanded = true;

            //    loadParentOfDirectory(DirNoInter.Tag.ToString(), firstChildNode);//this function fill dirNointer treeview

            //    General.chargerNodes(firstChildNode.Tag.ToString(), firstChildNode);
            //    DirNoInter.ActiveNode = firstChildNode;
            //}
            //Dir1.Nodes.Clear();
            //fc_fillTreevDesc(Dir1, File1);
            //fc_fillTreevDesc(DirNoInter, FileIntervention);
            
            //General.ListDirectory(DirNoInter, DirNoInter.Tag.ToString());
            //General.ListDirectory(Dir1, Dir1.Tag.ToString());
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="treeview"></param>
        /// <param name="list"></param>
        private void fc_fillTreevDesc(UltraTree treeview, ListBox list)
        {
            if (treeview.Tag != null)
            {
                var fileInfo = new DirectoryInfo(treeview.Tag.ToString());
                if (fileInfo.Exists)
                {
                    string pattern = ".doc;.xls;.tif;.pcx;.htm;.html;.txt;.bmp;.gif;.jpg;.png;.jpeg;.pdf;.msg;.docx;.accdb;.bmp;.contact;.pptx;.pub;.xlsx;.ppt;.mp3;.mp4;.wav.;.amr";
                    var extensionsPattern = pattern.Split(';');

                    int filesCount = fileInfo.GetFiles().Where(d => extensionsPattern.Contains(d.Extension)).Count();

                    int DirectoriesCount = fileInfo.GetDirectories().Length;

                    string text = fileInfo.Name + " ( " + DirectoriesCount + " dossiers / " + filesCount + " fichiers )";

                    UltraTreeNode firstChildNode = new UltraTreeNode(fileInfo.FullName, text) { Tag = fileInfo.FullName };

                    firstChildNode.Override.NodeAppearance.Image = Properties.Resources.folder_opened16;

                    firstChildNode.Expanded = true;
                    if (fileInfo.Parent == null)
                    {
                        treeview.Nodes.Add(firstChildNode);
                    }
                    else
                    {
                        loadParentOfDirectory(treeview.Tag.ToString(), firstChildNode, treeview, list);//this function fill dirNointer treeview
                    }
                    list.Tag = firstChildNode.Tag;
                    treeview.Tag = firstChildNode.Tag;
                    General.chargerNodes(firstChildNode.Tag.ToString(), firstChildNode);
                    treeview.ActiveNode = firstChildNode;
                }
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="chemin"></param>
        /// <param name="nodeChild"></param>
        private void loadParentOfDirectory(string chemin, UltraTreeNode nodeChild, UltraTree treeview, ListBox list)
        {
            var info = new DirectoryInfo(chemin);
            if (info.Parent != null && info.Exists)
            {
                string pattern = ".doc;.xls;.tif;.pcx;.htm;.html;.txt;.bmp;.gif;.jpg;.png;.jpeg;.pdf;.msg;.docx;.accdb;.bmp;.contact;.pptx;.pub;.xlsx;.ppt;.mp3;.mp4;.wav.;.amr";

                var extensionsPattern = pattern.Split(';');

                int filesCount = info.Parent.GetFiles().Where(d => extensionsPattern.Contains(d.Extension)).Count();

                int DirectoriesCount = info.Parent.GetDirectories().Length;

                string text = info.Parent.Name + " ( " + DirectoriesCount + " dossiers / " + filesCount + " fichiers )";

                UltraTreeNode nodeParent = new UltraTreeNode(info.Parent.FullName, text) { Tag = info.Parent.FullName }; //create new node of parent Directory
                nodeParent.Override.NodeAppearance.Image = Properties.Resources.folder_opened16;
                nodeParent.Nodes.Add(nodeChild);//add node child to nodeParent 
                loadParentOfDirectory(info.Parent.FullName, nodeParent, treeview, list);//pass to this function the parent node & the fullname of the parent directoy 
            }
            else if (info.Exists && info.Parent == null)
            {
                treeview.Nodes.Add(nodeChild);
                treeview.ExpandAll();//when we don't have a parent directoy so we just add all nodes to the treeview
                //treeview.ExpandAll();
                General.chargerFiles(treeview.Tag.ToString(), list);
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="path"></param>
        /// <param name="treev"></param>
        /// <param name="list"></param>
        private void fillTreeView(string path, UltraTree treev, ListBox list)
        {
            if (treev.Tag != null && Directory.Exists(treev.Tag.ToString()))
            {
                if (treev.Nodes.Count > 0)
                    treev.Nodes.Clear();
                var FileInfo = new DirectoryInfo(treev.Tag.ToString());
                UltraTreeNode node = new UltraTreeNode(FileInfo.FullName, FileInfo.Name) { Tag = FileInfo.FullName };
                node.Expanded = true;
                node.Override.NodeAppearance.Image = Properties.Resources.folder_opened16;
                treev.Nodes.Add(node);
                General.chargerNodes(node.Tag.ToString(), node);
                General.chargerFiles(node.Tag.ToString(), list);
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Dir1_BeforeSelect(object sender, Infragistics.Win.UltraWinTree.BeforeSelectEventArgs e)
        {
            //try
            //{
            //    if (e.NewSelections.Count > 0)
            //    {
            //        var node = e.NewSelections[0];
            //        File1.Tag = node.Tag;
            //        if (node.Nodes.Count == 0)
            //        {
            //            General.chargerNodes(node.Tag.ToString(), node);
            //            node.Expanded = true;
            //        }
            //        if (node.Tag == null) return;
            //        General.chargerFiles(node.Tag.ToString(), File1);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Program.SaveException(ex);
            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
            //}
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DirNoInter_BeforeSelect(object sender, Infragistics.Win.UltraWinTree.BeforeSelectEventArgs e)
        {
            //try
            //{
            //    if (e.NewSelections.Count > 0)
            //    {
            //        var node = e.NewSelections[0];
            //        node.Nodes.Clear();
            //        FileIntervention.Tag = node.Tag;
            //        if (node.Nodes.Count == 0)
            //        {
            //            General.chargerNodes(node.Tag.ToString(), node);
            //            node.Expanded = true;
            //        }
            //        if (node.Tag == null) return;
            //        General.chargerFiles(node.Tag.ToString(), FileIntervention);
            //    }
            //}
            //catch (Exception ex)
            //{
            //    Program.SaveException(ex);
            //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
            //}
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DirNoInter_DoubleClick(object sender, EventArgs e)
        {
            //if (DirNoInter.ActiveNode != null)
            //{
            //    try
            //    {

            //        var node = DirNoInter.ActiveNode;
            //        node.Nodes.Clear();
            //        FileIntervention.Tag = node.Tag;
            //        if (node.Nodes.Count == 0)
            //        {
            //            General.chargerNodes(node.Tag.ToString(), node);
            //            node.Expanded = true;
            //        }
            //        if (node.Tag == null) return;
            //        General.chargerFiles(node.Tag.ToString(), FileIntervention);
            //    }
            //    catch (Exception ex)
            //    {
            //        Program.SaveException(ex);
            //        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
            //    }
            //}
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Dir1_DoubleClick(object sender, EventArgs e)
        {
            //if (Dir1.ActiveNode != null)
            //{
            //    try
            //    {
            //        var node = Dir1.ActiveNode;
            //        node.Nodes.Clear();
            //        Dir1.Tag = node.Tag;
            //        File1.Tag = node.Tag;
            //        if (node.Nodes.Count == 0)
            //        {
            //            General.chargerNodes(node.Tag.ToString(), node);
            //            node.Expanded = true;
            //        }
            //        if (node.Tag == null) return;
            //        General.chargerFiles(node.Tag.ToString(), File1);

            //    }
            //    catch (Exception ex)
            //    {
            //        Program.SaveException(ex);
            //        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
            //    }
            //}

        }

        private void Dir1_Change(object sender, EventArgs e)
        {
            try
            {
                File1.Path = Dir1.Path;
            }
            catch(Exception ex)
            {
                Program.SaveException(ex);
                Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
            }
        }

        private void DirNoInter_Change(object sender, EventArgs e)
        {
            try
            {
                FileIntervention.Path = DirNoInter.Path;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
            }
        }

        private void Drive1_SelectedIndexChanged_1(object sender, EventArgs e)
        {
            try
            {
                ////=============> Mondir : Added the NOT (!) to the condition to fix 1705 & 1720 Bugs on Mantis
                if (!Drive1.Text.Contains("C:") && !Drive1.Text.Contains("c:"))
                {
                    Dir1.Path = Drive1.Drive;
                }
                else
                {
                   Dir1.Path=@"C:\";
                }
                //Dir1111.Tag = Drive1.Text;
                //Dir1.Tag = Drive1.Text;
                //File1.Tag = Drive1.Text;
                //Dir1.Nodes.Clear();
                //if (Dir1.Tag != null && Directory.Exists(Dir1.Tag.ToString()))
                //    fillTreeView(Dir1.Tag.ToString(), Dir1, File1);
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message);
            }
        }
    }
}
