﻿namespace Axe_interDT.Views.Devis.Forms
{
    partial class FrmFournture
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.SSOleDBGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtConnectionString = new iTalk.iTalk_TextBox_Small2();
            this.txtrequete = new iTalk.iTalk_TextBox_Small2();
            this.txtChoix = new iTalk.iTalk_TextBox_Small2();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // SSOleDBGrid1
            // 
            this.SSOleDBGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.SSOleDBGrid1.DisplayLayout.UseFixedHeaders = true;
            this.SSOleDBGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSOleDBGrid1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSOleDBGrid1.Location = new System.Drawing.Point(0, 0);
            this.SSOleDBGrid1.Name = "SSOleDBGrid1";
            this.SSOleDBGrid1.Size = new System.Drawing.Size(623, 213);
            this.SSOleDBGrid1.TabIndex = 572;
            this.SSOleDBGrid1.Text = "Fourniture";
            this.SSOleDBGrid1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSOleDBGrid1_InitializeLayout);
            this.SSOleDBGrid1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.SSOleDBGrid1_DoubleClickRow);
            this.SSOleDBGrid1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SSOleDBGrid1_KeyPress);
            // 
            // txtConnectionString
            // 
            this.txtConnectionString.AccAcceptNumbersOnly = false;
            this.txtConnectionString.AccAllowComma = false;
            this.txtConnectionString.AccBackgroundColor = System.Drawing.Color.White;
            this.txtConnectionString.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtConnectionString.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtConnectionString.AccHidenValue = "";
            this.txtConnectionString.AccNotAllowedChars = null;
            this.txtConnectionString.AccReadOnly = false;
            this.txtConnectionString.AccReadOnlyAllowDelete = false;
            this.txtConnectionString.AccRequired = false;
            this.txtConnectionString.BackColor = System.Drawing.Color.White;
            this.txtConnectionString.CustomBackColor = System.Drawing.Color.White;
            this.txtConnectionString.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtConnectionString.ForeColor = System.Drawing.Color.Black;
            this.txtConnectionString.Location = new System.Drawing.Point(42, 38);
            this.txtConnectionString.Margin = new System.Windows.Forms.Padding(2);
            this.txtConnectionString.MaxLength = 32767;
            this.txtConnectionString.Multiline = false;
            this.txtConnectionString.Name = "txtConnectionString";
            this.txtConnectionString.ReadOnly = false;
            this.txtConnectionString.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtConnectionString.Size = new System.Drawing.Size(135, 27);
            this.txtConnectionString.TabIndex = 573;
            this.txtConnectionString.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtConnectionString.UseSystemPasswordChar = false;
            this.txtConnectionString.Visible = false;
            // 
            // txtrequete
            // 
            this.txtrequete.AccAcceptNumbersOnly = false;
            this.txtrequete.AccAllowComma = false;
            this.txtrequete.AccBackgroundColor = System.Drawing.Color.White;
            this.txtrequete.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtrequete.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtrequete.AccHidenValue = "";
            this.txtrequete.AccNotAllowedChars = null;
            this.txtrequete.AccReadOnly = false;
            this.txtrequete.AccReadOnlyAllowDelete = false;
            this.txtrequete.AccRequired = false;
            this.txtrequete.BackColor = System.Drawing.Color.White;
            this.txtrequete.CustomBackColor = System.Drawing.Color.White;
            this.txtrequete.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtrequete.ForeColor = System.Drawing.Color.Black;
            this.txtrequete.Location = new System.Drawing.Point(224, 53);
            this.txtrequete.Margin = new System.Windows.Forms.Padding(2);
            this.txtrequete.MaxLength = 32767;
            this.txtrequete.Multiline = false;
            this.txtrequete.Name = "txtrequete";
            this.txtrequete.ReadOnly = false;
            this.txtrequete.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtrequete.Size = new System.Drawing.Size(135, 27);
            this.txtrequete.TabIndex = 574;
            this.txtrequete.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtrequete.UseSystemPasswordChar = false;
            this.txtrequete.Visible = false;
            // 
            // txtChoix
            // 
            this.txtChoix.AccAcceptNumbersOnly = false;
            this.txtChoix.AccAllowComma = false;
            this.txtChoix.AccBackgroundColor = System.Drawing.Color.White;
            this.txtChoix.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtChoix.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtChoix.AccHidenValue = "";
            this.txtChoix.AccNotAllowedChars = null;
            this.txtChoix.AccReadOnly = false;
            this.txtChoix.AccReadOnlyAllowDelete = false;
            this.txtChoix.AccRequired = false;
            this.txtChoix.BackColor = System.Drawing.Color.White;
            this.txtChoix.CustomBackColor = System.Drawing.Color.White;
            this.txtChoix.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtChoix.ForeColor = System.Drawing.Color.Black;
            this.txtChoix.Location = new System.Drawing.Point(447, 53);
            this.txtChoix.Margin = new System.Windows.Forms.Padding(2);
            this.txtChoix.MaxLength = 32767;
            this.txtChoix.Multiline = false;
            this.txtChoix.Name = "txtChoix";
            this.txtChoix.ReadOnly = false;
            this.txtChoix.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtChoix.Size = new System.Drawing.Size(135, 27);
            this.txtChoix.TabIndex = 575;
            this.txtChoix.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtChoix.UseSystemPasswordChar = false;
            this.txtChoix.Visible = false;
            // 
            // FrmFournture
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(623, 213);
            this.Controls.Add(this.txtChoix);
            this.Controls.Add(this.txtrequete);
            this.Controls.Add(this.txtConnectionString);
            this.Controls.Add(this.SSOleDBGrid1);
            this.Name = "FrmFournture";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Fourniture";
            this.Activated += new System.EventHandler(this.FrmFournture_Activated);
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.FrmFournture_FormClosing);
            this.Load += new System.EventHandler(this.FrmFournture_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        public Infragistics.Win.UltraWinGrid.UltraGrid SSOleDBGrid1;
        public iTalk.iTalk_TextBox_Small2 txtConnectionString;
        public iTalk.iTalk_TextBox_Small2 txtrequete;
        public iTalk.iTalk_TextBox_Small2 txtChoix;
    }
}