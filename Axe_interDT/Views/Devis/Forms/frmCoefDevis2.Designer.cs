﻿namespace Axe_interDT.Views.Devis.Forms
{
    partial class frmCoefDevis2
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdMAJ = new System.Windows.Forms.Button();
            this.Command1 = new System.Windows.Forms.Button();
            this.label9 = new System.Windows.Forms.Label();
            this.txtCoefFO = new iTalk.iTalk_TextBox_Small2();
            this.SuspendLayout();
            // 
            // cmdMAJ
            // 
            this.cmdMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJ.FlatAppearance.BorderSize = 0;
            this.cmdMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMAJ.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdMAJ.Location = new System.Drawing.Point(213, 11);
            this.cmdMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJ.Name = "cmdMAJ";
            this.cmdMAJ.Size = new System.Drawing.Size(60, 35);
            this.cmdMAJ.TabIndex = 515;
            this.cmdMAJ.UseVisualStyleBackColor = false;
            this.cmdMAJ.Click += new System.EventHandler(this.cmdMAJ_Click);
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.Command1.ForeColor = System.Drawing.Color.White;
            this.Command1.Location = new System.Drawing.Point(119, 11);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(90, 35);
            this.Command1.TabIndex = 516;
            this.Command1.Text = "Annuler";
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label9.Location = new System.Drawing.Point(43, 66);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(63, 19);
            this.label9.TabIndex = 518;
            this.label9.Text = "Coef Fo";
            // 
            // txtCoefFO
            // 
            this.txtCoefFO.AccAcceptNumbersOnly = false;
            this.txtCoefFO.AccAllowComma = false;
            this.txtCoefFO.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCoefFO.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCoefFO.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCoefFO.AccHidenValue = "";
            this.txtCoefFO.AccNotAllowedChars = null;
            this.txtCoefFO.AccReadOnly = false;
            this.txtCoefFO.AccReadOnlyAllowDelete = false;
            this.txtCoefFO.AccRequired = false;
            this.txtCoefFO.BackColor = System.Drawing.Color.White;
            this.txtCoefFO.CustomBackColor = System.Drawing.Color.White;
            this.txtCoefFO.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCoefFO.ForeColor = System.Drawing.Color.Black;
            this.txtCoefFO.Location = new System.Drawing.Point(119, 66);
            this.txtCoefFO.Margin = new System.Windows.Forms.Padding(2);
            this.txtCoefFO.MaxLength = 32767;
            this.txtCoefFO.Multiline = false;
            this.txtCoefFO.Name = "txtCoefFO";
            this.txtCoefFO.ReadOnly = false;
            this.txtCoefFO.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCoefFO.Size = new System.Drawing.Size(135, 27);
            this.txtCoefFO.TabIndex = 519;
            this.txtCoefFO.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCoefFO.UseSystemPasswordChar = false;
            // 
            // frmCoefDevis2
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(284, 122);
            this.Controls.Add(this.txtCoefFO);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.Command1);
            this.Controls.Add(this.cmdMAJ);
            this.MaximumSize = new System.Drawing.Size(300, 161);
            this.MinimumSize = new System.Drawing.Size(300, 161);
            this.Name = "frmCoefDevis2";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmCoefDevis2";
            this.Activated += new System.EventHandler(this.frmCoefDevis2_Activated);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public System.Windows.Forms.Button cmdMAJ;
        public System.Windows.Forms.Button Command1;
        private System.Windows.Forms.Label label9;
        public iTalk.iTalk_TextBox_Small2 txtCoefFO;
    }
}