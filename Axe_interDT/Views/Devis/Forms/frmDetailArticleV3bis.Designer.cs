﻿namespace Axe_interDT.Views.Devis.Forms
{
    partial class frmDetailArticleV3bis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab13 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.txtCODE_UNITE_FOURNISSEUR = new iTalk.iTalk_TextBox_Small2();
            this.txtLIBELLE_STATUT_CONTROLE = new iTalk.iTalk_TextBox_Small2();
            this.label67 = new System.Windows.Forms.Label();
            this.label87 = new System.Windows.Forms.Label();
            this.txtARTP_fournPref = new iTalk.iTalk_TextBox_Small2();
            this.label86 = new System.Windows.Forms.Label();
            this.txtARTP_PrixNetDepan = new iTalk.iTalk_TextBox_Small2();
            this.label85 = new System.Windows.Forms.Label();
            this.txtARTP_UsParUc = new iTalk.iTalk_TextBox_Small2();
            this.label84 = new System.Windows.Forms.Label();
            this.txtARTP_UdParUs = new iTalk.iTalk_TextBox_Small2();
            this.label83 = new System.Windows.Forms.Label();
            this.txtARTP_UvParUc = new iTalk.iTalk_TextBox_Small2();
            this.label82 = new System.Windows.Forms.Label();
            this.txtARTP_UcnfParUc = new iTalk.iTalk_TextBox_Small2();
            this.label81 = new System.Windows.Forms.Label();
            this.txtARTP_RefHisto = new iTalk.iTalk_TextBox_Small2();
            this.label80 = new System.Windows.Forms.Label();
            this.txtARTP_RefInfo = new iTalk.iTalk_TextBox_Small2();
            this.label79 = new System.Windows.Forms.Label();
            this.txtART_CategorieOutil = new iTalk.iTalk_TextBox_Small2();
            this.label78 = new System.Windows.Forms.Label();
            this.txtART_RubriqueAnaly = new iTalk.iTalk_TextBox_Small2();
            this.label77 = new System.Windows.Forms.Label();
            this.txtART_EtatDiffusion = new iTalk.iTalk_TextBox_Small2();
            this.label76 = new System.Windows.Forms.Label();
            this.txtART_FamAtoll = new iTalk.iTalk_TextBox_Small2();
            this.label75 = new System.Windows.Forms.Label();
            this.txtART_Nature = new iTalk.iTalk_TextBox_Small2();
            this.label74 = new System.Windows.Forms.Label();
            this.txtCODE_COND_FOURNISSEUR = new iTalk.iTalk_TextBox_Small2();
            this.label66 = new System.Windows.Forms.Label();
            this.txtCODE_UNITE_METIER = new iTalk.iTalk_TextBox_Small2();
            this.label65 = new System.Windows.Forms.Label();
            this.txtCODE_UNITE_DEVIS_2 = new iTalk.iTalk_TextBox_Small2();
            this.label64 = new System.Windows.Forms.Label();
            this.txtCODE_UNITE_STOCK = new iTalk.iTalk_TextBox_Small2();
            this.label63 = new System.Windows.Forms.Label();
            this.txtCODE_COND_STOCK = new iTalk.iTalk_TextBox_Small2();
            this.label62 = new System.Windows.Forms.Label();
            this.txtCODE_UNITE_VALEUR = new iTalk.iTalk_TextBox_Small2();
            this.label61 = new System.Windows.Forms.Label();
            this.txtCODE_UNITE_DEVIS = new iTalk.iTalk_TextBox_Small2();
            this.label60 = new System.Windows.Forms.Label();
            this.txtUNITE_DEVIS = new iTalk.iTalk_TextBox_Small2();
            this.label59 = new System.Windows.Forms.Label();
            this.txtART_Composant = new iTalk.iTalk_TextBox_Small2();
            this.label58 = new System.Windows.Forms.Label();
            this.txtUNITE_BASE = new iTalk.iTalk_TextBox_Small2();
            this.label57 = new System.Windows.Forms.Label();
            this.txtID_FAMILLE = new iTalk.iTalk_TextBox_Small2();
            this.label68 = new System.Windows.Forms.Label();
            this.iTalk_TextBox_Small21 = new iTalk.iTalk_TextBox_Small2();
            this.label19 = new System.Windows.Forms.Label();
            this.label93 = new System.Windows.Forms.Label();
            this.txtANCIENNE_REFERENCE = new iTalk.iTalk_TextBox_Small2();
            this.txtNATURE = new iTalk.iTalk_TextBox_Small2();
            this.label92 = new System.Windows.Forms.Label();
            this.txtSPECIAL_BBATH = new iTalk.iTalk_TextBox_Small2();
            this.txtSUPPRIME_CHEZ_FABRIQUANT = new iTalk.iTalk_TextBox_Small2();
            this.label91 = new System.Windows.Forms.Label();
            this.label90 = new System.Windows.Forms.Label();
            this.txtLIBELLE_DEVIS = new iTalk.iTalk_TextBox_Small2();
            this.txtREGROUPEMENT_ATOLL = new iTalk.iTalk_TextBox_Small2();
            this.label89 = new System.Windows.Forms.Label();
            this.label88 = new System.Windows.Forms.Label();
            this.txtART_dateFinActivite = new iTalk.iTalk_TextBox_Small2();
            this.txtNoAuto = new iTalk.iTalk_TextBox_Small2();
            this.GridCond = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtCORPS_D_ETAT = new iTalk.iTalk_TextBox_Small2();
            this.label26 = new System.Windows.Forms.Label();
            this.txtPOIDS_NET = new iTalk.iTalk_TextBox_Small2();
            this.label20 = new System.Windows.Forms.Label();
            this.txtSURFACE = new iTalk.iTalk_TextBox_Small2();
            this.label21 = new System.Windows.Forms.Label();
            this.txtLARGEUR = new iTalk.iTalk_TextBox_Small2();
            this.label22 = new System.Windows.Forms.Label();
            this.txtVOLUME = new iTalk.iTalk_TextBox_Small2();
            this.label23 = new System.Windows.Forms.Label();
            this.txtHAUTEUR = new iTalk.iTalk_TextBox_Small2();
            this.label24 = new System.Windows.Forms.Label();
            this.txtLONGUEUR = new iTalk.iTalk_TextBox_Small2();
            this.label25 = new System.Windows.Forms.Label();
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT = new iTalk.iTalk_TextBox_Small2();
            this.label18 = new System.Windows.Forms.Label();
            this.txtLIEN_FICHE_SECURITE = new iTalk.iTalk_TextBox_Small2();
            this.label19s = new System.Windows.Forms.Label();
            this.txtLIEN_URL_TECHNIQUE = new iTalk.iTalk_TextBox_Small2();
            this.label16 = new System.Windows.Forms.Label();
            this.txtIMAGE = new iTalk.iTalk_TextBox_Small2();
            this.label17 = new System.Windows.Forms.Label();
            this.txtLIBELLE_UNITE_DEVIS = new iTalk.iTalk_TextBox_Small2();
            this.label14 = new System.Windows.Forms.Label();
            this.txtUNCS_SUR_UV = new iTalk.iTalk_TextBox_Small2();
            this.label15 = new System.Windows.Forms.Label();
            this.txtLIBELLE_UNITE_BASE = new iTalk.iTalk_TextBox_Small2();
            this.label12 = new System.Windows.Forms.Label();
            this.txtLIBELLE_UNITE_CONDITIONNEMENT = new iTalk.iTalk_TextBox_Small2();
            this.label13 = new System.Windows.Forms.Label();
            this.txtCODE_UNITE_CONDITIONNEMENT = new iTalk.iTalk_TextBox_Small2();
            this.label10 = new System.Windows.Forms.Label();
            this.txtUNITE_CONDITIONNEMENT = new iTalk.iTalk_TextBox_Small2();
            this.label11 = new System.Windows.Forms.Label();
            this.txtsTatut = new iTalk.iTalk_TextBox_Small2();
            this.label9 = new System.Windows.Forms.Label();
            this.txtDATE_FIN_ACTIVITE = new iTalk.iTalk_TextBox_Small2();
            this.label8 = new System.Windows.Forms.Label();
            this.txtCOMPOSANT = new iTalk.iTalk_TextBox_Small2();
            this.label7 = new System.Windows.Forms.Label();
            this.txtTYPE = new iTalk.iTalk_TextBox_Small2();
            this.label6 = new System.Windows.Forms.Label();
            this.txtLIBELLE_SOUS_FAMILLE = new iTalk.iTalk_TextBox_Small2();
            this.label5 = new System.Windows.Forms.Label();
            this.txtLIBELLE_FAMILLE = new iTalk.iTalk_TextBox_Small2();
            this.label4 = new System.Windows.Forms.Label();
            this.txtLIBELLE_TECHNIQUE = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.txtDESIGNATION_ARTICLE = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.txtFAMILLE_ATOLL = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.txtCHRONO_INITIAL = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.txtLIBELLE_UNITE_PRIX_FABRICANT = new iTalk.iTalk_TextBox_Small2();
            this.txtPRIX_FABRICANT = new iTalk.iTalk_TextBox_Small2();
            this.txtQTE_UNITE_VALEUR = new iTalk.iTalk_TextBox_Small2();
            this.txtLIBELLE_UNITE_VALEUR = new iTalk.iTalk_TextBox_Small2();
            this.txtDATE_CREATION = new iTalk.iTalk_TextBox_Small2();
            this.txtPRIX_DONNE_PAR = new iTalk.iTalk_TextBox_Small2();
            this.TXTLIBELLE_UNITE_PRIX_DEVIS = new iTalk.iTalk_TextBox_Small2();
            this.txtPRX_DEVIS_NET_UD = new iTalk.iTalk_TextBox_Small2();
            this.label119 = new System.Windows.Forms.Label();
            this.label118 = new System.Windows.Forms.Label();
            this.label117 = new System.Windows.Forms.Label();
            this.label116 = new System.Windows.Forms.Label();
            this.label115 = new System.Windows.Forms.Label();
            this.label114 = new System.Windows.Forms.Label();
            this.label113 = new System.Windows.Forms.Label();
            this.label112 = new System.Windows.Forms.Label();
            this.label111 = new System.Windows.Forms.Label();
            this.label110 = new System.Windows.Forms.Label();
            this.label109 = new System.Windows.Forms.Label();
            this.label108 = new System.Windows.Forms.Label();
            this.label107 = new System.Windows.Forms.Label();
            this.label106 = new System.Windows.Forms.Label();
            this.label105 = new System.Windows.Forms.Label();
            this.label104 = new System.Windows.Forms.Label();
            this.label103 = new System.Windows.Forms.Label();
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE = new iTalk.iTalk_TextBox_Small2();
            this.txtPRIX_GECET_UNITE = new iTalk.iTalk_TextBox_Small2();
            this.txtPRIX_GECET = new iTalk.iTalk_TextBox_Small2();
            this.txtLIBELLE_UNITE_PRIX_GECET = new iTalk.iTalk_TextBox_Small2();
            this.label102 = new System.Windows.Forms.Label();
            this.label101 = new System.Windows.Forms.Label();
            this.label100 = new System.Windows.Forms.Label();
            this.txtPRIX_PUBLIC_DETAILS = new iTalk.iTalk_TextBox_Small2();
            this.txtPRIX_NET_DETAILS = new iTalk.iTalk_TextBox_Small2();
            this.txtREF_HISTORIQUE = new iTalk.iTalk_TextBox_Small2();
            this.label99 = new System.Windows.Forms.Label();
            this.label98 = new System.Windows.Forms.Label();
            this.label97 = new System.Windows.Forms.Label();
            this.label96 = new System.Windows.Forms.Label();
            this.label95 = new System.Windows.Forms.Label();
            this.label94 = new System.Windows.Forms.Label();
            this.txtUS_SUR_UV = new iTalk.iTalk_TextBox_Small2();
            this.txtQTE_MIN_CMD_UC = new iTalk.iTalk_TextBox_Small2();
            this.txtQTE_UNITE_FOURNISSEUR = new iTalk.iTalk_TextBox_Small2();
            this.txtLIBELLE_UNITE_FOURNISSEUR = new iTalk.iTalk_TextBox_Small2();
            this.txtNB_UC_DANS_UV = new iTalk.iTalk_TextBox_Small2();
            this.txtQTE_MIN_CMD_UCNF = new iTalk.iTalk_TextBox_Small2();
            this.txtUS_SUR_UC = new iTalk.iTalk_TextBox_Small2();
            this.TXTUD_SUR_US = new iTalk.iTalk_TextBox_Small2();
            this.txtUNCF_SUR_UC = new iTalk.iTalk_TextBox_Small2();
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE = new iTalk.iTalk_TextBox_Small2();
            this.txtPRIX_DEVIS_MAJORE = new iTalk.iTalk_TextBox_Small2();
            this.txtTAXE = new iTalk.iTalk_TextBox_Small2();
            this.label56 = new System.Windows.Forms.Label();
            this.txtLIBELLE_COND_PRIX_COND_STOCK = new iTalk.iTalk_TextBox_Small2();
            this.label55 = new System.Windows.Forms.Label();
            this.txtPRIX_COND_STOCK = new iTalk.iTalk_TextBox_Small2();
            this.label48 = new System.Windows.Forms.Label();
            this.txtLIBELLE_UNITE_PRIX_STOCK = new iTalk.iTalk_TextBox_Small2();
            this.label49 = new System.Windows.Forms.Label();
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR = new iTalk.iTalk_TextBox_Small2();
            this.label50 = new System.Windows.Forms.Label();
            this.txtPRIX_STOCK = new iTalk.iTalk_TextBox_Small2();
            this.label51 = new System.Windows.Forms.Label();
            this.txtPRIX_COND_FOUR_2 = new iTalk.iTalk_TextBox_Small2();
            this.label52 = new System.Windows.Forms.Label();
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR = new iTalk.iTalk_TextBox_Small2();
            this.label53 = new System.Windows.Forms.Label();
            this.txtPRIX_DISTRIBUTEUR = new iTalk.iTalk_TextBox_Small2();
            this.label54 = new System.Windows.Forms.Label();
            this.txtQTE_COND_FOURNISSEUR = new iTalk.iTalk_TextBox_Small2();
            this.label38 = new System.Windows.Forms.Label();
            this.txtLIBELLE_COND_FOURNISSEUR = new iTalk.iTalk_TextBox_Small2();
            this.label39 = new System.Windows.Forms.Label();
            this.txtQTE_UNITE_METIER = new iTalk.iTalk_TextBox_Small2();
            this.label40 = new System.Windows.Forms.Label();
            this.txtLIBELLE_UNITE_METIER = new iTalk.iTalk_TextBox_Small2();
            this.label41 = new System.Windows.Forms.Label();
            this.txtQTE_UNITE_DEVIS = new iTalk.iTalk_TextBox_Small2();
            this.label42 = new System.Windows.Forms.Label();
            this.txtLIBELLE_UNITE_DEVIS_2 = new iTalk.iTalk_TextBox_Small2();
            this.label43 = new System.Windows.Forms.Label();
            this.txtQTE_UNITE_STOCK = new iTalk.iTalk_TextBox_Small2();
            this.label44 = new System.Windows.Forms.Label();
            this.txtLIBELLE_UNITE_STOCK = new iTalk.iTalk_TextBox_Small2();
            this.label45 = new System.Windows.Forms.Label();
            this.txtQTE_COND_STOCK = new iTalk.iTalk_TextBox_Small2();
            this.label46 = new System.Windows.Forms.Label();
            this.txtLIBELLE_COND_STOCK = new iTalk.iTalk_TextBox_Small2();
            this.label47 = new System.Windows.Forms.Label();
            this.txtREF_FOURNISSEUR = new iTalk.iTalk_TextBox_Small2();
            this.label27 = new System.Windows.Forms.Label();
            this.txtREF_FABRICANT = new iTalk.iTalk_TextBox_Small2();
            this.label28 = new System.Windows.Forms.Label();
            this.txtLIBELLE_MARQUE = new iTalk.iTalk_TextBox_Small2();
            this.label29 = new System.Windows.Forms.Label();
            this.txtDATE_APPLICATION = new iTalk.iTalk_TextBox_Small2();
            this.label30 = new System.Windows.Forms.Label();
            this.txtLIBELLE_TECHNIQUE_2 = new iTalk.iTalk_TextBox_Small2();
            this.label31 = new System.Windows.Forms.Label();
            this.txtDESIGNATION_ARTICLE_2 = new iTalk.iTalk_TextBox_Small2();
            this.label32 = new System.Windows.Forms.Label();
            this.txtCHRONO_INITIAL_2 = new iTalk.iTalk_TextBox_Small2();
            this.label34 = new System.Windows.Forms.Label();
            this.txtNOM_FOURNISSEUR = new iTalk.iTalk_TextBox_Small2();
            this.label35 = new System.Windows.Forms.Label();
            this.txtCODE_FOURNISSEUR_ADHERENT = new iTalk.iTalk_TextBox_Small2();
            this.label36 = new System.Windows.Forms.Label();
            this.txtIDGEC_LIGNE_PRIX = new iTalk.iTalk_TextBox_Small2();
            this.label37 = new System.Windows.Forms.Label();
            this.SSTab1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.ultraTabPageControl1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.Frame1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCond)).BeginInit();
            this.ultraTabPageControl2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).BeginInit();
            this.SSTab1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.groupBox6);
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 21);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(1210, 631);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.Frame1);
            this.groupBox6.Controls.Add(this.iTalk_TextBox_Small21);
            this.groupBox6.Controls.Add(this.label19);
            this.groupBox6.Controls.Add(this.label93);
            this.groupBox6.Controls.Add(this.txtANCIENNE_REFERENCE);
            this.groupBox6.Controls.Add(this.txtNATURE);
            this.groupBox6.Controls.Add(this.label92);
            this.groupBox6.Controls.Add(this.txtSPECIAL_BBATH);
            this.groupBox6.Controls.Add(this.txtSUPPRIME_CHEZ_FABRIQUANT);
            this.groupBox6.Controls.Add(this.label91);
            this.groupBox6.Controls.Add(this.label90);
            this.groupBox6.Controls.Add(this.txtLIBELLE_DEVIS);
            this.groupBox6.Controls.Add(this.txtREGROUPEMENT_ATOLL);
            this.groupBox6.Controls.Add(this.label89);
            this.groupBox6.Controls.Add(this.label88);
            this.groupBox6.Controls.Add(this.txtART_dateFinActivite);
            this.groupBox6.Controls.Add(this.txtNoAuto);
            this.groupBox6.Controls.Add(this.GridCond);
            this.groupBox6.Controls.Add(this.txtCORPS_D_ETAT);
            this.groupBox6.Controls.Add(this.label26);
            this.groupBox6.Controls.Add(this.txtPOIDS_NET);
            this.groupBox6.Controls.Add(this.label20);
            this.groupBox6.Controls.Add(this.txtSURFACE);
            this.groupBox6.Controls.Add(this.label21);
            this.groupBox6.Controls.Add(this.txtLARGEUR);
            this.groupBox6.Controls.Add(this.label22);
            this.groupBox6.Controls.Add(this.txtVOLUME);
            this.groupBox6.Controls.Add(this.label23);
            this.groupBox6.Controls.Add(this.txtHAUTEUR);
            this.groupBox6.Controls.Add(this.label24);
            this.groupBox6.Controls.Add(this.txtLONGUEUR);
            this.groupBox6.Controls.Add(this.label25);
            this.groupBox6.Controls.Add(this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT);
            this.groupBox6.Controls.Add(this.label18);
            this.groupBox6.Controls.Add(this.txtLIEN_FICHE_SECURITE);
            this.groupBox6.Controls.Add(this.label19s);
            this.groupBox6.Controls.Add(this.txtLIEN_URL_TECHNIQUE);
            this.groupBox6.Controls.Add(this.label16);
            this.groupBox6.Controls.Add(this.txtIMAGE);
            this.groupBox6.Controls.Add(this.label17);
            this.groupBox6.Controls.Add(this.txtLIBELLE_UNITE_DEVIS);
            this.groupBox6.Controls.Add(this.label14);
            this.groupBox6.Controls.Add(this.txtUNCS_SUR_UV);
            this.groupBox6.Controls.Add(this.label15);
            this.groupBox6.Controls.Add(this.txtLIBELLE_UNITE_BASE);
            this.groupBox6.Controls.Add(this.label12);
            this.groupBox6.Controls.Add(this.txtLIBELLE_UNITE_CONDITIONNEMENT);
            this.groupBox6.Controls.Add(this.label13);
            this.groupBox6.Controls.Add(this.txtCODE_UNITE_CONDITIONNEMENT);
            this.groupBox6.Controls.Add(this.label10);
            this.groupBox6.Controls.Add(this.txtUNITE_CONDITIONNEMENT);
            this.groupBox6.Controls.Add(this.label11);
            this.groupBox6.Controls.Add(this.txtsTatut);
            this.groupBox6.Controls.Add(this.label9);
            this.groupBox6.Controls.Add(this.txtDATE_FIN_ACTIVITE);
            this.groupBox6.Controls.Add(this.label8);
            this.groupBox6.Controls.Add(this.txtCOMPOSANT);
            this.groupBox6.Controls.Add(this.label7);
            this.groupBox6.Controls.Add(this.txtTYPE);
            this.groupBox6.Controls.Add(this.label6);
            this.groupBox6.Controls.Add(this.txtLIBELLE_SOUS_FAMILLE);
            this.groupBox6.Controls.Add(this.label5);
            this.groupBox6.Controls.Add(this.txtLIBELLE_FAMILLE);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.txtLIBELLE_TECHNIQUE);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.txtDESIGNATION_ARTICLE);
            this.groupBox6.Controls.Add(this.label2);
            this.groupBox6.Controls.Add(this.txtFAMILLE_ATOLL);
            this.groupBox6.Controls.Add(this.label1);
            this.groupBox6.Controls.Add(this.txtCHRONO_INITIAL);
            this.groupBox6.Controls.Add(this.label33);
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox6.Location = new System.Drawing.Point(3, 18);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1198, 602);
            this.groupBox6.TabIndex = 413;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "ARTICLE";
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.Transparent;
            this.Frame1.Controls.Add(this.txtCODE_UNITE_FOURNISSEUR);
            this.Frame1.Controls.Add(this.txtLIBELLE_STATUT_CONTROLE);
            this.Frame1.Controls.Add(this.label67);
            this.Frame1.Controls.Add(this.label87);
            this.Frame1.Controls.Add(this.txtARTP_fournPref);
            this.Frame1.Controls.Add(this.label86);
            this.Frame1.Controls.Add(this.txtARTP_PrixNetDepan);
            this.Frame1.Controls.Add(this.label85);
            this.Frame1.Controls.Add(this.txtARTP_UsParUc);
            this.Frame1.Controls.Add(this.label84);
            this.Frame1.Controls.Add(this.txtARTP_UdParUs);
            this.Frame1.Controls.Add(this.label83);
            this.Frame1.Controls.Add(this.txtARTP_UvParUc);
            this.Frame1.Controls.Add(this.label82);
            this.Frame1.Controls.Add(this.txtARTP_UcnfParUc);
            this.Frame1.Controls.Add(this.label81);
            this.Frame1.Controls.Add(this.txtARTP_RefHisto);
            this.Frame1.Controls.Add(this.label80);
            this.Frame1.Controls.Add(this.txtARTP_RefInfo);
            this.Frame1.Controls.Add(this.label79);
            this.Frame1.Controls.Add(this.txtART_CategorieOutil);
            this.Frame1.Controls.Add(this.label78);
            this.Frame1.Controls.Add(this.txtART_RubriqueAnaly);
            this.Frame1.Controls.Add(this.label77);
            this.Frame1.Controls.Add(this.txtART_EtatDiffusion);
            this.Frame1.Controls.Add(this.label76);
            this.Frame1.Controls.Add(this.txtART_FamAtoll);
            this.Frame1.Controls.Add(this.label75);
            this.Frame1.Controls.Add(this.txtART_Nature);
            this.Frame1.Controls.Add(this.label74);
            this.Frame1.Controls.Add(this.txtCODE_COND_FOURNISSEUR);
            this.Frame1.Controls.Add(this.label66);
            this.Frame1.Controls.Add(this.txtCODE_UNITE_METIER);
            this.Frame1.Controls.Add(this.label65);
            this.Frame1.Controls.Add(this.txtCODE_UNITE_DEVIS_2);
            this.Frame1.Controls.Add(this.label64);
            this.Frame1.Controls.Add(this.txtCODE_UNITE_STOCK);
            this.Frame1.Controls.Add(this.label63);
            this.Frame1.Controls.Add(this.txtCODE_COND_STOCK);
            this.Frame1.Controls.Add(this.label62);
            this.Frame1.Controls.Add(this.txtCODE_UNITE_VALEUR);
            this.Frame1.Controls.Add(this.label61);
            this.Frame1.Controls.Add(this.txtCODE_UNITE_DEVIS);
            this.Frame1.Controls.Add(this.label60);
            this.Frame1.Controls.Add(this.txtUNITE_DEVIS);
            this.Frame1.Controls.Add(this.label59);
            this.Frame1.Controls.Add(this.txtART_Composant);
            this.Frame1.Controls.Add(this.label58);
            this.Frame1.Controls.Add(this.txtUNITE_BASE);
            this.Frame1.Controls.Add(this.label57);
            this.Frame1.Controls.Add(this.txtID_FAMILLE);
            this.Frame1.Controls.Add(this.label68);
            this.Frame1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Frame1.Location = new System.Drawing.Point(845, 354);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(499, 494);
            this.Frame1.TabIndex = 591;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "abc";
            this.Frame1.Visible = false;
            // 
            // txtCODE_UNITE_FOURNISSEUR
            // 
            this.txtCODE_UNITE_FOURNISSEUR.AccAcceptNumbersOnly = false;
            this.txtCODE_UNITE_FOURNISSEUR.AccAllowComma = false;
            this.txtCODE_UNITE_FOURNISSEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_FOURNISSEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCODE_UNITE_FOURNISSEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCODE_UNITE_FOURNISSEUR.AccHidenValue = "";
            this.txtCODE_UNITE_FOURNISSEUR.AccNotAllowedChars = null;
            this.txtCODE_UNITE_FOURNISSEUR.AccReadOnly = false;
            this.txtCODE_UNITE_FOURNISSEUR.AccReadOnlyAllowDelete = false;
            this.txtCODE_UNITE_FOURNISSEUR.AccRequired = false;
            this.txtCODE_UNITE_FOURNISSEUR.BackColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_FOURNISSEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_FOURNISSEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCODE_UNITE_FOURNISSEUR.ForeColor = System.Drawing.Color.Black;
            this.txtCODE_UNITE_FOURNISSEUR.Location = new System.Drawing.Point(54, 346);
            this.txtCODE_UNITE_FOURNISSEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtCODE_UNITE_FOURNISSEUR.MaxLength = 32767;
            this.txtCODE_UNITE_FOURNISSEUR.Multiline = false;
            this.txtCODE_UNITE_FOURNISSEUR.Name = "txtCODE_UNITE_FOURNISSEUR";
            this.txtCODE_UNITE_FOURNISSEUR.ReadOnly = false;
            this.txtCODE_UNITE_FOURNISSEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCODE_UNITE_FOURNISSEUR.Size = new System.Drawing.Size(135, 27);
            this.txtCODE_UNITE_FOURNISSEUR.TabIndex = 567;
            this.txtCODE_UNITE_FOURNISSEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCODE_UNITE_FOURNISSEUR.UseSystemPasswordChar = false;
            // 
            // txtLIBELLE_STATUT_CONTROLE
            // 
            this.txtLIBELLE_STATUT_CONTROLE.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_STATUT_CONTROLE.AccAllowComma = false;
            this.txtLIBELLE_STATUT_CONTROLE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_STATUT_CONTROLE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_STATUT_CONTROLE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLIBELLE_STATUT_CONTROLE.AccHidenValue = "";
            this.txtLIBELLE_STATUT_CONTROLE.AccNotAllowedChars = null;
            this.txtLIBELLE_STATUT_CONTROLE.AccReadOnly = false;
            this.txtLIBELLE_STATUT_CONTROLE.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_STATUT_CONTROLE.AccRequired = false;
            this.txtLIBELLE_STATUT_CONTROLE.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_STATUT_CONTROLE.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_STATUT_CONTROLE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_STATUT_CONTROLE.ForeColor = System.Drawing.Color.Black;
            this.txtLIBELLE_STATUT_CONTROLE.Location = new System.Drawing.Point(109, 300);
            this.txtLIBELLE_STATUT_CONTROLE.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_STATUT_CONTROLE.MaxLength = 32767;
            this.txtLIBELLE_STATUT_CONTROLE.Multiline = false;
            this.txtLIBELLE_STATUT_CONTROLE.Name = "txtLIBELLE_STATUT_CONTROLE";
            this.txtLIBELLE_STATUT_CONTROLE.ReadOnly = false;
            this.txtLIBELLE_STATUT_CONTROLE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_STATUT_CONTROLE.Size = new System.Drawing.Size(135, 27);
            this.txtLIBELLE_STATUT_CONTROLE.TabIndex = 566;
            this.txtLIBELLE_STATUT_CONTROLE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_STATUT_CONTROLE.UseSystemPasswordChar = false;
            // 
            // label67
            // 
            this.label67.AutoSize = true;
            this.label67.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label67.Location = new System.Drawing.Point(9, 304);
            this.label67.Name = "label67";
            this.label67.Size = new System.Drawing.Size(106, 16);
            this.label67.TabIndex = 565;
            this.label67.Text = "lib. statut contôle";
            // 
            // label87
            // 
            this.label87.AutoSize = true;
            this.label87.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label87.Location = new System.Drawing.Point(283, 420);
            this.label87.Name = "label87";
            this.label87.Size = new System.Drawing.Size(200, 16);
            this.label87.TabIndex = 564;
            this.label87.Text = "Quantité min commande (UCNF)";
            // 
            // txtARTP_fournPref
            // 
            this.txtARTP_fournPref.AccAcceptNumbersOnly = false;
            this.txtARTP_fournPref.AccAllowComma = false;
            this.txtARTP_fournPref.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_fournPref.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_fournPref.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtARTP_fournPref.AccHidenValue = "";
            this.txtARTP_fournPref.AccNotAllowedChars = null;
            this.txtARTP_fournPref.AccReadOnly = false;
            this.txtARTP_fournPref.AccReadOnlyAllowDelete = false;
            this.txtARTP_fournPref.AccRequired = false;
            this.txtARTP_fournPref.BackColor = System.Drawing.Color.White;
            this.txtARTP_fournPref.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_fournPref.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_fournPref.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_fournPref.Location = new System.Drawing.Point(311, 372);
            this.txtARTP_fournPref.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_fournPref.MaxLength = 32767;
            this.txtARTP_fournPref.Multiline = false;
            this.txtARTP_fournPref.Name = "txtARTP_fournPref";
            this.txtARTP_fournPref.ReadOnly = false;
            this.txtARTP_fournPref.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_fournPref.Size = new System.Drawing.Size(135, 27);
            this.txtARTP_fournPref.TabIndex = 563;
            this.txtARTP_fournPref.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_fournPref.UseSystemPasswordChar = false;
            // 
            // label86
            // 
            this.label86.AutoSize = true;
            this.label86.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label86.Location = new System.Drawing.Point(246, 372);
            this.label86.Name = "label86";
            this.label86.Size = new System.Drawing.Size(149, 16);
            this.label86.TabIndex = 562;
            this.label86.Text = "Fournisseur Préférentiel";
            // 
            // txtARTP_PrixNetDepan
            // 
            this.txtARTP_PrixNetDepan.AccAcceptNumbersOnly = false;
            this.txtARTP_PrixNetDepan.AccAllowComma = false;
            this.txtARTP_PrixNetDepan.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_PrixNetDepan.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_PrixNetDepan.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtARTP_PrixNetDepan.AccHidenValue = "";
            this.txtARTP_PrixNetDepan.AccNotAllowedChars = null;
            this.txtARTP_PrixNetDepan.AccReadOnly = false;
            this.txtARTP_PrixNetDepan.AccReadOnlyAllowDelete = false;
            this.txtARTP_PrixNetDepan.AccRequired = false;
            this.txtARTP_PrixNetDepan.BackColor = System.Drawing.Color.White;
            this.txtARTP_PrixNetDepan.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_PrixNetDepan.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_PrixNetDepan.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_PrixNetDepan.Location = new System.Drawing.Point(314, 344);
            this.txtARTP_PrixNetDepan.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_PrixNetDepan.MaxLength = 32767;
            this.txtARTP_PrixNetDepan.Multiline = false;
            this.txtARTP_PrixNetDepan.Name = "txtARTP_PrixNetDepan";
            this.txtARTP_PrixNetDepan.ReadOnly = false;
            this.txtARTP_PrixNetDepan.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_PrixNetDepan.Size = new System.Drawing.Size(135, 27);
            this.txtARTP_PrixNetDepan.TabIndex = 561;
            this.txtARTP_PrixNetDepan.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_PrixNetDepan.UseSystemPasswordChar = false;
            // 
            // label85
            // 
            this.label85.AutoSize = true;
            this.label85.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label85.Location = new System.Drawing.Point(249, 345);
            this.label85.Name = "label85";
            this.label85.Size = new System.Drawing.Size(169, 16);
            this.label85.TabIndex = 560;
            this.label85.Text = "Prix vente dépannage (US)";
            // 
            // txtARTP_UsParUc
            // 
            this.txtARTP_UsParUc.AccAcceptNumbersOnly = false;
            this.txtARTP_UsParUc.AccAllowComma = false;
            this.txtARTP_UsParUc.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_UsParUc.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_UsParUc.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtARTP_UsParUc.AccHidenValue = "";
            this.txtARTP_UsParUc.AccNotAllowedChars = null;
            this.txtARTP_UsParUc.AccReadOnly = false;
            this.txtARTP_UsParUc.AccReadOnlyAllowDelete = false;
            this.txtARTP_UsParUc.AccRequired = false;
            this.txtARTP_UsParUc.BackColor = System.Drawing.Color.White;
            this.txtARTP_UsParUc.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_UsParUc.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_UsParUc.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_UsParUc.Location = new System.Drawing.Point(314, 312);
            this.txtARTP_UsParUc.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_UsParUc.MaxLength = 32767;
            this.txtARTP_UsParUc.Multiline = false;
            this.txtARTP_UsParUc.Name = "txtARTP_UsParUc";
            this.txtARTP_UsParUc.ReadOnly = false;
            this.txtARTP_UsParUc.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_UsParUc.Size = new System.Drawing.Size(135, 27);
            this.txtARTP_UsParUc.TabIndex = 559;
            this.txtARTP_UsParUc.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_UsParUc.UseSystemPasswordChar = false;
            // 
            // label84
            // 
            this.label84.AutoSize = true;
            this.label84.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label84.Location = new System.Drawing.Point(249, 311);
            this.label84.Name = "label84";
            this.label84.Size = new System.Drawing.Size(56, 16);
            this.label84.TabIndex = 558;
            this.label84.Text = "US / UC";
            // 
            // txtARTP_UdParUs
            // 
            this.txtARTP_UdParUs.AccAcceptNumbersOnly = false;
            this.txtARTP_UdParUs.AccAllowComma = false;
            this.txtARTP_UdParUs.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_UdParUs.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_UdParUs.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtARTP_UdParUs.AccHidenValue = "";
            this.txtARTP_UdParUs.AccNotAllowedChars = null;
            this.txtARTP_UdParUs.AccReadOnly = false;
            this.txtARTP_UdParUs.AccReadOnlyAllowDelete = false;
            this.txtARTP_UdParUs.AccRequired = false;
            this.txtARTP_UdParUs.BackColor = System.Drawing.Color.White;
            this.txtARTP_UdParUs.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_UdParUs.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_UdParUs.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_UdParUs.Location = new System.Drawing.Point(314, 286);
            this.txtARTP_UdParUs.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_UdParUs.MaxLength = 32767;
            this.txtARTP_UdParUs.Multiline = false;
            this.txtARTP_UdParUs.Name = "txtARTP_UdParUs";
            this.txtARTP_UdParUs.ReadOnly = false;
            this.txtARTP_UdParUs.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_UdParUs.Size = new System.Drawing.Size(135, 27);
            this.txtARTP_UdParUs.TabIndex = 557;
            this.txtARTP_UdParUs.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_UdParUs.UseSystemPasswordChar = false;
            // 
            // label83
            // 
            this.label83.AutoSize = true;
            this.label83.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label83.Location = new System.Drawing.Point(249, 285);
            this.label83.Name = "label83";
            this.label83.Size = new System.Drawing.Size(57, 16);
            this.label83.TabIndex = 556;
            this.label83.Text = "UD / US";
            // 
            // txtARTP_UvParUc
            // 
            this.txtARTP_UvParUc.AccAcceptNumbersOnly = false;
            this.txtARTP_UvParUc.AccAllowComma = false;
            this.txtARTP_UvParUc.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_UvParUc.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_UvParUc.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtARTP_UvParUc.AccHidenValue = "";
            this.txtARTP_UvParUc.AccNotAllowedChars = null;
            this.txtARTP_UvParUc.AccReadOnly = false;
            this.txtARTP_UvParUc.AccReadOnlyAllowDelete = false;
            this.txtARTP_UvParUc.AccRequired = false;
            this.txtARTP_UvParUc.BackColor = System.Drawing.Color.White;
            this.txtARTP_UvParUc.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_UvParUc.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_UvParUc.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_UvParUc.Location = new System.Drawing.Point(311, 260);
            this.txtARTP_UvParUc.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_UvParUc.MaxLength = 32767;
            this.txtARTP_UvParUc.Multiline = false;
            this.txtARTP_UvParUc.Name = "txtARTP_UvParUc";
            this.txtARTP_UvParUc.ReadOnly = false;
            this.txtARTP_UvParUc.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_UvParUc.Size = new System.Drawing.Size(135, 27);
            this.txtARTP_UvParUc.TabIndex = 555;
            this.txtARTP_UvParUc.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_UvParUc.UseSystemPasswordChar = false;
            // 
            // label82
            // 
            this.label82.AutoSize = true;
            this.label82.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label82.Location = new System.Drawing.Point(246, 259);
            this.label82.Name = "label82";
            this.label82.Size = new System.Drawing.Size(56, 16);
            this.label82.TabIndex = 554;
            this.label82.Text = "UV / UC";
            // 
            // txtARTP_UcnfParUc
            // 
            this.txtARTP_UcnfParUc.AccAcceptNumbersOnly = false;
            this.txtARTP_UcnfParUc.AccAllowComma = false;
            this.txtARTP_UcnfParUc.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_UcnfParUc.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_UcnfParUc.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtARTP_UcnfParUc.AccHidenValue = "";
            this.txtARTP_UcnfParUc.AccNotAllowedChars = null;
            this.txtARTP_UcnfParUc.AccReadOnly = false;
            this.txtARTP_UcnfParUc.AccReadOnlyAllowDelete = false;
            this.txtARTP_UcnfParUc.AccRequired = false;
            this.txtARTP_UcnfParUc.BackColor = System.Drawing.Color.White;
            this.txtARTP_UcnfParUc.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_UcnfParUc.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_UcnfParUc.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_UcnfParUc.Location = new System.Drawing.Point(311, 234);
            this.txtARTP_UcnfParUc.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_UcnfParUc.MaxLength = 32767;
            this.txtARTP_UcnfParUc.Multiline = false;
            this.txtARTP_UcnfParUc.Name = "txtARTP_UcnfParUc";
            this.txtARTP_UcnfParUc.ReadOnly = false;
            this.txtARTP_UcnfParUc.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_UcnfParUc.Size = new System.Drawing.Size(135, 27);
            this.txtARTP_UcnfParUc.TabIndex = 553;
            this.txtARTP_UcnfParUc.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_UcnfParUc.UseSystemPasswordChar = false;
            // 
            // label81
            // 
            this.label81.AutoSize = true;
            this.label81.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label81.Location = new System.Drawing.Point(246, 233);
            this.label81.Name = "label81";
            this.label81.Size = new System.Drawing.Size(74, 16);
            this.label81.TabIndex = 552;
            this.label81.Text = "UCNF / UC";
            // 
            // txtARTP_RefHisto
            // 
            this.txtARTP_RefHisto.AccAcceptNumbersOnly = false;
            this.txtARTP_RefHisto.AccAllowComma = false;
            this.txtARTP_RefHisto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_RefHisto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_RefHisto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtARTP_RefHisto.AccHidenValue = "";
            this.txtARTP_RefHisto.AccNotAllowedChars = null;
            this.txtARTP_RefHisto.AccReadOnly = false;
            this.txtARTP_RefHisto.AccReadOnlyAllowDelete = false;
            this.txtARTP_RefHisto.AccRequired = false;
            this.txtARTP_RefHisto.BackColor = System.Drawing.Color.White;
            this.txtARTP_RefHisto.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_RefHisto.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_RefHisto.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_RefHisto.Location = new System.Drawing.Point(311, 208);
            this.txtARTP_RefHisto.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_RefHisto.MaxLength = 32767;
            this.txtARTP_RefHisto.Multiline = false;
            this.txtARTP_RefHisto.Name = "txtARTP_RefHisto";
            this.txtARTP_RefHisto.ReadOnly = false;
            this.txtARTP_RefHisto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_RefHisto.Size = new System.Drawing.Size(135, 27);
            this.txtARTP_RefHisto.TabIndex = 551;
            this.txtARTP_RefHisto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_RefHisto.UseSystemPasswordChar = false;
            // 
            // label80
            // 
            this.label80.AutoSize = true;
            this.label80.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label80.Location = new System.Drawing.Point(246, 207);
            this.label80.Name = "label80";
            this.label80.Size = new System.Drawing.Size(132, 16);
            this.label80.TabIndex = 550;
            this.label80.Text = "Référence historique";
            // 
            // txtARTP_RefInfo
            // 
            this.txtARTP_RefInfo.AccAcceptNumbersOnly = false;
            this.txtARTP_RefInfo.AccAllowComma = false;
            this.txtARTP_RefInfo.AccBackgroundColor = System.Drawing.Color.White;
            this.txtARTP_RefInfo.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtARTP_RefInfo.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtARTP_RefInfo.AccHidenValue = "";
            this.txtARTP_RefInfo.AccNotAllowedChars = null;
            this.txtARTP_RefInfo.AccReadOnly = false;
            this.txtARTP_RefInfo.AccReadOnlyAllowDelete = false;
            this.txtARTP_RefInfo.AccRequired = false;
            this.txtARTP_RefInfo.BackColor = System.Drawing.Color.White;
            this.txtARTP_RefInfo.CustomBackColor = System.Drawing.Color.White;
            this.txtARTP_RefInfo.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtARTP_RefInfo.ForeColor = System.Drawing.Color.Black;
            this.txtARTP_RefInfo.Location = new System.Drawing.Point(311, 173);
            this.txtARTP_RefInfo.Margin = new System.Windows.Forms.Padding(2);
            this.txtARTP_RefInfo.MaxLength = 32767;
            this.txtARTP_RefInfo.Multiline = false;
            this.txtARTP_RefInfo.Name = "txtARTP_RefInfo";
            this.txtARTP_RefInfo.ReadOnly = false;
            this.txtARTP_RefInfo.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtARTP_RefInfo.Size = new System.Drawing.Size(135, 27);
            this.txtARTP_RefInfo.TabIndex = 549;
            this.txtARTP_RefInfo.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtARTP_RefInfo.UseSystemPasswordChar = false;
            // 
            // label79
            // 
            this.label79.AutoSize = true;
            this.label79.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label79.Location = new System.Drawing.Point(246, 172);
            this.label79.Name = "label79";
            this.label79.Size = new System.Drawing.Size(105, 16);
            this.label79.TabIndex = 548;
            this.label79.Text = "Réf informatique";
            // 
            // txtART_CategorieOutil
            // 
            this.txtART_CategorieOutil.AccAcceptNumbersOnly = false;
            this.txtART_CategorieOutil.AccAllowComma = false;
            this.txtART_CategorieOutil.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_CategorieOutil.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_CategorieOutil.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtART_CategorieOutil.AccHidenValue = "";
            this.txtART_CategorieOutil.AccNotAllowedChars = null;
            this.txtART_CategorieOutil.AccReadOnly = false;
            this.txtART_CategorieOutil.AccReadOnlyAllowDelete = false;
            this.txtART_CategorieOutil.AccRequired = false;
            this.txtART_CategorieOutil.BackColor = System.Drawing.Color.White;
            this.txtART_CategorieOutil.CustomBackColor = System.Drawing.Color.White;
            this.txtART_CategorieOutil.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_CategorieOutil.ForeColor = System.Drawing.Color.Black;
            this.txtART_CategorieOutil.Location = new System.Drawing.Point(311, 137);
            this.txtART_CategorieOutil.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_CategorieOutil.MaxLength = 32767;
            this.txtART_CategorieOutil.Multiline = false;
            this.txtART_CategorieOutil.Name = "txtART_CategorieOutil";
            this.txtART_CategorieOutil.ReadOnly = false;
            this.txtART_CategorieOutil.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_CategorieOutil.Size = new System.Drawing.Size(135, 27);
            this.txtART_CategorieOutil.TabIndex = 547;
            this.txtART_CategorieOutil.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_CategorieOutil.UseSystemPasswordChar = false;
            // 
            // label78
            // 
            this.label78.AutoSize = true;
            this.label78.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label78.Location = new System.Drawing.Point(246, 136);
            this.label78.Name = "label78";
            this.label78.Size = new System.Drawing.Size(96, 16);
            this.label78.TabIndex = 546;
            this.label78.Text = "Catégorie Outil";
            // 
            // txtART_RubriqueAnaly
            // 
            this.txtART_RubriqueAnaly.AccAcceptNumbersOnly = false;
            this.txtART_RubriqueAnaly.AccAllowComma = false;
            this.txtART_RubriqueAnaly.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_RubriqueAnaly.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_RubriqueAnaly.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtART_RubriqueAnaly.AccHidenValue = "";
            this.txtART_RubriqueAnaly.AccNotAllowedChars = null;
            this.txtART_RubriqueAnaly.AccReadOnly = false;
            this.txtART_RubriqueAnaly.AccReadOnlyAllowDelete = false;
            this.txtART_RubriqueAnaly.AccRequired = false;
            this.txtART_RubriqueAnaly.BackColor = System.Drawing.Color.White;
            this.txtART_RubriqueAnaly.CustomBackColor = System.Drawing.Color.White;
            this.txtART_RubriqueAnaly.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_RubriqueAnaly.ForeColor = System.Drawing.Color.Black;
            this.txtART_RubriqueAnaly.Location = new System.Drawing.Point(311, 110);
            this.txtART_RubriqueAnaly.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_RubriqueAnaly.MaxLength = 32767;
            this.txtART_RubriqueAnaly.Multiline = false;
            this.txtART_RubriqueAnaly.Name = "txtART_RubriqueAnaly";
            this.txtART_RubriqueAnaly.ReadOnly = false;
            this.txtART_RubriqueAnaly.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_RubriqueAnaly.Size = new System.Drawing.Size(135, 27);
            this.txtART_RubriqueAnaly.TabIndex = 545;
            this.txtART_RubriqueAnaly.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_RubriqueAnaly.UseSystemPasswordChar = false;
            // 
            // label77
            // 
            this.label77.AutoSize = true;
            this.label77.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label77.Location = new System.Drawing.Point(246, 109);
            this.label77.Name = "label77";
            this.label77.Size = new System.Drawing.Size(129, 16);
            this.label77.TabIndex = 544;
            this.label77.Text = "Rubrique Analytique";
            // 
            // txtART_EtatDiffusion
            // 
            this.txtART_EtatDiffusion.AccAcceptNumbersOnly = false;
            this.txtART_EtatDiffusion.AccAllowComma = false;
            this.txtART_EtatDiffusion.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_EtatDiffusion.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_EtatDiffusion.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtART_EtatDiffusion.AccHidenValue = "";
            this.txtART_EtatDiffusion.AccNotAllowedChars = null;
            this.txtART_EtatDiffusion.AccReadOnly = false;
            this.txtART_EtatDiffusion.AccReadOnlyAllowDelete = false;
            this.txtART_EtatDiffusion.AccRequired = false;
            this.txtART_EtatDiffusion.BackColor = System.Drawing.Color.White;
            this.txtART_EtatDiffusion.CustomBackColor = System.Drawing.Color.White;
            this.txtART_EtatDiffusion.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_EtatDiffusion.ForeColor = System.Drawing.Color.Black;
            this.txtART_EtatDiffusion.Location = new System.Drawing.Point(311, 78);
            this.txtART_EtatDiffusion.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_EtatDiffusion.MaxLength = 32767;
            this.txtART_EtatDiffusion.Multiline = false;
            this.txtART_EtatDiffusion.Name = "txtART_EtatDiffusion";
            this.txtART_EtatDiffusion.ReadOnly = false;
            this.txtART_EtatDiffusion.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_EtatDiffusion.Size = new System.Drawing.Size(135, 27);
            this.txtART_EtatDiffusion.TabIndex = 543;
            this.txtART_EtatDiffusion.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_EtatDiffusion.UseSystemPasswordChar = false;
            // 
            // label76
            // 
            this.label76.AutoSize = true;
            this.label76.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label76.Location = new System.Drawing.Point(246, 77);
            this.label76.Name = "label76";
            this.label76.Size = new System.Drawing.Size(102, 16);
            this.label76.TabIndex = 542;
            this.label76.Text = "Etat de diffusion";
            // 
            // txtART_FamAtoll
            // 
            this.txtART_FamAtoll.AccAcceptNumbersOnly = false;
            this.txtART_FamAtoll.AccAllowComma = false;
            this.txtART_FamAtoll.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_FamAtoll.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_FamAtoll.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtART_FamAtoll.AccHidenValue = "";
            this.txtART_FamAtoll.AccNotAllowedChars = null;
            this.txtART_FamAtoll.AccReadOnly = false;
            this.txtART_FamAtoll.AccReadOnlyAllowDelete = false;
            this.txtART_FamAtoll.AccRequired = false;
            this.txtART_FamAtoll.BackColor = System.Drawing.Color.White;
            this.txtART_FamAtoll.CustomBackColor = System.Drawing.Color.White;
            this.txtART_FamAtoll.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_FamAtoll.ForeColor = System.Drawing.Color.Black;
            this.txtART_FamAtoll.Location = new System.Drawing.Point(311, 51);
            this.txtART_FamAtoll.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_FamAtoll.MaxLength = 32767;
            this.txtART_FamAtoll.Multiline = false;
            this.txtART_FamAtoll.Name = "txtART_FamAtoll";
            this.txtART_FamAtoll.ReadOnly = false;
            this.txtART_FamAtoll.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_FamAtoll.Size = new System.Drawing.Size(135, 27);
            this.txtART_FamAtoll.TabIndex = 541;
            this.txtART_FamAtoll.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_FamAtoll.UseSystemPasswordChar = false;
            // 
            // label75
            // 
            this.label75.AutoSize = true;
            this.label75.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label75.Location = new System.Drawing.Point(246, 50);
            this.label75.Name = "label75";
            this.label75.Size = new System.Drawing.Size(97, 16);
            this.label75.TabIndex = 540;
            this.label75.Text = "Famille ATOLL";
            // 
            // txtART_Nature
            // 
            this.txtART_Nature.AccAcceptNumbersOnly = false;
            this.txtART_Nature.AccAllowComma = false;
            this.txtART_Nature.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Nature.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Nature.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtART_Nature.AccHidenValue = "";
            this.txtART_Nature.AccNotAllowedChars = null;
            this.txtART_Nature.AccReadOnly = false;
            this.txtART_Nature.AccReadOnlyAllowDelete = false;
            this.txtART_Nature.AccRequired = false;
            this.txtART_Nature.BackColor = System.Drawing.Color.White;
            this.txtART_Nature.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Nature.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Nature.ForeColor = System.Drawing.Color.Black;
            this.txtART_Nature.Location = new System.Drawing.Point(311, 20);
            this.txtART_Nature.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Nature.MaxLength = 32767;
            this.txtART_Nature.Multiline = false;
            this.txtART_Nature.Name = "txtART_Nature";
            this.txtART_Nature.ReadOnly = false;
            this.txtART_Nature.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Nature.Size = new System.Drawing.Size(135, 27);
            this.txtART_Nature.TabIndex = 539;
            this.txtART_Nature.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Nature.UseSystemPasswordChar = false;
            // 
            // label74
            // 
            this.label74.AutoSize = true;
            this.label74.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label74.Location = new System.Drawing.Point(246, 19);
            this.label74.Name = "label74";
            this.label74.Size = new System.Drawing.Size(48, 16);
            this.label74.TabIndex = 538;
            this.label74.Text = "Nature";
            // 
            // txtCODE_COND_FOURNISSEUR
            // 
            this.txtCODE_COND_FOURNISSEUR.AccAcceptNumbersOnly = false;
            this.txtCODE_COND_FOURNISSEUR.AccAllowComma = false;
            this.txtCODE_COND_FOURNISSEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCODE_COND_FOURNISSEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCODE_COND_FOURNISSEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCODE_COND_FOURNISSEUR.AccHidenValue = "";
            this.txtCODE_COND_FOURNISSEUR.AccNotAllowedChars = null;
            this.txtCODE_COND_FOURNISSEUR.AccReadOnly = false;
            this.txtCODE_COND_FOURNISSEUR.AccReadOnlyAllowDelete = false;
            this.txtCODE_COND_FOURNISSEUR.AccRequired = false;
            this.txtCODE_COND_FOURNISSEUR.BackColor = System.Drawing.Color.White;
            this.txtCODE_COND_FOURNISSEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtCODE_COND_FOURNISSEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCODE_COND_FOURNISSEUR.ForeColor = System.Drawing.Color.Black;
            this.txtCODE_COND_FOURNISSEUR.Location = new System.Drawing.Point(109, 274);
            this.txtCODE_COND_FOURNISSEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtCODE_COND_FOURNISSEUR.MaxLength = 32767;
            this.txtCODE_COND_FOURNISSEUR.Multiline = false;
            this.txtCODE_COND_FOURNISSEUR.Name = "txtCODE_COND_FOURNISSEUR";
            this.txtCODE_COND_FOURNISSEUR.ReadOnly = false;
            this.txtCODE_COND_FOURNISSEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCODE_COND_FOURNISSEUR.Size = new System.Drawing.Size(135, 27);
            this.txtCODE_COND_FOURNISSEUR.TabIndex = 523;
            this.txtCODE_COND_FOURNISSEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCODE_COND_FOURNISSEUR.UseSystemPasswordChar = false;
            // 
            // label66
            // 
            this.label66.AutoSize = true;
            this.label66.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label66.Location = new System.Drawing.Point(9, 278);
            this.label66.Name = "label66";
            this.label66.Size = new System.Drawing.Size(99, 16);
            this.label66.TabIndex = 522;
            this.label66.Text = "Code unt. fourn.";
            // 
            // txtCODE_UNITE_METIER
            // 
            this.txtCODE_UNITE_METIER.AccAcceptNumbersOnly = false;
            this.txtCODE_UNITE_METIER.AccAllowComma = false;
            this.txtCODE_UNITE_METIER.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_METIER.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCODE_UNITE_METIER.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCODE_UNITE_METIER.AccHidenValue = "";
            this.txtCODE_UNITE_METIER.AccNotAllowedChars = null;
            this.txtCODE_UNITE_METIER.AccReadOnly = false;
            this.txtCODE_UNITE_METIER.AccReadOnlyAllowDelete = false;
            this.txtCODE_UNITE_METIER.AccRequired = false;
            this.txtCODE_UNITE_METIER.BackColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_METIER.CustomBackColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_METIER.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCODE_UNITE_METIER.ForeColor = System.Drawing.Color.Black;
            this.txtCODE_UNITE_METIER.Location = new System.Drawing.Point(106, 251);
            this.txtCODE_UNITE_METIER.Margin = new System.Windows.Forms.Padding(2);
            this.txtCODE_UNITE_METIER.MaxLength = 32767;
            this.txtCODE_UNITE_METIER.Multiline = false;
            this.txtCODE_UNITE_METIER.Name = "txtCODE_UNITE_METIER";
            this.txtCODE_UNITE_METIER.ReadOnly = false;
            this.txtCODE_UNITE_METIER.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCODE_UNITE_METIER.Size = new System.Drawing.Size(135, 27);
            this.txtCODE_UNITE_METIER.TabIndex = 521;
            this.txtCODE_UNITE_METIER.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCODE_UNITE_METIER.UseSystemPasswordChar = false;
            // 
            // label65
            // 
            this.label65.AutoSize = true;
            this.label65.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label65.Location = new System.Drawing.Point(6, 252);
            this.label65.Name = "label65";
            this.label65.Size = new System.Drawing.Size(109, 16);
            this.label65.TabIndex = 520;
            this.label65.Text = "Code cond. fourn";
            // 
            // txtCODE_UNITE_DEVIS_2
            // 
            this.txtCODE_UNITE_DEVIS_2.AccAcceptNumbersOnly = false;
            this.txtCODE_UNITE_DEVIS_2.AccAllowComma = false;
            this.txtCODE_UNITE_DEVIS_2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_DEVIS_2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCODE_UNITE_DEVIS_2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCODE_UNITE_DEVIS_2.AccHidenValue = "";
            this.txtCODE_UNITE_DEVIS_2.AccNotAllowedChars = null;
            this.txtCODE_UNITE_DEVIS_2.AccReadOnly = false;
            this.txtCODE_UNITE_DEVIS_2.AccReadOnlyAllowDelete = false;
            this.txtCODE_UNITE_DEVIS_2.AccRequired = false;
            this.txtCODE_UNITE_DEVIS_2.BackColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_DEVIS_2.CustomBackColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_DEVIS_2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCODE_UNITE_DEVIS_2.ForeColor = System.Drawing.Color.Black;
            this.txtCODE_UNITE_DEVIS_2.Location = new System.Drawing.Point(106, 225);
            this.txtCODE_UNITE_DEVIS_2.Margin = new System.Windows.Forms.Padding(2);
            this.txtCODE_UNITE_DEVIS_2.MaxLength = 32767;
            this.txtCODE_UNITE_DEVIS_2.Multiline = false;
            this.txtCODE_UNITE_DEVIS_2.Name = "txtCODE_UNITE_DEVIS_2";
            this.txtCODE_UNITE_DEVIS_2.ReadOnly = false;
            this.txtCODE_UNITE_DEVIS_2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCODE_UNITE_DEVIS_2.Size = new System.Drawing.Size(135, 27);
            this.txtCODE_UNITE_DEVIS_2.TabIndex = 519;
            this.txtCODE_UNITE_DEVIS_2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCODE_UNITE_DEVIS_2.UseSystemPasswordChar = false;
            // 
            // label64
            // 
            this.label64.AutoSize = true;
            this.label64.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label64.Location = new System.Drawing.Point(6, 226);
            this.label64.Name = "label64";
            this.label64.Size = new System.Drawing.Size(104, 16);
            this.label64.TabIndex = 518;
            this.label64.Text = "Code unt. métier";
            // 
            // txtCODE_UNITE_STOCK
            // 
            this.txtCODE_UNITE_STOCK.AccAcceptNumbersOnly = false;
            this.txtCODE_UNITE_STOCK.AccAllowComma = false;
            this.txtCODE_UNITE_STOCK.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_STOCK.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCODE_UNITE_STOCK.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCODE_UNITE_STOCK.AccHidenValue = "";
            this.txtCODE_UNITE_STOCK.AccNotAllowedChars = null;
            this.txtCODE_UNITE_STOCK.AccReadOnly = false;
            this.txtCODE_UNITE_STOCK.AccReadOnlyAllowDelete = false;
            this.txtCODE_UNITE_STOCK.AccRequired = false;
            this.txtCODE_UNITE_STOCK.BackColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_STOCK.CustomBackColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_STOCK.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCODE_UNITE_STOCK.ForeColor = System.Drawing.Color.Black;
            this.txtCODE_UNITE_STOCK.Location = new System.Drawing.Point(106, 198);
            this.txtCODE_UNITE_STOCK.Margin = new System.Windows.Forms.Padding(2);
            this.txtCODE_UNITE_STOCK.MaxLength = 32767;
            this.txtCODE_UNITE_STOCK.Multiline = false;
            this.txtCODE_UNITE_STOCK.Name = "txtCODE_UNITE_STOCK";
            this.txtCODE_UNITE_STOCK.ReadOnly = false;
            this.txtCODE_UNITE_STOCK.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCODE_UNITE_STOCK.Size = new System.Drawing.Size(135, 27);
            this.txtCODE_UNITE_STOCK.TabIndex = 517;
            this.txtCODE_UNITE_STOCK.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCODE_UNITE_STOCK.UseSystemPasswordChar = false;
            // 
            // label63
            // 
            this.label63.AutoSize = true;
            this.label63.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label63.Location = new System.Drawing.Point(6, 199);
            this.label63.Name = "label63";
            this.label63.Size = new System.Drawing.Size(100, 16);
            this.label63.TabIndex = 516;
            this.label63.Text = "Code unt. devis";
            // 
            // txtCODE_COND_STOCK
            // 
            this.txtCODE_COND_STOCK.AccAcceptNumbersOnly = false;
            this.txtCODE_COND_STOCK.AccAllowComma = false;
            this.txtCODE_COND_STOCK.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCODE_COND_STOCK.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCODE_COND_STOCK.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCODE_COND_STOCK.AccHidenValue = "";
            this.txtCODE_COND_STOCK.AccNotAllowedChars = null;
            this.txtCODE_COND_STOCK.AccReadOnly = false;
            this.txtCODE_COND_STOCK.AccReadOnlyAllowDelete = false;
            this.txtCODE_COND_STOCK.AccRequired = false;
            this.txtCODE_COND_STOCK.BackColor = System.Drawing.Color.White;
            this.txtCODE_COND_STOCK.CustomBackColor = System.Drawing.Color.White;
            this.txtCODE_COND_STOCK.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCODE_COND_STOCK.ForeColor = System.Drawing.Color.Black;
            this.txtCODE_COND_STOCK.Location = new System.Drawing.Point(106, 172);
            this.txtCODE_COND_STOCK.Margin = new System.Windows.Forms.Padding(2);
            this.txtCODE_COND_STOCK.MaxLength = 32767;
            this.txtCODE_COND_STOCK.Multiline = false;
            this.txtCODE_COND_STOCK.Name = "txtCODE_COND_STOCK";
            this.txtCODE_COND_STOCK.ReadOnly = false;
            this.txtCODE_COND_STOCK.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCODE_COND_STOCK.Size = new System.Drawing.Size(135, 27);
            this.txtCODE_COND_STOCK.TabIndex = 515;
            this.txtCODE_COND_STOCK.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCODE_COND_STOCK.UseSystemPasswordChar = false;
            // 
            // label62
            // 
            this.label62.AutoSize = true;
            this.label62.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label62.Location = new System.Drawing.Point(6, 173);
            this.label62.Name = "label62";
            this.label62.Size = new System.Drawing.Size(99, 16);
            this.label62.TabIndex = 514;
            this.label62.Text = "Code unt. stock";
            // 
            // txtCODE_UNITE_VALEUR
            // 
            this.txtCODE_UNITE_VALEUR.AccAcceptNumbersOnly = false;
            this.txtCODE_UNITE_VALEUR.AccAllowComma = false;
            this.txtCODE_UNITE_VALEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_VALEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCODE_UNITE_VALEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCODE_UNITE_VALEUR.AccHidenValue = "";
            this.txtCODE_UNITE_VALEUR.AccNotAllowedChars = null;
            this.txtCODE_UNITE_VALEUR.AccReadOnly = false;
            this.txtCODE_UNITE_VALEUR.AccReadOnlyAllowDelete = false;
            this.txtCODE_UNITE_VALEUR.AccRequired = false;
            this.txtCODE_UNITE_VALEUR.BackColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_VALEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_VALEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCODE_UNITE_VALEUR.ForeColor = System.Drawing.Color.Black;
            this.txtCODE_UNITE_VALEUR.Location = new System.Drawing.Point(106, 145);
            this.txtCODE_UNITE_VALEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtCODE_UNITE_VALEUR.MaxLength = 32767;
            this.txtCODE_UNITE_VALEUR.Multiline = false;
            this.txtCODE_UNITE_VALEUR.Name = "txtCODE_UNITE_VALEUR";
            this.txtCODE_UNITE_VALEUR.ReadOnly = false;
            this.txtCODE_UNITE_VALEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCODE_UNITE_VALEUR.Size = new System.Drawing.Size(135, 27);
            this.txtCODE_UNITE_VALEUR.TabIndex = 513;
            this.txtCODE_UNITE_VALEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCODE_UNITE_VALEUR.UseSystemPasswordChar = false;
            // 
            // label61
            // 
            this.label61.AutoSize = true;
            this.label61.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label61.Location = new System.Drawing.Point(6, 146);
            this.label61.Name = "label61";
            this.label61.Size = new System.Drawing.Size(112, 16);
            this.label61.TabIndex = 512;
            this.label61.Text = "Code cond. stock";
            // 
            // txtCODE_UNITE_DEVIS
            // 
            this.txtCODE_UNITE_DEVIS.AccAcceptNumbersOnly = false;
            this.txtCODE_UNITE_DEVIS.AccAllowComma = false;
            this.txtCODE_UNITE_DEVIS.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_DEVIS.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCODE_UNITE_DEVIS.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCODE_UNITE_DEVIS.AccHidenValue = "";
            this.txtCODE_UNITE_DEVIS.AccNotAllowedChars = null;
            this.txtCODE_UNITE_DEVIS.AccReadOnly = false;
            this.txtCODE_UNITE_DEVIS.AccReadOnlyAllowDelete = false;
            this.txtCODE_UNITE_DEVIS.AccRequired = false;
            this.txtCODE_UNITE_DEVIS.BackColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_DEVIS.CustomBackColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_DEVIS.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCODE_UNITE_DEVIS.ForeColor = System.Drawing.Color.Black;
            this.txtCODE_UNITE_DEVIS.Location = new System.Drawing.Point(106, 118);
            this.txtCODE_UNITE_DEVIS.Margin = new System.Windows.Forms.Padding(2);
            this.txtCODE_UNITE_DEVIS.MaxLength = 32767;
            this.txtCODE_UNITE_DEVIS.Multiline = false;
            this.txtCODE_UNITE_DEVIS.Name = "txtCODE_UNITE_DEVIS";
            this.txtCODE_UNITE_DEVIS.ReadOnly = false;
            this.txtCODE_UNITE_DEVIS.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCODE_UNITE_DEVIS.Size = new System.Drawing.Size(135, 27);
            this.txtCODE_UNITE_DEVIS.TabIndex = 511;
            this.txtCODE_UNITE_DEVIS.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCODE_UNITE_DEVIS.UseSystemPasswordChar = false;
            // 
            // label60
            // 
            this.label60.AutoSize = true;
            this.label60.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label60.Location = new System.Drawing.Point(6, 119);
            this.label60.Name = "label60";
            this.label60.Size = new System.Drawing.Size(101, 16);
            this.label60.TabIndex = 510;
            this.label60.Text = "Code unt.valeur";
            // 
            // txtUNITE_DEVIS
            // 
            this.txtUNITE_DEVIS.AccAcceptNumbersOnly = false;
            this.txtUNITE_DEVIS.AccAllowComma = false;
            this.txtUNITE_DEVIS.AccBackgroundColor = System.Drawing.Color.White;
            this.txtUNITE_DEVIS.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtUNITE_DEVIS.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtUNITE_DEVIS.AccHidenValue = "";
            this.txtUNITE_DEVIS.AccNotAllowedChars = null;
            this.txtUNITE_DEVIS.AccReadOnly = false;
            this.txtUNITE_DEVIS.AccReadOnlyAllowDelete = false;
            this.txtUNITE_DEVIS.AccRequired = false;
            this.txtUNITE_DEVIS.BackColor = System.Drawing.Color.White;
            this.txtUNITE_DEVIS.CustomBackColor = System.Drawing.Color.White;
            this.txtUNITE_DEVIS.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtUNITE_DEVIS.ForeColor = System.Drawing.Color.Black;
            this.txtUNITE_DEVIS.Location = new System.Drawing.Point(106, 93);
            this.txtUNITE_DEVIS.Margin = new System.Windows.Forms.Padding(2);
            this.txtUNITE_DEVIS.MaxLength = 32767;
            this.txtUNITE_DEVIS.Multiline = false;
            this.txtUNITE_DEVIS.Name = "txtUNITE_DEVIS";
            this.txtUNITE_DEVIS.ReadOnly = false;
            this.txtUNITE_DEVIS.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtUNITE_DEVIS.Size = new System.Drawing.Size(135, 27);
            this.txtUNITE_DEVIS.TabIndex = 509;
            this.txtUNITE_DEVIS.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtUNITE_DEVIS.UseSystemPasswordChar = false;
            // 
            // label59
            // 
            this.label59.AutoSize = true;
            this.label59.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label59.Location = new System.Drawing.Point(6, 94);
            this.label59.Name = "label59";
            this.label59.Size = new System.Drawing.Size(75, 16);
            this.label59.TabIndex = 508;
            this.label59.Text = "Unité devis";
            // 
            // txtART_Composant
            // 
            this.txtART_Composant.AccAcceptNumbersOnly = false;
            this.txtART_Composant.AccAllowComma = false;
            this.txtART_Composant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_Composant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_Composant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtART_Composant.AccHidenValue = "";
            this.txtART_Composant.AccNotAllowedChars = null;
            this.txtART_Composant.AccReadOnly = false;
            this.txtART_Composant.AccReadOnlyAllowDelete = false;
            this.txtART_Composant.AccRequired = false;
            this.txtART_Composant.BackColor = System.Drawing.Color.White;
            this.txtART_Composant.CustomBackColor = System.Drawing.Color.White;
            this.txtART_Composant.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_Composant.ForeColor = System.Drawing.Color.Black;
            this.txtART_Composant.Location = new System.Drawing.Point(106, 71);
            this.txtART_Composant.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_Composant.MaxLength = 32767;
            this.txtART_Composant.Multiline = false;
            this.txtART_Composant.Name = "txtART_Composant";
            this.txtART_Composant.ReadOnly = false;
            this.txtART_Composant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_Composant.Size = new System.Drawing.Size(135, 27);
            this.txtART_Composant.TabIndex = 507;
            this.txtART_Composant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_Composant.UseSystemPasswordChar = false;
            // 
            // label58
            // 
            this.label58.AutoSize = true;
            this.label58.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label58.Location = new System.Drawing.Point(6, 72);
            this.label58.Name = "label58";
            this.label58.Size = new System.Drawing.Size(98, 16);
            this.label58.TabIndex = 506;
            this.label58.Text = "Code unt. base";
            // 
            // txtUNITE_BASE
            // 
            this.txtUNITE_BASE.AccAcceptNumbersOnly = false;
            this.txtUNITE_BASE.AccAllowComma = false;
            this.txtUNITE_BASE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtUNITE_BASE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtUNITE_BASE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtUNITE_BASE.AccHidenValue = "";
            this.txtUNITE_BASE.AccNotAllowedChars = null;
            this.txtUNITE_BASE.AccReadOnly = false;
            this.txtUNITE_BASE.AccReadOnlyAllowDelete = false;
            this.txtUNITE_BASE.AccRequired = false;
            this.txtUNITE_BASE.BackColor = System.Drawing.Color.White;
            this.txtUNITE_BASE.CustomBackColor = System.Drawing.Color.White;
            this.txtUNITE_BASE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtUNITE_BASE.ForeColor = System.Drawing.Color.Black;
            this.txtUNITE_BASE.Location = new System.Drawing.Point(106, 44);
            this.txtUNITE_BASE.Margin = new System.Windows.Forms.Padding(2);
            this.txtUNITE_BASE.MaxLength = 32767;
            this.txtUNITE_BASE.Multiline = false;
            this.txtUNITE_BASE.Name = "txtUNITE_BASE";
            this.txtUNITE_BASE.ReadOnly = false;
            this.txtUNITE_BASE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtUNITE_BASE.Size = new System.Drawing.Size(135, 27);
            this.txtUNITE_BASE.TabIndex = 505;
            this.txtUNITE_BASE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtUNITE_BASE.UseSystemPasswordChar = false;
            // 
            // label57
            // 
            this.label57.AutoSize = true;
            this.label57.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label57.Location = new System.Drawing.Point(6, 45);
            this.label57.Name = "label57";
            this.label57.Size = new System.Drawing.Size(66, 16);
            this.label57.TabIndex = 504;
            this.label57.Text = "Unt. Base";
            // 
            // txtID_FAMILLE
            // 
            this.txtID_FAMILLE.AccAcceptNumbersOnly = false;
            this.txtID_FAMILLE.AccAllowComma = false;
            this.txtID_FAMILLE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtID_FAMILLE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtID_FAMILLE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtID_FAMILLE.AccHidenValue = "";
            this.txtID_FAMILLE.AccNotAllowedChars = null;
            this.txtID_FAMILLE.AccReadOnly = false;
            this.txtID_FAMILLE.AccReadOnlyAllowDelete = false;
            this.txtID_FAMILLE.AccRequired = false;
            this.txtID_FAMILLE.BackColor = System.Drawing.Color.White;
            this.txtID_FAMILLE.CustomBackColor = System.Drawing.Color.White;
            this.txtID_FAMILLE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtID_FAMILLE.ForeColor = System.Drawing.Color.Black;
            this.txtID_FAMILLE.Location = new System.Drawing.Point(106, 19);
            this.txtID_FAMILLE.Margin = new System.Windows.Forms.Padding(2);
            this.txtID_FAMILLE.MaxLength = 32767;
            this.txtID_FAMILLE.Multiline = false;
            this.txtID_FAMILLE.Name = "txtID_FAMILLE";
            this.txtID_FAMILLE.ReadOnly = false;
            this.txtID_FAMILLE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtID_FAMILLE.Size = new System.Drawing.Size(135, 27);
            this.txtID_FAMILLE.TabIndex = 503;
            this.txtID_FAMILLE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtID_FAMILLE.UseSystemPasswordChar = false;
            // 
            // label68
            // 
            this.label68.AutoSize = true;
            this.label68.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label68.Location = new System.Drawing.Point(6, 20);
            this.label68.Name = "label68";
            this.label68.Size = new System.Drawing.Size(58, 16);
            this.label68.TabIndex = 502;
            this.label68.Text = "Id famlle";
            // 
            // iTalk_TextBox_Small21
            // 
            this.iTalk_TextBox_Small21.AccAcceptNumbersOnly = false;
            this.iTalk_TextBox_Small21.AccAllowComma = false;
            this.iTalk_TextBox_Small21.AccBackgroundColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.iTalk_TextBox_Small21.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.iTalk_TextBox_Small21.AccHidenValue = "";
            this.iTalk_TextBox_Small21.AccNotAllowedChars = null;
            this.iTalk_TextBox_Small21.AccReadOnly = false;
            this.iTalk_TextBox_Small21.AccReadOnlyAllowDelete = false;
            this.iTalk_TextBox_Small21.AccRequired = false;
            this.iTalk_TextBox_Small21.BackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.CustomBackColor = System.Drawing.Color.White;
            this.iTalk_TextBox_Small21.Font = new System.Drawing.Font("Tahoma", 10F);
            this.iTalk_TextBox_Small21.ForeColor = System.Drawing.Color.DodgerBlue;
            this.iTalk_TextBox_Small21.Location = new System.Drawing.Point(172, 419);
            this.iTalk_TextBox_Small21.Margin = new System.Windows.Forms.Padding(2);
            this.iTalk_TextBox_Small21.MaxLength = 32767;
            this.iTalk_TextBox_Small21.Multiline = false;
            this.iTalk_TextBox_Small21.Name = "iTalk_TextBox_Small21";
            this.iTalk_TextBox_Small21.ReadOnly = false;
            this.iTalk_TextBox_Small21.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.iTalk_TextBox_Small21.Size = new System.Drawing.Size(187, 27);
            this.iTalk_TextBox_Small21.TabIndex = 590;
            this.iTalk_TextBox_Small21.TextAlignment = Infragistics.Win.HAlign.Left;
            this.iTalk_TextBox_Small21.UseSystemPasswordChar = false;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(10, 413);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(77, 16);
            this.label19.TabIndex = 589;
            this.label19.Text = "synonymes";
            // 
            // label93
            // 
            this.label93.AutoSize = true;
            this.label93.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label93.Location = new System.Drawing.Point(10, 449);
            this.label93.Name = "label93";
            this.label93.Size = new System.Drawing.Size(124, 16);
            this.label93.TabIndex = 588;
            this.label93.Text = "Ancienne référence";
            // 
            // txtANCIENNE_REFERENCE
            // 
            this.txtANCIENNE_REFERENCE.AccAcceptNumbersOnly = false;
            this.txtANCIENNE_REFERENCE.AccAllowComma = false;
            this.txtANCIENNE_REFERENCE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtANCIENNE_REFERENCE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtANCIENNE_REFERENCE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtANCIENNE_REFERENCE.AccHidenValue = "";
            this.txtANCIENNE_REFERENCE.AccNotAllowedChars = null;
            this.txtANCIENNE_REFERENCE.AccReadOnly = false;
            this.txtANCIENNE_REFERENCE.AccReadOnlyAllowDelete = false;
            this.txtANCIENNE_REFERENCE.AccRequired = false;
            this.txtANCIENNE_REFERENCE.BackColor = System.Drawing.Color.White;
            this.txtANCIENNE_REFERENCE.CustomBackColor = System.Drawing.Color.White;
            this.txtANCIENNE_REFERENCE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtANCIENNE_REFERENCE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtANCIENNE_REFERENCE.Location = new System.Drawing.Point(171, 451);
            this.txtANCIENNE_REFERENCE.Margin = new System.Windows.Forms.Padding(2);
            this.txtANCIENNE_REFERENCE.MaxLength = 32767;
            this.txtANCIENNE_REFERENCE.Multiline = false;
            this.txtANCIENNE_REFERENCE.Name = "txtANCIENNE_REFERENCE";
            this.txtANCIENNE_REFERENCE.ReadOnly = false;
            this.txtANCIENNE_REFERENCE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtANCIENNE_REFERENCE.Size = new System.Drawing.Size(187, 27);
            this.txtANCIENNE_REFERENCE.TabIndex = 587;
            this.txtANCIENNE_REFERENCE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtANCIENNE_REFERENCE.UseSystemPasswordChar = false;
            // 
            // txtNATURE
            // 
            this.txtNATURE.AccAcceptNumbersOnly = false;
            this.txtNATURE.AccAllowComma = false;
            this.txtNATURE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNATURE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNATURE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtNATURE.AccHidenValue = "";
            this.txtNATURE.AccNotAllowedChars = null;
            this.txtNATURE.AccReadOnly = false;
            this.txtNATURE.AccReadOnlyAllowDelete = false;
            this.txtNATURE.AccRequired = false;
            this.txtNATURE.BackColor = System.Drawing.Color.White;
            this.txtNATURE.CustomBackColor = System.Drawing.Color.White;
            this.txtNATURE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNATURE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtNATURE.Location = new System.Drawing.Point(1002, 231);
            this.txtNATURE.Margin = new System.Windows.Forms.Padding(2);
            this.txtNATURE.MaxLength = 32767;
            this.txtNATURE.Multiline = false;
            this.txtNATURE.Name = "txtNATURE";
            this.txtNATURE.ReadOnly = false;
            this.txtNATURE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNATURE.Size = new System.Drawing.Size(187, 27);
            this.txtNATURE.TabIndex = 586;
            this.txtNATURE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNATURE.UseSystemPasswordChar = false;
            // 
            // label92
            // 
            this.label92.AutoSize = true;
            this.label92.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label92.Location = new System.Drawing.Point(11, 354);
            this.label92.Name = "label92";
            this.label92.Size = new System.Drawing.Size(127, 16);
            this.label92.TabIndex = 585;
            this.label92.Text = "Regroupement Atoll";
            // 
            // txtSPECIAL_BBATH
            // 
            this.txtSPECIAL_BBATH.AccAcceptNumbersOnly = false;
            this.txtSPECIAL_BBATH.AccAllowComma = false;
            this.txtSPECIAL_BBATH.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSPECIAL_BBATH.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSPECIAL_BBATH.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtSPECIAL_BBATH.AccHidenValue = "";
            this.txtSPECIAL_BBATH.AccNotAllowedChars = null;
            this.txtSPECIAL_BBATH.AccReadOnly = false;
            this.txtSPECIAL_BBATH.AccReadOnlyAllowDelete = false;
            this.txtSPECIAL_BBATH.AccRequired = false;
            this.txtSPECIAL_BBATH.BackColor = System.Drawing.Color.White;
            this.txtSPECIAL_BBATH.CustomBackColor = System.Drawing.Color.White;
            this.txtSPECIAL_BBATH.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtSPECIAL_BBATH.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtSPECIAL_BBATH.Location = new System.Drawing.Point(551, 299);
            this.txtSPECIAL_BBATH.Margin = new System.Windows.Forms.Padding(2);
            this.txtSPECIAL_BBATH.MaxLength = 32767;
            this.txtSPECIAL_BBATH.Multiline = false;
            this.txtSPECIAL_BBATH.Name = "txtSPECIAL_BBATH";
            this.txtSPECIAL_BBATH.ReadOnly = false;
            this.txtSPECIAL_BBATH.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSPECIAL_BBATH.Size = new System.Drawing.Size(187, 27);
            this.txtSPECIAL_BBATH.TabIndex = 584;
            this.txtSPECIAL_BBATH.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSPECIAL_BBATH.UseSystemPasswordChar = false;
            // 
            // txtSUPPRIME_CHEZ_FABRIQUANT
            // 
            this.txtSUPPRIME_CHEZ_FABRIQUANT.AccAcceptNumbersOnly = false;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.AccAllowComma = false;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtSUPPRIME_CHEZ_FABRIQUANT.AccHidenValue = "";
            this.txtSUPPRIME_CHEZ_FABRIQUANT.AccNotAllowedChars = null;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.AccReadOnly = false;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.AccReadOnlyAllowDelete = false;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.AccRequired = false;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.BackColor = System.Drawing.Color.White;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.CustomBackColor = System.Drawing.Color.White;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtSUPPRIME_CHEZ_FABRIQUANT.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.Location = new System.Drawing.Point(1004, 299);
            this.txtSUPPRIME_CHEZ_FABRIQUANT.Margin = new System.Windows.Forms.Padding(2);
            this.txtSUPPRIME_CHEZ_FABRIQUANT.MaxLength = 32767;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.Multiline = false;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.Name = "txtSUPPRIME_CHEZ_FABRIQUANT";
            this.txtSUPPRIME_CHEZ_FABRIQUANT.ReadOnly = false;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.Size = new System.Drawing.Size(187, 27);
            this.txtSUPPRIME_CHEZ_FABRIQUANT.TabIndex = 583;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSUPPRIME_CHEZ_FABRIQUANT.UseSystemPasswordChar = false;
            // 
            // label91
            // 
            this.label91.AutoSize = true;
            this.label91.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label91.Location = new System.Drawing.Point(800, 309);
            this.label91.Name = "label91";
            this.label91.Size = new System.Drawing.Size(149, 16);
            this.label91.TabIndex = 582;
            this.label91.Text = "supprimé chez fabricant";
            // 
            // label90
            // 
            this.label90.AutoSize = true;
            this.label90.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label90.Location = new System.Drawing.Point(385, 309);
            this.label90.Name = "label90";
            this.label90.Size = new System.Drawing.Size(92, 16);
            this.label90.TabIndex = 581;
            this.label90.Text = "Spécial Bbath";
            // 
            // txtLIBELLE_DEVIS
            // 
            this.txtLIBELLE_DEVIS.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_DEVIS.AccAllowComma = false;
            this.txtLIBELLE_DEVIS.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_DEVIS.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_DEVIS.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_DEVIS.AccHidenValue = "";
            this.txtLIBELLE_DEVIS.AccNotAllowedChars = null;
            this.txtLIBELLE_DEVIS.AccReadOnly = false;
            this.txtLIBELLE_DEVIS.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_DEVIS.AccRequired = false;
            this.txtLIBELLE_DEVIS.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_DEVIS.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_DEVIS.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_DEVIS.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_DEVIS.Location = new System.Drawing.Point(172, 389);
            this.txtLIBELLE_DEVIS.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_DEVIS.MaxLength = 32767;
            this.txtLIBELLE_DEVIS.Multiline = false;
            this.txtLIBELLE_DEVIS.Name = "txtLIBELLE_DEVIS";
            this.txtLIBELLE_DEVIS.ReadOnly = false;
            this.txtLIBELLE_DEVIS.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_DEVIS.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_DEVIS.TabIndex = 580;
            this.txtLIBELLE_DEVIS.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_DEVIS.UseSystemPasswordChar = false;
            // 
            // txtREGROUPEMENT_ATOLL
            // 
            this.txtREGROUPEMENT_ATOLL.AccAcceptNumbersOnly = false;
            this.txtREGROUPEMENT_ATOLL.AccAllowComma = false;
            this.txtREGROUPEMENT_ATOLL.AccBackgroundColor = System.Drawing.Color.White;
            this.txtREGROUPEMENT_ATOLL.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtREGROUPEMENT_ATOLL.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtREGROUPEMENT_ATOLL.AccHidenValue = "";
            this.txtREGROUPEMENT_ATOLL.AccNotAllowedChars = null;
            this.txtREGROUPEMENT_ATOLL.AccReadOnly = false;
            this.txtREGROUPEMENT_ATOLL.AccReadOnlyAllowDelete = false;
            this.txtREGROUPEMENT_ATOLL.AccRequired = false;
            this.txtREGROUPEMENT_ATOLL.BackColor = System.Drawing.Color.White;
            this.txtREGROUPEMENT_ATOLL.CustomBackColor = System.Drawing.Color.White;
            this.txtREGROUPEMENT_ATOLL.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtREGROUPEMENT_ATOLL.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtREGROUPEMENT_ATOLL.Location = new System.Drawing.Point(172, 359);
            this.txtREGROUPEMENT_ATOLL.Margin = new System.Windows.Forms.Padding(2);
            this.txtREGROUPEMENT_ATOLL.MaxLength = 32767;
            this.txtREGROUPEMENT_ATOLL.Multiline = false;
            this.txtREGROUPEMENT_ATOLL.Name = "txtREGROUPEMENT_ATOLL";
            this.txtREGROUPEMENT_ATOLL.ReadOnly = false;
            this.txtREGROUPEMENT_ATOLL.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtREGROUPEMENT_ATOLL.Size = new System.Drawing.Size(187, 27);
            this.txtREGROUPEMENT_ATOLL.TabIndex = 579;
            this.txtREGROUPEMENT_ATOLL.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtREGROUPEMENT_ATOLL.UseSystemPasswordChar = false;
            // 
            // label89
            // 
            this.label89.AutoSize = true;
            this.label89.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label89.Location = new System.Drawing.Point(798, 235);
            this.label89.Name = "label89";
            this.label89.Size = new System.Drawing.Size(48, 16);
            this.label89.TabIndex = 578;
            this.label89.Text = "Nature";
            // 
            // label88
            // 
            this.label88.AutoSize = true;
            this.label88.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label88.Location = new System.Drawing.Point(11, 322);
            this.label88.Name = "label88";
            this.label88.Size = new System.Drawing.Size(81, 16);
            this.label88.TabIndex = 577;
            this.label88.Text = "Famille Atoll";
            // 
            // txtART_dateFinActivite
            // 
            this.txtART_dateFinActivite.AccAcceptNumbersOnly = false;
            this.txtART_dateFinActivite.AccAllowComma = false;
            this.txtART_dateFinActivite.AccBackgroundColor = System.Drawing.Color.White;
            this.txtART_dateFinActivite.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtART_dateFinActivite.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtART_dateFinActivite.AccHidenValue = "";
            this.txtART_dateFinActivite.AccNotAllowedChars = null;
            this.txtART_dateFinActivite.AccReadOnly = false;
            this.txtART_dateFinActivite.AccReadOnlyAllowDelete = false;
            this.txtART_dateFinActivite.AccRequired = false;
            this.txtART_dateFinActivite.BackColor = System.Drawing.Color.White;
            this.txtART_dateFinActivite.CustomBackColor = System.Drawing.Color.White;
            this.txtART_dateFinActivite.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtART_dateFinActivite.ForeColor = System.Drawing.Color.Black;
            this.txtART_dateFinActivite.Location = new System.Drawing.Point(95, 72);
            this.txtART_dateFinActivite.Margin = new System.Windows.Forms.Padding(2);
            this.txtART_dateFinActivite.MaxLength = 32767;
            this.txtART_dateFinActivite.Multiline = false;
            this.txtART_dateFinActivite.Name = "txtART_dateFinActivite";
            this.txtART_dateFinActivite.ReadOnly = false;
            this.txtART_dateFinActivite.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtART_dateFinActivite.Size = new System.Drawing.Size(57, 27);
            this.txtART_dateFinActivite.TabIndex = 576;
            this.txtART_dateFinActivite.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtART_dateFinActivite.UseSystemPasswordChar = false;
            this.txtART_dateFinActivite.Visible = false;
            // 
            // txtNoAuto
            // 
            this.txtNoAuto.AccAcceptNumbersOnly = false;
            this.txtNoAuto.AccAllowComma = false;
            this.txtNoAuto.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoAuto.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoAuto.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoAuto.AccHidenValue = "";
            this.txtNoAuto.AccNotAllowedChars = null;
            this.txtNoAuto.AccReadOnly = false;
            this.txtNoAuto.AccReadOnlyAllowDelete = false;
            this.txtNoAuto.AccRequired = false;
            this.txtNoAuto.BackColor = System.Drawing.Color.White;
            this.txtNoAuto.CustomBackColor = System.Drawing.Color.White;
            this.txtNoAuto.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNoAuto.ForeColor = System.Drawing.Color.Black;
            this.txtNoAuto.Location = new System.Drawing.Point(33, 66);
            this.txtNoAuto.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoAuto.MaxLength = 32767;
            this.txtNoAuto.Multiline = false;
            this.txtNoAuto.Name = "txtNoAuto";
            this.txtNoAuto.ReadOnly = false;
            this.txtNoAuto.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoAuto.Size = new System.Drawing.Size(57, 27);
            this.txtNoAuto.TabIndex = 575;
            this.txtNoAuto.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoAuto.UseSystemPasswordChar = false;
            this.txtNoAuto.Visible = false;
            // 
            // GridCond
            // 
            this.GridCond.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.GridCond.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridCond.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridCond.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridCond.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridCond.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridCond.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridCond.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.GridCond.DisplayLayout.UseFixedHeaders = true;
            this.GridCond.Location = new System.Drawing.Point(379, 341);
            this.GridCond.Name = "GridCond";
            this.GridCond.Size = new System.Drawing.Size(420, 224);
            this.GridCond.TabIndex = 572;
            this.GridCond.Text = "Conditionnement";
            this.GridCond.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridCond_InitializeLayout);
            this.GridCond.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridCond_BeforeRowsDeleted);
            // 
            // txtCORPS_D_ETAT
            // 
            this.txtCORPS_D_ETAT.AccAcceptNumbersOnly = false;
            this.txtCORPS_D_ETAT.AccAllowComma = false;
            this.txtCORPS_D_ETAT.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCORPS_D_ETAT.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCORPS_D_ETAT.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCORPS_D_ETAT.AccHidenValue = "";
            this.txtCORPS_D_ETAT.AccNotAllowedChars = null;
            this.txtCORPS_D_ETAT.AccReadOnly = false;
            this.txtCORPS_D_ETAT.AccReadOnlyAllowDelete = false;
            this.txtCORPS_D_ETAT.AccRequired = false;
            this.txtCORPS_D_ETAT.BackColor = System.Drawing.Color.White;
            this.txtCORPS_D_ETAT.CustomBackColor = System.Drawing.Color.White;
            this.txtCORPS_D_ETAT.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCORPS_D_ETAT.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtCORPS_D_ETAT.Location = new System.Drawing.Point(1002, 198);
            this.txtCORPS_D_ETAT.Margin = new System.Windows.Forms.Padding(2);
            this.txtCORPS_D_ETAT.MaxLength = 32767;
            this.txtCORPS_D_ETAT.Multiline = false;
            this.txtCORPS_D_ETAT.Name = "txtCORPS_D_ETAT";
            this.txtCORPS_D_ETAT.ReadOnly = false;
            this.txtCORPS_D_ETAT.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCORPS_D_ETAT.Size = new System.Drawing.Size(187, 27);
            this.txtCORPS_D_ETAT.TabIndex = 555;
            this.txtCORPS_D_ETAT.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCORPS_D_ETAT.UseSystemPasswordChar = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(798, 202);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(80, 16);
            this.label26.TabIndex = 554;
            this.label26.Text = "Corps d\'état";
            // 
            // txtPOIDS_NET
            // 
            this.txtPOIDS_NET.AccAcceptNumbersOnly = false;
            this.txtPOIDS_NET.AccAllowComma = false;
            this.txtPOIDS_NET.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPOIDS_NET.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPOIDS_NET.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPOIDS_NET.AccHidenValue = "";
            this.txtPOIDS_NET.AccNotAllowedChars = null;
            this.txtPOIDS_NET.AccReadOnly = false;
            this.txtPOIDS_NET.AccReadOnlyAllowDelete = false;
            this.txtPOIDS_NET.AccRequired = false;
            this.txtPOIDS_NET.BackColor = System.Drawing.Color.White;
            this.txtPOIDS_NET.CustomBackColor = System.Drawing.Color.White;
            this.txtPOIDS_NET.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPOIDS_NET.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtPOIDS_NET.Location = new System.Drawing.Point(1002, 167);
            this.txtPOIDS_NET.Margin = new System.Windows.Forms.Padding(2);
            this.txtPOIDS_NET.MaxLength = 32767;
            this.txtPOIDS_NET.Multiline = false;
            this.txtPOIDS_NET.Name = "txtPOIDS_NET";
            this.txtPOIDS_NET.ReadOnly = false;
            this.txtPOIDS_NET.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPOIDS_NET.Size = new System.Drawing.Size(187, 27);
            this.txtPOIDS_NET.TabIndex = 553;
            this.txtPOIDS_NET.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPOIDS_NET.UseSystemPasswordChar = false;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(798, 170);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(64, 16);
            this.label20.TabIndex = 552;
            this.label20.Text = "Poids net";
            // 
            // txtSURFACE
            // 
            this.txtSURFACE.AccAcceptNumbersOnly = false;
            this.txtSURFACE.AccAllowComma = false;
            this.txtSURFACE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSURFACE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSURFACE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtSURFACE.AccHidenValue = "";
            this.txtSURFACE.AccNotAllowedChars = null;
            this.txtSURFACE.AccReadOnly = false;
            this.txtSURFACE.AccReadOnlyAllowDelete = false;
            this.txtSURFACE.AccRequired = false;
            this.txtSURFACE.BackColor = System.Drawing.Color.White;
            this.txtSURFACE.CustomBackColor = System.Drawing.Color.White;
            this.txtSURFACE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtSURFACE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtSURFACE.Location = new System.Drawing.Point(1002, 136);
            this.txtSURFACE.Margin = new System.Windows.Forms.Padding(2);
            this.txtSURFACE.MaxLength = 32767;
            this.txtSURFACE.Multiline = false;
            this.txtSURFACE.Name = "txtSURFACE";
            this.txtSURFACE.ReadOnly = false;
            this.txtSURFACE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSURFACE.Size = new System.Drawing.Size(187, 27);
            this.txtSURFACE.TabIndex = 551;
            this.txtSURFACE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSURFACE.UseSystemPasswordChar = false;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(798, 138);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(54, 16);
            this.label21.TabIndex = 550;
            this.label21.Text = "Surface";
            // 
            // txtLARGEUR
            // 
            this.txtLARGEUR.AccAcceptNumbersOnly = false;
            this.txtLARGEUR.AccAllowComma = false;
            this.txtLARGEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLARGEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLARGEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLARGEUR.AccHidenValue = "";
            this.txtLARGEUR.AccNotAllowedChars = null;
            this.txtLARGEUR.AccReadOnly = false;
            this.txtLARGEUR.AccReadOnlyAllowDelete = false;
            this.txtLARGEUR.AccRequired = false;
            this.txtLARGEUR.BackColor = System.Drawing.Color.White;
            this.txtLARGEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtLARGEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLARGEUR.ForeColor = System.Drawing.Color.Black;
            this.txtLARGEUR.Location = new System.Drawing.Point(1002, 105);
            this.txtLARGEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtLARGEUR.MaxLength = 32767;
            this.txtLARGEUR.Multiline = false;
            this.txtLARGEUR.Name = "txtLARGEUR";
            this.txtLARGEUR.ReadOnly = false;
            this.txtLARGEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLARGEUR.Size = new System.Drawing.Size(187, 27);
            this.txtLARGEUR.TabIndex = 549;
            this.txtLARGEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLARGEUR.UseSystemPasswordChar = false;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(798, 109);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(50, 15);
            this.label22.TabIndex = 548;
            this.label22.Text = "Largeur";
            // 
            // txtVOLUME
            // 
            this.txtVOLUME.AccAcceptNumbersOnly = false;
            this.txtVOLUME.AccAllowComma = false;
            this.txtVOLUME.AccBackgroundColor = System.Drawing.Color.White;
            this.txtVOLUME.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtVOLUME.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtVOLUME.AccHidenValue = "";
            this.txtVOLUME.AccNotAllowedChars = null;
            this.txtVOLUME.AccReadOnly = false;
            this.txtVOLUME.AccReadOnlyAllowDelete = false;
            this.txtVOLUME.AccRequired = false;
            this.txtVOLUME.BackColor = System.Drawing.Color.White;
            this.txtVOLUME.CustomBackColor = System.Drawing.Color.White;
            this.txtVOLUME.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtVOLUME.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtVOLUME.Location = new System.Drawing.Point(1004, 74);
            this.txtVOLUME.Margin = new System.Windows.Forms.Padding(2);
            this.txtVOLUME.MaxLength = 32767;
            this.txtVOLUME.Multiline = false;
            this.txtVOLUME.Name = "txtVOLUME";
            this.txtVOLUME.ReadOnly = false;
            this.txtVOLUME.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtVOLUME.Size = new System.Drawing.Size(187, 27);
            this.txtVOLUME.TabIndex = 547;
            this.txtVOLUME.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtVOLUME.UseSystemPasswordChar = false;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(800, 74);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(54, 16);
            this.label23.TabIndex = 546;
            this.label23.Text = "Volume";
            // 
            // txtHAUTEUR
            // 
            this.txtHAUTEUR.AccAcceptNumbersOnly = false;
            this.txtHAUTEUR.AccAllowComma = false;
            this.txtHAUTEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtHAUTEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtHAUTEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtHAUTEUR.AccHidenValue = "";
            this.txtHAUTEUR.AccNotAllowedChars = null;
            this.txtHAUTEUR.AccReadOnly = false;
            this.txtHAUTEUR.AccReadOnlyAllowDelete = false;
            this.txtHAUTEUR.AccRequired = false;
            this.txtHAUTEUR.BackColor = System.Drawing.Color.White;
            this.txtHAUTEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtHAUTEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtHAUTEUR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtHAUTEUR.Location = new System.Drawing.Point(1002, 43);
            this.txtHAUTEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtHAUTEUR.MaxLength = 32767;
            this.txtHAUTEUR.Multiline = false;
            this.txtHAUTEUR.Name = "txtHAUTEUR";
            this.txtHAUTEUR.ReadOnly = false;
            this.txtHAUTEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtHAUTEUR.Size = new System.Drawing.Size(187, 27);
            this.txtHAUTEUR.TabIndex = 545;
            this.txtHAUTEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtHAUTEUR.UseSystemPasswordChar = false;
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(798, 43);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(55, 16);
            this.label24.TabIndex = 544;
            this.label24.Text = "Hauteur";
            // 
            // txtLONGUEUR
            // 
            this.txtLONGUEUR.AccAcceptNumbersOnly = false;
            this.txtLONGUEUR.AccAllowComma = false;
            this.txtLONGUEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLONGUEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLONGUEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLONGUEUR.AccHidenValue = "";
            this.txtLONGUEUR.AccNotAllowedChars = null;
            this.txtLONGUEUR.AccReadOnly = false;
            this.txtLONGUEUR.AccReadOnlyAllowDelete = false;
            this.txtLONGUEUR.AccRequired = false;
            this.txtLONGUEUR.BackColor = System.Drawing.Color.White;
            this.txtLONGUEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtLONGUEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLONGUEUR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLONGUEUR.Location = new System.Drawing.Point(1002, 10);
            this.txtLONGUEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtLONGUEUR.MaxLength = 32767;
            this.txtLONGUEUR.Multiline = false;
            this.txtLONGUEUR.Name = "txtLONGUEUR";
            this.txtLONGUEUR.ReadOnly = false;
            this.txtLONGUEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLONGUEUR.Size = new System.Drawing.Size(187, 27);
            this.txtLONGUEUR.TabIndex = 543;
            this.txtLONGUEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLONGUEUR.UseSystemPasswordChar = false;
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(798, 10);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(64, 16);
            this.label25.TabIndex = 542;
            this.label25.Text = "Longueur";
            // 
            // txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT
            // 
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.AccAcceptNumbersOnly = false;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.AccAllowComma = false;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.AccHidenValue = "";
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.AccNotAllowedChars = null;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.AccReadOnly = false;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.AccReadOnlyAllowDelete = false;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.AccRequired = false;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.BackColor = System.Drawing.Color.White;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.CustomBackColor = System.Drawing.Color.White;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.Location = new System.Drawing.Point(1002, 267);
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.Margin = new System.Windows.Forms.Padding(2);
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.MaxLength = 32767;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.Multiline = false;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.Name = "txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT";
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.ReadOnly = false;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.Size = new System.Drawing.Size(187, 27);
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.TabIndex = 541;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT.UseSystemPasswordChar = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(798, 272);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(123, 16);
            this.label18.TabIndex = 540;
            this.label18.Text = "remplace le chrono";
            // 
            // txtLIEN_FICHE_SECURITE
            // 
            this.txtLIEN_FICHE_SECURITE.AccAcceptNumbersOnly = false;
            this.txtLIEN_FICHE_SECURITE.AccAllowComma = false;
            this.txtLIEN_FICHE_SECURITE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIEN_FICHE_SECURITE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIEN_FICHE_SECURITE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIEN_FICHE_SECURITE.AccHidenValue = "";
            this.txtLIEN_FICHE_SECURITE.AccNotAllowedChars = null;
            this.txtLIEN_FICHE_SECURITE.AccReadOnly = false;
            this.txtLIEN_FICHE_SECURITE.AccReadOnlyAllowDelete = false;
            this.txtLIEN_FICHE_SECURITE.AccRequired = false;
            this.txtLIEN_FICHE_SECURITE.BackColor = System.Drawing.Color.White;
            this.txtLIEN_FICHE_SECURITE.CustomBackColor = System.Drawing.Color.White;
            this.txtLIEN_FICHE_SECURITE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIEN_FICHE_SECURITE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIEN_FICHE_SECURITE.Location = new System.Drawing.Point(551, 268);
            this.txtLIEN_FICHE_SECURITE.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIEN_FICHE_SECURITE.MaxLength = 32767;
            this.txtLIEN_FICHE_SECURITE.Multiline = false;
            this.txtLIEN_FICHE_SECURITE.Name = "txtLIEN_FICHE_SECURITE";
            this.txtLIEN_FICHE_SECURITE.ReadOnly = false;
            this.txtLIEN_FICHE_SECURITE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIEN_FICHE_SECURITE.Size = new System.Drawing.Size(187, 27);
            this.txtLIEN_FICHE_SECURITE.TabIndex = 539;
            this.txtLIEN_FICHE_SECURITE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIEN_FICHE_SECURITE.UseSystemPasswordChar = false;
            this.txtLIEN_FICHE_SECURITE.Click += new System.EventHandler(this.txtLIEN_FICHE_SECURITE_Click);
            // 
            // label19s
            // 
            this.label19s.AutoSize = true;
            this.label19s.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19s.Location = new System.Drawing.Point(385, 272);
            this.label19s.Name = "label19s";
            this.label19s.Size = new System.Drawing.Size(114, 16);
            this.label19s.TabIndex = 538;
            this.label19s.Text = "Lien fiche sécurité";
            // 
            // txtLIEN_URL_TECHNIQUE
            // 
            this.txtLIEN_URL_TECHNIQUE.AccAcceptNumbersOnly = false;
            this.txtLIEN_URL_TECHNIQUE.AccAllowComma = false;
            this.txtLIEN_URL_TECHNIQUE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIEN_URL_TECHNIQUE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIEN_URL_TECHNIQUE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIEN_URL_TECHNIQUE.AccHidenValue = "";
            this.txtLIEN_URL_TECHNIQUE.AccNotAllowedChars = null;
            this.txtLIEN_URL_TECHNIQUE.AccReadOnly = false;
            this.txtLIEN_URL_TECHNIQUE.AccReadOnlyAllowDelete = false;
            this.txtLIEN_URL_TECHNIQUE.AccRequired = false;
            this.txtLIEN_URL_TECHNIQUE.BackColor = System.Drawing.Color.White;
            this.txtLIEN_URL_TECHNIQUE.CustomBackColor = System.Drawing.Color.White;
            this.txtLIEN_URL_TECHNIQUE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIEN_URL_TECHNIQUE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIEN_URL_TECHNIQUE.Location = new System.Drawing.Point(551, 237);
            this.txtLIEN_URL_TECHNIQUE.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIEN_URL_TECHNIQUE.MaxLength = 32767;
            this.txtLIEN_URL_TECHNIQUE.Multiline = false;
            this.txtLIEN_URL_TECHNIQUE.Name = "txtLIEN_URL_TECHNIQUE";
            this.txtLIEN_URL_TECHNIQUE.ReadOnly = false;
            this.txtLIEN_URL_TECHNIQUE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIEN_URL_TECHNIQUE.Size = new System.Drawing.Size(187, 27);
            this.txtLIEN_URL_TECHNIQUE.TabIndex = 537;
            this.txtLIEN_URL_TECHNIQUE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIEN_URL_TECHNIQUE.UseSystemPasswordChar = false;
            this.txtLIEN_URL_TECHNIQUE.Click += new System.EventHandler(this.txtLIEN_URL_TECHNIQUE_Click);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(385, 241);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(111, 16);
            this.label16.TabIndex = 536;
            this.label16.Text = "Lien url technique";
            // 
            // txtIMAGE
            // 
            this.txtIMAGE.AccAcceptNumbersOnly = false;
            this.txtIMAGE.AccAllowComma = false;
            this.txtIMAGE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIMAGE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIMAGE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtIMAGE.AccHidenValue = "";
            this.txtIMAGE.AccNotAllowedChars = null;
            this.txtIMAGE.AccReadOnly = false;
            this.txtIMAGE.AccReadOnlyAllowDelete = false;
            this.txtIMAGE.AccRequired = false;
            this.txtIMAGE.BackColor = System.Drawing.Color.White;
            this.txtIMAGE.CustomBackColor = System.Drawing.Color.White;
            this.txtIMAGE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtIMAGE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtIMAGE.Location = new System.Drawing.Point(551, 206);
            this.txtIMAGE.Margin = new System.Windows.Forms.Padding(2);
            this.txtIMAGE.MaxLength = 32767;
            this.txtIMAGE.Multiline = false;
            this.txtIMAGE.Name = "txtIMAGE";
            this.txtIMAGE.ReadOnly = false;
            this.txtIMAGE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIMAGE.Size = new System.Drawing.Size(187, 27);
            this.txtIMAGE.TabIndex = 535;
            this.txtIMAGE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIMAGE.UseSystemPasswordChar = false;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(385, 214);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(46, 16);
            this.label17.TabIndex = 534;
            this.label17.Text = "Image";
            // 
            // txtLIBELLE_UNITE_DEVIS
            // 
            this.txtLIBELLE_UNITE_DEVIS.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_UNITE_DEVIS.AccAllowComma = false;
            this.txtLIBELLE_UNITE_DEVIS.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_DEVIS.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_UNITE_DEVIS.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_UNITE_DEVIS.AccHidenValue = "";
            this.txtLIBELLE_UNITE_DEVIS.AccNotAllowedChars = null;
            this.txtLIBELLE_UNITE_DEVIS.AccReadOnly = false;
            this.txtLIBELLE_UNITE_DEVIS.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_UNITE_DEVIS.AccRequired = false;
            this.txtLIBELLE_UNITE_DEVIS.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_DEVIS.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_DEVIS.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_UNITE_DEVIS.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_UNITE_DEVIS.Location = new System.Drawing.Point(551, 175);
            this.txtLIBELLE_UNITE_DEVIS.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_UNITE_DEVIS.MaxLength = 32767;
            this.txtLIBELLE_UNITE_DEVIS.Multiline = false;
            this.txtLIBELLE_UNITE_DEVIS.Name = "txtLIBELLE_UNITE_DEVIS";
            this.txtLIBELLE_UNITE_DEVIS.ReadOnly = false;
            this.txtLIBELLE_UNITE_DEVIS.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_UNITE_DEVIS.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_UNITE_DEVIS.TabIndex = 533;
            this.txtLIBELLE_UNITE_DEVIS.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_UNITE_DEVIS.UseSystemPasswordChar = false;
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(385, 185);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(75, 16);
            this.label14.TabIndex = 532;
            this.label14.Text = "Unité devis";
            // 
            // txtUNCS_SUR_UV
            // 
            this.txtUNCS_SUR_UV.AccAcceptNumbersOnly = false;
            this.txtUNCS_SUR_UV.AccAllowComma = false;
            this.txtUNCS_SUR_UV.AccBackgroundColor = System.Drawing.Color.White;
            this.txtUNCS_SUR_UV.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtUNCS_SUR_UV.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtUNCS_SUR_UV.AccHidenValue = "";
            this.txtUNCS_SUR_UV.AccNotAllowedChars = null;
            this.txtUNCS_SUR_UV.AccReadOnly = false;
            this.txtUNCS_SUR_UV.AccReadOnlyAllowDelete = false;
            this.txtUNCS_SUR_UV.AccRequired = false;
            this.txtUNCS_SUR_UV.BackColor = System.Drawing.Color.White;
            this.txtUNCS_SUR_UV.CustomBackColor = System.Drawing.Color.White;
            this.txtUNCS_SUR_UV.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtUNCS_SUR_UV.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtUNCS_SUR_UV.Location = new System.Drawing.Point(551, 144);
            this.txtUNCS_SUR_UV.Margin = new System.Windows.Forms.Padding(2);
            this.txtUNCS_SUR_UV.MaxLength = 32767;
            this.txtUNCS_SUR_UV.Multiline = false;
            this.txtUNCS_SUR_UV.Name = "txtUNCS_SUR_UV";
            this.txtUNCS_SUR_UV.ReadOnly = false;
            this.txtUNCS_SUR_UV.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtUNCS_SUR_UV.Size = new System.Drawing.Size(187, 27);
            this.txtUNCS_SUR_UV.TabIndex = 531;
            this.txtUNCS_SUR_UV.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtUNCS_SUR_UV.UseSystemPasswordChar = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(385, 152);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(75, 16);
            this.label15.TabIndex = 530;
            this.label15.Text = "UNSC / UV";
            // 
            // txtLIBELLE_UNITE_BASE
            // 
            this.txtLIBELLE_UNITE_BASE.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_UNITE_BASE.AccAllowComma = false;
            this.txtLIBELLE_UNITE_BASE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_BASE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_UNITE_BASE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_UNITE_BASE.AccHidenValue = "";
            this.txtLIBELLE_UNITE_BASE.AccNotAllowedChars = null;
            this.txtLIBELLE_UNITE_BASE.AccReadOnly = false;
            this.txtLIBELLE_UNITE_BASE.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_UNITE_BASE.AccRequired = false;
            this.txtLIBELLE_UNITE_BASE.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_BASE.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_BASE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_UNITE_BASE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_UNITE_BASE.Location = new System.Drawing.Point(551, 113);
            this.txtLIBELLE_UNITE_BASE.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_UNITE_BASE.MaxLength = 32767;
            this.txtLIBELLE_UNITE_BASE.Multiline = false;
            this.txtLIBELLE_UNITE_BASE.Name = "txtLIBELLE_UNITE_BASE";
            this.txtLIBELLE_UNITE_BASE.ReadOnly = false;
            this.txtLIBELLE_UNITE_BASE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_UNITE_BASE.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_UNITE_BASE.TabIndex = 529;
            this.txtLIBELLE_UNITE_BASE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_UNITE_BASE.UseSystemPasswordChar = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(385, 120);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(92, 16);
            this.label12.TabIndex = 528;
            this.label12.Text = "Unité de base";
            // 
            // txtLIBELLE_UNITE_CONDITIONNEMENT
            // 
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.AccAllowComma = false;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.AccHidenValue = "";
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.AccNotAllowedChars = null;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.AccReadOnly = false;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.AccRequired = false;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.Location = new System.Drawing.Point(551, 82);
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.MaxLength = 32767;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.Multiline = false;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.Name = "txtLIBELLE_UNITE_CONDITIONNEMENT";
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.ReadOnly = false;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.TabIndex = 527;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_UNITE_CONDITIONNEMENT.UseSystemPasswordChar = false;
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(385, 86);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(109, 16);
            this.label13.TabIndex = 526;
            this.label13.Text = "Libelle unt. Cond.";
            // 
            // txtCODE_UNITE_CONDITIONNEMENT
            // 
            this.txtCODE_UNITE_CONDITIONNEMENT.AccAcceptNumbersOnly = false;
            this.txtCODE_UNITE_CONDITIONNEMENT.AccAllowComma = false;
            this.txtCODE_UNITE_CONDITIONNEMENT.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_CONDITIONNEMENT.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCODE_UNITE_CONDITIONNEMENT.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCODE_UNITE_CONDITIONNEMENT.AccHidenValue = "";
            this.txtCODE_UNITE_CONDITIONNEMENT.AccNotAllowedChars = null;
            this.txtCODE_UNITE_CONDITIONNEMENT.AccReadOnly = false;
            this.txtCODE_UNITE_CONDITIONNEMENT.AccReadOnlyAllowDelete = false;
            this.txtCODE_UNITE_CONDITIONNEMENT.AccRequired = false;
            this.txtCODE_UNITE_CONDITIONNEMENT.BackColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_CONDITIONNEMENT.CustomBackColor = System.Drawing.Color.White;
            this.txtCODE_UNITE_CONDITIONNEMENT.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCODE_UNITE_CONDITIONNEMENT.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtCODE_UNITE_CONDITIONNEMENT.Location = new System.Drawing.Point(551, 51);
            this.txtCODE_UNITE_CONDITIONNEMENT.Margin = new System.Windows.Forms.Padding(2);
            this.txtCODE_UNITE_CONDITIONNEMENT.MaxLength = 32767;
            this.txtCODE_UNITE_CONDITIONNEMENT.Multiline = false;
            this.txtCODE_UNITE_CONDITIONNEMENT.Name = "txtCODE_UNITE_CONDITIONNEMENT";
            this.txtCODE_UNITE_CONDITIONNEMENT.ReadOnly = false;
            this.txtCODE_UNITE_CONDITIONNEMENT.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCODE_UNITE_CONDITIONNEMENT.Size = new System.Drawing.Size(187, 27);
            this.txtCODE_UNITE_CONDITIONNEMENT.TabIndex = 525;
            this.txtCODE_UNITE_CONDITIONNEMENT.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCODE_UNITE_CONDITIONNEMENT.UseSystemPasswordChar = false;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(385, 55);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(102, 16);
            this.label10.TabIndex = 524;
            this.label10.Text = "Code unt. Cond.";
            // 
            // txtUNITE_CONDITIONNEMENT
            // 
            this.txtUNITE_CONDITIONNEMENT.AccAcceptNumbersOnly = false;
            this.txtUNITE_CONDITIONNEMENT.AccAllowComma = false;
            this.txtUNITE_CONDITIONNEMENT.AccBackgroundColor = System.Drawing.Color.White;
            this.txtUNITE_CONDITIONNEMENT.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtUNITE_CONDITIONNEMENT.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtUNITE_CONDITIONNEMENT.AccHidenValue = "";
            this.txtUNITE_CONDITIONNEMENT.AccNotAllowedChars = null;
            this.txtUNITE_CONDITIONNEMENT.AccReadOnly = false;
            this.txtUNITE_CONDITIONNEMENT.AccReadOnlyAllowDelete = false;
            this.txtUNITE_CONDITIONNEMENT.AccRequired = false;
            this.txtUNITE_CONDITIONNEMENT.BackColor = System.Drawing.Color.White;
            this.txtUNITE_CONDITIONNEMENT.CustomBackColor = System.Drawing.Color.White;
            this.txtUNITE_CONDITIONNEMENT.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtUNITE_CONDITIONNEMENT.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtUNITE_CONDITIONNEMENT.Location = new System.Drawing.Point(551, 20);
            this.txtUNITE_CONDITIONNEMENT.Margin = new System.Windows.Forms.Padding(2);
            this.txtUNITE_CONDITIONNEMENT.MaxLength = 32767;
            this.txtUNITE_CONDITIONNEMENT.Multiline = false;
            this.txtUNITE_CONDITIONNEMENT.Name = "txtUNITE_CONDITIONNEMENT";
            this.txtUNITE_CONDITIONNEMENT.ReadOnly = false;
            this.txtUNITE_CONDITIONNEMENT.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtUNITE_CONDITIONNEMENT.Size = new System.Drawing.Size(187, 27);
            this.txtUNITE_CONDITIONNEMENT.TabIndex = 523;
            this.txtUNITE_CONDITIONNEMENT.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtUNITE_CONDITIONNEMENT.UseSystemPasswordChar = false;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(385, 21);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(64, 16);
            this.label11.TabIndex = 522;
            this.label11.Text = "Unt. cond";
            // 
            // txtsTatut
            // 
            this.txtsTatut.AccAcceptNumbersOnly = false;
            this.txtsTatut.AccAllowComma = false;
            this.txtsTatut.AccBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtsTatut.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtsTatut.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtsTatut.AccHidenValue = "";
            this.txtsTatut.AccNotAllowedChars = null;
            this.txtsTatut.AccReadOnly = false;
            this.txtsTatut.AccReadOnlyAllowDelete = false;
            this.txtsTatut.AccRequired = false;
            this.txtsTatut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtsTatut.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtsTatut.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtsTatut.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtsTatut.Location = new System.Drawing.Point(171, 519);
            this.txtsTatut.Margin = new System.Windows.Forms.Padding(2);
            this.txtsTatut.MaxLength = 32767;
            this.txtsTatut.Multiline = false;
            this.txtsTatut.Name = "txtsTatut";
            this.txtsTatut.ReadOnly = false;
            this.txtsTatut.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtsTatut.Size = new System.Drawing.Size(187, 27);
            this.txtsTatut.TabIndex = 521;
            this.txtsTatut.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtsTatut.UseSystemPasswordChar = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(11, 530);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(41, 16);
            this.label9.TabIndex = 520;
            this.label9.Text = "Statut";
            // 
            // txtDATE_FIN_ACTIVITE
            // 
            this.txtDATE_FIN_ACTIVITE.AccAcceptNumbersOnly = false;
            this.txtDATE_FIN_ACTIVITE.AccAllowComma = false;
            this.txtDATE_FIN_ACTIVITE.AccBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDATE_FIN_ACTIVITE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDATE_FIN_ACTIVITE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtDATE_FIN_ACTIVITE.AccHidenValue = "";
            this.txtDATE_FIN_ACTIVITE.AccNotAllowedChars = null;
            this.txtDATE_FIN_ACTIVITE.AccReadOnly = false;
            this.txtDATE_FIN_ACTIVITE.AccReadOnlyAllowDelete = false;
            this.txtDATE_FIN_ACTIVITE.AccRequired = false;
            this.txtDATE_FIN_ACTIVITE.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDATE_FIN_ACTIVITE.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDATE_FIN_ACTIVITE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDATE_FIN_ACTIVITE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtDATE_FIN_ACTIVITE.Location = new System.Drawing.Point(171, 483);
            this.txtDATE_FIN_ACTIVITE.Margin = new System.Windows.Forms.Padding(2);
            this.txtDATE_FIN_ACTIVITE.MaxLength = 32767;
            this.txtDATE_FIN_ACTIVITE.Multiline = false;
            this.txtDATE_FIN_ACTIVITE.Name = "txtDATE_FIN_ACTIVITE";
            this.txtDATE_FIN_ACTIVITE.ReadOnly = false;
            this.txtDATE_FIN_ACTIVITE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDATE_FIN_ACTIVITE.Size = new System.Drawing.Size(187, 27);
            this.txtDATE_FIN_ACTIVITE.TabIndex = 519;
            this.txtDATE_FIN_ACTIVITE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDATE_FIN_ACTIVITE.UseSystemPasswordChar = false;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(10, 488);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(128, 16);
            this.label8.TabIndex = 518;
            this.label8.Text = "Date de fin d\'activité";
            // 
            // txtCOMPOSANT
            // 
            this.txtCOMPOSANT.AccAcceptNumbersOnly = false;
            this.txtCOMPOSANT.AccAllowComma = false;
            this.txtCOMPOSANT.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCOMPOSANT.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCOMPOSANT.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCOMPOSANT.AccHidenValue = "";
            this.txtCOMPOSANT.AccNotAllowedChars = null;
            this.txtCOMPOSANT.AccReadOnly = false;
            this.txtCOMPOSANT.AccReadOnlyAllowDelete = false;
            this.txtCOMPOSANT.AccRequired = false;
            this.txtCOMPOSANT.BackColor = System.Drawing.Color.White;
            this.txtCOMPOSANT.CustomBackColor = System.Drawing.Color.White;
            this.txtCOMPOSANT.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCOMPOSANT.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtCOMPOSANT.Location = new System.Drawing.Point(172, 298);
            this.txtCOMPOSANT.Margin = new System.Windows.Forms.Padding(2);
            this.txtCOMPOSANT.MaxLength = 32767;
            this.txtCOMPOSANT.Multiline = false;
            this.txtCOMPOSANT.Name = "txtCOMPOSANT";
            this.txtCOMPOSANT.ReadOnly = false;
            this.txtCOMPOSANT.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCOMPOSANT.Size = new System.Drawing.Size(187, 27);
            this.txtCOMPOSANT.TabIndex = 517;
            this.txtCOMPOSANT.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCOMPOSANT.UseSystemPasswordChar = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(11, 293);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(77, 16);
            this.label7.TabIndex = 516;
            this.label7.Text = "Composant";
            // 
            // txtTYPE
            // 
            this.txtTYPE.AccAcceptNumbersOnly = false;
            this.txtTYPE.AccAllowComma = false;
            this.txtTYPE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTYPE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTYPE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtTYPE.AccHidenValue = "";
            this.txtTYPE.AccNotAllowedChars = null;
            this.txtTYPE.AccReadOnly = false;
            this.txtTYPE.AccReadOnlyAllowDelete = false;
            this.txtTYPE.AccRequired = false;
            this.txtTYPE.BackColor = System.Drawing.Color.White;
            this.txtTYPE.CustomBackColor = System.Drawing.Color.White;
            this.txtTYPE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtTYPE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtTYPE.Location = new System.Drawing.Point(172, 267);
            this.txtTYPE.Margin = new System.Windows.Forms.Padding(2);
            this.txtTYPE.MaxLength = 32767;
            this.txtTYPE.Multiline = false;
            this.txtTYPE.Name = "txtTYPE";
            this.txtTYPE.ReadOnly = false;
            this.txtTYPE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTYPE.Size = new System.Drawing.Size(187, 27);
            this.txtTYPE.TabIndex = 515;
            this.txtTYPE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTYPE.UseSystemPasswordChar = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(11, 261);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(40, 16);
            this.label6.TabIndex = 514;
            this.label6.Text = "Type";
            // 
            // txtLIBELLE_SOUS_FAMILLE
            // 
            this.txtLIBELLE_SOUS_FAMILLE.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_SOUS_FAMILLE.AccAllowComma = false;
            this.txtLIBELLE_SOUS_FAMILLE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_SOUS_FAMILLE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_SOUS_FAMILLE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_SOUS_FAMILLE.AccHidenValue = "";
            this.txtLIBELLE_SOUS_FAMILLE.AccNotAllowedChars = null;
            this.txtLIBELLE_SOUS_FAMILLE.AccReadOnly = false;
            this.txtLIBELLE_SOUS_FAMILLE.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_SOUS_FAMILLE.AccRequired = false;
            this.txtLIBELLE_SOUS_FAMILLE.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_SOUS_FAMILLE.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_SOUS_FAMILLE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_SOUS_FAMILLE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_SOUS_FAMILLE.Location = new System.Drawing.Point(172, 233);
            this.txtLIBELLE_SOUS_FAMILLE.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_SOUS_FAMILLE.MaxLength = 32767;
            this.txtLIBELLE_SOUS_FAMILLE.Multiline = false;
            this.txtLIBELLE_SOUS_FAMILLE.Name = "txtLIBELLE_SOUS_FAMILLE";
            this.txtLIBELLE_SOUS_FAMILLE.ReadOnly = false;
            this.txtLIBELLE_SOUS_FAMILLE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_SOUS_FAMILLE.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_SOUS_FAMILLE.TabIndex = 513;
            this.txtLIBELLE_SOUS_FAMILLE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_SOUS_FAMILLE.UseSystemPasswordChar = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(11, 234);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(81, 16);
            this.label5.TabIndex = 512;
            this.label5.Text = "Sous famille";
            // 
            // txtLIBELLE_FAMILLE
            // 
            this.txtLIBELLE_FAMILLE.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_FAMILLE.AccAllowComma = false;
            this.txtLIBELLE_FAMILLE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_FAMILLE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_FAMILLE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_FAMILLE.AccHidenValue = "";
            this.txtLIBELLE_FAMILLE.AccNotAllowedChars = null;
            this.txtLIBELLE_FAMILLE.AccReadOnly = false;
            this.txtLIBELLE_FAMILLE.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_FAMILLE.AccRequired = false;
            this.txtLIBELLE_FAMILLE.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_FAMILLE.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_FAMILLE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_FAMILLE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_FAMILLE.Location = new System.Drawing.Point(172, 203);
            this.txtLIBELLE_FAMILLE.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_FAMILLE.MaxLength = 32767;
            this.txtLIBELLE_FAMILLE.Multiline = false;
            this.txtLIBELLE_FAMILLE.Name = "txtLIBELLE_FAMILLE";
            this.txtLIBELLE_FAMILLE.ReadOnly = false;
            this.txtLIBELLE_FAMILLE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_FAMILLE.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_FAMILLE.TabIndex = 511;
            this.txtLIBELLE_FAMILLE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_FAMILLE.UseSystemPasswordChar = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(11, 203);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 16);
            this.label4.TabIndex = 510;
            this.label4.Text = "Libelle famille";
            // 
            // txtLIBELLE_TECHNIQUE
            // 
            this.txtLIBELLE_TECHNIQUE.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_TECHNIQUE.AccAllowComma = false;
            this.txtLIBELLE_TECHNIQUE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_TECHNIQUE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_TECHNIQUE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_TECHNIQUE.AccHidenValue = "";
            this.txtLIBELLE_TECHNIQUE.AccNotAllowedChars = null;
            this.txtLIBELLE_TECHNIQUE.AccReadOnly = false;
            this.txtLIBELLE_TECHNIQUE.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_TECHNIQUE.AccRequired = false;
            this.txtLIBELLE_TECHNIQUE.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_TECHNIQUE.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_TECHNIQUE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_TECHNIQUE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_TECHNIQUE.Location = new System.Drawing.Point(172, 122);
            this.txtLIBELLE_TECHNIQUE.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_TECHNIQUE.MaxLength = 32767;
            this.txtLIBELLE_TECHNIQUE.Multiline = true;
            this.txtLIBELLE_TECHNIQUE.Name = "txtLIBELLE_TECHNIQUE";
            this.txtLIBELLE_TECHNIQUE.ReadOnly = false;
            this.txtLIBELLE_TECHNIQUE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_TECHNIQUE.Size = new System.Drawing.Size(187, 77);
            this.txtLIBELLE_TECHNIQUE.TabIndex = 509;
            this.txtLIBELLE_TECHNIQUE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_TECHNIQUE.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(11, 119);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(109, 16);
            this.label3.TabIndex = 508;
            this.label3.Text = "Libellé technique";
            // 
            // txtDESIGNATION_ARTICLE
            // 
            this.txtDESIGNATION_ARTICLE.AccAcceptNumbersOnly = false;
            this.txtDESIGNATION_ARTICLE.AccAllowComma = false;
            this.txtDESIGNATION_ARTICLE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDESIGNATION_ARTICLE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDESIGNATION_ARTICLE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtDESIGNATION_ARTICLE.AccHidenValue = "";
            this.txtDESIGNATION_ARTICLE.AccNotAllowedChars = null;
            this.txtDESIGNATION_ARTICLE.AccReadOnly = false;
            this.txtDESIGNATION_ARTICLE.AccReadOnlyAllowDelete = false;
            this.txtDESIGNATION_ARTICLE.AccRequired = false;
            this.txtDESIGNATION_ARTICLE.BackColor = System.Drawing.Color.White;
            this.txtDESIGNATION_ARTICLE.CustomBackColor = System.Drawing.Color.White;
            this.txtDESIGNATION_ARTICLE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDESIGNATION_ARTICLE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtDESIGNATION_ARTICLE.Location = new System.Drawing.Point(171, 41);
            this.txtDESIGNATION_ARTICLE.Margin = new System.Windows.Forms.Padding(2);
            this.txtDESIGNATION_ARTICLE.MaxLength = 32767;
            this.txtDESIGNATION_ARTICLE.Multiline = true;
            this.txtDESIGNATION_ARTICLE.Name = "txtDESIGNATION_ARTICLE";
            this.txtDESIGNATION_ARTICLE.ReadOnly = false;
            this.txtDESIGNATION_ARTICLE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDESIGNATION_ARTICLE.Size = new System.Drawing.Size(187, 77);
            this.txtDESIGNATION_ARTICLE.TabIndex = 507;
            this.txtDESIGNATION_ARTICLE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDESIGNATION_ARTICLE.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(10, 41);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(80, 16);
            this.label2.TabIndex = 506;
            this.label2.Text = "Désignation";
            // 
            // txtFAMILLE_ATOLL
            // 
            this.txtFAMILLE_ATOLL.AccAcceptNumbersOnly = false;
            this.txtFAMILLE_ATOLL.AccAllowComma = false;
            this.txtFAMILLE_ATOLL.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFAMILLE_ATOLL.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFAMILLE_ATOLL.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtFAMILLE_ATOLL.AccHidenValue = "";
            this.txtFAMILLE_ATOLL.AccNotAllowedChars = null;
            this.txtFAMILLE_ATOLL.AccReadOnly = false;
            this.txtFAMILLE_ATOLL.AccReadOnlyAllowDelete = false;
            this.txtFAMILLE_ATOLL.AccRequired = false;
            this.txtFAMILLE_ATOLL.BackColor = System.Drawing.Color.White;
            this.txtFAMILLE_ATOLL.CustomBackColor = System.Drawing.Color.White;
            this.txtFAMILLE_ATOLL.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtFAMILLE_ATOLL.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtFAMILLE_ATOLL.Location = new System.Drawing.Point(172, 328);
            this.txtFAMILLE_ATOLL.Margin = new System.Windows.Forms.Padding(2);
            this.txtFAMILLE_ATOLL.MaxLength = 32767;
            this.txtFAMILLE_ATOLL.Multiline = false;
            this.txtFAMILLE_ATOLL.Name = "txtFAMILLE_ATOLL";
            this.txtFAMILLE_ATOLL.ReadOnly = false;
            this.txtFAMILLE_ATOLL.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFAMILLE_ATOLL.Size = new System.Drawing.Size(187, 27);
            this.txtFAMILLE_ATOLL.TabIndex = 505;
            this.txtFAMILLE_ATOLL.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFAMILLE_ATOLL.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(11, 383);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(86, 16);
            this.label1.TabIndex = 504;
            this.label1.Text = "Libelle Devis";
            // 
            // txtCHRONO_INITIAL
            // 
            this.txtCHRONO_INITIAL.AccAcceptNumbersOnly = false;
            this.txtCHRONO_INITIAL.AccAllowComma = false;
            this.txtCHRONO_INITIAL.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCHRONO_INITIAL.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCHRONO_INITIAL.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCHRONO_INITIAL.AccHidenValue = "";
            this.txtCHRONO_INITIAL.AccNotAllowedChars = null;
            this.txtCHRONO_INITIAL.AccReadOnly = false;
            this.txtCHRONO_INITIAL.AccReadOnlyAllowDelete = false;
            this.txtCHRONO_INITIAL.AccRequired = false;
            this.txtCHRONO_INITIAL.BackColor = System.Drawing.Color.White;
            this.txtCHRONO_INITIAL.CustomBackColor = System.Drawing.Color.White;
            this.txtCHRONO_INITIAL.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCHRONO_INITIAL.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtCHRONO_INITIAL.Location = new System.Drawing.Point(171, 10);
            this.txtCHRONO_INITIAL.Margin = new System.Windows.Forms.Padding(2);
            this.txtCHRONO_INITIAL.MaxLength = 32767;
            this.txtCHRONO_INITIAL.Multiline = false;
            this.txtCHRONO_INITIAL.Name = "txtCHRONO_INITIAL";
            this.txtCHRONO_INITIAL.ReadOnly = false;
            this.txtCHRONO_INITIAL.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCHRONO_INITIAL.Size = new System.Drawing.Size(187, 27);
            this.txtCHRONO_INITIAL.TabIndex = 503;
            this.txtCHRONO_INITIAL.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCHRONO_INITIAL.UseSystemPasswordChar = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(10, 16);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(49, 16);
            this.label33.TabIndex = 502;
            this.label33.Text = "chrono";
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.groupBox1);
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(1210, 631);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.txtLIBELLE_UNITE_PRIX_FABRICANT);
            this.groupBox1.Controls.Add(this.txtPRIX_FABRICANT);
            this.groupBox1.Controls.Add(this.txtQTE_UNITE_VALEUR);
            this.groupBox1.Controls.Add(this.txtLIBELLE_UNITE_VALEUR);
            this.groupBox1.Controls.Add(this.txtDATE_CREATION);
            this.groupBox1.Controls.Add(this.txtPRIX_DONNE_PAR);
            this.groupBox1.Controls.Add(this.TXTLIBELLE_UNITE_PRIX_DEVIS);
            this.groupBox1.Controls.Add(this.txtPRX_DEVIS_NET_UD);
            this.groupBox1.Controls.Add(this.label119);
            this.groupBox1.Controls.Add(this.label118);
            this.groupBox1.Controls.Add(this.label117);
            this.groupBox1.Controls.Add(this.label116);
            this.groupBox1.Controls.Add(this.label115);
            this.groupBox1.Controls.Add(this.label114);
            this.groupBox1.Controls.Add(this.label113);
            this.groupBox1.Controls.Add(this.label112);
            this.groupBox1.Controls.Add(this.label111);
            this.groupBox1.Controls.Add(this.label110);
            this.groupBox1.Controls.Add(this.label109);
            this.groupBox1.Controls.Add(this.label108);
            this.groupBox1.Controls.Add(this.label107);
            this.groupBox1.Controls.Add(this.label106);
            this.groupBox1.Controls.Add(this.label105);
            this.groupBox1.Controls.Add(this.label104);
            this.groupBox1.Controls.Add(this.label103);
            this.groupBox1.Controls.Add(this.txtLIBELLE_UNITE_PRIX_GECET_UNITE);
            this.groupBox1.Controls.Add(this.txtPRIX_GECET_UNITE);
            this.groupBox1.Controls.Add(this.txtPRIX_GECET);
            this.groupBox1.Controls.Add(this.txtLIBELLE_UNITE_PRIX_GECET);
            this.groupBox1.Controls.Add(this.label102);
            this.groupBox1.Controls.Add(this.label101);
            this.groupBox1.Controls.Add(this.label100);
            this.groupBox1.Controls.Add(this.txtPRIX_PUBLIC_DETAILS);
            this.groupBox1.Controls.Add(this.txtPRIX_NET_DETAILS);
            this.groupBox1.Controls.Add(this.txtREF_HISTORIQUE);
            this.groupBox1.Controls.Add(this.label99);
            this.groupBox1.Controls.Add(this.label98);
            this.groupBox1.Controls.Add(this.label97);
            this.groupBox1.Controls.Add(this.label96);
            this.groupBox1.Controls.Add(this.label95);
            this.groupBox1.Controls.Add(this.label94);
            this.groupBox1.Controls.Add(this.txtUS_SUR_UV);
            this.groupBox1.Controls.Add(this.txtQTE_MIN_CMD_UC);
            this.groupBox1.Controls.Add(this.txtQTE_UNITE_FOURNISSEUR);
            this.groupBox1.Controls.Add(this.txtLIBELLE_UNITE_FOURNISSEUR);
            this.groupBox1.Controls.Add(this.txtNB_UC_DANS_UV);
            this.groupBox1.Controls.Add(this.txtQTE_MIN_CMD_UCNF);
            this.groupBox1.Controls.Add(this.txtUS_SUR_UC);
            this.groupBox1.Controls.Add(this.TXTUD_SUR_US);
            this.groupBox1.Controls.Add(this.txtUNCF_SUR_UC);
            this.groupBox1.Controls.Add(this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE);
            this.groupBox1.Controls.Add(this.txtPRIX_DEVIS_MAJORE);
            this.groupBox1.Controls.Add(this.txtTAXE);
            this.groupBox1.Controls.Add(this.label56);
            this.groupBox1.Controls.Add(this.txtLIBELLE_COND_PRIX_COND_STOCK);
            this.groupBox1.Controls.Add(this.label55);
            this.groupBox1.Controls.Add(this.txtPRIX_COND_STOCK);
            this.groupBox1.Controls.Add(this.label48);
            this.groupBox1.Controls.Add(this.txtLIBELLE_UNITE_PRIX_STOCK);
            this.groupBox1.Controls.Add(this.label49);
            this.groupBox1.Controls.Add(this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR);
            this.groupBox1.Controls.Add(this.label50);
            this.groupBox1.Controls.Add(this.txtPRIX_STOCK);
            this.groupBox1.Controls.Add(this.label51);
            this.groupBox1.Controls.Add(this.txtPRIX_COND_FOUR_2);
            this.groupBox1.Controls.Add(this.label52);
            this.groupBox1.Controls.Add(this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR);
            this.groupBox1.Controls.Add(this.label53);
            this.groupBox1.Controls.Add(this.txtPRIX_DISTRIBUTEUR);
            this.groupBox1.Controls.Add(this.label54);
            this.groupBox1.Controls.Add(this.txtQTE_COND_FOURNISSEUR);
            this.groupBox1.Controls.Add(this.label38);
            this.groupBox1.Controls.Add(this.txtLIBELLE_COND_FOURNISSEUR);
            this.groupBox1.Controls.Add(this.label39);
            this.groupBox1.Controls.Add(this.txtQTE_UNITE_METIER);
            this.groupBox1.Controls.Add(this.label40);
            this.groupBox1.Controls.Add(this.txtLIBELLE_UNITE_METIER);
            this.groupBox1.Controls.Add(this.label41);
            this.groupBox1.Controls.Add(this.txtQTE_UNITE_DEVIS);
            this.groupBox1.Controls.Add(this.label42);
            this.groupBox1.Controls.Add(this.txtLIBELLE_UNITE_DEVIS_2);
            this.groupBox1.Controls.Add(this.label43);
            this.groupBox1.Controls.Add(this.txtQTE_UNITE_STOCK);
            this.groupBox1.Controls.Add(this.label44);
            this.groupBox1.Controls.Add(this.txtLIBELLE_UNITE_STOCK);
            this.groupBox1.Controls.Add(this.label45);
            this.groupBox1.Controls.Add(this.txtQTE_COND_STOCK);
            this.groupBox1.Controls.Add(this.label46);
            this.groupBox1.Controls.Add(this.txtLIBELLE_COND_STOCK);
            this.groupBox1.Controls.Add(this.label47);
            this.groupBox1.Controls.Add(this.txtREF_FOURNISSEUR);
            this.groupBox1.Controls.Add(this.label27);
            this.groupBox1.Controls.Add(this.txtREF_FABRICANT);
            this.groupBox1.Controls.Add(this.label28);
            this.groupBox1.Controls.Add(this.txtLIBELLE_MARQUE);
            this.groupBox1.Controls.Add(this.label29);
            this.groupBox1.Controls.Add(this.txtDATE_APPLICATION);
            this.groupBox1.Controls.Add(this.label30);
            this.groupBox1.Controls.Add(this.txtLIBELLE_TECHNIQUE_2);
            this.groupBox1.Controls.Add(this.label31);
            this.groupBox1.Controls.Add(this.txtDESIGNATION_ARTICLE_2);
            this.groupBox1.Controls.Add(this.label32);
            this.groupBox1.Controls.Add(this.txtCHRONO_INITIAL_2);
            this.groupBox1.Controls.Add(this.label34);
            this.groupBox1.Controls.Add(this.txtNOM_FOURNISSEUR);
            this.groupBox1.Controls.Add(this.label35);
            this.groupBox1.Controls.Add(this.txtCODE_FOURNISSEUR_ADHERENT);
            this.groupBox1.Controls.Add(this.label36);
            this.groupBox1.Controls.Add(this.txtIDGEC_LIGNE_PRIX);
            this.groupBox1.Controls.Add(this.label37);
            this.groupBox1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox1.Location = new System.Drawing.Point(3, 3);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(1198, 605);
            this.groupBox1.TabIndex = 415;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "TARIF";
            this.groupBox1.Visible = false;
            // 
            // txtLIBELLE_UNITE_PRIX_FABRICANT
            // 
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.AccAllowComma = false;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.AccHidenValue = "";
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.AccNotAllowedChars = null;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.AccReadOnly = false;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.AccRequired = false;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.Location = new System.Drawing.Point(1002, 506);
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.MaxLength = 32767;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.Multiline = false;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.Name = "txtLIBELLE_UNITE_PRIX_FABRICANT";
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.ReadOnly = false;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.TabIndex = 647;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.UseSystemPasswordChar = false;
            this.txtLIBELLE_UNITE_PRIX_FABRICANT.Visible = false;
            // 
            // txtPRIX_FABRICANT
            // 
            this.txtPRIX_FABRICANT.AccAcceptNumbersOnly = false;
            this.txtPRIX_FABRICANT.AccAllowComma = false;
            this.txtPRIX_FABRICANT.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRIX_FABRICANT.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRIX_FABRICANT.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPRIX_FABRICANT.AccHidenValue = "";
            this.txtPRIX_FABRICANT.AccNotAllowedChars = null;
            this.txtPRIX_FABRICANT.AccReadOnly = false;
            this.txtPRIX_FABRICANT.AccReadOnlyAllowDelete = false;
            this.txtPRIX_FABRICANT.AccRequired = false;
            this.txtPRIX_FABRICANT.BackColor = System.Drawing.Color.White;
            this.txtPRIX_FABRICANT.CustomBackColor = System.Drawing.Color.White;
            this.txtPRIX_FABRICANT.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPRIX_FABRICANT.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtPRIX_FABRICANT.Location = new System.Drawing.Point(1002, 475);
            this.txtPRIX_FABRICANT.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRIX_FABRICANT.MaxLength = 32767;
            this.txtPRIX_FABRICANT.Multiline = false;
            this.txtPRIX_FABRICANT.Name = "txtPRIX_FABRICANT";
            this.txtPRIX_FABRICANT.ReadOnly = false;
            this.txtPRIX_FABRICANT.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRIX_FABRICANT.Size = new System.Drawing.Size(187, 27);
            this.txtPRIX_FABRICANT.TabIndex = 646;
            this.txtPRIX_FABRICANT.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRIX_FABRICANT.UseSystemPasswordChar = false;
            this.txtPRIX_FABRICANT.Visible = false;
            // 
            // txtQTE_UNITE_VALEUR
            // 
            this.txtQTE_UNITE_VALEUR.AccAcceptNumbersOnly = false;
            this.txtQTE_UNITE_VALEUR.AccAllowComma = false;
            this.txtQTE_UNITE_VALEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_VALEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtQTE_UNITE_VALEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtQTE_UNITE_VALEUR.AccHidenValue = "";
            this.txtQTE_UNITE_VALEUR.AccNotAllowedChars = null;
            this.txtQTE_UNITE_VALEUR.AccReadOnly = false;
            this.txtQTE_UNITE_VALEUR.AccReadOnlyAllowDelete = false;
            this.txtQTE_UNITE_VALEUR.AccRequired = false;
            this.txtQTE_UNITE_VALEUR.BackColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_VALEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_VALEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtQTE_UNITE_VALEUR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtQTE_UNITE_VALEUR.Location = new System.Drawing.Point(1002, 444);
            this.txtQTE_UNITE_VALEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtQTE_UNITE_VALEUR.MaxLength = 32767;
            this.txtQTE_UNITE_VALEUR.Multiline = false;
            this.txtQTE_UNITE_VALEUR.Name = "txtQTE_UNITE_VALEUR";
            this.txtQTE_UNITE_VALEUR.ReadOnly = false;
            this.txtQTE_UNITE_VALEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtQTE_UNITE_VALEUR.Size = new System.Drawing.Size(187, 27);
            this.txtQTE_UNITE_VALEUR.TabIndex = 645;
            this.txtQTE_UNITE_VALEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtQTE_UNITE_VALEUR.UseSystemPasswordChar = false;
            this.txtQTE_UNITE_VALEUR.Visible = false;
            // 
            // txtLIBELLE_UNITE_VALEUR
            // 
            this.txtLIBELLE_UNITE_VALEUR.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_UNITE_VALEUR.AccAllowComma = false;
            this.txtLIBELLE_UNITE_VALEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_VALEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_UNITE_VALEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_UNITE_VALEUR.AccHidenValue = "";
            this.txtLIBELLE_UNITE_VALEUR.AccNotAllowedChars = null;
            this.txtLIBELLE_UNITE_VALEUR.AccReadOnly = false;
            this.txtLIBELLE_UNITE_VALEUR.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_UNITE_VALEUR.AccRequired = false;
            this.txtLIBELLE_UNITE_VALEUR.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_VALEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_VALEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_UNITE_VALEUR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_UNITE_VALEUR.Location = new System.Drawing.Point(1002, 413);
            this.txtLIBELLE_UNITE_VALEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_UNITE_VALEUR.MaxLength = 32767;
            this.txtLIBELLE_UNITE_VALEUR.Multiline = false;
            this.txtLIBELLE_UNITE_VALEUR.Name = "txtLIBELLE_UNITE_VALEUR";
            this.txtLIBELLE_UNITE_VALEUR.ReadOnly = false;
            this.txtLIBELLE_UNITE_VALEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_UNITE_VALEUR.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_UNITE_VALEUR.TabIndex = 644;
            this.txtLIBELLE_UNITE_VALEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_UNITE_VALEUR.UseSystemPasswordChar = false;
            this.txtLIBELLE_UNITE_VALEUR.Visible = false;
            // 
            // txtDATE_CREATION
            // 
            this.txtDATE_CREATION.AccAcceptNumbersOnly = false;
            this.txtDATE_CREATION.AccAllowComma = false;
            this.txtDATE_CREATION.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDATE_CREATION.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDATE_CREATION.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtDATE_CREATION.AccHidenValue = "";
            this.txtDATE_CREATION.AccNotAllowedChars = null;
            this.txtDATE_CREATION.AccReadOnly = false;
            this.txtDATE_CREATION.AccReadOnlyAllowDelete = false;
            this.txtDATE_CREATION.AccRequired = false;
            this.txtDATE_CREATION.BackColor = System.Drawing.Color.White;
            this.txtDATE_CREATION.CustomBackColor = System.Drawing.Color.White;
            this.txtDATE_CREATION.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDATE_CREATION.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtDATE_CREATION.Location = new System.Drawing.Point(1002, 297);
            this.txtDATE_CREATION.Margin = new System.Windows.Forms.Padding(2);
            this.txtDATE_CREATION.MaxLength = 32767;
            this.txtDATE_CREATION.Multiline = false;
            this.txtDATE_CREATION.Name = "txtDATE_CREATION";
            this.txtDATE_CREATION.ReadOnly = false;
            this.txtDATE_CREATION.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDATE_CREATION.Size = new System.Drawing.Size(187, 27);
            this.txtDATE_CREATION.TabIndex = 643;
            this.txtDATE_CREATION.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDATE_CREATION.UseSystemPasswordChar = false;
            // 
            // txtPRIX_DONNE_PAR
            // 
            this.txtPRIX_DONNE_PAR.AccAcceptNumbersOnly = false;
            this.txtPRIX_DONNE_PAR.AccAllowComma = false;
            this.txtPRIX_DONNE_PAR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRIX_DONNE_PAR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRIX_DONNE_PAR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPRIX_DONNE_PAR.AccHidenValue = "";
            this.txtPRIX_DONNE_PAR.AccNotAllowedChars = null;
            this.txtPRIX_DONNE_PAR.AccReadOnly = false;
            this.txtPRIX_DONNE_PAR.AccReadOnlyAllowDelete = false;
            this.txtPRIX_DONNE_PAR.AccRequired = false;
            this.txtPRIX_DONNE_PAR.BackColor = System.Drawing.Color.White;
            this.txtPRIX_DONNE_PAR.CustomBackColor = System.Drawing.Color.White;
            this.txtPRIX_DONNE_PAR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPRIX_DONNE_PAR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtPRIX_DONNE_PAR.Location = new System.Drawing.Point(1002, 377);
            this.txtPRIX_DONNE_PAR.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRIX_DONNE_PAR.MaxLength = 32767;
            this.txtPRIX_DONNE_PAR.Multiline = false;
            this.txtPRIX_DONNE_PAR.Name = "txtPRIX_DONNE_PAR";
            this.txtPRIX_DONNE_PAR.ReadOnly = false;
            this.txtPRIX_DONNE_PAR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRIX_DONNE_PAR.Size = new System.Drawing.Size(187, 27);
            this.txtPRIX_DONNE_PAR.TabIndex = 642;
            this.txtPRIX_DONNE_PAR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRIX_DONNE_PAR.UseSystemPasswordChar = false;
            this.txtPRIX_DONNE_PAR.Visible = false;
            // 
            // TXTLIBELLE_UNITE_PRIX_DEVIS
            // 
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.AccAcceptNumbersOnly = false;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.AccAllowComma = false;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.AccBackgroundColor = System.Drawing.Color.White;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.AccHidenValue = "";
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.AccNotAllowedChars = null;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.AccReadOnly = false;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.AccReadOnlyAllowDelete = false;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.AccRequired = false;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.BackColor = System.Drawing.Color.White;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.CustomBackColor = System.Drawing.Color.White;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.Font = new System.Drawing.Font("Tahoma", 10F);
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.ForeColor = System.Drawing.Color.DodgerBlue;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.Location = new System.Drawing.Point(1002, 346);
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.Margin = new System.Windows.Forms.Padding(2);
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.MaxLength = 32767;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.Multiline = false;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.Name = "TXTLIBELLE_UNITE_PRIX_DEVIS";
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.ReadOnly = false;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.Size = new System.Drawing.Size(187, 27);
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.TabIndex = 641;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.TextAlignment = Infragistics.Win.HAlign.Left;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.UseSystemPasswordChar = false;
            this.TXTLIBELLE_UNITE_PRIX_DEVIS.Visible = false;
            // 
            // txtPRX_DEVIS_NET_UD
            // 
            this.txtPRX_DEVIS_NET_UD.AccAcceptNumbersOnly = false;
            this.txtPRX_DEVIS_NET_UD.AccAllowComma = false;
            this.txtPRX_DEVIS_NET_UD.AccBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtPRX_DEVIS_NET_UD.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRX_DEVIS_NET_UD.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPRX_DEVIS_NET_UD.AccHidenValue = "";
            this.txtPRX_DEVIS_NET_UD.AccNotAllowedChars = null;
            this.txtPRX_DEVIS_NET_UD.AccReadOnly = false;
            this.txtPRX_DEVIS_NET_UD.AccReadOnlyAllowDelete = false;
            this.txtPRX_DEVIS_NET_UD.AccRequired = false;
            this.txtPRX_DEVIS_NET_UD.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtPRX_DEVIS_NET_UD.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtPRX_DEVIS_NET_UD.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPRX_DEVIS_NET_UD.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtPRX_DEVIS_NET_UD.Location = new System.Drawing.Point(1002, 258);
            this.txtPRX_DEVIS_NET_UD.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRX_DEVIS_NET_UD.MaxLength = 32767;
            this.txtPRX_DEVIS_NET_UD.Multiline = false;
            this.txtPRX_DEVIS_NET_UD.Name = "txtPRX_DEVIS_NET_UD";
            this.txtPRX_DEVIS_NET_UD.ReadOnly = false;
            this.txtPRX_DEVIS_NET_UD.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRX_DEVIS_NET_UD.Size = new System.Drawing.Size(187, 27);
            this.txtPRX_DEVIS_NET_UD.TabIndex = 640;
            this.txtPRX_DEVIS_NET_UD.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRX_DEVIS_NET_UD.UseSystemPasswordChar = false;
            // 
            // label119
            // 
            this.label119.AutoSize = true;
            this.label119.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label119.Location = new System.Drawing.Point(798, 510);
            this.label119.Name = "label119";
            this.label119.Size = new System.Drawing.Size(88, 16);
            this.label119.TabIndex = 639;
            this.label119.Text = "Unité prix fab.";
            this.label119.Visible = false;
            // 
            // label118
            // 
            this.label118.AutoSize = true;
            this.label118.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label118.Location = new System.Drawing.Point(798, 481);
            this.label118.Name = "label118";
            this.label118.Size = new System.Drawing.Size(55, 16);
            this.label118.TabIndex = 638;
            this.label118.Text = "Prix fab.";
            this.label118.Visible = false;
            // 
            // label117
            // 
            this.label117.AutoSize = true;
            this.label117.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label117.Location = new System.Drawing.Point(798, 450);
            this.label117.Name = "label117";
            this.label117.Size = new System.Drawing.Size(103, 16);
            this.label117.TabIndex = 637;
            this.label117.Text = "Qte untite valeur";
            this.label117.Visible = false;
            // 
            // label116
            // 
            this.label116.AutoSize = true;
            this.label116.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label116.Location = new System.Drawing.Point(798, 419);
            this.label116.Name = "label116";
            this.label116.Size = new System.Drawing.Size(79, 16);
            this.label116.TabIndex = 636;
            this.label116.Text = "Unité valeur";
            this.label116.Visible = false;
            // 
            // label115
            // 
            this.label115.AutoSize = true;
            this.label115.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label115.Location = new System.Drawing.Point(798, 300);
            this.label115.Name = "label115";
            this.label115.Size = new System.Drawing.Size(88, 16);
            this.label115.TabIndex = 635;
            this.label115.Text = "Date création";
            // 
            // label114
            // 
            this.label114.AutoSize = true;
            this.label114.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label114.Location = new System.Drawing.Point(798, 382);
            this.label114.Name = "label114";
            this.label114.Size = new System.Drawing.Size(97, 16);
            this.label114.TabIndex = 634;
            this.label114.Text = "Prix donné par ";
            this.label114.Visible = false;
            // 
            // label113
            // 
            this.label113.AutoSize = true;
            this.label113.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label113.Location = new System.Drawing.Point(786, 349);
            this.label113.Name = "label113";
            this.label113.Size = new System.Drawing.Size(217, 16);
            this.label113.TabIndex = 633;
            this.label113.Text = "TXTLIBELLE_UNITE_PRIX_DEVIS";
            this.label113.Visible = false;
            // 
            // label112
            // 
            this.label112.AutoSize = true;
            this.label112.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label112.Location = new System.Drawing.Point(798, 264);
            this.label112.Name = "label112";
            this.label112.Size = new System.Drawing.Size(110, 16);
            this.label112.TabIndex = 632;
            this.label112.Text = "Prix devis net UD";
            // 
            // label111
            // 
            this.label111.AutoSize = true;
            this.label111.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label111.Location = new System.Drawing.Point(388, 326);
            this.label111.Name = "label111";
            this.label111.Size = new System.Drawing.Size(136, 16);
            this.label111.TabIndex = 631;
            this.label111.Text = "QTE MIN CMD UCNF";
            // 
            // label110
            // 
            this.label110.AutoSize = true;
            this.label110.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label110.Location = new System.Drawing.Point(388, 574);
            this.label110.Name = "label110";
            this.label110.Size = new System.Drawing.Size(105, 16);
            this.label110.TabIndex = 630;
            this.label110.Text = "Unité prix Gecet ";
            this.label110.Visible = false;
            // 
            // label109
            // 
            this.label109.AutoSize = true;
            this.label109.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label109.Location = new System.Drawing.Point(390, 512);
            this.label109.Name = "label109";
            this.label109.Size = new System.Drawing.Size(92, 16);
            this.label109.TabIndex = 629;
            this.label109.Text = "Prix Gecet unt.";
            // 
            // label108
            // 
            this.label108.AutoSize = true;
            this.label108.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label108.Location = new System.Drawing.Point(807, 568);
            this.label108.Name = "label108";
            this.label108.Size = new System.Drawing.Size(105, 16);
            this.label108.TabIndex = 628;
            this.label108.Text = "UNité prix Gecet";
            this.label108.Visible = false;
            // 
            // label107
            // 
            this.label107.AutoSize = true;
            this.label107.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label107.Location = new System.Drawing.Point(388, 480);
            this.label107.Name = "label107";
            this.label107.Size = new System.Drawing.Size(69, 16);
            this.label107.TabIndex = 627;
            this.label107.Text = "Prix Gecet";
            // 
            // label106
            // 
            this.label106.AutoSize = true;
            this.label106.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label106.Location = new System.Drawing.Point(388, 450);
            this.label106.Name = "label106";
            this.label106.Size = new System.Drawing.Size(118, 16);
            this.label106.TabIndex = 626;
            this.label106.Text = "QTE MIN CMD UC";
            // 
            // label105
            // 
            this.label105.AutoSize = true;
            this.label105.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label105.Location = new System.Drawing.Point(388, 388);
            this.label105.Name = "label105";
            this.label105.Size = new System.Drawing.Size(74, 16);
            this.label105.TabIndex = 625;
            this.label105.Text = "Unité fourn.";
            // 
            // label104
            // 
            this.label104.AutoSize = true;
            this.label104.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label104.Location = new System.Drawing.Point(388, 419);
            this.label104.Name = "label104";
            this.label104.Size = new System.Drawing.Size(87, 16);
            this.label104.TabIndex = 624;
            this.label104.Text = "Qte unt. fourn.";
            // 
            // label103
            // 
            this.label103.AutoSize = true;
            this.label103.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label103.Location = new System.Drawing.Point(388, 357);
            this.label103.Name = "label103";
            this.label103.Size = new System.Drawing.Size(93, 16);
            this.label103.TabIndex = 623;
            this.label103.Text = "Nb uc dans uv";
            // 
            // txtLIBELLE_UNITE_PRIX_GECET_UNITE
            // 
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.AccAllowComma = false;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.AccHidenValue = "";
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.AccNotAllowedChars = null;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.AccReadOnly = false;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.AccRequired = false;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.Location = new System.Drawing.Point(594, 568);
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.MaxLength = 32767;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.Multiline = false;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.Name = "txtLIBELLE_UNITE_PRIX_GECET_UNITE";
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.ReadOnly = false;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.TabIndex = 622;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.UseSystemPasswordChar = false;
            this.txtLIBELLE_UNITE_PRIX_GECET_UNITE.Visible = false;
            // 
            // txtPRIX_GECET_UNITE
            // 
            this.txtPRIX_GECET_UNITE.AccAcceptNumbersOnly = false;
            this.txtPRIX_GECET_UNITE.AccAllowComma = false;
            this.txtPRIX_GECET_UNITE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRIX_GECET_UNITE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRIX_GECET_UNITE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPRIX_GECET_UNITE.AccHidenValue = "";
            this.txtPRIX_GECET_UNITE.AccNotAllowedChars = null;
            this.txtPRIX_GECET_UNITE.AccReadOnly = false;
            this.txtPRIX_GECET_UNITE.AccReadOnlyAllowDelete = false;
            this.txtPRIX_GECET_UNITE.AccRequired = false;
            this.txtPRIX_GECET_UNITE.BackColor = System.Drawing.Color.White;
            this.txtPRIX_GECET_UNITE.CustomBackColor = System.Drawing.Color.White;
            this.txtPRIX_GECET_UNITE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPRIX_GECET_UNITE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtPRIX_GECET_UNITE.Location = new System.Drawing.Point(594, 506);
            this.txtPRIX_GECET_UNITE.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRIX_GECET_UNITE.MaxLength = 32767;
            this.txtPRIX_GECET_UNITE.Multiline = false;
            this.txtPRIX_GECET_UNITE.Name = "txtPRIX_GECET_UNITE";
            this.txtPRIX_GECET_UNITE.ReadOnly = false;
            this.txtPRIX_GECET_UNITE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRIX_GECET_UNITE.Size = new System.Drawing.Size(187, 27);
            this.txtPRIX_GECET_UNITE.TabIndex = 621;
            this.txtPRIX_GECET_UNITE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRIX_GECET_UNITE.UseSystemPasswordChar = false;
            // 
            // txtPRIX_GECET
            // 
            this.txtPRIX_GECET.AccAcceptNumbersOnly = false;
            this.txtPRIX_GECET.AccAllowComma = false;
            this.txtPRIX_GECET.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRIX_GECET.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRIX_GECET.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPRIX_GECET.AccHidenValue = "";
            this.txtPRIX_GECET.AccNotAllowedChars = null;
            this.txtPRIX_GECET.AccReadOnly = false;
            this.txtPRIX_GECET.AccReadOnlyAllowDelete = false;
            this.txtPRIX_GECET.AccRequired = false;
            this.txtPRIX_GECET.BackColor = System.Drawing.Color.White;
            this.txtPRIX_GECET.CustomBackColor = System.Drawing.Color.White;
            this.txtPRIX_GECET.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPRIX_GECET.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtPRIX_GECET.Location = new System.Drawing.Point(594, 475);
            this.txtPRIX_GECET.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRIX_GECET.MaxLength = 32767;
            this.txtPRIX_GECET.Multiline = false;
            this.txtPRIX_GECET.Name = "txtPRIX_GECET";
            this.txtPRIX_GECET.ReadOnly = false;
            this.txtPRIX_GECET.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRIX_GECET.Size = new System.Drawing.Size(187, 27);
            this.txtPRIX_GECET.TabIndex = 620;
            this.txtPRIX_GECET.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRIX_GECET.UseSystemPasswordChar = false;
            // 
            // txtLIBELLE_UNITE_PRIX_GECET
            // 
            this.txtLIBELLE_UNITE_PRIX_GECET.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_UNITE_PRIX_GECET.AccAllowComma = false;
            this.txtLIBELLE_UNITE_PRIX_GECET.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_GECET.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_UNITE_PRIX_GECET.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_UNITE_PRIX_GECET.AccHidenValue = "";
            this.txtLIBELLE_UNITE_PRIX_GECET.AccNotAllowedChars = null;
            this.txtLIBELLE_UNITE_PRIX_GECET.AccReadOnly = false;
            this.txtLIBELLE_UNITE_PRIX_GECET.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_UNITE_PRIX_GECET.AccRequired = false;
            this.txtLIBELLE_UNITE_PRIX_GECET.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_GECET.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_GECET.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_UNITE_PRIX_GECET.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_UNITE_PRIX_GECET.Location = new System.Drawing.Point(1002, 568);
            this.txtLIBELLE_UNITE_PRIX_GECET.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_UNITE_PRIX_GECET.MaxLength = 32767;
            this.txtLIBELLE_UNITE_PRIX_GECET.Multiline = false;
            this.txtLIBELLE_UNITE_PRIX_GECET.Name = "txtLIBELLE_UNITE_PRIX_GECET";
            this.txtLIBELLE_UNITE_PRIX_GECET.ReadOnly = false;
            this.txtLIBELLE_UNITE_PRIX_GECET.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_UNITE_PRIX_GECET.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_UNITE_PRIX_GECET.TabIndex = 619;
            this.txtLIBELLE_UNITE_PRIX_GECET.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_UNITE_PRIX_GECET.UseSystemPasswordChar = false;
            this.txtLIBELLE_UNITE_PRIX_GECET.Visible = false;
            // 
            // label102
            // 
            this.label102.AutoSize = true;
            this.label102.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label102.Location = new System.Drawing.Point(10, 574);
            this.label102.Name = "label102";
            this.label102.Size = new System.Drawing.Size(105, 16);
            this.label102.TabIndex = 618;
            this.label102.Text = "Prix public détail";
            // 
            // label101
            // 
            this.label101.AutoSize = true;
            this.label101.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label101.Location = new System.Drawing.Point(10, 543);
            this.label101.Name = "label101";
            this.label101.Size = new System.Drawing.Size(87, 16);
            this.label101.TabIndex = 617;
            this.label101.Text = "Prix net détail";
            // 
            // label100
            // 
            this.label100.AutoSize = true;
            this.label100.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label100.Location = new System.Drawing.Point(10, 512);
            this.label100.Name = "label100";
            this.label100.Size = new System.Drawing.Size(93, 16);
            this.label100.TabIndex = 616;
            this.label100.Text = "Réf. historqiue";
            // 
            // txtPRIX_PUBLIC_DETAILS
            // 
            this.txtPRIX_PUBLIC_DETAILS.AccAcceptNumbersOnly = false;
            this.txtPRIX_PUBLIC_DETAILS.AccAllowComma = false;
            this.txtPRIX_PUBLIC_DETAILS.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRIX_PUBLIC_DETAILS.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRIX_PUBLIC_DETAILS.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPRIX_PUBLIC_DETAILS.AccHidenValue = "";
            this.txtPRIX_PUBLIC_DETAILS.AccNotAllowedChars = null;
            this.txtPRIX_PUBLIC_DETAILS.AccReadOnly = false;
            this.txtPRIX_PUBLIC_DETAILS.AccReadOnlyAllowDelete = false;
            this.txtPRIX_PUBLIC_DETAILS.AccRequired = false;
            this.txtPRIX_PUBLIC_DETAILS.BackColor = System.Drawing.Color.White;
            this.txtPRIX_PUBLIC_DETAILS.CustomBackColor = System.Drawing.Color.White;
            this.txtPRIX_PUBLIC_DETAILS.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPRIX_PUBLIC_DETAILS.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtPRIX_PUBLIC_DETAILS.Location = new System.Drawing.Point(170, 568);
            this.txtPRIX_PUBLIC_DETAILS.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRIX_PUBLIC_DETAILS.MaxLength = 32767;
            this.txtPRIX_PUBLIC_DETAILS.Multiline = false;
            this.txtPRIX_PUBLIC_DETAILS.Name = "txtPRIX_PUBLIC_DETAILS";
            this.txtPRIX_PUBLIC_DETAILS.ReadOnly = false;
            this.txtPRIX_PUBLIC_DETAILS.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRIX_PUBLIC_DETAILS.Size = new System.Drawing.Size(187, 27);
            this.txtPRIX_PUBLIC_DETAILS.TabIndex = 615;
            this.txtPRIX_PUBLIC_DETAILS.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRIX_PUBLIC_DETAILS.UseSystemPasswordChar = false;
            // 
            // txtPRIX_NET_DETAILS
            // 
            this.txtPRIX_NET_DETAILS.AccAcceptNumbersOnly = false;
            this.txtPRIX_NET_DETAILS.AccAllowComma = false;
            this.txtPRIX_NET_DETAILS.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRIX_NET_DETAILS.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRIX_NET_DETAILS.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPRIX_NET_DETAILS.AccHidenValue = "";
            this.txtPRIX_NET_DETAILS.AccNotAllowedChars = null;
            this.txtPRIX_NET_DETAILS.AccReadOnly = false;
            this.txtPRIX_NET_DETAILS.AccReadOnlyAllowDelete = false;
            this.txtPRIX_NET_DETAILS.AccRequired = false;
            this.txtPRIX_NET_DETAILS.BackColor = System.Drawing.Color.White;
            this.txtPRIX_NET_DETAILS.CustomBackColor = System.Drawing.Color.White;
            this.txtPRIX_NET_DETAILS.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPRIX_NET_DETAILS.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtPRIX_NET_DETAILS.Location = new System.Drawing.Point(170, 537);
            this.txtPRIX_NET_DETAILS.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRIX_NET_DETAILS.MaxLength = 32767;
            this.txtPRIX_NET_DETAILS.Multiline = false;
            this.txtPRIX_NET_DETAILS.Name = "txtPRIX_NET_DETAILS";
            this.txtPRIX_NET_DETAILS.ReadOnly = false;
            this.txtPRIX_NET_DETAILS.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRIX_NET_DETAILS.Size = new System.Drawing.Size(187, 27);
            this.txtPRIX_NET_DETAILS.TabIndex = 614;
            this.txtPRIX_NET_DETAILS.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRIX_NET_DETAILS.UseSystemPasswordChar = false;
            // 
            // txtREF_HISTORIQUE
            // 
            this.txtREF_HISTORIQUE.AccAcceptNumbersOnly = false;
            this.txtREF_HISTORIQUE.AccAllowComma = false;
            this.txtREF_HISTORIQUE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtREF_HISTORIQUE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtREF_HISTORIQUE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtREF_HISTORIQUE.AccHidenValue = "";
            this.txtREF_HISTORIQUE.AccNotAllowedChars = null;
            this.txtREF_HISTORIQUE.AccReadOnly = false;
            this.txtREF_HISTORIQUE.AccReadOnlyAllowDelete = false;
            this.txtREF_HISTORIQUE.AccRequired = false;
            this.txtREF_HISTORIQUE.BackColor = System.Drawing.Color.White;
            this.txtREF_HISTORIQUE.CustomBackColor = System.Drawing.Color.White;
            this.txtREF_HISTORIQUE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtREF_HISTORIQUE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtREF_HISTORIQUE.Location = new System.Drawing.Point(170, 506);
            this.txtREF_HISTORIQUE.Margin = new System.Windows.Forms.Padding(2);
            this.txtREF_HISTORIQUE.MaxLength = 32767;
            this.txtREF_HISTORIQUE.Multiline = false;
            this.txtREF_HISTORIQUE.Name = "txtREF_HISTORIQUE";
            this.txtREF_HISTORIQUE.ReadOnly = false;
            this.txtREF_HISTORIQUE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtREF_HISTORIQUE.Size = new System.Drawing.Size(187, 27);
            this.txtREF_HISTORIQUE.TabIndex = 613;
            this.txtREF_HISTORIQUE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtREF_HISTORIQUE.UseSystemPasswordChar = false;
            // 
            // label99
            // 
            this.label99.AutoSize = true;
            this.label99.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label99.Location = new System.Drawing.Point(10, 480);
            this.label99.Name = "label99";
            this.label99.Size = new System.Drawing.Size(56, 16);
            this.label99.TabIndex = 612;
            this.label99.Text = "US / UV";
            // 
            // label98
            // 
            this.label98.AutoSize = true;
            this.label98.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label98.Location = new System.Drawing.Point(10, 450);
            this.label98.Name = "label98";
            this.label98.Size = new System.Drawing.Size(56, 16);
            this.label98.TabIndex = 611;
            this.label98.Text = "US / UC";
            // 
            // label97
            // 
            this.label97.AutoSize = true;
            this.label97.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label97.Location = new System.Drawing.Point(10, 419);
            this.label97.Name = "label97";
            this.label97.Size = new System.Drawing.Size(57, 16);
            this.label97.TabIndex = 610;
            this.label97.Text = "UD / US";
            // 
            // label96
            // 
            this.label96.AutoSize = true;
            this.label96.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label96.Location = new System.Drawing.Point(10, 388);
            this.label96.Name = "label96";
            this.label96.Size = new System.Drawing.Size(74, 16);
            this.label96.TabIndex = 609;
            this.label96.Text = "UNCF / UC";
            // 
            // label95
            // 
            this.label95.AutoSize = true;
            this.label95.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label95.Location = new System.Drawing.Point(11, 357);
            this.label95.Name = "label95";
            this.label95.Size = new System.Drawing.Size(266, 16);
            this.label95.TabIndex = 608;
            this.label95.Text = "txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE";
            // 
            // label94
            // 
            this.label94.AutoSize = true;
            this.label94.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label94.Location = new System.Drawing.Point(10, 326);
            this.label94.Name = "label94";
            this.label94.Size = new System.Drawing.Size(111, 16);
            this.label94.TabIndex = 607;
            this.label94.Text = "Prix devis majoré";
            // 
            // txtUS_SUR_UV
            // 
            this.txtUS_SUR_UV.AccAcceptNumbersOnly = false;
            this.txtUS_SUR_UV.AccAllowComma = false;
            this.txtUS_SUR_UV.AccBackgroundColor = System.Drawing.Color.White;
            this.txtUS_SUR_UV.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtUS_SUR_UV.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtUS_SUR_UV.AccHidenValue = "";
            this.txtUS_SUR_UV.AccNotAllowedChars = null;
            this.txtUS_SUR_UV.AccReadOnly = false;
            this.txtUS_SUR_UV.AccReadOnlyAllowDelete = false;
            this.txtUS_SUR_UV.AccRequired = false;
            this.txtUS_SUR_UV.BackColor = System.Drawing.Color.White;
            this.txtUS_SUR_UV.CustomBackColor = System.Drawing.Color.White;
            this.txtUS_SUR_UV.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtUS_SUR_UV.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtUS_SUR_UV.Location = new System.Drawing.Point(170, 475);
            this.txtUS_SUR_UV.Margin = new System.Windows.Forms.Padding(2);
            this.txtUS_SUR_UV.MaxLength = 32767;
            this.txtUS_SUR_UV.Multiline = false;
            this.txtUS_SUR_UV.Name = "txtUS_SUR_UV";
            this.txtUS_SUR_UV.ReadOnly = false;
            this.txtUS_SUR_UV.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtUS_SUR_UV.Size = new System.Drawing.Size(187, 27);
            this.txtUS_SUR_UV.TabIndex = 606;
            this.txtUS_SUR_UV.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtUS_SUR_UV.UseSystemPasswordChar = false;
            // 
            // txtQTE_MIN_CMD_UC
            // 
            this.txtQTE_MIN_CMD_UC.AccAcceptNumbersOnly = false;
            this.txtQTE_MIN_CMD_UC.AccAllowComma = false;
            this.txtQTE_MIN_CMD_UC.AccBackgroundColor = System.Drawing.Color.White;
            this.txtQTE_MIN_CMD_UC.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtQTE_MIN_CMD_UC.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtQTE_MIN_CMD_UC.AccHidenValue = "";
            this.txtQTE_MIN_CMD_UC.AccNotAllowedChars = null;
            this.txtQTE_MIN_CMD_UC.AccReadOnly = false;
            this.txtQTE_MIN_CMD_UC.AccReadOnlyAllowDelete = false;
            this.txtQTE_MIN_CMD_UC.AccRequired = false;
            this.txtQTE_MIN_CMD_UC.BackColor = System.Drawing.Color.White;
            this.txtQTE_MIN_CMD_UC.CustomBackColor = System.Drawing.Color.White;
            this.txtQTE_MIN_CMD_UC.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtQTE_MIN_CMD_UC.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtQTE_MIN_CMD_UC.Location = new System.Drawing.Point(594, 444);
            this.txtQTE_MIN_CMD_UC.Margin = new System.Windows.Forms.Padding(2);
            this.txtQTE_MIN_CMD_UC.MaxLength = 32767;
            this.txtQTE_MIN_CMD_UC.Multiline = false;
            this.txtQTE_MIN_CMD_UC.Name = "txtQTE_MIN_CMD_UC";
            this.txtQTE_MIN_CMD_UC.ReadOnly = false;
            this.txtQTE_MIN_CMD_UC.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtQTE_MIN_CMD_UC.Size = new System.Drawing.Size(187, 27);
            this.txtQTE_MIN_CMD_UC.TabIndex = 605;
            this.txtQTE_MIN_CMD_UC.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtQTE_MIN_CMD_UC.UseSystemPasswordChar = false;
            // 
            // txtQTE_UNITE_FOURNISSEUR
            // 
            this.txtQTE_UNITE_FOURNISSEUR.AccAcceptNumbersOnly = false;
            this.txtQTE_UNITE_FOURNISSEUR.AccAllowComma = false;
            this.txtQTE_UNITE_FOURNISSEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_FOURNISSEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtQTE_UNITE_FOURNISSEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtQTE_UNITE_FOURNISSEUR.AccHidenValue = "";
            this.txtQTE_UNITE_FOURNISSEUR.AccNotAllowedChars = null;
            this.txtQTE_UNITE_FOURNISSEUR.AccReadOnly = false;
            this.txtQTE_UNITE_FOURNISSEUR.AccReadOnlyAllowDelete = false;
            this.txtQTE_UNITE_FOURNISSEUR.AccRequired = false;
            this.txtQTE_UNITE_FOURNISSEUR.BackColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_FOURNISSEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_FOURNISSEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtQTE_UNITE_FOURNISSEUR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtQTE_UNITE_FOURNISSEUR.Location = new System.Drawing.Point(594, 413);
            this.txtQTE_UNITE_FOURNISSEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtQTE_UNITE_FOURNISSEUR.MaxLength = 32767;
            this.txtQTE_UNITE_FOURNISSEUR.Multiline = false;
            this.txtQTE_UNITE_FOURNISSEUR.Name = "txtQTE_UNITE_FOURNISSEUR";
            this.txtQTE_UNITE_FOURNISSEUR.ReadOnly = false;
            this.txtQTE_UNITE_FOURNISSEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtQTE_UNITE_FOURNISSEUR.Size = new System.Drawing.Size(187, 27);
            this.txtQTE_UNITE_FOURNISSEUR.TabIndex = 604;
            this.txtQTE_UNITE_FOURNISSEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtQTE_UNITE_FOURNISSEUR.UseSystemPasswordChar = false;
            // 
            // txtLIBELLE_UNITE_FOURNISSEUR
            // 
            this.txtLIBELLE_UNITE_FOURNISSEUR.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_UNITE_FOURNISSEUR.AccAllowComma = false;
            this.txtLIBELLE_UNITE_FOURNISSEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_FOURNISSEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_UNITE_FOURNISSEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_UNITE_FOURNISSEUR.AccHidenValue = "";
            this.txtLIBELLE_UNITE_FOURNISSEUR.AccNotAllowedChars = null;
            this.txtLIBELLE_UNITE_FOURNISSEUR.AccReadOnly = false;
            this.txtLIBELLE_UNITE_FOURNISSEUR.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_UNITE_FOURNISSEUR.AccRequired = false;
            this.txtLIBELLE_UNITE_FOURNISSEUR.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_FOURNISSEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_FOURNISSEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_UNITE_FOURNISSEUR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_UNITE_FOURNISSEUR.Location = new System.Drawing.Point(594, 382);
            this.txtLIBELLE_UNITE_FOURNISSEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_UNITE_FOURNISSEUR.MaxLength = 32767;
            this.txtLIBELLE_UNITE_FOURNISSEUR.Multiline = false;
            this.txtLIBELLE_UNITE_FOURNISSEUR.Name = "txtLIBELLE_UNITE_FOURNISSEUR";
            this.txtLIBELLE_UNITE_FOURNISSEUR.ReadOnly = false;
            this.txtLIBELLE_UNITE_FOURNISSEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_UNITE_FOURNISSEUR.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_UNITE_FOURNISSEUR.TabIndex = 603;
            this.txtLIBELLE_UNITE_FOURNISSEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_UNITE_FOURNISSEUR.UseSystemPasswordChar = false;
            // 
            // txtNB_UC_DANS_UV
            // 
            this.txtNB_UC_DANS_UV.AccAcceptNumbersOnly = false;
            this.txtNB_UC_DANS_UV.AccAllowComma = false;
            this.txtNB_UC_DANS_UV.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNB_UC_DANS_UV.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNB_UC_DANS_UV.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtNB_UC_DANS_UV.AccHidenValue = "";
            this.txtNB_UC_DANS_UV.AccNotAllowedChars = null;
            this.txtNB_UC_DANS_UV.AccReadOnly = false;
            this.txtNB_UC_DANS_UV.AccReadOnlyAllowDelete = false;
            this.txtNB_UC_DANS_UV.AccRequired = false;
            this.txtNB_UC_DANS_UV.BackColor = System.Drawing.Color.White;
            this.txtNB_UC_DANS_UV.CustomBackColor = System.Drawing.Color.White;
            this.txtNB_UC_DANS_UV.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNB_UC_DANS_UV.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtNB_UC_DANS_UV.Location = new System.Drawing.Point(594, 351);
            this.txtNB_UC_DANS_UV.Margin = new System.Windows.Forms.Padding(2);
            this.txtNB_UC_DANS_UV.MaxLength = 32767;
            this.txtNB_UC_DANS_UV.Multiline = false;
            this.txtNB_UC_DANS_UV.Name = "txtNB_UC_DANS_UV";
            this.txtNB_UC_DANS_UV.ReadOnly = false;
            this.txtNB_UC_DANS_UV.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNB_UC_DANS_UV.Size = new System.Drawing.Size(187, 27);
            this.txtNB_UC_DANS_UV.TabIndex = 602;
            this.txtNB_UC_DANS_UV.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNB_UC_DANS_UV.UseSystemPasswordChar = false;
            // 
            // txtQTE_MIN_CMD_UCNF
            // 
            this.txtQTE_MIN_CMD_UCNF.AccAcceptNumbersOnly = false;
            this.txtQTE_MIN_CMD_UCNF.AccAllowComma = false;
            this.txtQTE_MIN_CMD_UCNF.AccBackgroundColor = System.Drawing.Color.White;
            this.txtQTE_MIN_CMD_UCNF.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtQTE_MIN_CMD_UCNF.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtQTE_MIN_CMD_UCNF.AccHidenValue = "";
            this.txtQTE_MIN_CMD_UCNF.AccNotAllowedChars = null;
            this.txtQTE_MIN_CMD_UCNF.AccReadOnly = false;
            this.txtQTE_MIN_CMD_UCNF.AccReadOnlyAllowDelete = false;
            this.txtQTE_MIN_CMD_UCNF.AccRequired = false;
            this.txtQTE_MIN_CMD_UCNF.BackColor = System.Drawing.Color.White;
            this.txtQTE_MIN_CMD_UCNF.CustomBackColor = System.Drawing.Color.White;
            this.txtQTE_MIN_CMD_UCNF.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtQTE_MIN_CMD_UCNF.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtQTE_MIN_CMD_UCNF.Location = new System.Drawing.Point(594, 320);
            this.txtQTE_MIN_CMD_UCNF.Margin = new System.Windows.Forms.Padding(2);
            this.txtQTE_MIN_CMD_UCNF.MaxLength = 32767;
            this.txtQTE_MIN_CMD_UCNF.Multiline = false;
            this.txtQTE_MIN_CMD_UCNF.Name = "txtQTE_MIN_CMD_UCNF";
            this.txtQTE_MIN_CMD_UCNF.ReadOnly = false;
            this.txtQTE_MIN_CMD_UCNF.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtQTE_MIN_CMD_UCNF.Size = new System.Drawing.Size(187, 27);
            this.txtQTE_MIN_CMD_UCNF.TabIndex = 601;
            this.txtQTE_MIN_CMD_UCNF.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtQTE_MIN_CMD_UCNF.UseSystemPasswordChar = false;
            // 
            // txtUS_SUR_UC
            // 
            this.txtUS_SUR_UC.AccAcceptNumbersOnly = false;
            this.txtUS_SUR_UC.AccAllowComma = false;
            this.txtUS_SUR_UC.AccBackgroundColor = System.Drawing.Color.White;
            this.txtUS_SUR_UC.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtUS_SUR_UC.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtUS_SUR_UC.AccHidenValue = "";
            this.txtUS_SUR_UC.AccNotAllowedChars = null;
            this.txtUS_SUR_UC.AccReadOnly = false;
            this.txtUS_SUR_UC.AccReadOnlyAllowDelete = false;
            this.txtUS_SUR_UC.AccRequired = false;
            this.txtUS_SUR_UC.BackColor = System.Drawing.Color.White;
            this.txtUS_SUR_UC.CustomBackColor = System.Drawing.Color.White;
            this.txtUS_SUR_UC.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtUS_SUR_UC.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtUS_SUR_UC.Location = new System.Drawing.Point(170, 444);
            this.txtUS_SUR_UC.Margin = new System.Windows.Forms.Padding(2);
            this.txtUS_SUR_UC.MaxLength = 32767;
            this.txtUS_SUR_UC.Multiline = false;
            this.txtUS_SUR_UC.Name = "txtUS_SUR_UC";
            this.txtUS_SUR_UC.ReadOnly = false;
            this.txtUS_SUR_UC.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtUS_SUR_UC.Size = new System.Drawing.Size(187, 27);
            this.txtUS_SUR_UC.TabIndex = 600;
            this.txtUS_SUR_UC.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtUS_SUR_UC.UseSystemPasswordChar = false;
            // 
            // TXTUD_SUR_US
            // 
            this.TXTUD_SUR_US.AccAcceptNumbersOnly = false;
            this.TXTUD_SUR_US.AccAllowComma = false;
            this.TXTUD_SUR_US.AccBackgroundColor = System.Drawing.Color.White;
            this.TXTUD_SUR_US.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.TXTUD_SUR_US.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.TXTUD_SUR_US.AccHidenValue = "";
            this.TXTUD_SUR_US.AccNotAllowedChars = null;
            this.TXTUD_SUR_US.AccReadOnly = false;
            this.TXTUD_SUR_US.AccReadOnlyAllowDelete = false;
            this.TXTUD_SUR_US.AccRequired = false;
            this.TXTUD_SUR_US.BackColor = System.Drawing.Color.White;
            this.TXTUD_SUR_US.CustomBackColor = System.Drawing.Color.White;
            this.TXTUD_SUR_US.Font = new System.Drawing.Font("Tahoma", 10F);
            this.TXTUD_SUR_US.ForeColor = System.Drawing.Color.DodgerBlue;
            this.TXTUD_SUR_US.Location = new System.Drawing.Point(171, 413);
            this.TXTUD_SUR_US.Margin = new System.Windows.Forms.Padding(2);
            this.TXTUD_SUR_US.MaxLength = 32767;
            this.TXTUD_SUR_US.Multiline = false;
            this.TXTUD_SUR_US.Name = "TXTUD_SUR_US";
            this.TXTUD_SUR_US.ReadOnly = false;
            this.TXTUD_SUR_US.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.TXTUD_SUR_US.Size = new System.Drawing.Size(187, 27);
            this.TXTUD_SUR_US.TabIndex = 599;
            this.TXTUD_SUR_US.TextAlignment = Infragistics.Win.HAlign.Left;
            this.TXTUD_SUR_US.UseSystemPasswordChar = false;
            // 
            // txtUNCF_SUR_UC
            // 
            this.txtUNCF_SUR_UC.AccAcceptNumbersOnly = false;
            this.txtUNCF_SUR_UC.AccAllowComma = false;
            this.txtUNCF_SUR_UC.AccBackgroundColor = System.Drawing.Color.White;
            this.txtUNCF_SUR_UC.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtUNCF_SUR_UC.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtUNCF_SUR_UC.AccHidenValue = "";
            this.txtUNCF_SUR_UC.AccNotAllowedChars = null;
            this.txtUNCF_SUR_UC.AccReadOnly = false;
            this.txtUNCF_SUR_UC.AccReadOnlyAllowDelete = false;
            this.txtUNCF_SUR_UC.AccRequired = false;
            this.txtUNCF_SUR_UC.BackColor = System.Drawing.Color.White;
            this.txtUNCF_SUR_UC.CustomBackColor = System.Drawing.Color.White;
            this.txtUNCF_SUR_UC.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtUNCF_SUR_UC.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtUNCF_SUR_UC.Location = new System.Drawing.Point(171, 382);
            this.txtUNCF_SUR_UC.Margin = new System.Windows.Forms.Padding(2);
            this.txtUNCF_SUR_UC.MaxLength = 32767;
            this.txtUNCF_SUR_UC.Multiline = false;
            this.txtUNCF_SUR_UC.Name = "txtUNCF_SUR_UC";
            this.txtUNCF_SUR_UC.ReadOnly = false;
            this.txtUNCF_SUR_UC.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtUNCF_SUR_UC.Size = new System.Drawing.Size(187, 27);
            this.txtUNCF_SUR_UC.TabIndex = 598;
            this.txtUNCF_SUR_UC.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtUNCF_SUR_UC.UseSystemPasswordChar = false;
            // 
            // txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE
            // 
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.AccAllowComma = false;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.AccHidenValue = "";
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.AccNotAllowedChars = null;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.AccReadOnly = false;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.AccRequired = false;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.Location = new System.Drawing.Point(171, 351);
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.MaxLength = 32767;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.Multiline = false;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.Name = "txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE";
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.ReadOnly = false;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.TabIndex = 597;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE.UseSystemPasswordChar = false;
            // 
            // txtPRIX_DEVIS_MAJORE
            // 
            this.txtPRIX_DEVIS_MAJORE.AccAcceptNumbersOnly = false;
            this.txtPRIX_DEVIS_MAJORE.AccAllowComma = false;
            this.txtPRIX_DEVIS_MAJORE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRIX_DEVIS_MAJORE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRIX_DEVIS_MAJORE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPRIX_DEVIS_MAJORE.AccHidenValue = "";
            this.txtPRIX_DEVIS_MAJORE.AccNotAllowedChars = null;
            this.txtPRIX_DEVIS_MAJORE.AccReadOnly = false;
            this.txtPRIX_DEVIS_MAJORE.AccReadOnlyAllowDelete = false;
            this.txtPRIX_DEVIS_MAJORE.AccRequired = false;
            this.txtPRIX_DEVIS_MAJORE.BackColor = System.Drawing.Color.White;
            this.txtPRIX_DEVIS_MAJORE.CustomBackColor = System.Drawing.Color.White;
            this.txtPRIX_DEVIS_MAJORE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPRIX_DEVIS_MAJORE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtPRIX_DEVIS_MAJORE.Location = new System.Drawing.Point(171, 320);
            this.txtPRIX_DEVIS_MAJORE.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRIX_DEVIS_MAJORE.MaxLength = 32767;
            this.txtPRIX_DEVIS_MAJORE.Multiline = false;
            this.txtPRIX_DEVIS_MAJORE.Name = "txtPRIX_DEVIS_MAJORE";
            this.txtPRIX_DEVIS_MAJORE.ReadOnly = false;
            this.txtPRIX_DEVIS_MAJORE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRIX_DEVIS_MAJORE.Size = new System.Drawing.Size(187, 27);
            this.txtPRIX_DEVIS_MAJORE.TabIndex = 596;
            this.txtPRIX_DEVIS_MAJORE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRIX_DEVIS_MAJORE.UseSystemPasswordChar = false;
            // 
            // txtTAXE
            // 
            this.txtTAXE.AccAcceptNumbersOnly = false;
            this.txtTAXE.AccAllowComma = false;
            this.txtTAXE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTAXE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTAXE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtTAXE.AccHidenValue = "";
            this.txtTAXE.AccNotAllowedChars = null;
            this.txtTAXE.AccReadOnly = false;
            this.txtTAXE.AccReadOnlyAllowDelete = false;
            this.txtTAXE.AccRequired = false;
            this.txtTAXE.BackColor = System.Drawing.Color.White;
            this.txtTAXE.CustomBackColor = System.Drawing.Color.White;
            this.txtTAXE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtTAXE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtTAXE.Location = new System.Drawing.Point(1002, 227);
            this.txtTAXE.Margin = new System.Windows.Forms.Padding(2);
            this.txtTAXE.MaxLength = 32767;
            this.txtTAXE.Multiline = false;
            this.txtTAXE.Name = "txtTAXE";
            this.txtTAXE.ReadOnly = false;
            this.txtTAXE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTAXE.Size = new System.Drawing.Size(187, 27);
            this.txtTAXE.TabIndex = 595;
            this.txtTAXE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTAXE.UseSystemPasswordChar = false;
            // 
            // label56
            // 
            this.label56.AutoSize = true;
            this.label56.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label56.Location = new System.Drawing.Point(798, 234);
            this.label56.Name = "label56";
            this.label56.Size = new System.Drawing.Size(39, 16);
            this.label56.TabIndex = 594;
            this.label56.Text = "Taxe";
            // 
            // txtLIBELLE_COND_PRIX_COND_STOCK
            // 
            this.txtLIBELLE_COND_PRIX_COND_STOCK.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.AccAllowComma = false;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_COND_PRIX_COND_STOCK.AccHidenValue = "";
            this.txtLIBELLE_COND_PRIX_COND_STOCK.AccNotAllowedChars = null;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.AccReadOnly = false;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.AccRequired = false;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_COND_PRIX_COND_STOCK.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.Location = new System.Drawing.Point(1002, 537);
            this.txtLIBELLE_COND_PRIX_COND_STOCK.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_COND_PRIX_COND_STOCK.MaxLength = 32767;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.Multiline = false;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.Name = "txtLIBELLE_COND_PRIX_COND_STOCK";
            this.txtLIBELLE_COND_PRIX_COND_STOCK.ReadOnly = false;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_COND_PRIX_COND_STOCK.TabIndex = 593;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.UseSystemPasswordChar = false;
            this.txtLIBELLE_COND_PRIX_COND_STOCK.Visible = false;
            // 
            // label55
            // 
            this.label55.AutoSize = true;
            this.label55.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label55.ForeColor = System.Drawing.Color.SpringGreen;
            this.label55.Location = new System.Drawing.Point(807, 537);
            this.label55.Name = "label55";
            this.label55.Size = new System.Drawing.Size(252, 16);
            this.label55.TabIndex = 592;
            this.label55.Text = "txtLIBELLE_COND_PRIX_COND_STOCK";
            this.label55.Visible = false;
            // 
            // txtPRIX_COND_STOCK
            // 
            this.txtPRIX_COND_STOCK.AccAcceptNumbersOnly = false;
            this.txtPRIX_COND_STOCK.AccAllowComma = false;
            this.txtPRIX_COND_STOCK.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRIX_COND_STOCK.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRIX_COND_STOCK.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPRIX_COND_STOCK.AccHidenValue = "";
            this.txtPRIX_COND_STOCK.AccNotAllowedChars = null;
            this.txtPRIX_COND_STOCK.AccReadOnly = false;
            this.txtPRIX_COND_STOCK.AccReadOnlyAllowDelete = false;
            this.txtPRIX_COND_STOCK.AccRequired = false;
            this.txtPRIX_COND_STOCK.BackColor = System.Drawing.Color.White;
            this.txtPRIX_COND_STOCK.CustomBackColor = System.Drawing.Color.White;
            this.txtPRIX_COND_STOCK.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPRIX_COND_STOCK.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtPRIX_COND_STOCK.Location = new System.Drawing.Point(1002, 196);
            this.txtPRIX_COND_STOCK.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRIX_COND_STOCK.MaxLength = 32767;
            this.txtPRIX_COND_STOCK.Multiline = false;
            this.txtPRIX_COND_STOCK.Name = "txtPRIX_COND_STOCK";
            this.txtPRIX_COND_STOCK.ReadOnly = false;
            this.txtPRIX_COND_STOCK.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRIX_COND_STOCK.Size = new System.Drawing.Size(187, 27);
            this.txtPRIX_COND_STOCK.TabIndex = 591;
            this.txtPRIX_COND_STOCK.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRIX_COND_STOCK.UseSystemPasswordChar = false;
            // 
            // label48
            // 
            this.label48.AutoSize = true;
            this.label48.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label48.Location = new System.Drawing.Point(798, 202);
            this.label48.Name = "label48";
            this.label48.Size = new System.Drawing.Size(101, 16);
            this.label48.TabIndex = 590;
            this.label48.Text = "Prix cond. stock";
            // 
            // txtLIBELLE_UNITE_PRIX_STOCK
            // 
            this.txtLIBELLE_UNITE_PRIX_STOCK.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_UNITE_PRIX_STOCK.AccAllowComma = false;
            this.txtLIBELLE_UNITE_PRIX_STOCK.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_STOCK.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_UNITE_PRIX_STOCK.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_UNITE_PRIX_STOCK.AccHidenValue = "";
            this.txtLIBELLE_UNITE_PRIX_STOCK.AccNotAllowedChars = null;
            this.txtLIBELLE_UNITE_PRIX_STOCK.AccReadOnly = false;
            this.txtLIBELLE_UNITE_PRIX_STOCK.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_UNITE_PRIX_STOCK.AccRequired = false;
            this.txtLIBELLE_UNITE_PRIX_STOCK.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_STOCK.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_STOCK.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_UNITE_PRIX_STOCK.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_UNITE_PRIX_STOCK.Location = new System.Drawing.Point(1002, 165);
            this.txtLIBELLE_UNITE_PRIX_STOCK.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_UNITE_PRIX_STOCK.MaxLength = 32767;
            this.txtLIBELLE_UNITE_PRIX_STOCK.Multiline = false;
            this.txtLIBELLE_UNITE_PRIX_STOCK.Name = "txtLIBELLE_UNITE_PRIX_STOCK";
            this.txtLIBELLE_UNITE_PRIX_STOCK.ReadOnly = false;
            this.txtLIBELLE_UNITE_PRIX_STOCK.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_UNITE_PRIX_STOCK.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_UNITE_PRIX_STOCK.TabIndex = 589;
            this.txtLIBELLE_UNITE_PRIX_STOCK.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_UNITE_PRIX_STOCK.UseSystemPasswordChar = false;
            // 
            // label49
            // 
            this.label49.AutoSize = true;
            this.label49.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label49.Location = new System.Drawing.Point(798, 171);
            this.label49.Name = "label49";
            this.label49.Size = new System.Drawing.Size(98, 16);
            this.label49.TabIndex = 588;
            this.label49.Text = "Unité prix stock";
            // 
            // txtLIBELLE_COND_PRIX_COND_FOURNISSEUR
            // 
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.AccAllowComma = false;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.AccHidenValue = "";
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.AccNotAllowedChars = null;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.AccReadOnly = false;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.AccRequired = false;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.Location = new System.Drawing.Point(1002, 103);
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.MaxLength = 32767;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.Multiline = false;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.Name = "txtLIBELLE_COND_PRIX_COND_FOURNISSEUR";
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.ReadOnly = false;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.TabIndex = 587;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_COND_PRIX_COND_FOURNISSEUR.UseSystemPasswordChar = false;
            // 
            // label50
            // 
            this.label50.AutoSize = true;
            this.label50.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label50.Location = new System.Drawing.Point(798, 140);
            this.label50.Name = "label50";
            this.label50.Size = new System.Drawing.Size(65, 16);
            this.label50.TabIndex = 586;
            this.label50.Text = "Prix stock";
            // 
            // txtPRIX_STOCK
            // 
            this.txtPRIX_STOCK.AccAcceptNumbersOnly = false;
            this.txtPRIX_STOCK.AccAllowComma = false;
            this.txtPRIX_STOCK.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRIX_STOCK.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRIX_STOCK.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPRIX_STOCK.AccHidenValue = "";
            this.txtPRIX_STOCK.AccNotAllowedChars = null;
            this.txtPRIX_STOCK.AccReadOnly = false;
            this.txtPRIX_STOCK.AccReadOnlyAllowDelete = false;
            this.txtPRIX_STOCK.AccRequired = false;
            this.txtPRIX_STOCK.BackColor = System.Drawing.Color.White;
            this.txtPRIX_STOCK.CustomBackColor = System.Drawing.Color.White;
            this.txtPRIX_STOCK.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPRIX_STOCK.ForeColor = System.Drawing.Color.Black;
            this.txtPRIX_STOCK.Location = new System.Drawing.Point(1002, 134);
            this.txtPRIX_STOCK.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRIX_STOCK.MaxLength = 32767;
            this.txtPRIX_STOCK.Multiline = false;
            this.txtPRIX_STOCK.Name = "txtPRIX_STOCK";
            this.txtPRIX_STOCK.ReadOnly = false;
            this.txtPRIX_STOCK.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRIX_STOCK.Size = new System.Drawing.Size(187, 27);
            this.txtPRIX_STOCK.TabIndex = 585;
            this.txtPRIX_STOCK.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRIX_STOCK.UseSystemPasswordChar = false;
            // 
            // label51
            // 
            this.label51.AutoSize = true;
            this.label51.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label51.Location = new System.Drawing.Point(798, 106);
            this.label51.Name = "label51";
            this.label51.Size = new System.Drawing.Size(109, 16);
            this.label51.TabIndex = 584;
            this.label51.Text = "cond. fournisseur";
            // 
            // txtPRIX_COND_FOUR_2
            // 
            this.txtPRIX_COND_FOUR_2.AccAcceptNumbersOnly = false;
            this.txtPRIX_COND_FOUR_2.AccAllowComma = false;
            this.txtPRIX_COND_FOUR_2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRIX_COND_FOUR_2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRIX_COND_FOUR_2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPRIX_COND_FOUR_2.AccHidenValue = "";
            this.txtPRIX_COND_FOUR_2.AccNotAllowedChars = null;
            this.txtPRIX_COND_FOUR_2.AccReadOnly = false;
            this.txtPRIX_COND_FOUR_2.AccReadOnlyAllowDelete = false;
            this.txtPRIX_COND_FOUR_2.AccRequired = false;
            this.txtPRIX_COND_FOUR_2.BackColor = System.Drawing.Color.White;
            this.txtPRIX_COND_FOUR_2.CustomBackColor = System.Drawing.Color.White;
            this.txtPRIX_COND_FOUR_2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPRIX_COND_FOUR_2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtPRIX_COND_FOUR_2.Location = new System.Drawing.Point(1002, 72);
            this.txtPRIX_COND_FOUR_2.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRIX_COND_FOUR_2.MaxLength = 32767;
            this.txtPRIX_COND_FOUR_2.Multiline = false;
            this.txtPRIX_COND_FOUR_2.Name = "txtPRIX_COND_FOUR_2";
            this.txtPRIX_COND_FOUR_2.ReadOnly = false;
            this.txtPRIX_COND_FOUR_2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRIX_COND_FOUR_2.Size = new System.Drawing.Size(187, 27);
            this.txtPRIX_COND_FOUR_2.TabIndex = 583;
            this.txtPRIX_COND_FOUR_2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRIX_COND_FOUR_2.UseSystemPasswordChar = false;
            // 
            // label52
            // 
            this.label52.AutoSize = true;
            this.label52.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label52.Location = new System.Drawing.Point(798, 73);
            this.label52.Name = "label52";
            this.label52.Size = new System.Drawing.Size(101, 16);
            this.label52.TabIndex = 582;
            this.label52.Text = "Prix cond. fourn.";
            // 
            // txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR
            // 
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.AccAllowComma = false;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.AccHidenValue = "";
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.AccNotAllowedChars = null;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.AccReadOnly = false;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.AccRequired = false;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.Location = new System.Drawing.Point(1002, 41);
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.MaxLength = 32767;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.Multiline = false;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.Name = "txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR";
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.ReadOnly = false;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.TabIndex = 581;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.UseSystemPasswordChar = false;
            this.txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR.Visible = false;
            // 
            // label53
            // 
            this.label53.AutoSize = true;
            this.label53.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label53.Location = new System.Drawing.Point(798, 41);
            this.label53.Name = "label53";
            this.label53.Size = new System.Drawing.Size(94, 16);
            this.label53.TabIndex = 580;
            this.label53.Text = "Unité prix distr.";
            this.label53.Visible = false;
            // 
            // txtPRIX_DISTRIBUTEUR
            // 
            this.txtPRIX_DISTRIBUTEUR.AccAcceptNumbersOnly = false;
            this.txtPRIX_DISTRIBUTEUR.AccAllowComma = false;
            this.txtPRIX_DISTRIBUTEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRIX_DISTRIBUTEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRIX_DISTRIBUTEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtPRIX_DISTRIBUTEUR.AccHidenValue = "";
            this.txtPRIX_DISTRIBUTEUR.AccNotAllowedChars = null;
            this.txtPRIX_DISTRIBUTEUR.AccReadOnly = false;
            this.txtPRIX_DISTRIBUTEUR.AccReadOnlyAllowDelete = false;
            this.txtPRIX_DISTRIBUTEUR.AccRequired = false;
            this.txtPRIX_DISTRIBUTEUR.BackColor = System.Drawing.Color.White;
            this.txtPRIX_DISTRIBUTEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtPRIX_DISTRIBUTEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtPRIX_DISTRIBUTEUR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtPRIX_DISTRIBUTEUR.Location = new System.Drawing.Point(594, 537);
            this.txtPRIX_DISTRIBUTEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRIX_DISTRIBUTEUR.MaxLength = 32767;
            this.txtPRIX_DISTRIBUTEUR.Multiline = false;
            this.txtPRIX_DISTRIBUTEUR.Name = "txtPRIX_DISTRIBUTEUR";
            this.txtPRIX_DISTRIBUTEUR.ReadOnly = false;
            this.txtPRIX_DISTRIBUTEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRIX_DISTRIBUTEUR.Size = new System.Drawing.Size(187, 27);
            this.txtPRIX_DISTRIBUTEUR.TabIndex = 579;
            this.txtPRIX_DISTRIBUTEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRIX_DISTRIBUTEUR.UseSystemPasswordChar = false;
            // 
            // label54
            // 
            this.label54.AutoSize = true;
            this.label54.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label54.Location = new System.Drawing.Point(388, 548);
            this.label54.Name = "label54";
            this.label54.Size = new System.Drawing.Size(72, 16);
            this.label54.TabIndex = 578;
            this.label54.Text = "Prix distrib.";
            // 
            // txtQTE_COND_FOURNISSEUR
            // 
            this.txtQTE_COND_FOURNISSEUR.AccAcceptNumbersOnly = false;
            this.txtQTE_COND_FOURNISSEUR.AccAllowComma = false;
            this.txtQTE_COND_FOURNISSEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtQTE_COND_FOURNISSEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtQTE_COND_FOURNISSEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtQTE_COND_FOURNISSEUR.AccHidenValue = "";
            this.txtQTE_COND_FOURNISSEUR.AccNotAllowedChars = null;
            this.txtQTE_COND_FOURNISSEUR.AccReadOnly = false;
            this.txtQTE_COND_FOURNISSEUR.AccReadOnlyAllowDelete = false;
            this.txtQTE_COND_FOURNISSEUR.AccRequired = false;
            this.txtQTE_COND_FOURNISSEUR.BackColor = System.Drawing.Color.White;
            this.txtQTE_COND_FOURNISSEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtQTE_COND_FOURNISSEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtQTE_COND_FOURNISSEUR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtQTE_COND_FOURNISSEUR.Location = new System.Drawing.Point(594, 289);
            this.txtQTE_COND_FOURNISSEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtQTE_COND_FOURNISSEUR.MaxLength = 32767;
            this.txtQTE_COND_FOURNISSEUR.Multiline = false;
            this.txtQTE_COND_FOURNISSEUR.Name = "txtQTE_COND_FOURNISSEUR";
            this.txtQTE_COND_FOURNISSEUR.ReadOnly = false;
            this.txtQTE_COND_FOURNISSEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtQTE_COND_FOURNISSEUR.Size = new System.Drawing.Size(187, 27);
            this.txtQTE_COND_FOURNISSEUR.TabIndex = 577;
            this.txtQTE_COND_FOURNISSEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtQTE_COND_FOURNISSEUR.UseSystemPasswordChar = false;
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label38.Location = new System.Drawing.Point(388, 297);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(100, 16);
            this.label38.TabIndex = 576;
            this.label38.Text = "Qte cond. fourn.";
            // 
            // txtLIBELLE_COND_FOURNISSEUR
            // 
            this.txtLIBELLE_COND_FOURNISSEUR.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_COND_FOURNISSEUR.AccAllowComma = false;
            this.txtLIBELLE_COND_FOURNISSEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_COND_FOURNISSEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_COND_FOURNISSEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_COND_FOURNISSEUR.AccHidenValue = "";
            this.txtLIBELLE_COND_FOURNISSEUR.AccNotAllowedChars = null;
            this.txtLIBELLE_COND_FOURNISSEUR.AccReadOnly = false;
            this.txtLIBELLE_COND_FOURNISSEUR.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_COND_FOURNISSEUR.AccRequired = false;
            this.txtLIBELLE_COND_FOURNISSEUR.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_COND_FOURNISSEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_COND_FOURNISSEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_COND_FOURNISSEUR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_COND_FOURNISSEUR.Location = new System.Drawing.Point(594, 258);
            this.txtLIBELLE_COND_FOURNISSEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_COND_FOURNISSEUR.MaxLength = 32767;
            this.txtLIBELLE_COND_FOURNISSEUR.Multiline = false;
            this.txtLIBELLE_COND_FOURNISSEUR.Name = "txtLIBELLE_COND_FOURNISSEUR";
            this.txtLIBELLE_COND_FOURNISSEUR.ReadOnly = false;
            this.txtLIBELLE_COND_FOURNISSEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_COND_FOURNISSEUR.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_COND_FOURNISSEUR.TabIndex = 575;
            this.txtLIBELLE_COND_FOURNISSEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_COND_FOURNISSEUR.UseSystemPasswordChar = false;
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label39.Location = new System.Drawing.Point(388, 264);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(109, 16);
            this.label39.TabIndex = 574;
            this.label39.Text = "cond. fournisseur";
            // 
            // txtQTE_UNITE_METIER
            // 
            this.txtQTE_UNITE_METIER.AccAcceptNumbersOnly = false;
            this.txtQTE_UNITE_METIER.AccAllowComma = false;
            this.txtQTE_UNITE_METIER.AccBackgroundColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_METIER.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtQTE_UNITE_METIER.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtQTE_UNITE_METIER.AccHidenValue = "";
            this.txtQTE_UNITE_METIER.AccNotAllowedChars = null;
            this.txtQTE_UNITE_METIER.AccReadOnly = false;
            this.txtQTE_UNITE_METIER.AccReadOnlyAllowDelete = false;
            this.txtQTE_UNITE_METIER.AccRequired = false;
            this.txtQTE_UNITE_METIER.BackColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_METIER.CustomBackColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_METIER.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtQTE_UNITE_METIER.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtQTE_UNITE_METIER.Location = new System.Drawing.Point(594, 227);
            this.txtQTE_UNITE_METIER.Margin = new System.Windows.Forms.Padding(2);
            this.txtQTE_UNITE_METIER.MaxLength = 32767;
            this.txtQTE_UNITE_METIER.Multiline = false;
            this.txtQTE_UNITE_METIER.Name = "txtQTE_UNITE_METIER";
            this.txtQTE_UNITE_METIER.ReadOnly = false;
            this.txtQTE_UNITE_METIER.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtQTE_UNITE_METIER.Size = new System.Drawing.Size(187, 27);
            this.txtQTE_UNITE_METIER.TabIndex = 573;
            this.txtQTE_UNITE_METIER.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtQTE_UNITE_METIER.UseSystemPasswordChar = false;
            // 
            // label40
            // 
            this.label40.AutoSize = true;
            this.label40.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label40.Location = new System.Drawing.Point(388, 234);
            this.label40.Name = "label40";
            this.label40.Size = new System.Drawing.Size(92, 16);
            this.label40.TabIndex = 572;
            this.label40.Text = "Qte unt. métier";
            // 
            // txtLIBELLE_UNITE_METIER
            // 
            this.txtLIBELLE_UNITE_METIER.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_UNITE_METIER.AccAllowComma = false;
            this.txtLIBELLE_UNITE_METIER.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_METIER.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_UNITE_METIER.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_UNITE_METIER.AccHidenValue = "";
            this.txtLIBELLE_UNITE_METIER.AccNotAllowedChars = null;
            this.txtLIBELLE_UNITE_METIER.AccReadOnly = false;
            this.txtLIBELLE_UNITE_METIER.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_UNITE_METIER.AccRequired = false;
            this.txtLIBELLE_UNITE_METIER.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_METIER.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_METIER.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_UNITE_METIER.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_UNITE_METIER.Location = new System.Drawing.Point(594, 196);
            this.txtLIBELLE_UNITE_METIER.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_UNITE_METIER.MaxLength = 32767;
            this.txtLIBELLE_UNITE_METIER.Multiline = false;
            this.txtLIBELLE_UNITE_METIER.Name = "txtLIBELLE_UNITE_METIER";
            this.txtLIBELLE_UNITE_METIER.ReadOnly = false;
            this.txtLIBELLE_UNITE_METIER.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_UNITE_METIER.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_UNITE_METIER.TabIndex = 571;
            this.txtLIBELLE_UNITE_METIER.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_UNITE_METIER.UseSystemPasswordChar = false;
            // 
            // label41
            // 
            this.label41.AutoSize = true;
            this.label41.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label41.Location = new System.Drawing.Point(388, 202);
            this.label41.Name = "label41";
            this.label41.Size = new System.Drawing.Size(79, 16);
            this.label41.TabIndex = 570;
            this.label41.Text = "Unité métier";
            // 
            // txtQTE_UNITE_DEVIS
            // 
            this.txtQTE_UNITE_DEVIS.AccAcceptNumbersOnly = false;
            this.txtQTE_UNITE_DEVIS.AccAllowComma = false;
            this.txtQTE_UNITE_DEVIS.AccBackgroundColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_DEVIS.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtQTE_UNITE_DEVIS.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtQTE_UNITE_DEVIS.AccHidenValue = "";
            this.txtQTE_UNITE_DEVIS.AccNotAllowedChars = null;
            this.txtQTE_UNITE_DEVIS.AccReadOnly = false;
            this.txtQTE_UNITE_DEVIS.AccReadOnlyAllowDelete = false;
            this.txtQTE_UNITE_DEVIS.AccRequired = false;
            this.txtQTE_UNITE_DEVIS.BackColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_DEVIS.CustomBackColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_DEVIS.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtQTE_UNITE_DEVIS.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtQTE_UNITE_DEVIS.Location = new System.Drawing.Point(594, 165);
            this.txtQTE_UNITE_DEVIS.Margin = new System.Windows.Forms.Padding(2);
            this.txtQTE_UNITE_DEVIS.MaxLength = 32767;
            this.txtQTE_UNITE_DEVIS.Multiline = false;
            this.txtQTE_UNITE_DEVIS.Name = "txtQTE_UNITE_DEVIS";
            this.txtQTE_UNITE_DEVIS.ReadOnly = false;
            this.txtQTE_UNITE_DEVIS.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtQTE_UNITE_DEVIS.Size = new System.Drawing.Size(187, 27);
            this.txtQTE_UNITE_DEVIS.TabIndex = 569;
            this.txtQTE_UNITE_DEVIS.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtQTE_UNITE_DEVIS.UseSystemPasswordChar = false;
            // 
            // label42
            // 
            this.label42.AutoSize = true;
            this.label42.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label42.Location = new System.Drawing.Point(388, 171);
            this.label42.Name = "label42";
            this.label42.Size = new System.Drawing.Size(88, 16);
            this.label42.TabIndex = 568;
            this.label42.Text = "Qte unt. devis";
            // 
            // txtLIBELLE_UNITE_DEVIS_2
            // 
            this.txtLIBELLE_UNITE_DEVIS_2.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_UNITE_DEVIS_2.AccAllowComma = false;
            this.txtLIBELLE_UNITE_DEVIS_2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_DEVIS_2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_UNITE_DEVIS_2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_UNITE_DEVIS_2.AccHidenValue = "";
            this.txtLIBELLE_UNITE_DEVIS_2.AccNotAllowedChars = null;
            this.txtLIBELLE_UNITE_DEVIS_2.AccReadOnly = false;
            this.txtLIBELLE_UNITE_DEVIS_2.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_UNITE_DEVIS_2.AccRequired = false;
            this.txtLIBELLE_UNITE_DEVIS_2.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_DEVIS_2.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_DEVIS_2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_UNITE_DEVIS_2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_UNITE_DEVIS_2.Location = new System.Drawing.Point(594, 134);
            this.txtLIBELLE_UNITE_DEVIS_2.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_UNITE_DEVIS_2.MaxLength = 32767;
            this.txtLIBELLE_UNITE_DEVIS_2.Multiline = false;
            this.txtLIBELLE_UNITE_DEVIS_2.Name = "txtLIBELLE_UNITE_DEVIS_2";
            this.txtLIBELLE_UNITE_DEVIS_2.ReadOnly = false;
            this.txtLIBELLE_UNITE_DEVIS_2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_UNITE_DEVIS_2.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_UNITE_DEVIS_2.TabIndex = 567;
            this.txtLIBELLE_UNITE_DEVIS_2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_UNITE_DEVIS_2.UseSystemPasswordChar = false;
            // 
            // label43
            // 
            this.label43.AutoSize = true;
            this.label43.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label43.Location = new System.Drawing.Point(388, 140);
            this.label43.Name = "label43";
            this.label43.Size = new System.Drawing.Size(94, 16);
            this.label43.TabIndex = 566;
            this.label43.Text = "Unité de devis";
            // 
            // txtQTE_UNITE_STOCK
            // 
            this.txtQTE_UNITE_STOCK.AccAcceptNumbersOnly = false;
            this.txtQTE_UNITE_STOCK.AccAllowComma = false;
            this.txtQTE_UNITE_STOCK.AccBackgroundColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_STOCK.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtQTE_UNITE_STOCK.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtQTE_UNITE_STOCK.AccHidenValue = "";
            this.txtQTE_UNITE_STOCK.AccNotAllowedChars = null;
            this.txtQTE_UNITE_STOCK.AccReadOnly = false;
            this.txtQTE_UNITE_STOCK.AccReadOnlyAllowDelete = false;
            this.txtQTE_UNITE_STOCK.AccRequired = false;
            this.txtQTE_UNITE_STOCK.BackColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_STOCK.CustomBackColor = System.Drawing.Color.White;
            this.txtQTE_UNITE_STOCK.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtQTE_UNITE_STOCK.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtQTE_UNITE_STOCK.Location = new System.Drawing.Point(594, 103);
            this.txtQTE_UNITE_STOCK.Margin = new System.Windows.Forms.Padding(2);
            this.txtQTE_UNITE_STOCK.MaxLength = 32767;
            this.txtQTE_UNITE_STOCK.Multiline = false;
            this.txtQTE_UNITE_STOCK.Name = "txtQTE_UNITE_STOCK";
            this.txtQTE_UNITE_STOCK.ReadOnly = false;
            this.txtQTE_UNITE_STOCK.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtQTE_UNITE_STOCK.Size = new System.Drawing.Size(187, 27);
            this.txtQTE_UNITE_STOCK.TabIndex = 565;
            this.txtQTE_UNITE_STOCK.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtQTE_UNITE_STOCK.UseSystemPasswordChar = false;
            // 
            // label44
            // 
            this.label44.AutoSize = true;
            this.label44.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label44.Location = new System.Drawing.Point(388, 106);
            this.label44.Name = "label44";
            this.label44.Size = new System.Drawing.Size(95, 16);
            this.label44.TabIndex = 564;
            this.label44.Text = "Qte unbt. stock";
            // 
            // txtLIBELLE_UNITE_STOCK
            // 
            this.txtLIBELLE_UNITE_STOCK.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_UNITE_STOCK.AccAllowComma = false;
            this.txtLIBELLE_UNITE_STOCK.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_STOCK.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_UNITE_STOCK.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_UNITE_STOCK.AccHidenValue = "";
            this.txtLIBELLE_UNITE_STOCK.AccNotAllowedChars = null;
            this.txtLIBELLE_UNITE_STOCK.AccReadOnly = false;
            this.txtLIBELLE_UNITE_STOCK.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_UNITE_STOCK.AccRequired = false;
            this.txtLIBELLE_UNITE_STOCK.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_STOCK.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_UNITE_STOCK.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_UNITE_STOCK.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_UNITE_STOCK.Location = new System.Drawing.Point(594, 72);
            this.txtLIBELLE_UNITE_STOCK.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_UNITE_STOCK.MaxLength = 32767;
            this.txtLIBELLE_UNITE_STOCK.Multiline = false;
            this.txtLIBELLE_UNITE_STOCK.Name = "txtLIBELLE_UNITE_STOCK";
            this.txtLIBELLE_UNITE_STOCK.ReadOnly = false;
            this.txtLIBELLE_UNITE_STOCK.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_UNITE_STOCK.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_UNITE_STOCK.TabIndex = 563;
            this.txtLIBELLE_UNITE_STOCK.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_UNITE_STOCK.UseSystemPasswordChar = false;
            // 
            // label45
            // 
            this.label45.AutoSize = true;
            this.label45.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label45.Location = new System.Drawing.Point(388, 73);
            this.label45.Name = "label45";
            this.label45.Size = new System.Drawing.Size(93, 16);
            this.label45.TabIndex = 562;
            this.label45.Text = "Unité de stock";
            // 
            // txtQTE_COND_STOCK
            // 
            this.txtQTE_COND_STOCK.AccAcceptNumbersOnly = false;
            this.txtQTE_COND_STOCK.AccAllowComma = false;
            this.txtQTE_COND_STOCK.AccBackgroundColor = System.Drawing.Color.White;
            this.txtQTE_COND_STOCK.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtQTE_COND_STOCK.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtQTE_COND_STOCK.AccHidenValue = "";
            this.txtQTE_COND_STOCK.AccNotAllowedChars = null;
            this.txtQTE_COND_STOCK.AccReadOnly = false;
            this.txtQTE_COND_STOCK.AccReadOnlyAllowDelete = false;
            this.txtQTE_COND_STOCK.AccRequired = false;
            this.txtQTE_COND_STOCK.BackColor = System.Drawing.Color.White;
            this.txtQTE_COND_STOCK.CustomBackColor = System.Drawing.Color.White;
            this.txtQTE_COND_STOCK.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtQTE_COND_STOCK.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtQTE_COND_STOCK.Location = new System.Drawing.Point(594, 41);
            this.txtQTE_COND_STOCK.Margin = new System.Windows.Forms.Padding(2);
            this.txtQTE_COND_STOCK.MaxLength = 32767;
            this.txtQTE_COND_STOCK.Multiline = false;
            this.txtQTE_COND_STOCK.Name = "txtQTE_COND_STOCK";
            this.txtQTE_COND_STOCK.ReadOnly = false;
            this.txtQTE_COND_STOCK.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtQTE_COND_STOCK.Size = new System.Drawing.Size(187, 27);
            this.txtQTE_COND_STOCK.TabIndex = 561;
            this.txtQTE_COND_STOCK.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtQTE_COND_STOCK.UseSystemPasswordChar = false;
            // 
            // label46
            // 
            this.label46.AutoSize = true;
            this.label46.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label46.Location = new System.Drawing.Point(388, 41);
            this.label46.Name = "label46";
            this.label46.Size = new System.Drawing.Size(100, 16);
            this.label46.TabIndex = 560;
            this.label46.Text = "Qte cond. stock";
            // 
            // txtLIBELLE_COND_STOCK
            // 
            this.txtLIBELLE_COND_STOCK.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_COND_STOCK.AccAllowComma = false;
            this.txtLIBELLE_COND_STOCK.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_COND_STOCK.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_COND_STOCK.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_COND_STOCK.AccHidenValue = "";
            this.txtLIBELLE_COND_STOCK.AccNotAllowedChars = null;
            this.txtLIBELLE_COND_STOCK.AccReadOnly = false;
            this.txtLIBELLE_COND_STOCK.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_COND_STOCK.AccRequired = false;
            this.txtLIBELLE_COND_STOCK.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_COND_STOCK.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_COND_STOCK.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_COND_STOCK.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_COND_STOCK.Location = new System.Drawing.Point(594, 10);
            this.txtLIBELLE_COND_STOCK.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_COND_STOCK.MaxLength = 32767;
            this.txtLIBELLE_COND_STOCK.Multiline = false;
            this.txtLIBELLE_COND_STOCK.Name = "txtLIBELLE_COND_STOCK";
            this.txtLIBELLE_COND_STOCK.ReadOnly = false;
            this.txtLIBELLE_COND_STOCK.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_COND_STOCK.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_COND_STOCK.TabIndex = 559;
            this.txtLIBELLE_COND_STOCK.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_COND_STOCK.UseSystemPasswordChar = false;
            // 
            // label47
            // 
            this.label47.AutoSize = true;
            this.label47.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label47.Location = new System.Drawing.Point(388, 10);
            this.label47.Name = "label47";
            this.label47.Size = new System.Drawing.Size(78, 16);
            this.label47.TabIndex = 558;
            this.label47.Text = "Cond. stock";
            // 
            // txtREF_FOURNISSEUR
            // 
            this.txtREF_FOURNISSEUR.AccAcceptNumbersOnly = false;
            this.txtREF_FOURNISSEUR.AccAllowComma = false;
            this.txtREF_FOURNISSEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtREF_FOURNISSEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtREF_FOURNISSEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtREF_FOURNISSEUR.AccHidenValue = "";
            this.txtREF_FOURNISSEUR.AccNotAllowedChars = null;
            this.txtREF_FOURNISSEUR.AccReadOnly = false;
            this.txtREF_FOURNISSEUR.AccReadOnlyAllowDelete = false;
            this.txtREF_FOURNISSEUR.AccRequired = false;
            this.txtREF_FOURNISSEUR.BackColor = System.Drawing.Color.White;
            this.txtREF_FOURNISSEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtREF_FOURNISSEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtREF_FOURNISSEUR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtREF_FOURNISSEUR.Location = new System.Drawing.Point(171, 289);
            this.txtREF_FOURNISSEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtREF_FOURNISSEUR.MaxLength = 32767;
            this.txtREF_FOURNISSEUR.Multiline = false;
            this.txtREF_FOURNISSEUR.Name = "txtREF_FOURNISSEUR";
            this.txtREF_FOURNISSEUR.ReadOnly = false;
            this.txtREF_FOURNISSEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtREF_FOURNISSEUR.Size = new System.Drawing.Size(187, 27);
            this.txtREF_FOURNISSEUR.TabIndex = 557;
            this.txtREF_FOURNISSEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtREF_FOURNISSEUR.UseSystemPasswordChar = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(10, 291);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(67, 16);
            this.label27.TabIndex = 556;
            this.label27.Text = "Réf. fourn.";
            // 
            // txtREF_FABRICANT
            // 
            this.txtREF_FABRICANT.AccAcceptNumbersOnly = false;
            this.txtREF_FABRICANT.AccAllowComma = false;
            this.txtREF_FABRICANT.AccBackgroundColor = System.Drawing.Color.White;
            this.txtREF_FABRICANT.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtREF_FABRICANT.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtREF_FABRICANT.AccHidenValue = "";
            this.txtREF_FABRICANT.AccNotAllowedChars = null;
            this.txtREF_FABRICANT.AccReadOnly = false;
            this.txtREF_FABRICANT.AccReadOnlyAllowDelete = false;
            this.txtREF_FABRICANT.AccRequired = false;
            this.txtREF_FABRICANT.BackColor = System.Drawing.Color.White;
            this.txtREF_FABRICANT.CustomBackColor = System.Drawing.Color.White;
            this.txtREF_FABRICANT.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtREF_FABRICANT.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtREF_FABRICANT.Location = new System.Drawing.Point(171, 258);
            this.txtREF_FABRICANT.Margin = new System.Windows.Forms.Padding(2);
            this.txtREF_FABRICANT.MaxLength = 32767;
            this.txtREF_FABRICANT.Multiline = false;
            this.txtREF_FABRICANT.Name = "txtREF_FABRICANT";
            this.txtREF_FABRICANT.ReadOnly = false;
            this.txtREF_FABRICANT.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtREF_FABRICANT.Size = new System.Drawing.Size(187, 27);
            this.txtREF_FABRICANT.TabIndex = 555;
            this.txtREF_FABRICANT.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtREF_FABRICANT.UseSystemPasswordChar = false;
            // 
            // label28
            // 
            this.label28.AutoSize = true;
            this.label28.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label28.Location = new System.Drawing.Point(10, 258);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(86, 16);
            this.label28.TabIndex = 554;
            this.label28.Text = "Réf. fabricant";
            // 
            // txtLIBELLE_MARQUE
            // 
            this.txtLIBELLE_MARQUE.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_MARQUE.AccAllowComma = false;
            this.txtLIBELLE_MARQUE.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_MARQUE.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_MARQUE.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_MARQUE.AccHidenValue = "";
            this.txtLIBELLE_MARQUE.AccNotAllowedChars = null;
            this.txtLIBELLE_MARQUE.AccReadOnly = false;
            this.txtLIBELLE_MARQUE.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_MARQUE.AccRequired = false;
            this.txtLIBELLE_MARQUE.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_MARQUE.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_MARQUE.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_MARQUE.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_MARQUE.Location = new System.Drawing.Point(171, 227);
            this.txtLIBELLE_MARQUE.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_MARQUE.MaxLength = 32767;
            this.txtLIBELLE_MARQUE.Multiline = false;
            this.txtLIBELLE_MARQUE.Name = "txtLIBELLE_MARQUE";
            this.txtLIBELLE_MARQUE.ReadOnly = false;
            this.txtLIBELLE_MARQUE.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_MARQUE.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_MARQUE.TabIndex = 553;
            this.txtLIBELLE_MARQUE.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_MARQUE.UseSystemPasswordChar = false;
            // 
            // label29
            // 
            this.label29.AutoSize = true;
            this.label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label29.Location = new System.Drawing.Point(10, 227);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(54, 16);
            this.label29.TabIndex = 552;
            this.label29.Text = "Marque";
            // 
            // txtDATE_APPLICATION
            // 
            this.txtDATE_APPLICATION.AccAcceptNumbersOnly = false;
            this.txtDATE_APPLICATION.AccAllowComma = false;
            this.txtDATE_APPLICATION.AccBackgroundColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDATE_APPLICATION.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDATE_APPLICATION.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtDATE_APPLICATION.AccHidenValue = "";
            this.txtDATE_APPLICATION.AccNotAllowedChars = null;
            this.txtDATE_APPLICATION.AccReadOnly = false;
            this.txtDATE_APPLICATION.AccReadOnlyAllowDelete = false;
            this.txtDATE_APPLICATION.AccRequired = false;
            this.txtDATE_APPLICATION.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDATE_APPLICATION.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(192)))));
            this.txtDATE_APPLICATION.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDATE_APPLICATION.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtDATE_APPLICATION.Location = new System.Drawing.Point(171, 196);
            this.txtDATE_APPLICATION.Margin = new System.Windows.Forms.Padding(2);
            this.txtDATE_APPLICATION.MaxLength = 32767;
            this.txtDATE_APPLICATION.Multiline = false;
            this.txtDATE_APPLICATION.Name = "txtDATE_APPLICATION";
            this.txtDATE_APPLICATION.ReadOnly = false;
            this.txtDATE_APPLICATION.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDATE_APPLICATION.Size = new System.Drawing.Size(187, 27);
            this.txtDATE_APPLICATION.TabIndex = 551;
            this.txtDATE_APPLICATION.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDATE_APPLICATION.UseSystemPasswordChar = false;
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label30.Location = new System.Drawing.Point(10, 200);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(117, 16);
            this.label30.TabIndex = 550;
            this.label30.Text = "Date d\'application";
            // 
            // txtLIBELLE_TECHNIQUE_2
            // 
            this.txtLIBELLE_TECHNIQUE_2.AccAcceptNumbersOnly = false;
            this.txtLIBELLE_TECHNIQUE_2.AccAllowComma = false;
            this.txtLIBELLE_TECHNIQUE_2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLIBELLE_TECHNIQUE_2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLIBELLE_TECHNIQUE_2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtLIBELLE_TECHNIQUE_2.AccHidenValue = "";
            this.txtLIBELLE_TECHNIQUE_2.AccNotAllowedChars = null;
            this.txtLIBELLE_TECHNIQUE_2.AccReadOnly = false;
            this.txtLIBELLE_TECHNIQUE_2.AccReadOnlyAllowDelete = false;
            this.txtLIBELLE_TECHNIQUE_2.AccRequired = false;
            this.txtLIBELLE_TECHNIQUE_2.BackColor = System.Drawing.Color.White;
            this.txtLIBELLE_TECHNIQUE_2.CustomBackColor = System.Drawing.Color.White;
            this.txtLIBELLE_TECHNIQUE_2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtLIBELLE_TECHNIQUE_2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtLIBELLE_TECHNIQUE_2.Location = new System.Drawing.Point(171, 165);
            this.txtLIBELLE_TECHNIQUE_2.Margin = new System.Windows.Forms.Padding(2);
            this.txtLIBELLE_TECHNIQUE_2.MaxLength = 32767;
            this.txtLIBELLE_TECHNIQUE_2.Multiline = false;
            this.txtLIBELLE_TECHNIQUE_2.Name = "txtLIBELLE_TECHNIQUE_2";
            this.txtLIBELLE_TECHNIQUE_2.ReadOnly = false;
            this.txtLIBELLE_TECHNIQUE_2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLIBELLE_TECHNIQUE_2.Size = new System.Drawing.Size(187, 27);
            this.txtLIBELLE_TECHNIQUE_2.TabIndex = 549;
            this.txtLIBELLE_TECHNIQUE_2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLIBELLE_TECHNIQUE_2.UseSystemPasswordChar = false;
            this.txtLIBELLE_TECHNIQUE_2.DoubleClick += new System.EventHandler(this.txtLIBELLE_TECHNIQUE_2_DoubleClick);
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label31.Location = new System.Drawing.Point(10, 171);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(109, 16);
            this.label31.TabIndex = 548;
            this.label31.Text = "Libelle technique";
            // 
            // txtDESIGNATION_ARTICLE_2
            // 
            this.txtDESIGNATION_ARTICLE_2.AccAcceptNumbersOnly = false;
            this.txtDESIGNATION_ARTICLE_2.AccAllowComma = false;
            this.txtDESIGNATION_ARTICLE_2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDESIGNATION_ARTICLE_2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDESIGNATION_ARTICLE_2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtDESIGNATION_ARTICLE_2.AccHidenValue = "";
            this.txtDESIGNATION_ARTICLE_2.AccNotAllowedChars = null;
            this.txtDESIGNATION_ARTICLE_2.AccReadOnly = false;
            this.txtDESIGNATION_ARTICLE_2.AccReadOnlyAllowDelete = false;
            this.txtDESIGNATION_ARTICLE_2.AccRequired = false;
            this.txtDESIGNATION_ARTICLE_2.BackColor = System.Drawing.Color.White;
            this.txtDESIGNATION_ARTICLE_2.CustomBackColor = System.Drawing.Color.White;
            this.txtDESIGNATION_ARTICLE_2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDESIGNATION_ARTICLE_2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtDESIGNATION_ARTICLE_2.Location = new System.Drawing.Point(171, 134);
            this.txtDESIGNATION_ARTICLE_2.Margin = new System.Windows.Forms.Padding(2);
            this.txtDESIGNATION_ARTICLE_2.MaxLength = 32767;
            this.txtDESIGNATION_ARTICLE_2.Multiline = false;
            this.txtDESIGNATION_ARTICLE_2.Name = "txtDESIGNATION_ARTICLE_2";
            this.txtDESIGNATION_ARTICLE_2.ReadOnly = false;
            this.txtDESIGNATION_ARTICLE_2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDESIGNATION_ARTICLE_2.Size = new System.Drawing.Size(187, 27);
            this.txtDESIGNATION_ARTICLE_2.TabIndex = 547;
            this.txtDESIGNATION_ARTICLE_2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDESIGNATION_ARTICLE_2.UseSystemPasswordChar = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label32.Location = new System.Drawing.Point(10, 136);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(119, 16);
            this.label32.TabIndex = 546;
            this.label32.Text = "Désignation article";
            // 
            // txtCHRONO_INITIAL_2
            // 
            this.txtCHRONO_INITIAL_2.AccAcceptNumbersOnly = false;
            this.txtCHRONO_INITIAL_2.AccAllowComma = false;
            this.txtCHRONO_INITIAL_2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCHRONO_INITIAL_2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCHRONO_INITIAL_2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCHRONO_INITIAL_2.AccHidenValue = "";
            this.txtCHRONO_INITIAL_2.AccNotAllowedChars = null;
            this.txtCHRONO_INITIAL_2.AccReadOnly = false;
            this.txtCHRONO_INITIAL_2.AccReadOnlyAllowDelete = false;
            this.txtCHRONO_INITIAL_2.AccRequired = false;
            this.txtCHRONO_INITIAL_2.BackColor = System.Drawing.Color.White;
            this.txtCHRONO_INITIAL_2.CustomBackColor = System.Drawing.Color.White;
            this.txtCHRONO_INITIAL_2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCHRONO_INITIAL_2.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtCHRONO_INITIAL_2.Location = new System.Drawing.Point(171, 103);
            this.txtCHRONO_INITIAL_2.Margin = new System.Windows.Forms.Padding(2);
            this.txtCHRONO_INITIAL_2.MaxLength = 32767;
            this.txtCHRONO_INITIAL_2.Multiline = false;
            this.txtCHRONO_INITIAL_2.Name = "txtCHRONO_INITIAL_2";
            this.txtCHRONO_INITIAL_2.ReadOnly = false;
            this.txtCHRONO_INITIAL_2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCHRONO_INITIAL_2.Size = new System.Drawing.Size(187, 27);
            this.txtCHRONO_INITIAL_2.TabIndex = 545;
            this.txtCHRONO_INITIAL_2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCHRONO_INITIAL_2.UseSystemPasswordChar = false;
            // 
            // label34
            // 
            this.label34.AutoSize = true;
            this.label34.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label34.Location = new System.Drawing.Point(10, 103);
            this.label34.Name = "label34";
            this.label34.Size = new System.Drawing.Size(51, 16);
            this.label34.TabIndex = 544;
            this.label34.Text = "Chrono";
            // 
            // txtNOM_FOURNISSEUR
            // 
            this.txtNOM_FOURNISSEUR.AccAcceptNumbersOnly = false;
            this.txtNOM_FOURNISSEUR.AccAllowComma = false;
            this.txtNOM_FOURNISSEUR.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNOM_FOURNISSEUR.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNOM_FOURNISSEUR.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtNOM_FOURNISSEUR.AccHidenValue = "";
            this.txtNOM_FOURNISSEUR.AccNotAllowedChars = null;
            this.txtNOM_FOURNISSEUR.AccReadOnly = false;
            this.txtNOM_FOURNISSEUR.AccReadOnlyAllowDelete = false;
            this.txtNOM_FOURNISSEUR.AccRequired = false;
            this.txtNOM_FOURNISSEUR.BackColor = System.Drawing.Color.White;
            this.txtNOM_FOURNISSEUR.CustomBackColor = System.Drawing.Color.White;
            this.txtNOM_FOURNISSEUR.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNOM_FOURNISSEUR.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtNOM_FOURNISSEUR.Location = new System.Drawing.Point(171, 72);
            this.txtNOM_FOURNISSEUR.Margin = new System.Windows.Forms.Padding(2);
            this.txtNOM_FOURNISSEUR.MaxLength = 32767;
            this.txtNOM_FOURNISSEUR.Multiline = false;
            this.txtNOM_FOURNISSEUR.Name = "txtNOM_FOURNISSEUR";
            this.txtNOM_FOURNISSEUR.ReadOnly = false;
            this.txtNOM_FOURNISSEUR.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNOM_FOURNISSEUR.Size = new System.Drawing.Size(187, 27);
            this.txtNOM_FOURNISSEUR.TabIndex = 543;
            this.txtNOM_FOURNISSEUR.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNOM_FOURNISSEUR.UseSystemPasswordChar = false;
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label35.Location = new System.Drawing.Point(10, 73);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(72, 16);
            this.label35.TabIndex = 542;
            this.label35.Text = "Nom fourn.";
            // 
            // txtCODE_FOURNISSEUR_ADHERENT
            // 
            this.txtCODE_FOURNISSEUR_ADHERENT.AccAcceptNumbersOnly = false;
            this.txtCODE_FOURNISSEUR_ADHERENT.AccAllowComma = false;
            this.txtCODE_FOURNISSEUR_ADHERENT.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCODE_FOURNISSEUR_ADHERENT.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCODE_FOURNISSEUR_ADHERENT.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCODE_FOURNISSEUR_ADHERENT.AccHidenValue = "";
            this.txtCODE_FOURNISSEUR_ADHERENT.AccNotAllowedChars = null;
            this.txtCODE_FOURNISSEUR_ADHERENT.AccReadOnly = false;
            this.txtCODE_FOURNISSEUR_ADHERENT.AccReadOnlyAllowDelete = false;
            this.txtCODE_FOURNISSEUR_ADHERENT.AccRequired = false;
            this.txtCODE_FOURNISSEUR_ADHERENT.BackColor = System.Drawing.Color.White;
            this.txtCODE_FOURNISSEUR_ADHERENT.CustomBackColor = System.Drawing.Color.White;
            this.txtCODE_FOURNISSEUR_ADHERENT.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCODE_FOURNISSEUR_ADHERENT.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtCODE_FOURNISSEUR_ADHERENT.Location = new System.Drawing.Point(171, 41);
            this.txtCODE_FOURNISSEUR_ADHERENT.Margin = new System.Windows.Forms.Padding(2);
            this.txtCODE_FOURNISSEUR_ADHERENT.MaxLength = 32767;
            this.txtCODE_FOURNISSEUR_ADHERENT.Multiline = false;
            this.txtCODE_FOURNISSEUR_ADHERENT.Name = "txtCODE_FOURNISSEUR_ADHERENT";
            this.txtCODE_FOURNISSEUR_ADHERENT.ReadOnly = false;
            this.txtCODE_FOURNISSEUR_ADHERENT.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCODE_FOURNISSEUR_ADHERENT.Size = new System.Drawing.Size(187, 27);
            this.txtCODE_FOURNISSEUR_ADHERENT.TabIndex = 541;
            this.txtCODE_FOURNISSEUR_ADHERENT.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCODE_FOURNISSEUR_ADHERENT.UseSystemPasswordChar = false;
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label36.Location = new System.Drawing.Point(10, 41);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(129, 16);
            this.label36.TabIndex = 540;
            this.label36.Text = "Code fourn adhérent";
            // 
            // txtIDGEC_LIGNE_PRIX
            // 
            this.txtIDGEC_LIGNE_PRIX.AccAcceptNumbersOnly = false;
            this.txtIDGEC_LIGNE_PRIX.AccAllowComma = false;
            this.txtIDGEC_LIGNE_PRIX.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIDGEC_LIGNE_PRIX.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIDGEC_LIGNE_PRIX.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtIDGEC_LIGNE_PRIX.AccHidenValue = "";
            this.txtIDGEC_LIGNE_PRIX.AccNotAllowedChars = null;
            this.txtIDGEC_LIGNE_PRIX.AccReadOnly = false;
            this.txtIDGEC_LIGNE_PRIX.AccReadOnlyAllowDelete = false;
            this.txtIDGEC_LIGNE_PRIX.AccRequired = false;
            this.txtIDGEC_LIGNE_PRIX.BackColor = System.Drawing.Color.White;
            this.txtIDGEC_LIGNE_PRIX.CustomBackColor = System.Drawing.Color.White;
            this.txtIDGEC_LIGNE_PRIX.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtIDGEC_LIGNE_PRIX.ForeColor = System.Drawing.Color.DodgerBlue;
            this.txtIDGEC_LIGNE_PRIX.Location = new System.Drawing.Point(171, 10);
            this.txtIDGEC_LIGNE_PRIX.Margin = new System.Windows.Forms.Padding(2);
            this.txtIDGEC_LIGNE_PRIX.MaxLength = 32767;
            this.txtIDGEC_LIGNE_PRIX.Multiline = false;
            this.txtIDGEC_LIGNE_PRIX.Name = "txtIDGEC_LIGNE_PRIX";
            this.txtIDGEC_LIGNE_PRIX.ReadOnly = false;
            this.txtIDGEC_LIGNE_PRIX.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIDGEC_LIGNE_PRIX.Size = new System.Drawing.Size(187, 27);
            this.txtIDGEC_LIGNE_PRIX.TabIndex = 539;
            this.txtIDGEC_LIGNE_PRIX.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIDGEC_LIGNE_PRIX.UseSystemPasswordChar = false;
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label37.Location = new System.Drawing.Point(10, 13);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(96, 16);
            this.label37.TabIndex = 538;
            this.label37.Text = "ID ligne de prix";
            // 
            // SSTab1
            // 
            this.SSTab1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl2);
            this.SSTab1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSTab1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.SSTab1.Location = new System.Drawing.Point(0, 0);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.SSTab1.Size = new System.Drawing.Size(1212, 653);
            this.SSTab1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Excel;
            this.SSTab1.TabIndex = 410;
            ultraTab13.TabPage = this.ultraTabPageControl1;
            ultraTab13.Text = "Article";
            ultraTab1.TabPage = this.ultraTabPageControl2;
            ultraTab1.Text = "Tarif";
            this.SSTab1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab13,
            ultraTab1});
            this.SSTab1.ViewStyle = Infragistics.Win.UltraWinTabControl.ViewStyle.Office2007;
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(1210, 631);
            // 
            // frmDetailArticleV3bis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(1212, 653);
            this.Controls.Add(this.SSTab1);
            this.Name = "frmDetailArticleV3bis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmDetailArticleV3bis";
            this.Load += new System.EventHandler(this.frmDetailArticleV3bis_Load);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridCond)).EndInit();
            this.ultraTabPageControl2.ResumeLayout(false);
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).EndInit();
            this.SSTab1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public Infragistics.Win.UltraWinTabControl.UltraTabControl SSTab1;
        public Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        public Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private System.Windows.Forms.GroupBox groupBox1;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_UNITE_PRIX_FABRICANT;
        public iTalk.iTalk_TextBox_Small2 txtPRIX_FABRICANT;
        public iTalk.iTalk_TextBox_Small2 txtQTE_UNITE_VALEUR;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_UNITE_VALEUR;
        public iTalk.iTalk_TextBox_Small2 txtDATE_CREATION;
        public iTalk.iTalk_TextBox_Small2 txtPRIX_DONNE_PAR;
        public iTalk.iTalk_TextBox_Small2 TXTLIBELLE_UNITE_PRIX_DEVIS;
        public iTalk.iTalk_TextBox_Small2 txtPRX_DEVIS_NET_UD;
        private System.Windows.Forms.Label label119;
        private System.Windows.Forms.Label label118;
        private System.Windows.Forms.Label label117;
        private System.Windows.Forms.Label label116;
        private System.Windows.Forms.Label label115;
        private System.Windows.Forms.Label label114;
        private System.Windows.Forms.Label label113;
        private System.Windows.Forms.Label label112;
        private System.Windows.Forms.Label label111;
        private System.Windows.Forms.Label label110;
        private System.Windows.Forms.Label label109;
        private System.Windows.Forms.Label label108;
        private System.Windows.Forms.Label label107;
        private System.Windows.Forms.Label label106;
        private System.Windows.Forms.Label label105;
        private System.Windows.Forms.Label label104;
        private System.Windows.Forms.Label label103;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_UNITE_PRIX_GECET_UNITE;
        public iTalk.iTalk_TextBox_Small2 txtPRIX_GECET_UNITE;
        public iTalk.iTalk_TextBox_Small2 txtPRIX_GECET;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_UNITE_PRIX_GECET;
        private System.Windows.Forms.Label label102;
        private System.Windows.Forms.Label label101;
        private System.Windows.Forms.Label label100;
        public iTalk.iTalk_TextBox_Small2 txtPRIX_PUBLIC_DETAILS;
        public iTalk.iTalk_TextBox_Small2 txtPRIX_NET_DETAILS;
        public iTalk.iTalk_TextBox_Small2 txtREF_HISTORIQUE;
        private System.Windows.Forms.Label label99;
        private System.Windows.Forms.Label label98;
        private System.Windows.Forms.Label label97;
        private System.Windows.Forms.Label label96;
        private System.Windows.Forms.Label label95;
        private System.Windows.Forms.Label label94;
        public iTalk.iTalk_TextBox_Small2 txtUS_SUR_UV;
        public iTalk.iTalk_TextBox_Small2 txtQTE_MIN_CMD_UC;
        public iTalk.iTalk_TextBox_Small2 txtQTE_UNITE_FOURNISSEUR;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_UNITE_FOURNISSEUR;
        public iTalk.iTalk_TextBox_Small2 txtNB_UC_DANS_UV;
        public iTalk.iTalk_TextBox_Small2 txtQTE_MIN_CMD_UCNF;
        public iTalk.iTalk_TextBox_Small2 txtUS_SUR_UC;
        public iTalk.iTalk_TextBox_Small2 TXTUD_SUR_US;
        public iTalk.iTalk_TextBox_Small2 txtUNCF_SUR_UC;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_UNITE_PRIX_DEVIS_MAJORE;
        public iTalk.iTalk_TextBox_Small2 txtPRIX_DEVIS_MAJORE;
        public iTalk.iTalk_TextBox_Small2 txtTAXE;
        private System.Windows.Forms.Label label56;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_COND_PRIX_COND_STOCK;
        private System.Windows.Forms.Label label55;
        public iTalk.iTalk_TextBox_Small2 txtPRIX_COND_STOCK;
        private System.Windows.Forms.Label label48;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_UNITE_PRIX_STOCK;
        private System.Windows.Forms.Label label49;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_COND_PRIX_COND_FOURNISSEUR;
        private System.Windows.Forms.Label label50;
        public iTalk.iTalk_TextBox_Small2 txtPRIX_STOCK;
        private System.Windows.Forms.Label label51;
        public iTalk.iTalk_TextBox_Small2 txtPRIX_COND_FOUR_2;
        private System.Windows.Forms.Label label52;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_UNITE_PRIX_DISTRIBUTEUR;
        private System.Windows.Forms.Label label53;
        public iTalk.iTalk_TextBox_Small2 txtPRIX_DISTRIBUTEUR;
        private System.Windows.Forms.Label label54;
        public iTalk.iTalk_TextBox_Small2 txtQTE_COND_FOURNISSEUR;
        private System.Windows.Forms.Label label38;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_COND_FOURNISSEUR;
        private System.Windows.Forms.Label label39;
        public iTalk.iTalk_TextBox_Small2 txtQTE_UNITE_METIER;
        private System.Windows.Forms.Label label40;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_UNITE_METIER;
        private System.Windows.Forms.Label label41;
        public iTalk.iTalk_TextBox_Small2 txtQTE_UNITE_DEVIS;
        private System.Windows.Forms.Label label42;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_UNITE_DEVIS_2;
        private System.Windows.Forms.Label label43;
        public iTalk.iTalk_TextBox_Small2 txtQTE_UNITE_STOCK;
        private System.Windows.Forms.Label label44;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_UNITE_STOCK;
        private System.Windows.Forms.Label label45;
        public iTalk.iTalk_TextBox_Small2 txtQTE_COND_STOCK;
        private System.Windows.Forms.Label label46;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_COND_STOCK;
        private System.Windows.Forms.Label label47;
        public iTalk.iTalk_TextBox_Small2 txtREF_FOURNISSEUR;
        private System.Windows.Forms.Label label27;
        public iTalk.iTalk_TextBox_Small2 txtREF_FABRICANT;
        private System.Windows.Forms.Label label28;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_MARQUE;
        private System.Windows.Forms.Label label29;
        public iTalk.iTalk_TextBox_Small2 txtDATE_APPLICATION;
        private System.Windows.Forms.Label label30;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_TECHNIQUE_2;
        private System.Windows.Forms.Label label31;
        public iTalk.iTalk_TextBox_Small2 txtDESIGNATION_ARTICLE_2;
        private System.Windows.Forms.Label label32;
        public iTalk.iTalk_TextBox_Small2 txtCHRONO_INITIAL_2;
        private System.Windows.Forms.Label label34;
        public iTalk.iTalk_TextBox_Small2 txtNOM_FOURNISSEUR;
        private System.Windows.Forms.Label label35;
        public iTalk.iTalk_TextBox_Small2 txtCODE_FOURNISSEUR_ADHERENT;
        private System.Windows.Forms.Label label36;
        public iTalk.iTalk_TextBox_Small2 txtIDGEC_LIGNE_PRIX;
        private System.Windows.Forms.Label label37;
        private System.Windows.Forms.GroupBox groupBox6;
        public iTalk.iTalk_TextBox_Small2 iTalk_TextBox_Small21;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.Label label93;
        public iTalk.iTalk_TextBox_Small2 txtANCIENNE_REFERENCE;
        public iTalk.iTalk_TextBox_Small2 txtNATURE;
        private System.Windows.Forms.Label label92;
        public iTalk.iTalk_TextBox_Small2 txtSPECIAL_BBATH;
        public iTalk.iTalk_TextBox_Small2 txtSUPPRIME_CHEZ_FABRIQUANT;
        private System.Windows.Forms.Label label91;
        private System.Windows.Forms.Label label90;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_DEVIS;
        public iTalk.iTalk_TextBox_Small2 txtREGROUPEMENT_ATOLL;
        private System.Windows.Forms.Label label89;
        private System.Windows.Forms.Label label88;
        public iTalk.iTalk_TextBox_Small2 txtART_dateFinActivite;
        public iTalk.iTalk_TextBox_Small2 txtNoAuto;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridCond;
        public iTalk.iTalk_TextBox_Small2 txtCORPS_D_ETAT;
        private System.Windows.Forms.Label label26;
        public iTalk.iTalk_TextBox_Small2 txtPOIDS_NET;
        private System.Windows.Forms.Label label20;
        public iTalk.iTalk_TextBox_Small2 txtSURFACE;
        private System.Windows.Forms.Label label21;
        public iTalk.iTalk_TextBox_Small2 txtLARGEUR;
        private System.Windows.Forms.Label label22;
        public iTalk.iTalk_TextBox_Small2 txtVOLUME;
        private System.Windows.Forms.Label label23;
        public iTalk.iTalk_TextBox_Small2 txtHAUTEUR;
        private System.Windows.Forms.Label label24;
        public iTalk.iTalk_TextBox_Small2 txtLONGUEUR;
        private System.Windows.Forms.Label label25;
        public iTalk.iTalk_TextBox_Small2 txtCHRONO_INITIAL_ARTICLE_REMPLACEMENT;
        private System.Windows.Forms.Label label18;
        public iTalk.iTalk_TextBox_Small2 txtLIEN_FICHE_SECURITE;
        private System.Windows.Forms.Label label19s;
        public iTalk.iTalk_TextBox_Small2 txtLIEN_URL_TECHNIQUE;
        private System.Windows.Forms.Label label16;
        public iTalk.iTalk_TextBox_Small2 txtIMAGE;
        private System.Windows.Forms.Label label17;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_UNITE_DEVIS;
        private System.Windows.Forms.Label label14;
        public iTalk.iTalk_TextBox_Small2 txtUNCS_SUR_UV;
        private System.Windows.Forms.Label label15;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_UNITE_BASE;
        private System.Windows.Forms.Label label12;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_UNITE_CONDITIONNEMENT;
        private System.Windows.Forms.Label label13;
        public iTalk.iTalk_TextBox_Small2 txtCODE_UNITE_CONDITIONNEMENT;
        private System.Windows.Forms.Label label10;
        public iTalk.iTalk_TextBox_Small2 txtUNITE_CONDITIONNEMENT;
        private System.Windows.Forms.Label label11;
        public iTalk.iTalk_TextBox_Small2 txtsTatut;
        private System.Windows.Forms.Label label9;
        public iTalk.iTalk_TextBox_Small2 txtDATE_FIN_ACTIVITE;
        private System.Windows.Forms.Label label8;
        public iTalk.iTalk_TextBox_Small2 txtCOMPOSANT;
        private System.Windows.Forms.Label label7;
        public iTalk.iTalk_TextBox_Small2 txtTYPE;
        private System.Windows.Forms.Label label6;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_SOUS_FAMILLE;
        private System.Windows.Forms.Label label5;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_FAMILLE;
        private System.Windows.Forms.Label label4;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_TECHNIQUE;
        private System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 txtDESIGNATION_ARTICLE;
        private System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtFAMILLE_ATOLL;
        private System.Windows.Forms.Label label1;
        public iTalk.iTalk_TextBox_Small2 txtCHRONO_INITIAL;
        private System.Windows.Forms.Label label33;
        public System.Windows.Forms.GroupBox Frame1;
        public iTalk.iTalk_TextBox_Small2 txtCODE_UNITE_FOURNISSEUR;
        public iTalk.iTalk_TextBox_Small2 txtLIBELLE_STATUT_CONTROLE;
        public System.Windows.Forms.Label label67;
        public System.Windows.Forms.Label label87;
        public iTalk.iTalk_TextBox_Small2 txtARTP_fournPref;
        public System.Windows.Forms.Label label86;
        public iTalk.iTalk_TextBox_Small2 txtARTP_PrixNetDepan;
        public System.Windows.Forms.Label label85;
        public iTalk.iTalk_TextBox_Small2 txtARTP_UsParUc;
        public System.Windows.Forms.Label label84;
        public iTalk.iTalk_TextBox_Small2 txtARTP_UdParUs;
        public System.Windows.Forms.Label label83;
        public iTalk.iTalk_TextBox_Small2 txtARTP_UvParUc;
        public System.Windows.Forms.Label label82;
        public iTalk.iTalk_TextBox_Small2 txtARTP_UcnfParUc;
        public System.Windows.Forms.Label label81;
        public iTalk.iTalk_TextBox_Small2 txtARTP_RefHisto;
        public System.Windows.Forms.Label label80;
        public iTalk.iTalk_TextBox_Small2 txtARTP_RefInfo;
        public System.Windows.Forms.Label label79;
        public iTalk.iTalk_TextBox_Small2 txtART_CategorieOutil;
        public System.Windows.Forms.Label label78;
        public iTalk.iTalk_TextBox_Small2 txtART_RubriqueAnaly;
        public System.Windows.Forms.Label label77;
        public iTalk.iTalk_TextBox_Small2 txtART_EtatDiffusion;
        public System.Windows.Forms.Label label76;
        public iTalk.iTalk_TextBox_Small2 txtART_FamAtoll;
        public System.Windows.Forms.Label label75;
        public iTalk.iTalk_TextBox_Small2 txtART_Nature;
        public System.Windows.Forms.Label label74;
        public iTalk.iTalk_TextBox_Small2 txtCODE_COND_FOURNISSEUR;
        public System.Windows.Forms.Label label66;
        public iTalk.iTalk_TextBox_Small2 txtCODE_UNITE_METIER;
        public System.Windows.Forms.Label label65;
        public iTalk.iTalk_TextBox_Small2 txtCODE_UNITE_DEVIS_2;
        public System.Windows.Forms.Label label64;
        public iTalk.iTalk_TextBox_Small2 txtCODE_UNITE_STOCK;
        public System.Windows.Forms.Label label63;
        public iTalk.iTalk_TextBox_Small2 txtCODE_COND_STOCK;
        public System.Windows.Forms.Label label62;
        public iTalk.iTalk_TextBox_Small2 txtCODE_UNITE_VALEUR;
        public System.Windows.Forms.Label label61;
        public iTalk.iTalk_TextBox_Small2 txtCODE_UNITE_DEVIS;
        public System.Windows.Forms.Label label60;
        public iTalk.iTalk_TextBox_Small2 txtUNITE_DEVIS;
        public System.Windows.Forms.Label label59;
        public iTalk.iTalk_TextBox_Small2 txtART_Composant;
        public System.Windows.Forms.Label label58;
        public iTalk.iTalk_TextBox_Small2 txtUNITE_BASE;
        public System.Windows.Forms.Label label57;
        public iTalk.iTalk_TextBox_Small2 txtID_FAMILLE;
        public System.Windows.Forms.Label label68;
    }
}