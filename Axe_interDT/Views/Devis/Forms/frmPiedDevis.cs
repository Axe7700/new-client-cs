﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class frmPiedDevis : Form
    {
        public frmPiedDevis()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            DataTable rsEntete = default(DataTable);
            ModAdo modAdorsEntete;

            if (string.IsNullOrEmpty(txtLibelle.Text) && string.IsNullOrEmpty(txtTexte.Text) && !string.IsNullOrEmpty(txtCode.Text))
            {
                return;
            }

            txtCode.Text = "";
            txtLibelle.Text = "";
            txtTexte.Text = "";
            lblUtilisateur.Text = "";

            modAdorsEntete = new ModAdo();
            rsEntete = modAdorsEntete.fc_OpenRecordSet("SELECT * FROM TypeReglementDevis WHERE CodeReglement=0");
            //    rsEntete.Close
            var NewRow = rsEntete.NewRow();
            NewRow["Utilisateur"] = General.gsUtilisateur;
            rsEntete.Rows.Add(NewRow);
            lblUtilisateur.Text = General.gsUtilisateur;
            modAdorsEntete.Update();
            using (var tmpModAdo = new ModAdo())
                txtCode.Text = tmpModAdo.fc_ADOlibelle("SELECT MAX(CodeReglement) FROM TypeReglementDevis");
            modAdorsEntete?.Dispose();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtLibelle.Text) && string.IsNullOrEmpty(txtTexte.Text) && !string.IsNullOrEmpty(txtCode.Text))
            {
                General.Execute("DELETE FROM TypeReglementDevis WHERE CodeReglement=" + txtCode.Text);
                cmdDernier_Click(cmdDernier, new System.EventArgs());
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDernier_Click(object sender, EventArgs e)
        {
            DataTable rsEntete = default(DataTable);
            var modAdorsEntete = new ModAdo();


            rsEntete = modAdorsEntete.fc_OpenRecordSet("SELECT TOP 1 TypeReglementDevis.* FROM TypeReglementDevis ORDER BY CodeReglement DESC");

            if (rsEntete.Rows.Count > 0)
            {
                txtCode.Text = rsEntete.Rows[0]["CodeReglement"] + "";
                txtLibelle.Text = rsEntete.Rows[0]["LibelleReglement"] + "";
                txtTexte.Text = rsEntete.Rows[0]["TexteReglement"] + "";
                lblUtilisateur.Text = rsEntete.Rows[0]["Utilisateur"] + "";
            }
            else
            {
                txtCode.Text = "";
                txtLibelle.Text = "";
                txtTexte.Text = "";
                lblUtilisateur.Text = "";
            }

            modAdorsEntete?.Dispose();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdPrecedent_Click(object sender, EventArgs e)
        {
            DataTable rsEntete = default(DataTable);
            var modAdorsEntete = new ModAdo();


            rsEntete = modAdorsEntete.fc_OpenRecordSet("SELECT TypeReglementDevis.* FROM TypeReglementDevis" +
                                                       " WHERE CodeReglement<'" + txtCode.Text + "' ORDER BY CodeReglement DESC");

            if (rsEntete.Rows.Count > 0)
            {
                txtCode.Text = rsEntete.Rows[0]["CodeReglement"] + "";
                txtLibelle.Text = rsEntete.Rows[0]["LibelleReglement"] + "";
                txtTexte.Text = rsEntete.Rows[0]["TexteReglement"] + "";
                lblUtilisateur.Text = rsEntete.Rows[0]["Utilisateur"] + "";
            }

            modAdorsEntete?.Dispose();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdPremier_Click(object sender, EventArgs e)
        {
            DataTable rsEntete = default(DataTable);
            var modAdorsEntete = new ModAdo();


            rsEntete = modAdorsEntete.fc_OpenRecordSet("SELECT TOP 1 TypeReglementDevis.* FROM TypeReglementDevis" +
                                                       " ORDER BY CodeReglement ASC");

            if (rsEntete.Rows.Count > 0)
            {
                txtCode.Text = rsEntete.Rows[0]["CodeReglement"] + "";
                txtLibelle.Text = rsEntete.Rows[0]["LibelleReglement"] + "";
                txtTexte.Text = rsEntete.Rows[0]["TexteReglement"] + "";
                lblUtilisateur.Text = rsEntete.Rows[0]["Utilisateur"] + "";
            }
            else
            {
                txtCode.Text = "";
                txtLibelle.Text = "";
                txtTexte.Text = "";
                lblUtilisateur.Text = "";
            }

            modAdorsEntete?.Dispose();
        }

        private void CmdRechercher_Click(object sender, EventArgs e)
        {
            try
            {
                string requete = "SELECT TypeReglementDevis.LibelleReglement AS [Libellé],TypeReglementDevis.TexteReglement AS [Texte], " +
                    "TypeReglementDevis.CodeReglement AS [Code], TypeReglementDevis.Utilisateur  " +
                                 " FROM TypeReglementDevis"; ;
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des Entêtes de devis" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    this.txtTexte.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    this.txtLibelle.Text = fg.ugResultat.ActiveRow.Cells[0].Value.ToString();
                    this.txtCode.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    this.lblUtilisateur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                    fg.Dispose(); fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        this.txtTexte.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        this.txtLibelle.Text = fg.ugResultat.ActiveRow.Cells[0].Value.ToString();
                        this.txtCode.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        this.lblUtilisateur.Text = fg.ugResultat.ActiveRow.Cells[3].Value.ToString();
                        fg.Dispose(); fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, "frmPiedDevis " + ";cmdRechercher_Click");
            }
        }

        private void CmdSauver_Click(object sender, EventArgs e)
        {
            string sqlSave = null;
            DataTable rsEntete = default(DataTable);
            var modAdorsEntete = new ModAdo();

            try
            {
                if (string.IsNullOrEmpty(txtCode.Text))
                {
                    rsEntete = modAdorsEntete.fc_OpenRecordSet("SELECT TOP 1 * FROM TypeReglementDevis ");
                    var NewRow = rsEntete.NewRow();
                    NewRow["Utilisateur"] = General.gsUtilisateur;
                    lblUtilisateur.Text = General.gsUtilisateur;
                    modAdorsEntete.Update();
                    using (var tmpModAdo = new ModAdo())
                        txtCode.Text = tmpModAdo.fc_ADOlibelle("SELECT MAX(CodeReglement) FROM TypeReglementDevis");
                    modAdorsEntete?.Dispose();
                }

                sqlSave = "UPDATE TypeReglementDevis SET TexteReglement='" + StdSQLchaine.gFr_DoublerQuote(txtTexte.Text) + "', LibelleReglement='" + StdSQLchaine.gFr_DoublerQuote(txtLibelle.Text) + "' ";
                sqlSave = sqlSave + " WHERE CodeReglement=" + txtCode.Text;

                General.Execute(sqlSave);
            }
            catch (Exception exception)
            {
                Erreurs.gFr_debug(exception, "frmEnteteDevis " + " cmdSauver_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSuivant_Click(object sender, EventArgs e)
        {
            DataTable rsEntete = default(DataTable);
            var modAdorsEntete = new ModAdo();

            rsEntete = modAdorsEntete.fc_OpenRecordSet("SELECT TypeReglementDevis.* FROM TypeReglementDevis WHERE CodeReglement>'" + txtCode.Text + "' ORDER BY CodeReglement ASC");

            if (rsEntete.Rows.Count > 0)
            {
                txtCode.Text = rsEntete.Rows[0]["CodeReglement"] + "";
                txtLibelle.Text = rsEntete.Rows[0]["LibelleReglement"] + "";
                txtTexte.Text = rsEntete.Rows[0]["TexteReglement"] + "";
                lblUtilisateur.Text = rsEntete.Rows[0]["Utilisateur"] + "";
            }

            modAdorsEntete?.Dispose();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdSupprimer_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCode.Text))
            {
                return;
            }
            if (lblUtilisateur.Text == General.gsUtilisateur)
            {
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez-vous vraiment supprimer ce pied de devis ?", "Suppression d'une entête", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                {
                    General.Execute("DELETE FROM TypeReglementDevis WHERE CodeReglement=" + txtCode.Text);
                    cmdPremier_Click(cmdPremier, new System.EventArgs());
                }
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas supprimer un pied de devis créé par une autre personne.", "Suppression non autorisée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmPiedDevis_Activated(object sender, EventArgs e)
        {
            DataTable rsEntete = default(DataTable);
            var modAdorsEntete = new ModAdo();

            rsEntete = modAdorsEntete.fc_OpenRecordSet("SELECT * FROM TypeReglementDevis WHERE Utilisateur='" + General.gsUtilisateur + "'");

            if (rsEntete.Rows.Count > 0)
            {
                txtCode.Text = rsEntete.Rows[0]["CodeReglement"] + "";
                txtLibelle.Text = rsEntete.Rows[0]["LibelleReglement"] + "";
                txtTexte.Text = rsEntete.Rows[0]["TexteReglement"] + "";
                lblUtilisateur.Text = rsEntete.Rows[0]["Utilisateur"] + "";
            }

            modAdorsEntete?.Dispose();
        }
    }
}
