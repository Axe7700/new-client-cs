﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Devis.Forms
{
    public partial class FrmFournture : Form
    {
        DataTable Adodc1 = null;
        SqlDataAdapter SDAAdodc1 = null;
        string ConnectionString = "";
        public FrmFournture()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmFournture_Activated(object sender, EventArgs e)
        {
            SSOleDBGrid1.DataSource = Adodc1;
            var _with1 = Adodc1;
            if (string.IsNullOrEmpty(txtConnectionString.Text))
            {
                ConnectionString = General.adocnn.ConnectionString;
            }
            else
            {
                ConnectionString = txtConnectionString.Text;
            }
            Adodc1 = new DataTable();
            SDAAdodc1 = new SqlDataAdapter(txtrequete.Text, ConnectionString);
            SDAAdodc1.Fill(Adodc1);
            SSOleDBGrid1.DataSource = Adodc1;
            txtChoix.Text = "";
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FrmFournture_FormClosing(object sender, FormClosingEventArgs e)
        {
            txtrequete.Text = "";
        }

        private void SSOleDBGrid1_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            if (SSOleDBGrid1.ActiveRow == null) return;
            txtChoix.Text = SSOleDBGrid1.ActiveRow.Cells["fournisseur"].Value.ToString();
            this.Visible = false;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == 13)
            {
                if (SSOleDBGrid1.ActiveRow == null) return;

                if (General.sGecetV3 == "1")//tested
                {
                    frmDetailArticleV3bis frmDetailArticleV3 = new frmDetailArticleV3bis();
                    frmDetailArticleV3.txtCHRONO_INITIAL.Text = SSOleDBGrid1.ActiveRow.Cells["Chrono"].Text;
                    frmDetailArticleV3.txtIDGEC_LIGNE_PRIX.Text = SSOleDBGrid1.ActiveRow.Cells["NoAuto"].Text;
                    frmDetailArticleV3.fc_LoadArticle();
                    frmDetailArticleV3.ShowDialog();
                    //frmDetailArticleV3.Close();

                }
                else if(General.sGecetV2 == "1" || General.sGecetV3 == "1")//tested
                {
                    frmDetailArticleV2 frmDetailArticleV2 = new frmDetailArticleV2();
                    frmDetailArticleV2.txtART_Chrono.Text = SSOleDBGrid1.ActiveRow.Cells["Chrono"].Text;
                    frmDetailArticleV2.txtART_ChronoPrix.Text = SSOleDBGrid1.ActiveRow.Cells["NoAuto"].Text;
                    frmDetailArticleV2.ShowDialog();
                    frmDetailArticleV2.Close();

                    frmDetailArticle frmDetailArticle = new frmDetailArticle();
                    frmDetailArticle.txtNoAuto.Text = SSOleDBGrid1.ActiveRow.Cells["NoAuto"].Text;
                    frmDetailArticle.ShowDialog();
                }
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            for(int i=0;i< SSOleDBGrid1.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                SSOleDBGrid1.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }
        }

        private void FrmFournture_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);
        }
    }
}
