﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.Views.Devis;
using Axe_interDT.Views.Theme.Menu;
using Axe_interDT.Views.Intervention;

namespace Axe_interDT.Views.Devis.Historique_Devis
{
    public partial class UserDocHistoDevis : UserControl
    {
        string strValeur;
        string strValeurRetour;
        string strSQL2;
        string strCheminRetour;
        DataTable adotemp;
        ModAdo adotempModAdo = new ModAdo();
        bool boolnow;
        string CodeFicheAppelante;
        public UserDocHistoDevis()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                var _with1 = SSOleDBGrid1;
                //= stocke le numero de devis pour positionner la fiche devis
                ModParametre.fc_SaveParamPosition(Variable.cUserDocHistoDevis, Variable.cUserDocDevis, _with1.ActiveRow.Cells["NumeroDEvis"].Value.ToString(), _with1.ActiveRow.Cells["CodeImmeuble"].Value.ToString(), _with1.ActiveRow.Cells["NumeroDEvis"].Value.ToString());
                View.Theme.Theme.Navigate(typeof(UserDocDevis));

                //        prcLiaison PROGDEVIS, .Columns("CodeImmeuble").value, .Columns("NumeroDEvis").value
                //        fc_Navigue Me, gsCheminPackage & PROGDEVIS
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSOleDBGrid1_DblClick");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    var _with2 = SSOleDBGrid1;
                    //= stocke le numero de devis pour positionner la fiche devis
                    ModParametre.fc_SaveParamPosition(Variable.cUserDocHistoDevis, Variable.cUserDocDevis, _with2.ActiveRow.Cells["NumeroDEvis"].Value.ToString(), _with2.ActiveRow.Cells["CodeImmeuble"].Value.ToString(), _with2.ActiveRow.Cells["NumeroDEvis"].Value.ToString());
                    View.Theme.Theme.Navigate(typeof(UserDocDevis));

                    //        prcLiaison PROGDEVIS, .Columns("CodeImmeuble").value, .Columns("NumeroDEvis").value
                    //        fc_Navigue Me, gsCheminPackage & PROGDEVIS
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSOleDBGrid1_KeyPress");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;

            try
            {

                //View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocHistoDevis");
                General.open_conn();


                /*   if (!string.IsNullOrEmpty(General.LogoSociete) | !Information.IsDBNull(General.LogoSociete))
                   {
                       imgLogoSociete.Image = System.Drawing.Image.FromFile(General.LogoSociete);
                       if (Information.IsNumeric(General.imgTop))
                       {
                           imgLogoSociete.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(General.imgTop);
                       }
                       if (Information.IsNumeric(General.imgLeft))
                       {
                           imgLogoSociete.Left = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsX(General.imgLeft);
                       }
                   }

                   if (!string.IsNullOrEmpty(General.NomSousSociete) & !Information.IsDBNull(General.NomSousSociete))
                   {
                       lblNomSociete.Text = General.NomSousSociete;
                       System.Windows.Forms.Application.DoEvents();
                   }*/
                boolnow = true;

                var _with4 = this;
                //- positionne sur le dernier enregistrement.
                if (ModParametre.fc_RecupParam(this.Name) == true)
                {
                    // charge les enregistremants
                    adotemp = new DataTable();

                    strSQL2 = "SELECT  DevisEntete.CodeImmeuble,NumeroDevis,"
                        + " DevisEntete.DateCreation,CodeEtat, "
                        + " Personnel.Matricule, Personnel.Nom, Personnel_DEVIS.Matricule,"
                        + " Personnel_DEVIS.Nom, TitreDevis"
                        + " FROM ((DevisEntete INNER JOIN Immeuble ON"
                        + " DevisEntete.CodeImmeuble = Immeuble.CodeImmeuble)"
                        + " LEFT JOIN Personnel ON Immeuble.CodeDepanneur = Personnel.Matricule)"
                        + " LEFT JOIN Personnel AS Personnel_DEVIS ON"
                        + " DevisEntete.CodeDeviseur = Personnel_DEVIS.Matricule"
                        + " WHERE DevisEntete.CodeImmeuble ='" + ModParametre.sNewVar
                        + "'" + " order by DevisEntete.DateCreation DESC";

                    adotemp = adotempModAdo.fc_OpenRecordSet(strSQL2);

                    //.Open "SELECT Intervention.CodeImmeuble, Intervention.DateRealise, Intervention.CodeEtat,  Intervention.Intervenant ,  Intervention.Designation, Intervention.Wave, Intervention.Commentaire, Intervention.Commentaire2, Intervention.Commentaire3, NoIntervention FROM Intervention where Intervention.CodeImmeuble ='" & sNewVar & "' order by DateRealise DESC", adocnn, adOpenKeyset, adLockOptimistic

                    boolnow = false;
                    SSOleDBGrid1.DataSource = adotemp;
                    SSOleDBGrid1.Visible = true;

                }

                switch (ModParametre.sFicheAppelante)
                {
                    case Variable.cUserIntervention:

                        /* lblGoFicheAppelante[2].Text = "Intervention";
                         CodeFicheAppelante = General.getFrmReg( Variable.cUserIntervention, "NewVar", "");
                         lblGoFicheAppelante[2].Visible = true;
                         lblGoFicheAppelante[3].Visible = true;
                         lblRetourFiche[0].Visible = true;*/

                        ////===>  après la modification du nouveau menu j'ai commenter ces lignes 20/06/2019

                        //Views.Theme.Menu.MainMenu.UserDocHistoDevisMenu[0].LabelItemName.Text = "Retour vers Intervention";
                        //CodeFicheAppelante = General.getFrmReg(Variable.cUserIntervention, "NewVar", "");
                        //ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, CodeFicheAppelante);                        
                        //Views.Theme.Menu.MainMenu.UserDocHistoDevisMenu[0].ClickEvent = (se, ev) =>
                        //{
                        //    View.Theme.Theme.Navigate(typeof(UserIntervention));

                        //};

                        CodeFicheAppelante = General.getFrmReg(Variable.cUserIntervention, "NewVar", "");
                        //ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, CodeFicheAppelante);

                        break;

                    //        Case cUserDocClient
                    //            lblGoFicheAppelante.Caption = "FICHE GERANT"
                    //            CodeFicheAppelante = GetSetting(cFrNomApp, cUserDocClient, "NewVar", "")
                    //            lblGoFicheAppelante.Visible = True

                    case Variable.cUserDocDevis:

                        ////===>  après la modification du nouveau menu j'ai commenter ces lignes modif 20/06/2019

                        // lblGoFicheAppelante[2].Text = "Devis";
                        //Views.Theme.Menu.MainMenu.UserDocHistoDevisMenu[0].LabelItemName.Text = "Retour vers Devis";
                        //CodeFicheAppelante = General.getFrmReg(Variable.cUserDocDevis, "NewVar", "");
                        //ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, CodeFicheAppelante);                      
                        //Views.Theme.Menu.MainMenu.UserDocHistoDevisMenu[0].ClickEvent = (se, ev) =>
                        //{
                        //    View.Theme.Theme.Navigate(typeof(UserDocDevis));

                        //};

                        CodeFicheAppelante = General.getFrmReg(Variable.cUserDocDevis, "NewVar", "");

                        //lblGoFicheAppelante[2].Visible = true;
                        //lblGoFicheAppelante[3].Visible = true;
                        //lblRetourFiche[0].Visible = true;
                        break;

                    default:
                        /* lblGoFicheAppelante[2].Visible = false;
                         lblGoFicheAppelante[3].Visible = false;
                         lblRetourFiche[0].Visible = false;*/
                        break;
                }

                /* SSOleDBGrid1.ColumnHeaders = false;

                 lblNavigation[0].Text = General.sNomLien0;
                 lblNavigation[1].Text = General.sNomLien1;
                 lblNavigation[2].Text = General.sNomLien2;*/




                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Me_Show");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Matricule"].Header.Caption = "Depanneur";
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Nom"].Header.Caption = "Libelle";
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Matricule1"].Header.Caption = "Deviseur";
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Nom1"].Header.Caption = "Libelle";
            for (int i = 0; i < SSOleDBGrid1.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                SSOleDBGrid1.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }
        }
    }
}
