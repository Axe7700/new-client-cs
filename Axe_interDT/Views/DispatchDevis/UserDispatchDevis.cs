﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Analytique;
using Axe_interDT.Views.Devis;
using Axe_interDT.Views.SharedViews;
using CrystalDecisions.CrystalReports.Engine;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Axe_interDT.Views.DispatchDevis
{

    public partial class UserDispatchDevis : UserControl
    {
        DataTable rsintervention;
        SqlDataAdapter SDArsintervention;
        SqlCommandBuilder SCBrsintervention;
        DataTable rsExportDetail;
        ModAdo modAdorsExportDetail;

        public short IndexDate;
        public System.Windows.Forms.TextBox champDate;
        bool blnKeyPress;
        bool bExportDetail;
        string sOrderBy;
        string sAscDesc;

        // CRPEAuto.Report Report;
        // CRPEAuto.Application crAppl;

        //  CRPEAuto.FormulaFieldDefinitions CRFrmDefns;
        //  CRPEAuto.FormulaFieldDefinition CRFrmDefn;

        const string cOuiRecept = "OuiRecept";
        const string cNonRecept = "NonRecept";
        const string cTousRecept = "TousRecept";
        const string cOuiCLot = "OuiCloture";
        const string cNonClot = "NonCLoture";
        const string cTousClot = "TousCLoture";
        public UserDispatchDevis()
        {
            InitializeComponent();
        }
        public string CalculDesHeures(ref string BisStrRequete)
        {

            string functionReturnValue = null;
            DataTable rs;
            int BisLongTemp = 0;
            int LongTemp = 0;
            decimal curtemp = default(decimal);

            string str_Renamed = null;
            SqlDataAdapter SDArs;
            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                rs = new DataTable();
                // rs.Open(BisStrRequete, General.adocnn);
                using (var modAdors = new ModAdo())
                    rs = modAdors.fc_OpenRecordSet(BisStrRequete);
                LongTemp = 0;

                if (rs.Rows.Count == 0)
                {
                    functionReturnValue = "00:00:00";
                    rs = null;
                    return functionReturnValue;
                }
                //----Calcul des heures en secondes

                foreach (DataRow rsRow in rs.Rows)
                {
                    if (rsRow["Duree"] != DBNull.Value)
                    {
                        LongTemp = LongTemp + Convert.ToDateTime(rsRow["Duree"]).Hour * 3600;
                        LongTemp = LongTemp + Convert.ToDateTime(rsRow["Duree"]).Minute * 60;
                        LongTemp = LongTemp + Convert.ToDateTime(rsRow["Duree"]).Second;
                    }

                    //rs.MoveNext();
                }

                rs.Dispose();
                rs = null;
                //----Calcul du temps en heures
                curtemp = 0;
                curtemp = LongTemp / 3600;
                str_Renamed = Convert.ToString(curtemp);
                if (str_Renamed.Contains("."))
                {
                    str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf('.'));
                    str_Renamed = Convert.ToInt16(str_Renamed).ToString("00");
                    functionReturnValue = str_Renamed + ":";
                    BisLongTemp = Convert.ToInt32(str_Renamed);
                    while (BisLongTemp != 0)
                    {
                        LongTemp = LongTemp - 3600;
                        BisLongTemp = BisLongTemp - 1;
                    }

                    //----Calcul du temps en minutes
                    curtemp = LongTemp / 60;
                    str_Renamed = Convert.ToString(curtemp);
                    if (str_Renamed.Contains('.'))
                    {
                        str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf('.'));
                        str_Renamed = Convert.ToInt16(str_Renamed).ToString("00");
                        functionReturnValue = functionReturnValue + str_Renamed + ":";
                        BisLongTemp = Convert.ToInt32(str_Renamed);
                        while (BisLongTemp != 0)
                        {
                            LongTemp = LongTemp - 60;
                            BisLongTemp = BisLongTemp - 1;
                        }
                        str_Renamed = Convert.ToInt16(LongTemp).ToString("00");
                        functionReturnValue = functionReturnValue + str_Renamed;
                    }
                    else
                    {
                        functionReturnValue = functionReturnValue + "00:00";
                    }
                }
                else if (str_Renamed.Contains(','))
                {
                    str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf(','));
                    str_Renamed = Convert.ToInt16(str_Renamed).ToString("00");
                    functionReturnValue = str_Renamed + ":";
                    BisLongTemp = Convert.ToInt32(str_Renamed);
                    while (BisLongTemp != 0)
                    {
                        LongTemp = LongTemp - 3600;
                        BisLongTemp = BisLongTemp - 1;
                    }

                    //----Calcul du temps en minutes
                    curtemp = LongTemp / 60;
                    str_Renamed = Convert.ToString(curtemp);
                    if (str_Renamed.Contains(','))
                    {
                        str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf(','));
                        str_Renamed = Convert.ToInt16(str_Renamed).ToString("00");
                        functionReturnValue = functionReturnValue + str_Renamed + ":";
                        BisLongTemp = Convert.ToInt32(str_Renamed);
                        while (BisLongTemp != 0)
                        {
                            LongTemp = LongTemp - 60;
                            BisLongTemp = BisLongTemp - 1;
                        }
                        str_Renamed = Convert.ToInt16(LongTemp).ToString("00");
                        functionReturnValue = functionReturnValue + str_Renamed;
                    }
                    else
                    {
                        functionReturnValue = functionReturnValue + "00:00";
                    }
                }
                else
                {
                    functionReturnValue = str_Renamed + ":00:00";
                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";CalculDesHeures");
            }

            return functionReturnValue;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbArticle_TextChanged(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibTypeOperation.Text = tmpAdo.fc_ADOlibelle("SELECT FacArticle.Designation1 FROM FacArticle WHERE FacArticle.CodeArticle='" + cmbArticle.Value + "'");
                if (blnKeyPress == true)
                {
                    txtCodeTitre.Text = "";
                    txtCodeTitreImp.Text = "";
                    blnKeyPress = false;
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbArticle_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibTypeOperation.Text = tmpAdo.fc_ADOlibelle("SELECT FacArticle.Designation1  FROM FacArticle WHERE FacArticle.CodeArticle='" + cmbArticle.Value + "'");
                txtCodeTitre.Text = "";
                txtCodeTitreImp.Text = "";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbArticle_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT CodeArticle , Designation1 From FacArticle WHERE CodeCategorieArticle = 'D' OR CodeCategorieArticle='P'  order by codearticle";
            if (cmbArticle.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(cmbArticle, General.sSQL, "CodeArticle");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbArticle_KeyPress(object sender, KeyPressEventArgs e)

        {
            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    using (var tmpModAdo = new ModAdo())
                        if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT FacArticle.Designation1  FROM FacArticle WHERE FacArticle.CodeArticle='" + cmbArticle.Value + "'").ToString(), "").ToString()))
                        {
                            requete = "SELECT CodeArticle as \"Type d'opération\" , Designation1 as \"Designation\"  From FacArticle";
                            where = " CodeCategorieArticle ='D' OR CodeCategorieArticle='P' ";
                            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'un type d'opération" };
                            fg.SetValues(new Dictionary<string, string> { { "CodeArticle", cmbArticle.Text } });

                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                cmbArticle.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                txtCodeTitre.Text = "";
                                txtCodeTitreImp.Text = "";
                                fg.Dispose(); fg.Close();
                            };
                            fg.ugResultat.KeyDown += (se, ev) =>
                            {
                                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                {
                                    cmbArticle.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    txtCodeTitre.Text = "";
                                    txtCodeTitreImp.Text = "";
                                    fg.Dispose(); fg.Close();
                                }

                            };
                            fg.StartPosition = FormStartPosition.CenterScreen;
                            fg.ShowDialog();

                        }
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";cmbArticle_KeyPress");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbArticleA_TextChanged(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibTypeOperationA.Text = tmpAdo.fc_ADOlibelle("SELECT FacArticle.Designation1  FROM FacArticle WHERE FacArticle.CodeArticle='" + cmbArticleA.Value + "'");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbArticleA_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibTypeOperationA.Text = tmpAdo.fc_ADOlibelle("SELECT FacArticle.Designation1  FROM FacArticle WHERE FacArticle.CodeArticle='" + cmbArticleA.Value + "'");
                txtCodeTitre.Text = "";
                txtCodeTitreImp.Text = "";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbArticleA_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT CodeArticle , Designation1  From FacArticle WHERE CodeCategorieArticle = 'D' OR CodeCategorieArticle='P'  order by codearticle";
            if (cmbArticleA.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(cmbArticleA, General.sSQL, "CodeArticle");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbArticleA_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    using (var tmpModAdo = new ModAdo())
                        if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT FacArticle.Designation1 FROM FacArticle WHERE FacArticle.CodeArticle='" + cmbArticleA.Value + "'").ToString(), "").ToString()))
                        {
                            requete = "SELECT CodeArticle as \"Type d'opération\" , Designation1 as \"Designation\" From FacArticle";
                            where = " CodeCategorieArticle = 'D' OR CodeCategorieArticle='P'";
                            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'un type d'opération" };
                            fg.SetValues(new Dictionary<string, string> { { "CodeArticle", cmbArticleA.Text } });

                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                cmbArticleA.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                txtCodeTitre.Text = "";
                                txtCodeTitreImp.Text = "";
                                fg.Dispose(); fg.Close();
                            };
                            fg.ugResultat.KeyDown += (se, ev) =>
                            {
                                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                {
                                    cmbArticleA.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    txtCodeTitre.Text = "";
                                    txtCodeTitreImp.Text = "";
                                    fg.Dispose(); fg.Close();
                                }

                            };
                            fg.StartPosition = FormStartPosition.CenterScreen;
                            fg.ShowDialog();

                        }
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";cmbArticleA_KeyPress");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbBe_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibBE.Text = tmpAdo.fc_ADOlibelle("SELECT Libelle FROM Be WHERE codeBe=" + General.nz(cmbBe.Value, 0));
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbBe_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT CodeBE,Libelle from BE ";
            // If cmbBE.Rows = 0 Then
            sheridan.InitialiseCombo(cmbBe, General.sSQL, "CodeBE", false);
            // End If
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbBe_Validating(object sender, CancelEventArgs e)
        {
            try
            {

                using (var tmpAdo = new ModAdo())
                {
                    lblLibBE.Text = tmpAdo.fc_ADOlibelle("SELECT Libelle FROM Be WHERE codeBe=" + General.nz(cmbBe.Value, 0));
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbBe_Validate");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeGerant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Table1.Code1, Table1.Adresse1, Table1.CodePostal, Table1.Ville From Table1 order by Code1";
            sheridan.InitialiseCombo(this.cmbCodeGerant, General.sSQL, "Code1");
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeimmeuble_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Immeuble.CodeImmeuble, Immeuble.Adresse, Immeuble.CodePostal, Immeuble.Ville From Immeuble order by CodeImmeuble";
            sheridan.InitialiseCombo(this.cmbCodeimmeuble, General.sSQL, "codeImmeuble");
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeimmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        //===> Mondir le 18.06.2021 https://groupe-dt.mantishub.io/view.php?id=2216
                        var count = tmpModAdo.fc_ADOlibelle("SELECT COUNT(*) FROM Immeuble WHERE Immeuble.CodeImmeuble LIKE '%" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) + "%'").ToInt();
                        //===> Fin Modif Mondir

                        //if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle(
                        //            "SELECT Immeuble.CodeImmeuble FROM Immeuble WHERE Immeuble.CodeImmeuble='" + cmbCodeimmeuble.Value + "'").ToString(), "").ToString()))
                        if (count == 0 || count > 1)
                        {
                            requete =
                                "SELECT CodeImmeuble as \"Ref Immeuble\", Adresse as \"Adresse\", Ville as \"Ville\" , anglerue as \"Angle de rue\", Code1 as \"Gerant\" FROM Immeuble ";
                            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "")
                            { Text = "Recherche d'un immeuble" };
                            fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", cmbCodeimmeuble.Text } });

                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                cmbCodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                fg.Dispose();
                                fg.Close();
                            };
                            fg.ugResultat.KeyDown += (se, ev) =>
                            {
                                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                {
                                    cmbCodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    fg.Dispose();
                                    fg.Close();
                                }

                            };
                            fg.StartPosition = FormStartPosition.CenterScreen;
                            fg.ShowDialog();

                        }
                    }

                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";cmbCodeimmeuble_KeyPress");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_TextChanged(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibCommercial.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbCommercial.Value + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibCommercial.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbCommercial.Value + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_BeforeDropDown(object sender, CancelEventArgs e)
        {
            int i;
            //    sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom," _
            //'       & " Qualification.CodeQualif, Qualification.Qualification" _
            //'       & " FROM Personnel INNER JOIN" _
            //'       & " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif" _
            //'       & " order by matricule"
            //
            //    InitialiseCombo cmbCommercial, Adodc12, sSQL, "Matricule"

            string req = null;
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom, Qualification.CodeQualif, Qualification.Qualification FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif";
            req = "";
            int l = General.CodeQualifCommercial.Length;
            if (General.CodeQualifCommercial.Length > 0)
            {
                req = req + " WHERE (";
                for (i = 1; i <= General.CodeQualifCommercial.Length - 1; i++)
                {

                    if (i < General.CodeQualifCommercial.Length - 1)
                    {
                        req = req + "Personnel.CodeQualif='" + General.CodeQualifCommercial[i] + "' OR ";
                    }
                    else
                    {

                        req = req + "Personnel.CodeQualif='" + General.CodeQualifCommercial[i] + "'";
                    }
                }
                req = req + ")";
            }

            if (string.IsNullOrEmpty(req))
            {
                req = " WHERE (Personnel.NonActif is null or Personnel.NonActif = 0) ";
            }
            else
            {
                req = req + " AND (Personnel.NonActif is null or Personnel.NonActif = 0)";
            }

            General.sSQL = General.sSQL + req + " ORDER BY Personnel.Nom";
            sheridan.InitialiseCombo(cmbCommercial, General.sSQL, "Matricule");

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    using (var tmpModAdo = new ModAdo())
                        if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbIntervenant.Value + "'").ToString(), "").ToString()))
                        {
                            requete = "SELECT Personnel.Matricule AS \"Matricule\", Personnel.Nom AS \"Nom\", Personnel.Prenom AS \"Prenom\", Qualification.CodeQualif as \"Code Qualification\", Qualification.Qualification as \"Libellé Qualification\"  FROM Personnel INNER JOIN  Qualification ON Personnel.CodeQualif = Qualification.CodeQualif";
                            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'un commercial" };
                            fg.SetValues(new Dictionary<string, string> { { "Matricule", cmbCommercial.Text } });

                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                cmbCommercial.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                fg.Dispose(); fg.Close();
                            };
                            fg.ugResultat.KeyDown += (se, ev) =>
                            {
                                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                {
                                    cmbCommercial.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    fg.Dispose(); fg.Close();
                                }

                            };
                            fg.StartPosition = FormStartPosition.CenterScreen;
                            fg.ShowDialog();

                        }
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";cmbCommercial_KeyPress");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_TextChanged(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibIntervenant.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbIntervenant.Value + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibIntervenant.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM  Personnel WHERE Personnel.Matricule='" + cmbIntervenant.Value + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            short i = 0;
            string req = null;
            try
            {
                req = "SELECT Personnel.Matricule,Personnel.Nom,Qualification.Qualification FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif ";

                if (General.nbCodeDeviseur > 0)
                {
                    req = req + " WHERE (";
                    for (i = 1; i <= General.nbCodeDeviseur; i++)
                    {
                        if (i < General.nbCodeDeviseur)
                        {
                            req = req + "Personnel.CodeQualif='" + General.CodeQualifDeviseur[i] + "' OR ";
                        }
                        else
                        {
                            req = req + "Personnel.CodeQualif='" + General.CodeQualifDeviseur[i] + "'";
                        }
                    }
                    req = req + ")";
                }

                if (string.IsNullOrEmpty(req))
                {

                    req = " WHERE (Personnel.NonActif is null or Personnel.NonActif = 0) ";
                }
                else
                {
                    req = req + " AND (Personnel.NonActif is null or Personnel.NonActif = 0)";
                }

                req = req + " ORDER BY Personnel.Matricule ";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                //If txtDeviseur.Rows = 0 Then
                sheridan.InitialiseCombo(cmbIntervenant, req, "Matricule");
                //End If

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtDeviseur_DropDown");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    using (var tmpModAdo = new ModAdo())
                        if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbIntervenant.Value + "'").ToString(), "").ToString()))
                        {
                            requete = "SELECT Personnel.Matricule AS \"Matricule\", Personnel.Nom AS \"Nom\", Personnel.Prenom AS \"Prenom\", Qualification.CodeQualif as \"Code Qualification\", Qualification.Qualification as \"Libellé Qualification\"  FROM Personnel INNER JOIN  Qualification ON Personnel.CodeQualif = Qualification.CodeQualif";
                            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'un intervenant" };
                            fg.SetValues(new Dictionary<string, string> { { "Matricule ", cmbIntervenant.Text } });

                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                cmbIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                fg.Dispose(); fg.Close();
                            };
                            fg.ugResultat.KeyDown += (se, ev) =>
                            {
                                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                {
                                    cmbIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    fg.Dispose(); fg.Close();
                                }

                            };
                            fg.StartPosition = FormStartPosition.CenterScreen;
                            fg.ShowDialog();

                        }
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervenant_KeyPress");
            }
        }

        private void cmbIntervention_Click(object sender, EventArgs e)
        {

            string sWhere = null;
            string sHaving = null;
            string sGroupBy = null;
            string sMatInitale = null;
            string sRecept = null;
            string sCloture = null;

            var searchEntred = false;

            try
            {
                if (optOuiRecept.Checked == true)
                {
                    sRecept = cOuiRecept;
                }
                else if (OptNonRecept.Checked == true)
                {
                    sRecept = cNonRecept;
                }
                else if (OptTousRecep.Checked == true)
                {
                    sRecept = cTousRecept;
                }

                if (OptOuiCloture.Checked == true)
                {
                    sCloture = cOuiCLot;
                }
                else if (OptNonCloture.Checked == true)
                {
                    sCloture = cNonClot;
                }
                else if (OptTousCloture.Checked == true)
                {
                    sCloture = cTousClot;
                }


                // stocke la position de la fiche immeuble
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDispatchDevis, txtNoIntervention.Text, "", this.txtinterDe.Text, this.txtIntereAu.Text, this.cmbStatus.Text, this.cmbIntervenant.Text, cmbArticle.Text, this.txtIntercom.Text,
                this.cmbCodeimmeuble.Text, "", "", this.txtDateSaisieDe.Text, this.txtDateSaisieFin.Text, this.cmbCodeGerant.Text, "", txtNoIntervention.Text, Convert.ToString(CHKRenovationChauff.Checked), txtNoEnregistrement.Text,
                Convert.ToString(chkSansContrat.Checked), sRecept, sCloture, cmbStatus2.Text, txtGroupe.Text);


                if (bExportDetail == true)
                {
                    General.sSQL = "SELECT CASE DevisEnTete.TotalAvantCoef WHEN 0 THEN 0 ELSE DevisEnTete.TotalHT / DevisEnTete.TotalAvantCoef END AS Coef, "
                        + " Immeuble.CodeImmeuble, DevisEnTete.NumeroDevis, DevisEnTete.CodeEtat,"
                         + " DevisEnTete.TitreDevis, DevisEnTete.DateCreation, '' as dateRealisation, DevisEnTete.DateAcceptation, " //===> Mondir le 15.02.2021 https://groupe-dt.mantishub.io/view.php?id=2187 added DateAcceptation
                         + " DevisEnTete.CodeDeviseur, DevisEnTete.ResponsableTravaux, Table1.Code1, DevisEnTete.Observations, " //===> Mondir le 09.04.2021 https://groupe-dt.mantishub.io/view.php?id=2400 added ResponsableTravaux
                         + " DevisEnTete.Codetitre, DevisEnTete.TotalAvantCoef, DevisEnTete.TotalHT, DevisEnTete.TotalTTC , "
                         + " Immeuble.Adresse AS Adresse1Immeuble, Immeuble.Adresse2_Imm AS Adresse2Immeuble,"
                         + " Immeuble.AngleRue AS AnglerueImmeuble,"
                         + " Immeuble.CodePostal AS CPimmeuble, Immeuble.Ville AS VilleImmeuble, ";

                    //+ " Table1.commercial AS CommercialDT, Personnel.Nom, Personnel.Prenom,"
                    //'=== modif du 06 05 2019, on prends le commaercial de l'immeuble et non celui de la fiche client.

                    General.sSQL = General.sSQL + "  Immeuble.CodeCommercial AS CommercialDT, Personnel.Nom, Personnel.Prenom,"
                         + " Table1.Nom AS RaisonSocialeGérant1, Table1.Adresse1 AS AdresseGérant1,"
                         + " Table1.Adresse2 AS AdresseGérant2, Table1.CodePostal AS CPGérant,"
                         + " Table1.Ville AS VilleGérant, Immeuble.CodeGestionnaire, "
                         + " Gestionnaire.NomGestion AS NomGestionnaireImmeuble,"
                         + " Gestionnaire.TelGestion AS TelGestionnaireImmeuble, "
                         + " Gestionnaire.TelPortable_GES AS TelPortableGestionnaireImmeuble,"
                         + " Gestionnaire.FaxGestion AS FaxGestionnaireImmeuble, "
                         + " Gestionnaire.Email_GES AS EmailGestionnaireImmeuble,"
                         + " DevisEnTete.CodeBE AS CodeBEImmeuble, BE.Libelle AS LibelleBEImmeuble,"
                         + " BE.Adresse AS AdresseBEImmeuble, BE.CP AS CPBEImmeuble,"
                         + " BE.Ville AS VilleBEImmeuble, BE.Contact AS ContactBEImmeuble, "
                         + " BE.Tel AS TelBEImmeuble, BE.Fax AS FaxBEImmeuble,"
                         + " BE.eMail AS EmailBEImmeuble, NumFicheStandard AS [Appel demandeur]";
                    //& " FROM         BE RIGHT OUTER JOIN" 
                    //& " Immeuble INNER JOIN" |

                    if (chkSansContrat.Checked == true)
                    {
                        General.sSQL = General.sSQL + "  FROM         Contrat RIGHT OUTER JOIN"
                            + " Immeuble INNER JOIN"
                            + " DevisEnTete ON Immeuble.CodeImmeuble = DevisEnTete.CodeImmeuble INNER JOIN"
                            + " Table1 ON Immeuble.Code1 = Table1.Code1 ON Contrat.CodeImmeuble = Immeuble.CodeImmeuble LEFT OUTER JOIN"
                            + " BE ON DevisEnTete.CodeBE = BE.CodeBE LEFT OUTER JOIN"
                             + " Gestionnaire ON Immeuble.CodeGestionnaire = Gestionnaire.CodeGestinnaire "
                            + " AND Immeuble.Code1 = Gestionnaire.Code1 LEFT OUTER JOIN";

                        //'& " Personnel ON Table1.commercial = Personnel.Matricule"
                        // '=== modif du 06 05 2019, on prends le commaercial de l'immeuble et non celui de la fiche client.

                        General.sSQL = General.sSQL + " Personnel ON Immeuble.CodeCommercial = Personnel.Matricule";

                        sWhere = "  WHERE Contrat.NumContrat IS NULL ";

                    }
                    else
                    {
                        // sSQL = sSQL & " FROM         DevisEnTete LEFT OUTER JOIN DevisEnTete ON Immeuble.CodeImmeuble = DevisEnTete.CodeImmeuble " _
                        //& " INNER JOIN Table1 ON Immeuble.Code1 = Table1.Code1 ON BE.CodeBE = DevisEnTete.CodeBE " _
                        //& " LEFT OUTER JOIN" _
                        //& " Gestionnaire ON Immeuble.CodeGestionnaire = Gestionnaire.CodeGestinnaire AND Immeuble.Code1 = Gestionnaire.Code1" _
                        //& " LEFT OUTER JOIN" _
                        //& " Personnel ON Table1.commercial = Personnel.Matricule"

                        General.sSQL = General.sSQL
                            + "  FROM         BE RIGHT OUTER JOIN"
                            + " Immeuble INNER JOIN"
                            + " DevisEnTete ON Immeuble.CodeImmeuble = DevisEnTete.CodeImmeuble "
                            + " INNER JOIN Table1 ON Immeuble.Code1 = Table1.Code1 ON BE.CodeBE = DevisEnTete.CodeBE "
                            + " LEFT OUTER JOIN"
                            + " Gestionnaire ON Immeuble.CodeGestionnaire = Gestionnaire.CodeGestinnaire AND Immeuble.Code1 = Gestionnaire.Code1"
                            + " LEFT OUTER JOIN";


                        //'& " Personnel ON Table1.commercial = Personnel.Matricule"
                        // '=== modif du 06 05 2019, on prends le commaercial de l'immeuble et non celui de la fiche client.
                        General.sSQL = General.sSQL + " Personnel ON Immeuble.CodeCommercial = Personnel.Matricule";


                    }


                }
                else
                {
                    //            If txtDateFactureDe <> "" Or txtDateFactureAu <> "" Then
                    //                    sSQL = "SELECT "
                    //                    sSQL = sSQL & " DEV.Coef,"
                    //                    sSQL = sSQL & "DEV.CodeImmeuble,DEV.NumeroDevis,DEV.CodeEtat,"
                    //                    sSQL = sSQL & " DEV.TitreDevis,DEV.DateCreation,DEV.CodeDeviseur,"
                    //                    sSQL = sSQL & " DEV.Code1,DevisEntete.Observations,DEV.CodeTitre, "
                    //                    sSQL = sSQL & " DEV.TotalAvantCoef,DEV.TotalHT"
                    //
                    //
                    //
                    //            Else
                    General.sSQL = "SELECT ";
                    General.sSQL = General.sSQL + " CASE DevisEnTete.TotalAvantCoef WHEN 0 THEN 0 ELSE DevisEnTete.TotalHT / DevisEnTete.TotalAvantCoef END AS Coef,";
                    General.sSQL = General.sSQL + "DevisEntete.CodeImmeuble,DevisEntete.NumeroDevis,DevisEntete.CodeEtat,";
                    General.sSQL = General.sSQL + " DevisEntete.TitreDevis,DevisEntete.DateCreation,DevisEntete.CodeDeviseur,";
                    General.sSQL = General.sSQL + " Immeuble.Code1,DevisEntete.Observations,DevisEntete.CodeTitre, ";
                    General.sSQL = General.sSQL + " DevisEntete.TotalAvantCoef,DevisEntete.TotalHT";
                    //            End If



                    if (string.IsNullOrEmpty(txtConccurent.Text))
                    {
                        //        sSQL = sSQL & " FROM DevisEntete "
                        //        sSQL = sSQL & " LEFT JOIN Immeuble "
                        //        sSQL = sSQL & " ON DevisEntete.CodeImmeuble = Immeuble.CodeImmeuble "
                        //        sSQL = sSQL & " INNER JOIN Table1 "
                        //        sSQL = sSQL & " ON Immeuble.Code1=Table1.Code1 "

                        //== modif rachid ajout de la table personnel.
                        if (chkSansContrat.Checked == true)
                        {
                            General.sSQL = General.sSQL + " FROM         DevisEnTete LEFT OUTER JOIN"
                                + " Immeuble ON DevisEnTete.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN"
                                + " Table1 ON Immeuble.Code1 = Table1.Code1 LEFT OUTER JOIN"
                                + " Contrat ON Immeuble.CodeImmeuble = Contrat.CodeImmeuble LEFT OUTER JOIN"
                                + " Personnel ON Immeuble.CodeDepanneur = Personnel.Matricule";

                            sWhere = " WHERE Contrat.NumContrat IS NULL ";

                        }
                        else
                        {
                            //                                If txtDateFactureDe <> "" Or txtDateFactureAu <> "" Then
                            //                                        sSQL = sSQL & " FROM "
                            //                                        sSQL = sSQL & " ( SELECT DISTINCT CASE DevisEnTete.TotalAvantCoef WHEN 0 THEN 0 ELSE DevisEnTete.TotalHT / DevisEnTete.TotalAvantCoef END AS Coef,"
                            //                                        sSQL = sSQL & " DevisEntete.CodeImmeuble,DevisEntete.NumeroDevis,DevisEntete.CodeEtat,"
                            //                                        sSQL = sSQL & " DevisEntete.TitreDevis,DevisEntete.DateCreation,DevisEntete.CodeDeviseur,"
                            //                                        sSQL = sSQL & " Immeuble.Code1,DevisEntete.CodeTitre, "
                            //                                        sSQL = sSQL & " DevisEntete.TotalAvantCoef,DevisEntete.TotalHT"
                            //                                        sSQL = sSQL & "  FROM DevisEnTete LEFT OUTER JOIN" _
                            //'                                            & " Immeuble ON DevisEnTete.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN " _
                            //'                                            & " Table1 ON Immeuble.Code1 = Table1.Code1 INNER JOIN " _
                            //'                                            & " GestionStandard ON DevisEnTete.NumeroDevis = GestionStandard.NoDevis INNER JOIN " _
                            //'                                            & " Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard INNER JOIN " _
                            //'                                            & " FactureManuelleEnTete ON Intervention.NoFacture = FactureManuelleEnTete.NoFacture LEFT OUTER JOIN " _
                            //'                                            & " Personnel ON Immeuble.CodeDepanneur = Personnel.Matricule"
                            //
                            //
                            //                                Else

                            General.sSQL = General.sSQL + " FROM         DevisEnTete LEFT OUTER JOIN"
                                + " Immeuble ON DevisEnTete.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN"
                                + " Table1 ON Immeuble.Code1 = Table1.Code1 LEFT OUTER JOIN"
                                + " Personnel ON Immeuble.CodeDepanneur = Personnel.Matricule ";
                            //                                End If
                        }
                    }
                    else
                    {
                        // sSQL = sSQL & " FROM DevisEnTete LEFT OUTER JOIN" _
                        //& " Immeuble ON DevisEnTete.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN" _
                        //& " Table1 ON Immeuble.Code1 = Table1.Code1 LEFT OUTER JOIN" _
                        //& " DEC_DevisConccurent ON DevisEnTete.NumeroDevis = DEC_DevisConccurent.NumeroDevis"

                        //== modif rachid ajout de la table personnel.
                        General.sSQL = General.sSQL + " FROM DevisEnTete LEFT OUTER JOIN"
                            + " Immeuble ON DevisEnTete.CodeImmeuble = Immeuble.CodeImmeuble"
                            + " INNER JOIN Table1 ON Immeuble.Code1 = Table1.Code1 LEFT OUTER JOIN"
                            + " Personnel ON Immeuble.CodeDepanneur = Personnel.Matricule "
                            + " LEFT OUTER JOIN" + " DEC_DevisConccurent ON DevisEnTete.NumeroDevis = "
                            + " DEC_DevisConccurent.NumeroDevis ";


                    }



                }

                if (!string.IsNullOrEmpty(this.cmbCodeimmeuble.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.CodeImmeuble" + (chkExclImm.CheckState == 0 ? "=" : "<>") + "'" + this.cmbCodeimmeuble.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND DevisEntete.CodeImmeuble" + (chkExclImm.CheckState == 0 ? "=" : "<>") + "'" + this.cmbCodeimmeuble.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(this.cmbCodeGerant.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Immeuble.code1" + (chkExclGerant.CheckState == 0 ? "=" : "<>") + "'" + this.cmbCodeGerant.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Immeuble.Code1" + (chkExclGerant.CheckState == 0 ? "=" : "<>") + "'" + this.cmbCodeGerant.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(this.cmbIntervenant.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.CodeDeviseur" + (chkExclDeviseur.CheckState == 0 ? "=" : "<>") + "'" + this.cmbIntervenant.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND DevisEntete.CodeDeviseur" + (chkExclDeviseur.CheckState == 0 ? "=" : "<>") + "'" + this.cmbIntervenant.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(this.txtRespTrav.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.ResponsableTravaux" + (chkExclRespTravaux.CheckState == 0 ? "=" : "<>") + "'" + this.txtRespTrav.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND DevisEntete.ResponsableTravaux" + (chkExclRespTravaux.CheckState == 0 ? "=" : "<>") + "'" + this.txtRespTrav.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(this.cmbCommercial.Text))
                {
                    searchEntred = true;

                    if (General.sVersionImmParComm == "1")
                    {

                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE Immeuble.CodeCommercial " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + this.cmbCommercial.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND Immeuble.CodeCommercial " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + this.cmbCommercial.Text + "'";
                        }

                    }
                    else
                    {

                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE (Table1.Commercial" + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + this.cmbCommercial.Text + "' OR Immeuble.CodeCommercial " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + this.cmbCommercial.Text + "')";
                        }
                        else
                        {
                            sWhere = sWhere + " AND (Table1.Commercial" + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + this.cmbCommercial.Text + "' OR Immeuble.CodeCommercial " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + this.cmbCommercial.Text + "')";
                        }
                    }
                }
                //== modif rachid du 07/09/2005 ajout du chef de secteur.
                if (!string.IsNullOrEmpty(txtINT_AnaCode.Text))
                {
                    searchEntred = true;

                    //==== modif le 25 10 2011, le chef de secteur est attaché à l'immeuble.
                    if (General.sVersionImmParComm == "1")
                    {
                        using (var tmpModAdo = new ModAdo())
                            sMatInitale = tmpModAdo.fc_ADOlibelle("SELECT MATRICULE FROM PERSONNEL WHERE initiales ='" + txtINT_AnaCode.Text + "'");
                        //=== modif temporaire à mettre à jour.
                        if (General.sRespExploitParImmeuble == "1")
                        {

                            if (string.IsNullOrEmpty(sWhere))
                            {
                                sWhere = " WHERE Immeuble.CodeRespExploit ='" + sMatInitale + "'";
                            }
                            else
                            {
                                sWhere = sWhere + " AND Immeuble.CodeRespExploit ='" + sMatInitale + "'";
                            }

                        }
                        else if (General.sChefDeSecteurParImmeuble == "1")
                        {

                            if (string.IsNullOrEmpty(sWhere))
                            {
                                sWhere = " WHERE Immeuble.CodeChefSecteur ='" + sMatInitale + "'";
                            }
                            else
                            {
                                sWhere = sWhere + " AND Immeuble.CodeChefSecteur ='" + sMatInitale + "'";
                            }

                        }
                        else
                        {

                            if (string.IsNullOrEmpty(sWhere))
                            {
                                sWhere = " WHERE Personnel.CRespExploit = '" + this.txtINT_AnaCode.Text + "'";
                            }
                            else
                            {
                                sWhere = sWhere + " AND  Personnel.CRespExploit = '" + this.txtINT_AnaCode.Text + "'";
                            }

                        }
                    }
                    else
                    {

                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE Personnel.CRespExploit = '" + this.txtINT_AnaCode.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND  Personnel.CRespExploit = '" + this.txtINT_AnaCode.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(this.cmbStatus.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        if (!string.IsNullOrEmpty(this.cmbStatus2.Text))
                        {
                            sWhere = " WHERE (DevisEntete.CodeEtat" + (chkExclStatut.CheckState == 0 ? "=" : "<>") + "'" + this.cmbStatus.Text + "'";
                        }
                        else
                        {
                            sWhere = " WHERE DevisEntete.CodeEtat" + (chkExclStatut.CheckState == 0 ? "=" : "<>") + "'" + this.cmbStatus.Text + "'";
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(this.cmbStatus2.Text))
                        {
                            sWhere = sWhere + " AND (DevisEntete.CodeEtat" + (chkExclStatut.CheckState == 0 ? "=" : "<>") + "'" + this.cmbStatus.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND DevisEntete.CodeEtat" + (chkExclStatut.CheckState == 0 ? "=" : "<>") + "'" + this.cmbStatus.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(this.cmbStatus2.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.CodeEtat" + (chkExclStatut2.CheckState == 0 ? "=" : "<>") + "'" + this.cmbStatus2.Text + "'";
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(cmbStatus.Text))
                        {
                            sWhere = sWhere + " OR DevisEntete.CodeEtat" + (chkExclStatut2.CheckState == 0 ? "=" : "<>") + "'" + this.cmbStatus2.Text + "')";
                        }
                        else
                        {
                            sWhere = sWhere + " AND DevisEntete.CodeEtat" + (chkExclStatut2.CheckState == 0 ? "=" : "<>") + "'" + this.cmbStatus2.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(this.txtinterDe.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.DateCreation>='" + Convert.ToDateTime(this.txtinterDe.Text).ToString(General.FormatDateSQL) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND DevisEntete.DateCreation>='" + Convert.ToDateTime(this.txtinterDe.Text).ToString(General.FormatDateSQL) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(this.txtIntereAu.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.DateCreation<='" + Convert.ToDateTime(this.txtIntereAu.Text).ToString(General.FormatDateSQL) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  DevisEntete.DateCreation<='" + Convert.ToDateTime(this.txtIntereAu.Text).ToString(General.FormatDateSQL) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(this.txtIntercom.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.TitreDevis like '%" + this.txtIntercom.Text + "%'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  DevisEntete.TitreDevis LIKE '%" + this.txtIntercom.Text + "%'";
                    }
                }

                if (!string.IsNullOrEmpty(txtNoIntervention.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.NumeroDevis='" + this.txtNoIntervention.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  DevisEntete.NumeroDevis='" + this.txtNoIntervention.Text + "'";
                    }
                }

                if (string.IsNullOrEmpty(txtCodeTitre.Text))
                {
                    if (!string.IsNullOrEmpty(this.cmbArticle.Text) && string.IsNullOrEmpty(this.cmbArticleA.Text))
                    {
                        searchEntred = true;

                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE DevisEntete.CodeTitre='" + this.cmbArticle.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND  DevisEntete.CodeTitre='" + this.cmbArticle.Text + "'";
                        }
                    }
                    else if (string.IsNullOrEmpty(this.cmbArticle.Text) && !string.IsNullOrEmpty(this.cmbArticleA.Text))
                    {
                        searchEntred = true;

                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE DevisEntete.CodeTitre='" + this.cmbArticleA.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND  DevisEntete.CodeTitre='" + this.cmbArticleA.Text + "'";
                        }
                    }
                    else if (!string.IsNullOrEmpty(this.cmbArticle.Text) && !string.IsNullOrEmpty(this.cmbArticleA.Text))
                    {
                        searchEntred = true;

                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE DevisEntete.CodeTitre>='" + this.cmbArticle.Text + "' AND DevisEntete.CodeTitre<='" + this.cmbArticleA.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND  DevisEntete.CodeTitre>='" + this.cmbArticle.Text + "' AND DevisEntete.CodeTitre<='" + this.cmbArticleA.Text + "'";
                        }
                    }
                }
                else
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE " + txtCodeTitre.Text;
                    }
                    else
                    {
                        sWhere = sWhere + " AND " + txtCodeTitre.Text;
                    }
                }

                if (!string.IsNullOrEmpty(this.txtDateSaisieDe.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE DevisEntete.DateAcceptation>='" + Convert.ToDateTime(this.txtDateSaisieDe.Text).ToString(General.FormatDateSansHeureSQL) + " 00:00:00" + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND DevisEntete.DateAcceptation>='" + Convert.ToDateTime(this.txtDateSaisieDe.Text).ToString(General.FormatDateSansHeureSQL) + " 00:00:00" + "'";

                    }
                }

                if (!string.IsNullOrEmpty(this.txtDateSaisieFin.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " WHERE DevisEntete.DateAcceptation<='" + Convert.ToDateTime(this.txtDateSaisieFin.Text).ToString(General.FormatDateSansHeureSQL) + " 23:59:59" + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND DevisEntete.DateAcceptation<='" + Convert.ToDateTime(this.txtDateSaisieFin.Text).ToString(General.FormatDateSansHeureSQL) + " 23:59:59" + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtCodeGestionnaire.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE immeuble.CodeGestionnaire " + (chkExclGestionnaire.CheckState == 0 ? "=" : "<>") + "'" + this.txtCodeGestionnaire.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and immeuble.CodeGestionnaire" + (chkExclGestionnaire.CheckState == 0 ? "=" : "<>") + "'" + this.txtCodeGestionnaire.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtConccurent.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DEC_DevisConccurent.DEC_Code = '" + txtConccurent.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and DEC_DevisConccurent.DEC_Code = '" + txtConccurent.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtGroupe.Text))//tested
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "  WHERE Table1.CRCL_Code='" + StdSQLchaine.gFr_DoublerQuote(txtGroupe.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Table1.CRCL_Code ='" + StdSQLchaine.gFr_DoublerQuote(txtGroupe.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtCoefDe.Text) && General.IsNumeric(txtCoefDe.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        // sWhere = " WHERE  (DevisEntete.TotalHT/DevisEntete.TotalAvantCoef)>=" + General.nz(txtCoefDe.Text, "0") + " DevisEntete.TotalAvantCoef<>0 AND ISNUMERIC(DevisEntete.TotalAvantCoef)=1";
                        sWhere = "WHERE  DevisEntete.TotalAvantCoef<>0 AND ISNUMERIC(DevisEntete.TotalAvantCoef)=1";
                    }
                    else
                    {

                        // sWhere = sWhere + " AND (DevisEntete.TotalHT/DevisEntete.TotalAvantCoef)>=" + General.nz(txtCoefDe.Text, "0") + " AND DevisEntete.TotalAvantCoef<>0 AND ISNUMERIC(DevisEntete.TotalAvantCoef)=1";
                        sWhere = sWhere + " AND DevisEntete.TotalAvantCoef<>0 AND ISNUMERIC(DevisEntete.TotalAvantCoef)=1";
                    }
                }

                // tester (voir ce qui est ecrit en commentaire en haut de la fct)
                if (!string.IsNullOrEmpty(txtCoefA.Text) && General.IsNumeric(txtCoefA.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        // sWhere = " WHERE (DevisEntete.TotalHT/DevisEntete.TotalAvantCoef)<=" + General.nz(txtCoefA.Text, "0") + " AND DevisEntete.TotalAvantCoef<>0 AND ISNUMERIC(DevisEntete.TotalAvantCoef)=1";
                        sWhere = " WHERE  DevisEntete.TotalAvantCoef<>0 AND ISNUMERIC(DevisEntete.TotalAvantCoef)=1";
                    }
                    else
                    {
                        //  sWhere = sWhere + " AND (DevisEntete.TotalHT/DevisEntete.TotalAvantCoef)<=" + General.nz(txtCoefA.Text, "0") + " AND DevisEntete.TotalAvantCoef<>0 AND ISNUMERIC(DevisEntete.TotalAvantCoef)=1";
                        sWhere = sWhere + "  AND DevisEntete.TotalAvantCoef<>0 AND ISNUMERIC(DevisEntete.TotalAvantCoef)=1";
                    }
                }

                if (!string.IsNullOrEmpty(txtMontantDe.Text) && General.IsNumeric(txtMontantDe.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE (DevisEntete.TotalHT)>=" + General.nz(txtMontantDe.Text, "0") + "";
                    }
                    else
                    {

                        sWhere = sWhere + " AND (DevisEntete.TotalHT)>=" + General.nz(txtMontantDe.Text, "0") + "";
                    }
                }

                if (!string.IsNullOrEmpty(txtMontantA.Text) && General.IsNumeric(txtMontantA.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (DevisEntete.TotalHT)<=" + General.nz(txtMontantA.Text, "0") + "";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (DevisEntete.TotalHT)<=" + General.nz(txtMontantA.Text, "0") + "";
                    }
                }

                if (!string.IsNullOrEmpty(this.txtCP.Text))
                {
                    searchEntred = true;

                    if (txtCP.Text.Length == 2)
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE Immeuble.CodePostal like '" + this.txtCP.Text + "%'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND Immeuble.CodePostal like '" + this.txtCP.Text + "%'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE Immeuble.CodePostal ='" + this.txtCP.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND Immeuble.CodePostal ='" + this.txtCP.Text + "'";
                        }
                    }
                }

                if (CHKRenovationChauff.Checked == true)
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.RenovationChauff = 1";
                    }
                    else
                    {
                        sWhere = sWhere + " and DevisEntete.RenovationChauff = 1";
                    }
                }

                //==== MODIF du 13 02 2017 ajout du champs cloturé et receptionné.
                if (optOuiRecept.Checked == true)
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.Receptionne = 1";
                    }
                    else
                    {
                        sWhere = sWhere + " and DevisEntete.Receptionne = 1";
                    }
                }

                else if (OptNonRecept.Checked == true)
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (DevisEntete.Receptionne = 0 or DevisEntete.Receptionne is null)";
                    }
                    else
                    {
                        sWhere = sWhere + " and (DevisEntete.Receptionne = 0 or DevisEntete.Receptionne is null)";
                    }
                }

                if (OptOuiCloture.Checked == true)
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.ClotureDevis = 1";
                    }
                    else
                    {
                        sWhere = sWhere + " and DevisEntete.ClotureDevis = 1";
                    }
                }

                else if (OptNonCloture.Checked == true)
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (DevisEntete.ClotureDevis = 0 or DevisEntete.ClotureDevis is null)";
                    }
                    else
                    {
                        sWhere = sWhere + " and (DevisEntete.ClotureDevis = 0 or DevisEntete.ClotureDevis is null)";
                    }
                }
                //==== FIN modif du 13 02 2017.

                if (!string.IsNullOrEmpty(this.txtNoEnregistrement.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.NoEnregistrement='" + this.txtNoEnregistrement.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND DevisEntete.NoEnregistrement='" + this.txtNoEnregistrement.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(this.cmbBe.Text))
                {
                    searchEntred = true;

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.codeBe=" + this.cmbBe.Text;
                    }
                    else
                    {
                        sWhere = sWhere + " AND DevisEntete.CodeBE=" + cmbBe.Text;
                    }
                }

                //===> Mondir le 04.02.2021, https://groupe-dt.mantishub.io/view.php?id=2250 & https://groupe-dt.mantishub.io/view.php?id=2251
                if (!txtCodeArticle.Text.IsNullOrEmpty())
                {
                    if (sWhere.IsNullOrEmpty())
                    {
                        sWhere = $"WHERE DevisEnTete.NumeroDevis IN (SELECT DISTINCT NumeroDevis FROM DevisDetail WHERE CodeArticle = '{txtCodeArticle.Text}')";
                    }
                    else
                    {
                        sWhere = sWhere + $" AND DevisEnTete.NumeroDevis IN (SELECT DISTINCT NumeroDevis FROM DevisDetail WHERE CodeArticle = '{txtCodeArticle.Text}')";
                    }
                }

                if (!txtPoste.Text.IsNullOrEmpty())
                {
                    if (sWhere.IsNullOrEmpty())
                    {
                        sWhere = $"WHERE DevisEnTete.NumeroDevis IN (SELECT DISTINCT NumeroDevis FROM DevisDetail WHERE CodeSousFamille = '{txtPoste.Text}')";
                    }
                    else
                    {
                        sWhere = sWhere + $" AND DevisEnTete.NumeroDevis IN (SELECT DISTINCT NumeroDevis FROM DevisDetail WHERE CodeSousFamille = '{txtPoste.Text}')";
                    }
                }
                //===> Fin Modif Mondir

                //    If txtDateFactureDe <> "" Then
                //        If sWhere = "" Then
                //            sWhere = " WHERE FactureManuelleEnTete.DateFacture  >='" & txtDateFactureDe & "'"
                //        Else
                //            sWhere = sWhere & " AND FactureManuelleEnTete.DateFacture  >='" & txtDateFactureDe & "'"
                //        End If
                //    End If
                //
                //
                //    If txtDateFactureAu <> "" Then
                //        If sWhere = "" Then
                //            sWhere = " WHERE FactureManuelleEnTete.DateFacture  <='" & txtDateFactureAu & "'"
                //        Else
                //            sWhere = sWhere & " AND FactureManuelleEnTete.DateFacture  <='" & txtDateFactureAu & "'"
                //        End If
                //    End If
                //    If txtDateFactureDe <> "" Or txtDateFactureAu <> "" Then
                //            sSQL = sSQL & sWhere & ") DEV INNER JOIN DevisEntete ON DEV.NumeroDevis = DevisEntete.NumeroDevis"
                //    ElseIf sWhere <> "" Then
                General.sSQL = General.sSQL + sWhere;
                //    End If

                if (string.IsNullOrEmpty(sOrderBy))
                {
                    General.sSQL = General.sSQL + " ORDER BY  DevisEntete.DateCreation DESC";
                }
                else
                {
                    General.sSQL = General.sSQL + " " + sOrderBy;
                }

                if (!searchEntred)
                {
                    var r = Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez mis aucune condition, Ça risque de prendre du temps, souhaitez-vous continuer ?", "Souhaitez-vous continuer ?", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                    if (r == DialogResult.No)
                    {
                        Cursor = Cursors.Default;
                        return;
                    }
                }

                if (bExportDetail == true)
                {
                    modAdorsExportDetail = new ModAdo();
                    rsExportDetail = modAdorsExportDetail.fc_OpenRecordSet(General.sSQL);

                    if (rsExportDetail.Rows.Count == 0)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun devis pour ces paramétres.", "Présélection Dispatch", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }
                }
                else
                {

                    var modAdorsintervention = new ModAdo();
                    rsintervention = modAdorsintervention.fc_OpenRecordSet(General.sSQL);

                    if (rsintervention == null) return;
                    txtTotal.Text = General.nz(rsintervention.Rows.Count, "0").ToString();
                    this.ssIntervention.DataSource = rsintervention;
                    ssIntervention.UpdateData();

                    if (rsintervention.Rows.Count == 0)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun devis pour ces paramétres.", "Présélection Dispatch", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    }


                    return;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " cmbIntervention_Click ");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatus_TextChanged(object sender, EventArgs e)
        {
            using (var tmpModAdo = new ModAdo())
            {
                lblLibStatus.Text = tmpModAdo.fc_ADOlibelle("SELECT DevisCodeEtat.Libelle" + " FROM DevisCodeEtat WHERE DevisCodeEtat.Code='" + cmbStatus.Value + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatus_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmpModAdo = new ModAdo())
            {
                lblLibStatus.Text = tmpModAdo.fc_ADOlibelle("SELECT DevisCodeEtat.Libelle" + " FROM DevisCodeEtat WHERE DevisCodeEtat.Code='" + cmbStatus.Value + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatus_BeforeDropDown(object sender, CancelEventArgs e)
        {

            General.sSQL = "SELECT DevisCodeEtat.Code, DevisCodeEtat.Libelle" + " FROM DevisCodeEtat ORDER BY Code";
            if (cmbStatus.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(cmbStatus, General.sSQL, "Code");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatus2_TextChanged(object sender, EventArgs e)
        {
            using (var tmpModAdo = new ModAdo())
            {
                lblLibStatus2.Text = tmpModAdo.fc_ADOlibelle("SELECT DevisCodeEtat.Libelle" + " FROM DevisCodeEtat WHERE DevisCodeEtat.Code='" + cmbStatus2.Value + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatus2_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmpModAdo = new ModAdo())
            {
                lblLibStatus2.Text = tmpModAdo.fc_ADOlibelle("SELECT DevisCodeEtat.Libelle" + " FROM DevisCodeEtat WHERE DevisCodeEtat.Code='" + cmbStatus2.Value + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatus2_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT DevisCodeEtat.Code, DevisCodeEtat.Libelle" + " FROM DevisCodeEtat ORDER BY Code";
            if (cmbStatus2.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(cmbStatus2, General.sSQL, "Code");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdBordereauJournee_Click(object sender, EventArgs e)
        {

            //    sSql = "SELECT Intervention.DateRealise,Intervention.Intervenant," _
            //'            & " Intervention.CodeImmeuble, Intervention.CodeEtat, " _
            //'            & " Intervention.Designation, Intervention.Commentaire,   Intervention.Article, " _
            //'            & " Personnel.Nom, Intervention.CptREndu_INT, TypeCodeEtat.LibelleCodeEtat, Intervention.NoIntervention" _
            //'            & " , INT_Rapport1" _
            //'            & " FROM (TypeCodeEtat RIGHT JOIN Intervention ON TypeCodeEtat.CodeEtat = Intervention.CodeEtat)" _
            //'            & " LEFT JOIN Personnel ON Intervention.Intervenant = Personnel.Matricule"

            cmdClean_Click(cmdClean, new System.EventArgs());

            this.cmbCodeimmeuble.Value = "";
            this.cmbIntervenant.Value = "";
            this.cmbStatus.Value = "U";
            this.cmbStatus2.Text = "";
            this.txtinterDe.Text = "";
            this.txtIntereAu.Text = "";
            this.txtIntercom.Text = "";
            this.txtNoIntervention.Text = "";
            this.txtINT_AnaCode.Text = "";
            //        .ssComboActivite.value = ""
            this.cmbArticle.Value = "";
            this.txtDateSaisieDe.Text = "";
            this.txtDateSaisieFin.Text = "";

            //        .txtDatePrevue1.Text = ""
            //        .txtDatePrevue2.Text = ""
            //    sSql = sSql & " WHERE Intervention.DateRealise='" & Format(Date, FormatDateSQL) & "'"
            //    sSql = sSql & " order by  Intervention.DateRealise desc"
            //    Set rsintervention = fc_OpenRecordSet(sSql)
            //    If rsintervention.EOF And rsintervention.BOF Then
            //        MsgBox "Aucune intervention n'a été faite ce jour.", vbCritical, ""
            //    End If
            //    Me.ssIntervention.ReBind

            cmbIntervention_Click(cmbIntervention, new System.EventArgs());
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdClean_Click(object sender, EventArgs e)
        {

            this.txtDateSaisieDe.Text = "";
            this.txtDateSaisieFin.Text = "";
            this.txtinterDe.Text = "";
            this.txtIntereAu.Text = "";

            this.cmbStatus.Text = "";
            this.cmbStatus2.Text = "";
            this.cmbIntervenant.Text = "";
            this.cmbArticle.Text = "";
            this.txtIntercom.Text = "";
            this.cmbCodeimmeuble.Text = "";
            this.txtINT_AnaCodeF.Text = "";
            this.txtNoIntervention.Text = "";
            this.cmbArticleA.Text = "";
            this.txtCodeTitre.Text = "";
            this.txtCodeTitreImp.Text = "";
            this.txtCoefDe.Text = "";
            this.txtCoefA.Text = "";
            this.txtMontantA.Text = "";
            this.txtMontantDe.Text = "";
            this.txtNoIntervention.Text = "";
            this.cmbCommercial.Text = "";
            this.chkExclComm.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this.chkExclDeviseur.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this.chkExclGerant.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this.chkExclImm.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this.chkExclStatut.CheckState = System.Windows.Forms.CheckState.Unchecked;
            this.cmbCodeGerant.Text = "";
            this.cmbBe.Text = "";
            this.lblLibBE.Text = "";
            txtRespTrav.Text = "";
            CHKRenovationChauff.CheckState = System.Windows.Forms.CheckState.Unchecked;
            //==== MODIF du 13 02 2017 ajout du champs cloturé et receptionné.
            OptTousRecep.Checked = true;
            OptTousCloture.Checked = true;
            //==== FIN modif du 13 02 2017.
            txtNoEnregistrement.Text = "";
            chkSansContrat.CheckState = System.Windows.Forms.CheckState.Unchecked;
            txtConccurent.Text = "";
            txtGroupe.Text = "";
            txtINT_AnaCode.Text = "";
            txtCodeGestionnaire.Text = "";

            //===> Mondir le 04.02.2021 https://groupe-dt.mantishub.io/view.php?id=2250 and https://groupe-dt.mantishub.io/view.php?id=2251
            txtCodeArticle.Text = "";
            txtPoste.Text = "";
            //===> Fin Modif Mondir
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdConcurrent_Click(object sender, EventArgs e)
        {
            txtConccurent_KeyPress(txtConccurent, new System.Windows.Forms.KeyPressEventArgs((char)(13)));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdEditer_Click(object sender, EventArgs e)
        {
            if (chkSansContrat.Checked == true)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas imprimer cette liste  tant que la case 'Immeuble sans fiche contrat' est cochée.Pour éditer cette liste utilisez l'export Excel sinon décochez la case.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }


            cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            Impression();
        }
        /* Todo
         * j'ai teste cette fonction mais au niveau de ces composants 'txtCoefDe.Text' et  'txtCoefAu.Text' (mm chose pour cmbIntervenant_click)
         */
        private void Impression()
        {

            string sSelect = null;
            string sWhere = "";
            string sOrder = null;
            int nbHeures = 0;
            string sMatInitale = null;

            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                //  CR.Reset(); 

                ReportDocument CR;
                if (string.IsNullOrEmpty(General.strEtatReportDevis))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun état est prévu pour imprimer cet historique devis", "Impression de l'historique devis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (Dossier.fc_ControleFichier(General.gsRpt + General.strEtatReportDevis, false) == true)
                {
                    // CR.ReportFileName = General.gsRpt + General.strEtatReportDevis;
                    CR = new ReportDocument();
                    CR.Load(General.gsRpt + General.strEtatReportDevis);
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun état est présent pour imprimer cet historique devis", "Impression de l'historique devis", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                // CR.WindowShowPrintSetupBtn = true;


                sWhere = "";

                if (!string.IsNullOrEmpty(this.cmbCodeimmeuble.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.codeimmeuble}" + (chkExclImm.CheckState == 0 ? "=" : "<>") + "\"" + this.cmbCodeimmeuble.Text + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " And {DevisEntete.codeimmeuble}" + (chkExclImm.CheckState == 0 ? "=" : "<>") + "\"" + this.cmbCodeimmeuble.Text + "\"";
                    }
                }
                if (!string.IsNullOrEmpty(this.cmbCodeGerant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{Immeuble.code1}" + (chkExclGerant.CheckState == 0 ? "=" : "<>") + "\"" + this.cmbCodeGerant.Text + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " And {Immeuble.code1}" + (chkExclGerant.CheckState == 0 ? "=" : "<>") + "\"" + this.cmbCodeGerant.Text + "\"";
                    }
                }

                if (!string.IsNullOrEmpty(this.cmbIntervenant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.CodeDeviseur}" + (chkExclDeviseur.CheckState == 0 ? "=" : "<>") + "\"" + this.cmbIntervenant.Text + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.CodeDeviseur}" + (chkExclDeviseur.CheckState == 0 ? "=" : "<>") + "\"" + this.cmbIntervenant.Text + "\"";
                    }
                }

                if (!string.IsNullOrEmpty(this.cmbCommercial.Text))
                {
                    //==== modif le 25 10 2011, le commercial est liée à l'immeuble et non au client.
                    if (General.sVersionImmParComm == "1")
                    {

                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " {Immeuble.CodeCommercial} " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + this.cmbCommercial.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND {Immeuble.CodeCommercial} " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + this.cmbCommercial.Text + "'";
                        }

                    }
                    else
                    {


                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " {Table1.Commercial}" + (chkExclComm.CheckState == 0 ? "=" : "<>") + "\"" + this.cmbCommercial.Text + "\"";
                        }
                        else
                        {
                            sWhere = sWhere + " AND {Table1.Commercial}" + (chkExclComm.CheckState == 0 ? "=" : "<>") + "\"" + this.cmbCommercial.Text + "\"";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(txtINT_AnaCode.Text))
                {
                    //==== modif le 25 10 2011, le chef de secteur est attaché à l'immeuble.
                    if (General.sVersionImmParComm == "1")
                    {
                        using (var tmpModAdo = new ModAdo())
                        {
                            sMatInitale = tmpModAdo.fc_ADOlibelle("SELECT MATRICULE FROM PERSONNEL WHERE Initiales ='" + txtINT_AnaCode.Text + "'");
                            //=== modif temporaire à mettre à jour.
                            if (General.sRespExploitParImmeuble == "1")
                            {

                                if (string.IsNullOrEmpty(sWhere))
                                {
                                    sWhere = " {Immeuble.CodeRespExploit} ='" + sMatInitale + "'";
                                }
                                else
                                {
                                    sWhere = sWhere + " AND {Immeuble.CodeRespExploit} ='" + sMatInitale + "'";
                                }

                            }
                            else if (General.sChefDeSecteurParImmeuble == "1")
                            {

                                if (string.IsNullOrEmpty(sWhere))
                                {
                                    sWhere = " {Immeuble.CodeChefSecteur} ='" + sMatInitale + "'";
                                }
                                else
                                {
                                    sWhere = sWhere + " AND {Immeuble.CodeChefSecteur} ='" + sMatInitale + "'";
                                }

                            }
                            else
                            {

                                if (string.IsNullOrEmpty(sWhere))
                                {
                                    sWhere = " {Personnel_2.CRespExploit} = '" + this.txtINT_AnaCode.Text + "'";
                                }
                                else
                                {
                                    sWhere = sWhere + " AND  {Personnel_2.CRespExploit} = '" + this.txtINT_AnaCode.Text + "'";
                                }
                            }
                        }
                    }
                    else
                    {


                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " {Personnel_2.CRespExploit} = '" + this.txtINT_AnaCode.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND  {Personnel_2.CRespExploit} = '" + this.txtINT_AnaCode.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(this.cmbStatus.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.CodeEtat}" + (chkExclStatut.CheckState == 0 ? "=" : "<>") + "\"" + this.cmbStatus.Text + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.CodeEtat}" + (chkExclStatut.CheckState == 0 ? "=" : "<>") + "\"" + this.cmbStatus.Text + "\"";
                    }
                }

                if (!string.IsNullOrEmpty(this.cmbStatus2.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.CodeEtat}" + (chkExclStatut2.CheckState == 0 ? "=" : "<>") + "\"" + this.cmbStatus2.Text + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.CodeEtat}" + (chkExclStatut2.CheckState == 0 ? "=" : "<>") + "\"" + this.cmbStatus2.Text + "\"";
                    }
                }


                if (!string.IsNullOrEmpty(this.txtinterDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.DateCreation}>=dateTime(" + Convert.ToDateTime(this.txtinterDe.Text) + ")";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.DateCreation}>=dateTime(" + Convert.ToDateTime(this.txtinterDe.Text) + ")";
                    }
                }



                if (!string.IsNullOrEmpty(this.txtIntereAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.DateCreation}<=dateTime(" + Convert.ToDateTime(this.txtIntereAu.Text) + ")";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.DateCreation}<=dateTime(" + Convert.ToDateTime(this.txtIntereAu.Text) + ")";
                    }
                }

                if (!string.IsNullOrEmpty(this.txtIntercom.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.TitreDevis} like \"*" + this.txtIntercom.Text + "*\"";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.TitreDevis} LIKE \"*" + this.txtIntercom.Text + "*\"";
                    }
                }

                if (!string.IsNullOrEmpty(txtNoIntervention.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.NumeroDevis}=\"" + this.txtNoIntervention.Text + "\"";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.NumeroDevis}=\"" + this.txtNoIntervention.Text + "\"";
                    }
                }

                if (!string.IsNullOrEmpty(this.txtCP.Text))
                {
                    if (txtCP.Text.Length == 2)
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE {Immeuble.CodePostal} LIKE \"" + this.txtCP.Text + "*\"";
                        }
                        else
                        {
                            sWhere = sWhere + " AND {Immeuble.CodePostal} LIKE \"" + this.txtCP.Text + "*\"";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE {Immeuble.CodePostal} ='" + this.txtCP.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND {Immeuble.CodePostal} ='" + this.txtCP.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(txtConccurent.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE {DEC_DevisConccurent.DEC_Code} = '" + txtConccurent.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and {DEC_DevisConccurent.DEC_Code} = '" + txtConccurent.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtGroupe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "  WHERE Table1.CRCL_Code='" + StdSQLchaine.gFr_DoublerQuote(txtGroupe.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Table1.CRCL_Code ='" + StdSQLchaine.gFr_DoublerQuote(txtGroupe.Text) + "'";
                    }
                }


                if (string.IsNullOrEmpty(txtCodeTitreImp.Text))
                {
                    if (!string.IsNullOrEmpty(this.cmbArticle.Text) && string.IsNullOrEmpty(this.cmbArticleA.Text))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = "{DevisEntete.CodeTitre}=\"" + this.cmbArticle.Text + "\"";
                        }
                        else
                        {
                            sWhere = sWhere + " AND {DevisEntete.CodeTitre}=\"" + this.cmbArticle.Text + "\"";
                        }
                    }
                    else if (string.IsNullOrEmpty(this.cmbArticle.Text) && !string.IsNullOrEmpty(this.cmbArticleA.Text))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " {DevisEntete.CodeTitre}=\"" + this.cmbArticleA.Text + "\"";
                        }
                        else
                        {
                            sWhere = sWhere + " AND {DevisEntete.CodeTitre}=\"" + this.cmbArticleA.Text + "\"";
                        }
                    }
                    else if (!string.IsNullOrEmpty(this.cmbArticle.Text) && !string.IsNullOrEmpty(this.cmbArticleA.Text))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " {DevisEntete.CodeTitre}>=\"" + this.cmbArticle.Text + "\" AND DevisEntete.CodeTitre<=\"" + this.cmbArticleA.Text + "\"";
                        }
                        else
                        {
                            sWhere = sWhere + " AND {DevisEntete.CodeTitre}>=\"" + this.cmbArticle.Text + "\" AND {DevisEntete.CodeTitre}<=\"" + this.cmbArticleA.Text + "\"";
                        }
                    }
                }
                else
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = txtCodeTitreImp.Text;
                    }
                    else
                    {
                        sWhere = sWhere + " AND " + txtCodeTitreImp.Text;
                    }
                }


                //     If .cmbArticle <> "" Then
                //        If sWhere = "" Then
                //            sWhere = "{DevisEntete.CodeTitre}=""" & .cmbArticle & """"
                //        Else
                //            sWhere = sWhere & " AND {DevisEntete.CodeTitre}=""" & .cmbArticle & """"
                //        End If
                //    End If

                if (!string.IsNullOrEmpty(this.txtDateSaisieDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = "{DevisEntete.DateAcceptation}>=dateTime(" + this.txtDateSaisieDe.Text + ")";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.DateAcceptation}>=dateTime(" + this.txtDateSaisieDe.Text + ")";

                    }
                }
                if (!string.IsNullOrEmpty(this.txtDateSaisieFin.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + "{DevisEntete.DateAcceptation}<=dateTime(" + this.txtDateSaisieFin.Text + " )";
                    }
                    else
                    {

                        sWhere = sWhere + " AND {DevisEntete.DateAcceptation}<=dateTime(" + this.txtDateSaisieFin.Text + ")";
                    }
                }
                //DevisEntete.TotalHT}/{DevisEntete.TotalAvantCoef ==> si TotalAvantCoef=0 divison par zero mm chose cmbIntervention_click
                if (!string.IsNullOrEmpty(txtCoefDe.Text) && General.IsNumeric(txtCoefDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = "({DevisEntete.TotalHT}/{DevisEntete.TotalAvantCoef})>=" + General.nz(txtCoefDe.Text, "0") + " AND {DevisEntete.TotalAvantCoef}<>0 AND ISNUMERIC(cstr({DevisEntete.TotalAvantCoef}))=True";
                    }
                    else
                    {

                        sWhere = sWhere + " AND ({DevisEntete.TotalHT}/{DevisEntete.TotalAvantCoef})>=" + General.nz(txtCoefDe.Text, "0") + " AND {DevisEntete.TotalAvantCoef}<>0 AND ISNUMERIC(cstr({DevisEntete.TotalAvantCoef}))=True";
                    }
                }
                //DevisEntete.TotalHT}/{DevisEntete.TotalAvantCoef ==> si TotalAvantCoef=0 divison par zero mm chose cmbIntervention_click
                if (!string.IsNullOrEmpty(txtCoefA.Text) && General.IsNumeric(txtCoefA.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " ({DevisEntete.TotalHT}/{DevisEntete.TotalAvantCoef})<=" + General.nz(txtCoefA.Text, "0") + " AND {DevisEntete.TotalAvantCoef}<>0 AND ISNUMERIC(cstr({DevisEntete.TotalAvantCoef}))=True";
                    }
                    else
                    {

                        sWhere = sWhere + " AND ({DevisEntete.TotalHT}/{DevisEntete.TotalAvantCoef})<=" + General.nz(txtCoefA.Text, "0") + " AND {DevisEntete.TotalAvantCoef}<>0 AND ISNUMERIC(cstr({DevisEntete.TotalAvantCoef}))=True";
                    }
                }

                if (!string.IsNullOrEmpty(txtMontantDe.Text) && General.IsNumeric(txtMontantDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " ({DevisEntete.TotalHT})>=" + General.nz(txtMontantDe.Text, "0") + "";
                    }
                    else
                    {

                        sWhere = sWhere + " AND ({DevisEntete.TotalHT})>=" + General.nz(txtMontantDe.Text, "0") + "";
                    }
                }

                if (!string.IsNullOrEmpty(txtMontantA.Text) && General.IsNumeric(txtMontantA.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE ({DevisEntete.TotalHT})<=" + General.nz(txtMontantA.Text, "0") + "";
                    }
                    else
                    {

                        sWhere = sWhere + " AND ({DevisEntete.TotalHT})<=" + General.nz(txtMontantA.Text, "0") + "";
                    }
                }

                if (this.CHKRenovationChauff.Checked == true)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{DevisEntete.RenovationChauff} = 1";
                    }
                    else
                    {
                        sWhere = sWhere + " And {DevisEntete.RenovationChauff} = 1";
                    }
                }

                //==== MODIF du 13 02 2017 ajout du champs cloturé et receptionné.
                if (optOuiRecept.Checked == true)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE {DevisEntete.Receptionne} = 1";
                    }
                    else
                    {
                        sWhere = sWhere + " and {DevisEntete.Receptionne} = 1";
                    }
                }
                else if (OptNonRecept.Checked == true)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE {(DevisEntete.Receptionne = 0 or DevisEntete.Receptionne is null)}";
                    }
                    else
                    {
                        sWhere = sWhere + " and {(DevisEntete.Receptionne = 0 or DevisEntete.Receptionne is null)}";
                    }
                }

                if (OptOuiCloture.Checked == true)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE {DevisEntete.ClotureDevis = 1}";
                    }
                    else
                    {
                        sWhere = sWhere + " and {DevisEntete.ClotureDevis = 1}";
                    }
                }
                else if (OptNonCloture.Checked == true)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE {(DevisEntete.ClotureDevis = 0 or DevisEntete.ClotureDevis is null)}";
                    }
                    else
                    {
                        sWhere = sWhere + " and {(DevisEntete.ClotureDevis = 0 or DevisEntete.ClotureDevis is null)}";
                    }
                }
                //==== FIN modif du 13 02 2017.


                //CR.SortFields(0) = "+{Intervention.CodeImmeuble}"
                //CR.SortFields(1) = "+{Intervention.DateSaisie}"
                //CR.SortFields(2) = "+{Intervention.CodeEtat}"

                //Debug.Print err.Description

                //  CR.Action = 1;
                var FormCR = new CrystalReportFormView(CR, sWhere);
                FormCR.Show();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Impression");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExportExcel_Click(object sender, EventArgs e)
        {
            Excel.Application oXL;
            Excel.Workbook oWB;
            Excel.Worksheet oSheet;
            Excel.Range oRng;
            Excel.Range oResizeRange;
            DataTable rsExport = default(DataTable);
            SqlDataAdapter SDArsExport;
            //ADODB.Field oField = default(ADODB.Field);


            int i = 0;
            int j = 0;
            string sMatricule = null;
            int lLigne = 0;

            if (ModAutorisation.fc_DroitDetail("ExportDevis") == 0)
            {
                return;
            }

            // ERROR: Not supported in C#: OnErrorStatement


            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
            // Start Excel and get Application object.
            oXL = new Excel.Application();
            oXL.Visible = false;

            // Get a new workbook.
            oWB = oXL.Workbooks.Add();
            oSheet = oWB.ActiveSheet;
            ModAdo rs = new ModAdo();
            rsExport = rs.fc_OpenRecordSet(General.sSQL);
            var _with14 = rsExport;
            try
            {
                if (_with14.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour ces critères.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }


                //== affiche les noms des champs.

                i = 1;
                foreach (DataColumn oField in rsExport.Columns)
                {
                    oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Cells[1, i].value = oField.ColumnName;
                    i = i + 1;
                }


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                // Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Interior.ColorIndex = 15;
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                lLigne = 3;

                //_with14.MoveFirst();

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                foreach (DataRow rsExportRows in _with14.Rows)
                {
                    j = 1;
                    //  oSheet.Cells(lLigne, 1).value = sANT_MatCode
                    foreach (DataColumn rsExportCol in _with14.Columns)
                    {

                        oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                        j = j + 1;
                    }
                    lLigne = lLigne + 1;
                    //_with14.MoveNext();
                }

                ///oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oXL.Visible = true;
                oXL.UserControl = true;
                oRng = null;
                oSheet = null;
                oWB = null;
                //oXL.Quit();
                oXL = null;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                //_with14.Close();
                rsExport = null;
                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Error: ", "" + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExportDetail_Click(object sender, EventArgs e)
        {
            Excel.Application oXL;
            //As Excel.application
            Excel.Workbook oWB;
            //As Excel.Workbook
            Excel.Worksheet oSheet;
            //As Excel.Worksheet
            Excel.Range oRng;
            //As Excel.Range
            Excel.Range oResizeRange;
            //As Excel.Range
            DataTable rsExport;
            //As ADODB.Recordset
            //DataColumn oField;
            //As ADODB.Field

            int i = 0;
            int j = 0;
            string sMatricule;
            string sSQLDev;
            DataTable rsDev = new DataTable();
            int lLigne = 0;
            try
            {

                if (ModAutorisation.fc_DroitDetail("ExportDevis") == 0)
                {
                    return;
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                // Start Excel and get Application object.
                oXL = new Excel.Application();
                oXL.Visible = false;
                // Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;

                bExportDetail = true;
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                bExportDetail = false;
                //rsExportDetail = new DataTable();

                var _with13 = rsExportDetail;
                if (_with13.Rows.Count == 0)
                {
                    //MsgBox "Aucun enregistrement pour ces critères.", vbInformation, ""
                    return;
                }


                //== affiche les noms des champs.
                i = 1;
                foreach (DataColumn oField in _with13.Columns)
                {
                    //  oSheet.Cells(1, i).HorizontalAlignment = xlVAlignCenter
                    oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Cells[1, i].value = oField.ColumnName;
                    i = i + 1;
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                // Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Interior.ColorIndex = 15;
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                //===> Mondir le 09.04.2021, commented line is old https://groupe-dt.mantishub.io/view.php?id=2400#c5818
                //lLigne = 3;
                lLigne = 2;
                //_with13.MoveFirst();

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                foreach (DataRow rsExportDetailRow in _with13.Rows)
                {
                    //if (General.fncUserName().ToUpper() == "mondir".ToUpper() ||
                    //    General.fncUserName().ToUpper() == "axeciel-user".ToUpper() ||
                    //    General.fncUserName().ToUpper() == "axeciel-user2".ToUpper())
                    //{
                    //    var tAffaire = new int[1];
                    //    tAffaire[0] = rsExportDetailRow["NumFicheStandard"].ToInt();

                    //    var codeImmeuble = rsExportDetailRow["CodeImmeuble"]?.ToString();
                    //    var gerant = rsExportDetailRow["Code1"]?.ToString();

                    //    var lETAE_NoAuto =
                    //        ModAnalytique.fc_AnalyAffaire(tAffaire, codeImmeuble, gerant, 0, false, "", false);

                    //    var lblChargesPrevues =
                    //        Convert.ToString(ModAnalytique.tCalcaffaire[0].dbMoprevue +
                    //                         ModAnalytique.tCalcaffaire[0].dbAchatPrevue +
                    //                         ModAnalytique.tCalcaffaire[0].dbSstPrevue);
                    //    var lblVentesPrevues = Convert.ToString(ModAnalytique.tCalcaffaire[0].dbVentesPrevues);
                    //}

                    j = 1;
                    sSQLDev = "SELECT  top 1   Intervention.DateRealise "
                        + "  FROM GestionStandard INNER JOIN "
                        + "Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard "
                        + " WHERE     (GestionStandard.NoDevis = '" + General.nz(rsExportDetail.Rows[0]["numerodevis"], 0) + "') "
                        + " AND (Intervention.DateRealise IS NOT NULL) "
                        + " ORDER BY Intervention.DateRealise";
                    ModAdo tmpModAdo = new ModAdo();
                    rsDev = tmpModAdo.fc_OpenRecordSet(sSQLDev);
                    sSQLDev = "";

                    if (rsDev.Rows.Count > 0)
                    {
                        if (General.IsDate(rsDev.Rows[0]["DateRealise"].ToString()))
                        {
                            //===> Mondir le 12.02.2021 https://groupe-dt.mantishub.io/view.php?id=2187
                            //sSQLDev = Convert.ToDateTime(rsDev.Rows[0]["DateRealise"]).ToString("dd/mm/yyyy");
                            sSQLDev = Convert.ToDateTime(rsDev.Rows[0]["DateRealise"]).ToString("dd/MM/yyyy");
                            //===> Fin Modif Mondir
                        }
                    }
                    tmpModAdo.Dispose();
                    rsDev = null;


                    //  oSheet.Cells(lLigne, 1).value = sANT_MatCode
                    foreach (DataColumn oField in _with13.Columns)
                    {

                        if ("dateRealisation".ToUpper() == oField.ColumnName.ToUpper())
                        {
                            //oSheet.Cells[lLigne, j + 0].NumberFormat = "dd/mm/yyyy;@";
                            oSheet.Cells[lLigne, j + 0].value = sSQLDev;
                        }
                        else
                        {
                            oSheet.Cells[lLigne, j + 0].value = rsExportDetailRow[oField.ColumnName];
                        }

                        j = j + 1;
                    }
                    lLigne = lLigne + 1;
                }
                // _with13.MoveNext();

                //  oSheet.Cells(1, i).AutoFit

                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                //    numRows = Selection.Rows.Count
                //numColumns = Selection.Columns.Count
                //Selection.Resize(numRows + 1, numColumns + 1).Select
                //
                //    oResizeRange.Resize(lLigne, j).Select

                oXL.Visible = true;
                oXL.UserControl = true;
                oRng = null;
                oSheet = null;
                oWB = null;
                //oXL.Quit();
                oXL = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                _with13.Dispose();
                _with13 = null;
                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);

                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Error: " + ex.Message, ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            bExportDetail = false;

        }

        private void cmdFindGestionnaire_Click(object sender, EventArgs e)
        {
            txtCodeGestionnaire_KeyPress(txtCodeGestionnaire, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }

        private void cmdMultiSelect_Click(object sender, EventArgs e)
        {
            int i = 0;
            Views.BaseDeDonnees.Gerant.Forms.frmSelect frmSelect = new Views.BaseDeDonnees.Gerant.Forms.frmSelect();
            frmSelect.txtSelect.Text = "SELECT '' As Sélection, FacArticle.CodeArticle AS \"Code Titre\",Facarticle.Designation1 as Designation FROM FacArticle";
            frmSelect.txtWhere.Text = " WHERE FacArticle.CodeCategorieArticle='D' OR FacArticle.CodeCategorieArticle='P'";
            frmSelect.ShowDialog();
            //TODO : Salma  
            //Il faut vérifier la frmSelect 
            // int s = frmSelect.GridSelect.Rows.Count; normalemnet le valeur de s doit etre =641

            if (frmSelect.chkAnnuler.Checked == false)
            {
                var _with15 = frmSelect;
                // _with15.GridSelect.MoveFirst();

                txtCodeTitre.Text = "";
                txtCodeTitreImp.Text = "";
                for (i = 0; i < _with15.GridSelect.Rows.Count; i++)
                {

                    if (_with15.GridSelect.DisplayLayout.Bands[0].Columns[0].Key == "1" || _with15.GridSelect.DisplayLayout.Bands[0].Columns[0].Key == "-1")
                    {
                        if (!string.IsNullOrEmpty(txtCodeTitre.Text))
                        {
                            txtCodeTitre.Text = txtCodeTitre.Text + " AND ";
                        }
                        if (!string.IsNullOrEmpty(txtCodeTitreImp.Text))
                        {
                            txtCodeTitreImp.Text = txtCodeTitreImp.Text + " AND ";
                        }
                        txtCodeTitre.Text = txtCodeTitre.Text + " DevisEntete.CodeTitre='" + _with15.GridSelect.DisplayLayout.Bands[0].Columns[1].Key + "'";
                        txtCodeTitreImp.Text = txtCodeTitreImp.Text + " {DevisEntete.CodeTitre}=\"" + _with15.GridSelect.DisplayLayout.Bands[0].Columns[1].Key + "\"";
                        // _with15.GridSelect.MoveNext();
                    }
                }
                cmbArticle.Text = "Multi";
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
            else
            {
                txtCodeTitre.Text = "";
            }
            frmSelect.ShowDialog();
            frmSelect.Close();

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheCommercial_Click(object sender, EventArgs e)
        {
            cmbCommercial.Text = "";
            cmbCommercial_KeyPress(cmbCommercial, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            cmbCodeimmeuble_KeyPress(cmbCodeimmeuble, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheIntervenant_Click(object sender, EventArgs e)
        {
            cmbIntervenant_KeyPress(cmbIntervenant, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheTypeOperation_Click(object sender, EventArgs e)
        {
            cmbArticle_KeyPress(cmbArticle, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheTypeOperationA_Click(object sender, EventArgs e)
        {
            cmbArticleA_KeyPress(cmbArticleA, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }

        private void CmdSauver_Click(object sender, EventArgs e)
        {

            try
            {
                ssIntervention.UpdateData();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "CmdSauver");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            txtRespTrav_KeyPress(txtRespTrav, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }

        private void UserDispatchDevisLoad1(Control C)
        {
            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    UserDispatchDevisLoad1(CC);

            if (C is UltraGrid)
            {
                sheridan.fc_DeleteRegGrille(Variable.cUserDocDevis, C as UltraGrid, false, true);
                //sheridan.fc_LoadDimensionGrille(this.Name, C as UltraGrid);

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="C"></param>
        private void UserDispatchDevisLoad2(Control C)
        {
            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    UserDispatchDevisLoad2(CC);

            if (C is UltraGrid)
            {
                //sheridan.fc_LoadDimensionGrille(this.Name, C as UltraGrid);

            }
        }
        private void loopControl(Control C)
        {
            ///this function Added By Mohammed
            foreach (Control c in C.Controls)
                loopControl(c);
            if (C is UltraGrid)
            {
                //sheridan.fc_SavDimensionGrille(this.Name, C as UltraGrid);
            }
        }
        private void UserDispatchDevis_Load(object sender, EventArgs e)
        {
            /////this event  added by moahammed
            //this.ParentForm.FormClosing += (se, ev) =>
            //{
            //    loopControl(this);
            //};
            //TODO / Salma :: l'erreur vient de la fonction getFrmReg 
            if (General.fc_InitRegGrilles(this.Name) == true)
            {
                UserDispatchDevisLoad1(this);
            }
            else
            {
                UserDispatchDevisLoad2(this);
            }

            /*
             * TODO : Salma 
             * 
              if (!string.IsNullOrEmpty(General.LogoSociete) | !Information.IsDBNull(General.LogoSociete))
              {
                  imgLogoSociete.Image = System.Drawing.Image.FromFile(General.LogoSociete);
                  if (Information.IsNumeric(General.imgTop))
                  {
                      imgLogoSociete.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(General.imgTop);
                  }
                  if (Information.IsNumeric(General.imgLeft))
                  {
                      imgLogoSociete.Left = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsX(General.imgLeft);
                  }
              }*/

            bExportDetail = false;

            //TODO : modification par mohammed .
            mnuAnalytique.Items.Add("analytique");
        }





        /// <summary>
        /// Tested
        /// </summary>
        private void fc_SaveSetting()
        {
            string sRecept = null;
            string sCloture = null;



            var _with19 = this;
            try
            {
                if (optOuiRecept.Checked == true)
                {
                    sRecept = cOuiRecept;
                }
                else if (OptNonRecept.Checked == true)
                {
                    sRecept = cNonRecept;
                }
                else if (OptTousRecep.Checked == true)
                {
                    sRecept = cTousRecept;
                }

                if (OptOuiCloture.Checked == true)
                {
                    sCloture = cOuiCLot;
                }
                else if (OptNonCloture.Checked == true)
                {
                    sCloture = cNonClot;
                }
                else if (OptTousCloture.Checked == true)
                {
                    sCloture = cTousClot;
                }


                //=== stocke la position de la fiche immeuble
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDispatchDevis, _with19.ssIntervention.ActiveRow.Cells["numerodevis"].Value.ToString(), "", _with19.txtinterDe.Text, _with19.txtIntereAu.Text, _with19.cmbStatus.Text, _with19.cmbIntervenant.Text, cmbArticle.Text, _with19.txtIntercom.Text,
                _with19.cmbCodeimmeuble.Text, "", txtINT_AnaCode.Text, _with19.txtDateSaisieDe.Text, _with19.txtDateSaisieFin.Text, "", "", txtNoIntervention.Text, Convert.ToString(CHKRenovationChauff.Checked), txtNoEnregistrement.Text,
                Convert.ToString(chkSansContrat.Checked), sRecept, sCloture, cmbStatus2.Text);

                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_SaveSetting");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_KeyPress(object sender, KeyPressEventArgs e)
        {
            blnKeyPress = true;
        }

        private void txtCodeGestionnaire_TextChanged(object sender, EventArgs e)
        {
            // à verfier le lablel !!!!!! ==> label inexistant dans le vb6
            /* using (var tmpAdo = new ModAdo())
             {

                 lblLibGestionnaire.Text = tmpAdo.fc_ADOlibelle("SELECT NomGestion" + " FROM Gestionnaire WHERE CodeGestinnaire='" + txtCodeGestionnaire.Text + "'");
             }*/
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeGestionnaire_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    requete = "SELECT    Gestionnaire.CodeGestinnaire as \"Code Gestionnaire\"," + " Gestionnaire.NomGestion as \"Nom Gestionnaire\"," + " Immeuble.CodeImmeuble as \"Code Immeuble\"," + " Gestionnaire.Code1 as \"code Gerant\"," + " Table1.Nom as \"Nom Gerant\"" + " FROM Gestionnaire " + " INNER JOIN" + " Immeuble ON Gestionnaire.CodeGestinnaire = Immeuble.CodeGestionnaire" + " INNER JOIN" + " Table1 ON Gestionnaire.Code1 = Table1.Code1";
                    SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'un Gestionnaire" };
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        txtCodeGestionnaire.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fg.Dispose(); fg.Close();
                    };
                    fg.ugResultat.KeyDown += (se, ev) =>
                    {
                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            txtCodeGestionnaire.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fg.Dispose(); fg.Close();
                        }

                    };
                    fg.StartPosition = FormStartPosition.CenterScreen;
                    fg.ShowDialog();

                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";txtCodeGestionnaire_KeyPress");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtConccurent_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    General.sSQL = "SELECT CNC_Code as \"Code\", CNC_Libelle as \"Libelle\"" + " FROM CNC_Conccurent";
                    SearchTemplate fg = new SearchTemplate(this, null, General.sSQL, where, "") { Text = "Recherche d'un Conccurent" };
                    fg.SetValues(new Dictionary<string, string> { { "CNC_Code", txtConccurent.Text } });

                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        txtConccurent.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fg.Dispose(); fg.Close();
                    };
                    fg.ugResultat.KeyDown += (se, ev) =>
                    {
                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            txtConccurent.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fg.Dispose(); fg.Close();
                        }

                    };
                    fg.StartPosition = FormStartPosition.CenterScreen;
                    fg.ShowDialog();
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";txtConccurent_KeyPress");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtINT_AnaCode_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;

            //    sSql = "Select sec_Code, Sec_Libelle from SEC_Secteur "
            //    sSQl = "Select Personnel.Initiales,Personnel.Nom " _
            //& " FROM Personnel WHERE Personnel.Initiales = Personnel.CSecteur "
            sSQL = "SELECT  Personnel.Initiales, Personnel.Nom"
                + " FROM         Qualification LEFT OUTER JOIN" + " SpecifQualif ON "
                + " Qualification.CodeQualif = SpecifQualif.CodeQualif " + " RIGHT OUTER JOIN"
                + " Personnel ON Qualification.CodeQualif = Personnel.CodeQualif"
                + " WHERE     (SpecifQualif.QualifRExp = 1) ";
            sheridan.InitialiseCombo(txtINT_AnaCode, sSQL, "Initiales");
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNoIntervention_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT DISTINCT DevisEntete.NumeroDevis" + " From DevisEntete" + " order by NumeroDevis";
            sheridan.InitialiseCombo(this.txtNoIntervention, General.sSQL, "NumeroDevis");
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtNoIntervention_KeyPress(object sender, KeyPressEventArgs e)
        {
            short keyAscii = (short)e.KeyChar;
            if (keyAscii == 13)
            {
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
        }

        private void UserDispatchDevis_VisibleChanged(object sender, EventArgs e)
        {
            string sNoIntervention = null;
            string sRecept = null;
            string sCloture = null;

            if (!Visible)
            {
                //loopControl(this);
                return;
            }



            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDispatchDevis");
            //=== version dédié à pz.
            if (General.sActiveVersionPz == "1")
            {
                label101.Visible = false;
                txtCoefDe.Visible = false;
                txtCoefA.Visible = false;
                label102.Visible = false;
                //label24.Visible = False
                txtINT_AnaCode.Visible = false;
                label6.Visible = false;
                cmbArticle.Visible = false;
                cmdRechercheTypeOperation.Visible = false;
                cmdMultiSelect.Visible = false;
                label9.Visible = false;
                cmbArticleA.Visible = false;
                cmdRechercheTypeOperationA.Visible = false;
                // Line1.Visible = false;
                // Line2.Visible = false;
                // Line4.Visible = false;
                label20.Visible = false;
                txtConccurent.Visible = false;
                cmdConcurrent.Visible = false;
                label21.Visible = false;
                txtRespTrav.Visible = false;
                Command1.Visible = false;
                chkExclRespTravaux.Visible = false;
                label3.Text = "N°Enregistrement";
                txtNoEnregistrement.Visible = true;
                //txtIntercom.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(2340);
                // label10.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(2340);
                // cmbBe.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(2700);
                // label8.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(2700);
                // 3480
                // lblLibBE.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(2700);
                //label7.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(3120);
                // txtCodeGestionnaire.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(3120);
                //cmdFindGestionnaire.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(3120);
                // chkExclGestionnaire.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(3120);
                CHKRenovationChauff.Visible = false;
                chkSansContrat.Visible = false;
                label10.Location = new Point(3, 195);
                tableLayoutPanel2.Controls.Add(label10);

            }
            else
            {
                label1.Visible = true;
                txtCoefDe.Visible = true;
                txtCoefA.Visible = true;
                label11.Visible = true;
                //label24.Visible = True
                txtINT_AnaCode.Visible = true;
                label6.Visible = true;
                cmbArticle.Visible = true;
                cmdRechercheTypeOperation.Visible = true;
                cmdMultiSelect.Visible = true;
                label9.Visible = true;
                cmbArticleA.Visible = true;
                cmdRechercheTypeOperationA.Visible = true;
                //Line1.Visible = true;
                // Line2.Visible = true;
                // Line4.Visible = true;
                // Label22.Visible = true;
                txtConccurent.Visible = true;
                cmdConcurrent.Visible = true;
                label21.Visible = true;
                txtRespTrav.Visible = true;
                Command1.Visible = true;
                chkExclRespTravaux.Visible = true;
                label3.Text = "Responsable Exploitation";
                txtNoEnregistrement.Visible = false;
                //txtIntercom.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(3120);
                //label10.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(3120);
                // cmbBe.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(3480);
                //label8.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(3480);
                //lblLibBE.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(3480);
                //label7.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(3840);
                //txtCodeGestionnaire.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(3840);
                //cmdFindGestionnaire.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(3840);
                //chkExclGestionnaire.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(3840);
                CHKRenovationChauff.Visible = true;
                chkSansContrat.Visible = true;

            }

            bExportDetail = false;

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            General.open_conn();

            if (!string.IsNullOrEmpty(General.NomSousSociete) && General.NomSousSociete != null)
            {
                //TODO  
                //lblNomSociete.Text = General.NomSousSociete;
                System.Windows.Forms.Application.DoEvents();
            }
            fc_DropCodeetat();
            // fc_DropMaticule &H00C0C0C0&

            var _with27 = this;
            //=== positionne sur l'historique.

            _with27.txtinterDe.Text = General.getFrmReg(Variable.cUserDispatchDevis, "De", "");
            _with27.txtIntereAu.Text = General.getFrmReg(Variable.cUserDispatchDevis, "Au", "");
            _with27.cmbStatus.Text = General.getFrmReg(Variable.cUserDispatchDevis, "Status", "");
            _with27.cmbStatus2.Text = General.getFrmReg(Variable.cUserDispatchDevis, "statut2", "");

            if (!string.IsNullOrEmpty(General.getFrmReg(Variable.cUserDispatchDevis, "Status", "")))
            {
                var tmpAdo = new ModAdo();
                _with27.lblLibStatus.Text = tmpAdo.fc_ADOlibelle("SELECT DevisCodeEtat.Libelle" + " FROM DevisCodeEtat WHERE DevisCodeEtat.Code='" + cmbStatus.Value + "'");
            }

            if (!string.IsNullOrEmpty(cmbStatus2.Text))
            {
                var tmpAdo = new ModAdo();
                _with27.lblLibStatus2.Text = tmpAdo.fc_ADOlibelle("SELECT DevisCodeEtat.Libelle" + " FROM DevisCodeEtat WHERE DevisCodeEtat.Code='" + cmbStatus2.Value + "'");
            }
            _with27.cmbIntervenant.Text = General.getFrmReg(Variable.cUserDispatchDevis, "Intervenant", "");
            _with27.cmbArticle.Text = General.getFrmReg(Variable.cUserDispatchDevis, "Article", "");
            _with27.txtIntercom.Text = General.getFrmReg(Variable.cUserDispatchDevis, "Commentaire", "");
            _with27.cmbCodeimmeuble.Text = General.getFrmReg(Variable.cUserDispatchDevis, "Immeuble", "");
            //    .ssComboActivite = GetSetting(cFrNomApp, cUserDispatchDevis, "Activite", "")
            _with27.txtINT_AnaCode.Text = General.getFrmReg(Variable.cUserDispatchDevis, "responsableExploit", "");
            _with27.txtDateSaisieDe.Text = General.getFrmReg(Variable.cUserDispatchDevis, "saisieDe", "");
            _with27.txtDateSaisieFin.Text = General.getFrmReg(Variable.cUserDispatchDevis, "saisieAu", "");
            //    .txtDatePrevue1 = GetSetting(cFrNomApp, cUserDispatchDevis, "PrevueDe", "")
            //    .txtDatePrevue2 = GetSetting(cFrNomApp, cUserDispatchDevis, "PrevueAu", "")
            txtNoIntervention.Text = General.getFrmReg(Variable.cUserDispatchDevis, "NumeroDevis", "");
            CHKRenovationChauff.CheckState = General.nz(General.getFrmReg(Variable.cUserDispatchDevis, "CHKRenovationChauff", Convert.ToString(0)), 0).ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;
            chkSansContrat.CheckState = General.nz(General.getFrmReg(Variable.cUserDispatchDevis, "chkSansContrat", Convert.ToString(0)), 0).ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;
            //==== MODIF du 13 02 2017 ajout du champs cloturé et receptionné.
            sRecept = General.getFrmReg(Variable.cUserDispatchDevis, "Receptionne", "");
            if (sRecept.ToUpper().ToString() == cOuiRecept.ToUpper().ToString())
            {
                optOuiRecept.Checked = true;
            }
            else if (sRecept.ToUpper().ToString() == cNonRecept.ToUpper().ToString())
            {
                OptNonRecept.Checked = true;
            }
            else
            {
                OptTousRecep.Checked = true;
            }

            sCloture = General.getFrmReg(Variable.cUserDispatchDevis, "ClotureDevis", "");
            if (sCloture.ToUpper().ToString() == cOuiCLot.ToUpper().ToString())
            {
                OptOuiCloture.Checked = true;
            }
            else if (sCloture.ToUpper().ToString() == cNonClot.ToUpper().ToString())
            {
                OptNonCloture.Checked = true;
            }
            else
            {
                OptTousCloture.Checked = true;
            }
            //=== FIN modif du 13 02 2017.

            _with27.txtGroupe.Text = General.getFrmReg(Variable.cUserDispatch, "txtGroupe ", "");
            _with27.txtNoEnregistrement.Text = General.getFrmReg(Variable.cUserDispatchDevis, "txtNoEnregistrement", "");

            //== recupere le numero de devis
            sNoIntervention = General.getFrmReg(Variable.cUserDispatchDevis, "NewVar", "");
            cmbIntervention_Click(cmbIntervention, new System.EventArgs());

            //Todo Verifier ces lignes 
            if (!string.IsNullOrEmpty(sNoIntervention) && General.IsNumeric(sNoIntervention))
            {
                if (rsintervention != null)
                {

                    //rsintervention.Find("numerodevis = " + sNoIntervention, , ADODB.SearchDirectionEnum.adSearchForward, ADODB.BookmarkEnum.adBookmarkFirst);                
                    var r = ssIntervention.Rows.FirstOrDefault(s => s.Cells["numerodevis"].Value.ToString() == sNoIntervention);
                    if (r != null)
                    {
                        r.Activate();
                        r.Selected = true;
                    }

                }
            }

            // lblNavigation[0].Text = General.sNomLien0;
            // lblNavigation[1].Text = General.sNomLien1;
            // lblNavigation[2].Text = General.sNomLien2;



        }

        private void fc_DropMaticule(object sNameGrille = null)
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            General.sSQL = "";
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Qualification.CodeQualif," + " Qualification.Qualification, Personnel.NumRadio, Personnel.Note_NOT" + " FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";

            if (ssDropMatricule.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(this.ssDropMatricule, General.sSQL, "Matricule", true);
            }

            //Me.ssIntervention.Columns("Intervenant").DropDownHwnd = Me.ssDropMatricule.hwnd
            if (this.ssIntervention.DataSource != null)
            {
                this.ssIntervention.DisplayLayout.Bands[0].Columns["CodeDeviseur"].ValueList = this.ssDropMatricule;

            }
            //        ssDropMatricule.Columns("CodeQualif").Visible = False
            //        ssDropMatricule.Columns("NumRadio").Visible = False
            //        ssDropMatricule.Columns("Note_NOT").Visible = False


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sNameGrille"></param>
        private void fc_DropCodeetat(object sNameGrille = null)
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            General.sSQL = "";
            General.sSQL = "SELECT DevisCodeEtat.Code, DevisCodeEtat.Libelle " + " FROM DevisCodeEtat WHERE CodeEtatManuel=1";

            if (ssDropCodeEtat.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(this.ssDropCodeEtat, General.sSQL, "Code", true);
            }
            if (this.ssIntervention.DataSource != null)
            {
                this.ssIntervention.DisplayLayout.Bands[0].Columns["CodeEtat"].ValueList = this.ssDropCodeEtat;
            }


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtRespTrav_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                short keyAscii = (short)e.KeyChar;
                string where = "";
                string requete;
                if (keyAscii == 13)
                {
                    var tmpAdo = new ModAdo();
                    if (!!string.IsNullOrEmpty(General.nz(tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + txtRespTrav.Text + "'"), "").ToString()))
                    {
                        General.sSQL = "SELECT Personnel.Matricule AS \"Matricule\", Personnel.Nom AS \"Nom\"," + "Qualification.Qualification as \"Libellé Qualification\" " + "FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif ";
                        SearchTemplate fg = new SearchTemplate(this, null, General.sSQL, where, "") { Text = "Recherche Responsable travaux" };
                        fg.SetValues(new Dictionary<string, string> { { "Matricule ", txtRespTrav.Text } });
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtRespTrav.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fg.Dispose(); fg.Close();
                        };
                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                txtRespTrav.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fg.Dispose(); fg.Close();
                        };
                        fg.StartPosition = FormStartPosition.CenterScreen;
                        fg.ShowDialog();

                    }
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdrecherche_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtRespTrav_TextChanged(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblResp.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + txtRespTrav.Text + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtRespTrav_BeforeDropDown(object sender, CancelEventArgs e)
        {

            short i = 0;
            string req = null;

            req = "SELECT Personnel.Matricule,Personnel.Nom," + "Qualification.Qualification " + "FROM Personnel INNER JOIN Qualification ON Personnel.CodeQualif = Qualification.CodeQualif ";
            try
            {
                if (General.nbCodeRespTrav > 0)
                {
                    if (General.CodeQualifRespTrav.Length > 0)
                    {
                        if (!(General.CodeQualifRespTrav[1] == "*"))
                        {
                            req = req + " WHERE ";
                            for (i = 1; i <= General.CodeQualifRespTrav.Length - 1; i++)
                            {
                                if (i < General.CodeQualifRespTrav.Length - 1)
                                {
                                    req = req + "Personnel.CodeQualif='" + General.CodeQualifRespTrav[i] + "' OR ";
                                }
                                else
                                {

                                    req = req + "Personnel.CodeQualif='" + General.CodeQualifRespTrav[i] + "'";
                                }
                            }
                        }
                    }
                }
                req = req + " ORDER BY Personnel.Matricule ";


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                if (txtRespTrav.Rows.Count == 0)
                {
                    sheridan.InitialiseCombo(txtRespTrav, req, "Matricule");
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtRespTrav_DropDown;");
            }


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtRespTrav_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblResp.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + txtRespTrav.Text + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            ssIntervention.DisplayLayout.Bands[0].Columns["CodeEtat"].ValueList = ssDropCodeEtat;
            ssIntervention.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Header.Caption = "Code Immeuble";
            ssIntervention.DisplayLayout.Bands[0].Columns["numerodevis"].Header.Caption = "N° Devis";
            ssIntervention.DisplayLayout.Bands[0].Columns["CodeEtat"].Header.Caption = "Code Etat";
            ssIntervention.DisplayLayout.Bands[0].Columns["TitreDevis"].Header.Caption = "Titre de devis";
            ssIntervention.DisplayLayout.Bands[0].Columns["DateCreation"].Header.Caption = "Date";
            ssIntervention.DisplayLayout.Bands[0].Columns["CodeDeviseur"].Header.Caption = "Deviseur";
            ssIntervention.DisplayLayout.Bands[0].Columns["Code1"].Header.Caption = "Gerant";
            ssIntervention.DisplayLayout.Bands[0].Columns["Observations"].Header.Caption = "Commentaire";
            ssIntervention.DisplayLayout.Bands[0].Columns["TotalAvantCoef"].Header.Caption = "Total Avant Coef";
            ssIntervention.DisplayLayout.Bands[0].Columns["TotalHT"].Header.Caption = "Total HT";
            ssIntervention.DisplayLayout.Bands[0].Columns["CodeTitre"].Hidden = true;
            ssIntervention.DisplayLayout.Bands[0].Columns["Coef"].CellActivation = Activation.NoEdit;
            ssIntervention.DisplayLayout.Bands[0].Columns["Code1"].CellActivation = Activation.NoEdit;
            ssIntervention.DisplayLayout.Bands[0].Columns["TotalHT"].CellActivation = Activation.NoEdit;
            ssIntervention.DisplayLayout.Bands[0].Columns["TotalAvantCoef"].CellActivation = Activation.NoEdit;
            ssIntervention.DisplayLayout.Bands[0].Columns["CodeDeviseur"].CellActivation = Activation.NoEdit;
            ssIntervention.DisplayLayout.Bands[0].Columns["DateCreation"].CellActivation = Activation.NoEdit;
            ssIntervention.DisplayLayout.Bands[0].Columns["TitreDevis"].CellActivation = Activation.NoEdit;
            ssIntervention.DisplayLayout.Bands[0].Columns["numerodevis"].CellActivation = Activation.NoEdit;
            ssIntervention.DisplayLayout.Bands[0].Columns["CodeImmeuble"].CellActivation = Activation.NoEdit;
            ssIntervention.DisplayLayout.TabNavigation = TabNavigation.NextControlOnLastCell;

            //foreach (var Col in ssIntervention.DisplayLayout.Bands[0].Columns)
            //    Col.CellMultiLine = DefaultableBoolean.True;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            var _with18 = this;
            try
            {
                fc_SaveSetting();

                //=== stocke les paramétres pour la feuille de destination.
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, ssIntervention.ActiveRow.Cells["numerodevis"].Value.ToString());
                View.Theme.Theme.Navigate(typeof(UserDocDevis));

                // ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGDEVIS); 

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssIntervention_DblClick");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_InitializeRow(object sender, InitializeRowEventArgs e)
        {

            try
            {
                if (!string.IsNullOrEmpty(e.Row.Cells["Coef"].Value.ToString()))
                {
                    e.Row.Cells["Coef"].Value = Convert.ToString(General.FncArrondir(Convert.ToDouble(e.Row.Cells["Coef"].Value)));

                    if (Convert.ToDouble(General.nz((e.Row.Cells["TotalAvantCoef"].Value), "0")) != 0)
                    {
                        if (Convert.ToDouble(General.nz((e.Row.Cells["TotalHT"].Value), "0")) / Convert.ToDouble(General.nz((e.Row.Cells["TotalAvantCoef"].Value), "0"))
                            < Convert.ToDouble(General.gfr_liaison("DevisCoefMin", "1")))
                        {
                            e.Row.Cells["Coef"].Appearance.BackColor = Color.Red;
                            e.Row.Cells["Coef"].Appearance.ForeColor = ColorTranslator.FromOle(0xffffff);
                        }
                    }
                }

                //--- code état 04 Imprimé
                if (e.Row.Cells["CodeEtat"].Value.ToString() == "04")
                {
                    e.Row.Appearance.BackColor = Color.CadetBlue;
                    e.Row.Appearance.ForeColor = ColorTranslator.FromOle(0x0);
                }

                //--- code état U urgent
                if (e.Row.Cells["CodeEtat"].Value.ToString() == "U")
                {
                    e.Row.Appearance.BackColor = Color.Red;
                    e.Row.Appearance.ForeColor = ColorTranslator.FromOle(0x0);
                }

                //--- code état A Accepté
                if (e.Row.Cells["CodeEtat"].Value.ToString() == "A")
                {
                    e.Row.Appearance.BackColor = Color.LightYellow;
                    e.Row.Appearance.ForeColor = ColorTranslator.FromOle(0x0);
                }

                //--- Code Etat T Facturé
                if (e.Row.Cells["CodeEtat"].Value.ToString() == "T")
                {
                    e.Row.Appearance.BackColor = Color.Aqua;
                    e.Row.Appearance.ForeColor = ColorTranslator.FromOle(0x0);
                }

                //--- Code Etat 03 en saisie
                if (e.Row.Cells["CodeEtat"].Value.ToString() == "03")
                {
                    e.Row.Appearance.BackColor = Color.Pink;
                    e.Row.Appearance.ForeColor = ColorTranslator.FromOle(0x0);
                }
                //look at manticehub  0001379: Dispatching devis -Alternance de couleur
                //if (e.Row.Cells["CodeEtat"].Value.ToString() != "04" &&
                //    e.Row.Cells["CodeEtat"].Value.ToString() != "U" &&
                //    e.Row.Cells["CodeEtat"].Value.ToString() != "A" &&
                //    e.Row.Cells["CodeEtat"].Value.ToString() != "T" &&
                //     e.Row.Cells["CodeEtat"].Value.ToString() != "03")
                //{
                //    e.Row.Appearance.BackColor = Color.Transparent;
                //    e.Row.Appearance.ForeColor = ColorTranslator.FromOle(0x0);
                //}
                /* if (Convert.ToDouble(General.nz(e.Row.Cells["MontantHT"].Value, "0")) < Convert.ToDouble("0"))
                  {
                    //  _with21.Columns("CodeEtat").CellStyleSet("Avoir", _with21.Row);

                  }
                  else if (Convert.ToDouble(General.nz(e.Row.Cells["MontantHT"].Value, "0")) > Convert.ToDouble("0"))
                  {
                    //  _with21.Columns("CodeEtat").CellStyleSet("Facture", _with21.Row);
                  }*/
                string totlatht = e.Row.Cells["TotalHt"].Value.ToString();
                if (!string.IsNullOrEmpty(totlatht) && General.IsNumeric(totlatht))
                    e.Row.Cells["TotalHt"].Value = decimal.Parse(totlatht).ToString("F2");
                else
                    e.Row.Cells["TotalHt"].Value = "";

                e.Row.Update();

                // ssIntervention.UpdateData();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ssIntervention_InitializeRow");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_AfterRowUpdate(object sender, RowEventArgs e)
        {
            blnKeyPress = false;

        }

        private void ssIntervention_CellChange(object sender, CellEventArgs e)
        {
            if (e.Cell.Column.Key.ToUpper() == "CodeEtat".ToUpper())
            {
                var r = e.Cell.Text;
                string req = $"update DevisEntete set {e.Cell.Column.Key} = '{r}'  WHERE NumeroDevis = '{e.Cell.Row.Cells["numerodevis"].Value}'";
                var x = General.Execute(req);
            }
            if (e.Cell.Column.Key.ToUpper() == "Observations".ToUpper())
            {
                var o = e.Cell.Text;
                string req = $"update DevisEntete set {e.Cell.Column.Key} = '{o}'  WHERE NumeroDevis = '{e.Cell.Row.Cells["numerodevis"].Value}'";
                var x = General.Execute(req);
            }

        }

        private void ssIntervention_MouseClick(object sender, MouseEventArgs e)
        {
            if (e.Button == MouseButtons.Right)
            {
                ssIntervention.ContextMenuStrip = mnuAnalytique;
                ssIntervention.ContextMenuStrip.GripStyle = ToolStripGripStyle.Visible;
                //ssIntervention.ContextMenuStrip.Show();

            }
        }

        private void mnuAnalytique_ItemClicked(object sender, ToolStripItemClickedEventArgs e)
        {

            if (ssIntervention.ActiveRow != null)
            {
                try
                {
                    string sSQL, sNodevis;
                    sNodevis = ssIntervention.ActiveRow.Cells["numerodevis"].Value.ToString();
                    if (sNodevis != "")
                    {
                        fc_SaveSetting();
                        sSQL =
                            "SELECT GestionStandard.NumFicheStandard FROM GestionStandard WHERE GestionStandard.NoDevis='" +
                            StdSQLchaine.gFr_DoublerQuote(sNodevis) + "'";
                        var ModAdo = new ModAdo();
                        sSQL = ModAdo.fc_ADOlibelle(sSQL);
                        General.saveInReg(Variable.cUserDocAnalytique2, "NewVar", sSQL);
                        General.saveInReg(Variable.cUserDocAnalytique2, "CodeImmeuble", "");
                        General.saveInReg(Variable.cUserDocAnalytique2, "Gerant", "");
                        ssIntervention.ContextMenuStrip.GripStyle = ToolStripGripStyle.Hidden;
                        View.Theme.Theme.Navigate(typeof(UserDocAnalytique2));
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Navigation annulée, aucun devis n'a été réalisée", "Navigation annulée", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                    }
                }
                catch (Exception ex)
                {
                    Erreurs.gFr_debug(ex, Name + ";mnuAnalytique_Click");
                }
            }
        }

        private void txtCoefDe_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }

        }

        private void txtCoefA_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
        }

        private void txtMontantDe_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
        }

        private void txtMontantA_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
        }

        private void txtINT_AnaCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
        }

        private void txtIntercom_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
        }

        private void cmbBe_KeyPress(object sender, KeyPressEventArgs e)
        {

            if (e.KeyChar == 13)
            {
                if (!General.IsNumeric(cmbBe.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Saisir une valeur numérique.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    cmbBe.Text = "";
                    lblLibBE.Text = "";
                    return;
                }
                else
                {

                    var tmpAdo = new ModAdo();
                    string ExistCmbBe = tmpAdo.fc_ADOlibelle("SELECT Count(CodeBE) from BE where CodeBE=" + cmbBe.Text);
                    int varCount = Convert.ToInt32(General.nz(ExistCmbBe, 0));
                    if (varCount == 0)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Code inexistant.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        cmbBe.Text = "";
                        lblLibBE.Text = "";
                        return;
                    }
                    cmbBe_Validating(cmbBe, null);
                }
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
        }

        private void cmbCodeGerant_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
        }

        private void txtDateSaisieDe_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (General.fc_ValidateDate(txtDateSaisieDe) == true)
                {
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
        }

        private void txtDateSaisieFin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (General.fc_ValidateDate(txtDateSaisieFin) == true)
                {
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
        }

        private void txtinterDe_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (General.fc_ValidateDate(txtinterDe) == true)
                {
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
        }

        private void txtIntereAu_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (General.fc_ValidateDate(txtIntereAu) == true)
                {
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
        }

        private void cmbStatus_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }

        }

        private void cmbStatus2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
        }

        private void cmdGroupe_Click(object sender, EventArgs e)
        {

            string requete = "";
            string where = "";
            try
            {

                requete = " SELECT     CRCL_Code AS \"Code\","
                            + " CRCL_RaisonSocial AS \"Raison Sociale\","
                            + " CRCL_Adresse1 AS \"Adresse\","
                            + " CRCL_CP AS \"Code Postal\","
                            + " CRCL_Ville As \"Ville\" "
                            + " FROM         CRCL_GroupeClient ";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'un groupe" };
                fg.SetValues(new Dictionary<string, string> { { "CRCL_Code", txtGroupe.Text } });

                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtGroupe.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                    fg.Dispose(); fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtGroupe.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                        fg.Dispose(); fg.Close();
                    }

                };
                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdGroupe_Click");
            }
        }

        private void txtGroupe_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    using (var tmpModAdo = new ModAdo())
                        if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT CRCL_Code From CRCL_GroupeClient WHERE CRCL_Code ='" + StdSQLchaine.gFr_DoublerQuote(txtGroupe.Text) + "'"), "").ToString()))
                        {
                            cmdGroupe_Click(cmdGroupe, new System.EventArgs());
                        }
                        else
                        {
                            cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                        }
                }
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";cmdGroupe_KeyPress");
            }
        }


        private void txtDateSaisieDe_Validating(object sender, CancelEventArgs e)
        {
            General.fc_ValidateDate(txtDateSaisieDe);
        }

        private void txtDateSaisieFin_Validating(object sender, CancelEventArgs e)
        {
            General.fc_ValidateDate(txtDateSaisieFin);
        }

        private void txtinterDe_Validating(object sender, CancelEventArgs e)
        {
            General.fc_ValidateDate(txtinterDe);
        }

        private void txtIntereAu_Validating(object sender, CancelEventArgs e)
        {
            General.fc_ValidateDate(txtIntereAu);
        }

        private void ultraDateTimeEditor1_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor1.Value == null) return;

            txtDateSaisieDe.Text = Convert.ToDateTime(ultraDateTimeEditor1.Value).ToShortDateString();

        }

        private void ultraDateTimeEditor1_ValueChanged(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor1.Value == null) return;

            txtDateSaisieDe.Text = Convert.ToDateTime(ultraDateTimeEditor1.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor2_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor2.Value == null) return;

            txtDateSaisieFin.Text = Convert.ToDateTime(ultraDateTimeEditor2.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor2_ValueChanged(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor2.Value == null) return;

            txtDateSaisieFin.Text = Convert.ToDateTime(ultraDateTimeEditor2.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor3_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor2.Value == null) return;

            txtinterDe.Text = Convert.ToDateTime(ultraDateTimeEditor3.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor3_ValueChanged(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor3.Value == null) return;

            txtinterDe.Text = Convert.ToDateTime(ultraDateTimeEditor3.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor4_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor4.Value == null) return;

            txtIntereAu.Text = Convert.ToDateTime(ultraDateTimeEditor4.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor4_ValueChanged(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor4.Value == null) return;

            txtIntereAu.Text = Convert.ToDateTime(ultraDateTimeEditor4.Value).ToShortDateString();
        }

        private void ssIntervention_AfterRowActivate(object sender, EventArgs e)
        {
            if (ssIntervention.ActiveRow == null) return;
            var noDevis = ssIntervention.ActiveRow.Cells["numerodevis"].Value.ToString();
            General.saveInReg(Variable.cUserDispatchDevis, "NewVar", noDevis);

        }
        private void fc_MajResp()
        {
            string sSQL;

            try
            {
                if (!string.IsNullOrEmpty(txtINT_AnaCodeBis.Text))
                {
                    sSQL = "SELECT    Personnel.Initiales "
                   + " FROM  Personnel "
                   + " WHERE     Personnel.Matricule ='" + StdSQLchaine.gFr_DoublerQuote(txtINT_AnaCodeBis.Text) + "'";
                    using (var tmpAdo = new ModAdo())
                        txtINT_AnaCode.Text = tmpAdo.fc_ADOlibelle(sSQL);
                }
                else
                {
                    txtINT_AnaCode.Text = "";
                }

            }
            catch (Exception ex)
            {

            }
        }
        private void txtINT_AnaCodeBis_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL;
            sSQL = "SELECT    Personnel.Matricule,  Personnel.Nom , Personnel.Initiales "
                  + " FROM         Qualification LEFT OUTER JOIN"
                  + " SpecifQualif ON "
                  + " Qualification.CodeQualif = SpecifQualif.CodeQualif "
                  + " RIGHT OUTER JOIN"
                  + " Personnel ON Qualification.CodeQualif = Personnel.CodeQualif"
                  + " WHERE     (SpecifQualif.QualifRExp = 1) ";
            sheridan.InitialiseCombo(txtINT_AnaCodeBis, sSQL, "Matricule");

        }

        private void txtINT_AnaCodeBis_AfterCloseUp(object sender, EventArgs e)
        {
            fc_MajResp();
        }

        private void txtINT_AnaCodeBis_ValueChanged(object sender, EventArgs e)
        {
            fc_MajResp();
        }

        /// <summary>
        /// Mondir le 04.02.2021, https://groupe-dt.mantishub.io/view.php?id=2250
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFindCodeArticle_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.02.2021, Ajout du LOG
            Program.SaveException(null, $"cmdFindCodeArticle_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                string requete = "SELECT CodeArticle AS \"Code Article\", Designation1 AS \"Designation\" FROM FacArticle";
                string where_order = " CodeCategorieArticle='D' ";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "" };
                fg.SetValues(new Dictionary<string, string> { { "CodeArticle", cmdFindCodeArticle.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtCodeArticle.Text = fg.ugResultat.ActiveRow.Cells["Code Article"].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCodeArticle.Text = fg.ugResultat.ActiveRow.Cells["Code Article"].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Mondir le 04.02.2021, https://groupe-dt.mantishub.io/view.php?id=2251
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFindPoste_Click(object sender, EventArgs e)
        {
            //===> Mondir 04.02.2021, Ajout du LOG
            Program.SaveException(null, $"cmdFindPoste_Click() - Enter In The Function");
            //===> Fin Modif Mondir
            try
            {
                string requete = "SELECT CodeSousFamille AS \"Code Poste\", IntituleSousFamille AS \"Intitulé\" FROM SousFamilleArticle";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "" };
                fg.SetValues(new Dictionary<string, string> { { "CodeSousFamille", txtPoste.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtPoste.Text = fg.ugResultat.ActiveRow.Cells["Code Poste"].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtPoste.Text = fg.ugResultat.ActiveRow.Cells["Code Poste"].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Mondir le 04.02.2021, https://groupe-dt.mantishub.io/view.php?id=2250
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == 13)
            {
                cmdFindCodeArticle_Click(null, null);
            }
        }

        /// <summary>
        /// Mondir le 04.02.2021, https://groupe-dt.mantishub.io/view.php?id=2251
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPoste_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == 13)
            {
                cmdFindPoste_Click(null, null);
            }
        }
    }
}