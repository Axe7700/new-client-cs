namespace Axe_interDT.Views.DispatchIntervention.HistoriqueIntervention
{
    partial class UserDocHistoriqueInterv
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.SSOleDBGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.lblDesignation = new System.Windows.Forms.Label();
            this.lblIntervenant = new System.Windows.Forms.Label();
            this.lblCodeEtat = new System.Windows.Forms.Label();
            this.lblDateRealise = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel1.Controls.Add(this.SSOleDBGrid1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Label1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblDateRealise, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblCodeEtat, 2, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblIntervenant, 3, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblDesignation, 4, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 38F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(780, 631);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // SSOleDBGrid1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.SSOleDBGrid1, 5);
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBGrid1.DisplayLayout.Appearance = appearance1;
            this.SSOleDBGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.SSOleDBGrid1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBGrid1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBGrid1.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.SSOleDBGrid1.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBGrid1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBGrid1.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBGrid1.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.SSOleDBGrid1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBGrid1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBGrid1.DisplayLayout.Override.CellAppearance = appearance8;
            this.SSOleDBGrid1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBGrid1.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBGrid1.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBGrid1.DisplayLayout.Override.RowAppearance = appearance11;
            this.SSOleDBGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBGrid1.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.SSOleDBGrid1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBGrid1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBGrid1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSOleDBGrid1.Font = new System.Drawing.Font("Ubuntu",11F);
            this.SSOleDBGrid1.Location = new System.Drawing.Point(1, 39);
            this.SSOleDBGrid1.Margin = new System.Windows.Forms.Padding(1);
            this.SSOleDBGrid1.Name = "SSOleDBGrid1";
            this.SSOleDBGrid1.Size = new System.Drawing.Size(778, 591);
            this.SSOleDBGrid1.TabIndex = 412;
            this.SSOleDBGrid1.Text = "ultraGrid1";
            this.SSOleDBGrid1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSOleDBGrid1_InitializeLayout);
            this.SSOleDBGrid1.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.SSOleDBGrid1_InitializeRow);
            this.SSOleDBGrid1.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.SSOleDBGrid1_ClickCellButton);
            this.SSOleDBGrid1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.SSOleDBGrid1_DoubleClickRow);
            this.SSOleDBGrid1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SSOleDBGrid1_KeyPress);
            // 
            // lblDesignation
            // 
            this.lblDesignation.AutoSize = true;
            this.lblDesignation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDesignation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDesignation.Font = new System.Drawing.Font("Ubuntu",11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDesignation.Location = new System.Drawing.Point(624, 0);
            this.lblDesignation.Margin = new System.Windows.Forms.Padding(0);
            this.lblDesignation.Name = "lblDesignation";
            this.lblDesignation.Size = new System.Drawing.Size(156, 38);
            this.lblDesignation.TabIndex = 388;
            this.lblDesignation.Text = "Désignation";
            this.lblDesignation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDesignation.Click += new System.EventHandler(this.lblDesignation_Click);
            // 
            // lblIntervenant
            // 
            this.lblIntervenant.AutoSize = true;
            this.lblIntervenant.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblIntervenant.Font = new System.Drawing.Font("Ubuntu",11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblIntervenant.Location = new System.Drawing.Point(468, 0);
            this.lblIntervenant.Margin = new System.Windows.Forms.Padding(0);
            this.lblIntervenant.Name = "lblIntervenant";
            this.lblIntervenant.Size = new System.Drawing.Size(156, 38);
            this.lblIntervenant.TabIndex = 387;
            this.lblIntervenant.Text = "Intervenant";
            this.lblIntervenant.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblIntervenant.Click += new System.EventHandler(this.lblIntervenant_Click);
            // 
            // lblCodeEtat
            // 
            this.lblCodeEtat.AutoSize = true;
            this.lblCodeEtat.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblCodeEtat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCodeEtat.Font = new System.Drawing.Font("Ubuntu",11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblCodeEtat.Location = new System.Drawing.Point(312, 0);
            this.lblCodeEtat.Margin = new System.Windows.Forms.Padding(0);
            this.lblCodeEtat.Name = "lblCodeEtat";
            this.lblCodeEtat.Size = new System.Drawing.Size(156, 38);
            this.lblCodeEtat.TabIndex = 386;
            this.lblCodeEtat.Text = "Code Etat";
            this.lblCodeEtat.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblCodeEtat.Click += new System.EventHandler(this.lblCodeEtat_Click);
            // 
            // lblDateRealise
            // 
            this.lblDateRealise.AutoSize = true;
            this.lblDateRealise.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.lblDateRealise.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDateRealise.Font = new System.Drawing.Font("Ubuntu",11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDateRealise.Location = new System.Drawing.Point(156, 0);
            this.lblDateRealise.Margin = new System.Windows.Forms.Padding(0);
            this.lblDateRealise.Name = "lblDateRealise";
            this.lblDateRealise.Size = new System.Drawing.Size(156, 38);
            this.lblDateRealise.TabIndex = 385;
            this.lblDateRealise.Text = "Date Réalisé";
            this.lblDateRealise.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblDateRealise.Click += new System.EventHandler(this.lblDateRealise_Click);
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.BackColor = System.Drawing.Color.Transparent;
            this.Label1.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.Label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label1.Font = new System.Drawing.Font("Ubuntu",11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(0, 0);
            this.Label1.Margin = new System.Windows.Forms.Padding(0);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(156, 38);
            this.Label1.TabIndex = 384;
            this.Label1.Text = "Code Immeuble";
            this.Label1.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.Label1.Click += new System.EventHandler(this.Label1_Click);
            // 
            // UserDocHistoriqueInterv
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserDocHistoriqueInterv";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Historique Intervention";
            this.VisibleChanged += new System.EventHandler(this.UserDocHistoriqueInterv_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public Infragistics.Win.UltraWinGrid.UltraGrid SSOleDBGrid1;
        public System.Windows.Forms.Label Label1;
        public System.Windows.Forms.Label lblDateRealise;
        public System.Windows.Forms.Label lblCodeEtat;
        public System.Windows.Forms.Label lblIntervenant;
        public System.Windows.Forms.Label lblDesignation;
    }
}
