﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using System.IO;
using Infragistics.Win;
using Axe_interDT.Views.Intervention;
using Axe_interDT.Views.Devis;

namespace Axe_interDT.Views.DispatchIntervention.HistoriqueIntervention
{
    public partial class UserDocHistoriqueInterv : UserControl
    {
        string strValeur = "";
        string strValeurRetour;
        string strSQL2;
        string strCheminRetour;
        string CodeFicheAppelante;

        DataTable adotemp;

        bool boolnow;
        int nbClickDate;
        int nbclickEtat;
        int nbclickImm;
        int nbclickInt;
        int nbclickDes;


        public UserDocHistoriqueInterv()
        {
            InitializeComponent();
        }

        private void Label1_Click(object sender, EventArgs e)
        {
            nbclickImm = nbclickImm + 1;
            var tmpAdo = new ModAdo();
            adotemp.Dispose();
            if (nbclickImm > 1)
            {

                adotemp = tmpAdo.fc_OpenRecordSet("SELECT Intervention.CodeImmeuble, Intervention.DateRealise, Intervention.CodeEtat,  Intervention.Intervenant ,"
                    + " Intervention.Designation,Intervention.Wave, Intervention.Commentaire, Intervention.Commentaire2, Intervention.Commentaire3, NoIntervention" +
                    " FROM Intervention where Intervention.CodeImmeuble ='" + strValeur + "' order by CodeImmeuble, DateRealise  ASC");
                nbclickImm = 0;
            }
            else
            {
                adotemp = tmpAdo.fc_OpenRecordSet("SELECT Intervention.CodeImmeuble, Intervention.DateRealise, Intervention.CodeEtat,  Intervention.Intervenant ,"
                    + "Intervention.Designation,Intervention.Wave, Intervention.Commentaire, Intervention.Commentaire2, Intervention.Commentaire3, NoIntervention"
                    + " FROM Intervention where Intervention.CodeImmeuble ='" + strValeur + "' order by CodeImmeuble, DateRealise  DESC");
            }
            boolnow = false;
            SSOleDBGrid1.DataSource = adotemp;
            SSOleDBGrid1.UpdateData();
        }

        private void lblCodeEtat_Click(object sender, EventArgs e)
        {
            nbclickEtat = nbclickEtat + 1;
            var tmpAdo = new ModAdo();
            adotemp.Dispose();
            if (nbclickEtat > 1)
            {
                adotemp = tmpAdo.fc_OpenRecordSet("SELECT Intervention.CodeImmeuble, Intervention.DateRealise, Intervention.CodeEtat,  Intervention.Intervenant ," +
                    "Intervention.Designation,Intervention.Wave, Intervention.Commentaire, Intervention.Commentaire2, Intervention.Commentaire3, NoIntervention " +
                    "FROM Intervention where Intervention.CodeImmeuble ='" + strValeur + "' order by CodeEtat, DateRealise, CodeImmeuble ASC");
                nbclickEtat = 0;
            }
            else
            {
                adotemp = tmpAdo.fc_OpenRecordSet("SELECT Intervention.CodeImmeuble, Intervention.DateRealise, Intervention.CodeEtat,  Intervention.Intervenant ," +
                    "  Intervention.Designation, Intervention.Wave,Intervention.Commentaire, Intervention.Commentaire2, Intervention.Commentaire3, NoIntervention"
                    + " FROM Intervention where Intervention.CodeImmeuble ='" + strValeur + "' order by CodeEtat, DateRealise, CodeImmeuble DESC");
            }
            boolnow = false;
            SSOleDBGrid1.DataSource = adotemp;
        }

        private void lblDateRealise_Click(object sender, EventArgs e)
        {
            nbClickDate = nbClickDate + 1;
            var tmpAdo = new ModAdo();
            adotemp.Dispose();
            if (nbClickDate > 1)
            {
                adotemp = tmpAdo.fc_OpenRecordSet("SELECT Intervention.CodeImmeuble, Intervention.DateRealise, Intervention.CodeEtat,  Intervention.Intervenant ," +
                    "  Intervention.Designation,Intervention.Wave, Intervention.Commentaire, Intervention.Commentaire2, Intervention.Commentaire3, NoIntervention" +
                    " FROM Intervention where Intervention.CodeImmeuble ='" + strValeur + "' order by DateRealise, CodeImmeuble ASC");
                nbClickDate = 0;
            }
            else
            {
                adotemp = tmpAdo.fc_OpenRecordSet("SELECT Intervention.CodeImmeuble, Intervention.DateRealise, Intervention.CodeEtat,  Intervention.Intervenant ,"
                    + "  Intervention.Designation,Intervention.Wave, Intervention.Commentaire, Intervention.Commentaire2, Intervention.Commentaire3, NoIntervention "
                    + "FROM Intervention where Intervention.CodeImmeuble ='" + strValeur + "' order by DateRealise, CodeImmeuble DESC");
            }
            boolnow = false;
            SSOleDBGrid1.DataSource = adotemp;

        }

        private void lblIntervenant_Click(object sender, EventArgs e)
        {
            nbclickInt = nbclickInt + 1;
            var tmpAdo = new ModAdo();
            adotemp.Dispose();
            if (nbclickInt > 1)
            {
                adotemp = tmpAdo.fc_OpenRecordSet("SELECT Intervention.CodeImmeuble, Intervention.DateRealise, Intervention.CodeEtat,  Intervention.Intervenant ," +
                    "  Intervention.Designation, Intervention.Wave,Intervention.Commentaire, Intervention.Commentaire2, Intervention.Commentaire3, NoIntervention" +
                    " FROM Intervention where Intervention.CodeImmeuble ='" + strValeur + "' order by Intervenant, DateRealise, CodeImmeuble ASC");
                nbclickInt = 0;
            }
            else
            {
                adotemp = tmpAdo.fc_OpenRecordSet("SELECT Intervention.CodeImmeuble, Intervention.DateRealise, Intervention.CodeEtat,  Intervention.Intervenant ,"
                    + "  Intervention.Designation,Intervention.Wave, Intervention.Commentaire, Intervention.Commentaire2, Intervention.Commentaire3, NoIntervention "
                    + "FROM Intervention where Intervention.CodeImmeuble ='" + strValeur + "' order by Intervenant, DateRealise, CodeImmeuble ASC");
            }
            boolnow = false;
            SSOleDBGrid1.DataSource = adotemp;
        }

        private void SSOleDBGrid1_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            try
            {
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, SSOleDBGrid1.ActiveRow.Cells["NoIntervention"].Value.ToString());
                View.Theme.Theme.Navigate(typeof(UserIntervention));
                // ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserIntervention);
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSOleDBGrid1_DblClick");
            }
        }

        private void SSOleDBGrid1_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, SSOleDBGrid1.ActiveRow.Cells["NoIntervention"].Value.ToString());
                View.Theme.Theme.Navigate(typeof(UserIntervention));
                // ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserIntervention);
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";SSOleDBGrid1_KeyPress");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                bool boolFolder = false;
                bool boolMp3 = false;
                string sMp3 = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;

                if (!string.IsNullOrEmpty(SSOleDBGrid1.ActiveRow.Cells["Wave"].Value.ToString()))
                {
                    //== modifie l'extension wav en mp3 et ouvre le fichier si existant.
                    sMp3 = General.Mid(SSOleDBGrid1.ActiveRow.Cells["Wave"].Value.ToString(), 1, SSOleDBGrid1.ActiveRow.Cells["Wave"].Value.ToString().Length - 3) + "mp3";
                    // sMp3 = txtWave
                    General.fso = null;
                    boolMp3 = File.Exists(sMp3);

                    if (boolMp3 == true)
                    {
                        ModuleAPI.Ouvrir(sMp3);
                        return;
                    }
                    else
                    {
                        General.fso = null;
                        boolFolder = File.Exists(SSOleDBGrid1.ActiveRow.Cells["Wave"].Value.ToString());

                        if (boolFolder == true)
                        {
                            ModuleAPI.Ouvrir(SSOleDBGrid1.ActiveRow.Cells["Wave"].Value.ToString());
                            return;
                        }
                    }
                    if (boolFolder == false & boolMp3 == false)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le fichier son a été déplacé ou supprimé.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    //Ouvrir Me.hWnd, sMp3
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun rapport d'intervention vocal n'est associé a cette intervention", "Document non trouvé", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdWave_Click");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            this.SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Wave"].Header.Caption = "RV";
            this.SSOleDBGrid1.DisplayLayout.Bands[0].Columns["NoIntervention"].Hidden = true;
            SSOleDBGrid1.UseOsThemes = DefaultableBoolean.False;
            //this.SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Wave"].Width = 9;
            SSOleDBGrid1.UseOsThemes = DefaultableBoolean.False;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Wave"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.Button;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Wave"].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Wave"].CellButtonAppearance.Image = Properties.Resources.volume_up_4_16;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocHistoriqueInterv_VisibleChanged(object sender, EventArgs e)
        {
            try
            {
                if (!Visible)
                    return;

                General.open_conn();
                boolnow = true;
                
                View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocHistoriqueInterv");
                //- positionne sur le dernier enregistrement.
                if (ModParametre.fc_RecupParam(this.Name) == true)
                {
                    // charge les enregistremants
                    adotemp = new DataTable();
                    var tmpAdo = new ModAdo();
                    string req = "SELECT Intervention.CodeImmeuble, Intervention.DateRealise, Intervention.CodeEtat,  Intervention.Intervenant ,"
                     + "  Intervention.Designation,Intervention.Wave, Intervention.Commentaire, Intervention.Commentaire2, Intervention.Commentaire3, NoIntervention "
                     + "FROM Intervention where Intervention.CodeImmeuble ='" + ModParametre.sNewVar + "'  order by DateRealise DESC";

                    adotemp = tmpAdo.fc_OpenRecordSet(req);
                    boolnow = false;
                    SSOleDBGrid1.DataSource = adotemp;
                    
                    SSOleDBGrid1.Visible = true;
                    SSOleDBGrid1.UpdateData();
                }


                /* if (!string.IsNullOrEmpty(General.LogoSociete) | !Information.IsDBNull(General.LogoSociete))
                 {
                     imgLogoSociete.Image = System.Drawing.Image.FromFile(General.LogoSociete);
                     //    If IsNumeric(imgTop) Then
                     //        imgLogoSociete.Top = imgTop
                     //    End If
                     //    If IsNumeric(imgLeft) Then
                     //        imgLogoSociete.Left = imgLeft
                     //    End If
                 }

                 if (!string.IsNullOrEmpty(General.NomSousSociete) & !Information.IsDBNull(General.NomSousSociete))
                 {
                     lblNomSociete.Text = General.NomSousSociete;
                     System.Windows.Forms.Application.DoEvents();
                 }
                 */
                switch (ModParametre.sFicheAppelante)
                {
                    case Variable.cUserIntervention:
                        ////===>  après la modification du nouveau menu j'ai commenter ces lignes modif 20/06/2019
                        // Views.Theme.Menu.MainMenu.UserDocHistoriqueIntervMenu[0].LabelItemName.Text = "Intervention";
                        //CodeFicheAppelante = General.getFrmReg( Variable.cUserIntervention, "NewVar", "");
                        // ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, CodeFicheAppelante);
                        // Views.Theme.Menu.MainMenu.UserDocHistoriqueIntervMenu[0].LabelItemName.Visible = true;
                        // Views.Theme.Menu.MainMenu.UserDocHistoriqueIntervMenu[0].ClickEvent = (se, ev) =>
                        // {
                        //     View.Theme.Theme.Navigate(typeof(UserIntervention));

                        // };
                        //lblGoFicheAppelante[3].Visible = true;
                        // lblRetourFiche[0].Visible = true;

                        CodeFicheAppelante = General.getFrmReg(Variable.cUserIntervention, "NewVar", "");
                        break;
                    //        Case cUserDocClient
                    //            lblGoFicheAppelante.Caption = "FICHE GERANT"
                    //            CodeFicheAppelante = GetSetting(cFrNomApp, cUserDocClient, "NewVar", "")
                    //            lblGoFicheAppelante.Visible = True
                    case Variable.cUserDocDevis:
                        ////===>  après la modification du nouveau menu j'ai commenter ces lignes modif 20/06/2019
                        // Views.Theme.Menu.MainMenu.UserDocHistoriqueIntervMenu[0].LabelItemName.Text = "Devis";
                        //CodeFicheAppelante = General.getFrmReg( Variable.cUserDocDevis, "NewVar", "");
                        // ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, CodeFicheAppelante);
                        // Views.Theme.Menu.MainMenu.UserDocHistoriqueIntervMenu[0].LabelItemName.Visible = true;
                        // Views.Theme.Menu.MainMenu.UserDocHistoriqueIntervMenu[0].ClickEvent = (se, ev) =>
                        // {
                        //     View.Theme.Theme.Navigate(typeof(UserDocDevis));

                        // };

                        // lblGoFicheAppelante[3].Visible = true;
                        // lblRetourFiche[0].Visible = true;
                        CodeFicheAppelante = General.getFrmReg(Variable.cUserDocDevis, "NewVar", "");

                        break;
                    case Variable.cUserDispatch:
                        ////===>  après la modification du nouveau menu j'ai commenter ces lignes modif 20/06/2019
                        // Views.Theme.Menu.MainMenu.UserDocHistoriqueIntervMenu[0].LabelItemName.Text = "Dispatch.Interv";
                        //CodeFicheAppelante = "";                       
                        // ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, CodeFicheAppelante);
                        // Views.Theme.Menu.MainMenu.UserDocHistoriqueIntervMenu[0].LabelItemName.Visible = true;
                        // Views.Theme.Menu.MainMenu.UserDocHistoriqueIntervMenu[0].ClickEvent = (se, ev) =>
                        // {
                        //     View.Theme.Theme.Navigate(typeof(UserDispatch));

                        // };
                        // lblGoFicheAppelante[3].Visible = true;
                        // lblRetourFiche[0].Visible = true;

                        CodeFicheAppelante = "";
                        break;
                    default:
                        //Views.Theme.Menu.MainMenu.UserDocHistoriqueIntervMenu[0].LabelItemName.Visible = false;
                        //  lblGoFicheAppelante[3].Visible = false;
                        //  lblRetourFiche[0].Visible = false;
                        break;
                }


                /*  SSOleDBGrid1.ColumnHeaders = false;
                   lblNavigation[0].Text = General.sNomLien0;
                    lblNavigation[1].Text = General.sNomLien1;
                    lblNavigation[2].Text = General.sNomLien2;*/



            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Me_Show");
            }
        }

        private void lblDesignation_Click(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// testde
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            e.Row.Activation = Infragistics.Win.UltraWinGrid.Activation.ActivateOnly;
            //SSOleDBGrid1.UpdateData();
        }
    }
}
