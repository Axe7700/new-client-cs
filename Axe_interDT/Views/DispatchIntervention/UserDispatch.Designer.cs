namespace Axe_interDT.Views.DispatchIntervention
{
    partial class UserDispatch
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton1 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton2 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton3 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDispatch));
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance73 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance74 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance75 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance76 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance77 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance78 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance79 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance80 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance81 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance82 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance83 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance84 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance85 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance86 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance87 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance88 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance89 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance90 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance91 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance92 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance93 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance94 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance95 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance96 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance97 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance98 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance99 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance100 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance101 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance102 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance103 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance104 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance105 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance106 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance107 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance108 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance109 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance110 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance111 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance112 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance113 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance114 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance115 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance116 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance117 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance118 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance119 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance120 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance121 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance122 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance123 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance124 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance125 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance126 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance127 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance128 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance129 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance130 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance131 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance132 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance133 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance134 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance135 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance136 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance137 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance138 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance139 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance140 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance141 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance142 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance143 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance144 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance145 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance146 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance147 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance148 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance149 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance150 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance151 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance152 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance153 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance154 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance155 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance156 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance157 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance158 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance159 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance160 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance161 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance162 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance163 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance164 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance165 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance166 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance167 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance168 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance169 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance170 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance171 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance172 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance173 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance174 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance175 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance176 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance177 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance178 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance179 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance180 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance181 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance182 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance183 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance184 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance185 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance186 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance187 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance188 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance189 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance190 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance191 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance192 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton4 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.Appearance appearance193 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance194 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance195 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance196 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance197 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance198 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance199 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton5 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.Appearance appearance200 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance201 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance202 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance203 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance204 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance205 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance206 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton6 = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            Infragistics.Win.Appearance appearance207 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance208 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance209 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance210 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance211 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance212 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance213 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance214 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance215 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance216 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance217 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance218 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance219 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance220 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance221 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance222 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance223 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance224 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance225 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance226 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance227 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance228 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance229 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance230 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance231 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance232 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance233 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance234 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance235 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance236 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance237 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance238 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance239 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance240 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance241 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance242 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance243 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance244 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance245 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance246 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.ultraDateTimeEditor4 = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.txtIntereAu = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.ultraDateTimeEditor3 = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.txtinterDe = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.ultraDateTimeEditor2 = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.txtDateSaisieFin = new iTalk.iTalk_TextBox_Small2();
            this.label13 = new System.Windows.Forms.Label();
            this.label11 = new System.Windows.Forms.Label();
            this.chkAffaireAvecFact = new System.Windows.Forms.CheckBox();
            this.chkAffaireSansFact = new System.Windows.Forms.CheckBox();
            this.chkAchat = new System.Windows.Forms.CheckBox();
            this.txtINT_AnaCodeF = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtNumContrat = new iTalk.iTalk_TextBox_Small2();
            this.flowLayoutPanel10 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdGerant = new System.Windows.Forms.Button();
            this.chkExclGerant = new System.Windows.Forms.CheckBox();
            this.label27 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel9 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdRechDepan = new System.Windows.Forms.Button();
            this.chkExclDepan = new System.Windows.Forms.CheckBox();
            this.lbllibDepanneur = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.cmbCodeimmeuble = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.flowLayoutPanel15 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdRechercheImmeuble = new System.Windows.Forms.Button();
            this.chkExclImm = new System.Windows.Forms.CheckBox();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.cmbStatus = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label6 = new System.Windows.Forms.Label();
            this.cmbCommercial = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label7 = new System.Windows.Forms.Label();
            this.cmbIntervenant = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label8 = new System.Windows.Forms.Label();
            this.label9 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.cmbArticle = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cmbEnergie = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCodeGestionnaire = new iTalk.iTalk_TextBox_Small2();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.chkExclStatut = new System.Windows.Forms.CheckBox();
            this.lblLibStatus = new System.Windows.Forms.Label();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdRechercheCommercial = new System.Windows.Forms.Button();
            this.chkExclComm = new System.Windows.Forms.CheckBox();
            this.lblLibCommercial = new System.Windows.Forms.Label();
            this.flowLayoutPanel4 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdRechercheIntervenant = new System.Windows.Forms.Button();
            this.chkExclInterv = new System.Windows.Forms.CheckBox();
            this.lblLibIntervenant = new System.Windows.Forms.Label();
            this.flowLayoutPanel5 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdRechercheTypeOperation = new System.Windows.Forms.Button();
            this.chkExclArticle = new System.Windows.Forms.CheckBox();
            this.lblLibTypeOperation = new System.Windows.Forms.Label();
            this.flowLayoutPanel6 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdFindGestionnaire = new System.Windows.Forms.Button();
            this.chkExclGestionnaire = new System.Windows.Forms.CheckBox();
            this.lblLibGestionnaire = new System.Windows.Forms.Label();
            this.flowLayoutPanel7 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdFindEnergie = new System.Windows.Forms.Button();
            this.chkExclEnergie = new System.Windows.Forms.CheckBox();
            this.Label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.ssComboActivite = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label16 = new System.Windows.Forms.Label();
            this.label17 = new System.Windows.Forms.Label();
            this.label23 = new System.Windows.Forms.Label();
            this.txtBordereauJournee = new iTalk.iTalk_TextBox_Small2();
            this.cmbCodeGerant = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtTotal = new iTalk.iTalk_TextBox_Small2();
            this.cmbEtatDevis = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtINT_AnaCode = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label25 = new System.Windows.Forms.Label();
            this.label22 = new System.Windows.Forms.Label();
            this.SSOleDBCombo1 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cmbDepan = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label21 = new System.Windows.Forms.Label();
            this.txtCP = new iTalk.iTalk_TextBox_Small2();
            this.label24 = new System.Windows.Forms.Label();
            this.txtNoIntervention = new iTalk.iTalk_TextBox_Small2();
            this.txtNoEnregistrement = new iTalk.iTalk_TextBox_Small2();
            this.cmdDispatch = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.flowLayoutPanel12 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdRechercheDispatch = new System.Windows.Forms.Button();
            this.chkExclDispatch = new System.Windows.Forms.CheckBox();
            this.lblLibDispatch = new System.Windows.Forms.Label();
            this.txtCommentaire = new iTalk.iTalk_TextBox_Small2();
            this.lblFullScreen = new System.Windows.Forms.Label();
            this.txtGroupe = new iTalk.iTalk_TextBox_Small2();
            this.cmdGroupe = new System.Windows.Forms.Button();
            this.flowLayoutPanel11 = new System.Windows.Forms.FlowLayoutPanel();
            this.chkEncRex = new System.Windows.Forms.CheckBox();
            this.chkEncCompta = new System.Windows.Forms.CheckBox();
            this.chkEncaissement = new System.Windows.Forms.CheckBox();
            this.txtIntercom = new iTalk.iTalk_TextBox_Small2();
            this.flowLayoutPanel8 = new System.Windows.Forms.FlowLayoutPanel();
            this.CHKP1 = new System.Windows.Forms.CheckBox();
            this.CHKP2 = new System.Windows.Forms.CheckBox();
            this.CHKP3 = new System.Windows.Forms.CheckBox();
            this.CHKP4 = new System.Windows.Forms.CheckBox();
            this.chkDevisPDA = new System.Windows.Forms.CheckBox();
            this.label20 = new System.Windows.Forms.Label();
            this.label19 = new System.Windows.Forms.Label();
            this.label18 = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.ultraDateTimeEditor5 = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.txtDatePrevue1 = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.ultraDateTimeEditor6 = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.txtDatePrevue2 = new iTalk.iTalk_TextBox_Small2();
            this.label12 = new System.Windows.Forms.Label();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.ultraDateTimeEditor1 = new Infragistics.Win.UltraWinSchedule.UltraCalendarCombo();
            this.txtDateSaisieDe = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.chkDemandeNonTraite = new System.Windows.Forms.CheckBox();
            this.chkVisiteEntretien = new System.Windows.Forms.CheckBox();
            this.chkDapeau = new System.Windows.Forms.CheckBox();
            this.chkDevisEtablir = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.lblLibelleContrat = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdAvisDePassage = new System.Windows.Forms.Button();
            this.cmdClean = new System.Windows.Forms.Button();
            this.cmdPrestations = new System.Windows.Forms.Button();
            this.cmdHistorique = new System.Windows.Forms.Button();
            this.cmbIntervention = new System.Windows.Forms.Button();
            this.cmdBordereauJournee = new System.Windows.Forms.Button();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.CmdEditer = new System.Windows.Forms.Button();
            this.cmdExportExcel = new System.Windows.Forms.Button();
            this.cmdExcelDetail = new System.Windows.Forms.Button();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.ssIntervention = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.panel1 = new System.Windows.Forms.Panel();
            this.panel4 = new System.Windows.Forms.Panel();
            this.label29 = new System.Windows.Forms.Label();
            this.panel3 = new System.Windows.Forms.Panel();
            this.label28 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.label26 = new System.Windows.Forms.Label();
            this.ssDropMatricule = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.ssDropCodeEtat = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblOrderBy = new iTalk.iTalk_TextBox_Small2();
            this.Command2 = new System.Windows.Forms.Button();
            this.Command1 = new System.Windows.Forms.Button();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor4)).BeginInit();
            this.tableLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor3)).BeginInit();
            this.tableLayoutPanel6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtINT_AnaCodeF)).BeginInit();
            this.flowLayoutPanel10.SuspendLayout();
            this.flowLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCodeimmeuble)).BeginInit();
            this.flowLayoutPanel15.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCommercial)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIntervenant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbArticle)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEnergie)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel4.SuspendLayout();
            this.flowLayoutPanel5.SuspendLayout();
            this.flowLayoutPanel6.SuspendLayout();
            this.flowLayoutPanel7.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssComboActivite)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCodeGerant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEtatDevis)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtINT_AnaCode)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBCombo1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDepan)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDispatch)).BeginInit();
            this.flowLayoutPanel12.SuspendLayout();
            this.flowLayoutPanel11.SuspendLayout();
            this.flowLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor5)).BeginInit();
            this.tableLayoutPanel10.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor6)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).BeginInit();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssIntervention)).BeginInit();
            this.panel1.SuspendLayout();
            this.panel4.SuspendLayout();
            this.panel3.SuspendLayout();
            this.panel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropMatricule)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropCodeEtat)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel3, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel4, 0, 2);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 3;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 368F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 9;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.51143F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 9.714038F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.51143F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 8.414285F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.51143F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 7.825974F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 18.51143F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel8, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel7, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel6, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label13, 0, 11);
            this.tableLayoutPanel2.Controls.Add(this.label11, 0, 10);
            this.tableLayoutPanel2.Controls.Add(this.chkAffaireAvecFact, 7, 5);
            this.tableLayoutPanel2.Controls.Add(this.chkAffaireSansFact, 7, 4);
            this.tableLayoutPanel2.Controls.Add(this.chkAchat, 7, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtINT_AnaCodeF, 8, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtNumContrat, 4, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel10, 7, 1);
            this.tableLayoutPanel2.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label1, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel9, 7, 8);
            this.tableLayoutPanel2.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.cmbCodeimmeuble, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel15, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label3, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label4, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.label5, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.cmbStatus, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.label6, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.cmbCommercial, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.label7, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.cmbIntervenant, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.label8, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.label10, 0, 8);
            this.tableLayoutPanel2.Controls.Add(this.cmbArticle, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.cmbEnergie, 1, 8);
            this.tableLayoutPanel2.Controls.Add(this.txtCodeGestionnaire, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel2, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel3, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel4, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel5, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel6, 2, 7);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel7, 2, 8);
            this.tableLayoutPanel2.Controls.Add(this.label15, 5, 0);
            this.tableLayoutPanel2.Controls.Add(this.ssComboActivite, 6, 0);
            this.tableLayoutPanel2.Controls.Add(this.label16, 5, 1);
            this.tableLayoutPanel2.Controls.Add(this.label17, 5, 2);
            this.tableLayoutPanel2.Controls.Add(this.label23, 5, 8);
            this.tableLayoutPanel2.Controls.Add(this.txtBordereauJournee, 7, 0);
            this.tableLayoutPanel2.Controls.Add(this.cmbCodeGerant, 6, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtTotal, 6, 2);
            this.tableLayoutPanel2.Controls.Add(this.cmbEtatDevis, 6, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtINT_AnaCode, 6, 5);
            this.tableLayoutPanel2.Controls.Add(this.label25, 7, 6);
            this.tableLayoutPanel2.Controls.Add(this.label22, 7, 7);
            this.tableLayoutPanel2.Controls.Add(this.SSOleDBCombo1, 8, 7);
            this.tableLayoutPanel2.Controls.Add(this.cmbDepan, 6, 8);
            this.tableLayoutPanel2.Controls.Add(this.label21, 5, 7);
            this.tableLayoutPanel2.Controls.Add(this.txtCP, 6, 7);
            this.tableLayoutPanel2.Controls.Add(this.label24, 7, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtNoIntervention, 4, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtNoEnregistrement, 4, 1);
            this.tableLayoutPanel2.Controls.Add(this.cmdDispatch, 1, 10);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel12, 2, 10);
            this.tableLayoutPanel2.Controls.Add(this.txtCommentaire, 6, 9);
            this.tableLayoutPanel2.Controls.Add(this.lblFullScreen, 8, 0);
            this.tableLayoutPanel2.Controls.Add(this.txtGroupe, 1, 11);
            this.tableLayoutPanel2.Controls.Add(this.cmdGroupe, 2, 11);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel11, 4, 11);
            this.tableLayoutPanel2.Controls.Add(this.txtIntercom, 3, 11);
            this.tableLayoutPanel2.Controls.Add(this.flowLayoutPanel8, 7, 10);
            this.tableLayoutPanel2.Controls.Add(this.chkDevisPDA, 6, 10);
            this.tableLayoutPanel2.Controls.Add(this.label20, 5, 6);
            this.tableLayoutPanel2.Controls.Add(this.label19, 5, 5);
            this.tableLayoutPanel2.Controls.Add(this.label18, 5, 4);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel9, 6, 6);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel10, 8, 6);
            this.tableLayoutPanel2.Controls.Add(this.label12, 5, 9);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel12, 1, 9);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 53);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 12;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1844, 362);
            this.tableLayoutPanel2.TabIndex = 1;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel8.Controls.Add(this.ultraDateTimeEditor4, 1, 0);
            this.tableLayoutPanel8.Controls.Add(this.txtIntereAu, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(555, 60);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(259, 30);
            this.tableLayoutPanel8.TabIndex = 713;
            // 
            // ultraDateTimeEditor4
            // 
            appearance1.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance1.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance1.FontData.Name = "Ubuntu";
            appearance1.FontData.SizeInPoints = 9F;
            this.ultraDateTimeEditor4.Appearance = appearance1;
            this.ultraDateTimeEditor4.AutoSize = false;
            this.ultraDateTimeEditor4.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraDateTimeEditor4.BorderStyleCalendar = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraDateTimeEditor4.BorderStyleDay = Infragistics.Win.UIElementBorderStyle.Rounded4;
            appearance2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance2.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance2.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance2.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance2.ForeColor = System.Drawing.Color.White;
            appearance2.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraDateTimeEditor4.DateButtonAppearance = appearance2;
            appearance3.BackColor = System.Drawing.Color.LightGray;
            appearance3.ForeColor = System.Drawing.Color.Black;
            this.ultraDateTimeEditor4.DateButtonAreaAppearance = appearance3;
            this.ultraDateTimeEditor4.DateButtons.Add(dateButton1);
            this.ultraDateTimeEditor4.DayOfWeekCaptionStyle = Infragistics.Win.UltraWinSchedule.DayOfWeekCaptionStyle.ShortDescription;
            this.ultraDateTimeEditor4.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance4.BackColor = System.Drawing.Color.White;
            appearance4.FontData.SizeInPoints = 11F;
            this.ultraDateTimeEditor4.DropDownAppearance = appearance4;
            this.ultraDateTimeEditor4.Location = new System.Drawing.Point(238, 2);
            this.ultraDateTimeEditor4.Margin = new System.Windows.Forms.Padding(0, 2, 2, 2);
            this.ultraDateTimeEditor4.MaximumSize = new System.Drawing.Size(19, 27);
            this.ultraDateTimeEditor4.MinimumDaySize = new System.Drawing.Size(1, 1);
            this.ultraDateTimeEditor4.MinimumSize = new System.Drawing.Size(19, 27);
            this.ultraDateTimeEditor4.MonthOrientation = Infragistics.Win.UltraWinSchedule.MonthOrientation.DownThenAcross;
            appearance5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ultraDateTimeEditor4.MonthPopupAppearance = appearance5;
            this.ultraDateTimeEditor4.Name = "ultraDateTimeEditor4";
            this.ultraDateTimeEditor4.NonAutoSizeHeight = 27;
            appearance6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance6.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance6.ForeColor = System.Drawing.Color.White;
            this.ultraDateTimeEditor4.ScrollButtonAppearance = appearance6;
            this.ultraDateTimeEditor4.Size = new System.Drawing.Size(19, 27);
            this.ultraDateTimeEditor4.TabIndex = 703;
            this.ultraDateTimeEditor4.Value = "";
            this.ultraDateTimeEditor4.WeekNumbersVisible = true;
            appearance7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance7.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance7.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance7.BackHatchStyle = Infragistics.Win.BackHatchStyle.None;
            appearance7.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance7.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance7.ForeColor = System.Drawing.Color.White;
            appearance7.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraDateTimeEditor4.YearScrollButtonAppearance = appearance7;
            this.ultraDateTimeEditor4.YearScrollButtonsVisible = Infragistics.Win.DefaultableBoolean.True;
            this.ultraDateTimeEditor4.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.ultraDateTimeEditor4_BeforeDropDown);
            this.ultraDateTimeEditor4.AfterDropDown += new System.EventHandler(this.ultraDateTimeEditor4_AfterDropDown);
            this.ultraDateTimeEditor4.AfterCloseUp += new System.EventHandler(this.ultraDateTimeEditor4_AfterCloseUp);
            this.ultraDateTimeEditor4.ValueChanged += new System.EventHandler(this.ultraDateTimeEditor4_ValueChanged);
            // 
            // txtIntereAu
            // 
            this.txtIntereAu.AccAcceptNumbersOnly = false;
            this.txtIntereAu.AccAllowComma = false;
            this.txtIntereAu.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntereAu.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntereAu.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntereAu.AccHidenValue = "";
            this.txtIntereAu.AccNotAllowedChars = null;
            this.txtIntereAu.AccReadOnly = false;
            this.txtIntereAu.AccReadOnlyAllowDelete = false;
            this.txtIntereAu.AccRequired = false;
            this.txtIntereAu.BackColor = System.Drawing.Color.White;
            this.txtIntereAu.CustomBackColor = System.Drawing.Color.White;
            this.txtIntereAu.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntereAu.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtIntereAu.ForeColor = System.Drawing.Color.Black;
            this.txtIntereAu.Location = new System.Drawing.Point(2, 2);
            this.txtIntereAu.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.txtIntereAu.MaxLength = 32767;
            this.txtIntereAu.Multiline = false;
            this.txtIntereAu.Name = "txtIntereAu";
            this.txtIntereAu.ReadOnly = false;
            this.txtIntereAu.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntereAu.Size = new System.Drawing.Size(236, 27);
            this.txtIntereAu.TabIndex = 707;
            this.txtIntereAu.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntereAu.UseSystemPasswordChar = false;
            this.txtIntereAu.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIntereAu_KeyPress);
            this.txtIntereAu.Validating += new System.ComponentModel.CancelEventHandler(this.txtIntereAu_Validating);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel7.Controls.Add(this.ultraDateTimeEditor3, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtinterDe, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(160, 60);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(259, 30);
            this.tableLayoutPanel7.TabIndex = 712;
            // 
            // ultraDateTimeEditor3
            // 
            appearance8.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance8.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance8.FontData.Name = "Ubuntu";
            appearance8.FontData.SizeInPoints = 9F;
            this.ultraDateTimeEditor3.Appearance = appearance8;
            this.ultraDateTimeEditor3.AutoSelectionUpdate = true;
            this.ultraDateTimeEditor3.AutoSize = false;
            this.ultraDateTimeEditor3.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraDateTimeEditor3.BorderStyleCalendar = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraDateTimeEditor3.BorderStyleDay = Infragistics.Win.UIElementBorderStyle.Rounded4;
            appearance9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance9.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance9.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance9.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance9.ForeColor = System.Drawing.Color.White;
            appearance9.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraDateTimeEditor3.DateButtonAppearance = appearance9;
            appearance10.BackColor = System.Drawing.Color.LightGray;
            appearance10.ForeColor = System.Drawing.Color.Black;
            this.ultraDateTimeEditor3.DateButtonAreaAppearance = appearance10;
            this.ultraDateTimeEditor3.DateButtons.Add(dateButton2);
            this.ultraDateTimeEditor3.DayOfWeekCaptionStyle = Infragistics.Win.UltraWinSchedule.DayOfWeekCaptionStyle.ShortDescription;
            this.ultraDateTimeEditor3.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance11.BackColor = System.Drawing.Color.White;
            appearance11.FontData.SizeInPoints = 11F;
            this.ultraDateTimeEditor3.DropDownAppearance = appearance11;
            this.ultraDateTimeEditor3.Location = new System.Drawing.Point(238, 2);
            this.ultraDateTimeEditor3.Margin = new System.Windows.Forms.Padding(0, 2, 2, 2);
            this.ultraDateTimeEditor3.MaximumSize = new System.Drawing.Size(19, 27);
            this.ultraDateTimeEditor3.MinimumDaySize = new System.Drawing.Size(1, 1);
            this.ultraDateTimeEditor3.MinimumSize = new System.Drawing.Size(19, 27);
            this.ultraDateTimeEditor3.MonthOrientation = Infragistics.Win.UltraWinSchedule.MonthOrientation.DownThenAcross;
            appearance12.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ultraDateTimeEditor3.MonthPopupAppearance = appearance12;
            this.ultraDateTimeEditor3.Name = "ultraDateTimeEditor3";
            this.ultraDateTimeEditor3.NonAutoSizeHeight = 27;
            appearance13.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance13.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance13.ForeColor = System.Drawing.Color.White;
            this.ultraDateTimeEditor3.ScrollButtonAppearance = appearance13;
            this.ultraDateTimeEditor3.Size = new System.Drawing.Size(19, 27);
            this.ultraDateTimeEditor3.TabIndex = 703;
            this.ultraDateTimeEditor3.Value = "";
            this.ultraDateTimeEditor3.WeekNumbersVisible = true;
            appearance14.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance14.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance14.BackHatchStyle = Infragistics.Win.BackHatchStyle.None;
            appearance14.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance14.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance14.ForeColor = System.Drawing.Color.White;
            appearance14.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraDateTimeEditor3.YearScrollButtonAppearance = appearance14;
            this.ultraDateTimeEditor3.YearScrollButtonsVisible = Infragistics.Win.DefaultableBoolean.True;
            this.ultraDateTimeEditor3.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.ultraDateTimeEditor3_BeforeDropDown);
            this.ultraDateTimeEditor3.AfterCloseUp += new System.EventHandler(this.ultraDateTimeEditor3_AfterCloseUp);
            this.ultraDateTimeEditor3.ValueChanged += new System.EventHandler(this.ultraDateTimeEditor3_ValueChanged);
            // 
            // txtinterDe
            // 
            this.txtinterDe.AccAcceptNumbersOnly = false;
            this.txtinterDe.AccAllowComma = false;
            this.txtinterDe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtinterDe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtinterDe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtinterDe.AccHidenValue = "";
            this.txtinterDe.AccNotAllowedChars = null;
            this.txtinterDe.AccReadOnly = false;
            this.txtinterDe.AccReadOnlyAllowDelete = false;
            this.txtinterDe.AccRequired = false;
            this.txtinterDe.BackColor = System.Drawing.Color.White;
            this.txtinterDe.CustomBackColor = System.Drawing.Color.White;
            this.txtinterDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtinterDe.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtinterDe.ForeColor = System.Drawing.Color.Black;
            this.txtinterDe.Location = new System.Drawing.Point(2, 2);
            this.txtinterDe.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.txtinterDe.MaxLength = 32767;
            this.txtinterDe.Multiline = false;
            this.txtinterDe.Name = "txtinterDe";
            this.txtinterDe.ReadOnly = false;
            this.txtinterDe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtinterDe.Size = new System.Drawing.Size(236, 27);
            this.txtinterDe.TabIndex = 706;
            this.txtinterDe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtinterDe.UseSystemPasswordChar = false;
            this.txtinterDe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtinterDe_KeyPress);
            this.txtinterDe.Validating += new System.ComponentModel.CancelEventHandler(this.txtinterDe_Validating);
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel6.Controls.Add(this.ultraDateTimeEditor2, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.txtDateSaisieFin, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(555, 0);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(259, 30);
            this.tableLayoutPanel6.TabIndex = 711;
            // 
            // ultraDateTimeEditor2
            // 
            appearance15.BorderColor = System.Drawing.Color.Black;
            appearance15.BorderColor2 = System.Drawing.Color.Black;
            appearance15.BorderColor3DBase = System.Drawing.Color.Black;
            appearance15.FontData.Name = "Ubuntu";
            appearance15.FontData.SizeInPoints = 9F;
            this.ultraDateTimeEditor2.Appearance = appearance15;
            this.ultraDateTimeEditor2.AutoSelectionUpdate = true;
            this.ultraDateTimeEditor2.AutoSize = false;
            this.ultraDateTimeEditor2.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraDateTimeEditor2.BorderStyleCalendar = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraDateTimeEditor2.BorderStyleDay = Infragistics.Win.UIElementBorderStyle.Rounded4;
            appearance16.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance16.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance16.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance16.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance16.ForeColor = System.Drawing.Color.White;
            appearance16.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraDateTimeEditor2.DateButtonAppearance = appearance16;
            appearance17.BackColor = System.Drawing.Color.LightGray;
            appearance17.ForeColor = System.Drawing.Color.Black;
            this.ultraDateTimeEditor2.DateButtonAreaAppearance = appearance17;
            this.ultraDateTimeEditor2.DateButtons.Add(dateButton3);
            this.ultraDateTimeEditor2.DayOfWeekCaptionStyle = Infragistics.Win.UltraWinSchedule.DayOfWeekCaptionStyle.ShortDescription;
            this.ultraDateTimeEditor2.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance18.BackColor = System.Drawing.Color.White;
            appearance18.FontData.SizeInPoints = 11F;
            this.ultraDateTimeEditor2.DropDownAppearance = appearance18;
            this.ultraDateTimeEditor2.Location = new System.Drawing.Point(239, 2);
            this.ultraDateTimeEditor2.Margin = new System.Windows.Forms.Padding(0, 2, 2, 2);
            this.ultraDateTimeEditor2.MaximumSize = new System.Drawing.Size(19, 27);
            this.ultraDateTimeEditor2.MinimumDaySize = new System.Drawing.Size(1, 1);
            this.ultraDateTimeEditor2.MinimumSize = new System.Drawing.Size(19, 27);
            this.ultraDateTimeEditor2.MonthOrientation = Infragistics.Win.UltraWinSchedule.MonthOrientation.DownThenAcross;
            appearance19.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ultraDateTimeEditor2.MonthPopupAppearance = appearance19;
            this.ultraDateTimeEditor2.Name = "ultraDateTimeEditor2";
            this.ultraDateTimeEditor2.NonAutoSizeHeight = 27;
            appearance20.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance20.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance20.ForeColor = System.Drawing.Color.White;
            this.ultraDateTimeEditor2.ScrollButtonAppearance = appearance20;
            this.ultraDateTimeEditor2.Size = new System.Drawing.Size(19, 27);
            this.ultraDateTimeEditor2.TabIndex = 703;
            this.ultraDateTimeEditor2.Value = "";
            this.ultraDateTimeEditor2.WeekNumbersVisible = true;
            appearance21.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance21.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance21.BackHatchStyle = Infragistics.Win.BackHatchStyle.None;
            appearance21.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance21.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance21.ForeColor = System.Drawing.Color.White;
            appearance21.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraDateTimeEditor2.YearScrollButtonAppearance = appearance21;
            this.ultraDateTimeEditor2.YearScrollButtonsVisible = Infragistics.Win.DefaultableBoolean.True;
            this.ultraDateTimeEditor2.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.ultraDateTimeEditor2_BeforeDropDown);
            this.ultraDateTimeEditor2.AfterCloseUp += new System.EventHandler(this.ultraDateTimeEditor2_AfterCloseUp);
            this.ultraDateTimeEditor2.ValueChanged += new System.EventHandler(this.ultraDateTimeEditor2_ValueChanged);
            // 
            // txtDateSaisieFin
            // 
            this.txtDateSaisieFin.AccAcceptNumbersOnly = false;
            this.txtDateSaisieFin.AccAllowComma = false;
            this.txtDateSaisieFin.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateSaisieFin.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateSaisieFin.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateSaisieFin.AccHidenValue = "";
            this.txtDateSaisieFin.AccNotAllowedChars = null;
            this.txtDateSaisieFin.AccReadOnly = false;
            this.txtDateSaisieFin.AccReadOnlyAllowDelete = false;
            this.txtDateSaisieFin.AccRequired = false;
            this.txtDateSaisieFin.BackColor = System.Drawing.Color.White;
            this.txtDateSaisieFin.CustomBackColor = System.Drawing.Color.White;
            this.txtDateSaisieFin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateSaisieFin.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtDateSaisieFin.ForeColor = System.Drawing.Color.Black;
            this.txtDateSaisieFin.Location = new System.Drawing.Point(2, 2);
            this.txtDateSaisieFin.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.txtDateSaisieFin.MaxLength = 32767;
            this.txtDateSaisieFin.Multiline = false;
            this.txtDateSaisieFin.Name = "txtDateSaisieFin";
            this.txtDateSaisieFin.ReadOnly = false;
            this.txtDateSaisieFin.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateSaisieFin.Size = new System.Drawing.Size(237, 27);
            this.txtDateSaisieFin.TabIndex = 705;
            this.txtDateSaisieFin.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateSaisieFin.UseSystemPasswordChar = false;
            this.txtDateSaisieFin.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDateSaisieFin_KeyPress);
            this.txtDateSaisieFin.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateSaisieFin_Validating);
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 330);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(154, 32);
            this.label13.TabIndex = 697;
            this.label13.Text = "Groupe";
            this.label13.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 300);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(154, 30);
            this.label11.TabIndex = 309;
            this.label11.Text = "Dispatcheur";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // chkAffaireAvecFact
            // 
            this.chkAffaireAvecFact.AutoSize = true;
            this.chkAffaireAvecFact.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tableLayoutPanel2.SetColumnSpan(this.chkAffaireAvecFact, 2);
            this.chkAffaireAvecFact.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkAffaireAvecFact.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAffaireAvecFact.Location = new System.Drawing.Point(1474, 152);
            this.chkAffaireAvecFact.Margin = new System.Windows.Forms.Padding(2);
            this.chkAffaireAvecFact.Name = "chkAffaireAvecFact";
            this.chkAffaireAvecFact.Size = new System.Drawing.Size(368, 26);
            this.chkAffaireAvecFact.TabIndex = 302;
            this.chkAffaireAvecFact.Text = "Affaire avec Facture";
            this.chkAffaireAvecFact.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkAffaireAvecFact.UseVisualStyleBackColor = true;
            // 
            // chkAffaireSansFact
            // 
            this.chkAffaireSansFact.AutoSize = true;
            this.chkAffaireSansFact.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tableLayoutPanel2.SetColumnSpan(this.chkAffaireSansFact, 2);
            this.chkAffaireSansFact.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkAffaireSansFact.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAffaireSansFact.Location = new System.Drawing.Point(1474, 122);
            this.chkAffaireSansFact.Margin = new System.Windows.Forms.Padding(2);
            this.chkAffaireSansFact.Name = "chkAffaireSansFact";
            this.chkAffaireSansFact.Size = new System.Drawing.Size(368, 26);
            this.chkAffaireSansFact.TabIndex = 301;
            this.chkAffaireSansFact.Text = "Affaire sans Facture";
            this.chkAffaireSansFact.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkAffaireSansFact.UseVisualStyleBackColor = true;
            // 
            // chkAchat
            // 
            this.chkAchat.AutoSize = true;
            this.chkAchat.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.tableLayoutPanel2.SetColumnSpan(this.chkAchat, 2);
            this.chkAchat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkAchat.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkAchat.Location = new System.Drawing.Point(1474, 92);
            this.chkAchat.Margin = new System.Windows.Forms.Padding(2);
            this.chkAchat.Name = "chkAchat";
            this.chkAchat.Size = new System.Drawing.Size(368, 26);
            this.chkAchat.TabIndex = 300;
            this.chkAchat.Text = "Interventions avec achat";
            this.chkAchat.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkAchat.UseVisualStyleBackColor = true;
            // 
            // txtINT_AnaCodeF
            // 
            appearance22.BackColor = System.Drawing.SystemColors.Window;
            appearance22.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtINT_AnaCodeF.DisplayLayout.Appearance = appearance22;
            this.txtINT_AnaCodeF.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtINT_AnaCodeF.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance23.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance23.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance23.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance23.BorderColor = System.Drawing.SystemColors.Window;
            this.txtINT_AnaCodeF.DisplayLayout.GroupByBox.Appearance = appearance23;
            appearance24.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtINT_AnaCodeF.DisplayLayout.GroupByBox.BandLabelAppearance = appearance24;
            this.txtINT_AnaCodeF.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance25.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance25.BackColor2 = System.Drawing.SystemColors.Control;
            appearance25.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance25.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtINT_AnaCodeF.DisplayLayout.GroupByBox.PromptAppearance = appearance25;
            this.txtINT_AnaCodeF.DisplayLayout.MaxColScrollRegions = 1;
            this.txtINT_AnaCodeF.DisplayLayout.MaxRowScrollRegions = 1;
            appearance26.BackColor = System.Drawing.SystemColors.Window;
            appearance26.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtINT_AnaCodeF.DisplayLayout.Override.ActiveCellAppearance = appearance26;
            appearance27.BackColor = System.Drawing.SystemColors.Highlight;
            appearance27.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txtINT_AnaCodeF.DisplayLayout.Override.ActiveRowAppearance = appearance27;
            this.txtINT_AnaCodeF.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtINT_AnaCodeF.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance28.BackColor = System.Drawing.SystemColors.Window;
            this.txtINT_AnaCodeF.DisplayLayout.Override.CardAreaAppearance = appearance28;
            appearance29.BorderColor = System.Drawing.Color.Silver;
            appearance29.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtINT_AnaCodeF.DisplayLayout.Override.CellAppearance = appearance29;
            this.txtINT_AnaCodeF.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtINT_AnaCodeF.DisplayLayout.Override.CellPadding = 0;
            appearance30.BackColor = System.Drawing.SystemColors.Control;
            appearance30.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance30.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance30.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance30.BorderColor = System.Drawing.SystemColors.Window;
            this.txtINT_AnaCodeF.DisplayLayout.Override.GroupByRowAppearance = appearance30;
            appearance31.TextHAlignAsString = "Left";
            this.txtINT_AnaCodeF.DisplayLayout.Override.HeaderAppearance = appearance31;
            this.txtINT_AnaCodeF.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtINT_AnaCodeF.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance32.BackColor = System.Drawing.SystemColors.Window;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            this.txtINT_AnaCodeF.DisplayLayout.Override.RowAppearance = appearance32;
            this.txtINT_AnaCodeF.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance33.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtINT_AnaCodeF.DisplayLayout.Override.TemplateAddRowAppearance = appearance33;
            this.txtINT_AnaCodeF.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtINT_AnaCodeF.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtINT_AnaCodeF.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtINT_AnaCodeF.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtINT_AnaCodeF.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtINT_AnaCodeF.Location = new System.Drawing.Point(1583, 62);
            this.txtINT_AnaCodeF.Margin = new System.Windows.Forms.Padding(2);
            this.txtINT_AnaCodeF.Name = "txtINT_AnaCodeF";
            this.txtINT_AnaCodeF.Size = new System.Drawing.Size(259, 27);
            this.txtINT_AnaCodeF.TabIndex = 405;
            this.txtINT_AnaCodeF.Visible = false;
            // 
            // txtNumContrat
            // 
            this.txtNumContrat.AccAcceptNumbersOnly = false;
            this.txtNumContrat.AccAllowComma = false;
            this.txtNumContrat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNumContrat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNumContrat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNumContrat.AccHidenValue = "";
            this.txtNumContrat.AccNotAllowedChars = null;
            this.txtNumContrat.AccReadOnly = false;
            this.txtNumContrat.AccReadOnlyAllowDelete = false;
            this.txtNumContrat.AccRequired = false;
            this.txtNumContrat.BackColor = System.Drawing.Color.White;
            this.txtNumContrat.CustomBackColor = System.Drawing.Color.White;
            this.txtNumContrat.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNumContrat.ForeColor = System.Drawing.Color.Black;
            this.txtNumContrat.Location = new System.Drawing.Point(816, 2);
            this.txtNumContrat.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumContrat.MaxLength = 32767;
            this.txtNumContrat.Multiline = false;
            this.txtNumContrat.Name = "txtNumContrat";
            this.txtNumContrat.ReadOnly = false;
            this.txtNumContrat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNumContrat.Size = new System.Drawing.Size(23, 27);
            this.txtNumContrat.TabIndex = 405;
            this.txtNumContrat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNumContrat.UseSystemPasswordChar = false;
            this.txtNumContrat.Visible = false;
            // 
            // flowLayoutPanel10
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel10, 2);
            this.flowLayoutPanel10.Controls.Add(this.cmdGerant);
            this.flowLayoutPanel10.Controls.Add(this.chkExclGerant);
            this.flowLayoutPanel10.Controls.Add(this.label27);
            this.flowLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel10.Location = new System.Drawing.Point(1475, 33);
            this.flowLayoutPanel10.Name = "flowLayoutPanel10";
            this.flowLayoutPanel10.Size = new System.Drawing.Size(366, 24);
            this.flowLayoutPanel10.TabIndex = 403;
            // 
            // cmdGerant
            // 
            this.cmdGerant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdGerant.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdGerant.FlatAppearance.BorderSize = 0;
            this.cmdGerant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdGerant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdGerant.Image = ((System.Drawing.Image)(resources.GetObject("cmdGerant.Image")));
            this.cmdGerant.Location = new System.Drawing.Point(3, 3);
            this.cmdGerant.Name = "cmdGerant";
            this.cmdGerant.Size = new System.Drawing.Size(25, 20);
            this.cmdGerant.TabIndex = 367;
            this.cmdGerant.Tag = "";
            this.toolTip1.SetToolTip(this.cmdGerant, "Recherche du gérant");
            this.cmdGerant.UseVisualStyleBackColor = false;
            this.cmdGerant.Click += new System.EventHandler(this.cmdGerant_Click);
            // 
            // chkExclGerant
            // 
            this.chkExclGerant.AutoSize = true;
            this.chkExclGerant.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExclGerant.Location = new System.Drawing.Point(34, 3);
            this.chkExclGerant.Name = "chkExclGerant";
            this.chkExclGerant.Size = new System.Drawing.Size(81, 23);
            this.chkExclGerant.TabIndex = 570;
            this.chkExclGerant.Text = "Exclure";
            this.chkExclGerant.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkExclGerant.UseVisualStyleBackColor = true;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(121, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(0, 17);
            this.label27.TabIndex = 571;
            this.label27.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(154, 30);
            this.label33.TabIndex = 384;
            this.label33.Text = "Ouvertures depuis le";
            this.label33.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(422, 0);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(130, 30);
            this.label1.TabIndex = 504;
            this.label1.Text = "jusqu\'au";
            this.label1.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // flowLayoutPanel9
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel9, 2);
            this.flowLayoutPanel9.Controls.Add(this.cmdRechDepan);
            this.flowLayoutPanel9.Controls.Add(this.chkExclDepan);
            this.flowLayoutPanel9.Controls.Add(this.lbllibDepanneur);
            this.flowLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel9.Location = new System.Drawing.Point(1475, 243);
            this.flowLayoutPanel9.Name = "flowLayoutPanel9";
            this.flowLayoutPanel9.Size = new System.Drawing.Size(366, 24);
            this.flowLayoutPanel9.TabIndex = 402;
            // 
            // cmdRechDepan
            // 
            this.cmdRechDepan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechDepan.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdRechDepan.FlatAppearance.BorderSize = 0;
            this.cmdRechDepan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechDepan.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechDepan.Image = ((System.Drawing.Image)(resources.GetObject("cmdRechDepan.Image")));
            this.cmdRechDepan.Location = new System.Drawing.Point(3, 3);
            this.cmdRechDepan.Name = "cmdRechDepan";
            this.cmdRechDepan.Size = new System.Drawing.Size(25, 20);
            this.cmdRechDepan.TabIndex = 367;
            this.cmdRechDepan.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechDepan, "Recherche de l\'intervenant");
            this.cmdRechDepan.UseVisualStyleBackColor = false;
            this.cmdRechDepan.Click += new System.EventHandler(this.cmdRechDepan_Click);
            // 
            // chkExclDepan
            // 
            this.chkExclDepan.AutoSize = true;
            this.chkExclDepan.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExclDepan.Location = new System.Drawing.Point(34, 3);
            this.chkExclDepan.Name = "chkExclDepan";
            this.chkExclDepan.Size = new System.Drawing.Size(81, 23);
            this.chkExclDepan.TabIndex = 570;
            this.chkExclDepan.Text = "Exclure";
            this.chkExclDepan.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkExclDepan.UseVisualStyleBackColor = true;
            // 
            // lbllibDepanneur
            // 
            this.lbllibDepanneur.AutoSize = true;
            this.lbllibDepanneur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbllibDepanneur.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllibDepanneur.Location = new System.Drawing.Point(121, 0);
            this.lbllibDepanneur.Name = "lbllibDepanneur";
            this.lbllibDepanneur.Size = new System.Drawing.Size(0, 29);
            this.lbllibDepanneur.TabIndex = 571;
            this.lbllibDepanneur.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 30);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(154, 30);
            this.label2.TabIndex = 506;
            this.label2.Text = "Sélection Imm.";
            this.label2.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbCodeimmeuble
            // 
            appearance34.BackColor = System.Drawing.SystemColors.Window;
            appearance34.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbCodeimmeuble.DisplayLayout.Appearance = appearance34;
            this.cmbCodeimmeuble.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbCodeimmeuble.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance35.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance35.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance35.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance35.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCodeimmeuble.DisplayLayout.GroupByBox.Appearance = appearance35;
            appearance36.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCodeimmeuble.DisplayLayout.GroupByBox.BandLabelAppearance = appearance36;
            this.cmbCodeimmeuble.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance37.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance37.BackColor2 = System.Drawing.SystemColors.Control;
            appearance37.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance37.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCodeimmeuble.DisplayLayout.GroupByBox.PromptAppearance = appearance37;
            this.cmbCodeimmeuble.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbCodeimmeuble.DisplayLayout.MaxRowScrollRegions = 1;
            appearance38.BackColor = System.Drawing.SystemColors.Window;
            appearance38.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbCodeimmeuble.DisplayLayout.Override.ActiveCellAppearance = appearance38;
            appearance39.BackColor = System.Drawing.SystemColors.Highlight;
            appearance39.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbCodeimmeuble.DisplayLayout.Override.ActiveRowAppearance = appearance39;
            this.cmbCodeimmeuble.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbCodeimmeuble.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance40.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCodeimmeuble.DisplayLayout.Override.CardAreaAppearance = appearance40;
            appearance41.BorderColor = System.Drawing.Color.Silver;
            appearance41.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbCodeimmeuble.DisplayLayout.Override.CellAppearance = appearance41;
            this.cmbCodeimmeuble.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbCodeimmeuble.DisplayLayout.Override.CellPadding = 0;
            appearance42.BackColor = System.Drawing.SystemColors.Control;
            appearance42.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance42.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance42.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance42.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCodeimmeuble.DisplayLayout.Override.GroupByRowAppearance = appearance42;
            appearance43.TextHAlignAsString = "Left";
            this.cmbCodeimmeuble.DisplayLayout.Override.HeaderAppearance = appearance43;
            this.cmbCodeimmeuble.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbCodeimmeuble.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance44.BackColor = System.Drawing.SystemColors.Window;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            this.cmbCodeimmeuble.DisplayLayout.Override.RowAppearance = appearance44;
            this.cmbCodeimmeuble.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance45.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbCodeimmeuble.DisplayLayout.Override.TemplateAddRowAppearance = appearance45;
            this.cmbCodeimmeuble.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbCodeimmeuble.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbCodeimmeuble.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbCodeimmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCodeimmeuble.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmbCodeimmeuble.Location = new System.Drawing.Point(162, 32);
            this.cmbCodeimmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCodeimmeuble.Name = "cmbCodeimmeuble";
            this.cmbCodeimmeuble.Size = new System.Drawing.Size(255, 27);
            this.cmbCodeimmeuble.TabIndex = 676;
            this.cmbCodeimmeuble.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbCodeimmeuble_BeforeDropDown);
            this.cmbCodeimmeuble.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbCodeimmeuble_KeyPress);
            // 
            // flowLayoutPanel15
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel15, 2);
            this.flowLayoutPanel15.Controls.Add(this.cmdRechercheImmeuble);
            this.flowLayoutPanel15.Controls.Add(this.chkExclImm);
            this.flowLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel15.Location = new System.Drawing.Point(419, 30);
            this.flowLayoutPanel15.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel15.Name = "flowLayoutPanel15";
            this.flowLayoutPanel15.Size = new System.Drawing.Size(395, 30);
            this.flowLayoutPanel15.TabIndex = 658;
            // 
            // cmdRechercheImmeuble
            // 
            this.cmdRechercheImmeuble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheImmeuble.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdRechercheImmeuble.FlatAppearance.BorderSize = 0;
            this.cmdRechercheImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheImmeuble.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheImmeuble.Image = ((System.Drawing.Image)(resources.GetObject("cmdRechercheImmeuble.Image")));
            this.cmdRechercheImmeuble.Location = new System.Drawing.Point(3, 3);
            this.cmdRechercheImmeuble.Name = "cmdRechercheImmeuble";
            this.cmdRechercheImmeuble.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheImmeuble.TabIndex = 366;
            this.cmdRechercheImmeuble.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechercheImmeuble, "Recherche de l\'immeuble");
            this.cmdRechercheImmeuble.UseVisualStyleBackColor = false;
            this.cmdRechercheImmeuble.Click += new System.EventHandler(this.cmdRechercheImmeuble_Click);
            // 
            // chkExclImm
            // 
            this.chkExclImm.AutoSize = true;
            this.chkExclImm.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExclImm.Location = new System.Drawing.Point(34, 3);
            this.chkExclImm.Name = "chkExclImm";
            this.chkExclImm.Size = new System.Drawing.Size(81, 23);
            this.chkExclImm.TabIndex = 577;
            this.chkExclImm.Text = "Exclure";
            this.chkExclImm.UseVisualStyleBackColor = true;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 60);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(154, 30);
            this.label3.TabIndex = 659;
            this.label3.Text = "Réalisées depuis le";
            this.label3.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(422, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(130, 30);
            this.label4.TabIndex = 661;
            this.label4.Text = "jusqu\'au";
            this.label4.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 90);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(154, 30);
            this.label5.TabIndex = 663;
            this.label5.Text = "Statut";
            this.label5.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbStatus
            // 
            appearance46.BackColor = System.Drawing.SystemColors.Window;
            appearance46.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbStatus.DisplayLayout.Appearance = appearance46;
            this.cmbStatus.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbStatus.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance47.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance47.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance47.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance47.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbStatus.DisplayLayout.GroupByBox.Appearance = appearance47;
            appearance48.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbStatus.DisplayLayout.GroupByBox.BandLabelAppearance = appearance48;
            this.cmbStatus.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance49.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance49.BackColor2 = System.Drawing.SystemColors.Control;
            appearance49.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance49.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbStatus.DisplayLayout.GroupByBox.PromptAppearance = appearance49;
            this.cmbStatus.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbStatus.DisplayLayout.MaxRowScrollRegions = 1;
            appearance50.BackColor = System.Drawing.SystemColors.Window;
            appearance50.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbStatus.DisplayLayout.Override.ActiveCellAppearance = appearance50;
            appearance51.BackColor = System.Drawing.SystemColors.Highlight;
            appearance51.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbStatus.DisplayLayout.Override.ActiveRowAppearance = appearance51;
            this.cmbStatus.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbStatus.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance52.BackColor = System.Drawing.SystemColors.Window;
            this.cmbStatus.DisplayLayout.Override.CardAreaAppearance = appearance52;
            appearance53.BorderColor = System.Drawing.Color.Silver;
            appearance53.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbStatus.DisplayLayout.Override.CellAppearance = appearance53;
            this.cmbStatus.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbStatus.DisplayLayout.Override.CellPadding = 0;
            appearance54.BackColor = System.Drawing.SystemColors.Control;
            appearance54.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance54.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance54.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance54.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbStatus.DisplayLayout.Override.GroupByRowAppearance = appearance54;
            appearance55.TextHAlignAsString = "Left";
            this.cmbStatus.DisplayLayout.Override.HeaderAppearance = appearance55;
            this.cmbStatus.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbStatus.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance56.BackColor = System.Drawing.SystemColors.Window;
            appearance56.BorderColor = System.Drawing.Color.Silver;
            this.cmbStatus.DisplayLayout.Override.RowAppearance = appearance56;
            this.cmbStatus.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance57.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbStatus.DisplayLayout.Override.TemplateAddRowAppearance = appearance57;
            this.cmbStatus.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbStatus.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbStatus.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbStatus.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmbStatus.Location = new System.Drawing.Point(162, 92);
            this.cmbStatus.Margin = new System.Windows.Forms.Padding(2);
            this.cmbStatus.Name = "cmbStatus";
            this.cmbStatus.Size = new System.Drawing.Size(255, 27);
            this.cmbStatus.TabIndex = 678;
            this.cmbStatus.AfterCloseUp += new System.EventHandler(this.cmbStatus_AfterCloseUp);
            this.cmbStatus.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbStatus_BeforeDropDown);
            this.cmbStatus.TextChanged += new System.EventHandler(this.cmbStatus_TextChanged);
            this.cmbStatus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbStatus_KeyPress);
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 120);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(154, 30);
            this.label6.TabIndex = 665;
            this.label6.Text = "Commercial";
            this.label6.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbCommercial
            // 
            appearance58.BackColor = System.Drawing.SystemColors.Window;
            appearance58.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbCommercial.DisplayLayout.Appearance = appearance58;
            this.cmbCommercial.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbCommercial.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance59.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance59.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance59.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance59.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCommercial.DisplayLayout.GroupByBox.Appearance = appearance59;
            appearance60.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCommercial.DisplayLayout.GroupByBox.BandLabelAppearance = appearance60;
            this.cmbCommercial.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance61.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance61.BackColor2 = System.Drawing.SystemColors.Control;
            appearance61.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance61.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCommercial.DisplayLayout.GroupByBox.PromptAppearance = appearance61;
            this.cmbCommercial.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbCommercial.DisplayLayout.MaxRowScrollRegions = 1;
            appearance62.BackColor = System.Drawing.SystemColors.Window;
            appearance62.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbCommercial.DisplayLayout.Override.ActiveCellAppearance = appearance62;
            appearance63.BackColor = System.Drawing.SystemColors.Highlight;
            appearance63.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbCommercial.DisplayLayout.Override.ActiveRowAppearance = appearance63;
            this.cmbCommercial.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbCommercial.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance64.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCommercial.DisplayLayout.Override.CardAreaAppearance = appearance64;
            appearance65.BorderColor = System.Drawing.Color.Silver;
            appearance65.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbCommercial.DisplayLayout.Override.CellAppearance = appearance65;
            this.cmbCommercial.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbCommercial.DisplayLayout.Override.CellPadding = 0;
            appearance66.BackColor = System.Drawing.SystemColors.Control;
            appearance66.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance66.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance66.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance66.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCommercial.DisplayLayout.Override.GroupByRowAppearance = appearance66;
            appearance67.TextHAlignAsString = "Left";
            this.cmbCommercial.DisplayLayout.Override.HeaderAppearance = appearance67;
            this.cmbCommercial.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbCommercial.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance68.BackColor = System.Drawing.SystemColors.Window;
            appearance68.BorderColor = System.Drawing.Color.Silver;
            this.cmbCommercial.DisplayLayout.Override.RowAppearance = appearance68;
            this.cmbCommercial.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance69.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbCommercial.DisplayLayout.Override.TemplateAddRowAppearance = appearance69;
            this.cmbCommercial.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbCommercial.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbCommercial.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbCommercial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCommercial.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmbCommercial.Location = new System.Drawing.Point(162, 122);
            this.cmbCommercial.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCommercial.Name = "cmbCommercial";
            this.cmbCommercial.Size = new System.Drawing.Size(255, 27);
            this.cmbCommercial.TabIndex = 679;
            this.cmbCommercial.AfterCloseUp += new System.EventHandler(this.cmbCommercial_AfterCloseUp);
            this.cmbCommercial.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbCommercial_BeforeDropDown);
            this.cmbCommercial.TextChanged += new System.EventHandler(this.cmbCommercial_TextChanged);
            this.cmbCommercial.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbCommercial_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 150);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(154, 30);
            this.label7.TabIndex = 667;
            this.label7.Text = "Intervenants";
            this.label7.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbIntervenant
            // 
            appearance70.BackColor = System.Drawing.SystemColors.Window;
            appearance70.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbIntervenant.DisplayLayout.Appearance = appearance70;
            this.cmbIntervenant.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbIntervenant.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance71.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance71.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance71.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance71.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenant.DisplayLayout.GroupByBox.Appearance = appearance71;
            appearance72.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbIntervenant.DisplayLayout.GroupByBox.BandLabelAppearance = appearance72;
            this.cmbIntervenant.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance73.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance73.BackColor2 = System.Drawing.SystemColors.Control;
            appearance73.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance73.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbIntervenant.DisplayLayout.GroupByBox.PromptAppearance = appearance73;
            this.cmbIntervenant.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbIntervenant.DisplayLayout.MaxRowScrollRegions = 1;
            appearance74.BackColor = System.Drawing.SystemColors.Window;
            appearance74.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbIntervenant.DisplayLayout.Override.ActiveCellAppearance = appearance74;
            appearance75.BackColor = System.Drawing.SystemColors.Highlight;
            appearance75.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbIntervenant.DisplayLayout.Override.ActiveRowAppearance = appearance75;
            this.cmbIntervenant.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbIntervenant.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance76.BackColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenant.DisplayLayout.Override.CardAreaAppearance = appearance76;
            appearance77.BorderColor = System.Drawing.Color.Silver;
            appearance77.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbIntervenant.DisplayLayout.Override.CellAppearance = appearance77;
            this.cmbIntervenant.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbIntervenant.DisplayLayout.Override.CellPadding = 0;
            appearance78.BackColor = System.Drawing.SystemColors.Control;
            appearance78.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance78.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance78.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance78.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenant.DisplayLayout.Override.GroupByRowAppearance = appearance78;
            appearance79.TextHAlignAsString = "Left";
            this.cmbIntervenant.DisplayLayout.Override.HeaderAppearance = appearance79;
            this.cmbIntervenant.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbIntervenant.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance80.BackColor = System.Drawing.SystemColors.Window;
            appearance80.BorderColor = System.Drawing.Color.Silver;
            this.cmbIntervenant.DisplayLayout.Override.RowAppearance = appearance80;
            this.cmbIntervenant.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance81.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbIntervenant.DisplayLayout.Override.TemplateAddRowAppearance = appearance81;
            this.cmbIntervenant.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbIntervenant.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbIntervenant.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbIntervenant.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmbIntervenant.Location = new System.Drawing.Point(162, 152);
            this.cmbIntervenant.Margin = new System.Windows.Forms.Padding(2);
            this.cmbIntervenant.Name = "cmbIntervenant";
            this.cmbIntervenant.Size = new System.Drawing.Size(255, 27);
            this.cmbIntervenant.TabIndex = 680;
            this.cmbIntervenant.AfterCloseUp += new System.EventHandler(this.cmbIntervenant_AfterCloseUp);
            this.cmbIntervenant.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbIntervenant_BeforeDropDown);
            this.cmbIntervenant.TextChanged += new System.EventHandler(this.cmbIntervenant_TextChanged);
            this.cmbIntervenant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbIntervenant_KeyPress);
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 180);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(154, 30);
            this.label8.TabIndex = 669;
            this.label8.Text = "Type d\'opération";
            this.label8.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(3, 210);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(154, 30);
            this.label9.TabIndex = 670;
            this.label9.Text = "Gestionnaire";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label10.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 240);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(154, 30);
            this.label10.TabIndex = 671;
            this.label10.Text = "Energie";
            this.label10.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // cmbArticle
            // 
            appearance82.BackColor = System.Drawing.SystemColors.Window;
            appearance82.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbArticle.DisplayLayout.Appearance = appearance82;
            this.cmbArticle.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbArticle.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance83.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance83.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance83.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance83.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbArticle.DisplayLayout.GroupByBox.Appearance = appearance83;
            appearance84.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbArticle.DisplayLayout.GroupByBox.BandLabelAppearance = appearance84;
            this.cmbArticle.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance85.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance85.BackColor2 = System.Drawing.SystemColors.Control;
            appearance85.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance85.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbArticle.DisplayLayout.GroupByBox.PromptAppearance = appearance85;
            this.cmbArticle.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbArticle.DisplayLayout.MaxRowScrollRegions = 1;
            appearance86.BackColor = System.Drawing.SystemColors.Window;
            appearance86.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbArticle.DisplayLayout.Override.ActiveCellAppearance = appearance86;
            appearance87.BackColor = System.Drawing.SystemColors.Highlight;
            appearance87.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbArticle.DisplayLayout.Override.ActiveRowAppearance = appearance87;
            this.cmbArticle.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbArticle.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance88.BackColor = System.Drawing.SystemColors.Window;
            this.cmbArticle.DisplayLayout.Override.CardAreaAppearance = appearance88;
            appearance89.BorderColor = System.Drawing.Color.Silver;
            appearance89.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbArticle.DisplayLayout.Override.CellAppearance = appearance89;
            this.cmbArticle.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbArticle.DisplayLayout.Override.CellPadding = 0;
            appearance90.BackColor = System.Drawing.SystemColors.Control;
            appearance90.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance90.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance90.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance90.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbArticle.DisplayLayout.Override.GroupByRowAppearance = appearance90;
            appearance91.TextHAlignAsString = "Left";
            this.cmbArticle.DisplayLayout.Override.HeaderAppearance = appearance91;
            this.cmbArticle.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbArticle.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance92.BackColor = System.Drawing.SystemColors.Window;
            appearance92.BorderColor = System.Drawing.Color.Silver;
            this.cmbArticle.DisplayLayout.Override.RowAppearance = appearance92;
            this.cmbArticle.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance93.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbArticle.DisplayLayout.Override.TemplateAddRowAppearance = appearance93;
            this.cmbArticle.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbArticle.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbArticle.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbArticle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbArticle.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmbArticle.Location = new System.Drawing.Point(162, 182);
            this.cmbArticle.Margin = new System.Windows.Forms.Padding(2);
            this.cmbArticle.Name = "cmbArticle";
            this.cmbArticle.Size = new System.Drawing.Size(255, 27);
            this.cmbArticle.TabIndex = 681;
            this.cmbArticle.AfterCloseUp += new System.EventHandler(this.cmbArticle_AfterCloseUp);
            this.cmbArticle.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbArticle_BeforeDropDown);
            this.cmbArticle.TextChanged += new System.EventHandler(this.cmbArticle_TextChanged);
            this.cmbArticle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbArticle_KeyPress);
            // 
            // cmbEnergie
            // 
            appearance94.BackColor = System.Drawing.SystemColors.Window;
            appearance94.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbEnergie.DisplayLayout.Appearance = appearance94;
            this.cmbEnergie.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbEnergie.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance95.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance95.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance95.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance95.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbEnergie.DisplayLayout.GroupByBox.Appearance = appearance95;
            appearance96.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbEnergie.DisplayLayout.GroupByBox.BandLabelAppearance = appearance96;
            this.cmbEnergie.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance97.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance97.BackColor2 = System.Drawing.SystemColors.Control;
            appearance97.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance97.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbEnergie.DisplayLayout.GroupByBox.PromptAppearance = appearance97;
            this.cmbEnergie.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbEnergie.DisplayLayout.MaxRowScrollRegions = 1;
            appearance98.BackColor = System.Drawing.SystemColors.Window;
            appearance98.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbEnergie.DisplayLayout.Override.ActiveCellAppearance = appearance98;
            appearance99.BackColor = System.Drawing.SystemColors.Highlight;
            appearance99.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbEnergie.DisplayLayout.Override.ActiveRowAppearance = appearance99;
            this.cmbEnergie.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbEnergie.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance100.BackColor = System.Drawing.SystemColors.Window;
            this.cmbEnergie.DisplayLayout.Override.CardAreaAppearance = appearance100;
            appearance101.BorderColor = System.Drawing.Color.Silver;
            appearance101.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbEnergie.DisplayLayout.Override.CellAppearance = appearance101;
            this.cmbEnergie.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbEnergie.DisplayLayout.Override.CellPadding = 0;
            appearance102.BackColor = System.Drawing.SystemColors.Control;
            appearance102.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance102.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance102.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance102.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbEnergie.DisplayLayout.Override.GroupByRowAppearance = appearance102;
            appearance103.TextHAlignAsString = "Left";
            this.cmbEnergie.DisplayLayout.Override.HeaderAppearance = appearance103;
            this.cmbEnergie.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbEnergie.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance104.BackColor = System.Drawing.SystemColors.Window;
            appearance104.BorderColor = System.Drawing.Color.Silver;
            this.cmbEnergie.DisplayLayout.Override.RowAppearance = appearance104;
            this.cmbEnergie.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance105.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbEnergie.DisplayLayout.Override.TemplateAddRowAppearance = appearance105;
            this.cmbEnergie.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbEnergie.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbEnergie.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbEnergie.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbEnergie.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmbEnergie.Location = new System.Drawing.Point(162, 242);
            this.cmbEnergie.Margin = new System.Windows.Forms.Padding(2);
            this.cmbEnergie.Name = "cmbEnergie";
            this.cmbEnergie.Size = new System.Drawing.Size(255, 27);
            this.cmbEnergie.TabIndex = 683;
            this.cmbEnergie.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbEnergie_BeforeDropDown);
            this.cmbEnergie.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbEnergie_KeyPress);
            // 
            // txtCodeGestionnaire
            // 
            this.txtCodeGestionnaire.AccAcceptNumbersOnly = false;
            this.txtCodeGestionnaire.AccAllowComma = false;
            this.txtCodeGestionnaire.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeGestionnaire.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeGestionnaire.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeGestionnaire.AccHidenValue = "";
            this.txtCodeGestionnaire.AccNotAllowedChars = null;
            this.txtCodeGestionnaire.AccReadOnly = false;
            this.txtCodeGestionnaire.AccReadOnlyAllowDelete = false;
            this.txtCodeGestionnaire.AccRequired = false;
            this.txtCodeGestionnaire.BackColor = System.Drawing.Color.White;
            this.txtCodeGestionnaire.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeGestionnaire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeGestionnaire.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeGestionnaire.ForeColor = System.Drawing.Color.Black;
            this.txtCodeGestionnaire.Location = new System.Drawing.Point(162, 212);
            this.txtCodeGestionnaire.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeGestionnaire.MaxLength = 32767;
            this.txtCodeGestionnaire.Multiline = false;
            this.txtCodeGestionnaire.Name = "txtCodeGestionnaire";
            this.txtCodeGestionnaire.ReadOnly = false;
            this.txtCodeGestionnaire.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeGestionnaire.Size = new System.Drawing.Size(255, 27);
            this.txtCodeGestionnaire.TabIndex = 682;
            this.txtCodeGestionnaire.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeGestionnaire.UseSystemPasswordChar = false;
            this.txtCodeGestionnaire.TextChanged += new System.EventHandler(this.txtCodeGestionnaire_TextChanged);
            this.txtCodeGestionnaire.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeGestionnaire_KeyPress);
            // 
            // flowLayoutPanel2
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel2, 3);
            this.flowLayoutPanel2.Controls.Add(this.chkExclStatut);
            this.flowLayoutPanel2.Controls.Add(this.lblLibStatus);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.Location = new System.Drawing.Point(422, 93);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(507, 24);
            this.flowLayoutPanel2.TabIndex = 383;
            // 
            // chkExclStatut
            // 
            this.chkExclStatut.AutoSize = true;
            this.chkExclStatut.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExclStatut.Location = new System.Drawing.Point(2, 2);
            this.chkExclStatut.Margin = new System.Windows.Forms.Padding(2);
            this.chkExclStatut.Name = "chkExclStatut";
            this.chkExclStatut.Size = new System.Drawing.Size(81, 23);
            this.chkExclStatut.TabIndex = 570;
            this.chkExclStatut.Text = "Exclure";
            this.chkExclStatut.UseVisualStyleBackColor = true;
            // 
            // lblLibStatus
            // 
            this.lblLibStatus.AutoSize = true;
            this.lblLibStatus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibStatus.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLibStatus.Location = new System.Drawing.Point(88, 0);
            this.lblLibStatus.Name = "lblLibStatus";
            this.lblLibStatus.Size = new System.Drawing.Size(0, 27);
            this.lblLibStatus.TabIndex = 727;
            this.lblLibStatus.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // flowLayoutPanel3
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel3, 3);
            this.flowLayoutPanel3.Controls.Add(this.cmdRechercheCommercial);
            this.flowLayoutPanel3.Controls.Add(this.chkExclComm);
            this.flowLayoutPanel3.Controls.Add(this.lblLibCommercial);
            this.flowLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel3.Location = new System.Drawing.Point(422, 123);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(507, 24);
            this.flowLayoutPanel3.TabIndex = 384;
            // 
            // cmdRechercheCommercial
            // 
            this.cmdRechercheCommercial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheCommercial.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdRechercheCommercial.FlatAppearance.BorderSize = 0;
            this.cmdRechercheCommercial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheCommercial.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheCommercial.Image = ((System.Drawing.Image)(resources.GetObject("cmdRechercheCommercial.Image")));
            this.cmdRechercheCommercial.Location = new System.Drawing.Point(3, 3);
            this.cmdRechercheCommercial.Name = "cmdRechercheCommercial";
            this.cmdRechercheCommercial.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheCommercial.TabIndex = 367;
            this.cmdRechercheCommercial.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechercheCommercial, "Recherche de l\'intervenant");
            this.cmdRechercheCommercial.UseVisualStyleBackColor = false;
            this.cmdRechercheCommercial.Click += new System.EventHandler(this.cmdRechercheCommercial_Click);
            // 
            // chkExclComm
            // 
            this.chkExclComm.AutoSize = true;
            this.chkExclComm.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExclComm.Location = new System.Drawing.Point(34, 3);
            this.chkExclComm.Name = "chkExclComm";
            this.chkExclComm.Size = new System.Drawing.Size(81, 23);
            this.chkExclComm.TabIndex = 570;
            this.chkExclComm.Text = "Exclure";
            this.chkExclComm.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkExclComm.UseVisualStyleBackColor = true;
            // 
            // lblLibCommercial
            // 
            this.lblLibCommercial.AutoSize = true;
            this.lblLibCommercial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibCommercial.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLibCommercial.Location = new System.Drawing.Point(121, 0);
            this.lblLibCommercial.Name = "lblLibCommercial";
            this.lblLibCommercial.Size = new System.Drawing.Size(0, 29);
            this.lblLibCommercial.TabIndex = 728;
            this.lblLibCommercial.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel4
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel4, 3);
            this.flowLayoutPanel4.Controls.Add(this.cmdRechercheIntervenant);
            this.flowLayoutPanel4.Controls.Add(this.chkExclInterv);
            this.flowLayoutPanel4.Controls.Add(this.lblLibIntervenant);
            this.flowLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel4.Location = new System.Drawing.Point(422, 153);
            this.flowLayoutPanel4.Name = "flowLayoutPanel4";
            this.flowLayoutPanel4.Size = new System.Drawing.Size(507, 24);
            this.flowLayoutPanel4.TabIndex = 385;
            // 
            // cmdRechercheIntervenant
            // 
            this.cmdRechercheIntervenant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheIntervenant.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdRechercheIntervenant.FlatAppearance.BorderSize = 0;
            this.cmdRechercheIntervenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheIntervenant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheIntervenant.Image = ((System.Drawing.Image)(resources.GetObject("cmdRechercheIntervenant.Image")));
            this.cmdRechercheIntervenant.Location = new System.Drawing.Point(3, 3);
            this.cmdRechercheIntervenant.Name = "cmdRechercheIntervenant";
            this.cmdRechercheIntervenant.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheIntervenant.TabIndex = 367;
            this.cmdRechercheIntervenant.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechercheIntervenant, "Recherche de l\'intervenant");
            this.cmdRechercheIntervenant.UseVisualStyleBackColor = false;
            this.cmdRechercheIntervenant.Click += new System.EventHandler(this.cmdRechercheIntervenant_Click);
            // 
            // chkExclInterv
            // 
            this.chkExclInterv.AutoSize = true;
            this.chkExclInterv.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExclInterv.Location = new System.Drawing.Point(34, 3);
            this.chkExclInterv.Name = "chkExclInterv";
            this.chkExclInterv.Size = new System.Drawing.Size(81, 23);
            this.chkExclInterv.TabIndex = 570;
            this.chkExclInterv.Text = "Exclure";
            this.chkExclInterv.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkExclInterv.UseVisualStyleBackColor = true;
            // 
            // lblLibIntervenant
            // 
            this.lblLibIntervenant.AutoSize = true;
            this.lblLibIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibIntervenant.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLibIntervenant.Location = new System.Drawing.Point(121, 0);
            this.lblLibIntervenant.Name = "lblLibIntervenant";
            this.lblLibIntervenant.Size = new System.Drawing.Size(0, 29);
            this.lblLibIntervenant.TabIndex = 729;
            this.lblLibIntervenant.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel5
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel5, 3);
            this.flowLayoutPanel5.Controls.Add(this.cmdRechercheTypeOperation);
            this.flowLayoutPanel5.Controls.Add(this.chkExclArticle);
            this.flowLayoutPanel5.Controls.Add(this.lblLibTypeOperation);
            this.flowLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel5.Location = new System.Drawing.Point(422, 183);
            this.flowLayoutPanel5.Name = "flowLayoutPanel5";
            this.flowLayoutPanel5.Size = new System.Drawing.Size(507, 24);
            this.flowLayoutPanel5.TabIndex = 386;
            // 
            // cmdRechercheTypeOperation
            // 
            this.cmdRechercheTypeOperation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheTypeOperation.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdRechercheTypeOperation.FlatAppearance.BorderSize = 0;
            this.cmdRechercheTypeOperation.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheTypeOperation.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheTypeOperation.Image = ((System.Drawing.Image)(resources.GetObject("cmdRechercheTypeOperation.Image")));
            this.cmdRechercheTypeOperation.Location = new System.Drawing.Point(3, 3);
            this.cmdRechercheTypeOperation.Name = "cmdRechercheTypeOperation";
            this.cmdRechercheTypeOperation.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheTypeOperation.TabIndex = 367;
            this.cmdRechercheTypeOperation.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechercheTypeOperation, "Recherche de type d\'opération");
            this.cmdRechercheTypeOperation.UseVisualStyleBackColor = false;
            this.cmdRechercheTypeOperation.Click += new System.EventHandler(this.cmdRechercheTypeOperation_Click);
            // 
            // chkExclArticle
            // 
            this.chkExclArticle.AutoSize = true;
            this.chkExclArticle.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExclArticle.Location = new System.Drawing.Point(34, 3);
            this.chkExclArticle.Name = "chkExclArticle";
            this.chkExclArticle.Size = new System.Drawing.Size(81, 23);
            this.chkExclArticle.TabIndex = 570;
            this.chkExclArticle.Text = "Exclure";
            this.chkExclArticle.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkExclArticle.UseVisualStyleBackColor = true;
            // 
            // lblLibTypeOperation
            // 
            this.lblLibTypeOperation.AutoSize = true;
            this.lblLibTypeOperation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibTypeOperation.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLibTypeOperation.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.lblLibTypeOperation.Location = new System.Drawing.Point(121, 0);
            this.lblLibTypeOperation.Name = "lblLibTypeOperation";
            this.lblLibTypeOperation.Size = new System.Drawing.Size(0, 29);
            this.lblLibTypeOperation.TabIndex = 730;
            this.lblLibTypeOperation.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel6
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel6, 3);
            this.flowLayoutPanel6.Controls.Add(this.cmdFindGestionnaire);
            this.flowLayoutPanel6.Controls.Add(this.chkExclGestionnaire);
            this.flowLayoutPanel6.Controls.Add(this.lblLibGestionnaire);
            this.flowLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel6.Location = new System.Drawing.Point(422, 213);
            this.flowLayoutPanel6.Name = "flowLayoutPanel6";
            this.flowLayoutPanel6.Size = new System.Drawing.Size(507, 24);
            this.flowLayoutPanel6.TabIndex = 387;
            // 
            // cmdFindGestionnaire
            // 
            this.cmdFindGestionnaire.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFindGestionnaire.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdFindGestionnaire.FlatAppearance.BorderSize = 0;
            this.cmdFindGestionnaire.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFindGestionnaire.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdFindGestionnaire.Image = ((System.Drawing.Image)(resources.GetObject("cmdFindGestionnaire.Image")));
            this.cmdFindGestionnaire.Location = new System.Drawing.Point(3, 3);
            this.cmdFindGestionnaire.Name = "cmdFindGestionnaire";
            this.cmdFindGestionnaire.Size = new System.Drawing.Size(25, 20);
            this.cmdFindGestionnaire.TabIndex = 367;
            this.cmdFindGestionnaire.Tag = "";
            this.toolTip1.SetToolTip(this.cmdFindGestionnaire, "Recherche de type d\'opération");
            this.cmdFindGestionnaire.UseVisualStyleBackColor = false;
            this.cmdFindGestionnaire.Click += new System.EventHandler(this.cmdFindGestionnaire_Click);
            // 
            // chkExclGestionnaire
            // 
            this.chkExclGestionnaire.AutoSize = true;
            this.chkExclGestionnaire.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExclGestionnaire.Location = new System.Drawing.Point(34, 3);
            this.chkExclGestionnaire.Name = "chkExclGestionnaire";
            this.chkExclGestionnaire.Size = new System.Drawing.Size(81, 23);
            this.chkExclGestionnaire.TabIndex = 570;
            this.chkExclGestionnaire.Text = "Exclure";
            this.chkExclGestionnaire.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkExclGestionnaire.UseVisualStyleBackColor = true;
            // 
            // lblLibGestionnaire
            // 
            this.lblLibGestionnaire.AutoSize = true;
            this.lblLibGestionnaire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibGestionnaire.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLibGestionnaire.Location = new System.Drawing.Point(121, 0);
            this.lblLibGestionnaire.Name = "lblLibGestionnaire";
            this.lblLibGestionnaire.Size = new System.Drawing.Size(0, 29);
            this.lblLibGestionnaire.TabIndex = 731;
            this.lblLibGestionnaire.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel7
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel7, 3);
            this.flowLayoutPanel7.Controls.Add(this.cmdFindEnergie);
            this.flowLayoutPanel7.Controls.Add(this.chkExclEnergie);
            this.flowLayoutPanel7.Controls.Add(this.Label14);
            this.flowLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel7.Location = new System.Drawing.Point(422, 243);
            this.flowLayoutPanel7.Name = "flowLayoutPanel7";
            this.flowLayoutPanel7.Size = new System.Drawing.Size(507, 24);
            this.flowLayoutPanel7.TabIndex = 388;
            // 
            // cmdFindEnergie
            // 
            this.cmdFindEnergie.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFindEnergie.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdFindEnergie.FlatAppearance.BorderSize = 0;
            this.cmdFindEnergie.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFindEnergie.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdFindEnergie.Image = ((System.Drawing.Image)(resources.GetObject("cmdFindEnergie.Image")));
            this.cmdFindEnergie.Location = new System.Drawing.Point(3, 3);
            this.cmdFindEnergie.Name = "cmdFindEnergie";
            this.cmdFindEnergie.Size = new System.Drawing.Size(25, 20);
            this.cmdFindEnergie.TabIndex = 367;
            this.cmdFindEnergie.Tag = "";
            this.toolTip1.SetToolTip(this.cmdFindEnergie, "Recherche de type d\'opération");
            this.cmdFindEnergie.UseVisualStyleBackColor = false;
            this.cmdFindEnergie.Click += new System.EventHandler(this.cmdFindEnergie_Click);
            // 
            // chkExclEnergie
            // 
            this.chkExclEnergie.AutoSize = true;
            this.chkExclEnergie.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExclEnergie.Location = new System.Drawing.Point(34, 3);
            this.chkExclEnergie.Name = "chkExclEnergie";
            this.chkExclEnergie.Size = new System.Drawing.Size(81, 23);
            this.chkExclEnergie.TabIndex = 570;
            this.chkExclEnergie.Text = "Exclure";
            this.chkExclEnergie.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkExclEnergie.UseVisualStyleBackColor = true;
            // 
            // Label14
            // 
            this.Label14.AutoSize = true;
            this.Label14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label14.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label14.Location = new System.Drawing.Point(121, 0);
            this.Label14.Name = "Label14";
            this.Label14.Size = new System.Drawing.Size(111, 29);
            this.Label14.TabIndex = 732;
            this.Label14.Text = "Commentaires";
            this.Label14.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Label14.Visible = false;
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label15.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(935, 0);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(275, 30);
            this.label15.TabIndex = 389;
            this.label15.Text = "Activite";
            this.label15.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label15.Visible = false;
            // 
            // ssComboActivite
            // 
            appearance106.BackColor = System.Drawing.SystemColors.Window;
            appearance106.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssComboActivite.DisplayLayout.Appearance = appearance106;
            this.ssComboActivite.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssComboActivite.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance107.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance107.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance107.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance107.BorderColor = System.Drawing.SystemColors.Window;
            this.ssComboActivite.DisplayLayout.GroupByBox.Appearance = appearance107;
            appearance108.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssComboActivite.DisplayLayout.GroupByBox.BandLabelAppearance = appearance108;
            this.ssComboActivite.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance109.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance109.BackColor2 = System.Drawing.SystemColors.Control;
            appearance109.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance109.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssComboActivite.DisplayLayout.GroupByBox.PromptAppearance = appearance109;
            this.ssComboActivite.DisplayLayout.MaxColScrollRegions = 1;
            this.ssComboActivite.DisplayLayout.MaxRowScrollRegions = 1;
            appearance110.BackColor = System.Drawing.SystemColors.Window;
            appearance110.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssComboActivite.DisplayLayout.Override.ActiveCellAppearance = appearance110;
            appearance111.BackColor = System.Drawing.SystemColors.Highlight;
            appearance111.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssComboActivite.DisplayLayout.Override.ActiveRowAppearance = appearance111;
            this.ssComboActivite.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssComboActivite.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance112.BackColor = System.Drawing.SystemColors.Window;
            this.ssComboActivite.DisplayLayout.Override.CardAreaAppearance = appearance112;
            appearance113.BorderColor = System.Drawing.Color.Silver;
            appearance113.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssComboActivite.DisplayLayout.Override.CellAppearance = appearance113;
            this.ssComboActivite.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssComboActivite.DisplayLayout.Override.CellPadding = 0;
            appearance114.BackColor = System.Drawing.SystemColors.Control;
            appearance114.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance114.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance114.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance114.BorderColor = System.Drawing.SystemColors.Window;
            this.ssComboActivite.DisplayLayout.Override.GroupByRowAppearance = appearance114;
            appearance115.TextHAlignAsString = "Left";
            this.ssComboActivite.DisplayLayout.Override.HeaderAppearance = appearance115;
            this.ssComboActivite.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssComboActivite.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance116.BackColor = System.Drawing.SystemColors.Window;
            appearance116.BorderColor = System.Drawing.Color.Silver;
            this.ssComboActivite.DisplayLayout.Override.RowAppearance = appearance116;
            this.ssComboActivite.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance117.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssComboActivite.DisplayLayout.Override.TemplateAddRowAppearance = appearance117;
            this.ssComboActivite.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssComboActivite.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssComboActivite.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ssComboActivite.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.ssComboActivite.Location = new System.Drawing.Point(1215, 2);
            this.ssComboActivite.Margin = new System.Windows.Forms.Padding(2);
            this.ssComboActivite.Name = "ssComboActivite";
            this.ssComboActivite.Size = new System.Drawing.Size(55, 27);
            this.ssComboActivite.TabIndex = 685;
            this.ssComboActivite.Visible = false;
            this.ssComboActivite.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.ssComboActivite_BeforeDropDown);
            // 
            // label16
            // 
            this.label16.AutoSize = true;
            this.label16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label16.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label16.Location = new System.Drawing.Point(935, 30);
            this.label16.Name = "label16";
            this.label16.Size = new System.Drawing.Size(275, 30);
            this.label16.TabIndex = 391;
            this.label16.Text = "Sélection gérant";
            this.label16.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label17
            // 
            this.label17.AutoSize = true;
            this.label17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label17.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label17.Location = new System.Drawing.Point(935, 60);
            this.label17.Name = "label17";
            this.label17.Size = new System.Drawing.Size(275, 30);
            this.label17.TabIndex = 392;
            this.label17.Text = "Nombre d\'interventions sélectionnées";
            this.label17.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label23.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(935, 240);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(275, 30);
            this.label23.TabIndex = 398;
            this.label23.Text = "Resp. Exploit.Inter";
            this.label23.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtBordereauJournee
            // 
            this.txtBordereauJournee.AccAcceptNumbersOnly = false;
            this.txtBordereauJournee.AccAllowComma = false;
            this.txtBordereauJournee.AccBackgroundColor = System.Drawing.Color.White;
            this.txtBordereauJournee.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtBordereauJournee.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtBordereauJournee.AccHidenValue = "";
            this.txtBordereauJournee.AccNotAllowedChars = null;
            this.txtBordereauJournee.AccReadOnly = false;
            this.txtBordereauJournee.AccReadOnlyAllowDelete = false;
            this.txtBordereauJournee.AccRequired = false;
            this.txtBordereauJournee.BackColor = System.Drawing.Color.White;
            this.txtBordereauJournee.CustomBackColor = System.Drawing.Color.White;
            this.txtBordereauJournee.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtBordereauJournee.ForeColor = System.Drawing.Color.Black;
            this.txtBordereauJournee.Location = new System.Drawing.Point(1474, 2);
            this.txtBordereauJournee.Margin = new System.Windows.Forms.Padding(2);
            this.txtBordereauJournee.MaxLength = 32767;
            this.txtBordereauJournee.Multiline = false;
            this.txtBordereauJournee.Name = "txtBordereauJournee";
            this.txtBordereauJournee.ReadOnly = false;
            this.txtBordereauJournee.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtBordereauJournee.Size = new System.Drawing.Size(21, 27);
            this.txtBordereauJournee.TabIndex = 399;
            this.txtBordereauJournee.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtBordereauJournee.UseSystemPasswordChar = false;
            this.txtBordereauJournee.Visible = false;
            // 
            // cmbCodeGerant
            // 
            appearance118.BackColor = System.Drawing.SystemColors.Window;
            appearance118.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbCodeGerant.DisplayLayout.Appearance = appearance118;
            this.cmbCodeGerant.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbCodeGerant.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance119.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance119.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance119.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance119.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCodeGerant.DisplayLayout.GroupByBox.Appearance = appearance119;
            appearance120.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCodeGerant.DisplayLayout.GroupByBox.BandLabelAppearance = appearance120;
            this.cmbCodeGerant.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance121.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance121.BackColor2 = System.Drawing.SystemColors.Control;
            appearance121.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance121.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCodeGerant.DisplayLayout.GroupByBox.PromptAppearance = appearance121;
            this.cmbCodeGerant.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbCodeGerant.DisplayLayout.MaxRowScrollRegions = 1;
            appearance122.BackColor = System.Drawing.SystemColors.Window;
            appearance122.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbCodeGerant.DisplayLayout.Override.ActiveCellAppearance = appearance122;
            appearance123.BackColor = System.Drawing.SystemColors.Highlight;
            appearance123.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbCodeGerant.DisplayLayout.Override.ActiveRowAppearance = appearance123;
            this.cmbCodeGerant.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbCodeGerant.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance124.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCodeGerant.DisplayLayout.Override.CardAreaAppearance = appearance124;
            appearance125.BorderColor = System.Drawing.Color.Silver;
            appearance125.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbCodeGerant.DisplayLayout.Override.CellAppearance = appearance125;
            this.cmbCodeGerant.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbCodeGerant.DisplayLayout.Override.CellPadding = 0;
            appearance126.BackColor = System.Drawing.SystemColors.Control;
            appearance126.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance126.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance126.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance126.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCodeGerant.DisplayLayout.Override.GroupByRowAppearance = appearance126;
            appearance127.TextHAlignAsString = "Left";
            this.cmbCodeGerant.DisplayLayout.Override.HeaderAppearance = appearance127;
            this.cmbCodeGerant.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbCodeGerant.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance128.BackColor = System.Drawing.SystemColors.Window;
            appearance128.BorderColor = System.Drawing.Color.Silver;
            this.cmbCodeGerant.DisplayLayout.Override.RowAppearance = appearance128;
            this.cmbCodeGerant.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance129.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbCodeGerant.DisplayLayout.Override.TemplateAddRowAppearance = appearance129;
            this.cmbCodeGerant.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbCodeGerant.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbCodeGerant.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbCodeGerant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCodeGerant.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmbCodeGerant.Location = new System.Drawing.Point(1215, 32);
            this.cmbCodeGerant.Margin = new System.Windows.Forms.Padding(2);
            this.cmbCodeGerant.Name = "cmbCodeGerant";
            this.cmbCodeGerant.Size = new System.Drawing.Size(255, 27);
            this.cmbCodeGerant.TabIndex = 686;
            this.cmbCodeGerant.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbCodeGerant_BeforeDropDown);
            this.cmbCodeGerant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbCodeGerant_KeyPress);
            // 
            // txtTotal
            // 
            this.txtTotal.AccAcceptNumbersOnly = false;
            this.txtTotal.AccAllowComma = false;
            this.txtTotal.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTotal.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTotal.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTotal.AccHidenValue = "";
            this.txtTotal.AccNotAllowedChars = null;
            this.txtTotal.AccReadOnly = false;
            this.txtTotal.AccReadOnlyAllowDelete = false;
            this.txtTotal.AccRequired = false;
            this.txtTotal.BackColor = System.Drawing.Color.White;
            this.txtTotal.CustomBackColor = System.Drawing.Color.White;
            this.txtTotal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTotal.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtTotal.ForeColor = System.Drawing.Color.Black;
            this.txtTotal.Location = new System.Drawing.Point(1215, 62);
            this.txtTotal.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotal.MaxLength = 32767;
            this.txtTotal.Multiline = false;
            this.txtTotal.Name = "txtTotal";
            this.txtTotal.ReadOnly = false;
            this.txtTotal.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTotal.Size = new System.Drawing.Size(255, 27);
            this.txtTotal.TabIndex = 689;
            this.txtTotal.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTotal.UseSystemPasswordChar = false;
            // 
            // cmbEtatDevis
            // 
            appearance130.BackColor = System.Drawing.SystemColors.Window;
            appearance130.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbEtatDevis.DisplayLayout.Appearance = appearance130;
            this.cmbEtatDevis.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbEtatDevis.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance131.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance131.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance131.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance131.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbEtatDevis.DisplayLayout.GroupByBox.Appearance = appearance131;
            appearance132.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbEtatDevis.DisplayLayout.GroupByBox.BandLabelAppearance = appearance132;
            this.cmbEtatDevis.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance133.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance133.BackColor2 = System.Drawing.SystemColors.Control;
            appearance133.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance133.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbEtatDevis.DisplayLayout.GroupByBox.PromptAppearance = appearance133;
            this.cmbEtatDevis.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbEtatDevis.DisplayLayout.MaxRowScrollRegions = 1;
            appearance134.BackColor = System.Drawing.SystemColors.Window;
            appearance134.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbEtatDevis.DisplayLayout.Override.ActiveCellAppearance = appearance134;
            appearance135.BackColor = System.Drawing.SystemColors.Highlight;
            appearance135.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbEtatDevis.DisplayLayout.Override.ActiveRowAppearance = appearance135;
            this.cmbEtatDevis.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbEtatDevis.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance136.BackColor = System.Drawing.SystemColors.Window;
            this.cmbEtatDevis.DisplayLayout.Override.CardAreaAppearance = appearance136;
            appearance137.BorderColor = System.Drawing.Color.Silver;
            appearance137.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbEtatDevis.DisplayLayout.Override.CellAppearance = appearance137;
            this.cmbEtatDevis.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbEtatDevis.DisplayLayout.Override.CellPadding = 0;
            appearance138.BackColor = System.Drawing.SystemColors.Control;
            appearance138.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance138.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance138.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance138.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbEtatDevis.DisplayLayout.Override.GroupByRowAppearance = appearance138;
            appearance139.TextHAlignAsString = "Left";
            this.cmbEtatDevis.DisplayLayout.Override.HeaderAppearance = appearance139;
            this.cmbEtatDevis.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbEtatDevis.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance140.BackColor = System.Drawing.SystemColors.Window;
            appearance140.BorderColor = System.Drawing.Color.Silver;
            this.cmbEtatDevis.DisplayLayout.Override.RowAppearance = appearance140;
            this.cmbEtatDevis.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance141.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbEtatDevis.DisplayLayout.Override.TemplateAddRowAppearance = appearance141;
            this.cmbEtatDevis.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbEtatDevis.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbEtatDevis.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbEtatDevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbEtatDevis.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmbEtatDevis.Location = new System.Drawing.Point(1215, 122);
            this.cmbEtatDevis.Margin = new System.Windows.Forms.Padding(2);
            this.cmbEtatDevis.Name = "cmbEtatDevis";
            this.cmbEtatDevis.Size = new System.Drawing.Size(255, 27);
            this.cmbEtatDevis.TabIndex = 690;
            this.cmbEtatDevis.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbEtatDevis_BeforeDropDown);
            this.cmbEtatDevis.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbEtatDevis_KeyPress);
            // 
            // txtINT_AnaCode
            // 
            appearance142.BackColor = System.Drawing.SystemColors.Window;
            appearance142.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtINT_AnaCode.DisplayLayout.Appearance = appearance142;
            this.txtINT_AnaCode.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtINT_AnaCode.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance143.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance143.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance143.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance143.BorderColor = System.Drawing.SystemColors.Window;
            this.txtINT_AnaCode.DisplayLayout.GroupByBox.Appearance = appearance143;
            appearance144.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtINT_AnaCode.DisplayLayout.GroupByBox.BandLabelAppearance = appearance144;
            this.txtINT_AnaCode.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance145.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance145.BackColor2 = System.Drawing.SystemColors.Control;
            appearance145.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance145.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtINT_AnaCode.DisplayLayout.GroupByBox.PromptAppearance = appearance145;
            this.txtINT_AnaCode.DisplayLayout.MaxColScrollRegions = 1;
            this.txtINT_AnaCode.DisplayLayout.MaxRowScrollRegions = 1;
            appearance146.BackColor = System.Drawing.SystemColors.Window;
            appearance146.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtINT_AnaCode.DisplayLayout.Override.ActiveCellAppearance = appearance146;
            appearance147.BackColor = System.Drawing.SystemColors.Highlight;
            appearance147.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txtINT_AnaCode.DisplayLayout.Override.ActiveRowAppearance = appearance147;
            this.txtINT_AnaCode.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtINT_AnaCode.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance148.BackColor = System.Drawing.SystemColors.Window;
            this.txtINT_AnaCode.DisplayLayout.Override.CardAreaAppearance = appearance148;
            appearance149.BorderColor = System.Drawing.Color.Silver;
            appearance149.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtINT_AnaCode.DisplayLayout.Override.CellAppearance = appearance149;
            this.txtINT_AnaCode.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtINT_AnaCode.DisplayLayout.Override.CellPadding = 0;
            appearance150.BackColor = System.Drawing.SystemColors.Control;
            appearance150.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance150.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance150.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance150.BorderColor = System.Drawing.SystemColors.Window;
            this.txtINT_AnaCode.DisplayLayout.Override.GroupByRowAppearance = appearance150;
            appearance151.TextHAlignAsString = "Left";
            this.txtINT_AnaCode.DisplayLayout.Override.HeaderAppearance = appearance151;
            this.txtINT_AnaCode.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtINT_AnaCode.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance152.BackColor = System.Drawing.SystemColors.Window;
            appearance152.BorderColor = System.Drawing.Color.Silver;
            this.txtINT_AnaCode.DisplayLayout.Override.RowAppearance = appearance152;
            this.txtINT_AnaCode.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance153.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtINT_AnaCode.DisplayLayout.Override.TemplateAddRowAppearance = appearance153;
            this.txtINT_AnaCode.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtINT_AnaCode.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtINT_AnaCode.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtINT_AnaCode.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtINT_AnaCode.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtINT_AnaCode.Location = new System.Drawing.Point(1215, 152);
            this.txtINT_AnaCode.Margin = new System.Windows.Forms.Padding(2);
            this.txtINT_AnaCode.Name = "txtINT_AnaCode";
            this.txtINT_AnaCode.Size = new System.Drawing.Size(255, 27);
            this.txtINT_AnaCode.TabIndex = 691;
            this.txtINT_AnaCode.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.txtINT_AnaCode_BeforeDropDown);
            this.txtINT_AnaCode.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtINT_AnaCode_KeyPress);
            // 
            // label25
            // 
            this.label25.AutoSize = true;
            this.label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label25.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25.Location = new System.Drawing.Point(1475, 180);
            this.label25.Name = "label25";
            this.label25.Size = new System.Drawing.Size(103, 30);
            this.label25.TabIndex = 400;
            this.label25.Text = "jusqu\'au";
            this.label25.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // label22
            // 
            this.label22.AutoSize = true;
            this.label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label22.Location = new System.Drawing.Point(1475, 210);
            this.label22.Name = "label22";
            this.label22.Size = new System.Drawing.Size(103, 30);
            this.label22.TabIndex = 397;
            this.label22.Text = "No Intervention";
            this.label22.Visible = false;
            // 
            // SSOleDBCombo1
            // 
            appearance154.BackColor = System.Drawing.SystemColors.Window;
            appearance154.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBCombo1.DisplayLayout.Appearance = appearance154;
            this.SSOleDBCombo1.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBCombo1.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance155.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance155.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance155.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance155.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBCombo1.DisplayLayout.GroupByBox.Appearance = appearance155;
            appearance156.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBCombo1.DisplayLayout.GroupByBox.BandLabelAppearance = appearance156;
            this.SSOleDBCombo1.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance157.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance157.BackColor2 = System.Drawing.SystemColors.Control;
            appearance157.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance157.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBCombo1.DisplayLayout.GroupByBox.PromptAppearance = appearance157;
            this.SSOleDBCombo1.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBCombo1.DisplayLayout.MaxRowScrollRegions = 1;
            appearance158.BackColor = System.Drawing.SystemColors.Window;
            appearance158.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBCombo1.DisplayLayout.Override.ActiveCellAppearance = appearance158;
            appearance159.BackColor = System.Drawing.SystemColors.Highlight;
            appearance159.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBCombo1.DisplayLayout.Override.ActiveRowAppearance = appearance159;
            this.SSOleDBCombo1.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBCombo1.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance160.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBCombo1.DisplayLayout.Override.CardAreaAppearance = appearance160;
            appearance161.BorderColor = System.Drawing.Color.Silver;
            appearance161.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBCombo1.DisplayLayout.Override.CellAppearance = appearance161;
            this.SSOleDBCombo1.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBCombo1.DisplayLayout.Override.CellPadding = 0;
            appearance162.BackColor = System.Drawing.SystemColors.Control;
            appearance162.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance162.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance162.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance162.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBCombo1.DisplayLayout.Override.GroupByRowAppearance = appearance162;
            appearance163.TextHAlignAsString = "Left";
            this.SSOleDBCombo1.DisplayLayout.Override.HeaderAppearance = appearance163;
            this.SSOleDBCombo1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBCombo1.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance164.BackColor = System.Drawing.SystemColors.Window;
            appearance164.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBCombo1.DisplayLayout.Override.RowAppearance = appearance164;
            this.SSOleDBCombo1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance165.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBCombo1.DisplayLayout.Override.TemplateAddRowAppearance = appearance165;
            this.SSOleDBCombo1.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBCombo1.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBCombo1.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBCombo1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.SSOleDBCombo1.Location = new System.Drawing.Point(1583, 212);
            this.SSOleDBCombo1.Margin = new System.Windows.Forms.Padding(2);
            this.SSOleDBCombo1.Name = "SSOleDBCombo1";
            this.SSOleDBCombo1.Size = new System.Drawing.Size(58, 27);
            this.SSOleDBCombo1.TabIndex = 695;
            this.SSOleDBCombo1.Visible = false;
            this.SSOleDBCombo1.AfterCloseUp += new System.EventHandler(this.SSOleDBCombo1_AfterCloseUp);
            this.SSOleDBCombo1.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.SSOleDBCombo1_BeforeDropDown);
            // 
            // cmbDepan
            // 
            appearance166.BackColor = System.Drawing.SystemColors.Window;
            appearance166.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbDepan.DisplayLayout.Appearance = appearance166;
            this.cmbDepan.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbDepan.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance167.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance167.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance167.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance167.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbDepan.DisplayLayout.GroupByBox.Appearance = appearance167;
            appearance168.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbDepan.DisplayLayout.GroupByBox.BandLabelAppearance = appearance168;
            this.cmbDepan.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance169.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance169.BackColor2 = System.Drawing.SystemColors.Control;
            appearance169.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance169.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbDepan.DisplayLayout.GroupByBox.PromptAppearance = appearance169;
            this.cmbDepan.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbDepan.DisplayLayout.MaxRowScrollRegions = 1;
            appearance170.BackColor = System.Drawing.SystemColors.Window;
            appearance170.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbDepan.DisplayLayout.Override.ActiveCellAppearance = appearance170;
            appearance171.BackColor = System.Drawing.SystemColors.Highlight;
            appearance171.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbDepan.DisplayLayout.Override.ActiveRowAppearance = appearance171;
            this.cmbDepan.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbDepan.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance172.BackColor = System.Drawing.SystemColors.Window;
            this.cmbDepan.DisplayLayout.Override.CardAreaAppearance = appearance172;
            appearance173.BorderColor = System.Drawing.Color.Silver;
            appearance173.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbDepan.DisplayLayout.Override.CellAppearance = appearance173;
            this.cmbDepan.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbDepan.DisplayLayout.Override.CellPadding = 0;
            appearance174.BackColor = System.Drawing.SystemColors.Control;
            appearance174.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance174.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance174.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance174.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbDepan.DisplayLayout.Override.GroupByRowAppearance = appearance174;
            appearance175.TextHAlignAsString = "Left";
            this.cmbDepan.DisplayLayout.Override.HeaderAppearance = appearance175;
            this.cmbDepan.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbDepan.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance176.BackColor = System.Drawing.SystemColors.Window;
            appearance176.BorderColor = System.Drawing.Color.Silver;
            this.cmbDepan.DisplayLayout.Override.RowAppearance = appearance176;
            this.cmbDepan.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance177.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbDepan.DisplayLayout.Override.TemplateAddRowAppearance = appearance177;
            this.cmbDepan.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbDepan.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbDepan.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbDepan.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbDepan.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmbDepan.Location = new System.Drawing.Point(1215, 242);
            this.cmbDepan.Margin = new System.Windows.Forms.Padding(2);
            this.cmbDepan.Name = "cmbDepan";
            this.cmbDepan.Size = new System.Drawing.Size(255, 27);
            this.cmbDepan.TabIndex = 696;
            this.cmbDepan.TabStop = false;
            this.cmbDepan.AfterCloseUp += new System.EventHandler(this.cmbDepan_AfterCloseUp);
            this.cmbDepan.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbDepan_BeforeDropDown);
            this.cmbDepan.TextChanged += new System.EventHandler(this.cmbDepan_TextChanged);
            this.cmbDepan.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbDepan_KeyPress);
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label21.Location = new System.Drawing.Point(935, 210);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(275, 30);
            this.label21.TabIndex = 396;
            this.label21.Text = "Code Postal";
            this.label21.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // txtCP
            // 
            this.txtCP.AccAcceptNumbersOnly = false;
            this.txtCP.AccAllowComma = false;
            this.txtCP.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCP.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCP.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCP.AccHidenValue = "";
            this.txtCP.AccNotAllowedChars = null;
            this.txtCP.AccReadOnly = false;
            this.txtCP.AccReadOnlyAllowDelete = false;
            this.txtCP.AccRequired = false;
            this.txtCP.BackColor = System.Drawing.Color.White;
            this.txtCP.CustomBackColor = System.Drawing.Color.White;
            this.txtCP.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCP.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCP.ForeColor = System.Drawing.Color.Black;
            this.txtCP.Location = new System.Drawing.Point(1215, 212);
            this.txtCP.Margin = new System.Windows.Forms.Padding(2);
            this.txtCP.MaxLength = 32767;
            this.txtCP.Multiline = false;
            this.txtCP.Name = "txtCP";
            this.txtCP.ReadOnly = false;
            this.txtCP.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCP.Size = new System.Drawing.Size(255, 27);
            this.txtCP.TabIndex = 694;
            this.txtCP.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCP.UseSystemPasswordChar = false;
            this.txtCP.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCP_KeyPress);
            this.txtCP.Validating += new System.ComponentModel.CancelEventHandler(this.txtCP_Validating);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label24.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label24.Location = new System.Drawing.Point(1475, 60);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(103, 30);
            this.label24.TabIndex = 304;
            this.label24.Text = "Code analytique";
            this.label24.Visible = false;
            // 
            // txtNoIntervention
            // 
            this.txtNoIntervention.AccAcceptNumbersOnly = false;
            this.txtNoIntervention.AccAllowComma = false;
            this.txtNoIntervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoIntervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoIntervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoIntervention.AccHidenValue = "";
            this.txtNoIntervention.AccNotAllowedChars = null;
            this.txtNoIntervention.AccReadOnly = false;
            this.txtNoIntervention.AccReadOnlyAllowDelete = false;
            this.txtNoIntervention.AccRequired = false;
            this.txtNoIntervention.BackColor = System.Drawing.Color.White;
            this.txtNoIntervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNoIntervention.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoIntervention.ForeColor = System.Drawing.Color.Black;
            this.txtNoIntervention.Location = new System.Drawing.Point(816, 62);
            this.txtNoIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoIntervention.MaxLength = 32767;
            this.txtNoIntervention.Multiline = false;
            this.txtNoIntervention.Name = "txtNoIntervention";
            this.txtNoIntervention.ReadOnly = false;
            this.txtNoIntervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoIntervention.Size = new System.Drawing.Size(23, 27);
            this.txtNoIntervention.TabIndex = 403;
            this.txtNoIntervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoIntervention.UseSystemPasswordChar = false;
            this.txtNoIntervention.Visible = false;
            // 
            // txtNoEnregistrement
            // 
            this.txtNoEnregistrement.AccAcceptNumbersOnly = false;
            this.txtNoEnregistrement.AccAllowComma = false;
            this.txtNoEnregistrement.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoEnregistrement.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoEnregistrement.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoEnregistrement.AccHidenValue = "";
            this.txtNoEnregistrement.AccNotAllowedChars = null;
            this.txtNoEnregistrement.AccReadOnly = false;
            this.txtNoEnregistrement.AccReadOnlyAllowDelete = false;
            this.txtNoEnregistrement.AccRequired = false;
            this.txtNoEnregistrement.BackColor = System.Drawing.Color.White;
            this.txtNoEnregistrement.CustomBackColor = System.Drawing.Color.White;
            this.txtNoEnregistrement.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoEnregistrement.ForeColor = System.Drawing.Color.Black;
            this.txtNoEnregistrement.Location = new System.Drawing.Point(816, 32);
            this.txtNoEnregistrement.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoEnregistrement.MaxLength = 32767;
            this.txtNoEnregistrement.Multiline = false;
            this.txtNoEnregistrement.Name = "txtNoEnregistrement";
            this.txtNoEnregistrement.ReadOnly = false;
            this.txtNoEnregistrement.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoEnregistrement.Size = new System.Drawing.Size(23, 27);
            this.txtNoEnregistrement.TabIndex = 307;
            this.txtNoEnregistrement.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoEnregistrement.UseSystemPasswordChar = false;
            this.txtNoEnregistrement.Visible = false;
            // 
            // cmdDispatch
            // 
            appearance178.BackColor = System.Drawing.SystemColors.Window;
            appearance178.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmdDispatch.DisplayLayout.Appearance = appearance178;
            this.cmdDispatch.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmdDispatch.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance179.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance179.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance179.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance179.BorderColor = System.Drawing.SystemColors.Window;
            this.cmdDispatch.DisplayLayout.GroupByBox.Appearance = appearance179;
            appearance180.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmdDispatch.DisplayLayout.GroupByBox.BandLabelAppearance = appearance180;
            this.cmdDispatch.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance181.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance181.BackColor2 = System.Drawing.SystemColors.Control;
            appearance181.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance181.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmdDispatch.DisplayLayout.GroupByBox.PromptAppearance = appearance181;
            this.cmdDispatch.DisplayLayout.MaxColScrollRegions = 1;
            this.cmdDispatch.DisplayLayout.MaxRowScrollRegions = 1;
            appearance182.BackColor = System.Drawing.SystemColors.Window;
            appearance182.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdDispatch.DisplayLayout.Override.ActiveCellAppearance = appearance182;
            appearance183.BackColor = System.Drawing.SystemColors.Highlight;
            appearance183.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmdDispatch.DisplayLayout.Override.ActiveRowAppearance = appearance183;
            this.cmdDispatch.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmdDispatch.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance184.BackColor = System.Drawing.SystemColors.Window;
            this.cmdDispatch.DisplayLayout.Override.CardAreaAppearance = appearance184;
            appearance185.BorderColor = System.Drawing.Color.Silver;
            appearance185.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmdDispatch.DisplayLayout.Override.CellAppearance = appearance185;
            this.cmdDispatch.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmdDispatch.DisplayLayout.Override.CellPadding = 0;
            appearance186.BackColor = System.Drawing.SystemColors.Control;
            appearance186.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance186.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance186.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance186.BorderColor = System.Drawing.SystemColors.Window;
            this.cmdDispatch.DisplayLayout.Override.GroupByRowAppearance = appearance186;
            appearance187.TextHAlignAsString = "Left";
            this.cmdDispatch.DisplayLayout.Override.HeaderAppearance = appearance187;
            this.cmdDispatch.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmdDispatch.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance188.BackColor = System.Drawing.SystemColors.Window;
            appearance188.BorderColor = System.Drawing.Color.Silver;
            this.cmdDispatch.DisplayLayout.Override.RowAppearance = appearance188;
            this.cmdDispatch.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance189.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmdDispatch.DisplayLayout.Override.TemplateAddRowAppearance = appearance189;
            this.cmdDispatch.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmdDispatch.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmdDispatch.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmdDispatch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdDispatch.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmdDispatch.Location = new System.Drawing.Point(162, 302);
            this.cmdDispatch.Margin = new System.Windows.Forms.Padding(2);
            this.cmdDispatch.Name = "cmdDispatch";
            this.cmdDispatch.Size = new System.Drawing.Size(255, 27);
            this.cmdDispatch.TabIndex = 201;
            this.cmdDispatch.AfterCloseUp += new System.EventHandler(this.cmdDispatch_AfterCloseUp);
            this.cmdDispatch.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmdDispatch_BeforeDropDown);
            this.cmdDispatch.TextChanged += new System.EventHandler(this.cmdDispatch_TextChanged);
            this.cmdDispatch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmdDispatch_KeyPress);
            // 
            // flowLayoutPanel12
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel12, 3);
            this.flowLayoutPanel12.Controls.Add(this.cmdRechercheDispatch);
            this.flowLayoutPanel12.Controls.Add(this.chkExclDispatch);
            this.flowLayoutPanel12.Controls.Add(this.lblLibDispatch);
            this.flowLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel12.Location = new System.Drawing.Point(422, 303);
            this.flowLayoutPanel12.Name = "flowLayoutPanel12";
            this.flowLayoutPanel12.Size = new System.Drawing.Size(507, 24);
            this.flowLayoutPanel12.TabIndex = 310;
            // 
            // cmdRechercheDispatch
            // 
            this.cmdRechercheDispatch.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheDispatch.Dock = System.Windows.Forms.DockStyle.Top;
            this.cmdRechercheDispatch.FlatAppearance.BorderSize = 0;
            this.cmdRechercheDispatch.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheDispatch.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheDispatch.Image = ((System.Drawing.Image)(resources.GetObject("cmdRechercheDispatch.Image")));
            this.cmdRechercheDispatch.Location = new System.Drawing.Point(3, 3);
            this.cmdRechercheDispatch.Name = "cmdRechercheDispatch";
            this.cmdRechercheDispatch.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheDispatch.TabIndex = 367;
            this.cmdRechercheDispatch.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechercheDispatch, "Recherche de l\'intervenant");
            this.cmdRechercheDispatch.UseVisualStyleBackColor = false;
            // 
            // chkExclDispatch
            // 
            this.chkExclDispatch.AutoSize = true;
            this.chkExclDispatch.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkExclDispatch.Location = new System.Drawing.Point(34, 3);
            this.chkExclDispatch.Name = "chkExclDispatch";
            this.chkExclDispatch.Size = new System.Drawing.Size(81, 23);
            this.chkExclDispatch.TabIndex = 570;
            this.chkExclDispatch.Text = "Exclure";
            this.chkExclDispatch.TextAlign = System.Drawing.ContentAlignment.TopLeft;
            this.chkExclDispatch.UseVisualStyleBackColor = true;
            // 
            // lblLibDispatch
            // 
            this.lblLibDispatch.AutoSize = true;
            this.lblLibDispatch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibDispatch.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLibDispatch.Location = new System.Drawing.Point(121, 0);
            this.lblLibDispatch.Name = "lblLibDispatch";
            this.lblLibDispatch.Size = new System.Drawing.Size(41, 29);
            this.lblLibDispatch.TabIndex = 728;
            this.lblLibDispatch.Text = "4545";
            this.lblLibDispatch.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // txtCommentaire
            // 
            this.txtCommentaire.AccAcceptNumbersOnly = false;
            this.txtCommentaire.AccAllowComma = false;
            this.txtCommentaire.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCommentaire.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCommentaire.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCommentaire.AccHidenValue = "";
            this.txtCommentaire.AccNotAllowedChars = null;
            this.txtCommentaire.AccReadOnly = false;
            this.txtCommentaire.AccReadOnlyAllowDelete = false;
            this.txtCommentaire.AccRequired = false;
            this.txtCommentaire.BackColor = System.Drawing.Color.White;
            this.txtCommentaire.CustomBackColor = System.Drawing.Color.White;
            this.txtCommentaire.Dock = System.Windows.Forms.DockStyle.Top;
            this.txtCommentaire.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCommentaire.ForeColor = System.Drawing.Color.Black;
            this.txtCommentaire.Location = new System.Drawing.Point(1215, 272);
            this.txtCommentaire.Margin = new System.Windows.Forms.Padding(2);
            this.txtCommentaire.MaxLength = 32767;
            this.txtCommentaire.Multiline = false;
            this.txtCommentaire.Name = "txtCommentaire";
            this.txtCommentaire.ReadOnly = false;
            this.txtCommentaire.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCommentaire.Size = new System.Drawing.Size(255, 27);
            this.txtCommentaire.TabIndex = 197;
            this.txtCommentaire.Text = "1A";
            this.txtCommentaire.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCommentaire.UseSystemPasswordChar = false;
            this.txtCommentaire.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIntercom_KeyPress);
            // 
            // lblFullScreen
            // 
            this.lblFullScreen.AutoSize = true;
            this.lblFullScreen.Location = new System.Drawing.Point(1584, 0);
            this.lblFullScreen.Name = "lblFullScreen";
            this.lblFullScreen.Size = new System.Drawing.Size(0, 13);
            this.lblFullScreen.TabIndex = 303;
            this.lblFullScreen.DoubleClick += new System.EventHandler(this.lblFullScreen_DoubleClick);
            // 
            // txtGroupe
            // 
            this.txtGroupe.AccAcceptNumbersOnly = false;
            this.txtGroupe.AccAllowComma = false;
            this.txtGroupe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGroupe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGroupe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGroupe.AccHidenValue = "";
            this.txtGroupe.AccNotAllowedChars = null;
            this.txtGroupe.AccReadOnly = false;
            this.txtGroupe.AccReadOnlyAllowDelete = false;
            this.txtGroupe.AccRequired = false;
            this.txtGroupe.BackColor = System.Drawing.Color.White;
            this.txtGroupe.CustomBackColor = System.Drawing.Color.White;
            this.txtGroupe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGroupe.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtGroupe.ForeColor = System.Drawing.Color.Black;
            this.txtGroupe.Location = new System.Drawing.Point(162, 332);
            this.txtGroupe.Margin = new System.Windows.Forms.Padding(2);
            this.txtGroupe.MaxLength = 32767;
            this.txtGroupe.Multiline = false;
            this.txtGroupe.Name = "txtGroupe";
            this.txtGroupe.ReadOnly = false;
            this.txtGroupe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGroupe.Size = new System.Drawing.Size(255, 27);
            this.txtGroupe.TabIndex = 698;
            this.txtGroupe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGroupe.UseSystemPasswordChar = false;
            this.txtGroupe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtGroupe_KeyPress);
            // 
            // cmdGroupe
            // 
            this.cmdGroupe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdGroupe.FlatAppearance.BorderSize = 0;
            this.cmdGroupe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdGroupe.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdGroupe.Image = ((System.Drawing.Image)(resources.GetObject("cmdGroupe.Image")));
            this.cmdGroupe.Location = new System.Drawing.Point(422, 333);
            this.cmdGroupe.Name = "cmdGroupe";
            this.cmdGroupe.Size = new System.Drawing.Size(25, 20);
            this.cmdGroupe.TabIndex = 699;
            this.cmdGroupe.Tag = "";
            this.toolTip1.SetToolTip(this.cmdGroupe, "Recherche de l\'intervenant");
            this.cmdGroupe.UseVisualStyleBackColor = false;
            this.cmdGroupe.Click += new System.EventHandler(this.cmdGroupe_Click);
            // 
            // flowLayoutPanel11
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel11, 5);
            this.flowLayoutPanel11.Controls.Add(this.chkEncRex);
            this.flowLayoutPanel11.Controls.Add(this.chkEncCompta);
            this.flowLayoutPanel11.Controls.Add(this.chkEncaissement);
            this.flowLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel11.Location = new System.Drawing.Point(815, 331);
            this.flowLayoutPanel11.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel11.Name = "flowLayoutPanel11";
            this.flowLayoutPanel11.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.flowLayoutPanel11.Size = new System.Drawing.Size(1028, 30);
            this.flowLayoutPanel11.TabIndex = 703;
            // 
            // chkEncRex
            // 
            this.chkEncRex.AutoSize = true;
            this.chkEncRex.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEncRex.Location = new System.Drawing.Point(871, 2);
            this.chkEncRex.Margin = new System.Windows.Forms.Padding(2);
            this.chkEncRex.Name = "chkEncRex";
            this.chkEncRex.Size = new System.Drawing.Size(155, 23);
            this.chkEncRex.TabIndex = 701;
            this.chkEncRex.Text = "Enc. validé par Rex";
            this.chkEncRex.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkEncRex.UseVisualStyleBackColor = true;
            // 
            // chkEncCompta
            // 
            this.chkEncCompta.AutoSize = true;
            this.chkEncCompta.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEncCompta.Location = new System.Drawing.Point(678, 2);
            this.chkEncCompta.Margin = new System.Windows.Forms.Padding(2);
            this.chkEncCompta.Name = "chkEncCompta";
            this.chkEncCompta.Size = new System.Drawing.Size(189, 23);
            this.chkEncCompta.TabIndex = 702;
            this.chkEncCompta.Text = "Enc. validé par Compta.";
            this.chkEncCompta.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkEncCompta.UseVisualStyleBackColor = true;
            // 
            // chkEncaissement
            // 
            this.chkEncaissement.AutoSize = true;
            this.chkEncaissement.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkEncaissement.Location = new System.Drawing.Point(548, 3);
            this.chkEncaissement.Name = "chkEncaissement";
            this.chkEncaissement.Size = new System.Drawing.Size(125, 23);
            this.chkEncaissement.TabIndex = 700;
            this.chkEncaissement.Text = "Encaissement";
            this.chkEncaissement.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkEncaissement.UseVisualStyleBackColor = true;
            // 
            // txtIntercom
            // 
            this.txtIntercom.AccAcceptNumbersOnly = false;
            this.txtIntercom.AccAllowComma = false;
            this.txtIntercom.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntercom.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntercom.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntercom.AccHidenValue = "";
            this.txtIntercom.AccNotAllowedChars = null;
            this.txtIntercom.AccReadOnly = false;
            this.txtIntercom.AccReadOnlyAllowDelete = false;
            this.txtIntercom.AccRequired = false;
            this.txtIntercom.BackColor = System.Drawing.Color.White;
            this.txtIntercom.CustomBackColor = System.Drawing.Color.White;
            this.txtIntercom.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtIntercom.ForeColor = System.Drawing.Color.Black;
            this.txtIntercom.Location = new System.Drawing.Point(557, 332);
            this.txtIntercom.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntercom.MaxLength = 32767;
            this.txtIntercom.Multiline = false;
            this.txtIntercom.Name = "txtIntercom";
            this.txtIntercom.ReadOnly = false;
            this.txtIntercom.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntercom.Size = new System.Drawing.Size(57, 27);
            this.txtIntercom.TabIndex = 311;
            this.txtIntercom.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntercom.UseSystemPasswordChar = false;
            this.txtIntercom.Visible = false;
            // 
            // flowLayoutPanel8
            // 
            this.tableLayoutPanel2.SetColumnSpan(this.flowLayoutPanel8, 2);
            this.flowLayoutPanel8.Controls.Add(this.CHKP1);
            this.flowLayoutPanel8.Controls.Add(this.CHKP2);
            this.flowLayoutPanel8.Controls.Add(this.CHKP3);
            this.flowLayoutPanel8.Controls.Add(this.CHKP4);
            this.flowLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel8.Location = new System.Drawing.Point(1472, 300);
            this.flowLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel8.Name = "flowLayoutPanel8";
            this.flowLayoutPanel8.RightToLeft = System.Windows.Forms.RightToLeft.No;
            this.flowLayoutPanel8.Size = new System.Drawing.Size(372, 30);
            this.flowLayoutPanel8.TabIndex = 401;
            // 
            // CHKP1
            // 
            this.CHKP1.AutoSize = true;
            this.CHKP1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHKP1.Location = new System.Drawing.Point(2, 2);
            this.CHKP1.Margin = new System.Windows.Forms.Padding(2);
            this.CHKP1.Name = "CHKP1";
            this.CHKP1.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CHKP1.Size = new System.Drawing.Size(45, 23);
            this.CHKP1.TabIndex = 570;
            this.CHKP1.Text = "P1";
            this.CHKP1.UseVisualStyleBackColor = true;
            // 
            // CHKP2
            // 
            this.CHKP2.AutoSize = true;
            this.CHKP2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHKP2.Location = new System.Drawing.Point(52, 3);
            this.CHKP2.Name = "CHKP2";
            this.CHKP2.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CHKP2.Size = new System.Drawing.Size(45, 23);
            this.CHKP2.TabIndex = 571;
            this.CHKP2.Text = "P2";
            this.CHKP2.UseVisualStyleBackColor = true;
            // 
            // CHKP3
            // 
            this.CHKP3.AutoSize = true;
            this.CHKP3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHKP3.Location = new System.Drawing.Point(103, 3);
            this.CHKP3.Name = "CHKP3";
            this.CHKP3.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CHKP3.Size = new System.Drawing.Size(45, 23);
            this.CHKP3.TabIndex = 572;
            this.CHKP3.Text = "P3";
            this.CHKP3.UseVisualStyleBackColor = true;
            // 
            // CHKP4
            // 
            this.CHKP4.AutoSize = true;
            this.CHKP4.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CHKP4.Location = new System.Drawing.Point(154, 3);
            this.CHKP4.Name = "CHKP4";
            this.CHKP4.RightToLeft = System.Windows.Forms.RightToLeft.Yes;
            this.CHKP4.Size = new System.Drawing.Size(45, 23);
            this.CHKP4.TabIndex = 573;
            this.CHKP4.Text = "P4";
            this.CHKP4.UseVisualStyleBackColor = true;
            // 
            // chkDevisPDA
            // 
            this.chkDevisPDA.AutoSize = true;
            this.chkDevisPDA.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDevisPDA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkDevisPDA.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDevisPDA.Location = new System.Drawing.Point(1215, 302);
            this.chkDevisPDA.Margin = new System.Windows.Forms.Padding(2);
            this.chkDevisPDA.Name = "chkDevisPDA";
            this.chkDevisPDA.Size = new System.Drawing.Size(255, 26);
            this.chkDevisPDA.TabIndex = 200;
            this.chkDevisPDA.Text = "Devis effectué sur PDA";
            this.chkDevisPDA.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDevisPDA.UseVisualStyleBackColor = true;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label20.Location = new System.Drawing.Point(935, 180);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(275, 30);
            this.label20.TabIndex = 395;
            this.label20.Text = "Prévue depuis le";
            this.label20.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label19
            // 
            this.label19.AutoSize = true;
            this.label19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label19.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label19.Location = new System.Drawing.Point(935, 150);
            this.label19.Name = "label19";
            this.label19.Size = new System.Drawing.Size(275, 30);
            this.label19.TabIndex = 394;
            this.label19.Text = "Resp. Exploit.Immeuble";
            this.label19.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label18.Location = new System.Drawing.Point(935, 120);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(275, 30);
            this.label18.TabIndex = 393;
            this.label18.Text = "Code état Devis";
            this.label18.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel9.Controls.Add(this.ultraDateTimeEditor5, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.txtDatePrevue1, 0, 0);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(1213, 180);
            this.tableLayoutPanel9.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 1;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(259, 30);
            this.tableLayoutPanel9.TabIndex = 714;
            // 
            // ultraDateTimeEditor5
            // 
            appearance190.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance190.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance190.FontData.Name = "Ubuntu";
            appearance190.FontData.SizeInPoints = 9F;
            this.ultraDateTimeEditor5.Appearance = appearance190;
            this.ultraDateTimeEditor5.AutoSize = false;
            this.ultraDateTimeEditor5.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraDateTimeEditor5.BorderStyleCalendar = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraDateTimeEditor5.BorderStyleDay = Infragistics.Win.UIElementBorderStyle.Rounded4;
            appearance191.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance191.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance191.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance191.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance191.ForeColor = System.Drawing.Color.White;
            appearance191.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraDateTimeEditor5.DateButtonAppearance = appearance191;
            appearance192.BackColor = System.Drawing.Color.LightGray;
            appearance192.ForeColor = System.Drawing.Color.Black;
            this.ultraDateTimeEditor5.DateButtonAreaAppearance = appearance192;
            this.ultraDateTimeEditor5.DateButtons.Add(dateButton4);
            this.ultraDateTimeEditor5.DayOfWeekCaptionStyle = Infragistics.Win.UltraWinSchedule.DayOfWeekCaptionStyle.ShortDescription;
            this.ultraDateTimeEditor5.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance193.BackColor = System.Drawing.Color.White;
            appearance193.FontData.SizeInPoints = 11F;
            this.ultraDateTimeEditor5.DropDownAppearance = appearance193;
            this.ultraDateTimeEditor5.Location = new System.Drawing.Point(238, 2);
            this.ultraDateTimeEditor5.Margin = new System.Windows.Forms.Padding(0, 2, 2, 2);
            this.ultraDateTimeEditor5.MaximumSize = new System.Drawing.Size(19, 27);
            this.ultraDateTimeEditor5.MinimumDaySize = new System.Drawing.Size(1, 1);
            this.ultraDateTimeEditor5.MinimumSize = new System.Drawing.Size(15, 27);
            this.ultraDateTimeEditor5.MonthOrientation = Infragistics.Win.UltraWinSchedule.MonthOrientation.DownThenAcross;
            appearance194.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ultraDateTimeEditor5.MonthPopupAppearance = appearance194;
            this.ultraDateTimeEditor5.Name = "ultraDateTimeEditor5";
            this.ultraDateTimeEditor5.NonAutoSizeHeight = 27;
            appearance195.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance195.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance195.ForeColor = System.Drawing.Color.White;
            this.ultraDateTimeEditor5.ScrollButtonAppearance = appearance195;
            this.ultraDateTimeEditor5.Size = new System.Drawing.Size(19, 27);
            this.ultraDateTimeEditor5.TabIndex = 703;
            this.ultraDateTimeEditor5.Value = "";
            this.ultraDateTimeEditor5.WeekNumbersVisible = true;
            appearance196.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance196.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance196.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance196.BackHatchStyle = Infragistics.Win.BackHatchStyle.None;
            appearance196.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance196.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance196.ForeColor = System.Drawing.Color.White;
            appearance196.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraDateTimeEditor5.YearScrollButtonAppearance = appearance196;
            this.ultraDateTimeEditor5.YearScrollButtonsVisible = Infragistics.Win.DefaultableBoolean.True;
            this.ultraDateTimeEditor5.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.ultraDateTimeEditor5_BeforeDropDown);
            this.ultraDateTimeEditor5.AfterCloseUp += new System.EventHandler(this.ultraDateTimeEditor5_AfterCloseUp);
            this.ultraDateTimeEditor5.ValueChanged += new System.EventHandler(this.ultraDateTimeEditor5_ValueChanged);
            // 
            // txtDatePrevue1
            // 
            this.txtDatePrevue1.AccAcceptNumbersOnly = false;
            this.txtDatePrevue1.AccAllowComma = false;
            this.txtDatePrevue1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDatePrevue1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDatePrevue1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDatePrevue1.AccHidenValue = "";
            this.txtDatePrevue1.AccNotAllowedChars = null;
            this.txtDatePrevue1.AccReadOnly = false;
            this.txtDatePrevue1.AccReadOnlyAllowDelete = false;
            this.txtDatePrevue1.AccRequired = false;
            this.txtDatePrevue1.BackColor = System.Drawing.Color.White;
            this.txtDatePrevue1.CustomBackColor = System.Drawing.Color.White;
            this.txtDatePrevue1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDatePrevue1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtDatePrevue1.ForeColor = System.Drawing.Color.Black;
            this.txtDatePrevue1.Location = new System.Drawing.Point(2, 2);
            this.txtDatePrevue1.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.txtDatePrevue1.MaxLength = 32767;
            this.txtDatePrevue1.Multiline = false;
            this.txtDatePrevue1.Name = "txtDatePrevue1";
            this.txtDatePrevue1.ReadOnly = false;
            this.txtDatePrevue1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDatePrevue1.Size = new System.Drawing.Size(236, 27);
            this.txtDatePrevue1.TabIndex = 708;
            this.txtDatePrevue1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDatePrevue1.UseSystemPasswordChar = false;
            this.txtDatePrevue1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDatePrevue1_KeyPress);
            this.txtDatePrevue1.Validating += new System.ComponentModel.CancelEventHandler(this.txtDatePrevue1_Validating);
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel10.Controls.Add(this.ultraDateTimeEditor6, 1, 0);
            this.tableLayoutPanel10.Controls.Add(this.txtDatePrevue2, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(1581, 180);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(263, 30);
            this.tableLayoutPanel10.TabIndex = 715;
            // 
            // ultraDateTimeEditor6
            // 
            appearance197.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance197.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance197.FontData.Name = "Ubuntu";
            appearance197.FontData.SizeInPoints = 9F;
            this.ultraDateTimeEditor6.Appearance = appearance197;
            this.ultraDateTimeEditor6.AutoSize = false;
            this.ultraDateTimeEditor6.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraDateTimeEditor6.BorderStyleCalendar = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraDateTimeEditor6.BorderStyleDay = Infragistics.Win.UIElementBorderStyle.Rounded4;
            appearance198.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance198.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance198.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance198.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance198.ForeColor = System.Drawing.Color.White;
            appearance198.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraDateTimeEditor6.DateButtonAppearance = appearance198;
            appearance199.BackColor = System.Drawing.Color.LightGray;
            appearance199.ForeColor = System.Drawing.Color.Black;
            this.ultraDateTimeEditor6.DateButtonAreaAppearance = appearance199;
            this.ultraDateTimeEditor6.DateButtons.Add(dateButton5);
            this.ultraDateTimeEditor6.DayOfWeekCaptionStyle = Infragistics.Win.UltraWinSchedule.DayOfWeekCaptionStyle.ShortDescription;
            this.ultraDateTimeEditor6.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance200.BackColor = System.Drawing.Color.White;
            appearance200.FontData.SizeInPoints = 11F;
            this.ultraDateTimeEditor6.DropDownAppearance = appearance200;
            this.ultraDateTimeEditor6.Location = new System.Drawing.Point(243, 2);
            this.ultraDateTimeEditor6.Margin = new System.Windows.Forms.Padding(0, 2, 2, 2);
            this.ultraDateTimeEditor6.MaximumSize = new System.Drawing.Size(18, 26);
            this.ultraDateTimeEditor6.MinimumDaySize = new System.Drawing.Size(1, 1);
            this.ultraDateTimeEditor6.MinimumSize = new System.Drawing.Size(15, 26);
            this.ultraDateTimeEditor6.MonthOrientation = Infragistics.Win.UltraWinSchedule.MonthOrientation.DownThenAcross;
            appearance201.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ultraDateTimeEditor6.MonthPopupAppearance = appearance201;
            this.ultraDateTimeEditor6.Name = "ultraDateTimeEditor6";
            this.ultraDateTimeEditor6.NonAutoSizeHeight = 27;
            appearance202.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance202.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance202.ForeColor = System.Drawing.Color.White;
            this.ultraDateTimeEditor6.ScrollButtonAppearance = appearance202;
            this.ultraDateTimeEditor6.Size = new System.Drawing.Size(18, 26);
            this.ultraDateTimeEditor6.TabIndex = 703;
            this.ultraDateTimeEditor6.Value = "";
            this.ultraDateTimeEditor6.WeekNumbersVisible = true;
            appearance203.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance203.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance203.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance203.BackHatchStyle = Infragistics.Win.BackHatchStyle.None;
            appearance203.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance203.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance203.ForeColor = System.Drawing.Color.White;
            appearance203.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraDateTimeEditor6.YearScrollButtonAppearance = appearance203;
            this.ultraDateTimeEditor6.YearScrollButtonsVisible = Infragistics.Win.DefaultableBoolean.True;
            this.ultraDateTimeEditor6.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.ultraDateTimeEditor6_BeforeDropDown);
            this.ultraDateTimeEditor6.AfterCloseUp += new System.EventHandler(this.ultraDateTimeEditor6_AfterCloseUp);
            this.ultraDateTimeEditor6.ValueChanged += new System.EventHandler(this.ultraDateTimeEditor6_ValueChanged);
            // 
            // txtDatePrevue2
            // 
            this.txtDatePrevue2.AccAcceptNumbersOnly = false;
            this.txtDatePrevue2.AccAllowComma = false;
            this.txtDatePrevue2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDatePrevue2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDatePrevue2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDatePrevue2.AccHidenValue = "";
            this.txtDatePrevue2.AccNotAllowedChars = null;
            this.txtDatePrevue2.AccReadOnly = false;
            this.txtDatePrevue2.AccReadOnlyAllowDelete = false;
            this.txtDatePrevue2.AccRequired = false;
            this.txtDatePrevue2.BackColor = System.Drawing.Color.White;
            this.txtDatePrevue2.CustomBackColor = System.Drawing.Color.White;
            this.txtDatePrevue2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDatePrevue2.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtDatePrevue2.ForeColor = System.Drawing.Color.Black;
            this.txtDatePrevue2.Location = new System.Drawing.Point(2, 2);
            this.txtDatePrevue2.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.txtDatePrevue2.MaxLength = 32767;
            this.txtDatePrevue2.Multiline = false;
            this.txtDatePrevue2.Name = "txtDatePrevue2";
            this.txtDatePrevue2.ReadOnly = false;
            this.txtDatePrevue2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDatePrevue2.Size = new System.Drawing.Size(241, 27);
            this.txtDatePrevue2.TabIndex = 709;
            this.txtDatePrevue2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDatePrevue2.UseSystemPasswordChar = false;
            this.txtDatePrevue2.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDatePrevue2_KeyPress);
            this.txtDatePrevue2.Validating += new System.ComponentModel.CancelEventHandler(this.txtDatePrevue2_Validating);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(935, 270);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(275, 30);
            this.label12.TabIndex = 311;
            this.label12.Text = "Commentaires";
            this.label12.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.label12.Click += new System.EventHandler(this.label12_Click);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Controls.Add(this.ultraDateTimeEditor1, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtDateSaisieDe, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(160, 0);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 1;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(259, 30);
            this.tableLayoutPanel5.TabIndex = 710;
            // 
            // ultraDateTimeEditor1
            // 
            appearance204.BackColor = System.Drawing.Color.White;
            appearance204.BackColor2 = System.Drawing.Color.White;
            appearance204.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance204.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance204.BorderColor3DBase = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance204.FontData.Name = "Ubuntu";
            appearance204.FontData.SizeInPoints = 9F;
            this.ultraDateTimeEditor1.Appearance = appearance204;
            this.ultraDateTimeEditor1.AutoSelectionUpdate = true;
            this.ultraDateTimeEditor1.AutoSize = false;
            this.ultraDateTimeEditor1.BackColor = System.Drawing.Color.White;
            this.ultraDateTimeEditor1.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraDateTimeEditor1.BorderStyleCalendar = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ultraDateTimeEditor1.BorderStyleDay = Infragistics.Win.UIElementBorderStyle.Rounded4;
            appearance205.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance205.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance205.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance205.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance205.ForeColor = System.Drawing.Color.White;
            appearance205.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraDateTimeEditor1.DateButtonAppearance = appearance205;
            appearance206.BackColor = System.Drawing.Color.LightGray;
            appearance206.ForeColor = System.Drawing.Color.Black;
            this.ultraDateTimeEditor1.DateButtonAreaAppearance = appearance206;
            this.ultraDateTimeEditor1.DateButtons.Add(dateButton6);
            this.ultraDateTimeEditor1.DayOfWeekCaptionStyle = Infragistics.Win.UltraWinSchedule.DayOfWeekCaptionStyle.ShortDescription;
            this.ultraDateTimeEditor1.Dock = System.Windows.Forms.DockStyle.Fill;
            appearance207.BackColor = System.Drawing.Color.White;
            appearance207.FontData.SizeInPoints = 11F;
            this.ultraDateTimeEditor1.DropDownAppearance = appearance207;
            this.ultraDateTimeEditor1.Location = new System.Drawing.Point(239, 2);
            this.ultraDateTimeEditor1.Margin = new System.Windows.Forms.Padding(0, 2, 2, 2);
            this.ultraDateTimeEditor1.MaximumSize = new System.Drawing.Size(19, 27);
            this.ultraDateTimeEditor1.MinimumDaySize = new System.Drawing.Size(1, 1);
            this.ultraDateTimeEditor1.MinimumSize = new System.Drawing.Size(19, 27);
            this.ultraDateTimeEditor1.MonthOrientation = Infragistics.Win.UltraWinSchedule.MonthOrientation.DownThenAcross;
            appearance208.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.ultraDateTimeEditor1.MonthPopupAppearance = appearance208;
            this.ultraDateTimeEditor1.Name = "ultraDateTimeEditor1";
            this.ultraDateTimeEditor1.NonAutoSizeHeight = 27;
            appearance209.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance209.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance209.ForeColor = System.Drawing.Color.White;
            this.ultraDateTimeEditor1.ScrollButtonAppearance = appearance209;
            this.ultraDateTimeEditor1.Size = new System.Drawing.Size(19, 27);
            this.ultraDateTimeEditor1.TabIndex = 703;
            this.ultraDateTimeEditor1.Value = "";
            this.ultraDateTimeEditor1.WeekNumbersVisible = true;
            appearance210.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance210.BackColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            appearance210.BackGradientStyle = Infragistics.Win.GradientStyle.None;
            appearance210.BackHatchStyle = Infragistics.Win.BackHatchStyle.None;
            appearance210.BorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance210.BorderColor2 = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            appearance210.ForeColor = System.Drawing.Color.White;
            appearance210.ImageBackground = global::Axe_interDT.Properties.Resources.CutomColorButton;
            this.ultraDateTimeEditor1.YearScrollButtonAppearance = appearance210;
            this.ultraDateTimeEditor1.YearScrollButtonsVisible = Infragistics.Win.DefaultableBoolean.True;
            this.ultraDateTimeEditor1.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.ultraDateTimeEditor1_BeforeDropDown);
            this.ultraDateTimeEditor1.AfterCloseUp += new System.EventHandler(this.ultraDateTimeEditor1_AfterCloseUp);
            this.ultraDateTimeEditor1.ValueChanged += new System.EventHandler(this.ultraDateTimeEditor1_ValueChanged);
            // 
            // txtDateSaisieDe
            // 
            this.txtDateSaisieDe.AccAcceptNumbersOnly = false;
            this.txtDateSaisieDe.AccAllowComma = false;
            this.txtDateSaisieDe.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateSaisieDe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateSaisieDe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateSaisieDe.AccHidenValue = "";
            this.txtDateSaisieDe.AccNotAllowedChars = null;
            this.txtDateSaisieDe.AccReadOnly = false;
            this.txtDateSaisieDe.AccReadOnlyAllowDelete = false;
            this.txtDateSaisieDe.AccRequired = false;
            this.txtDateSaisieDe.BackColor = System.Drawing.Color.White;
            this.txtDateSaisieDe.CustomBackColor = System.Drawing.Color.White;
            this.txtDateSaisieDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateSaisieDe.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtDateSaisieDe.ForeColor = System.Drawing.Color.Black;
            this.txtDateSaisieDe.Location = new System.Drawing.Point(2, 2);
            this.txtDateSaisieDe.Margin = new System.Windows.Forms.Padding(2, 2, 0, 2);
            this.txtDateSaisieDe.MaxLength = 32767;
            this.txtDateSaisieDe.Multiline = false;
            this.txtDateSaisieDe.Name = "txtDateSaisieDe";
            this.txtDateSaisieDe.ReadOnly = false;
            this.txtDateSaisieDe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateSaisieDe.Size = new System.Drawing.Size(237, 27);
            this.txtDateSaisieDe.TabIndex = 704;
            this.txtDateSaisieDe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateSaisieDe.UseSystemPasswordChar = false;
            this.txtDateSaisieDe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtDateSaisieDe_KeyPress);
            this.txtDateSaisieDe.Validating += new System.ComponentModel.CancelEventHandler(this.txtDateSaisieDe_Validating);
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 4;
            this.tableLayoutPanel2.SetColumnSpan(this.tableLayoutPanel12, 4);
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 160F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 255F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.00249F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.99751F));
            this.tableLayoutPanel12.Controls.Add(this.chkDemandeNonTraite, 1, 0);
            this.tableLayoutPanel12.Controls.Add(this.chkVisiteEntretien, 3, 0);
            this.tableLayoutPanel12.Controls.Add(this.chkDapeau, 2, 0);
            this.tableLayoutPanel12.Controls.Add(this.chkDevisEtablir, 0, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(160, 270);
            this.tableLayoutPanel12.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(772, 30);
            this.tableLayoutPanel12.TabIndex = 716;
            // 
            // chkDemandeNonTraite
            // 
            this.chkDemandeNonTraite.AutoSize = true;
            this.chkDemandeNonTraite.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDemandeNonTraite.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDemandeNonTraite.Location = new System.Drawing.Point(162, 2);
            this.chkDemandeNonTraite.Margin = new System.Windows.Forms.Padding(2);
            this.chkDemandeNonTraite.Name = "chkDemandeNonTraite";
            this.chkDemandeNonTraite.Size = new System.Drawing.Size(242, 23);
            this.chkDemandeNonTraite.TabIndex = 381;
            this.chkDemandeNonTraite.Text = "Demande de devis non traîtées";
            this.chkDemandeNonTraite.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDemandeNonTraite.UseVisualStyleBackColor = true;
            this.chkDemandeNonTraite.CheckedChanged += new System.EventHandler(this.chkDemandeNonTraite_CheckedChanged);
            // 
            // chkVisiteEntretien
            // 
            this.chkVisiteEntretien.AutoSize = true;
            this.chkVisiteEntretien.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkVisiteEntretien.Dock = System.Windows.Forms.DockStyle.Right;
            this.chkVisiteEntretien.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkVisiteEntretien.Location = new System.Drawing.Point(636, 2);
            this.chkVisiteEntretien.Margin = new System.Windows.Forms.Padding(2);
            this.chkVisiteEntretien.Name = "chkVisiteEntretien";
            this.chkVisiteEntretien.Size = new System.Drawing.Size(134, 26);
            this.chkVisiteEntretien.TabIndex = 382;
            this.chkVisiteEntretien.Text = "Visite Entretien";
            this.chkVisiteEntretien.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkVisiteEntretien.UseVisualStyleBackColor = true;
            this.chkVisiteEntretien.Visible = false;
            // 
            // chkDapeau
            // 
            this.chkDapeau.AutoSize = true;
            this.chkDapeau.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDapeau.Dock = System.Windows.Forms.DockStyle.Left;
            this.chkDapeau.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDapeau.Location = new System.Drawing.Point(417, 2);
            this.chkDapeau.Margin = new System.Windows.Forms.Padding(2);
            this.chkDapeau.Name = "chkDapeau";
            this.chkDapeau.Size = new System.Drawing.Size(88, 26);
            this.chkDapeau.TabIndex = 381;
            this.chkDapeau.Text = "Drapeau";
            this.chkDapeau.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDapeau.UseVisualStyleBackColor = true;
            // 
            // chkDevisEtablir
            // 
            this.chkDevisEtablir.AutoSize = true;
            this.chkDevisEtablir.CheckAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDevisEtablir.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.chkDevisEtablir.Location = new System.Drawing.Point(2, 2);
            this.chkDevisEtablir.Margin = new System.Windows.Forms.Padding(2);
            this.chkDevisEtablir.Name = "chkDevisEtablir";
            this.chkDevisEtablir.Size = new System.Drawing.Size(155, 23);
            this.chkDevisEtablir.TabIndex = 380;
            this.chkDevisEtablir.Text = "Demande de devis";
            this.chkDevisEtablir.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.chkDevisEtablir.UseVisualStyleBackColor = true;
            this.chkDevisEtablir.CheckedChanged += new System.EventHandler(this.chkDevisEtablir_CheckedChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 165F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Controls.Add(this.lblLibelleContrat, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(1, 1);
            this.tableLayoutPanel3.Margin = new System.Windows.Forms.Padding(1);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 1;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 48F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1848, 48);
            this.tableLayoutPanel3.TabIndex = 1;
            this.tableLayoutPanel3.TabStop = true;
            // 
            // lblLibelleContrat
            // 
            this.lblLibelleContrat.AutoSize = true;
            this.lblLibelleContrat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibelleContrat.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLibelleContrat.Location = new System.Drawing.Point(3, 0);
            this.lblLibelleContrat.Name = "lblLibelleContrat";
            this.lblLibelleContrat.Size = new System.Drawing.Size(159, 48);
            this.lblLibelleContrat.TabIndex = 384;
            this.lblLibelleContrat.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cmdAvisDePassage);
            this.flowLayoutPanel1.Controls.Add(this.cmdClean);
            this.flowLayoutPanel1.Controls.Add(this.cmdPrestations);
            this.flowLayoutPanel1.Controls.Add(this.cmdHistorique);
            this.flowLayoutPanel1.Controls.Add(this.cmbIntervention);
            this.flowLayoutPanel1.Controls.Add(this.cmdBordereauJournee);
            this.flowLayoutPanel1.Controls.Add(this.CmdSauver);
            this.flowLayoutPanel1.Controls.Add(this.CmdEditer);
            this.flowLayoutPanel1.Controls.Add(this.cmdExportExcel);
            this.flowLayoutPanel1.Controls.Add(this.cmdExcelDetail);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1240, 3);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(605, 42);
            this.flowLayoutPanel1.TabIndex = 697;
            // 
            // cmdAvisDePassage
            // 
            this.cmdAvisDePassage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAvisDePassage.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAvisDePassage.FlatAppearance.BorderSize = 0;
            this.cmdAvisDePassage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAvisDePassage.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdAvisDePassage.ForeColor = System.Drawing.Color.White;
            this.cmdAvisDePassage.Location = new System.Drawing.Point(2, 2);
            this.cmdAvisDePassage.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAvisDePassage.Name = "cmdAvisDePassage";
            this.cmdAvisDePassage.Size = new System.Drawing.Size(53, 42);
            this.cmdAvisDePassage.TabIndex = 400;
            this.cmdAvisDePassage.Text = "A.D.P";
            this.cmdAvisDePassage.UseVisualStyleBackColor = false;
            this.cmdAvisDePassage.Visible = false;
            this.cmdAvisDePassage.Click += new System.EventHandler(this.cmdAvisDePassage_Click);
            // 
            // cmdClean
            // 
            this.cmdClean.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdClean.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdClean.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdClean.FlatAppearance.BorderSize = 0;
            this.cmdClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClean.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdClean.ForeColor = System.Drawing.Color.White;
            this.cmdClean.Image = global::Axe_interDT.Properties.Resources.Eraser_16x16;
            this.cmdClean.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdClean.Location = new System.Drawing.Point(59, 2);
            this.cmdClean.Margin = new System.Windows.Forms.Padding(2);
            this.cmdClean.Name = "cmdClean";
            this.cmdClean.Size = new System.Drawing.Size(53, 42);
            this.cmdClean.TabIndex = 401;
            this.cmdClean.Text = "   Clean";
            this.cmdClean.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdClean.UseVisualStyleBackColor = false;
            this.cmdClean.Click += new System.EventHandler(this.cmdClean_Click);
            // 
            // cmdPrestations
            // 
            this.cmdPrestations.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdPrestations.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdPrestations.FlatAppearance.BorderSize = 0;
            this.cmdPrestations.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPrestations.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdPrestations.ForeColor = System.Drawing.Color.White;
            this.cmdPrestations.Location = new System.Drawing.Point(116, 2);
            this.cmdPrestations.Margin = new System.Windows.Forms.Padding(2);
            this.cmdPrestations.Name = "cmdPrestations";
            this.cmdPrestations.Size = new System.Drawing.Size(86, 42);
            this.cmdPrestations.TabIndex = 402;
            this.cmdPrestations.Text = "Prestations";
            this.cmdPrestations.UseVisualStyleBackColor = false;
            this.cmdPrestations.Click += new System.EventHandler(this.cmdPrestations_Click);
            // 
            // cmdHistorique
            // 
            this.cmdHistorique.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdHistorique.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdHistorique.FlatAppearance.BorderSize = 0;
            this.cmdHistorique.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdHistorique.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdHistorique.Image = ((System.Drawing.Image)(resources.GetObject("cmdHistorique.Image")));
            this.cmdHistorique.Location = new System.Drawing.Point(206, 2);
            this.cmdHistorique.Margin = new System.Windows.Forms.Padding(2);
            this.cmdHistorique.Name = "cmdHistorique";
            this.cmdHistorique.Size = new System.Drawing.Size(48, 42);
            this.cmdHistorique.TabIndex = 403;
            this.cmdHistorique.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdHistorique.UseVisualStyleBackColor = false;
            this.cmdHistorique.Click += new System.EventHandler(this.cmdHistorique_Click);
            // 
            // cmbIntervention
            // 
            this.cmbIntervention.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmbIntervention.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbIntervention.FlatAppearance.BorderSize = 0;
            this.cmbIntervention.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbIntervention.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIntervention.Image = ((System.Drawing.Image)(resources.GetObject("cmbIntervention.Image")));
            this.cmbIntervention.Location = new System.Drawing.Point(258, 2);
            this.cmbIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.cmbIntervention.Name = "cmbIntervention";
            this.cmbIntervention.Size = new System.Drawing.Size(50, 42);
            this.cmbIntervention.TabIndex = 404;
            this.cmbIntervention.Tag = "";
            this.cmbIntervention.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmbIntervention, "Rechercher");
            this.cmbIntervention.UseVisualStyleBackColor = false;
            this.cmbIntervention.Click += new System.EventHandler(this.cmbIntervention_Click);
            // 
            // cmdBordereauJournee
            // 
            this.cmdBordereauJournee.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdBordereauJournee.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdBordereauJournee.FlatAppearance.BorderSize = 0;
            this.cmdBordereauJournee.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdBordereauJournee.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdBordereauJournee.ForeColor = System.Drawing.Color.White;
            this.cmdBordereauJournee.Location = new System.Drawing.Point(312, 2);
            this.cmdBordereauJournee.Margin = new System.Windows.Forms.Padding(2);
            this.cmdBordereauJournee.Name = "cmdBordereauJournee";
            this.cmdBordereauJournee.Size = new System.Drawing.Size(86, 42);
            this.cmdBordereauJournee.TabIndex = 405;
            this.cmdBordereauJournee.Text = "Bordeau de journée";
            this.cmdBordereauJournee.UseVisualStyleBackColor = false;
            this.cmdBordereauJournee.Click += new System.EventHandler(this.cmdBordereauJournee_Click);
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.ForeColor = System.Drawing.Color.White;
            this.CmdSauver.Image = ((System.Drawing.Image)(resources.GetObject("CmdSauver.Image")));
            this.CmdSauver.Location = new System.Drawing.Point(402, 2);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(50, 42);
            this.CmdSauver.TabIndex = 406;
            this.CmdSauver.Tag = "";
            this.toolTip1.SetToolTip(this.CmdSauver, "Enregistrer");
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // CmdEditer
            // 
            this.CmdEditer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdEditer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdEditer.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.CmdEditer.FlatAppearance.BorderSize = 0;
            this.CmdEditer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdEditer.Image = ((System.Drawing.Image)(resources.GetObject("CmdEditer.Image")));
            this.CmdEditer.Location = new System.Drawing.Point(456, 2);
            this.CmdEditer.Margin = new System.Windows.Forms.Padding(2);
            this.CmdEditer.Name = "CmdEditer";
            this.CmdEditer.Size = new System.Drawing.Size(50, 42);
            this.CmdEditer.TabIndex = 407;
            this.CmdEditer.Tag = "";
            this.toolTip1.SetToolTip(this.CmdEditer, "Imprimer");
            this.CmdEditer.UseVisualStyleBackColor = false;
            this.CmdEditer.Click += new System.EventHandler(this.CmdEditer_Click);
            // 
            // cmdExportExcel
            // 
            this.cmdExportExcel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdExportExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExportExcel.FlatAppearance.BorderSize = 0;
            this.cmdExportExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExportExcel.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdExportExcel.ForeColor = System.Drawing.Color.White;
            this.cmdExportExcel.Image = global::Axe_interDT.Properties.Resources.Excel_16x16;
            this.cmdExportExcel.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdExportExcel.Location = new System.Drawing.Point(510, 2);
            this.cmdExportExcel.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExportExcel.Name = "cmdExportExcel";
            this.cmdExportExcel.Size = new System.Drawing.Size(44, 42);
            this.cmdExportExcel.TabIndex = 408;
            this.cmdExportExcel.Text = " Export";
            this.cmdExportExcel.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdExportExcel.UseVisualStyleBackColor = false;
            this.cmdExportExcel.Click += new System.EventHandler(this.cmdExportExcel_Click);
            // 
            // cmdExcelDetail
            // 
            this.cmdExcelDetail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdExcelDetail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExcelDetail.FlatAppearance.BorderSize = 0;
            this.cmdExcelDetail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExcelDetail.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdExcelDetail.ForeColor = System.Drawing.Color.White;
            this.cmdExcelDetail.Image = global::Axe_interDT.Properties.Resources.Excel_16x16;
            this.cmdExcelDetail.ImageAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdExcelDetail.Location = new System.Drawing.Point(558, 2);
            this.cmdExcelDetail.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExcelDetail.Name = "cmdExcelDetail";
            this.cmdExcelDetail.Size = new System.Drawing.Size(44, 42);
            this.cmdExcelDetail.TabIndex = 409;
            this.cmdExcelDetail.Text = "Det";
            this.cmdExcelDetail.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdExcelDetail.UseVisualStyleBackColor = false;
            this.cmdExcelDetail.Click += new System.EventHandler(this.cmdExcelDetail_Click);
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 3;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 90F));
            this.tableLayoutPanel4.Controls.Add(this.groupBox6, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.lblOrderBy, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.Command2, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.Command1, 1, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(0, 418);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 2;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1850, 539);
            this.tableLayoutPanel4.TabIndex = 1;
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel4.SetColumnSpan(this.groupBox6, 3);
            this.groupBox6.Controls.Add(this.tableLayoutPanel11);
            this.groupBox6.Controls.Add(this.ssDropMatricule);
            this.groupBox6.Controls.Add(this.ssDropCodeEtat);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.groupBox6.Location = new System.Drawing.Point(3, 3);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(1844, 503);
            this.groupBox6.TabIndex = 700;
            this.groupBox6.TabStop = false;
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 1;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Controls.Add(this.ssIntervention, 0, 1);
            this.tableLayoutPanel11.Controls.Add(this.panel1, 0, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 2;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(1838, 480);
            this.tableLayoutPanel11.TabIndex = 703;
            // 
            // ssIntervention
            // 
            appearance211.BackColor = System.Drawing.SystemColors.Window;
            appearance211.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssIntervention.DisplayLayout.Appearance = appearance211;
            this.ssIntervention.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssIntervention.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance212.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance212.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance212.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance212.BorderColor = System.Drawing.SystemColors.Window;
            this.ssIntervention.DisplayLayout.GroupByBox.Appearance = appearance212;
            appearance213.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssIntervention.DisplayLayout.GroupByBox.BandLabelAppearance = appearance213;
            this.ssIntervention.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance214.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance214.BackColor2 = System.Drawing.SystemColors.Control;
            appearance214.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance214.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssIntervention.DisplayLayout.GroupByBox.PromptAppearance = appearance214;
            this.ssIntervention.DisplayLayout.MaxColScrollRegions = 1;
            this.ssIntervention.DisplayLayout.MaxRowScrollRegions = 1;
            appearance215.BackColor = System.Drawing.SystemColors.Window;
            appearance215.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssIntervention.DisplayLayout.Override.ActiveCellAppearance = appearance215;
            appearance216.BackColor = System.Drawing.SystemColors.Highlight;
            appearance216.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssIntervention.DisplayLayout.Override.ActiveRowAppearance = appearance216;
            this.ssIntervention.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssIntervention.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance217.BackColor = System.Drawing.SystemColors.Window;
            this.ssIntervention.DisplayLayout.Override.CardAreaAppearance = appearance217;
            appearance218.BorderColor = System.Drawing.Color.Silver;
            appearance218.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssIntervention.DisplayLayout.Override.CellAppearance = appearance218;
            this.ssIntervention.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssIntervention.DisplayLayout.Override.CellPadding = 0;
            appearance219.BackColor = System.Drawing.SystemColors.Control;
            appearance219.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance219.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance219.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance219.BorderColor = System.Drawing.SystemColors.Window;
            this.ssIntervention.DisplayLayout.Override.GroupByRowAppearance = appearance219;
            appearance220.TextHAlignAsString = "Left";
            this.ssIntervention.DisplayLayout.Override.HeaderAppearance = appearance220;
            this.ssIntervention.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssIntervention.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance221.BackColor = System.Drawing.SystemColors.Window;
            appearance221.BorderColor = System.Drawing.Color.Silver;
            this.ssIntervention.DisplayLayout.Override.RowAppearance = appearance221;
            this.ssIntervention.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssIntervention.DisplayLayout.Override.SelectTypeCell = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ssIntervention.DisplayLayout.Override.SelectTypeCol = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.ssIntervention.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance222.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssIntervention.DisplayLayout.Override.TemplateAddRowAppearance = appearance222;
            this.ssIntervention.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssIntervention.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssIntervention.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssIntervention.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.ssIntervention.Location = new System.Drawing.Point(3, 31);
            this.ssIntervention.Name = "ssIntervention";
            this.ssIntervention.Size = new System.Drawing.Size(1832, 446);
            this.ssIntervention.TabIndex = 700;
            this.ssIntervention.Text = "ultraGrid1";
            this.ssIntervention.AfterCellActivate += new System.EventHandler(this.ssIntervention_AfterCellActivate);
            this.ssIntervention.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssIntervention_InitializeLayout);
            this.ssIntervention.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssIntervention_InitializeRow);
            this.ssIntervention.AfterExitEditMode += new System.EventHandler(this.ssIntervention_AfterExitEditMode);
            this.ssIntervention.AfterRowActivate += new System.EventHandler(this.ssIntervention_AfterRowActivate);
            this.ssIntervention.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ssIntervention_AfterRowUpdate);
            this.ssIntervention.BeforeRowActivate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ssIntervention_BeforeRowActivate);
            this.ssIntervention.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.ssIntervention_ClickCellButton);
            this.ssIntervention.BeforeCellActivate += new Infragistics.Win.UltraWinGrid.CancelableCellEventHandler(this.ssIntervention_BeforeCellActivate);
            this.ssIntervention.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.ssIntervention_BeforeExitEditMode);
            this.ssIntervention.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.ssIntervention_DoubleClickRow);
            this.ssIntervention.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.ssIntervention_KeyPress);
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.panel4);
            this.panel1.Controls.Add(this.panel3);
            this.panel1.Controls.Add(this.panel2);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel1.Location = new System.Drawing.Point(3, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(3, 0, 3, 0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(1832, 28);
            this.panel1.TabIndex = 701;
            // 
            // panel4
            // 
            this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(111)))), ((int)(((byte)(111)))));
            this.panel4.Controls.Add(this.label29);
            this.panel4.Location = new System.Drawing.Point(88, 3);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(79, 23);
            this.panel4.TabIndex = 0;
            // 
            // label29
            // 
            this.label29.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label29.AutoSize = true;
            this.label29.BackColor = System.Drawing.Color.Transparent;
            this.label29.ForeColor = System.Drawing.Color.Black;
            this.label29.Location = new System.Drawing.Point(3, 0);
            this.label29.Name = "label29";
            this.label29.Size = new System.Drawing.Size(71, 19);
            this.label29.TabIndex = 1;
            this.label29.Text = "Statut 03";
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(224)))), ((int)(((byte)(192)))));
            this.panel3.Controls.Add(this.label28);
            this.panel3.Location = new System.Drawing.Point(3, 3);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(79, 23);
            this.panel3.TabIndex = 0;
            // 
            // label28
            // 
            this.label28.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label28.AutoSize = true;
            this.label28.BackColor = System.Drawing.Color.Transparent;
            this.label28.ForeColor = System.Drawing.Color.Black;
            this.label28.Location = new System.Drawing.Point(3, 0);
            this.label28.Name = "label28";
            this.label28.Size = new System.Drawing.Size(71, 19);
            this.label28.TabIndex = 1;
            this.label28.Text = "Statut 00";
            // 
            // panel2
            // 
            this.panel2.BackColor = System.Drawing.Color.Teal;
            this.panel2.Controls.Add(this.label26);
            this.panel2.Location = new System.Drawing.Point(173, 3);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(79, 23);
            this.panel2.TabIndex = 0;
            // 
            // label26
            // 
            this.label26.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.label26.AutoSize = true;
            this.label26.ForeColor = System.Drawing.Color.White;
            this.label26.Location = new System.Drawing.Point(3, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(71, 19);
            this.label26.TabIndex = 1;
            this.label26.Text = "Statut 04";
            // 
            // ssDropMatricule
            // 
            appearance223.BackColor = System.Drawing.SystemColors.Window;
            appearance223.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssDropMatricule.DisplayLayout.Appearance = appearance223;
            this.ssDropMatricule.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssDropMatricule.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance224.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance224.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance224.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance224.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropMatricule.DisplayLayout.GroupByBox.Appearance = appearance224;
            appearance225.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropMatricule.DisplayLayout.GroupByBox.BandLabelAppearance = appearance225;
            this.ssDropMatricule.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance226.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance226.BackColor2 = System.Drawing.SystemColors.Control;
            appearance226.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance226.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropMatricule.DisplayLayout.GroupByBox.PromptAppearance = appearance226;
            this.ssDropMatricule.DisplayLayout.LoadStyle = Infragistics.Win.UltraWinGrid.LoadStyle.LoadOnDemand;
            this.ssDropMatricule.DisplayLayout.MaxColScrollRegions = 1;
            this.ssDropMatricule.DisplayLayout.MaxRowScrollRegions = 1;
            appearance227.BackColor = System.Drawing.SystemColors.Window;
            appearance227.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssDropMatricule.DisplayLayout.Override.ActiveCellAppearance = appearance227;
            appearance228.BackColor = System.Drawing.SystemColors.Highlight;
            appearance228.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssDropMatricule.DisplayLayout.Override.ActiveRowAppearance = appearance228;
            this.ssDropMatricule.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssDropMatricule.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance229.BackColor = System.Drawing.SystemColors.Window;
            this.ssDropMatricule.DisplayLayout.Override.CardAreaAppearance = appearance229;
            appearance230.BorderColor = System.Drawing.Color.Silver;
            appearance230.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssDropMatricule.DisplayLayout.Override.CellAppearance = appearance230;
            this.ssDropMatricule.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssDropMatricule.DisplayLayout.Override.CellPadding = 0;
            appearance231.BackColor = System.Drawing.SystemColors.Control;
            appearance231.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance231.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance231.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance231.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropMatricule.DisplayLayout.Override.GroupByRowAppearance = appearance231;
            appearance232.TextHAlignAsString = "Left";
            this.ssDropMatricule.DisplayLayout.Override.HeaderAppearance = appearance232;
            this.ssDropMatricule.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssDropMatricule.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance233.BackColor = System.Drawing.SystemColors.Window;
            appearance233.BorderColor = System.Drawing.Color.Silver;
            this.ssDropMatricule.DisplayLayout.Override.RowAppearance = appearance233;
            this.ssDropMatricule.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance234.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssDropMatricule.DisplayLayout.Override.TemplateAddRowAppearance = appearance234;
            this.ssDropMatricule.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssDropMatricule.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssDropMatricule.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ssDropMatricule.Location = new System.Drawing.Point(128, 79);
            this.ssDropMatricule.Name = "ssDropMatricule";
            this.ssDropMatricule.Size = new System.Drawing.Size(186, 27);
            this.ssDropMatricule.TabIndex = 702;
            this.ssDropMatricule.Visible = false;
            // 
            // ssDropCodeEtat
            // 
            this.ssDropCodeEtat.AutoCompleteMode = Infragistics.Win.AutoCompleteMode.None;
            appearance235.BackColor = System.Drawing.SystemColors.Window;
            appearance235.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssDropCodeEtat.DisplayLayout.Appearance = appearance235;
            this.ssDropCodeEtat.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssDropCodeEtat.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance236.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance236.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance236.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance236.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropCodeEtat.DisplayLayout.GroupByBox.Appearance = appearance236;
            appearance237.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropCodeEtat.DisplayLayout.GroupByBox.BandLabelAppearance = appearance237;
            this.ssDropCodeEtat.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance238.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance238.BackColor2 = System.Drawing.SystemColors.Control;
            appearance238.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance238.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropCodeEtat.DisplayLayout.GroupByBox.PromptAppearance = appearance238;
            this.ssDropCodeEtat.DisplayLayout.LoadStyle = Infragistics.Win.UltraWinGrid.LoadStyle.LoadOnDemand;
            this.ssDropCodeEtat.DisplayLayout.MaxColScrollRegions = 1;
            this.ssDropCodeEtat.DisplayLayout.MaxRowScrollRegions = 1;
            appearance239.BackColor = System.Drawing.SystemColors.Window;
            appearance239.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssDropCodeEtat.DisplayLayout.Override.ActiveCellAppearance = appearance239;
            appearance240.BackColor = System.Drawing.SystemColors.Highlight;
            appearance240.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssDropCodeEtat.DisplayLayout.Override.ActiveRowAppearance = appearance240;
            this.ssDropCodeEtat.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssDropCodeEtat.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance241.BackColor = System.Drawing.SystemColors.Window;
            this.ssDropCodeEtat.DisplayLayout.Override.CardAreaAppearance = appearance241;
            appearance242.BorderColor = System.Drawing.Color.Silver;
            appearance242.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssDropCodeEtat.DisplayLayout.Override.CellAppearance = appearance242;
            this.ssDropCodeEtat.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssDropCodeEtat.DisplayLayout.Override.CellPadding = 0;
            appearance243.BackColor = System.Drawing.SystemColors.Control;
            appearance243.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance243.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance243.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance243.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropCodeEtat.DisplayLayout.Override.GroupByRowAppearance = appearance243;
            appearance244.TextHAlignAsString = "Left";
            this.ssDropCodeEtat.DisplayLayout.Override.HeaderAppearance = appearance244;
            this.ssDropCodeEtat.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssDropCodeEtat.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance245.BackColor = System.Drawing.SystemColors.Window;
            appearance245.BorderColor = System.Drawing.Color.Silver;
            this.ssDropCodeEtat.DisplayLayout.Override.RowAppearance = appearance245;
            this.ssDropCodeEtat.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance246.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssDropCodeEtat.DisplayLayout.Override.TemplateAddRowAppearance = appearance246;
            this.ssDropCodeEtat.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssDropCodeEtat.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssDropCodeEtat.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ssDropCodeEtat.Location = new System.Drawing.Point(412, 79);
            this.ssDropCodeEtat.Name = "ssDropCodeEtat";
            this.ssDropCodeEtat.Size = new System.Drawing.Size(186, 27);
            this.ssDropCodeEtat.TabIndex = 702;
            this.ssDropCodeEtat.Visible = false;
            // 
            // lblOrderBy
            // 
            this.lblOrderBy.AccAcceptNumbersOnly = false;
            this.lblOrderBy.AccAllowComma = false;
            this.lblOrderBy.AccBackgroundColor = System.Drawing.Color.White;
            this.lblOrderBy.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblOrderBy.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblOrderBy.AccHidenValue = "";
            this.lblOrderBy.AccNotAllowedChars = null;
            this.lblOrderBy.AccReadOnly = false;
            this.lblOrderBy.AccReadOnlyAllowDelete = false;
            this.lblOrderBy.AccRequired = false;
            this.lblOrderBy.BackColor = System.Drawing.Color.White;
            this.lblOrderBy.CustomBackColor = System.Drawing.Color.White;
            this.lblOrderBy.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOrderBy.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lblOrderBy.ForeColor = System.Drawing.Color.Black;
            this.lblOrderBy.Location = new System.Drawing.Point(2, 511);
            this.lblOrderBy.Margin = new System.Windows.Forms.Padding(2);
            this.lblOrderBy.MaxLength = 32767;
            this.lblOrderBy.Multiline = false;
            this.lblOrderBy.Name = "lblOrderBy";
            this.lblOrderBy.ReadOnly = false;
            this.lblOrderBy.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblOrderBy.Size = new System.Drawing.Size(1666, 27);
            this.lblOrderBy.TabIndex = 706;
            this.lblOrderBy.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblOrderBy.UseSystemPasswordChar = false;
            this.lblOrderBy.Visible = false;
            // 
            // Command2
            // 
            this.Command2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Command2.FlatAppearance.BorderSize = 0;
            this.Command2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command2.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.Command2.ForeColor = System.Drawing.Color.White;
            this.Command2.Location = new System.Drawing.Point(1762, 511);
            this.Command2.Margin = new System.Windows.Forms.Padding(2);
            this.Command2.Name = "Command2";
            this.Command2.Size = new System.Drawing.Size(86, 26);
            this.Command2.TabIndex = 698;
            this.Command2.Text = "Dernier";
            this.Command2.UseVisualStyleBackColor = false;
            this.Command2.Click += new System.EventHandler(this.Command2_Click);
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.Command1.ForeColor = System.Drawing.Color.White;
            this.Command1.Location = new System.Drawing.Point(1672, 511);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(86, 26);
            this.Command1.TabIndex = 699;
            this.Command1.Text = "Premier";
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // UserDispatch
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserDispatch";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Dispatching Intervention";
            this.Load += new System.EventHandler(this.UserDispatch_Load);
            this.VisibleChanged += new System.EventHandler(this.UserDispatch_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor4)).EndInit();
            this.tableLayoutPanel7.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor3)).EndInit();
            this.tableLayoutPanel6.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtINT_AnaCodeF)).EndInit();
            this.flowLayoutPanel10.ResumeLayout(false);
            this.flowLayoutPanel10.PerformLayout();
            this.flowLayoutPanel9.ResumeLayout(false);
            this.flowLayoutPanel9.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCodeimmeuble)).EndInit();
            this.flowLayoutPanel15.ResumeLayout(false);
            this.flowLayoutPanel15.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbStatus)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCommercial)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIntervenant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbArticle)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEnergie)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            this.flowLayoutPanel2.PerformLayout();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel3.PerformLayout();
            this.flowLayoutPanel4.ResumeLayout(false);
            this.flowLayoutPanel4.PerformLayout();
            this.flowLayoutPanel5.ResumeLayout(false);
            this.flowLayoutPanel5.PerformLayout();
            this.flowLayoutPanel6.ResumeLayout(false);
            this.flowLayoutPanel6.PerformLayout();
            this.flowLayoutPanel7.ResumeLayout(false);
            this.flowLayoutPanel7.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssComboActivite)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCodeGerant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbEtatDevis)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.txtINT_AnaCode)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBCombo1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbDepan)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdDispatch)).EndInit();
            this.flowLayoutPanel12.ResumeLayout(false);
            this.flowLayoutPanel12.PerformLayout();
            this.flowLayoutPanel11.ResumeLayout(false);
            this.flowLayoutPanel11.PerformLayout();
            this.flowLayoutPanel8.ResumeLayout(false);
            this.flowLayoutPanel8.PerformLayout();
            this.tableLayoutPanel9.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor5)).EndInit();
            this.tableLayoutPanel10.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor6)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraDateTimeEditor1)).EndInit();
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel12.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.tableLayoutPanel11.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ssIntervention)).EndInit();
            this.panel1.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.panel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropMatricule)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropCodeEtat)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbCodeimmeuble;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel15;
        public System.Windows.Forms.Button cmdRechercheImmeuble;
        private System.Windows.Forms.CheckBox chkExclImm;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label5;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbStatus;
        public System.Windows.Forms.Label label6;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbCommercial;
        public System.Windows.Forms.Label label7;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbIntervenant;
        public System.Windows.Forms.Label label8;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Label label10;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbArticle;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbEnergie;
        public iTalk.iTalk_TextBox_Small2 txtCodeGestionnaire;
        public System.Windows.Forms.CheckBox chkDevisEtablir;
        public System.Windows.Forms.CheckBox chkDapeau;
        public System.Windows.Forms.CheckBox chkVisiteEntretien;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        public System.Windows.Forms.CheckBox chkExclStatut;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        public System.Windows.Forms.Button cmdRechercheCommercial;
        public System.Windows.Forms.CheckBox chkExclComm;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel4;
        public System.Windows.Forms.Button cmdRechercheIntervenant;
        public System.Windows.Forms.CheckBox chkExclInterv;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel5;
        public System.Windows.Forms.Button cmdRechercheTypeOperation;
        public System.Windows.Forms.CheckBox chkExclArticle;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel6;
        public System.Windows.Forms.Button cmdFindGestionnaire;
        public System.Windows.Forms.CheckBox chkExclGestionnaire;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel7;
        public System.Windows.Forms.Button cmdFindEnergie;
        public System.Windows.Forms.Label label15;
        public Infragistics.Win.UltraWinGrid.UltraCombo ssComboActivite;
        public System.Windows.Forms.Label label16;
        public System.Windows.Forms.Label label17;
        public System.Windows.Forms.Label label18;
        public System.Windows.Forms.Label label19;
        public System.Windows.Forms.Label label20;
        public System.Windows.Forms.Label label21;
        public System.Windows.Forms.Label label22;
        public System.Windows.Forms.Label label23;
        public iTalk.iTalk_TextBox_Small2 txtBordereauJournee;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbCodeGerant;
        public iTalk.iTalk_TextBox_Small2 txtTotal;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbEtatDevis;
        public Infragistics.Win.UltraWinGrid.UltraCombo txtINT_AnaCode;
        public System.Windows.Forms.Label label25;
        public iTalk.iTalk_TextBox_Small2 txtCP;
        public Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBCombo1;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbDepan;
        public iTalk.iTalk_TextBox_Small2 txtCommentaire;
        public iTalk.iTalk_TextBox_Small2 txtNumContrat;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel10;
        public System.Windows.Forms.Button cmdGerant;
        public System.Windows.Forms.CheckBox chkExclGerant;
        public System.Windows.Forms.Label label27;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel8;
        private System.Windows.Forms.CheckBox CHKP1;
        private System.Windows.Forms.CheckBox CHKP2;
        private System.Windows.Forms.CheckBox CHKP3;
        private System.Windows.Forms.CheckBox CHKP4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel9;
        public System.Windows.Forms.Button cmdRechDepan;
        public System.Windows.Forms.CheckBox chkExclDepan;
        public System.Windows.Forms.Label lbllibDepanneur;
        public iTalk.iTalk_TextBox_Small2 txtNoIntervention;
        private System.Windows.Forms.Timer Timer1;
        public System.Windows.Forms.CheckBox chkAffaireAvecFact;
        public System.Windows.Forms.CheckBox chkAffaireSansFact;
        public System.Windows.Forms.CheckBox chkAchat;
        public Infragistics.Win.UltraWinGrid.UltraCombo txtINT_AnaCodeF;
        private System.Windows.Forms.GroupBox groupBox6;
        public Infragistics.Win.UltraWinGrid.UltraCombo ssDropCodeEtat;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssIntervention;
        public System.Windows.Forms.Label label24;
        public System.Windows.Forms.CheckBox chkExclEnergie;
        public System.Windows.Forms.Button Command1;
        public System.Windows.Forms.Button Command2;
        public iTalk.iTalk_TextBox_Small2 txtNoEnregistrement;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdClean;
        public System.Windows.Forms.Button cmdPrestations;
        public System.Windows.Forms.Button cmdHistorique;
        public System.Windows.Forms.Button cmbIntervention;
        public System.Windows.Forms.Button cmdBordereauJournee;
        public System.Windows.Forms.Button CmdSauver;
        public System.Windows.Forms.Button CmdEditer;
        public System.Windows.Forms.Button cmdExportExcel;
        public System.Windows.Forms.Label lblLibelleContrat;
        public System.Windows.Forms.Label lblLibGestionnaire;
        public System.Windows.Forms.Label lblLibIntervenant;
        public System.Windows.Forms.Label lblLibStatus;
        public System.Windows.Forms.Label lblLibCommercial;
        public System.Windows.Forms.Label lblLibTypeOperation;
        public System.Windows.Forms.Label Label14;
        public System.Windows.Forms.Button cmdAvisDePassage;
        public System.Windows.Forms.Label label11;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmdDispatch;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel12;
        public System.Windows.Forms.Button cmdRechercheDispatch;
        public System.Windows.Forms.CheckBox chkExclDispatch;
        public System.Windows.Forms.Label lblLibDispatch;
        public System.Windows.Forms.Label label12;
        public iTalk.iTalk_TextBox_Small2 txtIntercom;
        public iTalk.iTalk_TextBox_Small2 lblOrderBy;
        private System.Windows.Forms.Label lblFullScreen;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.CheckBox chkDevisPDA;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.Label label13;
        public iTalk.iTalk_TextBox_Small2 txtGroupe;
        public System.Windows.Forms.Button cmdGroupe;
        public System.Windows.Forms.CheckBox chkEncaissement;
        public System.Windows.Forms.CheckBox chkEncRex;
        public System.Windows.Forms.CheckBox chkEncCompta;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel11;
        public iTalk.iTalk_TextBox_Small2 txtDateSaisieDe;
        public iTalk.iTalk_TextBox_Small2 txtIntereAu;
        public iTalk.iTalk_TextBox_Small2 txtinterDe;
        public iTalk.iTalk_TextBox_Small2 txtDateSaisieFin;
        public iTalk.iTalk_TextBox_Small2 txtDatePrevue1;
        public iTalk.iTalk_TextBox_Small2 txtDatePrevue2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        public Infragistics.Win.UltraWinGrid.UltraCombo ssDropMatricule;
        public System.Windows.Forms.Button cmdExcelDetail;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraDateTimeEditor5;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraDateTimeEditor6;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraDateTimeEditor2;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraDateTimeEditor1;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraDateTimeEditor4;
        private Infragistics.Win.UltraWinSchedule.UltraCalendarCombo ultraDateTimeEditor3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label label28;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Label label29;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        public System.Windows.Forms.CheckBox chkDemandeNonTraite;
    }
}
