﻿using Axe_interDT.Views.DispatchIntervention.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.DispatchIntervention.Forms
{
    public partial class frmMailAvis : Form
    {
        frmSendAvisDepassage frm;

        public frmMailAvis()
        {
            InitializeComponent();
        }
        public frmMailAvis(frmSendAvisDepassage args)
        {
            InitializeComponent();
            frm = args;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMAJ_Click(object sender, EventArgs e)
        {
           
            frm.GridADP.ActiveRow.Cells["Nom Gestionnaire"].Value = txtNom.Text;
            frm.GridADP.ActiveRow.Cells["Email"].Value = txtdest.Text;
            frm.GridADP.ActiveRow.Cells["Object Mail"].Value = txtObjet.Text;
            frm.GridADP.ActiveRow.Cells["Corps Mail"].Value = txtMessage.Text;
        }

        private void Command1_Click(object sender, EventArgs e)
        {
            this.Close();
        }
    }
}
