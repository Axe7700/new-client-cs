﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.DispatchIntervention.Forms
{
    public partial class frmHistoAvisPassage : Form
    {
        public frmHistoAvisPassage()
        {
            InitializeComponent();
        }
        DataTable rsHisto = new DataTable();
        ModAdo rsHistoAdo = new ModAdo();
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadHisto()
        {
            string sSQL = "SELECT     CodeImmeuble, NoIntervention, AVPH_EnvoyeLe, AVPH_EnvoyePar, AVPH_TypeEnvoi"
                        + " From AVPH_AvisDePassageHisto "
                        + " WHERE     NoIntervention = " + General.nz(txtNoIntervention.Text, 0) + " ORDER BY AVPH_EnvoyeLe DESC";
            rsHisto = rsHistoAdo.fc_OpenRecordSet(sSQL);
            GridHisto.DataSource = rsHisto;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmHistoAvisPassage_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_LoadHisto();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridHisto_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridHisto.DisplayLayout.Bands[0].Columns["AVPH_EnvoyeLe"].Header.Caption = "Envoye Le";
            GridHisto.DisplayLayout.Bands[0].Columns["AVPH_EnvoyePar"].Header.Caption = "Envoye Par";
            GridHisto.DisplayLayout.Bands[0].Columns["AVPH_TypeEnvoi"].Header.Caption = "Type d'envoi";
            GridHisto.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Hidden = true;
            GridHisto.DisplayLayout.Bands[0].Columns["NoIntervention"].Hidden = true;
            for(int i=0;i< GridHisto.DisplayLayout.Bands[0].Columns.Count; i++)
            {
                GridHisto.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }
        }
    }
}
