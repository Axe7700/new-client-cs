﻿namespace Axe_interDT.Views.DispatchIntervention.Forms
{
    partial class frmMailAvis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            this.txtCodeimmeuble = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNom = new iTalk.iTalk_TextBox_Small2();
            this.txtObjet = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtdest = new iTalk.iTalk_TextBox_Small2();
            this.label5 = new System.Windows.Forms.Label();
            this.txtMessage = new iTalk.iTalk_RichTextBox();
            this.Command1 = new System.Windows.Forms.Button();
            this.cmdMAJ = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // txtCodeimmeuble
            // 
            this.txtCodeimmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeimmeuble.AccAllowComma = false;
            this.txtCodeimmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeimmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeimmeuble.AccHidenValue = "";
            this.txtCodeimmeuble.AccNotAllowedChars = null;
            this.txtCodeimmeuble.AccReadOnly = false;
            this.txtCodeimmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeimmeuble.AccRequired = false;
            this.txtCodeimmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeimmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeimmeuble.Location = new System.Drawing.Point(166, 28);
            this.txtCodeimmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeimmeuble.MaxLength = 32767;
            this.txtCodeimmeuble.Multiline = false;
            this.txtCodeimmeuble.Name = "txtCodeimmeuble";
            this.txtCodeimmeuble.ReadOnly = true;
            this.txtCodeimmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeimmeuble.Size = new System.Drawing.Size(258, 27);
            this.txtCodeimmeuble.TabIndex = 0;
            this.txtCodeimmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeimmeuble.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(46, 59);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(45, 19);
            this.label3.TabIndex = 515;
            this.label3.Text = "Nom ";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(43, 28);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 19);
            this.label2.TabIndex = 514;
            this.label2.Text = "Code immeuble";
            // 
            // txtNom
            // 
            this.txtNom.AccAcceptNumbersOnly = false;
            this.txtNom.AccAllowComma = false;
            this.txtNom.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNom.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNom.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNom.AccHidenValue = "";
            this.txtNom.AccNotAllowedChars = null;
            this.txtNom.AccReadOnly = false;
            this.txtNom.AccReadOnlyAllowDelete = false;
            this.txtNom.AccRequired = false;
            this.txtNom.BackColor = System.Drawing.Color.White;
            this.txtNom.CustomBackColor = System.Drawing.Color.White;
            this.txtNom.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNom.ForeColor = System.Drawing.Color.Black;
            this.txtNom.Location = new System.Drawing.Point(166, 59);
            this.txtNom.Margin = new System.Windows.Forms.Padding(2);
            this.txtNom.MaxLength = 32767;
            this.txtNom.Multiline = false;
            this.txtNom.Name = "txtNom";
            this.txtNom.ReadOnly = false;
            this.txtNom.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNom.Size = new System.Drawing.Size(258, 27);
            this.txtNom.TabIndex = 1;
            this.txtNom.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNom.UseSystemPasswordChar = false;
            // 
            // txtObjet
            // 
            this.txtObjet.AccAcceptNumbersOnly = false;
            this.txtObjet.AccAllowComma = false;
            this.txtObjet.AccBackgroundColor = System.Drawing.Color.White;
            this.txtObjet.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtObjet.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtObjet.AccHidenValue = "";
            this.txtObjet.AccNotAllowedChars = null;
            this.txtObjet.AccReadOnly = false;
            this.txtObjet.AccReadOnlyAllowDelete = false;
            this.txtObjet.AccRequired = false;
            this.txtObjet.BackColor = System.Drawing.Color.White;
            this.txtObjet.CustomBackColor = System.Drawing.Color.White;
            this.txtObjet.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtObjet.ForeColor = System.Drawing.Color.Black;
            this.txtObjet.Location = new System.Drawing.Point(166, 127);
            this.txtObjet.Margin = new System.Windows.Forms.Padding(2);
            this.txtObjet.MaxLength = 32767;
            this.txtObjet.Multiline = false;
            this.txtObjet.Name = "txtObjet";
            this.txtObjet.ReadOnly = false;
            this.txtObjet.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtObjet.Size = new System.Drawing.Size(258, 27);
            this.txtObjet.TabIndex = 3;
            this.txtObjet.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtObjet.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(43, 96);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(94, 19);
            this.label1.TabIndex = 519;
            this.label1.Text = "Destinataire";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(43, 133);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(51, 19);
            this.label4.TabIndex = 518;
            this.label4.Text = "Objet ";
            // 
            // txtdest
            // 
            this.txtdest.AccAcceptNumbersOnly = false;
            this.txtdest.AccAllowComma = false;
            this.txtdest.AccBackgroundColor = System.Drawing.Color.White;
            this.txtdest.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtdest.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtdest.AccHidenValue = "";
            this.txtdest.AccNotAllowedChars = null;
            this.txtdest.AccReadOnly = false;
            this.txtdest.AccReadOnlyAllowDelete = false;
            this.txtdest.AccRequired = false;
            this.txtdest.BackColor = System.Drawing.Color.White;
            this.txtdest.CustomBackColor = System.Drawing.Color.White;
            this.txtdest.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtdest.ForeColor = System.Drawing.Color.Black;
            this.txtdest.Location = new System.Drawing.Point(166, 96);
            this.txtdest.Margin = new System.Windows.Forms.Padding(2);
            this.txtdest.MaxLength = 32767;
            this.txtdest.Multiline = false;
            this.txtdest.Name = "txtdest";
            this.txtdest.ReadOnly = false;
            this.txtdest.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtdest.Size = new System.Drawing.Size(258, 27);
            this.txtdest.TabIndex = 2;
            this.txtdest.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtdest.UseSystemPasswordChar = false;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(43, 174);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(71, 19);
            this.label5.TabIndex = 521;
            this.label5.Text = "Message";
            // 
            // txtMessage
            // 
            this.txtMessage.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMessage.AutoWordSelection = false;
            this.txtMessage.BackColor = System.Drawing.Color.Transparent;
            this.txtMessage.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtMessage.ForeColor = System.Drawing.Color.Black;
            this.txtMessage.Location = new System.Drawing.Point(166, 174);
            this.txtMessage.Name = "txtMessage";
            this.txtMessage.ReadOnly = false;
            this.txtMessage.Size = new System.Drawing.Size(484, 270);
            this.txtMessage.TabIndex = 4;
            this.txtMessage.WordWrap = true;
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command1.Image = global::Axe_interDT.Properties.Resources.cancel_24;
            this.Command1.Location = new System.Drawing.Point(312, 449);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(60, 35);
            this.Command1.TabIndex = 524;
            this.Command1.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // cmdMAJ
            // 
            this.cmdMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJ.FlatAppearance.BorderSize = 0;
            this.cmdMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMAJ.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdMAJ.Location = new System.Drawing.Point(390, 449);
            this.cmdMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJ.Name = "cmdMAJ";
            this.cmdMAJ.Size = new System.Drawing.Size(60, 35);
            this.cmdMAJ.TabIndex = 523;
            this.cmdMAJ.UseVisualStyleBackColor = false;
            this.cmdMAJ.Click += new System.EventHandler(this.cmdMAJ_Click);
            // 
            // frmMailAvis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(672, 515);
            this.Controls.Add(this.Command1);
            this.Controls.Add(this.cmdMAJ);
            this.Controls.Add(this.txtMessage);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.txtObjet);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.txtdest);
            this.Controls.Add(this.txtCodeimmeuble);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNom);
            this.MaximumSize = new System.Drawing.Size(688, 554);
            this.MinimumSize = new System.Drawing.Size(688, 554);
            this.Name = "frmMailAvis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmMailAvis";
            this.ResumeLayout(false);
            this.PerformLayout();

        }



        public iTalk.iTalk_TextBox_Small2 txtCodeimmeuble;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtNom;
        public iTalk.iTalk_TextBox_Small2 txtObjet;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label4;
        public iTalk.iTalk_TextBox_Small2 txtdest;
        public System.Windows.Forms.Label label5;
        public iTalk.iTalk_RichTextBox txtMessage;
        public System.Windows.Forms.Button Command1;
        public System.Windows.Forms.Button cmdMAJ;
    }
}