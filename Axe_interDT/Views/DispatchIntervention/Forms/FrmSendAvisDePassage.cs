﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Infragistics.Win.UltraWinGrid;
using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Windows.Forms;

namespace Axe_interDT.Views.DispatchIntervention.Forms
{
    public partial class frmSendAvisDepassage : Form
    {
        public frmSendAvisDepassage()
        {
            InitializeComponent();
        }
        private const string cSep = "§"; //' "@^@^@^@";
        private const string cDelim = "(None)";
        DataTable rsADP = new DataTable();
        ModAdo rsADPAdo = new ModAdo();
        DataTable dt = new DataTable();

        public struct AvisPassage
        {
            public string sCodeimmeuble;
            public int lNoIntervention;
            public string sNomMail;
            public string sAdresseMail;
            public string sSujet;
            public string sCorps;
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadAvisDePassage()
        {
            string sSQL;
            string sWhere = "";
            string sAddItem;
            string[] tTab;
            string sCodeEtat;
            string sWhereStatut;
            string sCorpsMail;
            int lNoLigne;
            int i;

            try
            {
                Label11.Text = "Total: 0";
                if (txtDateRealiseDu.Text == "")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir la date réalisée du.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (txtDaterealiseAu.Text == "")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir la date réalisée du.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (!General.IsDate(txtDateRealiseDu.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date réalisée du n'est pas correct.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                if (!General.IsDate(txtDaterealiseAu.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date réalisée du n'est pas correct.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                sSQL = "SELECT     0 AS Selection, Intervention.NoIntervention, Intervention.CodeImmeuble,Gestionnaire.CodeQualif_Qua, Intervention.Article, Intervention.Designation, Intervention.CodeEtat, " +
                     " Intervention_2.AvisImprimeCpt, Intervention_2.AvisMailCpt, Intervention_2.CreeLe, Intervention_2.CreePar, Intervention_2.MajLe, " +
                     " Intervention_2.MajPar , Gestionnaire.NomGestion, Gestionnaire.Email_GES, Intervention.DateRealise " +
                     " FROM         Intervention INNER JOIN" +
                     " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble LEFT OUTER JOIN " +
                     " Gestionnaire ON Immeuble.CodeGestionnaire = Gestionnaire.CodeGestinnaire AND Immeuble.Code1 = Gestionnaire.Code1 LEFT OUTER JOIN " +
                     " Intervention_2 ON Intervention.NoIntervention = Intervention_2.NoIntervention";


                if (!string.IsNullOrEmpty(txtDateRealiseDu.Text))//tested
                {
                    if (sWhere == "")//Todo ==> il faut changer la valeur de cette vraibale 'FormatDateSQL' dans le regitsre
                                     //(au lieu "dd/mm/yy hh:mm:ss" par  "dd/mm/yy HH:mm:ss")
                    {
                        sWhere = " where Intervention.daterealise>='" + Convert.ToDateTime(txtDateRealiseDu.Text).ToString(General.FormatDateSQL) + "'";

                    }
                    else
                    {
                        sWhere = sWhere + " and Intervention.daterealise>='" + Convert.ToDateTime(txtDateRealiseDu.Text).ToString(General.FormatDateSQL) + "'";
                    }

                }
                if (!string.IsNullOrEmpty(txtDaterealiseAu.Text))//tested
                {
                    if (sWhere == "")
                    {
                        sWhere = " where Intervention.daterealise<='" + Convert.ToDateTime(txtDaterealiseAu.Text).ToString(General.FormatDateSQL) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Intervention.daterealise<='" + Convert.ToDateTime(txtDaterealiseAu.Text).ToString(General.FormatDateSQL) + "'";
                    }
                }
                if (!string.IsNullOrEmpty(txtCodeimmeuble.Text))
                {
                    if (sWhere == "")
                    {
                        sWhere = " where Intervention.codeimmeuble<='" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Intervention.codeimmeuble<='" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";
                    }
                }
                if (chkParticulier.Checked)
                {
                    if (sWhere == "")
                    {
                        sWhere = " where Gestionnaire.CodeQualif_Qua = 'part'";
                    }
                    else
                    {
                        sWhere = sWhere + " and   Gestionnaire.CodeQualif_Qua = 'part'";
                    }
                }
                if (!string.IsNullOrEmpty(txtStatut.Text))
                {
                    sCodeEtat = txtStatut.Text;
                    sCodeEtat = sCodeEtat.Trim();
                    sCodeEtat = sCodeEtat.Replace(" ", "");
                    sWhereStatut = "";
                    //'===controle si il y a plusieurs code état.
                    if (sCodeEtat.IndexOf(";") > 1)
                    {
                        tTab = sCodeEtat.Split(';');

                        for (i = 0; i < tTab.Length; i++)
                        {
                            if (sWhereStatut == "")
                            {
                                sWhereStatut = " Intervention.CodeEtat = '" + tTab[i] + "'";
                            }
                            else
                            {
                                sWhereStatut = sWhereStatut + " OR Intervention.CodeEtat = '" + tTab[i] + "'";
                            }
                        }
                        sWhereStatut = "( " + sWhereStatut + " )";
                        if (sWhere != "")
                        {
                            sWhere = " WHERE " + sWhereStatut;
                        }
                        else
                        {
                            sWhere = sWhere + " AND " + sWhereStatut;
                        }
                    }
                    else
                    {
                        if (sWhere != "")
                        {
                            sWhere = " WHERE intervention.CodeEtat = '" + txtStatut.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND intervention.CodeEtat = '" + txtStatut.Text + "'";
                        }
                    }
                }
                // 'sWhere = " WHERE (Intervention.NoIntervention = 945621) OR (Intervention.NoIntervention = 945620) OR (Intervention.NoIntervention = 945619) "

                rsADP = rsADPAdo.fc_OpenRecordSet(sSQL + sWhere);
                Label11.Text = "Total: " + rsADP.Rows.Count;

                dt.Clear();
                var NewRow = dt.NewRow();

                lNoLigne = 1;
                if (rsADP.Rows.Count > 0)
                {
                    foreach (DataRow r in rsADP.Rows)
                    {
                        NewRow["N°Ligne"] = lNoLigne;
                        lNoLigne++;
                        NewRow["No intervention"] = r["NoIntervention"];
                        NewRow["CodeImmeuble"] = r["CodeImmeuble"];
                        NewRow["CodeQualif"] = r["CodeQualif_QUA"];

                        if (General.IsDate(r["DateRealise"]))
                        {
                            NewRow["DateRealise"] = r["DateRealise"];
                        }
                        NewRow["Article"] = r["Article"];
                        NewRow["Designation"] = r["Designation"];
                        NewRow["Nom Gestionnaire"] = r["NomGestion"];
                        NewRow["Email"] = r["EMail_GES"];
                        //'=== object du mail.
                        NewRow["Object Mail"] = "Avis de passage le " + r["DateRealise"];

                        //'=== Corps du mail.
                        sCorpsMail = "Bonjour " + r["NomGestion"] + ",";
                        sCorpsMail = sCorpsMail + "Dans le cadre du contrat d’entretien de votre installation de chauffage," + "\n" +
                                    "nous avons le plaisir de vous informer que notre spécialiste se présentera chez vous le " + r["DateRealise"] +
                                     " Pour effectuer " + r["Designation"] + ".";


                        sCorpsMail = sCorpsMail + "\n";
                        sCorpsMail = sCorpsMail + "\n";
                        sCorpsMail = sCorpsMail + "En cas d’empêchement de votre part pour la date ci-dessus, veuillez prendre contact avec LONG CHAUFFAGE au 01 42 83 80 26 " + "\n"
                       + "ou par mail à rdv@groupe-long.fr afin de vous proposer un autre rendez-vous." + "\n";

                        sCorpsMail = sCorpsMail + "\n";
                        sCorpsMail = sCorpsMail + "Vous remerciant à l’avance de nous faciliter l’accès de votre chaufferie, nous vous prions d’agréer l’assurance de notre entier dévouement." + "\n";



                        sCorpsMail = sCorpsMail + "\n";
                        sCorpsMail = sCorpsMail + "Merci de nous faire savoir suffisamment à l’avance si vous avez un conduit ou une cheminée supplémentaire au contrat afin de pouvoir assurer une prestation complémentaire le jour même.";
                        sCorpsMail = sCorpsMail + "\n";
                        sCorpsMail = sCorpsMail + "\n";
                        sCorpsMail = sCorpsMail + "Le service dispatch" + "\n";
                        sCorpsMail = sCorpsMail + "LONG CHAUFFAGE" + "\n";

                        NewRow["Corps Mail"] = sCorpsMail;

                        NewRow["Compteur Imprime"] = r["AvisImprimeCpt"];
                        NewRow["Compteur Mail"] = r["AvisMailCpt"];
                        NewRow["Cree Le"] = r["CreeLe"];
                        NewRow["Cree Par"] = r["CreePar"];
                        NewRow["MajLe"] = r["MajLe"];
                        NewRow["MajPar"] = r["MajPar"];
                        dt.Rows.Add(NewRow.ItemArray);
                        GridADP.DataSource = dt;

                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadAvisDePassage");
            }
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervention_Click(object sender, EventArgs e)
        {
            fc_LoadAvisDePassage();
        }


        private void fc_Envoie(int lTypeEnvoie)
        {
            //'=== lTypeEnvoie = 0 => demande d'impréssion.
            //'=== lTypeEnvoie = 1 => demande d'ebnvoie par maim.
            int a;
            int i;

            SqlConnection adotrans = new SqlConnection();
            DataTable rs = new DataTable();

            string sWhere;
            string FichierCrystalName;
            string sSQL;
            string sMess = null;

            AvisPassage[] tTabAVS = null;
            bool bImprime;
            DateTime dtNow = new DateTime();
            try
            {
                if (lTypeEnvoie == 0)
                {
                    sMess = "Voulez-vous imprimer ces avis de passage ?";
                }
                else if (lTypeEnvoie == 1)
                {
                    sMess = "Voulez-vous envoyer ces avis de passage par mail ?";
                }

                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMess, "Impression", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
                adotrans = General.adocnn;
                //===> Mondir le 25.03.2021 pour ajouter la version V24.03.2021 ajout du .Replace("-","").Replace(".","")
                General.Execute("BEGIN TRANSACTION Trans1" + General.fncUserName().Replace(" ", "").Replace("-", "").Replace(".", ""));
                GridADP.UpdateData();
                sWhere = "";
                i = 0;
                if (GridADP.Rows.Count > 0)
                {
                    for (a = 0; a < GridADP.Rows.Count - 1; a++)
                    {
                        if (GridADP.Rows[a].Cells["selection"].Text == "True" /*|| GridADP.Rows[a].Cells["selection"].Text == "False"*/)
                        {
                            bImprime = true;
                            if (lTypeEnvoie == 0)
                            {
                                if (Convert.ToInt32(General.nz(GridADP.Rows[a].Cells["Compteur Imprime"].Value, 0)) >= 1)
                                {
                                    sMess = "L'avis de passade de l'intervention n°" + GridADP.Rows[a].Cells["No Intervention"].Text + ", immeuble " + GridADP.Rows[a].Cells["Codeimmeuble"].Text
                                       + " (ligne " + GridADP.Rows[a].Cells["N°Ligne"].Text + ") a déjà été imprimé " + GridADP.Rows[a].Cells["Compteur Imprime"].Text + " fois."
                                       + "Voulez-vous l'imprimer de nouveau.";
                                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMess, "Impression", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                                    {
                                        bImprime = false;
                                    }
                                }
                            }
                            if (bImprime == true)//tested
                            {
                                Array.Resize(ref tTabAVS, i + 1);

                                tTabAVS[i].sCodeimmeuble = GridADP.Rows[a].Cells["Codeimmeuble"].Text;
                                tTabAVS[i].lNoIntervention = Convert.ToInt32(GridADP.Rows[a].Cells["No Intervention"].Text);
                                tTabAVS[i].sNomMail = GridADP.Rows[a].Cells["N°Ligne"].Text;
                                tTabAVS[i].sAdresseMail = GridADP.Rows[a].Cells["Email"].Text.Trim();
                                if (tTabAVS[i].sAdresseMail == "" && lTypeEnvoie == 1)
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La ligne " + GridADP.Rows[a].Cells["N°Ligne"].Text + " ne contient pas d'adresse de messagerie, veuillez en saisir une ou désélectionner cette ligne.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    return;
                                }
                                tTabAVS[i].sSujet = GridADP.Rows[a].Cells["Object Mail"].Text;
                                tTabAVS[i].sCorps = GridADP.Rows[a].Cells["Corps Mail"].Text;


                                i = i + 1;
                                if (sWhere == "")
                                {
                                    sWhere = sWhere + "{intervention.Nointervention} =" + GridADP.Rows[a].Cells["No Intervention"].Value;
                                }
                                else
                                {
                                    sWhere = sWhere + " OR {intervention.Nointervention} =" + GridADP.Rows[a].Cells["No Intervention"].Value;
                                }

                                sSQL = "SELECT      NoIntervention From Intervention_2 WHERE NoIntervention =" + GridADP.Rows[a].Cells["No Intervention"].Text;
                                using (var tmp = new ModAdo())
                                {
                                    sSQL = tmp.fc_ADOlibelle(sSQL);
                                }

                                if (lTypeEnvoie == 0)//tested
                                {
                                    GridADP.Rows[a].Cells["Compteur Imprime"].Value = Convert.ToInt32(General.nz(GridADP.Rows[a].Cells["Compteur Imprime"].Value, 0)) + 1;
                                }
                                else if (lTypeEnvoie == 1)
                                {
                                    GridADP.Rows[a].Cells["Compteur Mail"].Value = Convert.ToInt32(General.nz(GridADP.Rows[a].Cells["Compteur Mail"].Value, 0)) + 1;
                                }

                                GridADP.UpdateData();
                                if (sSQL == "")
                                {
                                    sSQL = " INSERT INTO INtervention_2 (Nointervention, CodeImmeuble, CreeLe, CreePar, AvisImprimeCpt, AvisMailCpt)";


                                    sSQL = sSQL + " VALUES (" + GridADP.Rows[a].Cells["No Intervention"].Text + ",'" + StdSQLchaine.gFr_DoublerQuote(GridADP.Rows[a].Cells["Codeimmeuble"].Text) + "'," +
                                        "'" + DateTime.Now + "','" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "',0,0 )";
                                    General.Execute(sSQL);
                                }
                            }
                        }
                    }
                }
                if (i == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Opération annulée, vous n'avez sélectionné aucune intervention.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    adotrans.Close();
                    return;
                }
                //===> Mondir le 25.03.2021 pour ajouter la version V21.03.2021, ajout du .Replace("-","").Replace(".","")
                General.Execute("COMMIT TRANSACTION Trans1" + General.fncUserName().Replace(" ", "").Replace("-", "").Replace(".", ""));
                if (lTypeEnvoie == 0)
                {
                    FichierCrystalName = devModeCrystal.fc_ExportCrystalSansFormule(sWhere, General.gsRpt + General.sEtatAvisDePassage);
                    ModuleAPI.Ouvrir(FichierCrystalName);
                    dtNow = DateTime.Now;
                    for (i = 0; i < tTabAVS.Length; i++)//tested
                    {
                        //===> Mondir le 25.03.2021 pour ajouter la version V24.03.2021 ajout du .Replace("-","").Replace(".","")
                        General.Execute("BEGIN TRANSACTION Trans1" + General.fncUserName().Replace(" ", "").Replace("-", "").Replace(".", ""));

                        sSQL = "UPDATE    Intervention_2 Set AvisImprimeCpt = AvisImprimeCpt + 1, MajLe ='" +
                            dtNow + "', MajPar ='" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'" +
                             " WHERE NoIntervention = " + tTabAVS[i].lNoIntervention;
                        General.Execute(sSQL);

                        sSQL = "INSERT INTO AVPH_AvisDePassageHisto" +
                        " (CodeImmeuble, NoIntervention, AVPH_EnvoyeLe, AVPH_EnvoyePar, AVPH_TypeEnvoi)" +
                         " VALUES     ('" + tTabAVS[i].sCodeimmeuble + "'," + tTabAVS[i].lNoIntervention + ",'"
                        + dtNow + "', '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'" + " , 'Impression')";
                        General.Execute(sSQL);

                        //===> Mondir le 25.03.2021 pour ajouter la version V24.03.2021 ajout du .Replace("-","").Replace(".","")
                        General.Execute("COMMIT TRANSACTION Trans1" + General.fncUserName().Replace(" ", "").Replace("-", "").Replace(".", ""));

                    }
                }
                else if (lTypeEnvoie == 1)
                {
                    for (i = 0; i < tTabAVS.Length; i++)
                    {
                        sWhere = "{intervention.Nointervention} =" + tTabAVS[i].lNoIntervention;
                        FichierCrystalName = devModeCrystal.fc_ExportCrystalSansFormule(sWhere, General.gsRpt + General.sEtatAvisDePassage);
                        //ModuleAPI.Ouvrir(FichierCrystalName);

                        if (General.sRDOmail == "1")
                        {
                            ModCourrier.CreateMailRDO(tTabAVS[i].sAdresseMail, "", tTabAVS[i].sSujet, tTabAVS[i].sCorps, FichierCrystalName, '\n');
                        }
                        else
                        {
                            ModCourrier.CreateMailRDO(tTabAVS[i].sAdresseMail, "", tTabAVS[i].sSujet, tTabAVS[i].sCorps, FichierCrystalName, '\n');
                        }
                        dtNow = DateTime.Now;

                        //===> Mondir le 25.03.2021 pour ajouter la version V24.03.2021 ajout du .Replace("-","").Replace(".","")
                        General.Execute("COMMIT TRANSACTION Trans1" + General.fncUserName().Replace(" ", "").Replace("-", "").Replace(".", ""));

                        sSQL = "UPDATE    Intervention_2 Set AvisMailCpt = AvisMailCpt+ 1, MajLe ='" + dtNow + "', MajPar = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'"
                             + " WHERE NoIntervention = " + tTabAVS[i].lNoIntervention;
                        General.Execute(sSQL);

                        sSQL = "INSERT INTO AVPH_AvisDePassageHisto" +
                         " (CodeImmeuble, NoIntervention, AVPH_EnvoyeLe, AVPH_EnvoyePar, AVPH_TypeEnvoi)" +
                         " VALUES     ('" + tTabAVS[i].sCodeimmeuble + "'," + tTabAVS[i].lNoIntervention + ",'" + dtNow + "', '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'" +
                         " , 'Mail')";
                        General.Execute(sSQL);

                        //===> Mondir le 25.03.2021 pour ajouter la version V24.03.2021 ajout du .Replace("-","").Replace(".","")
                        General.Execute("COMMIT TRANSACTION Trans1" + General.fncUserName().Replace(" ", "").Replace("-", "").Replace(".", ""));
                    }

                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Envoi de mail terminé.", "Mail", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
            catch (Exception ex)
            {
                //===> Mondir le 25.03.2021 pour ajouter la version V24.03.2021 ajout du .Replace("-","").Replace(".","")
                General.Execute("ROLLBACK TRANSACTION Trans1" + General.fncUserName().Replace(" ", "").Replace("-", "").Replace(".", ""));

                Erreurs.gFr_debug(ex, this.Name + "CmdSelect_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdClean_Click(object sender, EventArgs e)
        {
            txtStatut.Text = "";
            txtCodeimmeuble.Text = "";
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDeselect_Click(object sender, EventArgs e)
        {
            int a;
            GridADP.UpdateData();
            if (GridADP.Rows.Count > 0)
            {
                for (a = 0; a < GridADP.Rows.Count; a++)
                {
                    GridADP.Rows[a].Cells["Selection"].Value = false;
                    rsADPAdo.Update();
                }
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdHisto_Click(object sender, EventArgs e)
        {

            if (GridADP.ActiveRow == null)// GridADP.ActiveRow.Cells["NoIntervention"].Text == ""
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner une ligne dans la grille.");
                return;
            }

            frmHistoAvisPassage frm = new frmHistoAvisPassage();
            frm.txtNoIntervention.Text = GridADP.ActiveRow.Cells["No Intervention"].Text;
            frm.txtCodeimmeuble.Text = GridADP.ActiveRow.Cells["CodeImmeuble"].Text;
            frm.ShowDialog();

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdImprime_Click(object sender, EventArgs e)
        {
            fc_Envoie(0);
        }

        private void cmdMail_Click(object sender, EventArgs e)
        {
            fc_Envoie(1);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            txtCodeimmeuble_KeyPress(txtCodeimmeuble, new System.Windows.Forms.KeyPressEventArgs((char)13));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSelect_Click(object sender, EventArgs e)
        {
            int a;
            GridADP.UpdateData();
            if (GridADP.Rows.Count > 0)
            {
                for (a = 0; a < GridADP.Rows.Count; a++)
                {
                    GridADP.Rows[a].Cells["Selection"].Value = true;
                    rsADPAdo.Update();
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdStatut_Click(object sender, EventArgs e)
        {
            txtStatut_KeyPress(txtStatut, new System.Windows.Forms.KeyPressEventArgs((char)13));
        }


        /// <summary>
        /// tested
        /// </summary>
        /// <param name="row"></param>
        private void fc_Color(UltraGridRow row)
        {
            int i = 0;

            if (row.Cells["Selection"].Value.ToString() == "True" /*|| row.Cells["Selection"].Value.ToString() == "False"*/)
            {
                for (i = 0; i < row.Cells.Count; i++)
                {

                    row.Cells[i].Appearance.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0ffc0);//&HC0FFC0
                    row.Cells[i].Appearance.ForeColor = Color.Black;//&H0&
                }

            }
            else//tested
            {
                for (i = 0; i < row.Cells.Count; i++)
                {
                    row.Cells[i].Appearance.BackColor = Color.White;//&HFFFFFF
                    row.Cells[i].Appearance.ForeColor = Color.Black;//&H0&
                }
            }
            if (General.UCase(row.Cells["CodeQualif"].Value) == General.UCase("Part"))
            {
                row.Cells[i].Appearance.BackColor = Color.Pink;//&HFFC0FF
                row.Cells[i].Appearance.ForeColor = Color.Black; //&H0 &
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeimmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            using (var tmpModAdo = new ModAdo())
            {
                string requete;
                string where_order;
                short KeyAscii = (short)e.KeyChar;
                if (KeyAscii == 13)
                {
                    if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Immeuble.CodeImmeuble FROM Immeuble WHERE Immeuble.CodeImmeuble='" + txtCodeimmeuble.Text + "'"), "").ToString()))
                    {
                        requete = "SELECT CodeImmeuble as \"Code Immeuble\",Adresse AS \"Adresse\"," +
                        " Ville as \"Ville\", " +
                        " anglerue as \"Angle de rue \", Code1 as \"Gerant \" FROM Immeuble ";
                        where_order = "";
                        SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un immeuble" };
                        fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeimmeuble.Text } });
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            string sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            txtCodeimmeuble.Text = sCode;
                            fg.Dispose();
                            fg.Close();
                        };

                        fg.ugResultat.KeyDown += (se, ev) =>
                                                {

                                                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                                    {
                                                        string sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                                        txtCodeimmeuble.Text = sCode;
                                                        fg.Dispose();
                                                        fg.Close();
                                                    }
                                                };
                        fg.StartPosition = FormStartPosition.CenterParent;
                        fg.ShowDialog();
                    }


                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtStatut_KeyPress(object sender, KeyPressEventArgs e)
        {
            string requete;
            string where_order;
            short KeyAscii = (short)e.KeyChar;
            if (KeyAscii == 13)
            {
                requete = "SELECT TypeCodeEtat.CodeEtat as \"Code Etat\" ,TypeCodeEtat.LibelleCodeEtat as \"Libelle\" FROM TypeCodeEtat";
                where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un statut" };

                fg.SetValues(new Dictionary<string, string> { { "CodeEtat", txtStatut.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    if (txtStatut.Text != "")
                    {
                        txtStatut.Text = txtStatut.Text + ";" + fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    }
                    else
                    {
                        txtStatut.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    }

                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                                {

                                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                    {
                                        if (txtStatut.Text != "")
                                        {
                                            txtStatut.Text = txtStatut.Text + ";" + fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                        }
                                        else
                                        {
                                            txtStatut.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                        }

                                        fg.Dispose();
                                        fg.Close();
                                    }
                                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridADP_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.Row.Cells["Selection"].Value == DBNull.Value || e.Row.Cells["Selection"].Value.ToString() == "0")
            {
                e.Row.Cells["Selection"].Value = false;
            }
            var row = e.Row;
            fc_Color(row);
            GridADP.UpdateData();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridADP_DoubleClickRow(object sender, DoubleClickRowEventArgs e)
        {
            frmMailAvis frm = new frmMailAvis(this);
            frm.txtCodeimmeuble.Text = GridADP.ActiveRow.Cells["codeimmeuble"].Text;
            frm.txtNom.Text = GridADP.ActiveRow.Cells["Nom Gestionnaire"].Text;
            frm.txtdest.Text = GridADP.ActiveRow.Cells["Email"].Text;

            frm.txtObjet.Text = GridADP.ActiveRow.Cells["Object Mail"].Text;
            frm.txtMessage.Text = GridADP.ActiveRow.Cells["Corps Mail"].Text;
            //refRow = GridADP.ActiveRow;
            frm.ShowDialog();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridADP_AfterRowActivate(object sender, EventArgs e)
        {
            var row = GridADP.ActiveRow;
            fc_Color(row);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmSendAvisDepassage_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            ToolTip tt = new ToolTip();
            tt.Show("Vous pouvez cumuler des statuts en le séparant par un ;", txtStatut);

            txtDateRealiseDu.Value = DateTime.Now;
            txtDaterealiseAu.Value = DateTime.Now;


            dt.Columns.Add("Selection");
            dt.Columns.Add("N°Ligne");
            dt.Columns.Add("No intervention");
            dt.Columns.Add("CodeImmeuble");
            dt.Columns.Add("CodeQualif");
            dt.Columns.Add("DateRealise");
            dt.Columns.Add("Article");
            dt.Columns.Add("Designation");
            dt.Columns.Add("Code etat");
            dt.Columns.Add("Nom Gestionnaire");
            dt.Columns.Add("Email");
            dt.Columns.Add("Object Mail");
            dt.Columns.Add("Corps Mail");
            dt.Columns.Add("Compteur Imprime");
            dt.Columns.Add("Compteur Mail");
            dt.Columns.Add("Cree Le");
            dt.Columns.Add("Cree Par");
            dt.Columns.Add("MajLe");
            dt.Columns.Add("MajPar");
            GridADP.DataSource = dt;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridADP_InitializeLayout(object sender, InitializeLayoutEventArgs e)
        {
            GridADP.DisplayLayout.Bands[0].Columns["Selection"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
        }
    }
}
