﻿using Axe_interDT.Views.DispatchIntervention.Forms;

namespace Axe_interDT.Views.DispatchIntervention.Forms
{
    partial class frmSendAvisDepassage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmSendAvisDepassage));
            this.GridADP = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtStatut = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.txtCodeimmeuble = new iTalk.iTalk_TextBox_Small2();
            this.txtDateRealiseDu = new System.Windows.Forms.DateTimePicker();
            this.Label11 = new iTalk.iTalk_TextBox_Small2();
            this.txtDaterealiseAu = new System.Windows.Forms.DateTimePicker();
            this.chkParticulier = new System.Windows.Forms.CheckBox();
            this.cmdStatut = new System.Windows.Forms.Button();
            this.cmdRechercheImmeuble = new System.Windows.Forms.Button();
            this.cmdMail = new System.Windows.Forms.Button();
            this.cmdClean = new System.Windows.Forms.Button();
            this.cmdImprime = new System.Windows.Forms.Button();
            this.flowLayoutPanel3 = new System.Windows.Forms.FlowLayoutPanel();
            this.CmdSelect = new System.Windows.Forms.Button();
            this.cmdDeselect = new System.Windows.Forms.Button();
            this.cmdHisto = new System.Windows.Forms.Button();
            this.cmbIntervention = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            ((System.ComponentModel.ISupportInitialize)(this.GridADP)).BeginInit();
            this.flowLayoutPanel3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // GridADP
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridADP.DisplayLayout.Appearance = appearance1;
            this.GridADP.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridADP.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridADP.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridADP.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridADP.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridADP.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridADP.DisplayLayout.MaxColScrollRegions = 1;
            this.GridADP.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridADP.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridADP.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridADP.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridADP.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridADP.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridADP.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridADP.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridADP.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridADP.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridADP.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridADP.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridADP.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridADP.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridADP.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridADP.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridADP.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridADP.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridADP.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridADP.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.GridADP.Location = new System.Drawing.Point(12, 250);
            this.GridADP.Name = "GridADP";
            this.GridADP.Size = new System.Drawing.Size(794, 359);
            this.GridADP.TabIndex = 412;
            this.GridADP.Text = "ultraGrid1";
            this.GridADP.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridADP_InitializeLayout);
            this.GridADP.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.GridADP_InitializeRow);
            this.GridADP.AfterRowActivate += new System.EventHandler(this.GridADP_AfterRowActivate);
            this.GridADP.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.GridADP_DoubleClickRow);
            // 
            // txtStatut
            // 
            this.txtStatut.AccAcceptNumbersOnly = false;
            this.txtStatut.AccAllowComma = false;
            this.txtStatut.AccBackgroundColor = System.Drawing.Color.White;
            this.txtStatut.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtStatut.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtStatut.AccHidenValue = "";
            this.txtStatut.AccNotAllowedChars = null;
            this.txtStatut.AccReadOnly = false;
            this.txtStatut.AccReadOnlyAllowDelete = false;
            this.txtStatut.AccRequired = false;
            this.txtStatut.BackColor = System.Drawing.Color.White;
            this.txtStatut.CustomBackColor = System.Drawing.Color.White;
            this.txtStatut.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtStatut.ForeColor = System.Drawing.Color.Black;
            this.txtStatut.Location = new System.Drawing.Point(146, 97);
            this.txtStatut.Margin = new System.Windows.Forms.Padding(2);
            this.txtStatut.MaxLength = 32767;
            this.txtStatut.Multiline = false;
            this.txtStatut.Name = "txtStatut";
            this.txtStatut.ReadOnly = false;
            this.txtStatut.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtStatut.Size = new System.Drawing.Size(277, 27);
            this.txtStatut.TabIndex = 2;
            this.txtStatut.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtStatut.UseSystemPasswordChar = false;
            this.txtStatut.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtStatut_KeyPress);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(18, 73);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(122, 19);
            this.label33.TabIndex = 503;
            this.label33.Text = "Date réalisée du";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(274, 67);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(26, 19);
            this.label1.TabIndex = 504;
            this.label1.Text = "au";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 131);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 19);
            this.label2.TabIndex = 505;
            this.label2.Text = "Code immeuble";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(22, 97);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(52, 19);
            this.label3.TabIndex = 506;
            this.label3.Text = "Statut";
            // 
            // txtCodeimmeuble
            // 
            this.txtCodeimmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeimmeuble.AccAllowComma = false;
            this.txtCodeimmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeimmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeimmeuble.AccHidenValue = "";
            this.txtCodeimmeuble.AccNotAllowedChars = null;
            this.txtCodeimmeuble.AccReadOnly = false;
            this.txtCodeimmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeimmeuble.AccRequired = false;
            this.txtCodeimmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeimmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeimmeuble.Location = new System.Drawing.Point(146, 128);
            this.txtCodeimmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeimmeuble.MaxLength = 32767;
            this.txtCodeimmeuble.Multiline = false;
            this.txtCodeimmeuble.Name = "txtCodeimmeuble";
            this.txtCodeimmeuble.ReadOnly = false;
            this.txtCodeimmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeimmeuble.Size = new System.Drawing.Size(278, 27);
            this.txtCodeimmeuble.TabIndex = 3;
            this.txtCodeimmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeimmeuble.UseSystemPasswordChar = false;
            this.txtCodeimmeuble.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeimmeuble_KeyPress);
            // 
            // txtDateRealiseDu
            // 
            this.txtDateRealiseDu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDateRealiseDu.Location = new System.Drawing.Point(146, 72);
            this.txtDateRealiseDu.Name = "txtDateRealiseDu";
            this.txtDateRealiseDu.Size = new System.Drawing.Size(127, 20);
            this.txtDateRealiseDu.TabIndex = 0;
            // 
            // Label11
            // 
            this.Label11.AccAcceptNumbersOnly = false;
            this.Label11.AccAllowComma = false;
            this.Label11.AccBackgroundColor = System.Drawing.Color.White;
            this.Label11.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Label11.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Label11.AccHidenValue = "";
            this.Label11.AccNotAllowedChars = null;
            this.Label11.AccReadOnly = false;
            this.Label11.AccReadOnlyAllowDelete = false;
            this.Label11.AccRequired = false;
            this.Label11.BackColor = System.Drawing.Color.White;
            this.Label11.CustomBackColor = System.Drawing.Color.White;
            this.Label11.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Label11.ForeColor = System.Drawing.Color.Black;
            this.Label11.Location = new System.Drawing.Point(686, 223);
            this.Label11.Margin = new System.Windows.Forms.Padding(2);
            this.Label11.MaxLength = 32767;
            this.Label11.Multiline = false;
            this.Label11.Name = "Label11";
            this.Label11.ReadOnly = false;
            this.Label11.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Label11.Size = new System.Drawing.Size(120, 27);
            this.Label11.TabIndex = 509;
            this.Label11.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Label11.UseSystemPasswordChar = false;
            // 
            // txtDaterealiseAu
            // 
            this.txtDaterealiseAu.Format = System.Windows.Forms.DateTimePickerFormat.Short;
            this.txtDaterealiseAu.Location = new System.Drawing.Point(313, 72);
            this.txtDaterealiseAu.Name = "txtDaterealiseAu";
            this.txtDaterealiseAu.Size = new System.Drawing.Size(140, 20);
            this.txtDaterealiseAu.TabIndex = 1;
            // 
            // chkParticulier
            // 
            this.chkParticulier.AutoSize = true;
            this.chkParticulier.Checked = true;
            this.chkParticulier.CheckState = System.Windows.Forms.CheckState.Checked;
            this.chkParticulier.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.chkParticulier.Location = new System.Drawing.Point(146, 158);
            this.chkParticulier.Name = "chkParticulier";
            this.chkParticulier.Size = new System.Drawing.Size(277, 23);
            this.chkParticulier.TabIndex = 570;
            this.chkParticulier.Text = "Afficher uniquement les particuliers";
            this.chkParticulier.UseVisualStyleBackColor = true;
            // 
            // cmdStatut
            // 
            this.cmdStatut.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdStatut.FlatAppearance.BorderSize = 0;
            this.cmdStatut.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdStatut.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdStatut.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdStatut.Location = new System.Drawing.Point(429, 97);
            this.cmdStatut.Name = "cmdStatut";
            this.cmdStatut.Size = new System.Drawing.Size(25, 20);
            this.cmdStatut.TabIndex = 571;
            this.cmdStatut.UseVisualStyleBackColor = false;
            this.cmdStatut.Click += new System.EventHandler(this.cmdStatut_Click);
            // 
            // cmdRechercheImmeuble
            // 
            this.cmdRechercheImmeuble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheImmeuble.FlatAppearance.BorderSize = 0;
            this.cmdRechercheImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheImmeuble.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheImmeuble.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheImmeuble.Location = new System.Drawing.Point(429, 131);
            this.cmdRechercheImmeuble.Name = "cmdRechercheImmeuble";
            this.cmdRechercheImmeuble.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheImmeuble.TabIndex = 572;
            this.cmdRechercheImmeuble.UseVisualStyleBackColor = false;
            this.cmdRechercheImmeuble.Click += new System.EventHandler(this.cmdRechercheImmeuble_Click);
            // 
            // cmdMail
            // 
            this.cmdMail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdMail.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdMail.FlatAppearance.BorderSize = 0;
            this.cmdMail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMail.Image = global::Axe_interDT.Properties.Resources.new_post_24;
            this.cmdMail.Location = new System.Drawing.Point(130, 2);
            this.cmdMail.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMail.Name = "cmdMail";
            this.cmdMail.Size = new System.Drawing.Size(60, 35);
            this.cmdMail.TabIndex = 573;
            this.cmdMail.UseVisualStyleBackColor = false;
            this.cmdMail.Click += new System.EventHandler(this.cmdMail_Click);
            // 
            // cmdClean
            // 
            this.cmdClean.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdClean.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdClean.FlatAppearance.BorderSize = 0;
            this.cmdClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClean.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdClean.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.cmdClean.Location = new System.Drawing.Point(66, 2);
            this.cmdClean.Margin = new System.Windows.Forms.Padding(2);
            this.cmdClean.Name = "cmdClean";
            this.cmdClean.Size = new System.Drawing.Size(60, 35);
            this.cmdClean.TabIndex = 574;
            this.cmdClean.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdClean.UseVisualStyleBackColor = false;
            this.cmdClean.Click += new System.EventHandler(this.cmdClean_Click);
            // 
            // cmdImprime
            // 
            this.cmdImprime.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdImprime.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdImprime.FlatAppearance.BorderSize = 0;
            this.cmdImprime.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImprime.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdImprime.Image = global::Axe_interDT.Properties.Resources.Printer_24x24;
            this.cmdImprime.Location = new System.Drawing.Point(194, 2);
            this.cmdImprime.Margin = new System.Windows.Forms.Padding(2);
            this.cmdImprime.Name = "cmdImprime";
            this.cmdImprime.Size = new System.Drawing.Size(60, 35);
            this.cmdImprime.TabIndex = 575;
            this.cmdImprime.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdImprime.UseVisualStyleBackColor = false;
            this.cmdImprime.Click += new System.EventHandler(this.cmdImprime_Click);
            // 
            // flowLayoutPanel3
            // 
            this.flowLayoutPanel3.Controls.Add(this.CmdSelect);
            this.flowLayoutPanel3.Controls.Add(this.cmdDeselect);
            this.flowLayoutPanel3.Controls.Add(this.cmdHisto);
            this.flowLayoutPanel3.Location = new System.Drawing.Point(12, 213);
            this.flowLayoutPanel3.Margin = new System.Windows.Forms.Padding(1);
            this.flowLayoutPanel3.Name = "flowLayoutPanel3";
            this.flowLayoutPanel3.Size = new System.Drawing.Size(572, 33);
            this.flowLayoutPanel3.TabIndex = 576;
            // 
            // CmdSelect
            // 
            this.CmdSelect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdSelect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSelect.FlatAppearance.BorderSize = 0;
            this.CmdSelect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSelect.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.CmdSelect.ForeColor = System.Drawing.Color.White;
            this.CmdSelect.Image = ((System.Drawing.Image)(resources.GetObject("CmdSelect.Image")));
            this.CmdSelect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdSelect.Location = new System.Drawing.Point(2, 2);
            this.CmdSelect.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSelect.Name = "CmdSelect";
            this.CmdSelect.Size = new System.Drawing.Size(185, 30);
            this.CmdSelect.TabIndex = 388;
            this.CmdSelect.Text = "  Tout sélectionner";
            this.CmdSelect.UseVisualStyleBackColor = false;
            this.CmdSelect.Click += new System.EventHandler(this.CmdSelect_Click);
            // 
            // cmdDeselect
            // 
            this.cmdDeselect.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdDeselect.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdDeselect.FlatAppearance.BorderSize = 0;
            this.cmdDeselect.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDeselect.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmdDeselect.ForeColor = System.Drawing.Color.White;
            this.cmdDeselect.Image = ((System.Drawing.Image)(resources.GetObject("cmdDeselect.Image")));
            this.cmdDeselect.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdDeselect.Location = new System.Drawing.Point(191, 2);
            this.cmdDeselect.Margin = new System.Windows.Forms.Padding(2);
            this.cmdDeselect.Name = "cmdDeselect";
            this.cmdDeselect.Size = new System.Drawing.Size(185, 30);
            this.cmdDeselect.TabIndex = 391;
            this.cmdDeselect.Text = "Tout désélectionner";
            this.cmdDeselect.UseVisualStyleBackColor = false;
            this.cmdDeselect.Click += new System.EventHandler(this.cmdDeselect_Click);
            // 
            // cmdHisto
            // 
            this.cmdHisto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdHisto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdHisto.FlatAppearance.BorderSize = 0;
            this.cmdHisto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdHisto.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmdHisto.ForeColor = System.Drawing.Color.White;
            this.cmdHisto.Image = ((System.Drawing.Image)(resources.GetObject("cmdHisto.Image")));
            this.cmdHisto.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdHisto.Location = new System.Drawing.Point(380, 2);
            this.cmdHisto.Margin = new System.Windows.Forms.Padding(2);
            this.cmdHisto.Name = "cmdHisto";
            this.cmdHisto.Size = new System.Drawing.Size(185, 30);
            this.cmdHisto.TabIndex = 392;
            this.cmdHisto.Text = "    Historique d\'envoi ";
            this.cmdHisto.UseVisualStyleBackColor = false;
            this.cmdHisto.Click += new System.EventHandler(this.cmdHisto_Click);
            // 
            // cmbIntervention
            // 
            this.cmbIntervention.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmbIntervention.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbIntervention.FlatAppearance.BorderSize = 0;
            this.cmbIntervention.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbIntervention.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIntervention.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmbIntervention.Location = new System.Drawing.Point(2, 2);
            this.cmbIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.cmbIntervention.Name = "cmbIntervention";
            this.cmbIntervention.Size = new System.Drawing.Size(60, 35);
            this.cmbIntervention.TabIndex = 577;
            this.cmbIntervention.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmbIntervention.UseVisualStyleBackColor = false;
            this.cmbIntervention.Click += new System.EventHandler(this.cmbIntervention_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(94, 184);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(146, 19);
            this.label4.TabIndex = 578;
            this.label4.Text = "Immeuble pariculier";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cmbIntervention);
            this.flowLayoutPanel1.Controls.Add(this.cmdClean);
            this.flowLayoutPanel1.Controls.Add(this.cmdMail);
            this.flowLayoutPanel1.Controls.Add(this.cmdImprime);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(550, 12);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(263, 39);
            this.flowLayoutPanel1.TabIndex = 579;
            // 
            // panel3
            // 
            this.panel3.BackColor = System.Drawing.Color.Pink;
            this.panel3.Location = new System.Drawing.Point(14, 184);
            this.panel3.Margin = new System.Windows.Forms.Padding(5, 1, 1, 1);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(59, 16);
            this.panel3.TabIndex = 580;
            // 
            // frmSendAvisDepassage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(818, 620);
            this.Controls.Add(this.panel3);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.flowLayoutPanel3);
            this.Controls.Add(this.cmdRechercheImmeuble);
            this.Controls.Add(this.cmdStatut);
            this.Controls.Add(this.chkParticulier);
            this.Controls.Add(this.txtDaterealiseAu);
            this.Controls.Add(this.Label11);
            this.Controls.Add(this.txtDateRealiseDu);
            this.Controls.Add(this.txtCodeimmeuble);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.txtStatut);
            this.Controls.Add(this.GridADP);
            this.MaximumSize = new System.Drawing.Size(834, 659);
            this.MinimumSize = new System.Drawing.Size(834, 659);
            this.Name = "frmSendAvisDepassage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "FrmSendAvisDePassage";
            this.Load += new System.EventHandler(this.frmSendAvisDepassage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridADP)).EndInit();
            this.flowLayoutPanel3.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);
            this.PerformLayout();

        }



        public Infragistics.Win.UltraWinGrid.UltraGrid GridADP;
        public iTalk.iTalk_TextBox_Small2 txtStatut;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        public iTalk.iTalk_TextBox_Small2 txtCodeimmeuble;
        private System.Windows.Forms.DateTimePicker txtDateRealiseDu;
        public iTalk.iTalk_TextBox_Small2 Label11;
        private System.Windows.Forms.DateTimePicker txtDaterealiseAu;
        public System.Windows.Forms.CheckBox chkParticulier;
        public System.Windows.Forms.Button cmdStatut;
        public System.Windows.Forms.Button cmdRechercheImmeuble;
        public System.Windows.Forms.Button cmdMail;
        public System.Windows.Forms.Button cmdClean;
        public System.Windows.Forms.Button cmdImprime;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel3;
        public System.Windows.Forms.Button CmdSelect;
        public System.Windows.Forms.Button cmdDeselect;
        public System.Windows.Forms.Button cmdHisto;
        public System.Windows.Forms.Button cmbIntervention;
        public System.Windows.Forms.Label label4;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.Panel panel3;
    }
}