﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.SharedViews;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.DispatchDevis.Forms
{
    public partial class frmPrestations : Form
    {
        int lNumFicheStandard = 0;
        int lNoIntervention = 0;

        bool boolVisible;
        bool boolEofBof;
        bool boolErrDecele;
        bool boolHead;
        bool boolsens;

        DataTable rsStd;
        DataTable rs;
        DataTable rsAdo;
        ModAdo tmpAdorsSTD = new ModAdo();
        ModAdo tmpAdorsAddt = new ModAdo();
        ModAdo tmpAdorsAddEntete = new ModAdo();
        ModAdo tmprsAdo = new ModAdo();
        ModAdo tmpAdors = new ModAdo();
        ModAdo tmpAdorsImm = new ModAdo();
        ModAdo tmpAdorsint = new ModAdo();
        public frmPrestations()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdLancer_Click(object sender, EventArgs e)
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            bool bEtatV2 = false;

            try
            {
                if (String.IsNullOrEmpty(txtAnnee.Text) && String.IsNullOrEmpty(SSdbMois.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une année et un mois avant de lancer les prestations.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                // Or UCase(fncUserName) = UCase("Rachid abbouchi") Then
                if (General.sCreatePrestV2 == "1")
                {
                    //=== modif du 29 10 2014, noueau  programme de création de prestattion contractuelle.
                    //=== fonction de creation de prestation.
                    CreationPrestationV2();//tested
                    //=== imprime l'état.
                    if (boolEofBof == false)
                    {
                        fc_impressionV2();
                    }
                    return;

                }
                else if (General.sSendPrestV2limite == "1")
                {
                    bEtatV2 = false;
                    sSQL = "SELECT     Matricule From Personnel WHERE     (EtatV = 1)";
                    rs = tmpAdors.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)
                    {
                        foreach (DataRow rsRow in rs.Rows)
                        {
                            if (rsRow["Matricule"].ToString().ToUpper() == txtTechnicien.Text.ToUpper())
                            {
                                bEtatV2 = true;
                                break;
                            }
                            //rs.MoveNext();
                        }
                    }
                    rs.Dispose();
                    rs = null;

                    if (bEtatV2 == true)
                    {
                        //=== fonction de creation de prestation.
                        CreationPrestationV2();
                        //=== imprime l'état.
                        if (boolEofBof == false)
                        {
                            fc_impressionV2();
                        }
                        return;
                    }
                    else
                    {
                        CreationPrestation();//tested
                    }

                }
                else
                {
                    CreationPrestation();
                }

                //EnvoiePrestation
                if (boolEofBof == false)
                {
                    Impression();// Todo il faut tester cette fct
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "Prestations " + ";cmdLancer_Click");
            }


        }
        
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_impressionV2()
        {
            string sSelectionFormula = null;
            string sReturn = null;
            try
            {
                //CR.Reset
                //CR.ReportFileName = gsRpt & CHEMINETATPRESTv2
                //CR.WindowShowPrintSetupBtn = True

                sSelectionFormula = "{Intervention.DateSaisie}>=" + "date("
                    + Convert.ToDateTime("01/" + RecupValeurInDbcombo() + "/" + txtAnnee.Text).ToString("yyyy,MM,dd") + ")"
                    + " and {Intervention.DateSaisie}<="
                    + "date(" + Convert.ToDateTime(JourFinDeMois(RecupValeurInDbcombo(), txtAnnee.Text) + "/"
                    + RecupValeurInDbcombo() + "/" + txtAnnee.Text).ToString("yyyy,MM,dd") + ")"
                    + " and {Intervention.Intervenant}='" + txtTechnicien.Text + "'" + " and {Intervention.VisiteEntretien}= 1 ";


                //CR.SelectionFormula = sSelectionFormula

                //CR.SelectionFormula = "{PreparationIntervention.Intervenant}='" & txtTechnicien.Text & "' and {PreparationIntervention." & SSdbMois.Text & "}= 1"
                //CR.Formulas(0) = "Mois ='" & SSdbMois.Text & "'"
                //CR.Action = 1
                //CR.SelectionFormula = ""


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                ModCrystalPDF.tabFormulas = new ModCrystalPDF.tpFormulas[1];
                ModCrystalPDF.tabFormulas[0].sNom = "";

                sReturn = devModeCrystal.fc_ExportCrystal(sSelectionFormula,
                     General.gsRpt + General.CHEMINETATPRESTv2, "", "", "", "",
                     System.Windows.Forms.CheckState.Checked);
                //sReturn = fc_ExportCrystalSansFormule(strRequete, CHEMINEUROONLY)
                if (!string.IsNullOrEmpty(sReturn))
                {
                    ModuleAPI.Ouvrir(sReturn);//exception le fichier est introuvable
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                }

                return;
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";Impression");
            }

        }
        private void CreationPrestationV2()
        {
            bool boolAutorise = false;
            bool boolEOF = false;
            string SQL = null;
            string strDesignation = null;
            string sAnalytiqueActivite = null;
            DataTable rsAddt = default(DataTable);
            DataTable rsAddEntete = default(DataTable);
            DataTable rsImm = default(DataTable);
            DataTable rsint = default(DataTable);
            int longMsgb = 0;
            System.DateTime dtDateDeb = default(System.DateTime);
            System.DateTime dtDateFin = default(System.DateTime);
            var tmpAdo = new ModAdo();
            try
            {

                //=== recherche le code analytique contrat.

                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {

                }
                else//tested
                {
                    SAGE.fc_OpenConnSage();
                    DataTable dt = new DataTable();
                    SqlDataAdapter sda = new SqlDataAdapter("SELECT CA_Num FROM F_COMPTEA WHERE ActiviteContrat = '1'", SAGE.adoSage);
                    sda.Fill(dt);
                    sAnalytiqueActivite = dt.Rows[0]["CA_Num"].ToString();
                    SAGE.fc_CloseConnSage();
                }
                //=== recherche la désignation de l'article.
                strDesignation = tmpAdo.fc_ADOlibelle("SELECT Designation1 from facArticle where CodeArticle='" + ModP2v2.cCodeVisite + "'");

                SQL = "UPDATE PreparationIntervention SET RESILIEE = 0 where Resiliee IS NULL";
                int xx = General.Execute(SQL);

                boolAutorise = false;
                boolEOF = false;
                boolEofBof = false;
                //adocnn.Execute "UPDATE Contrat INNER JOIN PreparationIntervention ON Contrat.CodeImmeuble = PreparationIntervention.Codeimmeuble SET PreparationIntervention.Resiliee = True WHERE Contrat.CodeArticle ='999'"
                rs = new DataTable();
                rsAdo = new DataTable();
                rsStd = new DataTable();

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;


                //=== requete pour ajout dans la fiche standard.
                SQL = "SELECT NumFicheStandard, Utilisateur, DateAjout,"
                        + " CodeImmeuble, CodeOrigine1, CodeOrigine2, Source,Intervention, AnalytiqueActivite "
                        + " FROM GestionStandard WHERE NumFicheStandard=0";

                rsStd = tmpAdorsSTD.fc_OpenRecordSet(SQL);


                //=== requete pour ajout dans la table InterP2Complement.
                SQL = "SELECT     Nointervention, Codeimmeuble, CodeArticle, Designation, Commentaire, ID, Intervenant , AFaire, TypeIntervenant"
                    + " ,Devis, NumeroDevis, TitreDevis, DateDevis, CodeEtatDevis" + " From InterP2Complement " + " WHERE     (ID = 0)";

                rsAddt = tmpAdorsAddt.fc_OpenRecordSet(SQL);

                //=== requete pour l ajout dans la table en entete.
                SQL = "SELECT ID, NoIntervention, CodeImmeuble, Gardien, TelGardien,  HorairesLoge, CodeAcces1, " + " CodeAcces2, SpecifAcces, CodeQualif_QUA, NomCorrespondant, "
                    + " DateDerniereVisite , NbInterMoisDernier, NbPanneEcs, NbPanneCh, NbPanneAutre," + " NbInterFacturee, NbInterAstreinte, NbPanneEcsCh "
                    + " From InterP2EnTete " + " WHERE     (ID = 0)";

                rsAddEntete = tmpAdorsAddEntete.fc_OpenRecordSet(SQL);


                //---Recupere la designation de l'article PO1
                //SQL = "SELECT Designation1 from facArticle where CodeArticle='PO1'"

                //rs.Open SQL, adocnn
                //strDesignation = rs!Designation1 & ""
                //rs.Close

                string y = SSdbMois.Value.ToString();
                string c = SSdbMois.ActiveRow.Cells[1].Value.ToString();
                string cc = Convert.ToString(Convert.ToInt16(SSdbMois.ActiveRow.Cells[1].Value) + 1);
                //=== Extrait les donnees lies à l immeuble.
                SQL = "SELECT     Immeuble.CodeImmeuble, Immeuble.Gardien, Immeuble.TelGardien, Immeuble.HorairesLoge, "
                        + " Immeuble.CodeAcces1 , Immeuble.CodeAcces2, "
                        + " Immeuble.SpecifAcces , IMC_ImmCorrespondant.CodeQualif_QUA, IMC_ImmCorrespondant.Nom_IMC "
                        + " FROM         Immeuble LEFT OUTER JOIN "
                        + " IMC_ImmCorrespondant ON Immeuble.CodeImmeuble = IMC_ImmCorrespondant.CodeImmeuble_IMM "
                        + " WHERE  (Codeimmeuble IN "
                        + "(SELECT distinct Contrat.CodeImmeuble "
                        + "FROM  Contrat INNER JOIN "
                        + "PreparationIntervention ON Contrat.CodeImmeuble = PreparationIntervention.CodeImmeuble "
                        + "WHERE ((Contrat.DateFin >='01/"
                        + (SSdbMois.ActiveRow.Cells[1].Value.ToString() == "12" ? "01" : Convert.ToString(Convert.ToInt16(SSdbMois.ActiveRow.Cells[1].Value) + 1))
                        + "/2004' OR Contrat.Resiliee = 1) AND "
                        + "Contrat.Datefin IS NULL OR "
                        + "Contrat.Resiliee = 0) AND PreparationIntervention.Intervenant = '"
                        + txtTechnicien.Text + "'))" + " ORDER BY Immeuble.CodeImmeuble";

                rsImm = tmpAdorsImm.fc_OpenRecordSet(SQL);


                dtDateDeb = Convert.ToDateTime("01/" + SSdbMois.ActiveRow.Cells[1].Value + "/" + txtAnnee.Text);//quand txtAnnee.Text est vide une erreur se déclenche je dois 
                dtDateFin = dtDateDeb.AddDays(-1);
                dtDateDeb = dtDateDeb.AddMonths(-1);

                //=== extrait les visites (table intervention) pour un mois donnée afin de controler si celle ce a déjà éte crée.
                General.sSQL = "SELECT   Intervention.NoIntervention, Intervention.DateRealise, FacArticle.TYI_Code, Intervention.CodeEtat, GestionStandard.Astreinte,"
                    + " Intervention.codeimmeuble, Intervention.VisiteEntretien, Intervention.Article "
                    + " FROM         Intervention INNER JOIN "
                    + " FacArticle ON Intervention.Article = FacArticle.CodeArticle INNER JOIN"
                    + " GestionStandard ON Intervention.NumFicheStandard = GestionStandard.NumFicheStandard"
                    + " WHERE  Intervention.Codeimmeuble IN "
                    + "(SELECT Contrat.CodeImmeuble "
                    + "FROM Contrat INNER JOIN "
                    + "PreparationIntervention ON Contrat.CodeImmeuble = PreparationIntervention.CodeImmeuble "
                    + "WHERE ((Contrat.DateFin >='01/"
                    + (SSdbMois.ActiveRow.Cells[1].Value.ToString() == "12" ? "01" : Convert.ToString(Convert.ToInt16(SSdbMois.ActiveRow.Cells[1].Value) + 1))
                    + "/2004' OR Contrat.Resiliee = 1) AND "
                    + "Contrat.Datefin IS NULL OR "
                    + "Contrat.Resiliee = 0)) and Intervention.DateRealise >='"
                    + dtDateDeb + "' and Intervention.DateRealise <='" + dtDateFin + "'";


                rsint = tmpAdorsint.fc_OpenRecordSet(General.sSQL);

                //----Récupére toutes les interventions a créer dans la table PreparationIntervention
                //SQL = "SELECT PreparationIntervention.Codeimmeuble,PreparationIntervention.Commentaire, PreparationIntervention.CodeArticle " _
                //& " From PreparationIntervention" _
                //& " WHERE PreparationIntervention.Intervenant ='" & txtTechnicien & "'" _
                //& " AND " & SSdbMois & "=1 and PreparationIntervention.resiliee=0"
                //SQL = SQL & " AND (Codeimmeuble IN "
                //SQL = SQL & "(SELECT Contrat.CodeImmeuble "
                //SQL = SQL & "FROM Contrat INNER JOIN "
                //SQL = SQL & "PreparationIntervention ON Contrat.CodeImmeuble = PreparationIntervention.CodeImmeuble "
                //SQL = SQL & "WHERE ((Contrat.DateFin >='01/" & Format(IIf(SSdbMois.Columns(1).value = "12", "01", CStr(CInt(SSdbMois.Columns(1).value) + 1)), "00") & "/2004' OR Contrat.Resiliee = 1) AND "
                //SQL = SQL & "Contrat.Datefin IS NULL OR "
                //SQL = SQL & "Contrat.Resiliee = 0) AND PreparationIntervention.Intervenant = '" & txtTechnicien.Text & "'))"


                //===Récupére toutes les interventions a créer dans la table PreparationIntervention
                SQL = "SELECT distinct PreparationIntervention.Codeimmeuble "
                        + " From PreparationIntervention"
                        + " WHERE PreparationIntervention.Intervenant ='"
                        + txtTechnicien.Text + "'"
                        + " AND " + SSdbMois.Text + "=1 and PreparationIntervention.resiliee=0";
                SQL = SQL + " AND (Codeimmeuble IN ";
                SQL = SQL + "(SELECT Contrat.CodeImmeuble ";
                SQL = SQL + "FROM Contrat INNER JOIN ";
                SQL = SQL + "PreparationIntervention ON Contrat.CodeImmeuble = PreparationIntervention.CodeImmeuble ";
                SQL = SQL + "WHERE ((Contrat.DateFin >='01/"
                        + (SSdbMois.ActiveRow.Cells[1].Value.ToString() == "12" ? "01" : Convert.ToString(Convert.ToInt16(SSdbMois.ActiveRow.Cells[1].Value) + 1)) + "/2004' OR Contrat.Resiliee = 1) AND ";
                SQL = SQL + "Contrat.Datefin IS NULL OR ";
                SQL = SQL + "Contrat.Resiliee = 0) AND PreparationIntervention.Intervenant = '" + txtTechnicien.Text + "'))";

                rs = tmpAdo.fc_OpenRecordSet(SQL);

                if (rs.Rows.Count > 0)
                {
                    //=== Vérifie si il éxiste déja des interventions.
                    SQL = "SELECT Intervention.NoIntervention,Intervention.CodeImmeuble, Intervention.Article,"
                            + " Intervention.Designation, Intervention.Commentaire,"
                            + " Intervention.Intervenant, Intervention.Dispatcheur,"
                            + " Intervention.DateSaisie, Intervention.NumFicheStandard,"
                            + " intervention.CodeEtat, intervention.VisiteEntretien, "
                            + " intervention.VisiteEntretienAuto,  "
                            + " intervention.CreePar , intervention.CreeLe "
                            + " FROM Intervention"
                            + " WHERE Intervention.DateSaisie >='"
                            + Convert.ToDateTime("01/" + RecupValeurInDbcombo() + "/"
                            + txtAnnee.Text)
                            + "' and Intervention.DateSaisie <='"
                            + Convert.ToDateTime(JourFinDeMois(RecupValeurInDbcombo(), txtAnnee.Text) + "/"
                            + RecupValeurInDbcombo() + "/" + txtAnnee.Text)
                            + "' and Intervention.Intervenant='" + txtTechnicien.Text + "'"
                            + " and VisiteEntretienAuto = 1 ";

                    rsAdo = tmprsAdo.fc_OpenRecordSet(SQL);

                    if (rsAdo.Rows.Count > 0)
                    {
                        boolEOF = false;
                    }
                    else
                    {
                        boolEOF = true;
                    }
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                    foreach (DataRow rsRow in rs.Rows)
                    {
                        if (boolEOF == false)
                        {
                            //rsAdo.Find "CodeImmeuble='" & rs!CodeImmeuble & "' AND Article ='" & gFr_DoublerQuote(rs!CodeArticle) & "'", , adSearchForward, adBookmarkFirst
                            //rsAdo.Find("CodeImmeuble='" + rs.Fields("CodeImmeuble").Value + "'", , ADODB.SearchDirectionEnum.adSearchForward, ADODB.BookmarkEnum.adBookmarkFirst);
                            var r = rsAdo.Select("CodeImmeuble='" + rsRow["CodeImmeuble"] + "'");
                            // var CodeImm = rsAdo.Select(rsRow["CodeImmeuble"].ToString());
                            //var noImm= rs.Select();

                            if (rsAdo.Rows.Count == 0)
                            {

                                if (boolAutorise == false)
                                {
                                    longMsgb = Convert.ToInt16(Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Desirez-vous créer des prestations ?", "Suppression", MessageBoxButtons.YesNo, MessageBoxIcon.Question));
                                    if (longMsgb != 7)
                                    {
                                        boolAutorise = true;
                                    }
                                    else
                                    {
                                        break;
                                        boolEofBof = true;
                                    }
                                }

                                fc_CreateViste(rsAdo, rsRow, rsAddt, rsAddEntete, rsImm, sAnalytiqueActivite, strDesignation, rsint);
                            }

                        }
                        else if (boolEOF == true)
                        {
                            fc_CreateViste(rsAdo, rsRow, rsAddt, rsAddEntete, rsImm, sAnalytiqueActivite, strDesignation, rsint);

                        }
                        // rs.MoveNext();
                    }

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'a pas de prestations liées à ce dépanneur pour le mois de " + SSdbMois.Text, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    boolEofBof = true;
                }

                rs.Dispose();
                rsAdo.Dispose();
                rs = null;
                rsAdo = null;
                rsStd.Dispose();
                rsStd = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CreationPrestation");
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="rsAdo"></param>
        /// <param name="rsRow"></param>
        /// <param name="rsAddt"></param>
        /// <param name="rsAddEntete"></param>
        /// <param name="rsImm"></param>
        /// <param name="sAnalytiqueActivite"></param>
        /// <param name="sDesignation"></param>
        /// <param name="rsint"></param>
        private void fc_CreateViste(DataTable rsAdo, DataRow rsRow, DataTable rsAddt, DataTable rsAddEntete, DataTable rsImm,
            string sAnalytiqueActivite, string sDesignation, DataTable rsint)
        {
            string sSQLt = null;
            string sSQLspecif = null;
            DataTable rsT = default(DataTable);
            DataTable rsSpec = default(DataTable);
            System.DateTime dtDate = default(System.DateTime);
            System.DateTime dtDateDerniere = default(System.DateTime);
            int lNointervention = 0;
            int lNbDevis = 0;
            int lnbInterMoisDernier = 0;
            int lNbPanneECS = 0;
            int lNbPanneCH = 0;
            int lnbPanneAutre = 0;
            int lnbPanneFacture = 0;
            int lNbAstreinte = 0;
            int lNbPanneEcsCh = 0;
            dtDate = DateTime.Now;
            General.gsUtilisateur = General.fncUserName();

            try
            {

                //=== ajout d'une fiche d appel. ==>Tested

                var NewRow = rsStd.NewRow();

                //SqlCommandBuilder cb = new SqlCommandBuilder(tmpAdorsSTD.SDArsAdo);
                //  tmpAdorsSTD.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                //  tmpAdorsSTD.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();              
                //  SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                //  insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                //  insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                //  tmpAdorsSTD.SDArsAdo.InsertCommand = insertCmd;
                //  tmpAdorsSTD.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                //  {
                //      if (e.StatementType == StatementType.Insert)
                //      {
                //         var id =e.Command.Parameters["@ID"].Value;
                //          NewRow["NumFicheStandard"] = Convert.ToInt32(id);
                //      }
                //  });

                int NumFicheStandard = 0;
                var tmp = new ModAdo();
                NewRow["Utilisateur"] = General.gsUtilisateur;
                NewRow["DateAjout"] = DateTime.Now;
                NewRow["CodeImmeuble"] = rsRow["CodeImmeuble"] + "";
                NewRow["CodeOrigine1"] = "6";
                NewRow["CodeOrigine2"] = "15";
                NewRow["Source"] = "Prestation";
                NewRow["AnalytiqueActivite"] = sAnalytiqueActivite;
                NewRow["Intervention"] = "1";
                rsStd.Rows.Add(NewRow);
                int xx = tmpAdorsSTD.Update();
                using (var tmpFichStandard = new ModAdo())
                    NumFicheStandard = Convert.ToInt32(tmpFichStandard.fc_ADOlibelle("select max(NumFicheStandard) from  GestionStandard"));

                //=== ajout d'une fiche d'intervention.==>tested
                var NewRowrs = rsAdo.NewRow();

                NewRowrs["CodeImmeuble"] = rsRow["CodeImmeuble"].ToString();
                NewRowrs["Numfichestandard"] = NumFicheStandard;
                // If sCodeArticleP2 = "1" Then
                NewRowrs["Article"] = ModP2v2.cCodeVisite;
                NewRowrs["Designation"] = sDesignation;
                // Else
                //      rsAdo!Article = "PO1"
                //      rsAdo!Designation = strDesignation
                //  End If
                NewRowrs["Commentaire"] = "VISITE D'ENTRETIEN DU MOIS : " + SSdbMois.Text;
                //& vbCrLf & rs!Commentaire & ""
                NewRowrs["Intervenant"] = txtTechnicien.Text;
                NewRowrs["dispatcheur"] = "8";
                NewRowrs["DateSaisie"] = Convert.ToDateTime(ModP2v2.Date1JourSemaine("Dimanche", Convert.ToDateTime("01/" + RecupValeurInDbcombo() + "/" + txtAnnee.Text)));
                NewRowrs["CodeEtat"] = "00";
                NewRowrs["VisiteEntretienAuto"] = 1;
                NewRowrs["VisiteEntretien"] = 1;
                NewRowrs["CreePar"] = General.gsUtilisateur;
                NewRowrs["CreeLe"] = dtDate;
                rsAdo.Rows.Add(NewRowrs);
                int zz = tmprsAdo.Update();
                using (var tmpADOIntev = new ModAdo())
                    lNointervention = Convert.ToInt32(tmpADOIntev.fc_ADOlibelle("select max(Nointervention) from  Intervention"));


                //=== ajout de l'entete des visites P2.

                //rsImm.Filter = ADODB.FilterGroupEnum.adFilterNone;
                var rsImmFilter = rsImm.Select("CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(rsRow["CodeImmeuble"].ToString()) + "'");
                //rsImm.Filter = ADODB.FilterGroupEnum.adFilterNone;
                var rsintFilter = rsint.Select("CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(rsRow["CodeImmeuble"].ToString()) + "'");

                //rsint.Sort = "DateSaisie DESC"

                var NewRowrsAddEntete = rsAddEntete.NewRow();
                NewRowrsAddEntete["NoIntervention"] = lNointervention;
                NewRowrsAddEntete["CodeImmeuble"] = rsRow["CodeImmeuble"] + "";
                NewRowrsAddEntete["Gardien"] = rsImmFilter[0]["Gardien"];
                NewRowrsAddEntete["TelGardien"] = rsImmFilter[0]["TelGardien"];
                NewRowrsAddEntete["HorairesLoge"] = rsImmFilter[0]["HorairesLoge"];
                NewRowrsAddEntete["codeAcces1"] = rsImmFilter[0]["codeAcces1"];
                NewRowrsAddEntete["CodeAcces2"] = rsImmFilter[0]["CodeAcces2"];
                NewRowrsAddEntete["SpecifAcces"] = rsImmFilter[0]["SpecifAcces"];
                NewRowrsAddEntete["CodeQualif_QUA"] = rsImmFilter[0]["CodeQualif_QUA"];
                NewRowrsAddEntete["NomCorrespondant"] = rsImmFilter[0]["nom_imc"];


                lnbInterMoisDernier = 0;
                lNbPanneCH = 0;
                lNbPanneECS = 0;
                lnbPanneAutre = 0;
                lnbPanneFacture = 0;
                lNbAstreinte = 0;
                lNbPanneEcsCh = 0;
                dtDateDerniere = Convert.ToDateTime("01/01/1950 00:00:00");

                if (rsintFilter.Count() > 0)
                {
                    foreach (DataRow rsintFilterRow in rsintFilter)
                    {
                        //==== nbre total d'intervention effactuées le mois dernier.
                        lnbInterMoisDernier = lnbInterMoisDernier + 1;

                        if (rsintFilterRow["CodeEtat"].ToString().ToUpper() == "ZZ")
                        {

                            //If UCase(rsint!TYI_Code) = UCase("CH") Then

                            if (General.nz(rsintFilterRow["Article"], "").ToString().ToUpper() == "I11".ToString().ToUpper())
                            {
                                //==== nbre total d'intervention chauffage effectuées le mois dernier.
                                lNbPanneCH = lNbPanneCH + 1;

                                //ElseIf UCase(rsint!TYI_Code) = UCase("ECS") Then
                            }
                            else if (rsintFilterRow["Article"].ToString().ToUpper() == "I25".ToString().ToUpper())
                            {
                                //==== nbre total d'intervention ECS effectuées le mois dernier.
                                lNbPanneECS = lNbPanneECS + 1;

                            }
                            else if (rsintFilterRow["Article"].ToString().ToUpper() == "I10".ToUpper())
                            {
                                //==== nbre total d'intervention ECS+CH effectuées le mois dernier.
                                lNbPanneEcsCh = lNbPanneEcsCh + 1;
                                //ElseIf UCase(rsint!TYI_Code) = UCase("AUTRE") Then
                            }
                            else
                            {
                                //==== nbre total d'intervention AUTRE effectuées le mois dernier.
                                lnbPanneAutre = lnbPanneAutre + 1;
                            }

                            if (Convert.ToInt16(General.nz(rsintFilterRow["VisiteEntretien"].ToString(), 0)) == 1 && General.IsDate(rsintFilterRow["DateRealise"].ToString()))
                            {
                                //=== stocke la date de la derniére visite.
                                if (Convert.ToDateTime(rsintFilterRow["DateRealise"].ToString()) > dtDateDerniere)
                                {
                                    dtDateDerniere = Convert.ToDateTime(rsintFilterRow["DateRealise"].ToString());
                                }
                            }
                        }

                        if (rsintFilterRow["CodeEtat"].ToString().ToUpper() == "T")
                        {
                            //==== nbre total d'intervention facturées effectuées le mois dernier.
                            lnbPanneFacture = lnbPanneFacture + 1;

                            //If IsDate(rsint!DateRealise) Then
                            //        '=== stocke la date de la derniére visite.
                            //        If CDate(rsint!DateRealise) > dtDateDerniere Then
                            //                dtDateDerniere = rsint!DateRealise
                            //        End If
                            //End If
                        }

                        if (!string.IsNullOrEmpty(rsintFilterRow["Astreinte"].ToString()) && Convert.ToInt16(rsintFilterRow["Astreinte"]) == 1)
                        {
                            //==== nbre total d'intervention d'astreinte effectuées le mois dernier.
                            lNbAstreinte = lNbAstreinte + 1;

                            //If IsDate(rsint!DateRealise) Then
                            //        '=== stocke la date de la derniére visite.
                            //        If CDate(rsint!DateRealise) > dtDateDerniere Then
                            //                dtDateDerniere = rsint!DateRealise
                            //        End If
                            //End If
                        }

                        //rsint.MoveNext();
                    }
                }
                DateTime dt = Convert.ToDateTime("01/01/1950 00:00:00");
                if (dtDateDerniere == Convert.ToDateTime("01/01/1950 00:00:00"))
                {

                    sSQLt = "SELECT     TOP (1) DateRealise From Intervention "
                            + " WHERE   CodeEtat = 'ZZ' AND CodeImmeuble = '"
                            + StdSQLchaine.gFr_DoublerQuote(rsRow["CodeImmeuble"].ToString()) + "' "
                            + " and VisiteEntretien = 1 " + " ORDER BY DateRealise DESC";

                    sSQLt = tmp.fc_ADOlibelle(sSQLt);

                    if (General.IsDate(sSQLt))
                    {
                        NewRowrsAddEntete["DateDerniereVisite"] = Convert.ToDateTime(sSQLt);
                    }
                    else
                    {

                        NewRowrsAddEntete["DateDerniereVisite"] = System.DBNull.Value;
                    }
                }
                else
                {
                    NewRowrsAddEntete["DateDerniereVisite"] = dtDateDerniere;
                }

                NewRowrsAddEntete["NbInterMoisDernier"] = lnbInterMoisDernier;
                NewRowrsAddEntete["NbPanneEcs"] = lNbPanneECS;
                NewRowrsAddEntete["NbPanneCh"] = lNbPanneCH;
                NewRowrsAddEntete["NbPanneEcsCh"] = lNbPanneEcsCh;
                NewRowrsAddEntete["NbPanneAutre"] = lnbPanneAutre;
                NewRowrsAddEntete["NbInterFacturee"] = lnbPanneFacture;
                NewRowrsAddEntete["NbInterAstreinte"] = lNbAstreinte;

                rsAddEntete.Rows.Add(NewRowrsAddEntete);


                int z = tmpAdorsAddEntete.Update();//everifier

                //=== controle que l'intervenant est l'intervenant par defaut de l'immeuble, si vrai
                //===  on recherche les autres prestations à effectuer apr les autres intervenants(par ex un sous traitant)
                //===  pour chaque immeuble.

                sSQLt = "SELECT CodeDepanneur From Immeuble "
                        + " WHERE CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(rsRow["CodeImmeuble"].ToString())
                        + "'" + " AND CodeDepanneur = '" + StdSQLchaine.gFr_DoublerQuote(txtTechnicien.Text) + "'";
                sSQLt = tmp.fc_ADOlibelle(sSQLt);

                //If sSQLt <> "" Then

                //sSQL = "SELECT   PreparationIntervention.Codeimmeuble, PreparationIntervention.Commentaire," _
                //& " PreparationIntervention.CodeArticle, FacArticle.Designation1, PreparationIntervention.Intervenant " _
                //& " FROM         PreparationIntervention LEFT OUTER JOIN" _
                //& " FacArticle ON PreparationIntervention.CodeArticle = FacArticle.CodeArticle" _
                //& " WHERE   " & SSdbMois & " = 1 AND " _
                //& " Intervenant <> '" & gFr_DoublerQuote(txtTechnicien) & "' and Codeimmeuble ='" & gFr_DoublerQuote(rs!CodeImmeuble) & "'"

                //===  ajout de toutes les prestations asscoiées à l'immmeuble quelque soit l'intervenant. 
                //==>/Tested

                General.sSQL = "SELECT   PreparationIntervention.Codeimmeuble, PreparationIntervention.Commentaire, PreparationIntervention."
                        + SSdbMois.Text + "," + " PreparationIntervention.CodeArticle, FacArticle.Designation1, PreparationIntervention.Intervenant "
                        + " , Personnel.CodeQualif" + " FROM PreparationIntervention LEFT OUTER JOIN "
                        + " Personnel ON PreparationIntervention.Intervenant = Personnel.Matricule LEFT OUTER JOIN "
                        + " FacArticle ON PreparationIntervention.CodeArticle = FacArticle.CodeArticle "
                        + " WHERE Codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(rsRow["CodeImmeuble"].ToString()) + "'";
                var tmpAdorsT = new ModAdo();
                rsT = tmpAdorsT.fc_OpenRecordSet(General.sSQL);
                var NewRows = rsAddt.NewRow();



                if (rsT.Rows.Count > 0)
                {
                    foreach (DataRow rsTRow in rsT.Rows)
                    {
                        //rsAddt.AddNew();            
                        // rsAddt.Rows.Add();
                        NewRows["Nointervention"] = lNointervention;
                        NewRows["CodeImmeuble"] = rsRow["CodeImmeuble"];
                        NewRows["CodeArticle"] = rsTRow["CodeArticle"];
                        NewRows["Designation"] = rsTRow["Designation1"];
                        NewRows["Commentaire"] = rsTRow["Commentaire"];
                        NewRows["Intervenant"] = rsTRow["Intervenant"];
                        NewRows["Devis"] = 0;

                        //If UCase(rst!Intervenant) = UCase(sSQLt) Then
                        //    rsAddt!TypeIntervenant = "T"
                        //
                        //Else
                        //    sSQLspecif = "SELECT     SpecifQualif.QualifSousTraitant, SpecifQualif.QualifSpecialiste, SpecifQualif.CodeQualif, Personnel.Matricule" _
                        //& " FROM         Qualification INNER JOIN " _
                        //& " Personnel ON Qualification.CodeQualif = Personnel.CodeQualif INNER JOIN " _
                        //& " SpecifQualif ON Qualification.CodeQualif = SpecifQualif.CodeQualif " _
                        //& " WHERE Personnel.Matricule = '" & gFr_DoublerQuote(rst!Intervenant) & "'"


                        //        Set rsSpec = fc_OpenRecordSet(sSQLspecif)
                        //        If Not (rsSpec.EOF And rsSpec.bof) Then
                        //                If nz(rsSpec!QualifSpecialiste, False) = True Then
                        //                        rsAddt!TypeIntervenant = "SP"
                        //                ElseIf nz(rsSpec!QualifSousTraitant, False) = True Then
                        //                        rsAddt!TypeIntervenant = "ST"
                        //                Else
                        //                        rsAddt!TypeIntervenant = "XX"
                        //                End If
                        //        Else
                        //                rsAddt!TypeIntervenant = "XX"
                        //        End If
                        //        rsSpec.Close
                        //        Set rsSpec = Nothing
                        //End If
                        if (rsTRow[SSdbMois.Value.ToString()].ToString() == "-1")
                        {
                            NewRows["AFaire"] = 1;
                        }
                        else
                        {
                            NewRows["AFaire"] = 0;
                        }
                        rsAddt.Rows.Add(NewRows.ItemArray);
                        int rr = tmpAdorsAddt.Update();

                    }
                }
                rsT.Dispose();

                //==== ajoute les 4 derniers devis en priorité les A, puis les 04, puis les 00.
                lNbDevis = 0;

                //=== devis en A.

                General.sSQL = "SELECT  TOP 4   NumeroDevis, TitreDevis, DateCreation, DateAcceptation, CodeImmeuble, DateImpression, CodeEtat"
                        + " From DevisEnTete " + " WHERE     (CodeEtat = 'A') AND (CodeImmeuble = '"
                        + StdSQLchaine.gFr_DoublerQuote(rsRow["CodeImmeuble"].ToString()) + "') "
                        + " AND (AdrEnvoi2 IN (SELECT     Table1.Adresse1 "
                        + " FROM          Table1 INNER JOIN "
                        + " Immeuble ON Table1.Code1 = Immeuble.Code1 "
                        + " WHERE   Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(rsRow["CodeImmeuble"].ToString()) + "'))"
                        + " ORDER BY DateCreation DESC ";
                rsT = tmpAdorsT.fc_OpenRecordSet(General.sSQL);
                var NewRowAddt = rsAddt.NewRow();

                if (rsT.Rows.Count > 0)
                {
                    foreach (DataRow rsTRow in rsT.Rows)
                    {
                        //rsAddt.AddNew();

                        NewRowAddt["Nointervention"] = lNointervention;
                        NewRowAddt["CodeImmeuble"] = rsRow["CodeImmeuble"];
                        NewRowAddt["Devis"] = 1;
                        NewRowAddt["numerodevis"] = rsTRow["numerodevis"];
                        NewRowAddt["TitreDevis"] = rsTRow["TitreDevis"];
                        NewRowAddt["DateDevis"] = rsTRow["DateAcceptation"];
                        NewRowAddt["CodeEtatDevis"] = "A";
                        rsAddt.Rows.Add(NewRowAddt.ItemArray);
                        int rr = tmpAdorsAddt.Update();

                        lNbDevis = lNbDevis + 1;

                        // rsT.MoveNext();
                    }
                }
                rsT.Dispose();
                //=== devis en 04.
                if (lNbDevis < 4)
                {

                    General.sSQL = "SELECT  TOP 4   NumeroDevis, TitreDevis, DateCreation, DateAcceptation, CodeImmeuble, DateImpression, CodeEtat"
                            + " From DevisEnTete " + " WHERE     (CodeEtat = '04') AND (CodeImmeuble = '"
                            + StdSQLchaine.gFr_DoublerQuote(rsRow["CodeImmeuble"].ToString()) + "') "
                            + " AND (AdrEnvoi2 IN (SELECT     Table1.Adresse1 "
                            + " FROM          Table1 INNER JOIN " + " Immeuble ON Table1.Code1 = Immeuble.Code1 "
                            + " WHERE   Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(rsRow["CodeImmeuble"].ToString()) + "'))"
                            + " ORDER BY DateImpression DESC ";

                    rsT = tmpAdorsT.fc_OpenRecordSet(General.sSQL);
                    NewRowAddt = rsAddt.NewRow();
                    if (rsT.Rows.Count > 0)
                    {
                        foreach (DataRow rsTRow in rsT.Rows)
                        {

                            NewRowAddt["NoIntervention"] = lNointervention;
                            NewRowAddt["CodeImmeuble"] = rsRow["CodeImmeuble"];
                            NewRowAddt["Devis"] = 1;
                            NewRowAddt["numerodevis"] = rsTRow["numerodevis"];
                            NewRowAddt["TitreDevis"] = rsTRow["TitreDevis"];
                            NewRowAddt["DateDevis"] = rsTRow["DateImpression"];
                            NewRowAddt["CodeEtatDevis"] = "04";
                            rsAddt.Rows.Add(NewRowAddt.ItemArray);
                            int rr = tmpAdorsAddt.Update();
                            lNbDevis = lNbDevis + 1;

                        }
                    }

                }
                rsT.Dispose();


                //=== devis en 00.
                if (lNbDevis < 4)
                {

                    General.sSQL = "SELECT  TOP 4   NumeroDevis, TitreDevis, DateCreation, DateAcceptation, CodeImmeuble, DateImpression, CodeEtat"
                            + " From DevisEnTete " + " WHERE     (CodeEtat = '00') AND (CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(rsRow["CodeImmeuble"].ToString()) + "') "
                            + " AND (AdrEnvoi2 IN (SELECT     Table1.Adresse1 " + " FROM          Table1 INNER JOIN " + " Immeuble ON Table1.Code1 = Immeuble.Code1 "
                            + " WHERE   Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(rsRow["CodeImmeuble"].ToString()) + "'))" + " ORDER BY DateCreation DESC ";
                    rsT = tmpAdorsT.fc_OpenRecordSet(General.sSQL);
                    NewRowAddt = rsAddt.NewRow();
                    if (rsT.Rows.Count > 0)
                    {
                        foreach (DataRow rsTRow in rsT.Rows)
                        {
                            //rsAddt.AddNew();

                            NewRowAddt["NoIntervention"] = lNointervention;
                            NewRowAddt["CodeImmeuble"] = rsRow["CodeImmeuble"];
                            NewRowAddt["Devis"] = 1;
                            NewRowAddt["numerodevis"] = rsTRow["numerodevis"];
                            NewRowAddt["TitreDevis"] = rsTRow["TitreDevis"];
                            NewRowAddt["DateDevis"] = rsTRow["DateAcceptation"];
                            NewRowAddt["CodeEtatDevis"] = "00";
                            rsAddt.Rows.Add(NewRowAddt.ItemArray);
                            int rr = tmpAdorsAddt.Update();

                            lNbDevis = lNbDevis + 1;

                            // rsT.MoveNext();
                        }
                    }

                }
                rsT.Dispose();
                rsT = null;
                rsAddEntete.Clear();
                rsAddt.Clear();
                rsAdo.Clear();
                rsStd.Clear();

                //End If
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";FournisStandard");
            }
        }
        private void CreationPrestation()
        {
            bool boolAutorise = false;
            bool boolEOF = false;
            string SQL = null;
            string strDesignation = null;
            int longMsgb = 0;
            try
            {

                SQL = "UPDATE PreparationIntervention SET RESILIEE = 0 where Resiliee IS NULL";
                General.Execute(SQL);

                boolAutorise = false;
                boolEOF = false;
                boolEofBof = false;
                //adocnn.Execute "UPDATE Contrat INNER JOIN PreparationIntervention ON Contrat.CodeImmeuble = PreparationIntervention.Codeimmeuble SET PreparationIntervention.Resiliee = True WHERE Contrat.CodeArticle ='999'"
                rs = new DataTable();
                rsAdo = new DataTable();
                rsStd = new DataTable();
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                //--- stocke la fiche standard.
                SQL = "SELECT NumFicheStandard, Utilisateur, DateAjout," + " CodeImmeuble, CodeOrigine1, CodeOrigine2, Source,Intervention, AnalytiqueActivite "
                        + " FROM GestionStandard WHERE NumFicheStandard=0";
                //var tmpAdorsStd = new ModAdo();
                rsStd = tmpAdorsSTD.fc_OpenRecordSet(SQL);


                //---Recupere la designation de l'article PO1
                SQL = "SELECT Designation1 from facArticle where CodeArticle='PO1'";
                //var tmpAdors = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(SQL);
                strDesignation = rs.Rows[0]["Designation1"] + "";
                rs.Dispose();

                //----Récupére toutes les interventions a créer dans la table PreparationIntervention
                SQL = "SELECT PreparationIntervention.Codeimmeuble,PreparationIntervention.Commentaire, PreparationIntervention.CodeArticle "
                        + " From PreparationIntervention" + " WHERE PreparationIntervention.Intervenant ='" + txtTechnicien.Text + "'"
                        + " AND " + SSdbMois.Text + "=1 and PreparationIntervention.resiliee=0";
                SQL = SQL + " AND (Codeimmeuble IN ";
                SQL = SQL + "(SELECT Contrat.CodeImmeuble ";
                SQL = SQL + "FROM Contrat INNER JOIN ";
                SQL = SQL + "PreparationIntervention ON Contrat.CodeImmeuble = PreparationIntervention.CodeImmeuble ";
                SQL = SQL + "WHERE ((Contrat.DateFin >='01/"
                        + (SSdbMois.ActiveRow.Cells[1].Value.ToString() == "12" ? "01"
                        : Convert.ToString(Convert.ToInt16(SSdbMois.ActiveRow.Cells[1].Value) + 1)) + "/2004' OR Contrat.Resiliee = 1) AND ";
                SQL = SQL + "Contrat.Datefin IS NULL OR ";
                SQL = SQL + "Contrat.Resiliee = 0) AND PreparationIntervention.Intervenant = '" + txtTechnicien.Text + "'))";
                // var tmpAdo = new ModAdo();
                rs = tmpAdors.fc_OpenRecordSet(SQL);

                if (rs.Rows.Count > 0)
                {
                    //=== Vérifie si il éxiste déja des interventions.
                    SQL = "SELECT Intervention.NoIntervention,Intervention.CodeImmeuble, Intervention.Article," + " Intervention.Designation, Intervention.Commentaire,"
                            + " Intervention.Intervenant, Intervention.Dispatcheur," + " Intervention.DateSaisie, Intervention.NumFicheStandard,"
                            + " intervention.CodeEtat, intervention.VisiteEntretien, " + " intervention.VisiteEntretienAuto "
                            + " FROM Intervention" + " WHERE Intervention.DateSaisie >='"
                            + Convert.ToDateTime("01/" + RecupValeurInDbcombo() + "/" + txtAnnee.Text)
                            + "' and Intervention.DateSaisie <='" + Convert.ToDateTime(JourFinDeMois(RecupValeurInDbcombo(), txtAnnee.Text) + "/" + RecupValeurInDbcombo() + "/" + txtAnnee.Text)
                            + "' and Intervention.Intervenant='" + txtTechnicien.Text + "'" + " and Article = 'PO1'";
                    // var tmpAdorsAdo = new ModAdo();
                    rsAdo = tmprsAdo.fc_OpenRecordSet(SQL);


                    if (rsAdo.Rows.Count > 0)
                    {
                        boolEOF = false;
                    }
                    else
                    {
                        boolEOF = true;
                    }

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                    foreach (DataRow rsRow in rs.Rows)
                    {

                        if (boolEOF == false)
                        {
                            //rsAdo.Find("CodeImmeuble='" + rs.Fields("CodeImmeuble").Value + "'", , ADODB.SearchDirectionEnum.adSearchForward, ADODB.BookmarkEnum.adBookmarkFirst);
                            var r = rsAdo.Select("CodeImmeuble='" + rsRow["CodeImmeuble"] + "'");
                            //  var CodeImm = rsAdo.Select(rsRow["CodeImmeuble"].ToString());
                            if (r.Count() == 0)
                            {
                                if (boolAutorise == false)
                                {
                                    longMsgb = Convert.ToInt16(Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Desirez-vous créer des prestations ?", "Suppression", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question));
                                    if (longMsgb != 7)
                                    {
                                        boolAutorise = true;
                                    }
                                    else
                                    {
                                        break;
                                        boolEofBof = true;
                                    }
                                }


                                FournisStandard(rsRow);
                                var NewRow = rsAdo.NewRow();


                                NewRow["CodeImmeuble"] = rsRow["CodeImmeuble"];
                                NewRow["Numfichestandard"] = lNumFicheStandard;
                                if (General.sCodeArticleP2 == "1")
                                {
                                    NewRow["Article"] = rsRow["CodeArticle"];
                                    NewRow["Designation"] = tmprsAdo.fc_ADOlibelle("SELECT Designation1 from facArticle where CodeArticle='"
                                        + General.nz(rsRow["CodeArticle"], "PO1") + " '");
                                }
                                else
                                {
                                    NewRow["Article"] = "PO1";
                                    NewRow["Designation"] = strDesignation;
                                }
                                NewRow["Designation"] = strDesignation;
                                NewRow["Commentaire"] = "VISITE D'ENTRETIEN DU MOIS : " + SSdbMois.Text + "\n" + rsRow["Commentaire"] + "";
                                NewRow["Intervenant"] = txtTechnicien.Text;
                                NewRow["dispatcheur"] = "8";
                                NewRow["DateSaisie"] = ModP2v2.Date1JourSemaine("Dimanche", Convert.ToDateTime("01/" + RecupValeurInDbcombo() + "/" + txtAnnee.Text));
                                //                             rsAdo!datesaisie = Date
                                NewRow["CodeEtat"] = "00";
                                NewRow["VisiteEntretienAuto"] = 1;
                                NewRow["VisiteEntretien"] = 1;
                                rsAdo.Rows.Add(NewRow);
                                int yy = tmprsAdo.Update();

                            }

                        }
                        else if (boolEOF == true)
                        {
                            FournisStandard(rsRow);
                            var NewRow = rsAdo.NewRow();



                            NewRow["CodeImmeuble"] = rsRow["CodeImmeuble"];
                            NewRow["Numfichestandard"] = lNumFicheStandard;// rsStd.Rows[0]["Numfichestandard"];
                            if (General.sCodeArticleP2 == "1")
                            {
                                NewRow["Article"] = rsRow["CodeArticle"];
                                NewRow["Designation"] = tmpAdors.fc_ADOlibelle("SELECT Designation1 from facArticle where CodeArticle='"
                                    + General.nz(rsRow["CodeArticle"], "PO1") + " '");
                            }
                            else
                            {
                                NewRow["Article"] = "PO1";
                                NewRow["Designation"] = strDesignation;
                            }
                            NewRow["Commentaire"] = "VISITE D'ENTRETIEN DU MOIS : " + SSdbMois.Text + "\n" + rsRow["Commentaire"] + "";
                            NewRow["Intervenant"] = txtTechnicien.Text;
                            NewRow["dispatcheur"] = "8";
                            NewRow["DateSaisie"] = ModP2v2.Date1JourSemaine("Dimanche", Convert.ToDateTime("01/" + RecupValeurInDbcombo() + "/" + txtAnnee.Text));
                            //                                rsAdo!datesaisie = Date
                            NewRow["CodeEtat"] = "00";
                            NewRow["VisiteEntretienAuto"] = 1;
                            NewRow["VisiteEntretien"] = 1;
                            rsAdo.Rows.Add(NewRow);
                            int yy = tmprsAdo.Update();
                        }

                        // rs.MoveNext();
                        rsStd.Clear();
                    }


                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'a pas de prestations liées à ce dépanneur pour le mois de " + SSdbMois.Text, "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    boolEofBof = true;
                }
                /*if (rs.State == ADODB.ObjectStateEnum.adStateOpen)
                    rs.Close();
                if (rsAdo.State == ADODB.ObjectStateEnum.adStateOpen)
                    rsAdo.Close();*/

                if ((rs != null))
                    rs = null;

                if ((rsAdo != null))
                    rsAdo = null;
                rsStd.Dispose();
                rsStd = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CreationPrestation");
            }

        }
        private void FournisStandard(DataRow rsRow)
        {
            string AnalytiqueActivite = null;
            try
            {
                var tmpAdo = new ModAdo();
                //===@@@ modif du 29 05 2017, desactive Sage.
                if (General.sDesActiveSage == "1")
                {

                }
                else
                {
                    SAGE.fc_OpenConnSage();
                    DataTable dt = new DataTable();
                    SqlDataAdapter sda = new SqlDataAdapter("SELECT CA_Num FROM F_COMPTEA WHERE ActiviteContrat = '1'", SAGE.adoSage);
                    sda.Fill(dt);
                    AnalytiqueActivite = dt.Rows[0]["CA_Num"].ToString();
                    SAGE.fc_CloseConnSage();

                }

                var _with3 = rsStd;
                var NewRow = rsStd.NewRow();


                NewRow["Utilisateur"] = General.gsUtilisateur;
                NewRow["DateAjout"] = DateTime.Now;
                NewRow["CodeImmeuble"] = rsRow["CodeImmeuble"] + "";
                NewRow["CodeOrigine1"] = "6";
                NewRow["CodeOrigine2"] = "15";
                NewRow["Source"] = "Prestation";
                NewRow["AnalytiqueActivite"] = AnalytiqueActivite;
                NewRow["Intervention"] = "1";
                rsStd.Rows.Add(NewRow);
                int yy = tmpAdorsSTD.Update();
                using (var tmp = new ModAdo())
                    lNumFicheStandard = Convert.ToInt32(tmp.fc_ADOlibelle("select max(NumFicheStandard) from  GestionStandard"));

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";FournisStandard");

            }

        }


        private void Impression()
        {

            try
            {
                string SelectionFormula = "";
                ReportDocument CR = new ReportDocument();
                CR.Load(General.gsRpt + General.CHEMINETATPREST);
                //CR.ReportFileName = "C:\Base Lib-Net\Etats DT\" & CHEMINETATPREST
                //  CR.WindowShowPrintSetupBtn = true;
                if (General.sCodeArticleP2 == "1")
                {
                    SelectionFormula = "{Intervention.DateSaisie}>=" + Convert.ToDateTime("01/" + RecupValeurInDbcombo() + "/" + txtAnnee.Text).ToString("yyyy,MM,dd") + "'"
                          + " and {Intervention.DateSaisie}<=" + Convert.ToDateTime(JourFinDeMois(RecupValeurInDbcombo(), txtAnnee.Text)
                          + "/" + RecupValeurInDbcombo() + "/" + txtAnnee.Text).ToString("yyyy,MM,dd") + "'"
                          + " and {Intervention.Intervenant}='" + txtTechnicien.Text + "'" + " and {Intervention.VisiteEntretien}= 1 ";
                }
                else
                {

                    SelectionFormula = "{Intervention.DateSaisie}>=dateTime(" + Convert.ToDateTime("01 /" + RecupValeurInDbcombo() + "/" + txtAnnee.Text).ToString("yyyy,MM,dd") + ")"
                           + " and {Intervention.DateSaisie}<=dateTime(" + Convert.ToDateTime(JourFinDeMois(RecupValeurInDbcombo(), txtAnnee.Text) + "/" + RecupValeurInDbcombo() + "/" + txtAnnee.Text).ToString("yyyy,MM,dd") + ")"
                           + " and {Intervention.Intervenant}='" + txtTechnicien.Text + "'" + " and {Intervention.Article}='PO1' ";
                }
                // CR.SelectionFormula = "{PreparationIntervention.Intervenant}='" & txtTechnicien.Text & "' and {PreparationIntervention." & SSdbMois.Text & "}= 1"
               
                CR.DataDefinition.FormulaFields["Mois"].Text = "'" + SSdbMois.Text + "'" ;                       
                var FormCR = new CrystalReportFormView(CR, SelectionFormula);
                FormCR.Show();
                SelectionFormula = "";
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Impression");

            }

        }

        private string RecupValeurInDbcombo()
        {
            string functionReturnValue = null;
            int i = 0;
            try
            {
                // SSdbMois.MoveFirst();
                for (i = 0; i <= SSdbMois.Rows.Count; i++)
                {
                    if (SSdbMois.ActiveRow.Cells["Mois"].Value.ToString().ToUpper() == SSdbMois.Text.ToUpper())
                    {
                        functionReturnValue = SSdbMois.ActiveRow.Cells["Num"].Value.ToString();
                        return functionReturnValue;
                    }
                    // SSdbMois.MoveNext();
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";RecupValeurInDbcombo");
                return functionReturnValue;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdRechercheTech_Click(object sender, EventArgs e)
        {
            try
            {
                string req = "";
                string where = "";
                req = "SELECT Personnel.Matricule AS \"Matricule\", Personnel.Nom AS \"Nom\", Personnel.NumRadio AS \"No Téléphone\" From Personnel";
                SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche du technicien" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtTechnicien.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fg.Dispose(); fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtTechnicien.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fg.Dispose(); fg.Close();
                    }

                };
                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdRechercheTech_Click");
            }

        }


        public string JourFinDeMois(string strMois, string strAnnee)
        {
            string functionReturnValue = null;
            try
            {

                //----on envoie en paramatre un mois , une annee, celle-çi retournera
                //----le dernier jour de ce mois.
                //----Exemple: JourFinDeMois ("02", "2000") renvoie 29.
                int longJour = 0;
                string strDate = null;
                longJour = 31;
                while (longJour != 0)
                {
                    strDate = longJour + "/" + strMois + "/" + strAnnee;
                    if (General.IsDate(strDate))
                    {
                        functionReturnValue = Convert.ToString(Convert.ToDateTime(strDate).Day);
                        break;
                    }
                    longJour = longJour - 1;
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";JourFinDeMois");
                return functionReturnValue;
            }

        }

        private void frmPrestations_Load(object sender, EventArgs e)
        {
            DataTable SSdbMoisSource = new DataTable();
            SSdbMoisSource.Columns.Add("Mois");
            SSdbMoisSource.Columns.Add("Num");

            SSdbMoisSource.Rows.Add("Janvier", "01");
            SSdbMoisSource.Rows.Add("Fevrier", "02");
            SSdbMoisSource.Rows.Add("Mars", "03");
            SSdbMoisSource.Rows.Add("Avril", "04");
            SSdbMoisSource.Rows.Add("Mai", "05");
            SSdbMoisSource.Rows.Add("Juin", "06");
            SSdbMoisSource.Rows.Add("Juillet", "07");
            SSdbMoisSource.Rows.Add("Aout", "08");
            SSdbMoisSource.Rows.Add("Septembre", "09");
            SSdbMoisSource.Rows.Add("Octobre", "10");
            SSdbMoisSource.Rows.Add("Novembre", "11");
            SSdbMoisSource.Rows.Add("Decembre", "12");
            SSdbMois.DataSource = SSdbMoisSource;

        }
    }
}
