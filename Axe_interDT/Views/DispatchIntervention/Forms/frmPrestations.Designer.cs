﻿namespace Axe_interDT.Views.DispatchDevis.Forms
{
    partial class frmPrestations
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.txtAnnee = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.SSdbMois = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label2 = new System.Windows.Forms.Label();
            this.txtTechnicien = new iTalk.iTalk_TextBox_Small2();
            this.CmdRechercheTech = new System.Windows.Forms.Button();
            this.cmdLancer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SSdbMois)).BeginInit();
            this.SuspendLayout();
            // 
            // txtAnnee
            // 
            this.txtAnnee.AccAcceptNumbersOnly = false;
            this.txtAnnee.AccAllowComma = false;
            this.txtAnnee.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAnnee.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAnnee.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtAnnee.AccHidenValue = "";
            this.txtAnnee.AccNotAllowedChars = null;
            this.txtAnnee.AccReadOnly = false;
            this.txtAnnee.AccReadOnlyAllowDelete = false;
            this.txtAnnee.AccRequired = false;
            this.txtAnnee.BackColor = System.Drawing.Color.White;
            this.txtAnnee.CustomBackColor = System.Drawing.Color.White;
            this.txtAnnee.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtAnnee.ForeColor = System.Drawing.Color.Black;
            this.txtAnnee.Location = new System.Drawing.Point(64, 83);
            this.txtAnnee.Margin = new System.Windows.Forms.Padding(2);
            this.txtAnnee.MaxLength = 32767;
            this.txtAnnee.Multiline = false;
            this.txtAnnee.Name = "txtAnnee";
            this.txtAnnee.ReadOnly = false;
            this.txtAnnee.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAnnee.Size = new System.Drawing.Size(94, 27);
            this.txtAnnee.TabIndex = 0;
            this.txtAnnee.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAnnee.UseSystemPasswordChar = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(12, 83);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(54, 19);
            this.label33.TabIndex = 502;
            this.label33.Text = "Année";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(180, 83);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(41, 19);
            this.label1.TabIndex = 504;
            this.label1.Text = "Mois";
            // 
            // SSdbMois
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSdbMois.DisplayLayout.Appearance = appearance1;
            this.SSdbMois.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSdbMois.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.SSdbMois.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSdbMois.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.SSdbMois.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSdbMois.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.SSdbMois.DisplayLayout.MaxColScrollRegions = 1;
            this.SSdbMois.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSdbMois.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSdbMois.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.SSdbMois.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSdbMois.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.SSdbMois.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSdbMois.DisplayLayout.Override.CellAppearance = appearance8;
            this.SSdbMois.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSdbMois.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.SSdbMois.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.SSdbMois.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.SSdbMois.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSdbMois.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.SSdbMois.DisplayLayout.Override.RowAppearance = appearance11;
            this.SSdbMois.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSdbMois.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.SSdbMois.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSdbMois.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSdbMois.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSdbMois.Location = new System.Drawing.Point(222, 83);
            this.SSdbMois.Name = "SSdbMois";
            this.SSdbMois.Size = new System.Drawing.Size(94, 22);
            this.SSdbMois.TabIndex = 1;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(333, 84);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(84, 19);
            this.label2.TabIndex = 506;
            this.label2.Text = "Technicien";
            // 
            // txtTechnicien
            // 
            this.txtTechnicien.AccAcceptNumbersOnly = false;
            this.txtTechnicien.AccAllowComma = false;
            this.txtTechnicien.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTechnicien.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTechnicien.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTechnicien.AccHidenValue = "";
            this.txtTechnicien.AccNotAllowedChars = null;
            this.txtTechnicien.AccReadOnly = false;
            this.txtTechnicien.AccReadOnlyAllowDelete = false;
            this.txtTechnicien.AccRequired = false;
            this.txtTechnicien.BackColor = System.Drawing.Color.White;
            this.txtTechnicien.CustomBackColor = System.Drawing.Color.White;
            this.txtTechnicien.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtTechnicien.ForeColor = System.Drawing.Color.Black;
            this.txtTechnicien.Location = new System.Drawing.Point(422, 83);
            this.txtTechnicien.Margin = new System.Windows.Forms.Padding(2);
            this.txtTechnicien.MaxLength = 32767;
            this.txtTechnicien.Multiline = false;
            this.txtTechnicien.Name = "txtTechnicien";
            this.txtTechnicien.ReadOnly = false;
            this.txtTechnicien.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTechnicien.Size = new System.Drawing.Size(94, 27);
            this.txtTechnicien.TabIndex = 2;
            this.txtTechnicien.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTechnicien.UseSystemPasswordChar = false;
            // 
            // CmdRechercheTech
            // 
            this.CmdRechercheTech.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdRechercheTech.FlatAppearance.BorderSize = 0;
            this.CmdRechercheTech.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdRechercheTech.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.CmdRechercheTech.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.CmdRechercheTech.Location = new System.Drawing.Point(518, 83);
            this.CmdRechercheTech.Margin = new System.Windows.Forms.Padding(0, 3, 0, 0);
            this.CmdRechercheTech.Name = "CmdRechercheTech";
            this.CmdRechercheTech.Size = new System.Drawing.Size(25, 20);
            this.CmdRechercheTech.TabIndex = 508;
            this.CmdRechercheTech.UseVisualStyleBackColor = false;
            this.CmdRechercheTech.Click += new System.EventHandler(this.CmdRechercheTech_Click);
            // 
            // cmdLancer
            // 
            this.cmdLancer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdLancer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdLancer.FlatAppearance.BorderSize = 0;
            this.cmdLancer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdLancer.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.cmdLancer.ForeColor = System.Drawing.Color.White;
            this.cmdLancer.Location = new System.Drawing.Point(511, 11);
            this.cmdLancer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdLancer.Name = "cmdLancer";
            this.cmdLancer.Size = new System.Drawing.Size(86, 42);
            this.cmdLancer.TabIndex = 509;
            this.cmdLancer.Text = "Lancer";
            this.cmdLancer.UseVisualStyleBackColor = false;
            this.cmdLancer.Click += new System.EventHandler(this.cmdLancer_Click);
            // 
            // frmPrestations
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(608, 153);
            this.Controls.Add(this.cmdLancer);
            this.Controls.Add(this.CmdRechercheTech);
            this.Controls.Add(this.txtTechnicien);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.SSdbMois);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtAnnee);
            this.Controls.Add(this.label33);
            this.MaximumSize = new System.Drawing.Size(624, 192);
            this.MinimumSize = new System.Drawing.Size(624, 192);
            this.Name = "frmPrestations";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmPrestations";
            this.Load += new System.EventHandler(this.frmPrestations_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SSdbMois)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public iTalk.iTalk_TextBox_Small2 txtAnnee;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        public Infragistics.Win.UltraWinGrid.UltraCombo SSdbMois;
        public System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtTechnicien;
        public System.Windows.Forms.Button CmdRechercheTech;
        public System.Windows.Forms.Button cmdLancer;
    }
}