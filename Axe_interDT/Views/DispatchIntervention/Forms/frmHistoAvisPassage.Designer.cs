﻿namespace Axe_interDT.Views.DispatchIntervention.Forms
{
    partial class frmHistoAvisPassage
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.txtCodeimmeuble = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtNoIntervention = new iTalk.iTalk_TextBox_Small2();
            this.GridHisto = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.GridHisto)).BeginInit();
            this.SuspendLayout();
            // 
            // txtCodeimmeuble
            // 
            this.txtCodeimmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeimmeuble.AccAllowComma = false;
            this.txtCodeimmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeimmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeimmeuble.AccHidenValue = "";
            this.txtCodeimmeuble.AccNotAllowedChars = null;
            this.txtCodeimmeuble.AccReadOnly = false;
            this.txtCodeimmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeimmeuble.AccRequired = false;
            this.txtCodeimmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeimmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeimmeuble.Location = new System.Drawing.Point(145, 57);
            this.txtCodeimmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeimmeuble.MaxLength = 32767;
            this.txtCodeimmeuble.Multiline = false;
            this.txtCodeimmeuble.Name = "txtCodeimmeuble";
            this.txtCodeimmeuble.ReadOnly = false;
            this.txtCodeimmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeimmeuble.Size = new System.Drawing.Size(258, 27);
            this.txtCodeimmeuble.TabIndex = 1;
            this.txtCodeimmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeimmeuble.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(22, 24);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(113, 19);
            this.label3.TabIndex = 511;
            this.label3.Text = "N° intervention";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(22, 57);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 19);
            this.label2.TabIndex = 510;
            this.label2.Text = "Code immeuble";
            // 
            // txtNoIntervention
            // 
            this.txtNoIntervention.AccAcceptNumbersOnly = false;
            this.txtNoIntervention.AccAllowComma = false;
            this.txtNoIntervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoIntervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoIntervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoIntervention.AccHidenValue = "";
            this.txtNoIntervention.AccNotAllowedChars = null;
            this.txtNoIntervention.AccReadOnly = false;
            this.txtNoIntervention.AccReadOnlyAllowDelete = false;
            this.txtNoIntervention.AccRequired = false;
            this.txtNoIntervention.BackColor = System.Drawing.Color.White;
            this.txtNoIntervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNoIntervention.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoIntervention.ForeColor = System.Drawing.Color.Black;
            this.txtNoIntervention.Location = new System.Drawing.Point(145, 24);
            this.txtNoIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoIntervention.MaxLength = 32767;
            this.txtNoIntervention.Multiline = false;
            this.txtNoIntervention.Name = "txtNoIntervention";
            this.txtNoIntervention.ReadOnly = false;
            this.txtNoIntervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoIntervention.Size = new System.Drawing.Size(258, 27);
            this.txtNoIntervention.TabIndex = 0;
            this.txtNoIntervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoIntervention.UseSystemPasswordChar = false;
            // 
            // GridHisto
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridHisto.DisplayLayout.Appearance = appearance1;
            this.GridHisto.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridHisto.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridHisto.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridHisto.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridHisto.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridHisto.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridHisto.DisplayLayout.MaxColScrollRegions = 1;
            this.GridHisto.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridHisto.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridHisto.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridHisto.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridHisto.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridHisto.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridHisto.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridHisto.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridHisto.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridHisto.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridHisto.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridHisto.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridHisto.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridHisto.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridHisto.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridHisto.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridHisto.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridHisto.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridHisto.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridHisto.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.GridHisto.Location = new System.Drawing.Point(12, 104);
            this.GridHisto.Name = "GridHisto";
            this.GridHisto.Size = new System.Drawing.Size(630, 269);
            this.GridHisto.TabIndex = 508;
            this.GridHisto.Text = "ultraGrid1";
            this.GridHisto.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridHisto_InitializeLayout);
            // 
            // frmHistoAvisPassage
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(654, 384);
            this.Controls.Add(this.txtCodeimmeuble);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtNoIntervention);
            this.Controls.Add(this.GridHisto);
            this.MaximumSize = new System.Drawing.Size(670, 423);
            this.MinimumSize = new System.Drawing.Size(670, 423);
            this.Name = "frmHistoAvisPassage";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmHistoAvisPassage";
            this.Load += new System.EventHandler(this.frmHistoAvisPassage_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridHisto)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }


        public iTalk.iTalk_TextBox_Small2 txtCodeimmeuble;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtNoIntervention;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridHisto;
    }
}