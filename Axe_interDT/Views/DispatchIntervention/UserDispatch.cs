﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.DispatchIntervention.Forms;
using Axe_interDT.Views.DispatchIntervention.HistoriqueIntervention;
using Axe_interDT.Views.Intervention;
using Axe_interDT.Views.SharedViews;
using Axe_interDT.Views.Theme.CustomMessageBox;
using CrystalDecisions.CrystalReports.Engine;
using Infragistics.Win;
using Infragistics.Win.UltraWinGrid;
using Infragistics.Win.UltraWinSchedule;
using System;
using System.Collections;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Windows.Forms;
using Excel = Microsoft.Office.Interop.Excel;

namespace Axe_interDT.Views.DispatchIntervention
{
    public partial class UserDispatch : UserControl
    {
        DataTable rsintervention;
        ModAdo modAdorsintervention;
        public short IndexDate;
        public System.Windows.Forms.TextBox champDate;
        public UltraCalendarCombo displayedEditorForDateTime;
        bool bBorderau;

        object ControleSurFeuille;

        int longMsgb;
        int Index;
        int longSaveNoIntervention;

        bool boolVisible;
        bool boolEofBof;
        bool boolErrDecele;
        bool boolHead;
        bool boolsens;

        string strColumn;
        string BisStrRequete;
        string strVilleImmeuble;
        string strAdresseImmeuble;
        string strCode;
        string[] strTabParam = new string[4];
        string strOptionChoisi;
        string strRequete;
        string strSql;
        string strDate;
        string SQLbis;
        bool boolBordereauJournee;
        bool boolModifRow;
        int lngBookMark;
        bool boolStopBookmark;
        bool boolEtatFactInterv;
        bool blnKeyPress = false;
        string sOrderBy;
        string sAscDesc;
        private const string cWhere = "Where";
        bool bBordereauJ = false;
        bool bMajStatut = false;
        bool bExportDetail = false;
        public UserDispatch()
        {
            InitializeComponent();
            displayedEditorForDateTime = new UltraCalendarCombo();

            setStyleToEditor();
        }

        void setStyleToEditor()
        {
            Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton dateButton = new Infragistics.Win.UltraWinSchedule.CalendarCombo.DateButton();
            this.displayedEditorForDateTime.Appearance = this.ultraDateTimeEditor3.Appearance;
            this.displayedEditorForDateTime.AutoSelectionUpdate = true;
            this.displayedEditorForDateTime.AutoSize = false;
            this.displayedEditorForDateTime.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.displayedEditorForDateTime.BorderStyleCalendar = Infragistics.Win.UIElementBorderStyle.Solid;
            this.displayedEditorForDateTime.BorderStyleDay = Infragistics.Win.UIElementBorderStyle.Rounded4;
            this.displayedEditorForDateTime.DateButtonAppearance = this.ultraDateTimeEditor3.DateButtonAppearance;
            this.displayedEditorForDateTime.DateButtonAreaAppearance = this.ultraDateTimeEditor3.DateButtonAreaAppearance;
            this.displayedEditorForDateTime.DateButtons.Add(dateButton);
            this.displayedEditorForDateTime.DayOfWeekCaptionStyle = Infragistics.Win.UltraWinSchedule.DayOfWeekCaptionStyle.ShortDescription;
            this.displayedEditorForDateTime.DropDownAppearance = this.ultraDateTimeEditor3.DropDownAppearance;
            this.displayedEditorForDateTime.MonthOrientation = Infragistics.Win.UltraWinSchedule.MonthOrientation.DownThenAcross;
            this.displayedEditorForDateTime.MonthPopupAppearance = this.ultraDateTimeEditor3.MonthPopupAppearance;
            this.displayedEditorForDateTime.ScrollButtonAppearance = this.ultraDateTimeEditor3.ScrollButtonAppearance;
            this.displayedEditorForDateTime.AllowNull = true;
            this.displayedEditorForDateTime.Value = "";
            this.displayedEditorForDateTime.WeekNumbersVisible = true;
            this.displayedEditorForDateTime.YearScrollButtonAppearance = this.ultraDateTimeEditor3.YearScrollButtonAppearance;
            this.displayedEditorForDateTime.YearScrollButtonsVisible = Infragistics.Win.DefaultableBoolean.True;
            this.displayedEditorForDateTime.OverrideFontSettings = true;
            this.displayedEditorForDateTime.NullDateLabel = "";
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbArticle_TextChanged(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibTypeOperation.Text = tmpAdo.fc_ADOlibelle("SELECT FacArticle.Designation1 "
                    + " FROM FacArticle WHERE FacArticle.CodeArticle='" + StdSQLchaine.gFr_DoublerQuote(cmbArticle.Text) + "'");
            }
        }

        private void cmbArticle_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibTypeOperation.Text = tmpAdo.fc_ADOlibelle("SELECT FacArticle.Designation1 "
                    + " FROM FacArticle WHERE FacArticle.CodeArticle='" + StdSQLchaine.gFr_DoublerQuote(cmbArticle.Text) + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbArticle_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT CodeArticle , Designation1 "
                + " From FacArticle"
                + " WHERE Masquer IS NULL OR Masquer =   " + General.fc_bln(false)
                + " order by codearticle";
            if (cmbArticle.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(cmbArticle, General.sSQL, "CodeArticle");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    using (var tmpModAdo = new ModAdo())
                        if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT FacArticle.Designation1 "
                            + " FROM FacArticle WHERE FacArticle.CodeArticle='" + StdSQLchaine.gFr_DoublerQuote(cmbArticle.Text) + "'"), "").ToString()))
                        {
                            requete = "SELECT CodeArticle as \"Type d'opération\" , Designation1 as \"Designation\" From FacArticle";
                            where = " Masquer IS NULL OR Masquer = " + General.fc_bln(false);
                            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'un type d'opération" };

                            fg.SetValues(new Dictionary<string, string> { { "CodeArticle", cmbArticle.Text } });

                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                cmbArticle.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                fg.Dispose(); fg.Close();
                            };
                            fg.ugResultat.KeyDown += (se, ev) =>
                            {
                                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                {
                                    cmbArticle.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    fg.Dispose(); fg.Close();
                                }

                            };
                            fg.StartPosition = FormStartPosition.CenterScreen;
                            fg.ShowDialog();

                        }
                    //        cmbIntervention_Click
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbArticle_KeyPress");
                return;

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeGerant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (cmbCodeGerant.Rows.Count < 1)
            {
                General.sSQL = "SELECT Table1.Code1 AS [Code Client], Table1.Adresse1,"
                    + " Table1.CodePostal, Table1.Ville" + " From Table1" + " ORDER BY Code1";
                sheridan.InitialiseCombo(this.cmbCodeGerant, General.sSQL, "Code Client");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeGerant_KeyPress(object sender, KeyPressEventArgs e)
        {
            DataTable rsGerant = default(DataTable);
            bool blnGoRecherche = false;
            short KeyAscii = (short)e.KeyChar;
            try
            {
                blnGoRecherche = false;
                if (KeyAscii == 13)
                {
                    rsGerant = new DataTable();
                    var tmpAdorsGerant = new ModAdo();
                    rsGerant = tmpAdorsGerant.fc_OpenRecordSet("SELECT Table1.Code1 FROM Table1 WHERE Table1.Code1 LIKE '%" + cmbCodeGerant.Value + "%'");
                    if (rsGerant.Rows.Count > 1)
                    {
                        blnGoRecherche = true;
                    }
                    else
                    {
                        blnGoRecherche = true;
                    }

                    if (blnGoRecherche == true)
                    {
                        string requete = "SELECT Code1 as \"Code Gérant\"," + " Adresse1 as \"Adresse\"," + " Ville as \"Ville\" , CodePostal AS \"Code Postal\" FROM Table1 ";
                        string where = "";
                        SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'un gérant" };
                        fg.SetValues(new Dictionary<string, string> { { "Code1", cmbCodeGerant.Text } });

                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbCodeGerant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fg.Dispose(); fg.Close();
                        };
                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                cmbCodeGerant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                fg.Dispose(); fg.Close();
                            }

                        };
                        fg.StartPosition = FormStartPosition.CenterScreen;
                        fg.ShowDialog();
                    }
                    else
                    {
                        cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                    }
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                    rsGerant.Dispose();
                    rsGerant = null;
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbCodeGerant_KeyPress");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeimmeuble_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (cmbCodeimmeuble.Rows.Count < 1)
            {
                General.sSQL = "SELECT Immeuble.CodeImmeuble, Immeuble.Adresse," + " Immeuble.CodePostal, Immeuble.Ville" + " From Immeuble" + " order by CodeImmeuble";
                sheridan.InitialiseCombo(this.cmbCodeimmeuble, General.sSQL, "codeImmeuble");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeimmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        //===> Mondir le 18.06.2021 https://groupe-dt.mantishub.io/view.php?id=2216
                        var count = tmpModAdo.fc_ADOlibelle("SELECT COUNT(*) FROM Immeuble WHERE Immeuble.CodeImmeuble LIKE '%" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) + "%'").ToInt();
                        //===> Fin Modif Mondir

                        //===> Commented lines are old
                        //if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle(
                        //            "SELECT Immeuble.CodeImmeuble FROM Immeuble WHERE Immeuble.CodeImmeuble='" +
                        //            StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) + "'"), "").ToString()))
                        //if (count > 1 || count == 0) ===> Old
                        //===> Mondir le 24.06.2021 https://groupe-dt.mantishub.io/view.php?id=2509
                        if (count == 1 && !string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Immeuble.CodeImmeuble FROM Immeuble WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) + "'"), "").ToString()))
                        {

                            cmbIntervention_Click(cmbIntervention, new System.EventArgs());

                        }
                        else
                        {
                            requete = " SELECT CodeImmeuble as \"Code Immeuble\", Adresse as \"Adresse\","
                                      + " Ville as \"Ville\" , anglerue as \"Angle de rue\", Code1 as \"Gerant\" FROM Immeuble ";
                            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "")
                            { Text = "Recherche d'une immeuble" };
                            fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", cmbCodeimmeuble.Text } });

                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                cmbCodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                fg.Dispose();
                                fg.Close();
                            };
                            fg.ugResultat.KeyDown += (se, ev) =>
                            {
                                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                {
                                    cmbCodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    fg.Dispose();
                                    fg.Close();
                                }

                            };
                            fg.StartPosition = FormStartPosition.CenterScreen;
                            fg.ShowDialog();
                        }
                    }

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdrecherche_Click");
            }

        }
        /// <summary>
        /// Testd
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_TextChanged(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibCommercial.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM"
                + " Personnel WHERE Personnel.Matricule='" + StdSQLchaine.gFr_DoublerQuote(cmbCommercial.Text) + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibCommercial.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM"
                + " Personnel WHERE Personnel.Matricule='" + StdSQLchaine.gFr_DoublerQuote(cmbCommercial.Text) + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_BeforeDropDown(object sender, CancelEventArgs e)
        {
            int i = 0;
            string req = null;
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom,"
                + " Qualification.CodeQualif, Qualification.Qualification"
                + " FROM Personnel INNER JOIN"
                + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif";
            req = "";

            if (General.CodeQualifCommercial.Length > 0)
            {
                req = req + " WHERE (";
                for (i = 1; i <= General.CodeQualifCommercial.Length - 1; i++)
                {

                    if (i < General.CodeQualifCommercial.Length - 1)
                    {

                        req = req + "Personnel.CodeQualif='" + General.CodeQualifCommercial[i] + "' OR ";
                    }
                    else
                    {

                        req = req + "Personnel.CodeQualif='" + General.CodeQualifCommercial[i] + "'";
                    }
                }
                req = req + ")";
            }

            if (string.IsNullOrEmpty(req))
            {

                req = " WHERE (Personnel.NonActif is null or Personnel.NonActif = 0) ";
            }
            else
            {
                req = req + " AND (Personnel.NonActif is null or Personnel.NonActif = 0)";
            }

            General.sSQL = General.sSQL + req + " ORDER BY Personnel.Nom";
            sheridan.InitialiseCombo(cmbCommercial, General.sSQL, "Matricule");
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCommercial_KeyPress(object sender, KeyPressEventArgs e)

        {
            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    using (var tmpModAdo = new ModAdo())
                        if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + StdSQLchaine.gFr_DoublerQuote(cmbCommercial.Text) + "'"), "").ToString()))
                        {
                            requete = "SELECT Personnel.Matricule as \"Matricule\","
                                + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\","
                                + " Qualification.Qualification as \"Qualification\","
                                + " Personnel.Memoguard as \"MemoGuard\","
                                + " Personnel.NumRadio as \"NumRadio\" "
                                + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                            where = " (NonActif is null or NonActif = 0) ";
                            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'une immeuble" };
                            if (General.IsNumeric(cmbCommercial.Text))
                            {
                                fg.SetValues(new Dictionary<string, string> { { "Matricule", cmbCommercial.Text } });
                            }
                            else
                            {
                                fg.SetValues(new Dictionary<string, string> { { "Nom", cmbCommercial.Text } });
                            }

                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                cmbCommercial.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                lblLibCommercial.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                                fg.Dispose(); fg.Close();
                            };
                            fg.ugResultat.KeyDown += (se, ev) =>
                            {
                                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                {
                                    cmbCommercial.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    lblLibCommercial.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                                    fg.Dispose(); fg.Close();
                                }

                            };
                            fg.StartPosition = FormStartPosition.CenterScreen;
                            fg.ShowDialog();

                        }

                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbCommercial_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbDepan_TextChanged(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lbllibDepanneur.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + StdSQLchaine.gFr_DoublerQuote(cmbDepan.Text) + "'");
            }
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbDepan_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lbllibDepanneur.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + StdSQLchaine.gFr_DoublerQuote(cmbDepan.Text) + "'");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbDepan_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;
            sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom,"
                + " Qualification.CodeQualif, Qualification.Qualification"
                + " FROM Personnel INNER JOIN"
                + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif";

            sSQL = sSQL + " WHERE (NonActif is null or NonActif = 0) ";
            sSQL = sSQL + " order by matricule";

            sheridan.InitialiseCombo(cmbDepan, sSQL, "Matricule");
        }
        /// <summary>
        /// le where de searchtemplate ne marche pas
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbDepan_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    using (var tmpModAdo = new ModAdo())
                        if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + StdSQLchaine.gFr_DoublerQuote(cmbDepan.Text) + "'"), "").ToString()))
                        {

                            requete = "SELECT Personnel.Matricule as \"Matricule\","
                                + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\","
                                + " Qualification.Qualification as \"Qualification\","
                                + " Personnel.Memoguard as \"MemoGuard\","
                                + " Personnel.NumRadio as \"NumRadio\" "
                                + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                            where = "  (NonActif is null or NonActif = 0) ";
                            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'une immeuble" };
                            if (General.IsNumeric(cmbIntervenant.Text))
                            {
                                fg.SetValues(new Dictionary<string, string> { { "Matricule", cmbDepan.Text } });
                            }
                            else
                            {
                                fg.SetValues(new Dictionary<string, string> { { "Nom", cmbDepan.Text } });
                            }

                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                cmbDepan.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                lbllibDepanneur.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                                fg.Dispose(); fg.Close();
                            };
                            fg.ugResultat.KeyDown += (se, ev) =>
                            {
                                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                {
                                    cmbDepan.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    lbllibDepanneur.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                                    fg.Dispose(); fg.Close();
                                }

                            };
                            fg.StartPosition = FormStartPosition.CenterScreen;
                            fg.ShowDialog();

                        }

                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbDepan_Click");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbEnergie_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;
            sSQL = "SELECT Code,Libelle FROM Energie ORDER BY CODE";
            if (cmbEnergie.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(cmbEnergie, sSQL, "Code");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbEnergie_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    requete = "SELECT Code as \"Code\" , Libelle as \"Libelle\"  FROM Energie ";
                    SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'un code enregie" };
                    fg.SetValues(new Dictionary<string, string> { { "Code", cmbEnergie.Text } });

                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        cmbEnergie.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fg.Dispose(); fg.Close();
                    };
                    fg.ugResultat.KeyDown += (se, ev) =>
                    {
                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            cmbEnergie.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fg.Dispose(); fg.Close();
                        }

                    };
                    fg.StartPosition = FormStartPosition.CenterScreen;
                    fg.ShowDialog();

                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbDepan_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbEtatDevis_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "";
            //    sSql = "Select sec_Code, Sec_Libelle from SEC_Secteur "
            General.sSQL = "Select DevisCodeEtat.Code,DevisCodeEtat.Libelle FROM DevisCodeEtat ";
            sheridan.InitialiseCombo(cmbEtatDevis, General.sSQL, "Code");
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_TextChanged(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibIntervenant.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" +
                                                              StdSQLchaine.gFr_DoublerQuote(cmbIntervenant.Text) + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibIntervenant.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" +
                                                              StdSQLchaine.gFr_DoublerQuote(cmbIntervenant.Text) + "'");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom,"
                + " Qualification.CodeQualif, Qualification.Qualification"
                + " FROM Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif";
            General.sSQL = General.sSQL + " WHERE (NonActif is null or NonActif = 0) ";
            General.sSQL = General.sSQL + " order by matricule";
            sheridan.InitialiseCombo(cmbIntervenant, General.sSQL, "Matricule");
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    using (var tmpModAdo = new ModAdo())
                        if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + StdSQLchaine.gFr_DoublerQuote(cmbIntervenant.Text) + "'"), "").ToString()))
                        {

                            requete = "SELECT Personnel.Matricule as \"Matricule\","
                                + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\","
                                + " Qualification.Qualification as \"Qualification\","
                                + " Personnel.Memoguard as \"MemoGuard\","
                                + " Personnel.NumRadio as \"NumRadio\" "
                                + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                            where = "  (NonActif is null or NonActif = 0) ";
                            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'une intervenant" };

                            if (General.IsNumeric(cmbIntervenant.Text))
                            {
                                fg.SetValues(new Dictionary<string, string> { { "Matricule", cmbIntervenant.Text } });
                            }
                            else
                            {
                                fg.SetValues(new Dictionary<string, string> { { "Nom", cmbIntervenant.Text } });
                            }

                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                cmbIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                lblLibIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                                fg.Dispose(); fg.Close();
                            };
                            fg.ugResultat.KeyDown += (se, ev) =>
                            {
                                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                {

                                    cmbIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    lblLibIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                                    fg.Dispose(); fg.Close();
                                }

                            };
                            fg.StartPosition = FormStartPosition.CenterScreen;
                            fg.ShowDialog();

                        }
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervenant_Click");
            }
        }

        private void changeSearchButtonState(bool enabled)
        {
            cmbIntervention.Enabled = enabled;
            cmdBordereauJournee.Enabled = enabled;
        }

        private void cmbIntervention_Click(object sender, EventArgs e)
        {
            changeSearchButtonState(false);
            boolBordereauJournee = false;
            fc_loadIntervention();
            changeSearchButtonState(true);
        }

        private void fc_loadIntervention()
        {
            //===> Mondir 22.01.2021, Ajout du LOG
            Program.SaveException(null, $"fc_loadIntervention() - Enter In The Function");
            //===> Fin Modif Mondir

            var tmpAdo = new ModAdo();
            string sWhere = "";
            string sOrder = "";
            string sInitale = "";
            string sComentaire;
            string sSQLInitia;

            ssIntervention.DataSource = null;

            try
            {
                if (!string.IsNullOrEmpty(txtDateSaisieFin.Text))//tested
                {
                    if (txtDateSaisieFin.Text.Contains(".") || !General.IsDate(txtDateSaisieFin.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtDateSaisieFin.Focus();
                        //txtDateSaisieFin.SelectionStart = 0;
                        //txtDateSaisieFin.SelectionLength = txtDateSaisieFin.Text.Length;
                        return;
                    }
                    else//tested
                    {
                        txtDateSaisieFin.Text = Convert.ToDateTime(txtDateSaisieFin.Text).ToShortDateString();
                        if (Convert.ToDateTime(txtDateSaisieFin.Text) < Convert.ToDateTime("01/01/1900"))//tested
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDateSaisieFin.Focus();
                            //txtDateSaisieFin.SelectionStart = 0;
                            //txtDateSaisieFin.SelectionLength = txtDateSaisieFin.Text.Length;
                            return;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(txtDateSaisieDe.Text))//tested
                {
                    if (txtDateSaisieDe.Text.Contains(".") || !General.IsDate(txtDateSaisieDe.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtDateSaisieDe.Focus();
                        //txtDateSaisieDe.SelectionStart = 0;
                        //txtDateSaisieDe.SelectionLength = txtDateSaisieDe.Text.Length;
                        return;
                    }
                    else//tested
                    {
                        txtDateSaisieDe.Text = Convert.ToDateTime(txtDateSaisieDe.Text).ToShortDateString();
                        if (Convert.ToDateTime(txtDateSaisieDe.Text) < Convert.ToDateTime("01/01/1900"))//tested
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDateSaisieDe.Focus();
                            //txtDateSaisieDe.SelectionStart = 0;
                            //txtDateSaisieDe.SelectionLength = txtDateSaisieDe.Text.Length;
                            return;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(txtIntereAu.Text))//tested
                {
                    if (txtIntereAu.Text.Contains(".") || !General.IsDate(txtIntereAu.Text))//tested
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtIntereAu.Focus();
                        //txtIntereAu.SelectionStart = 0;
                        //txtIntereAu.SelectionLength = txtIntereAu.Text.Length;
                        return;
                    }
                    else//tested
                    {
                        txtIntereAu.Text = Convert.ToDateTime(txtIntereAu.Text).ToShortDateString();
                        if (Convert.ToDateTime(txtIntereAu.Text) < Convert.ToDateTime("01/01/1900"))//tested
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtIntereAu.Focus();
                            //txtIntereAu.SelectionStart = 0;
                            //txtIntereAu.SelectionLength = txtIntereAu.Text.Length;
                            return;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(txtinterDe.Text))//tested
                {
                    if (txtinterDe.Text.Contains(".") || !General.IsDate(txtinterDe.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtinterDe.Focus();
                        //txtinterDe.SelectionStart = 0;
                        //txtinterDe.SelectionLength = txtinterDe.Text.Length;
                        return;
                    }
                    else//tested
                    {
                        txtinterDe.Text = Convert.ToDateTime(txtinterDe.Text).ToShortDateString();
                        if (Convert.ToDateTime(txtinterDe.Text) < Convert.ToDateTime("01/01/1900"))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtinterDe.Focus();
                            //txtinterDe.SelectionStart = 0;
                            //txtinterDe.SelectionLength = txtinterDe.Text.Length;
                            return;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(txtDatePrevue1.Text))//tested
                {
                    if (txtDatePrevue1.Text.Contains(".") || !General.IsDate(txtDatePrevue1.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtDatePrevue1.Focus();
                        //txtDatePrevue1.SelectionStart = 0;
                        //txtDatePrevue1.SelectionLength = txtDatePrevue1.Text.Length;
                        return;
                    }
                    else//tested
                    {
                        txtDatePrevue1.Text = Convert.ToDateTime(txtDatePrevue1.Text).ToShortDateString();
                        if (Convert.ToDateTime(txtDatePrevue1.Text) < Convert.ToDateTime("01/01/1900"))//tested
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDatePrevue1.Focus();
                            //txtDatePrevue1.SelectionStart = 0;
                            //txtDatePrevue1.SelectionLength = txtDatePrevue1.Text.Length;
                            return;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(txtDatePrevue2.Text))//tested
                {
                    if (txtDatePrevue2.Text.Contains(".") || !General.IsDate(txtDatePrevue2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtDatePrevue2.Focus();
                        //txtDatePrevue2.SelectionStart = 0;
                        //txtDatePrevue2.SelectionLength = txtDatePrevue2.Text.Length;
                        return;
                    }
                    else//tested
                    {
                        txtDatePrevue2.Text = Convert.ToDateTime(txtDatePrevue2.Text).ToShortDateString();
                        if (Convert.ToDateTime(txtDatePrevue2.Text) < Convert.ToDateTime("01/01/1900"))//tested
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDatePrevue2.Focus();
                            //txtDatePrevue2.SelectionStart = 0;
                            //txtDatePrevue2.SelectionLength = txtDatePrevue2.Text.Length;
                            return;
                        }
                    }
                }

                var _with10 = this;
                // stocke la position de la fiche DISPATCH.
                // fc_SaveParamPosition Me.Name, cUserDispatch, .ssIntervention.Columns("NoIntervention").value, "", _
                //.txtinterDe, .txtIntereAu, .cmbStatus, .cmbIntervenant, cmbArticle, .txtIntercom, .cmbCodeimmeuble, ssComboActivite, .txtINT_AnaCode _
                //, .txtDateSaisieDe, .txtDateSaisieFin, .txtDatePrevue1, .txtDatePrevue2, .cmbCodeGerant.Text, .cmbEtatDevis.Text, .chkAchat.value, chkVisiteEntretien.value

                //sSQl = "SELECT Intervention.DateRealise, Intervention.NoIntervention, Intervention.CodeImmeuble," _
                //& " Intervention.CodeEtat, TypeCodeEtat.LibelleCodeEtat, Intervention.Intervenant," _
                //& " Personnel.Nom, Intervention.Commentaire, Intervention.CptREndu_INT" _
                //& " FROM (TypeCodeEtat RIGHT JOIN Intervention ON TypeCodeEtat.CodeEtat =" _
                //& " Intervention.CodeEtat) LEFT JOIN Personnel ON Intervention.Intervenant =" _
                //& " Personnel.Matricule"

                General.sSQL = "SELECT Intervention.DateRealise,Intervention.Intervenant,"
                        + " Intervention.CodeImmeuble, Immeuble.CodePostal, Immeuble.ville,Intervention.CodeEtat, "
                        + " Intervention.Designation, Intervention.Commentaire,   Intervention.Article, "
                        + " Personnel.Nom,Personnel.CSecteur , Intervention.CptREndu_INT, TypeCodeEtat.LibelleCodeEtat, "
                        + " Intervention.NoIntervention"
                        + " , INT_Rapport1,Wave, Duree, Immeuble.CodeDepanneur, Intervention.INT_AnaActivite, "
                        + " Table1.Prioritaire,FactureManuellePied.MontantHT,Immeuble.Prioritaire AS Prior, "
                        + " Table1.code1,HeureDebut, HeureFin ";

                // sSQL = sSQL & " FROM (TypeCodeEtat RIGHT JOIN Intervention ON TypeCodeEtat.CodeEtat = Intervention.CodeEtat)" _
                //& " LEFT JOIN Personnel ON Intervention.Intervenant = Personnel.Matricule INNER JOIN IMMEUBLE ON Intervention.CodeImmeuble=Immeuble.CodeImmeuble " _
                //& " INNER JOIN Table1 ON Immeuble.Code1=Table1.Code1 " _
                //& " LEFT OUTER JOIN FactureManuelleEntete ON " _
                //& " Intervention.NoFacture=FactureManuelleEntete.NoFacture " _
                //& " LEFT OUTER JOIN FactureManuellePied ON " _
                //& " FactureManuelleEntete.CleFactureManuelle=FactureManuellePied.CleFactureManuelle " _
                //& " INNER JOIN GestionStandard ON " _
                //& " Intervention.NumFicheStandard=GestionStandard.NumFicheStandard " _
                //& " LEFT OUTER JOIN DevisEntete ON " _
                //& " GestionStandard.NoDevis=DevisEntete.NumeroDevis "

                //== ajout de la table energie.
                General.sSQL = General.sSQL + " FROM         TypeCodeEtat RIGHT OUTER JOIN"
                        + " Intervention ON TypeCodeEtat.CodeEtat = Intervention.CodeEtat "
                        + " LEFT OUTER JOIN Personnel "
                        + " ON Intervention.Intervenant = Personnel.Matricule "
                        + " INNER JOIN Immeuble "
                        + " ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble "
                        + " INNER JOIN Table1 "
                        + " ON Immeuble.Code1 = Table1.Code1 "
                        + " LEFT OUTER JOIN  FactureManuelleEnTete "
                        + " ON Intervention.NoFacture = FactureManuelleEnTete.NoFacture "
                        + " LEFT OUTER JOIN FactureManuellePied "
                        + " ON FactureManuelleEnTete.CleFactureManuelle = FactureManuellePied.CleFactureManuelle "
                        + " INNER JOIN GestionStandard "
                        + " ON Intervention.NumFicheStandard = GestionStandard.NumFicheStandard "
                        + " LEFT OUTER JOIN TechReleve "
                        + " ON Immeuble.CodeImmeuble = TechReleve.CodeImmeuble "
                        + " LEFT OUTER JOIN DevisEnTete "
                        + " ON GestionStandard.NoDevis = DevisEnTete.NumeroDevis ";

                //== Modif rachid du 10 02 2005, ajout du gestionnaire.
                //sSQl = sSQl & " FROM TypeCodeEtat RIGHT OUTER JOIN" _
                //& " Intervention ON TypeCodeEtat.CodeEtat = Intervention.CodeEtat LEFT OUTER JOIN" _
                //& " Personnel ON Intervention.Intervenant = Personnel.Matricule INNER JOIN" _
                //& " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN" _
                //& " Table1 ON Immeuble.Code1 = Table1.Code1 LEFT OUTER JOIN" _
                //& " FactureManuelleEnTete ON Intervention.NoFacture = FactureManuelleEnTete.NoFacture LEFT OUTER JOIN" _
                //& " FactureManuellePied ON FactureManuelleEnTete.CleFactureManuelle" _
                //& " = FactureManuellePied.CleFactureManuelle INNER JOIN" _
                //& " GestionStandard ON Intervention.NumFicheStandard = " _
                //& " GestionStandard.NumFicheStandard LEFT OUTER JOIN" _
                //& " Gestionnaire ON Immeuble.CodeGestionnaire = Gestionnaire.CodeGestinnaire" _
                //& " LEFT OUTER JOIN DevisEnTete ON GestionStandard.NoDevis = DevisEnTete.NumeroDevis"

                var _with11 = this;

                if (!string.IsNullOrEmpty(_with11.cmbCodeimmeuble.Text))//tested
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.CodeImmeuble" + (chkExclImm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCodeimmeuble.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.CodeImmeuble" + (chkExclImm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCodeimmeuble.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.cmbCodeGerant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Immeuble.Code1" + (chkExclGerant.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCodeGerant.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Immeuble.Code1" + (chkExclGerant.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCodeGerant.Text + "'";
                    }
                }

                if (_with11.chkVisiteEntretien.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.VisiteEntretien = 1 ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.VisiteEntretien = 1";
                    }
                }

                if (_with11.chkDevisEtablir.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.DevisEtablir = 1 ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.DevisEtablir = 1";
                    }
                }

                if (_with11.chkDapeau.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.Drapeau = 1 ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.Drapeau = 1";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtCP.Text))//Tested
                {
                    if (txtCP.Text.Length == 2)//Tested
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE Immeuble.CodePostal like '" + _with11.txtCP.Text + "%'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND Immeuble.CodePostal like '" + _with11.txtCP.Text + "%'";
                        }
                    }
                    else//tested
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE Immeuble.CodePostal ='" + _with11.txtCP.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND Immeuble.CodePostal ='" + _with11.txtCP.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_with11.cmbIntervenant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.Intervenant" + (chkExclInterv.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbIntervenant.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Intervention.Intervenant" + (chkExclInterv.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbIntervenant.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(cmdDispatch.Text))//tested
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  Intervention.Dispatcheur" + (chkExclDispatch.CheckState == 0 ? "=" : "<>") + "'" + cmdDispatch.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Intervention.Dispatcheur" + (chkExclDispatch.CheckState == 0 ? "=" : "<>") + "'" + cmdDispatch.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtCommentaire.Text))
                {
                    sComentaire = txtCommentaire.Text.Replace(" ", "%");
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " where Intervention.Commentaire like '%" + StdSQLchaine.gFr_DoublerQuote(sComentaire) + "%'";
                    }
                    else
                    {
                        sWhere = sWhere + " and  Intervention.Commentaire LIKE '%" + StdSQLchaine.gFr_DoublerQuote(sComentaire) + "%'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.cmbDepan.Text))
                {
                    sSQLInitia = "SELECT Initiales FROM PERSONNEL where matricule ='" + StdSQLchaine.gFr_DoublerQuote(cmbDepan.Text) + "'";
                    using (var tmp = new ModAdo())
                        sSQLInitia = tmp.fc_ADOlibelle(sSQLInitia);

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Personnel.CRespExploit" + (chkExclDepan.CheckState == 0 ? "=" : "<>") + "'" + sSQLInitia + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Personnel.CRespExploit" + (chkExclDepan.CheckState == 0 ? "=" : "<>") + "'" + sSQLInitia + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtCodeGestionnaire.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE immeuble.CodeGestionnaire " + (chkExclGestionnaire.CheckState == 0 ? "=" : "<>") + "'" + _with11.txtCodeGestionnaire.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and immeuble.CodeGestionnaire" + (chkExclGestionnaire.CheckState == 0 ? "=" : "<>") + "'" + _with11.txtCodeGestionnaire.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(cmbEnergie.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE TechReleve.CodeEnergie1 " + (chkExclEnergie.CheckState == 0 ? "=" : "<>") + "'" + StdSQLchaine.gFr_DoublerQuote(_with11.cmbEnergie.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " and TechReleve.CodeEnergie1 " + (chkExclEnergie.CheckState == 0 ? "=" : "<>") + "'" + StdSQLchaine.gFr_DoublerQuote(_with11.cmbEnergie.Text) + "'";
                    }

                }

                //==== modif le 25 10 2011, le commercial est liée à l'immeuble et non au client.
                if (General.sVersionImmParComm == "1")//teste cette condition
                {
                    if (!string.IsNullOrEmpty(_with11.cmbCommercial.Text))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE Immeuble.CodeCommercial " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCommercial.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND Immeuble.CodeCommercial " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCommercial.Text + "'";
                        }
                    }

                }
                else
                {

                    if (!string.IsNullOrEmpty(_with11.cmbCommercial.Text))
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE (Table1.Commercial" + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCommercial.Text + "' OR Immeuble.CodeCommercial " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCommercial.Text + "')";
                        }
                        else
                        {
                            sWhere = sWhere + " AND (Table1.Commercial" + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCommercial.Text + "' OR Immeuble.CodeCommercial " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCommercial.Text + "')";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_with11.cmbStatus.Text))//tested
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.CodeEtat" + (chkExclStatut.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbStatus.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.CodeEtat" + (chkExclStatut.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbStatus.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtGroupe.Text))//tested
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "  WHERE Table1.CRCL_Code='" + StdSQLchaine.gFr_DoublerQuote(txtGroupe.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Table1.CRCL_Code ='" + StdSQLchaine.gFr_DoublerQuote(txtGroupe.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtinterDe.Text))//tested
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        //===> Mondir le 31.10.2020 ticket : 2147
                        //sWhere = " where Intervention.daterealise>='"
                        //         + Convert.ToDateTime(_with11.txtinterDe.Text).ToString(General.FormatDateSansHeureSQL) + " 00:00:00 " + "'";

                        sWhere = $" where (Intervention.daterealise>='{Convert.ToDateTime(_with11.txtinterDe.Text).ToString(General.FormatDateSansHeureSQL)} 00:00:00' " +
                                 $"OR CONVERT(DATE, Intervention.DateRealise) >= '{Convert.ToDateTime(_with11.txtinterDe.Text).ToString(General.FormatDateSansHeureSQL)}')";

                    }
                    else
                    {
                        sWhere = sWhere + " and (Intervention.daterealise>='"
                            + Convert.ToDateTime(_with11.txtinterDe.Text).ToString(General.FormatDateSansHeureSQL) + " 00:00:00 " + "' " +
                            $"OR CONVERT(DATE, Intervention.DateRealise) >= '{Convert.ToDateTime(_with11.txtinterDe.Text).ToString(General.FormatDateSansHeureSQL)}')";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtIntereAu.Text))//tested
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        //===> Mondir le 15.10.2020, removed to the hours and minutes from the qury to fix 2040
                        //sWhere = sWhere + " and  Intervention.daterealise<='"
                        //    + Convert.ToDateTime(_with11.txtIntereAu.Text).ToString(General.FormatDateSansHeureSQL) + " 23:59:59" + "'";
                        //===> Fin Modif Mondir

                        sWhere = " Where (Intervention.daterealise<='"
                           + Convert.ToDateTime(_with11.txtIntereAu.Text).ToString(General.FormatDateSansHeureSQL) + " 00:00:00" + "' " +
                           $"OR CONVERT(DATE, Intervention.DateRealise) <='{Convert.ToDateTime(_with11.txtIntereAu.Text).ToString(General.FormatDateSansHeureSQL)}')";
                    }
                    else
                    {
                        //===> Mondir le 15.10.2020, removed to the hours and minutes from the qury to fix 2040
                        //sWhere = sWhere + " and  Intervention.daterealise<='"
                        //    + Convert.ToDateTime(_with11.txtIntereAu.Text).ToString(General.FormatDateSansHeureSQL) + " 23:59:59" + "'";
                        //===> Fin Modif Mondir

                        sWhere = sWhere + " and (Intervention.daterealise<='"
                            + Convert.ToDateTime(_with11.txtIntereAu.Text).ToString(General.FormatDateSansHeureSQL) + " 00:00:00" + "' " +
                            $"OR CONVERT(DATE, Intervention.DateRealise) <= '{Convert.ToDateTime(_with11.txtIntereAu.Text).ToString(General.FormatDateSansHeureSQL)}')";
                    }
                }

                if (!string.IsNullOrEmpty(txtNoIntervention.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " where Intervention.NoIntervention = " + _with11.txtNoIntervention.Text + "";
                    }
                    else
                    {
                        sWhere = sWhere + " and  Intervention.NoIntervention = " + _with11.txtNoIntervention.Text + "";
                    }
                }

                if (!string.IsNullOrEmpty(txtINT_AnaCode.Text))
                {
                    //==== modif le 25 10 2011, le chef de secteur est attaché à l'immeuble.
                    if (General.sVersionImmParComm == "1")//tested
                    {
                        //=== modif temporaire à mettre à jour.
                        if (General.sRespExploitParImmeuble == "1")//tested
                        {

                            if (string.IsNullOrEmpty(sWhere))
                            {
                                sWhere = " WHERE Immeuble.CodeRespExploit ='" + txtINT_AnaCode.Text + "'";
                            }
                            else
                            {
                                sWhere = sWhere + " AND Immeuble.CodeRespExploit ='" + txtINT_AnaCode.Text + "'";
                            }

                        }
                        else if (General.sChefDeSecteurParImmeuble == "1")
                        {

                            if (string.IsNullOrEmpty(sWhere))
                            {
                                sWhere = " WHERE Immeuble.CodeChefSecteur ='" + txtINT_AnaCode.Text + "'";
                            }
                            else
                            {
                                sWhere = sWhere + " AND Immeuble.CodeChefSecteur ='" + txtINT_AnaCode.Text + "'";
                            }

                        }
                        else//tested
                        {
                            sInitale = tmpAdo.fc_ADOlibelle("SELECT Initiales FROM PERSONNEL WHERE MATRICULE ='" + StdSQLchaine.gFr_DoublerQuote(txtINT_AnaCode.Text) + "'");
                            if (string.IsNullOrEmpty(sWhere))
                            {
                                //sWhere = " WHERE Personnel.CSecteur = '" & .txtINT_AnaCode & "'"
                                sWhere = " WHERE Personnel.CRespExploit = '" + sInitale + "'";
                            }
                            else
                            {
                                //sWhere = sWhere & " AND  Personnel.CSecteur = '" & .txtINT_AnaCode & "'"
                                sWhere = sWhere + " AND  Personnel.CRespExploit = '" + sInitale + "'";
                            }

                        }


                    }
                    else//tested
                    {
                        sInitale = tmpAdo.fc_ADOlibelle("SELECT Initiales FROM PERSONNEL WHERE MATRICULE ='" + StdSQLchaine.gFr_DoublerQuote(txtINT_AnaCode.Text) + "'");
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            //sWhere = " WHERE Personnel.CSecteur = '" & .txtINT_AnaCode & "'"
                            sWhere = " WHERE Personnel.CRespExploit = '" + sInitale + "'";
                        }
                        else
                        {
                            //sWhere = sWhere & " AND  Personnel.CSecteur = '" & .txtINT_AnaCode & "'"
                            sWhere = sWhere + " AND  Personnel.CRespExploit = '" + sInitale + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(ssComboActivite.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " where Intervention.INT_AnaCode LIKE '" + _with11.ssComboActivite.Text + "%'";
                    }
                    else
                    {
                        sWhere = sWhere + " and  Intervention.INT_AnaCode LIKE '" + _with11.ssComboActivite.Text + "%'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.cmbArticle.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " where Intervention.Article" + (chkExclArticle.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbArticle.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and  Intervention.Article" + (chkExclArticle.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbArticle.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtDateSaisieDe.Text))//tested
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " where Intervention.dateSaisie>='" + Convert.ToDateTime(_with11.txtDateSaisieDe.Text).ToString(General.FormatDateSansHeureSQL) + " 00:00:00 " + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Intervention.dateSaisie>='" + Convert.ToDateTime(_with11.txtDateSaisieDe.Text).ToString(General.FormatDateSansHeureSQL) + " 00:00:00 " + "'";

                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtDateSaisieFin.Text))//testeed
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " where  Intervention.dateSaisie<='" + Convert.ToDateTime(_with11.txtDateSaisieFin.Text).ToString(General.FormatDateSansHeureSQL) + " 23:59:59" + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " and  Intervention.dateSaisie<='" + Convert.ToDateTime(_with11.txtDateSaisieFin.Text).ToString(General.FormatDateSansHeureSQL) + " 23:59:59" + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtDatePrevue1.Text))//tested
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " where  Intervention.DatePrevue>='" + Convert.ToDateTime(_with11.txtDatePrevue1.Text).ToString(General.FormatDateSansHeureSQL) + " 00:00:00 " + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " and  Intervention.DatePrevue>='" + Convert.ToDateTime(_with11.txtDatePrevue1.Text).ToString(General.FormatDateSansHeureSQL) + " 00:00:00 " + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtDatePrevue2.Text))//tested
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " where  Intervention.DatePrevue<='" + Convert.ToDateTime(_with11.txtDatePrevue2.Text).ToString(General.FormatDateSansHeureSQL) + " 23:59:59" + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " and  Intervention.DatePrevue<='" + Convert.ToDateTime(_with11.txtDatePrevue2.Text).ToString(General.FormatDateSansHeureSQL) + " 23:59:59" + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.cmbEtatDevis.Text))//tested
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.CodeEtat='" + _with11.cmbEtatDevis.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND DevisEntete.CodeEtat='" + _with11.cmbEtatDevis.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtNoEnregistrement.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.NoEnregistrement='" + _with11.txtNoEnregistrement.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND DevisEntete.NoEnregistrement='" + _with11.txtNoEnregistrement.Text + "'";
                    }
                }

                if (_with11.chkAchat.CheckState == CheckState.Checked)//tested
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.Achat<>0 and NOT Intervention.Achat IS NULL ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.Achat<>0 and NOT Intervention.Achat IS NULL ";
                    }
                }

                if (chkAffaireSansFact.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE gestionStandard.NumFicheStandard NOT IN(SELECT NumFicheStandard FROM INTERVENTION WHERE Intervention.Nofacture is not null and Intervention.Nofacture <> '') ";
                    }
                    else
                    {
                        sWhere = sWhere + " and gestionStandard.NumFicheStandard NOT IN(SELECT NumFicheStandard FROM INTERVENTION WHERE Intervention.Nofacture is not null and Intervention.Nofacture <> '') ";
                    }
                }

                if (chkAffaireAvecFact.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE gestionStandard.NumFicheStandard  IN(SELECT NumFicheStandard FROM INTERVENTION WHERE Intervention.Nofacture is not null and Intervention.Nofacture <> '') ";
                    }
                    else
                    {
                        sWhere = sWhere + " and gestionStandard.NumFicheStandard  IN(SELECT NumFicheStandard FROM INTERVENTION WHERE Intervention.Nofacture is not null and Intervention.Nofacture <> '') ";
                    }
                }

                if (CHKP1.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  (Immeuble.P1= 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  (Immeuble.P1 = 1) ";
                    }

                }

                if (CHKP2.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  (Immeuble.P2= 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  (Immeuble.P2 = 1) ";
                    }

                }

                if (CHKP3.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  (Immeuble.P3= 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  (Immeuble.P3 = 1) ";
                    }

                }

                if (CHKP4.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  (Immeuble.P4 = 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  (Immeuble.P4 = 1) ";
                    }

                }

                if (chkDevisPDA.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  Intervention.Nointervention IN (SELECT NoIntervention FROM  DEM_DevisEnTeteMobile) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  Intervention.Nointervention IN (SELECT NoIntervention FROM  DEM_DevisEnTeteMobile)  ";
                    }
                }

                //'=== modif du 08 10 2019
                if (chkEncaissement.Checked)
                {
                    var chkencaiss = General.nz(chkEncaissement.Checked, 0).ToString() == "True" ? "1" : "0";
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  Intervention.Encaissement =" + chkencaiss;
                    }
                    else
                    {
                        sWhere = sWhere + "  AND  Intervention.Encaissement =" + chkencaiss;
                    }
                }

                if (chkEncRex.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  Intervention.nointervention IN (SELECT     NoIntervention From InterEncaissement  WHERE(ValideREX = 1))";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.nointervention IN(SELECT NoIntervention From InterEncaissement WHERE (ValideREX = 1))";
                    }
                }

                if (chkEncCompta.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  Intervention.nointervention IN (SELECT     NoIntervention From InterEncaissement WHERE (ValideCompta = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + "  AND  Intervention.nointervention IN (SELECT     NoIntervention From InterEncaissement WHERE (ValideCompta = 1))";
                    }
                }

                //====> Mondir le 01.07.2020, demandé via mantis : https://groupe-dt.mantishub.io/view.php?id=1549
                if (chkDemandeNonTraite.Checked)
                {
                    //DdeDeDevis
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  GestionStandard.DdeDeDevis = 0";
                    }
                    else
                    {
                        //sWhere = sWhere + "  AND  WHERE  GestionStandard.DdeDeDevis = 0";
                        //===> Mondir le 07.12.2020, remove WHere from the condition to fix https://groupe-dt.mantishub.io/view.php?id=2118
                        sWhere = sWhere + "  AND  GestionStandard.DdeDeDevis = 0";
                        //===> Fin Modif Mondir
                    }
                }
                //===> Fin Modif Mondir

                if (!string.IsNullOrEmpty(sWhere))
                {
                    General.sSQL = General.sSQL + sWhere;
                }

                BisStrRequete = General.sSQL;
                if (boolBordereauJournee == true)//tested
                {
                    // sOrder = " ORDER BY Intervention.Intervenant ASC, Intervention.CodeImmeuble ASC, Intervention.CodeEtat ASC "
                    //sOrder = " ORDER BY Intervention.Intervenant,Intervention.DateSaisie desc, Intervention.HeureDebut DESC, " + " Intervention.CodeImmeuble ASC, Intervention.CodeEtat ASC ";
                    //sOrderBy = "";
                    sOrder = " ORDER BY Intervention.Intervenant,Intervention.DateRealise DESC, cast( Intervention.HeureDebut as time) DESC ";
                    sOrderBy = sOrder;
                }
                else
                {
                    if (string.IsNullOrEmpty(sOrderBy))//tested
                    {
                        //sOrder = " ORDER BY Intervention.DateRealise DESC,Intervention.Intervenant ASC,Intervention.HeureDebut DESC, Intervention.CodeImmeuble ASC,  Intervention.CodeEtat ASC ";
                        sOrder = " ORDER BY Intervention.DateRealise DESC,Intervention.Intervenant ASC, cast( Intervention.HeureDebut as time) DESC ";
                    }
                    else
                    {

                        sOrder = " " + sOrderBy;
                        // '=== modif du 18 10 2018, ajout systematique de du champ daterealise asc et heure de debut.
                        //'sOrder = Replace(sOrderBy, ", Intervention.HeureDebut DESC", "")
                        //'sOrderBy = Replace(sOrderBy, ", Intervention.HeureDebut ASC", "")
                        //'sOrderBy = Replace(sOrderBy, ", HeureDebut ASC", "")
                        //'sOrderBy = Replace(sOrderBy, ", HeureDebut DESC", "")
                        //'sOrderBy = Replace(sOrderBy, ", HeureDebut", "")


                        //'sOrder = " " & sOrderBy & ""
                        if (sOrderBy.IndexOf("DateRealise") > 1 && sOrderBy.IndexOf("Intervenant") > 1 && sOrderBy.IndexOf("HeureDebut") > 1)
                        {
                            //   '===  borderaux de journée.
                            //    'sOrder = " ORDER BY Intervention.DateRealise desc,Intervention.Intervenant,  Intervention.HeureDebut DESc "
                            sOrder = " ORDER BY Intervention.DateRealise desc,Intervention.Intervenant, cast( Intervention.HeureDebut as time) DESC ";
                            //' sOrderBy = sOrder
                        }
                        else if (sOrderBy.IndexOf("DateRealise") > 1)
                        {
                            sOrder = " " + sOrderBy + ", cast( Intervention.HeureDebut as time) DESC";
                        }
                        else
                        {
                            sOrder = " " + sOrderBy + ", Intervention.DateRealise DESC, cast( Intervention.HeureDebut as time) DESC";
                        }
                    }
                }

                lblOrderBy.Text = sOrder;


                General.sSQL = General.sSQL + sOrder;

                if (fc_CtrlCriterEmpty() == true)
                {
                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention vous n avez pas sélectionné de critère de recherche pour les interventions. Ça risque de prendre du temps."
                        + "\n" + "Voulez vous continuer ?", "Avertissement", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        return;
                    }
                }
                var modAdorsintervention = new ModAdo();

                if ((rsintervention != null))
                {
                    rsintervention.Dispose();

                }
                else
                {
                    rsintervention = new DataTable();
                }

                //rsintervention = modAdorsintervention.fc_OpenRecordSet(General.sSQL);

                //===> Mondir 22.01.2021, Ajout du LOG
                Program.SaveException(null, $"fc_loadIntervention() - General.sSQL = {General.sSQL}");
                //===> Fin Modif Mondir

                var tmpAdapter = new SqlDataAdapter(General.sSQL, General.adocnn);
                rsintervention = new DataTable();
                tmpAdapter.Fill(rsintervention);

                //===> Mondir 22.01.2021, Ajout du LOG
                Program.SaveException(null, $"fc_loadIntervention() - rsintervention.Rows.Count = {rsintervention.Rows.Count}");
                //===> Fin Modif Mondir

                txtTotal.Text = General.nz(rsintervention.Rows.Count, "0").ToString();
                this.ssIntervention.DataSource = rsintervention;

                //===> Mondir le 30.09.2020, https://groupe-dt.mantishub.io/view.php?id=1999
                //this.ssIntervention.DisplayLayout.Bands[0].Columns["Intervenant"].SortComparer = new CustomComparer();
                //if(boolBordereauJournee)
                //{
                //    this.ssIntervention.DisplayLayout.Bands[0].Columns["Intervenant"].SortIndicator = SortIndicator.Ascending;
                //}
                //===> Fin Modif Mondir

                //fc_intializeRow();

                boolModifRow = false;

                if (rsintervention.Rows.Count == 0)//tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune intervention pour ces paramétres.", "Présélection Dispatch", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                //boolBordereauJournee = false;

                // fc_Stocke("");

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " cmbIntervention_Click ");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatus_TextChanged(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibStatus.Text = tmpAdo.fc_ADOlibelle("SELECT TypeCodeEtat.LibelleCodeEtat" + " FROM TypeCodeEtat WHERE TypeCodeEtat.CodeEtat='" + StdSQLchaine.gFr_DoublerQuote(cmbStatus.Text) + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatus_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibStatus.Text = tmpAdo.fc_ADOlibelle("SELECT TypeCodeEtat.LibelleCodeEtat" + " FROM TypeCodeEtat WHERE TypeCodeEtat.CodeEtat='" + StdSQLchaine.gFr_DoublerQuote(cmbStatus.Text) + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatus_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (cmbStatus.Rows.Count == 0)
            {
                General.sSQL = "SELECT TypeCodeEtat.CodeEtat, TypeCodeEtat.LibelleCodeEtat" + " FROM TypeCodeEtat order by CodeEtat";
                sheridan.InitialiseCombo(cmbStatus, General.sSQL, "CodeEtat");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStatus_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                //cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                //fc_loadIntervention();
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
        }

        private void cmdBordereauJournee_Click(object sender, EventArgs e)
        {
            //    sSql = "SELECT Intervention.DateRealise,Intervention.Intervenant," _
            //'            & " Intervention.CodeImmeuble, Intervention.CodeEtat, " _
            //'            & " Intervention.Designation, Intervention.Commentaire,   Intervention.Article, " _
            //'            & " Personnel.Nom, Intervention.CptREndu_INT, TypeCodeEtat.LibelleCodeEtat, Intervention.NoIntervention" _
            //'            & " , INT_Rapport1" _
            //'            & " FROM (TypeCodeEtat RIGHT JOIN Intervention ON TypeCodeEtat.CodeEtat = Intervention.CodeEtat)" _
            //'            & " LEFT JOIN Personnel ON Intervention.Intervenant = Personnel.Matricule"

            var _with12 = this;
            cmdDispatch.Text = "";
            _with12.cmbCodeimmeuble.Text = "";
            _with12.cmbCodeGerant.Text = "";
            _with12.cmbIntervenant.Text = "";
            cmdDispatch.Text = "";
            _with12.cmbStatus.Text = "";
            _with12.txtinterDe.Text = DateTime.Now.ToShortDateString();
            _with12.txtIntereAu.Text = DateTime.Now.ToShortDateString();
            _with12.txtCommentaire.Text = "";
            _with12.txtNoIntervention.Text = "";
            _with12.txtINT_AnaCode.Text = "";
            _with12.ssComboActivite.Text = "";
            _with12.cmbArticle.Text = "";
            _with12.txtDateSaisieDe.Text = "";
            _with12.txtDateSaisieFin.Text = "";
            _with12.txtDatePrevue1.Text = "";
            _with12.txtDatePrevue2.Text = "";
            _with12.txtCP.Text = "";

            txtCommentaire.Text = "";
            cmbDepan.Text = "";
            //    sSql = sSql & " WHERE Intervention.DateRealise='" & Format(Date, FormatDateSQL) & "'"
            //    sSql = sSql & " order by  Intervention.DateRealise desc"
            //    Set rsintervention = fc_OpenRecordSet(sSql)
            //    If rsintervention.EOF And rsintervention.BOF Then
            //        MsgBox "Aucune intervention n'a été faite ce jour.", vbCritical, ""
            //    End If
            //    Me.ssIntervention.ReBind
            boolBordereauJournee = true;
            //sOrderBy = " ORDER BY Intervention.DateSaisie DESGGGSC"

            //cmbIntervention_Click(cmbIntervention, new System.EventArgs());

            fc_loadIntervention();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdClean_Click(object sender, EventArgs e)
        {
            var _with13 = this;
            _with13.txtinterDe.Text = "";

            _with13.txtIntereAu.Text = "";
            _with13.cmbStatus.Text = "";
            _with13.cmbIntervenant.Text = "";

            cmdDispatch.Text = "";
            txtCommentaire.Text = "";
            cmbDepan.Text = "";
            _with13.cmbArticle.Text = "";
            _with13.txtCommentaire.Text = "";
            _with13.cmbCodeimmeuble.Text = "";
            _with13.cmbCodeGerant.Text = "";
            _with13.ssComboActivite.Text = "";
            _with13.txtINT_AnaCode.Text = "";
            _with13.txtDateSaisieDe.Text = "";
            _with13.txtDateSaisieFin.Text = "";
            _with13.txtDatePrevue1.Text = "";
            _with13.txtDatePrevue2.Text = "";
            _with13.txtCP.Text = "";
            _with13.cmbCommercial.Text = "";
            _with13.cmbEtatDevis.Text = "";

            _with13.chkAchat.CheckState = System.Windows.Forms.CheckState.Unchecked;
            _with13.chkExclArticle.CheckState = System.Windows.Forms.CheckState.Unchecked;
            _with13.chkExclComm.CheckState = System.Windows.Forms.CheckState.Unchecked;
            _with13.chkExclGerant.CheckState = System.Windows.Forms.CheckState.Unchecked;
            _with13.chkExclImm.CheckState = System.Windows.Forms.CheckState.Unchecked;
            _with13.chkExclInterv.CheckState = System.Windows.Forms.CheckState.Unchecked;
            _with13.chkExclStatut.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkExclGestionnaire.CheckState = System.Windows.Forms.CheckState.Unchecked;
            txtCodeGestionnaire.Text = "";

            lblLibGestionnaire.Text = "";
            _with13.cmbEnergie.Text = "";
            _with13.chkExclEnergie.CheckState = System.Windows.Forms.CheckState.Unchecked;
            _with13.txtNoEnregistrement.Text = "";
            chkAffaireSansFact.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkAffaireAvecFact.CheckState = System.Windows.Forms.CheckState.Unchecked;
            CHKP1.CheckState = System.Windows.Forms.CheckState.Unchecked;
            CHKP2.CheckState = System.Windows.Forms.CheckState.Unchecked;
            CHKP3.CheckState = System.Windows.Forms.CheckState.Unchecked;
            CHKP4.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkVisiteEntretien.CheckState = System.Windows.Forms.CheckState.Unchecked;
            chkDevisEtablir.CheckState = System.Windows.Forms.CheckState.Unchecked;
            //===> Mondir le 25.01.2021, https://groupe-dt.mantishub.io/view.php?id=2228
            chkDemandeNonTraite.CheckState = System.Windows.Forms.CheckState.Unchecked;
            //===> Fin Modif Mondir
            chkDapeau.CheckState = System.Windows.Forms.CheckState.Unchecked;
            //  '=== 08 10 2019.
            chkEncaissement.CheckState = System.Windows.Forms.CheckState.Unchecked;
            txtGroupe.Text = "";
            chkEncRex.Checked = false;
            chkEncCompta.Checked = false;
            chkDevisPDA.Checked = false;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdEditer_Click(object sender, EventArgs e)
        {
            try
            {
                DateTime X = new DateTime();
                string sqlUpdate = null;
                //    Dim NumJourSemaine  As Integer
                //    Dim sDateButtoire   As Date

                if (cmbStatus.Text.ToUpper() == "AF" && !string.IsNullOrEmpty(txtinterDe.Text) && !string.IsNullOrEmpty(txtIntereAu.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Les Interventions en AF passent en E", "Edition des interventions pour facturation", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    //        NumJourSemaine = Weekday(Date, vbSunday)
                    //        sDateButtoire = Format(Date - (NumJourSemaine - 8), "dd/mm/yyyy")
                    //        txtIntereAu.Text = CStr(sDateButtoire)
                    //        sDateButtoire = Format(sDateButtoire, FormatDateSansHeureSQL)

                    sqlUpdate = "UPDATE Intervention SET Intervention.CodeEtat='E' WHERE ";
                    sqlUpdate = sqlUpdate + "DateRealise>='" + Convert.ToDateTime(txtinterDe.Text) + "'";
                    sqlUpdate = sqlUpdate + " AND DateRealise<='" + Convert.ToDateTime(txtIntereAu.Text) + "'";
                    sqlUpdate = sqlUpdate + " AND CodeEtat='" + "AF" + "'";
                    General.Execute(sqlUpdate);
                    cmbStatus.Text = "E";
                    ssIntervention.UpdateData();
                    boolEtatFactInterv = true;
                }
                if (cmbStatus.Text.ToUpper() == "E")
                {
                    boolEtatFactInterv = true;
                }

                X = DateTime.Now;
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                Impression();

            }
            catch (Exception ex)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur lors de l'édition des interventions" + ex.Source, "Edition des interventions", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Erreurs.gFr_debug(ex, this.Name + " CmdEditer_Click");

            }
        }
        private void Impression()
        {
            string sWhere = "";

            string sSelect = "";
            string sWere = "";
            string sOrder = "";
            int nbHeures = 0;
            string sComentaire;
            try
            {

                //If strOptionChoisi = "7" Then
                //   CR.Reset
                //   ' CR.Connect = "DSN = DTMDB,UID = admin ,PWD =;DSQ = agcDelostal"
                //    CR.ReportFileName = CHEMINETATFACT
                //ElseIf strOptionChoisi = "11" Then
                //    CR.Reset
                //    CR.ReportFileName = CHEMINETATPREST
                //Else
                //CR.Reset();
                ReportDocument CR = new ReportDocument();
                if (boolEtatFactInterv == true)
                {
                    CR.Load(General.gsRpt + General.CHEMINETATFACT);
                }
                else
                {
                    CR.Load(General.gsRpt + General.CHEMINETAT);
                }
                //End If

                var _with14 = this;

                sWhere = "";

                if (!string.IsNullOrEmpty(_with14.cmbCodeimmeuble.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = "{Intervention.codeimmeuble}" + (chkExclImm.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmbCodeimmeuble.Text + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " {Intervention.codeimmeuble}" + (chkExclImm.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmbCodeimmeuble.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with14.cmbCodeGerant.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " {Immeuble.Code1}" + (chkExclGerant.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmbCodeGerant.Text + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " {Immeuble.Code1}" + (chkExclGerant.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmbCodeGerant.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with14.cmbIntervenant.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = "{Intervention.Intervenant}" + (chkExclInterv.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmbIntervenant.Text + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND {Intervention.Intervenant}" + (chkExclInterv.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmbIntervenant.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with14.cmdDispatch.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = "{Intervention.Dispatcheur}" + (chkExclDispatch.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmdDispatch.Text + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND {Intervention.Dispatcheur}" + (chkExclDispatch.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmdDispatch.Text + "'";
                    }
                }
                if (!string.IsNullOrEmpty(_with14.txtCommentaire.Text))
                {
                    sComentaire = txtCommentaire.Text.TrimStart();
                    sComentaire = sComentaire.TrimEnd();
                    sComentaire = sComentaire.Replace(" ", "*");
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = "{Intervention.Commentaire}  like '*" + StdSQLchaine.gFr_DoublerQuote(sComentaire) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND {Intervention.Commentaire} like '*" + StdSQLchaine.gFr_DoublerQuote(sComentaire) + "'";
                    }
                }
                if (!string.IsNullOrEmpty(_with14.cmbCommercial.Text))
                {
                    //==== modif le 25 10 2011, le commercial est liée à l'immeuble et non au client.
                    if (General.sVersionImmParComm == "1")
                    {

                        if (string.IsNullOrEmpty(sWhere))
                        {

                            sWhere = " {Immeuble.CodeCommercial} " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmbCommercial.Text + "'";
                        }
                        else
                        {

                            sWhere = sWhere + " AND {Immeuble.CodeCommercial} " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmbCommercial.Text + "'";
                        }

                    }
                    else
                    {

                        if (string.IsNullOrEmpty(sWhere))
                        {

                            sWhere = "{Table1.Commercial}" + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmbCommercial.Text + "'";
                        }
                        else
                        {

                            sWhere = sWhere + " AND {Table1.Commercial}" + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmbCommercial.Text + "'";
                        }
                    }
                }

                //==== modif le 25 10 2011, le chef de secteur est attaché à l'immeuble.
                if (!string.IsNullOrEmpty(txtINT_AnaCode.Text))
                {
                    if (General.sVersionImmParComm == "1")
                    {
                        //=== modif temporaire à mettre à jour.
                        if (General.sRespExploitParImmeuble == "1")
                        {


                            if (string.IsNullOrEmpty(sWhere))
                            {

                                sWhere = " {Immeuble.CodeRespExploit} ='" + txtINT_AnaCode.Text + "'";
                            }
                            else
                            {

                                sWhere = sWhere + " AND {Immeuble.CodeRespExploit} ='" + txtINT_AnaCode.Text + "'";
                            }

                        }
                        else if (General.sChefDeSecteurParImmeuble == "1")
                        {


                            if (string.IsNullOrEmpty(sWhere))
                            {

                                sWhere = " WHERE {Immeuble.CodeChefSecteur} ='" + txtINT_AnaCode.Text + "'";
                            }
                            else
                            {

                                sWhere = sWhere + " AND {Immeuble.CodeChefSecteur} ='" + txtINT_AnaCode.Text + "'";
                            }

                        }
                        else
                        {
                            //                    sInitale = fc_ADOlibelle("SELECT Initiales FROM PERSONNEL WHERE MATRICULE ='" & txtINT_AnaCode & "'")
                            //                    If sWhere = "" Then
                            //                        'sWhere = " WHERE Personnel.CSecteur = '" & .txtINT_AnaCode & "'"
                            //                        sWhere = " WHERE Personnel.CRespExploit = '" & sInitale & "'"
                            //                    Else
                            //                        'sWhere = sWhere & " AND  Personnel.CSecteur = '" & .txtINT_AnaCode & "'"
                            //                        sWhere = sWhere & " AND  Personnel.CRespExploit = '" & sInitale & "'"
                            //                    End If

                        }
                    }
                }

                if (chkExclStatut.CheckState == CheckState.Checked && !string.IsNullOrEmpty(_with14.cmbStatus.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = "{Intervention.CodeEtat}" + (chkExclStatut.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmbStatus.Text + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND {Intervention.CodeEtat}" + (chkExclStatut.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmbStatus.Text + "'";
                    }
                }
                else
                {
                    if (!string.IsNullOrEmpty(_with14.cmbStatus.Text))
                    {
                        //Imprime les interventions en E si le choix est d'afficher
                        //les interventions en AF
                        if (_with14.cmbStatus.Text.ToUpper() == "AF")
                        {

                            if (string.IsNullOrEmpty(sWhere))
                            {

                                sWhere = "{Intervention.CodeEtat}='E'";
                            }
                            else
                            {

                                sWhere = sWhere + " AND {Intervention.CodeEtat}='E'";
                            }
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(sWhere))
                            {

                                sWhere = "{Intervention.CodeEtat}='" + _with14.cmbStatus.Text + "'";
                            }
                            else
                            {

                                sWhere = sWhere + " AND {Intervention.CodeEtat}='" + _with14.cmbStatus.Text + "'";
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_with14.txtinterDe.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = "{Intervention.DateRealise}>=dateTime(" + Convert.ToDateTime(_with14.txtinterDe.Text).ToString("yyyy,MM,dd") + ",00,00,00)";
                    }
                    else
                    {

                        sWhere = sWhere + " AND {Intervention.DateRealise}>=dateTime(" + Convert.ToDateTime(_with14.txtinterDe.Text).ToString("yyyy,MM,dd") + ",00,00,00)";
                    }
                }

                if (!string.IsNullOrEmpty(_with14.txtIntereAu.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " {Intervention.DateRealise}<=dateTime(" + Convert.ToDateTime(_with14.txtIntereAu.Text).ToString("yyyy,MM,dd") + ",23,59,59)";
                    }
                    else
                    {

                        sWhere = sWhere + " AND {Intervention.DateRealise}<=dateTime(" + Convert.ToDateTime(_with14.txtIntereAu.Text).ToString("yyyy,MM,dd") + ",23,59,59)";
                    }
                }
                if (!string.IsNullOrEmpty(_with14.cmbArticle.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " {Intervention.Article}" + (chkExclArticle.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmbArticle.Text + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND {Intervention.Article}" + (chkExclArticle.CheckState == 0 ? "=" : "<>") + "'" + _with14.cmbArticle.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with14.txtDateSaisieDe.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {


                        sWhere = " {Intervention.DateSaisie}>=dateTime(" + Convert.ToDateTime(_with14.txtDateSaisieDe.Text).ToString("yyyy,MM,dd") + ",00,00,00)";
                    }
                    else
                    {

                        sWhere = sWhere + " AND {Intervention.DateSaisie}>=dateTime(" + Convert.ToDateTime(_with14.txtDateSaisieDe.Text).ToString("yyyy,MM,dd") + ",00,00,00)";

                    }
                }

                if (!string.IsNullOrEmpty(_with14.txtDateSaisieFin.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " {Intervention.dateSaisie}<=dateTime(" + Convert.ToDateTime(_with14.txtDateSaisieFin.Text).ToString("yyyy,MM,dd") + ",23,59,59)";
                    }
                    else
                    {

                        sWhere = sWhere + " AND {Intervention.dateSaisie}<=dateTime(" + Convert.ToDateTime(_with14.txtDateSaisieFin.Text).ToString("yyyy,MM,dd") + ",23,59,59)";
                    }
                }

                if (!string.IsNullOrEmpty(_with14.txtDatePrevue1.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " {Intervention.DatePrevue}>=dateTime(" + Convert.ToDateTime(_with14.txtDatePrevue1.Text).ToString("yyyy,MM,dd") + ",00,00,00)";
                    }
                    else
                    {

                        sWhere = sWhere + " AND {Intervention.DatePrevue}>=dateTime(" + Convert.ToDateTime(_with14.txtDatePrevue1.Text).ToString("yyyy,MM,dd") + ",00,00,00)";
                    }
                }

                if (!string.IsNullOrEmpty(_with14.txtDatePrevue2.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " {Intervention.DatePrevue}<=dateTime(" + Convert.ToDateTime(_with14.txtDatePrevue2.Text).ToString("yyyy,MM,dd") + ",23,59,59)";
                    }
                    else
                    {

                        sWhere = sWhere + " AND {Intervention.DatePrevue}<=dateTime(" + Convert.ToDateTime(_with14.txtDatePrevue2.Text).ToString("yyyy,MM,dd") + ",23,59,59)";
                    }
                }

                if (_with14.chkAchat.CheckState == CheckState.Checked)
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " {Intervention.Achat}=True ";
                    }
                    else
                    {

                        sWhere = sWhere + " AND {Intervention.Achat}=True ";
                    }
                }

                if (!string.IsNullOrEmpty(_with14.cmbEtatDevis.Text))
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " {DevisEntete.CodeEtat}= '" + _with14.cmbEtatDevis.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND {DevisEntete.CodeEtat}= '" + _with14.cmbEtatDevis.Text + "'";
                    }
                }
                //=== modif du 14 05 2014, ajout des criteres P1,P2,P3.
                if (CHKP1.CheckState == CheckState.Checked)
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE  {Immeuble.P1} = true ";
                    }
                    else
                    {

                        sWhere = sWhere + " AND  {Immeuble.P1} = true ";
                        //sWhere = sWhere & " AND  {Immeuble.P1}  "
                    }

                }

                if (CHKP2.CheckState == CheckState.Checked)
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE  {Immeuble.P2} = true ";
                    }
                    else
                    {

                        sWhere = sWhere + " AND  {Immeuble.P2} = true ";
                    }

                }

                if (CHKP3.CheckState == CheckState.Checked)
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE  {Immeuble.P3} = true ";
                    }
                    else
                    {

                        sWhere = sWhere + " AND  {Immeuble.P3}  = true";
                    }

                }


                if (CHKP4.CheckState == CheckState.Checked)
                {

                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE  {Immeuble.P4}  = true ";
                    }
                    else
                    {

                        sWhere = sWhere + " AND  {Immeuble.P4} = true ";
                    }

                }

                //modif du 13 03 2019, controle si contrat en cours + si particuliers.

                if (!string.IsNullOrEmpty(General.sSQL) && boolEtatFactInterv == false)
                {

                    strDate = CalculDesHeures();
                    //CR.SetParameterValue("", strDate);
                    CR.DataDefinition.FormulaFields["Total"].Text = "'" + strDate + "'";
                }
                else
                {
                    CR.DataDefinition.FormulaFields["Total"].Text = "";
                }

                string SelectionFormula = sWhere;

                if (General.UCase(General.adocnn.Database) != General.UCase("AXECIEL_LONG"))
                {
                    //CR.set_SortFields(0, "+{Intervention.CodeImmeuble}");                            
                    // CR.set_SortFields(1, "+{Intervention.DateSaisie}");
                    // CR.set_SortFields(2, "+{Intervention.CodeEtat}");
                }

                var tr = CR.RecordSelectionFormula;
                var FormCR = new CrystalReportFormView(CR, SelectionFormula);
                FormCR.Show();
                boolEtatFactInterv = false;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Impression");
            }

        }
        public string CalculDesHeures()
        {

            string functionReturnValue = null;
            DataTable rs;
            int BisLongTemp = 0;
            int LongTemp = 0;
            float curtemp = default(float);
            string str_Renamed = null;

            try
            {
                rs = new DataTable();
                // rs.Open(BisStrRequete, General.adocnn);
                using (var modAdors = new ModAdo())
                    rs = modAdors.fc_OpenRecordSet(BisStrRequete);
                LongTemp = 0;

                if (rs.Rows.Count == 0)
                {
                    functionReturnValue = "00:00:00";
                    rs = null;
                    return functionReturnValue;
                }
                //----Calcul des heures en secondes

                foreach (DataRow rsRow in rs.Rows)
                {
                    if (rsRow["Duree"] != DBNull.Value)
                    {
                        LongTemp = LongTemp + Convert.ToDateTime(rsRow["Duree"]).Hour * 3600;
                        LongTemp = LongTemp + Convert.ToDateTime(rsRow["Duree"]).Minute * 60;
                        LongTemp = LongTemp + Convert.ToDateTime(rsRow["Duree"]).Second;
                    }

                    //rs.MoveNext();
                }

                rs.Dispose();
                rs = null;
                //----Calcul du temps en heures
                curtemp = 0;
                curtemp = (float)LongTemp / (float)3600;
                str_Renamed = curtemp.ToString();
                if (str_Renamed.Contains("."))
                {
                    str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf('.'));
                    str_Renamed = Convert.ToInt16(str_Renamed).ToString("D2");
                    functionReturnValue = str_Renamed + ":";
                    BisLongTemp = Convert.ToInt32(str_Renamed);
                    while (BisLongTemp != 0)
                    {
                        LongTemp = LongTemp - 3600;
                        BisLongTemp = BisLongTemp - 1;
                    }

                    //----Calcul du temps en minutes
                    curtemp = (float)LongTemp / (float)60;
                    str_Renamed = Convert.ToString(curtemp);
                    if (str_Renamed.Contains('.'))
                    {
                        str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf('.'));
                        str_Renamed = Convert.ToInt16(str_Renamed).ToString("D2");
                        functionReturnValue = functionReturnValue + str_Renamed + ":";
                        BisLongTemp = Convert.ToInt32(str_Renamed);
                        while (BisLongTemp != 0)
                        {
                            LongTemp = LongTemp - 60;
                            BisLongTemp = BisLongTemp - 1;
                        }
                        str_Renamed = Convert.ToInt16(LongTemp).ToString("D2");
                        functionReturnValue = functionReturnValue + str_Renamed;
                    }
                    else
                    {
                        functionReturnValue = functionReturnValue + "00:00";
                    }
                }
                else if (str_Renamed.Contains(','))
                {
                    str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf(','));
                    str_Renamed = Convert.ToInt16(str_Renamed).ToString("D2");
                    functionReturnValue = str_Renamed + ":";
                    BisLongTemp = Convert.ToInt32(str_Renamed);
                    while (BisLongTemp != 0)
                    {
                        LongTemp = LongTemp - 3600;
                        BisLongTemp = BisLongTemp - 1;
                    }

                    //----Calcul du temps en minutes
                    curtemp = (float)LongTemp / (float)60;
                    str_Renamed = Convert.ToString(curtemp);
                    if (str_Renamed.Contains(','))
                    {
                        str_Renamed = General.Left(str_Renamed, str_Renamed.IndexOf(','));
                        str_Renamed = Convert.ToInt16(str_Renamed).ToString("D2");
                        functionReturnValue = functionReturnValue + str_Renamed + ":";
                        BisLongTemp = Convert.ToInt32(str_Renamed);
                        while (BisLongTemp != 0)
                        {
                            LongTemp = LongTemp - 60;
                            BisLongTemp = BisLongTemp - 1;
                        }
                        str_Renamed = Convert.ToInt16(LongTemp).ToString("D2");
                        functionReturnValue = functionReturnValue + str_Renamed;
                    }
                    else
                    {
                        functionReturnValue = functionReturnValue + "00:00";
                    }
                }
                else
                {
                    functionReturnValue = str_Renamed + ":00:00";
                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";CalculDesHeures");
            }

            return functionReturnValue;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExportExcel_Click(object sender, EventArgs e)
        {
            try
            {
                var tmpAdo = new ModAdo();
                string sWhere = "";
                string sOrder = "";
                string sInitale = "";
                string sSQL = "";
                Excel.Application oXL;
                Excel.Workbook oWB;
                Excel.Worksheet oSheet;
                Excel.Range oRng;
                Excel.Range oResizeRange;
                DataTable rsExport = default(DataTable);
                string sComentaire;

                //ADODB.Field oField = default(ADODB.Field);


                int i = 0;
                int j = 0;
                string sMatricule = null;
                int lLigne = 0;
                if (!string.IsNullOrEmpty(txtDateSaisieFin.Text))
                {
                    if (txtDateSaisieFin.Text.Contains(".") || !General.IsDate(txtDateSaisieFin.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtDateSaisieFin.Focus();
                        //txtDateSaisieFin.SelectionStart = 0;
                        //txtDateSaisieFin.SelectionLength = txtDateSaisieFin.Text.Length;
                        return;
                    }
                    else
                    {
                        txtDateSaisieFin.Text = Convert.ToDateTime(txtDateSaisieFin.Text).ToShortDateString();
                        if (Convert.ToDateTime(txtDateSaisieFin.Text) < Convert.ToDateTime("01/01/1900"))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDateSaisieFin.Focus();
                            //txtDateSaisieFin.SelectionStart = 0;
                            //txtDateSaisieFin.SelectionLength = txtDateSaisieFin.Text.Length;
                            return;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(txtDateSaisieDe.Text))
                {
                    if (txtDateSaisieDe.Text.Contains(".") || !General.IsDate(txtDateSaisieDe.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtDateSaisieDe.Focus();
                        //txtDateSaisieDe.SelectionStart = 0;
                        //txtDateSaisieDe.SelectionLength = txtDateSaisieDe.Text.Length;
                        return;
                    }
                    else
                    {
                        txtDateSaisieDe.Text = Convert.ToDateTime(txtDateSaisieDe.Text).ToShortDateString();
                        if (Convert.ToDateTime(txtDateSaisieDe.Text) < Convert.ToDateTime("01/01/1900"))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDateSaisieDe.Focus();
                            //txtDateSaisieDe.SelectionStart = 0;
                            //txtDateSaisieDe.SelectionLength = txtDateSaisieDe.Text.Length;
                            return;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(txtIntereAu.Text))
                {
                    if (txtIntereAu.Text.Contains(".") || !General.IsDate(txtIntereAu.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtIntereAu.Focus();
                        //txtIntereAu.SelectionStart = 0;
                        //txtIntereAu.SelectionLength = txtIntereAu.Text.Length;
                        return;
                    }
                    else
                    {
                        txtIntereAu.Text = Convert.ToDateTime(txtIntereAu.Text).ToShortDateString();
                        if (Convert.ToDateTime(txtIntereAu.Text) < Convert.ToDateTime("01/01/1900"))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtIntereAu.Focus();
                            //txtIntereAu.SelectionStart = 0;
                            //txtIntereAu.SelectionLength = txtIntereAu.Text.Length;
                            return;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(txtinterDe.Text))
                {
                    if (txtinterDe.Text.Contains(".") || !General.IsDate(txtinterDe.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtinterDe.Focus();
                        //txtinterDe.SelectionStart = 0;
                        //txtinterDe.SelectionLength = txtinterDe.Text.Length;
                        return;
                    }
                    else
                    {
                        txtinterDe.Text = Convert.ToDateTime(txtinterDe.Text).ToShortDateString();
                        if (Convert.ToDateTime(txtinterDe.Text) < Convert.ToDateTime("01/01/1900"))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtinterDe.Focus();
                            //txtinterDe.SelectionStart = 0;
                            //txtinterDe.SelectionLength = txtinterDe.Text.Length;
                            return;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(txtDatePrevue1.Text))
                {
                    if (txtDatePrevue1.Text.Contains(".") || !General.IsDate(txtDatePrevue1.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtDatePrevue1.Focus();
                        //txtDatePrevue1.SelectionStart = 0;
                        //txtDatePrevue1.SelectionLength = txtDatePrevue1.Text.Length;
                        return;
                    }
                    else
                    {
                        txtDatePrevue1.Text = Convert.ToDateTime(txtDatePrevue1.Text).ToShortDateString();
                        if (Convert.ToDateTime(txtDatePrevue1.Text) < Convert.ToDateTime("01/01/1900"))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDatePrevue1.Focus();
                            //txtDatePrevue1.SelectionStart = 0;
                            //txtDatePrevue1.SelectionLength = txtDatePrevue1.Text.Length;
                            return;
                        }
                    }
                }

                if (!string.IsNullOrEmpty(txtDatePrevue2.Text))
                {
                    if (txtDatePrevue2.Text.Contains(".") || !General.IsDate(txtDatePrevue2.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisi une date valide, veuillez en saisir une autre.", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        txtDatePrevue2.Focus();
                        //txtDatePrevue2.SelectionStart = 0;
                        //txtDatePrevue2.SelectionLength = txtDatePrevue2.Text.Length;
                        return;
                    }
                    else
                    {
                        txtDatePrevue2.Text = Convert.ToDateTime(txtDatePrevue2.Text).ToShortDateString();
                        if (Convert.ToDateTime(txtDatePrevue2.Text) < Convert.ToDateTime("01/01/1900"))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une date supérieure au 01/01/1900", "Date non valide", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            txtDatePrevue2.Focus();
                            //txtDatePrevue2.SelectionStart = 0;
                            //txtDatePrevue2.SelectionLength = txtDatePrevue2.Text.Length;
                            return;
                        }
                    }
                }
                var _with15 = this;
                // stocke la position de la fiche immeuble
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDispatch, _with15.ssIntervention.ActiveRow != null ? _with15.ssIntervention.ActiveRow.Cells["NoIntervention"].Value.ToString() : "",
                 "", _with15.txtinterDe.Text, _with15.txtIntereAu.Text, _with15.cmbStatus.Text, _with15.cmbIntervenant.Text, cmbArticle.Text,
                _with15.txtCommentaire.Text, _with15.cmbCodeimmeuble.Text, ssComboActivite.Text, _with15.txtINT_AnaCode.Text, _with15.txtDateSaisieDe.Text,
                _with15.txtDateSaisieFin.Text, _with15.txtDatePrevue1.Text, _with15.txtDatePrevue2.Text, _with15.cmbCodeGerant.Text,
                _with15.cmbEtatDevis.Text, Convert.ToString(_with15.chkAchat.CheckState), "", "", "", "", "", "", "", "", cmdDispatch.Text, txtCommentaire.Text);

                bool allowadditionalInfos = false;
                //===> Mondir le 05.05.2021, https://groupe-dt.mantishub.io/view.php?id=912
                var aut = "SELECT     AUT_Formulaire" +
                           " From AUT_Autorisation" +
                           " WHERE  AUT_Nom = '" + General.fncUserName() +
                           "' and    AUT_Objet = 'ExportExcelAdditionalInfosUserDispatch' AND AUT_Formulaire = 'UserDispatch'";

                using (var tmpModAdo = new ModAdo())
                {
                    allowadditionalInfos = !tmpAdo.fc_ADOlibelle(aut).IsNullOrEmpty();
                }
                //===> Fin Modif Mondir

                if (bExportDetail)
                {

                    sSQL = "SELECT     CRCL_GroupeClient.CRCL_Code AS [Code Groupe], CRCL_GroupeClient.CRCL_RaisonSocial AS [Nom Groupe], Table1.Code1, "
                         + " Table1.Nom AS [Raison social], Table1.Adresse1, Table1.Adresse2, Table1.CodePostal, Table1.Ville, Immeuble.CodeImmeuble, Immeuble.Adresse, "
                         + " Immeuble.Adresse2_Imm, Immeuble.CodePostal AS [CP imm], Immeuble.Ville AS [CP Ville], Intervention.NoIntervention, Intervention.DateRealise, "
                         + " Intervention.CodeEtat, Intervention.Designation, Intervention.Commentaire, Personnel.Matricule, Personnel.Nom AS Expr3, Personnel.Prenom ";


                    sSQL = sSQL + " ,FactureManuelleEnTete.NoFacture, FactureManuellePied.MontantHT";

                    //===> Mondir le 05.05.2021, https://groupe-dt.mantishub.io/view.php?id=912
                    //===> Mondir le 10.05.2021, https://groupe-dt.mantishub.io/view.php?id=912#c5986 added some columns
                    //===> Mondir le 14.05.2021, https://groupe-dt.mantishub.io/view.php?id=912#c6023
                    if (allowadditionalInfos)
                    {
                        sSQL +=
                            " , Immeuble.Latitude AS 'Latitude Immeuble', Immeuble.Longitude AS 'Longitude Immeuble', Intervention.GPS_Latt AS 'Latitude Début Intervention', Intervention.GPS_Longi AS 'Longitude Début Intervention', " +
                            "Intervention.GPS_LattFin AS 'Latitude Fin Intervention', Intervention.GPS_LongiFin AS 'Longitude Fin Intervention', Intervention.ErrGpsHZDebut AS 'Anomalie HZ Début', " +
                            "Intervention.GPS_LatDebHZ AS 'Latitude HZ début', Intervention.GPS_LongiDebHZ AS 'Longitude HZ début', '' AS 'Distance HZ début', Intervention.ErrGpsHZFin AS 'Anomalie HZ Fin'," +
                            "Intervention.GPS_LatFinbHZ AS 'Latitude HZ fin', Intervention.GPS_LongFinHZ AS 'Longitude HZ fin', '' AS 'Distance HZ Fin' ";
                    }
                    //===> Fin Modif Mondir

                    sSQL = sSQL + " FROM         CRCL_GroupeClient RIGHT OUTER JOIN "
                         + " Table1 ON CRCL_GroupeClient.CRCL_Code = Table1.CRCL_Code RIGHT OUTER JOIN "
                         + " Personnel RIGHT OUTER JOIN "
                         + " Intervention INNER JOIN "
                         + " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble ON Personnel.Matricule = Intervention.Intervenant ON "
                         + " Table1.Code1 = Immeuble.Code1 LEFT OUTER JOIN "
                         + " FactureManuellePied RIGHT OUTER JOIN "
                         + " FactureManuelleEnTete ON FactureManuellePied.CleFactureManuelle = FactureManuelleEnTete.CleFactureManuelle ON "
                         + " Intervention.NoIntervention = FactureManuelleEnTete.NoIntervention";
                    bExportDetail = false;
                }
                else
                {
                    //===> Mondir le 06.07.2020, demandé par Fabiola, https://groupe-dt.mantishub.io/view.php?id=1794#c4615
                    //===> I have added CRCL_Code and CRCL_RaisonSocial to the select list and join with  CRCL_GroupeClient
                    sSQL = "SELECT   CRCL_GroupeClient.CRCL_Code AS [Code Groupe], CRCL_GroupeClient.CRCL_RaisonSocial AS [Nom Groupe],  Intervention.DateRealise,"
                          + "  Intervention.CodeImmeuble,"
                          + " Immeuble.CodePostal, Intervention.CodeEtat, Intervention.Designation, "
                          + " Intervention.Intervenant,Personnel.Nom, GestionStandard.NumFicheStandard AS [N°Affaire],"
                          + " Intervention.HeureDebut, Intervention.HeureFin, Intervention.Duree,"
                          + " FactureManuelleEnTete.NoFacture,"
                          + " FactureManuellePied.MontantHT, Intervention.NoIntervention,Personnel.CRespExploit,Personnel.CSecteur ";

                    //===> Mondir le 05.05.2021, https://groupe-dt.mantishub.io/view.php?id=912
                    //===> Mondir le 14.05.2021, https://groupe-dt.mantishub.io/view.php?id=912#c6023
                    if (allowadditionalInfos)
                    {
                        sSQL +=
                            " , Immeuble.Latitude AS 'Latitude Immeuble', Immeuble.Longitude AS 'Longitude Immeuble', Intervention.GPS_Latt AS 'Latitude Début Intervention', Intervention.GPS_Longi AS 'Longitude Début Intervention', " +
                            "Intervention.GPS_LattFin AS 'Latitude Fin Intervention', Intervention.GPS_LongiFin AS 'Longitude Fin Intervention', Intervention.ErrGpsHZDebut AS 'Anomalie HZ Début', " +
                            "Intervention.GPS_LatDebHZ AS 'Latitude HZ début', Intervention.GPS_LongiDebHZ AS 'Longitude HZ début', '' AS 'Distance HZ début', Intervention.ErrGpsHZFin AS 'Anomalie HZ Fin'," +
                            "Intervention.GPS_LatFinbHZ AS 'Latitude HZ fin', Intervention.GPS_LongFinHZ AS 'Longitude HZ fin', '' AS 'Distance HZ Fin' ";
                    }
                    //===> Fin Modif Mondir

                    //'== ajout de la table energie.
                    sSQL = sSQL + "FROM         TypeCodeEtat RIGHT OUTER JOIN"
                          + " Intervention ON TypeCodeEtat.CodeEtat = Intervention.CodeEtat LEFT OUTER JOIN"
                          + " Personnel ON Intervention.Intervenant = Personnel.Matricule INNER JOIN"
                          + " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble INNER JOIN"
                          + " Table1 ON Immeuble.Code1 = Table1.Code1 LEFT OUTER JOIN"
                          + " FactureManuelleEnTete ON Intervention.NoFacture = FactureManuelleEnTete.NoFacture LEFT OUTER JOIN"
                          + " FactureManuellePied ON FactureManuelleEnTete.CleFactureManuelle = FactureManuellePied.CleFactureManuelle INNER JOIN"
                          + " GestionStandard ON Intervention.NumFicheStandard = GestionStandard.NumFicheStandard LEFT OUTER JOIN"
                          + " TechReleve ON Immeuble.CodeImmeuble = TechReleve.CodeImmeuble LEFT OUTER JOIN"
                          + " DevisEnTete ON GestionStandard.NoDevis = DevisEnTete.NumeroDevis " +
                          " LEFT OUTER JOIN CRCL_GroupeClient ON CRCL_GroupeClient.CRCL_Code = Table1.CRCL_Code";
                }

                var _with11 = this;

                //===> Mondir le 10.12.2020 pour corrigé https://groupe-dt.mantishub.io/view.php?id=2121
                if (!string.IsNullOrEmpty(_with11.cmbDepan.Text))
                {
                    var sSQLInitia = "SELECT Initiales FROM PERSONNEL where matricule ='" + StdSQLchaine.gFr_DoublerQuote(cmbDepan.Text) + "'";
                    using (var tmp = new ModAdo())
                        sSQLInitia = tmp.fc_ADOlibelle(sSQLInitia);

                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Personnel.CRespExploit" + (chkExclDepan.CheckState == 0 ? "=" : "<>") + "'" + sSQLInitia + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Personnel.CRespExploit" + (chkExclDepan.CheckState == 0 ? "=" : "<>") + "'" + sSQLInitia + "'";
                    }
                }
                //===> Fin Modif Mondir

                if (!string.IsNullOrEmpty(_with11.cmbCodeimmeuble.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.CodeImmeuble" + (chkExclImm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCodeimmeuble.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.CodeImmeuble" + (chkExclImm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCodeimmeuble.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.cmbCodeGerant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Immeuble.Code1" + (chkExclGerant.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCodeGerant.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Immeuble.Code1" + (chkExclGerant.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCodeGerant.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtCP.Text))
                {
                    if (txtCP.Text.Length == 2)
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE Immeuble.CodePostal like '" + _with11.txtCP.Text + "%'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND Immeuble.CodePostal like '" + _with11.txtCP.Text + "%'";
                        }
                    }
                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE Immeuble.CodePostal ='" + _with11.txtCP.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND Immeuble.CodePostal ='" + _with11.txtCP.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_with11.cmbIntervenant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.Intervenant" + (chkExclInterv.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbIntervenant.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Intervention.Intervenant" + (chkExclInterv.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbIntervenant.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(cmdDispatch.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  Intervention.Dispatcheur" + (chkExclDispatch.CheckState == 0 ? "=" : "<>") + "'" + cmdDispatch.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Intervention.Dispatcheur" + (chkExclDispatch.CheckState == 0 ? "=" : "<>") + "'" + cmdDispatch.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtCommentaire.Text))
                {
                    sComentaire = txtCommentaire.Text.Replace(" ", "%");
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        //===> Mondir le 15.04.2021, remove "'" https://groupe-dt.mantishub.io/view.php?id=2405
                        //sWhere = " WHERE  Intervention.Commentaire like'%" + "'" + StdSQLchaine.gFr_DoublerQuote(sComentaire) + "%'";
                        sWhere = " WHERE  Intervention.Commentaire like'%" + "'" + StdSQLchaine.gFr_DoublerQuote(sComentaire) + "%'";
                    }
                    else
                    {
                        //===> Mondir le 15.04.2021, remove "'" https://groupe-dt.mantishub.io/view.php?id=2405
                        //sWhere = sWhere + " and Intervention.Commentaire like '%" + "'" + StdSQLchaine.gFr_DoublerQuote(sComentaire) + "%'";
                        sWhere = sWhere + " and Intervention.Commentaire like '%" + StdSQLchaine.gFr_DoublerQuote(sComentaire) + "%'";
                    }
                }

                if (!string.IsNullOrEmpty(cmbEnergie.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE TechReleve.CodeEnergie1 " + (chkExclEnergie.CheckState == 0 ? "=" : "<>") + "'" + StdSQLchaine.gFr_DoublerQuote(_with11.cmbEnergie.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " and TechReleve.CodeEnergie1 " + (chkExclEnergie.CheckState == 0 ? "=" : "<>") + "'" + StdSQLchaine.gFr_DoublerQuote(_with11.cmbEnergie.Text) + "'";
                    }

                }

                if (!string.IsNullOrEmpty(_with11.cmbCommercial.Text))
                {
                    if (General.sVersionImmParComm == "1")
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE Immeuble.CodeCommercial " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCommercial.Text + "'";
                        }
                        else
                        {
                            sWhere = sWhere + " AND Immeuble.CodeCommercial " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCommercial.Text + "'";
                        }
                    }

                    else
                    {
                        if (string.IsNullOrEmpty(sWhere))
                        {
                            sWhere = " WHERE (Table1.Commercial" + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCommercial.Text + "' OR Immeuble.CodeCommercial " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCommercial.Text + "')";
                        }
                        else
                        {
                            sWhere = sWhere + " AND (Table1.Commercial" + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCommercial.Text + "' OR Immeuble.CodeCommercial " + (chkExclComm.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbCommercial.Text + "')";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(_with11.cmbStatus.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.CodeEtat" + (chkExclStatut.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbStatus.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.CodeEtat" + (chkExclStatut.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbStatus.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtinterDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " where Intervention.daterealise>='" + Convert.ToDateTime(_with11.txtinterDe.Text).ToString(General.FormatDateSQL) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Intervention.daterealise>='" + Convert.ToDateTime(_with11.txtinterDe.Text).ToString(General.FormatDateSQL) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtIntereAu.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = sWhere + " and  Intervention.daterealise<='" + Convert.ToDateTime(_with11.txtIntereAu.Text).ToString(General.FormatDateSQL) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and  Intervention.daterealise<='" + Convert.ToDateTime(_with11.txtIntereAu.Text).ToString(General.FormatDateSQL) + "'";
                    }
                }

                //===> Mondir le 15.04.2021, je ne sais pas pourquoi il y a deux champs commentaire txtIntercom.Text et txtCommentaire
                //===> https://groupe-dt.mantishub.io/view.php?id=2405
                //if (!string.IsNullOrEmpty(txtIntercom.Text))
                //{
                //    if (string.IsNullOrEmpty(sWhere))
                //    {
                //        sWhere = " where Intervention.Commentaire like '%" + txtIntercom.Text + "'";
                //    }
                //    else
                //    {
                //        sWhere = sWhere + "and  Intervention.Commentaire LIKE '%" + txtIntercom.Text + "'";
                //    }
                //}

                if (!string.IsNullOrEmpty(txtNoIntervention.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " where Intervention.NoIntervention = " + _with11.txtNoIntervention.Text + "";
                    }
                    else
                    {
                        sWhere = sWhere + " and  Intervention.NoIntervention = " + _with11.txtNoIntervention.Text + "";
                    }
                }

                if (!string.IsNullOrEmpty(txtINT_AnaCode.Text))
                {
                    //==== modif le 25 10 2011, le chef de secteur est attaché à l'immeuble.
                    if (General.sVersionImmParComm == "1")
                    {
                        //=== modif temporaire à mettre à jour.
                        if (General.sRespExploitParImmeuble == "1")
                        {

                            if (string.IsNullOrEmpty(sWhere))
                            {
                                sWhere = " WHERE Immeuble.CodeRespExploit ='" + txtINT_AnaCode.Text + "'";
                            }
                            else
                            {
                                sWhere = sWhere + " AND Immeuble.CodeRespExploit ='" + txtINT_AnaCode.Text + "'";
                            }

                        }
                        else if (General.sChefDeSecteurParImmeuble == "1")
                        {

                            if (string.IsNullOrEmpty(sWhere))
                            {
                                sWhere = " WHERE Immeuble.CodeChefSecteur ='" + txtINT_AnaCode.Text + "'";
                            }
                            else
                            {
                                sWhere = sWhere + " AND Immeuble.CodeChefSecteur ='" + txtINT_AnaCode.Text + "'";
                            }

                        }
                        else
                        {
                            sInitale = tmpAdo.fc_ADOlibelle("SELECT Initiales FROM PERSONNEL WHERE MATRICULE ='" + StdSQLchaine.gFr_DoublerQuote(txtINT_AnaCode.Text) + "'");
                            if (string.IsNullOrEmpty(sWhere))
                            {
                                //sWhere = " WHERE Personnel.CSecteur = '" & .txtINT_AnaCode & "'"
                                sWhere = " WHERE Personnel.CRespExploit = '" + sInitale + "'";
                            }
                            else
                            {
                                //sWhere = sWhere & " AND  Personnel.CSecteur = '" & .txtINT_AnaCode & "'"
                                sWhere = sWhere + " AND  Personnel.CRespExploit = '" + sInitale + "'";
                            }

                        }
                    }
                    else
                    {

                        if (string.IsNullOrEmpty(sWhere))
                        {
                            //sWhere = " WHERE Personnel.CSecteur = '" & .txtINT_AnaCode & "'"
                            sWhere = " WHERE Personnel.CSecteur = '" + txtINT_AnaCode.Text + "'";
                        }
                        else
                        {
                            //sWhere = sWhere & " AND  Personnel.CSecteur = '" & .txtINT_AnaCode & "'"
                            sWhere = sWhere + " AND  Personnel.CSecteur = '" + txtINT_AnaCode.Text + "'";
                        }
                    }
                }

                if (!string.IsNullOrEmpty(ssComboActivite.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " where Intervention.INT_AnaCode LIKE '" + _with11.ssComboActivite.Text + "%'";
                    }
                    else
                    {
                        sWhere = sWhere + " and  Intervention.INT_AnaCode LIKE '" + _with11.ssComboActivite.Text + "%'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.cmbArticle.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " where Intervention.Article" + (chkExclArticle.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbArticle.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and  Intervention.Article" + (chkExclArticle.CheckState == 0 ? "=" : "<>") + "'" + _with11.cmbArticle.Text + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtDateSaisieDe.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " where Intervention.dateSaisie>='" + Convert.ToDateTime(_with11.txtDateSaisieDe.Text).ToString(General.FormatDateSansHeureSQL) + " 00:00:00" + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and Intervention.dateSaisie>='" + Convert.ToDateTime(_with11.txtDateSaisieDe.Text).ToString(General.FormatDateSansHeureSQL) + " 00:00:00" + "'";

                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtDateSaisieFin.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " where  Intervention.dateSaisie<='" + Convert.ToDateTime(_with11.txtDateSaisieFin.Text).ToString(General.FormatDateSansHeureSQL) + " 23:59:59" + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " and  Intervention.dateSaisie<='" + Convert.ToDateTime(_with11.txtDateSaisieFin.Text).ToString(General.FormatDateSansHeureSQL) + " 23:59:59" + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtDatePrevue1.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " where  Intervention.DatePrevue>='" + Convert.ToDateTime(_with11.txtDatePrevue1.Text).ToString(General.FormatDateSQL) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " and  Intervention.DatePrevue>='" + Convert.ToDateTime(_with11.txtDatePrevue1.Text).ToString(General.FormatDateSQL) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.txtDatePrevue2.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = sWhere + " where  Intervention.DatePrevue<='" + Convert.ToDateTime(_with11.txtDatePrevue2.Text).ToString(General.FormatDateSQL) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " and  Intervention.DatePrevue<='" + Convert.ToDateTime(_with11.txtDatePrevue2.Text).ToString(General.FormatDateSQL) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(_with11.cmbEtatDevis.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DevisEntete.CodeEtat='" + _with11.cmbEtatDevis.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND DevisEntete.CodeEtat='" + _with11.cmbEtatDevis.Text + "'";
                    }
                }

                if (_with11.chkAchat.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.Achat<>0 and NOT Intervention.Achat IS NULL ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.Achat<>0 and NOT Intervention.Achat IS NULL ";
                    }
                }

                if (chkAffaireSansFact.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE gestionStandard.NumFicheStandard NOT IN(SELECT NumFicheStandard FROM INTERVENTION WHERE Intervention.Nofacture is not null and Intervention.Nofacture <> '') ";
                    }
                    else
                    {
                        sWhere = sWhere + " and gestionStandard.NumFicheStandard NOT IN(SELECT NumFicheStandard FROM INTERVENTION WHERE Intervention.Nofacture is not null and Intervention.Nofacture <> '') ";
                    }
                }

                if (chkAffaireAvecFact.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE gestionStandard.NumFicheStandard  IN(SELECT NumFicheStandard FROM INTERVENTION WHERE Intervention.Nofacture is not null and Intervention.Nofacture <> '') ";
                    }
                    else
                    {
                        sWhere = sWhere + " and gestionStandard.NumFicheStandard  IN(SELECT NumFicheStandard FROM INTERVENTION WHERE Intervention.Nofacture is not null and Intervention.Nofacture <> '') ";
                    }
                }

                if (CHKP1.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  (Immeuble.P1= 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  (Immeuble.P1 = 1) ";
                    }

                }

                if (CHKP2.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  (Immeuble.P2= 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  (Immeuble.P2 = 1) ";
                    }

                }

                if (CHKP3.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  (Immeuble.P3= 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  (Immeuble.P3 = 1) ";
                    }

                }

                if (CHKP4.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  (Immeuble.P4 = 1) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  (Immeuble.P4 = 1) ";
                    }

                }

                if (_with11.chkVisiteEntretien.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.VisiteEntretien = 1 ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.VisiteEntretien = 1";
                    }
                }

                if (_with11.chkDevisEtablir.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.DevisEtablir = 1 ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.DevisEtablir = 1";
                    }
                }

                if (_with11.chkDapeau.CheckState == CheckState.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.Drapeau = 1 ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.Drapeau = 1";
                    }
                }

                if (!string.IsNullOrEmpty(txtCodeGestionnaire.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE immeuble.CodeGestionnaire " + (chkExclGestionnaire.CheckState == 0 ? "=" : "<>") + "'" + _with11.txtCodeGestionnaire.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " and immeuble.CodeGestionnaire" + (chkExclGestionnaire.CheckState == 0 ? "=" : "<>") + "'" + _with11.txtCodeGestionnaire.Text + "'";
                    }
                }

                if (chkDevisPDA.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  Intervention.Nointervention IN (SELECT NoIntervention FROM  DEM_DevisEnTeteMobile) ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  Intervention.Nointervention IN (SELECT NoIntervention FROM  DEM_DevisEnTeteMobile)  ";
                    }
                }

                if (chkEncaissement.Checked)
                {
                    var chkencaiss = General.nz(chkEncaissement.Checked, 0).ToString() == "True" ? "1" : "0";
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  Intervention.Encaissement =" + chkencaiss;
                    }
                    else
                    {
                        sWhere = sWhere + "  AND  Intervention.Encaissement =" + chkencaiss;
                    }
                }

                if (chkEncRex.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  Intervention.nointervention IN (SELECT     NoIntervention From InterEncaissement  WHERE(ValideREX = 1))";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Intervention.nointervention IN(SELECT NoIntervention From InterEncaissement WHERE (ValideREX = 1))";
                    }
                }

                if (chkEncCompta.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE  Intervention.nointervention IN (SELECT     NoIntervention From InterEncaissement WHERE (ValideCompta = 1)";
                    }
                    else
                    {
                        sWhere = sWhere + "  AND  Intervention.nointervention IN (SELECT     NoIntervention From InterEncaissement WHERE (ValideCompta = 1))";
                    }
                }

                if (!string.IsNullOrEmpty(sWhere))
                {
                    sSQL = sSQL + sWhere;
                }
                //sOrder = " ORDER BY GestionStandard.NumFicheStandard";
                //sSQL = sSQL + sOrder;
                sOrder = lblOrderBy.Text;
                sSQL = sSQL + sOrder;

                modAdorsintervention = new ModAdo();
                rsintervention = new DataTable();
                rsintervention = modAdorsintervention.fc_OpenRecordSet(sSQL);
                //txtTotal.Text = General.nz(rsintervention.Rows.Count, "0").ToString();

                if (rsintervention.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune intervention pour ces paramétres.", "Présélection Dispatch", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                // Start Excel and get Application object.
                oXL = new Excel.Application();
                oXL.Visible = false;

                // Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;
                ModAdo rs = new ModAdo();
                rsExport = rs.fc_OpenRecordSet(sSQL);
                if (rsExport.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour ces critères.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                //== affiche les noms des champs.
                i = 1;

                //== affiche les noms des champs.

                i = 1;
                foreach (DataColumn oField in rsExport.Columns)
                {


                    oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Cells[1, i].value = oField.ColumnName;
                    i = i + 1;
                }


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                // Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Interior.ColorIndex = 15;
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;
                lLigne = 2;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                //sSQL += " , Immeuble.Latitude AS 'Latitude Immeuble', Immeuble.Longitude AS 'Longitude Immeuble', " +
                //            "Intervention.ErrGpsHZDebut AS 'Anomalie Début HZ', Intervention.GPS_Latt AS 'Latitude Début Intervention', Intervention.GPS_Longi AS 'Longitude Début Intervention', " +
                //            "Intervention.GPS_LattFin AS 'Latitude Fin Intervention', Intervention.GPS_LongiFin AS 'Longitude Fin Intervention', '' AS 'La distance du point', '' AS 'La distance du point en fin d''intervention', " +
                //            "Intervention.ErrGpsHZFin AS 'Anomalie HZ Fin', Intervention.GPS_LatDebHZ AS 'Latitude HZ début', Intervention.GPS_LongiDebHZ AS 'Longitude HZ début', " +
                //            "Intervention.GPS_LatFinbHZ AS 'Latitude HZ fin', Intervention.GPS_LongFinHZ AS 'Longitude HZ fin'";

                foreach (DataRow rsExportRows in rsExport.Rows)
                {
                    j = 1;
                    //  oSheet.Cells(lLigne, 1).value = sANT_MatCode
                    foreach (DataColumn rsExportCol in rsExport.Columns)
                    {
                        string stemp = "";

                        stemp = rsExportRows[rsExportCol.ColumnName] + "";

                        //===> Mondir le 05.05.2021 https://groupe-dt.mantishub.io/view.php?id=912
                        if (allowadditionalInfos)
                        {

                            var dbLatImm = Convert.ToDouble(General.nz(rsExportRows["Latitude Immeuble"], 0));
                            var dbLonImm = Convert.ToDouble(General.nz(rsExportRows["Longitude Immeuble"], 0));

                            if (rsExportCol.ColumnName.ToUpper() == "Anomalie HZ Début".ToUpper() || rsExportCol.ColumnName.ToUpper() == "Anomalie HZ Fin".ToUpper())
                            {
                                if (rsExportRows[rsExportCol.ColumnName].ToString() == "1")
                                {
                                    stemp = "Oui";
                                }
                                else if (rsExportRows[rsExportCol.ColumnName].ToString() == "0")
                                {
                                    stemp = "";
                                }
                            }
                            else if (rsExportCol.ColumnName.ToUpper() == "Distance HZ début".ToUpper())
                            {

                                if (General.IsNumeric(rsExportRows["Latitude HZ début"].ToString()) && General.IsNumeric(rsExportRows["Longitude HZ début"].ToString()))
                                {
                                    var dbLatDbHZ = Convert.ToDouble(General.nz(rsExportRows["Latitude HZ début"], 0));
                                    var dbLonDbHZ = Convert.ToDouble(General.nz(rsExportRows["Longitude HZ début"], 0));

                                    var dis = Convert.ToString(General.FncArrondir(ModGPS.fc_DistanceGPS(dbLatImm, dbLonImm, dbLatDbHZ, dbLonDbHZ, "K"), 3));
                                    stemp = dis.ToString();
                                }
                            }
                            else if (rsExportCol.ColumnName.ToUpper() == "Distance HZ Fin".ToUpper())
                            {

                                if (General.IsNumeric(rsExportRows["Latitude HZ fin"].ToString()) && General.IsNumeric(rsExportRows["Longitude HZ fin"].ToString()))
                                {
                                    var dbLatFinHZ = Convert.ToDouble(General.nz(rsExportRows["Latitude HZ fin"], 0));
                                    var dbLonFinHZ = Convert.ToDouble(General.nz(rsExportRows["Longitude HZ fin"], 0));

                                    var dis = Convert.ToString(General.FncArrondir(ModGPS.fc_DistanceGPS(dbLatImm, dbLonImm, dbLatFinHZ, dbLonFinHZ, "K"), 3));
                                    stemp = dis.ToString();
                                }
                            }
                            else
                            {
                                stemp = rsExportRows[rsExportCol.ColumnName] + "";
                            }
                        }
                        //===> Fin Modif Mondir

                        if (General.UCase(rsExportCol.ColumnName) == General.UCase("HeureDebut")
                            || General.UCase(rsExportCol.ColumnName) == General.UCase("HeureFin"))
                        {
                            oSheet.Cells[lLigne, j + 0].NumberFormat = "HH:mm";
                            if (stemp.Contains("/1900"))
                            {
                                stemp = Convert.ToDateTime(rsExportRows[rsExportCol.ColumnName]).ToString("HH:mm");
                            }

                            // Mondir : to fix https://groupe-dt.mantishub.io/view.php?id=1723
                            if (General.IsDate(stemp))
                                stemp = Convert.ToDateTime(stemp).ToString("HH:mm");
                        }
                        else if (General.UCase(rsExportCol.ColumnName) == General.UCase("Duree"))
                        {
                            //' oSheet.Cells(lLigne, j + cCol).NumberFormat = "h:mm:ss"
                            oSheet.Cells[lLigne, j + 0].NumberFormat = "[H]:mm";

                            if (stemp.Contains("/1900"))
                            {
                                stemp = Convert.ToDateTime(rsExportRows[rsExportCol.ColumnName]).ToString("H:mm");
                            }
                            else if (string.IsNullOrEmpty(stemp))
                            {
                                stemp = "00:00:00";
                            }
                            else
                            {
                                stemp = Convert.ToDateTime(rsExportRows[rsExportCol.ColumnName]).ToString("H:mm");

                            }

                        }

                        if (General.UCase(rsExportCol.ColumnName) == General.UCase("DateRealise"))
                        {
                            //'Stop
                            if (General.IsDate(stemp))
                            {
                                oSheet.Cells[lLigne, j + 0].value = Convert.ToDateTime(stemp);
                            }
                            else
                            {
                                oSheet.Cells[lLigne, j + 0].value = stemp;
                            }

                        }
                        else
                        {
                            oSheet.Cells[lLigne, j + 0].value = stemp;

                        }
                        //oSheet.Cells[lLigne, j + 0].value = stemp;
                        j = j + 1;
                    }
                    lLigne = lLigne + 1;
                    //_with14.MoveNext();
                }

                ///oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oXL.Visible = true;
                oXL.UserControl = true;
                oRng = null;
                oSheet = null;
                oWB = null;
                //oXL.Quit();
                oXL = null;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                //_with14.Close();
                rsExport = null;
                return;
            }
            catch (Exception ex)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Error: ", "" + ex.Message, MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFindEnergie_Click(object sender, EventArgs e)
        {
            cmbEnergie_KeyPress(cmbEnergie, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFindGestionnaire_Click(object sender, EventArgs e)
        {
            txtCodeGestionnaire_KeyPress(cmbCodeGerant, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdGerant_Click(object sender, EventArgs e)
        {
            //cmbCodeGerant_KeyPress
            //cmdGerant_Clic(cmdGerant, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
            cmbEnergie_KeyPress(cmbEnergie, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdHistorique_Click(object sender, EventArgs e)
        {
            if (ssIntervention.ActiveRow != null)
            {
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocHistoInterv, ssIntervention.ActiveRow.Cells["CodeImmeuble"].Value.ToString());
                View.Theme.Theme.Navigate(typeof(UserDocHistoriqueInterv));
                // ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocHistoInterv);
            }
            else
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune immeuble est sélectionnée.", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdPrestations_Click(object sender, EventArgs e)
        {
            DispatchDevis.Forms.frmPrestations frm = new DispatchDevis.Forms.frmPrestations();
            frm.ShowDialog();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechDepan_Click(object sender, EventArgs e)
        {
            cmbDepan_KeyPress(cmbDepan, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// Tested 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheCommercial_Click(object sender, EventArgs e)
        {
            cmbCommercial_KeyPress(cmbCommercial, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            cmbCodeimmeuble_KeyPress(cmbCodeimmeuble, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheIntervenant_Click(object sender, EventArgs e)
        {
            cmbIntervenant_KeyPress(cmbIntervenant, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheTypeOperation_Click(object sender, EventArgs e)
        {
            cmbArticle_KeyPress(cmbArticle, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            ssIntervention.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            if (ssIntervention.Rows.Count > 0)
            {
                UltraGridRow FirstRow = ssIntervention.Rows[0];
                FirstRow.Activate();
                ssIntervention.ActiveRowScrollRegion.ScrollRowIntoView(FirstRow);
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command2_Click(object sender, EventArgs e)
        {
            if (ssIntervention.Rows.Count > 0)
            {
                UltraGridRow lastRow = ssIntervention.Rows[ssIntervention.Rows.Count - 1];
                lastRow.Activate();
                ssIntervention.ActiveRowScrollRegion.ScrollRowIntoView(lastRow);


            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="C"></param>
        private void UserDispatchLoad1(Control C)
        {
            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    UserDispatchLoad1(CC);

            if (C is UltraGrid)
            {
                sheridan.fc_DeleteRegGrille(Variable.cUserDispatch, C as UltraGrid, false, true);
                //sheridan.fc_LoadDimensionGrille(this.Name, C as UltraGrid);

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="C"></param>
        private void UserDispatchLoad2(Control C)
        {
            if (C.HasChildren)
                foreach (Control CC in C.Controls)
                    UserDispatchLoad2(CC);

            if (C is UltraGrid)
            {
                //sheridan.fc_LoadDimensionGrille(this.Name, C as UltraGrid);

            }
        }

        private void loopControl(Control C)
        {
            ///this function added by mohammed
            foreach (Control c in C.Controls)
                loopControl(c);
            if (C is UltraGrid)
            {
                //sheridan.fc_SavDimensionGrille(this.Name, C as UltraGrid);
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDispatch_Load(object sender, EventArgs e)
        {
            /////this event added by mohammed
            //this.ParentForm.FormClosing += (se, ev) =>
            //{
            //    loopControl(this);
            //};
            if (General.fc_InitRegGrilles(Variable.cUserDispatch) == true)
            {

                UserDispatchLoad1(this);
                /*foreach (object objcontrole_loopVariable in this.Controls) {

                    if (objcontrole is AxSSDataWidgets_B_OLEDB.AxSSOleDBGrid) {
                        sheridan.fc_DeleteRegGrille(ref Variable.cUserDocDevis, ref objcontrole, ref false, ref true);
                        sheridan.fc_LoadDimensionGrille(ref this.Name, ref objcontrole);
                    }
                }*/
            }
            else
            {
                UserDispatchLoad2(this);
                /*foreach (object objcontrole_loopVariable in this.Controls) {
                    objcontrole = objcontrole_loopVariable;
                    if (objcontrole is AxSSDataWidgets_B_OLEDB.AxSSOleDBGrid) {
                        sheridan.fc_LoadDimensionGrille(ref this.Name, ref objcontrole);
                    }
                }*/
            }

            if (string.IsNullOrEmpty(General.sEtatAvisDePassage))
            {
                cmdAvisDePassage.Visible = true;
            }
            else
            {
                cmdAvisDePassage.Visible = false;
            }
            //ssIntervention.MouseWheel += (se, ev) =>
            //{
            //    if(ssIntervention.ActiveCell != null)
            //    {
            //        ssIntervention.ActiveCell = ssIntervention.ActiveRow.Cells[0];
            //    }
            //};
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssComboActivite_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "SELECT ACT_Code, ACT_Libelle" + " FROM  ACT_AnaActivite order by ACT_Code";
            sheridan.InitialiseCombo((this.ssComboActivite), General.sSQL, "ACT_Code");
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            boolModifRow = false;
            blnKeyPress = false;

            // ssIntervention.UpdateData();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            DataTable rsCodeEtat;
            ModAdo rsCodeEtatAdo = new ModAdo();

            if (ssIntervention.ActiveRow == null || ssIntervention.ActiveCell == null)
                return;

            if (ssIntervention.ActiveCell.Column.Index == ssIntervention.ActiveRow.Cells["CodeEtat"].Column.Index)//tested
            {
                rsCodeEtat = new DataTable();
                rsCodeEtat = rsCodeEtatAdo.fc_OpenRecordSet("SELECT TypeCodeEtat.CodeEtat FROM TypeCodeEtat WHERE CodeEtat = '" + ssIntervention.ActiveRow.Cells["CodeEtat"].Text + "'");
                if (rsCodeEtat.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un code état existant.", "Code état érroné", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                    rsCodeEtat = null;
                    return;
                }
                else
                {
                    fc_ControleEtat(ssIntervention.ActiveRow.Cells["CodeEtat"].Text);

                    if (General.UCase(ssIntervention.ActiveRow.Cells["CodeEtat"].Text) == General.UCase(General.cEtatZZ))//tested
                    {
                        using (var tmp = new ModAdo())
                        {
                            if (tmp.fc_ADOlibelle("Select NoBonDeCommande FROM BonDeCommande WHERE NoIntervention ="
                                + General.nz(ssIntervention.ActiveRow.Cells["NoIntervention"].Value, 0).ToString()) != "")
                            {
                                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention Vous allez passer en " + General.cEtatZZ
                                    + " une intervention "
                                    + " qui est attachée à un bon de commande. Voulez vous continuer?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                                {
                                    e.Cancel = true;
                                    rsCodeEtat = null;
                                    return;
                                }
                            }

                        }
                    }
                }
                rsCodeEtat.Dispose();
                rsCodeEtat = null;

                if (ssIntervention.ActiveCell.Column.Index == ssIntervention.ActiveRow.Cells["CodeEtat"].Column.Index)//tested
                {
                    if (General.UCase(ssIntervention.ActiveCell.OriginalValue) != General.UCase(ssIntervention.ActiveRow.Cells["CodeEtat"].Text))
                    {
                        //'=== modif du 05 03 2019.
                        if (bMajStatut == true)
                        {
                            General.fc_HistoStatut(Convert.ToInt32(ssIntervention.ActiveRow.Cells["NoIntervention"].Text),
                                          ssIntervention.ActiveRow.Cells["codeimmeuble"].Text, "Dispatch", ssIntervention.ActiveCell.OriginalValue.ToString(),
                                          ssIntervention.ActiveRow.Cells["CodeEtat"].Text);
                        }

                    }
                }
                //'== controle si le devis a été facturé.
                if (General.UCase(ssIntervention.ActiveRow.Cells["CodeEtat"].Text) == General.UCase("D"))
                {
                    fc_Ctrlfacture(Convert.ToInt32(General.nz(ssIntervention.ActiveRow.Cells["Nointervention"].Text, 0)));
                }

            }

            // '=== modif du 05 03 2019.
            //'If UCase(ssIntervention.Columns("INTERVENANT").Text) = UCase("445") And UCase(ssIntervention.Columns("CodeEtat").Text) <> UCase("ZZ") _
            // And bMajStatut = True And UCase(sImmeubleDefaut) = UCase(ssIntervention.Columns("codeimmeuble").Text) Then
            if (General.UCase(ssIntervention.ActiveRow.Cells["INTERVENANT"].Text) == General.UCase("445")
                && General.UCase(ssIntervention.ActiveRow.Cells["CodeEtat"].Text) == General.UCase("ZZ"))//tested
            {
                General.fc_HistoStatut(Convert.ToInt32(ssIntervention.ActiveRow.Cells["NoIntervention"].Text),
                                         ssIntervention.ActiveRow.Cells["codeimmeuble"].Text, "Dispatch", ssIntervention.ActiveRow.Cells["CodeEtat"].Text,
                                         ssIntervention.ActiveRow.Cells["CodeEtat"].Text);
            }
            //if( ssIntervention.ActiveCell.Column.Index == ssIntervention.ActiveRow.Cells["DateRealise"].Column.Index)
            //{
            //    if (ssIntervention.ActiveCell.Text=="" || ssIntervention.ActiveCell.Text== "none")
            //        ssIntervention.ActiveCell.Value = ssIntervention.ActiveCell.Value;
            //}
        }
        private void fc_Ctrlfacture(int lNoInter)
        {
            string sSQL = "";
            sSQL = "SELECT   GestionStandard.NoDevis"
                + " FROM         GestionStandard INNER JOIN"
                + " Intervention ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard"
                + " WHERE     (Intervention.NoIntervention= " + lNoInter + ")";
            var tmpAdo = new ModAdo();
            sSQL = tmpAdo.fc_ADOlibelle(sSQL);
            if (string.IsNullOrEmpty(sSQL))
                return;
            sSQL = "SELECT      FactureManuelleEnTete.NoFacture" + " FROM         GestionStandard INNER JOIN" + " Intervention " + " ON GestionStandard.NumFicheStandard = Intervention.NumFicheStandard INNER JOIN" + " FactureManuelleEnTete ON Intervention.NoFacture = FactureManuelleEnTete.NoFacture " + " INNER JOIN DevisEnTete " + " ON GestionStandard.NoDevis = DevisEnTete.NumeroDevis" + " WHERE DevisEnTete.NumeroDevis = '" + sSQL + "'" + " AND FactureManuelleEnTete.NoFacture NOT LIKE 'xx%'";


            sSQL = tmpAdo.fc_ADOlibelle(sSQL);

            if (!string.IsNullOrEmpty(sSQL))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention vous allez changer le statut d'une intervention dont le devis a déjà été facturé.", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);

            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sEtat"></param>
        /// <returns></returns>
        private string fc_ControleEtat(string sEtat)
        {
            string sAnalytiqueActivite = null;
            DataTable rsCodeEtat = default(DataTable);
            rsCodeEtat = new DataTable();
            var tmpAdorsCodeEtat = new ModAdo();
            rsCodeEtat = tmpAdorsCodeEtat.fc_OpenRecordSet("SELECT AnalytiqueDevis,AnalytiqueContrat,AnalytiqueTravaux,RazAnalytique" + " FROM TypeCodeEtat WHERE CodeEtat='" + sEtat + "'");

            if (rsCodeEtat.Rows.Count > 0)
            {
                foreach (DataRow rsCodeEtatRow in rsCodeEtat.Rows)
                {
                    // rsCodeEtat.MoveFirst();
                    if (rsCodeEtatRow["RazAnalytique"] != null && Convert.ToInt32(rsCodeEtatRow["RazAnalytique"]) == 1)
                    {
                        ssIntervention.ActiveRow.Cells["INT_AnaActivite"].Value = "";
                    }
                    else
                    {
                        //===@@@ modif du 29 05 2017, desactive Sage.
                        if (General.sDesActiveSage == "1")
                        {
                            ssIntervention.ActiveRow.Cells["INT_AnaActivite"].Value = "";
                        }
                        else
                        {
                            SAGE.fc_OpenConnSage();
                            sAnalytiqueActivite = tmpAdorsCodeEtat.fc_ADOlibelle("SELECT CA_NUM FROM F_COMPTEA WHERE ActiviteDevis="
                                + General.nz(rsCodeEtatRow["AnalytiqueDevis"], "0") + " AND ActiviteTravaux=" + General.nz(rsCodeEtatRow["AnalytiqueTravaux"], "0")
                                + " AND ActiviteContrat="
                                + General.nz(rsCodeEtatRow["AnalytiqueContrat"], "0"), false, SAGE.adoSage);
                            SAGE.fc_CloseConnSage();
                        }

                        if (!string.IsNullOrEmpty(sAnalytiqueActivite))
                        {
                            ssIntervention.ActiveRow.Cells["INT_AnaActivite"].Value = sAnalytiqueActivite;
                        }
                    }
                }
            }
            rsCodeEtat.Dispose();

            rsCodeEtat = null;

            return sEtat;

        }

        private void ssIntervention_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            // sub_afficheLibelleContrat(e.Row.Cells["CodeImmeuble"].Value.ToString());
            fc_Stocke(e.Row.Cells["NoIntervention"].Value.ToString());
            // stocke les paramétres pour la feuille de destination.
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, e.Row.Cells["NoIntervention"].Value.ToString());
            View.Theme.Theme.Navigate(typeof(UserIntervention));

        }
        private void sub_afficheLibelleContrat(string sCodeImmeuble)
        {
            DataTable rsContrat = default(DataTable);
            string sqlContrat = null;
            string sCodeContrat = null;
            string sContrat = null;

            try
            {
                General.sSQL = "";
                General.sSQL = "SELECT MAX(Contrat.DateEffet) as MaxiDate,Contrat.NumContrat AS MaxiNum, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1 " + " From Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle ";
                General.sSQL = General.sSQL + " WHERE Contrat.CodeImmeuble like '" + sCodeImmeuble + "%' AND Avenant=0";
                General.sSQL = General.sSQL + " GROUP BY Contrat.NumContrat, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1";
                var tmpAdorstmp = new ModAdo();
                var rstmp = tmpAdorstmp.fc_OpenRecordSet(General.sSQL);
                if (rstmp.Rows.Count > 0)
                {
                    sCodeContrat = rstmp.Rows[0]["MaxiNum"].ToString() + "";
                    sContrat = rstmp.Rows[0]["Designation1"].ToString() + "";
                }
                else
                {
                    sCodeContrat = "";
                    sContrat = "";
                }

                rstmp.Dispose();

                //Si j'ai un contrat, je vérifie qu'il n'est pas résilié
                if (!string.IsNullOrEmpty(sCodeContrat))//tested
                {
                    //sSQL = "SELECT Contrat.NumContrat,Contrat.LibelleCont1,FacArticle.Designation1 FROM Contrat INNER JOIN FacArticle " _
                    //& " ON Contrat.CodeArticle=FacArticle.CodeArticle " _
                    //& " WHERE (Contrat.Resiliee=0 OR (Contrat.Resiliee=1 AND (Contrat.DateFin>'" & Date & "' OR Contrat.DateFin IS NULL))) " _
                    //& " AND Contrat.CodeImmeuble='" & sCodeImmeuble & "'"

                    General.sSQL = "SELECT Contrat.NumContrat,Contrat.LibelleCont1,FacArticle.Designation1 FROM Contrat INNER JOIN FacArticle "
                        + " ON Contrat.CodeArticle=FacArticle.CodeArticle " + " WHERE (Contrat.Resiliee=0 OR (Contrat.Resiliee=1 AND (Contrat.DateFin>'"
                        + DateTime.Today + "' ))) " + " AND Contrat.CodeImmeuble='" + sCodeImmeuble + "'";

                    rstmp = tmpAdorstmp.fc_OpenRecordSet(General.sSQL);
                    if (rstmp.Rows.Count > 0)
                    {
                        lblLibelleContrat.Text = "Contrat : " + rstmp.Rows[0]["Designation1"].ToString() + "";
                        txtNumContrat.Text = rstmp.Rows[0]["NumContrat"].ToString() + "";
                        lblLibelleContrat.ForeColor = System.Drawing.ColorTranslator.FromOle(0xc00000);
                    }
                    else
                    {
                        lblLibelleContrat.Text = "Contrat : " + sContrat;
                        txtNumContrat.Text = sCodeContrat;
                        lblLibelleContrat.ForeColor = System.Drawing.ColorTranslator.FromOle(0xc0);
                    }

                    rstmp.Dispose();
                    rstmp = null;
                }
                else
                {
                    lblLibelleContrat.Text = "";
                    txtNumContrat.Text = "";
                }

                rstmp?.Dispose();
                rstmp = null;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "RechercheContrat");
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sNoIntervention"></param>
        private void fc_Stocke(string sNoIntervention)
        {
            // stocke la position de la fiche immeuble
            var _with20 = this;

            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDispatch, sNoIntervention, "",
                _with20.txtinterDe.Text, _with20.txtIntereAu.Text, _with20.cmbStatus.Text, _with20.cmbIntervenant.Text, cmbArticle.Text, _with20.txtCommentaire.Text,
            _with20.cmbCodeimmeuble.Text, ssComboActivite.Text, _with20.txtINT_AnaCode.Text, _with20.txtDateSaisieDe.Text, _with20.txtDateSaisieFin.Text,
            _with20.txtDatePrevue1.Text, _with20.txtDatePrevue2.Text, cmbCodeGerant.Text, cmbEtatDevis.Text, Convert.ToString(chkAchat.Checked),
             txtCodeGestionnaire.Text, Convert.ToString(chkAffaireAvecFact.Checked), Convert.ToString(chkAffaireSansFact.Checked),
             txtNoEnregistrement.Text, Convert.ToString(chkVisiteEntretien.Checked), cmbDepan.Text, Convert.ToString(chkDevisEtablir.Checked),
             Convert.ToString(chkDapeau.Checked), cmdDispatch.Text, txtCommentaire.Text, boolBordereauJournee == true ? true.ToString() : false.ToString(), Convert.ToString(chkDevisPDA.Checked), txtGroupe.Text, Convert.ToString(chkEncaissement.Checked)
                ///====> Modif Mondir le 02.07.2020, demande par Mantis : https://groupe-dt.mantishub.io/view.php?id=1549, ajout de chkDemandeNonTraite
                , Convert.ToString(chkEncRex.Checked), Convert.ToString(chkEncCompta.Checked), Convert.ToString(chkDemandeNonTraite.Checked));

        }

        private void ssIntervention_KeyPress(object sender, KeyPressEventArgs e)
        {
            blnKeyPress = true;

        }

        private void txtCodeGestionnaire_TextChanged(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibGestionnaire.Text = tmpAdo.fc_ADOlibelle("SELECT NomGestion" + " FROM Gestionnaire WHERE CodeGestinnaire='" + StdSQLchaine.gFr_DoublerQuote(txtCodeGestionnaire.Text) + "'");
            }
        }

        private void txtCodeGestionnaire_KeyPress(object sender, KeyPressEventArgs e)
        {

            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    requete = "SELECT    Gestionnaire.CodeGestinnaire as \"Code Gestionnaire\","
                    + " Gestionnaire.NomGestion as \"Nom Gestionnaire\","
                    + " Immeuble.CodeImmeuble as \"Code Immeuble\","
                    + " Gestionnaire.Code1 as \"code Gerant\","
                    + " Table1.Nom as \"Nom Gerant\" " +
                    " FROM Gestionnaire "
                    + " INNER JOIN"
                    + " Immeuble ON Gestionnaire.CodeGestinnaire = Immeuble.CodeGestionnaire"
                    + " INNER JOIN"
                    + " Table1 ON Gestionnaire.Code1 = Table1.Code1";
                    SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'un gestionnaire" };
                    fg.SetValues(new Dictionary<string, string> { { "CodeGestinnaire", txtCodeGestionnaire.Text } });

                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    // charge les enregistrements de l'immeuble
                    txtCodeGestionnaire.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fg.Dispose(); fg.Close();
                };
                    fg.ugResultat.KeyDown += (se, ev) =>
                    {
                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        { // charge les enregistrements de l'immeuble
                            txtCodeGestionnaire.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fg.Dispose(); fg.Close();
                        }

                    };
                    fg.StartPosition = FormStartPosition.CenterScreen;
                    fg.ShowDialog();

                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCodeGestionnaire_Click");
            }
        }


        private void txtDateSaisieDe_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (General.fc_ValidateDate(txtDateSaisieDe) == true)
                {
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }
            }
        }

        private void txtDateSaisieFin_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {

                if (General.fc_ValidateDate(txtDateSaisieFin) == true)
                {
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }
            }
        }

        private void txtINT_AnaCode_BeforeDropDown(object sender, CancelEventArgs e)
        {
            General.sSQL = "";
            //    sSql = "Select sec_Code, Sec_Libelle from SEC_Secteur "
            //sSQL = "Select Personnel.Initiales,Personnel.Nom FROM Personnel WHERE Personnel.Initiales = Personnel.CSecteur "
            //InitialiseCombo txtINT_AnaCode, Adodc7, sSQL, "Initiales"

            General.sSQL = "SELECT MATRICULE, Nom, Prenom";
            General.sSQL = General.sSQL + " FROM Personnel INNER JOIN SpecifQualif ON ";
            General.sSQL = General.sSQL + " Personnel.CodeQualif=SpecifQualif.CodeQualif ";
            General.sSQL = General.sSQL + " WHERE SpecifQualif.QualifRExp=1";
            General.sSQL = General.sSQL + " and (NonActif is null or NonActif = 0)";
            sheridan.InitialiseCombo(txtINT_AnaCode, General.sSQL, "MATRICULE");

        }

        private void txtIntercom_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
        }

        private void txtinterDe_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (General.fc_ValidateDate(txtinterDe) == true)
                {
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
        }

        private void txtIntereAu_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (General.fc_ValidateDate(txtIntereAu) == true)
                {
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }
            }
        }
        private void UserDispatch_VisibleChanged(object sender, EventArgs e)
        {
            string sNoIntervention;
            string sSQlEtat;
            var tmpAdo = new ModAdo();
            if (!Visible)
            {
                ssIntervention.UpdateData();
                return;
            }


            //Microsoft Sans Serif; 8.25pt

            //tableLayoutPanel1.Font = new Font(tableLayoutPanel1.Font.FontFamily, 15, FontStyle.Regular);
            try
            {
                View.Theme.Theme.MainMenu.ShowHomeMenu("UserDispatch");

                //=== version dédié à pz.
                if (General.sActiveVersionPz == "1")//tested
                {

                    cmbEnergie.Visible = false;
                    cmdFindEnergie.Visible = false;
                    chkExclEnergie.Visible = false;
                    CHKP1.Visible = false;
                    CHKP2.Visible = false;
                    CHKP3.Visible = false;
                    CHKP4.Visible = false;
                    //Label26.Visible = False
                    label10.Text = "N°Enregistrement";//label "Energie"
                    txtNoEnregistrement.Visible = true;

                }
                else//tetsed
                {
                    label10.Visible = true;
                    cmbEnergie.Visible = true;
                    cmdFindEnergie.Visible = true;
                    chkExclEnergie.Visible = true;
                    CHKP1.Visible = true;
                    CHKP2.Visible = true;
                    CHKP3.Visible = true;
                    CHKP4.Visible = true;
                    label10.Text = "Energie";
                    txtNoEnregistrement.Text = "";
                    txtNoEnregistrement.Visible = false;
                }

                if (General.sCodeArticleP2 == "1")
                {
                    chkVisiteEntretien.Visible = true;
                }
                else//tested
                {
                    chkVisiteEntretien.Visible = false;
                }
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
                General.open_conn();

                //    If fc_InitRegGrilles(cUserDispatch) = True Then
                //        For Each objcontrole In Me.Controls
                //           If TypeOf objcontrole Is SSOleDBGrid Then
                //                fc_DeleteRegGrille cUserDocDevis, objcontrole, False, True
                //                fc_LoadDimensionGrille Me.Name, objcontrole
                //           End If
                //        Next
                //    Else
                //        For Each objcontrole In Me.Controls
                //           If TypeOf objcontrole Is SSOleDBGrid Then
                //              fc_LoadDimensionGrille Me.Name, objcontrole
                //           End If
                //        Next
                //    End If
                fc_DropCodeetat();//tested
                fc_DropMaticule();//tested
                var _with27 = this;
                // positionne sur l'historique.
                /*_with27.DTPicker1.value = DateAndTime.Today;
                _with27.DTPicker2.value = DateAndTime.Today;
                _with27.DTPicker3.value = DateAndTime.Today;
                _with27.DTPicker4.value = DateAndTime.Today;
                _with27.DTPicker5.value = DateAndTime.Today;
                _with27.DTPicker6.value = DateAndTime.Today;*/

                _with27.txtinterDe.Text = General.getFrmReg(Variable.cUserDispatch, "De", "");
                _with27.txtIntereAu.Text = General.getFrmReg(Variable.cUserDispatch, "Au", "");
                _with27.cmbStatus.Text = General.getFrmReg(Variable.cUserDispatch, "Status", "");
                if (!string.IsNullOrEmpty(General.getFrmReg(Variable.cUserDispatch, "Status", "")))
                {
                    _with27.lblLibStatus.Text = tmpAdo.fc_ADOlibelle("SELECT TypeCodeEtat.LibelleCodeEtat" + " FROM TypeCodeEtat WHERE TypeCodeEtat.CodeEtat='" + StdSQLchaine.gFr_DoublerQuote(cmbStatus.Text) + "'");
                }


                _with27.cmbIntervenant.Text = General.getFrmReg(Variable.cUserDispatch, "Intervenant", "");
                _with27.cmdDispatch.Text = General.getFrmReg(Variable.cUserDispatch, "cmdDispatch", "");
                _with27.txtCommentaire.Text = General.getFrmReg(Variable.cUserDispatch, "txtCommentaire", "");

                _with27.cmbDepan.Text = General.getFrmReg(Variable.cUserDispatch, "cmbDepan", "");
                _with27.cmbArticle.Text = General.getFrmReg(Variable.cUserDispatch, "Article", "");
                _with27.txtIntercom.Text = General.getFrmReg(Variable.cUserDispatch, "Commentaire", "");
                _with27.cmbCodeimmeuble.Text = General.getFrmReg(Variable.cUserDispatch, "Immeuble", "");
                _with27.ssComboActivite.Text = General.getFrmReg(Variable.cUserDispatch, "Activite", "");
                _with27.txtINT_AnaCode.Text = General.getFrmReg(Variable.cUserDispatch, "Secteur", "");
                _with27.txtDateSaisieDe.Text = General.getFrmReg(Variable.cUserDispatch, "saisieDe", "");
                _with27.txtDateSaisieFin.Text = General.getFrmReg(Variable.cUserDispatch, "saisieAu", "");
                _with27.txtDatePrevue1.Text = General.getFrmReg(Variable.cUserDispatch, "PrevueDe", "");
                _with27.txtDatePrevue2.Text = General.getFrmReg(Variable.cUserDispatch, "PrevueAu", "");
                _with27.cmbCodeGerant.Text = General.getFrmReg(Variable.cUserDispatch, "Gerant", "");
                _with27.cmbEtatDevis.Text = General.getFrmReg(Variable.cUserDispatch, "EtatDevis", "");

                _with27.chkDevisPDA.CheckState = General.nz(General.getFrmReg(Variable.cUserDispatch, "chkDevisPDA", Convert.ToString(0)), "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;

                _with27.chkAchat.CheckState = General.nz(General.getFrmReg(Variable.cUserDispatch, "Achat", Convert.ToString(0)), "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;
                _with27.txtCodeGestionnaire.Text = General.getFrmReg(Variable.cUserDispatch, "Gestionnaire", "");

                _with27.chkAffaireAvecFact.CheckState = General.nz(General.getFrmReg(Variable.cUserDispatch, "chkAffaireAvecFact", Convert.ToString(0)), "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;

                _with27.chkAffaireSansFact.CheckState = General.nz(General.getFrmReg(Variable.cUserDispatch, "chkAffaireSansFact", Convert.ToString(0)), "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;
                _with27.txtNoEnregistrement.Text = General.getFrmReg(Variable.cUserDispatch, "txtNoEnregistrement", "");

                chkVisiteEntretien.CheckState = General.nz(General.getFrmReg(Variable.cUserDispatch, "chkVisiteEntretien", Convert.ToString(0)), "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;


                chkDevisEtablir.CheckState = General.nz(General.getFrmReg(Variable.cUserDispatch, "chkDevisEtablir", Convert.ToString(0)), "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;

                chkDapeau.CheckState = General.nz(General.getFrmReg(Variable.cUserDispatch, "chkDapeau", Convert.ToString(0)), "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;

                boolBordereauJournee = Convert.ToBoolean(General.nz(General.getFrmReg(Variable.cUserDispatch, "boolBordereauJournee", "false"), false));

                chkEncaissement.CheckState = General.nz(General.getFrmReg(Variable.cUserDispatch, "chkEncaissement", Convert.ToString(0)), "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;
                chkEncRex.CheckState = General.nz(General.getFrmReg(Variable.cUserDispatch, "chkEncRex", Convert.ToString(0)), "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;
                chkEncCompta.CheckState = General.nz(General.getFrmReg(Variable.cUserDispatch, "chkEncCompta", Convert.ToString(0)), "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;

                /// Mondir le 02.07.2020, demande via mantis : https://groupe-dt.mantishub.io/view.php?id=1549
                chkDemandeNonTraite.CheckState = General.nz(General.getFrmReg(Variable.cUserDispatch, "chkDemandeNonTraite", Convert.ToString(0)), "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;
                /// Fin Modif Mondir


                if (fc_CtrlCriterEmpty() == true)
                    return;

                sNoIntervention = General.getFrmReg(Variable.cUserDispatch, "NewVar", "");
                //cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                //    cmbCodeimmeuble.SetFocus

                fc_loadIntervention();
                fc_Stocke(sNoIntervention);
                if (!string.IsNullOrEmpty(sNoIntervention) && General.IsNumeric(sNoIntervention))
                {
                    // cmbIntervention_Click
                    if ((rsintervention != null))
                    {
                        /* if (rsintervention.State == ADODB.ObjectStateEnum.adStateOpen)
                         {
                             rsintervention.Find("NoIntervention = " + sNoIntervention, , ADODB.SearchDirectionEnum.adSearchForward, ADODB.BookmarkEnum.adBookmarkFirst);
                             if (!rsintervention.EOF)
                             {

                                 ssIntervention.Bookmark = rsintervention.Bookmark;
                                 ssIntervention.SelBookmarks.RemoveAll();
                                 ssIntervention.SelBookmarks.Add((ssIntervention.GetBookmark(0)));
                             }
                         }*/
                        //ssIntervention.Rows.Where(l => l.Cells["NoIntervention"].Value.ToString() == sNoIntervention).ToList().ForEach(l => l.Selected = true);
                        var row = ssIntervention.Rows.Where(l => l.Cells["NoIntervention"].Value.ToString() == sNoIntervention).FirstOrDefault();
                        if (row != null)
                        {
                            row.Activate();
                            row.Selected = true;
                        }


                    }
                }

                General.saveInReg(Variable.cUserDispatch, "NewVar", sNoIntervention);

                // lblNavigation[0].Text = General.sNomLien0;
                // lblNavigation[1].Text = General.sNomLien1;
                // lblNavigation[2].Text = General.sNomLien2;

                if (DateTime.Now == Convert.ToDateTime("2018/10/30") && General.UCase(General.fncUserName()) == General.UCase("rachid abbouchi"))
                {
                    cmdAvisDePassage.Visible = true;
                }
                if (!string.IsNullOrEmpty(General.sEtatAvisDePassage))
                {
                    cmdAvisDePassage.Visible = true;
                }
                else
                {
                    cmdAvisDePassage.Visible = false;
                }

                sSQlEtat = "SELECT     AUT_Formulaire " + " From AUT_Autorisation " + " WHERE     AUT_Nom = '"
                    + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "' AND AUT_Formulaire = '" + Variable.cUserDispatch + "' AND (AUT_Objet = N'MajStatut')";

                using (var tmp = new ModAdo())
                {
                    sSQlEtat = tmp.fc_ADOlibelle(sSQlEtat);
                }

                if (!string.IsNullOrEmpty(sSQlEtat))
                {
                    bMajStatut = true;
                }
                else
                {
                    bMajStatut = false;
                }

                //===> Mondir le 09.04.2021, https://groupe-dt.mantishub.io/view.php?id=2284
                LockCodeEtatGrid();

            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }

        }


        private bool fc_CtrlCriterEmpty()
        {
            object obj = null;
            bool bEmpty = false;

            bEmpty = true;


            if (!string.IsNullOrEmpty(txtinterDe.Text))
            {
                bEmpty = false;
            }
            if (!string.IsNullOrEmpty(txtIntereAu.Text))
            {
                bEmpty = false;
            }
            if (!string.IsNullOrEmpty(cmbStatus.Text))
            {
                bEmpty = false;
            }


            if (!string.IsNullOrEmpty(cmbIntervenant.Text))
            {
                bEmpty = false;
            }

            if (!string.IsNullOrEmpty(cmdDispatch.Text))
            {
                bEmpty = false;
            }

            if (!string.IsNullOrEmpty(txtCommentaire.Text))
            {
                bEmpty = false;
            }

            if (!string.IsNullOrEmpty(cmbDepan.Text))
            {
                bEmpty = false;
            }

            if (!string.IsNullOrEmpty(cmbArticle.Text))
            {
                bEmpty = false;
            }
            if (!string.IsNullOrEmpty(txtCommentaire.Text))
            {
                bEmpty = false;
            }

            if (!string.IsNullOrEmpty(cmbCodeimmeuble.Text))
            {
                bEmpty = false;
            }
            if (!string.IsNullOrEmpty(ssComboActivite.Text))
            {
                bEmpty = false;
            }
            if (!string.IsNullOrEmpty(txtINT_AnaCode.Text))
            {
                bEmpty = false;
            }
            if (!string.IsNullOrEmpty(txtDateSaisieDe.Text))
            {
                bEmpty = false;
            }
            if (!string.IsNullOrEmpty(txtDateSaisieFin.Text))
            {
                bEmpty = false;
            }
            if (!string.IsNullOrEmpty(txtDatePrevue1.Text))
            {
                bEmpty = false;
            }
            if (!string.IsNullOrEmpty(txtDatePrevue2.Text))
            {
                bEmpty = false;
            }
            if (!string.IsNullOrEmpty(cmbCodeGerant.Text))
            {
                bEmpty = false;
            }
            if (!string.IsNullOrEmpty(cmbEtatDevis.Text))
            {
                bEmpty = false;
            }
            if (!string.IsNullOrEmpty(txtCodeGestionnaire.Text))
            {
                bEmpty = false;
            }
            if (chkAchat.CheckState != 0)
            {
                bEmpty = false;
            }
            if (chkAffaireAvecFact.CheckState != 0)
            {
                bEmpty = false;
            }
            if (chkAffaireSansFact.CheckState != 0)
            {
                bEmpty = false;
            }

            if (!string.IsNullOrEmpty(txtNoEnregistrement.Text))
            {
                bEmpty = false;
            }

            if (chkVisiteEntretien.CheckState == CheckState.Checked)
            {
                bEmpty = false;
            }

            if (chkDevisEtablir.CheckState == CheckState.Checked)
            {
                bEmpty = false;
            }
            if (chkDevisPDA.CheckState != 0)
            {
                bEmpty = false;
            }
            if (chkDapeau.CheckState == CheckState.Checked)
            {
                bEmpty = false;
            }

            if (!string.IsNullOrEmpty(txtNoIntervention.Text))
            {
                bEmpty = false;
            }

            if (chkEncaissement.CheckState == CheckState.Checked)
            {
                bEmpty = false;
            }
            if (chkEncCompta.CheckState == CheckState.Checked)
            {
                bEmpty = false;
            }
            if (chkEncRex.CheckState == CheckState.Checked)
            {
                bEmpty = false;
            }
            if (!string.IsNullOrEmpty(txtGroupe.Text))
            {
                bEmpty = false;
            }

            //====> Mondir le 01.07.2020, demandé via mantis : https://groupe-dt.mantishub.io/view.php?id=1549
            if (chkDemandeNonTraite.CheckState == CheckState.Checked)
            {
                bEmpty = false;
            }
            //====> Fin modif Mondir

            return bEmpty;

        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_DropMaticule()
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            General.sSQL = "";
            General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Qualification.CodeQualif,"
                + " Qualification.Qualification, Personnel.NumRadio, Personnel.Note_NOT"
                + " FROM Qualification INNER JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";

            if (ssDropMatricule.Rows.Count == 0)
            {
                sheridan.InitialiseCombo((this.ssDropMatricule), General.sSQL, "Matricule", true);

                //Me.ssIntervention.Columns("Intervenant").DropDownHwnd = Me.ssDropMatricule.hwnd
                /*  this.ssIntervention.Columns["Intervenant"].DropDownHwnd = this.ssDropMatricule.hWnd;
                  ssDropMatricule.Columns["CodeQualif"].Visible = false;
                  ssDropMatricule.Columns["NumRadio"].Visible = false;
                  ssDropMatricule.Columns["Note_NOT"].Visible = false;*/
                ssDropMatricule.DisplayLayout.Bands[0].Columns["CodeQualif"].Hidden = true;
                ssDropMatricule.DisplayLayout.Bands[0].Columns["NumRadio"].Hidden = true;
                ssDropMatricule.DisplayLayout.Bands[0].Columns["Note_NOT"].Hidden = true;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_DropCodeetat()
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            General.sSQL = "";
            General.sSQL = "SELECT TypeCodeEtat.CodeEtat, TypeCodeEtat.LibelleCodeEtat"
                + " FROM TypeCodeEtat";

            if (ssDropCodeEtat.Rows.Count == 0)
            {
                sheridan.InitialiseCombo((this.ssDropCodeEtat), General.sSQL, "CodeEtat", true);
                //  this.ssIntervention.Columns["CodeEtat"].DropDownHwnd = this.ssDropCodeEtat.hWnd;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            if (!e.Layout.Bands[0].Columns.Cast<UltraGridColumn>().Any(ele => ele.Key.ToUpper() == "CodeEtat".ToUpper()))
                return;

            e.Layout.Bands[0].Columns["CodeEtat"].ValueList = this.ssDropCodeEtat;
            //e.Layout.Bands[0].Columns["CodeEtat"].CellClickAction = CellClickAction.CellSelect;

            e.Layout.Bands[0].Columns["Intervenant"].ValueList = this.ssDropMatricule;
            //e.Layout.Bands[0].Columns["Intervenant"].CellClickAction = CellClickAction.CellSelect;

            e.Layout.Bands[0].Columns["CSecteur"].Header.Caption = "Chef Secteur";
            e.Layout.Bands[0].Columns["Wave"].Header.Caption = "Rapport vocal";
            e.Layout.Bands[0].Columns["Code1"].Header.Caption = "Code Gérant";
            e.Layout.Bands[0].Columns["CodeDepanneur"].Header.Caption = "Depanneur";
            e.Layout.Bands[0].Columns["HeureDebut"].Hidden = true;
            e.Layout.Bands[0].Columns["HeureFin"].Hidden = true;
            e.Layout.Bands[0].Columns["CptREndu_INT"].Hidden = true;
            e.Layout.Bands[0].Columns["LibelleCodeEtat"].Hidden = true;
            e.Layout.Bands[0].Columns["NoIntervention"].Hidden = true;
            e.Layout.Bands[0].Columns["INT_Rapport1"].Hidden = true;
            e.Layout.Bands[0].Columns["CptREndu_INT"].Hidden = true;
            e.Layout.Bands[0].Columns["LibelleCodeEtat"].Hidden = true;
            e.Layout.Bands[0].Columns["INT_AnaActivite"].Hidden = true;
            e.Layout.Bands[0].Columns["Prioritaire"].Hidden = true;
            e.Layout.Bands[0].Columns["MontantHT"].Hidden = true;
            e.Layout.Bands[0].Columns["Prior"].Hidden = true;
            e.Layout.Bands[0].Columns["Article"].Hidden = true;
            ssIntervention.UseOsThemes = DefaultableBoolean.False;
            e.Layout.Bands[0].Columns["Wave"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton;
            e.Layout.Bands[0].Columns["Wave"].ButtonDisplayStyle = Infragistics.Win.UltraWinGrid.ButtonDisplayStyle.Always;
            e.Layout.Bands[0].Columns["Wave"].CellButtonAppearance.Image = Properties.Resources.volume_up_4_16;

            e.Layout.Bands[0].Columns["Duree"].Format = "HH:mm:ss";
            e.Layout.Bands[0].Columns["DateRealise"].EditorComponent = displayedEditorForDateTime;

            //e.Layout.Bands[0.Override.HeaderClickAction = HeaderClickAction.Select;

            //===> Mondir le 01.04.2021, demandé par Xavier de le faire en dur pour le moment https://groupe-dt.mantishub.io/view.php?id=2284
            //===> Mondir le 09.04.2021, checked there a control in the database 
            //var allowedUsers = new[] { "S.GUERIN", "XST", "PQ", "MONDIR" };
            //var currentUser = General.fncUserName().ToUpper();
            //if (!allowedUsers.Contains(currentUser))
            //{
            //    e.Layout.Bands[0].Columns["CodeEtat"].CellActivation = Activation.Disabled;
            //}
            LockCodeEtatGrid();
            //===> Fin Modif Mondir
        }

        /// <summary>
        /// Mondir le 09.04.2021 https://groupe-dt.mantishub.io/view.php?id=2284
        /// </summary>
        private void LockCodeEtatGrid()
        {
            if (!bMajStatut)
            {
                //===> Mondir le 07.05.2021, changed Activation.Disabled to Activation.NoEdit
                ssIntervention.DisplayLayout.Bands[0].Columns["CodeEtat"].CellActivation = Activation.NoEdit;
            }
            else
            {
                ssIntervention.DisplayLayout.Bands[0].Columns["CodeEtat"].CellActivation = Activation.AllowEdit;
            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        /// 
        ///voir sur manticeHub =====>0001354: Dispatching Devis - alternance couleur grille
        private void ssIntervention_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            if (bMajStatut == false && e.ReInitialize)
                return;
            //// e.Row.Selected = false;
            ////--- code état U urgent
            //if (e.Row.Cells["CodeEtat"].Value.ToString() == "U")
            //{
            //    e.Row.Appearance.BackColor = Color.FromArgb(255, 128, 128);
            //    e.Row.Appearance.ForeColor = Color.FromArgb(255, 255, 255);
            //}
            //--- code état 04 Imprimé
            if (e.Row.GetCellValue("CodeEtat").ToString() == "04")
            {
                e.Row.Appearance.BackColor = Color.FromArgb(0, 128, 128);
                e.Row.Appearance.ForeColor = Color.FromArgb(255, 255, 255);
            }
            ////--- Code Etat 03 en saisie
            ////// ==> look at manticeHub 0001365: Dispatching intervention - Couleur sur code état 00
            if (e.Row.GetCellValue("CodeEtat").ToString() == "00")
            {
                e.Row.Appearance.BackColor = Color.FromArgb(255, 224, 192);
                e.Row.Appearance.ForeColor = ColorTranslator.FromOle(0x0);
            }
            ////--- Code Etat T Facturé
            //if (e.Row.Cells["CodeEtat"].Value.ToString() == "T")
            //{
            //    e.Row.Appearance.BackColor = Color.FromArgb(192, 192, 255);
            //    e.Row.Appearance.ForeColor = ColorTranslator.FromOle(0x0);
            //}
            ////--- code état AF 
            //if (e.Row.Cells["CodeEtat"].Value.ToString() == "AF")
            //{
            //    e.Row.Appearance.BackColor = Color.FromArgb(240, 155, 234);
            //    e.Row.Appearance.ForeColor = ColorTranslator.FromOle(0x0);
            //}

            ////--- Code Etat 03 en saisie
            if (e.Row.GetCellValue("CodeEtat").ToString() == "03")
            {
                e.Row.Appearance.BackColor = Color.FromArgb(255, 111, 111);
                e.Row.Cells["Nom"].Appearance.BackColor = Color.FromArgb(192, 192, 192);
                e.Row.Appearance.ForeColor = ColorTranslator.FromOle(0x0);
            }
            //if (e.Row.Cells["DateRealise"].Value == null || e.Row.Cells["DateRealise"].Value + "" == "")
            //    e.Row.Cells["DateRealise"].Value = DBNull.Value;
            e.Row.Update();
            //if (Convert.ToDouble(General.nz(e.Row.Cells["MontantHT"].Value.ToString(), "0")) < 0)
            //{
            //    e.Row.Cells["CodeEtat"].Appearance.BackColor = ColorTranslator.FromOle(0xc0e0ff);//verifier
            //    e.Row.Cells["CodeEtat"].Appearance.ForeColor = ColorTranslator.FromOle(0x0);

            //}
            //else if (Convert.ToDouble(General.nz(e.Row.Cells["MontantHT"].Value.ToString(), "0")) > 0)
            //{
            //    e.Row.Cells["CodeEtat"].Appearance.BackColor = ColorTranslator.FromOle(0xffc0c0);//verifier
            //    e.Row.Cells["CodeEtat"].Appearance.ForeColor = ColorTranslator.FromOle(0x0);
            //}

            //string fp = "#" + General.gfr_liaison("CouleurFondPrior", "80FF80").ToString();
            //string pp = "#" + General.gfr_liaison("CouleurPlcePrior", "000000").ToString();

            //if (e.Row.Cells["Prioritaire"].Value.ToString() == "1" || e.Row.Cells["Prioritaire"].Value.ToString() == "-1")
            //{
            //    e.Row.Cells["CodeImmeuble"].Appearance.BackColor = ColorTranslator.FromHtml(fp);
            //    e.Row.Cells["CodeImmeuble"].Appearance.ForeColor = ColorTranslator.FromHtml(pp);
            //}
            //if (e.Row.Cells["Prior"].Value.ToString() == "1" || e.Row.Cells["Prior"].Value.ToString() == "-1")
            //{
            //    e.Row.Cells["CodeImmeuble"].Appearance.BackColor = ColorTranslator.FromHtml(fp);
            //    e.Row.Cells["CodeImmeuble"].Appearance.ForeColor = ColorTranslator.FromHtml(pp);
            //}
            ////--- Sans rapport vocal
            ////See Readmine   ==>  Defect #375 Dispatching Intervention -Couleur rapport
            ////if (e.Row.Cells["Wave"].Value == DBNull.Value)
            ////{
            ////    e.Row.Cells["Wave"].Appearance.BackColor = ColorTranslator.FromHtml("#4d79ff");
            ////    e.Row.Cells["Wave"].Appearance.BackColor = Color.Blue;
            ////}

        }

        private void ssIntervention_AfterExitEditMode(object sender, EventArgs e)
        {
            if (ssIntervention.ActiveRow == null || ssIntervention.ActiveCell == null)
                return;

            sub_afficheLibelleContrat(ssIntervention.ActiveRow.Cells["CodeImmeuble"].Value.ToString());
            string sqlUpdate;
            if (ssIntervention.ActiveCell.Column.Key == "DateRealise")//tested
            {
                //===> Mondir le 05.11.2020, ajout la condition is date et modifier le format sans heure https://groupe-dt.mantishub.io/view.php?id=2076
                sqlUpdate = "update Intervention set DateRealise=" +
                    (ssIntervention.ActiveRow.Cells["DateRealise"].Value.ToString() == "" || !ssIntervention.ActiveRow.Cells["DateRealise"].Value.IsDate() ? "NULL" : "'" +
                    ssIntervention.ActiveRow.Cells["DateRealise"].Value.ToDate().ToString(General.FormatDateSansHeureSQL) + "'") + ""
                           + " where NoIntervention=" + ssIntervention.ActiveRow.Cells["NoIntervention"].Text;
                General.Execute(sqlUpdate);
            }
            if (ssIntervention.ActiveCell.Column.Key == "CodeEtat")//tested
            {
                if (bMajStatut == true)
                {
                    sqlUpdate = "update Intervention set CodeEtat='" + ssIntervention.ActiveRow.Cells["CodeEtat"].Text + "'"
                               + " where NoIntervention=" + ssIntervention.ActiveRow.Cells["NoIntervention"].Text;
                    General.Execute(sqlUpdate);
                }
                else
                {

                    ssIntervention.ActiveCell.Value = ssIntervention.ActiveCell.OriginalValue;

                }

            }
            if (ssIntervention.ActiveCell.Column.Key == "Intervenant")//tested
            {
                //===> Mondir le 12.04.2021 https://groupe-dt.mantishub.io/view.php?id=2389#c5846
                using (var tmpModAdo = new ModAdo())
                {
                    var req = $"SELECT Nom FROM Personnel WHERE NonActif = 0 AND Matricule = '{ssIntervention.ActiveRow.Cells["Intervenant"].Text}'";
                    req = tmpModAdo.fc_ADOlibelle(req);
                    if (string.IsNullOrEmpty(req))
                    {
                        //CustomMessageBox.Show("Veuillez vérifier que cet Intervenant existe et qu'il est actif", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        CustomMessageBox.Show("Cet intervenant n'existe pas ou est inactif\nModification annulée", "Modification annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        var interventant = tmpModAdo.fc_ADOlibelle($"SELECT Intervenant FROM Intervention WHERE NoIntervention = '{ssIntervention.ActiveRow.Cells["NoIntervention"].Text}'");
                        ssIntervention.ActiveRow.Cells["Intervenant"].Value = interventant;
                        return;
                    }
                }

                sqlUpdate = "update Intervention set Intervenant='" + ssIntervention.ActiveRow.Cells["Intervenant"].Text + "'"
                            + " where  NoIntervention=" + ssIntervention.ActiveRow.Cells["NoIntervention"].Text;
                General.Execute(sqlUpdate);
            }
            if (ssIntervention.ActiveCell.Column.Key == "INT_AnaActivite")
            {
                sqlUpdate = "update Intervention set INT_AnaActivite='" + ssIntervention.ActiveRow.Cells["INT_AnaActivite"].Text + "'"
                             + " where  NoIntervention=" + ssIntervention.ActiveRow.Cells["NoIntervention"].Text;
                General.Execute(sqlUpdate);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            try
            {
                bool boolFolder = false;
                bool boolMp3 = false;
                string sMp3 = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;

                if (!string.IsNullOrEmpty(ssIntervention.ActiveRow.Cells["Wave"].Value.ToString()))
                {
                    //== modifie l'extension wav en mp3 et ouvre le fichier si existant.
                    sMp3 = General.Mid(ssIntervention.ActiveRow.Cells["Wave"].Value.ToString(), 1, ssIntervention.ActiveRow.Cells["Wave"].Value.ToString().Length - 3) + "mp3";
                    // sMp3 = txtWave
                    General.fso = null;
                    boolMp3 = File.Exists(sMp3);

                    if (boolMp3 == true)
                    {
                        ModuleAPI.Ouvrir(sMp3);
                        return;
                    }
                    else
                    {

                        General.fso = null;
                        boolFolder = File.Exists(ssIntervention.ActiveRow.Cells["Wave"].Value.ToString());

                        if (boolFolder == true)
                        {
                            ModuleAPI.Ouvrir(ssIntervention.ActiveRow.Cells["Wave"].Value.ToString());
                            return;
                        }
                    }
                    if (boolFolder == false && boolMp3 == false)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le fichier son a été déplacé ou supprimé.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return;
                    }
                    //Ouvrir Me.hWnd, sMp3
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun rapport d'intervention vocal n'est associé a cette intervention", "Document non trouvé", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                return;


            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdWave_Click");
                return;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAvisDePassage_Click(object sender, EventArgs e)
        {
            frmSendAvisDepassage frm = new frmSendAvisDepassage();
            frm.ShowDialog();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDispatch_TextChanged(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibDispatch.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + StdSQLchaine.gFr_DoublerQuote(cmdDispatch.Text) + "'");
            }
        }
        /// <summary>
        /// Teested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDispatch_AfterCloseUp(object sender, EventArgs e)
        {
            using (var tmpAdo = new ModAdo())
            {
                lblLibDispatch.Text = tmpAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + StdSQLchaine.gFr_DoublerQuote(cmdDispatch.Text) + "'");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDispatch_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQl;
            sSQl = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom,"
                + " Qualification.CodeQualif, Qualification.Qualification"
                + " FROM Personnel INNER JOIN"
                + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif";

            sSQl = sSQl + " WHERE (NonActif is null or NonActif = 0) ";
            sSQl = sSQl + " order by matricule";


            sheridan.InitialiseCombo(cmdDispatch, sSQl, "Matricule", true);

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDispatch_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            string requete = "";
            string where = "";
            try
            {
                if (KeyAscii == 13)
                {
                    using (var tmpModAdo = new ModAdo())
                        if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + StdSQLchaine.gFr_DoublerQuote(cmdDispatch.Text) + "'"), "").ToString()))
                        {
                            requete = "SELECT Personnel.Matricule as \"Matricule\","
                            + " Personnel.Nom as \"Nom\","
                            + " Personnel.prenom as \"Prenom\","
                            + " Qualification.Qualification as \"Qualification\","
                            + " Personnel.Memoguard as \"MemoGuard\","
                            + " Personnel.NumRadio as \"NumRadio\""
                            + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                            where = " (NonActif is null or NonActif = 0)";

                            SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'un type d'opération" };
                            if (General.IsNumeric(cmbIntervenant.Text))
                            {
                                fg.SetValues(new Dictionary<string, string> { { "Matricule", cmdDispatch.Text } });
                            }
                            else
                            {

                                fg.SetValues(new Dictionary<string, string> { { "Nom", cmdDispatch.Text } });
                            }

                            fg.ugResultat.DoubleClickRow += (se, ev) =>
                            {
                                // ' charge les enregistrements de l'immeuble
                                cmdDispatch.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                lblLibDispatch.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                                fg.Dispose(); fg.Close();
                            };
                            fg.ugResultat.KeyDown += (se, ev) =>
                            {
                                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                                {
                                    // ' charge les enregistrements de l'immeuble
                                    cmdDispatch.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                    lblLibDispatch.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                                    fg.Dispose(); fg.Close();
                                }

                            };
                            fg.StartPosition = FormStartPosition.CenterScreen;
                            fg.ShowDialog();

                        }
                    //        cmbIntervention_Click
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdDispatch_KeyPress");
                return;

            }
        }

        private void lblFullScreen_DoubleClick(object sender, EventArgs e)
        {
            if (txtNoIntervention.Visible == true)
            {
                txtNoIntervention.Visible = false;
            }
            else
            {
                txtNoIntervention.Visible = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_AfterCellActivate(object sender, EventArgs e)
        {
            sub_afficheLibelleContrat(ssIntervention.ActiveRow.Cells["CodeImmeuble"].Value.ToString());
            fc_Stocke(ssIntervention.ActiveRow.Cells["NoIntervention"].Value.ToString());
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_BeforeCellActivate(object sender, CancelableCellEventArgs e)
        {
            boolModifRow = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_AfterRowActivate(object sender, EventArgs e)
        {
            if (ssIntervention.ActiveRow == null) return;
            sub_afficheLibelleContrat(ssIntervention.ActiveRow.Cells["CodeImmeuble"].Value.ToString());
            fc_Stocke(ssIntervention.ActiveRow.Cells["NoIntervention"].Value.ToString());
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssIntervention_BeforeRowActivate(object sender, RowEventArgs e)
        {
            boolModifRow = true;
        }

        private void SSOleDBCombo1_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL;
            sSQL = "SELECT DISTINCT Intervention.NoIntervention" + " From Intervention"
                   + " order by NoIntervention";
            sheridan.InitialiseCombo(SSOleDBCombo1, sSQL, "NoIntervention");

        }

        private void SSOleDBCombo1_AfterCloseUp(object sender, EventArgs e)
        {
            txtNoIntervention.Text = SSOleDBCombo1.Value.ToString();
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbEtatDevis_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtINT_AnaCode_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
        }

        private void txtCP_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
        }

        private void txtCP_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(txtCP.Text))
            {
                //        If Len(txtCP.Text) = 1 Then
                //            txtCP.Text = txtCP.Text & "0000"
                //        ElseIf Len(txtCP.Text) = 2 Then
                //            txtCP.Text = txtCP & "000"
                if (txtCP.Text.Length == 3)
                {
                    txtCP.Text = txtCP.Text + "00";
                }
                else if (txtCP.Text.Length == 4)
                {
                    txtCP.Text = txtCP.Text + "0";
                }

            }
        }

        private void txtDatePrevue1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (General.fc_ValidateDate(txtDatePrevue1) == true)
                {
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }
            }

        }

        private void txtDatePrevue2_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (e.KeyChar == 13)
            {
                if (General.fc_ValidateDate(txtDatePrevue2) == true)
                {
                    cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                }
            }
        }

        private void cmdGroupe_Click(object sender, EventArgs e)
        {

            string requete = "";
            string where = "";
            try
            {

                requete = " SELECT     CRCL_Code AS \"Code\","
                            + " CRCL_RaisonSocial AS \"Raison Sociale\","
                            + " CRCL_Adresse1 AS \"Adresse\","
                            + " CRCL_CP AS \"Code Postal\","
                            + " CRCL_Ville As \"Ville\" "
                            + " FROM         CRCL_GroupeClient ";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where, "") { Text = "Recherche d'un groupe" };
                fg.SetValues(new Dictionary<string, string> { { "CRCL_Code", txtGroupe.Text } });

                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtGroupe.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                    fg.Dispose(); fg.Close();
                };
                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtGroupe.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                        fg.Dispose(); fg.Close();
                    }

                };
                fg.StartPosition = FormStartPosition.CenterScreen;
                fg.ShowDialog();

                cmbIntervention_Click(cmbIntervention, new System.EventArgs());
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdGroupe_Click");
            }

        }

        private void txtGroupe_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if (e.KeyChar == 13)
                {
                    using (var tmpModAdo = new ModAdo())
                        if (!!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT CRCL_Code From CRCL_GroupeClient WHERE CRCL_Code ='" + StdSQLchaine.gFr_DoublerQuote(txtGroupe.Text) + "'"), "").ToString()))
                        {
                            cmdGroupe_Click(cmdGroupe, new System.EventArgs());
                        }
                        else
                        {
                            cmbIntervention_Click(cmbIntervention, new System.EventArgs());
                        }
                }
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";cmdGroupe_KeyPress");
            }

        }

        private void txtDateSaisieDe_Validating(object sender, CancelEventArgs e)
        {
            General.fc_ValidateDate(txtDateSaisieDe);
        }

        private void txtDateSaisieFin_Validating(object sender, CancelEventArgs e)
        {
            General.fc_ValidateDate(txtDateSaisieFin);
        }

        private void txtinterDe_Validating(object sender, CancelEventArgs e)
        {
            General.fc_ValidateDate(txtinterDe);
        }

        private void txtIntereAu_Validating(object sender, CancelEventArgs e)
        {
            General.fc_ValidateDate(txtIntereAu);
        }

        private void txtDatePrevue1_Validating(object sender, CancelEventArgs e)
        {
            General.fc_ValidateDate(txtDatePrevue1);
        }

        private void txtDatePrevue2_Validating(object sender, CancelEventArgs e)
        {
            General.fc_ValidateDate(txtDatePrevue2);
        }

        private void ultraDateTimeEditor1_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor1.Value == null || ultraDateTimeEditor1.Value == DBNull.Value) return;

            txtDateSaisieDe.Text = Convert.ToDateTime(ultraDateTimeEditor1.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor1_ValueChanged(object sender, EventArgs e)
        {
            //if (ultraDateTimeEditor1.Value == null || ultraDateTimeEditor1.Value == DBNull.Value) return;

            //txtDateSaisieDe.Text = Convert.ToDateTime(ultraDateTimeEditor1.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor2_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor2.Value == null || ultraDateTimeEditor2.Value == DBNull.Value) return;

            txtDateSaisieFin.Text = Convert.ToDateTime(ultraDateTimeEditor2.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor2_ValueChanged(object sender, EventArgs e)
        {
            //if (ultraDateTimeEditor2.Value == null || ultraDateTimeEditor2.Value == DBNull.Value) return;

            //txtDateSaisieFin.Text = Convert.ToDateTime(ultraDateTimeEditor2.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor3_ValueChanged(object sender, EventArgs e)
        {
            //if (ultraDateTimeEditor3.Value == null || ultraDateTimeEditor3.Value == DBNull.Value) return;

            //txtinterDe.Text = Convert.ToDateTime(ultraDateTimeEditor3.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor4_ValueChanged(object sender, EventArgs e)
        {
            //if (ultraDateTimeEditor4.Value == null || ultraDateTimeEditor4.Value == DBNull.Value) return;

            //txtIntereAu.Text = Convert.ToDateTime(ultraDateTimeEditor4.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor5_ValueChanged(object sender, EventArgs e)
        {
            //if (ultraDateTimeEditor5.Value == null || ultraDateTimeEditor5.Value == DBNull.Value) return;

            //txtDatePrevue1.Text = Convert.ToDateTime(ultraDateTimeEditor5.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor6_ValueChanged(object sender, EventArgs e)
        {
            //if (ultraDateTimeEditor6.Value == null || ultraDateTimeEditor6.Value == DBNull.Value) return;

            //txtDatePrevue2.Text = Convert.ToDateTime(ultraDateTimeEditor6.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor3_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor3.Value == null || ultraDateTimeEditor3.Value == DBNull.Value) return;

            txtinterDe.Text = Convert.ToDateTime(ultraDateTimeEditor3.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor4_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor4.Value == null || ultraDateTimeEditor4.Value == DBNull.Value) return;

            txtIntereAu.Text = Convert.ToDateTime(ultraDateTimeEditor4.Value).ToShortDateString();
        }

        private void cmdExcelDetail_Click(object sender, EventArgs e)
        {
            try
            {
                bExportDetail = true;
                cmdExportExcel_Click(cmdExcelDetail, new System.EventArgs());
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdExcelDetail_Click");
            }
        }

        private void ultraDateTimeEditor1_BeforeDropDown(object sender, CancelEventArgs e)
        {
            View.Theme.Theme.SetDateForCalendar(ultraDateTimeEditor1, txtDateSaisieDe);
        }

        private void ultraDateTimeEditor2_BeforeDropDown(object sender, CancelEventArgs e)
        {
            View.Theme.Theme.SetDateForCalendar(ultraDateTimeEditor2, txtDateSaisieFin);
        }

        private void ultraDateTimeEditor3_BeforeDropDown(object sender, CancelEventArgs e)
        {
            View.Theme.Theme.SetDateForCalendar(ultraDateTimeEditor3, txtinterDe);
        }

        private void ultraDateTimeEditor4_BeforeDropDown(object sender, CancelEventArgs e)
        {
            View.Theme.Theme.SetDateForCalendar(ultraDateTimeEditor4, txtIntereAu);
        }

        private void ultraDateTimeEditor5_BeforeDropDown(object sender, CancelEventArgs e)
        {
            View.Theme.Theme.SetDateForCalendar(ultraDateTimeEditor5, txtDatePrevue1);
        }

        private void ultraDateTimeEditor6_BeforeDropDown(object sender, CancelEventArgs e)
        {
            View.Theme.Theme.SetDateForCalendar(ultraDateTimeEditor6, txtDatePrevue2);
        }

        private void ultraDateTimeEditor4_AfterDropDown(object sender, EventArgs e)
        {
            //if (!string.IsNullOrEmpty(txtIntereAu.Text) && General.IsDate(txtIntereAu.Text))
            //    ultraDateTimeEditor4.Value = txtIntereAu.Text;
        }

        private void ultraDateTimeEditor5_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor5.Value == null || ultraDateTimeEditor5.Value == DBNull.Value) return;

            txtDatePrevue1.Text = Convert.ToDateTime(ultraDateTimeEditor5.Value).ToShortDateString();
        }

        private void ultraDateTimeEditor6_AfterCloseUp(object sender, EventArgs e)
        {
            if (ultraDateTimeEditor6.Value == null || ultraDateTimeEditor6.Value == DBNull.Value) return;

            txtDatePrevue2.Text = Convert.ToDateTime(ultraDateTimeEditor6.Value).ToShortDateString();
        }

        /// <summary>
        /// Tested
        /// Mondir le 01.07.2020, demande via mantis : https://groupe-dt.mantishub.io/view.php?id=1549
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkDevisEtablir_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDevisEtablir.Checked)
                chkDemandeNonTraite.Checked = false;
        }

        /// <summary>
        /// Tested
        /// Mondir le 01.07.2020, demande via mantis : https://groupe-dt.mantishub.io/view.php?id=1549
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkDemandeNonTraite_CheckedChanged(object sender, EventArgs e)
        {
            if (chkDemandeNonTraite.Checked)
                chkDevisEtablir.Checked = false;
        }



        //private void fc_intializeRow()
        //{
        //   ssIntervention.Rows.Where(r => r.Cells["CodeEtat"].Value.ToString() == "U").ToList().ForEach(r 
        //       => { r.Appearance.BackColor = Color.FromArgb(255, 128, 128);
        //           r.Appearance.ForeColor = Color.FromArgb(255, 255, 255); });
        //    ssIntervention.Rows.Where(r => r.Cells["CodeEtat"].Value.ToString() == "04").ToList().ForEach(r
        //       => {
        //           r.Appearance.BackColor = Color.FromArgb(0, 128, 128);
        //           r.Appearance.ForeColor = Color.FromArgb(255, 255, 255);
        //       });
        //    ssIntervention.Rows.Where(r => r.Cells["CodeEtat"].Value.ToString() == "00").ToList().ForEach(r
        //       => {
        //           r.Appearance.BackColor = Color.FromArgb(255, 224, 192);
        //           r.Appearance.ForeColor = ColorTranslator.FromOle(0x0);
        //       });
        //    ssIntervention.Rows.Where(r => r.Cells["CodeEtat"].Value.ToString() == "T").ToList().ForEach(r
        //       =>
        //    {
        //        r.Appearance.BackColor = Color.FromArgb(192, 192, 255);
        //        r.Appearance.ForeColor = ColorTranslator.FromOle(0x0);
        //    });
        //    ssIntervention.Rows.Where(r => r.Cells["CodeEtat"].Value.ToString() == "AF").ToList().ForEach(r
        //      =>
        //    {
        //        r.Appearance.BackColor = Color.FromArgb(240, 155, 234);
        //        r.Appearance.ForeColor = ColorTranslator.FromOle(0x0);
        //    });
        //    ssIntervention.Rows.Where(r => r.Cells["CodeEtat"].Value.ToString() == "03").ToList().ForEach(r
        //     =>
        //    {
        //        r.Cells["Nom"].Appearance.BackColor = Color.FromArgb(192, 192, 192);
        //        r.Appearance.ForeColor = ColorTranslator.FromOle(0x0);
        //    });

        //    ssIntervention.Rows.Where(r=>Convert.ToDouble(General.nz(r.Cells["MontantHT"].Value.ToString(), "0")) < 0).ToList().ForEach(r
        //    =>
        //    {
        //        r.Cells["CodeEtat"].Appearance.BackColor = ColorTranslator.FromOle(0xc0e0ff);//verifier
        //        r.Cells["CodeEtat"].Appearance.ForeColor = ColorTranslator.FromOle(0x0);
        //    });

        //    ssIntervention.Rows.Where(r => Convert.ToDouble(General.nz(r.Cells["MontantHT"].Value.ToString(), "0")) > 0).ToList().ForEach(r
        //    =>
        //    {
        //        r.Cells["CodeEtat"].Appearance.BackColor = ColorTranslator.FromOle(0xffc0c0);//verifier
        //        r.Cells["CodeEtat"].Appearance.ForeColor = ColorTranslator.FromOle(0x0);
        //    });


        //    string fp = "#" + General.gfr_liaison("CouleurFondPrior", "80FF80").ToString();
        //    string pp = "#" + General.gfr_liaison("CouleurPlcePrior", "000000").ToString();
        //    ssIntervention.Rows.Where(r=>r.Cells["Prioritaire"].Value.ToString() == "1" || r.Cells["Prioritaire"].Value.ToString() == "-1").ToList().ForEach(r
        //    =>
        //    {
        //        r.Cells["CodeImmeuble"].Appearance.BackColor = ColorTranslator.FromHtml(fp);
        //        r.Cells["CodeImmeuble"].Appearance.ForeColor = ColorTranslator.FromHtml(pp);
        //    });
        //    ssIntervention.Rows.Where(r=>r.Cells["Prior"].Value.ToString() == "1" || r.Cells["Prior"].Value.ToString() == "-1").ToList().ForEach(r
        //      =>
        //    {
        //        r.Cells["CodeImmeuble"].Appearance.BackColor = ColorTranslator.FromHtml(fp);
        //        r.Cells["CodeImmeuble"].Appearance.ForeColor = ColorTranslator.FromHtml(pp);
        //    });

        //}

        //===> Mondir le 30.09.2020, https://groupe-dt.mantishub.io/view.php?id=1999
        public class CustomComparer : IComparer
        {
            public int Compare(object x, object y)
            {
                var firstCell = x as UltraGridCell;
                var secondCell = y as UltraGridCell;

                var firstCellValue = firstCell.Value;
                var secondCellValue = secondCell.Value;

                if (firstCellValue == null && secondCellValue == null)
                {
                    return 0;
                }

                if (firstCellValue == null && secondCellValue != null)
                {
                    return -1;
                }

                if (firstCellValue != null && secondCellValue == null)
                {
                    return 1;
                }

                int out1;
                int out2;

                if (!int.TryParse(firstCellValue.ToString(), out out1) && !int.TryParse(secondCellValue.ToString(), out out2))
                {
                    return 0;
                }
                else if (!int.TryParse(firstCellValue.ToString(), out out1) && int.TryParse(secondCellValue.ToString(), out out2))
                {
                    return -1;
                }
                else if (int.TryParse(firstCellValue.ToString(), out out1) && !int.TryParse(secondCellValue.ToString(), out out2))
                {
                    return 1;
                }

                if (Convert.ToInt32(firstCellValue) == Convert.ToInt32(secondCellValue))
                {
                    return 0;
                }
                else if (Convert.ToInt32(firstCellValue) > Convert.ToInt32(secondCellValue))
                {
                    return 1;
                }
                else
                {
                    return -1;
                }
            }
        }

        private void label12_Click(object sender, EventArgs e)
        {

        }
        //===> Fin Modif Mondir
    }
}
