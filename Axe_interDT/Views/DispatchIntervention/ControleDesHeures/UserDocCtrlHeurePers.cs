﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Web.ModelBinding;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Intervention;
using Infragistics.Win.UltraWinEditors;
using Infragistics.Win.UltraWinGrid;
using Microsoft.Office.Interop;
//using Microsoft.VisualBasic;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Axe_interDT.Views.DispatchIntervention.ControleDesHeures
{
    public partial class UserDocCtrlHeurePers : UserControl
    {
        public UserDocCtrlHeurePers()
        {
            InitializeComponent();
        }

        private struct typeCalHour
        {
            public string sIntervenant;
            public System.DateTime dtDay;
            public int lTotInter;
            public int lTotDevis;
            public int lTotDeplacementFact;
            public int lTotP3;
            public DateTime dtCumulHinter;
            public DateTime dtCumulHDevis;
            public DateTime dtCumulHFactInter;
            public DateTime dtCumulHfactDevis;
            public DateTime dtCumulHNofact;
            public DateTime dtCumulHP3;
            public bool bErreur;
        }

        bool bFindErr;

        private struct TypeErr
        {
            public string sIntervenant;
            public System.DateTime dtDay;
            public int lNointervention;
        }

        private struct TypeTotGen
        {
            public string[] sAdresseCel;
        }

        const short CR1 = 1;
        const short CR2 = 2;
        const short CR3 = 3;
        const short cR4 = 4;
        const short cR5 = 5;
        const short cR6 = 6;
        const short cR7 = 8;
        const short cR8 = 9;
        const short cR9 = 10;
        const short cR10 = 11;
        const short cR11 = 12;
        const short cDeb = 1;

        const string cStatutAutorise = "AF,D,ZZ,E,S,T,TS,P3";

        const short cLargeur = 8;

        DataTable rsHMI;

        string sOrderBy;
        int iOldCol;
        bool bTriAsc;

        private struct tpService
        {
            public string sCode;
            public string sLibelle;
            public int lLigne;
        }


        private struct tpCountAnomalie
        {
            public string sCode;
            public int lAnnee;
            public int lMois;
            public int ltotAno;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_AfterCloseUp(object sender, EventArgs e)
        {
            ModAdo ModAdo = new ModAdo();
            lblLibIntervenant.Text =
                ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" +
                                     cmbIntervenant.Value + "'");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;

            sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom," +
                   " Qualification.CodeQualif, Qualification.Qualification" + " FROM Personnel INNER JOIN" +
                   " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif";
            sSQL = sSQL + " WHERE (NonActif is null or NonActif = 0) ";

            sSQL = sSQL + " order by matricule";

            sheridan.InitialiseCombo(cmbIntervenant, sSQL, "Matricule");

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenant_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {

                var keyAscii = (short)e.KeyChar;
                if (keyAscii == 13)
                {
                    var ModAdo = new ModAdo();
                    if (string.IsNullOrEmpty(General.nz(ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbIntervenant.Text + "'") + "", "") + ""))
                    {
                        string req = "SELECT Personnel.Matricule as \"Matricule\"," + " Personnel.Nom as \"Nom\"," +
                                     " Personnel.prenom as \"Prenom\"," +
                                     " Qualification.Qualification as \"Qualification\"," +
                                     " Personnel.Memoguard as \"MemoGuard\"," + " Personnel.NumRadio as \"NumRadio\"" +
                                     " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                        string where = " (NonActif is null or NonActif = 0) ";
                        var _with1 = new SearchTemplate(null, null, req, where, "");
                        _with1.SetValues(new Dictionary<string, string>
                        {
                            {General.IsNumeric(cmbIntervenant.Text) ? "Matricule" : "Nom", cmbIntervenant.Text}
                        });

                        _with1.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            cmbIntervenant.Text = _with1.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                            lblLibIntervenant.Text = _with1.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                            _with1.Close();
                        };
                        _with1.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13)
                            {
                                if (_with1.ugResultat.ActiveRow != null)
                                {
                                    cmbIntervenant.Text = _with1.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                                    lblLibIntervenant.Text = _with1.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                                    _with1.Close();
                                    _with1.Dispose();
                                    _with1.Close();
                                }
                            }
                        };
                        _with1.ShowDialog();
                    }
                    return;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervenant_KeyPress");
            }
        }

        private void cmbIntervenant_Click(object sender, EventArgs e)
        {
            //fc_LoadErr();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="bExportExcel"></param>
        private void fc_LoadErr(bool bExportExcel = false)
        {
            string sSQL = null;
            string sWhere = null;
            try
            {
                if (bExportExcel == true)
                {
                    sSQL =
                        "SELECT     SCH_ServiceCtrlHeure.SCH_Code, SCH_ServiceCtrlHeure.SCH_Libelle, MONTH(HMI_HourMoInterv.DateRealise) AS Mois," +
                        "    YEAR(HMI_HourMoInterv.DateRealise) AS Annee " +
                        " FROM         SCH_ServiceCtrlHeure INNER JOIN " +
                        " Personnel ON SCH_ServiceCtrlHeure.SCH_Code = Personnel.SCH_Code RIGHT OUTER JOIN " +
                        " HMI_HourMoInterv ON Personnel.Matricule = HMI_HourMoInterv.Matricule";

                }
                else
                {
                    sSQL = "SELECT    Controle, HMI_ID, Codeimmeuble, NoIntervention, Erreur, " +
                           " HMI_HourMoInterv.Matricule, DateRealise,  CreeLe, CreePar, ModifieLe, ModifiePar" +
                           " FROM         HMI_HourMoInterv LEFT OUTER JOIN" +
                           " Personnel ON HMI_HourMoInterv.Matricule = Personnel.Matricule ";
                }

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                sWhere = "";

                if ((DTPicker1.ButtonsLeft[0] as StateEditorButton).Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DateRealise >='" + Convert.ToDateTime(DTPicker1.Value).ToShortDateString() + " 00:00:00'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND DateRealise >='" + Convert.ToDateTime(DTPicker1.Value).ToShortDateString() + " 00:00:00'";
                    }
                }

                if ((DTPicker2.ButtonsLeft[0] as StateEditorButton).Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE DateRealise <='" + Convert.ToDateTime(DTPicker2.Value).ToShortDateString() + " 23:59:59'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND DateRealise <='" + Convert.ToDateTime(DTPicker2.Value).ToShortDateString() + " 23:59:59'";
                    }
                }

                if (cmbIntervErr.Text != "")
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE HMI_HourMoInterv.Matricule ='" +
                                 StdSQLchaine.gFr_DoublerQuote(cmbIntervErr.Value.ToString()) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND HMI_HourMoInterv.Matricule ='" +
                                 StdSQLchaine.gFr_DoublerQuote(cmbIntervErr.Value.ToString()) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(cmbCodeimmeuble.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Codeimmeuble ='" + StdSQLchaine.gFr_DoublerQuote(cmbCodeimmeuble.Text) +
                                 "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtNoIntervention.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE NoIntervention =" + txtNoIntervention.Text;
                    }
                    else
                    {
                        sWhere = sWhere + " AND NoIntervention =" + txtNoIntervention.Text;
                    }
                }

                if (OptControle.Checked == true)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Controle = 1";
                    }
                    else
                    {
                        sWhere = sWhere + " AND Controle = 1";
                    }
                }
                else if (OptNonControle.Checked == true)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Controle = 0 or Controle is null)";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Controle = 0 or Controle is null)";
                    }

                }

                if (!string.IsNullOrEmpty(cmdService2.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE SCH_Code ='" + cmdService2.Text + "'";
                    }
                    else
                    {
                        sWhere = sWhere + " AND SCH_Code ='" + cmdService2.Text + "'";
                    }

                }

                if (string.IsNullOrEmpty(sOrderBy))
                {
                    sOrderBy = "ORDER BY DateRealise DESC";
                }
                if (bExportExcel == false)
                {
                    var ModAdo = new ModAdo();
                    rsHMI = ModAdo.fc_OpenRecordSet(sSQL + sWhere + sOrderBy);

                    lblTotal.Text = "Total: " + Convert.ToString(rsHMI.Rows.Count);

                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    GridHMI.DataSource = rsHMI;
                    GridHMI.UpdateData();
                }
                else
                {
                    sSQL = sSQL + sWhere + " ORDER BY  Annee, Mois, SCH_ServiceCtrlHeure.SCH_Code";
                    fc_exportExl(sSQL, DTPicker1.DateTime, DTPicker2.DateTime);
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadErr");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sSQL"></param>
        /// <param name="dtDebut"></param>
        /// <param name="dtFin"></param>
        /// <returns></returns>
        private object fc_exportExl(string sSQL, System.DateTime dtDebut, System.DateTime dtFin)
        {
            object functionReturnValue = null;
            DataTable rsExl = default(DataTable);
            DataTable rs = default(DataTable);

            Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
            Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
            Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
            Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);
            DataTable rsExport = default(DataTable);
            ADODB.Field oField = default(ADODB.Field);

            int i = 0;
            int lCol = 0;
            int X = 0;
            int lLigne = 0;
            int lLigneBis = 0;
            int lMonth = 0;
            int lYear = 0;
            int lCount = 0;
            int lligneFin = 0;

            string sMatricule = null;
            string sMois = null;
            string sCode = null;
            tpService[] sTabServ = null;
            tpCountAnomalie[] sTabCountServ = null;

            bool bFirst = false;
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                var ModAdo = new ModAdo();
                rsExl = ModAdo.fc_OpenRecordSet(sSQL);
                if (rsExl.Rows.Count == 0)//tested
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour ces critères.", "Opération annulée", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    return functionReturnValue;
                }
                sSQL = "SELECT     SCH_Code, SCH_Libelle" + " FROM         SCH_ServiceCtrlHeure order by SCH_Code ";

                rs = ModAdo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count == 0)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun service n'a été trouvé.", "Opération annulée", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    return functionReturnValue;
                }
                X = 0;
                foreach (DataRow dr in rs.Rows)//TESTED
                {
                    Array.Resize(ref sTabServ, X + 1);
                    sTabServ[X].sCode = dr["SCH_Code"] + "";
                    sTabServ[X].sLibelle = dr["SCH_Libelle"] + "";

                    X = X + 1;
                }
                ModAdo.fc_CloseRecordset(rs);
                rs = null;
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;
                oXL.Visible = false;
                lCol = 1;
                lLigne = 1;
                //===Titre.
                oSheet.Cells[lLigne, 2].value = "ANOMALIES DU " + Convert.ToDateTime(dtDeb.Value).ToShortDateString() + " AU " + dtFin.ToShortDateString();

                lLigne = lLigne + 2;

                //=== 1 Colonne service.
                lLigneBis = lLigne;

                oSheet.Cells[lLigne, lCol].value = "SERVICE";
                lLigneBis = lLigneBis + 1;
                lligneFin = lLigneBis;
                for (int XX = 0; XX < sTabServ.Length; XX++)
                {
                    oSheet.Cells[lLigneBis, lCol].value = sTabServ[XX].sLibelle;
                    oSheet.Cells[lLigneBis, lCol].VerticalAlignment =
                        Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    sTabServ[XX].lLigne = lLigneBis;
                    lLigneBis = lLigneBis + 1;
                    lligneFin = lLigneBis;
                }
                lMonth = 0;
                lYear = 0;
                sCode = "";
                X = 0;
                lCount = 0;
                bFirst = true;

                foreach (DataRow dr in rsExl.Rows)
                {
                    if (bFirst == true)//tested
                    {
                        lYear = Convert.ToInt32(General.nz(dr["Annee"], 0));
                        lMonth = Convert.ToInt32(General.nz(dr["mois"], 0));
                        sCode = dr["SCH_Code"] + "";

                        Array.Resize(ref sTabCountServ, X + 1);

                        sTabCountServ[X].sCode = sCode;
                        sTabCountServ[X].lAnnee = lYear;
                        sTabCountServ[X].lMois = lMonth;
                        lCount = 0;
                        bFirst = false;
                    }
                    if ((lYear != Convert.ToInt32(General.nz(dr["Annee"], 0)) &&
                         Convert.ToInt32(General.nz(dr["Annee"], 0)) != 0) ||
                         (lMonth != Convert.ToInt32(General.nz(dr["mois"], 0)) &&
                          Convert.ToInt32(General.nz(dr["mois"], 0)) != 0) ||
                         (sCode != General.nz(dr["SCH_Code"], "") + ""))//tested
                    {
                        lYear = Convert.ToInt32(General.nz(dr["Annee"], 0));
                        lMonth = Convert.ToInt32(General.nz(dr["mois"], 0));
                        sCode = dr["SCH_Code"] + "";

                        X = X + 1;

                        Array.Resize(ref sTabCountServ, X + 1);

                        sTabCountServ[X].sCode = sCode;
                        sTabCountServ[X].lAnnee = lYear;
                        sTabCountServ[X].lMois = lMonth;
                        lCount = 0;
                    }
                    lCount = lCount + 1;
                    sTabCountServ[X].ltotAno = lCount;
                }
                //=== Affiche les données sur le fichier Excel.
                lMonth = 0;
                lYear = 0;
                sCode = "";
                bFirst = true;
                lLigneBis = lLigne;
                i = 0;
                for (int XX = 0; XX < sTabCountServ.Length; XX++)
                {
                    if (bFirst == true)//tested
                    {
                        lYear = sTabCountServ[XX].lAnnee;
                        lMonth = sTabCountServ[XX].lMois;
                        sCode = sTabCountServ[XX].sCode;

                        bFirst = false;
                        lCol = lCol + 1;
                        oSheet.Cells[lLigneBis, lCol].value = General.fc_ReturnMonthParam(Convert.ToString(lMonth)) + " " + lYear;
                        lLigneBis = lLigne;

                        // oSheet.Cells(lLigneBis, lCol).VerticalAlignment = xlVAlignCenter

                    }

                    if (lMonth != sTabCountServ[XX].lMois || lYear != sTabCountServ[XX].lAnnee)//tested
                    {
                        lCol = lCol + 1;
                        lLigneBis = lLigne;
                        lYear = sTabCountServ[XX].lAnnee;
                        lMonth = sTabCountServ[XX].lMois;

                        oSheet.Cells[lLigneBis, lCol].value = General.fc_ReturnMonthParam(Convert.ToString(lMonth)) +
                                                              " " + lYear;
                        //oSheet.Cells(lLigneBis, lCol).VerticalAlignment = xlVAlignCenter
                    }

                    for (i = 0; i < sTabServ.Length; i++)
                    {
                        if (General.UCase(sTabCountServ[XX].sCode) == General.UCase(sTabServ[i].sCode))//tested
                        {
                            oSheet.Cells[sTabServ[i].lLigne, lCol].value = sTabCountServ[XX].ltotAno;
                            //    oSheet.Cells(sTabServ(i).lLigne, lCol).VerticalAlignment = xlVAlignCenter
                            break; 
                        }
                    }
                }
                oSheet.Range[oSheet.Cells[lLigne, 1], oSheet.Cells[lligneFin - 1, lCol]].Borders.LineStyle =
                    Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                oSheet.Range[oSheet.Cells[lLigne, 1], oSheet.Cells[lligneFin - 1, lCol]].HorizontalAlignment =
                    Microsoft.Office.Interop.Excel.Constants.xlCenter;
                Microsoft.Office.Interop.Excel.Shape oShap = default(Microsoft.Office.Interop.Excel.Shape);
                oShap = oSheet.Shapes.AddChart(Microsoft.Office.Interop.Excel.XlChartType.xlColumnClustered, 10, 100);
                oShap.Chart.SetSourceData(oSheet.Range[oSheet.Cells[lLigne, 1], oSheet.Cells[lligneFin - 1, lCol]]);
                oShap = null;
                ModAdo.fc_CloseRecordset(rsExl);
                rsExl = null;
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;

                oXL.Visible = true;
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_exportExl");
                return functionReturnValue;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <returns></returns>
        public bool fc_cTrlErreur()
        {
            bool functionReturnValue = false;
            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                functionReturnValue = false;

                if (!(dtDeb.ButtonsLeft[0] as StateEditorButton).Checked)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date de début est obligatoire.", "Opération annulée", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    functionReturnValue = true;
                    return functionReturnValue;
                }

                if (!(dtFin.ButtonsLeft[0] as StateEditorButton).Checked)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date de début est obligatoire.", "Opération annulée", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    functionReturnValue = true;
                    return functionReturnValue;
                }

                if (!string.IsNullOrEmpty(dtDeb.Value + "") && !string.IsNullOrEmpty(dtFin.Value + ""))
                {
                    if (Convert.ToDateTime(dtDeb.Value).Month != Convert.ToDateTime(dtFin.Value).Month)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionnez une fourchette de dates sur un même mois.",
                            "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        functionReturnValue = true;
                        return functionReturnValue;
                    }
                }

                if (Convert.ToDateTime(dtDeb.Value) > Convert.ToDateTime(dtFin.Value))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("La date 'réalisée du' doit être inférieur ou égale à la date 'réalisée au'.",
                        "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = true;
                    return functionReturnValue;
                }


                if (optService.Checked == true)
                {
                    if (string.IsNullOrEmpty(cmdService.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un code service.", "Opération annulée", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                        functionReturnValue = true;
                        return functionReturnValue;
                    }
                }

                if (OptParInterv.Checked == true)
                {
                    if (string.IsNullOrEmpty(cmbIntervenant.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un intervenant.", "Opération annulée", MessageBoxButtons.OK,
                            MessageBoxIcon.Information);
                        functionReturnValue = true;
                        return functionReturnValue;
                    }
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_cTrlErreur");
            }
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="bExport"></param>
        private void fc_LoadData(bool bExport)
        {
            Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
            Microsoft.Office.Interop.Excel.Workbook oWBMod = default(Microsoft.Office.Interop.Excel.Workbook);
            Microsoft.Office.Interop.Excel.Worksheet oSheetMod = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
            Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
            string sSQL = null;
            string sCodeEtat = null;
            string sMat = null;
            string sWhere = null;
            string sNumeroDevis = null;

            typeCalHour[] tpCalHour = null;

            TypeErr[] tpErr = null;

            System.DateTime dtDate = default(System.DateTime);

            DataTable rs = default(DataTable);
            DataTable rsErr = default(DataTable);

            int lCount = 0;
            int X = 0;
            int i = 0;
            int o = 0;
            int iErr = 0;
            int lNbInter = 0;
            int lNbDeplacInter = 0;
            int lNbDevis = 0;
            int lFistRow = 0;

            bool bErr = false;

            try
            {
                if (fc_cTrlErreur() == true)//tested
                {
                    return;
                }
                bFindErr = false;

                o = 0;
                tpErr = null;
                tpErr = new TypeErr[o + 1];
                sSQL = "SELECT     HMI_ID, Codeimmeuble, NoIntervention, Erreur, Matricule, DateRealise, Controle" + " From HMI_HourMoInterv " + " WHERE     (Controle = 0) OR " + " (Controle IS NULL)";
                var ModAdo = new ModAdo();
                rsErr = ModAdo.fc_OpenRecordSet(sSQL);

                iErr = 0;

                foreach (DataRow dr in rsErr.Rows)//tested
                {
                    Array.Resize(ref tpErr, iErr + 1);
                    tpErr[iErr].dtDay = Convert.ToDateTime(dr["DateRealise"]);
                    tpErr[iErr].sIntervenant = dr["Matricule"] + "";
                    tpErr[iErr].lNointervention = Convert.ToInt32(dr["Nointervention"]);
                    iErr = iErr + 1;
                }

                sSQL = "SELECT     Intervention.DateRealise, Intervention.HeureDebut, Intervention.HeureFin, " + " Intervention.Duree, Intervention.Intervenant, Intervention.CodeEtat, " + " Intervention.CodeImmeuble , Intervention.NoDevis, DevisEnTete.NumeroDevis, Intervention.NoIntervention" + " FROM         Intervention INNER JOIN " + " GestionStandard ON Intervention.NumFicheStandard = GestionStandard.NumFicheStandard LEFT OUTER JOIN " + " DevisEnTete ON GestionStandard.NoDevis = DevisEnTete.NumeroDevis";
                if (optService.Checked == true)
                {
                    sSQL = sSQL + " INNER JOIN Personnel ON Intervention.Intervenant = Personnel.Matricule";
                }
                sWhere = " WHERE DateRealise >= '" + Convert.ToDateTime(dtDeb.Value).ToShortDateString() + " 00:00:00' AND DateRealise <='" + Convert.ToDateTime(dtFin.Value).ToShortDateString() + " 23:59:59'";
                if (OptParInterv.Checked == true)
                {
                    sWhere = sWhere + " and Intervenant ='" + cmbIntervenant.Text + "'";
                }
                else if (optService.Checked == true)
                {
                    sWhere = sWhere + " and SCH_Code ='" + cmdService.Text + "'";
                }
                if (txtNoInterventionBis.Visible == true)
                {
                    if (!string.IsNullOrEmpty(txtNoInterventionBis.Text))
                    {
                        sWhere = sWhere + " AND Intervention.NoIntervention = " + txtNoInterventionBis.Text;
                    }
                }
                sSQL = sSQL + sWhere + " ORDER BY Intervenant, DateRealise";


                rs = ModAdo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count == 0)//tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'existe pas d'enregistrement pour ces paramétres.", "Erreur",
                        MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                sMat = "";
                lCount = 0;
                lNbInter = 0;
                lNbDevis = 0;
                lNbDeplacInter = 0;

                foreach (DataRow dr in rs.Rows)
                {
                    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    sCodeEtat = dr["CodeEtat"] + "";
                    sNumeroDevis = General.Trim(dr["numerodevis"] + "");
                    //=== gére les erreurs.
                    bErr = false;
                    if (dr["Duree"] != DBNull.Value && (dr["HeureDebut"] == DBNull.Value || DBNull.Value == dr["HeureFin"]))
                    {
                        if (DBNull.Value == dr["HeureDebut"] && DBNull.Value == dr["HeureFin"])//tested
                        {
                            fc_InsertErr(dr["CodeImmeuble"] + "", Convert.ToInt32(dr["Nointervention"]), "Heure de début et fin manquante.", dr["Intervenant"] + "", Convert.ToDateTime(dr["DateRealise"]), tpErr);

                        }
                        else if (DBNull.Value == dr["HeureDebut"])//tested
                        {
                            fc_InsertErr(dr["CodeImmeuble"] + "", Convert.ToInt32(dr["Nointervention"]), "Heure de début manquante.", dr["Intervenant"] + "", Convert.ToDateTime(dr["DateRealise"]), tpErr);

                        }
                        else if (DBNull.Value == dr["HeureFin"])//tested
                        {
                            fc_InsertErr(dr["CodeImmeuble"] + "", Convert.ToInt32(dr["Nointervention"]), "Heure de fin manquante.", dr["Intervenant"] + "", Convert.ToDateTime(dr["DateRealise"]), tpErr);
                        }
                        //=== si l'heure de début et fin existe et que la durée n'est pas calculé on genere une erreur.
                        bErr = true;
                        Array.Resize(ref tpErr, iErr + 1);
                        tpErr[iErr].sIntervenant = dr["Intervenant"] + "";
                        tpErr[iErr].dtDay = Convert.ToDateTime(dr["DateRealise"]);
                        tpErr[iErr].lNointervention = Convert.ToInt32(dr["Nointervention"]);
                        iErr = iErr + 1;
                    }
                    else if (DBNull.Value != dr["HeureFin"] && (DBNull.Value == dr["HeureDebut"] || DBNull.Value == dr["Duree"]))
                    {

                        if (DBNull.Value == dr["HeureDebut"] && DBNull.Value == dr["Duree"])//TESTED
                        {
                            fc_InsertErr(dr["CodeImmeuble"] + "", Convert.ToInt32(dr["Nointervention"]), "Heure de début et durée manquante.", dr["Intervenant"] + "", Convert.ToDateTime(dr["DateRealise"]), tpErr);

                        }
                        else if (DBNull.Value == dr["HeureDebut"])
                        {
                            fc_InsertErr(dr["CodeImmeuble"] + "", Convert.ToInt32(dr["Nointervention"]), "Heure de début manquante.", dr["Intervenant"] + "", Convert.ToDateTime(dr["DateRealise"]), tpErr);

                        }
                        else if (DBNull.Value == dr["Duree"])//tested
                        {
                            fc_InsertErr(dr["CodeImmeuble"] + "", Convert.ToInt32(dr["Nointervention"]), "Durée manquante.", dr["Intervenant"] + "", Convert.ToDateTime(dr["DateRealise"]), tpErr);
                        }

                        //=== si l'heure de début et fin existe et que la durée n'est pas calculé on genere une erreur.
                        bErr = true;
                        Array.Resize(ref tpErr, iErr + 1);
                        tpErr[iErr].sIntervenant = dr["Intervenant"] + "";
                        tpErr[iErr].dtDay = Convert.ToDateTime(dr["DateRealise"]);
                        tpErr[iErr].lNointervention = Convert.ToInt32(dr["Nointervention"]);
                        iErr = iErr + 1;
                    }
                    else if (DBNull.Value != dr["HeureDebut"] && (DBNull.Value == dr["HeureFin"] || DBNull.Value == dr["Duree"]))
                    {
                        if (DBNull.Value == dr["HeureFin"] && DBNull.Value == dr["Duree"])//tested
                        {
                            fc_InsertErr(dr["CodeImmeuble"] + "", Convert.ToInt32(dr["Nointervention"]), "Heure de fin et durée manquante.", dr["Intervenant"] + "", Convert.ToDateTime(dr["DateRealise"]), tpErr);

                        }
                        else if (DBNull.Value == dr["HeureFin"])
                        {
                            fc_InsertErr(dr["CodeImmeuble"] + "", Convert.ToInt32(dr["Nointervention"]), "Heure de fin manquante.", dr["Intervenant"] + "", Convert.ToDateTime(dr["DateRealise"]), tpErr);

                        }
                        else if (DBNull.Value == dr["Duree"])
                        {
                            fc_InsertErr(dr["CodeImmeuble"] + "", Convert.ToInt32(dr["Nointervention"]), "durée manquante.", dr["Intervenant"] + "", Convert.ToDateTime(dr["DateRealise"]), tpErr);
                        }

                        //=== si l'heure de début et fin existe et que la durée n'est pas calculé on genere une erreur.
                        bErr = true;
                        Array.Resize(ref tpErr, iErr + 1);
                        tpErr[iErr].sIntervenant = dr["Intervenant"] + "";
                        tpErr[iErr].dtDay = Convert.ToDateTime(dr["DateRealise"]);
                        tpErr[iErr].lNointervention = Convert.ToInt32(dr["Nointervention"]);
                        iErr = iErr + 1;
                    }
                    else if (DBNull.Value != dr["HeureFin"] && DBNull.Value != dr["HeureDebut"] && DBNull.Value != dr["Duree"])//tested
                    {
                        bErr = false;
                    }
                    else
                    {
                        //=== erreur non déterminé
                        // bErr = True
                        //=== voir avec fred si statut ci dessus et aucune heure et duréé.
                    }
                    //=== crée une anomalie en si le staut est fifferent de zz,af,d, ts,t.
                    if (General.UCase(sCodeEtat) != General.UCase("ZZ") && General.UCase(sCodeEtat) != General.UCase("AF") && General.UCase(sCodeEtat) != General.UCase("D") && General.UCase(sCodeEtat) != General.UCase("TS") && General.UCase(sCodeEtat) != General.UCase("T") && General.UCase(sCodeEtat) != General.UCase("E") && General.UCase(sCodeEtat) != General.UCase("S") && General.UCase(sCodeEtat) != General.UCase("P3"))//TESTED
                    {

                        fc_InsertErr(dr["CodeImmeuble"] + "", Convert.ToInt32(dr["Nointervention"]), "Le code état " + sCodeEtat + " est interdit," + "seuls les codes états " + cStatutAutorise + " sont autorisés", dr["Intervenant"] + "", Convert.ToDateTime(dr["DateRealise"]), tpErr);

                        //=== si l'heure de début et fin existe et que la durée n'est pas calculé on genere une erreur.
                        bErr = true;
                        Array.Resize(ref tpErr, iErr + 1);
                        tpErr[iErr].sIntervenant = dr["Intervenant"] + "";
                        tpErr[iErr].dtDay = Convert.ToDateTime(dr["DateRealise"]);
                        tpErr[iErr].lNointervention = Convert.ToInt32(dr["Nointervention"]);
                        iErr = iErr + 1;
                    }

                    //=== si une interention liés à un devis à un statut ZZ, on génére une erreur.
                    if (General.UCase(sCodeEtat) == General.UCase("ZZ") && !string.IsNullOrEmpty(sNumeroDevis))//tested
                    {
                        fc_InsertErr(dr["CodeImmeuble"] + "", Convert.ToInt32(dr["Nointervention"]), "Le statut ZZ n'est pas autorisé pour " + "les interventions liés à un devis.", dr["Intervenant"] + "", Convert.ToDateTime(dr["DateRealise"]), tpErr);

                        //=== si l'heure de début et fin existe et que la durée n'est pas calculé on genere une erreur.
                        bErr = true;
                        Array.Resize(ref tpErr, iErr + 1);
                        tpErr[iErr].sIntervenant = dr["Intervenant"] + "";
                        tpErr[iErr].dtDay = Convert.ToDateTime(dr["DateRealise"]);
                        tpErr[iErr].lNointervention = Convert.ToInt32(dr["Nointervention"]);
                        iErr = iErr + 1;
                    }
                    //=== compte les interrventions.
                    if (dtDate != Convert.ToDateTime(dr["DateRealise"]) || General.UCase(sMat) != General.UCase(General.nz(dr["Intervenant"], "") + ""))//tested
                    {
                        lCount = 1;
                        dtDate = Convert.ToDateTime(dr["DateRealise"]);
                        sMat = dr["Intervenant"] + "";
                        lNbInter = 0;
                        lNbDevis = 0;
                        lNbDeplacInter = 0;

                        Array.Resize(ref tpCalHour, X + 1);
                        tpCalHour[X].dtDay = dtDate;
                        tpCalHour[X].sIntervenant = dr["Intervenant"] + "";
                        X = X + 1;
                    }
                    else//TESTED
                    {
                        lCount = lCount + 1;
                        //ReDim Preserve tpCalHour(x)
                        //x = x + 1
                    }
                    if (bErr == false)//tested
                    {
                        if (!string.IsNullOrEmpty(sNumeroDevis))//tested
                        {
                            //=== cumul le nombre d'intervention lié à un devis.
                            //==== modif du 11 06 2015, ne plus prendre en compte les ZZ.
                            if (General.UCase(sCodeEtat) == General.UCase("D") || General.UCase(sCodeEtat) == General.UCase("T") || General.UCase(sCodeEtat) == General.UCase("TS") || General.UCase(sCodeEtat) == General.UCase("S") || General.UCase(sCodeEtat) == General.UCase("E"))//tested
                            {
                                lNbDevis = lNbDevis + 1;
                                tpCalHour[X - 1].lTotDevis = lNbDevis;
                            }

                            if (General.UCase(sCodeEtat) == General.UCase("D") || General.UCase(sCodeEtat) == General.UCase("T") || General.UCase(sCodeEtat) == General.UCase("TS") || General.UCase(sCodeEtat) == General.UCase("E") || General.UCase(sCodeEtat) == General.UCase("S"))//tested
                            {
                                if (DBNull.Value != dr["Duree"] && dr["Duree"] + "" != "")//tested
                                {
                                    tpCalHour[X - 1].dtCumulHfactDevis = tpCalHour[X - 1].dtCumulHfactDevis + Convert.ToDateTime(dr["Duree"]).TimeOfDay;
                                    //tpCalHour(X - 1).dtCumulHFactInter = TimeValue(tpCalHour(X - 1).dtCumulHFactInter) + TimeValue(rs!Duree)
                                }
                            }

                            if (General.UCase(sCodeEtat) == General.UCase("D") || General.UCase(sCodeEtat) == General.UCase("T") || General.UCase(sCodeEtat) == General.UCase("TS") || General.UCase(sCodeEtat) == General.UCase("E") || General.UCase(sCodeEtat) == General.UCase("S"))
                            {
                                if (DBNull.Value != dr["Duree"] && dr["Duree"] + "" != "")
                                {
                                    //==== cumul le nombre d'heure d'intervention avec devis.
                                    tpCalHour[X - 1].dtCumulHDevis = tpCalHour[X - 1].dtCumulHDevis + (Convert.ToDateTime(dr["Duree"]).TimeOfDay);
                                }
                            }

                            //=== modif du 09 06 2015, ajout d'une ligne P3.
                            if (General.UCase(sCodeEtat) == General.UCase("P3"))//tested
                            {
                                if (DBNull.Value != dr["Duree"] && dr["Duree"] + "" != "")//tested
                                {
                                    //==== cumul le nombre d'heure zn P3.
                                    tpCalHour[X - 1].dtCumulHP3 = tpCalHour[X - 1].dtCumulHP3 + (Convert.ToDateTime(dr["Duree"]).TimeOfDay);
                                }
                            }

                        }
                        else
                        {
                            //=== cumul le nombre d'intervention.
                            if (General.UCase(sCodeEtat) == General.UCase("T") || General.UCase(sCodeEtat) == General.UCase("TS") || General.UCase(sCodeEtat) == General.UCase("AF") || General.UCase(sCodeEtat) == General.UCase("ZZ") || General.UCase(sCodeEtat) == General.UCase("E") || General.UCase(sCodeEtat) == General.UCase("S") || General.UCase(sCodeEtat) == General.UCase("P3"))//TESTED
                            {

                                lNbInter = lNbInter + 1;
                                tpCalHour[X - 1].lTotInter = lNbInter;
                            }

                            if (General.UCase(sCodeEtat) == General.UCase("T") || General.UCase(sCodeEtat) == General.UCase("TS") || General.UCase(sCodeEtat) == General.UCase("AF") || General.UCase(sCodeEtat) == General.UCase("E") || General.UCase(sCodeEtat) == General.UCase("S"))//TESTED
                            {

                                if (DBNull.Value != dr["Duree"] && dr["Duree"] + "" != "")//TESTED
                                {
                                    //=== cumul le nomdbre d'heure d'intervention sans devis facturé.
                                    tpCalHour[X - 1].dtCumulHFactInter = tpCalHour[X - 1].dtCumulHFactInter + (Convert.ToDateTime(dr["Duree"]).TimeOfDay);
                                }
                            }


                            //====  voir fred si il faut ajouter le code état E.
                            if (General.UCase(sCodeEtat) == General.UCase("AF") || General.UCase(sCodeEtat) == General.UCase("ZZ") || General.UCase(sCodeEtat) == General.UCase("E") || General.UCase(sCodeEtat) == General.UCase("S") || General.UCase(sCodeEtat) == General.UCase("T") || General.UCase(sCodeEtat) == General.UCase("TS") || General.UCase(sCodeEtat) == General.UCase("P3"))//TESTED
                            {
                                if (DBNull.Value != dr["Duree"] && dr["Duree"] + "" != "")//TESTED
                                {
                                    //==== cumul le nombre d'heure d'intervention sans devis.
                                    tpCalHour[X - 1].dtCumulHinter = tpCalHour[X - 1].dtCumulHinter + (Convert.ToDateTime(dr["Duree"]).TimeOfDay);
                                }
                            }

                            if (General.UCase(sCodeEtat) == General.UCase("ZZ") && null != dr["HeureFin"] && null != dr["HeureDebut"] && null != dr["Duree"])//TESTED
                            {
                                if (DBNull.Value != dr["Duree"] && dr["Duree"] + "" != "")//TESTED
                                {
                                    //==== cumul le nombre d'heure d'intervention sans devis non facturé.
                                    tpCalHour[X - 1].dtCumulHNofact = tpCalHour[X - 1].dtCumulHNofact + (Convert.ToDateTime(dr["Duree"]).TimeOfDay);
                                }
                            }

                            //If UCase(sCodeEtat) = UCase("T") Or UCase(sCodeEtat) = UCase("TS") Or UCase(sCodeEtat) = UCase("AF") Or UCase(sCodeEtat) = UCase("E") Then
                            if (General.UCase(sCodeEtat) == General.UCase("T") || General.UCase(sCodeEtat) == General.UCase("TS") || General.UCase(sCodeEtat) == General.UCase("AF") || General.UCase(sCodeEtat) == General.UCase("E") || General.UCase(sCodeEtat) == General.UCase("S") || General.UCase(sCodeEtat) == General.UCase("P3"))//TESTED
                            {
                                //=== cumul le nomdbre de déplacement facturé.
                                lNbDeplacInter = lNbDeplacInter + 1;
                                tpCalHour[X - 1].lTotDeplacementFact = lNbDeplacInter;
                            }

                            //=== modif du 09 06 2015, ajout d'une ligne P3.
                            if (General.UCase(sCodeEtat) == General.UCase("P3"))
                            {
                                if (DBNull.Value != dr["Duree"] && dr["Duree"] + "" != "")
                                {
                                    //==== cumul le nombre d'heure d'intervention P3.
                                    tpCalHour[X - 1].dtCumulHP3 = tpCalHour[X - 1].dtCumulHP3 + (Convert.ToDateTime(dr["Duree"]).TimeOfDay);
                                }
                            }
                        }
                    }
                }
                ModAdo.fc_CloseRecordset(rs);
                rs = null;
                string sHour = null;
                if (bExport == true)
                {
                    //== Start Excel and get Application object.
                    oXL = new Microsoft.Office.Interop.Excel.Application();
                    //== ouvre le fichier modele de excel.
                    //Set oWBMod = oXL.Workbooks.Open("C:\DATA\Donnee\delostal\data\Modele\ModeleHeure.xlsx")
                    //=== charge la feuille matrice.
                    //Set oSheetMod = oWBMod.Sheets(1) '"Matrice")

                    //=== créee un  nouveau classeur excel.
                    oWB = oXL.Workbooks.Add();
                    oSheet = oWB.ActiveSheet;
                    oSheet.Cells[1, 2].value = "DU " + Convert.ToDateTime(dtDeb.Value).ToShortDateString() + " AU " + Convert.ToDateTime(dtFin.Value).ToShortDateString();
                    sMat = "";
                    lFistRow = 0;
                    for (i = 0; i < tpCalHour.Length; i++)
                    {
                        System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                        if (General.UCase(sMat) != General.UCase(tpCalHour[i].sIntervenant))//TESTED
                        {
                            sMat = tpCalHour[i].sIntervenant;
                            if (lFistRow == 0)//tested
                            {
                                lFistRow = 3;
                            }
                            else//tested
                            {
                                lFistRow = lFistRow + 14;
                            }

                            fc_CreateMatrice(ref oSheet, Convert.ToDateTime(dtDeb.Value).Month, Convert.ToDateTime(dtDeb.Value).Year, lFistRow, sMat, tpErr);

                            //==== inscrit le nom de l'intervenant.
                            oSheet.Cells[lFistRow, 1].value = ModAdo.fc_ADOlibelle("SELECT     Nom, Prenom From Personnel WHERE  Matricule = '" + StdSQLchaine.gFr_DoublerQuote(tpCalHour[i].sIntervenant) + "'", true);
                            oSheet.Cells[lFistRow, 1].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                        }
                        //=== affiche les valeurs sur excel.
                        //==== affiche des heures d'intervntion sans devis.
                        if (tpCalHour[i].dtCumulHinter.TimeOfDay.ToString() != "00:00:00")//tested
                        {
                            oSheet.Range["JOUR" + tpCalHour[i].dtDay.Day + "L" + lFistRow + CR1 + "M" + sMat].Value = tpCalHour[i].dtCumulHinter.TimeOfDay.ToString();
                        }

                        //==== affiche des heures d'intervntion avec devis.
                        if (tpCalHour[i].dtCumulHDevis.TimeOfDay.ToString() != "00:00:00")//tested
                        {
                            oSheet.Range["JOUR" + tpCalHour[i].dtDay.Day + "L" + lFistRow + CR2 + "M" + sMat].Value = tpCalHour[i].dtCumulHDevis.TimeOfDay.ToString();
                        }

                        //==== affiche le nombre d'intervntion sans devis.
                        if (tpCalHour[i].lTotInter != 0)//tested
                        {
                            oSheet.Range["JOUR" + tpCalHour[i].dtDay.Day + "L" + lFistRow + CR3 + "M" + sMat].Value = tpCalHour[i].lTotInter;
                        }

                        //=== afiiche le nombre d 'intervention avec devis.
                        if (tpCalHour[i].lTotDevis != 0)//tested
                        {
                            oSheet.Range["JOUR" + tpCalHour[i].dtDay.Day + "L" + lFistRow + cR4 + "M" + sMat].Value = tpCalHour[i].lTotDevis;
                        }

                        //=== afiiche le nombre d'heure des interventions sans devis facturé.
                        if (tpCalHour[i].dtCumulHFactInter.TimeOfDay.ToString() != "00:00:00")//tested
                        {
                            oSheet.Range["JOUR" + tpCalHour[i].dtDay.Day + "L" + lFistRow + cR7 + "M" + sMat].Value = tpCalHour[i].dtCumulHFactInter.TimeOfDay.ToString();
                        }

                        //=== afiiche le nombre d'heure des interventions sans devis facturé.
                        if (tpCalHour[i].dtCumulHfactDevis.TimeOfDay.ToString() != "00:00:00")//tested
                        {
                            oSheet.Range["JOUR" + tpCalHour[i].dtDay.Day + "L" + lFistRow + cR8 + "M" + sMat].Value = tpCalHour[i].dtCumulHfactDevis.TimeOfDay.ToString();
                        }

                        //=== afiiche le nombre d'heure des interventions sans devis NON  facturé.
                        if (tpCalHour[i].dtCumulHNofact.TimeOfDay.ToString() != "00:00:00")//tested
                        {
                            oSheet.Range["JOUR" + tpCalHour[i].dtDay.Day + "L" + lFistRow + cR9 + "M" + sMat].Value = tpCalHour[i].dtCumulHNofact.TimeOfDay.ToString();
                        }

                        //=== affiche le nombre de déplacememnt facturé.
                        if (tpCalHour[i].lTotDeplacementFact != 0)//tested
                        {
                            oSheet.Range["JOUR" + tpCalHour[i].dtDay.Day + "L" + lFistRow + cR10 + "M" + sMat].Value = tpCalHour[i].lTotDeplacementFact;
                        }

                        //=== afiiche le nombre d'heure des interventions avec ou sans devis avec le statut P3.
                        if (tpCalHour[i].dtCumulHP3.TimeOfDay.ToString() != "00:00:00")
                        {
                            oSheet.Range["JOUR" + tpCalHour[i].dtDay.Day + "L" + lFistRow + cR11 + "M" + sMat].Value = tpCalHour[i].dtCumulHP3.TimeOfDay.ToString();
                        }

                    }

                    oXL.Visible = true;
                }
                fc_LoadErr();
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                if (bFindErr == true)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention des erreurs ont été décelés, veuillez consulter la grille des anomalies.", "Erreur", MessageBoxButtons.OK, MessageBoxIcon
                        .Information);
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadData");
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="lNoInterv"></param>
        /// <param name="sErreur"></param>
        /// <param name="sMat"></param>
        /// <param name="dtDate"></param>
        /// <param name="tpErr"></param>
        private void fc_InsertErr(string sCodeImmeuble, int lNoInterv, string sErreur, string sMat, System.DateTime dtDate, TypeErr[] tpErr)
        {
            string sSQL = null;
            int i = 0;
            bool bFind = false;

            // ERROR: Not supported in C#: OnErrorStatement

            try
            {
                bFindErr = true;

                bFind = false;

                for (i = 0; i < tpErr.Length; i++)//tested
                {
                    if (!string.IsNullOrEmpty(tpErr[i].sIntervenant))//tested
                    {
                        if (tpErr[i].dtDay == dtDate && General.UCase(tpErr[i].sIntervenant) == General.UCase(sMat) && tpErr[i].lNointervention == lNoInterv)//tested
                        {
                            bFind = true;
                            break; 
                        }
                    }
                }

                if (bFind == false)//TESTED
                {
                    sSQL = "INSERT INTO HMI_HourMoInterv" + " (Codeimmeuble, NoIntervention, Erreur, Matricule, DateRealise, Controle, CreeLe, CreePar) " + " VALUES ('" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'," + lNoInterv + " , '" + StdSQLchaine.gFr_DoublerQuote(General.Left(sErreur, 500)) + "','" + StdSQLchaine.gFr_DoublerQuote(sMat) + "','" + dtDate + "',0,'" + DateTime.Now + "','" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "')";

                    General.Execute(sSQL);
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_InsertErr");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="oSheet"></param>
        /// <param name="lMonth"></param>
        /// <param name="lYear"></param>
        /// <param name="lFistRow"></param>
        /// <param name="sMat"></param>
        /// <param name="tpErr"></param>
        /// <returns></returns>
        private object fc_CreateMatrice(ref Microsoft.Office.Interop.Excel.Worksheet oSheet, int lMonth,
             int lYear, int lFistRow, string sMat, TypeErr[] tpErr)
        {
            object functionReturnValue = null;
            System.DateTime dtFirstDay = default(System.DateTime);
            System.DateTime dtEndDay = default(System.DateTime);
            System.DateTime dtemp = default(System.DateTime);
            int X = 0;
            int lday = 0;
            bool bFirstDo = false;
            string sName = null;
            int lPos = 0;
            int lPosDebWeek = 0;
            int lTab = 0;
            int Lxxx = 0;
            TypeTotGen[] sTabTotGen = null;
            string sFormTotGen = null;
            try
            {
                sTabTotGen = null;

                //=== défini le 1er et le 2 eme jour du mois.
                dtFirstDay = Convert.ToDateTime("01/" + lMonth + "/" + lYear);
                dtEndDay = fDate.fc_FinDeMois(Convert.ToString(dtFirstDay.Month), Convert.ToString(dtFirstDay.Year));

                X = lFistRow + 1;

                oSheet.Cells[lFistRow + CR1, 1].value = "NOMBRE HEURE / INTER";
                oSheet.Cells[lFistRow + CR2, 1].value = "NOMBRE HEURE /DEVIS";
                oSheet.Cells[lFistRow + CR3, 1].value = "NOMBRE INTERVENTION";
                oSheet.Cells[lFistRow + cR4, 1].value = "NOMBRE DEVIS";
                oSheet.Cells[lFistRow + cR5, 1].value = "PAUSE DEJEUNER";
                oSheet.Cells[lFistRow + cR6, 1].value = "TOTAL HEURE";
                oSheet.Cells[lFistRow + cR7, 1].value = "HEURE FACTURE/INTER";
                oSheet.Cells[lFistRow + cR8, 1].value = "HEURE FACTURE/DEVIS";
                oSheet.Cells[lFistRow + cR9, 1].value = "HEURE NON FACTURE";
                oSheet.Cells[lFistRow + cR10, 1].value = "NBRE DEPLACEMENT FACTURE";
                oSheet.Cells[lFistRow + cR11, 1].value = "HEURE P3";

                oSheet.Range[oSheet.Cells[lFistRow + CR1, 1], oSheet.Cells[lFistRow + cR6, 1]].Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                oSheet.Range[oSheet.Cells[lFistRow + cR7, 1], oSheet.Cells[lFistRow + cR11, 1]].Borders.LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                //=== agrandit automatiquement la colonne selon la taille du texte.
                oSheet.Range[oSheet.Cells[X + CR1, 1], oSheet.Cells[X + cR6, 1]].EntireColumn.AutoFit();


                //=== crée les colonnes jours sur le mois donnée.
                dtemp = dtFirstDay;
                bFirstDo = true;
                lPos = 1 + cDeb;
                lPosDebWeek = lPos - 1;
                lTab = 0;
                //ReDim Preserve sTabTotGen(lTab)
                sTabTotGen = new TypeTotGen[lTab + 2];
                sTabTotGen[lTab].sAdresseCel = new string[11];
                while (dtemp <= dtEndDay)
                {

                    fc_ColorErr(tpErr, lPos, lFistRow, ref oSheet, sMat, dtemp);

                    if ((dtemp.DayOfWeek == DayOfWeek.Sunday && bFirstDo == false) || dtemp == dtEndDay)//TESTED
                    {
                        //=== si le jour correspond au dimanche on le grise et on ajoute
                        //=== un colonne total ainsi qu'une formule, sauf si le premier jour du mois
                        //=== est un dimanche.
                        // Stop
                        //=== affiche le jour.
                        oSheet.Cells[lFistRow, lPos].value = dtemp.Day;
                        oSheet.Cells[lFistRow, lPos].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                        oSheet.Cells[lFistRow, lPos].ColumnWidth = cLargeur;
                        //=== applique une formule.
                        oSheet.Range[oSheet.Cells[lFistRow + cR6, lPos], oSheet.Cells[lFistRow + cR6, lPos]].NumberFormat = "[h]:mm";
                        oSheet.Range[oSheet.Cells[lFistRow + cR6, lPos], oSheet.Cells[lFistRow + cR6, lPos]].FormulaR1C1 = "=SUM(R[-5]C:R[-4]C)";
                        //=== ajoute unh motifs à l'intérieur de la cellule
                        oSheet.Range[oSheet.Cells[lFistRow + CR1, lPos], oSheet.Cells[lFistRow + cR6, lPos]].Interior.Pattern = Microsoft.Office.Interop.Excel.Constants.xlLightDown;
                        oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR11, lPos]].Interior.Pattern = Microsoft.Office.Interop.Excel.Constants.xlLightDown;
                        //=== nomme les cellules.
                        fc_NameCells(ref oSheet, lFistRow, lPos, dtemp.Day, sMat);


                        //===**** ajoute une colonne sous total.
                        //=== applique une formule.
                        fc_FormulaLine(ref oSheet, lFistRow, lPos, lPosDebWeek);
                        //=== format la ligne.
                        oSheet.Range[oSheet.Cells[lFistRow + CR1, lPos + 1], oSheet.Cells[lFistRow + CR1, lPos + 1]].NumberFormat = "[h]:mm";
                        oSheet.Range[oSheet.Cells[lFistRow + CR2, lPos + 1], oSheet.Cells[lFistRow + CR2, lPos + 1]].NumberFormat = "[h]:mm";
                        oSheet.Range[oSheet.Cells[lFistRow + cR6, lPos + 1], oSheet.Cells[lFistRow + cR6, lPos + 1]].NumberFormat = "[h]:mm";
                        oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos + 1], oSheet.Cells[lFistRow + cR7, lPos + 1]].NumberFormat = "[h]:mm";
                        oSheet.Range[oSheet.Cells[lFistRow + cR8, lPos + 1], oSheet.Cells[lFistRow + cR8, lPos + 1]].NumberFormat = "[h]:mm";
                        oSheet.Range[oSheet.Cells[lFistRow + cR9, lPos + 1], oSheet.Cells[lFistRow + cR9, lPos + 1]].NumberFormat = "[h]:mm";
                        oSheet.Range[oSheet.Cells[lFistRow + cR11, lPos + 1], oSheet.Cells[lFistRow + cR11, lPos + 1]].NumberFormat = "[h]:mm";

                        //=== stocke la position du début de semaine.
                        lPosDebWeek = lPos - 1 + 2;
                        lPos = lPos + 1;
                        oSheet.Cells[lFistRow, lPos].value = "TOTAL";
                        //=== centre la ligne.
                        oSheet.Range[oSheet.Cells[lFistRow, lPos], oSheet.Cells[lFistRow + cR6, lPos]].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                        oSheet.Range[oSheet.Cells[lFistRow, lPos], oSheet.Cells[lFistRow + cR6, lPos]].ColumnWidth = cLargeur;
                        //=== crée un 1 er encadré.
                        oSheet.Range[oSheet.Cells[lFistRow, lPos], oSheet.Cells[lFistRow + cR6, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        oSheet.Range[oSheet.Cells[lFistRow, lPos], oSheet.Cells[lFistRow + cR6, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        oSheet.Range[oSheet.Cells[lFistRow, lPos], oSheet.Cells[lFistRow + cR6, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        oSheet.Range[oSheet.Cells[lFistRow, lPos], oSheet.Cells[lFistRow + cR6, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        oSheet.Range[oSheet.Cells[lFistRow, lPos], oSheet.Cells[lFistRow + cR6, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;
                        oSheet.Range[oSheet.Cells[lFistRow, lPos], oSheet.Cells[lFistRow + cR6, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;
                        oSheet.Range[oSheet.Cells[lFistRow, lPos], oSheet.Cells[lFistRow + cR6, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;
                        oSheet.Range[oSheet.Cells[lFistRow, lPos], oSheet.Cells[lFistRow + cR6, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;

                        //=== crée un 2 er encadré.
                        oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR11, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR11, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR11, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                        oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR11, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                        oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR11, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;
                        oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR11, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;
                        oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR11, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;
                        oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR11, lPos]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;




                        //=== stocke m'adresse des cellules sous total afin de gérer un total général.

                        // sTabTotGen (lTab) =
                        //ReDim Preserve sTabTotGen(lTab).sAdresseCel(0)
                        sTabTotGen[lTab].sAdresseCel[0] = sTabTotGen[lTab].sAdresseCel[0] + oSheet.Range[oSheet.Cells[lFistRow + CR1, lPos], oSheet.Cells[lFistRow + CR1, lPos]].Address + ";";

                        //ReDim Preserve sTabTotGen(lTab).sAdresseCel(1)
                        sTabTotGen[lTab].sAdresseCel[1] = sTabTotGen[lTab].sAdresseCel[1] + oSheet.Range[oSheet.Cells[lFistRow + CR2, lPos], oSheet.Cells[lFistRow + CR2, lPos]].Address + ";";

                        //ReDim Preserve sTabTotGen(lTab).sAdresseCel(2)
                        sTabTotGen[lTab].sAdresseCel[2] = sTabTotGen[lTab].sAdresseCel[2] + oSheet.Range[oSheet.Cells[lFistRow + CR3, lPos], oSheet.Cells[lFistRow + CR3, lPos]].Address + ";";

                        //ReDim Preserve sTabTotGen(lTab).sAdresseCel(3)
                        sTabTotGen[lTab].sAdresseCel[3] = sTabTotGen[lTab].sAdresseCel[3] + oSheet.Range[oSheet.Cells[lFistRow + cR4, lPos], oSheet.Cells[lFistRow + cR4, lPos]].Address + ";";

                        //ReDim Preserve sTabTotGen(lTab).sAdresseCel(4)
                        sTabTotGen[lTab].sAdresseCel[4] = sTabTotGen[lTab].sAdresseCel[4] + oSheet.Range[oSheet.Cells[lFistRow + cR6, lPos], oSheet.Cells[lFistRow + cR6, lPos]].Address + ";";

                        //ReDim Preserve sTabTotGen(lTab).sAdresseCel(5)
                        sTabTotGen[lTab].sAdresseCel[5] = sTabTotGen[lTab].sAdresseCel[5] + oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR7, lPos]].Address + ";";

                        //ReDim Preserve sTabTotGen(lTab).sAdresseCel(6)
                        sTabTotGen[lTab].sAdresseCel[6] = sTabTotGen[lTab].sAdresseCel[6] + oSheet.Range[oSheet.Cells[lFistRow + cR8, lPos], oSheet.Cells[lFistRow + cR8, lPos]].Address + ";";

                        //ReDim Preserve sTabTotGen(lTab).sAdresseCel(7)
                        sTabTotGen[lTab].sAdresseCel[7] = sTabTotGen[lTab].sAdresseCel[7] + oSheet.Range[oSheet.Cells[lFistRow + cR9, lPos], oSheet.Cells[lFistRow + cR9, lPos]].Address + ";";

                        //ReDim Preserve sTabTotGen(lTab).sAdresseCel(8)
                        sTabTotGen[lTab].sAdresseCel[8] = sTabTotGen[lTab].sAdresseCel[8] + oSheet.Range[oSheet.Cells[lFistRow + cR10, lPos], oSheet.Cells[lFistRow + cR10, lPos]].Address + ";";

                        //ReDim Preserve sTabTotGen(lTab).sAdresseCel(9)
                        sTabTotGen[lTab].sAdresseCel[9] = sTabTotGen[lTab].sAdresseCel[9] + oSheet.Range[oSheet.Cells[lFistRow + cR11, lPos], oSheet.Cells[lFistRow + cR11, lPos]].Address + ";";


                        //====**** ajoute une colonne total général aprés le dernier jours.
                        if (dtemp == dtEndDay)//TESTED
                        {
                            //=== ajoute une colonne sous total géneral.
                            lPos = lPos + 1;
                            oSheet.Cells[lFistRow, lPos].value = "TOTAL GEN.";

                            //TODO ===> EXCEL 2013. probleme with function SOMME doesn't work. so i changed it to SUM but in vb they use SOMME function

                            sFormTotGen = "=SUM(";
                            sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[0];
                            sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            sFormTotGen = sFormTotGen + ")";
                            sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            oSheet.Range[oSheet.Cells[lFistRow + CR1, lPos], oSheet.Cells[lFistRow + CR1, lPos]].FormulaLocal = sFormTotGen;
                            //.Range[.Cells(lFistRow + CR1, lPos], .Cells(lFistRow + CR1, lPos]].value = "test"
                            oSheet.Cells[lFistRow + CR1, lPos].NumberFormat = "[h]:mm";

                            sFormTotGen = "=SUM(";
                            sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[1];
                            sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            sFormTotGen = sFormTotGen + ")";
                            sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            oSheet.Range[oSheet.Cells[lFistRow + CR2, lPos], oSheet.Cells[lFistRow + CR2, lPos]].FormulaLocal = sFormTotGen;
                            oSheet.Cells[lFistRow + CR2, lPos].NumberFormat = "[h]:mm";

                            sFormTotGen = "=SUM(";
                            sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[2];
                            sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            sFormTotGen = sFormTotGen + ")";
                            sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            oSheet.Range[oSheet.Cells[lFistRow + CR3, lPos], oSheet.Cells[lFistRow + CR3, lPos]].FormulaLocal = sFormTotGen;

                            sFormTotGen = "=SUM(";
                            sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[3];
                            sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            sFormTotGen = sFormTotGen + ")";
                            sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            oSheet.Range[oSheet.Cells[lFistRow + cR4, lPos], oSheet.Cells[lFistRow + cR4, lPos]].FormulaLocal = sFormTotGen;

                            sFormTotGen = "=SUM(";
                            sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[4];
                            sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            sFormTotGen = sFormTotGen + ")";
                            sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            oSheet.Range[oSheet.Cells[lFistRow + cR6, lPos], oSheet.Cells[lFistRow + cR6, lPos]].FormulaLocal = sFormTotGen;
                            oSheet.Cells[lFistRow + cR6, lPos].NumberFormat = "[h]:mm";

                            sFormTotGen = "=SUM(";
                            sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[5];
                            sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            sFormTotGen = sFormTotGen + ")";
                            sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR7, lPos]].FormulaLocal = sFormTotGen;
                            oSheet.Cells[lFistRow + cR7, lPos].NumberFormat = "[h]:mm";

                            sFormTotGen = "=SUM(";
                            sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[6];
                            sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            sFormTotGen = sFormTotGen + ")";
                            sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            oSheet.Range[oSheet.Cells[lFistRow + cR8, lPos], oSheet.Cells[lFistRow + cR8, lPos]].FormulaLocal = sFormTotGen;
                            oSheet.Cells[lFistRow + cR8, lPos].NumberFormat = "[h]:mm";

                            sFormTotGen = "=SUM(";
                            sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[7];
                            sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            sFormTotGen = sFormTotGen + ")";
                            sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            oSheet.Range[oSheet.Cells[lFistRow + cR9, lPos], oSheet.Cells[lFistRow + cR9, lPos]].FormulaLocal = sFormTotGen;
                            oSheet.Cells[lFistRow + cR9, lPos].NumberFormat = "[h]:mm";

                            sFormTotGen = "=SUM(";
                            sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[8];
                            sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            sFormTotGen = sFormTotGen + ")";
                            sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            oSheet.Range[oSheet.Cells[lFistRow + cR10, lPos], oSheet.Cells[lFistRow + cR10, lPos]].FormulaLocal = sFormTotGen;

                            sFormTotGen = "=SUM(";
                            sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[9];
                            sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            sFormTotGen = sFormTotGen + ")";
                            sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            oSheet.Range[oSheet.Cells[lFistRow + cR11, lPos], oSheet.Cells[lFistRow + cR11, lPos]].FormulaLocal = sFormTotGen;
                            oSheet.Cells[lFistRow + cR11, lPos].NumberFormat = "[h]:mm";

                            //sFormTotGen = "=SOMME(";
                            //sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[0];
                            //sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            //sFormTotGen = sFormTotGen + ")";
                            //sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            //oSheet.Range[oSheet.Cells[lFistRow + CR1, lPos], oSheet.Cells[lFistRow + CR1, lPos]].FormulaLocal = sFormTotGen;
                            ////.Range[.Cells(lFistRow + CR1, lPos], .Cells(lFistRow + CR1, lPos]].value = "test"
                            //oSheet.Cells[lFistRow + CR1, lPos].NumberFormat = "[h]:mm";

                            //sFormTotGen = "=SOMME(";
                            //sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[1];
                            //sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            //sFormTotGen = sFormTotGen + ")";
                            //sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            //oSheet.Range[oSheet.Cells[lFistRow + CR2, lPos], oSheet.Cells[lFistRow + CR2, lPos]].FormulaLocal = sFormTotGen;
                            //oSheet.Cells[lFistRow + CR2, lPos].NumberFormat = "[h]:mm";

                            //sFormTotGen = "=SOMME(";
                            //sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[2];
                            //sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            //sFormTotGen = sFormTotGen + ")";
                            //sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            //oSheet.Range[oSheet.Cells[lFistRow + CR3, lPos], oSheet.Cells[lFistRow + CR3, lPos]].FormulaLocal = sFormTotGen;

                            //sFormTotGen = "=SOMME(";
                            //sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[3];
                            //sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            //sFormTotGen = sFormTotGen + ")";
                            //sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            //oSheet.Range[oSheet.Cells[lFistRow + cR4, lPos], oSheet.Cells[lFistRow + cR4, lPos]].FormulaLocal = sFormTotGen;

                            //sFormTotGen = "=SOMME(";
                            //sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[4];
                            //sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            //sFormTotGen = sFormTotGen + ")";
                            //sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            //oSheet.Range[oSheet.Cells[lFistRow + cR6, lPos], oSheet.Cells[lFistRow + cR6, lPos]].FormulaLocal = sFormTotGen;
                            //oSheet.Cells[lFistRow + cR6, lPos].NumberFormat = "[h]:mm";

                            //sFormTotGen = "=SOMME(
                            //sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[5];
                            //sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            //sFormTotGen = sFormTotGen + ")";
                            //sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            //oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR7, lPos]].FormulaLocal = sFormTotGen;
                            //oSheet.Cells[lFistRow + cR7, lPos].NumberFormat = "[h]:mm";

                            //sFormTotGen = "=SOMME(";
                            //sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[6];
                            //sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            //sFormTotGen = sFormTotGen + ")";
                            //sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            //oSheet.Range[oSheet.Cells[lFistRow + cR8, lPos], oSheet.Cells[lFistRow + cR8, lPos]].FormulaLocal = sFormTotGen;
                            //oSheet.Cells[lFistRow + cR8, lPos].NumberFormat = "[h]:mm";

                            //sFormTotGen = "=SOMME(";
                            //sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[7];
                            //sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            //sFormTotGen = sFormTotGen + ")";
                            //sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            //oSheet.Range[oSheet.Cells[lFistRow + cR9, lPos], oSheet.Cells[lFistRow + cR9, lPos]].FormulaLocal = sFormTotGen;
                            //oSheet.Cells[lFistRow + cR9, lPos].NumberFormat = "[h]:mm";

                            //sFormTotGen = "=SOMME(";
                            //sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[8];
                            //sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            //sFormTotGen = sFormTotGen + ")";
                            //sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            //oSheet.Range[oSheet.Cells[lFistRow + cR10, lPos], oSheet.Cells[lFistRow + cR10, lPos]].FormulaLocal = sFormTotGen;

                            //sFormTotGen = "=SOMME(";
                            //sFormTotGen = sFormTotGen + sTabTotGen[lTab].sAdresseCel[9];
                            //sFormTotGen = General.Left(sFormTotGen, General.Len(sFormTotGen) - 1);
                            //sFormTotGen = sFormTotGen + ")";
                            //sFormTotGen = General.Replace(sFormTotGen, "$", "");
                            //oSheet.Range[oSheet.Cells[lFistRow + cR11, lPos], oSheet.Cells[lFistRow + cR11, lPos]].FormulaLocal = sFormTotGen;
                            //oSheet.Cells[lFistRow + cR11, lPos].NumberFormat = "[h]:mm";
                        }

                    }
                    else if (dtemp.DayOfWeek == DayOfWeek.Sunday && bFirstDo == true)//TESTED
                    {
                        //=== affiche le jour.
                        oSheet.Cells[lFistRow, lPos].value = dtemp.Day;
                        oSheet.Cells[lFistRow, lPos].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                        oSheet.Cells[lFistRow, lPos].ColumnWidth = cLargeur;
                        //=== ajoute unh motifs à l'intérieur de la cellule
                        oSheet.Range[oSheet.Cells[lFistRow + CR1, lPos], oSheet.Cells[lFistRow + cR6, lPos]].Interior.Pattern = Microsoft.Office.Interop.Excel.Constants.xlLightDown;
                        oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR11, lPos]].Interior.Pattern = Microsoft.Office.Interop.Excel.Constants.xlLightDown;
                        //=== nomme les cellules.
                        fc_NameCells(ref oSheet, lFistRow, lPos, dtemp.Day, sMat);
                        //=== ajoute une bordure
                        oSheet.Range[oSheet.Cells[lFistRow + CR1, lPos], oSheet.Cells[lFistRow + cR6, lPos]].BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous);
                        oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR11, lPos]].BorderAround(Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous);
                    }
                    else//TESTED
                    {
                        //=== affiche le jour.
                        oSheet.Cells[lFistRow, lPos].value = dtemp.Day;
                        oSheet.Cells[lFistRow, lPos].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;
                        oSheet.Cells[lFistRow, lPos].ColumnWidth = cLargeur;
                        //=== nomme les cellules.
                        fc_NameCells(ref oSheet, lFistRow, lPos, dtemp.Day, sMat);
                        //=== applique une formule et formate la cellule.
                        oSheet.Range[oSheet.Cells[lFistRow + cR6, lPos], oSheet.Cells[lFistRow + cR6, lPos]].NumberFormat = "[h]:mm";
                        oSheet.Range[oSheet.Cells[lFistRow + cR6, lPos], oSheet.Cells[lFistRow + cR6, lPos]].FormulaR1C1 = "=SUM(R[-5]C:R[-4]C)";


                    }
                    //=== ajoute une formule
                    //.Cells(x + cR7, cdeb + lday).
                    lPos = lPos + 1;
                    bFirstDo = false;
                    dtemp = dtemp.AddDays(1);
                }

                //=== encadre l  entete des jours.
                oSheet.Range[oSheet.Cells[lFistRow, cDeb + 1], oSheet.Cells[lFistRow, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                oSheet.Range[oSheet.Cells[lFistRow, cDeb + 1], oSheet.Cells[lFistRow, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                oSheet.Range[oSheet.Cells[lFistRow, cDeb + 1], oSheet.Cells[lFistRow, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                oSheet.Range[oSheet.Cells[lFistRow, cDeb + 1], oSheet.Cells[lFistRow, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                oSheet.Range[oSheet.Cells[lFistRow, cDeb + 1], oSheet.Cells[lFistRow, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeLeft].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;
                oSheet.Range[oSheet.Cells[lFistRow, cDeb + 1], oSheet.Cells[lFistRow, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;
                oSheet.Range[oSheet.Cells[lFistRow, cDeb + 1], oSheet.Cells[lFistRow, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;
                oSheet.Range[oSheet.Cells[lFistRow, cDeb + 1], oSheet.Cells[lFistRow, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;

                //=== encadre le 1er tableau.
                oSheet.Range[oSheet.Cells[lFistRow + 1, 1], oSheet.Cells[lFistRow + cR6, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                oSheet.Range[oSheet.Cells[lFistRow + 1, 1], oSheet.Cells[lFistRow + cR6, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                oSheet.Range[oSheet.Cells[lFistRow + 1, 1], oSheet.Cells[lFistRow + cR6, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                //.Range[.Cells(lFistRow,1], .Cells(lFistRow + R6, lPos - 1]].Borders[xlEdgeLeft].Weight = xlMedium
                oSheet.Range[oSheet.Cells[lFistRow + 1, 1], oSheet.Cells[lFistRow + cR6, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;
                oSheet.Range[oSheet.Cells[lFistRow + 1, 1], oSheet.Cells[lFistRow + cR6, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;
                oSheet.Range[oSheet.Cells[lFistRow + 1, 1], oSheet.Cells[lFistRow + cR6, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;

                //=== encadre le 2 eme tableau.
                oSheet.Range[oSheet.Cells[lFistRow + cR7, 1], oSheet.Cells[lFistRow + cR11, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                oSheet.Range[oSheet.Cells[lFistRow + cR7, 1], oSheet.Cells[lFistRow + cR11, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;
                oSheet.Range[oSheet.Cells[lFistRow + cR7, 1], oSheet.Cells[lFistRow + cR11, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].LineStyle = Microsoft.Office.Interop.Excel.XlLineStyle.xlContinuous;

                oSheet.Range[oSheet.Cells[lFistRow + cR7, 1], oSheet.Cells[lFistRow + cR11, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeTop].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;
                oSheet.Range[oSheet.Cells[lFistRow + cR7, 1], oSheet.Cells[lFistRow + cR11, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeBottom].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;
                oSheet.Range[oSheet.Cells[lFistRow + cR7, 1], oSheet.Cells[lFistRow + cR11, lPos - 1]].Borders[Microsoft.Office.Interop.Excel.XlBordersIndex.xlEdgeRight].Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlMedium;
                return functionReturnValue;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_CreateMatrice");
            }
            return functionReturnValue;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="tpErr"></param>
        /// <param name="lPos"></param>
        /// <param name="lFistRow"></param>
        /// <param name="oSheet"></param>
        /// <param name="sMat"></param>
        /// <param name="dtemp"></param>
        private void fc_ColorErr(TypeErr[] tpErr, int lPos, int lFistRow, ref Microsoft.Office.Interop.Excel.Worksheet oSheet, string sMat, System.DateTime dtemp)
        {
            int o = 0;


            //=== applique une couleur sur une plage de cellule en cas d'erreur décélé.
            for (o = 0; o <= tpErr.Length-1; o++)
            {
                if (!string.IsNullOrEmpty(tpErr[o].sIntervenant))
                {
                    if (General.UCase(sMat) == General.UCase(tpErr[o].sIntervenant) && dtemp == tpErr[o].dtDay)
                    {
                        oSheet.Range[oSheet.Cells[lFistRow + CR1, lPos], oSheet.Cells[lFistRow + cR6, lPos]].Interior.Color = 49407;
                        oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos], oSheet.Cells[lFistRow + cR11, lPos]].Interior.Color = 49407;
                        break; 
                    }
                }
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="oSheet"></param>
        /// <param name="lFistRow"></param>
        /// <param name="lPos"></param>
        /// <param name="lday"></param>
        /// <param name="sMat"></param>
        private void fc_NameCells(ref Microsoft.Office.Interop.Excel.Worksheet oSheet, int lFistRow, int lPos, int lday, string sMat)
        {
            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                //==== nomme les cellules.
                oSheet.Cells[lFistRow + CR1, lPos].Name = "JOUR" + lday + "L" + lFistRow + CR1 + "M" + sMat;
                oSheet.Cells[lFistRow + CR2, lPos].Name = "JOUR" + lday + "L" + lFistRow + CR2 + "M" + sMat;
                oSheet.Cells[lFistRow + CR3, lPos].Name = "JOUR" + lday + "L" + lFistRow + CR3 + "M" + sMat;
                oSheet.Cells[lFistRow + cR4, lPos].Name = "JOUR" + lday + "L" + lFistRow + cR4 + "M" + sMat;
                oSheet.Cells[lFistRow + cR5, lPos].Name = "JOUR" + lday + "L" + lFistRow + cR5 + "M" + sMat;

                oSheet.Cells[lFistRow + cR6, lPos].Name = "JOUR" + lday + "L" + lFistRow + cR6 + "M" + sMat;

                oSheet.Cells[lFistRow + cR7, lPos].Name = "JOUR" + lday + "L" + lFistRow + cR7 + "M" + sMat;

                oSheet.Cells[lFistRow + cR8, lPos].Name = "JOUR" + lday + "L" + lFistRow + cR8 + "M" + sMat;

                oSheet.Cells[lFistRow + cR9, lPos].Name = "JOUR" + lday + "L" + lFistRow + cR9 + "M" + sMat;

                oSheet.Cells[lFistRow + cR10, lPos].Name = "JOUR" + lday + "L" + lFistRow + cR10 + "M" + sMat;

                oSheet.Cells[lFistRow + cR11, lPos].Name = "JOUR" + lday + "L" + lFistRow + cR11 + "M" + sMat;

                //=== modif du 26 05 2015, applique un format sur certaines cellules.

                oSheet.Cells[lFistRow + CR1, lPos].NumberFormat = "h:mm;@";

                oSheet.Cells[lFistRow + CR2, lPos].NumberFormat = "h:mm;@";

                oSheet.Cells[lFistRow + cR7, lPos].NumberFormat = "h:mm;@";

                oSheet.Cells[lFistRow + cR8, lPos].NumberFormat = "h:mm;@";

                oSheet.Cells[lFistRow + cR9, lPos].NumberFormat = "h:mm;@";

                oSheet.Cells[lFistRow + cR11, lPos].NumberFormat = "h:mm;@";

                //=== aligne les cellules

                oSheet.Cells[lFistRow + CR1, lPos].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                oSheet.Cells[lFistRow + CR2, lPos].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                oSheet.Cells[lFistRow + CR3, lPos].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                oSheet.Cells[lFistRow + cR4, lPos].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                oSheet.Cells[lFistRow + cR5, lPos].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                oSheet.Cells[lFistRow + cR6, lPos].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                oSheet.Cells[lFistRow + cR7, lPos].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                oSheet.Cells[lFistRow + cR8, lPos].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                oSheet.Cells[lFistRow + cR9, lPos].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                oSheet.Cells[lFistRow + cR10, lPos].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                oSheet.Cells[lFistRow + cR11, lPos].HorizontalAlignment = Microsoft.Office.Interop.Excel.Constants.xlCenter;

                //=== apllique une taille.

                oSheet.Cells[lFistRow + CR1, lPos].ColumnWidth = cLargeur;

                oSheet.Cells[lFistRow + CR2, lPos].ColumnWidth = cLargeur;

                oSheet.Cells[lFistRow + CR3, lPos].ColumnWidth = cLargeur;

                oSheet.Cells[lFistRow + cR4, lPos].ColumnWidth = cLargeur;

                oSheet.Cells[lFistRow + cR5, lPos].ColumnWidth = cLargeur;

                oSheet.Cells[lFistRow + cR6, lPos].ColumnWidth = cLargeur;

                oSheet.Cells[lFistRow + cR7, lPos].ColumnWidth = cLargeur;

                oSheet.Cells[lFistRow + cR8, lPos].ColumnWidth = cLargeur;

                oSheet.Cells[lFistRow + cR9, lPos].ColumnWidth = cLargeur;

                oSheet.Cells[lFistRow + cR10, lPos].ColumnWidth = cLargeur;

                oSheet.Cells[lFistRow + cR11, lPos].ColumnWidth = cLargeur;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_NameCells");
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="oSheet"></param>
        /// <param name="lFistRow"></param>
        /// <param name="lPos"></param>
        /// <param name="lPosDebWeek"></param>
        private void fc_FormulaLine(ref Microsoft.Office.Interop.Excel.Worksheet oSheet, int lFistRow, int lPos, int lPosDebWeek)
        {
            // ERROR: Not supported in C#: OnErrorStatement
            try
            {
                //==== applique une formaule de sous total de la semaine.
                oSheet.Range[oSheet.Cells[lFistRow + CR1, lPos + 1], oSheet.Cells[lFistRow + CR1, lPos + 1]].FormulaR1C1 = "=SUM(RC[-" + (lPos - lPosDebWeek) + "]:RC[-1])";
                oSheet.Range[oSheet.Cells[lFistRow + CR2, lPos + 1], oSheet.Cells[lFistRow + CR2, lPos + 1]].FormulaR1C1 = "=SUM(RC[-" + (lPos - lPosDebWeek) + "]:RC[-1])";
                oSheet.Range[oSheet.Cells[lFistRow + CR3, lPos + 1], oSheet.Cells[lFistRow + CR3, lPos + 1]].FormulaR1C1 = "=SUM(RC[-" + (lPos - lPosDebWeek) + "]:RC[-1])";
                oSheet.Range[oSheet.Cells[lFistRow + cR4, lPos + 1], oSheet.Cells[lFistRow + cR4, lPos + 1]].FormulaR1C1 = "=SUM(RC[-" + (lPos - lPosDebWeek) + "]:RC[-1])";
                oSheet.Range[oSheet.Cells[lFistRow + cR6, lPos + 1], oSheet.Cells[lFistRow + cR6, lPos + 1]].FormulaR1C1 = "=SUM(RC[-" + (lPos - lPosDebWeek) + "]:RC[-1])";

                oSheet.Range[oSheet.Cells[lFistRow + cR7, lPos + 1], oSheet.Cells[lFistRow + cR7, lPos + 1]].FormulaR1C1 = "=SUM(RC[-" + (lPos - lPosDebWeek) + "]:RC[-1])";
                oSheet.Range[oSheet.Cells[lFistRow + cR8, lPos + 1], oSheet.Cells[lFistRow + cR8, lPos + 1]].FormulaR1C1 = "=SUM(RC[-" + (lPos - lPosDebWeek) + "]:RC[-1])";
                oSheet.Range[oSheet.Cells[lFistRow + cR9, lPos + 1], oSheet.Cells[lFistRow + cR9, lPos + 1]].FormulaR1C1 = "=SUM(RC[-" + (lPos - lPosDebWeek) + "]:RC[-1])";
                oSheet.Range[oSheet.Cells[lFistRow + cR10, lPos + 1], oSheet.Cells[lFistRow + cR10, lPos + 1]].FormulaR1C1 = "=SUM(RC[-" + (lPos - lPosDebWeek) + "]:RC[-1])";
                oSheet.Range[oSheet.Cells[lFistRow + cR11, lPos + 1], oSheet.Cells[lFistRow + cR11, lPos + 1]].FormulaR1C1 = "=SUM(RC[-" + (lPos - lPosDebWeek) + "]:RC[-1])";


                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_NameCells");
            }

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervErr_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                var key = (short)e.KeyChar;
                if (key == 13)
                {
                    var ModAdo = new ModAdo();
                    if (string.IsNullOrEmpty(General.nz(ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + cmbIntervErr.Value + "'"), "") + ""))
                    {
                        //On Error GoTo Erreur

                        string req = "SELECT Personnel.Matricule as \"Matricule\"," + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\"," + " Qualification.Qualification as \"Qualification\"," + " Personnel.Memoguard as \"MemoGuard\"," + " Personnel.NumRadio as \"NumRadio\"" + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";
                        string where = " (NonActif is null or NonActif = 0) ";
                        var Search = new SearchTemplate(null, null, req, where, "");
                        Search.SetValues(new Dictionary<string, string>
                        {
                            {General.IsNumeric(cmbIntervErr.Text) ? "Matricule" : "Nom", cmbIntervErr.Text}
                        });
                        Search.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            // charge les enregistrements de l'immeuble
                            cmbIntervErr.Text = Search.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                            lblLibIntervenanterr.Text = Search.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                            Search.Close();
                        };

                        Search.ugResultat.KeyPress += (se, ev) =>
                        {
                            if ((short)ev.KeyChar == 13)
                            {
                                if (Search.ugResultat.ActiveRow != null)
                                {
                                    cmbIntervErr.Text = Search.ugResultat.ActiveRow.GetCellValue("Matricule") + "";
                                    lblLibIntervenanterr.Text = Search.ugResultat.ActiveRow.GetCellValue("Nom") + "";
                                    Search.Close();
                                    Search.Dispose();
                                }
                            }
                        };
                        Search.ShowDialog();
                    }
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervErr_KeyPress");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExecute_Click(object sender, EventArgs e)
        {
            fc_LoadData(false);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExportExcel_Click(object sender, EventArgs e)
        {
            if (!(DTPicker1.ButtonsLeft[0] as StateEditorButton).Checked)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur dans la 'date réalisée du'");
                return;

            }
            if (!(DTPicker2.ButtonsLeft[0] as StateEditorButton).Checked)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir une valeur dans la 'date réalisée au'");
                return;

            }
            fc_LoadErr(true);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFndMatErr_Click(object sender, EventArgs e)
        {
            cmbIntervErr_KeyPress(cmbIntervErr, new KeyPressEventArgs((char)13));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheIntervenant_Click(object sender, EventArgs e)
        {
            cmbIntervenant_KeyPress(cmbIntervenant, new KeyPressEventArgs((char)13));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            cmbCodeimmeuble_KeyPress(cmbCodeimmeuble, new KeyPressEventArgs((char)13));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            fc_LoadData(true);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            GridHMI.UpdateData();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdService_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;

            sSQL = "SELECT     SCH_Code AS Code, SCH_Libelle AS Libelle" + " From SCH_ServiceCtrlHeure " + " ORDER BY SCH_Code";

            sheridan.InitialiseCombo(cmdService, sSQL, "Code");

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdService2_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;

            sSQL = "SELECT     SCH_Code AS Code, SCH_Libelle AS Libelle" + " From SCH_ServiceCtrlHeure " + " ORDER BY SCH_Code";

            sheridan.InitialiseCombo(cmdService2, sSQL, "Code");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridHMI_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            if (GridHMI.ActiveRow.Cells["Controle"].Text == "-1")
            {
                GridHMI.ActiveRow.Cells["Controle"].Value = 1;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridHMI_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {
            if (GridHMI.ActiveRow != null)
            {
                GridHMI.ActiveRow.Cells["ModifieLe"].Value = DateTime.Now.ToShortDateString();
                GridHMI.ActiveRow.Cells["ModifiePar"].Value = General.fncUserName();
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridHMI_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, GridHMI.ActiveRow.Cells["NoIntervention"].Value + "");

            View.Theme.Theme.Navigate(typeof(UserIntervention));
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeimmeuble_BeforeDropDown(object sender, CancelEventArgs e)
        {
            if (cmbCodeimmeuble.Rows.Count < 1)
            {
                string req = "SELECT Immeuble.CodeImmeuble, Immeuble.Adresse,"
                             + " Immeuble.CodePostal, Immeuble.Ville"
                             + " From Immeuble"
                             + " order by CodeImmeuble";
                sheridan.InitialiseCombo(cmbCodeimmeuble, req, "codeImmeuble");
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCodeimmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            var index = (short)e.KeyChar;
            if (index == 13)
            {
                var ModAdo = new ModAdo();
                if (string.IsNullOrEmpty(General.nz(ModAdo.fc_ADOlibelle("SELECT Immeuble.CodeImmeuble FROM Immeuble WHERE Immeuble.CodeImmeuble='" + cmbCodeimmeuble.Text + "'"), "") + ""))
                {
                    string req = "SELECT CodeImmeuble as \"Code Immeuble\"," + " Adresse as \"Adresse\"," + " Ville as \"Ville\", anglerue as \"Angle de rue\", Code1 as \"Gerant\" FROM Immeuble ";
                    var search = new SearchTemplate(null, null, req, "", "") { Text = "Recherche d'un immeuble" };
                    search.SetValues(new Dictionary<string, string> { { "CodeImmeuble", cmbCodeimmeuble.Text } });

                    search.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        cmbCodeimmeuble.Text = search.ugResultat.ActiveRow.GetCellValue("Code Immeuble") + "";
                        search.Close();
                    };
                    search.ugResultat.KeyPress += (se, ev) =>
                    {
                        if ((short)ev.KeyChar == 13)
                        {
                            if (search.ugResultat.ActiveRow != null)
                            {
                                cmbCodeimmeuble.Text = search.ugResultat.ActiveRow.GetCellValue("Code Immeuble") + "";
                                search.Close();
                                search.Dispose();
                            }
                        }
                    };
                    search.FormClosed += (se, ev) =>
                    {
                        return;
                    };
                    search.ShowDialog();
                }
                cmbIntervention_Click(cmbIntervention, null);
            }
            return;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervention_Click(object sender, EventArgs e)
        {
            fc_LoadErr();
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void label5_Click(object sender, EventArgs e)
        {
            if (txtNoInterventionBis.Visible == true)
            {
                txtNoInterventionBis.Visible = false;
                lblNoIntervention.Visible = false;
            }
            else
            {
                txtNoInterventionBis.Visible = true;
                lblNoIntervention.Visible = true;
            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervErr_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;

            sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom," + " Qualification.CodeQualif, Qualification.Qualification" + " FROM Personnel INNER JOIN" + " Qualification ON Personnel.CodeQualif = Qualification.CodeQualif";
            sSQL = sSQL + " WHERE (NonActif is null or NonActif = 0) ";

            sSQL = sSQL + " order by matricule";

            sheridan.InitialiseCombo(cmbIntervErr, sSQL, "Matricule");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervErr_AfterCloseUp(object sender, EventArgs e)
        {
            var ModAdo = new ModAdo();
            lblLibIntervenanterr.Text = ModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + cmbIntervErr.Value + "'");
        }

        private void cmbIntervErr_Click(object sender, EventArgs e)
        {
            //fc_LoadErr();

        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocCtrlHeurePers_VisibleChanged(object sender, EventArgs e)
        {
            ModMain.bActivate = false;
            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocCtrlHeurePers");
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridHMI_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

            GridHMI.DisplayLayout.Bands[0].Columns["Controle"].Header.Caption = "Controlé";
            GridHMI.DisplayLayout.Bands[0].Columns["Codeimmeuble"].Header.Caption = "Code Immeuble";
            GridHMI.DisplayLayout.Bands[0].Columns["NoIntervention"].Header.Caption = "No Intervention";
            GridHMI.DisplayLayout.Bands[0].Columns["DateRealise"].Header.Caption = "Date Réalisée";
            GridHMI.DisplayLayout.Bands[0].Columns["CreeLe"].Header.Caption = "Crée Le";
            GridHMI.DisplayLayout.Bands[0].Columns["CreePar"].Header.Caption = "Crée Par";
            GridHMI.DisplayLayout.Bands[0].Columns["ModifieLe"].Header.Caption = "Modifié Le";
            GridHMI.DisplayLayout.Bands[0].Columns["ModifiePar"].Header.Caption = "Modifié Par";
            GridHMI.DisplayLayout.Bands[0].Columns["Controle"].Style = ColumnStyle.CheckBox;
            GridHMI.DisplayLayout.Bands[0].Columns["HMI_ID"].Hidden = true;
            foreach (var column in GridHMI.DisplayLayout.Bands[0].Columns)
            {
                if (column.Style != ColumnStyle.CheckBox && column.Key.ToLower() != "erreur")
                    column.CellActivation = Activation.NoEdit;
            }
            GridHMI.Rows.ToList().ForEach(l => l.Cells["Erreur"].Appearance.BackColor = l.Cells["Controle"].Appearance.BackColor = l.IsAlternate ? Color.LightGoldenrodYellow : Color.White);
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridHMI_InitializeRow(object sender, InitializeRowEventArgs e)
        {
            if (e.Row.Cells["Controle"].Value + "" == "1")
                e.Row.Cells["Controle"].Value = true;
            else
                e.Row.Cells["Controle"].Value = false;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DTPicker1_BeforeDropDown(object sender, CancelEventArgs e)
        {
            var dtPicker = sender as UltraDateTimeEditor;
            var stateButton = dtPicker.ButtonsLeft[0] as StateEditorButton;
            if (!stateButton.Checked)
                stateButton.Checked = true;
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridHMI_AfterRowUpdate(object sender, RowEventArgs e)
        {
            if (GridHMI.ActiveRow != null)
            {
                string SQL = "Update HMI_HourMoInterv set Erreur='" + e.Row.Cells["Erreur"].Value + "', Controle='" + Convert.ToInt16(e.Row.Cells["Controle"].Value) + "', ModifieLe='" + Convert.ToDateTime(e.Row.Cells["ModifieLe"].Value).ToShortDateString() + "', ModifiePar='" + e.Row.Cells["ModifiePar"].Value + "'";
               
                string where = "";
                if (e.Row.Cells["HMI_ID"].Value + "" != "")
                    where = " WHERE HMI_ID='" + e.Row.Cells["HMI_ID"].Value + "'";
                if (!string.IsNullOrEmpty(where))
                    General.Execute(SQL + where);
                GridHMI.UpdateData();
            }

        }
    }
}
