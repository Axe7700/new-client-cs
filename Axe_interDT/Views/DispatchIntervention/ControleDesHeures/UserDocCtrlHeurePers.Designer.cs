namespace Axe_interDT.Views.DispatchIntervention.ControleDesHeures
{
    partial class UserDocCtrlHeurePers
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton1 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton2 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton3 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton4 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.Appearance appearance49 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance50 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance51 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance52 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance53 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance54 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance55 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance56 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance57 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance58 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance59 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance60 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance61 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance62 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance63 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance64 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance65 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance66 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance67 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance68 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance69 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance70 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance71 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance72 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdService = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.optService = new System.Windows.Forms.RadioButton();
            this.cmdRechercheIntervenant = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.OptParInterv = new System.Windows.Forms.RadioButton();
            this.Option2 = new System.Windows.Forms.RadioButton();
            this.dtDeb = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cmbIntervenant = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblLibIntervenant = new System.Windows.Forms.Label();
            this.dtFin = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblNoIntervention = new System.Windows.Forms.Label();
            this.txtNoInterventionBis = new iTalk.iTalk_TextBox_Small2();
            this.label9 = new System.Windows.Forms.Label();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Command1 = new System.Windows.Forms.Button();
            this.cmdExecute = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdFndMatErr = new System.Windows.Forms.Button();
            this.label2 = new System.Windows.Forms.Label();
            this.lblLibIntervenanterr = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.optTous = new System.Windows.Forms.RadioButton();
            this.label4 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtNoIntervention = new iTalk.iTalk_TextBox_Small2();
            this.cmbIntervErr = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.DTPicker2 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.cmdService2 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.OptControle = new System.Windows.Forms.RadioButton();
            this.OptNonControle = new System.Windows.Forms.RadioButton();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.DTPicker1 = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.lblTotal = new System.Windows.Forms.Label();
            this.cmdRechercheImmeuble = new System.Windows.Forms.Button();
            this.cmbCodeimmeuble = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.flowLayoutPanel2 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdExportExcel = new System.Windows.Forms.Button();
            this.cmbIntervention = new System.Windows.Forms.Button();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.GridHMI = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdService)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDeb)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIntervenant)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFin)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIntervErr)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTPicker2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdService2)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTPicker1)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCodeimmeuble)).BeginInit();
            this.flowLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridHMI)).BeginInit();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 1;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.ultraGroupBox1, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.ultraGroupBox2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.GridHMI, 0, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 4;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 13F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 197F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 203F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox1.Controls.Add(this.tableLayoutPanel2);
            this.ultraGroupBox1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ultraGroupBox1.Location = new System.Drawing.Point(3, 16);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(1844, 191);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "CONTROLE DES HEURES";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.ultraGroupBox3, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 21);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 24.69136F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 75.30864F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1838, 167);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 6;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 220F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 45.61404F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 106F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 54.38596F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 141F));
            this.tableLayoutPanel5.Controls.Add(this.cmdService, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.optService, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.cmdRechercheIntervenant, 2, 1);
            this.tableLayoutPanel5.Controls.Add(this.label33, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.OptParInterv, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.Option2, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.dtDeb, 1, 3);
            this.tableLayoutPanel5.Controls.Add(this.cmbIntervenant, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.lblLibIntervenant, 3, 1);
            this.tableLayoutPanel5.Controls.Add(this.dtFin, 4, 3);
            this.tableLayoutPanel5.Controls.Add(this.lblNoIntervention, 3, 0);
            this.tableLayoutPanel5.Controls.Add(this.txtNoInterventionBis, 4, 0);
            this.tableLayoutPanel5.Controls.Add(this.label9, 3, 3);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel5.Location = new System.Drawing.Point(3, 44);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1832, 120);
            this.tableLayoutPanel5.TabIndex = 1;
            // 
            // cmdService
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmdService.DisplayLayout.Appearance = appearance1;
            this.cmdService.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmdService.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.cmdService.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmdService.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.cmdService.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmdService.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.cmdService.DisplayLayout.MaxColScrollRegions = 1;
            this.cmdService.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdService.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmdService.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.cmdService.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmdService.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.cmdService.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmdService.DisplayLayout.Override.CellAppearance = appearance8;
            this.cmdService.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmdService.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.cmdService.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.cmdService.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.cmdService.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmdService.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.cmdService.DisplayLayout.Override.RowAppearance = appearance11;
            this.cmdService.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmdService.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.cmdService.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmdService.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmdService.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmdService.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdService.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdService.Location = new System.Drawing.Point(223, 3);
            this.cmdService.Name = "cmdService";
            this.cmdService.Size = new System.Drawing.Size(602, 27);
            this.cmdService.TabIndex = 650;
            this.cmdService.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmdService_BeforeDropDown);
            // 
            // optService
            // 
            this.optService.AutoSize = true;
            this.optService.Checked = true;
            this.optService.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optService.Location = new System.Drawing.Point(3, 3);
            this.optService.Name = "optService";
            this.optService.Size = new System.Drawing.Size(214, 23);
            this.optService.TabIndex = 538;
            this.optService.TabStop = true;
            this.optService.Text = "Par Service pour contrôle heure";
            this.optService.UseVisualStyleBackColor = true;
            // 
            // cmdRechercheIntervenant
            // 
            this.cmdRechercheIntervenant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheIntervenant.FlatAppearance.BorderSize = 0;
            this.cmdRechercheIntervenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheIntervenant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdRechercheIntervenant.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheIntervenant.Location = new System.Drawing.Point(831, 33);
            this.cmdRechercheIntervenant.Name = "cmdRechercheIntervenant";
            this.cmdRechercheIntervenant.Size = new System.Drawing.Size(24, 19);
            this.cmdRechercheIntervenant.TabIndex = 543;
            this.toolTip1.SetToolTip(this.cmdRechercheIntervenant, "Recherche de l\'intervenant");
            this.cmdRechercheIntervenant.UseVisualStyleBackColor = false;
            this.cmdRechercheIntervenant.Click += new System.EventHandler(this.cmdRechercheIntervenant_Click);
            // 
            // label33
            // 
            this.label33.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(3, 95);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(101, 19);
            this.label33.TabIndex = 539;
            this.label33.Text = "Date réalisée";
            // 
            // OptParInterv
            // 
            this.OptParInterv.AutoSize = true;
            this.OptParInterv.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.OptParInterv.Location = new System.Drawing.Point(3, 33);
            this.OptParInterv.Name = "OptParInterv";
            this.OptParInterv.Size = new System.Drawing.Size(134, 23);
            this.OptParInterv.TabIndex = 538;
            this.OptParInterv.Text = "Par intervenant";
            this.OptParInterv.UseVisualStyleBackColor = true;
            // 
            // Option2
            // 
            this.Option2.AutoSize = true;
            this.Option2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Option2.Location = new System.Drawing.Point(3, 63);
            this.Option2.Name = "Option2";
            this.Option2.Size = new System.Drawing.Size(174, 23);
            this.Option2.TabIndex = 538;
            this.Option2.Text = "Tous les intervenants";
            this.Option2.UseVisualStyleBackColor = true;
            // 
            // dtDeb
            // 
            stateEditorButton1.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dtDeb.ButtonsLeft.Add(stateEditorButton1);
            this.dtDeb.DateTime = new System.DateTime(2015, 3, 1, 0, 0, 0, 0);
            this.dtDeb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtDeb.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.dtDeb.Location = new System.Drawing.Point(223, 93);
            this.dtDeb.Name = "dtDeb";
            this.dtDeb.Size = new System.Drawing.Size(602, 26);
            this.dtDeb.TabIndex = 652;
            this.dtDeb.Value = new System.DateTime(2015, 3, 1, 0, 0, 0, 0);
            this.dtDeb.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTPicker1_BeforeDropDown);
            // 
            // cmbIntervenant
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbIntervenant.DisplayLayout.Appearance = appearance13;
            this.cmbIntervenant.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbIntervenant.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenant.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbIntervenant.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.cmbIntervenant.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbIntervenant.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.cmbIntervenant.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbIntervenant.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbIntervenant.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbIntervenant.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.cmbIntervenant.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbIntervenant.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenant.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbIntervenant.DisplayLayout.Override.CellAppearance = appearance20;
            this.cmbIntervenant.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbIntervenant.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenant.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.cmbIntervenant.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.cmbIntervenant.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbIntervenant.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.cmbIntervenant.DisplayLayout.Override.RowAppearance = appearance23;
            this.cmbIntervenant.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbIntervenant.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.cmbIntervenant.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbIntervenant.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbIntervenant.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbIntervenant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbIntervenant.Location = new System.Drawing.Point(223, 33);
            this.cmbIntervenant.Name = "cmbIntervenant";
            this.cmbIntervenant.Size = new System.Drawing.Size(602, 27);
            this.cmbIntervenant.TabIndex = 651;
            this.cmbIntervenant.Tag = "UserDocCtrlHeurePers";
            this.cmbIntervenant.AfterCloseUp += new System.EventHandler(this.cmbIntervenant_AfterCloseUp);
            this.cmbIntervenant.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbIntervenant_BeforeDropDown);
            this.cmbIntervenant.Click += new System.EventHandler(this.cmbIntervenant_Click);
            this.cmbIntervenant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbIntervenant_KeyPress);
            // 
            // lblLibIntervenant
            // 
            this.lblLibIntervenant.AutoSize = true;
            this.lblLibIntervenant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLibIntervenant.Location = new System.Drawing.Point(863, 30);
            this.lblLibIntervenant.Name = "lblLibIntervenant";
            this.lblLibIntervenant.Size = new System.Drawing.Size(0, 19);
            this.lblLibIntervenant.TabIndex = 539;
            // 
            // dtFin
            // 
            stateEditorButton2.CheckState = System.Windows.Forms.CheckState.Checked;
            this.dtFin.ButtonsLeft.Add(stateEditorButton2);
            this.dtFin.DateTime = new System.DateTime(2015, 3, 31, 0, 0, 0, 0);
            this.dtFin.Dock = System.Windows.Forms.DockStyle.Fill;
            this.dtFin.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.dtFin.Location = new System.Drawing.Point(969, 93);
            this.dtFin.Name = "dtFin";
            this.dtFin.Size = new System.Drawing.Size(718, 26);
            this.dtFin.TabIndex = 653;
            this.dtFin.Value = new System.DateTime(2015, 3, 31, 0, 0, 0, 0);
            this.dtFin.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTPicker1_BeforeDropDown);
            // 
            // lblNoIntervention
            // 
            this.lblNoIntervention.AutoSize = true;
            this.lblNoIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblNoIntervention.Location = new System.Drawing.Point(863, 0);
            this.lblNoIntervention.Name = "lblNoIntervention";
            this.lblNoIntervention.Size = new System.Drawing.Size(94, 30);
            this.lblNoIntervention.TabIndex = 539;
            this.lblNoIntervention.Text = "No Intervention";
            this.lblNoIntervention.Visible = false;
            // 
            // txtNoInterventionBis
            // 
            this.txtNoInterventionBis.AccAcceptNumbersOnly = false;
            this.txtNoInterventionBis.AccAllowComma = false;
            this.txtNoInterventionBis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoInterventionBis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoInterventionBis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoInterventionBis.AccHidenValue = "";
            this.txtNoInterventionBis.AccNotAllowedChars = null;
            this.txtNoInterventionBis.AccReadOnly = false;
            this.txtNoInterventionBis.AccReadOnlyAllowDelete = false;
            this.txtNoInterventionBis.AccRequired = false;
            this.txtNoInterventionBis.BackColor = System.Drawing.Color.White;
            this.txtNoInterventionBis.CustomBackColor = System.Drawing.Color.White;
            this.txtNoInterventionBis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoInterventionBis.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNoInterventionBis.ForeColor = System.Drawing.Color.Black;
            this.txtNoInterventionBis.Location = new System.Drawing.Point(968, 2);
            this.txtNoInterventionBis.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoInterventionBis.MaxLength = 32767;
            this.txtNoInterventionBis.Multiline = false;
            this.txtNoInterventionBis.Name = "txtNoInterventionBis";
            this.txtNoInterventionBis.ReadOnly = false;
            this.txtNoInterventionBis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoInterventionBis.Size = new System.Drawing.Size(720, 27);
            this.txtNoInterventionBis.TabIndex = 543;
            this.txtNoInterventionBis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoInterventionBis.UseSystemPasswordChar = false;
            this.txtNoInterventionBis.Visible = false;
            // 
            // label9
            // 
            this.label9.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label9.Location = new System.Drawing.Point(863, 95);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(27, 19);
            this.label9.TabIndex = 539;
            this.label9.Text = "Au";
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.BorderStyle = Infragistics.Win.Misc.GroupBoxBorderStyle.None;
            this.ultraGroupBox3.Controls.Add(this.flowLayoutPanel1);
            this.ultraGroupBox3.Controls.Add(this.label5);
            this.ultraGroupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ultraGroupBox3.Location = new System.Drawing.Point(0, 0);
            this.ultraGroupBox3.Margin = new System.Windows.Forms.Padding(0);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(1838, 41);
            this.ultraGroupBox3.TabIndex = 2;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.Command1);
            this.flowLayoutPanel1.Controls.Add(this.cmdExecute);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1838, 41);
            this.flowLayoutPanel1.TabIndex = 540;
            // 
            // Command1
            // 
            this.Command1.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Command1.ForeColor = System.Drawing.Color.White;
            this.Command1.Image = global::Axe_interDT.Properties.Resources.Excel_32x32;
            this.Command1.Location = new System.Drawing.Point(1776, 2);
            this.Command1.Margin = new System.Windows.Forms.Padding(2);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(60, 35);
            this.Command1.TabIndex = 547;
            this.Command1.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // cmdExecute
            // 
            this.cmdExecute.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdExecute.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExecute.FlatAppearance.BorderSize = 0;
            this.cmdExecute.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExecute.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdExecute.ForeColor = System.Drawing.Color.White;
            this.cmdExecute.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdExecute.Location = new System.Drawing.Point(1626, 2);
            this.cmdExecute.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExecute.Name = "cmdExecute";
            this.cmdExecute.Size = new System.Drawing.Size(146, 35);
            this.cmdExecute.TabIndex = 392;
            this.cmdExecute.Text = "Contrôle des heures";
            this.cmdExecute.UseVisualStyleBackColor = false;
            this.cmdExecute.Click += new System.EventHandler(this.cmdExecute_Click);
            // 
            // label5
            // 
            this.label5.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label5.Location = new System.Drawing.Point(6, 0);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(105, 38);
            this.label5.TabIndex = 539;
            this.label5.Click += new System.EventHandler(this.label5_Click);
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.CaptionAlignment = Infragistics.Win.Misc.GroupBoxCaptionAlignment.Center;
            this.ultraGroupBox2.Controls.Add(this.tableLayoutPanel3);
            this.ultraGroupBox2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ultraGroupBox2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ultraGroupBox2.Location = new System.Drawing.Point(3, 213);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(1844, 197);
            this.ultraGroupBox2.TabIndex = 1;
            this.ultraGroupBox2.Text = "CONTROLE DES ANOMALIES";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 1;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel4, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.flowLayoutPanel2, 0, 0);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 21);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 42F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1838, 173);
            this.tableLayoutPanel3.TabIndex = 0;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 7;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 107F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 148F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 69F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33334F));
            this.tableLayoutPanel4.Controls.Add(this.cmdFndMatErr, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.label2, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.lblLibIntervenanterr, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.label3, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.optTous, 4, 1);
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.label15, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.txtNoIntervention, 1, 3);
            this.tableLayoutPanel4.Controls.Add(this.cmbIntervErr, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.DTPicker2, 4, 0);
            this.tableLayoutPanel4.Controls.Add(this.cmdService2, 6, 0);
            this.tableLayoutPanel4.Controls.Add(this.OptControle, 4, 2);
            this.tableLayoutPanel4.Controls.Add(this.OptNonControle, 4, 3);
            this.tableLayoutPanel4.Controls.Add(this.label7, 5, 0);
            this.tableLayoutPanel4.Controls.Add(this.label8, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.DTPicker1, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.lblTotal, 6, 3);
            this.tableLayoutPanel4.Controls.Add(this.cmdRechercheImmeuble, 2, 2);
            this.tableLayoutPanel4.Controls.Add(this.cmbCodeimmeuble, 1, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 45);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 4;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1832, 125);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // cmdFndMatErr
            // 
            this.cmdFndMatErr.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFndMatErr.FlatAppearance.BorderSize = 0;
            this.cmdFndMatErr.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFndMatErr.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdFndMatErr.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdFndMatErr.Location = new System.Drawing.Point(602, 33);
            this.cmdFndMatErr.Name = "cmdFndMatErr";
            this.cmdFndMatErr.Size = new System.Drawing.Size(24, 19);
            this.cmdFndMatErr.TabIndex = 543;
            this.toolTip1.SetToolTip(this.cmdFndMatErr, "Recherche de l\'intervenant");
            this.cmdFndMatErr.UseVisualStyleBackColor = false;
            this.cmdFndMatErr.Click += new System.EventHandler(this.cmdFndMatErr_Click);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(3, 0);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(101, 30);
            this.label2.TabIndex = 539;
            this.label2.Text = "Date réalisée du";
            // 
            // lblLibIntervenanterr
            // 
            this.lblLibIntervenanterr.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.lblLibIntervenanterr.AutoSize = true;
            this.lblLibIntervenanterr.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLibIntervenanterr.Location = new System.Drawing.Point(634, 35);
            this.lblLibIntervenanterr.Name = "lblLibIntervenanterr";
            this.lblLibIntervenanterr.Size = new System.Drawing.Size(0, 19);
            this.lblLibIntervenanterr.TabIndex = 539;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(3, 30);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(97, 19);
            this.label3.TabIndex = 539;
            this.label3.Text = "Intervenants";
            // 
            // optTous
            // 
            this.optTous.AutoSize = true;
            this.optTous.Checked = true;
            this.optTous.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optTous.Location = new System.Drawing.Point(782, 33);
            this.optTous.Name = "optTous";
            this.optTous.Size = new System.Drawing.Size(60, 23);
            this.optTous.TabIndex = 538;
            this.optTous.TabStop = true;
            this.optTous.Text = "Tous";
            this.optTous.UseVisualStyleBackColor = true;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label4.Location = new System.Drawing.Point(3, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(78, 30);
            this.label4.TabIndex = 539;
            this.label4.Text = "Sélection immeuble";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label15.Location = new System.Drawing.Point(3, 90);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(94, 35);
            this.label15.TabIndex = 539;
            this.label15.Text = "No intervention";
            // 
            // txtNoIntervention
            // 
            this.txtNoIntervention.AccAcceptNumbersOnly = false;
            this.txtNoIntervention.AccAllowComma = false;
            this.txtNoIntervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoIntervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoIntervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoIntervention.AccHidenValue = "";
            this.txtNoIntervention.AccNotAllowedChars = null;
            this.txtNoIntervention.AccReadOnly = false;
            this.txtNoIntervention.AccReadOnlyAllowDelete = false;
            this.txtNoIntervention.AccRequired = false;
            this.txtNoIntervention.BackColor = System.Drawing.Color.White;
            this.txtNoIntervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNoIntervention.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtNoIntervention.ForeColor = System.Drawing.Color.Black;
            this.txtNoIntervention.Location = new System.Drawing.Point(109, 92);
            this.txtNoIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoIntervention.MaxLength = 32767;
            this.txtNoIntervention.Multiline = false;
            this.txtNoIntervention.Name = "txtNoIntervention";
            this.txtNoIntervention.ReadOnly = false;
            this.txtNoIntervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoIntervention.Size = new System.Drawing.Size(488, 27);
            this.txtNoIntervention.TabIndex = 655;
            this.txtNoIntervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoIntervention.UseSystemPasswordChar = false;
            // 
            // cmbIntervErr
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbIntervErr.DisplayLayout.Appearance = appearance25;
            this.cmbIntervErr.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbIntervErr.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbIntervErr.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbIntervErr.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.cmbIntervErr.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbIntervErr.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.cmbIntervErr.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbIntervErr.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbIntervErr.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbIntervErr.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.cmbIntervErr.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbIntervErr.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.cmbIntervErr.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbIntervErr.DisplayLayout.Override.CellAppearance = appearance32;
            this.cmbIntervErr.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbIntervErr.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbIntervErr.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.cmbIntervErr.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.cmbIntervErr.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbIntervErr.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.cmbIntervErr.DisplayLayout.Override.RowAppearance = appearance35;
            this.cmbIntervErr.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbIntervErr.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.cmbIntervErr.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbIntervErr.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbIntervErr.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbIntervErr.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbIntervErr.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbIntervErr.Location = new System.Drawing.Point(110, 33);
            this.cmbIntervErr.Name = "cmbIntervErr";
            this.cmbIntervErr.Size = new System.Drawing.Size(486, 27);
            this.cmbIntervErr.TabIndex = 653;
            this.cmbIntervErr.AfterCloseUp += new System.EventHandler(this.cmbIntervErr_AfterCloseUp);
            this.cmbIntervErr.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbIntervErr_BeforeDropDown);
            this.cmbIntervErr.Click += new System.EventHandler(this.cmbIntervErr_Click);
            this.cmbIntervErr.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbIntervErr_KeyPress);
            // 
            // DTPicker2
            // 
            this.DTPicker2.ButtonsLeft.Add(stateEditorButton3);
            this.DTPicker2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DTPicker2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.DTPicker2.Location = new System.Drawing.Point(782, 3);
            this.DTPicker2.Name = "DTPicker2";
            this.DTPicker2.Size = new System.Drawing.Size(485, 26);
            this.DTPicker2.TabIndex = 651;
            this.DTPicker2.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTPicker1_BeforeDropDown);
            // 
            // cmdService2
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmdService2.DisplayLayout.Appearance = appearance37;
            this.cmdService2.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmdService2.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.cmdService2.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmdService2.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.cmdService2.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmdService2.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.cmdService2.DisplayLayout.MaxColScrollRegions = 1;
            this.cmdService2.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmdService2.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmdService2.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.cmdService2.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmdService2.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.cmdService2.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmdService2.DisplayLayout.Override.CellAppearance = appearance44;
            this.cmdService2.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmdService2.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.cmdService2.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.cmdService2.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.cmdService2.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmdService2.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.cmdService2.DisplayLayout.Override.RowAppearance = appearance47;
            this.cmdService2.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmdService2.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.cmdService2.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmdService2.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmdService2.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmdService2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdService2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdService2.Location = new System.Drawing.Point(1342, 3);
            this.cmdService2.Name = "cmdService2";
            this.cmdService2.Size = new System.Drawing.Size(487, 27);
            this.cmdService2.TabIndex = 652;
            this.cmdService2.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmdService2_BeforeDropDown);
            // 
            // OptControle
            // 
            this.OptControle.AutoSize = true;
            this.OptControle.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.OptControle.Location = new System.Drawing.Point(782, 63);
            this.OptControle.Name = "OptControle";
            this.OptControle.Size = new System.Drawing.Size(275, 23);
            this.OptControle.TabIndex = 700;
            this.OptControle.Text = "Uniquement les éléments contrôlés";
            this.OptControle.UseVisualStyleBackColor = true;
            // 
            // OptNonControle
            // 
            this.OptNonControle.AutoSize = true;
            this.OptNonControle.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.OptNonControle.Location = new System.Drawing.Point(782, 93);
            this.OptNonControle.Name = "OptNonControle";
            this.OptNonControle.Size = new System.Drawing.Size(305, 23);
            this.OptNonControle.TabIndex = 701;
            this.OptNonControle.Text = "Uniquement les éléments non contrôlés";
            this.OptNonControle.UseVisualStyleBackColor = true;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label7.Location = new System.Drawing.Point(1273, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(59, 19);
            this.label7.TabIndex = 539;
            this.label7.Text = "Service";
            // 
            // label8
            // 
            this.label8.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.label8.AutoSize = true;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label8.Location = new System.Drawing.Point(602, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(18, 30);
            this.label8.TabIndex = 539;
            this.label8.Text = "Au";
            // 
            // DTPicker1
            // 
            this.DTPicker1.ButtonsLeft.Add(stateEditorButton4);
            this.DTPicker1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DTPicker1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.DTPicker1.Location = new System.Drawing.Point(110, 3);
            this.DTPicker1.Name = "DTPicker1";
            this.DTPicker1.Size = new System.Drawing.Size(486, 26);
            this.DTPicker1.TabIndex = 650;
            this.DTPicker1.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.DTPicker1_BeforeDropDown);
            // 
            // lblTotal
            // 
            this.lblTotal.AutoSize = true;
            this.lblTotal.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblTotal.Location = new System.Drawing.Point(1342, 90);
            this.lblTotal.Name = "lblTotal";
            this.lblTotal.Size = new System.Drawing.Size(51, 19);
            this.lblTotal.TabIndex = 539;
            this.lblTotal.Text = "Total :";
            // 
            // cmdRechercheImmeuble
            // 
            this.cmdRechercheImmeuble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheImmeuble.FlatAppearance.BorderSize = 0;
            this.cmdRechercheImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdRechercheImmeuble.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheImmeuble.Location = new System.Drawing.Point(602, 63);
            this.cmdRechercheImmeuble.Name = "cmdRechercheImmeuble";
            this.cmdRechercheImmeuble.Size = new System.Drawing.Size(24, 19);
            this.cmdRechercheImmeuble.TabIndex = 546;
            this.toolTip1.SetToolTip(this.cmdRechercheImmeuble, "Recherche de l\'immeuble");
            this.cmdRechercheImmeuble.UseVisualStyleBackColor = false;
            this.cmdRechercheImmeuble.Click += new System.EventHandler(this.cmdRechercheImmeuble_Click);
            // 
            // cmbCodeimmeuble
            // 
            appearance49.BackColor = System.Drawing.SystemColors.Window;
            appearance49.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbCodeimmeuble.DisplayLayout.Appearance = appearance49;
            this.cmbCodeimmeuble.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbCodeimmeuble.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance50.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance50.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance50.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance50.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCodeimmeuble.DisplayLayout.GroupByBox.Appearance = appearance50;
            appearance51.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCodeimmeuble.DisplayLayout.GroupByBox.BandLabelAppearance = appearance51;
            this.cmbCodeimmeuble.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance52.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance52.BackColor2 = System.Drawing.SystemColors.Control;
            appearance52.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance52.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCodeimmeuble.DisplayLayout.GroupByBox.PromptAppearance = appearance52;
            this.cmbCodeimmeuble.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbCodeimmeuble.DisplayLayout.MaxRowScrollRegions = 1;
            appearance53.BackColor = System.Drawing.SystemColors.Window;
            appearance53.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbCodeimmeuble.DisplayLayout.Override.ActiveCellAppearance = appearance53;
            appearance54.BackColor = System.Drawing.SystemColors.Highlight;
            appearance54.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbCodeimmeuble.DisplayLayout.Override.ActiveRowAppearance = appearance54;
            this.cmbCodeimmeuble.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbCodeimmeuble.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance55.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCodeimmeuble.DisplayLayout.Override.CardAreaAppearance = appearance55;
            appearance56.BorderColor = System.Drawing.Color.Silver;
            appearance56.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbCodeimmeuble.DisplayLayout.Override.CellAppearance = appearance56;
            this.cmbCodeimmeuble.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbCodeimmeuble.DisplayLayout.Override.CellPadding = 0;
            appearance57.BackColor = System.Drawing.SystemColors.Control;
            appearance57.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance57.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance57.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance57.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCodeimmeuble.DisplayLayout.Override.GroupByRowAppearance = appearance57;
            appearance58.TextHAlignAsString = "Left";
            this.cmbCodeimmeuble.DisplayLayout.Override.HeaderAppearance = appearance58;
            this.cmbCodeimmeuble.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbCodeimmeuble.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance59.BackColor = System.Drawing.SystemColors.Window;
            appearance59.BorderColor = System.Drawing.Color.Silver;
            this.cmbCodeimmeuble.DisplayLayout.Override.RowAppearance = appearance59;
            this.cmbCodeimmeuble.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance60.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbCodeimmeuble.DisplayLayout.Override.TemplateAddRowAppearance = appearance60;
            this.cmbCodeimmeuble.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbCodeimmeuble.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbCodeimmeuble.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbCodeimmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCodeimmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbCodeimmeuble.Location = new System.Drawing.Point(110, 63);
            this.cmbCodeimmeuble.Name = "cmbCodeimmeuble";
            this.cmbCodeimmeuble.Size = new System.Drawing.Size(486, 27);
            this.cmbCodeimmeuble.TabIndex = 654;
            this.cmbCodeimmeuble.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbCodeimmeuble_BeforeDropDown);
            this.cmbCodeimmeuble.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbCodeimmeuble_KeyPress);
            // 
            // flowLayoutPanel2
            // 
            this.flowLayoutPanel2.Controls.Add(this.cmdExportExcel);
            this.flowLayoutPanel2.Controls.Add(this.cmbIntervention);
            this.flowLayoutPanel2.Controls.Add(this.CmdSauver);
            this.flowLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel2.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.flowLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel2.Name = "flowLayoutPanel2";
            this.flowLayoutPanel2.Size = new System.Drawing.Size(1838, 42);
            this.flowLayoutPanel2.TabIndex = 1;
            // 
            // cmdExportExcel
            // 
            this.cmdExportExcel.Anchor = System.Windows.Forms.AnchorStyles.Right;
            this.cmdExportExcel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdExportExcel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExportExcel.FlatAppearance.BorderSize = 0;
            this.cmdExportExcel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExportExcel.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdExportExcel.ForeColor = System.Drawing.Color.White;
            this.cmdExportExcel.Image = global::Axe_interDT.Properties.Resources.Excel_32x32;
            this.cmdExportExcel.Location = new System.Drawing.Point(1776, 2);
            this.cmdExportExcel.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExportExcel.Name = "cmdExportExcel";
            this.cmdExportExcel.Size = new System.Drawing.Size(60, 35);
            this.cmdExportExcel.TabIndex = 545;
            this.cmdExportExcel.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdExportExcel.UseVisualStyleBackColor = false;
            this.cmdExportExcel.Click += new System.EventHandler(this.cmdExportExcel_Click);
            // 
            // cmbIntervention
            // 
            this.cmbIntervention.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmbIntervention.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbIntervention.FlatAppearance.BorderSize = 0;
            this.cmbIntervention.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmbIntervention.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmbIntervention.Location = new System.Drawing.Point(1712, 2);
            this.cmbIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.cmbIntervention.Name = "cmbIntervention";
            this.cmbIntervention.Size = new System.Drawing.Size(60, 36);
            this.cmbIntervention.TabIndex = 377;
            this.cmbIntervention.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmbIntervention, "Recherche");
            this.cmbIntervention.UseVisualStyleBackColor = false;
            this.cmbIntervention.Click += new System.EventHandler(this.cmbIntervention_Click);
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(1648, 2);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 36);
            this.CmdSauver.TabIndex = 378;
            this.toolTip1.SetToolTip(this.CmdSauver, "Enregistrer");
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // GridHMI
            // 
            appearance61.BackColor = System.Drawing.SystemColors.Window;
            appearance61.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridHMI.DisplayLayout.Appearance = appearance61;
            this.GridHMI.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridHMI.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridHMI.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance62.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance62.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance62.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance62.BorderColor = System.Drawing.SystemColors.Window;
            this.GridHMI.DisplayLayout.GroupByBox.Appearance = appearance62;
            appearance63.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridHMI.DisplayLayout.GroupByBox.BandLabelAppearance = appearance63;
            this.GridHMI.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance64.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance64.BackColor2 = System.Drawing.SystemColors.Control;
            appearance64.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance64.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridHMI.DisplayLayout.GroupByBox.PromptAppearance = appearance64;
            this.GridHMI.DisplayLayout.MaxColScrollRegions = 1;
            this.GridHMI.DisplayLayout.MaxRowScrollRegions = 1;
            appearance65.BackColor = System.Drawing.SystemColors.Window;
            appearance65.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridHMI.DisplayLayout.Override.ActiveCellAppearance = appearance65;
            appearance66.BackColor = System.Drawing.SystemColors.Highlight;
            appearance66.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridHMI.DisplayLayout.Override.ActiveRowAppearance = appearance66;
            this.GridHMI.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridHMI.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridHMI.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridHMI.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance67.BackColor = System.Drawing.SystemColors.Window;
            this.GridHMI.DisplayLayout.Override.CardAreaAppearance = appearance67;
            appearance68.BorderColor = System.Drawing.Color.Silver;
            appearance68.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridHMI.DisplayLayout.Override.CellAppearance = appearance68;
            this.GridHMI.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridHMI.DisplayLayout.Override.CellPadding = 0;
            appearance69.BackColor = System.Drawing.SystemColors.Control;
            appearance69.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance69.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance69.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance69.BorderColor = System.Drawing.SystemColors.Window;
            this.GridHMI.DisplayLayout.Override.GroupByRowAppearance = appearance69;
            appearance70.TextHAlignAsString = "Left";
            this.GridHMI.DisplayLayout.Override.HeaderAppearance = appearance70;
            this.GridHMI.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridHMI.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance71.BackColor = System.Drawing.SystemColors.Window;
            appearance71.BorderColor = System.Drawing.Color.Silver;
            this.GridHMI.DisplayLayout.Override.RowAppearance = appearance71;
            this.GridHMI.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridHMI.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance72.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridHMI.DisplayLayout.Override.TemplateAddRowAppearance = appearance72;
            this.GridHMI.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridHMI.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridHMI.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridHMI.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridHMI.Location = new System.Drawing.Point(3, 416);
            this.GridHMI.Name = "GridHMI";
            this.GridHMI.Size = new System.Drawing.Size(1844, 538);
            this.GridHMI.TabIndex = 412;
            this.GridHMI.Text = "ultraGrid1";
            this.GridHMI.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridHMI_InitializeLayout);
            this.GridHMI.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.GridHMI_InitializeRow);
            this.GridHMI.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.GridHMI_AfterRowUpdate);
            this.GridHMI.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.GridHMI_BeforeRowUpdate);
            this.GridHMI.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.GridHMI_BeforeExitEditMode);
            this.GridHMI.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.GridHMI_DoubleClickRow);
            // 
            // UserDocCtrlHeurePers
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserDocCtrlHeurePers";
            this.Size = new System.Drawing.Size(1850, 957);
            this.VisibleChanged += new System.EventHandler(this.UserDocCtrlHeurePers_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmdService)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtDeb)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIntervenant)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.dtFin)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIntervErr)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTPicker2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmdService2)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DTPicker1)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCodeimmeuble)).EndInit();
            this.flowLayoutPanel2.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.GridHMI)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label15;
        public iTalk.iTalk_TextBox_Small2 txtNoIntervention;
        public System.Windows.Forms.RadioButton optTous;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbIntervErr;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor DTPicker2;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmdService2;
        public System.Windows.Forms.RadioButton OptControle;
        public System.Windows.Forms.RadioButton OptNonControle;
        public System.Windows.Forms.Label lblTotal;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label8;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridHMI;
        public System.Windows.Forms.Button cmbIntervention;
        public System.Windows.Forms.Button CmdSauver;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor DTPicker1;
        public System.Windows.Forms.Button cmdFndMatErr;
        public System.Windows.Forms.Button cmdRechercheImmeuble;
        public System.Windows.Forms.Label lblLibIntervenanterr;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmdService;
        public System.Windows.Forms.RadioButton optService;
        public System.Windows.Forms.Button cmdRechercheIntervenant;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.RadioButton OptParInterv;
        public System.Windows.Forms.RadioButton Option2;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtDeb;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor dtFin;
        public iTalk.iTalk_TextBox_Small2 txtNoInterventionBis;
        public System.Windows.Forms.Label lblNoIntervention;
        public System.Windows.Forms.Label label9;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbIntervenant;
        public System.Windows.Forms.Label lblLibIntervenant;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbCodeimmeuble;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        public System.Windows.Forms.Label label5;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button Command1;
        public System.Windows.Forms.Button cmdExecute;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel2;
        public System.Windows.Forms.Button cmdExportExcel;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
