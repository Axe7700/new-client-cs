﻿
using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.CompteRendu
{
    public partial class frmPrimaire : Form
    {
        public frmPrimaire()
        {
            InitializeComponent();
        }
       
        ModAdo ModAdo1 = new ModAdo();
        DataTable DatPrimaryRS=new DataTable();
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmPrimaire_Load(object sender, EventArgs e)
        {
           
            cmdAjouter.Enabled = false;
            cmdAjouter.Visible = false;
            cmdMAJ.Enabled = false;

            var Data1 = new DataTable();

            sheridan.InitialiseCombo(Combo1, "SELECT * FROM MarqueMateriel WHERE CodeUtilisation=401", "marque", true);
            sheridan.InitialiseCombo(Combo3, "SELECT * FROM MarqueMateriel WHERE CodeUtilisation=402", "marque", true);
            sheridan.InitialiseCombo(Combo4, "SELECT * FROM MarqueMateriel WHERE CodeUtilisation=403", "marque", true);
            sheridan.InitialiseCombo(Combo2, "SELECT * FROM MarqueMateriel WHERE CodeUtilisation=404", "marque", true);
            sheridan.InitialiseCombo(Combo8, "SELECT * FROM MarqueMateriel WHERE CodeUtilisation=404", "marque", true);
            sheridan.InitialiseCombo(Combo7, "SELECT * FROM MarqueMateriel WHERE CodeUtilisation=405", "marque", true);
            sheridan.InitialiseCombo(Combo6, "SELECT * FROM MarqueMateriel WHERE CodeUtilisation=406", "marque", true);
            sheridan.InitialiseCombo(Combo5, "SELECT * FROM MarqueMateriel WHERE CodeUtilisation=407", "marque", true);
            sheridan.InitialiseCombo(Combo12, "SELECT * FROM MarqueMateriel WHERE CodeUtilisation=407", "marque", true);
            sheridan.InitialiseCombo(Combo10, "SELECT * FROM MarqueMateriel WHERE CodeUtilisation=109", "marque", true);
            sheridan.InitialiseCombo(Combo9, "SELECT * FROM MarqueMateriel WHERE CodeUtilisation=113", "marque", true);
            sheridan.InitialiseCombo(Combo11, "SELECT * FROM MarqueMateriel WHERE CodeUtilisation=114", "marque", true);

            this.Text = this.Text + " pour l'immeuble " + General.gVar;

            General.SQL = "SELECT * FROM MaterielPrimaire " + "WHERE CodeImmeuble='" + General.gVar.ToString() + "'";          
            DatPrimaryRS = ModAdo1.fc_OpenRecordSet(General.SQL);

            if (DatPrimaryRS.Rows.Count == 0)
            {
                cmdAjouter.Enabled = true;
                cmdAjouter_Click(cmdAjouter, new System.EventArgs());
            }
            else
            {
                cmdMAJ.Enabled = true;
                Combo5.Text = DatPrimaryRS.Rows[0]["PompeCHMarque"].ToString();
                txtFields38.Text=DatPrimaryRS.Rows[0]["PompeCHType"].ToString();
                txtFields1.Text = DatPrimaryRS.Rows[0]["PompeCHNbre"].ToString();
                Check8.CheckState =( DatPrimaryRS.Rows[0]["PompeCHVanne"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked; 
                Combo6.Text = DatPrimaryRS.Rows[0]["PompeCondMarque"].ToString();
                txtFields0.Text = DatPrimaryRS.Rows[0]["PompeCondType"].ToString();
                txtFields2.Text = DatPrimaryRS.Rows[0]["PompeCondNbre"].ToString();
                Combo7.Value = DatPrimaryRS.Rows[0]["PompePUMarque"].ToString();
                txtFields54.Text = DatPrimaryRS.Rows[0]["PompePUType"].ToString();
                txtFields59.Text = DatPrimaryRS.Rows[0]["PompePUHauteur"].ToString();
                txtFields55.Text = DatPrimaryRS.Rows[0]["PompePUNbre"].ToString();
                Combo8.Text = DatPrimaryRS.Rows[0]["RegulationMarque"].ToString();
                txtFields11.Text = DatPrimaryRS.Rows[0]["RegulationNbre"].ToString();
                Check19.CheckState = ( DatPrimaryRS.Rows[0]["RegulationActionBruleur"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Check210.CheckState = ( DatPrimaryRS.Rows[0]["RegulationActionVanne3"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Check211.CheckState = ( DatPrimaryRS.Rows[0]["RegulationActionVanne4"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                txtFields64.Text = DatPrimaryRS.Rows[0]["RegulationType"].ToString();
                txtFields61.Text = DatPrimaryRS.Rows[0]["VanneMarque"].ToString();
                txtFields60.Text = DatPrimaryRS.Rows[0]["VanneType"].ToString();
                txtFields4.Text = DatPrimaryRS.Rows[0]["VanneNbre"].ToString();
                txtFields13.Text= Convert.ToInt32(General.nz(DatPrimaryRS.Rows[0]["RegulationASecteur"],0)).ToString();
                Check22.CheckState = (DatPrimaryRS.Rows[0]["CptEau"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                txtFields68.Text = DatPrimaryRS.Rows[0]["CptEauDiam"].ToString();
                Check23.CheckState = (DatPrimaryRS.Rows[0]["AlimEauCoupure"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                txtFields66.Text = DatPrimaryRS.Rows[0]["AlimEauDiam"].ToString();
                Check24.CheckState = ( DatPrimaryRS.Rows[0]["Disconnecteur"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Combo9.Text = DatPrimaryRS.Rows[0]["DisconnecteurMarque"].ToString();
                txtFields15.Text = DatPrimaryRS.Rows[0]["DisconnecteurType"].ToString();
                txtFields17.Text = DatPrimaryRS.Rows[0]["DisconnecteurNumero"].ToString();
                txtFields18.Text = DatPrimaryRS.Rows[0]["DisconnecteurDiametre"].ToString();
                txtFields87.Text = DatPrimaryRS.Rows[0]["ExpansionType"].ToString();
                Combo10.Text = DatPrimaryRS.Rows[0]["ExpansionMarque"].ToString();
                txtFields70.Text = DatPrimaryRS.Rows[0]["ExpansionCapacite"].ToString();
                Combo11.Text = DatPrimaryRS.Rows[0]["MaintienMarque"].ToString();
                txtFields14.Text = DatPrimaryRS.Rows[0]["MaintienType"].ToString();
                txtFields89.Text = DatPrimaryRS.Rows[0]["MaintienPompeMarque"].ToString();
                txtFields88.Text = DatPrimaryRS.Rows[0]["MaintienPompeNbre"].ToString();
                Check25.CheckState = ( DatPrimaryRS.Rows[0]["PotBoue"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Check260.CheckState = (DatPrimaryRS.Rows[0]["PotIntroduction"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Check261.CheckState = ( DatPrimaryRS.Rows[0]["SiphonSol"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Text1.Text = DatPrimaryRS.Rows[0]["Observation"].ToString();
                Combo12.Text = DatPrimaryRS.Rows[0]["PompeRECMarque"].ToString();
                txtFields12.Text = DatPrimaryRS.Rows[0]["PompeRECType"].ToString();
                Text4.Text = DatPrimaryRS.Rows[0]["PompeRECNbre"].ToString();
                Check2.CheckState = ( DatPrimaryRS.Rows[0]["PompeRECIsole"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Combo1.Text = DatPrimaryRS.Rows[0]["EchangeurMarque"].ToString();
                txtFields28.Text = DatPrimaryRS.Rows[0]["EchangeurType"].ToString();
                txtFields48.Text = DatPrimaryRS.Rows[0]["EchangeurPuissance"].ToString();
                Text5.Text = DatPrimaryRS.Rows[0]["EchangeurNbre"].ToString();
                txtFields16.Text = DatPrimaryRS.Rows[0]["EchangeurFluide"].ToString();
                Check1.CheckState = ( DatPrimaryRS.Rows[0]["EchangeurVanne"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Check4.CheckState = ( DatPrimaryRS.Rows[0]["EchangeurAsser"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Combo2.Text = DatPrimaryRS.Rows[0]["RegulateurMarque"].ToString();
                txtFields5.Text = DatPrimaryRS.Rows[0]["RegulateurType"].ToString();
                txtFields10.Text = DatPrimaryRS.Rows[0]["RegulateurNbre"].ToString();
                txtFields7.Text = DatPrimaryRS.Rows[0]["RegulateurAss"].ToString();
                Check3.CheckState = (DatPrimaryRS.Rows[0]["RegulteurTherm"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Check5.CheckState = (DatPrimaryRS.Rows[0]["RegulateurAsser"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Combo3.Text = DatPrimaryRS.Rows[0]["VanneDetenteMarque"].ToString();
                txtFields3.Text = DatPrimaryRS.Rows[0]["VanneDetenteType"].ToString();
                Text2.Text = DatPrimaryRS.Rows[0]["VanneDetenteNbre"].ToString();
                Check6.CheckState = (DatPrimaryRS.Rows[0]["VanneDetenteAsser"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                txtFields6.Text = DatPrimaryRS.Rows[0]["VanneDetenteAss"].ToString();
                Combo4.Text = DatPrimaryRS.Rows[0]["VanneCondMarque"].ToString();
                txtFields9.Text = DatPrimaryRS.Rows[0]["VanneCondType"].ToString();
                Text3.Text = DatPrimaryRS.Rows[0]["VanneCondNbre"].ToString();
                txtFields8.Text = DatPrimaryRS.Rows[0]["VanneCondAss"].ToString();
                Check7.CheckState = (DatPrimaryRS.Rows[0]["VanneCondAsser"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
            }
           
        }

       
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        public void cmdAjouter_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            DatPrimaryRS.Rows.Add(DatPrimaryRS.NewRow());
            //txtFields(86).Text = gVar
            cmdAjouter.Enabled = false;
            cmdMAJ.Enabled = true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdMAJ_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            int existImmeuble;
            /// initialisation des boutons d'options en création
            if (string.IsNullOrEmpty(txtFields16.Text))
            {
                txtFields16.Text = "2";
            }
            if (string.IsNullOrEmpty(txtFields13.Text))
            {
                txtFields13.Text = "1";
            }
            if (string.IsNullOrEmpty(txtFields7.Text))
            {
                txtFields7.Text = "3";
            }
            if (string.IsNullOrEmpty(txtFields6.Text))
            {
                txtFields6.Text = "3";
            }
            if (string.IsNullOrEmpty(txtFields8.Text))
            {
                txtFields8.Text = "3";
            }
            if (string.IsNullOrEmpty(txtFields87.Text))
            {
                txtFields87.Text = "2";
            }
            /// initialisation des quantités en création
            if (string.IsNullOrEmpty(txtFields1.Text))
            {
                txtFields1.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields2.Text))
            {
                txtFields2.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields4.Text))
            {
                txtFields4.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields10.Text))
            {
                txtFields10.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields11.Text))
            {
                txtFields11.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields55.Text))
            {
                txtFields55.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields88.Text))
            {
                txtFields88.Text = "0";
            }


            txtFields86.Text = General.gVar.ToString();
            using (var tmp = new ModAdo())
                existImmeuble = Convert.ToInt32(tmp.fc_ADOlibelle("select  count( CodeImmeuble) from MaterielPrimaire   WHERE CodeImmeuble='" + General.gVar + "'"));

            if (existImmeuble == 0)
            {
                DatPrimaryRS.Rows[0]["CodeImmeuble"] = txtFields86.Text;
                DatPrimaryRS.Rows[0]["PompeCHMarque"] = Combo5.Text;
                DatPrimaryRS.Rows[0]["PompeCHType"] = txtFields38.Text;
                DatPrimaryRS.Rows[0]["PompeCHNbre"] = Convert.ToInt32(txtFields1.Text);
                DatPrimaryRS.Rows[0]["PompeCHVanne"] = (Check8.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["PompeCondMarque"] = Combo6.Text;
                DatPrimaryRS.Rows[0]["PompeCondType"] = txtFields0.Text;
                DatPrimaryRS.Rows[0]["PompeCondNbre"] = txtFields2.Text;
                DatPrimaryRS.Rows[0]["PompePUMarque"] = Combo7.Value.ToString();
                DatPrimaryRS.Rows[0]["PompePUType"] = txtFields54.Text;
                DatPrimaryRS.Rows[0]["PompePUHauteur"] = Convert.ToDouble(General.nz(txtFields59.Text,0));
                DatPrimaryRS.Rows[0]["PompePUNbre"] = Convert.ToInt32(General.nz(txtFields55.Text,0));
                DatPrimaryRS.Rows[0]["RegulationMarque"] = Combo8.Text;
                DatPrimaryRS.Rows[0]["RegulationNbre"] = Convert.ToInt32(General.nz(txtFields11.Text,0));
                DatPrimaryRS.Rows[0]["RegulationActionBruleur"] = (Check19.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["RegulationActionVanne3"] = (Check210.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["RegulationActionVanne4"] = (Check211.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["RegulationType"] = txtFields64.Text;
                DatPrimaryRS.Rows[0]["VanneMarque"] = txtFields61.Text;
                DatPrimaryRS.Rows[0]["VanneType"] = txtFields60.Text;
                DatPrimaryRS.Rows[0]["VanneNbre"] = Convert.ToInt32(General.nz(txtFields4.Text,0));
                DatPrimaryRS.Rows[0]["RegulationASecteur"] = Convert.ToBoolean(Convert.ToInt32(txtFields13.Text));
                DatPrimaryRS.Rows[0]["CptEau"] = (Check22.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["CptEauDiam"] = txtFields68.Text;
                DatPrimaryRS.Rows[0]["AlimEauCoupure"] = (Check23.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["AlimEauDiam"] = txtFields66.Text;
                DatPrimaryRS.Rows[0]["Disconnecteur"] = (Check24.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["DisconnecteurMarque"] = Combo9.Text;
                DatPrimaryRS.Rows[0]["DisconnecteurType"] = txtFields15.Text;
                DatPrimaryRS.Rows[0]["DisconnecteurNumero"] = txtFields17.Text;
                DatPrimaryRS.Rows[0]["DisconnecteurDiametre"] = txtFields18.Text;
                DatPrimaryRS.Rows[0]["ExpansionType"] = Convert.ToInt32(General.nz(txtFields87.Text,0));
                DatPrimaryRS.Rows[0]["ExpansionMarque"] = Combo10.Text;
                DatPrimaryRS.Rows[0]["ExpansionCapacite"] = txtFields70.Text;
                DatPrimaryRS.Rows[0]["MaintienMarque"] = Combo11.Text; ;
                DatPrimaryRS.Rows[0]["MaintienType"] = txtFields14.Text;
                DatPrimaryRS.Rows[0]["MaintienPompeMarque"] = txtFields89.Text;
                DatPrimaryRS.Rows[0]["MaintienPompeNbre"] = Convert.ToInt32(General.nz(txtFields88.Text,0));
                DatPrimaryRS.Rows[0]["PotBoue"] = (Check25.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["PotIntroduction"] = (Check260.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["SiphonSol"] = (Check261.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["Observation"] = Text1.Text;
                DatPrimaryRS.Rows[0]["PompeRECMarque"] = Combo12.Text;
                DatPrimaryRS.Rows[0]["PompeRECType"] = txtFields12.Text;
                DatPrimaryRS.Rows[0]["PompeRECNbre"] = Convert.ToInt32(General.nz(Text4.Text,0));
                DatPrimaryRS.Rows[0]["PompeRECIsole"] = (Check2.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["EchangeurMarque"] = Combo1.Text;
                DatPrimaryRS.Rows[0]["EchangeurType"] = txtFields28.Text;
                DatPrimaryRS.Rows[0]["EchangeurPuissance"] = txtFields48.Text; ;
                DatPrimaryRS.Rows[0]["EchangeurNbre"] = Convert.ToInt32(General.nz(Text5.Text,0));
                DatPrimaryRS.Rows[0]["EchangeurFluide"] = Convert.ToInt32(General.nz(txtFields16.Text,0));
                DatPrimaryRS.Rows[0]["EchangeurVanne"] = (Check1.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["EchangeurAsser"] = (Check4.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["RegulateurMarque"] = Combo2.Text;
                DatPrimaryRS.Rows[0]["RegulateurType"] = txtFields5.Text;
                DatPrimaryRS.Rows[0]["RegulateurNbre"] = Convert.ToInt32(General.nz(txtFields10.Text,0));
                DatPrimaryRS.Rows[0]["RegulateurAss"] = Convert.ToInt32(General.nz(txtFields7.Text,0));
                DatPrimaryRS.Rows[0]["RegulteurTherm"] = (Check3.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["RegulateurAsser"] = (Check5.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["VanneDetenteMarque"] = Combo3.Text;
                DatPrimaryRS.Rows[0]["VanneDetenteType"] = txtFields3.Text;
                DatPrimaryRS.Rows[0]["VanneDetenteNbre"] = Convert.ToInt32(General.nz(Text2.Text,0));
                DatPrimaryRS.Rows[0]["VanneDetenteAsser"] = (Check6.Checked ? 1 : 0);
                DatPrimaryRS.Rows[0]["VanneDetenteAss"] = Convert.ToInt32(General.nz(txtFields6.Text,0));
                DatPrimaryRS.Rows[0]["VanneCondMarque"] = Combo4.Text;
                DatPrimaryRS.Rows[0]["VanneCondType"] = txtFields9.Text;
                DatPrimaryRS.Rows[0]["VanneCondNbre"] = Convert.ToInt32(General.nz(Text3.Text,0));
                DatPrimaryRS.Rows[0]["VanneCondAss"] = Convert.ToInt32(General.nz(txtFields8.Text,0));
                DatPrimaryRS.Rows[0]["VanneCondAsser"] = (Check7.Checked ? 1 : 0);
                ModAdo1.Update();
            }
            else
            {
                string reqUpdate = "update MaterielPrimaire set " ;
                reqUpdate = reqUpdate + "PompeCHMarque ='" + Combo5.Text + "',";
                reqUpdate = reqUpdate + "PompeCHType ='" + txtFields38.Text + "',";
                reqUpdate = reqUpdate + "PompeCHNbre =" + Convert.ToInt32(General.nz(txtFields1.Text, 0)) + ",";
                reqUpdate = reqUpdate + "PompeCHVanne =" + (Check8.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "PompeCondMarque ='" + Combo6.Text + "',";
                reqUpdate = reqUpdate + "PompeCondType ='" + txtFields0.Text + "',";
                reqUpdate = reqUpdate + "PompeCondNbre ='" + txtFields2.Text + "',";
                reqUpdate = reqUpdate + "PompePUMarque ='" + Combo7.Text + "',";
                reqUpdate = reqUpdate + "PompePUType ='" + txtFields54.Text + "',";
                reqUpdate = reqUpdate + "PompePUHauteur =" + Convert.ToDouble(General.nz(txtFields59.Text, 0)) + ",";
                reqUpdate = reqUpdate + "PompePUNbre =" + Convert.ToInt32(General.nz(txtFields55.Text, 0)) + ",";
                reqUpdate = reqUpdate + "RegulationMarque ='" + Combo8.Text + "',";
                reqUpdate = reqUpdate + "RegulationNbre =" + Convert.ToInt32(General.nz(txtFields11.Text, 0)) + ",";
                reqUpdate = reqUpdate + "RegulationActionBruleur =" + (Check19.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "RegulationActionVanne3 =" + (Check210.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "RegulationActionVanne4 =" + (Check211.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "RegulationType ='" + txtFields64.Text + "',";
                reqUpdate = reqUpdate + "VanneMarque ='" + txtFields61.Text + "',";
                reqUpdate = reqUpdate + "VanneType ='" + txtFields60.Text + "',";
                reqUpdate = reqUpdate + "VanneNbre =" + Convert.ToInt32(General.nz(txtFields4.Text, 0)) + ",";
                reqUpdate = reqUpdate + "RegulationASecteur =" + Convert.ToInt32(General.nz(txtFields13.Text, 0)) + ",";
                reqUpdate = reqUpdate + "CptEau =" + (Check22.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "CptEauDiam ='" + txtFields68.Text + "',";
                reqUpdate = reqUpdate + "AlimEauCoupure =" + (Check23.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "AlimEauDiam ='" + txtFields66.Text + "',";
                reqUpdate = reqUpdate + "Disconnecteur =" + (Check24.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "DisconnecteurMarque ='" + Combo9.Text + "',";
                reqUpdate = reqUpdate + "DisconnecteurType ='" + txtFields15.Text + "',";
                reqUpdate = reqUpdate + "DisconnecteurNumero ='" + txtFields17.Text + "',";
                reqUpdate = reqUpdate + "DisconnecteurDiametre ='" + txtFields18.Text + "',";
                reqUpdate = reqUpdate + "ExpansionType =" + Convert.ToInt32(General.nz(txtFields87.Text, 0)) + ",";
                reqUpdate = reqUpdate + "ExpansionMarque ='" + Combo10.Text + "',";
                reqUpdate = reqUpdate + "ExpansionCapacite ='" + txtFields70.Text + "',";
                reqUpdate = reqUpdate + "MaintienMarque ='" + Combo11.Text + "',";
                reqUpdate = reqUpdate + "MaintienType ='" + txtFields14.Text + "',";
                reqUpdate = reqUpdate + "MaintienPompeMarque ='" + txtFields89.Text + "',";
                reqUpdate = reqUpdate + "MaintienPompeNbre =" + Convert.ToInt32(General.nz(txtFields88.Text, 0)) + ",";
                reqUpdate = reqUpdate + "PotBoue =" + (Check25.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "PotIntroduction =" + (Check260.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "SiphonSol =" + (Check261.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "Observation ='" + Text1.Text + "',";
                reqUpdate = reqUpdate + "PompeRECMarque ='" + Combo12.Text + "',";
                reqUpdate = reqUpdate + "PompeRECType ='" + txtFields12.Text + "',";
                reqUpdate = reqUpdate + "PompeRECNbre =" + Convert.ToInt32(General.nz(Text4.Text, 0)) + ",";
                reqUpdate = reqUpdate + "PompeRECIsole =" + (Check2.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "EchangeurMarque ='" + Combo1.Text + "',";
                reqUpdate = reqUpdate + "EchangeurType ='" + txtFields28.Text + "',";
                reqUpdate = reqUpdate + "EchangeurPuissance ='" + txtFields48.Text + "',";
                reqUpdate = reqUpdate + "EchangeurNbre =" + Convert.ToInt32(General.nz(Text5.Text, 0)) + ",";
                reqUpdate = reqUpdate + "EchangeurFluide =" + Convert.ToInt32(General.nz(txtFields16.Text, 0)) + ",";
                reqUpdate = reqUpdate + "EchangeurVanne =" + (Check1.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "EchangeurAsser =" + (Check4.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "RegulateurMarque ='" + Combo2.Text + "',";
                reqUpdate = reqUpdate + "RegulateurType ='" + txtFields5.Text + "',";
                reqUpdate = reqUpdate + "RegulateurNbre =" + Convert.ToInt32(General.nz(txtFields10.Text, 0)) + ",";
                reqUpdate = reqUpdate + "RegulateurAss =" + Convert.ToInt32(General.nz(txtFields7.Text, 0)) + ",";
                reqUpdate = reqUpdate + "RegulteurTherm =" + (Check3.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "RegulateurAsser =" + (Check5.Checked ? 1 : 0) + ",";
                reqUpdate = reqUpdate + "VanneDetenteMarque ='" + Combo3.Text + "'," ;
                reqUpdate = reqUpdate + "VanneDetenteType ='" + txtFields3.Text + "',";
               reqUpdate = reqUpdate + "VanneDetenteNbre =" + Convert.ToInt32(General.nz(Text2.Text, 0)) + ",";
                reqUpdate = reqUpdate + "VanneDetenteAsser =" + (Check6.Checked ? 1 : 0) + ",";
                 reqUpdate = reqUpdate + "VanneDetenteAss =" + Convert.ToInt32(General.nz(txtFields6.Text, 0)) + ",";
                reqUpdate = reqUpdate + "VanneCondMarque ='" + Combo4.Text + "',";
                reqUpdate = reqUpdate + "VanneCondType ='" + txtFields9.Text + "',";
                reqUpdate = reqUpdate + "VanneCondNbre =" + Convert.ToInt32(General.nz(Text3.Text, 0)) + "," ;
                reqUpdate = reqUpdate + "VanneCondAss =" + Convert.ToInt32(General.nz(txtFields8.Text, 0)) + ",";
               reqUpdate = reqUpdate + "VanneCondAsser =" + (Check7.Checked ? 1 : 0) + "  WHERE CodeImmeuble='" + General.gVar + "'";
                int xx = General.Execute(reqUpdate);
            }

            string SQL = "update TechReleve set RelPrimaire=" + -1 + " WHERE CodeImmeuble='" + General.gVar + "'";
            int yy = General.Execute(SQL);
        }

    
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option1_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option1.Checked)
            {
                txtFields16.Text = "0";
            }
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option10_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option10.Checked)
            {
                txtFields8.Text = "1";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option11_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option11.Checked)
            {
                txtFields8.Text = "2";

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option12_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option12.Checked)
            {
                txtFields16.Text = "2";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option13_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option13.Checked)
            {
                txtFields7.Text = "3";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option14_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option14.Checked)
            {
                txtFields6.Text = "3";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option15_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option15.Checked)
            {
                txtFields8.Text = "3";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option16_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option16.Checked)
            {
                txtFields87.Text = "2";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option17_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option17.Checked)
            {
               txtFields13.Text = "1";

            }
        }

        private void Option18_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option18.Checked)
            {
                txtFields13.Text = "0";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option2_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option2.Checked)
            {
                txtFields16.Text = "1";

            }
        }

        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option3_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option3.Checked)
            {
                txtFields7.Text = "0";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option37_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option37.Checked)
            {
                txtFields87.Text = "0";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option38_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option38.Checked)
            {
                txtFields87.Text = "1";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option4_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option4.Checked)
            {
                txtFields7.Text = "1";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option5_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option5.Checked)
            {
                txtFields7.Text = "2";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option6_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option6.Checked)
            {
                txtFields6.Text = "2";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option7_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option7.Checked)
            {
                txtFields6.Text = "1";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option8_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option8.Checked)
            {
                txtFields6.Text = "0";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option9_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option9.Checked)
            {
                txtFields8.Text = "0";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void PrimQuit_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            this.Close();
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtFields_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            // short Index = txtFields.GetIndex(eventSender);
            var Txtfields = eventSender as iTalk.iTalk_TextBox_Small2;
            Int16 Index = Convert.ToInt16(Txtfields.Tag);

            switch (Index)
            {

                case 6:
                    //CH1EV
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option8.Checked = true;
                            break;
                        case "1":
                            Option7.Checked = true;
                            break;
                        case "2":
                            Option6.Checked = true;
                            break;
                        case "3":
                            Option14.Checked = true;
                            break;
                    }
                    break;

                case 7:
                    //CH1EV
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option3.Checked = true;
                            break;
                        case "1":
                            Option4.Checked = true;
                            break;
                        case "2":
                            Option5.Checked = true;
                            break;
                        case "3":
                            Option13.Checked = true;
                            break;
                    }
                    break;

                case 8:
                    //CH1EV
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option9.Checked = true;
                            break;
                        case "1":
                            Option10.Checked = true;
                            break;
                        case "2":
                            Option11.Checked = true;
                            break;
                        case "3":
                            Option15.Checked = true;
                            break;
                    }
                    break;

                case 13:
                    //CH1EB
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option18.Checked = true;
                            break;
                        case "1":
                            Option17.Checked = true;
                            break;
                    }
                    break;

                case 16:
                    //CH1EV
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option1.Checked = true;
                            break;
                        case "1":
                            Option2.Checked = true;
                            break;
                        case "2":
                            Option12.Checked = true;
                            break;
                    }
                    break;


                case 87:
                    //expansion
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option37.Checked = true;
                            break;
                        case "1":
                            Option38.Checked = true;
                            break;
                        case "2":
                            Option16.Checked = true;
                            break;
                    }
                    break;

            }
        }
    }
}
