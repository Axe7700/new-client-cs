﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.CompteRendu
{
    public partial class frmReleveConf : Form
    {
        ModAdo ModAdo1 = new ModAdo();
        DataTable DatPrimaryRS = new DataTable();
        public frmReleveConf()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void ConfQuit_Click(Object eventSender, EventArgs eventArgs)
        {
            this.Close();
        }

        /// button invisible
        private void cmdAjouter_Click(Object eventSender, EventArgs eventArgs)
        {
            //DatPrimaryRS.Recordset.AddNew();
            ////Text3.Text = gVar
            DatPrimaryRS.Rows.Add(DatPrimaryRS.NewRow());
            cmdAjouter.Enabled = false;
            cmdMAJ.Enabled = true;
        }


        private void cmdMAJ_Click(Object eventSender, EventArgs eventArgs)
        {
            /// initialisation des boutons d'options en création
            if (string.IsNullOrEmpty(Text5.Text))
            {
                Text5.Text = "2";
            }
            if (string.IsNullOrEmpty(Text6.Text))
            {
                Text6.Text = "2";
            }
            if (string.IsNullOrEmpty(Text4.Text))
            {
                Text4.Text = "4";
            }
            if (string.IsNullOrEmpty(Text2.Text))
            {
                Text2.Text = "4";
            }
            if (string.IsNullOrEmpty(Text8.Text))
            {
                Text8.Text = "2";
            }
            if (string.IsNullOrEmpty(Text7.Text))
            {
                Text7.Text = "2";
            }
            if (string.IsNullOrEmpty(Text10.Text))
            {
                Text10.Text = "0";
            }

            //Text3.Text = General.gVar.ToString();
            
            //Modifier Par bind
            var ModAdo = new ModAdo();
            var getSqlCount = ModAdo.fc_ADOlibelle("select count(*) from TechConformite WHERE CodeImmeuble='" + General.gVar + "'");
            if(getSqlCount=="0")
            {
                DatPrimaryRS.Rows[0]["CodeImmeuble"] = General.gVar;
                ModAdo1.Update();
            }
            getSqlCount = ModAdo.fc_ADOlibelle("select count(*) from TechConformite WHERE CodeImmeuble='" + General.gVar + "'");
            if (getSqlCount == "1")
            {
                var sqlUpdate = "update  TechConformite SET ";
                sqlUpdate = sqlUpdate + " A1 ='" + (Check10.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A2 ='" + (Check11.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A4 ='" + (Check12.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A5 ='" + (Check13.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A3 ='" + (Check14.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A6 ='" + (Check15.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A7 ='" + (Check16.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A8 ='" + (Check17.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A9 ='" + (Check18.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A10 ='" + (Check19.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A31 ='" + (Check3.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A11 ='" + (Check110.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A12 ='" + (Check111.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A13 ='" + (Check112.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A14 ='" + (Check113.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A15 ='" + (Check114.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A16 ='" + (Check115.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A17 ='" + (Check116.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A18 ='" + (Check117.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A19 ='" + (Check118.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A20 ='" + (Check119.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A21 ='" + (Check120.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A26 ='" + (Check123.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A28 ='" + (Check153.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A27 ='" + (Check143.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A33 ='" + (Check5.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A30 ='" + (Check2.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A32 ='" + (Check4.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " C1 ='" + (Check139.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " C2 ='" + (Check140.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " C3 ='" + (Check141.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " C4 ='" + (Check142.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " C5 ='" + (Check125.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " C6 ='" + (Check124.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " C7 ='" + (Check144.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " C8 ='" + (Check145.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " C11 ='" + (Check135.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " C12 ='" + (Check147.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " C14 ='" + (Check148.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " C9 ='" + (Check129.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " C15 ='" + (Check146.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " C13 ='" + (Check136.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " D1 ='" + (Check149.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " D2 ='" + (Check150.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " D3 ='" + (Check151.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " D4 ='" + (Check152.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A34 ='" + (Check6.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A35 ='" + (Check7.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A36 ='" + (Check8.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A37 ='" + (Check9.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A38 ='" + (Check10.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A39 ='" + (Check11.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " A40 ='" + (Check12.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " B1 ='" + (Check126.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " B2 ='" + (Check127.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " B3 ='" + (Check128.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " B8 ='" + (Check121.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " B10 ='" + (Check122.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " B9 ='" + (Check137.Checked ? 1 : 0) + "',";
                sqlUpdate = sqlUpdate + " PropreteSoute ='" +  Text4.Text + "',";
                sqlUpdate = sqlUpdate + " Temperature ='" + Text1.Text  + "',";
                sqlUpdate = sqlUpdate + " PropreteChaufferie ='" + Text2.Text + "',";
                sqlUpdate = sqlUpdate + " TypePorteGene ='" + Text5.Text  + "',";
                sqlUpdate = sqlUpdate + " TypeCFChauff ='" + Text7.Text + "',";
                sqlUpdate = sqlUpdate + " TypePorteChauff ='" + Text8.Text + "',";
                sqlUpdate = sqlUpdate + " TypeCFGene ='" + Text6.Text + "',";
                sqlUpdate = sqlUpdate + " EspaceChaudieres ='" + Text10.Text + "',";
                sqlUpdate = sqlUpdate + " Observation ='" +StdSQLchaine.gFr_DoublerQuote(Text9.Text) + "' ";
                sqlUpdate = sqlUpdate + " WHERE CodeImmeuble='" + General.gVar + "'";
                General.Execute(sqlUpdate);
                General.SQL = "update TechReleve set RelConformite=" + -1 + " WHERE CodeImmeuble='" + General.gVar + "'";
                //gdb.Execute SQL
                General.Execute(General.SQL);
            }
        }


        //private void DatPrimaryRS_Error(int ErrorNumber, ref string Description, int sCode, string Source, string HelpFile, int HelpContext, ref bool fCancelDisplay)
        //{
        //    fCancelDisplay = true;
        //}

        private void frmReleveConf_Load(Object eventSender, EventArgs eventArgs)
        {
            cmdAjouter.Enabled = false;
            cmdAjouter.Visible = false;
            cmdMAJ.Enabled = false;

            this.Text = this.Text + " pour l'immeuble " + General.gVar;
            
            Text1.Visible = false;
            Text2.Visible = false;
            Text4.Visible = false;
            Text5.Visible = false;
            Text6.Visible = false;
            Text7.Visible = false;
            Text8.Visible = false;
            Text10.Visible = false;
            General.SQL = "SELECT * FROM TechConformite " + "WHERE CodeImmeuble='" + General.gVar + "'";
            DatPrimaryRS = ModAdo1.fc_OpenRecordSet(General.SQL);
            if (DatPrimaryRS.Rows.Count == 0)
            {
                cmdAjouter.Enabled = true;
                cmdAjouter_Click(cmdAjouter, new EventArgs());
                cmdMAJ_Click(cmdMAJ, new EventArgs());
            }
            else
            {
                cmdMAJ.Enabled = true;
            }
            var row = DatPrimaryRS.Rows[0];
            Check10.Checked = row["A1"].ToString().ToLower() == "true";
            Check11.Checked = row["A2"].ToString().ToLower() == "true";
            Check12.Checked = row["A4"].ToString().ToLower() == "true";
            Check13.Checked = row["A5"].ToString().ToLower() == "true";
            Check14.Checked = row["A3"].ToString().ToLower() == "true";
            Check15.Checked = row["A6"].ToString().ToLower() == "true";
            Check16.Checked = row["A7"].ToString().ToLower() == "true";
            Check17.Checked = row["A8"].ToString().ToLower() == "true";
            Check18.Checked = row["A9"].ToString().ToLower() == "true";
            Check19.Checked = row["A10"].ToString().ToLower() == "true";
            Check3.Checked = row["A31"].ToString().ToLower() == "true";
            Check110.Checked = row["A11"].ToString().ToLower() == "true";
            Check111.Checked = row["A12"].ToString().ToLower() == "true";
            Check112.Checked = row["A13"].ToString().ToLower() == "true";
            Check113.Checked = row["A14"].ToString().ToLower() == "true";
            Check114.Checked = row["A15"].ToString().ToLower() == "true";
            Check115.Checked = row["A16"].ToString().ToLower() == "true";
            Check116.Checked = row["A17"].ToString().ToLower() == "true";
            Check117.Checked = row["A18"].ToString().ToLower() == "true";
            Check118.Checked = row["A19"].ToString().ToLower() == "true";
            Check119.Checked = row["A20"].ToString().ToLower() == "true";
            Check120.Checked = row["A21"].ToString().ToLower() == "true";
            Check123.Checked = row["A26"].ToString().ToLower() == "true";
            Check153.Checked = row["A28"].ToString().ToLower() == "true";
            Check143.Checked = row["A27"].ToString().ToLower() == "true";
            Check5.Checked = row["A33"].ToString().ToLower() == "true";
            Check2.Checked = row["A30"].ToString().ToLower() == "true";
            Check4.Checked = row["A32"].ToString().ToLower() == "true";
            Check139.Checked = row["C1"].ToString().ToLower() == "true";
            Check140.Checked = row["C2"].ToString().ToLower() == "true";
            Check141.Checked = row["C3"].ToString().ToLower() == "true";
            Check142.Checked = row["C4"].ToString().ToLower() == "true";
            Check125.Checked = row["C5"].ToString().ToLower() == "true";
            Check124.Checked = row["C6"].ToString().ToLower() == "true";
            Check144.Checked = row["C7"].ToString().ToLower() == "true";
            Check145.Checked = row["C8"].ToString().ToLower() == "true";
            Check135.Checked = row["C11"].ToString().ToLower() == "true";
            Check147.Checked = row["C12"].ToString().ToLower() == "true";
            Check148.Checked = row["C14"].ToString().ToLower() == "true";
            Check129.Checked = row["C9"].ToString().ToLower() == "true";
            Check146.Checked = row["C15"].ToString().ToLower() == "true";
            Check136.Checked = row["C13"].ToString().ToLower() == "true";
            Check149.Checked = row["D1"].ToString().ToLower() == "true";
            Check150.Checked = row["D2"].ToString().ToLower() == "true";
            Check151.Checked = row["D3"].ToString().ToLower() == "true";
            Check152.Checked = row["D4"].ToString().ToLower() == "true";
            Check6.Checked = row["A34"].ToString().ToLower() == "true";
            Check7.Checked = row["A35"].ToString().ToLower() == "true";
            Check8.Checked = row["A36"].ToString().ToLower() == "true";
            Check9.Checked = row["A37"].ToString().ToLower() == "true";
            Check10.Checked = row["A38"].ToString().ToLower() == "true";
            Check11.Checked = row["A39"].ToString().ToLower() == "true";
            Check12.Checked = row["A40"].ToString().ToLower() == "true";
            Check126.Checked = row["B1"].ToString().ToLower() == "true";
            Check127.Checked = row["B2"].ToString().ToLower() == "true";
            Check128.Checked = row["B3"].ToString().ToLower() == "true";
            Check121.Checked = row["B8"].ToString().ToLower() == "true";
            Check122.Checked = row["B10"].ToString().ToLower() == "true";
            Check137.Checked = row["B9"].ToString().ToLower() == "true";
            Text4.Text = row["PropreteSoute"].ToString();
            Text1.Text = row["Temperature"].ToString();
            Text2.Text = row["PropreteChaufferie"].ToString();
            Text5.Text = row["TypePorteGene"].ToString();
            Text7.Text = row["TypeCFChauff"].ToString();
            Text8.Text = row["TypePorteChauff"].ToString();
            Text6.Text = row["TypeCFGene"].ToString();
            Text10.Text = row["EspaceChaudieres"].ToString();
            Text9.Text = row["Observation"].ToString();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void frmReleveConf_FormClosed(Object eventSender, FormClosedEventArgs eventArgs)
        {
            Cursor.Current = Cursors.Default;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option1_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option1.Checked)
            {
                Text2.Text = "0";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option25_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option25.Checked)
            {
                Text10.Text = "0";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option26_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option26.Checked)
            {
                Text10.Text = "1";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option11_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option11.Checked)
            {
                Text5.Text = "0";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option12_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option12.Checked)
            {
                Text5.Text = "1";
            }
        }
        /// <summary>
        /// Testeed
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option13_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option13.Checked)
            {
                Text6.Text = "3";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option14_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option14.Checked)
            {
                Text6.Text = "1";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option15_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option15.Checked)
            {
                Text7.Text = "0";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option16_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option16.Checked)
            {
                Text7.Text = "3";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option17_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option17.Checked)
            {
                Text8.Text = "0";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option18_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option18.Checked)
            {
                Text8.Text = "1";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option19_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option19.Checked)
            {
                Text5.Text = "2";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option2_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option2.Checked)
            {
                Text2.Text = "1";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option20_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option20.Checked)
            {
                Text2.Text = "4";
            }
        }

        private void Option21_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option21.Checked)
            {
                Text8.Text = "2";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option22_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option22.Checked)
            {
                Text4.Text = "4";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option23_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option23.Checked)
            {
                Text6.Text = "2";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option24_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option24.Checked)
            {
                Text7.Text = "2";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option3_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option3.Checked)
            {
                Text2.Text = "2";
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option4_CheckedChanged(Object eventSender, EventArgs eventArgs)
        {
            if (Option4.Checked)
            {
                Text2.Text = "3";
            }
        }
        /// <summary>
        /// testeed
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Text2_TextChanged(Object eventSender, EventArgs eventArgs)
        {
            switch (Text2.Text)
            {
                case "0":
                    Option1.Checked = true;
                    break;
                case "1":
                    Option2.Checked = true;
                    break;
                case "2":
                    Option3.Checked = true;
                    break;
                case "3":
                    Option4.Checked = true;
                    break;
                case "4":
                    Option20.Checked = true;
                    break;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Text4_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            switch (Text4.Text)
            {
                case "0":
                    Option5.Checked = true;
                    break;
                case "1":
                    Option6.Checked = true;
                    break;
                case "2":
                    Option7.Checked = true;
                    break;
                case "3":
                    Option8.Checked = true;
                    break;
                case "4":
                    Option22.Checked = true;
                    break;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Text1_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {

            switch (Text1.Text)
            {
                case "0":
                    Option9.Checked = true;
                    break;
                case "1":
                    Option10.Checked = true;
                    break;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Text10_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            switch (Text10.Text)
            {
                case "0":
                    Option25.Checked = true;
                    break;
                case "1":
                    Option26.Checked = true;
                    break;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option5_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option5.Checked)
            {
                Text4.Text = "0";
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option6_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option6.Checked)
            {
                Text4.Text = "1";
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option7_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option7.Checked)
            {
                Text4.Text = "2";
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option8_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option8.Checked)
            {
                Text4.Text = "3";
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option9_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option9.Checked)
            {
                Text1.Text = "0";
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option10_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option10.Checked)
            {
                Text1.Text = "1";
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Text5_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {

            switch (Text5.Text)
            {
                case "0":
                    Option11.Checked = true;
                    break;
                case "1":
                    Option12.Checked = true;
                    break;
                case "2":
                    Option19.Checked = true;
                    break;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Text6_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            switch (Text6.Text)
            {
                case "3":
                    Option13.Checked = true;
                    break;
                case "1":
                    Option14.Checked = true;
                    break;
                case "2":
                    Option23.Checked = true;
                    break;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Text7_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            switch (Text7.Text)
            {
                case "0":
                    Option15.Checked = true;
                    break;
                case "3":
                    Option16.Checked = true;
                    break;
                case "2":
                    Option24.Checked = true;
                    break;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Text8_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            switch (Text8.Text)
            {
                case "0":
                    Option17.Checked = true;
                    break;
                case "1":
                    Option18.Checked = true;
                    break;
                case "2":
                    Option21.Checked = true;
                    break;
            }

        }
    }
}
