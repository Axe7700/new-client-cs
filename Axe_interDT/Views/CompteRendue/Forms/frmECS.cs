﻿
using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.CompteRendu
{
    public partial class frmECS : Form
    {
        public frmECS()
        {
            InitializeComponent();
        }
        int swchange;

        ModAdo ModAdo2 = new ModAdo();
        DataTable DatPrimaryRS = new DataTable();
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Check5_CheckedChanged(object sender, EventArgs e)
        {
            if (Check5.Checked == true)
            {
                Frame1.Enabled = false;
                Frame17.Enabled = false;
                Frame25.Enabled = false;
            }
            else
            {

                Frame1.Enabled = true;
                Frame17.Enabled = true;
                Frame25.Enabled = true;

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void EcsQuit_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            this.Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void frmECS_Load(System.Object eventSender, System.EventArgs eventArgs)
        {
           
            cmdAjouter.Enabled = false;
            cmdAjouter.Visible = false;
            cmdMAJ.Enabled = false;

        
            //ouverture des data pour les marque de materiel
            
            sheridan.InitialiseCombo(Combo10, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=201", "Marque", false, null, "");
            sheridan.InitialiseCombo(Combo100, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=219", "Marque", false, null, "");          
            sheridan.InitialiseCombo(Combo100, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=203", "Marque", false, null, "");
            sheridan.InitialiseCombo(Combo20, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=203", "Marque", false, null, "");
            sheridan.InitialiseCombo(Combo3, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=204", "Marque", false, null, "");           
            sheridan.InitialiseCombo(Combo4, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=206", "Marque", false, null, "");
            sheridan.InitialiseCombo(Combo9, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=206", "Marque", false, null, "");
            sheridan.InitialiseCombo(Combo11, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=206", "Marque", false, null, "");
            sheridan.InitialiseCombo(Combo12, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=206", "Marque", false, null, "");         
            sheridan.InitialiseCombo(Combo7, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=208", "Marque", false, null, "");         
            sheridan.InitialiseCombo(Combo5, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=210", "Marque", false, null, "");       
            sheridan.InitialiseCombo(Combo6, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=211", "Marque", false, null, "");       
            sheridan.InitialiseCombo(Combo8, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=216", "Marque", false, null, "");
            

            this.Text = this.Text + " pour l'immeuble " + General.gVar;
            General.SQL = "SELECT * FROM MaterielECS " + "WHERE CodeImmeuble='" + General.gVar + "'";
            DatPrimaryRS = ModAdo2.fc_OpenRecordSet(General.SQL);
            if (DatPrimaryRS.Rows.Count == 0)
            {
                cmdAjouter.Enabled = true;
                cmdAjouter_Click(cmdAjouter, new System.EventArgs());
            }
            else
            {
                cmdMAJ.Enabled = true;
                Combo10.Text = DatPrimaryRS.Rows[0]["ChaudiereMarque1"].ToString();
                txtFields28.Text = DatPrimaryRS.Rows[0]["ChaudiereType1"].ToString();
                txtFields48.Text = DatPrimaryRS.Rows[0]["ChaudierePuissance1"].ToString();
                txtFields6.Text = DatPrimaryRS.Rows[0]["ChaudiereNbre1"].ToString();
                txtFields16.Text = DatPrimaryRS.Rows[0]["ChaudiereFluide1"].ToString();
                txtFields32.Text = DatPrimaryRS.Rows[0]["ChaudiereTypeBriquetage1"].ToString();
                txtFields13.Text = DatPrimaryRS.Rows[0]["ChaudiereEtatBriquetage1"].ToString();
                Combo20.Text = DatPrimaryRS.Rows[0]["BruleurMarque1"].ToString();
                txtFields5.Text = DatPrimaryRS.Rows[0]["BruleurType1"].ToString();
                txtFields7.Text = DatPrimaryRS.Rows[0]["BruleurNbre1"].ToString();
                Text3.Text = DatPrimaryRS.Rows[0]["BruleurMarqueBoite1"].ToString();
                Text4.Text = DatPrimaryRS.Rows[0]["BruleurTypeBoite1"].ToString();
                txtFields9.Text = DatPrimaryRS.Rows[0]["BruleurSystem1"].ToString();
                txtFields28.Text = DatPrimaryRS.Rows[0]["ChaudiereType1"].ToString();
                Check2.CheckState = (DatPrimaryRS.Rows[0]["BruleurDoubleSecurite1"]!=null || DatPrimaryRS.Rows[0]["BruleurDoubleSecurite1"].ToString() =="True") ? CheckState.Checked:CheckState.Unchecked  ;
                txtFields10.Text = DatPrimaryRS.Rows[0]["ChemineeHauteur1"].ToString();
                txtFields1.Text = DatPrimaryRS.Rows[0]["ChemineeSection1"].ToString();
                Check9.Text = DatPrimaryRS.Rows[0]["ChemineeRegulateur1"].ToString();
                txtFields12.Text = DatPrimaryRS.Rows[0]["ChemineeLongueur1"].ToString();
                txtFields11.Text = DatPrimaryRS.Rows[0]["ChemineeDiam1"].ToString();
                txtFields86.Text = DatPrimaryRS.Rows[0]["CodeImmeuble"].ToString();
                Check5.CheckState = (DatPrimaryRS.Rows[0]["ChIdemChauff"]!=null || DatPrimaryRS.Rows[0]["ChIdemChauff"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Combo3.Text = DatPrimaryRS.Rows[0]["PompeCHMarque1"].ToString();
                txtFields38.Text = DatPrimaryRS.Rows[0]["PompeCHType1"].ToString();
                txtFields39.Text = DatPrimaryRS.Rows[0]["PompeCHNbre1"].ToString();
                Check13.CheckState = (DatPrimaryRS.Rows[0]["PompeCHVanne1"]!=null || DatPrimaryRS.Rows[0]["PompeCHVanne1"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Combo4.Text = DatPrimaryRS.Rows[0]["PompeRECHMarque"].ToString();
                txtFields57.Text = DatPrimaryRS.Rows[0]["PompeRECHType"].ToString();
                txtFields56.Text = DatPrimaryRS.Rows[0]["PompeRECHNbre"].ToString();
                Check20.CheckState =( DatPrimaryRS.Rows[0]["PompeRECHAsser"]!=null || DatPrimaryRS.Rows[0]["PompeRECHAsser"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Check1.CheckState =(DatPrimaryRS.Rows[0]["PompeRECHVanne"]!=null || DatPrimaryRS.Rows[0]["PompeRECHVanne"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Combo9.Text = DatPrimaryRS.Rows[0]["PompeREINMarque"].ToString();
                txtFields2.Text = DatPrimaryRS.Rows[0]["PompeREINType"].ToString();
                txtFields3.Text = DatPrimaryRS.Rows[0]["PompeREINNbre"].ToString();
               Check3.CheckState = (DatPrimaryRS.Rows[0]["PompeREINVanne"]!=null || DatPrimaryRS.Rows[0]["PompeREINVanne"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Combo5.Text = DatPrimaryRS.Rows[0]["BallonMarque"].ToString();
                txtFields41.Text = DatPrimaryRS.Rows[0]["BallonType"].ToString();
                txtFields40.Text = DatPrimaryRS.Rows[0]["BallonCapacite"].ToString();
                Text5.Text = DatPrimaryRS.Rows[0]["BallonNbre"].ToString();
                Check14.CheckState = (DatPrimaryRS.Rows[0]["BallonTraditionnel"]!=null || DatPrimaryRS.Rows[0]["BallonTraditionnel"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Check16.CheckState = (DatPrimaryRS.Rows[0]["BallonChasse"]!=null ||  DatPrimaryRS.Rows[0]["BallonChasse"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Combo11.Text = DatPrimaryRS.Rows[0]["PompeREBAMarque"].ToString();
                Text7.Text = DatPrimaryRS.Rows[0]["PompeREBAType"].ToString();
                Text8.Text = DatPrimaryRS.Rows[0]["PompeREBANbre"].ToString();
                Check4.CheckState = (DatPrimaryRS.Rows[0]["PompeREBAVanne"]!=null || DatPrimaryRS.Rows[0]["PompeREBAVanne"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Combo12.Text = DatPrimaryRS.Rows[0]["BallonTamponMarque"].ToString();
                Text9.Text = DatPrimaryRS.Rows[0]["BallonTamponCapacite"].ToString();
                Combo7.Text = DatPrimaryRS.Rows[0]["RegulationMarque"].ToString();
                Text2.Text = DatPrimaryRS.Rows[0]["RegulationNbre"].ToString();
                Check19.CheckState =( DatPrimaryRS.Rows[0]["RegulationActionBruleur"]!=null || DatPrimaryRS.Rows[0]["RegulationActionBruleur"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Check210.CheckState =(DatPrimaryRS.Rows[0]["RegulationActionVanne3"]!=null || DatPrimaryRS.Rows[0]["RegulationActionVanne3"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                Check211.CheckState = (DatPrimaryRS.Rows[0]["RegulationActionVanne4"]!=null ||DatPrimaryRS.Rows[0]["RegulationActionVanne4"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                txtFields64.Text = DatPrimaryRS.Rows[0]["RegulationType"].ToString();
                txtFields61.Text = DatPrimaryRS.Rows[0]["VanneMarque"].ToString();
                txtFields4.Text = DatPrimaryRS.Rows[0]["VanneASecteur"].ToString();
                txtFields60.Text = DatPrimaryRS.Rows[0]["VanneType"].ToString();
                Combo6.Text = DatPrimaryRS.Rows[0]["PreparateurMarque"].ToString();
                txtFields44.Text = DatPrimaryRS.Rows[0]["PreparateurType"].ToString();
                txtFields43.Text = DatPrimaryRS.Rows[0]["PreparateurNbre"].ToString();
                Text6.Text = DatPrimaryRS.Rows[0]["PreparateurCapacite"].ToString();
                txtFields87.Text = DatPrimaryRS.Rows[0]["ExpansionType"].ToString();
                Combo8.Text = DatPrimaryRS.Rows[0]["ExpansionMarque"].ToString();
                txtFields70.Text = DatPrimaryRS.Rows[0]["ExpansionCapacite"].ToString();
                Combo100.Text = DatPrimaryRS.Rows[0]["CptGazMarque"].ToString();
                txtFields15.Text = DatPrimaryRS.Rows[0]["CptGazType"].ToString();
                txtFields0.Text = DatPrimaryRS.Rows[0]["CptGazNum"].ToString();
                Check6.CheckState = (DatPrimaryRS.Rows[0]["ByPass"] != null || DatPrimaryRS.Rows[0]["ByPass"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked; 
                Check213.CheckState = (DatPrimaryRS.Rows[0]["CleBarrage"]!= null || DatPrimaryRS.Rows[0]["CleBarrage"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked; 
                Check212.CheckState = (DatPrimaryRS.Rows[0]["CleExt"] != null || DatPrimaryRS.Rows[0]["CleExt"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked; 
                Text1.Text = DatPrimaryRS.Rows[0]["Observation"].ToString();

            
            }
            
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        public void cmdAjouter_Click(System.Object eventSender, System.EventArgs eventArgs)
        {           
            DatPrimaryRS.Rows.Add(DatPrimaryRS.NewRow());
            ////txtFields(86).Text = gVar
            cmdAjouter.Enabled = false;
            cmdMAJ.Enabled = true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdMAJ_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            int existImmeuble;
            short intI = 0;
            /// initialisation des boutons d'options en création
            if (string.IsNullOrEmpty(txtFields16.Text))
            {
                txtFields16.Text = "2";
            }
            if (string.IsNullOrEmpty(txtFields9.Text))
            {
                txtFields9.Text = "2";
            }
            if (string.IsNullOrEmpty(txtFields13.Text))
            {
                txtFields13.Text = "3";
            }
            if (string.IsNullOrEmpty(txtFields32.Text))
            {
                txtFields32.Text = "2";
            }
            if (string.IsNullOrEmpty(txtFields87.Text))
            {
                txtFields87.Text = "2";
            }
            if (string.IsNullOrEmpty(txtFields61.Text))
            {
                txtFields4.Text = "0";
            }

            /// initialisation des quantités en création
            txtFields3.Text = InitField(txtFields3.Text);
            txtFields6.Text = InitField(txtFields6.Text);
            txtFields7.Text = InitField(txtFields7.Text);
            txtFields39.Text = InitField(txtFields39.Text);
            txtFields43.Text = InitField(txtFields43.Text);
            txtFields56.Text = InitField(txtFields56.Text);
            Text2.Text = InitField(Text2.Text);
            Text5.Text = InitField(Text5.Text);
            Text8.Text = InitField(Text8.Text);
            txtFields86.Text = General.gVar.ToString();

            using (var tmp = new ModAdo())
                existImmeuble = Convert.ToInt32(tmp.fc_ADOlibelle("select  count( CodeImmeuble) from  MaterielECS   WHERE CodeImmeuble='" + General.gVar + "'"));
            
            if (existImmeuble == 0)
            {
                DatPrimaryRS.Rows[0]["ChaudiereMarque1"] = Combo10.Text;
                DatPrimaryRS.Rows[0]["ChaudiereType1"] = txtFields28.Text;
                DatPrimaryRS.Rows[0]["ChaudierePuissance1"] = txtFields48.Text;
                DatPrimaryRS.Rows[0]["ChaudiereNbre1"] =  Convert.ToInt16(General.nz(txtFields6.Text, 0)) ;
                DatPrimaryRS.Rows[0]["ChaudiereFluide1"] = Convert.ToInt16(General.nz(txtFields16.Text, 0));
                DatPrimaryRS.Rows[0]["ChaudiereTypeBriquetage1"] = Convert.ToInt16(General.nz(txtFields32.Text, 0));
                DatPrimaryRS.Rows[0]["ChaudiereEtatBriquetage1"] = Convert.ToInt16(General.nz(txtFields13.Text, 0));
                DatPrimaryRS.Rows[0]["BruleurMarque1"] = Combo20.Text;
                DatPrimaryRS.Rows[0]["BruleurType1"] = txtFields5.Text;
                DatPrimaryRS.Rows[0]["BruleurNbre1"] = Convert.ToInt16(General.nz(txtFields7.Text, 0));
                DatPrimaryRS.Rows[0]["BruleurMarqueBoite1"]= Text3.Text;
                DatPrimaryRS.Rows[0]["BruleurTypeBoite1"] = Text4.Text;
                DatPrimaryRS.Rows[0]["BruleurSystem1"] = Convert.ToInt16(General.nz(txtFields9.Text, 0));
                DatPrimaryRS.Rows[0]["ChaudiereType1"] =txtFields28.Text;
                DatPrimaryRS.Rows[0]["BruleurDoubleSecurite1"] = (Check2.Checked ? "True" : "False");
                DatPrimaryRS.Rows[0]["ChemineeHauteur1"] = Convert.ToDouble(General.nz(txtFields10.Text, 0));
                DatPrimaryRS.Rows[0]["ChemineeSection1"] = txtFields1.Text;
                DatPrimaryRS.Rows[0]["ChemineeRegulateur1"]= (Check9.Checked ? "True" : "False");
                DatPrimaryRS.Rows[0]["ChemineeLongueur1"] = txtFields12.Text;
                DatPrimaryRS.Rows[0]["ChemineeDiam1"] = txtFields11.Text;
               DatPrimaryRS.Rows[0]["CodeImmeuble"] =txtFields86.Text;
               DatPrimaryRS.Rows[0]["ChIdemChauff"]= (Check5.Checked ? "True" : "False");
                 DatPrimaryRS.Rows[0]["PompeCHMarque1"]=Combo3.Text;
                DatPrimaryRS.Rows[0]["PompeCHType1"] = txtFields38.Text;
                DatPrimaryRS.Rows[0]["PompeCHNbre1"] = txtFields39.Text;
                DatPrimaryRS.Rows[0]["PompeCHVanne1"] = (Check13.Checked ? "True" : "False");
                DatPrimaryRS.Rows[0]["PompeRECHMarque"]=Combo4.Text;
                DatPrimaryRS.Rows[0]["PompeRECHType"] = txtFields57.Text;
                DatPrimaryRS.Rows[0]["PompeRECHNbre"] = Convert.ToInt16(General.nz(txtFields56.Text, 0));
                DatPrimaryRS.Rows[0]["PompeRECHAsser"] = (Check20.Checked ? "True" : "False");
                DatPrimaryRS.Rows[0]["PompeRECHVanne"] = (Check1.Checked ? "True" : "False");
                DatPrimaryRS.Rows[0]["PompeREINMarque"] = Combo9.Text;
                DatPrimaryRS.Rows[0]["PompeREINType"] = txtFields2.Text;
                DatPrimaryRS.Rows[0]["PompeREINNbre"] = Convert.ToInt16(General.nz(txtFields3.Text, 0));
                DatPrimaryRS.Rows[0]["PompeREINVanne"] = (Check3.Checked ? "True" : "False");
                DatPrimaryRS.Rows[0]["BallonMarque"]=Combo5.Text;
                DatPrimaryRS.Rows[0]["BallonType"] = txtFields41.Text;
                DatPrimaryRS.Rows[0]["BallonCapacite"] = txtFields40.Text;
                DatPrimaryRS.Rows[0]["BallonNbre"] = Convert.ToInt16(General.nz(Text5.Text, 0));
                DatPrimaryRS.Rows[0]["BallonTraditionnel"]= (Check14.Checked ? "True" : "False");
                DatPrimaryRS.Rows[0]["BallonChasse"] = (Check16.Checked ? "True" : "False");
                DatPrimaryRS.Rows[0]["PompeREBAMarque"] = Combo11.Text;
                DatPrimaryRS.Rows[0]["PompeREBAType"] = Text7.Text;
                DatPrimaryRS.Rows[0]["PompeREBANbre"] = Text8.Text;
                DatPrimaryRS.Rows[0]["PompeREBAVanne"] = (Check4.Checked ? "True" : "False");
                DatPrimaryRS.Rows[0]["BallonTamponMarque"]=Combo12.Text;
                DatPrimaryRS.Rows[0]["BallonTamponCapacite"] = Text9.Text;
                DatPrimaryRS.Rows[0]["RegulationMarque"]=Combo7.Text;
                DatPrimaryRS.Rows[0]["RegulationNbre"] = Convert.ToInt16(General.nz(Text2.Text, 0));
                DatPrimaryRS.Rows[0]["RegulationActionBruleur"] = (Check19.Checked ? "True" : "False");
                DatPrimaryRS.Rows[0]["RegulationActionVanne3"] = (Check210.Checked ? "True" : "False");
                DatPrimaryRS.Rows[0]["RegulationActionVanne4"] = (Check211.Checked ? "True" : "False");
                DatPrimaryRS.Rows[0]["RegulationType"] = txtFields64.Text;
                DatPrimaryRS.Rows[0]["VanneMarque"] = txtFields61.Text;
                DatPrimaryRS.Rows[0]["VanneASecteur"] = Convert.ToInt16(General.nz(txtFields4.Text, 0));
                DatPrimaryRS.Rows[0]["VanneType"] = txtFields60.Text;
                DatPrimaryRS.Rows[0]["PreparateurMarque"]=Combo6.Text;
                DatPrimaryRS.Rows[0]["PreparateurType"] = txtFields44.Text;
                DatPrimaryRS.Rows[0]["PreparateurNbre"] = Convert.ToInt16(General.nz(txtFields43.Text, 0));
                DatPrimaryRS.Rows[0]["PreparateurCapacite"] = Text6.Text;
                DatPrimaryRS.Rows[0]["ExpansionType"] = txtFields87.Text;
                 DatPrimaryRS.Rows[0]["ExpansionMarque"]=Combo8.Text;
               DatPrimaryRS.Rows[0]["ExpansionCapacite"]= txtFields70.Text;
                DatPrimaryRS.Rows[0]["CptGazMarque"]=Combo100.Text;
                DatPrimaryRS.Rows[0]["CptGazType"] = txtFields15.Text;
                DatPrimaryRS.Rows[0]["CptGazNum"] =txtFields0.Text;
                DatPrimaryRS.Rows[0]["ByPass"]= (Check6.Checked ? "True" : "False"); 
                DatPrimaryRS.Rows[0]["CleBarrage"]= (Check213.Checked ? "True" : "False"); 
               DatPrimaryRS.Rows[0]["CleExt"] = (Check212.Checked ? "True" : "False"); 
              DatPrimaryRS.Rows[0]["Observation"]  =Text1.Text;

              
                ModAdo2.Update();
            }

            else
            {

                string reqUpdate = "update MaterielECS set  ChaudiereMarque1 ='" + Combo10.Text + "'," +
               "ChaudiereType1 ='" + txtFields28.Text + "'," +
               "ChaudierePuissance1 ='" + txtFields48.Text + "'," +
               "ChaudiereNbre1 =" + Convert.ToInt16(General.nz(txtFields6.Text, 0)) + "," +
               "ChaudiereFluide1 =" + Convert.ToInt16(General.nz(txtFields16.Text, 0)) + "," +
               "ChaudiereTypeBriquetage1 =" + Convert.ToInt16(General.nz(txtFields32.Text, 0)) + "," +
               "ChaudiereEtatBriquetage1 =" + Convert.ToInt16(General.nz(txtFields13.Text, 0)) + "," +
               "BruleurMarque1 ='" + Combo20.Text + "'," +
               "BruleurType1 ='" + txtFields5.Text + "'," +
               "BruleurNbre1 =" + Convert.ToInt16(General.nz(txtFields7.Text, 0)) + "," +
               "BruleurSystem1 =" + Convert.ToInt16(General.nz(txtFields9.Text, 0)) + "," +
               "BruleurMarqueBoite1 ='" + Text3.Text + "'," +
               "BruleurTypeBoite1 ='" + Text4.Text + "'," +
               "BruleurDoubleSecurite1 =" + (Check2.Checked ? 1 : 0) + "," +
               "ChemineeHauteur1 =" + Convert.ToDouble(General.nz(txtFields10.Text, 0)) + "," +
               "ChemineeSection1 ='" + txtFields1.Text + "'," +
               "ChemineeRegulateur1 =" + (Check9.Checked ? 1 : 0) + "," +
               "ChemineeLongueur1 ='" + txtFields12.Text + "'," +
               "ChemineeDiam1 ='" + txtFields11.Text + "'," +
               "CarneauLongueur1 ='" + txtFields19.Text + "'," +
               "CarneauDiam1 ='" + txtFields18.Text + "'," +
               "ChIdemChauff =" + (Check5.Checked ? 1 : 0) + "," +
               "PompeCHMarque1 ='" + Combo3.Text + "'," +
               "PompeCHType1 ='" + txtFields38.Text + "'," +
               "PompeCHNbre1 ='" + txtFields39.Text + "'," +
               "PompeCHVanne1 =" + (Check13.Checked ? 1 : 0) + "," +
               "PompeRECHMarque ='" + Combo4.Text + "'," +
               "PompeRECHType ='" + txtFields57.Text + "'," +
               "PompeRECHNbre =" + Convert.ToInt16(General.nz(txtFields56.Text, 0)) + "," +
               "PompeRECHAsser =" + (Check20.Checked ? 1 : 0) + "," +
               "PompeRECHVanne =" + (Check1.Checked ? 1 : 0) + "," +
               "PompeREINMarque ='" + Combo9.Text + "'," +
               "PompeREINType ='" + txtFields2.Text + "'," +
               "PompeREINNbre =" + Convert.ToInt16(General.nz(txtFields3.Text, 0)) + "," +
               "PompeREINVanne =" + (Check3.Checked ? 1 : 0) + "," +
               "BallonMarque ='" + Combo5.Text + "'," +
               "Ballontype ='" + txtFields41.Text + "'," +
               "BallonCapacite ='" + txtFields40.Text + "'," +
               "BallonNbre =" + Convert.ToInt16(General.nz(Text5.Text, 0)) + "," +
               "BallonTraditionnel =" + (Check14.Checked ? 1 : 0) + "," +
               "BallonChasse =" + (Check16.Checked ? 1 : 0) + "," +
               "PompeREBAMarque ='" + Combo11.Text + "'," +
               "PompeREBAType ='" + Text7.Text + "'," +
               "PompeREBANbre =" + Convert.ToInt16(General.nz(Text8.Text, 0)) + "," +
               "PompeREBAVanne =" + (Check4.Checked ? 1 : 0) + "," +
               "BallonTamponMarque ='" + Combo12.Text + "'," +
               "BallonTamponCapacite ='" + Text9.Text + "'," +
               "RegulationMarque ='" + Combo7.Text + "'," +
               "RegulationNbre =" + Convert.ToInt16(General.nz(Text2.Text, 0)) + "," +
               "RegulationActionBruleur =" + (Check19.Checked ? 1 : 0) + "," +
               "RegulationActionVanne3 =" + (Check210.Checked ? 1 : 0) + "," +
               "RegulationActionVanne4 =" + (Check211.Checked ? 1 : 0) + "," +
               "RegulationType ='" + txtFields64.Text + "'," +
               "VanneMarque ='" + txtFields61.Text + "'," +
               "VanneASecteur =" + Convert.ToInt16(General.nz(txtFields4.Text, 0)) + "," +
               "VanneType ='" + txtFields60.Text + "'," +
               "PreparateurMarque ='" + Combo6.Text + "'," +
               "PreparateurType ='" + txtFields44.Text + "'," +
               "PreparateurNbre =" + Convert.ToInt16(General.nz(txtFields43.Text, 0)) + "," +
                "ExpansionType='" + txtFields87.Text + "'," +
                "ExpansionMarque='" + Combo8.Text + "'," +
                "ExpansionCapacite='" + txtFields70.Text + "'," +
                "CptGazMarque='" + Combo100.Text + "'," +
                "CptGazType='" + txtFields15.Text + "'," +
                 "CptGazNum='" + txtFields0.Text + "'," +
               "ByPass=" + (Check6.Checked ? 1 : 0) + "," +
                "CleBarrage=" + (Check213.Checked ? 1 : 0) + "," +
                "CleExt=" + (Check212.Checked ? 1 : 0) + "," +
                "Observation ='" + Text1.Text + "'," +
                "PreparateurCapacite ='" + Text6.Text + "'  WHERE CodeImmeuble='" + General.gVar + "'";
                int xx = General.Execute(reqUpdate);
            }
               

            General.SQL = "update TechReleve set RelECS=" + -1 + " WHERE CodeImmeuble='" + General.gVar + "'";
            int yy=General.Execute(General.SQL);

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="txtTest"></param>
        /// <returns></returns>
        private string InitField(string txtTest)
        {
            if (string.IsNullOrEmpty(txtTest) || !General.IsNumeric(txtTest))
            {
                txtTest = "0";
            }
            return txtTest;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option1_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option1.Checked)
            {
                txtFields16.Text = "0";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option10_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option10.Checked)
            {
                txtFields87.Text = "2";

            }
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option11_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option11.Checked)
            {
                txtFields13.Text = "3";

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option12_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option12.Checked)
            {
                txtFields16.Text = "2";

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkSecteur_CheckedChanged(object sender, EventArgs e)
        {
            // If txtFields(4) = "" Then txtFields(4) = "0"
            if (swchange == 1)
            {
                return;
            }

            if (txtFields4.Text == "0" || txtFields4.Text == "2")
            {
                txtFields4.Text = "-1";
            }
            else
            {
                txtFields4.Text = "2";
            }
        }
        /// <summary>
        /// TEsteed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void chkSoupape_CheckedChanged(object sender, EventArgs e)
        {
            // If txtFields(4) = "" Then txtFields(4) = "0"
            bool Recom = false;
       
            try
            {
                if (swchange == 1)
                {
                    return;
                }

                if (txtFields4.Text == "-1" || txtFields4.Text == "2")
                {
                    Recom = true;                   
                    txtFields4.Text = "0";
                    Recom = false;
                }
                else
                {
                    txtFields4.Text = "2";
                }
                return;
               
            }
            catch (Exception ex)
            {
                if (Recom == true)
                {                   
                    return;
                }
                else
                {
                    Erreurs.gFr_debug(ex, this.Name + " chkSoupape_Click ");
                }
            }
        }
      
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option2_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option2.Checked)
            {
                txtFields16.Text = "1";
            }
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option29_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option29.Checked)
            {
                txtFields9.Text = "1";

            }
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option3_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option3.Checked)
            {
                txtFields32.Text = "0";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option30_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option30.Checked)
            {
                txtFields9.Text = "0";

            }
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option37_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option37.Checked)
            {
                txtFields87.Text = "0";

            }
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option38_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option38.Checked)
            {
                txtFields87.Text = "1";

            }
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option4_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option4.Checked)
            {
                txtFields32.Text = "1";

            }
        }
        /// <summary>
        /// testeed
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option5_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option5.Checked)
            {
                txtFields13.Text = "0";

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option6_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option6.Checked)
            {
                txtFields13.Text = "1";

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option7_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option7.Checked)
            {
                txtFields13.Text = "2";

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option8_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option8.Checked)
            {
                txtFields9.Text = "2";

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option9_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option9.Checked)
            {
                //txtFields(49) = "1"
                txtFields32.Text = "2";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtFields_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //short Index = txtFields13.GetIndex(eventSender);

            var txtfields = eventSender as iTalk.iTalk_TextBox_Small2;
            Int16 Index = Convert.ToInt16(txtfields.Tag);

            switch (Index)
            {

                case 9:
                    //B1TB
                    switch (txtfields.Text)
                    {
                        case "0":
                            Option30.Checked = true;
                            break;
                        case "1":
                            Option29.Checked = true;
                            break;
                        case "2":
                            Option8.Checked = true;
                            break;
                    }
                    break;

                case 13:
                    //CH1EB
                    switch (txtfields.Text)
                    {
                        case "0":
                            Option5.Checked = true;
                            break;
                        case "1":
                            Option6.Checked = true;
                            break;
                        case "2":
                            Option7.Checked = true;
                            break;
                        case "3":
                            Option11.Checked = true;
                            break;
                    }
                    break;

                case 16:
                    //CH1EV
                    switch (txtfields.Text)
                    {
                        case "0":
                            Option1.Checked = true;
                            break;
                        case "1":
                            Option2.Checked = true;
                            break;
                        case "2":
                            Option12.Checked = true;
                            break;
                    }
                    break;

                case 32:
                    //CH1TB
                    switch (txtfields.Text)
                    {
                        case "0":
                            Option3.Checked = true;
                            break;
                        case "1":
                            Option4.Checked = true;
                            break;
                        case "2":
                            Option9.Checked = true;
                            break;
                    }
                    break;

                case 49:
                    //CH2ET
                    switch (txtfields.Text)
                    {
                        case "0":
                            Option10.Checked = true;
                            break;
                        case "1":
                            Option9.Checked = true;
                            break;
                        case "2":
                            Option8.Checked = true;
                            break;
                    }
                    break;

                case 50:
                    //CH2EB
                    switch (txtfields.Text)
                    {
                        case "0":
                            Option12.Checked = true;
                            break;
                        case "1":
                            Option11.Checked = true;
                            break;
                    }
                    break;

                case 4:
                    //CH2EV
                    swchange = 1;
                    switch (txtfields.Text)
                    {
                        case "0":
                            chkSecteur.CheckState = System.Windows.Forms.CheckState.Unchecked;
                            chkSoupape.CheckState = System.Windows.Forms.CheckState.Checked;
                            break;
                        case "-1":
                            chkSecteur.CheckState = System.Windows.Forms.CheckState.Checked;
                            chkSoupape.CheckState = System.Windows.Forms.CheckState.Unchecked;
                            break;
                        case "2":
                            chkSecteur.CheckState = System.Windows.Forms.CheckState.Unchecked;
                            chkSoupape.CheckState = System.Windows.Forms.CheckState.Unchecked;
                            break;
                    }
                    swchange = 0;
                    break;

                case 87:
                    //expansion
                    switch (txtfields.Text)
                    {
                        case "0":
                            Option37.Checked = true;
                            break;
                        case "1":
                            Option38.Checked = true;
                            break;
                        case "2":
                            Option10.Checked = true;
                            break;
                    }
                    break;

            }
        }
    }
}
