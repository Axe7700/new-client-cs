﻿namespace Axe_interDT.Views.CompteRendu
{
    partial class frmReleveConf
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab13 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab1 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab2 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            Infragistics.Win.UltraWinTabControl.UltraTab ultraTab3 = new Infragistics.Win.UltraWinTabControl.UltraTab();
            this.ultraTabPageControl1 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.Frame4 = new System.Windows.Forms.GroupBox();
            this.Text1 = new iTalk.iTalk_TextBox_Small2();
            this.Frame3 = new System.Windows.Forms.GroupBox();
            this.Option10 = new System.Windows.Forms.RadioButton();
            this.Option9 = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.Check143 = new System.Windows.Forms.CheckBox();
            this.Check123 = new System.Windows.Forms.CheckBox();
            this.Check153 = new System.Windows.Forms.CheckBox();
            this.Check5 = new System.Windows.Forms.CheckBox();
            this.Frame6 = new System.Windows.Forms.GroupBox();
            this.Text6 = new iTalk.iTalk_TextBox_Small2();
            this.Option13 = new System.Windows.Forms.RadioButton();
            this.Option14 = new System.Windows.Forms.RadioButton();
            this.Option23 = new System.Windows.Forms.RadioButton();
            this.Frame5 = new System.Windows.Forms.GroupBox();
            this.Text5 = new iTalk.iTalk_TextBox_Small2();
            this.Option11 = new System.Windows.Forms.RadioButton();
            this.Option12 = new System.Windows.Forms.RadioButton();
            this.Option19 = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.Check113 = new System.Windows.Forms.CheckBox();
            this.Check114 = new System.Windows.Forms.CheckBox();
            this.Check115 = new System.Windows.Forms.CheckBox();
            this.Check116 = new System.Windows.Forms.CheckBox();
            this.Check117 = new System.Windows.Forms.CheckBox();
            this.Check118 = new System.Windows.Forms.CheckBox();
            this.Check119 = new System.Windows.Forms.CheckBox();
            this.Check120 = new System.Windows.Forms.CheckBox();
            this.Check4 = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Check2 = new System.Windows.Forms.CheckBox();
            this.Check10 = new System.Windows.Forms.CheckBox();
            this.Check11 = new System.Windows.Forms.CheckBox();
            this.Check12 = new System.Windows.Forms.CheckBox();
            this.Check13 = new System.Windows.Forms.CheckBox();
            this.Check14 = new System.Windows.Forms.CheckBox();
            this.Check15 = new System.Windows.Forms.CheckBox();
            this.Check16 = new System.Windows.Forms.CheckBox();
            this.Check17 = new System.Windows.Forms.CheckBox();
            this.Check18 = new System.Windows.Forms.CheckBox();
            this.Check19 = new System.Windows.Forms.CheckBox();
            this.Check110 = new System.Windows.Forms.CheckBox();
            this.Check111 = new System.Windows.Forms.CheckBox();
            this.Check112 = new System.Windows.Forms.CheckBox();
            this.Check3 = new System.Windows.Forms.CheckBox();
            this.Frame10 = new System.Windows.Forms.GroupBox();
            this.Text9 = new iTalk.iTalk_RichTextBox();
            this.ultraTabPageControl2 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.Frame2 = new System.Windows.Forms.GroupBox();
            this.Option22 = new System.Windows.Forms.RadioButton();
            this.Option7 = new System.Windows.Forms.RadioButton();
            this.Option8 = new System.Windows.Forms.RadioButton();
            this.Option6 = new System.Windows.Forms.RadioButton();
            this.Option5 = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.Check148 = new System.Windows.Forms.CheckBox();
            this.Check145 = new System.Windows.Forms.CheckBox();
            this.Check135 = new System.Windows.Forms.CheckBox();
            this.Check147 = new System.Windows.Forms.CheckBox();
            this.Check146 = new System.Windows.Forms.CheckBox();
            this.Check129 = new System.Windows.Forms.CheckBox();
            this.Check136 = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.Text4 = new iTalk.iTalk_TextBox_Small2();
            this.Check139 = new System.Windows.Forms.CheckBox();
            this.Check140 = new System.Windows.Forms.CheckBox();
            this.Check141 = new System.Windows.Forms.CheckBox();
            this.Check142 = new System.Windows.Forms.CheckBox();
            this.Check125 = new System.Windows.Forms.CheckBox();
            this.Check124 = new System.Windows.Forms.CheckBox();
            this.Check144 = new System.Windows.Forms.CheckBox();
            this.ultraTabPageControl3 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.Check149 = new System.Windows.Forms.CheckBox();
            this.Check150 = new System.Windows.Forms.CheckBox();
            this.Check151 = new System.Windows.Forms.CheckBox();
            this.Check152 = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.Check6 = new System.Windows.Forms.CheckBox();
            this.Check7 = new System.Windows.Forms.CheckBox();
            this.Check8 = new System.Windows.Forms.CheckBox();
            this.Check9 = new System.Windows.Forms.CheckBox();
            this.Check100 = new System.Windows.Forms.CheckBox();
            this.Check101 = new System.Windows.Forms.CheckBox();
            this.Check102 = new System.Windows.Forms.CheckBox();
            this.ultraTabPageControl4 = new Infragistics.Win.UltraWinTabControl.UltraTabPageControl();
            this.Text7 = new iTalk.iTalk_TextBox_Small2();
            this.Text8 = new iTalk.iTalk_TextBox_Small2();
            this.Text2 = new iTalk.iTalk_TextBox_Small2();
            this.Text10 = new iTalk.iTalk_TextBox_Small2();
            this.Check128 = new System.Windows.Forms.CheckBox();
            this.Check127 = new System.Windows.Forms.CheckBox();
            this.Check126 = new System.Windows.Forms.CheckBox();
            this.Frame7 = new System.Windows.Forms.GroupBox();
            this.Check137 = new System.Windows.Forms.CheckBox();
            this.Check122 = new System.Windows.Forms.CheckBox();
            this.Check121 = new System.Windows.Forms.CheckBox();
            this.Frame8 = new System.Windows.Forms.GroupBox();
            this.Option16 = new System.Windows.Forms.RadioButton();
            this.Option15 = new System.Windows.Forms.RadioButton();
            this.Option24 = new System.Windows.Forms.RadioButton();
            this.Frame9 = new System.Windows.Forms.GroupBox();
            this.Option17 = new System.Windows.Forms.RadioButton();
            this.Option18 = new System.Windows.Forms.RadioButton();
            this.Option21 = new System.Windows.Forms.RadioButton();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.Option20 = new System.Windows.Forms.RadioButton();
            this.Option3 = new System.Windows.Forms.RadioButton();
            this.Option4 = new System.Windows.Forms.RadioButton();
            this.Option2 = new System.Windows.Forms.RadioButton();
            this.Option1 = new System.Windows.Forms.RadioButton();
            this.Frame11 = new System.Windows.Forms.GroupBox();
            this.Option26 = new System.Windows.Forms.RadioButton();
            this.Option25 = new System.Windows.Forms.RadioButton();
            this.SSTab1 = new Infragistics.Win.UltraWinTabControl.UltraTabControl();
            this.ultraTabSharedControlsPage1 = new Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage();
            this.Text3 = new iTalk.iTalk_TextBox_Small2();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.cmdMAJ = new System.Windows.Forms.Button();
            this.ConfQuit = new System.Windows.Forms.Button();
            this.ultraTabPageControl1.SuspendLayout();
            this.Frame4.SuspendLayout();
            this.Frame3.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.Frame6.SuspendLayout();
            this.Frame5.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.Frame10.SuspendLayout();
            this.ultraTabPageControl2.SuspendLayout();
            this.Frame2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.ultraTabPageControl3.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.ultraTabPageControl4.SuspendLayout();
            this.Frame7.SuspendLayout();
            this.Frame8.SuspendLayout();
            this.Frame9.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.Frame11.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).BeginInit();
            this.SSTab1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraTabPageControl1
            // 
            this.ultraTabPageControl1.Controls.Add(this.Frame4);
            this.ultraTabPageControl1.Controls.Add(this.tableLayoutPanel2);
            this.ultraTabPageControl1.Controls.Add(this.tableLayoutPanel1);
            this.ultraTabPageControl1.Controls.Add(this.Frame10);
            this.ultraTabPageControl1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTabPageControl1.Location = new System.Drawing.Point(1, 23);
            this.ultraTabPageControl1.Name = "ultraTabPageControl1";
            this.ultraTabPageControl1.Size = new System.Drawing.Size(769, 509);
            // 
            // Frame4
            // 
            this.Frame4.BackColor = System.Drawing.Color.Transparent;
            this.Frame4.Controls.Add(this.Text1);
            this.Frame4.Controls.Add(this.Frame3);
            this.Frame4.Controls.Add(this.tableLayoutPanel3);
            this.Frame4.Controls.Add(this.Frame6);
            this.Frame4.Controls.Add(this.Frame5);
            this.Frame4.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame4.Location = new System.Drawing.Point(353, 224);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(416, 214);
            this.Frame4.TabIndex = 415;
            this.Frame4.TabStop = false;
            // 
            // Text1
            // 
            this.Text1.AccAcceptNumbersOnly = false;
            this.Text1.AccAllowComma = false;
            this.Text1.AccBackgroundColor = System.Drawing.Color.White;
            this.Text1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text1.AccHidenValue = "";
            this.Text1.AccNotAllowedChars = null;
            this.Text1.AccReadOnly = false;
            this.Text1.AccReadOnlyAllowDelete = false;
            this.Text1.AccRequired = false;
            this.Text1.BackColor = System.Drawing.Color.White;
            this.Text1.CustomBackColor = System.Drawing.Color.White;
            this.Text1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text1.ForeColor = System.Drawing.Color.Black;
            this.Text1.Location = new System.Drawing.Point(232, 186);
            this.Text1.Margin = new System.Windows.Forms.Padding(2);
            this.Text1.MaxLength = 32767;
            this.Text1.Multiline = false;
            this.Text1.Name = "Text1";
            this.Text1.ReadOnly = false;
            this.Text1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text1.Size = new System.Drawing.Size(78, 27);
            this.Text1.TabIndex = 586;
            this.Text1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text1.UseSystemPasswordChar = false;
            this.Text1.TextChanged += new System.EventHandler(this.Text1_TextChanged);
            // 
            // Frame3
            // 
            this.Frame3.BackColor = System.Drawing.Color.Transparent;
            this.Frame3.Controls.Add(this.Option10);
            this.Frame3.Controls.Add(this.Option9);
            this.Frame3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame3.Location = new System.Drawing.Point(6, 167);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(201, 53);
            this.Frame3.TabIndex = 585;
            this.Frame3.TabStop = false;
            this.Frame3.Text = "Température";
            // 
            // Option10
            // 
            this.Option10.AutoSize = true;
            this.Option10.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option10.Location = new System.Drawing.Point(100, 21);
            this.Option10.Name = "Option10";
            this.Option10.Size = new System.Drawing.Size(74, 23);
            this.Option10.TabIndex = 539;
            this.Option10.TabStop = true;
            this.Option10.Text = "Elevée";
            this.Option10.UseVisualStyleBackColor = true;
            this.Option10.CheckedChanged += new System.EventHandler(this.Option10_CheckedChanged);
            // 
            // Option9
            // 
            this.Option9.AutoSize = true;
            this.Option9.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option9.Location = new System.Drawing.Point(5, 21);
            this.Option9.Name = "Option9";
            this.Option9.Size = new System.Drawing.Size(87, 23);
            this.Option9.TabIndex = 538;
            this.Option9.TabStop = true;
            this.Option9.Text = "Normale";
            this.Option9.UseVisualStyleBackColor = true;
            this.Option9.CheckedChanged += new System.EventHandler(this.Option9_CheckedChanged);
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 2;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.62406F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.37594F));
            this.tableLayoutPanel3.Controls.Add(this.Check143, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.Check123, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.Check153, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.Check5, 1, 1);
            this.tableLayoutPanel3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel3.Location = new System.Drawing.Point(8, 111);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 2;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(399, 55);
            this.tableLayoutPanel3.TabIndex = 584;
            // 
            // Check143
            // 
            this.Check143.AutoSize = true;
            this.Check143.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check143.Location = new System.Drawing.Point(201, 3);
            this.Check143.Name = "Check143";
            this.Check143.Size = new System.Drawing.Size(195, 22);
            this.Check143.TabIndex = 584;
            this.Check143.Text = "A fermeture automatique";
            this.Check143.UseVisualStyleBackColor = true;
            // 
            // Check123
            // 
            this.Check123.AutoSize = true;
            this.Check123.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check123.Location = new System.Drawing.Point(3, 3);
            this.Check123.Name = "Check123";
            this.Check123.Size = new System.Drawing.Size(192, 22);
            this.Check123.TabIndex = 570;
            this.Check123.Text = "A ouverture automatique";
            this.Check123.UseVisualStyleBackColor = true;
            // 
            // Check153
            // 
            this.Check153.AutoSize = true;
            this.Check153.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check153.Location = new System.Drawing.Point(3, 31);
            this.Check153.Name = "Check153";
            this.Check153.Size = new System.Drawing.Size(124, 22);
            this.Check153.TabIndex = 571;
            this.Check153.Text = "Seuil de porte";
            this.Check153.UseVisualStyleBackColor = true;
            // 
            // Check5
            // 
            this.Check5.AutoSize = true;
            this.Check5.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check5.Location = new System.Drawing.Point(201, 31);
            this.Check5.Name = "Check5";
            this.Check5.Size = new System.Drawing.Size(102, 22);
            this.Check5.TabIndex = 583;
            this.Check5.Text = "Ventilation";
            this.Check5.UseVisualStyleBackColor = true;
            // 
            // Frame6
            // 
            this.Frame6.BackColor = System.Drawing.Color.Transparent;
            this.Frame6.Controls.Add(this.Text6);
            this.Frame6.Controls.Add(this.Option13);
            this.Frame6.Controls.Add(this.Option14);
            this.Frame6.Controls.Add(this.Option23);
            this.Frame6.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame6.Location = new System.Drawing.Point(7, 59);
            this.Frame6.Name = "Frame6";
            this.Frame6.Size = new System.Drawing.Size(402, 49);
            this.Frame6.TabIndex = 417;
            this.Frame6.TabStop = false;
            // 
            // Text6
            // 
            this.Text6.AccAcceptNumbersOnly = false;
            this.Text6.AccAllowComma = false;
            this.Text6.AccBackgroundColor = System.Drawing.Color.White;
            this.Text6.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text6.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text6.AccHidenValue = "";
            this.Text6.AccNotAllowedChars = null;
            this.Text6.AccReadOnly = false;
            this.Text6.AccReadOnlyAllowDelete = false;
            this.Text6.AccRequired = false;
            this.Text6.BackColor = System.Drawing.Color.White;
            this.Text6.CustomBackColor = System.Drawing.Color.White;
            this.Text6.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text6.ForeColor = System.Drawing.Color.Black;
            this.Text6.Location = new System.Drawing.Point(366, 19);
            this.Text6.Margin = new System.Windows.Forms.Padding(2);
            this.Text6.MaxLength = 32767;
            this.Text6.Multiline = false;
            this.Text6.Name = "Text6";
            this.Text6.ReadOnly = false;
            this.Text6.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text6.Size = new System.Drawing.Size(42, 27);
            this.Text6.TabIndex = 541;
            this.Text6.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text6.UseSystemPasswordChar = false;
            this.Text6.TextChanged += new System.EventHandler(this.Text6_TextChanged);
            // 
            // Option13
            // 
            this.Option13.AutoSize = true;
            this.Option13.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option13.Location = new System.Drawing.Point(225, 14);
            this.Option13.Name = "Option13";
            this.Option13.Size = new System.Drawing.Size(206, 23);
            this.Option13.TabIndex = 540;
            this.Option13.TabStop = true;
            this.Option13.Text = "Porte coupe-feu 2 heures";
            this.Option13.UseVisualStyleBackColor = true;
            this.Option13.CheckedChanged += new System.EventHandler(this.Option13_CheckedChanged);
            // 
            // Option14
            // 
            this.Option14.AutoSize = true;
            this.Option14.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option14.Location = new System.Drawing.Point(56, 14);
            this.Option14.Name = "Option14";
            this.Option14.Size = new System.Drawing.Size(199, 23);
            this.Option14.TabIndex = 539;
            this.Option14.TabStop = true;
            this.Option14.Text = "Porte coupe-feu 1 heure";
            this.Option14.UseVisualStyleBackColor = true;
            this.Option14.CheckedChanged += new System.EventHandler(this.Option14_CheckedChanged);
            // 
            // Option23
            // 
            this.Option23.AutoSize = true;
            this.Option23.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option23.Location = new System.Drawing.Point(6, 14);
            this.Option23.Name = "Option23";
            this.Option23.Size = new System.Drawing.Size(53, 23);
            this.Option23.TabIndex = 538;
            this.Option23.TabStop = true;
            this.Option23.Text = "S/O";
            this.Option23.UseVisualStyleBackColor = true;
            this.Option23.CheckedChanged += new System.EventHandler(this.Option23_CheckedChanged);
            // 
            // Frame5
            // 
            this.Frame5.BackColor = System.Drawing.Color.Transparent;
            this.Frame5.Controls.Add(this.Text5);
            this.Frame5.Controls.Add(this.Option11);
            this.Frame5.Controls.Add(this.Option12);
            this.Frame5.Controls.Add(this.Option19);
            this.Frame5.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame5.Location = new System.Drawing.Point(8, 12);
            this.Frame5.Name = "Frame5";
            this.Frame5.Size = new System.Drawing.Size(402, 41);
            this.Frame5.TabIndex = 416;
            this.Frame5.TabStop = false;
            // 
            // Text5
            // 
            this.Text5.AccAcceptNumbersOnly = false;
            this.Text5.AccAllowComma = false;
            this.Text5.AccBackgroundColor = System.Drawing.Color.White;
            this.Text5.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text5.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text5.AccHidenValue = "";
            this.Text5.AccNotAllowedChars = null;
            this.Text5.AccReadOnly = false;
            this.Text5.AccReadOnlyAllowDelete = false;
            this.Text5.AccRequired = false;
            this.Text5.BackColor = System.Drawing.Color.White;
            this.Text5.CustomBackColor = System.Drawing.Color.White;
            this.Text5.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text5.ForeColor = System.Drawing.Color.Black;
            this.Text5.Location = new System.Drawing.Point(72, 14);
            this.Text5.Margin = new System.Windows.Forms.Padding(2);
            this.Text5.MaxLength = 32767;
            this.Text5.Multiline = false;
            this.Text5.Name = "Text5";
            this.Text5.ReadOnly = false;
            this.Text5.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text5.Size = new System.Drawing.Size(41, 27);
            this.Text5.TabIndex = 541;
            this.Text5.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text5.UseSystemPasswordChar = false;
            this.Text5.TextChanged += new System.EventHandler(this.Text5_TextChanged);
            // 
            // Option11
            // 
            this.Option11.AutoSize = true;
            this.Option11.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option11.Location = new System.Drawing.Point(130, 16);
            this.Option11.Name = "Option11";
            this.Option11.Size = new System.Drawing.Size(122, 23);
            this.Option11.TabIndex = 539;
            this.Option11.TabStop = true;
            this.Option11.Text = "SAS Porte FER";
            this.Option11.UseVisualStyleBackColor = true;
            this.Option11.CheckedChanged += new System.EventHandler(this.Option11_CheckedChanged);
            // 
            // Option12
            // 
            this.Option12.AutoSize = true;
            this.Option12.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option12.Location = new System.Drawing.Point(269, 15);
            this.Option12.Name = "Option12";
            this.Option12.Size = new System.Drawing.Size(130, 23);
            this.Option12.TabIndex = 540;
            this.Option12.TabStop = true;
            this.Option12.Text = "SAS Porte BOIS";
            this.Option12.UseVisualStyleBackColor = true;
            this.Option12.CheckedChanged += new System.EventHandler(this.Option12_CheckedChanged);
            // 
            // Option19
            // 
            this.Option19.AutoSize = true;
            this.Option19.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option19.Location = new System.Drawing.Point(6, 14);
            this.Option19.Name = "Option19";
            this.Option19.Size = new System.Drawing.Size(53, 23);
            this.Option19.TabIndex = 538;
            this.Option19.TabStop = true;
            this.Option19.Text = "S/O";
            this.Option19.UseVisualStyleBackColor = true;
            this.Option19.CheckedChanged += new System.EventHandler(this.Option19_CheckedChanged);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 2;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.55621F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.44379F));
            this.tableLayoutPanel2.Controls.Add(this.Check113, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.Check114, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.Check115, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.Check116, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.Check117, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.Check118, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.Check119, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.Check120, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.Check4, 1, 1);
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel2.Location = new System.Drawing.Point(375, 8);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 26F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 11F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(338, 222);
            this.tableLayoutPanel2.TabIndex = 414;
            // 
            // Check113
            // 
            this.Check113.AutoSize = true;
            this.Check113.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check113.Location = new System.Drawing.Point(3, 3);
            this.Check113.Name = "Check113";
            this.Check113.Size = new System.Drawing.Size(183, 22);
            this.Check113.TabIndex = 570;
            this.Check113.Text = "Etiquettes indicatrices";
            this.Check113.UseVisualStyleBackColor = true;
            // 
            // Check114
            // 
            this.Check114.AutoSize = true;
            this.Check114.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check114.Location = new System.Drawing.Point(3, 31);
            this.Check114.Name = "Check114";
            this.Check114.Size = new System.Drawing.Size(79, 22);
            this.Check114.TabIndex = 571;
            this.Check114.Text = "Puisard";
            this.Check114.UseVisualStyleBackColor = true;
            // 
            // Check115
            // 
            this.Check115.AutoSize = true;
            this.Check115.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check115.Location = new System.Drawing.Point(3, 59);
            this.Check115.Name = "Check115";
            this.Check115.Size = new System.Drawing.Size(159, 21);
            this.Check115.TabIndex = 572;
            this.Check115.Text = "Robinet de puisage";
            this.Check115.UseVisualStyleBackColor = true;
            // 
            // Check116
            // 
            this.Check116.AutoSize = true;
            this.Check116.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check116.Location = new System.Drawing.Point(3, 86);
            this.Check116.Name = "Check116";
            this.Check116.Size = new System.Drawing.Size(234, 21);
            this.Check116.TabIndex = 573;
            this.Check116.Text = "Bac de rétention sous brûleur";
            this.Check116.UseVisualStyleBackColor = true;
            // 
            // Check117
            // 
            this.Check117.AutoSize = true;
            this.Check117.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check117.Location = new System.Drawing.Point(3, 113);
            this.Check117.Name = "Check117";
            this.Check117.Size = new System.Drawing.Size(105, 20);
            this.Check117.TabIndex = 574;
            this.Check117.Text = "Bac à sable";
            this.Check117.UseVisualStyleBackColor = true;
            // 
            // Check118
            // 
            this.Check118.AutoSize = true;
            this.Check118.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check118.Location = new System.Drawing.Point(3, 139);
            this.Check118.Name = "Check118";
            this.Check118.Size = new System.Drawing.Size(63, 21);
            this.Check118.TabIndex = 575;
            this.Check118.Text = "Pelle";
            this.Check118.UseVisualStyleBackColor = true;
            // 
            // Check119
            // 
            this.Check119.AutoSize = true;
            this.Check119.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check119.Location = new System.Drawing.Point(3, 166);
            this.Check119.Name = "Check119";
            this.Check119.Size = new System.Drawing.Size(101, 20);
            this.Check119.TabIndex = 576;
            this.Check119.Text = "Extincteur";
            this.Check119.UseVisualStyleBackColor = true;
            // 
            // Check120
            // 
            this.Check120.AutoSize = true;
            this.Check120.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check120.Location = new System.Drawing.Point(3, 192);
            this.Check120.Name = "Check120";
            this.Check120.Size = new System.Drawing.Size(166, 23);
            this.Check120.TabIndex = 577;
            this.Check120.Text = "Plan de l\'installation";
            this.Check120.UseVisualStyleBackColor = true;
            // 
            // Check4
            // 
            this.Check4.AutoSize = true;
            this.Check4.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check4.Location = new System.Drawing.Point(255, 31);
            this.Check4.Name = "Check4";
            this.Check4.Size = new System.Drawing.Size(75, 22);
            this.Check4.TabIndex = 583;
            this.Check4.Text = "Siphon";
            this.Check4.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 68.34319F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 31.65681F));
            this.tableLayoutPanel1.Controls.Add(this.Check2, 0, 13);
            this.tableLayoutPanel1.Controls.Add(this.Check10, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.Check11, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.Check12, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.Check13, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.Check14, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.Check15, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.Check16, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.Check17, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.Check18, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.Check19, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.Check110, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.Check111, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.Check112, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.Check3, 1, 1);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel1.Location = new System.Drawing.Point(9, 9);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 14;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(338, 399);
            this.tableLayoutPanel1.TabIndex = 413;
            // 
            // Check2
            // 
            this.Check2.AutoSize = true;
            this.Check2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check2.Location = new System.Drawing.Point(3, 367);
            this.Check2.Name = "Check2";
            this.Check2.Size = new System.Drawing.Size(224, 23);
            this.Check2.TabIndex = 584;
            this.Check2.Text = "Commande extérieure coupure vapeur";
            this.Check2.UseVisualStyleBackColor = true;
            // 
            // Check10
            // 
            this.Check10.AutoSize = true;
            this.Check10.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check10.Location = new System.Drawing.Point(3, 3);
            this.Check10.Name = "Check10";
            this.Check10.Size = new System.Drawing.Size(224, 22);
            this.Check10.TabIndex = 570;
            this.Check10.Text = "Interrupteur hors chaufferie";
            this.Check10.UseVisualStyleBackColor = true;
            // 
            // Check11
            // 
            this.Check11.AutoSize = true;
            this.Check11.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check11.Location = new System.Drawing.Point(3, 31);
            this.Check11.Name = "Check11";
            this.Check11.Size = new System.Drawing.Size(180, 22);
            this.Check11.TabIndex = 571;
            this.Check11.Text = "Tableau en chaufferie";
            this.Check11.UseVisualStyleBackColor = true;
            // 
            // Check12
            // 
            this.Check12.AutoSize = true;
            this.Check12.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check12.Location = new System.Drawing.Point(3, 59);
            this.Check12.Name = "Check12";
            this.Check12.Size = new System.Drawing.Size(224, 22);
            this.Check12.TabIndex = 572;
            this.Check12.Text = "Régulation automatique chauffage";
            this.Check12.UseVisualStyleBackColor = true;
            // 
            // Check13
            // 
            this.Check13.AutoSize = true;
            this.Check13.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check13.Location = new System.Drawing.Point(3, 87);
            this.Check13.Name = "Check13";
            this.Check13.Size = new System.Drawing.Size(224, 22);
            this.Check13.TabIndex = 573;
            this.Check13.Text = "Régulation automatique E.C.S";
            this.Check13.UseVisualStyleBackColor = true;
            // 
            // Check14
            // 
            this.Check14.AutoSize = true;
            this.Check14.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check14.Location = new System.Drawing.Point(3, 115);
            this.Check14.Name = "Check14";
            this.Check14.Size = new System.Drawing.Size(224, 22);
            this.Check14.TabIndex = 574;
            this.Check14.Text = "Disconnecteur sur alimentation";
            this.Check14.UseVisualStyleBackColor = true;
            // 
            // Check15
            // 
            this.Check15.AutoSize = true;
            this.Check15.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check15.Location = new System.Drawing.Point(3, 143);
            this.Check15.Name = "Check15";
            this.Check15.Size = new System.Drawing.Size(224, 22);
            this.Check15.TabIndex = 575;
            this.Check15.Text = "Réglementation installation E.C.S";
            this.Check15.UseVisualStyleBackColor = true;
            // 
            // Check16
            // 
            this.Check16.AutoSize = true;
            this.Check16.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check16.Location = new System.Drawing.Point(3, 171);
            this.Check16.Name = "Check16";
            this.Check16.Size = new System.Drawing.Size(224, 22);
            this.Check16.TabIndex = 576;
            this.Check16.Text = "Calorifuge tuyauteries chauffage";
            this.Check16.UseVisualStyleBackColor = true;
            // 
            // Check17
            // 
            this.Check17.AutoSize = true;
            this.Check17.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check17.Location = new System.Drawing.Point(3, 199);
            this.Check17.Name = "Check17";
            this.Check17.Size = new System.Drawing.Size(221, 22);
            this.Check17.TabIndex = 577;
            this.Check17.Text = "Calorifuge tuyauteries E.C.S";
            this.Check17.UseVisualStyleBackColor = true;
            // 
            // Check18
            // 
            this.Check18.AutoSize = true;
            this.Check18.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check18.Location = new System.Drawing.Point(3, 227);
            this.Check18.Name = "Check18";
            this.Check18.Size = new System.Drawing.Size(224, 22);
            this.Check18.TabIndex = 578;
            this.Check18.Text = "Eclairage étanche chaufferie";
            this.Check18.UseVisualStyleBackColor = true;
            // 
            // Check19
            // 
            this.Check19.AutoSize = true;
            this.Check19.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check19.Location = new System.Drawing.Point(3, 255);
            this.Check19.Name = "Check19";
            this.Check19.Size = new System.Drawing.Size(124, 22);
            this.Check19.TabIndex = 579;
            this.Check19.Text = "Basse tension";
            this.Check19.UseVisualStyleBackColor = true;
            // 
            // Check110
            // 
            this.Check110.AutoSize = true;
            this.Check110.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check110.Location = new System.Drawing.Point(3, 283);
            this.Check110.Name = "Check110";
            this.Check110.Size = new System.Drawing.Size(157, 22);
            this.Check110.TabIndex = 580;
            this.Check110.Text = "Baladeuse 24 Volts";
            this.Check110.UseVisualStyleBackColor = true;
            // 
            // Check111
            // 
            this.Check111.AutoSize = true;
            this.Check111.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check111.Location = new System.Drawing.Point(3, 311);
            this.Check111.Name = "Check111";
            this.Check111.Size = new System.Drawing.Size(125, 22);
            this.Check111.TabIndex = 581;
            this.Check111.Text = "Mise à la terre";
            this.Check111.UseVisualStyleBackColor = true;
            // 
            // Check112
            // 
            this.Check112.AutoSize = true;
            this.Check112.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check112.Location = new System.Drawing.Point(3, 339);
            this.Check112.Name = "Check112";
            this.Check112.Size = new System.Drawing.Size(187, 22);
            this.Check112.TabIndex = 582;
            this.Check112.Text = "Signalisation à distance";
            this.Check112.UseVisualStyleBackColor = true;
            // 
            // Check3
            // 
            this.Check3.AutoSize = true;
            this.Check3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check3.Location = new System.Drawing.Point(233, 31);
            this.Check3.Name = "Check3";
            this.Check3.Size = new System.Drawing.Size(83, 22);
            this.Check3.TabIndex = 583;
            this.Check3.Text = "Armoire";
            this.Check3.UseVisualStyleBackColor = true;
            // 
            // Frame10
            // 
            this.Frame10.BackColor = System.Drawing.Color.Transparent;
            this.Frame10.Controls.Add(this.Text9);
            this.Frame10.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame10.Location = new System.Drawing.Point(1, 435);
            this.Frame10.Name = "Frame10";
            this.Frame10.Size = new System.Drawing.Size(768, 70);
            this.Frame10.TabIndex = 412;
            this.Frame10.TabStop = false;
            this.Frame10.Text = "Observations";
            // 
            // Text9
            // 
            this.Text9.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text9.AutoWordSelection = false;
            this.Text9.BackColor = System.Drawing.Color.Transparent;
            this.Text9.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text9.ForeColor = System.Drawing.Color.Black;
            this.Text9.Location = new System.Drawing.Point(8, 21);
            this.Text9.Name = "Text9";
            this.Text9.ReadOnly = false;
            this.Text9.Size = new System.Drawing.Size(756, 41);
            this.Text9.TabIndex = 569;
            this.Text9.WordWrap = true;
            // 
            // ultraTabPageControl2
            // 
            this.ultraTabPageControl2.Controls.Add(this.Frame2);
            this.ultraTabPageControl2.Controls.Add(this.tableLayoutPanel5);
            this.ultraTabPageControl2.Controls.Add(this.tableLayoutPanel4);
            this.ultraTabPageControl2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTabPageControl2.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl2.Name = "ultraTabPageControl2";
            this.ultraTabPageControl2.Size = new System.Drawing.Size(769, 509);
            // 
            // Frame2
            // 
            this.Frame2.BackColor = System.Drawing.Color.Transparent;
            this.Frame2.Controls.Add(this.Option22);
            this.Frame2.Controls.Add(this.Option7);
            this.Frame2.Controls.Add(this.Option8);
            this.Frame2.Controls.Add(this.Option6);
            this.Frame2.Controls.Add(this.Option5);
            this.Frame2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame2.Location = new System.Drawing.Point(62, 222);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(582, 67);
            this.Frame2.TabIndex = 417;
            this.Frame2.TabStop = false;
            this.Frame2.Text = "Etat de propreté";
            // 
            // Option22
            // 
            this.Option22.AutoSize = true;
            this.Option22.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option22.Location = new System.Drawing.Point(465, 31);
            this.Option22.Name = "Option22";
            this.Option22.Size = new System.Drawing.Size(53, 23);
            this.Option22.TabIndex = 542;
            this.Option22.TabStop = true;
            this.Option22.Text = "S/O";
            this.Option22.UseVisualStyleBackColor = true;
            this.Option22.CheckedChanged += new System.EventHandler(this.Option22_CheckedChanged);
            // 
            // Option7
            // 
            this.Option7.AutoSize = true;
            this.Option7.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option7.Location = new System.Drawing.Point(340, 31);
            this.Option7.Name = "Option7";
            this.Option7.Size = new System.Drawing.Size(75, 23);
            this.Option7.TabIndex = 541;
            this.Option7.TabStop = true;
            this.Option7.Text = "Propre";
            this.Option7.UseVisualStyleBackColor = true;
            this.Option7.CheckedChanged += new System.EventHandler(this.Option7_CheckedChanged);
            // 
            // Option8
            // 
            this.Option8.AutoSize = true;
            this.Option8.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option8.Location = new System.Drawing.Point(217, 31);
            this.Option8.Name = "Option8";
            this.Option8.Size = new System.Drawing.Size(74, 23);
            this.Option8.TabIndex = 540;
            this.Option8.TabStop = true;
            this.Option8.Text = "Moyen";
            this.Option8.UseVisualStyleBackColor = true;
            this.Option8.CheckedChanged += new System.EventHandler(this.Option8_CheckedChanged);
            // 
            // Option6
            // 
            this.Option6.AutoSize = true;
            this.Option6.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option6.Location = new System.Drawing.Point(110, 31);
            this.Option6.Name = "Option6";
            this.Option6.Size = new System.Drawing.Size(56, 23);
            this.Option6.TabIndex = 539;
            this.Option6.TabStop = true;
            this.Option6.Text = "Sale";
            this.Option6.UseVisualStyleBackColor = true;
            this.Option6.CheckedChanged += new System.EventHandler(this.Option6_CheckedChanged);
            // 
            // Option5
            // 
            this.Option5.AutoSize = true;
            this.Option5.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option5.Location = new System.Drawing.Point(3, 31);
            this.Option5.Name = "Option5";
            this.Option5.Size = new System.Drawing.Size(88, 23);
            this.Option5.TabIndex = 538;
            this.Option5.TabStop = true;
            this.Option5.Text = "Très sale";
            this.Option5.UseVisualStyleBackColor = true;
            this.Option5.CheckedChanged += new System.EventHandler(this.Option5_CheckedChanged);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 2;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.80695F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.19305F));
            this.tableLayoutPanel5.Controls.Add(this.Check148, 0, 3);
            this.tableLayoutPanel5.Controls.Add(this.Check145, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.Check135, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.Check147, 0, 2);
            this.tableLayoutPanel5.Controls.Add(this.Check146, 1, 1);
            this.tableLayoutPanel5.Controls.Add(this.Check129, 1, 0);
            this.tableLayoutPanel5.Controls.Add(this.Check136, 1, 2);
            this.tableLayoutPanel5.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel5.Location = new System.Drawing.Point(65, 347);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 4;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(518, 113);
            this.tableLayoutPanel5.TabIndex = 416;
            // 
            // Check148
            // 
            this.Check148.AutoSize = true;
            this.Check148.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check148.Location = new System.Drawing.Point(3, 87);
            this.Check148.Name = "Check148";
            this.Check148.Size = new System.Drawing.Size(212, 23);
            this.Check148.TabIndex = 585;
            this.Check148.Text = "Visible du point de livraison";
            this.Check148.UseVisualStyleBackColor = true;
            // 
            // Check145
            // 
            this.Check145.AutoSize = true;
            this.Check145.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check145.Location = new System.Drawing.Point(3, 3);
            this.Check145.Name = "Check145";
            this.Check145.Size = new System.Drawing.Size(95, 22);
            this.Check145.TabIndex = 570;
            this.Check145.Text = "Porte FER";
            this.Check145.UseVisualStyleBackColor = true;
            // 
            // Check135
            // 
            this.Check135.AutoSize = true;
            this.Check135.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check135.Location = new System.Drawing.Point(3, 31);
            this.Check135.Name = "Check135";
            this.Check135.Size = new System.Drawing.Size(163, 22);
            this.Check135.TabIndex = 571;
            this.Check135.Text = "Coupe-feu 1 heure ";
            this.Check135.UseVisualStyleBackColor = true;
            // 
            // Check147
            // 
            this.Check147.AutoSize = true;
            this.Check147.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check147.Location = new System.Drawing.Point(3, 59);
            this.Check147.Name = "Check147";
            this.Check147.Size = new System.Drawing.Size(202, 22);
            this.Check147.TabIndex = 572;
            this.Check147.Text = "A ouverture automatique";
            this.Check147.UseVisualStyleBackColor = true;
            // 
            // Check146
            // 
            this.Check146.AutoSize = true;
            this.Check146.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check146.Location = new System.Drawing.Point(260, 31);
            this.Check146.Name = "Check146";
            this.Check146.Size = new System.Drawing.Size(167, 22);
            this.Check146.TabIndex = 583;
            this.Check146.Text = "Coupe-feu 2 heures";
            this.Check146.UseVisualStyleBackColor = true;
            // 
            // Check129
            // 
            this.Check129.AutoSize = true;
            this.Check129.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check129.Location = new System.Drawing.Point(260, 3);
            this.Check129.Name = "Check129";
            this.Check129.Size = new System.Drawing.Size(103, 22);
            this.Check129.TabIndex = 573;
            this.Check129.Text = "Porte BOIS";
            this.Check129.UseVisualStyleBackColor = true;
            // 
            // Check136
            // 
            this.Check136.AutoSize = true;
            this.Check136.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check136.Location = new System.Drawing.Point(260, 59);
            this.Check136.Name = "Check136";
            this.Check136.Size = new System.Drawing.Size(205, 22);
            this.Check136.TabIndex = 584;
            this.Check136.Text = "A fermeture automatique";
            this.Check136.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 74.55621F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.44379F));
            this.tableLayoutPanel4.Controls.Add(this.Text4, 0, 6);
            this.tableLayoutPanel4.Controls.Add(this.Check139, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.Check140, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.Check141, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.Check142, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.Check125, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.Check124, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.Check144, 0, 6);
            this.tableLayoutPanel4.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel4.Location = new System.Drawing.Point(62, 18);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 7;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(416, 198);
            this.tableLayoutPanel4.TabIndex = 415;
            // 
            // Text4
            // 
            this.Text4.AccAcceptNumbersOnly = false;
            this.Text4.AccAllowComma = false;
            this.Text4.AccBackgroundColor = System.Drawing.Color.White;
            this.Text4.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text4.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text4.AccHidenValue = "";
            this.Text4.AccNotAllowedChars = null;
            this.Text4.AccReadOnly = false;
            this.Text4.AccReadOnlyAllowDelete = false;
            this.Text4.AccRequired = false;
            this.Text4.BackColor = System.Drawing.Color.White;
            this.Text4.CustomBackColor = System.Drawing.Color.White;
            this.Text4.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text4.ForeColor = System.Drawing.Color.Black;
            this.Text4.Location = new System.Drawing.Point(312, 170);
            this.Text4.Margin = new System.Windows.Forms.Padding(2);
            this.Text4.MaxLength = 32767;
            this.Text4.Multiline = false;
            this.Text4.Name = "Text4";
            this.Text4.ReadOnly = false;
            this.Text4.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text4.Size = new System.Drawing.Size(102, 27);
            this.Text4.TabIndex = 577;
            this.Text4.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text4.UseSystemPasswordChar = false;
            this.Text4.TextChanged += new System.EventHandler(this.Text4_TextChanged);
            // 
            // Check139
            // 
            this.Check139.AutoSize = true;
            this.Check139.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check139.Location = new System.Drawing.Point(3, 3);
            this.Check139.Name = "Check139";
            this.Check139.Size = new System.Drawing.Size(102, 22);
            this.Check139.TabIndex = 570;
            this.Check139.Text = "Ventilation";
            this.Check139.UseVisualStyleBackColor = true;
            // 
            // Check140
            // 
            this.Check140.AutoSize = true;
            this.Check140.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check140.Location = new System.Drawing.Point(3, 31);
            this.Check140.Name = "Check140";
            this.Check140.Size = new System.Drawing.Size(92, 22);
            this.Check140.TabIndex = 571;
            this.Check140.Text = "Eclairage";
            this.Check140.UseVisualStyleBackColor = true;
            // 
            // Check141
            // 
            this.Check141.AutoSize = true;
            this.Check141.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check141.Location = new System.Drawing.Point(3, 59);
            this.Check141.Name = "Check141";
            this.Check141.Size = new System.Drawing.Size(93, 22);
            this.Check141.TabIndex = 572;
            this.Check141.Text = "Cuvelage";
            this.Check141.UseVisualStyleBackColor = true;
            // 
            // Check142
            // 
            this.Check142.AutoSize = true;
            this.Check142.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check142.Location = new System.Drawing.Point(3, 87);
            this.Check142.Name = "Check142";
            this.Check142.Size = new System.Drawing.Size(122, 22);
            this.Check142.TabIndex = 573;
            this.Check142.Text = "Ancrage cuve";
            this.Check142.UseVisualStyleBackColor = true;
            // 
            // Check125
            // 
            this.Check125.AutoSize = true;
            this.Check125.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check125.Location = new System.Drawing.Point(3, 115);
            this.Check125.Name = "Check125";
            this.Check125.Size = new System.Drawing.Size(117, 22);
            this.Check125.TabIndex = 574;
            this.Check125.Text = "Vanne Police";
            this.Check125.UseVisualStyleBackColor = true;
            // 
            // Check124
            // 
            this.Check124.AutoSize = true;
            this.Check124.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check124.Location = new System.Drawing.Point(3, 143);
            this.Check124.Name = "Check124";
            this.Check124.Size = new System.Drawing.Size(181, 22);
            this.Check124.TabIndex = 575;
            this.Check124.Text = "Commande à distance";
            this.Check124.UseVisualStyleBackColor = true;
            // 
            // Check144
            // 
            this.Check144.AutoSize = true;
            this.Check144.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check144.Location = new System.Drawing.Point(3, 171);
            this.Check144.Name = "Check144";
            this.Check144.Size = new System.Drawing.Size(125, 23);
            this.Check144.TabIndex = 576;
            this.Check144.Text = "Mise à la terre";
            this.Check144.UseVisualStyleBackColor = true;
            // 
            // ultraTabPageControl3
            // 
            this.ultraTabPageControl3.Controls.Add(this.tableLayoutPanel7);
            this.ultraTabPageControl3.Controls.Add(this.tableLayoutPanel6);
            this.ultraTabPageControl3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTabPageControl3.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl3.Name = "ultraTabPageControl3";
            this.ultraTabPageControl3.Size = new System.Drawing.Size(769, 509);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 1;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Controls.Add(this.Check149, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.Check150, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.Check151, 0, 2);
            this.tableLayoutPanel7.Controls.Add(this.Check152, 0, 3);
            this.tableLayoutPanel7.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel7.Location = new System.Drawing.Point(35, 166);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 4;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(338, 116);
            this.tableLayoutPanel7.TabIndex = 416;
            // 
            // Check149
            // 
            this.Check149.AutoSize = true;
            this.Check149.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check149.Location = new System.Drawing.Point(3, 3);
            this.Check149.Name = "Check149";
            this.Check149.Size = new System.Drawing.Size(180, 22);
            this.Check149.TabIndex = 570;
            this.Check149.Text = "Dégazage Départ ECS";
            this.Check149.UseVisualStyleBackColor = true;
            // 
            // Check150
            // 
            this.Check150.AutoSize = true;
            this.Check150.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check150.Location = new System.Drawing.Point(3, 31);
            this.Check150.Name = "Check150";
            this.Check150.Size = new System.Drawing.Size(257, 22);
            this.Check150.TabIndex = 571;
            this.Check150.Text = "Dégazage Retour recyclage E.C.S";
            this.Check150.UseVisualStyleBackColor = true;
            // 
            // Check151
            // 
            this.Check151.AutoSize = true;
            this.Check151.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check151.Location = new System.Drawing.Point(3, 59);
            this.Check151.Name = "Check151";
            this.Check151.Size = new System.Drawing.Size(246, 22);
            this.Check151.TabIndex = 572;
            this.Check151.Text = "Manchettes témoin Départ ECS";
            this.Check151.UseVisualStyleBackColor = true;
            // 
            // Check152
            // 
            this.Check152.AutoSize = true;
            this.Check152.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check152.Location = new System.Drawing.Point(3, 87);
            this.Check152.Name = "Check152";
            this.Check152.Size = new System.Drawing.Size(252, 23);
            this.Check152.TabIndex = 573;
            this.Check152.Text = "Manchettes témoin Retour E.C.S";
            this.Check152.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 1;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel6.Controls.Add(this.Check6, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.Check7, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.Check8, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.Check9, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.Check100, 0, 4);
            this.tableLayoutPanel6.Controls.Add(this.Check101, 0, 5);
            this.tableLayoutPanel6.Controls.Add(this.Check102, 0, 6);
            this.tableLayoutPanel6.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.tableLayoutPanel6.Location = new System.Drawing.Point(410, 130);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 7;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 28F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(338, 202);
            this.tableLayoutPanel6.TabIndex = 415;
            // 
            // Check6
            // 
            this.Check6.AutoSize = true;
            this.Check6.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check6.Location = new System.Drawing.Point(3, 3);
            this.Check6.Name = "Check6";
            this.Check6.Size = new System.Drawing.Size(230, 22);
            this.Check6.TabIndex = 570;
            this.Check6.Text = "Pièces tournantes protégées";
            this.Check6.UseVisualStyleBackColor = true;
            // 
            // Check7
            // 
            this.Check7.AutoSize = true;
            this.Check7.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check7.Location = new System.Drawing.Point(3, 31);
            this.Check7.Name = "Check7";
            this.Check7.Size = new System.Drawing.Size(186, 22);
            this.Check7.TabIndex = 571;
            this.Check7.Text = "Etiquetage de sécurité";
            this.Check7.UseVisualStyleBackColor = true;
            // 
            // Check8
            // 
            this.Check8.AutoSize = true;
            this.Check8.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check8.Location = new System.Drawing.Point(3, 59);
            this.Check8.Name = "Check8";
            this.Check8.Size = new System.Drawing.Size(173, 22);
            this.Check8.TabIndex = 572;
            this.Check8.Text = "Eclairage de sécurité";
            this.Check8.UseVisualStyleBackColor = true;
            // 
            // Check9
            // 
            this.Check9.AutoSize = true;
            this.Check9.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check9.Location = new System.Drawing.Point(3, 87);
            this.Check9.Name = "Check9";
            this.Check9.Size = new System.Drawing.Size(241, 22);
            this.Check9.TabIndex = 573;
            this.Check9.Text = "Coupure extérieure électrique";
            this.Check9.UseVisualStyleBackColor = true;
            // 
            // Check100
            // 
            this.Check100.AutoSize = true;
            this.Check100.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check100.Location = new System.Drawing.Point(3, 115);
            this.Check100.Name = "Check100";
            this.Check100.Size = new System.Drawing.Size(251, 22);
            this.Check100.TabIndex = 574;
            this.Check100.Text = "Installation électrique intérieure";
            this.Check100.UseVisualStyleBackColor = true;
            // 
            // Check101
            // 
            this.Check101.AutoSize = true;
            this.Check101.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check101.Location = new System.Drawing.Point(3, 143);
            this.Check101.Name = "Check101";
            this.Check101.Size = new System.Drawing.Size(211, 22);
            this.Check101.TabIndex = 575;
            this.Check101.Text = "Calorifuge des tuyauteries";
            this.Check101.UseVisualStyleBackColor = true;
            // 
            // Check102
            // 
            this.Check102.AutoSize = true;
            this.Check102.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check102.Location = new System.Drawing.Point(3, 171);
            this.Check102.Name = "Check102";
            this.Check102.Size = new System.Drawing.Size(299, 23);
            this.Check102.TabIndex = 576;
            this.Check102.Text = "Tuyauteries d\'évacuation des soupapes";
            this.Check102.UseVisualStyleBackColor = true;
            // 
            // ultraTabPageControl4
            // 
            this.ultraTabPageControl4.Controls.Add(this.Text7);
            this.ultraTabPageControl4.Controls.Add(this.Text8);
            this.ultraTabPageControl4.Controls.Add(this.Text2);
            this.ultraTabPageControl4.Controls.Add(this.Text10);
            this.ultraTabPageControl4.Controls.Add(this.Check128);
            this.ultraTabPageControl4.Controls.Add(this.Check127);
            this.ultraTabPageControl4.Controls.Add(this.Check126);
            this.ultraTabPageControl4.Controls.Add(this.Frame7);
            this.ultraTabPageControl4.Controls.Add(this.Frame1);
            this.ultraTabPageControl4.Controls.Add(this.Frame11);
            this.ultraTabPageControl4.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTabPageControl4.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabPageControl4.Name = "ultraTabPageControl4";
            this.ultraTabPageControl4.Size = new System.Drawing.Size(769, 509);
            // 
            // Text7
            // 
            this.Text7.AccAcceptNumbersOnly = false;
            this.Text7.AccAllowComma = false;
            this.Text7.AccBackgroundColor = System.Drawing.Color.White;
            this.Text7.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text7.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text7.AccHidenValue = "";
            this.Text7.AccNotAllowedChars = null;
            this.Text7.AccReadOnly = false;
            this.Text7.AccReadOnlyAllowDelete = false;
            this.Text7.AccRequired = false;
            this.Text7.BackColor = System.Drawing.Color.White;
            this.Text7.CustomBackColor = System.Drawing.Color.White;
            this.Text7.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text7.ForeColor = System.Drawing.Color.Black;
            this.Text7.Location = new System.Drawing.Point(663, 326);
            this.Text7.Margin = new System.Windows.Forms.Padding(2);
            this.Text7.MaxLength = 32767;
            this.Text7.Multiline = false;
            this.Text7.Name = "Text7";
            this.Text7.ReadOnly = false;
            this.Text7.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text7.Size = new System.Drawing.Size(103, 27);
            this.Text7.TabIndex = 577;
            this.Text7.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text7.UseSystemPasswordChar = false;
            this.Text7.TextChanged += new System.EventHandler(this.Text7_TextChanged);
            // 
            // Text8
            // 
            this.Text8.AccAcceptNumbersOnly = false;
            this.Text8.AccAllowComma = false;
            this.Text8.AccBackgroundColor = System.Drawing.Color.White;
            this.Text8.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text8.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text8.AccHidenValue = "";
            this.Text8.AccNotAllowedChars = null;
            this.Text8.AccReadOnly = false;
            this.Text8.AccReadOnlyAllowDelete = false;
            this.Text8.AccRequired = false;
            this.Text8.BackColor = System.Drawing.Color.White;
            this.Text8.CustomBackColor = System.Drawing.Color.White;
            this.Text8.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text8.ForeColor = System.Drawing.Color.Black;
            this.Text8.Location = new System.Drawing.Point(661, 263);
            this.Text8.Margin = new System.Windows.Forms.Padding(2);
            this.Text8.MaxLength = 32767;
            this.Text8.Multiline = false;
            this.Text8.Name = "Text8";
            this.Text8.ReadOnly = false;
            this.Text8.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text8.Size = new System.Drawing.Size(103, 27);
            this.Text8.TabIndex = 576;
            this.Text8.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text8.UseSystemPasswordChar = false;
            this.Text8.TextChanged += new System.EventHandler(this.Text8_TextChanged);
            // 
            // Text2
            // 
            this.Text2.AccAcceptNumbersOnly = false;
            this.Text2.AccAllowComma = false;
            this.Text2.AccBackgroundColor = System.Drawing.Color.White;
            this.Text2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text2.AccHidenValue = "";
            this.Text2.AccNotAllowedChars = null;
            this.Text2.AccReadOnly = false;
            this.Text2.AccReadOnlyAllowDelete = false;
            this.Text2.AccRequired = false;
            this.Text2.BackColor = System.Drawing.Color.White;
            this.Text2.CustomBackColor = System.Drawing.Color.White;
            this.Text2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text2.ForeColor = System.Drawing.Color.Black;
            this.Text2.Location = new System.Drawing.Point(663, 199);
            this.Text2.Margin = new System.Windows.Forms.Padding(2);
            this.Text2.MaxLength = 32767;
            this.Text2.Multiline = false;
            this.Text2.Name = "Text2";
            this.Text2.ReadOnly = false;
            this.Text2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text2.Size = new System.Drawing.Size(105, 27);
            this.Text2.TabIndex = 575;
            this.Text2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text2.UseSystemPasswordChar = false;
            this.Text2.TextChanged += new System.EventHandler(this.Text2_TextChanged);
            // 
            // Text10
            // 
            this.Text10.AccAcceptNumbersOnly = false;
            this.Text10.AccAllowComma = false;
            this.Text10.AccBackgroundColor = System.Drawing.Color.White;
            this.Text10.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text10.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text10.AccHidenValue = "";
            this.Text10.AccNotAllowedChars = null;
            this.Text10.AccReadOnly = false;
            this.Text10.AccReadOnlyAllowDelete = false;
            this.Text10.AccRequired = false;
            this.Text10.BackColor = System.Drawing.Color.White;
            this.Text10.CustomBackColor = System.Drawing.Color.White;
            this.Text10.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text10.ForeColor = System.Drawing.Color.Black;
            this.Text10.Location = new System.Drawing.Point(665, 138);
            this.Text10.Margin = new System.Windows.Forms.Padding(2);
            this.Text10.MaxLength = 32767;
            this.Text10.Multiline = false;
            this.Text10.Name = "Text10";
            this.Text10.ReadOnly = false;
            this.Text10.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text10.Size = new System.Drawing.Size(105, 27);
            this.Text10.TabIndex = 574;
            this.Text10.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text10.UseSystemPasswordChar = false;
            this.Text10.TextChanged += new System.EventHandler(this.Text10_TextChanged);
            // 
            // Check128
            // 
            this.Check128.AutoSize = true;
            this.Check128.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check128.Location = new System.Drawing.Point(78, 71);
            this.Check128.Name = "Check128";
            this.Check128.Size = new System.Drawing.Size(128, 23);
            this.Check128.TabIndex = 573;
            this.Check128.Text = "Gaine pompier";
            this.Check128.UseVisualStyleBackColor = true;
            // 
            // Check127
            // 
            this.Check127.AutoSize = true;
            this.Check127.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check127.Location = new System.Drawing.Point(78, 45);
            this.Check127.Name = "Check127";
            this.Check127.Size = new System.Drawing.Size(145, 23);
            this.Check127.TabIndex = 572;
            this.Check127.Text = "Ventilation basse";
            this.Check127.UseVisualStyleBackColor = true;
            // 
            // Check126
            // 
            this.Check126.AutoSize = true;
            this.Check126.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check126.Location = new System.Drawing.Point(78, 19);
            this.Check126.Name = "Check126";
            this.Check126.Size = new System.Drawing.Size(146, 23);
            this.Check126.TabIndex = 571;
            this.Check126.Text = "Ventilation haute";
            this.Check126.UseVisualStyleBackColor = true;
            // 
            // Frame7
            // 
            this.Frame7.BackColor = System.Drawing.Color.Transparent;
            this.Frame7.Controls.Add(this.Check137);
            this.Frame7.Controls.Add(this.Check122);
            this.Frame7.Controls.Add(this.Check121);
            this.Frame7.Controls.Add(this.Frame8);
            this.Frame7.Controls.Add(this.Frame9);
            this.Frame7.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame7.Location = new System.Drawing.Point(11, 227);
            this.Frame7.Name = "Frame7";
            this.Frame7.Size = new System.Drawing.Size(651, 281);
            this.Frame7.TabIndex = 413;
            this.Frame7.TabStop = false;
            // 
            // Check137
            // 
            this.Check137.AutoSize = true;
            this.Check137.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check137.Location = new System.Drawing.Point(385, 185);
            this.Check137.Name = "Check137";
            this.Check137.Size = new System.Drawing.Size(205, 23);
            this.Check137.TabIndex = 576;
            this.Check137.Text = "A fermeture automatique";
            this.Check137.UseVisualStyleBackColor = true;
            // 
            // Check122
            // 
            this.Check122.AutoSize = true;
            this.Check122.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check122.Location = new System.Drawing.Point(97, 221);
            this.Check122.Name = "Check122";
            this.Check122.Size = new System.Drawing.Size(124, 23);
            this.Check122.TabIndex = 575;
            this.Check122.Text = "Seuil de porte";
            this.Check122.UseVisualStyleBackColor = true;
            // 
            // Check121
            // 
            this.Check121.AutoSize = true;
            this.Check121.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Check121.Location = new System.Drawing.Point(97, 185);
            this.Check121.Name = "Check121";
            this.Check121.Size = new System.Drawing.Size(202, 23);
            this.Check121.TabIndex = 574;
            this.Check121.Text = "A ouverture automatique";
            this.Check121.UseVisualStyleBackColor = true;
            // 
            // Frame8
            // 
            this.Frame8.BackColor = System.Drawing.Color.Transparent;
            this.Frame8.Controls.Add(this.Option16);
            this.Frame8.Controls.Add(this.Option15);
            this.Frame8.Controls.Add(this.Option24);
            this.Frame8.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame8.Location = new System.Drawing.Point(6, 76);
            this.Frame8.Name = "Frame8";
            this.Frame8.Size = new System.Drawing.Size(639, 67);
            this.Frame8.TabIndex = 411;
            this.Frame8.TabStop = false;
            // 
            // Option16
            // 
            this.Option16.AutoSize = true;
            this.Option16.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option16.Location = new System.Drawing.Point(443, 23);
            this.Option16.Name = "Option16";
            this.Option16.Size = new System.Drawing.Size(206, 23);
            this.Option16.TabIndex = 545;
            this.Option16.TabStop = true;
            this.Option16.Text = "Porte coupe-feu 2 heures";
            this.Option16.UseVisualStyleBackColor = true;
            this.Option16.CheckedChanged += new System.EventHandler(this.Option16_CheckedChanged);
            // 
            // Option15
            // 
            this.Option15.AutoSize = true;
            this.Option15.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option15.Location = new System.Drawing.Point(234, 21);
            this.Option15.Name = "Option15";
            this.Option15.Size = new System.Drawing.Size(199, 23);
            this.Option15.TabIndex = 544;
            this.Option15.TabStop = true;
            this.Option15.Text = "Porte coupe-feu 1 heure";
            this.Option15.UseVisualStyleBackColor = true;
            this.Option15.CheckedChanged += new System.EventHandler(this.Option15_CheckedChanged);
            // 
            // Option24
            // 
            this.Option24.AutoSize = true;
            this.Option24.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option24.Location = new System.Drawing.Point(61, 21);
            this.Option24.Name = "Option24";
            this.Option24.Size = new System.Drawing.Size(53, 23);
            this.Option24.TabIndex = 543;
            this.Option24.TabStop = true;
            this.Option24.Text = "S/O";
            this.Option24.UseVisualStyleBackColor = true;
            this.Option24.CheckedChanged += new System.EventHandler(this.Option24_CheckedChanged);
            // 
            // Frame9
            // 
            this.Frame9.BackColor = System.Drawing.Color.Transparent;
            this.Frame9.Controls.Add(this.Option17);
            this.Frame9.Controls.Add(this.Option18);
            this.Frame9.Controls.Add(this.Option21);
            this.Frame9.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame9.Location = new System.Drawing.Point(6, 7);
            this.Frame9.Name = "Frame9";
            this.Frame9.Size = new System.Drawing.Size(639, 67);
            this.Frame9.TabIndex = 411;
            this.Frame9.TabStop = false;
            // 
            // Option17
            // 
            this.Option17.AutoSize = true;
            this.Option17.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option17.Location = new System.Drawing.Point(486, 31);
            this.Option17.Name = "Option17";
            this.Option17.Size = new System.Drawing.Size(102, 23);
            this.Option17.TabIndex = 545;
            this.Option17.TabStop = true;
            this.Option17.Text = "Porte BOIS";
            this.Option17.UseVisualStyleBackColor = true;
            this.Option17.CheckedChanged += new System.EventHandler(this.Option17_CheckedChanged);
            // 
            // Option18
            // 
            this.Option18.AutoSize = true;
            this.Option18.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option18.Location = new System.Drawing.Point(295, 31);
            this.Option18.Name = "Option18";
            this.Option18.Size = new System.Drawing.Size(94, 23);
            this.Option18.TabIndex = 544;
            this.Option18.TabStop = true;
            this.Option18.Text = "Porte FER";
            this.Option18.UseVisualStyleBackColor = true;
            this.Option18.CheckedChanged += new System.EventHandler(this.Option18_CheckedChanged);
            // 
            // Option21
            // 
            this.Option21.AutoSize = true;
            this.Option21.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option21.Location = new System.Drawing.Point(115, 31);
            this.Option21.Name = "Option21";
            this.Option21.Size = new System.Drawing.Size(53, 23);
            this.Option21.TabIndex = 543;
            this.Option21.TabStop = true;
            this.Option21.Text = "S/O";
            this.Option21.UseVisualStyleBackColor = true;
            this.Option21.CheckedChanged += new System.EventHandler(this.Option21_CheckedChanged);
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.Transparent;
            this.Frame1.Controls.Add(this.Option20);
            this.Frame1.Controls.Add(this.Option3);
            this.Frame1.Controls.Add(this.Option4);
            this.Frame1.Controls.Add(this.Option2);
            this.Frame1.Controls.Add(this.Option1);
            this.Frame1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame1.Location = new System.Drawing.Point(11, 166);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(651, 55);
            this.Frame1.TabIndex = 412;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "Etat de propreté";
            // 
            // Option20
            // 
            this.Option20.AutoSize = true;
            this.Option20.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option20.Location = new System.Drawing.Point(544, 17);
            this.Option20.Name = "Option20";
            this.Option20.Size = new System.Drawing.Size(53, 23);
            this.Option20.TabIndex = 542;
            this.Option20.TabStop = true;
            this.Option20.Text = "S/O";
            this.Option20.UseVisualStyleBackColor = true;
            this.Option20.CheckedChanged += new System.EventHandler(this.Option20_CheckedChanged);
            // 
            // Option3
            // 
            this.Option3.AutoSize = true;
            this.Option3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option3.Location = new System.Drawing.Point(421, 17);
            this.Option3.Name = "Option3";
            this.Option3.Size = new System.Drawing.Size(75, 23);
            this.Option3.TabIndex = 541;
            this.Option3.TabStop = true;
            this.Option3.Text = "Propre";
            this.Option3.UseVisualStyleBackColor = true;
            this.Option3.CheckedChanged += new System.EventHandler(this.Option3_CheckedChanged);
            // 
            // Option4
            // 
            this.Option4.AutoSize = true;
            this.Option4.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option4.Location = new System.Drawing.Point(292, 18);
            this.Option4.Name = "Option4";
            this.Option4.Size = new System.Drawing.Size(74, 23);
            this.Option4.TabIndex = 540;
            this.Option4.TabStop = true;
            this.Option4.Text = "Moyen";
            this.Option4.UseVisualStyleBackColor = true;
            this.Option4.CheckedChanged += new System.EventHandler(this.Option4_CheckedChanged);
            // 
            // Option2
            // 
            this.Option2.AutoSize = true;
            this.Option2.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option2.Location = new System.Drawing.Point(159, 19);
            this.Option2.Name = "Option2";
            this.Option2.Size = new System.Drawing.Size(56, 23);
            this.Option2.TabIndex = 539;
            this.Option2.TabStop = true;
            this.Option2.Text = "Sale";
            this.Option2.UseVisualStyleBackColor = true;
            this.Option2.CheckedChanged += new System.EventHandler(this.Option2_CheckedChanged);
            // 
            // Option1
            // 
            this.Option1.AutoSize = true;
            this.Option1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option1.Location = new System.Drawing.Point(39, 21);
            this.Option1.Name = "Option1";
            this.Option1.Size = new System.Drawing.Size(88, 23);
            this.Option1.TabIndex = 538;
            this.Option1.TabStop = true;
            this.Option1.Text = "Très sale";
            this.Option1.UseVisualStyleBackColor = true;
            this.Option1.CheckedChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // Frame11
            // 
            this.Frame11.BackColor = System.Drawing.Color.Transparent;
            this.Frame11.Controls.Add(this.Option26);
            this.Frame11.Controls.Add(this.Option25);
            this.Frame11.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Frame11.Location = new System.Drawing.Point(11, 103);
            this.Frame11.Name = "Frame11";
            this.Frame11.Size = new System.Drawing.Size(651, 57);
            this.Frame11.TabIndex = 411;
            this.Frame11.TabStop = false;
            this.Frame11.Text = "Espace autour des chaudières";
            // 
            // Option26
            // 
            this.Option26.AutoSize = true;
            this.Option26.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option26.Location = new System.Drawing.Point(355, 21);
            this.Option26.Name = "Option26";
            this.Option26.Size = new System.Drawing.Size(67, 23);
            this.Option26.TabIndex = 539;
            this.Option26.TabStop = true;
            this.Option26.Text = "Faible";
            this.Option26.UseVisualStyleBackColor = true;
            this.Option26.CheckedChanged += new System.EventHandler(this.Option26_CheckedChanged);
            // 
            // Option25
            // 
            this.Option25.AutoSize = true;
            this.Option25.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Option25.Location = new System.Drawing.Point(159, 21);
            this.Option25.Name = "Option25";
            this.Option25.Size = new System.Drawing.Size(55, 23);
            this.Option25.TabIndex = 538;
            this.Option25.TabStop = true;
            this.Option25.Text = "Bon";
            this.Option25.UseVisualStyleBackColor = true;
            this.Option25.CheckedChanged += new System.EventHandler(this.Option25_CheckedChanged);
            // 
            // SSTab1
            // 
            this.SSTab1.Anchor = System.Windows.Forms.AnchorStyles.None;
            this.SSTab1.Controls.Add(this.ultraTabSharedControlsPage1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl1);
            this.SSTab1.Controls.Add(this.ultraTabPageControl2);
            this.SSTab1.Controls.Add(this.ultraTabPageControl3);
            this.SSTab1.Controls.Add(this.ultraTabPageControl4);
            this.SSTab1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SSTab1.Location = new System.Drawing.Point(0, 0);
            this.SSTab1.Name = "SSTab1";
            this.SSTab1.SharedControlsPage = this.ultraTabSharedControlsPage1;
            this.SSTab1.Size = new System.Drawing.Size(771, 533);
            this.SSTab1.Style = Infragistics.Win.UltraWinTabControl.UltraTabControlStyle.Excel;
            this.SSTab1.TabIndex = 410;
            ultraTab13.TabPage = this.ultraTabPageControl1;
            ultraTab13.Text = "Conformité Générale";
            ultraTab1.TabPage = this.ultraTabPageControl2;
            ultraTab1.Text = "Conformité Soute";
            ultraTab2.TabPage = this.ultraTabPageControl3;
            ultraTab2.Text = "Conformité Installation et Protection du Personnel";
            ultraTab3.TabPage = this.ultraTabPageControl4;
            ultraTab3.Text = "Conformité Chaufferie";
            this.SSTab1.Tabs.AddRange(new Infragistics.Win.UltraWinTabControl.UltraTab[] {
            ultraTab13,
            ultraTab1,
            ultraTab2,
            ultraTab3});
            // 
            // ultraTabSharedControlsPage1
            // 
            this.ultraTabSharedControlsPage1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ultraTabSharedControlsPage1.Location = new System.Drawing.Point(-10000, -10000);
            this.ultraTabSharedControlsPage1.Name = "ultraTabSharedControlsPage1";
            this.ultraTabSharedControlsPage1.Size = new System.Drawing.Size(769, 509);
            // 
            // Text3
            // 
            this.Text3.AccAcceptNumbersOnly = false;
            this.Text3.AccAllowComma = false;
            this.Text3.AccBackgroundColor = System.Drawing.Color.White;
            this.Text3.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Text3.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Text3.AccHidenValue = "";
            this.Text3.AccNotAllowedChars = null;
            this.Text3.AccReadOnly = false;
            this.Text3.AccReadOnlyAllowDelete = false;
            this.Text3.AccRequired = false;
            this.Text3.BackColor = System.Drawing.Color.White;
            this.Text3.CustomBackColor = System.Drawing.Color.White;
            this.Text3.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Text3.ForeColor = System.Drawing.Color.Black;
            this.Text3.Location = new System.Drawing.Point(63, 543);
            this.Text3.Margin = new System.Windows.Forms.Padding(2);
            this.Text3.MaxLength = 32767;
            this.Text3.Multiline = false;
            this.Text3.Name = "Text3";
            this.Text3.ReadOnly = false;
            this.Text3.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Text3.Size = new System.Drawing.Size(78, 27);
            this.Text3.TabIndex = 587;
            this.Text3.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Text3.UseSystemPasswordChar = false;
            this.Text3.Visible = false;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.cmdAjouter);
            this.flowLayoutPanel1.Controls.Add(this.cmdMAJ);
            this.flowLayoutPanel1.Controls.Add(this.ConfQuit);
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.flowLayoutPanel1.Location = new System.Drawing.Point(230, 537);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(380, 41);
            this.flowLayoutPanel1.TabIndex = 588;
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAjouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAjouter.ForeColor = System.Drawing.Color.White;
            this.cmdAjouter.Image = global::Axe_interDT.Properties.Resources.Add_File_16x16;
            this.cmdAjouter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAjouter.Location = new System.Drawing.Point(2, 2);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(122, 35);
            this.cmdAjouter.TabIndex = 569;
            this.cmdAjouter.Text = "     Ajouter";
            this.cmdAjouter.UseVisualStyleBackColor = false;
            this.cmdAjouter.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // cmdMAJ
            // 
            this.cmdMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJ.FlatAppearance.BorderSize = 0;
            this.cmdMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJ.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMAJ.ForeColor = System.Drawing.Color.White;
            this.cmdMAJ.Image = global::Axe_interDT.Properties.Resources.Ok_16x16;
            this.cmdMAJ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdMAJ.Location = new System.Drawing.Point(128, 2);
            this.cmdMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJ.Name = "cmdMAJ";
            this.cmdMAJ.Size = new System.Drawing.Size(122, 35);
            this.cmdMAJ.TabIndex = 570;
            this.cmdMAJ.Text = "     Valider";
            this.cmdMAJ.UseVisualStyleBackColor = false;
            this.cmdMAJ.Click += new System.EventHandler(this.cmdMAJ_Click);
            // 
            // ConfQuit
            // 
            this.ConfQuit.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.ConfQuit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.ConfQuit.Cursor = System.Windows.Forms.Cursors.Hand;
            this.ConfQuit.FlatAppearance.BorderSize = 0;
            this.ConfQuit.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.ConfQuit.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.ConfQuit.ForeColor = System.Drawing.Color.White;
            this.ConfQuit.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.ConfQuit.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.ConfQuit.Location = new System.Drawing.Point(254, 2);
            this.ConfQuit.Margin = new System.Windows.Forms.Padding(2);
            this.ConfQuit.Name = "ConfQuit";
            this.ConfQuit.Size = new System.Drawing.Size(122, 35);
            this.ConfQuit.TabIndex = 571;
            this.ConfQuit.Text = "     Fermer";
            this.ConfQuit.UseVisualStyleBackColor = false;
            this.ConfQuit.Click += new System.EventHandler(this.ConfQuit_Click);
            // 
            // frmReleveConf
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(773, 578);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.Text3);
            this.Controls.Add(this.SSTab1);
            this.MaximumSize = new System.Drawing.Size(789, 617);
            this.MinimumSize = new System.Drawing.Size(789, 617);
            this.Name = "frmReleveConf";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Relevé Conformité";
            this.FormClosed += new System.Windows.Forms.FormClosedEventHandler(this.frmReleveConf_FormClosed);
            this.Load += new System.EventHandler(this.frmReleveConf_Load);
            this.ultraTabPageControl1.ResumeLayout(false);
            this.Frame4.ResumeLayout(false);
            this.Frame3.ResumeLayout(false);
            this.Frame3.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.Frame6.ResumeLayout(false);
            this.Frame6.PerformLayout();
            this.Frame5.ResumeLayout(false);
            this.Frame5.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.Frame10.ResumeLayout(false);
            this.ultraTabPageControl2.ResumeLayout(false);
            this.Frame2.ResumeLayout(false);
            this.Frame2.PerformLayout();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel5.PerformLayout();
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.ultraTabPageControl3.ResumeLayout(false);
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.ultraTabPageControl4.ResumeLayout(false);
            this.ultraTabPageControl4.PerformLayout();
            this.Frame7.ResumeLayout(false);
            this.Frame7.PerformLayout();
            this.Frame8.ResumeLayout(false);
            this.Frame8.PerformLayout();
            this.Frame9.ResumeLayout(false);
            this.Frame9.PerformLayout();
            this.Frame1.ResumeLayout(false);
            this.Frame1.PerformLayout();
            this.Frame11.ResumeLayout(false);
            this.Frame11.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSTab1)).EndInit();
            this.SSTab1.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        public Infragistics.Win.UltraWinTabControl.UltraTabControl SSTab1;
        public Infragistics.Win.UltraWinTabControl.UltraTabSharedControlsPage ultraTabSharedControlsPage1;
        public Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl1;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl2;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl3;
        private Infragistics.Win.UltraWinTabControl.UltraTabPageControl ultraTabPageControl4;
        public System.Windows.Forms.GroupBox Frame10;
        public iTalk.iTalk_RichTextBox Text9;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.CheckBox Check3;
        public System.Windows.Forms.CheckBox Check10;
        public System.Windows.Forms.CheckBox Check11;
        public System.Windows.Forms.CheckBox Check12;
        public System.Windows.Forms.CheckBox Check13;
        public System.Windows.Forms.CheckBox Check14;
        public System.Windows.Forms.CheckBox Check15;
        public System.Windows.Forms.CheckBox Check16;
        public System.Windows.Forms.CheckBox Check17;
        public System.Windows.Forms.CheckBox Check18;
        public System.Windows.Forms.CheckBox Check19;
        public System.Windows.Forms.CheckBox Check110;
        public System.Windows.Forms.CheckBox Check111;
        public System.Windows.Forms.CheckBox Check112;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.CheckBox Check113;
        public System.Windows.Forms.CheckBox Check114;
        public System.Windows.Forms.CheckBox Check115;
        public System.Windows.Forms.CheckBox Check116;
        public System.Windows.Forms.CheckBox Check117;
        public System.Windows.Forms.CheckBox Check118;
        public System.Windows.Forms.CheckBox Check119;
        public System.Windows.Forms.CheckBox Check120;
        public System.Windows.Forms.CheckBox Check4;
        public System.Windows.Forms.CheckBox Check2;
        public System.Windows.Forms.GroupBox Frame4;
        public System.Windows.Forms.GroupBox Frame6;
        public System.Windows.Forms.GroupBox Frame5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.CheckBox Check123;
        public System.Windows.Forms.CheckBox Check153;
        public System.Windows.Forms.CheckBox Check5;
        public System.Windows.Forms.GroupBox Frame3;
        public System.Windows.Forms.CheckBox Check143;
        public System.Windows.Forms.RadioButton Option13;
        public System.Windows.Forms.RadioButton Option14;
        public System.Windows.Forms.RadioButton Option23;
        public System.Windows.Forms.RadioButton Option11;
        public System.Windows.Forms.RadioButton Option12;
        public System.Windows.Forms.RadioButton Option19;
        public System.Windows.Forms.RadioButton Option10;
        public System.Windows.Forms.RadioButton Option9;
        public iTalk.iTalk_TextBox_Small2 Text1;
        public iTalk.iTalk_TextBox_Small2 Text6;
        public iTalk.iTalk_TextBox_Small2 Text5;
        public iTalk.iTalk_TextBox_Small2 Text3;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdAjouter;
        public System.Windows.Forms.Button cmdMAJ;
        public System.Windows.Forms.Button ConfQuit;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        public System.Windows.Forms.CheckBox Check145;
        public System.Windows.Forms.CheckBox Check135;
        public System.Windows.Forms.CheckBox Check147;
        public System.Windows.Forms.CheckBox Check129;
        public System.Windows.Forms.CheckBox Check146;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.CheckBox Check139;
        public System.Windows.Forms.CheckBox Check140;
        public System.Windows.Forms.CheckBox Check141;
        public System.Windows.Forms.CheckBox Check142;
        public System.Windows.Forms.CheckBox Check125;
        public System.Windows.Forms.CheckBox Check124;
        public System.Windows.Forms.CheckBox Check144;
        public iTalk.iTalk_TextBox_Small2 Text4;
        public System.Windows.Forms.CheckBox Check148;
        public System.Windows.Forms.CheckBox Check136;
        public System.Windows.Forms.GroupBox Frame2;
        public System.Windows.Forms.RadioButton Option22;
        public System.Windows.Forms.RadioButton Option7;
        public System.Windows.Forms.RadioButton Option8;
        public System.Windows.Forms.RadioButton Option6;
        public System.Windows.Forms.RadioButton Option5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        public System.Windows.Forms.CheckBox Check149;
        public System.Windows.Forms.CheckBox Check150;
        public System.Windows.Forms.CheckBox Check151;
        public System.Windows.Forms.CheckBox Check152;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        public System.Windows.Forms.CheckBox Check6;
        public System.Windows.Forms.CheckBox Check7;
        public System.Windows.Forms.CheckBox Check8;
        public System.Windows.Forms.CheckBox Check9;
        public System.Windows.Forms.CheckBox Check100;
        public System.Windows.Forms.CheckBox Check101;
        public System.Windows.Forms.CheckBox Check102;
        public System.Windows.Forms.GroupBox Frame7;
        public System.Windows.Forms.GroupBox Frame1;
        public System.Windows.Forms.GroupBox Frame11;
        public System.Windows.Forms.CheckBox Check128;
        public System.Windows.Forms.CheckBox Check127;
        public System.Windows.Forms.CheckBox Check126;
        public iTalk.iTalk_TextBox_Small2 Text7;
        public iTalk.iTalk_TextBox_Small2 Text8;
        public iTalk.iTalk_TextBox_Small2 Text2;
        public iTalk.iTalk_TextBox_Small2 Text10;
        public System.Windows.Forms.RadioButton Option20;
        public System.Windows.Forms.RadioButton Option3;
        public System.Windows.Forms.RadioButton Option4;
        public System.Windows.Forms.RadioButton Option2;
        public System.Windows.Forms.RadioButton Option1;
        public System.Windows.Forms.RadioButton Option26;
        public System.Windows.Forms.RadioButton Option25;
        public System.Windows.Forms.CheckBox Check137;
        public System.Windows.Forms.CheckBox Check122;
        public System.Windows.Forms.CheckBox Check121;
        public System.Windows.Forms.GroupBox Frame8;
        public System.Windows.Forms.GroupBox Frame9;
        public System.Windows.Forms.RadioButton Option16;
        public System.Windows.Forms.RadioButton Option15;
        public System.Windows.Forms.RadioButton Option24;
        public System.Windows.Forms.RadioButton Option17;
        public System.Windows.Forms.RadioButton Option18;
        public System.Windows.Forms.RadioButton Option21;
    }
}