﻿namespace Axe_interDT.Views.CompteRendu
{
    partial class frmCEE
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.CmdFermer = new System.Windows.Forms.Button();
            this.cmdMAJ = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCodeImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.label32 = new System.Windows.Forms.Label();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.chkCEEI_ChauffEcs = new System.Windows.Forms.CheckBox();
            this.chkCEEI_ChauffageSeul = new System.Windows.Forms.CheckBox();
            this.txtCEEI_Nb3piece = new iTalk.iTalk_TextBox_Small2();
            this.txtCEEI_Nb2piece = new iTalk.iTalk_TextBox_Small2();
            this.txtCEEI_NbLogement = new iTalk.iTalk_TextBox_Small2();
            this.label319 = new System.Windows.Forms.Label();
            this.label35 = new System.Windows.Forms.Label();
            this.label38 = new System.Windows.Forms.Label();
            this.label36 = new System.Windows.Forms.Label();
            this.label37 = new System.Windows.Forms.Label();
            this.label31 = new System.Windows.Forms.Label();
            this.label310 = new System.Windows.Forms.Label();
            this.label39 = new System.Windows.Forms.Label();
            this.label311 = new System.Windows.Forms.Label();
            this.label312 = new System.Windows.Forms.Label();
            this.label30 = new System.Windows.Forms.Label();
            this.label313 = new System.Windows.Forms.Label();
            this.label314 = new System.Windows.Forms.Label();
            this.label315 = new System.Windows.Forms.Label();
            this.label316 = new System.Windows.Forms.Label();
            this.label317 = new System.Windows.Forms.Label();
            this.label318 = new System.Windows.Forms.Label();
            this.txtCEEI_Nb1piece = new iTalk.iTalk_TextBox_Small2();
            this.txtCEEI_Nb4piece = new iTalk.iTalk_TextBox_Small2();
            this.txtCEEI_Nb5piece = new iTalk.iTalk_TextBox_Small2();
            this.txtCEEI_Nb6piece = new iTalk.iTalk_TextBox_Small2();
            this.txtCEEI_CommOuBureau = new iTalk.iTalk_TextBox_Small2();
            this.chkCEEI_ConstrAvt1975 = new System.Windows.Forms.CheckBox();
            this.chkCEEI_ConstAp1975 = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.CmdFermer);
            this.flowLayoutPanel1.Controls.Add(this.cmdMAJ);
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(205, 326);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(257, 39);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // CmdFermer
            // 
            this.CmdFermer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CmdFermer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdFermer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdFermer.FlatAppearance.BorderSize = 0;
            this.CmdFermer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdFermer.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.CmdFermer.ForeColor = System.Drawing.Color.White;
            this.CmdFermer.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.CmdFermer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdFermer.Location = new System.Drawing.Point(2, 2);
            this.CmdFermer.Margin = new System.Windows.Forms.Padding(2);
            this.CmdFermer.Name = "CmdFermer";
            this.CmdFermer.Size = new System.Drawing.Size(122, 35);
            this.CmdFermer.TabIndex = 567;
            this.CmdFermer.Text = "    Fermer";
            this.CmdFermer.UseVisualStyleBackColor = false;
            this.CmdFermer.Click += new System.EventHandler(this.cmdFermer_Click);
            // 
            // cmdMAJ
            // 
            this.cmdMAJ.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJ.FlatAppearance.BorderSize = 0;
            this.cmdMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJ.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdMAJ.ForeColor = System.Drawing.Color.White;
            this.cmdMAJ.Image = global::Axe_interDT.Properties.Resources.Ok_16x16;
            this.cmdMAJ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdMAJ.Location = new System.Drawing.Point(128, 2);
            this.cmdMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJ.Name = "cmdMAJ";
            this.cmdMAJ.Size = new System.Drawing.Size(122, 35);
            this.cmdMAJ.TabIndex = 391;
            this.cmdMAJ.Text = "     Valider";
            this.cmdMAJ.UseVisualStyleBackColor = false;
            this.cmdMAJ.Click += new System.EventHandler(this.cmdMAJ_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 338F));
            this.tableLayoutPanel1.Controls.Add(this.txtCodeImmeuble, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.label32, 0, 0);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(7, 12);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(517, 33);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // txtCodeImmeuble
            // 
            this.txtCodeImmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeImmeuble.AccAllowComma = false;
            this.txtCodeImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeuble.AccHidenValue = "";
            this.txtCodeImmeuble.AccNotAllowedChars = null;
            this.txtCodeImmeuble.AccReadOnly = false;
            this.txtCodeImmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeuble.AccRequired = false;
            this.txtCodeImmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeImmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeImmeuble.Location = new System.Drawing.Point(181, 2);
            this.txtCodeImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeuble.MaxLength = 32767;
            this.txtCodeImmeuble.Multiline = false;
            this.txtCodeImmeuble.Name = "txtCodeImmeuble";
            this.txtCodeImmeuble.ReadOnly = false;
            this.txtCodeImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeuble.Size = new System.Drawing.Size(334, 27);
            this.txtCodeImmeuble.TabIndex = 502;
            this.txtCodeImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeuble.UseSystemPasswordChar = false;
            // 
            // label32
            // 
            this.label32.AutoSize = true;
            this.label32.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label32.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label32.Location = new System.Drawing.Point(3, 0);
            this.label32.Name = "label32";
            this.label32.Size = new System.Drawing.Size(173, 33);
            this.label32.TabIndex = 384;
            this.label32.Text = "CODE IMMEUBLE";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 4;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 56.2701F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 43.7299F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 157F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 230F));
            this.tableLayoutPanel2.Controls.Add(this.chkCEEI_ChauffEcs, 3, 2);
            this.tableLayoutPanel2.Controls.Add(this.chkCEEI_ChauffageSeul, 3, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtCEEI_Nb3piece, 1, 3);
            this.tableLayoutPanel2.Controls.Add(this.txtCEEI_Nb2piece, 1, 2);
            this.tableLayoutPanel2.Controls.Add(this.txtCEEI_NbLogement, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.label319, 3, 0);
            this.tableLayoutPanel2.Controls.Add(this.label35, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.label38, 0, 1);
            this.tableLayoutPanel2.Controls.Add(this.label36, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.label37, 0, 3);
            this.tableLayoutPanel2.Controls.Add(this.label31, 0, 4);
            this.tableLayoutPanel2.Controls.Add(this.label310, 0, 5);
            this.tableLayoutPanel2.Controls.Add(this.label39, 0, 6);
            this.tableLayoutPanel2.Controls.Add(this.label311, 0, 7);
            this.tableLayoutPanel2.Controls.Add(this.label312, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.label30, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.label313, 2, 2);
            this.tableLayoutPanel2.Controls.Add(this.label314, 2, 3);
            this.tableLayoutPanel2.Controls.Add(this.label315, 2, 4);
            this.tableLayoutPanel2.Controls.Add(this.label316, 2, 5);
            this.tableLayoutPanel2.Controls.Add(this.label317, 2, 6);
            this.tableLayoutPanel2.Controls.Add(this.label318, 3, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtCEEI_Nb1piece, 1, 1);
            this.tableLayoutPanel2.Controls.Add(this.txtCEEI_Nb4piece, 1, 4);
            this.tableLayoutPanel2.Controls.Add(this.txtCEEI_Nb5piece, 1, 5);
            this.tableLayoutPanel2.Controls.Add(this.txtCEEI_Nb6piece, 1, 6);
            this.tableLayoutPanel2.Controls.Add(this.txtCEEI_CommOuBureau, 1, 7);
            this.tableLayoutPanel2.Controls.Add(this.chkCEEI_ConstrAvt1975, 3, 5);
            this.tableLayoutPanel2.Controls.Add(this.chkCEEI_ConstAp1975, 3, 6);
            this.tableLayoutPanel2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 16);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 8;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(689, 249);
            this.tableLayoutPanel2.TabIndex = 2;
            // 
            // chkCEEI_ChauffEcs
            // 
            this.chkCEEI_ChauffEcs.AutoSize = true;
            this.chkCEEI_ChauffEcs.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkCEEI_ChauffEcs.Location = new System.Drawing.Point(461, 65);
            this.chkCEEI_ChauffEcs.Name = "chkCEEI_ChauffEcs";
            this.chkCEEI_ChauffEcs.Size = new System.Drawing.Size(150, 23);
            this.chkCEEI_ChauffEcs.TabIndex = 571;
            this.chkCEEI_ChauffEcs.Text = "Chauffage et ECS";
            this.chkCEEI_ChauffEcs.UseVisualStyleBackColor = true;
            // 
            // chkCEEI_ChauffageSeul
            // 
            this.chkCEEI_ChauffageSeul.AutoSize = true;
            this.chkCEEI_ChauffageSeul.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkCEEI_ChauffageSeul.Location = new System.Drawing.Point(461, 34);
            this.chkCEEI_ChauffageSeul.Name = "chkCEEI_ChauffageSeul";
            this.chkCEEI_ChauffageSeul.Size = new System.Drawing.Size(141, 23);
            this.chkCEEI_ChauffageSeul.TabIndex = 570;
            this.chkCEEI_ChauffageSeul.Text = "Chauffage seul :";
            this.chkCEEI_ChauffageSeul.UseVisualStyleBackColor = true;
            // 
            // txtCEEI_Nb3piece
            // 
            this.txtCEEI_Nb3piece.AccAcceptNumbersOnly = false;
            this.txtCEEI_Nb3piece.AccAllowComma = false;
            this.txtCEEI_Nb3piece.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCEEI_Nb3piece.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCEEI_Nb3piece.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCEEI_Nb3piece.AccHidenValue = "";
            this.txtCEEI_Nb3piece.AccNotAllowedChars = null;
            this.txtCEEI_Nb3piece.AccReadOnly = false;
            this.txtCEEI_Nb3piece.AccReadOnlyAllowDelete = false;
            this.txtCEEI_Nb3piece.AccRequired = false;
            this.txtCEEI_Nb3piece.BackColor = System.Drawing.Color.White;
            this.txtCEEI_Nb3piece.CustomBackColor = System.Drawing.Color.White;
            this.txtCEEI_Nb3piece.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCEEI_Nb3piece.ForeColor = System.Drawing.Color.Black;
            this.txtCEEI_Nb3piece.Location = new System.Drawing.Point(171, 95);
            this.txtCEEI_Nb3piece.Margin = new System.Windows.Forms.Padding(2);
            this.txtCEEI_Nb3piece.MaxLength = 32767;
            this.txtCEEI_Nb3piece.Multiline = false;
            this.txtCEEI_Nb3piece.Name = "txtCEEI_Nb3piece";
            this.txtCEEI_Nb3piece.ReadOnly = false;
            this.txtCEEI_Nb3piece.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCEEI_Nb3piece.Size = new System.Drawing.Size(128, 27);
            this.txtCEEI_Nb3piece.TabIndex = 3;
            this.txtCEEI_Nb3piece.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCEEI_Nb3piece.UseSystemPasswordChar = false;
            // 
            // txtCEEI_Nb2piece
            // 
            this.txtCEEI_Nb2piece.AccAcceptNumbersOnly = false;
            this.txtCEEI_Nb2piece.AccAllowComma = false;
            this.txtCEEI_Nb2piece.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCEEI_Nb2piece.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCEEI_Nb2piece.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCEEI_Nb2piece.AccHidenValue = "";
            this.txtCEEI_Nb2piece.AccNotAllowedChars = null;
            this.txtCEEI_Nb2piece.AccReadOnly = false;
            this.txtCEEI_Nb2piece.AccReadOnlyAllowDelete = false;
            this.txtCEEI_Nb2piece.AccRequired = false;
            this.txtCEEI_Nb2piece.BackColor = System.Drawing.Color.White;
            this.txtCEEI_Nb2piece.CustomBackColor = System.Drawing.Color.White;
            this.txtCEEI_Nb2piece.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCEEI_Nb2piece.ForeColor = System.Drawing.Color.Black;
            this.txtCEEI_Nb2piece.Location = new System.Drawing.Point(171, 64);
            this.txtCEEI_Nb2piece.Margin = new System.Windows.Forms.Padding(2);
            this.txtCEEI_Nb2piece.MaxLength = 32767;
            this.txtCEEI_Nb2piece.Multiline = false;
            this.txtCEEI_Nb2piece.Name = "txtCEEI_Nb2piece";
            this.txtCEEI_Nb2piece.ReadOnly = false;
            this.txtCEEI_Nb2piece.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCEEI_Nb2piece.Size = new System.Drawing.Size(128, 27);
            this.txtCEEI_Nb2piece.TabIndex = 2;
            this.txtCEEI_Nb2piece.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCEEI_Nb2piece.UseSystemPasswordChar = false;
            // 
            // txtCEEI_NbLogement
            // 
            this.txtCEEI_NbLogement.AccAcceptNumbersOnly = false;
            this.txtCEEI_NbLogement.AccAllowComma = false;
            this.txtCEEI_NbLogement.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCEEI_NbLogement.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCEEI_NbLogement.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCEEI_NbLogement.AccHidenValue = "";
            this.txtCEEI_NbLogement.AccNotAllowedChars = null;
            this.txtCEEI_NbLogement.AccReadOnly = false;
            this.txtCEEI_NbLogement.AccReadOnlyAllowDelete = false;
            this.txtCEEI_NbLogement.AccRequired = false;
            this.txtCEEI_NbLogement.BackColor = System.Drawing.Color.White;
            this.txtCEEI_NbLogement.CustomBackColor = System.Drawing.Color.White;
            this.txtCEEI_NbLogement.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCEEI_NbLogement.ForeColor = System.Drawing.Color.Black;
            this.txtCEEI_NbLogement.Location = new System.Drawing.Point(171, 2);
            this.txtCEEI_NbLogement.Margin = new System.Windows.Forms.Padding(2);
            this.txtCEEI_NbLogement.MaxLength = 32767;
            this.txtCEEI_NbLogement.Multiline = false;
            this.txtCEEI_NbLogement.Name = "txtCEEI_NbLogement";
            this.txtCEEI_NbLogement.ReadOnly = false;
            this.txtCEEI_NbLogement.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCEEI_NbLogement.Size = new System.Drawing.Size(128, 27);
            this.txtCEEI_NbLogement.TabIndex = 0;
            this.txtCEEI_NbLogement.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCEEI_NbLogement.UseSystemPasswordChar = false;
            // 
            // label319
            // 
            this.label319.AutoSize = true;
            this.label319.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label319.Location = new System.Drawing.Point(461, 0);
            this.label319.Name = "label319";
            this.label319.Size = new System.Drawing.Size(62, 19);
            this.label319.TabIndex = 520;
            this.label319.Text = "USAGE :";
            // 
            // label35
            // 
            this.label35.AutoSize = true;
            this.label35.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label35.Location = new System.Drawing.Point(3, 0);
            this.label35.Name = "label35";
            this.label35.Size = new System.Drawing.Size(98, 31);
            this.label35.TabIndex = 504;
            this.label35.Text = "NOMBRE DE LOGEMENT";
            // 
            // label38
            // 
            this.label38.AutoSize = true;
            this.label38.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label38.Location = new System.Drawing.Point(3, 31);
            this.label38.Name = "label38";
            this.label38.Size = new System.Drawing.Size(153, 19);
            this.label38.TabIndex = 503;
            this.label38.Text = "Nombre de 1 pièces :";
            // 
            // label36
            // 
            this.label36.AutoSize = true;
            this.label36.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label36.Location = new System.Drawing.Point(3, 62);
            this.label36.Name = "label36";
            this.label36.Size = new System.Drawing.Size(153, 19);
            this.label36.TabIndex = 505;
            this.label36.Text = "Nombre de 2 pièces :";
            // 
            // label37
            // 
            this.label37.AutoSize = true;
            this.label37.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label37.Location = new System.Drawing.Point(3, 93);
            this.label37.Name = "label37";
            this.label37.Size = new System.Drawing.Size(153, 19);
            this.label37.TabIndex = 506;
            this.label37.Text = "Nombre de 3 pièces :";
            // 
            // label31
            // 
            this.label31.AutoSize = true;
            this.label31.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label31.Location = new System.Drawing.Point(3, 124);
            this.label31.Name = "label31";
            this.label31.Size = new System.Drawing.Size(153, 19);
            this.label31.TabIndex = 507;
            this.label31.Text = "Nombre de 4 pièces :";
            // 
            // label310
            // 
            this.label310.AutoSize = true;
            this.label310.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label310.Location = new System.Drawing.Point(3, 155);
            this.label310.Name = "label310";
            this.label310.Size = new System.Drawing.Size(153, 19);
            this.label310.TabIndex = 508;
            this.label310.Text = "Nombre de 5 pièces :";
            // 
            // label39
            // 
            this.label39.AutoSize = true;
            this.label39.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label39.Location = new System.Drawing.Point(3, 186);
            this.label39.Name = "label39";
            this.label39.Size = new System.Drawing.Size(153, 19);
            this.label39.TabIndex = 509;
            this.label39.Text = "Nombre de 6 pièces :";
            // 
            // label311
            // 
            this.label311.AutoSize = true;
            this.label311.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label311.Location = new System.Drawing.Point(3, 217);
            this.label311.Name = "label311";
            this.label311.Size = new System.Drawing.Size(117, 32);
            this.label311.TabIndex = 510;
            this.label311.Text = "Commerces ou bureaux :";
            // 
            // label312
            // 
            this.label312.AutoSize = true;
            this.label312.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label312.Location = new System.Drawing.Point(304, 31);
            this.label312.Name = "label312";
            this.label312.Size = new System.Drawing.Size(63, 19);
            this.label312.TabIndex = 512;
            this.label312.Text = " < 35 m2";
            // 
            // label30
            // 
            this.label30.AutoSize = true;
            this.label30.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label30.Location = new System.Drawing.Point(304, 0);
            this.label30.Name = "label30";
            this.label30.Size = new System.Drawing.Size(48, 19);
            this.label30.TabIndex = 511;
            this.label30.Text = "Logts";
            // 
            // label313
            // 
            this.label313.AutoSize = true;
            this.label313.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label313.Location = new System.Drawing.Point(304, 62);
            this.label313.Name = "label313";
            this.label313.Size = new System.Drawing.Size(79, 19);
            this.label313.TabIndex = 513;
            this.label313.Text = "35 à 60 m2";
            // 
            // label314
            // 
            this.label314.AutoSize = true;
            this.label314.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label314.Location = new System.Drawing.Point(304, 93);
            this.label314.Name = "label314";
            this.label314.Size = new System.Drawing.Size(79, 19);
            this.label314.TabIndex = 514;
            this.label314.Text = "60 à 80 m2";
            // 
            // label315
            // 
            this.label315.AutoSize = true;
            this.label315.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label315.Location = new System.Drawing.Point(304, 124);
            this.label315.Name = "label315";
            this.label315.Size = new System.Drawing.Size(87, 19);
            this.label315.TabIndex = 515;
            this.label315.Text = "80 à 100 m2";
            // 
            // label316
            // 
            this.label316.AutoSize = true;
            this.label316.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label316.Location = new System.Drawing.Point(304, 155);
            this.label316.Name = "label316";
            this.label316.Size = new System.Drawing.Size(95, 19);
            this.label316.TabIndex = 516;
            this.label316.Text = "100 à 130 m2";
            // 
            // label317
            // 
            this.label317.AutoSize = true;
            this.label317.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label317.Location = new System.Drawing.Point(304, 186);
            this.label317.Name = "label317";
            this.label317.Size = new System.Drawing.Size(68, 19);
            this.label317.TabIndex = 517;
            this.label317.Text = "> 130 m2";
            // 
            // label318
            // 
            this.label318.AutoSize = true;
            this.label318.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label318.Location = new System.Drawing.Point(461, 124);
            this.label318.Name = "label318";
            this.label318.Size = new System.Drawing.Size(199, 19);
            this.label318.TabIndex = 524;
            this.label318.Text = "ANCIENNETE DU BATIMENT";
            // 
            // txtCEEI_Nb1piece
            // 
            this.txtCEEI_Nb1piece.AccAcceptNumbersOnly = false;
            this.txtCEEI_Nb1piece.AccAllowComma = false;
            this.txtCEEI_Nb1piece.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCEEI_Nb1piece.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCEEI_Nb1piece.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCEEI_Nb1piece.AccHidenValue = "";
            this.txtCEEI_Nb1piece.AccNotAllowedChars = null;
            this.txtCEEI_Nb1piece.AccReadOnly = false;
            this.txtCEEI_Nb1piece.AccReadOnlyAllowDelete = false;
            this.txtCEEI_Nb1piece.AccRequired = false;
            this.txtCEEI_Nb1piece.BackColor = System.Drawing.Color.White;
            this.txtCEEI_Nb1piece.CustomBackColor = System.Drawing.Color.White;
            this.txtCEEI_Nb1piece.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCEEI_Nb1piece.ForeColor = System.Drawing.Color.Black;
            this.txtCEEI_Nb1piece.Location = new System.Drawing.Point(171, 33);
            this.txtCEEI_Nb1piece.Margin = new System.Windows.Forms.Padding(2);
            this.txtCEEI_Nb1piece.MaxLength = 32767;
            this.txtCEEI_Nb1piece.Multiline = false;
            this.txtCEEI_Nb1piece.Name = "txtCEEI_Nb1piece";
            this.txtCEEI_Nb1piece.ReadOnly = false;
            this.txtCEEI_Nb1piece.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCEEI_Nb1piece.Size = new System.Drawing.Size(128, 27);
            this.txtCEEI_Nb1piece.TabIndex = 1;
            this.txtCEEI_Nb1piece.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCEEI_Nb1piece.UseSystemPasswordChar = false;
            // 
            // txtCEEI_Nb4piece
            // 
            this.txtCEEI_Nb4piece.AccAcceptNumbersOnly = false;
            this.txtCEEI_Nb4piece.AccAllowComma = false;
            this.txtCEEI_Nb4piece.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCEEI_Nb4piece.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCEEI_Nb4piece.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCEEI_Nb4piece.AccHidenValue = "";
            this.txtCEEI_Nb4piece.AccNotAllowedChars = null;
            this.txtCEEI_Nb4piece.AccReadOnly = false;
            this.txtCEEI_Nb4piece.AccReadOnlyAllowDelete = false;
            this.txtCEEI_Nb4piece.AccRequired = false;
            this.txtCEEI_Nb4piece.BackColor = System.Drawing.Color.White;
            this.txtCEEI_Nb4piece.CustomBackColor = System.Drawing.Color.White;
            this.txtCEEI_Nb4piece.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCEEI_Nb4piece.ForeColor = System.Drawing.Color.Black;
            this.txtCEEI_Nb4piece.Location = new System.Drawing.Point(171, 126);
            this.txtCEEI_Nb4piece.Margin = new System.Windows.Forms.Padding(2);
            this.txtCEEI_Nb4piece.MaxLength = 32767;
            this.txtCEEI_Nb4piece.Multiline = false;
            this.txtCEEI_Nb4piece.Name = "txtCEEI_Nb4piece";
            this.txtCEEI_Nb4piece.ReadOnly = false;
            this.txtCEEI_Nb4piece.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCEEI_Nb4piece.Size = new System.Drawing.Size(128, 27);
            this.txtCEEI_Nb4piece.TabIndex = 4;
            this.txtCEEI_Nb4piece.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCEEI_Nb4piece.UseSystemPasswordChar = false;
            // 
            // txtCEEI_Nb5piece
            // 
            this.txtCEEI_Nb5piece.AccAcceptNumbersOnly = false;
            this.txtCEEI_Nb5piece.AccAllowComma = false;
            this.txtCEEI_Nb5piece.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCEEI_Nb5piece.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCEEI_Nb5piece.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCEEI_Nb5piece.AccHidenValue = "";
            this.txtCEEI_Nb5piece.AccNotAllowedChars = null;
            this.txtCEEI_Nb5piece.AccReadOnly = false;
            this.txtCEEI_Nb5piece.AccReadOnlyAllowDelete = false;
            this.txtCEEI_Nb5piece.AccRequired = false;
            this.txtCEEI_Nb5piece.BackColor = System.Drawing.Color.White;
            this.txtCEEI_Nb5piece.CustomBackColor = System.Drawing.Color.White;
            this.txtCEEI_Nb5piece.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCEEI_Nb5piece.ForeColor = System.Drawing.Color.Black;
            this.txtCEEI_Nb5piece.Location = new System.Drawing.Point(171, 157);
            this.txtCEEI_Nb5piece.Margin = new System.Windows.Forms.Padding(2);
            this.txtCEEI_Nb5piece.MaxLength = 32767;
            this.txtCEEI_Nb5piece.Multiline = false;
            this.txtCEEI_Nb5piece.Name = "txtCEEI_Nb5piece";
            this.txtCEEI_Nb5piece.ReadOnly = false;
            this.txtCEEI_Nb5piece.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCEEI_Nb5piece.Size = new System.Drawing.Size(128, 27);
            this.txtCEEI_Nb5piece.TabIndex = 5;
            this.txtCEEI_Nb5piece.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCEEI_Nb5piece.UseSystemPasswordChar = false;
            // 
            // txtCEEI_Nb6piece
            // 
            this.txtCEEI_Nb6piece.AccAcceptNumbersOnly = false;
            this.txtCEEI_Nb6piece.AccAllowComma = false;
            this.txtCEEI_Nb6piece.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCEEI_Nb6piece.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCEEI_Nb6piece.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCEEI_Nb6piece.AccHidenValue = "";
            this.txtCEEI_Nb6piece.AccNotAllowedChars = null;
            this.txtCEEI_Nb6piece.AccReadOnly = false;
            this.txtCEEI_Nb6piece.AccReadOnlyAllowDelete = false;
            this.txtCEEI_Nb6piece.AccRequired = false;
            this.txtCEEI_Nb6piece.BackColor = System.Drawing.Color.White;
            this.txtCEEI_Nb6piece.CustomBackColor = System.Drawing.Color.White;
            this.txtCEEI_Nb6piece.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCEEI_Nb6piece.ForeColor = System.Drawing.Color.Black;
            this.txtCEEI_Nb6piece.Location = new System.Drawing.Point(171, 188);
            this.txtCEEI_Nb6piece.Margin = new System.Windows.Forms.Padding(2);
            this.txtCEEI_Nb6piece.MaxLength = 32767;
            this.txtCEEI_Nb6piece.Multiline = false;
            this.txtCEEI_Nb6piece.Name = "txtCEEI_Nb6piece";
            this.txtCEEI_Nb6piece.ReadOnly = false;
            this.txtCEEI_Nb6piece.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCEEI_Nb6piece.Size = new System.Drawing.Size(128, 27);
            this.txtCEEI_Nb6piece.TabIndex = 6;
            this.txtCEEI_Nb6piece.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCEEI_Nb6piece.UseSystemPasswordChar = false;
            // 
            // txtCEEI_CommOuBureau
            // 
            this.txtCEEI_CommOuBureau.AccAcceptNumbersOnly = false;
            this.txtCEEI_CommOuBureau.AccAllowComma = false;
            this.txtCEEI_CommOuBureau.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCEEI_CommOuBureau.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCEEI_CommOuBureau.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCEEI_CommOuBureau.AccHidenValue = "";
            this.txtCEEI_CommOuBureau.AccNotAllowedChars = null;
            this.txtCEEI_CommOuBureau.AccReadOnly = false;
            this.txtCEEI_CommOuBureau.AccReadOnlyAllowDelete = false;
            this.txtCEEI_CommOuBureau.AccRequired = false;
            this.txtCEEI_CommOuBureau.BackColor = System.Drawing.Color.White;
            this.txtCEEI_CommOuBureau.CustomBackColor = System.Drawing.Color.White;
            this.txtCEEI_CommOuBureau.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCEEI_CommOuBureau.ForeColor = System.Drawing.Color.Black;
            this.txtCEEI_CommOuBureau.Location = new System.Drawing.Point(171, 219);
            this.txtCEEI_CommOuBureau.Margin = new System.Windows.Forms.Padding(2);
            this.txtCEEI_CommOuBureau.MaxLength = 32767;
            this.txtCEEI_CommOuBureau.Multiline = false;
            this.txtCEEI_CommOuBureau.Name = "txtCEEI_CommOuBureau";
            this.txtCEEI_CommOuBureau.ReadOnly = false;
            this.txtCEEI_CommOuBureau.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCEEI_CommOuBureau.Size = new System.Drawing.Size(128, 27);
            this.txtCEEI_CommOuBureau.TabIndex = 7;
            this.txtCEEI_CommOuBureau.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCEEI_CommOuBureau.UseSystemPasswordChar = false;
            // 
            // chkCEEI_ConstrAvt1975
            // 
            this.chkCEEI_ConstrAvt1975.AutoSize = true;
            this.chkCEEI_ConstrAvt1975.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkCEEI_ConstrAvt1975.Location = new System.Drawing.Point(461, 158);
            this.chkCEEI_ConstrAvt1975.Name = "chkCEEI_ConstrAvt1975";
            this.chkCEEI_ConstrAvt1975.Size = new System.Drawing.Size(202, 23);
            this.chkCEEI_ConstrAvt1975.TabIndex = 572;
            this.chkCEEI_ConstrAvt1975.Text = "Construction avant 1975 :";
            this.chkCEEI_ConstrAvt1975.UseVisualStyleBackColor = true;
            // 
            // chkCEEI_ConstAp1975
            // 
            this.chkCEEI_ConstAp1975.AutoSize = true;
            this.chkCEEI_ConstAp1975.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.chkCEEI_ConstAp1975.Location = new System.Drawing.Point(461, 189);
            this.chkCEEI_ConstAp1975.Name = "chkCEEI_ConstAp1975";
            this.chkCEEI_ConstAp1975.Size = new System.Drawing.Size(203, 23);
            this.chkCEEI_ConstAp1975.TabIndex = 573;
            this.chkCEEI_ConstAp1975.Text = "Construction après 1975 :";
            this.chkCEEI_ConstAp1975.UseVisualStyleBackColor = true;
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.tableLayoutPanel2);
            this.groupBox1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.groupBox1.Location = new System.Drawing.Point(4, 51);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(695, 269);
            this.groupBox1.TabIndex = 3;
            this.groupBox1.TabStop = false;
            // 
            // frmCEE
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(700, 367);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.tableLayoutPanel1);
            this.Controls.Add(this.flowLayoutPanel1);
            this.MinimumSize = new System.Drawing.Size(716, 379);
            this.Name = "frmCEE";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmCEE";
            this.Load += new System.EventHandler(this.frmCEE_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.groupBox1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdMAJ;
        public System.Windows.Forms.Button CmdFermer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.Label label32;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeuble;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.Label label35;
        public System.Windows.Forms.Label label38;
        public System.Windows.Forms.Label label36;
        public System.Windows.Forms.Label label37;
        public System.Windows.Forms.Label label31;
        public System.Windows.Forms.Label label310;
        public System.Windows.Forms.Label label39;
        public System.Windows.Forms.Label label311;
        public System.Windows.Forms.Label label312;
        public System.Windows.Forms.Label label30;
        public System.Windows.Forms.Label label313;
        public System.Windows.Forms.Label label314;
        public System.Windows.Forms.Label label315;
        public System.Windows.Forms.Label label316;
        public System.Windows.Forms.Label label317;
        public System.Windows.Forms.Label label319;
        public System.Windows.Forms.Label label318;
        public iTalk.iTalk_TextBox_Small2 txtCEEI_Nb3piece;
        public iTalk.iTalk_TextBox_Small2 txtCEEI_Nb2piece;
        public iTalk.iTalk_TextBox_Small2 txtCEEI_NbLogement;
        public iTalk.iTalk_TextBox_Small2 txtCEEI_Nb1piece;
        public iTalk.iTalk_TextBox_Small2 txtCEEI_Nb4piece;
        public iTalk.iTalk_TextBox_Small2 txtCEEI_Nb5piece;
        public iTalk.iTalk_TextBox_Small2 txtCEEI_Nb6piece;
        public iTalk.iTalk_TextBox_Small2 txtCEEI_CommOuBureau;
        public System.Windows.Forms.CheckBox chkCEEI_ChauffEcs;
        public System.Windows.Forms.CheckBox chkCEEI_ChauffageSeul;
        public System.Windows.Forms.CheckBox chkCEEI_ConstrAvt1975;
        public System.Windows.Forms.CheckBox chkCEEI_ConstAp1975;
        private System.Windows.Forms.GroupBox groupBox1;
    }
}