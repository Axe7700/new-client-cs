﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.CompteRendu
{
    public partial class frmFOD : Form
    {
        public frmFOD()
        {
            InitializeComponent();
        }
        DataTable Data8;
        DataTable DatPrimaryRS = new DataTable();
        ModAdo ModAdo0 = new ModAdo();
        ModAdo ModAdoDatPrimaryRS = new ModAdo();
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdAjouter_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //DatPrimaryRS.AddNew();
            DatPrimaryRS.Rows.Add(DatPrimaryRS.NewRow());
            //txtFields(86).Text = gVar
            cmdAjouter.Enabled = false;
            cmdMAJ.Enabled = true;

        }

        private void cmdFermer_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            this.Close();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdMAJ_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            int existImmeuble;
            txtFields86.Text = General.gVar.ToString();
            //inisialiser les champs
            using (var tmp = new ModAdo())
                existImmeuble = Convert.ToInt32(tmp.fc_ADOlibelle("select  count( CodeImmeuble) from  techreleve  WHERE CodeImmeuble='" + General.gVar + "'"));

            if (existImmeuble == 0)
            {
                DatPrimaryRS.Rows[0]["CodeImmeuble"] = txtFields86.Text;
                DatPrimaryRS.Rows[0]["SituationSouteFOD"] = SSDBCombo11.Value;
                DatPrimaryRS.Rows[0]["SituationCuveFOD"] = SSDBCombo10.Value;
                DatPrimaryRS.Rows[0]["Cuve1"] = txtFields0.Text;
                DatPrimaryRS.Rows[0]["Cuve2"] = txtFields1.Text;
                DatPrimaryRS.Rows[0]["Cuve3"] = txtFields2.Text;
                DatPrimaryRS.Rows[0]["DiamRemplissage"] = txtFields7.Text;
                DatPrimaryRS.Rows[0]["DiamEvent"] = txtFields8.Text;
                DatPrimaryRS.Rows[0]["SitDepotage"] = txtFields4.Text;
                DatPrimaryRS.Rows[0]["SitEvent"] = txtFields5.Text;
                DatPrimaryRS.Rows[0]["HeureLiv"] = txtFields6.Text;
                DatPrimaryRS.Rows[0]["NatureFioul"] = txtFields3.Text;
                DatPrimaryRS.Rows[0]["JaugeMarqueFioul"] = txtFields9.Text;
                DatPrimaryRS.Rows[0]["EventVisiste"] = (Check2.Checked ? 1 : 0);
                ModAdoDatPrimaryRS.Update();
            }
            else
            {
                string sqlUpdate = "Update  techreleve  SET ";

                sqlUpdate = sqlUpdate + " SituationSouteFOD = " +Convert.ToInt32(General.nz(SSDBCombo11.Value,0)) + ", ";
                sqlUpdate = sqlUpdate + "SituationCuveFOD= " + Convert.ToInt32(General.nz(SSDBCombo10.Value, 0)) + ", ";
                sqlUpdate = sqlUpdate + " Cuve1 = '" + txtFields0.Text + "', ";
                sqlUpdate = sqlUpdate + " Cuve2 = '" + txtFields1.Text + "', ";
                sqlUpdate = sqlUpdate + " Cuve3 = '" + txtFields2.Text + "', ";
                sqlUpdate = sqlUpdate + " DiamRemplissage = '" + txtFields7.Text + "', ";
                sqlUpdate = sqlUpdate + " DiamEvent = '" + txtFields8.Text + "', ";
                sqlUpdate = sqlUpdate + " SitDepotage = '" + txtFields4.Text + "', ";
                sqlUpdate = sqlUpdate + " SitEvent = '" + txtFields5.Text + "', ";
                sqlUpdate = sqlUpdate + "HeureLiv = '" + txtFields6.Text + "', ";
                sqlUpdate = sqlUpdate + " NatureFioul = '" + txtFields3.Text + "', ";
                sqlUpdate = sqlUpdate + " JaugeMarqueFioul = '" + txtFields9.Text + "', ";
                sqlUpdate = sqlUpdate + " EventVisiste = " + (Check2.Checked ? 1 : 0) + " ";
                sqlUpdate = sqlUpdate + " Where CodeImmeuble='" + General.gVar + "'";
                General.Execute(sqlUpdate);
            }



        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void frmFOD_Load(System.Object eventSender, System.EventArgs eventArgs)
        {
           
            cmdAjouter.Enabled = false;
            cmdAjouter.Visible = false;
            cmdMAJ.Enabled = false;

             Data8=new DataTable();
          
            Data8=ModAdo0.fc_OpenRecordSet ("SELECT * FROM typeemplacementgeo");
            SSDBCombo11.DataSource = Data8;
            SSDBCombo11.ValueMember = "CodeEmplacementGeo";
            SSDBCombo11.DisplayMember = "IntituleEmplacementGeo";
            SSDBCombo10.DataSource = Data8;
            SSDBCombo10.ValueMember = "CodeEmplacementGeo";
            SSDBCombo10.DisplayMember = "IntituleEmplacementGeo";
            this.Text = this.Text + " pour l'immeuble " + General.gVar;
            General.SQL = "SELECT * FROM techreleve " + "WHERE CodeImmeuble='" + General.gVar + "'";
          
           
            DatPrimaryRS = ModAdoDatPrimaryRS.fc_OpenRecordSet(General.SQL);
            
            if (DatPrimaryRS.Rows.Count==0)
            {
                cmdAjouter.Enabled = true;
                cmdAjouter_Click(cmdAjouter, new System.EventArgs());
            }
            else
            {
                cmdMAJ.Enabled = true;
                SSDBCombo11.Text= valueOfCombo(Convert.ToInt32(DatPrimaryRS.Rows[0]["SituationSouteFOD"]));
                SSDBCombo10.Text = valueOfCombo(Convert.ToInt32(DatPrimaryRS.Rows[0]["SituationCuveFOD"]));
                txtFields0.Text=DatPrimaryRS.Rows[0]["Cuve1"].ToString();
                txtFields1.Text=DatPrimaryRS.Rows[0]["Cuve2"].ToString();
                txtFields2.Text=DatPrimaryRS.Rows[0]["Cuve3"].ToString();
                txtFields7.Text=DatPrimaryRS.Rows[0]["DiamRemplissage"].ToString();
                txtFields8.Text=DatPrimaryRS.Rows[0]["DiamEvent"].ToString();
                txtFields4.Text=DatPrimaryRS.Rows[0]["SitDepotage"].ToString();
                txtFields5.Text=DatPrimaryRS.Rows[0]["SitEvent"].ToString();
                txtFields6.Text=DatPrimaryRS.Rows[0]["HeureLiv"].ToString();
                txtFields3.Text=DatPrimaryRS.Rows[0]["NatureFioul"].ToString();
                txtFields9.Text=DatPrimaryRS.Rows[0]["JaugeMarqueFioul"].ToString();
                Check2.CheckState= DatPrimaryRS.Rows[0]["EventVisiste"].ToString()=="True"? CheckState.Checked:CheckState.Unchecked ;

            }


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>
        private string  valueOfCombo(int value)
        {
            if (value == 0)
            {
                return "";
            }
            else
            {
                string txt = Data8.Rows[value - 1]["IntituleEmplacementGeo"].ToString();
                return txt;
            }
          
        }
 
    }
}
