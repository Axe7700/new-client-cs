﻿namespace Axe_interDT.Views.CompteRendu
{
    partial class frmImpression
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.panel1 = new System.Windows.Forms.Panel();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.optOption1 = new System.Windows.Forms.RadioButton();
            this.optOption3 = new System.Windows.Forms.RadioButton();
            this.optOption2 = new System.Windows.Forms.RadioButton();
            this.optOption4 = new System.Windows.Forms.RadioButton();
            this.txtFin = new iTalk.iTalk_TextBox_Small2();
            this.txtDebut = new iTalk.iTalk_TextBox_Small2();
            this.txtApartir = new iTalk.iTalk_TextBox_Small2();
            this.Label1 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.CmdImprimer = new System.Windows.Forms.Button();
            this.panel1.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.Frame1);
            this.panel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.panel1.Location = new System.Drawing.Point(4, 5);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(467, 194);
            this.panel1.TabIndex = 0;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.tableLayoutPanel1);
            this.Frame1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Frame1.Location = new System.Drawing.Point(1, 1);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(461, 179);
            this.Frame1.TabIndex = 0;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "Sélection";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.optOption1, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.optOption3, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.optOption2, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.optOption4, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtFin, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtDebut, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtApartir, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.Label1, 0, 3);
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(10, 19);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 5;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(445, 151);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // optOption1
            // 
            this.optOption1.AutoSize = true;
            this.optOption1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optOption1.Location = new System.Drawing.Point(3, 3);
            this.optOption1.Name = "optOption1";
            this.optOption1.Size = new System.Drawing.Size(201, 23);
            this.optOption1.TabIndex = 538;
            this.optOption1.Text = "Tous les Comptes rendus";
            this.optOption1.UseVisualStyleBackColor = true;
            this.optOption1.CheckedChanged += new System.EventHandler(this.optOption1_CheckedChanged);
            // 
            // optOption3
            // 
            this.optOption3.AutoSize = true;
            this.optOption3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optOption3.Location = new System.Drawing.Point(3, 63);
            this.optOption3.Name = "optOption3";
            this.optOption3.Size = new System.Drawing.Size(127, 23);
            this.optOption3.TabIndex = 539;
            this.optOption3.Text = "De l\'immeuble";
            this.optOption3.UseVisualStyleBackColor = true;
            this.optOption3.CheckedChanged += new System.EventHandler(this.optOption3_CheckedChanged);
            // 
            // optOption2
            // 
            this.optOption2.AutoSize = true;
            this.optOption2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optOption2.Location = new System.Drawing.Point(3, 33);
            this.optOption2.Name = "optOption2";
            this.optOption2.Size = new System.Drawing.Size(178, 23);
            this.optOption2.TabIndex = 540;
            this.optOption2.Text = "A partir de l\'immeuble";
            this.optOption2.UseVisualStyleBackColor = true;
            this.optOption2.CheckedChanged += new System.EventHandler(this.optOption2_CheckedChanged);
            // 
            // optOption4
            // 
            this.optOption4.AutoSize = true;
            this.optOption4.Checked = true;
            this.optOption4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optOption4.Location = new System.Drawing.Point(3, 123);
            this.optOption4.Name = "optOption4";
            this.optOption4.Size = new System.Drawing.Size(154, 23);
            this.optOption4.TabIndex = 541;
            this.optOption4.TabStop = true;
            this.optOption4.Text = "Immeuble courant";
            this.optOption4.UseVisualStyleBackColor = true;
            // 
            // txtFin
            // 
            this.txtFin.AccAcceptNumbersOnly = false;
            this.txtFin.AccAllowComma = false;
            this.txtFin.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFin.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFin.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFin.AccHidenValue = "";
            this.txtFin.AccNotAllowedChars = null;
            this.txtFin.AccReadOnly = false;
            this.txtFin.AccReadOnlyAllowDelete = false;
            this.txtFin.AccRequired = false;
            this.txtFin.BackColor = System.Drawing.Color.White;
            this.txtFin.CustomBackColor = System.Drawing.Color.White;
            this.txtFin.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFin.ForeColor = System.Drawing.Color.Black;
            this.txtFin.Location = new System.Drawing.Point(224, 92);
            this.txtFin.Margin = new System.Windows.Forms.Padding(2);
            this.txtFin.MaxLength = 32767;
            this.txtFin.Multiline = false;
            this.txtFin.Name = "txtFin";
            this.txtFin.ReadOnly = false;
            this.txtFin.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFin.Size = new System.Drawing.Size(219, 27);
            this.txtFin.TabIndex = 542;
            this.txtFin.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFin.UseSystemPasswordChar = false;
            // 
            // txtDebut
            // 
            this.txtDebut.AccAcceptNumbersOnly = false;
            this.txtDebut.AccAllowComma = false;
            this.txtDebut.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDebut.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDebut.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDebut.AccHidenValue = "";
            this.txtDebut.AccNotAllowedChars = null;
            this.txtDebut.AccReadOnly = false;
            this.txtDebut.AccReadOnlyAllowDelete = false;
            this.txtDebut.AccRequired = false;
            this.txtDebut.BackColor = System.Drawing.Color.White;
            this.txtDebut.CustomBackColor = System.Drawing.Color.White;
            this.txtDebut.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDebut.ForeColor = System.Drawing.Color.Black;
            this.txtDebut.Location = new System.Drawing.Point(224, 62);
            this.txtDebut.Margin = new System.Windows.Forms.Padding(2);
            this.txtDebut.MaxLength = 32767;
            this.txtDebut.Multiline = false;
            this.txtDebut.Name = "txtDebut";
            this.txtDebut.ReadOnly = false;
            this.txtDebut.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDebut.Size = new System.Drawing.Size(219, 27);
            this.txtDebut.TabIndex = 543;
            this.txtDebut.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDebut.UseSystemPasswordChar = false;
            // 
            // txtApartir
            // 
            this.txtApartir.AccAcceptNumbersOnly = false;
            this.txtApartir.AccAllowComma = false;
            this.txtApartir.AccBackgroundColor = System.Drawing.Color.White;
            this.txtApartir.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtApartir.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtApartir.AccHidenValue = "";
            this.txtApartir.AccNotAllowedChars = null;
            this.txtApartir.AccReadOnly = false;
            this.txtApartir.AccReadOnlyAllowDelete = false;
            this.txtApartir.AccRequired = false;
            this.txtApartir.BackColor = System.Drawing.Color.White;
            this.txtApartir.CustomBackColor = System.Drawing.Color.White;
            this.txtApartir.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtApartir.ForeColor = System.Drawing.Color.Black;
            this.txtApartir.Location = new System.Drawing.Point(224, 32);
            this.txtApartir.Margin = new System.Windows.Forms.Padding(2);
            this.txtApartir.MaxLength = 32767;
            this.txtApartir.Multiline = false;
            this.txtApartir.Name = "txtApartir";
            this.txtApartir.ReadOnly = false;
            this.txtApartir.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtApartir.Size = new System.Drawing.Size(219, 27);
            this.txtApartir.TabIndex = 544;
            this.txtApartir.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtApartir.UseSystemPasswordChar = false;
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Label1.Location = new System.Drawing.Point(3, 90);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(97, 19);
            this.Label1.TabIndex = 545;
            this.Label1.Text = "à l\'immeuble";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.CmdImprimer);
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(186, 205);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(156, 44);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // CmdImprimer
            // 
            this.CmdImprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdImprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdImprimer.FlatAppearance.BorderSize = 0;
            this.CmdImprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdImprimer.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.CmdImprimer.ForeColor = System.Drawing.Color.White;
            this.CmdImprimer.Image = global::Axe_interDT.Properties.Resources.Printer_16x16;
            this.CmdImprimer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdImprimer.Location = new System.Drawing.Point(2, 2);
            this.CmdImprimer.Margin = new System.Windows.Forms.Padding(2);
            this.CmdImprimer.Name = "CmdImprimer";
            this.CmdImprimer.Size = new System.Drawing.Size(154, 35);
            this.CmdImprimer.TabIndex = 392;
            this.CmdImprimer.Text = "      Impr.";
            this.CmdImprimer.UseVisualStyleBackColor = false;
            this.CmdImprimer.Click += new System.EventHandler(this.cmdImprimer_Click);
            // 
            // frmImpression
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(471, 255);
            this.Controls.Add(this.flowLayoutPanel1);
            this.Controls.Add(this.panel1);
            this.MaximumSize = new System.Drawing.Size(487, 294);
            this.MinimumSize = new System.Drawing.Size(487, 294);
            this.Name = "frmImpression";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmImpression";
            this.Click += new System.EventHandler(this.frmImpression_Load);
            this.panel1.ResumeLayout(false);
            this.Frame1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox Frame1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.RadioButton optOption1;
        public System.Windows.Forms.RadioButton optOption3;
        public System.Windows.Forms.RadioButton optOption2;
        public System.Windows.Forms.RadioButton optOption4;
        public iTalk.iTalk_TextBox_Small2 txtFin;
        public iTalk.iTalk_TextBox_Small2 txtDebut;
        public iTalk.iTalk_TextBox_Small2 txtApartir;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button CmdImprimer;
        public System.Windows.Forms.Label Label1;
    }
}