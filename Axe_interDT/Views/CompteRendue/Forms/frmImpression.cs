﻿using Axe_interDT.Shared;
using Axe_interDT.Views.SharedViews;
using CrystalDecisions.CrystalReports.Engine;
using CrystalDecisions.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.CompteRendu
{
    public partial class frmImpression : Form
    {
        public frmImpression()
        {
            InitializeComponent();
        }
        short intReponse;
        ModAdo ModAdo1 = new ModAdo();
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdImprimer_Click(System.Object eventSender, System.EventArgs eventArgs)
        {

            string txtFormula = null;
            string sSelectionFormula = null;
            string sReturn = null;

            const string cFin = "ZZZZZZZZZZZZZZZZZZZZZZZZZZ";

          
            //tous les CR
            if ((optOption1.Checked == true))//tested
            {
                intReponse =Convert.ToInt16(Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(General.PROMPTOUTIMPR, General.TITRETOUTIMPR, MessageBoxButtons.OKCancel , MessageBoxIcon.Question));
                if ((intReponse == 1))
                {

                }
            }

            //a partir de
            if ((optOption2.Checked == true))
            {
                if ((string.IsNullOrEmpty(txtApartir.Text)))
                {
                    intReponse = Convert.ToInt16(Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(General.PROMPTSANSPARAM, General.TITRESANSPARAM, MessageBoxButtons.OKCancel , MessageBoxIcon.Exclamation));
                    if ((intReponse == 1))
                    {
                        txtApartir.Focus();
                    }
                }
                else
                {

                    txtFormula = "{Immeuble.CodeImmeuble} >= ('" + txtDebut.Text + "') and {Immeuble.CodeImmeuble} <= ('" + cFin + "')";

                }
            }

            //De A
            if ((optOption3.Checked == true))
            {
                if ((string.IsNullOrEmpty(txtDebut.Text)) || (string.IsNullOrEmpty(txtFin.Text)))
                {
                    intReponse = Convert.ToInt16(Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(General.PROMPTSANSPARAM, General.TITRESANSPARAM, MessageBoxButtons.OKCancel , MessageBoxIcon.Exclamation));
                    if ((intReponse == 1) && (string.IsNullOrEmpty(txtDebut.Text)))
                    {
                        txtDebut.Focus();
                    }
                    else if ((intReponse == 1) && (string.IsNullOrEmpty(txtFin.Text)))
                    {
                        txtFin.Focus();
                    }
                }
                else
                {

                    txtFormula = "{immeuble.codeimmeuble} >= ('" + txtDebut.Text + "') and {immeuble.codeimmeuble} <= ('" + txtFin.Text + "')";

                }
            }

            //page courante
            if ((optOption4.Checked == true))
            {

                txtFormula = "{immeuble.codeimmeuble}=('" + General.gVar + "')";

            }

            if (General.sExportCrImm == "1" && optOption4.Checked == true)
            {

                 fc_ExportCptRendu("WHERE  CodeImmeuble = ('" + StdSQLchaine.gFr_DoublerQuote(General.gVar.ToString()) + "')");
            

                //            ReDim tabFormulas(0)
                //            tabFormulas(0).sNom = ""
                //
                //            sSelectionFormula = ""
                //            sSelectionFormula = " {immeuble.codeimmeuble}=('" & gFr_DoublerQuote(gVar) & "')"
                //            sReturn = fc_ExportCrystal(sSelectionFormula, gsRpt & sPathReportCRv9, , , , , vbChecked)
                //
                //            If sReturn <> "" Then
                //
                //                Ouvrir Me.hWnd, sReturn
                //                Screen.MousePointer = 1
                //            End If


            }
            else
            {
                //===Impression des factures par Crystal Reports
                ReportDocument R = new ReportDocument();
                R.Load(General.gsRpt + General.CHEMINETATRelTech);
                CrystalReportFormView cr = new CrystalReportFormView(R, txtFormula);
                cr.ShowDialog();
               // var _with1 = CR1;
                //.WindowShowExportBtn = True
                //ReportFileName = gsPath & "\CR.rpt"
                //_with1.ReportFileName = General.gsRpt + General.CHEMINETATRelTech;
                //_with1.SelectionFormula = txtFormula;

                //.Destination = crptToPrinter
                //_with1.WindowShowPrintBtn = true;
                //_with1.WindowShowPrintSetupBtn = true;
                //_with1.WindowShowExportBtn = true;
                //_with1.Destination = Crystal.DestinationConstants.crptToWindow;

                //_with1.Action = 1;
                //Declenche C.R
                //.SelectionFormula = ""

            }

            this.Visible = false;
            return;
        

            //Erreurs.gFr_debug(ref " Impression des relevés techniques ", ref false);
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sWhere"></param>
        /// <returns></returns>
        public double fc_ExportCptRendu( string sWhere)
        {
            double functionReturnValue = 0;

            //CRAXDRT.Report Report = null;
            ReportDocument Report ;
            //CRAXDRT.Application crAppl = null;
            DataTable rs ;
            string sCodeimmeuble = null;
            string sNumeroFact = null;
            string sTitre = null;
            int i = 0;
            double nbRec = 0;
            object X = null;
            //CREXPORTLib.CrystalReportExport ReportExp = null;
            CrystalReportFormView ReportExp;
            string sFiltreDevis1 = null;
            string sRep = null;
            string sPathFile = null;


            // ERROR: Not supported in C#: OnErrorStatement


            rs = new DataTable();

            General.SQL = "SELECT     CodeImmeuble FROM         TechReleve ";

            General.SQL = General.SQL + sWhere;
            General.SQL = General.SQL + " ORDER BY codeimmeuble DESC ";

            rs=ModAdo1.fc_OpenRecordSet(General.SQL);

            // crAppl = new CRAXDRT.Application();
            //Report = crAppl.OpenReport(General.gsRpt + General.sPathReportCRv9);

            Report = new ReportDocument();
            Report.Load(General.gsRpt + General.sPathReportCRv9);
            //ReportExp = new CrystalReportFormView(Report, General.SQL);
          
           

            i = 0;
            nbRec = rs.Rows.Count;
            //lblLabelDeroule.Visible = True
            //lblDeroule.Visible = True
            //lblDeroule.Caption = i & " / " & nbRec
            Application.DoEvents();

            if (rs.Rows.Count>0)
            {

                //rs.MoveFirst();
                
                 foreach(DataRow Dr in rs.Rows)  
                {
                    i = i + 1;

                    //lblDeroule.Caption = i & " / " & nbRec

                    Application.DoEvents();

                    sCodeimmeuble = General.Trim(General.Replace(General.Replace(Dr["CodeImmeuble"] + "", ".", ""), "\"", ""));

                    //sNumeroFact = Replace(rs!NoFacture & "", "/", "-")

                    // sTitre = "Compte-rendu technique pour " & sCodeimmeuble & " du " & rs!DateFacture

                    sRep = General.cFolderCptRendu;
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeimmeuble);
                    //fc_CreateDossier (sCheminDossier & "\" & sCodeimmeuble & "\Factures ")
                    Dossier.fc_CreateDossier(General.sCheminDossier + "\\" + sCodeimmeuble + "\\" + sRep);

                    sPathFile = General.sCheminDossier + "\\" + sCodeimmeuble + "\\" + sRep + "\\Compte-rendus technique.pdf";

                    if (Dossier.fc_ControleFichier(sPathFile) == true)
                    {
                        if (Dossier.fc_SupprimeFichier(sPathFile) == false)
                            return functionReturnValue;

                    }
                    //Report.ParameterFields(1).AddDefaultValue "IDimmeuble." & rs.Fields("CodeImmeuble").value
                    //Report.ParameterFields.GetItemByName("IDimmeuble").SetCurrentValue rs.Fields("CodeImmeuble").value
                    Report.SetParameterValue("IDimmeuble", rs.Rows[0]["CodeImmeuble"].ToString());

                    Report.ExportToDisk(CrystalDecisions.Shared.ExportFormatType.PortableDocFormat, sPathFile);
                   

                    ModuleAPI.Ouvrir(sPathFile);

                    //                adocnn.Execute "UPDATE FactEntete SET ExportPDF=1 WHERE NoFacture='" & sNumeroFact & "'"
                    //                SQL = "INSERT INTO DOC_Documents ("
                    //                SQL = SQL & "DOC_Nat,"
                    //                SQL = SQL & "DOC_PhyPath,"
                    //                SQL = SQL & "DOC_VirPath,"
                    //                SQL = SQL & "DOC_DTPath,"
                    //                SQL = SQL & "DOC_Name,"
                    //                SQL = SQL & "DOC_DCreat,"
                    //                SQL = SQL & "DOC_DModif,"
                    //                SQL = SQL & "DOC_FType,"
                    //                SQL = SQL & "DOC_Imm,"
                    //                SQL = SQL & "DOC_InOut,"
                    //                SQL = SQL & "DOC_Sujet,"
                    //                SQL = SQL & "DOC_NoPiece"
                    //                SQL = SQL & ") VALUES ("
                    //                SQL = SQL & "'Factures',"
                    //                'SQL = SQL & "'\\DELOSTAL-DC-01\DataDT\" & sCodeimmeuble & sRep & "\Facture-" & Replace(sNumeroFact, "/", "-") & ".pdf',"
                    //                SQL = SQL & "'" & sCheminDossier & "\" & sCodeimmeuble & sRep & "\Facture-" & Replace(sNumeroFact, "/", "-") & ".pdf',"
                    //                SQL = SQL & "'../Data/" & sCodeimmeuble & sRep & "/Facture-" & Replace(sNumeroFact, "/", "-") & ".pdf',"
                    //                SQL = SQL & "'" & sCheminDossier & "\" & sCodeimmeuble & sRep & "\Facture-" & Replace(sNumeroFact, "/", "-") & ".pdf" & "',"
                    //                SQL = SQL & "'Facture-" & Replace(sNumeroFact, "/", "-") & ".pdf',"
                    //                SQL = SQL & "'" & Now & "',"
                    //                SQL = SQL & "'" & Now & "',"
                    //                SQL = SQL & "'PDF File',"
                    //                SQL = SQL & "'" & sCodeimmeuble & "',"
                    //                SQL = SQL & "'out',"
                    //                SQL = SQL & "'" & gFr_DoublerQuote(sTitre) & "',"
                    //                SQL = SQL & "'" & Replace(sNumeroFact, "/", "-") & ".pdf'"
                    //                SQL = SQL & ")"
                    //                adocnn.Execute SQL

                    //       adocnn.Execute "UPDATE FactEntete SET ExportPDF=1 WHERE NoFacture='" & sNumeroFact & "'"
                    //rs.MoveNext();
                }
            }
            //lblLabelDeroule.Visible = False
            //lblDeroule.Visible = False

            functionReturnValue = nbRec;
            return functionReturnValue;
      
            //lblLabelDeroule.Visible = False
            //lblDeroule.Visible = False

            //Error
            //functionReturnValue = 0;
            //err().Clear();
            //return functionReturnValue;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void frmImpression_Load(System.Object eventSender, System.EventArgs eventArgs)
        {
            txtFin.Enabled = false;
            txtDebut.Enabled = false;
            txtApartir.Enabled = false;
        }

      
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optOption1_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optOption1.Checked)
            {
                txtFin.Enabled = false;
                txtDebut.Enabled = false;
                txtApartir.Enabled = false;
                txtDebut.Text = "";
                txtFin.Text = "";
                txtApartir.Text = "";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optOption2_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optOption2.Checked)
            {
                txtFin.Enabled = false;
                txtDebut.Enabled = false;
                txtApartir.Enabled = true;
                txtApartir.Focus();
                txtDebut.Text = "";
                txtFin.Text = "";
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optOption3_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optOption3.Checked)
            {
                txtApartir.Enabled = false;
                txtFin.Enabled = true;
                txtDebut.Enabled = true;
                txtDebut.Focus();
                txtApartir.Text = "";
            }
        }


    }
}
