﻿namespace Axe_interDT.Views.CompteRendu
{
    partial class frmFOD
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.cmdMAJ = new System.Windows.Forms.Button();
            this.CmdFermer = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblLabels11 = new System.Windows.Forms.Label();
            this.txtFields3 = new iTalk.iTalk_TextBox_Small2();
            this.txtFields6 = new iTalk.iTalk_TextBox_Small2();
            this.txtFields5 = new iTalk.iTalk_TextBox_Small2();
            this.txtFields4 = new iTalk.iTalk_TextBox_Small2();
            this.txtFields8 = new iTalk.iTalk_TextBox_Small2();
            this.txtFields7 = new iTalk.iTalk_TextBox_Small2();
            this.txtFields2 = new iTalk.iTalk_TextBox_Small2();
            this.txtFields1 = new iTalk.iTalk_TextBox_Small2();
            this.txtFields0 = new iTalk.iTalk_TextBox_Small2();
            this.SSDBCombo10 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblLabels5 = new System.Windows.Forms.Label();
            this.lblLabels4 = new System.Windows.Forms.Label();
            this.lblLabels7 = new System.Windows.Forms.Label();
            this.lblLabels9 = new System.Windows.Forms.Label();
            this.lblLabels2 = new System.Windows.Forms.Label();
            this.lblLabels1 = new System.Windows.Forms.Label();
            this.lblLabels6 = new System.Windows.Forms.Label();
            this.lblLabels0 = new System.Windows.Forms.Label();
            this.lblLabels8 = new System.Windows.Forms.Label();
            this.lblLabels10 = new System.Windows.Forms.Label();
            this.lblLabels3 = new System.Windows.Forms.Label();
            this.SSDBCombo11 = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.Check2 = new System.Windows.Forms.CheckBox();
            this.txtFields9 = new iTalk.iTalk_TextBox_Small2();
            this.panel1 = new System.Windows.Forms.Panel();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.txtFields86 = new iTalk.iTalk_TextBox_Small2();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSDBCombo10)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSDBCombo11)).BeginInit();
            this.panel1.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cmdAjouter);
            this.flowLayoutPanel1.Controls.Add(this.cmdMAJ);
            this.flowLayoutPanel1.Controls.Add(this.CmdFermer);
            this.flowLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(133, 438);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(382, 41);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAjouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdAjouter.ForeColor = System.Drawing.Color.White;
            this.cmdAjouter.Image = global::Axe_interDT.Properties.Resources.Add_File_16x16;
            this.cmdAjouter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAjouter.Location = new System.Drawing.Point(2, 2);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(122, 35);
            this.cmdAjouter.TabIndex = 392;
            this.cmdAjouter.Text = "     Ajouter";
            this.cmdAjouter.UseVisualStyleBackColor = false;
            this.cmdAjouter.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // cmdMAJ
            // 
            this.cmdMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJ.FlatAppearance.BorderSize = 0;
            this.cmdMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJ.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdMAJ.ForeColor = System.Drawing.Color.White;
            this.cmdMAJ.Image = global::Axe_interDT.Properties.Resources.Ok_16x16;
            this.cmdMAJ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdMAJ.Location = new System.Drawing.Point(128, 2);
            this.cmdMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJ.Name = "cmdMAJ";
            this.cmdMAJ.Size = new System.Drawing.Size(122, 35);
            this.cmdMAJ.TabIndex = 393;
            this.cmdMAJ.Text = "     Valider";
            this.cmdMAJ.UseVisualStyleBackColor = false;
            this.cmdMAJ.Click += new System.EventHandler(this.cmdMAJ_Click);
            // 
            // CmdFermer
            // 
            this.CmdFermer.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.CmdFermer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdFermer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdFermer.FlatAppearance.BorderSize = 0;
            this.CmdFermer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdFermer.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.CmdFermer.ForeColor = System.Drawing.Color.White;
            this.CmdFermer.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.CmdFermer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.CmdFermer.Location = new System.Drawing.Point(254, 2);
            this.CmdFermer.Margin = new System.Windows.Forms.Padding(2);
            this.CmdFermer.Name = "CmdFermer";
            this.CmdFermer.Size = new System.Drawing.Size(122, 35);
            this.CmdFermer.TabIndex = 568;
            this.CmdFermer.Text = "     Fermer";
            this.CmdFermer.UseVisualStyleBackColor = false;
            this.CmdFermer.Click += new System.EventHandler(this.cmdFermer_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 42.56757F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 57.43243F));
            this.tableLayoutPanel1.Controls.Add(this.lblLabels11, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.txtFields3, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.txtFields6, 1, 9);
            this.tableLayoutPanel1.Controls.Add(this.txtFields5, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtFields4, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtFields8, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.txtFields7, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.txtFields2, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.txtFields1, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtFields0, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.SSDBCombo10, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblLabels5, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.lblLabels4, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblLabels7, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblLabels9, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblLabels2, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblLabels1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblLabels6, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblLabels0, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblLabels8, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblLabels10, 0, 9);
            this.tableLayoutPanel1.Controls.Add(this.lblLabels3, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.SSDBCombo11, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.Check2, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.txtFields9, 1, 11);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 21);
            this.tableLayoutPanel1.MaximumSize = new System.Drawing.Size(595, 408);
            this.tableLayoutPanel1.MinimumSize = new System.Drawing.Size(595, 408);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 13;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(595, 408);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // lblLabels11
            // 
            this.lblLabels11.AutoSize = true;
            this.lblLabels11.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLabels11.Location = new System.Drawing.Point(3, 341);
            this.lblLabels11.Name = "lblLabels11";
            this.lblLabels11.Size = new System.Drawing.Size(113, 19);
            this.lblLabels11.TabIndex = 572;
            this.lblLabels11.Text = "Jauge marque:";
            // 
            // txtFields3
            // 
            this.txtFields3.AccAcceptNumbersOnly = false;
            this.txtFields3.AccAllowComma = false;
            this.txtFields3.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFields3.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFields3.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFields3.AccHidenValue = "";
            this.txtFields3.AccNotAllowedChars = null;
            this.txtFields3.AccReadOnly = false;
            this.txtFields3.AccReadOnlyAllowDelete = false;
            this.txtFields3.AccRequired = false;
            this.txtFields3.BackColor = System.Drawing.Color.White;
            this.txtFields3.CustomBackColor = System.Drawing.Color.White;
            this.txtFields3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFields3.ForeColor = System.Drawing.Color.Black;
            this.txtFields3.Location = new System.Drawing.Point(255, 312);
            this.txtFields3.Margin = new System.Windows.Forms.Padding(2);
            this.txtFields3.MaxLength = 32767;
            this.txtFields3.Multiline = false;
            this.txtFields3.Name = "txtFields3";
            this.txtFields3.ReadOnly = false;
            this.txtFields3.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFields3.Size = new System.Drawing.Size(291, 27);
            this.txtFields3.TabIndex = 510;
            this.txtFields3.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFields3.UseSystemPasswordChar = false;
            // 
            // txtFields6
            // 
            this.txtFields6.AccAcceptNumbersOnly = false;
            this.txtFields6.AccAllowComma = false;
            this.txtFields6.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFields6.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFields6.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFields6.AccHidenValue = "";
            this.txtFields6.AccNotAllowedChars = null;
            this.txtFields6.AccReadOnly = false;
            this.txtFields6.AccReadOnlyAllowDelete = false;
            this.txtFields6.AccRequired = false;
            this.txtFields6.BackColor = System.Drawing.Color.White;
            this.txtFields6.CustomBackColor = System.Drawing.Color.White;
            this.txtFields6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFields6.ForeColor = System.Drawing.Color.Black;
            this.txtFields6.Location = new System.Drawing.Point(255, 281);
            this.txtFields6.Margin = new System.Windows.Forms.Padding(2);
            this.txtFields6.MaxLength = 32767;
            this.txtFields6.Multiline = false;
            this.txtFields6.Name = "txtFields6";
            this.txtFields6.ReadOnly = false;
            this.txtFields6.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFields6.Size = new System.Drawing.Size(291, 27);
            this.txtFields6.TabIndex = 509;
            this.txtFields6.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFields6.UseSystemPasswordChar = false;
            // 
            // txtFields5
            // 
            this.txtFields5.AccAcceptNumbersOnly = false;
            this.txtFields5.AccAllowComma = false;
            this.txtFields5.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFields5.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFields5.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFields5.AccHidenValue = "";
            this.txtFields5.AccNotAllowedChars = null;
            this.txtFields5.AccReadOnly = false;
            this.txtFields5.AccReadOnlyAllowDelete = false;
            this.txtFields5.AccRequired = false;
            this.txtFields5.BackColor = System.Drawing.Color.White;
            this.txtFields5.CustomBackColor = System.Drawing.Color.White;
            this.txtFields5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFields5.ForeColor = System.Drawing.Color.Black;
            this.txtFields5.Location = new System.Drawing.Point(255, 250);
            this.txtFields5.Margin = new System.Windows.Forms.Padding(2);
            this.txtFields5.MaxLength = 32767;
            this.txtFields5.Multiline = false;
            this.txtFields5.Name = "txtFields5";
            this.txtFields5.ReadOnly = false;
            this.txtFields5.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFields5.Size = new System.Drawing.Size(291, 27);
            this.txtFields5.TabIndex = 508;
            this.txtFields5.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFields5.UseSystemPasswordChar = false;
            // 
            // txtFields4
            // 
            this.txtFields4.AccAcceptNumbersOnly = false;
            this.txtFields4.AccAllowComma = false;
            this.txtFields4.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFields4.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFields4.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFields4.AccHidenValue = "";
            this.txtFields4.AccNotAllowedChars = null;
            this.txtFields4.AccReadOnly = false;
            this.txtFields4.AccReadOnlyAllowDelete = false;
            this.txtFields4.AccRequired = false;
            this.txtFields4.BackColor = System.Drawing.Color.White;
            this.txtFields4.CustomBackColor = System.Drawing.Color.White;
            this.txtFields4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFields4.ForeColor = System.Drawing.Color.Black;
            this.txtFields4.Location = new System.Drawing.Point(255, 219);
            this.txtFields4.Margin = new System.Windows.Forms.Padding(2);
            this.txtFields4.MaxLength = 32767;
            this.txtFields4.Multiline = false;
            this.txtFields4.Name = "txtFields4";
            this.txtFields4.ReadOnly = false;
            this.txtFields4.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFields4.Size = new System.Drawing.Size(291, 27);
            this.txtFields4.TabIndex = 507;
            this.txtFields4.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFields4.UseSystemPasswordChar = false;
            // 
            // txtFields8
            // 
            this.txtFields8.AccAcceptNumbersOnly = false;
            this.txtFields8.AccAllowComma = false;
            this.txtFields8.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFields8.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFields8.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFields8.AccHidenValue = "";
            this.txtFields8.AccNotAllowedChars = null;
            this.txtFields8.AccReadOnly = false;
            this.txtFields8.AccReadOnlyAllowDelete = false;
            this.txtFields8.AccRequired = false;
            this.txtFields8.BackColor = System.Drawing.Color.White;
            this.txtFields8.CustomBackColor = System.Drawing.Color.White;
            this.txtFields8.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFields8.ForeColor = System.Drawing.Color.Black;
            this.txtFields8.Location = new System.Drawing.Point(255, 188);
            this.txtFields8.Margin = new System.Windows.Forms.Padding(2);
            this.txtFields8.MaxLength = 32767;
            this.txtFields8.Multiline = false;
            this.txtFields8.Name = "txtFields8";
            this.txtFields8.ReadOnly = false;
            this.txtFields8.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFields8.Size = new System.Drawing.Size(291, 27);
            this.txtFields8.TabIndex = 506;
            this.txtFields8.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFields8.UseSystemPasswordChar = false;
            // 
            // txtFields7
            // 
            this.txtFields7.AccAcceptNumbersOnly = false;
            this.txtFields7.AccAllowComma = false;
            this.txtFields7.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFields7.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFields7.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFields7.AccHidenValue = "";
            this.txtFields7.AccNotAllowedChars = null;
            this.txtFields7.AccReadOnly = false;
            this.txtFields7.AccReadOnlyAllowDelete = false;
            this.txtFields7.AccRequired = false;
            this.txtFields7.BackColor = System.Drawing.Color.White;
            this.txtFields7.CustomBackColor = System.Drawing.Color.White;
            this.txtFields7.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFields7.ForeColor = System.Drawing.Color.Black;
            this.txtFields7.Location = new System.Drawing.Point(255, 157);
            this.txtFields7.Margin = new System.Windows.Forms.Padding(2);
            this.txtFields7.MaxLength = 32767;
            this.txtFields7.Multiline = false;
            this.txtFields7.Name = "txtFields7";
            this.txtFields7.ReadOnly = false;
            this.txtFields7.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFields7.Size = new System.Drawing.Size(291, 27);
            this.txtFields7.TabIndex = 505;
            this.txtFields7.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFields7.UseSystemPasswordChar = false;
            // 
            // txtFields2
            // 
            this.txtFields2.AccAcceptNumbersOnly = false;
            this.txtFields2.AccAllowComma = false;
            this.txtFields2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFields2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFields2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFields2.AccHidenValue = "";
            this.txtFields2.AccNotAllowedChars = null;
            this.txtFields2.AccReadOnly = false;
            this.txtFields2.AccReadOnlyAllowDelete = false;
            this.txtFields2.AccRequired = false;
            this.txtFields2.BackColor = System.Drawing.Color.White;
            this.txtFields2.CustomBackColor = System.Drawing.Color.White;
            this.txtFields2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFields2.ForeColor = System.Drawing.Color.Black;
            this.txtFields2.Location = new System.Drawing.Point(255, 126);
            this.txtFields2.Margin = new System.Windows.Forms.Padding(2);
            this.txtFields2.MaxLength = 32767;
            this.txtFields2.Multiline = false;
            this.txtFields2.Name = "txtFields2";
            this.txtFields2.ReadOnly = false;
            this.txtFields2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFields2.Size = new System.Drawing.Size(291, 27);
            this.txtFields2.TabIndex = 504;
            this.txtFields2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFields2.UseSystemPasswordChar = false;
            // 
            // txtFields1
            // 
            this.txtFields1.AccAcceptNumbersOnly = false;
            this.txtFields1.AccAllowComma = false;
            this.txtFields1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFields1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFields1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFields1.AccHidenValue = "";
            this.txtFields1.AccNotAllowedChars = null;
            this.txtFields1.AccReadOnly = false;
            this.txtFields1.AccReadOnlyAllowDelete = false;
            this.txtFields1.AccRequired = false;
            this.txtFields1.BackColor = System.Drawing.Color.White;
            this.txtFields1.CustomBackColor = System.Drawing.Color.White;
            this.txtFields1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFields1.ForeColor = System.Drawing.Color.Black;
            this.txtFields1.Location = new System.Drawing.Point(255, 95);
            this.txtFields1.Margin = new System.Windows.Forms.Padding(2);
            this.txtFields1.MaxLength = 32767;
            this.txtFields1.Multiline = false;
            this.txtFields1.Name = "txtFields1";
            this.txtFields1.ReadOnly = false;
            this.txtFields1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFields1.Size = new System.Drawing.Size(291, 27);
            this.txtFields1.TabIndex = 503;
            this.txtFields1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFields1.UseSystemPasswordChar = false;
            // 
            // txtFields0
            // 
            this.txtFields0.AccAcceptNumbersOnly = false;
            this.txtFields0.AccAllowComma = false;
            this.txtFields0.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFields0.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFields0.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFields0.AccHidenValue = "";
            this.txtFields0.AccNotAllowedChars = null;
            this.txtFields0.AccReadOnly = false;
            this.txtFields0.AccReadOnlyAllowDelete = false;
            this.txtFields0.AccRequired = false;
            this.txtFields0.BackColor = System.Drawing.Color.White;
            this.txtFields0.CustomBackColor = System.Drawing.Color.White;
            this.txtFields0.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFields0.ForeColor = System.Drawing.Color.Black;
            this.txtFields0.Location = new System.Drawing.Point(255, 64);
            this.txtFields0.Margin = new System.Windows.Forms.Padding(2);
            this.txtFields0.MaxLength = 32767;
            this.txtFields0.Multiline = false;
            this.txtFields0.Name = "txtFields0";
            this.txtFields0.ReadOnly = false;
            this.txtFields0.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFields0.Size = new System.Drawing.Size(291, 27);
            this.txtFields0.TabIndex = 502;
            this.txtFields0.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFields0.UseSystemPasswordChar = false;
            // 
            // SSDBCombo10
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSDBCombo10.DisplayLayout.Appearance = appearance1;
            this.SSDBCombo10.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSDBCombo10.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.SSDBCombo10.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSDBCombo10.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.SSDBCombo10.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSDBCombo10.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.SSDBCombo10.DisplayLayout.MaxColScrollRegions = 1;
            this.SSDBCombo10.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSDBCombo10.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSDBCombo10.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.SSDBCombo10.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSDBCombo10.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.SSDBCombo10.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSDBCombo10.DisplayLayout.Override.CellAppearance = appearance8;
            this.SSDBCombo10.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSDBCombo10.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.SSDBCombo10.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.SSDBCombo10.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.SSDBCombo10.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSDBCombo10.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.SSDBCombo10.DisplayLayout.Override.RowAppearance = appearance11;
            this.SSDBCombo10.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSDBCombo10.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.SSDBCombo10.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSDBCombo10.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSDBCombo10.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSDBCombo10.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSDBCombo10.Location = new System.Drawing.Point(256, 34);
            this.SSDBCombo10.Name = "SSDBCombo10";
            this.SSDBCombo10.Size = new System.Drawing.Size(185, 27);
            this.SSDBCombo10.TabIndex = 396;
            // 
            // lblLabels5
            // 
            this.lblLabels5.AutoSize = true;
            this.lblLabels5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLabels5.Location = new System.Drawing.Point(3, 0);
            this.lblLabels5.Name = "lblLabels5";
            this.lblLabels5.Size = new System.Drawing.Size(90, 19);
            this.lblLabels5.TabIndex = 384;
            this.lblLabels5.Text = "Soute Fioul:";
            // 
            // lblLabels4
            // 
            this.lblLabels4.AutoSize = true;
            this.lblLabels4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLabels4.Location = new System.Drawing.Point(3, 217);
            this.lblLabels4.Name = "lblLabels4";
            this.lblLabels4.Size = new System.Drawing.Size(145, 19);
            this.lblLabels4.TabIndex = 386;
            this.lblLabels4.Text = "Situation dépotage:";
            // 
            // lblLabels7
            // 
            this.lblLabels7.AutoSize = true;
            this.lblLabels7.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLabels7.Location = new System.Drawing.Point(3, 155);
            this.lblLabels7.Name = "lblLabels7";
            this.lblLabels7.Size = new System.Drawing.Size(137, 19);
            this.lblLabels7.TabIndex = 387;
            this.lblLabels7.Text = "Diam Remplissage:";
            // 
            // lblLabels9
            // 
            this.lblLabels9.AutoSize = true;
            this.lblLabels9.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLabels9.Location = new System.Drawing.Point(3, 248);
            this.lblLabels9.Name = "lblLabels9";
            this.lblLabels9.Size = new System.Drawing.Size(117, 19);
            this.lblLabels9.TabIndex = 388;
            this.lblLabels9.Text = "Situation Event:";
            // 
            // lblLabels2
            // 
            this.lblLabels2.AutoSize = true;
            this.lblLabels2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLabels2.Location = new System.Drawing.Point(3, 124);
            this.lblLabels2.Name = "lblLabels2";
            this.lblLabels2.Size = new System.Drawing.Size(123, 19);
            this.lblLabels2.TabIndex = 389;
            this.lblLabels2.Text = "Capacité Cuve 3:";
            // 
            // lblLabels1
            // 
            this.lblLabels1.AutoSize = true;
            this.lblLabels1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLabels1.Location = new System.Drawing.Point(3, 93);
            this.lblLabels1.Name = "lblLabels1";
            this.lblLabels1.Size = new System.Drawing.Size(123, 19);
            this.lblLabels1.TabIndex = 390;
            this.lblLabels1.Text = "Capacité Cuve 2:";
            // 
            // lblLabels6
            // 
            this.lblLabels6.AutoSize = true;
            this.lblLabels6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLabels6.Location = new System.Drawing.Point(3, 31);
            this.lblLabels6.Name = "lblLabels6";
            this.lblLabels6.Size = new System.Drawing.Size(84, 19);
            this.lblLabels6.TabIndex = 391;
            this.lblLabels6.Text = "Cuve Fioul:";
            // 
            // lblLabels0
            // 
            this.lblLabels0.AutoSize = true;
            this.lblLabels0.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLabels0.Location = new System.Drawing.Point(3, 62);
            this.lblLabels0.Name = "lblLabels0";
            this.lblLabels0.Size = new System.Drawing.Size(123, 19);
            this.lblLabels0.TabIndex = 385;
            this.lblLabels0.Text = "Capacité Cuve 1:";
            // 
            // lblLabels8
            // 
            this.lblLabels8.AutoSize = true;
            this.lblLabels8.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLabels8.Location = new System.Drawing.Point(3, 186);
            this.lblLabels8.Name = "lblLabels8";
            this.lblLabels8.Size = new System.Drawing.Size(91, 19);
            this.lblLabels8.TabIndex = 392;
            this.lblLabels8.Text = "Diam Event:";
            // 
            // lblLabels10
            // 
            this.lblLabels10.AutoSize = true;
            this.lblLabels10.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLabels10.Location = new System.Drawing.Point(3, 279);
            this.lblLabels10.Name = "lblLabels10";
            this.lblLabels10.Size = new System.Drawing.Size(144, 19);
            this.lblLabels10.TabIndex = 393;
            this.lblLabels10.Text = "Heures de livraison:";
            // 
            // lblLabels3
            // 
            this.lblLabels3.AutoSize = true;
            this.lblLabels3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblLabels3.Location = new System.Drawing.Point(3, 310);
            this.lblLabels3.Name = "lblLabels3";
            this.lblLabels3.Size = new System.Drawing.Size(62, 19);
            this.lblLabels3.TabIndex = 394;
            this.lblLabels3.Text = "Nature:";
            // 
            // SSDBCombo11
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSDBCombo11.DisplayLayout.Appearance = appearance13;
            this.SSDBCombo11.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSDBCombo11.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.SSDBCombo11.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSDBCombo11.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.SSDBCombo11.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSDBCombo11.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.SSDBCombo11.DisplayLayout.MaxColScrollRegions = 1;
            this.SSDBCombo11.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSDBCombo11.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSDBCombo11.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.SSDBCombo11.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSDBCombo11.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.SSDBCombo11.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSDBCombo11.DisplayLayout.Override.CellAppearance = appearance20;
            this.SSDBCombo11.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSDBCombo11.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.SSDBCombo11.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.SSDBCombo11.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.SSDBCombo11.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSDBCombo11.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.SSDBCombo11.DisplayLayout.Override.RowAppearance = appearance23;
            this.SSDBCombo11.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSDBCombo11.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.SSDBCombo11.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSDBCombo11.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSDBCombo11.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSDBCombo11.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSDBCombo11.Location = new System.Drawing.Point(256, 3);
            this.SSDBCombo11.Name = "SSDBCombo11";
            this.SSDBCombo11.Size = new System.Drawing.Size(185, 27);
            this.SSDBCombo11.TabIndex = 395;
            // 
            // Check2
            // 
            this.Check2.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.Check2, 2);
            this.Check2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Check2.Location = new System.Drawing.Point(3, 375);
            this.Check2.Name = "Check2";
            this.Check2.Size = new System.Drawing.Size(253, 23);
            this.Check2.TabIndex = 570;
            this.Check2.Text = "Event visible du point de livraison";
            this.Check2.UseVisualStyleBackColor = true;
            // 
            // txtFields9
            // 
            this.txtFields9.AccAcceptNumbersOnly = false;
            this.txtFields9.AccAllowComma = false;
            this.txtFields9.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFields9.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFields9.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFields9.AccHidenValue = "";
            this.txtFields9.AccNotAllowedChars = null;
            this.txtFields9.AccReadOnly = false;
            this.txtFields9.AccReadOnlyAllowDelete = false;
            this.txtFields9.AccRequired = false;
            this.txtFields9.BackColor = System.Drawing.Color.White;
            this.txtFields9.CustomBackColor = System.Drawing.Color.White;
            this.txtFields9.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFields9.ForeColor = System.Drawing.Color.Black;
            this.txtFields9.Location = new System.Drawing.Point(255, 343);
            this.txtFields9.Margin = new System.Windows.Forms.Padding(2);
            this.txtFields9.MaxLength = 32767;
            this.txtFields9.Multiline = false;
            this.txtFields9.Name = "txtFields9";
            this.txtFields9.ReadOnly = false;
            this.txtFields9.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFields9.Size = new System.Drawing.Size(291, 27);
            this.txtFields9.TabIndex = 571;
            this.txtFields9.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFields9.UseSystemPasswordChar = false;
            // 
            // panel1
            // 
            this.panel1.Anchor = ((System.Windows.Forms.AnchorStyles)((((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left) 
            | System.Windows.Forms.AnchorStyles.Right)));
            this.panel1.Controls.Add(this.Frame1);
            this.panel1.Controls.Add(this.flowLayoutPanel1);
            this.panel1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.panel1.Location = new System.Drawing.Point(0, 38);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(616, 503);
            this.panel1.TabIndex = 512;
            // 
            // Frame1
            // 
            this.Frame1.Controls.Add(this.tableLayoutPanel1);
            this.Frame1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Frame1.Location = new System.Drawing.Point(3, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(601, 432);
            this.Frame1.TabIndex = 2;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "Fioul";
            // 
            // txtFields86
            // 
            this.txtFields86.AccAcceptNumbersOnly = false;
            this.txtFields86.AccAllowComma = false;
            this.txtFields86.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFields86.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFields86.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFields86.AccHidenValue = "";
            this.txtFields86.AccNotAllowedChars = null;
            this.txtFields86.AccReadOnly = false;
            this.txtFields86.AccReadOnlyAllowDelete = false;
            this.txtFields86.AccRequired = false;
            this.txtFields86.BackColor = System.Drawing.Color.White;
            this.txtFields86.CustomBackColor = System.Drawing.Color.White;
            this.txtFields86.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtFields86.ForeColor = System.Drawing.Color.Black;
            this.txtFields86.Location = new System.Drawing.Point(41, 11);
            this.txtFields86.Margin = new System.Windows.Forms.Padding(2);
            this.txtFields86.MaxLength = 32767;
            this.txtFields86.Multiline = false;
            this.txtFields86.Name = "txtFields86";
            this.txtFields86.ReadOnly = false;
            this.txtFields86.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFields86.Size = new System.Drawing.Size(135, 27);
            this.txtFields86.TabIndex = 511;
            this.txtFields86.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFields86.UseSystemPasswordChar = false;
            this.txtFields86.Visible = false;
            // 
            // frmFOD
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(628, 547);
            this.Controls.Add(this.panel1);
            this.Controls.Add(this.txtFields86);
            this.Name = "frmFOD";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmFOD";
            this.Load += new System.EventHandler(this.frmFOD_Load);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSDBCombo10)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSDBCombo11)).EndInit();
            this.panel1.ResumeLayout(false);
            this.Frame1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdAjouter;
        public System.Windows.Forms.Button cmdMAJ;
        public System.Windows.Forms.Button CmdFermer;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.Label lblLabels5;
        public System.Windows.Forms.Label lblLabels0;
        public System.Windows.Forms.Label lblLabels4;
        public System.Windows.Forms.Label lblLabels7;
        public System.Windows.Forms.Label lblLabels9;
        public System.Windows.Forms.Label lblLabels2;
        public System.Windows.Forms.Label lblLabels1;
        public System.Windows.Forms.Label lblLabels6;
        public System.Windows.Forms.Label lblLabels8;
        public System.Windows.Forms.Label lblLabels10;
        public System.Windows.Forms.Label lblLabels3;
        private Infragistics.Win.UltraWinGrid.UltraCombo SSDBCombo10;
        private Infragistics.Win.UltraWinGrid.UltraCombo SSDBCombo11;
        public iTalk.iTalk_TextBox_Small2 txtFields3;
        public iTalk.iTalk_TextBox_Small2 txtFields6;
        public iTalk.iTalk_TextBox_Small2 txtFields5;
        public iTalk.iTalk_TextBox_Small2 txtFields4;
        public iTalk.iTalk_TextBox_Small2 txtFields8;
        public iTalk.iTalk_TextBox_Small2 txtFields7;
        public iTalk.iTalk_TextBox_Small2 txtFields2;
        public iTalk.iTalk_TextBox_Small2 txtFields1;
        public iTalk.iTalk_TextBox_Small2 txtFields0;
        public iTalk.iTalk_TextBox_Small2 txtFields86;
        public System.Windows.Forms.CheckBox Check2;
        public System.Windows.Forms.Label lblLabels11;
        public iTalk.iTalk_TextBox_Small2 txtFields9;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.GroupBox Frame1;
    }
}