﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.CompteRendu
{
    public partial class frmCEE : Form
    {
        public frmCEE()
        {
            InitializeComponent();
        }
        ModAdo ModAdo1 = new ModAdo();
        DataTable rs;
        DataRow dr;
       
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>

        private void cmdFermer_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            this.Close();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdMAJ_Click(System.Object eventSender, System.EventArgs eventArgs)
        {

            bool bEof = false;

            if (rs.Rows.Count == 0)
            {
                bEof = true;            
               rs.Rows.Add(rs.NewRow());
                rs.Rows[0]["CodeImmeuble"] = txtCodeImmeuble.Text + "";
                // rs.Fields("CodeImmeuble").Value = txtCodeImmeuble.Text + "";
            }

            rs.Rows[0]["CEEI_NbLogement"] = txtCEEI_NbLogement.Text ;
            rs.Rows[0]["CEEI_NbLogement"] = txtCEEI_NbLogement.Text;
            rs.Rows[0]["CEEI_Nb1piece"] = txtCEEI_Nb1piece.Text ;
            rs.Rows[0]["CEEI_Nb2piece"] = txtCEEI_Nb2piece.Text ;
            rs.Rows[0]["CEEI_Nb3piece"] = txtCEEI_Nb3piece.Text ;
            rs.Rows[0]["CEEI_Nb4piece"] = txtCEEI_Nb4piece.Text;
            rs.Rows[0]["CEEI_Nb5piece"] = txtCEEI_Nb5piece.Text ;
            rs.Rows[0]["CEEI_Nb6piece"] = txtCEEI_Nb6piece.Text;
            rs.Rows[0]["CEEI_CommOuBureau"] = txtCEEI_CommOuBureau.Text ;
            rs.Rows[0]["CEEI_ChauffageSeul"] = General.nz((chkCEEI_ChauffageSeul.CheckState), 0);
            rs.Rows[0]["CEEI_ChauffEcs"] = General.nz((chkCEEI_ChauffEcs.CheckState), 0);
            rs.Rows[0]["CEEI_ConstrAvt1975"] = General.nz((chkCEEI_ConstrAvt1975.CheckState), 0);
            rs.Rows[0]["CEEI_ConstAp1975"] = General.nz((chkCEEI_ConstAp1975.CheckState), 0);
            
           
            ModAdo1.Update();
            if (string.IsNullOrEmpty(rs.Rows[0]["CEEI_Noauto"].ToString()))
            {
                using (var tmp = new ModAdo())
                    rs.Rows[0]["CEEI_Noauto"] = tmp.fc_ADOlibelle("select max(CEEI_Noauto) from CEEI_CeeImmeuble");

                ModAdo1.Update();
            }
           

            return;
     
            //Erreurs.gFr_debug(this.Name + "cmdMAJ_Click");

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmCEE_Load(object sender, EventArgs e)
        {
            string sSQL = null;


            if (ModMain.bActivate == true)
            {

                sSQL = "SELECT   CEEI_Noauto, CodeImmeuble,CEEI_NbLogement, CEEI_Nb1piece, CEEI_Nb2piece," 
                    + " CEEI_Nb3piece, CEEI_Nb4piece, CEEI_Nb5piece, CEEI_Nb6piece," 
                    + " CEEI_CommOuBureau , CEEI_ChauffageSeul," 
                    + " CEEI_ChauffEcs, CEEI_ConstrAvt1975, CEEI_ConstAp1975" 
                    + " From CEEI_CeeImmeuble" + " WHERE CodeImmeuble = '" 
                    + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";


                rs = ModAdo1.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count>0)
                {
                    txtCEEI_NbLogement.Text = rs.Rows[0]["CEEI_NbLogement"].ToString();
                    txtCEEI_Nb1piece.Text = rs.Rows[0]["CEEI_Nb1piece"].ToString();
                    txtCEEI_Nb2piece.Text = rs.Rows[0]["CEEI_Nb2piece"].ToString();
                    txtCEEI_Nb3piece.Text = rs.Rows[0]["CEEI_Nb3piece"].ToString();
                    txtCEEI_Nb4piece.Text = rs.Rows[0]["CEEI_Nb3piece"].ToString();
                    txtCEEI_Nb5piece.Text = rs.Rows[0]["CEEI_Nb5piece"].ToString();
                    txtCEEI_Nb6piece.Text = rs.Rows[0]["CEEI_Nb6piece"].ToString();
                    txtCEEI_CommOuBureau.Text = rs.Rows[0]["CEEI_CommOuBureau"].ToString();
                   
                    chkCEEI_ChauffageSeul.Checked =Convert.ToBoolean(Convert.ToUInt16(rs.Rows[0]["CEEI_ChauffageSeul"].ToString()));
                    chkCEEI_ChauffEcs.Checked = Convert.ToBoolean(General.nz( rs.Rows[0]["CEEI_ChauffEcs"],  0));
                    chkCEEI_ConstrAvt1975.Checked = Convert.ToBoolean(Convert.ToInt16(General.nz( rs.Rows[0]["CEEI_ConstrAvt1975"].ToString(),  0)));
                    chkCEEI_ConstAp1975.Checked =Convert.ToBoolean(General.nz( rs.Rows[0]["CEEI_ConstAp1975"],  0));
                }
                else
                {
                    txtCEEI_NbLogement.Text = "";
                    txtCEEI_Nb1piece.Text = "";
                    txtCEEI_Nb2piece.Text = "";
                    txtCEEI_Nb3piece.Text = "";
                    txtCEEI_Nb4piece.Text = "";
                    txtCEEI_Nb5piece.Text = "";
                    txtCEEI_Nb6piece.Text = "";
                    txtCEEI_CommOuBureau.Text = "";
                    chkCEEI_ChauffageSeul.CheckState = CheckState.Unchecked;
                    chkCEEI_ChauffEcs.CheckState = CheckState.Unchecked;
                    chkCEEI_ConstrAvt1975.CheckState = CheckState.Unchecked;
                    chkCEEI_ConstAp1975.CheckState = CheckState.Unchecked;

                }
            }

            ModMain.bActivate = false;
            return;
        
            //Erreurs.gFr_debug(this.Name + "cmdMAJ_Click");
        }


        private void frmCEE_FormClosed(System.Object eventSender, System.Windows.Forms.FormClosedEventArgs eventArgs)
        {
            
            if ((rs != null))
            {
              
                rs = null;
            }
        }

   
    }

}
