﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.CompteRendu
{
    public partial class frmChauffage1 : Form
    {
        ModAdo ModAdo1;
        ModAdo ModAdoToUpdate;
        DataTable DatPrimaryRS;
        public frmChauffage1()
        {
            InitializeComponent();
        }
        private void ChauffQuit_Click(Object eventSender, EventArgs eventArgs)
        {
            this.Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void frmChauffage1_Load(Object eventSender, EventArgs eventArgs)
        {
            cmdAjouter.Enabled = false;
            cmdAjouter.Visible = false;
            cmdMAJ.Enabled = false;

            //ouverture des data pour les marque de materiel
            DataTable Data1 = new DataTable();
            ModAdo1 = new ModAdo();

            Data1 = ModAdo1.fc_OpenRecordSet("SELECT * FROM MarqueMateriel WHERE CodeUtilisation=101");

            //remplissage de la liste des combo

            sheridan.InitialiseCombo(Combo10, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=101", "Marque", false, null, "");
            sheridan.InitialiseCombo(Combo11, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=101", "Marque", false, null, "");
            sheridan.InitialiseCombo(Combo20, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=103", "Marque", false, null, "");

            sheridan.InitialiseCombo(Combo100, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=115", "Marque", false, null, "");

            sheridan.InitialiseCombo(Combo20, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=115", "Marque", false, null, "");
            sheridan.InitialiseCombo(Combo21, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=115", "Marque", false, null, "");

            sheridan.InitialiseCombo(Combo30, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=106", "Marque", false, null, "");
            sheridan.InitialiseCombo(Combo31, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=106", "Marque", false, null, "");
            sheridan.InitialiseCombo(Combo32, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=106", "Marque", false, null, "");

            sheridan.InitialiseCombo(Combo40, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=107", "Marque", false, null, "");
            sheridan.InitialiseCombo(Combo41, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=107", "Marque", false, null, "");


            sheridan.InitialiseCombo(Combo6, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=108", "Marque", false, null, "");

            sheridan.InitialiseCombo(Combo8, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=109", "Marque", false, null, "");

            sheridan.InitialiseCombo(Combo5, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=111", "Marque", false, null, "");

            sheridan.InitialiseCombo(Combo7, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=113", "Marque", false, null, "");

            sheridan.InitialiseCombo(Combo9, "SELECT Marque FROM MarqueMateriel WHERE CodeUtilisation=114", "Marque", false, null, "");

            this.Text = this.Text + " pour l'immeuble " + General.gVar;
            //ToDo

            General.SQL = "SELECT * FROM MaterielChauffage " + "WHERE CodeImmeuble='" + General.gVar + "'";
            ModAdoToUpdate = new ModAdo();
            DatPrimaryRS = ModAdoToUpdate.fc_OpenRecordSet(General.SQL);
            if (DatPrimaryRS.Rows.Count == 0)
            {
                cmdAjouter.Enabled = true;
                cmdAjouter_Click(cmdAjouter, new System.EventArgs());
            }
            else
            {
                cmdMAJ.Enabled = true;

                var row = DatPrimaryRS.Rows[0];
                Combo10.Text = row["ChaudiereMarque1"].ToString();
                txtFields28.Text = row["ChaudiereType1"].ToString();
                txtFields48.Text = row["ChaudierePuissance1"].ToString();
                Text1.Text = row["ChaudiereNbre1"].ToString();
                txtFields16.Text = row["ChaudiereFluide1"].ToString();
                txtFields32.Text = row["ChaudiereTypeBriquetage1"].ToString();
                txtFields13.Text = row["ChaudiereEtatBriquetage1"].ToString();
                Combo20.Text = row["BruleurMarque1"].ToString();
                txtFields5.Text = row["BruleurType1"].ToString();
                Text5.Text = row["BruleurNbre1"].ToString();
                txtFields9.Text = row["BruleurSystem1"].ToString();
                Text8.Text = row["BruleurMarqueB1"].ToString();
                Text9.Text = row["BruleurTypeB1"].ToString();
                Check2.Checked =row["BruleurDoubleSecurite1"].ToString().ToLower() == "true";
                txtFields10.Text = row["ChemineeHauteur1"].ToString();
                txtFields37.Text = row["ChemineeSection1"].ToString();
                Check9.Checked = row["ChemineeRegulateur1"].ToString().ToLower() == "true";
                txtFields12.Text = row["ChemineeLongueur1"].ToString();
                txtFields11.Text = row["ChemineeDiam1"].ToString();
                txtFields19.Text = row["CarneauLongueur1"].ToString();
                txtFields18.Text = row["CarneauDiam1"].ToString();
                Combo11.Text = row["ChaudiereMarque2"].ToString();
                txtFields72.Text = row["ChaudiereType2"].ToString();
                txtFields71.Text = row["ChaudierePuissance2"].ToString();
                Text2.Text = row["ChaudiereNbre2"].ToString();
                txtFields51.Text = row["ChaudiereFluide2"].ToString();
                txtFields50.Text = row["ChaudiereTypeBriquetage2"].ToString();
                txtFields49.Text = row["EtatBriquetage2"].ToString();
                Combo21.Text = row["BruleurMarque2"].ToString();
                txtFields2.Text = row["BruleurType2"].ToString();
                Text6.Text = row["BruleurNbre2"].ToString();
                txtFields4.Text = row["BruleurSysteme2"].ToString();
                Text10.Text = row["BruleurMarqueB2"].ToString();
                Text11.Text = row["BruleurTypeB2"].ToString();
                Check4.Checked = row["BruleurDoubleSecurite2"].ToString().ToLower() == "true";
                txtFields24.Text = row["ChemineeHauteur2"].ToString();
                txtFields35.Text = row["ChemineeSection2"].ToString();
                Check10.Checked =  row["ChemineeRegulateur2"].ToString().ToLower() == "true";
                txtFields22.Text = row["ChemineeLongueur2"].ToString();
                txtFields23.Text = row["ChemineeDiam2"].ToString();
                txtFields0.Text = row["CarneauLongueur2"].ToString();
                txtFields21.Text = row["CarneauDiam2"].ToString();
                Combo30.Text = row["PompeCHMarque1"].ToString();
                txtFields38.Text = row["PompeCHType1"].ToString();
                txtFields1.Text = row["PompeCHNbre1"].ToString();
                Check13.Checked = row["PompeCHVanne1"].ToString().ToLower() == "true";
                Combo31.Text = row["PompeCHMarque2"].ToString();
                txtFields39.Text = row["PompeCHType2"].ToString();
                txtFields3.Text = row["PompeCHNbre2"].ToString();
                Check14.Checked =  row["PompeCHVanne2"].ToString().ToLower() == "true";
                Combo40.Text = row["PompeRECHMarque"].ToString();
                txtFields57.Text = row["PompeRECHType"].ToString();
                txtFields56.Text = row["PompeRECHNbre"].ToString();
                Check20.Checked =  row["PompeRECHAsser"].ToString().ToLower() == "true";
                txtFields38.Text = row["PompeCHType1"].ToString();
                Check1.Checked =  row["PompeRECHVanne"].ToString().ToLower() == "true";
                Combo6.Text = row["RegulationMarque"].ToString();
                txtFields64.Text = row["RegulationType"].ToString();
                txtFields34.Text = row["RegulationNbre"].ToString();
                Check19.Checked = row["RegulationActionBruleur"].ToString().ToLower() == "true";
                Check210.Checked = row["RegulationActionVanne3"].ToString().ToLower() == "true";
                Check211.Checked =  row["RegulationActionVanne4"].ToString().ToLower() == "true";
                Combo32.Text = row["PompePUMarque"].ToString();
                txtFields54.Text = row["PompePUType"].ToString();
                txtFields59.Text = row["PompePUHauteur"].ToString();
                txtFields55.Text = row["PompePUNbre"].ToString();
                Combo100.Text = row["CptGazMarque"].ToString();
                txtFields15.Text = row["CptGazType"].ToString();
                txtFields6.Text = row["CptGazNum"].ToString();
                Check6.Checked = row["ByPass"].ToString().ToLower() == "true";
                Check213.Checked = row["CleBarrage"].ToString().ToLower() == "true";
                Check212.Checked =  row["CleExt"].ToString().ToLower() == "true";
                Combo41.Text = row["PompeRECHMarque2"].ToString();
                txtFields17.Text = row["PompeRECHType2"].ToString();
                txtFields20.Text = row["PompeRECHNbre2"].ToString();
                Check3.Checked = row["PompeRECHAsser2"].ToString().ToLower() == "true";
                Check7.Checked = row["PompeRECHVanne2"].ToString().ToLower() == "true";
                txtFields61.Text = row["VanneMarque"].ToString();
                txtFields60.Text = row["VanneType"].ToString();
                Text15.Text = row["VanneNbre"].ToString();
                txtFields14.Text = row["VanneSecteur"].ToString();
                Combo5.Text = row["PompeTRMarque"].ToString();
                txtFields7.Text = row["PompeTRNbre1"].ToString();
                txtFields62.Text = row["PompeTRType"].ToString();
                Text12.Text = row["PompeTRHauteur"].ToString();
                Text13.Text = row["PompeTRNourrice"].ToString();
                Text14.Text = row["PompeTRCapacite"].ToString();
                txtFields86.Text = row["CodeImmeuble"].ToString();
                Check22.Checked =  row["CptEau"].ToString().ToLower() == "true";
                txtFields68.Text = row["CptEauDiam"].ToString();
                Check23.Checked =  row["AlimEauCoupure"].ToString().ToLower() == "true";
                txtFields66.Text = row["AlimEauDiam"].ToString();
                Check24.Checked = row["Disconnecteur"].ToString().ToLower() == "true";
                Combo7.Text = row["DisconnecteurMarque"].ToString();
                txtFields8.Text = row["DisconnecteurType"].ToString();
                Text4.Text = row["DisconnecteurNumero"].ToString();
                Text7.Text = row["DisconnecteurDiametre"].ToString();
                txtFields87.Text = row["ExpansionType"].ToString();
                Combo8.Text = row["ExpansionMarque"].ToString();
                txtFields70.Text = row["ExpansionCapacite"].ToString();
                Combo9.Text = row["MaintienMarque"].ToString();
                Text16.Text = row["MaintienType"].ToString();
                txtFields89.Text = row["MaintienPompeMarque"].ToString();
                Text17.Text = row["MaintienPompeType"].ToString();
                txtFields88.Text = row["MaintienPompeNbre"].ToString();
                Check25.Checked =  row["PotBoue"].ToString().ToLower() == "true";
                Check26.Checked =  row["PotIntroduction"].ToString().ToLower() == "true";
                Check5.Checked = row["SiphonSol"].ToString().ToLower() == "true";
                Text3.Text = row["Observation"].ToString();
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        public void cmdAjouter_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //DatPrimaryRS.Recordset.AddNew();
            DatPrimaryRS.Rows.Add(DatPrimaryRS.NewRow());
            //txtFields(86).Text = gVar
            cmdAjouter.Enabled = false;
            cmdMAJ.Enabled = true;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdMAJ_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            int existImmeuble;
            /// initialisation des boutons d'options en création
            if (string.IsNullOrEmpty(txtFields16.Text))
            {
                txtFields16.Text = "2";
            }
            if (string.IsNullOrEmpty(txtFields32.Text))
            {
                txtFields32.Text = "2";
            }
            if (string.IsNullOrEmpty(txtFields13.Text))
            {
                txtFields13.Text = "3";
            }
            if (string.IsNullOrEmpty(txtFields9.Text))
            {
                txtFields9.Text = "2";
            }
            if (string.IsNullOrEmpty(txtFields51.Text))
            {
                txtFields51.Text = "2";
            }
            if (string.IsNullOrEmpty(txtFields50.Text))
            {
                txtFields50.Text = "2";
            }
            if (string.IsNullOrEmpty(txtFields49.Text))
            {
                txtFields49.Text = "3";
            }
            if (string.IsNullOrEmpty(txtFields4.Text))
            {
                txtFields4.Text = "2";
            }
            if (string.IsNullOrEmpty(txtFields87.Text))
            {
                txtFields87.Text = "2";
            }
            if (string.IsNullOrEmpty(txtFields14.Text))
            {
                txtFields14.Text = "1";
            }
            if (string.IsNullOrEmpty(txtFields1.Text))
            {
                txtFields1.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields3.Text))
            {
                txtFields3.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields7.Text))
            {
                txtFields7.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields10.Text) || !General.IsNumeric(txtFields10.Text))
            {
                txtFields10.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields15.Text))
            {
                txtFields15.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields20.Text))
            {
                txtFields20.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields24.Text) || !General.IsNumeric(txtFields24.Text))
            {
                txtFields24.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields34.Text))
            {
                txtFields34.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields55.Text))
            {
                txtFields55.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields56.Text))
            {
                txtFields56.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields59.Text))
            {
                txtFields59.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields88.Text))
            {
                txtFields88.Text = "0";
            }
            /// initialisation des quantités en création
            if (string.IsNullOrEmpty(Text1.Text) || !General.IsNumeric(Text1.Text))
            {
                Text1.Text = "0";
            }
            if (string.IsNullOrEmpty(Text2.Text) || !General.IsNumeric(Text2.Text))
            {
                Text2.Text = "0";
            }
            if (string.IsNullOrEmpty(Text5.Text) || !General.IsNumeric(Text5.Text))
            {
                Text5.Text = "0";
            }
            if (string.IsNullOrEmpty(Text6.Text) || !General.IsNumeric(Text6.Text))
            {
                Text6.Text = "0";
            }
            if (string.IsNullOrEmpty(Text15.Text) || !General.IsNumeric(Text15.Text))
            {
                Text15.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields1.Text) || !General.IsNumeric(txtFields1.Text))
            {
                txtFields1.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields3.Text) || !General.IsNumeric(txtFields3.Text))
            {
                txtFields3.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields7.Text) || !General.IsNumeric(txtFields7.Text))
            {
                txtFields7.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields20.Text) || !General.IsNumeric(txtFields20.Text))
            {
                txtFields20.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields34.Text) || !General.IsNumeric(txtFields34.Text))
            {
                txtFields34.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields55.Text) || !General.IsNumeric(txtFields55.Text))
            {
                txtFields55.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields56.Text) || !General.IsNumeric(txtFields56.Text))
            {
                txtFields56.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields59.Text) || !General.IsNumeric(txtFields59.Text))
            {
                txtFields59.Text = "0";
            }
            if (string.IsNullOrEmpty(txtFields88.Text) || !General.IsNumeric(txtFields88.Text))
            {
                txtFields88.Text = "0";
            }

            txtFields86.Text = General.gVar.ToString();
            using (var tmp = new ModAdo())
                existImmeuble = Convert.ToInt32(tmp.fc_ADOlibelle("select  count(CodeImmeuble)  FROM MaterielChauffage  WHERE CodeImmeuble='" + General.gVar + "'"));

            if (existImmeuble == 0)
            {
                DatPrimaryRS.Rows[0]["CodeImmeuble"] = General.gVar;
                ModAdoToUpdate.Update();
            }
            using (var tmp = new ModAdo())
                existImmeuble = Convert.ToInt32(tmp.fc_ADOlibelle("select  count(*)  FROM MaterielChauffage  WHERE CodeImmeuble='" + General.gVar + "'"));
            if (existImmeuble == 1)
            {
                string sqlUpdate = "Update  MaterielChauffage SET ";

                sqlUpdate = sqlUpdate + " ChaudiereMarque1 = '" + Combo10.Text + "', ";
                sqlUpdate = sqlUpdate + " ChaudiereType1 = '" + txtFields28.Text + "', ";
                sqlUpdate = sqlUpdate + " ChaudierePuissance1 = '" + txtFields48.Text + "', ";
                sqlUpdate = sqlUpdate + " ChaudiereNbre1 = '" + Text1.Text + "', ";
                sqlUpdate = sqlUpdate + " ChaudiereFluide1 = '" + txtFields16.Text  + "', ";
                sqlUpdate = sqlUpdate + " ChaudiereTypeBriquetage1 = '" + txtFields32.Text + "', ";
                sqlUpdate = sqlUpdate + " ChaudiereEtatBriquetage1 = '" + txtFields13.Text + "', ";
                sqlUpdate = sqlUpdate + " BruleurMarque1 = '" + Combo20.Text + "', ";
                sqlUpdate = sqlUpdate + " BruleurType1 = '" + txtFields5.Text + "', ";
                sqlUpdate = sqlUpdate + " BruleurNbre1 = '" + Text5.Text + "', ";
                sqlUpdate = sqlUpdate + " BruleurSystem1 = '" + txtFields9.Text+ "', ";
                sqlUpdate = sqlUpdate + " BruleurMarqueB1 = '" + Text8.Text + "', ";
                sqlUpdate = sqlUpdate + " BruleurTypeB1 = '" + Text9.Text + "', ";
                sqlUpdate = sqlUpdate + " BruleurDoubleSecurite1 = " + (Check2.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " ChemineeHauteur1 = '" + txtFields10.Text + "', ";
                sqlUpdate = sqlUpdate + " ChemineeSection1 = '" + txtFields37.Text + "', ";
                sqlUpdate = sqlUpdate + " ChemineeRegulateur1 = " + (Check9.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " ChemineeLongueur1 = '" + txtFields12.Text + "', ";
                sqlUpdate = sqlUpdate + " ChemineeDiam1 = '" + txtFields11.Text + "', ";
                sqlUpdate = sqlUpdate + " CarneauLongueur1 = '" + txtFields19.Text + "', ";
                sqlUpdate = sqlUpdate + " CarneauDiam1 = '" + txtFields18.Text + "', ";
                sqlUpdate = sqlUpdate + " ChaudiereMarque2 = '" + Combo11.Text + "', ";
                sqlUpdate = sqlUpdate + " ChaudiereType2 = '" + txtFields72.Text + "', ";
                sqlUpdate = sqlUpdate + " ChaudierePuissance2 = '" + txtFields71.Text + "', ";
                sqlUpdate = sqlUpdate + " ChaudiereNbre2 = '" + Text2.Text+ "', ";
                sqlUpdate = sqlUpdate + " ChaudiereFluide2 ='" + txtFields51.Text+ "', ";
                sqlUpdate = sqlUpdate + " ChaudiereTypeBriquetage2 = '" + txtFields50.Text+ "', ";
                sqlUpdate = sqlUpdate + " EtatBriquetage2 = '" + txtFields49.Text+ "', ";
                sqlUpdate = sqlUpdate + " BruleurMarque2 = '" + Combo21.Text + "', ";
                sqlUpdate = sqlUpdate + " BruleurType2 = '" + txtFields2.Text + "', ";
                sqlUpdate = sqlUpdate + " BruleurNbre2 = '" + Text6.Text+ "', ";
                sqlUpdate = sqlUpdate + " BruleurSysteme2 = '" + txtFields4.Text + "', ";
                sqlUpdate = sqlUpdate + " BruleurMarqueB2 = '" + Text10.Text + "', ";
                sqlUpdate = sqlUpdate + " BruleurTypeB2 = '" + Text11.Text + "', ";
                sqlUpdate = sqlUpdate + " BruleurDoubleSecurite2 = " + (Check4.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " ChemineeHauteur2 = '" + txtFields24.Text + "', ";
                sqlUpdate = sqlUpdate + " ChemineeSection2 = '" + txtFields35.Text + "', ";
                sqlUpdate = sqlUpdate + " ChemineeRegulateur2 =" + (Check10.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " ChemineeLongueur2 = '" + txtFields22.Text + "', ";
                sqlUpdate = sqlUpdate + " ChemineeDiam2 = '" + txtFields23.Text + "', ";
                sqlUpdate = sqlUpdate + " CarneauLongueur2 = '" + txtFields0.Text + "', ";
                sqlUpdate = sqlUpdate + " CarneauDiam2 = '" + txtFields21.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeCHMarque1 = '" + Combo30.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeCHNbre1 = '" + txtFields1.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeCHVanne1 = " + (Check13.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " PompeCHMarque2 = '" + Combo31.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeCHType2 = '" + txtFields39.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeCHNbre2 = '" + txtFields3.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeCHVanne2 = " + (Check14.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " PompeRECHMarque = '" + Combo40.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeRECHType = '" + txtFields57.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeRECHNbre = '" + txtFields56.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeRECHAsser = " + (Check20.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " PompeCHType1 = '" + txtFields38.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeRECHVanne = " + (Check1.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " RegulationMarque = '" + Combo6.Text + "', ";
                sqlUpdate = sqlUpdate + " RegulationType = '" + txtFields64.Text + "', ";
                sqlUpdate = sqlUpdate + " RegulationNbre = '" + txtFields34.Text + "', ";
                sqlUpdate = sqlUpdate + " RegulationActionBruleur = " + (Check19.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " RegulationActionVanne3 = " + (Check210.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " RegulationActionVanne4 = " + (Check211.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " PompePUMarque = '" + Combo32.Text + "', ";
                sqlUpdate = sqlUpdate + " PompePUType = '" + txtFields54.Text + "', ";
                sqlUpdate = sqlUpdate + " PompePUHauteur = '" + txtFields59.Text+ "', ";
                sqlUpdate = sqlUpdate + " PompePUNbre = '" + txtFields55.Text + "', ";
                sqlUpdate = sqlUpdate + " CptGazMarque = '" + Combo100.Text + "', ";
                sqlUpdate = sqlUpdate + " CptGazType = '" + txtFields15.Text + "', ";
                sqlUpdate = sqlUpdate + " CptGazNum = '" + txtFields6.Text + "', ";
                sqlUpdate = sqlUpdate + " ByPass = " + (Check6.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " CleBarrage = " + (Check213.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " CleExt = " + (Check212.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " PompeRECHMarque2 = '" + Combo41.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeRECHType2 = '" + txtFields17.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeRECHNbre2 = '" + txtFields20.Text+ "', ";
                sqlUpdate = sqlUpdate + " PompeRECHAsser2 = '" + (Check3.Checked ? 1 : 0) + "', ";
                sqlUpdate = sqlUpdate + " PompeRECHVanne2 = " + (Check7.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " VanneMarque = '" + txtFields61.Text + "', ";
                sqlUpdate = sqlUpdate + " VanneType = '" + txtFields60.Text + "', ";
                sqlUpdate = sqlUpdate + " VanneNbre = '" + Text15.Text+ "', ";
                sqlUpdate = sqlUpdate + " VanneSecteur = '" + txtFields14.Text+ "', ";
                sqlUpdate = sqlUpdate + " PompeTRMarque = '" + Combo5.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeTRNbre1 = '" + txtFields7.Text+ "', ";
                sqlUpdate = sqlUpdate + " PompeTRType = '" + txtFields62.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeTRHauteur = '" + Text12.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeTRNourrice = '" + Text13.Text + "', ";
                sqlUpdate = sqlUpdate + " PompeTRCapacite = '" + Text14.Text + "', ";
                sqlUpdate = sqlUpdate + " CodeImmeuble = '" + txtFields86.Text + "', ";
                sqlUpdate = sqlUpdate + " CptEau = " + (Check22.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " CptEauDiam = '" + txtFields68.Text + "', ";
                sqlUpdate = sqlUpdate + " AlimEauCoupure = " + (Check23.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " AlimEauDiam = '" + txtFields66.Text + "', ";
                sqlUpdate = sqlUpdate + " Disconnecteur = " + (Check24.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " DisconnecteurMarque = '" + Combo7.Text + "', ";
                sqlUpdate = sqlUpdate + " DisconnecteurType = '" + txtFields8.Text + "', ";
                sqlUpdate = sqlUpdate + " DisconnecteurNumero = '" + Text4.Text + "', ";
                sqlUpdate = sqlUpdate + " DisconnecteurDiametre = '" + Text7.Text + "', ";
                sqlUpdate = sqlUpdate + " ExpansionType = '" + txtFields87.Text+ "', ";
                sqlUpdate = sqlUpdate + " ExpansionMarque = '" + Combo8.Text + "', ";
                sqlUpdate = sqlUpdate + " ExpansionCapacite = '" + txtFields70.Text + "', ";
                sqlUpdate = sqlUpdate + " MaintienMarque = '" + Combo9.Text + "', ";
                sqlUpdate = sqlUpdate + " MaintienType = '" + Text16.Text + "', ";
                sqlUpdate = sqlUpdate + " MaintienPompeMarque = '" + txtFields89.Text + "', ";
                sqlUpdate = sqlUpdate + " MaintienPompeType = '" + Text17.Text + "', ";
                sqlUpdate = sqlUpdate + " MaintienPompeNbre = '" + txtFields88.Text+ "', ";
                sqlUpdate = sqlUpdate + " PotBoue = " + (Check25.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " PotIntroduction = " + (Check26.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " SiphonSol = " + (Check5.Checked ? 1 : 0) + ", ";
                sqlUpdate = sqlUpdate + " Observation = '" + StdSQLchaine.gFr_DoublerQuote(Text3.Text) + "' ";
                sqlUpdate = sqlUpdate + " Where CodeImmeuble='" + General.gVar.ToString() + "'";
                General.Execute(sqlUpdate);


                General.SQL = "update TechReleve set RelChauffage=" + -1 + " WHERE CodeImmeuble='" + General.gVar + "'";
                General.Execute(General.SQL);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option1_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option1.Checked)
            {
                txtFields16.Text = "0";
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option10_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option10.Checked)
            {
                txtFields49.Text = "0";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option11_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option11.Checked)
            {
                txtFields50.Text = "1";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option12_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option12.Checked)
            {
                txtFields50.Text = "0";

            }
        }
        /// <summary>
        /// /tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option13_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option13.Checked)
            {
                txtFields51.Text = "1";

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option14_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option14.Checked)
            {
                txtFields51.Text = "0";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option15_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option15.Checked)
            {
                txtFields4.Text = "2";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option16_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option16.Checked)
            {
                txtFields87.Text = "2";
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option17_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option17.Checked)
            {
                txtFields32.Text = "2";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option18_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option18.Checked)
            {
                txtFields50.Text = "2";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option19_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option19.Checked)
            {
                txtFields13.Text = "3";
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option2_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option2.Checked)
            {
                txtFields16.Text = "1";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option20_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option20.Checked)
            {
                txtFields49.Text = "3";

            }
        }
        /// <summary>
        /// /tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option21_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option21.Checked)
            {
                txtFields16.Text = "2";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option22_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option22.Checked)
            {
                txtFields51.Text = "2";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option23_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option23.Checked)
            {
                txtFields14.Text = "1";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option24_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option24.Checked)
            {
                txtFields14.Text = "0";

            }
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option26_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option26.Checked)
            {
                //Txtfields(81) = "0"
                txtFields14.Text = "2";

            }
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option29_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option29.Checked)
            {
                txtFields9.Text = "1";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option3_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option3.Checked)
            {
                txtFields32.Text = "0";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option30_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option30.Checked)
            {
                txtFields9.Text = "0";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option31_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option31.Checked)
            {
                txtFields4.Text = "0";

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option32_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option32.Checked)
            {
                txtFields4.Text = "1";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option33_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option33.Checked)
            {
                txtFields9.Text = "2";

            }
        }
        private void Option34_Click()
        {
            txtFields8.Text = "1";

        }
        private void Option35_Click()
        {
            txtFields17.Text = "0";

        }
        private void Option36_Click()
        {
            txtFields17.Text = "1";

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option37_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option37.Checked)
            {
                txtFields87.Text = "0";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option38_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option38.Checked)
            {
                txtFields87.Text = "1";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option4_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option4.Checked)
            {
                txtFields32.Text = "1";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option5_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option5.Checked)
            {
                txtFields13.Text = "0";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option6_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option6.Checked)
            {
                txtFields13.Text = "1";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option7_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option7.Checked)
            {
                txtFields13.Text = "2";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option8_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option8.Checked)
            {
                txtFields49.Text = "2";

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Option9_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option9.Checked)
            {
                txtFields49.Text = "1";

            }
        }
        /// <summary>
        /// Tetsted
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtFields_TextChanged(Object eventSender, EventArgs eventArgs)
        {
            //short Index = Txtfields9.GetIndex(eventSender);
            var Txtfields = eventSender as iTalk.iTalk_TextBox_Small2;
            Int16 Index = Convert.ToInt16(Txtfields.Tag);

            switch (Index)
            {
                case 4:
                    //B2TB
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option31.Checked = true;
                            break;
                        case "1":
                            Option32.Checked = true;
                            break;
                        case "2":
                            Option15.Checked = true;
                            break;
                    }
                    break;
                case 8:
                    //B3TB
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option33.Checked = true;
                            break;
                        case "1":
                            //not exist
                            //  Option34 = true;
                            break;
                    }
                    break;

                case 9:
                    //B1TB
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option30.Checked = true;
                            break;
                        case "1":
                            Option29.Checked = true;
                            break;
                        case "2":
                            Option33.Checked = true;
                            break;
                    }
                    break;

                case 13:
                    //CH1EB
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option5.Checked = true;
                            break;
                        case "1":
                            Option6.Checked = true;
                            break;
                        case "2":
                            Option7.Checked = true;
                            break;
                        case "3":
                            Option19.Checked = true;
                            break;
                    }
                    break;

                case 14:
                    //CH1EB
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option24.Checked = true;
                            break;
                        case "1":
                            Option23.Checked = true;
                            break;
                        case "2":
                            Option26.Checked = true;
                            break;
                    }
                    break;


                case 16:
                    //CH1EV
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option1.Checked = true;
                            break;
                        case "1":
                            Option2.Checked = true;
                            break;
                        case "2":
                            Option21.Checked = true;
                            break;
                    }
                    break;

                case 17:
                    //B4TB
                    switch (Txtfields.Text)
                    {
                        case "0":
                            //not exist
                            // Option35 = true;
                            break;
                        case "1":
                            //not exist
                            // Option36 = true;
                            break;
                    }
                    break;

                case 32:
                    //CH1TB
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option3.Checked = true;
                            break;
                        case "1":
                            Option4.Checked = true;
                            break;
                        case "2":
                            Option17.Checked = true;
                            break;
                    }
                    break;

                case 49:
                    //CH2ET
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option10.Checked = true;
                            break;
                        case "1":
                            Option9.Checked = true;
                            break;
                        case "2":
                            Option8.Checked = true;
                            break;
                        case "3":
                            Option20.Checked = true;
                            break;
                    }
                    break;

                case 50:
                    //CH2EB
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option12.Checked = true;
                            break;
                        case "1":
                            Option11.Checked = true;
                            break;
                        case "2":
                            Option18.Checked = true;
                            break;
                    }
                    break;

                case 51:
                    //CH2EV
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option14.Checked = true;
                            break;
                        case "1":
                            Option13.Checked = true;
                            break;
                        case "2":
                            Option22.Checked = true;
                            break;
                    }
                    break;

                case 74:
                    //CH3EB
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option17.Checked = true;
                            break;
                        case "1":
                            Option16.Checked = true;
                            break;
                        case "2":
                            Option15.Checked = true;
                            break;
                    }
                    break;

                case 75:
                    //CH3TB
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option19.Checked = true;
                            break;
                        case "1":
                            Option18.Checked = true;
                            break;
                    }
                    break;

                case 76:
                    //CH3EV
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option21.Checked = true;
                            break;
                        case "1":
                            Option20.Checked = true;
                            break;
                    }
                    break;

                case 80:
                    //CH4EB
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option24.Checked = true;
                            break;
                        case "1":
                            Option23.Checked = true;
                            break;
                        case "2":
                            Option22.Checked = true;
                            break;
                    }
                    break;

                case 81:
                    //CH4TB
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option26.Checked = true;
                            break;
                        case "1":

                            //Option25 = true;
                            break;
                    }
                    break;

                case 82:
                    //CH4EV
                    switch (Txtfields.Text)
                    {
                        case "0":
                            //not exist
                            //Option28 = true;
                            break;
                        case "1":
                            //not exist
                            // Option27 = true;
                            break;
                    }
                    break;

                case 87:
                    //expansion
                    switch (Txtfields.Text)
                    {
                        case "0":
                            Option37.Checked = true;
                            break;
                        case "1":
                            Option38.Checked = true;
                            break;
                        case "2":
                            //not exist
                            //option87 = true;
                            break;
                    }
                    break;

            }
        }
    }
}
