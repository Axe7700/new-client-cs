﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.BaseDeDonnees.Immeuble;
using Axe_interDT.Views.CompteRendu;
using System;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Windows.Forms;

namespace Axe_interDT.Views
{
    public partial class UserDocRelTech : UserControl
    {
        public UserDocRelTech()
        {
            InitializeComponent();
        }

        string strCheminRetour;
        string strValeur;
        string CodeFicheAppelante;
        DataTable rsCorr = new DataTable();
        DataTable Data1 = new DataTable();
        ModAdo ModAdoData1 = new ModAdo();
        DataTable Data2 = new DataTable();
        ModAdo ModAdoData2 = new ModAdo();
        DataTable Data3 = new DataTable();
        ModAdo ModAdoData3 = new ModAdo();
        DataTable Data4 = new DataTable();
        ModAdo ModAdoData4 = new ModAdo();
        DataTable Data5 = new DataTable();
        ModAdo ModAdoData5 = new ModAdo();
        DataTable Data8 = new DataTable();
        ModAdo ModAdoData8 = new ModAdo();
        ModAdo ModAdodatPrimaryRS = new ModAdo();
        ModAdo ModAdorsCorr = new ModAdo();
        ModAdo ModAdo7 = new ModAdo();
        DataTable datPrimaryRS;
        DataTable Adodc7 = new DataTable();
        ModAdo MD = new ModAdo();
        DataTable Data7 = new DataTable();
        string ReqData7;
        bool bLoadActivate;
        string whereData8;
        private void Checkboite_CheckStateChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Checkboite.Checked == true)
            {
                SituaBoite.Enabled = true;
            }
            else
            {
                SituaBoite.Enabled = false;
            }
        }

        private void cmdAjouter_Click(System.Object eventSender, System.EventArgs eventArgs)
        {

            cmdAjouter.Enabled = false;
            cmdSupprimer.Enabled = false;
            cmdRechercheClient.Enabled = false;
            cmdImprimer.Enabled = false;
            //Clear

            Text100.Text = "";
            Text113.Text = "";
            Text19.Text = "";
            Text12.Text = "";
            Text110.Text = "";
            Text111.Text = "";
            SSDBCombo1.Text = "";
            SSDBCombo2.Text = "";
            SSDBCombo3.Text = "";
            SSDBCombo4.Text = "";
            SSDBCombo5.Text = "";
            Check1.Checked = false;


            datPrimaryRS = new DataTable();
            datPrimaryRS.NewRow();

            Text13.Text = DateTime.Now.ToString("dd/MM/yy");

            string requete;
            string where_order = "";
            string SQL = "";
            requete = "SELECT " + "CodeImmeuble as \"Code Immeuble\", Adresse, Ville, " + "CodePostal as \"Code Postal\" FROM Immeuble ";
            SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des immeubles" };
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                SQL = "SELECT * FROM immeuble WHERE CodeImmeuble='" + fg.ugResultat.ActiveRow.Cells[1].Value.ToString() + "'";
                Text100.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyPress += (se, ev) =>
            {
                if (Convert.ToInt32(ev.KeyChar) == 13)
                {
                    SQL = "SELECT * FROM immeuble WHERE CodeImmeuble='" + fg.ugResultat.ActiveRow.Cells[1].Value.ToString() + "'";
                    Text100.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                    fg.Dispose();
                    fg.Close();
                }

            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();

            if (string.IsNullOrEmpty(SQL))
            {
                return;
            }
            else
            {
                Data7 = ModAdo7.fc_OpenRecordSet(SQL);
                if (Data7.Rows.Count > 0)
                {
                    Text14.Text = Data7.Rows[0]["Gardien"].ToString();
                    Text9.Text = Data7.Rows[0]["AdresseGardien"].ToString();
                    Text15.Text = Data7.Rows[0]["TelGardien"].ToString();
                    Text17.Text = Data7.Rows[0]["CodeAcces1"].ToString();
                    Text11.Text = Data7.Rows[0]["CodeAcces2"].ToString();
                    Text16.Text = Data7.Rows[0]["FaxGardien"].ToString();
                    Text112.Text = Data7.Rows[0]["SpecifAcces"].ToString();
                    Text18.Text = Data7.Rows[0]["horaires"].ToString();
                    Checkboite.Checked = Convert.ToBoolean(Data7.Rows[0]["BoiteAClef"]);
                    SituaBoite.Text = Data7.Rows[0]["SitBoiteAClef"].ToString();
                }

            }


        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdCEE_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            ModMain.bActivate = true;
            frmCEE frmCEE = new frmCEE();
            frmCEE.txtCodeImmeuble.Text = Text10.Text;
            frmCEE.ShowDialog();
            frmCEE.Close();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdChauffage_Click(System.Object eventSender, System.EventArgs eventArgs)
        {

            var frmChauffage1 = new frmChauffage1();
            frmChauffage1.ShowDialog();
            //vbModal
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdClim_Click(System.Object eventSender, System.EventArgs eventArgs)
        {


            string sDesPathFile = null;
            string sPathCpt = null;
            string sPathDef = null;
            string sMessage = null;


            try
            {

                if (!string.IsNullOrEmpty(txtFilePathClim.Text))
                {
                    sMessage = "Voulez-vous remplacer le fichier" + Dossier.ExtractFileName(txtFilePathClim.Text) + "?";
                    //& vbCrLf & "ATTENTION cette manipulation supprimera définitivement le fichier " & ExtractFileName(txtFilePathClim) _
                    //& vbCrLf & ", si vous voulez garder ce fichier cliquez sur le bouton 'détacher le fichier' " _
                    //& vbCrLf & "et rendez-vous dans le répertoire compte-rendu de l'immeuble afin de récupérer ce fichier."

                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMessage, "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        return;
                    }

                }
                sPathDef = General.getFrmReg(Variable.cUserDocRelTech, "CheminDefCptClim", "");

                SaveFileDialog cd1 = new SaveFileDialog();

                if (!string.IsNullOrEmpty(sPathDef))
                {
                    cd1.InitialDirectory = sPathDef;
                    cd1.FileName = "";
                }

                cd1.ShowDialog();

                sPathDef = Dossier.ExtractNameDossier(cd1.FileName);

                General.saveInReg(Variable.cUserDocRelTech, "CheminDefCptClim", sPathDef);

                if (!string.IsNullOrEmpty(cd1.FileName) && !string.IsNullOrEmpty(Text10.Text))
                {
                    //sDesPathFile = sCheminDossier & "\" & Text10 & "\" & cFolderCptRendu & "\out\" & ExtractFileName(CD1.filename)
                    //TODO ==> Dans la version vb6 le chemein  sDesPathFile contient un sous dossier 'out' 
                    // et la fct 'fc_CreeDossierImm()' ne crée pas ce dossier et fc_DeplaceFichier() déclenche un beug car le dossier out est introuvable
                    //j'ai supprimé le sous dossier 'out' 

                    sDesPathFile = General.sCheminDossier + "\\" + Text10.Text + "\\" + General.cFolderCptRendu + "\\" + Dossier.ExtractFileName(cd1.FileName);
                    Video.fc_CreeDossierImm(Text100.Text);//le dossier comptes-rendus cree par cette fct ne contient pas de sous-dossier nommée out 
                    if (Dossier.fc_ControleFichier(sDesPathFile) == false)
                    {
                        Dossier.fc_DeplaceFichier((cd1.FileName), sDesPathFile);
                    }
                    Text115.Text = Dossier.ExtractFileName(cd1.FileName);
                    txtFilePathClim.Text = sDesPathFile;

                    lblPathCptClim.Font = new Font(lblPathCptClim.Font, FontStyle.Regular);
                    lblPathCptClim.ForeColor = ColorTranslator.FromOle(0xc00000);
                }

                //sPathCpt = sCheminDossier & "\" & txtCodeImmeuble & "\" & cFolderCptRendu

            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";cmdClim_Click");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdConformite_Click(Object eventSender, EventArgs eventArgs)
        {

            var frmReleveConf = new frmReleveConf();
            frmReleveConf.Show();
            ////vbModal
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdEcs_Click(Object eventSender, EventArgs eventArgs)
        {
            General.gVar = Text10.Text;
            var frmECS = new frmECS();
            frmECS.ShowDialog();
            //vbModal
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdFOD_Click(Object eventSender, EventArgs eventArgs)
        {
            frmFOD frmFOD = new frmFOD();
            frmFOD.ShowDialog();
            ////vbModal
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdImprimer_Click(Object eventSender, EventArgs eventArgs)
        {

            General.gVar = Text10.Text;
            frmImpression frmImpression = new frmImpression();
            frmImpression.ShowDialog();
            //vbModal

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdMAJ_Click(Object eventSender, EventArgs eventArgs)
        {
            try
            {
                cmdAjouter.Enabled = true;
                cmdSupprimer.Enabled = true;
                cmdRechercheClient.Enabled = true;
                cmdImprimer.Enabled = true;


                //test des champs obligatoires

                if (SSDBCombo1.Text.Length == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Validation impossible, champ obligatoire");
                    SSDBCombo1.Focus();
                    return;
                }


                if (SSDBCombo2.Text.Length == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Validation impossible, champ obligatoire");
                    SSDBCombo2.Focus();
                    return;
                }


                if (SSDBCombo3.Text.Length == 0 || SSDBCombo3.Value.ToString() == "0")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Validation impossible, champ obligatoire");
                    SSDBCombo3.Focus();
                    return;
                }


                if (SSDBCombo4.Text.Length == 0 || SSDBCombo4.Value.ToString() == "0")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Validation impossible, champ obligatoire");
                    SSDBCombo4.Focus();
                    return;
                }


                if (SSDBCombo5.Text.Length == 0 || SSDBCombo5.Value.ToString() == "0")
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Validation impossible, champ obligatoire");
                    SSDBCombo5.Focus();
                    return;
                }


                if (!General.IsDate(Text13.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date invalide");
                    Text13.Text = "";
                    Text13.Focus();
                    return;
                }

                Text100.Text = Text10.Text;

                //**********************Pourquoi ???****************
                //    Text10_KeyPress (13)
                //**************************************************

                //Todo
                var datPrimaryRS = new DataTable();

                //On Error Resume Next
                //.UpdateRecord
                //_with15.Recordset.UpdateBatch(ADODB.AffectEnum.adAffectCurrent);
                //_with15.Recordset.Requery();
                //.Recordset.Bookmark = .Recordset.LastModified

                string reqUpdate = "update  TechReleve set DateReleve='" + Text13.Text + "'" +
                                   ",Surface=" + Convert.ToDouble(General.nz(Text113.Text, 0)) + "" +
                                   ",NbreEtages=" + Convert.ToInt32(General.nz(Text19.Text, 0)) + "" +
                                   ",NbreAppartement=" + Convert.ToInt32(General.nz(Text12.Text, 0)) + "" +
                                   ",Vidange='" + Text110.Text.Replace("'", "''") + "'" +
                                   ",VidangeDernierEtage='" + Text111.Text.Replace("'", "''") + "'" +
                                   ",CodeEnergie1='" + SSDBCombo1.Value.ToString().Replace("'", "''") + "'" +
                                   ",Electricite=" + SSDBCombo2.Value + "" +
                                   ",CodeDistribution=" + SSDBCombo3.Value + "" +
                                   ",TypeImmeuble=" + SSDBCombo4.Value + "" +
                                   ",CodeFluide=" + SSDBCombo5.Value + "" +
                                   ",VannePied=" + (Check1.Checked ? 1 : 0) + "" +
                                   ",PanneauSolaire=" + (Check2.Checked ? 1 : 0) + "" +
                                   ",obs='" + Text2.Text.Replace("'", "''") + "'" +
                                   "WHERE CodeImmeuble='" + Text100.Text + "'";
                int yy = General.Execute(reqUpdate);


                //===> Mondir le 07.05.2021 https://groupe-dt.mantishub.io/view.php?id=1381#c5964
                var req = $"UPDATE Immeuble SET SitBoiteAClef = '{SituaBoite.Text}', HorairesLoge = '{Text18.Text}' WHERE CodeImmeuble = '{Text10.Text}'";
                General.Execute(req);
                //===> Fin Modif Mondir

                RemplitGroupBoxAcces();
                ssGridImmCorrespondant.UpdateData();

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " Erreur Enregistrement cmdMaj_Click ");
            }


        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdPrimaire_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            var frmPrimaire = new frmPrimaire();
            frmPrimaire.Show();
            ////vbModal
        }

        private void cmdRafraichir_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            // datPrimaryRS.Refresh
            ModAdodatPrimaryRS.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdRechercheClient_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            string sCode = null;
            string requete;
            string where_order = "";

            requete = "SELECT " + "CodeImmeuble as \"Réf Immeuble\", CodeEnergie1 as \"Energie\" FROM TechReleve ";
            SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des relevés techniques" };

            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                General.SQL = "SELECT * FROM TechReleve WHERE CodeImmeuble='" + sCode + "'";

                fg.Dispose();
                fg.Close();
            };
            fg.ugResultat.KeyPress += (se, ev) =>
            {
                if (Convert.ToInt32(ev.KeyChar) == 13)
                {
                    if (fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        General.SQL = "SELECT * FROM TechReleve WHERE CodeImmeuble='" + sCode + "'";

                        fg.Dispose();
                        fg.Close();
                    }
                }

            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();


            datPrimaryRS = ModAdodatPrimaryRS.fc_OpenRecordSet(General.SQL);
            //string whereData8 = datPrimaryRS.Rows[0]["CodeEnergie1"].ToString();
            //datPrimaryRS.Refresh();
            ModAdodatPrimaryRS.Update();
            Text10.Text = sCode;
            General.gVar = Text10.Text;
            //Text10_KeyPress(Text10, new KeyPressEventArgs(Convert.ToInt32(eventArgs.KeyChar)));
            Text10_KeyPress(Text10, new KeyPressEventArgs((char)13));


        }

        //dont work
        //private void cmdRechercheImmeuble_Click()
        //{
        //    string requete = "SELECT " + "CodeImmeuble as [Ref Immeuble], Adresse, Ville FROM Immeuble ";
        //    string where_order = "";

        //    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des immeubles" };

        //    fg.ugResultat.DoubleClickRow += (se, ev) =>
        //    {

        //        General.SQL = "SELECT * FROM Immeuble WHERE CodeImmeuble='" + fg.ugResultat.ActiveRow.Cells[1].Value.ToString() + "'";

        //        fg.Dispose();
        //        fg.Close();
        //    };
        //    fg.ugResultat.KeyPress += (se, ev) =>
        //    {
        //        if (Convert.ToInt32(ev.KeyChar) == 13)
        //        {

        //            General.SQL = "SELECT * FROM Immeuble WHERE CodeImmeuble='" + fg.ugResultat.ActiveRow.Cells[1].Value.ToString() + "'";
        //        }
        //        fg.Dispose();
        //        fg.Close();
        //    };
        //    fg.StartPosition = FormStartPosition.CenterParent;
        //    fg.ShowDialog();

        //    Data7 = new DataTable();
        //    ModAdo ModAdo3 =new ModAdo();
        //    Data7 = ModAdo3.fc_OpenRecordSet(General.SQL);

        //}

        private void CmdRetour_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //  If strCheminRetour <> "" And strValeur <> "" Then
            //        prcLiaison strCheminRetour, Text10, strValeur
            //        fc_Navigue Me, gsCheminPackage & strCheminRetour
            //   End If
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdSupFichier_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            string sMessage = null;

            try
            {

                if (!string.IsNullOrEmpty(txtFilePathClim.Text))
                {
                    sMessage = "Voulez-vous supprimer le lien avec le fichier ?" + Dossier.ExtractFileName(txtFilePathClim.Text) + "." + "\r\n" + "Attention cette commande ne supprimera pas le fichier du répertoire, " + "\r\n" + "Pour supprimer définitivement ce fichier rendez-vous dans le dossier compte-rendu de l'immeuble.";

                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMessage, "Supprimer le lien", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button3) == DialogResult.Yes)
                    {
                        txtFilePathClim.Text = "";
                        Text115.Text = "";
                        lblPathCptClim.Font = new Font(lblPathCptClim.Font, FontStyle.Regular);
                        lblPathCptClim.ForeColor = ColorTranslator.FromOle(0x0);
                        cmdMAJ_Click(cmdMAJ, new EventArgs());
                    }
                }

                return;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";cmdSupFichier_Click");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdSupprimer_Click(Object eventSender, EventArgs eventArgs)
        {
            int intMessage = 0;

            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Suppression interdite", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Warning);
            return;

            intMessage = Convert.ToInt32(Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(General.PROMPTSUPPRESSION, General.TITRESUPPRESSION, MessageBoxButtons.YesNo, MessageBoxIcon.Warning, MessageBoxDefaultButton.Button2));
            switch (intMessage)
            {
                case 6:
                    //Todo
                    //var _with21 = datPrimaryRS.Recordset;
                    //datPrimaryRS.Delete();
                    ModAdodatPrimaryRS.Update();
                    //_with21.MoveNext();
                    //if (_with21.EOF)
                    //    _with21.MoveLast();
                    break;
                case 7:
                    break;
            }

        }




        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Label1_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            Cursor.Current = Cursors.Default;
            // stocke les paramétres pour la feuille de destination.
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, Text10.Text);
            View.Theme.Theme.Navigate(typeof(UserDocImmeuble));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void lblPathCptClim_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            ModuleAPI.Ouvrir(txtFilePathClim.Text);
            return;

        }

        private void SSDBCombo1_Leave(System.Object eventSender, System.EventArgs eventArgs)
        {
            //TODO
            if (!string.IsNullOrEmpty(SSDBCombo1.Text))
            {
                //var _with26 = Data8;
                //_with26.ConnectionString = General.adocnn.ConnectionString;
                //.DatabaseName = gDatabase
                DataTable Data8 = new DataTable();
                ModAdo ModAdo8 = new ModAdo();
                Data8 = ModAdo8.fc_OpenRecordSet("SELECT Fioul,ReseauPrimaire FROM Energie " + "WHERE code='" + SSDBCombo1.Value + "'");
                SSDBCombo1.DataSource = Data8;
                // _with26.RecordSource = "SELECT Fioul,ReseauPrimaire FROM Energie " + "WHERE code='" + SSDBCombo1.Value + "'";
                //_with26.Refresh();
                if (SSDBCombo1.ActiveRow != null && SSDBCombo1.ActiveRow.Cells["Fioul"].Value.ToString() == "1")
                {
                    cmdFOD.Visible = true;
                }
                else
                {
                    cmdFOD.Visible = false;
                }
                if (SSDBCombo1.ActiveRow != null && SSDBCombo1.ActiveRow.Cells["ReseauPrimaire"].Value.ToString() == "1")
                {
                    cmdPrimaire.Visible = true;
                }
                else
                {
                    cmdPrimaire.Visible = false;
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {

            //ecris le code code immeuble

            if (ssGridImmCorrespondant.ActiveRow != null && string.IsNullOrEmpty(ssGridImmCorrespondant.ActiveRow.Cells["CodeImmeuble_IMM"].Text))
            {
                ssGridImmCorrespondant.ActiveRow.Cells["CodeImmeuble_IMM"].Value = Text10.Text;
            }
            if (ssGridImmCorrespondant.ActiveRow != null && !string.IsNullOrEmpty(ssGridImmCorrespondant.ActiveRow.Cells["CodeQualif"].Text))
            {
                fc_RechercheQualite(ssGridImmCorrespondant.ActiveRow.Cells["CodeQualif"].Text);
            }


        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_DropQualite()
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            General.sSQL = "";
            General.sSQL = "SELECT Qualification.CodeQualif, Qualification.Qualification" + " FROM Qualification where Correspondant_COR = 1";

            Adodc7 = MD.fc_OpenRecordSet(General.sSQL);
            if (ssDropQualite.Rows.Count == 0)
            {
                sheridan.InitialiseCombo(ssDropQualite, General.sSQL, "CodeQualif", true);


            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {

            if (string.IsNullOrEmpty(e.Row.Cells["CodeQualif"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code qualification est obligatoire", "Validation annulée");
                e.Cancel = true;
                e.Row.Cells["CodeQualif"].Activate();
            }
            //       If ssGridImmCorrespondant.Columns("CodeCorresp_IMC").value = "" Then
            //            MsgBox "Le code correspondant est obligatoire.", vbCritical, "Validation annulée"
            //            Cancel = True
            //            Exit Sub
            //        End If
        }
        /// <summary>
        /// Tested
        /// </summary>
        int k = 0;
        private void ssGridImmCorrespondant_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            if (e.ReInitialize)
                return;
            if (e.Row.Cells["PresidentCS_IMC"].Value.ToString() == "-1")
            {
                e.Row.Cells["Président du conseil syndical"].Value = true;
            }
            if (e.Row.Cells["PresidentCS_IMC"].Value.ToString() == "0" || e.Row.Cells["PresidentCS_IMC"].Value == DBNull.Value)
            {
                e.Row.Cells["Président du conseil syndical"].Value = false;
            }
            if (!string.IsNullOrEmpty(e.Row.Cells["CodeQualif"].Text))
            {
                k = e.Row.Index;
                fc_RechercheQualite(e.Row.Cells["CodeQualif"].Text);

            }

        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>

        private void fc_RechercheQualite(string sCode)
        {
            // récupére dans le controle adodc1 les informations liées au matricule.
            //sSQl = "";
            //sSQl = "SELECT Qualification.CodeQualif, Qualification.Qualification"
            //+ " FROM Qualification where Correspondant_COR = 1";
            if ((Adodc7 != null))
            {
                //var _with33 = Adodc7.Recordset;
                //_with33.MoveFirst();
                if (Adodc7.Rows.Count > 0)
                {
                    int i = 0;
                    foreach (DataRow Dr in Adodc7.Rows)
                    {
                        i++;
                        if (Dr["CodeQualif"].ToString().ToUpper() == sCode.ToUpper())
                        {
                            if (ssGridImmCorrespondant.ActiveRow != null)
                            {
                                ssGridImmCorrespondant.ActiveRow.Cells["Qualification"].Value = Dr["Qualification"];

                                break;
                            }
                            else if (ssGridImmCorrespondant.Rows.Count > 0)
                            {
                                ssGridImmCorrespondant.Rows[k].Cells["Qualification"].Value = Dr["Qualification"];

                                break;
                            }
                        }

                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Text1_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            //short Index =GetIndex(eventArgs);
            var text = eventSender as iTalk.iTalk_TextBox_Small2;
            Int16 Index = Convert.ToInt16(text.Tag);

            if (Index == 0)
            {
                Text10.Text = Text100.Text;
            }

        }

        private void Text1_Leave(System.Object eventSender, System.EventArgs eventArgs)
        {
            var text = eventSender as iTalk.iTalk_TextBox_Small2;
            Int16 Index = Convert.ToInt16(text.Tag);


            if (!General.IsDate(Text13.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous n'avez pas saisie de date valide");
                Text13.Text = "";
                Text13.Focus();
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Text10_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)eventArgs.KeyChar;
            string sSQLtech;

            if (KeyAscii == 13)
            {
                //'=== modif du 13 12 2018, recherche si ce code immeuble existe, si négatif on affiche la
                // '=== fiche de recherche
                if (bLoadActivate == false)
                {
                    sSQLtech = "SELECT * FROM TechReleve WHERE CodeImmeuble='" + Text10.Text + "'";
                    using (var tmp = new ModAdo())
                        sSQLtech = tmp.fc_ADOlibelle(sSQLtech);

                    if (string.IsNullOrEmpty(sSQLtech))
                    {
                        cmdRechercheClient_Click(cmdRechercheClient, new EventArgs());
                        return;
                    }
                }


                //remplissage du recorset general
                General.SQL = "SELECT * FROM TechReleve WHERE CodeImmeuble='" + Text10.Text + "'";

                DataTable datPrimaryRS = new DataTable();
                datPrimaryRS = ModAdodatPrimaryRS.fc_OpenRecordSet(General.SQL);
                //_with35.Refresh();

                if (datPrimaryRS.Rows.Count == 0)
                {
                    DataRow Nr = datPrimaryRS.NewRow();
                    Nr["CodeImmeuble"] = Text10.Text;
                    datPrimaryRS.Rows.Add(Nr);
                }

                //===> Mondir le 03.09.2020, pour corrigé https://groupe-dt.mantishub.io/view.php?id=1963
                var xx = ModAdodatPrimaryRS.Update();

                RemplitGrid();

                if (datPrimaryRS.Rows.Count > 0)
                {
                    whereData8 = datPrimaryRS.Rows[0]["CodeEnergie1"].ToString();
                    Data8 = ModAdoData8.fc_OpenRecordSet("SELECT Fioul, ReseauPrimaire FROM Energie WHERE code = '" + whereData8 + "'");
                    Text100.Text = datPrimaryRS.Rows[0]["CodeImmeuble"].ToString();
                    Text13.Text = (datPrimaryRS.Rows[0]["DateReleve"] != null && General.IsDate(datPrimaryRS.Rows[0]["DateReleve"].ToString())) ? Convert.ToDateTime(datPrimaryRS.Rows[0]["DateReleve"]).ToShortDateString() : "";
                    Text113.Text = datPrimaryRS.Rows[0]["Surface"].ToString() == "0" ? "" : datPrimaryRS.Rows[0]["Surface"].ToString();
                    Text19.Text = datPrimaryRS.Rows[0]["NbreEtages"].ToString() == "0" ? "" : datPrimaryRS.Rows[0]["NbreEtages"].ToString();
                    Text12.Text = datPrimaryRS.Rows[0]["NbreAppartement"].ToString() == "0" ? "" : datPrimaryRS.Rows[0]["NbreAppartement"].ToString();
                    Text110.Text = datPrimaryRS.Rows[0]["Vidange"].ToString();
                    Text111.Text = datPrimaryRS.Rows[0]["VidangeDernierEtage"].ToString();
                    SSDBCombo1.Text = datPrimaryRS.Rows[0]["CodeEnergie1"].ToString();
                    SSDBCombo2.Text = datPrimaryRS.Rows[0]["Electricite"].ToString();
                    SSDBCombo3.Text = datPrimaryRS.Rows[0]["CodeDistribution"].ToString();
                    SSDBCombo4.Text = datPrimaryRS.Rows[0]["TypeImmeuble"].ToString();
                    SSDBCombo5.Text = datPrimaryRS.Rows[0]["CodeFluide"].ToString();
                    Check1.CheckState = (datPrimaryRS.Rows[0]["VannePied"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                    Check2.CheckState = (datPrimaryRS.Rows[0]["PanneauSolaire"].ToString() == "True") ? CheckState.Checked : CheckState.Unchecked;
                    Text2.Text = datPrimaryRS.Rows[0]["obs"].ToString();
                }

                if (Data8.Rows.Count > 0)
                {
                    if (Data8.Rows[0]["fioul"].ToString() == "1")
                    {
                        cmdFOD.Visible = true;
                    }
                    else
                    {
                        cmdFOD.Visible = false;
                    }

                    if (Data8.Rows[0]["ReseauPrimaire"].ToString() == "1")
                    {
                        cmdPrimaire.Visible = true;
                    }
                    else
                    {
                        cmdPrimaire.Visible = false;
                    }
                }

                RemplitGroupBoxAcces();

            }

        }
        /// <summary>
        /// tested
        /// </summary>
        private void RemplitGrid()
        {
            General.sSQL = "SELECT IMC_ImmCorrespondant.CodeImmeuble_IMM , "
                            + " IMC_ImmCorrespondant.CodeCorresp_IMC ,"
                            + "IMC_ImmCorrespondant.nom_imc as Nom,"
                            + "IMC_ImmCorrespondant.CodeQualif_QUA as CodeQualif,"
                            + " '' as Qualification, "
                            + " IMC_ImmCorrespondant.tel_imc as Tel,"
                            + "IMC_ImmCorrespondant.telportable_imc as TelPortable,"
                            + "IMC_ImmCorrespondant.Fax_IMC as Fax,"
                            + " IMC_ImmCorrespondant.email_imc as Email,"
                            + " IMC_ImmCorrespondant.Note_imc as Note,"
                            + " PresidentCS_IMC ,'' as 'Président du conseil syndical' FROM  IMC_ImmCorrespondant"
                            + " where CodeImmeuble_IMM= '" + Text10.Text + "'";

            General.sSQL = General.sSQL + " order by IMC_ImmCorrespondant.CodeCorresp_IMC ";
            rsCorr = ModAdorsCorr.fc_OpenRecordSet(General.sSQL, ssGridImmCorrespondant, "CodeCorresp_IMC");
            ssGridImmCorrespondant.DataSource = rsCorr;
            ssGridImmCorrespondant.UpdateData();

        }
        private void RemplitGroupBoxAcces()
        {
            ReqData7 = "SELECT * FROM immeuble WHERE CodeImmeuble='" + Text10.Text + "'";
            Data7 = ModAdo7.fc_OpenRecordSet(ReqData7);
            //remplire les texts (DataField)

            if (Data7.Rows.Count > 0)
            {
                //LES CHAMPS DE LA GROUPEBOX Acces
                Text14.Text = Data7.Rows[0]["Gardien"].ToString();
                Text9.Text = Data7.Rows[0]["AdresseGardien"].ToString();
                Text15.Text = Data7.Rows[0]["TelGardien"].ToString();
                Text17.Text = Data7.Rows[0]["CodeAcces1"].ToString();
                Text11.Text = Data7.Rows[0]["CodeAcces2"].ToString();
                Text16.Text = Data7.Rows[0]["FaxGardien"].ToString();
                Text112.Text = Data7.Rows[0]["SpecifAcces"].ToString();
                //===> Mondir le 07.05.2021 https://groupe-dt.mantishub.io/view.php?id=1381#c3897
                //Text18.Text = Data7.Rows[0]["horaires"].ToString();
                Text18.Text = Data7.Rows[0]["HorairesLoge"].ToString();
                //===> Fin Modif Mondir
                Checkboite.CheckState = Data7.Rows[0]["BoiteAClef"] != null && Data7.Rows[0]["BoiteAClef"].ToString() == "True" ? CheckState.Checked : CheckState.Unchecked;
                SituaBoite.Text = Data7.Rows[0]["SitBoiteAClef"].ToString();

                //LES CHAMPS DE LA GROUPEBOX Responsable qui est invisible
                Text3.Text = Data7.Rows[0]["CS1"].ToString();
                Text4.Text = Data7.Rows[0]["TelCS1"].ToString();
                Text5.Text = Data7.Rows[0]["CS2"].ToString();
                Text6.Text = Data7.Rows[0]["TelCS2"].ToString();
                Text7.Text = Data7.Rows[0]["CS3"].ToString();
                Text8.Text = Data7.Rows[0]["TelCS3"].ToString();
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>

        private void UserDocRelTech_VisibleChanged(object sender, EventArgs e)
        {

            DataTable rs = new DataTable();
            if (!Visible)
                return;


            //'=== mdofi du 13 12 2018, ajout de la variable bLoadActivate.
            bLoadActivate = true;
            General.gbnew = false;

            //recherche du nom d utilisateur
            General.gsUtilisateur = General.fncUserName();
            General.open_conn();

            //remplaser cette fonction apres la charge de grid
            fc_DropQualite();//tested


            //lecture du chemin d'acces
            //gDatabase = GetSetting(App.Title, "Acces", "RelTech", "L:\dt\data\agcdelostal.mdb")
            //gsPath = "L:\dt\data"
            //gsCheminPackage = "\\Libnt400\Data\LibVB\DT\"

            // gDatabase = "\\dtnt500\data\agcdelostal.mdb"
            // gsPath = "\\dtnt500\data"
            // gsCheminPackage = "http://dtnt500/INTRANET/packages/"

            //On Error GoTo errConnexion

            //    SQL = "SELECT * FROM TechReleve ORDER BY CodeImmeuble "
            //
            //// var _with36 = datPrimaryRS;
            //.DatabaseName = gDatabase
            datPrimaryRS = new DataTable();
            //// _with36.ConnectionString = General.adocnn.ConnectionString;
            //        .RecordSource = SQL
            //        .Refresh

            //Set gdb = OpenDatabase(gDatabase)

            //ouverture des tables annexes

            //_with37.ConnectionString = General.adocnn.ConnectionString;
            //_with37.CommandType = ADODB.CommandTypeEnum.adCmdUnknown;
            //.DatabaseName = gDatabase
            //_with37.RecordSource = "SELECT Code,Libelle FROM Energie";          
            //_with37.Refresh();

            Data1 = ModAdoData1.fc_OpenRecordSet("SELECT Code,Libelle FROM Energie");
            SSDBCombo1.DataSource = Data1;
            SSDBCombo1.ValueMember = "Code";
            SSDBCombo1.DisplayMember = "Libelle";

            var Data2 = new DataTable();
            // _with38.ConnectionString = General.adocnn.ConnectionString;
            //.DatabaseName = gDatabase
            // _with38.RecordSource = "SELECT * FROM TypeElec";
            Data2 = ModAdoData2.fc_OpenRecordSet("SELECT * FROM TypeElec");
            SSDBCombo2.DataSource = Data2;
            SSDBCombo2.ValueMember = "TypeElec";
            SSDBCombo2.DisplayMember = "Libelle";
            //_with38.Refresh();

            var Data3 = new DataTable();
            //_with39.ConnectionString = General.adocnn.ConnectionString;
            //.DatabaseName = gDatabase
            //_with39.RecordSource = "SELECT TypeChauffage,Libelle FROM TypeChauffage";
            Data3 = ModAdoData3.fc_OpenRecordSet("SELECT TypeChauffage,Libelle FROM TypeChauffage");
            SSDBCombo3.DataSource = Data3;
            SSDBCombo3.ValueMember = "TypeChauffage";
            SSDBCombo3.DisplayMember = "Libelle";


            //_with39.Refresh();

            var Data4 = new DataTable();
            //_with40.ConnectionString = General.adocnn.ConnectionString;
            //.DatabaseName = gDatabase
            //_with40.RecordSource = "SELECT * FROM TypeImmeuble";
            Data4 = ModAdoData4.fc_OpenRecordSet("SELECT * FROM TypeImmeuble");
            SSDBCombo4.ValueMember = "TypeImmeble";
            SSDBCombo4.DisplayMember = "Libelle";
            SSDBCombo4.DataSource = Data4;

            //_with40.Refresh();

            var Data5 = new DataTable();
            // _with41.ConnectionString = General.adocnn.ConnectionString;
            //.DatabaseName = gDatabase
            //_with41.RecordSource = "SELECT * FROM Typefluide";
            Data5 = ModAdoData5.fc_OpenRecordSet("SELECT * FROM Typefluide");
            SSDBCombo5.ValueMember = "CodeFluide";
            SSDBCombo5.DisplayMember = "IntituleFluide";
            SSDBCombo5.DataSource = Data5;

            //_with41.Refresh();

            //rend invisible les boutons FOD et Reseau primaire
            cmdFOD.Visible = false;
            cmdPrimaire.Visible = false;


            //lecture des infos de connexion si il y a lieu
            //    SQL = "SELECT newvar,ancVar,AncProg FROM LiaisonTemporaire "
            //    SQL1 = "WHERE User='" & gsUtilisateur & "' AND NewProg='" & gsProg & "' order by numauto Desc"
            //    SQL = SQL & SQL1
            //    Set rs = gdb.OpenRecordset(SQL, dbOpenSnapshot)
            //    If Not (rs.BOF Or rs.EOF) Then
            //        '----Verifie si le programme appelant est la fiche standard.
            //        If rs!ancprog = PROGINTERVENTION Then
            //            strCheminRetour = rs!ancprog & ""
            //            strValeur = rs!ancVar & ""
            //            cmdRetour.Visible = True
            //        End If
            //         If rs!ancprog = PROGINTERVENTION Then
            //            strCheminRetour = rs!ancprog & ""
            //            strValeur = rs!ancVar & ""
            //            cmdRetour.Visible = True
            //        End If
            //        If rs!ancprog = PROGDEVIS Then
            //            strCheminRetour = rs!ancprog & ""
            //            strValeur = rs!ancVar & ""
            //            cmdRetour.Visible = True
            //        End If
            //        Text10 = rs(0)
            //        Text10_KeyPress (13)
            //        rs.Close
            //        Set rs = Nothing
            //        gbnew = True
            //    End If

            var _with42 = this;

            //- positionne sur le dernier enregistrement.
            if (ModParametre.fc_RecupParam(this.Name) == true)
            {
                // charge les enregistrements
                Text10.Text = ModParametre.sNewVar;
                General.gVar = Text10.Text;
                Text10_KeyPress(Text10, new KeyPressEventArgs((char)13));
                General.gbnew = true;
            }
            else
            {
                //fc_BloqueForm
            }

            //
            //==================== Menu Changé
            //
            //switch (ModParametre.sFicheAppelante)
            //{
            //    case Variable.cUserIntervention:
            //        Views.Theme.Menu.MainMenu.UserDocRelTechMenu[0].LabelItemName.Text = "Intervention";
            //        CodeFicheAppelante = General.getFrmReg(Variable.cUserIntervention, "NewVar", "");
            //        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, CodeFicheAppelante);
            //        Views.Theme.Menu.MainMenu.UserDocRelTechMenu[0].LabelItemName.Visible = true;
            //        Views.Theme.Menu.MainMenu.UserDocRelTechMenu[0].ClickEvent = (se, ev) =>
            //        {
            //            View.Theme.Theme.Navigate(typeof(UserIntervention));

            //        };

            //        break;
            //    case Variable.cUserDocStandard:

            //        Views.Theme.Menu.MainMenu.UserDocRelTechMenu[0].LabelItemName.Text = "FICHE D'APPEL";
            //        CodeFicheAppelante = General.getFrmReg(Variable.cUserDocStandard, "NewVar", "");
            //        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, CodeFicheAppelante);
            //        Views.Theme.Menu.MainMenu.UserDocRelTechMenu[0].LabelItemName.Visible = true;
            //        Views.Theme.Menu.MainMenu.UserDocRelTechMenu[0].ClickEvent = (se, ev) =>
            //        {
            //            View.Theme.Theme.Navigate(typeof(UserDocStandard));

            //        };


            //        break;
            //    //        Case cUserDocClient
            //    //            lblGoFicheAppelante.Caption = "FICHE GERANT"
            //    //            CodeFicheAppelante = GetSetting(cFrNomApp, cUserDocClient, "NewVar", "")
            //    //            lblGoFicheAppelante.Visible = True


            //    case Variable.cUserDocDevis:

            //        Views.Theme.Menu.MainMenu.UserDocRelTechMenu[0].LabelItemName.Text = "Devis";
            //        CodeFicheAppelante = General.getFrmReg(Variable.cUserDocDevis, "NewVar", "");
            //        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocDevis, CodeFicheAppelante);
            //        Views.Theme.Menu.MainMenu.UserDocRelTechMenu[0].LabelItemName.Visible = true;
            //        Views.Theme.Menu.MainMenu.UserDocRelTechMenu[0].ClickEvent = (se, ev) =>
            //        {
            //            View.Theme.Theme.Navigate(typeof(UserDocDevis));

            //        };
            //        break;

            //    default:
            //        // lblGoFicheAppelante.Visible = false;

            //        Views.Theme.Menu.MainMenu.UserDocRelTechMenu[0].LabelItemName.Visible = false;

            //        break;
            //}


            //    If Not gbnew Then
            //
            //        Set gdb = OpenDatabase(gDatabase)
            //        'lecture de la table des liens temporaires
            //        SQL = "SELECT ancVar FROM LiaisonTemporaire "
            //        SQL1 = "WHERE AncProg='" & gsProg & "' AND User='" & gsUtilisateur & "'"
            //        SQL = SQL & SQL1
            //'        Set rs = New ADODB.Recordset
            //'        rs.Open SQL, adocnn
            //        Set rs = gdb.OpenRecordset(SQL)
            //        If Not (rs.BOF Or rs.EOF) Then
            //            Text10 = rs(0)
            //            Text10_KeyPress (13)
            //        End If
            //
            //        'destruction de l'enregistrement
            //        SQL = "DELETE FROM Liaisontemporaire "
            //        SQL = SQL & SQL1
            //        gdb.Execute SQL
            //        rs.Close
            //        Set rs = Nothing
            //        '
            //    End If

            //lblNavigation[0].Text = General.sNomLien0;
            //lblNavigation[1].Text = General.sNomLien1;
            //lblNavigation[2].Text = General.sNomLien2;

            if (!string.IsNullOrEmpty(Text115.Text))
            {
                lblPathCptClim.Font = new Font(lblPathCptClim.Font, FontStyle.Underline);
                // Microsoft.VisualBasic.Compatibility.VB6.Support.FontChangeUnderline(lblPathCptClim.Font, true);
                lblPathCptClim.ForeColor = ColorTranslator.FromOle(0xc00000);
                txtFilePathClim.Text = General.sCheminDossier + "\\" + Text10.Text + "\\" + General.cFolderCptRendu + "\\out\\" + Text115.Text;
                cmdSupFichier.Visible = true;

            }
            else
            {
                lblPathCptClim.Font = new Font(lblPathCptClim.Font, FontStyle.Underline);
                //Microsoft.VisualBasic.Compatibility.VB6.Support.FontChangeUnderline(lblPathCptClim.Font, false);
                lblPathCptClim.ForeColor = ColorTranslator.FromOle(0x0);
                txtFilePathClim.Text = "";
                cmdSupFichier.Visible = false;
            }
            bLoadActivate = false;



        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_AfterRowsDeleted(object sender, EventArgs e)
        {
            // ModAdorsCorr.Update();

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {

            if (ssGridImmCorrespondant.ActiveRow == null) return;
            if (string.IsNullOrEmpty(ssGridImmCorrespondant.ActiveRow.Cells["CodeCorresp_IMC"].Text))
            {

                string insert = "insert into IMC_ImmCorrespondant " +
                     "(CodeImmeuble_IMM,nom_imc,CodeQualif_QUA, tel_imc,  telportable_imc, Fax_IMC, email_imc,  Note_imc,PresidentCS_IMC )" +
                     " values('" + Text10.Text + "','" +
                       ssGridImmCorrespondant.ActiveRow.Cells["Nom"].Text + "','" +
                       ssGridImmCorrespondant.ActiveRow.Cells["CodeQualif"].Text + "','" +
                       ssGridImmCorrespondant.ActiveRow.Cells["Tel"].Text + "','" +
                       ssGridImmCorrespondant.ActiveRow.Cells["TelPortable"].Text + "','" +
                       ssGridImmCorrespondant.ActiveRow.Cells["Fax"].Text + "','" +
                       ssGridImmCorrespondant.ActiveRow.Cells["Email"].Text + "','" +
                       ssGridImmCorrespondant.ActiveRow.Cells["Note"].Text + "','" +
                       ssGridImmCorrespondant.ActiveRow.Cells["PresidentCS_IMC"].Text + "')";
                int aa = General.Execute(insert);

                using (var tmp = new ModAdo())
                {
                    int cle = Convert.ToInt32(tmp.fc_ADOlibelle("select max(CodeCorresp_IMC) from IMC_ImmCorrespondant"));
                    ssGridImmCorrespondant.ActiveRow.Cells["CodeCorresp_IMC"].Value = cle.ToString();
                }
            }
            if (General.nz(ssGridImmCorrespondant.ActiveRow.Cells["PresidentCS_IMC"].Value, "0").ToString() != "0")
            {
                int xx = General.Execute("UPDATE IMC_ImmCorrespondant SET PresidentCS_IMC='0' WHERE CodeImmeuble_IMM='" + Text10.Text + "'");
            }

            string update = "UPDATE IMC_ImmCorrespondant SET PresidentCS_IMC='" + ssGridImmCorrespondant.ActiveRow.Cells["PresidentCS_IMC"].Text + "'";
            update = update + ",  nom_imc = '" + ssGridImmCorrespondant.ActiveRow.Cells["Nom"].Text + "'";
            update = update + ", CodeQualif_QUA = '" + ssGridImmCorrespondant.ActiveRow.Cells["CodeQualif"].Text + "'";
            update = update + " , tel_imc = '" + ssGridImmCorrespondant.ActiveRow.Cells["Tel"].Text + "'";
            update = update + ",  telportable_imc = '" + ssGridImmCorrespondant.ActiveRow.Cells["TelPortable"].Text + "'";
            update = update + ", Fax_IMC= '" + ssGridImmCorrespondant.ActiveRow.Cells["Fax"].Text + "'";
            update = update + ", email_imc = '" + ssGridImmCorrespondant.ActiveRow.Cells["Email"].Text + "'";
            update = update + ",  Note_imc = '" + ssGridImmCorrespondant.ActiveRow.Cells["Note"].Text + "'";
            update = update + " WHERE CodeCorresp_IMC =" + ssGridImmCorrespondant.ActiveRow.Cells["CodeCorresp_IMC"].Value + " and  CodeImmeuble_IMM='" + Text10.Text + "'";
            int yy = General.Execute(update);
            //ModAdorsCorr.Update();
            RemplitGrid();

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            this.ssGridImmCorrespondant.DisplayLayout.Bands[0].Columns["CodeImmeuble_IMM"].Hidden = true;
            this.ssGridImmCorrespondant.DisplayLayout.Bands[0].Columns["CodeCorresp_IMC"].Hidden = true;
            this.ssGridImmCorrespondant.DisplayLayout.Bands[0].Columns["PresidentCS_IMC"].Hidden = true;
            this.ssGridImmCorrespondant.DisplayLayout.Bands[0].Columns["Qualification"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            this.ssGridImmCorrespondant.DisplayLayout.Bands[0].Columns["Qualification"].CellAppearance.BackColor = Color.Gray;
            this.ssGridImmCorrespondant.DisplayLayout.Bands[0].Columns["Président du conseil syndical"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            ssGridImmCorrespondant.DisplayLayout.Bands[0].Columns["CodeQualif"].ValueList = ssDropQualite;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_AfterExitEditMode(object sender, EventArgs e)
        {
            if (ssGridImmCorrespondant.ActiveRow.Cells["Président du conseil syndical"].Value.ToString() == "True")
            {
                ssGridImmCorrespondant.ActiveRow.Cells["PresidentCS_IMC"].Value = "-1";
            }
            else
            {
                ssGridImmCorrespondant.ActiveRow.Cells["PresidentCS_IMC"].Value = "0";
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void linkLabel1_LinkClicked(object sender, LinkLabelLinkClickedEventArgs e)
        {
            Cursor = Cursors.Default;
            //stocke les paramétres pour la feuille de destination.
            ModParametre.fc_SaveParamPosition(Name, Variable.cUserDocImmeuble, Text10.Text);
            View.Theme.Theme.Navigate(typeof(UserDocImmeuble));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridImmCorrespondant_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
            string delete = "delete from IMC_ImmCorrespondant  where CodeCorresp_IMC ="
                + ssGridImmCorrespondant.ActiveRow.Cells["CodeCorresp_IMC"].Text + " and  CodeImmeuble_IMM='" + Text10.Text + "'";
            int xx = General.Execute(delete);
        }


        private void Text113_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(Text113.Text) && !General.IsNumeric(Text113.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("la valeur saisie est invalide.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text113.Focus();
                return;
            }
        }

        private void Text19_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(Text19.Text) && !General.IsNumeric(Text19.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("la valeur saisie est invalide.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text19.Focus();
                return;
            }
        }

        private void Text12_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(Text12.Text) && !General.IsNumeric(Text12.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("la valeur saisie est invalide.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text12.Focus();
                return;
            }
        }

        private void Text13_Validating(object sender, CancelEventArgs e)
        {
            if (!string.IsNullOrEmpty(Text13.Text) && !General.IsDate(Text13.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("la date saisie est invalide.", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                Text13.Focus();
                //return;
            }

        }








        //Private Sub Me_Terminate()
        //    Dim i As Integer
        //    'fermeture de toutes les feuilles annexes
        //    For i = 0 To Forms.Count - 1
        //        Unload Forms(i)
        //    Next
        //
        //    If Not rsCorr Is Nothing Then
        //        If rsCorr.State = adStateOpen Then
        //            rsCorr.Close
        //        End If
        //        Set rsCorr = Nothing
        //    End If
        //
        //End Sub
        //=======================================================================================

    }
}
