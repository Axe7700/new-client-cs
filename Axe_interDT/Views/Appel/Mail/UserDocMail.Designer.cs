namespace Axe_interDT.Views.Appel.Mail
{
    partial class UserDocMail
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTextCommentaires = new System.Windows.Forms.Label();
            this.lblTexteDe = new System.Windows.Forms.Label();
            this.lblDestinataire = new System.Windows.Forms.Label();
            this.lblTexteOrigine = new System.Windows.Forms.Label();
            this.lblTexteLe = new System.Windows.Forms.Label();
            this.lblTexteEmplacement = new System.Windows.Forms.Label();
            this.lblTextQui = new System.Windows.Forms.Label();
            this.Label1 = new System.Windows.Forms.Label();
            this.lblTextNom = new System.Windows.Forms.Label();
            this.lblTextCodeProduit = new System.Windows.Forms.Label();
            this.lblDe = new iTalk.iTalk_TextBox_Small2();
            this.lblLe = new iTalk.iTalk_TextBox_Small2();
            this.lblOrigine = new iTalk.iTalk_TextBox_Small2();
            this.lblEmplacement = new iTalk.iTalk_TextBox_Small2();
            this.lblQui = new iTalk.iTalk_TextBox_Small2();
            this.lblNom = new iTalk.iTalk_TextBox_Small2();
            this.txtDocuments = new iTalk.iTalk_TextBox_Small2();
            this.SSOleDBcmbDestinataire = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.lblSociete = new iTalk.iTalk_TextBox_Small2();
            this.lblCodeImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.lblTextSociete = new System.Windows.Forms.Label();
            this.lblTextCodeImmeuble = new System.Windows.Forms.Label();
            this.cmdCC = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.optBasse = new System.Windows.Forms.RadioButton();
            this.optNormale = new System.Windows.Forms.RadioButton();
            this.optHaute = new System.Windows.Forms.RadioButton();
            this.label11 = new System.Windows.Forms.Label();
            this.txtCommentaires = new iTalk.iTalk_RichTextBox();
            this.GridDocument = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.SSOleDBCmbCodProd = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCopie = new iTalk.iTalk_RichTextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.btnAnnuler = new System.Windows.Forms.Button();
            this.btnEnvoyer = new System.Windows.Forms.Button();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBcmbDestinataire)).BeginInit();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDocument)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBCmbCodProd)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 5;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 43F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.lblTextCommentaires, 0, 12);
            this.tableLayoutPanel1.Controls.Add(this.lblTexteDe, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblDestinataire, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblTexteOrigine, 0, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblTexteLe, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblTexteEmplacement, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblTextQui, 0, 6);
            this.tableLayoutPanel1.Controls.Add(this.Label1, 0, 10);
            this.tableLayoutPanel1.Controls.Add(this.lblTextNom, 0, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblTextCodeProduit, 0, 8);
            this.tableLayoutPanel1.Controls.Add(this.lblDe, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.lblLe, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblOrigine, 1, 4);
            this.tableLayoutPanel1.Controls.Add(this.lblEmplacement, 1, 5);
            this.tableLayoutPanel1.Controls.Add(this.lblQui, 1, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblNom, 1, 7);
            this.tableLayoutPanel1.Controls.Add(this.txtDocuments, 0, 11);
            this.tableLayoutPanel1.Controls.Add(this.SSOleDBcmbDestinataire, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblSociete, 4, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblCodeImmeuble, 4, 7);
            this.tableLayoutPanel1.Controls.Add(this.lblTextSociete, 3, 6);
            this.tableLayoutPanel1.Controls.Add(this.lblTextCodeImmeuble, 3, 7);
            this.tableLayoutPanel1.Controls.Add(this.cmdCC, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.groupBox6, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.label11, 3, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtCommentaires, 1, 12);
            this.tableLayoutPanel1.Controls.Add(this.GridDocument, 1, 10);
            this.tableLayoutPanel1.Controls.Add(this.SSOleDBCmbCodProd, 1, 8);
            this.tableLayoutPanel1.Controls.Add(this.txtCopie, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 2, 13);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 14;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // lblTextCommentaires
            // 
            this.lblTextCommentaires.AutoSize = true;
            this.lblTextCommentaires.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTextCommentaires.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextCommentaires.Location = new System.Drawing.Point(3, 603);
            this.lblTextCommentaires.Name = "lblTextCommentaires";
            this.lblTextCommentaires.Size = new System.Drawing.Size(188, 313);
            this.lblTextCommentaires.TabIndex = 515;
            this.lblTextCommentaires.Text = "Commentaires";
            // 
            // lblTexteDe
            // 
            this.lblTexteDe.AutoSize = true;
            this.lblTexteDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTexteDe.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTexteDe.Location = new System.Drawing.Point(3, 10);
            this.lblTexteDe.Name = "lblTexteDe";
            this.lblTexteDe.Size = new System.Drawing.Size(188, 30);
            this.lblTexteDe.TabIndex = 388;
            this.lblTexteDe.Text = "Emetteur de la fiche d\'appel";
            // 
            // lblDestinataire
            // 
            this.lblDestinataire.AutoSize = true;
            this.lblDestinataire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDestinataire.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblDestinataire.Location = new System.Drawing.Point(3, 40);
            this.lblDestinataire.Name = "lblDestinataire";
            this.lblDestinataire.Size = new System.Drawing.Size(188, 30);
            this.lblDestinataire.TabIndex = 387;
            this.lblDestinataire.Text = "Destinataire";
            // 
            // lblTexteOrigine
            // 
            this.lblTexteOrigine.AutoSize = true;
            this.lblTexteOrigine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTexteOrigine.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTexteOrigine.Location = new System.Drawing.Point(3, 100);
            this.lblTexteOrigine.Name = "lblTexteOrigine";
            this.lblTexteOrigine.Size = new System.Drawing.Size(188, 30);
            this.lblTexteOrigine.TabIndex = 384;
            this.lblTexteOrigine.Text = "Origine communication";
            // 
            // lblTexteLe
            // 
            this.lblTexteLe.AutoSize = true;
            this.lblTexteLe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTexteLe.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTexteLe.Location = new System.Drawing.Point(3, 70);
            this.lblTexteLe.Name = "lblTexteLe";
            this.lblTexteLe.Size = new System.Drawing.Size(188, 30);
            this.lblTexteLe.TabIndex = 389;
            this.lblTexteLe.Text = "Message crée le";
            // 
            // lblTexteEmplacement
            // 
            this.lblTexteEmplacement.AutoSize = true;
            this.lblTexteEmplacement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTexteEmplacement.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTexteEmplacement.Location = new System.Drawing.Point(3, 130);
            this.lblTexteEmplacement.Name = "lblTexteEmplacement";
            this.lblTexteEmplacement.Size = new System.Drawing.Size(188, 30);
            this.lblTexteEmplacement.TabIndex = 386;
            this.lblTexteEmplacement.Text = "Emplacement";
            // 
            // lblTextQui
            // 
            this.lblTextQui.AutoSize = true;
            this.lblTextQui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTextQui.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextQui.Location = new System.Drawing.Point(3, 160);
            this.lblTextQui.Name = "lblTextQui";
            this.lblTextQui.Size = new System.Drawing.Size(188, 30);
            this.lblTextQui.TabIndex = 385;
            this.lblTextQui.Text = "Interlocuteur";
            // 
            // Label1
            // 
            this.Label1.AutoSize = true;
            this.Label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(3, 260);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(188, 30);
            this.Label1.TabIndex = 392;
            this.Label1.Text = "Documents joints";
            // 
            // lblTextNom
            // 
            this.lblTextNom.AutoSize = true;
            this.lblTextNom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTextNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextNom.Location = new System.Drawing.Point(3, 190);
            this.lblTextNom.Name = "lblTextNom";
            this.lblTextNom.Size = new System.Drawing.Size(188, 30);
            this.lblTextNom.TabIndex = 390;
            this.lblTextNom.Text = "Nom";
            // 
            // lblTextCodeProduit
            // 
            this.lblTextCodeProduit.AutoSize = true;
            this.lblTextCodeProduit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTextCodeProduit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextCodeProduit.Location = new System.Drawing.Point(3, 220);
            this.lblTextCodeProduit.Name = "lblTextCodeProduit";
            this.lblTextCodeProduit.Size = new System.Drawing.Size(188, 30);
            this.lblTextCodeProduit.TabIndex = 393;
            this.lblTextCodeProduit.Text = "Code Produit";
            // 
            // lblDe
            // 
            this.lblDe.AccAcceptNumbersOnly = false;
            this.lblDe.AccAllowComma = false;
            this.lblDe.AccBackgroundColor = System.Drawing.Color.White;
            this.lblDe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblDe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblDe.AccHidenValue = "";
            this.lblDe.AccNotAllowedChars = null;
            this.lblDe.AccReadOnly = false;
            this.lblDe.AccReadOnlyAllowDelete = false;
            this.lblDe.AccRequired = false;
            this.lblDe.BackColor = System.Drawing.Color.White;
            this.lblDe.CustomBackColor = System.Drawing.Color.White;
            this.lblDe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblDe.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblDe.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblDe.Location = new System.Drawing.Point(196, 12);
            this.lblDe.Margin = new System.Windows.Forms.Padding(2);
            this.lblDe.MaxLength = 32767;
            this.lblDe.Multiline = false;
            this.lblDe.Name = "lblDe";
            this.lblDe.ReadOnly = true;
            this.lblDe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblDe.Size = new System.Drawing.Size(724, 27);
            this.lblDe.TabIndex = 0;
            this.lblDe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblDe.UseSystemPasswordChar = false;
            this.lblDe.TextChanged += new System.EventHandler(this.lblDe_TextChanged);
            // 
            // lblLe
            // 
            this.lblLe.AccAcceptNumbersOnly = false;
            this.lblLe.AccAllowComma = false;
            this.lblLe.AccBackgroundColor = System.Drawing.Color.White;
            this.lblLe.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblLe.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblLe.AccHidenValue = "";
            this.lblLe.AccNotAllowedChars = null;
            this.lblLe.AccReadOnly = false;
            this.lblLe.AccReadOnlyAllowDelete = false;
            this.lblLe.AccRequired = false;
            this.lblLe.BackColor = System.Drawing.Color.White;
            this.lblLe.CustomBackColor = System.Drawing.Color.White;
            this.lblLe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLe.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblLe.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblLe.Location = new System.Drawing.Point(196, 72);
            this.lblLe.Margin = new System.Windows.Forms.Padding(2);
            this.lblLe.MaxLength = 32767;
            this.lblLe.Multiline = false;
            this.lblLe.Name = "lblLe";
            this.lblLe.ReadOnly = true;
            this.lblLe.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblLe.Size = new System.Drawing.Size(724, 27);
            this.lblLe.TabIndex = 2;
            this.lblLe.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblLe.UseSystemPasswordChar = false;
            // 
            // lblOrigine
            // 
            this.lblOrigine.AccAcceptNumbersOnly = false;
            this.lblOrigine.AccAllowComma = false;
            this.lblOrigine.AccBackgroundColor = System.Drawing.Color.White;
            this.lblOrigine.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblOrigine.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblOrigine.AccHidenValue = "";
            this.lblOrigine.AccNotAllowedChars = null;
            this.lblOrigine.AccReadOnly = false;
            this.lblOrigine.AccReadOnlyAllowDelete = false;
            this.lblOrigine.AccRequired = false;
            this.lblOrigine.BackColor = System.Drawing.Color.White;
            this.lblOrigine.CustomBackColor = System.Drawing.Color.White;
            this.lblOrigine.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblOrigine.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblOrigine.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblOrigine.Location = new System.Drawing.Point(196, 102);
            this.lblOrigine.Margin = new System.Windows.Forms.Padding(2);
            this.lblOrigine.MaxLength = 32767;
            this.lblOrigine.Multiline = false;
            this.lblOrigine.Name = "lblOrigine";
            this.lblOrigine.ReadOnly = true;
            this.lblOrigine.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblOrigine.Size = new System.Drawing.Size(724, 27);
            this.lblOrigine.TabIndex = 3;
            this.lblOrigine.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblOrigine.UseSystemPasswordChar = false;
            // 
            // lblEmplacement
            // 
            this.lblEmplacement.AccAcceptNumbersOnly = false;
            this.lblEmplacement.AccAllowComma = false;
            this.lblEmplacement.AccBackgroundColor = System.Drawing.Color.White;
            this.lblEmplacement.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblEmplacement.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblEmplacement.AccHidenValue = "";
            this.lblEmplacement.AccNotAllowedChars = null;
            this.lblEmplacement.AccReadOnly = false;
            this.lblEmplacement.AccReadOnlyAllowDelete = false;
            this.lblEmplacement.AccRequired = false;
            this.lblEmplacement.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel1.SetColumnSpan(this.lblEmplacement, 4);
            this.lblEmplacement.CustomBackColor = System.Drawing.Color.White;
            this.lblEmplacement.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblEmplacement.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblEmplacement.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblEmplacement.Location = new System.Drawing.Point(196, 132);
            this.lblEmplacement.Margin = new System.Windows.Forms.Padding(2);
            this.lblEmplacement.MaxLength = 32767;
            this.lblEmplacement.Multiline = false;
            this.lblEmplacement.Name = "lblEmplacement";
            this.lblEmplacement.ReadOnly = true;
            this.lblEmplacement.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblEmplacement.Size = new System.Drawing.Size(1652, 27);
            this.lblEmplacement.TabIndex = 4;
            this.lblEmplacement.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblEmplacement.UseSystemPasswordChar = false;
            // 
            // lblQui
            // 
            this.lblQui.AccAcceptNumbersOnly = false;
            this.lblQui.AccAllowComma = false;
            this.lblQui.AccBackgroundColor = System.Drawing.Color.White;
            this.lblQui.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblQui.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblQui.AccHidenValue = "";
            this.lblQui.AccNotAllowedChars = null;
            this.lblQui.AccReadOnly = false;
            this.lblQui.AccReadOnlyAllowDelete = false;
            this.lblQui.AccRequired = false;
            this.lblQui.BackColor = System.Drawing.Color.White;
            this.lblQui.CustomBackColor = System.Drawing.Color.White;
            this.lblQui.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblQui.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblQui.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblQui.Location = new System.Drawing.Point(196, 162);
            this.lblQui.Margin = new System.Windows.Forms.Padding(2);
            this.lblQui.MaxLength = 32767;
            this.lblQui.Multiline = false;
            this.lblQui.Name = "lblQui";
            this.lblQui.ReadOnly = true;
            this.lblQui.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblQui.Size = new System.Drawing.Size(724, 27);
            this.lblQui.TabIndex = 5;
            this.lblQui.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblQui.UseSystemPasswordChar = false;
            // 
            // lblNom
            // 
            this.lblNom.AccAcceptNumbersOnly = false;
            this.lblNom.AccAllowComma = false;
            this.lblNom.AccBackgroundColor = System.Drawing.Color.White;
            this.lblNom.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblNom.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblNom.AccHidenValue = "";
            this.lblNom.AccNotAllowedChars = null;
            this.lblNom.AccReadOnly = false;
            this.lblNom.AccReadOnlyAllowDelete = false;
            this.lblNom.AccRequired = false;
            this.lblNom.BackColor = System.Drawing.Color.White;
            this.lblNom.CustomBackColor = System.Drawing.Color.White;
            this.lblNom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblNom.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblNom.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblNom.Location = new System.Drawing.Point(196, 192);
            this.lblNom.Margin = new System.Windows.Forms.Padding(2);
            this.lblNom.MaxLength = 32767;
            this.lblNom.Multiline = false;
            this.lblNom.Name = "lblNom";
            this.lblNom.ReadOnly = true;
            this.lblNom.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblNom.Size = new System.Drawing.Size(724, 27);
            this.lblNom.TabIndex = 6;
            this.lblNom.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblNom.UseSystemPasswordChar = false;
            // 
            // txtDocuments
            // 
            this.txtDocuments.AccAcceptNumbersOnly = false;
            this.txtDocuments.AccAllowComma = false;
            this.txtDocuments.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDocuments.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDocuments.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDocuments.AccHidenValue = "";
            this.txtDocuments.AccNotAllowedChars = null;
            this.txtDocuments.AccReadOnly = false;
            this.txtDocuments.AccReadOnlyAllowDelete = false;
            this.txtDocuments.AccRequired = false;
            this.txtDocuments.BackColor = System.Drawing.Color.White;
            this.txtDocuments.CustomBackColor = System.Drawing.Color.White;
            this.txtDocuments.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtDocuments.ForeColor = System.Drawing.Color.Black;
            this.txtDocuments.Location = new System.Drawing.Point(2, 292);
            this.txtDocuments.Margin = new System.Windows.Forms.Padding(2);
            this.txtDocuments.MaxLength = 32767;
            this.txtDocuments.Multiline = false;
            this.txtDocuments.Name = "txtDocuments";
            this.txtDocuments.ReadOnly = false;
            this.txtDocuments.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDocuments.Size = new System.Drawing.Size(135, 27);
            this.txtDocuments.TabIndex = 509;
            this.txtDocuments.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDocuments.UseSystemPasswordChar = false;
            this.txtDocuments.Visible = false;
            // 
            // SSOleDBcmbDestinataire
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBcmbDestinataire.DisplayLayout.Appearance = appearance1;
            this.SSOleDBcmbDestinataire.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBcmbDestinataire.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBcmbDestinataire.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBcmbDestinataire.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.SSOleDBcmbDestinataire.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBcmbDestinataire.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.SSOleDBcmbDestinataire.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBcmbDestinataire.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.CellAppearance = appearance8;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.RowAppearance = appearance11;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBcmbDestinataire.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.SSOleDBcmbDestinataire.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBcmbDestinataire.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBcmbDestinataire.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBcmbDestinataire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSOleDBcmbDestinataire.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SSOleDBcmbDestinataire.Location = new System.Drawing.Point(197, 43);
            this.SSOleDBcmbDestinataire.Name = "SSOleDBcmbDestinataire";
            this.SSOleDBcmbDestinataire.Size = new System.Drawing.Size(722, 27);
            this.SSOleDBcmbDestinataire.TabIndex = 1;
            this.SSOleDBcmbDestinataire.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSOleDBcmbDestinataire_InitializeLayout);
            this.SSOleDBcmbDestinataire.Click += new System.EventHandler(this.SSOleDBcmbDestinataire_Click);
            // 
            // lblSociete
            // 
            this.lblSociete.AccAcceptNumbersOnly = false;
            this.lblSociete.AccAllowComma = false;
            this.lblSociete.AccBackgroundColor = System.Drawing.Color.White;
            this.lblSociete.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblSociete.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblSociete.AccHidenValue = "";
            this.lblSociete.AccNotAllowedChars = null;
            this.lblSociete.AccReadOnly = false;
            this.lblSociete.AccReadOnlyAllowDelete = false;
            this.lblSociete.AccRequired = false;
            this.lblSociete.BackColor = System.Drawing.Color.White;
            this.lblSociete.CustomBackColor = System.Drawing.Color.White;
            this.lblSociete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblSociete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblSociete.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblSociete.Location = new System.Drawing.Point(1124, 162);
            this.lblSociete.Margin = new System.Windows.Forms.Padding(2);
            this.lblSociete.MaxLength = 32767;
            this.lblSociete.Multiline = false;
            this.lblSociete.Name = "lblSociete";
            this.lblSociete.ReadOnly = true;
            this.lblSociete.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblSociete.Size = new System.Drawing.Size(724, 27);
            this.lblSociete.TabIndex = 9;
            this.lblSociete.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblSociete.UseSystemPasswordChar = false;
            // 
            // lblCodeImmeuble
            // 
            this.lblCodeImmeuble.AccAcceptNumbersOnly = false;
            this.lblCodeImmeuble.AccAllowComma = false;
            this.lblCodeImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.lblCodeImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblCodeImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblCodeImmeuble.AccHidenValue = "";
            this.lblCodeImmeuble.AccNotAllowedChars = null;
            this.lblCodeImmeuble.AccReadOnly = false;
            this.lblCodeImmeuble.AccReadOnlyAllowDelete = false;
            this.lblCodeImmeuble.AccRequired = false;
            this.lblCodeImmeuble.BackColor = System.Drawing.Color.White;
            this.lblCodeImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.lblCodeImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCodeImmeuble.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.lblCodeImmeuble.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblCodeImmeuble.Location = new System.Drawing.Point(1124, 192);
            this.lblCodeImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.lblCodeImmeuble.MaxLength = 32767;
            this.lblCodeImmeuble.Multiline = false;
            this.lblCodeImmeuble.Name = "lblCodeImmeuble";
            this.lblCodeImmeuble.ReadOnly = true;
            this.lblCodeImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblCodeImmeuble.Size = new System.Drawing.Size(724, 27);
            this.lblCodeImmeuble.TabIndex = 10;
            this.lblCodeImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblCodeImmeuble.UseSystemPasswordChar = false;
            // 
            // lblTextSociete
            // 
            this.lblTextSociete.AutoSize = true;
            this.lblTextSociete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTextSociete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextSociete.Location = new System.Drawing.Point(968, 160);
            this.lblTextSociete.Name = "lblTextSociete";
            this.lblTextSociete.Size = new System.Drawing.Size(151, 30);
            this.lblTextSociete.TabIndex = 396;
            this.lblTextSociete.Text = "Société";
            // 
            // lblTextCodeImmeuble
            // 
            this.lblTextCodeImmeuble.AutoSize = true;
            this.lblTextCodeImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblTextCodeImmeuble.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblTextCodeImmeuble.Location = new System.Drawing.Point(968, 190);
            this.lblTextCodeImmeuble.Name = "lblTextCodeImmeuble";
            this.lblTextCodeImmeuble.Size = new System.Drawing.Size(151, 30);
            this.lblTextCodeImmeuble.TabIndex = 394;
            this.lblTextCodeImmeuble.Text = "Code Immeuble";
            // 
            // cmdCC
            // 
            this.cmdCC.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdCC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdCC.FlatAppearance.BorderSize = 0;
            this.cmdCC.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCC.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.cmdCC.ForeColor = System.Drawing.Color.White;
            this.cmdCC.Location = new System.Drawing.Point(925, 43);
            this.cmdCC.Name = "cmdCC";
            this.cmdCC.Size = new System.Drawing.Size(37, 24);
            this.cmdCC.TabIndex = 516;
            this.cmdCC.Text = "Cc..";
            this.cmdCC.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdCC.UseVisualStyleBackColor = false;
            this.cmdCC.Click += new System.EventHandler(this.cmdCC_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.groupBox6, 2);
            this.groupBox6.Controls.Add(this.tableLayoutPanel2);
            this.groupBox6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox6.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.groupBox6.Location = new System.Drawing.Point(968, 73);
            this.groupBox6.Name = "groupBox6";
            this.tableLayoutPanel1.SetRowSpan(this.groupBox6, 2);
            this.groupBox6.Size = new System.Drawing.Size(879, 54);
            this.groupBox6.TabIndex = 514;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Importance";
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.optBasse, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.optNormale, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.optHaute, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(873, 31);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // optBasse
            // 
            this.optBasse.AutoSize = true;
            this.optBasse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optBasse.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.optBasse.Location = new System.Drawing.Point(585, 3);
            this.optBasse.Name = "optBasse";
            this.optBasse.Size = new System.Drawing.Size(285, 25);
            this.optBasse.TabIndex = 540;
            this.optBasse.Text = "Basse";
            this.optBasse.UseVisualStyleBackColor = true;
            // 
            // optNormale
            // 
            this.optNormale.AutoSize = true;
            this.optNormale.Checked = true;
            this.optNormale.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optNormale.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.optNormale.Location = new System.Drawing.Point(294, 3);
            this.optNormale.Name = "optNormale";
            this.optNormale.Size = new System.Drawing.Size(285, 25);
            this.optNormale.TabIndex = 539;
            this.optNormale.TabStop = true;
            this.optNormale.Text = "Normale";
            this.optNormale.UseVisualStyleBackColor = true;
            // 
            // optHaute
            // 
            this.optHaute.AutoSize = true;
            this.optHaute.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optHaute.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.optHaute.Location = new System.Drawing.Point(3, 3);
            this.optHaute.Name = "optHaute";
            this.optHaute.Size = new System.Drawing.Size(285, 25);
            this.optHaute.TabIndex = 538;
            this.optHaute.Text = "Haute";
            this.optHaute.UseVisualStyleBackColor = true;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.label11, 2);
            this.label11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label11.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.ForeColor = System.Drawing.Color.Navy;
            this.label11.Location = new System.Drawing.Point(968, 10);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(879, 30);
            this.label11.TabIndex = 395;
            this.label11.Text = "Messagerie Interne";
            this.label11.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // txtCommentaires
            // 
            this.txtCommentaires.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCommentaires.AutoWordSelection = false;
            this.txtCommentaires.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtCommentaires, 4);
            this.txtCommentaires.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCommentaires.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtCommentaires.ForeColor = System.Drawing.Color.Black;
            this.txtCommentaires.Location = new System.Drawing.Point(197, 606);
            this.txtCommentaires.Name = "txtCommentaires";
            this.txtCommentaires.ReadOnly = false;
            this.txtCommentaires.Size = new System.Drawing.Size(1650, 307);
            this.txtCommentaires.TabIndex = 12;
            this.txtCommentaires.WordWrap = true;
            // 
            // GridDocument
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.GridDocument, 4);
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridDocument.DisplayLayout.Appearance = appearance13;
            this.GridDocument.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridDocument.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.GridDocument.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridDocument.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.GridDocument.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridDocument.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.GridDocument.DisplayLayout.MaxColScrollRegions = 1;
            this.GridDocument.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridDocument.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridDocument.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.GridDocument.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.GridDocument.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.GridDocument.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.GridDocument.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridDocument.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.GridDocument.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridDocument.DisplayLayout.Override.CellAppearance = appearance20;
            this.GridDocument.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridDocument.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.GridDocument.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.GridDocument.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.GridDocument.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridDocument.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.GridDocument.DisplayLayout.Override.RowAppearance = appearance23;
            this.GridDocument.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridDocument.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridDocument.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.GridDocument.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridDocument.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridDocument.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridDocument.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.GridDocument.Location = new System.Drawing.Point(197, 263);
            this.GridDocument.Name = "GridDocument";
            this.tableLayoutPanel1.SetRowSpan(this.GridDocument, 2);
            this.GridDocument.Size = new System.Drawing.Size(1650, 337);
            this.GridDocument.TabIndex = 11;
            this.GridDocument.Text = "ultraGrid1";
            this.GridDocument.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridDocument_InitializeLayout);
            this.GridDocument.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.GridDocument_ClickCellButton);
            this.GridDocument.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.GridDocument_DoubleClickRow);
            // 
            // SSOleDBCmbCodProd
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSOleDBCmbCodProd.DisplayLayout.Appearance = appearance25;
            this.SSOleDBCmbCodProd.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSOleDBCmbCodProd.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBCmbCodProd.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBCmbCodProd.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.SSOleDBCmbCodProd.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSOleDBCmbCodProd.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.SSOleDBCmbCodProd.DisplayLayout.MaxColScrollRegions = 1;
            this.SSOleDBCmbCodProd.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSOleDBCmbCodProd.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSOleDBCmbCodProd.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.SSOleDBCmbCodProd.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSOleDBCmbCodProd.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.SSOleDBCmbCodProd.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSOleDBCmbCodProd.DisplayLayout.Override.CellAppearance = appearance32;
            this.SSOleDBCmbCodProd.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSOleDBCmbCodProd.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.SSOleDBCmbCodProd.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.SSOleDBCmbCodProd.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.SSOleDBCmbCodProd.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBCmbCodProd.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.SSOleDBCmbCodProd.DisplayLayout.Override.RowAppearance = appearance35;
            this.SSOleDBCmbCodProd.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSOleDBCmbCodProd.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.SSOleDBCmbCodProd.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSOleDBCmbCodProd.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSOleDBCmbCodProd.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.SSOleDBCmbCodProd.Dock = System.Windows.Forms.DockStyle.Fill;
            this.SSOleDBCmbCodProd.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.SSOleDBCmbCodProd.Location = new System.Drawing.Point(197, 223);
            this.SSOleDBCmbCodProd.Name = "SSOleDBCmbCodProd";
            this.SSOleDBCmbCodProd.Size = new System.Drawing.Size(722, 27);
            this.SSOleDBCmbCodProd.TabIndex = 7;
            this.SSOleDBCmbCodProd.Click += new System.EventHandler(this.SSOleDBCmbCodProd_Click);
            // 
            // txtCopie
            // 
            this.txtCopie.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCopie.AutoWordSelection = false;
            this.txtCopie.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.txtCopie, 2);
            this.txtCopie.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCopie.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.txtCopie.ForeColor = System.Drawing.Color.Black;
            this.txtCopie.Location = new System.Drawing.Point(968, 43);
            this.txtCopie.Name = "txtCopie";
            this.txtCopie.ReadOnly = true;
            this.txtCopie.Size = new System.Drawing.Size(879, 24);
            this.txtCopie.TabIndex = 8;
            this.txtCopie.WordWrap = true;
            // 
            // flowLayoutPanel1
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.flowLayoutPanel1, 2);
            this.flowLayoutPanel1.Controls.Add(this.btnAnnuler);
            this.flowLayoutPanel1.Controls.Add(this.btnEnvoyer);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(922, 916);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(200, 41);
            this.flowLayoutPanel1.TabIndex = 575;
            // 
            // btnAnnuler
            // 
            this.btnAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.btnAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.btnAnnuler.FlatAppearance.BorderSize = 0;
            this.btnAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.btnAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.btnAnnuler.Location = new System.Drawing.Point(2, 2);
            this.btnAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.btnAnnuler.Name = "btnAnnuler";
            this.btnAnnuler.Size = new System.Drawing.Size(60, 35);
            this.btnAnnuler.TabIndex = 575;
            this.btnAnnuler.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.btnAnnuler.UseVisualStyleBackColor = false;
            this.btnAnnuler.Click += new System.EventHandler(this.btnAnnuler_Click);
            // 
            // btnEnvoyer
            // 
            this.btnEnvoyer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.btnEnvoyer.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnEnvoyer.FlatAppearance.BorderSize = 0;
            this.btnEnvoyer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnEnvoyer.Image = global::Axe_interDT.Properties.Resources.new_post_24;
            this.btnEnvoyer.Location = new System.Drawing.Point(66, 2);
            this.btnEnvoyer.Margin = new System.Windows.Forms.Padding(2);
            this.btnEnvoyer.Name = "btnEnvoyer";
            this.btnEnvoyer.Size = new System.Drawing.Size(60, 35);
            this.btnEnvoyer.TabIndex = 574;
            this.btnEnvoyer.UseVisualStyleBackColor = false;
            this.btnEnvoyer.Click += new System.EventHandler(this.btnEnvoyer_Click);
            // 
            // UserDocMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserDocMail";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "UserDocMail";
            this.VisibleChanged += new System.EventHandler(this.UserDocMail_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBcmbDestinataire)).EndInit();
            this.groupBox6.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridDocument)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBCmbCodProd)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.Label lblTexteDe;
        public System.Windows.Forms.Label lblDestinataire;
        public System.Windows.Forms.Label lblTexteOrigine;
        public System.Windows.Forms.Label lblTexteLe;
        public System.Windows.Forms.Label lblTexteEmplacement;
        public System.Windows.Forms.Label lblTextQui;
        public System.Windows.Forms.Label Label1;
        public System.Windows.Forms.Label lblTextNom;
        public System.Windows.Forms.Label lblTextCodeProduit;
        public System.Windows.Forms.Label lblTextSociete;
        public System.Windows.Forms.Label lblTextCodeImmeuble;
        public System.Windows.Forms.Label label11;
        public iTalk.iTalk_TextBox_Small2 lblDe;
        public iTalk.iTalk_TextBox_Small2 lblLe;
        public iTalk.iTalk_TextBox_Small2 lblOrigine;
        public iTalk.iTalk_TextBox_Small2 lblEmplacement;
        public iTalk.iTalk_TextBox_Small2 lblQui;
        public iTalk.iTalk_TextBox_Small2 lblNom;
        public iTalk.iTalk_TextBox_Small2 txtDocuments;
        public iTalk.iTalk_TextBox_Small2 lblSociete;
        public iTalk.iTalk_TextBox_Small2 lblCodeImmeuble;
        public Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBcmbDestinataire;
        public System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.RadioButton optBasse;
        public System.Windows.Forms.RadioButton optNormale;
        public System.Windows.Forms.RadioButton optHaute;
        public System.Windows.Forms.Label lblTextCommentaires;
        public System.Windows.Forms.Button cmdCC;
        public iTalk.iTalk_RichTextBox txtCommentaires;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridDocument;
        public Infragistics.Win.UltraWinGrid.UltraCombo SSOleDBCmbCodProd;
        public iTalk.iTalk_RichTextBox txtCopie;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button btnEnvoyer;
        public System.Windows.Forms.Button btnAnnuler;
    }
}
