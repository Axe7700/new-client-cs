﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.Views.Devis.Forms;

namespace Axe_interDT.Views.Appel.Mail
{
    public partial class UserDocMail : UserControl
    {
        DataTable adotemp;
        ModAdo adotempModAdo = new ModAdo();
        public UserDocMail()
        {
            InitializeComponent();
        }
        public void initialiseComboMailOutlook(string Dossier)
        {
            try
            {
                int Index = 0;
                Redemption.AddressLists rals = new Redemption.AddressLists();
                Redemption.AddressList ral = null;
                string s = null;

                ral = rals.Item(Dossier);
                s = Convert.ToString(ral.AddressEntries.Count);

                DataTable SSOleDBcmbDestinataireSource = new DataTable();

                SSOleDBcmbDestinataireSource.Columns.Add("1");
                SSOleDBcmbDestinataireSource.Columns.Add("2");
                var newRow = SSOleDBcmbDestinataireSource.NewRow();
                if (SSOleDBcmbDestinataire.Rows.Count == 0)
                {
                    for (Index = 1; Index <= ral.AddressEntries.Count; Index++)
                    {
                        newRow["1"] = ral.AddressEntries[Index].Name;
                        newRow["2"] = ral.AddressEntries[Index].Name;
                        // SSOleDBcmbDestinataire.AddItem(ral.AddressEntries[Index].Name + Constants.vbTab + ral.AddressEntries[Index].Name);
                        SSOleDBcmbDestinataireSource.Rows.Add(newRow.ItemArray);
                        SSOleDBcmbDestinataire.DataSource = SSOleDBcmbDestinataireSource;
                    }
                }

                rals = null;
                ral = null;

                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Impossible d'initialiser Outlook, l'envoie par mail sera impossible", "Initialisation d'Outlook", MessageBoxButtons.OK, MessageBoxIcon.Error);


            }

        }

        public void initComboDestinataire()
        {

            short i = 0;
            int X = 0;

            //    SQL = "Select UserInitial,UserEmail " _
            //'    & "From uemails " _
            //'    & "ORDER BY UserInitial ASC"
            //
            //    Set adotemp = New ADODB.Recordset
            //    adotemp.Open SQL, adocnn
            //
            //   ' SSOleDBcmbDestinataire.RemoveAll
            //
            //
            //    If Not (adotemp.EOF) Then
            //        adotemp.MoveFirst
            //        While Not adotemp.EOF
            //            SSOleDBcmbDestinataire.AddItem adotemp!UserInitial & vbTab & adotemp!UserEmail
            //            adotemp.MoveNext
            //        Wend
            //        adotemp.Close
            //    End If
            DataTable SSOleDBcmbDestinataireSource = new DataTable();
            SSOleDBcmbDestinataireSource.Columns.Add("Initiales");

            var newRow = SSOleDBcmbDestinataireSource.NewRow();

            for (i = 1; i <= General.MesDossiers.Length; i++)
            {
                //initialiseComboMailOutlook MesDossiers(i)
                if (General.sInitAdressListOutlook345_V2 == "1")
                {
                    ModCourrier.InitAdressListOutlook345_V2();

                }
                else if (General.UCase(General.adocnn.Database) == General.UCase(General.cNameLong))
                {
                    ModCourrier.InitAdressListOutlook345();
                }
                else if (General.sRDOmail == "1")
                {
                    ModCourrier.InitAdressListOutlookRDO();
                }
                else
                {
                    ModCourrier.InitAdressListOutlook();
                }
            }

            if (SSOleDBcmbDestinataire.Rows.Count == 0 || SSOleDBcmbDestinataire.Rows.Count == 1)//tested
            {
                for (X = 0; X < ModCourrier.sTabAdresseMail.Length; X++)
                {
                    newRow["Initiales"] = ModCourrier.sTabAdresseMail[X].sNom;
                    //SSOleDBcmbDestinataire.AddItem(sTabAdresseMail(X).sNom + Constants.vbTab + sTabAdresseMail(X).sMail);                  
                    SSOleDBcmbDestinataireSource.Rows.Add(newRow.ItemArray);
                    SSOleDBcmbDestinataire.DataSource = SSOleDBcmbDestinataireSource;


                }
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        public void initComboCodeProduit()
        {

            General.SQL = "Select Designation1 "
                + "From FacArticle "
                + "WHERE ((left(FacArticle.CodeArticle,1)='R') AND ((FacArticle.CodeTypeArticle)=0))"
                + "ORDER BY Designation1 ASC";

            adotemp = new DataTable();
            adotemp = adotempModAdo.fc_OpenRecordSet(General.SQL);
            DataTable SSOleDBCmbCodProdSource = new DataTable();
            SSOleDBCmbCodProdSource.Columns.Add("Designation");

            var newRow = SSOleDBCmbCodProdSource.NewRow();

            if (adotemp.Rows.Count > 0)
            {
                foreach (DataRow adotempRow in adotemp.Rows)
                {
                    // SSOleDBCmbCodProd.AddItem(adotemp.Fields("Designation1").Value);                  
                    SSOleDBCmbCodProdSource.Rows.Add(adotempRow["Designation1"] + "");
                }
                SSOleDBCmbCodProd.DataSource = SSOleDBCmbCodProdSource;
                SSOleDBCmbCodProd.DisplayLayout.Bands[0].Columns[0].Width = 600;
            }
            adotemp.Dispose();

        }

        private void btnAnnuler_Click(object sender, EventArgs e)
        {
            if (ModMain.lLien >= 0)
            {
                if (ModMain.lLien != 0)
                    ModMain.lLien = ModMain.lLien - 1;
                // ModMain.fc_Navigue(ref this, ref ModMain.stabLien[ModMain.lLien], ref true);

            }
        }

        private void btnEnvoyer_Click(object sender, EventArgs e)
        {
            string Message = null;
            string SQL1 = null;
            bool MsgEnvoye = false;
            int i = 0;

            MsgEnvoye = false;

            try
            {
                if (string.IsNullOrEmpty(SSOleDBcmbDestinataire.Text))//tested
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un destinataire", "Destinataire manquant");
                    SSOleDBcmbDestinataire.Focus();
                    return;
                }
                
                if (General.MesEmplacement == " Pas de document attaché")//tested
                {
                    Message = lblTexteDe.Text + " " + lblDe.Text + "\r\n"
                        + lblTexteLe.Text + " " + lblLe.Text + "\r\n" + "\r\n"
                        + lblTexteOrigine.Text + " : " + lblOrigine.Text
                        + " " + "\r\n" + lblTexteEmplacement.Text + " : "
                        + lblEmplacement.Text + "\r\n" + "\r\n"
                        + lblTextSociete.Text + " : " + lblSociete.Text + "\r\n"
                        + lblTextCodeImmeuble.Text + " : " + lblCodeImmeuble.Text + "\r\n"
                        + "\r\n" + lblTextQui.Text + " : " + lblQui.Text + "\r\n"
                        + lblTextNom.Text + " : " + lblNom.Text + "\r\n"
                        + "\r\n" + lblTextCodeProduit.Text + " : " + SSOleDBCmbCodProd.Text + "\r\n"
                        + "\r\n" + "Commentaires :" + "\r\n" + txtCommentaires.Text;
                }
                else
                {


                    Message = lblTexteDe.Text + " " + lblDe.Text + "\r\n" + lblTexteLe.Text
                        + " " + lblLe.Text + "\r\n" + "\r\n" + lblTexteOrigine.Text
                        + " : " + lblOrigine.Text + " " + "\r\n" + lblTexteEmplacement.Text
                        + " : Voir pièce jointe " + "\r\n" + "\r\n" + lblTextSociete.Text + " : "
                        + lblSociete.Text + "\r\n" + lblTextCodeImmeuble.Text
                        + " : " + lblCodeImmeuble.Text + "\r\n" + "\r\n"
                        + lblTextQui.Text + " : " + lblQui.Text + "\r\n"
                        + lblTextNom.Text + " : " + lblNom.Text + "\r\n" + "\r\n"
                        + lblTextCodeProduit.Text + " : " + SSOleDBCmbCodProd.Text
                        + "\r\n" + "\r\n" + "Commentaires :" + "\r\n" + txtCommentaires.Text;

                }
               
                if (optHaute.Checked == true)
                {
                    ModCourrier.ImportanceMail = "Haute";
                }
                else if (optNormale.Checked == true)
                {
                    ModCourrier.ImportanceMail = "Normale";
                }
                else
                {
                    ModCourrier.ImportanceMail = "Basse";
                }

                if (Dossier.fc_ControleDossier(General.DossierAppel + General.NumFicheStdMsg) == false)
                {
                    Dossier.fc_CreateDossier(General.DossierAppel + General.NumFicheStdMsg);
                }

                //=== modification du 14 12 2008, les fax sont désormais envoyés en piéce jointe
                //=== et non plus sous la forme d'un lien.
                ModCourrier.FichierAttache = lblEmplacement.Text;

                for (i = 0; i <= GridDocument.Rows.Count - 1; i++)
                {
                    if (GridDocument.Rows[i].Cells[0].Text == "True")
                    {
                        if (ModCourrier.FichierAttache == " Pas de document attaché")
                        {
                            ModCourrier.FichierAttache = GridDocument.Rows[i].Cells[1].Text;
                        }
                        else
                        {
                            ModCourrier.FichierAttache = ModCourrier.FichierAttache + "\t" + GridDocument.Rows[i].Cells[1].Text;
                        }
                    }

                }

                if (General.sRDOmail == "1")
                {
                    MsgEnvoye = ModCourrier.CreateMailRDO((SSOleDBcmbDestinataire.Text), (txtCopie.Text), (SSOleDBCmbCodProd.Text), Message, ModCourrier.FichierAttache, '\t', General.DossierAppel + General.NumFicheStdMsg + "\\Mail\\");
                }
                else
                {
                    MsgEnvoye = ModCourrier.CreateMail((SSOleDBcmbDestinataire.Text), (txtCopie.Text), (SSOleDBCmbCodProd.Text), Message, ModCourrier.FichierAttache, "\t", General.DossierAppel + General.NumFicheStdMsg + "\\Mail\\");
                }

                ModCourrier.FichierAttache = "";
                //  ModuleMail.MonExplorer.Close(); 
                //MsgBox "Message envoyé", vbInformation, "Envoie de message"

                if (MsgEnvoye == true)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Message envoyé", "Envoie de message", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }

                //=== met a jour la fiche d'appel.
                SQL1 = "UPDATE GestionStandard " + "SET Messagerie = 1, GestionStandard.DossierAppel = 1 "
                    + "WHERE  NumFicheStandard =" + General.NumFicheStdMsg;
                int yy = General.Execute(SQL1);

                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocStandard, General.NumFicheStdMsg);

                View.Theme.Theme.Navigate(typeof(UserDocStandard));

                return;
            }
            catch (Exception ex)
            {
                if (MsgEnvoye == false)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Pb Messagerie, le message n'a pas été envoyé", "Envoie du message annulé", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    Erreurs.gFr_debug(ex, this.Name + ";btnEnvoyer_Click()");

                }
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCC_Click(object sender, EventArgs e)
        {
            frmCopie frmcp = new frmCopie();
            frmcp.ShowDialog();
            txtCopie.Text = ModCourrier.list;
            SSOleDBCmbCodProd.Focus();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridDocument_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {

            string sFilter = null;
            sFilter = "Documents (*.doc;*.txt;*rtf;*.xls)|*.doc;*.txt;*rtf;*.xls";
            sFilter = sFilter + "|Images (*.gif;*.jpg;*.jpeg;*.bmp;*.pcx)|*.gif;*.jpg;*.jpeg;*.bmp;*.pcx";
            var CDOpen = new OpenFileDialog();
            CDOpen.Filter = sFilter;           
            if (CDOpen.ShowDialog() == DialogResult.OK)
            {
                GridDocument.ActiveRow.Cells[0].Value = "True";
                GridDocument.ActiveRow.Cells[1].Value = CDOpen.FileName;

            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridDocument_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {

            if (!string.IsNullOrEmpty(e.Row.Cells[1].Text))
            {
                ModuleAPI.Ouvrir(e.Row.Cells[1].Text);
            }
        }

        private void SSOleDBCmbCodProd_Click(object sender, EventArgs e)
        {
            txtCommentaires.Focus();
        }

        private void SSOleDBcmbDestinataire_Click(object sender, EventArgs e)
        {
            cmdCC.Focus();
        }

        private void UserDocMail_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;

            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocClient");

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
            lblQui.Text = General.MesQui;
            lblOrigine.Text = General.MesOrigineCom;
            lblLe.Text = DateTime.Today + " ";
            lblDe.Text = General.MesUtilFichStd;
            lblEmplacement.Text = General.MesEmplacement;
            lblSociete.Text = General.MesSociete;
            lblNom.Text = General.MesNom;
            lblCodeImmeuble.Text = General.MesCodeImmeuble;
            txtCommentaires.Text = General.MsgCommentaire;
            SSOleDBCmbCodProd.Text = General.MsgCodProd;

            if (!string.IsNullOrEmpty(General.MsgDestinataire)){

                SSOleDBcmbDestinataire.Text = General.MsgDestinataire;
                General.MsgDestinataire = "";
            }
            else
            {
                SSOleDBcmbDestinataire.Text = "";

            }
           
            txtCopie.Text = "";

            General.open_conn();

            /*  if (!string.IsNullOrEmpty(General.LogoSociete) | !Information.IsDBNull(General.LogoSociete))
              {
                  imgLogoSociete.Image = System.Drawing.Image.FromFile(General.LogoSociete);
                  if (Information.IsNumeric(General.imgTop))
                  {
                      imgLogoSociete.Top = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsY(General.imgTop);
                  }
                  if (Information.IsNumeric(General.imgLeft))
                  {
                      imgLogoSociete.Left = Microsoft.VisualBasic.Compatibility.VB6.Support.TwipsToPixelsX(General.imgLeft);
                  }
              }


              if (!string.IsNullOrEmpty(General.NomSousSociete) & !Information.IsDBNull(General.NomSousSociete))
              {
                  lblNomSociete.Text = General.NomSousSociete;
                  System.Windows.Forms.Application.DoEvents();
              }
              */

            initComboDestinataire();

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

            initComboCodeProduit();

            DataTable GridDocumentSource = new DataTable();
            GridDocumentSource.Columns.Add("Attacher");
            GridDocumentSource.Columns.Add("Document");
            GridDocumentSource.Columns["Attacher"].DefaultValue = false;
            GridDocument.DataSource = GridDocumentSource;
            GridDocument.UpdateData();


            /*lblNavigation[0].Text = General.sNomLien0;
            lblNavigation[1].Text = General.sNomLien1;
            lblNavigation[2].Text = General.sNomLien2;*/



        }

        private void GridDocument_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridDocument.DisplayLayout.Bands[0].Columns["Attacher"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.CheckBox;
            GridDocument.DisplayLayout.Bands[0].Columns["Attacher"].Width = 50;
            GridDocument.DisplayLayout.Bands[0].Columns["Document"].Style = Infragistics.Win.UltraWinGrid.ColumnStyle.EditButton;
            GridDocument.DisplayLayout.Bands[0].Columns["Document"].Width = 600;
        }

        private void lblDe_TextChanged(object sender, EventArgs e)
        {

        }

        private void SSOleDBcmbDestinataire_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

        }
    }
}
