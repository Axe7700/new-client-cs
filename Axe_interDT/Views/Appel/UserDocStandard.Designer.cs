namespace Axe_interDT.Views.Appel
{
    partial class UserDocStandard
    {
        /// <summary> 
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Component Designer generated code

        /// <summary> 
        /// Required method for Designer support - do not modify 
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.UltraWinEditors.StateEditorButton stateEditorButton2 = new Infragistics.Win.UltraWinEditors.StateEditorButton();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDocStandard));
            this.label33 = new System.Windows.Forms.Label();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.CmdEditer = new System.Windows.Forms.Button();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.CmdRechercher = new System.Windows.Forms.Button();
            this.cmdDossierIntervention = new System.Windows.Forms.Button();
            this.lblSolde = new System.Windows.Forms.Label();
            this.cmdSupprimer = new System.Windows.Forms.Button();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.Frame4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lblRenovation = new System.Windows.Forms.Label();
            this.cmdDevis1 = new System.Windows.Forms.Button();
            this.cmdMail = new System.Windows.Forms.Button();
            this.cmdGMAO = new System.Windows.Forms.Button();
            this.cmdGoIntervention = new System.Windows.Forms.Button();
            this.cmdContrat = new System.Windows.Forms.Button();
            this.cmdMAJ = new System.Windows.Forms.Button();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.txtObs = new iTalk.iTalk_TextBox_Small2();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.txtObservations = new iTalk.iTalk_TextBox_Small2();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCheminOS = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel30 = new System.Windows.Forms.TableLayoutPanel();
            this.Label29 = new System.Windows.Forms.Label();
            this.txtRefOS = new iTalk.iTalk_TextBox_Small2();
            this.txtDateOS = new Infragistics.Win.UltraWinEditors.UltraDateTimeEditor();
            this.Label19 = new System.Windows.Forms.Label();
            this.optOriFax = new System.Windows.Forms.RadioButton();
            this.optOriMail = new System.Windows.Forms.RadioButton();
            this.tableLayoutPanel29 = new System.Windows.Forms.TableLayoutPanel();
            this.lblFaxouCourier = new System.Windows.Forms.Label();
            this.file1 = new System.Windows.Forms.ListBox();
            this.cmdStocker = new System.Windows.Forms.Button();
            this.optOriCourrier = new System.Windows.Forms.RadioButton();
            this.optOriRepondeur = new System.Windows.Forms.RadioButton();
            this.OptOriAstreinte = new System.Windows.Forms.RadioButton();
            this.optOriDirect = new System.Windows.Forms.RadioButton();
            this.optOriTelephone = new System.Windows.Forms.RadioButton();
            this.optOriSiteWeb = new System.Windows.Forms.RadioButton();
            this.optOriRadio = new System.Windows.Forms.RadioButton();
            this.panel4 = new System.Windows.Forms.Panel();
            this.Picture5 = new System.Windows.Forms.Panel();
            this.tableLayoutPanel31 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdFichierOS = new System.Windows.Forms.Button();
            this.OptOSori = new System.Windows.Forms.RadioButton();
            this.OptOSfichier = new System.Windows.Forms.RadioButton();
            this.Label30 = new System.Windows.Forms.Label();
            this.Option1 = new System.Windows.Forms.RadioButton();
            this.Frame3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.Label22 = new System.Windows.Forms.Label();
            this.lbllibCodeParticulier = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeParticulier = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtSource = new iTalk.iTalk_TextBox_Small2();
            this.lblSociete = new System.Windows.Forms.Label();
            this.txtSociete = new iTalk.iTalk_TextBox_Small2();
            this.lblLibIntervenantDT = new iTalk.iTalk_TextBox_Small2();
            this.optQuiSociete = new System.Windows.Forms.RadioButton();
            this.optQuiGerant = new System.Windows.Forms.RadioButton();
            this.optQuiGardien = new System.Windows.Forms.RadioButton();
            this.optQuiAutre = new System.Windows.Forms.RadioButton();
            this.optQuiFournisseur = new System.Windows.Forms.RadioButton();
            this.optQuiCopro = new System.Windows.Forms.RadioButton();
            this.OptQuiGestionnaire = new System.Windows.Forms.RadioButton();
            this.cmbIntervenantDT = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cmdIntervenant = new System.Windows.Forms.Button();
            this.cmdEctituresCopro = new System.Windows.Forms.Button();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdRechercheFour = new System.Windows.Forms.Button();
            this.lblSource = new System.Windows.Forms.Label();
            this.lblParticulier = new System.Windows.Forms.Label();
            this.txtNcompte = new iTalk.iTalk_TextBox_Small2();
            this.cmdAjoutgestion = new System.Windows.Forms.Button();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel17 = new System.Windows.Forms.TableLayoutPanel();
            this.txtObservation = new iTalk.iTalk_TextBox_Small2();
            this.label18 = new System.Windows.Forms.Label();
            this.label22s = new System.Windows.Forms.Label();
            this.txtObservationComm_IMM = new iTalk.iTalk_TextBox_Small2();
            this.groupBox3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.txtGardien = new iTalk.iTalk_TextBox_Small2();
            this.lblConseilSyndical = new iTalk.iTalk_TextBox_Small2();
            this.lblRespExploit = new iTalk.iTalk_TextBox_Small2();
            this.lblCommercial = new iTalk.iTalk_TextBox_Small2();
            this.txtcontrat = new iTalk.iTalk_TextBox_Small2();
            this.txtCode1 = new iTalk.iTalk_TextBox_Small2();
            this.Label16 = new System.Windows.Forms.Label();
            this.Label4 = new System.Windows.Forms.Label();
            this.Label17 = new System.Windows.Forms.Label();
            this.lblGoContrat = new System.Windows.Forms.Label();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.label16s = new System.Windows.Forms.Label();
            this.label17s = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdEcritures = new System.Windows.Forms.Button();
            this.cmdRechercheImmeuble = new System.Windows.Forms.Button();
            this.txtCodeImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel11 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel12 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCodeAcces2 = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeAcces1 = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel13 = new System.Windows.Forms.TableLayoutPanel();
            this.label20 = new System.Windows.Forms.Label();
            this.panel1 = new System.Windows.Forms.Panel();
            this.cmdFindIntervention = new System.Windows.Forms.Button();
            this.tableLayoutPanel14 = new System.Windows.Forms.TableLayoutPanel();
            this.Label25 = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel15 = new System.Windows.Forms.TableLayoutPanel();
            this.label21 = new System.Windows.Forms.Label();
            this.panel2 = new System.Windows.Forms.Panel();
            this.cmdHistoAppels = new System.Windows.Forms.Button();
            this.tableLayoutPanel16 = new System.Windows.Forms.TableLayoutPanel();
            this.CHKP3 = new System.Windows.Forms.CheckBox();
            this.CHKP4 = new System.Windows.Forms.CheckBox();
            this.CHKP1 = new System.Windows.Forms.CheckBox();
            this.CHKP2 = new System.Windows.Forms.CheckBox();
            this.lblGerant = new iTalk.iTalk_TextBox_Small2();
            this.groupBox4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.cmbCAI_CategorInterv = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cmdGestionMotif = new System.Windows.Forms.Button();
            this.label23 = new System.Windows.Forms.Label();
            this.lbllibCAI_CategorInterv = new iTalk.iTalk_TextBox_Small2();
            this.frmDevis = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel19 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel20 = new System.Windows.Forms.TableLayoutPanel();
            this.txtQuiDevis = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel21 = new System.Windows.Forms.TableLayoutPanel();
            this.ssoleDbQuiDevis = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label24 = new System.Windows.Forms.Label();
            this.tableLayoutPanel22 = new System.Windows.Forms.TableLayoutPanel();
            this.ChkDmdeDevis = new System.Windows.Forms.CheckBox();
            this.ChkDmdeDevisContrat = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel23 = new System.Windows.Forms.TableLayoutPanel();
            this.lblComDevis = new iTalk.iTalk_TextBox_Small2();
            this.cmdQuiDevis = new System.Windows.Forms.Button();
            this.label25s = new System.Windows.Forms.Label();
            this.cmdVisuDevis = new System.Windows.Forms.Button();
            this.txtTexte = new iTalk.iTalk_TextBox_Small2();
            this.ChkDevisAcc = new System.Windows.Forms.CheckBox();
            this.tableLayoutPanel24 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdRechNoDev = new System.Windows.Forms.Button();
            this.txtNoDevis = new iTalk.iTalk_TextBox_Small2();
            this.label26 = new System.Windows.Forms.Label();
            this.tableLayoutPanel25 = new System.Windows.Forms.TableLayoutPanel();
            this.txtDateDevis = new iTalk.iTalk_TextBox_Small2();
            this.label27 = new System.Windows.Forms.Label();
            this.lblGoIntervention = new System.Windows.Forms.Label();
            this.optDepannage = new System.Windows.Forms.RadioButton();
            this.optChauffage = new System.Windows.Forms.RadioButton();
            this.optFuite = new System.Windows.Forms.RadioButton();
            this.optECS = new System.Windows.Forms.RadioButton();
            this.optRadiateur = new System.Windows.Forms.RadioButton();
            this.cmdRechercheRaisonSocial = new System.Windows.Forms.Button();
            this.cmdPremier = new System.Windows.Forms.Button();
            this.cmdPrecedent = new System.Windows.Forms.Button();
            this.cmdDernier = new System.Windows.Forms.Button();
            this.cmdSuivant = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel27 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel28 = new System.Windows.Forms.TableLayoutPanel();
            this.panel3 = new System.Windows.Forms.Panel();
            this.txtUtilisateur = new iTalk.iTalk_TextBox_Small2();
            this.txtDateAjout = new iTalk.iTalk_TextBox_Small2();
            this.txtNumFicheStandard = new iTalk.iTalk_TextBox_Small2();
            this.pageSetupDialog1 = new System.Windows.Forms.PageSetupDialog();
            this.txtVille = new iTalk.iTalk_TextBox_Small2();
            this.txtCodePostal = new iTalk.iTalk_TextBox_Small2();
            this.txtAdresse2_IMM = new iTalk.iTalk_TextBox_Small2();
            this.txtAdresse = new iTalk.iTalk_TextBox_Small2();
            this.txtSujet = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeOrigine3_TO3 = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeContrat = new iTalk.iTalk_TextBox_Small2();
            this.txtIntervention = new iTalk.iTalk_TextBox_Small2();
            this.txtDocumentDevis = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeOriCommentDevis = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeGerant = new iTalk.iTalk_TextBox_Small2();
            this.txtRaisonSocial_IMM = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeOrigine2 = new iTalk.iTalk_TextBox_Small2();
            this.txtDocument = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeOrigine1 = new iTalk.iTalk_TextBox_Small2();
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.Frame4.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.tableLayoutPanel7.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.tableLayoutPanel30.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateOS)).BeginInit();
            this.tableLayoutPanel29.SuspendLayout();
            this.panel4.SuspendLayout();
            this.Picture5.SuspendLayout();
            this.tableLayoutPanel31.SuspendLayout();
            this.Frame3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodeParticulier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIntervenantDT)).BeginInit();
            this.tableLayoutPanel26.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.tableLayoutPanel17.SuspendLayout();
            this.groupBox3.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel11.SuspendLayout();
            this.tableLayoutPanel12.SuspendLayout();
            this.tableLayoutPanel13.SuspendLayout();
            this.panel1.SuspendLayout();
            this.tableLayoutPanel14.SuspendLayout();
            this.tableLayoutPanel15.SuspendLayout();
            this.panel2.SuspendLayout();
            this.tableLayoutPanel16.SuspendLayout();
            this.groupBox4.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCAI_CategorInterv)).BeginInit();
            this.frmDevis.SuspendLayout();
            this.tableLayoutPanel19.SuspendLayout();
            this.tableLayoutPanel20.SuspendLayout();
            this.tableLayoutPanel21.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssoleDbQuiDevis)).BeginInit();
            this.tableLayoutPanel22.SuspendLayout();
            this.tableLayoutPanel23.SuspendLayout();
            this.tableLayoutPanel24.SuspendLayout();
            this.tableLayoutPanel25.SuspendLayout();
            this.tableLayoutPanel27.SuspendLayout();
            this.tableLayoutPanel28.SuspendLayout();
            this.panel3.SuspendLayout();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Microsoft Sans Serif", 11F);
            this.label33.Location = new System.Drawing.Point(241, 13);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(65, 18);
            this.label33.TabIndex = 504;
            this.label33.Text = "Fiche N°";
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.CmdEditer);
            this.flowLayoutPanel1.Controls.Add(this.CmdSauver);
            this.flowLayoutPanel1.Controls.Add(this.cmdAnnuler);
            this.flowLayoutPanel1.Controls.Add(this.cmdAjouter);
            this.flowLayoutPanel1.Controls.Add(this.CmdRechercher);
            this.flowLayoutPanel1.Controls.Add(this.cmdDossierIntervention);
            this.flowLayoutPanel1.Controls.Add(this.lblSolde);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1295, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(555, 44);
            this.flowLayoutPanel1.TabIndex = 576;
            // 
            // CmdEditer
            // 
            this.CmdEditer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdEditer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdEditer.FlatAppearance.BorderSize = 0;
            this.CmdEditer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdEditer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdEditer.Image = global::Axe_interDT.Properties.Resources.Printer_24x24;
            this.CmdEditer.Location = new System.Drawing.Point(493, 2);
            this.CmdEditer.Margin = new System.Windows.Forms.Padding(2);
            this.CmdEditer.Name = "CmdEditer";
            this.CmdEditer.Size = new System.Drawing.Size(60, 35);
            this.CmdEditer.TabIndex = 581;
            this.CmdEditer.Tag = "";
            this.CmdEditer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.CmdEditer, "Imprimer");
            this.CmdEditer.UseVisualStyleBackColor = false;
            this.CmdEditer.Click += new System.EventHandler(this.CmdEditer_Click);
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(429, 2);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 580;
            this.CmdSauver.Tag = "";
            this.toolTip1.SetToolTip(this.CmdSauver, "Enregistrer");
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.cmdAnnuler.Location = new System.Drawing.Point(365, 2);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(60, 35);
            this.cmdAnnuler.TabIndex = 579;
            this.cmdAnnuler.Tag = "";
            this.cmdAnnuler.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAnnuler, "Annuler");
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAjouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAjouter.Image = global::Axe_interDT.Properties.Resources.Add_File_24x24;
            this.cmdAjouter.Location = new System.Drawing.Point(301, 2);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(60, 35);
            this.cmdAjouter.TabIndex = 578;
            this.cmdAjouter.Tag = "";
            this.cmdAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAjouter, "Ajouter");
            this.cmdAjouter.UseVisualStyleBackColor = false;
            this.cmdAjouter.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // CmdRechercher
            // 
            this.CmdRechercher.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.CmdRechercher.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdRechercher.FlatAppearance.BorderSize = 0;
            this.CmdRechercher.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdRechercher.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdRechercher.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.CmdRechercher.Location = new System.Drawing.Point(237, 2);
            this.CmdRechercher.Margin = new System.Windows.Forms.Padding(2);
            this.CmdRechercher.Name = "CmdRechercher";
            this.CmdRechercher.Size = new System.Drawing.Size(60, 35);
            this.CmdRechercher.TabIndex = 577;
            this.CmdRechercher.Tag = "";
            this.CmdRechercher.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.CmdRechercher, "Rechercher");
            this.CmdRechercher.UseVisualStyleBackColor = false;
            this.CmdRechercher.Click += new System.EventHandler(this.CmdRechercher_Click);
            // 
            // cmdDossierIntervention
            // 
            this.cmdDossierIntervention.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdDossierIntervention.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdDossierIntervention.FlatAppearance.BorderSize = 0;
            this.cmdDossierIntervention.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDossierIntervention.Image = global::Axe_interDT.Properties.Resources.folder_24x24;
            this.cmdDossierIntervention.Location = new System.Drawing.Point(173, 2);
            this.cmdDossierIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.cmdDossierIntervention.Name = "cmdDossierIntervention";
            this.cmdDossierIntervention.Size = new System.Drawing.Size(60, 35);
            this.cmdDossierIntervention.TabIndex = 576;
            this.cmdDossierIntervention.UseVisualStyleBackColor = false;
            this.cmdDossierIntervention.Click += new System.EventHandler(this.cmdDossierIntervention_Click);
            // 
            // lblSolde
            // 
            this.lblSolde.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.lblSolde.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblSolde.Font = new System.Drawing.Font("Ubuntu", 15F);
            this.lblSolde.ForeColor = System.Drawing.Color.White;
            this.lblSolde.Location = new System.Drawing.Point(30, 2);
            this.lblSolde.Margin = new System.Windows.Forms.Padding(2);
            this.lblSolde.Name = "lblSolde";
            this.lblSolde.Size = new System.Drawing.Size(139, 35);
            this.lblSolde.TabIndex = 582;
            this.lblSolde.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            this.lblSolde.TextChanged += new System.EventHandler(this.lblSolde_TextChanged);
            this.lblSolde.Click += new System.EventHandler(this.lblSolde_Click);
            // 
            // cmdSupprimer
            // 
            this.cmdSupprimer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSupprimer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSupprimer.FlatAppearance.BorderSize = 0;
            this.cmdSupprimer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSupprimer.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSupprimer.Image = global::Axe_interDT.Properties.Resources.Trash_24x24;
            this.cmdSupprimer.Location = new System.Drawing.Point(897, 3);
            this.cmdSupprimer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSupprimer.Name = "cmdSupprimer";
            this.cmdSupprimer.Size = new System.Drawing.Size(60, 35);
            this.cmdSupprimer.TabIndex = 364;
            this.cmdSupprimer.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdSupprimer.UseVisualStyleBackColor = false;
            this.cmdSupprimer.Visible = false;
            this.cmdSupprimer.Click += new System.EventHandler(this.cmdSupprimer_Click);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(241, 12);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(58, 16);
            this.label1.TabIndex = 504;
            this.label1.Text = "Fiche N°";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel8, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 47);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1844, 907);
            this.tableLayoutPanel1.TabIndex = 577;
            this.tableLayoutPanel1.TabStop = true;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel5, 0, 2);
            this.tableLayoutPanel2.Controls.Add(this.Frame1, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.Frame3, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 3;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 350F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 295F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(922, 907);
            this.tableLayoutPanel2.TabIndex = 0;
            this.tableLayoutPanel2.TabStop = true;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Controls.Add(this.Frame4, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel7, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 645);
            this.tableLayoutPanel5.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 131F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(922, 262);
            this.tableLayoutPanel5.TabIndex = 413;
            this.tableLayoutPanel5.TabStop = true;
            // 
            // Frame4
            // 
            this.Frame4.BackColor = System.Drawing.Color.Transparent;
            this.Frame4.Controls.Add(this.tableLayoutPanel6);
            this.Frame4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Frame4.Location = new System.Drawing.Point(3, 3);
            this.Frame4.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.Frame4.Name = "Frame4";
            this.Frame4.Padding = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.Frame4.Size = new System.Drawing.Size(916, 128);
            this.Frame4.TabIndex = 411;
            this.Frame4.TabStop = false;
            this.Frame4.Text = "Action";
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 4;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel6.Controls.Add(this.lblRenovation, 0, 3);
            this.tableLayoutPanel6.Controls.Add(this.cmdDevis1, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.cmdMail, 2, 0);
            this.tableLayoutPanel6.Controls.Add(this.cmdGMAO, 0, 2);
            this.tableLayoutPanel6.Controls.Add(this.cmdGoIntervention, 0, 0);
            this.tableLayoutPanel6.Controls.Add(this.cmdContrat, 0, 1);
            this.tableLayoutPanel6.Controls.Add(this.cmdMAJ, 3, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 4;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33555F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33223F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 33.33223F));
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(910, 110);
            this.tableLayoutPanel6.TabIndex = 0;
            this.tableLayoutPanel6.TabStop = true;
            // 
            // lblRenovation
            // 
            this.lblRenovation.AutoSize = true;
            this.lblRenovation.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
            this.lblRenovation.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.tableLayoutPanel6.SetColumnSpan(this.lblRenovation, 4);
            this.lblRenovation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRenovation.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.lblRenovation.ForeColor = System.Drawing.Color.Red;
            this.lblRenovation.Location = new System.Drawing.Point(3, 81);
            this.lblRenovation.Name = "lblRenovation";
            this.lblRenovation.Size = new System.Drawing.Size(904, 29);
            this.lblRenovation.TabIndex = 585;
            this.lblRenovation.Text = "IMMEUBLE EN RENOVATION CHAUFFERIE";
            this.lblRenovation.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // cmdDevis1
            // 
            this.cmdDevis1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdDevis1.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdDevis1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdDevis1.FlatAppearance.BorderSize = 0;
            this.cmdDevis1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDevis1.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.cmdDevis1.ForeColor = System.Drawing.Color.White;
            this.cmdDevis1.Image = global::Axe_interDT.Properties.Resources.bill_24x24;
            this.cmdDevis1.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdDevis1.Location = new System.Drawing.Point(229, 2);
            this.cmdDevis1.Margin = new System.Windows.Forms.Padding(2);
            this.cmdDevis1.Name = "cmdDevis1";
            this.tableLayoutPanel6.SetRowSpan(this.cmdDevis1, 3);
            this.cmdDevis1.Size = new System.Drawing.Size(223, 77);
            this.cmdDevis1.TabIndex = 584;
            this.cmdDevis1.Tag = "1";
            this.cmdDevis1.Text = "     Devis";
            this.cmdDevis1.UseVisualStyleBackColor = false;
            this.cmdDevis1.Click += new System.EventHandler(this.cmdDevis1_Click);
            // 
            // cmdMail
            // 
            this.cmdMail.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdMail.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdMail.FlatAppearance.BorderSize = 0;
            this.cmdMail.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.cmdMail.ForeColor = System.Drawing.Color.White;
            this.cmdMail.Image = global::Axe_interDT.Properties.Resources.Email_24x24;
            this.cmdMail.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdMail.Location = new System.Drawing.Point(456, 2);
            this.cmdMail.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMail.Name = "cmdMail";
            this.tableLayoutPanel6.SetRowSpan(this.cmdMail, 3);
            this.cmdMail.Size = new System.Drawing.Size(223, 77);
            this.cmdMail.TabIndex = 583;
            this.cmdMail.Text = "     Mail";
            this.cmdMail.UseVisualStyleBackColor = false;
            this.cmdMail.Click += new System.EventHandler(this.cmdMail_Click);
            // 
            // cmdGMAO
            // 
            this.cmdGMAO.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdGMAO.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdGMAO.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdGMAO.FlatAppearance.BorderSize = 0;
            this.cmdGMAO.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdGMAO.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.cmdGMAO.ForeColor = System.Drawing.Color.White;
            this.cmdGMAO.Location = new System.Drawing.Point(2, 56);
            this.cmdGMAO.Margin = new System.Windows.Forms.Padding(2);
            this.cmdGMAO.Name = "cmdGMAO";
            this.cmdGMAO.Size = new System.Drawing.Size(223, 23);
            this.cmdGMAO.TabIndex = 581;
            this.cmdGMAO.Text = "Fiche GMAO";
            this.cmdGMAO.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdGMAO.UseVisualStyleBackColor = false;
            this.cmdGMAO.Click += new System.EventHandler(this.cmdGMAO_Click);
            // 
            // cmdGoIntervention
            // 
            this.cmdGoIntervention.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(128)))), ((int)(((byte)(128)))));
            this.cmdGoIntervention.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdGoIntervention.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdGoIntervention.FlatAppearance.BorderSize = 0;
            this.cmdGoIntervention.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdGoIntervention.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.cmdGoIntervention.ForeColor = System.Drawing.Color.White;
            this.cmdGoIntervention.Location = new System.Drawing.Point(2, 2);
            this.cmdGoIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.cmdGoIntervention.Name = "cmdGoIntervention";
            this.cmdGoIntervention.Size = new System.Drawing.Size(223, 23);
            this.cmdGoIntervention.TabIndex = 579;
            this.cmdGoIntervention.Text = "Intervention";
            this.cmdGoIntervention.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdGoIntervention.UseVisualStyleBackColor = false;
            this.cmdGoIntervention.Click += new System.EventHandler(this.cmdGoIntervention_Click);
            // 
            // cmdContrat
            // 
            this.cmdContrat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdContrat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdContrat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdContrat.FlatAppearance.BorderSize = 0;
            this.cmdContrat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdContrat.Font = new System.Drawing.Font("Microsoft Sans Serif", 7F);
            this.cmdContrat.ForeColor = System.Drawing.Color.White;
            this.cmdContrat.Location = new System.Drawing.Point(2, 29);
            this.cmdContrat.Margin = new System.Windows.Forms.Padding(2);
            this.cmdContrat.Name = "cmdContrat";
            this.cmdContrat.Size = new System.Drawing.Size(223, 23);
            this.cmdContrat.TabIndex = 580;
            this.cmdContrat.Text = "Contrat";
            this.cmdContrat.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdContrat.UseVisualStyleBackColor = false;
            this.cmdContrat.Click += new System.EventHandler(this.cmdContrat_Click);
            // 
            // cmdMAJ
            // 
            this.cmdMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJ.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdMAJ.FlatAppearance.BorderSize = 0;
            this.cmdMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.cmdMAJ.ForeColor = System.Drawing.Color.White;
            this.cmdMAJ.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdMAJ.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdMAJ.Location = new System.Drawing.Point(683, 2);
            this.cmdMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJ.Name = "cmdMAJ";
            this.tableLayoutPanel6.SetRowSpan(this.cmdMAJ, 3);
            this.cmdMAJ.Size = new System.Drawing.Size(225, 77);
            this.cmdMAJ.TabIndex = 582;
            this.cmdMAJ.Text = "     Valider";
            this.cmdMAJ.UseVisualStyleBackColor = false;
            this.cmdMAJ.Click += new System.EventHandler(this.cmdMAJ_Click);
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 2;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel7.Controls.Add(this.txtObs, 0, 1);
            this.tableLayoutPanel7.Controls.Add(this.label7, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.label8, 1, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtObservations, 0, 1);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(3, 134);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 2;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(916, 125);
            this.tableLayoutPanel7.TabIndex = 412;
            // 
            // txtObs
            // 
            this.txtObs.AccAcceptNumbersOnly = false;
            this.txtObs.AccAllowComma = false;
            this.txtObs.AccBackgroundColor = System.Drawing.Color.White;
            this.txtObs.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtObs.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtObs.AccHidenValue = "";
            this.txtObs.AccNotAllowedChars = null;
            this.txtObs.AccReadOnly = false;
            this.txtObs.AccReadOnlyAllowDelete = false;
            this.txtObs.AccRequired = false;
            this.txtObs.BackColor = System.Drawing.Color.White;
            this.txtObs.CustomBackColor = System.Drawing.Color.White;
            this.txtObs.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObs.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtObs.ForeColor = System.Drawing.Color.Black;
            this.txtObs.Location = new System.Drawing.Point(460, 34);
            this.txtObs.Margin = new System.Windows.Forms.Padding(2);
            this.txtObs.MaxLength = 32767;
            this.txtObs.Multiline = true;
            this.txtObs.Name = "txtObs";
            this.txtObs.ReadOnly = false;
            this.txtObs.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtObs.Size = new System.Drawing.Size(454, 89);
            this.txtObs.TabIndex = 586;
            this.txtObs.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtObs.UseSystemPasswordChar = false;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label7.Location = new System.Drawing.Point(3, 6);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(452, 26);
            this.label7.TabIndex = 584;
            this.label7.Text = "Commentaire sur appel";
            this.label7.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label8.Location = new System.Drawing.Point(461, 6);
            this.label8.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(452, 26);
            this.label8.TabIndex = 585;
            this.label8.Text = "Informations techniques";
            this.label8.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtObservations
            // 
            this.txtObservations.AccAcceptNumbersOnly = false;
            this.txtObservations.AccAllowComma = false;
            this.txtObservations.AccBackgroundColor = System.Drawing.Color.White;
            this.txtObservations.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtObservations.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtObservations.AccHidenValue = "";
            this.txtObservations.AccNotAllowedChars = null;
            this.txtObservations.AccReadOnly = false;
            this.txtObservations.AccReadOnlyAllowDelete = false;
            this.txtObservations.AccRequired = false;
            this.txtObservations.BackColor = System.Drawing.Color.White;
            this.txtObservations.CustomBackColor = System.Drawing.Color.White;
            this.txtObservations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObservations.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtObservations.ForeColor = System.Drawing.Color.Black;
            this.txtObservations.Location = new System.Drawing.Point(2, 34);
            this.txtObservations.Margin = new System.Windows.Forms.Padding(2);
            this.txtObservations.MaxLength = 200;
            this.txtObservations.Multiline = true;
            this.txtObservations.Name = "txtObservations";
            this.txtObservations.ReadOnly = false;
            this.txtObservations.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtObservations.Size = new System.Drawing.Size(454, 89);
            this.txtObservations.TabIndex = 587;
            this.txtObservations.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtObservations.UseSystemPasswordChar = false;
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.Transparent;
            this.Frame1.Controls.Add(this.tableLayoutPanel3);
            this.Frame1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame1.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Frame1.Location = new System.Drawing.Point(3, 3);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(916, 344);
            this.Frame1.TabIndex = 411;
            this.Frame1.TabStop = false;
            this.Frame1.Text = "Origine";
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00062F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 24.99812F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25.00063F));
            this.tableLayoutPanel3.Controls.Add(this.txtCheminOS, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel30, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.optOriFax, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.optOriMail, 3, 3);
            this.tableLayoutPanel3.Controls.Add(this.tableLayoutPanel29, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.optOriCourrier, 0, 4);
            this.tableLayoutPanel3.Controls.Add(this.optOriRepondeur, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.OptOriAstreinte, 3, 4);
            this.tableLayoutPanel3.Controls.Add(this.optOriDirect, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.optOriTelephone, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.optOriSiteWeb, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.optOriRadio, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.panel4, 3, 5);
            this.tableLayoutPanel3.Controls.Add(this.Option1, 3, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 6;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 142F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 27F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(910, 323);
            this.tableLayoutPanel3.TabIndex = 585;
            // 
            // txtCheminOS
            // 
            this.txtCheminOS.AccAcceptNumbersOnly = false;
            this.txtCheminOS.AccAllowComma = false;
            this.txtCheminOS.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCheminOS.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCheminOS.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCheminOS.AccHidenValue = "";
            this.txtCheminOS.AccNotAllowedChars = null;
            this.txtCheminOS.AccReadOnly = false;
            this.txtCheminOS.AccReadOnlyAllowDelete = false;
            this.txtCheminOS.AccRequired = false;
            this.txtCheminOS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtCheminOS.BackColor = System.Drawing.Color.White;
            this.txtCheminOS.CustomBackColor = System.Drawing.Color.White;
            this.txtCheminOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtCheminOS.ForeColor = System.Drawing.Color.Blue;
            this.txtCheminOS.Location = new System.Drawing.Point(456, 2);
            this.txtCheminOS.Margin = new System.Windows.Forms.Padding(2);
            this.txtCheminOS.MaxLength = 32767;
            this.txtCheminOS.Multiline = false;
            this.txtCheminOS.Name = "txtCheminOS";
            this.txtCheminOS.ReadOnly = false;
            this.txtCheminOS.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCheminOS.Size = new System.Drawing.Size(223, 27);
            this.txtCheminOS.TabIndex = 20;
            this.txtCheminOS.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCheminOS.UseSystemPasswordChar = false;
            this.txtCheminOS.Visible = false;
            // 
            // tableLayoutPanel30
            // 
            this.tableLayoutPanel30.ColumnCount = 4;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel30, 3);
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.71751F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 80F));
            this.tableLayoutPanel30.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.28249F));
            this.tableLayoutPanel30.Controls.Add(this.Label29, 2, 0);
            this.tableLayoutPanel30.Controls.Add(this.txtRefOS, 1, 0);
            this.tableLayoutPanel30.Controls.Add(this.txtDateOS, 3, 0);
            this.tableLayoutPanel30.Controls.Add(this.Label19, 0, 0);
            this.tableLayoutPanel30.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel30.Location = new System.Drawing.Point(3, 265);
            this.tableLayoutPanel30.Name = "tableLayoutPanel30";
            this.tableLayoutPanel30.RowCount = 1;
            this.tableLayoutPanel30.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel30.Size = new System.Drawing.Size(675, 55);
            this.tableLayoutPanel30.TabIndex = 584;
            // 
            // Label29
            // 
            this.Label29.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label29.AutoSize = true;
            this.Label29.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.Label29.Location = new System.Drawing.Point(339, 18);
            this.Label29.Name = "Label29";
            this.Label29.Size = new System.Drawing.Size(39, 18);
            this.Label29.TabIndex = 1;
            this.Label29.Text = "Date";
            // 
            // txtRefOS
            // 
            this.txtRefOS.AccAcceptNumbersOnly = false;
            this.txtRefOS.AccAllowComma = false;
            this.txtRefOS.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRefOS.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRefOS.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtRefOS.AccHidenValue = "";
            this.txtRefOS.AccNotAllowedChars = null;
            this.txtRefOS.AccReadOnly = false;
            this.txtRefOS.AccReadOnlyAllowDelete = false;
            this.txtRefOS.AccRequired = false;
            this.txtRefOS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtRefOS.BackColor = System.Drawing.Color.White;
            this.txtRefOS.CustomBackColor = System.Drawing.Color.White;
            this.txtRefOS.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtRefOS.ForeColor = System.Drawing.Color.Blue;
            this.txtRefOS.Location = new System.Drawing.Point(82, 14);
            this.txtRefOS.Margin = new System.Windows.Forms.Padding(2);
            this.txtRefOS.MaxLength = 32767;
            this.txtRefOS.Multiline = false;
            this.txtRefOS.Name = "txtRefOS";
            this.txtRefOS.ReadOnly = false;
            this.txtRefOS.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRefOS.Size = new System.Drawing.Size(252, 27);
            this.txtRefOS.TabIndex = 20;
            this.txtRefOS.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRefOS.UseSystemPasswordChar = false;
            // 
            // txtDateOS
            // 
            this.txtDateOS.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Left | System.Windows.Forms.AnchorStyles.Right)));
            this.txtDateOS.ButtonsLeft.Add(stateEditorButton2);
            this.txtDateOS.Location = new System.Drawing.Point(419, 15);
            this.txtDateOS.Name = "txtDateOS";
            this.txtDateOS.Size = new System.Drawing.Size(253, 24);
            this.txtDateOS.TabIndex = 21;
            this.txtDateOS.Value = null;
            // 
            // Label19
            // 
            this.Label19.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label19.AutoSize = true;
            this.Label19.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.Label19.Location = new System.Drawing.Point(3, 18);
            this.Label19.Name = "Label19";
            this.Label19.Size = new System.Drawing.Size(30, 18);
            this.Label19.TabIndex = 0;
            this.Label19.Text = "OS";
            this.Label19.Click += new System.EventHandler(this.Label19_Click);
            // 
            // optOriFax
            // 
            this.optOriFax.AutoSize = true;
            this.optOriFax.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optOriFax.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.optOriFax.Location = new System.Drawing.Point(3, 93);
            this.optOriFax.Name = "optOriFax";
            this.optOriFax.Size = new System.Drawing.Size(221, 24);
            this.optOriFax.TabIndex = 541;
            this.optOriFax.Text = "Fax";
            this.optOriFax.UseVisualStyleBackColor = true;
            this.optOriFax.CheckedChanged += new System.EventHandler(this.optOriFax_CheckedChanged);
            this.optOriFax.Click += new System.EventHandler(this.lblFax_Click);
            // 
            // optOriMail
            // 
            this.optOriMail.AutoSize = true;
            this.optOriMail.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optOriMail.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.optOriMail.Location = new System.Drawing.Point(684, 93);
            this.optOriMail.Name = "optOriMail";
            this.optOriMail.Size = new System.Drawing.Size(223, 24);
            this.optOriMail.TabIndex = 546;
            this.optOriMail.Text = "Email";
            this.optOriMail.UseVisualStyleBackColor = true;
            this.optOriMail.CheckedChanged += new System.EventHandler(this.optOriMail_CheckedChanged);
            this.optOriMail.Click += new System.EventHandler(this.lblMail_Click);
            // 
            // tableLayoutPanel29
            // 
            this.tableLayoutPanel29.ColumnCount = 2;
            this.tableLayoutPanel3.SetColumnSpan(this.tableLayoutPanel29, 2);
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel29.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 60F));
            this.tableLayoutPanel29.Controls.Add(this.lblFaxouCourier, 0, 0);
            this.tableLayoutPanel29.Controls.Add(this.file1, 0, 1);
            this.tableLayoutPanel29.Controls.Add(this.cmdStocker, 1, 1);
            this.tableLayoutPanel29.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel29.Location = new System.Drawing.Point(230, 33);
            this.tableLayoutPanel29.Name = "tableLayoutPanel29";
            this.tableLayoutPanel29.RowCount = 2;
            this.tableLayoutPanel3.SetRowSpan(this.tableLayoutPanel29, 4);
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 24F));
            this.tableLayoutPanel29.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel29.Size = new System.Drawing.Size(448, 226);
            this.tableLayoutPanel29.TabIndex = 583;
            // 
            // lblFaxouCourier
            // 
            this.lblFaxouCourier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblFaxouCourier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblFaxouCourier.Location = new System.Drawing.Point(3, 0);
            this.lblFaxouCourier.Name = "lblFaxouCourier";
            this.lblFaxouCourier.Size = new System.Drawing.Size(382, 24);
            this.lblFaxouCourier.TabIndex = 547;
            // 
            // file1
            // 
            this.file1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.file1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.file1.FormattingEnabled = true;
            this.file1.ItemHeight = 18;
            this.file1.Location = new System.Drawing.Point(3, 27);
            this.file1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 4);
            this.file1.Name = "file1";
            this.file1.ScrollAlwaysVisible = true;
            this.file1.Size = new System.Drawing.Size(382, 195);
            this.file1.TabIndex = 548;
            this.file1.DoubleClick += new System.EventHandler(this.File1_DoubleClick);
            // 
            // cmdStocker
            // 
            this.cmdStocker.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdStocker.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdStocker.FlatAppearance.BorderSize = 0;
            this.cmdStocker.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdStocker.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdStocker.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdStocker.Location = new System.Drawing.Point(390, 26);
            this.cmdStocker.Margin = new System.Windows.Forms.Padding(2);
            this.cmdStocker.Name = "cmdStocker";
            this.cmdStocker.Size = new System.Drawing.Size(56, 33);
            this.cmdStocker.TabIndex = 581;
            this.cmdStocker.Tag = "";
            this.toolTip1.SetToolTip(this.cmdStocker, "Stocker");
            this.cmdStocker.UseVisualStyleBackColor = false;
            this.cmdStocker.Click += new System.EventHandler(this.cmdStocker_Click);
            // 
            // optOriCourrier
            // 
            this.optOriCourrier.AutoSize = true;
            this.optOriCourrier.Dock = System.Windows.Forms.DockStyle.Top;
            this.optOriCourrier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.optOriCourrier.Location = new System.Drawing.Point(3, 123);
            this.optOriCourrier.Name = "optOriCourrier";
            this.optOriCourrier.Size = new System.Drawing.Size(221, 22);
            this.optOriCourrier.TabIndex = 542;
            this.optOriCourrier.Text = "Courrier";
            this.optOriCourrier.UseVisualStyleBackColor = true;
            this.optOriCourrier.CheckedChanged += new System.EventHandler(this.optOriCourrier_CheckedChanged);
            this.optOriCourrier.Click += new System.EventHandler(this.lblCourier_Click);
            // 
            // optOriRepondeur
            // 
            this.optOriRepondeur.AutoSize = true;
            this.optOriRepondeur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optOriRepondeur.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.optOriRepondeur.Location = new System.Drawing.Point(3, 33);
            this.optOriRepondeur.Name = "optOriRepondeur";
            this.optOriRepondeur.Size = new System.Drawing.Size(221, 24);
            this.optOriRepondeur.TabIndex = 539;
            this.optOriRepondeur.Text = "Répondeur";
            this.optOriRepondeur.UseVisualStyleBackColor = true;
            this.optOriRepondeur.CheckedChanged += new System.EventHandler(this.optOriRepondeur_CheckedChanged);
            // 
            // OptOriAstreinte
            // 
            this.OptOriAstreinte.AutoSize = true;
            this.OptOriAstreinte.Dock = System.Windows.Forms.DockStyle.Top;
            this.OptOriAstreinte.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.OptOriAstreinte.Location = new System.Drawing.Point(684, 123);
            this.OptOriAstreinte.Name = "OptOriAstreinte";
            this.OptOriAstreinte.Size = new System.Drawing.Size(223, 22);
            this.OptOriAstreinte.TabIndex = 582;
            this.OptOriAstreinte.Text = "Astreinte";
            this.OptOriAstreinte.UseVisualStyleBackColor = true;
            this.OptOriAstreinte.CheckedChanged += new System.EventHandler(this.OptOriAstreinte_CheckedChanged);
            // 
            // optOriDirect
            // 
            this.optOriDirect.AutoSize = true;
            this.optOriDirect.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optOriDirect.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.optOriDirect.Location = new System.Drawing.Point(3, 63);
            this.optOriDirect.Name = "optOriDirect";
            this.optOriDirect.Size = new System.Drawing.Size(221, 24);
            this.optOriDirect.TabIndex = 540;
            this.optOriDirect.Text = "Interne";
            this.optOriDirect.UseVisualStyleBackColor = true;
            this.optOriDirect.CheckedChanged += new System.EventHandler(this.optOriDirect_CheckedChanged);
            // 
            // optOriTelephone
            // 
            this.optOriTelephone.AutoSize = true;
            this.optOriTelephone.Checked = true;
            this.optOriTelephone.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.optOriTelephone.Location = new System.Drawing.Point(3, 3);
            this.optOriTelephone.Name = "optOriTelephone";
            this.optOriTelephone.Size = new System.Drawing.Size(95, 22);
            this.optOriTelephone.TabIndex = 538;
            this.optOriTelephone.TabStop = true;
            this.optOriTelephone.Text = "Téléphone";
            this.optOriTelephone.UseVisualStyleBackColor = true;
            this.optOriTelephone.CheckedChanged += new System.EventHandler(this.optOriTelephone_CheckedChanged);
            // 
            // optOriSiteWeb
            // 
            this.optOriSiteWeb.AutoSize = true;
            this.optOriSiteWeb.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optOriSiteWeb.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.optOriSiteWeb.Location = new System.Drawing.Point(684, 63);
            this.optOriSiteWeb.Name = "optOriSiteWeb";
            this.optOriSiteWeb.Size = new System.Drawing.Size(223, 24);
            this.optOriSiteWeb.TabIndex = 545;
            this.optOriSiteWeb.Text = "Site Web";
            this.optOriSiteWeb.UseVisualStyleBackColor = true;
            this.optOriSiteWeb.CheckedChanged += new System.EventHandler(this.optOriSiteWeb_CheckedChanged);
            // 
            // optOriRadio
            // 
            this.optOriRadio.AutoSize = true;
            this.optOriRadio.Dock = System.Windows.Forms.DockStyle.Fill;
            this.optOriRadio.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.optOriRadio.Location = new System.Drawing.Point(684, 3);
            this.optOriRadio.Name = "optOriRadio";
            this.optOriRadio.Size = new System.Drawing.Size(223, 24);
            this.optOriRadio.TabIndex = 543;
            this.optOriRadio.Text = "Radio";
            this.optOriRadio.UseVisualStyleBackColor = true;
            this.optOriRadio.CheckedChanged += new System.EventHandler(this.optOriRadio_CheckedChanged);
            // 
            // panel4
            // 
            this.panel4.Controls.Add(this.Picture5);
            this.panel4.Controls.Add(this.Label30);
            this.panel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel4.Location = new System.Drawing.Point(681, 262);
            this.panel4.Margin = new System.Windows.Forms.Padding(0);
            this.panel4.Name = "panel4";
            this.panel4.Size = new System.Drawing.Size(229, 61);
            this.panel4.TabIndex = 585;
            // 
            // Picture5
            // 
            this.Picture5.Controls.Add(this.tableLayoutPanel31);
            this.Picture5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Picture5.Location = new System.Drawing.Point(0, 0);
            this.Picture5.Margin = new System.Windows.Forms.Padding(0);
            this.Picture5.Name = "Picture5";
            this.Picture5.Size = new System.Drawing.Size(229, 61);
            this.Picture5.TabIndex = 583;
            // 
            // tableLayoutPanel31
            // 
            this.tableLayoutPanel31.ColumnCount = 2;
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 86F));
            this.tableLayoutPanel31.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 143F));
            this.tableLayoutPanel31.Controls.Add(this.cmdFichierOS, 1, 0);
            this.tableLayoutPanel31.Controls.Add(this.OptOSori, 0, 0);
            this.tableLayoutPanel31.Controls.Add(this.OptOSfichier, 0, 1);
            this.tableLayoutPanel31.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel31.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel31.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel31.Name = "tableLayoutPanel31";
            this.tableLayoutPanel31.RowCount = 2;
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel31.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel31.Size = new System.Drawing.Size(229, 61);
            this.tableLayoutPanel31.TabIndex = 0;
            // 
            // cmdFichierOS
            // 
            this.cmdFichierOS.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFichierOS.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdFichierOS.FlatAppearance.BorderSize = 0;
            this.cmdFichierOS.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFichierOS.Image = global::Axe_interDT.Properties.Resources.Add_File_16x16;
            this.cmdFichierOS.Location = new System.Drawing.Point(88, 2);
            this.cmdFichierOS.Margin = new System.Windows.Forms.Padding(2);
            this.cmdFichierOS.Name = "cmdFichierOS";
            this.tableLayoutPanel31.SetRowSpan(this.cmdFichierOS, 2);
            this.cmdFichierOS.Size = new System.Drawing.Size(37, 26);
            this.cmdFichierOS.TabIndex = 582;
            this.cmdFichierOS.UseVisualStyleBackColor = false;
            this.cmdFichierOS.Click += new System.EventHandler(this.cmdFichierOS_Click);
            // 
            // OptOSori
            // 
            this.OptOSori.AutoSize = true;
            this.OptOSori.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptOSori.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.OptOSori.Location = new System.Drawing.Point(3, 3);
            this.OptOSori.Name = "OptOSori";
            this.OptOSori.Size = new System.Drawing.Size(80, 24);
            this.OptOSori.TabIndex = 540;
            this.OptOSori.Text = "Origine";
            this.OptOSori.UseVisualStyleBackColor = true;
            this.OptOSori.CheckedChanged += new System.EventHandler(this.OptOSori_CheckedChanged);
            // 
            // OptOSfichier
            // 
            this.OptOSfichier.AutoSize = true;
            this.OptOSfichier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptOSfichier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.OptOSfichier.Location = new System.Drawing.Point(3, 33);
            this.OptOSfichier.Name = "OptOSfichier";
            this.OptOSfichier.Size = new System.Drawing.Size(80, 25);
            this.OptOSfichier.TabIndex = 540;
            this.OptOSfichier.Text = "Fichier";
            this.OptOSfichier.UseVisualStyleBackColor = true;
            this.OptOSfichier.CheckedChanged += new System.EventHandler(this.OptOSfichier_CheckedChanged);
            this.OptOSfichier.Click += new System.EventHandler(this.OptOSfichier_Click);
            // 
            // Label30
            // 
            this.Label30.Anchor = System.Windows.Forms.AnchorStyles.Left;
            this.Label30.AutoSize = true;
            this.Label30.Location = new System.Drawing.Point(125, -4);
            this.Label30.Name = "Label30";
            this.Label30.Size = new System.Drawing.Size(70, 16);
            this.Label30.TabIndex = 0;
            this.Label30.Text = "Fichier OS";
            // 
            // Option1
            // 
            this.Option1.AutoSize = true;
            this.Option1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Option1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.Option1.Location = new System.Drawing.Point(684, 33);
            this.Option1.Name = "Option1";
            this.Option1.Size = new System.Drawing.Size(223, 24);
            this.Option1.TabIndex = 544;
            this.Option1.Text = "Transmetteur";
            this.Option1.UseVisualStyleBackColor = true;
            // 
            // Frame3
            // 
            this.Frame3.BackColor = System.Drawing.Color.Transparent;
            this.Frame3.Controls.Add(this.tableLayoutPanel4);
            this.Frame3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.Frame3.Location = new System.Drawing.Point(3, 353);
            this.Frame3.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(916, 292);
            this.Frame3.TabIndex = 412;
            this.Frame3.TabStop = false;
            this.Frame3.Text = "Interlocuteur";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33444F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33445F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33112F));
            this.tableLayoutPanel4.Controls.Add(this.Label22, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.lbllibCodeParticulier, 3, 7);
            this.tableLayoutPanel4.Controls.Add(this.txtCodeParticulier, 1, 7);
            this.tableLayoutPanel4.Controls.Add(this.txtSource, 2, 4);
            this.tableLayoutPanel4.Controls.Add(this.lblSociete, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtSociete, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.lblLibIntervenantDT, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.optQuiSociete, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.optQuiGerant, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.optQuiGardien, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.optQuiAutre, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.optQuiFournisseur, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.optQuiCopro, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.OptQuiGestionnaire, 0, 6);
            this.tableLayoutPanel4.Controls.Add(this.cmbIntervenantDT, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.cmdIntervenant, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.cmdEctituresCopro, 2, 7);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel26, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.lblParticulier, 0, 7);
            this.tableLayoutPanel4.Controls.Add(this.txtNcompte, 1, 8);
            this.tableLayoutPanel4.Controls.Add(this.cmdAjoutgestion, 1, 6);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 9;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(910, 271);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.Label22.Location = new System.Drawing.Point(296, 150);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(92, 18);
            this.Label22.TabIndex = 589;
            this.Label22.Text = "hors syndics";
            this.Label22.Click += new System.EventHandler(this.Label22_Click);
            // 
            // lbllibCodeParticulier
            // 
            this.lbllibCodeParticulier.AccAcceptNumbersOnly = false;
            this.lbllibCodeParticulier.AccAllowComma = false;
            this.lbllibCodeParticulier.AccBackgroundColor = System.Drawing.Color.White;
            this.lbllibCodeParticulier.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lbllibCodeParticulier.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lbllibCodeParticulier.AccHidenValue = "";
            this.lbllibCodeParticulier.AccNotAllowedChars = null;
            this.lbllibCodeParticulier.AccReadOnly = false;
            this.lbllibCodeParticulier.AccReadOnlyAllowDelete = false;
            this.lbllibCodeParticulier.AccRequired = false;
            this.lbllibCodeParticulier.BackColor = System.Drawing.Color.White;
            this.lbllibCodeParticulier.CustomBackColor = System.Drawing.Color.White;
            this.lbllibCodeParticulier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbllibCodeParticulier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lbllibCodeParticulier.ForeColor = System.Drawing.Color.Black;
            this.lbllibCodeParticulier.Location = new System.Drawing.Point(618, 212);
            this.lbllibCodeParticulier.Margin = new System.Windows.Forms.Padding(2);
            this.lbllibCodeParticulier.MaxLength = 32767;
            this.lbllibCodeParticulier.Multiline = false;
            this.lbllibCodeParticulier.Name = "lbllibCodeParticulier";
            this.lbllibCodeParticulier.ReadOnly = false;
            this.lbllibCodeParticulier.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lbllibCodeParticulier.Size = new System.Drawing.Size(290, 27);
            this.lbllibCodeParticulier.TabIndex = 24;
            this.lbllibCodeParticulier.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lbllibCodeParticulier.UseSystemPasswordChar = false;
            // 
            // txtCodeParticulier
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtCodeParticulier.DisplayLayout.Appearance = appearance1;
            this.txtCodeParticulier.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtCodeParticulier.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.txtCodeParticulier.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtCodeParticulier.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.txtCodeParticulier.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtCodeParticulier.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.txtCodeParticulier.DisplayLayout.MaxColScrollRegions = 1;
            this.txtCodeParticulier.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtCodeParticulier.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txtCodeParticulier.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.txtCodeParticulier.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtCodeParticulier.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.txtCodeParticulier.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtCodeParticulier.DisplayLayout.Override.CellAppearance = appearance8;
            this.txtCodeParticulier.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtCodeParticulier.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.txtCodeParticulier.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.txtCodeParticulier.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.txtCodeParticulier.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtCodeParticulier.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.txtCodeParticulier.DisplayLayout.Override.RowAppearance = appearance11;
            this.txtCodeParticulier.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtCodeParticulier.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.txtCodeParticulier.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtCodeParticulier.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtCodeParticulier.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtCodeParticulier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeParticulier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtCodeParticulier.Location = new System.Drawing.Point(296, 213);
            this.txtCodeParticulier.Name = "txtCodeParticulier";
            this.txtCodeParticulier.Size = new System.Drawing.Size(287, 27);
            this.txtCodeParticulier.TabIndex = 23;
            this.txtCodeParticulier.AfterCloseUp += new System.EventHandler(this.txtCodeParticulier_AfterCloseUp);
            this.txtCodeParticulier.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.txtCodeParticulier_BeforeDropDown);
            this.txtCodeParticulier.ValueChanged += new System.EventHandler(this.txtCodeParticulier_ValueChanged);
            this.txtCodeParticulier.Click += new System.EventHandler(this.txtCodeParticulier_ClickEvent);
            this.txtCodeParticulier.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeParticulier_Validating);
            // 
            // txtSource
            // 
            this.txtSource.AccAcceptNumbersOnly = false;
            this.txtSource.AccAllowComma = false;
            this.txtSource.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSource.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSource.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtSource.AccHidenValue = "";
            this.txtSource.AccNotAllowedChars = null;
            this.txtSource.AccReadOnly = false;
            this.txtSource.AccReadOnlyAllowDelete = false;
            this.txtSource.AccRequired = false;
            this.txtSource.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel4.SetColumnSpan(this.txtSource, 2);
            this.txtSource.CustomBackColor = System.Drawing.Color.White;
            this.txtSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtSource.ForeColor = System.Drawing.Color.Black;
            this.txtSource.Location = new System.Drawing.Point(588, 122);
            this.txtSource.Margin = new System.Windows.Forms.Padding(2);
            this.txtSource.MaxLength = 50;
            this.txtSource.Multiline = true;
            this.txtSource.Name = "txtSource";
            this.txtSource.ReadOnly = false;
            this.tableLayoutPanel4.SetRowSpan(this.txtSource, 3);
            this.txtSource.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSource.Size = new System.Drawing.Size(320, 86);
            this.txtSource.TabIndex = 22;
            this.txtSource.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSource.UseSystemPasswordChar = false;
            this.txtSource.TextChanged += new System.EventHandler(this.txtSource_TextChanged);
            // 
            // lblSociete
            // 
            this.lblSociete.AutoSize = true;
            this.lblSociete.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblSociete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblSociete.Location = new System.Drawing.Point(525, 35);
            this.lblSociete.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.lblSociete.Name = "lblSociete";
            this.lblSociete.Size = new System.Drawing.Size(58, 25);
            this.lblSociete.TabIndex = 579;
            this.lblSociete.Text = "Société";
            // 
            // txtSociete
            // 
            this.txtSociete.AccAcceptNumbersOnly = false;
            this.txtSociete.AccAllowComma = false;
            this.txtSociete.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSociete.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSociete.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtSociete.AccHidenValue = "";
            this.txtSociete.AccNotAllowedChars = null;
            this.txtSociete.AccReadOnly = false;
            this.txtSociete.AccReadOnlyAllowDelete = false;
            this.txtSociete.AccRequired = false;
            this.txtSociete.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel4.SetColumnSpan(this.txtSociete, 2);
            this.txtSociete.CustomBackColor = System.Drawing.Color.White;
            this.txtSociete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSociete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtSociete.ForeColor = System.Drawing.Color.Black;
            this.txtSociete.Location = new System.Drawing.Point(588, 32);
            this.txtSociete.Margin = new System.Windows.Forms.Padding(2);
            this.txtSociete.MaxLength = 50;
            this.txtSociete.Multiline = true;
            this.txtSociete.Name = "txtSociete";
            this.txtSociete.ReadOnly = false;
            this.tableLayoutPanel4.SetRowSpan(this.txtSociete, 3);
            this.txtSociete.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSociete.Size = new System.Drawing.Size(320, 86);
            this.txtSociete.TabIndex = 21;
            this.txtSociete.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSociete.UseSystemPasswordChar = false;
            this.txtSociete.TextChanged += new System.EventHandler(this.txtSociete_TextChanged);
            // 
            // lblLibIntervenantDT
            // 
            this.lblLibIntervenantDT.AccAcceptNumbersOnly = false;
            this.lblLibIntervenantDT.AccAllowComma = false;
            this.lblLibIntervenantDT.AccBackgroundColor = System.Drawing.Color.White;
            this.lblLibIntervenantDT.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblLibIntervenantDT.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblLibIntervenantDT.AccHidenValue = "";
            this.lblLibIntervenantDT.AccNotAllowedChars = null;
            this.lblLibIntervenantDT.AccReadOnly = false;
            this.lblLibIntervenantDT.AccReadOnlyAllowDelete = false;
            this.lblLibIntervenantDT.AccRequired = false;
            this.lblLibIntervenantDT.BackColor = System.Drawing.Color.White;
            this.lblLibIntervenantDT.CustomBackColor = System.Drawing.Color.White;
            this.lblLibIntervenantDT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibIntervenantDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblLibIntervenantDT.ForeColor = System.Drawing.Color.Blue;
            this.lblLibIntervenantDT.Location = new System.Drawing.Point(618, 2);
            this.lblLibIntervenantDT.Margin = new System.Windows.Forms.Padding(2);
            this.lblLibIntervenantDT.MaxLength = 32767;
            this.lblLibIntervenantDT.Multiline = false;
            this.lblLibIntervenantDT.Name = "lblLibIntervenantDT";
            this.lblLibIntervenantDT.ReadOnly = false;
            this.lblLibIntervenantDT.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblLibIntervenantDT.Size = new System.Drawing.Size(290, 27);
            this.lblLibIntervenantDT.TabIndex = 20;
            this.lblLibIntervenantDT.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblLibIntervenantDT.UseSystemPasswordChar = false;
            // 
            // optQuiSociete
            // 
            this.optQuiSociete.AutoSize = true;
            this.optQuiSociete.Checked = true;
            this.optQuiSociete.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.optQuiSociete.Location = new System.Drawing.Point(3, 3);
            this.optQuiSociete.Name = "optQuiSociete";
            this.optQuiSociete.Size = new System.Drawing.Size(70, 22);
            this.optQuiSociete.TabIndex = 543;
            this.optQuiSociete.TabStop = true;
            this.optQuiSociete.Text = "Interne";
            this.optQuiSociete.UseVisualStyleBackColor = true;
            this.optQuiSociete.CheckedChanged += new System.EventHandler(this.optQuiSociete_CheckedChanged);
            // 
            // optQuiGerant
            // 
            this.optQuiGerant.AutoSize = true;
            this.optQuiGerant.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.optQuiGerant.Location = new System.Drawing.Point(3, 33);
            this.optQuiGerant.Name = "optQuiGerant";
            this.optQuiGerant.Size = new System.Drawing.Size(63, 22);
            this.optQuiGerant.TabIndex = 544;
            this.optQuiGerant.Text = "Client";
            this.optQuiGerant.UseVisualStyleBackColor = true;
            this.optQuiGerant.CheckedChanged += new System.EventHandler(this.optQuiGerant_CheckedChanged);
            // 
            // optQuiGardien
            // 
            this.optQuiGardien.AutoSize = true;
            this.optQuiGardien.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.optQuiGardien.Location = new System.Drawing.Point(3, 63);
            this.optQuiGardien.Name = "optQuiGardien";
            this.optQuiGardien.Size = new System.Drawing.Size(78, 22);
            this.optQuiGardien.TabIndex = 545;
            this.optQuiGardien.Text = "Gardien";
            this.optQuiGardien.UseVisualStyleBackColor = true;
            this.optQuiGardien.CheckedChanged += new System.EventHandler(this.optQuiGardien_CheckedChanged);
            // 
            // optQuiAutre
            // 
            this.optQuiAutre.AutoSize = true;
            this.optQuiAutre.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.optQuiAutre.Location = new System.Drawing.Point(3, 93);
            this.optQuiAutre.Name = "optQuiAutre";
            this.optQuiAutre.Size = new System.Drawing.Size(60, 22);
            this.optQuiAutre.TabIndex = 546;
            this.optQuiAutre.Text = "Autre";
            this.optQuiAutre.UseVisualStyleBackColor = true;
            this.optQuiAutre.CheckedChanged += new System.EventHandler(this.optQuiAutre_CheckedChanged);
            // 
            // optQuiFournisseur
            // 
            this.optQuiFournisseur.AutoSize = true;
            this.optQuiFournisseur.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.optQuiFournisseur.Location = new System.Drawing.Point(3, 123);
            this.optQuiFournisseur.Name = "optQuiFournisseur";
            this.optQuiFournisseur.Size = new System.Drawing.Size(105, 22);
            this.optQuiFournisseur.TabIndex = 547;
            this.optQuiFournisseur.Text = "Fournisseur";
            this.optQuiFournisseur.UseVisualStyleBackColor = true;
            this.optQuiFournisseur.CheckedChanged += new System.EventHandler(this.optQuiFournisseur_CheckedChanged);
            // 
            // optQuiCopro
            // 
            this.optQuiCopro.AutoSize = true;
            this.optQuiCopro.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.optQuiCopro.Location = new System.Drawing.Point(3, 153);
            this.optQuiCopro.Name = "optQuiCopro";
            this.optQuiCopro.Size = new System.Drawing.Size(173, 22);
            this.optQuiCopro.TabIndex = 548;
            this.optQuiCopro.Text = "Adresse de facturation\r\n";
            this.optQuiCopro.UseVisualStyleBackColor = true;
            this.optQuiCopro.CheckedChanged += new System.EventHandler(this.optQuiCopro_CheckedChanged);
            // 
            // OptQuiGestionnaire
            // 
            this.OptQuiGestionnaire.AutoSize = true;
            this.OptQuiGestionnaire.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.OptQuiGestionnaire.Location = new System.Drawing.Point(3, 183);
            this.OptQuiGestionnaire.Name = "OptQuiGestionnaire";
            this.OptQuiGestionnaire.Size = new System.Drawing.Size(110, 22);
            this.OptQuiGestionnaire.TabIndex = 549;
            this.OptQuiGestionnaire.Text = "Gestionnaire";
            this.OptQuiGestionnaire.UseVisualStyleBackColor = true;
            // 
            // cmbIntervenantDT
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbIntervenantDT.DisplayLayout.Appearance = appearance13;
            this.cmbIntervenantDT.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbIntervenantDT.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenantDT.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbIntervenantDT.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.cmbIntervenantDT.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbIntervenantDT.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.cmbIntervenantDT.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbIntervenantDT.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbIntervenantDT.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbIntervenantDT.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.cmbIntervenantDT.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbIntervenantDT.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenantDT.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbIntervenantDT.DisplayLayout.Override.CellAppearance = appearance20;
            this.cmbIntervenantDT.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbIntervenantDT.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenantDT.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.cmbIntervenantDT.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.cmbIntervenantDT.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbIntervenantDT.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.cmbIntervenantDT.DisplayLayout.Override.RowAppearance = appearance23;
            this.cmbIntervenantDT.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbIntervenantDT.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.cmbIntervenantDT.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbIntervenantDT.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbIntervenantDT.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbIntervenantDT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbIntervenantDT.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.cmbIntervenantDT.Location = new System.Drawing.Point(296, 3);
            this.cmbIntervenantDT.Name = "cmbIntervenantDT";
            this.cmbIntervenantDT.Size = new System.Drawing.Size(287, 27);
            this.cmbIntervenantDT.TabIndex = 19;
            this.cmbIntervenantDT.AfterCloseUp += new System.EventHandler(this.cmbIntervenantDT_AfterCloseUp);
            this.cmbIntervenantDT.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbIntervenantDT_BeforeDropDown);
            this.cmbIntervenantDT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbIntervenantDT_KeyPress);
            this.cmbIntervenantDT.Validating += new System.ComponentModel.CancelEventHandler(this.cmbIntervenantDT_Validating);
            // 
            // cmdIntervenant
            // 
            this.cmdIntervenant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdIntervenant.FlatAppearance.BorderSize = 0;
            this.cmdIntervenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdIntervenant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdIntervenant.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdIntervenant.Location = new System.Drawing.Point(588, 2);
            this.cmdIntervenant.Margin = new System.Windows.Forms.Padding(2, 2, 0, 0);
            this.cmdIntervenant.Name = "cmdIntervenant";
            this.cmdIntervenant.Size = new System.Drawing.Size(25, 20);
            this.cmdIntervenant.TabIndex = 551;
            this.cmdIntervenant.Tag = "";
            this.toolTip1.SetToolTip(this.cmdIntervenant, "Recherche de l\'immeuble");
            this.cmdIntervenant.UseVisualStyleBackColor = false;
            this.cmdIntervenant.Click += new System.EventHandler(this.cmdIntervenant_Click);
            // 
            // cmdEctituresCopro
            // 
            this.cmdEctituresCopro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdEctituresCopro.FlatAppearance.BorderSize = 0;
            this.cmdEctituresCopro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdEctituresCopro.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cmdEctituresCopro.ForeColor = System.Drawing.Color.White;
            this.cmdEctituresCopro.Location = new System.Drawing.Point(589, 213);
            this.cmdEctituresCopro.Name = "cmdEctituresCopro";
            this.cmdEctituresCopro.Size = new System.Drawing.Size(24, 20);
            this.cmdEctituresCopro.TabIndex = 585;
            this.cmdEctituresCopro.Text = "?";
            this.cmdEctituresCopro.UseVisualStyleBackColor = false;
            this.cmdEctituresCopro.Click += new System.EventHandler(this.cmdEctituresCopro_Click);
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 2;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.Controls.Add(this.cmdRechercheFour, 0, 0);
            this.tableLayoutPanel26.Controls.Add(this.lblSource, 1, 0);
            this.tableLayoutPanel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel26.Location = new System.Drawing.Point(293, 120);
            this.tableLayoutPanel26.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 1;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(293, 30);
            this.tableLayoutPanel26.TabIndex = 587;
            // 
            // cmdRechercheFour
            // 
            this.cmdRechercheFour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheFour.FlatAppearance.BorderSize = 0;
            this.cmdRechercheFour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheFour.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheFour.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheFour.Location = new System.Drawing.Point(0, 2);
            this.cmdRechercheFour.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.cmdRechercheFour.Name = "cmdRechercheFour";
            this.cmdRechercheFour.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheFour.TabIndex = 582;
            this.cmdRechercheFour.Tag = "";
            this.toolTip1.SetToolTip(this.cmdRechercheFour, "Recherche d\'un fournisseur");
            this.cmdRechercheFour.UseVisualStyleBackColor = false;
            this.cmdRechercheFour.Click += new System.EventHandler(this.cmdRechercheFour_Click);
            // 
            // lblSource
            // 
            this.lblSource.AutoSize = true;
            this.lblSource.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblSource.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblSource.Location = new System.Drawing.Point(249, 5);
            this.lblSource.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(41, 25);
            this.lblSource.TabIndex = 581;
            this.lblSource.Text = "Nom";
            // 
            // lblParticulier
            // 
            this.lblParticulier.AutoSize = true;
            this.lblParticulier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblParticulier.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblParticulier.Location = new System.Drawing.Point(3, 216);
            this.lblParticulier.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.lblParticulier.Name = "lblParticulier";
            this.lblParticulier.Size = new System.Drawing.Size(287, 24);
            this.lblParticulier.TabIndex = 583;
            this.lblParticulier.Text = "Particulier";
            // 
            // txtNcompte
            // 
            this.txtNcompte.AccAcceptNumbersOnly = false;
            this.txtNcompte.AccAllowComma = false;
            this.txtNcompte.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNcompte.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNcompte.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNcompte.AccHidenValue = "";
            this.txtNcompte.AccNotAllowedChars = null;
            this.txtNcompte.AccReadOnly = false;
            this.txtNcompte.AccReadOnlyAllowDelete = false;
            this.txtNcompte.AccRequired = false;
            this.txtNcompte.BackColor = System.Drawing.Color.White;
            this.txtNcompte.CustomBackColor = System.Drawing.Color.White;
            this.txtNcompte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNcompte.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNcompte.ForeColor = System.Drawing.Color.Black;
            this.txtNcompte.Location = new System.Drawing.Point(295, 242);
            this.txtNcompte.Margin = new System.Windows.Forms.Padding(2);
            this.txtNcompte.MaxLength = 32767;
            this.txtNcompte.Multiline = false;
            this.txtNcompte.Name = "txtNcompte";
            this.txtNcompte.ReadOnly = false;
            this.txtNcompte.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNcompte.Size = new System.Drawing.Size(289, 27);
            this.txtNcompte.TabIndex = 590;
            this.txtNcompte.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNcompte.UseSystemPasswordChar = false;
            this.txtNcompte.Visible = false;
            // 
            // cmdAjoutgestion
            // 
            this.cmdAjoutgestion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(204)))));
            this.cmdAjoutgestion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjoutgestion.FlatAppearance.BorderSize = 0;
            this.cmdAjoutgestion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjoutgestion.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F);
            this.cmdAjoutgestion.ForeColor = System.Drawing.Color.White;
            this.cmdAjoutgestion.Location = new System.Drawing.Point(293, 180);
            this.cmdAjoutgestion.Margin = new System.Windows.Forms.Padding(0);
            this.cmdAjoutgestion.Name = "cmdAjoutgestion";
            this.cmdAjoutgestion.Size = new System.Drawing.Size(293, 30);
            this.cmdAjoutgestion.TabIndex = 588;
            this.cmdAjoutgestion.Text = "Ajouter Gestionnaire";
            this.cmdAjoutgestion.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdAjoutgestion.UseVisualStyleBackColor = false;
            this.cmdAjoutgestion.Visible = false;
            this.cmdAjoutgestion.Click += new System.EventHandler(this.cmdAjoutgestion_Click);
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 1;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.Controls.Add(this.tableLayoutPanel17, 0, 3);
            this.tableLayoutPanel8.Controls.Add(this.groupBox3, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.groupBox4, 0, 1);
            this.tableLayoutPanel8.Controls.Add(this.frmDevis, 0, 2);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(922, 0);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 5;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 355F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 222F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 0F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(922, 907);
            this.tableLayoutPanel8.TabIndex = 1;
            this.tableLayoutPanel8.TabStop = true;
            // 
            // tableLayoutPanel17
            // 
            this.tableLayoutPanel17.ColumnCount = 2;
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel17.Controls.Add(this.txtObservation, 0, 1);
            this.tableLayoutPanel17.Controls.Add(this.label18, 0, 0);
            this.tableLayoutPanel17.Controls.Add(this.label22s, 1, 0);
            this.tableLayoutPanel17.Controls.Add(this.txtObservationComm_IMM, 0, 1);
            this.tableLayoutPanel17.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.tableLayoutPanel17.Location = new System.Drawing.Point(3, 636);
            this.tableLayoutPanel17.Name = "tableLayoutPanel17";
            this.tableLayoutPanel17.RowCount = 2;
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel17.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel17.Size = new System.Drawing.Size(916, 268);
            this.tableLayoutPanel17.TabIndex = 413;
            this.tableLayoutPanel17.TabStop = true;
            // 
            // txtObservation
            // 
            this.txtObservation.AccAcceptNumbersOnly = false;
            this.txtObservation.AccAllowComma = false;
            this.txtObservation.AccBackgroundColor = System.Drawing.Color.White;
            this.txtObservation.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtObservation.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtObservation.AccHidenValue = "";
            this.txtObservation.AccNotAllowedChars = null;
            this.txtObservation.AccReadOnly = false;
            this.txtObservation.AccReadOnlyAllowDelete = false;
            this.txtObservation.AccRequired = false;
            this.txtObservation.BackColor = System.Drawing.Color.White;
            this.txtObservation.CustomBackColor = System.Drawing.Color.White;
            this.txtObservation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObservation.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtObservation.ForeColor = System.Drawing.Color.Black;
            this.txtObservation.Location = new System.Drawing.Point(460, 32);
            this.txtObservation.Margin = new System.Windows.Forms.Padding(2);
            this.txtObservation.MaxLength = 200;
            this.txtObservation.Multiline = true;
            this.txtObservation.Name = "txtObservation";
            this.txtObservation.ReadOnly = false;
            this.txtObservation.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtObservation.Size = new System.Drawing.Size(454, 234);
            this.txtObservation.TabIndex = 586;
            this.txtObservation.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtObservation.UseSystemPasswordChar = false;
            // 
            // label18
            // 
            this.label18.AutoSize = true;
            this.label18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label18.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label18.Location = new System.Drawing.Point(3, 6);
            this.label18.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label18.Name = "label18";
            this.label18.Size = new System.Drawing.Size(452, 24);
            this.label18.TabIndex = 584;
            this.label18.Text = "Informations commerciales";
            this.label18.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // label22s
            // 
            this.label22s.AutoSize = true;
            this.label22s.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label22s.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label22s.Location = new System.Drawing.Point(461, 6);
            this.label22s.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label22s.Name = "label22s";
            this.label22s.Size = new System.Drawing.Size(452, 24);
            this.label22s.TabIndex = 585;
            this.label22s.Text = "Informations générales";
            this.label22s.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // txtObservationComm_IMM
            // 
            this.txtObservationComm_IMM.AccAcceptNumbersOnly = false;
            this.txtObservationComm_IMM.AccAllowComma = false;
            this.txtObservationComm_IMM.AccBackgroundColor = System.Drawing.Color.White;
            this.txtObservationComm_IMM.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtObservationComm_IMM.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtObservationComm_IMM.AccHidenValue = "";
            this.txtObservationComm_IMM.AccNotAllowedChars = null;
            this.txtObservationComm_IMM.AccReadOnly = false;
            this.txtObservationComm_IMM.AccReadOnlyAllowDelete = false;
            this.txtObservationComm_IMM.AccRequired = false;
            this.txtObservationComm_IMM.BackColor = System.Drawing.Color.White;
            this.txtObservationComm_IMM.CustomBackColor = System.Drawing.Color.White;
            this.txtObservationComm_IMM.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObservationComm_IMM.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtObservationComm_IMM.ForeColor = System.Drawing.Color.Black;
            this.txtObservationComm_IMM.Location = new System.Drawing.Point(2, 32);
            this.txtObservationComm_IMM.Margin = new System.Windows.Forms.Padding(2);
            this.txtObservationComm_IMM.MaxLength = 200;
            this.txtObservationComm_IMM.Multiline = true;
            this.txtObservationComm_IMM.Name = "txtObservationComm_IMM";
            this.txtObservationComm_IMM.ReadOnly = false;
            this.txtObservationComm_IMM.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtObservationComm_IMM.Size = new System.Drawing.Size(454, 234);
            this.txtObservationComm_IMM.TabIndex = 587;
            this.txtObservationComm_IMM.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtObservationComm_IMM.UseSystemPasswordChar = false;
            // 
            // groupBox3
            // 
            this.groupBox3.BackColor = System.Drawing.Color.Transparent;
            this.groupBox3.Controls.Add(this.tableLayoutPanel9);
            this.groupBox3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox3.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox3.Location = new System.Drawing.Point(3, 3);
            this.groupBox3.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.groupBox3.Name = "groupBox3";
            this.groupBox3.Size = new System.Drawing.Size(916, 352);
            this.groupBox3.TabIndex = 411;
            this.groupBox3.TabStop = false;
            this.groupBox3.Text = "Immeuble";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.30303F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.69697F));
            this.tableLayoutPanel9.Controls.Add(this.txtGardien, 1, 6);
            this.tableLayoutPanel9.Controls.Add(this.lblConseilSyndical, 1, 5);
            this.tableLayoutPanel9.Controls.Add(this.lblRespExploit, 1, 4);
            this.tableLayoutPanel9.Controls.Add(this.lblCommercial, 1, 3);
            this.tableLayoutPanel9.Controls.Add(this.txtcontrat, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.txtCode1, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.Label16, 0, 10);
            this.tableLayoutPanel9.Controls.Add(this.Label4, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.Label17, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.lblGoContrat, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.label12, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.label13, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.label14, 0, 5);
            this.tableLayoutPanel9.Controls.Add(this.label15, 0, 6);
            this.tableLayoutPanel9.Controls.Add(this.label16s, 0, 7);
            this.tableLayoutPanel9.Controls.Add(this.label17s, 0, 8);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel10, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel11, 1, 7);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel14, 1, 8);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel16, 1, 9);
            this.tableLayoutPanel9.Controls.Add(this.lblGerant, 1, 10);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 11;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(910, 331);
            this.tableLayoutPanel9.TabIndex = 0;
            this.tableLayoutPanel9.TabStop = true;
            // 
            // txtGardien
            // 
            this.txtGardien.AccAcceptNumbersOnly = false;
            this.txtGardien.AccAllowComma = false;
            this.txtGardien.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGardien.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGardien.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGardien.AccHidenValue = "";
            this.txtGardien.AccNotAllowedChars = null;
            this.txtGardien.AccReadOnly = false;
            this.txtGardien.AccReadOnlyAllowDelete = false;
            this.txtGardien.AccRequired = false;
            this.txtGardien.BackColor = System.Drawing.Color.White;
            this.txtGardien.CustomBackColor = System.Drawing.Color.White;
            this.txtGardien.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGardien.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtGardien.ForeColor = System.Drawing.Color.Black;
            this.txtGardien.Location = new System.Drawing.Point(277, 182);
            this.txtGardien.Margin = new System.Windows.Forms.Padding(2);
            this.txtGardien.MaxLength = 32767;
            this.txtGardien.Multiline = false;
            this.txtGardien.Name = "txtGardien";
            this.txtGardien.ReadOnly = false;
            this.txtGardien.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGardien.Size = new System.Drawing.Size(631, 27);
            this.txtGardien.TabIndex = 6;
            this.txtGardien.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGardien.UseSystemPasswordChar = false;
            // 
            // lblConseilSyndical
            // 
            this.lblConseilSyndical.AccAcceptNumbersOnly = false;
            this.lblConseilSyndical.AccAllowComma = false;
            this.lblConseilSyndical.AccBackgroundColor = System.Drawing.Color.White;
            this.lblConseilSyndical.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblConseilSyndical.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblConseilSyndical.AccHidenValue = "";
            this.lblConseilSyndical.AccNotAllowedChars = null;
            this.lblConseilSyndical.AccReadOnly = true;
            this.lblConseilSyndical.AccReadOnlyAllowDelete = false;
            this.lblConseilSyndical.AccRequired = false;
            this.lblConseilSyndical.BackColor = System.Drawing.Color.White;
            this.lblConseilSyndical.CustomBackColor = System.Drawing.Color.White;
            this.lblConseilSyndical.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblConseilSyndical.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblConseilSyndical.ForeColor = System.Drawing.Color.Black;
            this.lblConseilSyndical.Location = new System.Drawing.Point(277, 152);
            this.lblConseilSyndical.Margin = new System.Windows.Forms.Padding(2);
            this.lblConseilSyndical.MaxLength = 32767;
            this.lblConseilSyndical.Multiline = false;
            this.lblConseilSyndical.Name = "lblConseilSyndical";
            this.lblConseilSyndical.ReadOnly = true;
            this.lblConseilSyndical.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblConseilSyndical.Size = new System.Drawing.Size(631, 27);
            this.lblConseilSyndical.TabIndex = 5;
            this.lblConseilSyndical.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblConseilSyndical.UseSystemPasswordChar = false;
            // 
            // lblRespExploit
            // 
            this.lblRespExploit.AccAcceptNumbersOnly = false;
            this.lblRespExploit.AccAllowComma = false;
            this.lblRespExploit.AccBackgroundColor = System.Drawing.Color.White;
            this.lblRespExploit.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblRespExploit.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblRespExploit.AccHidenValue = "";
            this.lblRespExploit.AccNotAllowedChars = null;
            this.lblRespExploit.AccReadOnly = true;
            this.lblRespExploit.AccReadOnlyAllowDelete = false;
            this.lblRespExploit.AccRequired = false;
            this.lblRespExploit.BackColor = System.Drawing.Color.White;
            this.lblRespExploit.CustomBackColor = System.Drawing.Color.White;
            this.lblRespExploit.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblRespExploit.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblRespExploit.ForeColor = System.Drawing.Color.Black;
            this.lblRespExploit.Location = new System.Drawing.Point(277, 122);
            this.lblRespExploit.Margin = new System.Windows.Forms.Padding(2);
            this.lblRespExploit.MaxLength = 32767;
            this.lblRespExploit.Multiline = false;
            this.lblRespExploit.Name = "lblRespExploit";
            this.lblRespExploit.ReadOnly = true;
            this.lblRespExploit.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblRespExploit.Size = new System.Drawing.Size(631, 27);
            this.lblRespExploit.TabIndex = 4;
            this.lblRespExploit.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblRespExploit.UseSystemPasswordChar = false;
            // 
            // lblCommercial
            // 
            this.lblCommercial.AccAcceptNumbersOnly = false;
            this.lblCommercial.AccAllowComma = false;
            this.lblCommercial.AccBackgroundColor = System.Drawing.Color.White;
            this.lblCommercial.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblCommercial.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblCommercial.AccHidenValue = "";
            this.lblCommercial.AccNotAllowedChars = null;
            this.lblCommercial.AccReadOnly = true;
            this.lblCommercial.AccReadOnlyAllowDelete = false;
            this.lblCommercial.AccRequired = false;
            this.lblCommercial.BackColor = System.Drawing.Color.White;
            this.lblCommercial.CustomBackColor = System.Drawing.Color.White;
            this.lblCommercial.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblCommercial.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblCommercial.ForeColor = System.Drawing.Color.Black;
            this.lblCommercial.Location = new System.Drawing.Point(277, 92);
            this.lblCommercial.Margin = new System.Windows.Forms.Padding(2);
            this.lblCommercial.MaxLength = 50;
            this.lblCommercial.Multiline = false;
            this.lblCommercial.Name = "lblCommercial";
            this.lblCommercial.ReadOnly = true;
            this.lblCommercial.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblCommercial.Size = new System.Drawing.Size(631, 27);
            this.lblCommercial.TabIndex = 3;
            this.lblCommercial.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblCommercial.UseSystemPasswordChar = false;
            // 
            // txtcontrat
            // 
            this.txtcontrat.AccAcceptNumbersOnly = false;
            this.txtcontrat.AccAllowComma = false;
            this.txtcontrat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtcontrat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtcontrat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtcontrat.AccHidenValue = "";
            this.txtcontrat.AccNotAllowedChars = null;
            this.txtcontrat.AccReadOnly = false;
            this.txtcontrat.AccReadOnlyAllowDelete = false;
            this.txtcontrat.AccRequired = false;
            this.txtcontrat.BackColor = System.Drawing.Color.White;
            this.txtcontrat.CustomBackColor = System.Drawing.Color.White;
            this.txtcontrat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtcontrat.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtcontrat.ForeColor = System.Drawing.Color.Blue;
            this.txtcontrat.Location = new System.Drawing.Point(277, 62);
            this.txtcontrat.Margin = new System.Windows.Forms.Padding(2);
            this.txtcontrat.MaxLength = 50;
            this.txtcontrat.Multiline = false;
            this.txtcontrat.Name = "txtcontrat";
            this.txtcontrat.ReadOnly = true;
            this.txtcontrat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtcontrat.Size = new System.Drawing.Size(631, 27);
            this.txtcontrat.TabIndex = 2;
            this.txtcontrat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtcontrat.UseSystemPasswordChar = false;
            // 
            // txtCode1
            // 
            this.txtCode1.AccAcceptNumbersOnly = false;
            this.txtCode1.AccAllowComma = false;
            this.txtCode1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCode1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCode1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCode1.AccHidenValue = "";
            this.txtCode1.AccNotAllowedChars = null;
            this.txtCode1.AccReadOnly = false;
            this.txtCode1.AccReadOnlyAllowDelete = false;
            this.txtCode1.AccRequired = false;
            this.txtCode1.BackColor = System.Drawing.Color.White;
            this.txtCode1.CustomBackColor = System.Drawing.Color.White;
            this.txtCode1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCode1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtCode1.ForeColor = System.Drawing.Color.Blue;
            this.txtCode1.Location = new System.Drawing.Point(277, 32);
            this.txtCode1.Margin = new System.Windows.Forms.Padding(2);
            this.txtCode1.MaxLength = 50;
            this.txtCode1.Multiline = false;
            this.txtCode1.Name = "txtCode1";
            this.txtCode1.ReadOnly = true;
            this.txtCode1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCode1.Size = new System.Drawing.Size(631, 27);
            this.txtCode1.TabIndex = 1;
            this.txtCode1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCode1.UseSystemPasswordChar = false;
            this.txtCode1.TextChanged += new System.EventHandler(this.txtCode1_TextChanged);
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.Label16.Location = new System.Drawing.Point(3, 300);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(53, 18);
            this.Label16.TabIndex = 515;
            this.Label16.Text = "Gérant";
            // 
            // Label4
            // 
            this.Label4.AutoSize = true;
            this.Label4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label4.ForeColor = System.Drawing.Color.Blue;
            this.Label4.Location = new System.Drawing.Point(3, 0);
            this.Label4.Name = "Label4";
            this.Label4.Size = new System.Drawing.Size(41, 18);
            this.Label4.TabIndex = 505;
            this.Label4.Text = "Nom";
            this.Label4.Click += new System.EventHandler(this.Label4_Click);
            // 
            // Label17
            // 
            this.Label17.AutoSize = true;
            this.Label17.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Label17.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label17.ForeColor = System.Drawing.Color.Blue;
            this.Label17.Location = new System.Drawing.Point(3, 30);
            this.Label17.Name = "Label17";
            this.Label17.Size = new System.Drawing.Size(53, 18);
            this.Label17.TabIndex = 506;
            this.Label17.Text = "Gérant";
            this.Label17.Click += new System.EventHandler(this.Label17_Click);
            // 
            // lblGoContrat
            // 
            this.lblGoContrat.AutoSize = true;
            this.lblGoContrat.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGoContrat.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoContrat.ForeColor = System.Drawing.Color.Blue;
            this.lblGoContrat.Location = new System.Drawing.Point(3, 60);
            this.lblGoContrat.Name = "lblGoContrat";
            this.lblGoContrat.Size = new System.Drawing.Size(57, 18);
            this.lblGoContrat.TabIndex = 507;
            this.lblGoContrat.Text = "Contrat";
            this.lblGoContrat.Click += new System.EventHandler(this.lblGoContrat_Click);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label12.Location = new System.Drawing.Point(3, 90);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(89, 18);
            this.label12.TabIndex = 508;
            this.label12.Text = "Commercial";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label13.Location = new System.Drawing.Point(3, 120);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(127, 18);
            this.label13.TabIndex = 509;
            this.label13.Text = "Resp. Exploitation";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label14.Location = new System.Drawing.Point(3, 150);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(117, 18);
            this.label14.TabIndex = 510;
            this.label14.Text = "Conseil Syndical";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label15.Location = new System.Drawing.Point(3, 180);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(60, 18);
            this.label15.TabIndex = 511;
            this.label15.Text = "Gardien";
            // 
            // label16s
            // 
            this.label16s.AutoSize = true;
            this.label16s.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label16s.Location = new System.Drawing.Point(3, 210);
            this.label16s.Name = "label16s";
            this.label16s.Size = new System.Drawing.Size(107, 18);
            this.label16s.TabIndex = 512;
            this.label16s.Text = "Codes d\'accès";
            // 
            // label17s
            // 
            this.label17s.AutoSize = true;
            this.label17s.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label17s.Location = new System.Drawing.Point(3, 240);
            this.label17s.Name = "label17s";
            this.label17s.Size = new System.Drawing.Size(47, 18);
            this.label17s.TabIndex = 513;
            this.label17s.Text = "Fluide";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 3;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.Controls.Add(this.cmdEcritures, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.cmdRechercheImmeuble, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.txtCodeImmeuble, 0, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(275, 0);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(635, 30);
            this.tableLayoutPanel10.TabIndex = 0;
            this.tableLayoutPanel10.TabStop = true;
            // 
            // cmdEcritures
            // 
            this.cmdEcritures.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdEcritures.FlatAppearance.BorderSize = 0;
            this.cmdEcritures.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdEcritures.Font = new System.Drawing.Font("Tahoma", 9F);
            this.cmdEcritures.ForeColor = System.Drawing.Color.White;
            this.cmdEcritures.Location = new System.Drawing.Point(598, 3);
            this.cmdEcritures.Name = "cmdEcritures";
            this.cmdEcritures.Size = new System.Drawing.Size(25, 19);
            this.cmdEcritures.TabIndex = 1;
            this.cmdEcritures.Text = "?";
            this.cmdEcritures.UseVisualStyleBackColor = false;
            this.cmdEcritures.Click += new System.EventHandler(this.cmdEcritures_Click);
            // 
            // cmdRechercheImmeuble
            // 
            this.cmdRechercheImmeuble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheImmeuble.FlatAppearance.BorderSize = 0;
            this.cmdRechercheImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheImmeuble.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheImmeuble.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheImmeuble.Location = new System.Drawing.Point(558, 3);
            this.cmdRechercheImmeuble.Name = "cmdRechercheImmeuble";
            this.cmdRechercheImmeuble.Size = new System.Drawing.Size(25, 19);
            this.cmdRechercheImmeuble.TabIndex = 0;
            this.cmdRechercheImmeuble.UseVisualStyleBackColor = false;
            this.cmdRechercheImmeuble.Click += new System.EventHandler(this.cmdRechercheImmeuble_Click);
            // 
            // txtCodeImmeuble
            // 
            this.txtCodeImmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeImmeuble.AccAllowComma = false;
            this.txtCodeImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeuble.AccHidenValue = "";
            this.txtCodeImmeuble.AccNotAllowedChars = null;
            this.txtCodeImmeuble.AccReadOnly = false;
            this.txtCodeImmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeuble.AccRequired = false;
            this.txtCodeImmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeImmeuble.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtCodeImmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeImmeuble.Location = new System.Drawing.Point(2, 2);
            this.txtCodeImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeuble.MaxLength = 50;
            this.txtCodeImmeuble.Multiline = false;
            this.txtCodeImmeuble.Name = "txtCodeImmeuble";
            this.txtCodeImmeuble.ReadOnly = false;
            this.txtCodeImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeuble.Size = new System.Drawing.Size(551, 27);
            this.txtCodeImmeuble.TabIndex = 2;
            this.txtCodeImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeuble.UseSystemPasswordChar = false;
            this.txtCodeImmeuble.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeimmeuble_KeyPress);
            this.txtCodeImmeuble.Validating += new System.ComponentModel.CancelEventHandler(this.txtCodeImmeuble_Validating);
            // 
            // tableLayoutPanel11
            // 
            this.tableLayoutPanel11.ColumnCount = 2;
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel12, 0, 0);
            this.tableLayoutPanel11.Controls.Add(this.tableLayoutPanel13, 1, 0);
            this.tableLayoutPanel11.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel11.Location = new System.Drawing.Point(275, 210);
            this.tableLayoutPanel11.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel11.Name = "tableLayoutPanel11";
            this.tableLayoutPanel11.RowCount = 1;
            this.tableLayoutPanel11.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel11.Size = new System.Drawing.Size(635, 30);
            this.tableLayoutPanel11.TabIndex = 523;
            this.tableLayoutPanel11.TabStop = true;
            // 
            // tableLayoutPanel12
            // 
            this.tableLayoutPanel12.ColumnCount = 2;
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Controls.Add(this.txtCodeAcces2, 0, 0);
            this.tableLayoutPanel12.Controls.Add(this.txtCodeAcces1, 0, 0);
            this.tableLayoutPanel12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel12.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel12.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel12.Name = "tableLayoutPanel12";
            this.tableLayoutPanel12.RowCount = 1;
            this.tableLayoutPanel12.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel12.Size = new System.Drawing.Size(317, 30);
            this.tableLayoutPanel12.TabIndex = 0;
            this.tableLayoutPanel12.TabStop = true;
            // 
            // txtCodeAcces2
            // 
            this.txtCodeAcces2.AccAcceptNumbersOnly = false;
            this.txtCodeAcces2.AccAllowComma = false;
            this.txtCodeAcces2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeAcces2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeAcces2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeAcces2.AccHidenValue = "";
            this.txtCodeAcces2.AccNotAllowedChars = null;
            this.txtCodeAcces2.AccReadOnly = false;
            this.txtCodeAcces2.AccReadOnlyAllowDelete = false;
            this.txtCodeAcces2.AccRequired = false;
            this.txtCodeAcces2.BackColor = System.Drawing.Color.White;
            this.txtCodeAcces2.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeAcces2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeAcces2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtCodeAcces2.ForeColor = System.Drawing.Color.Blue;
            this.txtCodeAcces2.Location = new System.Drawing.Point(160, 2);
            this.txtCodeAcces2.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeAcces2.MaxLength = 20;
            this.txtCodeAcces2.Multiline = false;
            this.txtCodeAcces2.Name = "txtCodeAcces2";
            this.txtCodeAcces2.ReadOnly = false;
            this.txtCodeAcces2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeAcces2.Size = new System.Drawing.Size(155, 27);
            this.txtCodeAcces2.TabIndex = 1;
            this.txtCodeAcces2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeAcces2.UseSystemPasswordChar = false;
            // 
            // txtCodeAcces1
            // 
            this.txtCodeAcces1.AccAcceptNumbersOnly = false;
            this.txtCodeAcces1.AccAllowComma = false;
            this.txtCodeAcces1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeAcces1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeAcces1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeAcces1.AccHidenValue = "";
            this.txtCodeAcces1.AccNotAllowedChars = null;
            this.txtCodeAcces1.AccReadOnly = false;
            this.txtCodeAcces1.AccReadOnlyAllowDelete = false;
            this.txtCodeAcces1.AccRequired = false;
            this.txtCodeAcces1.BackColor = System.Drawing.Color.White;
            this.txtCodeAcces1.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeAcces1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeAcces1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtCodeAcces1.ForeColor = System.Drawing.Color.Blue;
            this.txtCodeAcces1.Location = new System.Drawing.Point(2, 2);
            this.txtCodeAcces1.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeAcces1.MaxLength = 20;
            this.txtCodeAcces1.Multiline = false;
            this.txtCodeAcces1.Name = "txtCodeAcces1";
            this.txtCodeAcces1.ReadOnly = false;
            this.txtCodeAcces1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeAcces1.Size = new System.Drawing.Size(154, 27);
            this.txtCodeAcces1.TabIndex = 0;
            this.txtCodeAcces1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeAcces1.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel13
            // 
            this.tableLayoutPanel13.ColumnCount = 2;
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel13.Controls.Add(this.label20, 0, 0);
            this.tableLayoutPanel13.Controls.Add(this.panel1, 1, 0);
            this.tableLayoutPanel13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel13.Location = new System.Drawing.Point(317, 0);
            this.tableLayoutPanel13.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel13.Name = "tableLayoutPanel13";
            this.tableLayoutPanel13.RowCount = 1;
            this.tableLayoutPanel13.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel13.Size = new System.Drawing.Size(318, 30);
            this.tableLayoutPanel13.TabIndex = 1;
            // 
            // label20
            // 
            this.label20.AutoSize = true;
            this.label20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label20.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label20.Location = new System.Drawing.Point(3, 0);
            this.label20.Name = "label20";
            this.label20.Size = new System.Drawing.Size(272, 30);
            this.label20.TabIndex = 585;
            this.label20.Text = "Historique des interventions";
            // 
            // panel1
            // 
            this.panel1.Controls.Add(this.cmdFindIntervention);
            this.panel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.panel1.Location = new System.Drawing.Point(278, 0);
            this.panel1.Margin = new System.Windows.Forms.Padding(0);
            this.panel1.Name = "panel1";
            this.panel1.Size = new System.Drawing.Size(40, 30);
            this.panel1.TabIndex = 586;
            // 
            // cmdFindIntervention
            // 
            this.cmdFindIntervention.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdFindIntervention.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFindIntervention.FlatAppearance.BorderSize = 0;
            this.cmdFindIntervention.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFindIntervention.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdFindIntervention.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdFindIntervention.Location = new System.Drawing.Point(11, 3);
            this.cmdFindIntervention.Name = "cmdFindIntervention";
            this.cmdFindIntervention.Size = new System.Drawing.Size(25, 20);
            this.cmdFindIntervention.TabIndex = 584;
            this.cmdFindIntervention.Tag = "";
            this.toolTip1.SetToolTip(this.cmdFindIntervention, "Historique des interventions");
            this.cmdFindIntervention.UseVisualStyleBackColor = false;
            this.cmdFindIntervention.Click += new System.EventHandler(this.cmdFindIntervention_Click);
            // 
            // tableLayoutPanel14
            // 
            this.tableLayoutPanel14.ColumnCount = 2;
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Controls.Add(this.Label25, 0, 0);
            this.tableLayoutPanel14.Controls.Add(this.tableLayoutPanel15, 1, 0);
            this.tableLayoutPanel14.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel14.Location = new System.Drawing.Point(275, 240);
            this.tableLayoutPanel14.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel14.Name = "tableLayoutPanel14";
            this.tableLayoutPanel14.RowCount = 1;
            this.tableLayoutPanel14.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel14.Size = new System.Drawing.Size(635, 30);
            this.tableLayoutPanel14.TabIndex = 524;
            // 
            // Label25
            // 
            this.Label25.AccAcceptNumbersOnly = false;
            this.Label25.AccAllowComma = false;
            this.Label25.AccBackgroundColor = System.Drawing.Color.White;
            this.Label25.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.Label25.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.Label25.AccHidenValue = "";
            this.Label25.AccNotAllowedChars = null;
            this.Label25.AccReadOnly = true;
            this.Label25.AccReadOnlyAllowDelete = false;
            this.Label25.AccRequired = false;
            this.Label25.BackColor = System.Drawing.Color.White;
            this.Label25.CustomBackColor = System.Drawing.Color.White;
            this.Label25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label25.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.Label25.ForeColor = System.Drawing.Color.Black;
            this.Label25.Location = new System.Drawing.Point(2, 2);
            this.Label25.Margin = new System.Windows.Forms.Padding(2);
            this.Label25.MaxLength = 32767;
            this.Label25.Multiline = false;
            this.Label25.Name = "Label25";
            this.Label25.ReadOnly = false;
            this.Label25.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.Label25.Size = new System.Drawing.Size(313, 27);
            this.Label25.TabIndex = 0;
            this.Label25.TextAlignment = Infragistics.Win.HAlign.Left;
            this.Label25.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel15
            // 
            this.tableLayoutPanel15.ColumnCount = 2;
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel15.Controls.Add(this.label21, 0, 0);
            this.tableLayoutPanel15.Controls.Add(this.panel2, 1, 0);
            this.tableLayoutPanel15.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel15.Location = new System.Drawing.Point(317, 0);
            this.tableLayoutPanel15.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel15.Name = "tableLayoutPanel15";
            this.tableLayoutPanel15.RowCount = 1;
            this.tableLayoutPanel15.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel15.Size = new System.Drawing.Size(318, 30);
            this.tableLayoutPanel15.TabIndex = 509;
            // 
            // label21
            // 
            this.label21.AutoSize = true;
            this.label21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label21.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label21.Location = new System.Drawing.Point(3, 0);
            this.label21.Name = "label21";
            this.label21.Size = new System.Drawing.Size(272, 30);
            this.label21.TabIndex = 0;
            this.label21.Text = "Historique des appels";
            // 
            // panel2
            // 
            this.panel2.Controls.Add(this.cmdHistoAppels);
            this.panel2.Location = new System.Drawing.Point(278, 0);
            this.panel2.Margin = new System.Windows.Forms.Padding(0);
            this.panel2.Name = "panel2";
            this.panel2.Size = new System.Drawing.Size(40, 25);
            this.panel2.TabIndex = 587;
            // 
            // cmdHistoAppels
            // 
            this.cmdHistoAppels.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdHistoAppels.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdHistoAppels.FlatAppearance.BorderSize = 0;
            this.cmdHistoAppels.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdHistoAppels.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdHistoAppels.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdHistoAppels.Location = new System.Drawing.Point(11, 3);
            this.cmdHistoAppels.Name = "cmdHistoAppels";
            this.cmdHistoAppels.Size = new System.Drawing.Size(25, 20);
            this.cmdHistoAppels.TabIndex = 585;
            this.cmdHistoAppels.Tag = "";
            this.toolTip1.SetToolTip(this.cmdHistoAppels, "Historique des appels");
            this.cmdHistoAppels.UseVisualStyleBackColor = false;
            this.cmdHistoAppels.Click += new System.EventHandler(this.cmdHistoAppels_Click);
            // 
            // tableLayoutPanel16
            // 
            this.tableLayoutPanel16.ColumnCount = 4;
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel16.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 25F));
            this.tableLayoutPanel16.Controls.Add(this.CHKP3, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.CHKP4, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.CHKP1, 0, 0);
            this.tableLayoutPanel16.Controls.Add(this.CHKP2, 0, 0);
            this.tableLayoutPanel16.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel16.Location = new System.Drawing.Point(275, 270);
            this.tableLayoutPanel16.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel16.Name = "tableLayoutPanel16";
            this.tableLayoutPanel16.RowCount = 1;
            this.tableLayoutPanel16.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel16.Size = new System.Drawing.Size(635, 30);
            this.tableLayoutPanel16.TabIndex = 525;
            // 
            // CHKP3
            // 
            this.CHKP3.AutoSize = true;
            this.CHKP3.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.CHKP3.Location = new System.Drawing.Point(319, 3);
            this.CHKP3.Name = "CHKP3";
            this.CHKP3.Size = new System.Drawing.Size(45, 22);
            this.CHKP3.TabIndex = 1;
            this.CHKP3.Text = "P3";
            this.CHKP3.UseVisualStyleBackColor = true;
            // 
            // CHKP4
            // 
            this.CHKP4.AutoSize = true;
            this.CHKP4.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.CHKP4.Location = new System.Drawing.Point(477, 3);
            this.CHKP4.Name = "CHKP4";
            this.CHKP4.Size = new System.Drawing.Size(45, 22);
            this.CHKP4.TabIndex = 572;
            this.CHKP4.Text = "P4";
            this.CHKP4.UseVisualStyleBackColor = true;
            // 
            // CHKP1
            // 
            this.CHKP1.AutoSize = true;
            this.CHKP1.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.CHKP1.Location = new System.Drawing.Point(3, 3);
            this.CHKP1.Name = "CHKP1";
            this.CHKP1.Size = new System.Drawing.Size(45, 22);
            this.CHKP1.TabIndex = 0;
            this.CHKP1.Text = "P1";
            this.CHKP1.UseVisualStyleBackColor = true;
            // 
            // CHKP2
            // 
            this.CHKP2.AutoSize = true;
            this.CHKP2.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.CHKP2.Location = new System.Drawing.Point(161, 3);
            this.CHKP2.Name = "CHKP2";
            this.CHKP2.Size = new System.Drawing.Size(45, 22);
            this.CHKP2.TabIndex = 570;
            this.CHKP2.Text = "P2";
            this.CHKP2.UseVisualStyleBackColor = true;
            // 
            // lblGerant
            // 
            this.lblGerant.AccAcceptNumbersOnly = false;
            this.lblGerant.AccAllowComma = false;
            this.lblGerant.AccBackgroundColor = System.Drawing.Color.White;
            this.lblGerant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblGerant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblGerant.AccHidenValue = "";
            this.lblGerant.AccNotAllowedChars = null;
            this.lblGerant.AccReadOnly = true;
            this.lblGerant.AccReadOnlyAllowDelete = false;
            this.lblGerant.AccRequired = false;
            this.lblGerant.BackColor = System.Drawing.Color.White;
            this.lblGerant.CustomBackColor = System.Drawing.Color.White;
            this.lblGerant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGerant.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lblGerant.ForeColor = System.Drawing.Color.Black;
            this.lblGerant.Location = new System.Drawing.Point(277, 302);
            this.lblGerant.Margin = new System.Windows.Forms.Padding(2);
            this.lblGerant.MaxLength = 32767;
            this.lblGerant.Multiline = false;
            this.lblGerant.Name = "lblGerant";
            this.lblGerant.ReadOnly = false;
            this.lblGerant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblGerant.Size = new System.Drawing.Size(631, 27);
            this.lblGerant.TabIndex = 6;
            this.lblGerant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblGerant.UseSystemPasswordChar = false;
            // 
            // groupBox4
            // 
            this.groupBox4.BackColor = System.Drawing.Color.Transparent;
            this.groupBox4.Controls.Add(this.tableLayoutPanel18);
            this.groupBox4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.groupBox4.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.groupBox4.Location = new System.Drawing.Point(3, 358);
            this.groupBox4.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.groupBox4.Name = "groupBox4";
            this.groupBox4.Padding = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.groupBox4.Size = new System.Drawing.Size(916, 53);
            this.groupBox4.TabIndex = 412;
            this.groupBox4.TabStop = false;
            this.groupBox4.Text = "Motif de l\'appel";
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 4;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 57F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Controls.Add(this.cmbCAI_CategorInterv, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.cmdGestionMotif, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.label23, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.lbllibCAI_CategorInterv, 3, 0);
            this.tableLayoutPanel18.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel18.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel18.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 1;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(910, 35);
            this.tableLayoutPanel18.TabIndex = 0;
            // 
            // cmbCAI_CategorInterv
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbCAI_CategorInterv.DisplayLayout.Appearance = appearance25;
            this.cmbCAI_CategorInterv.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbCAI_CategorInterv.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCAI_CategorInterv.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCAI_CategorInterv.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.cmbCAI_CategorInterv.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCAI_CategorInterv.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.cmbCAI_CategorInterv.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbCAI_CategorInterv.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.CellAppearance = appearance32;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.cmbCAI_CategorInterv.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.RowAppearance = appearance35;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.cmbCAI_CategorInterv.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbCAI_CategorInterv.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbCAI_CategorInterv.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbCAI_CategorInterv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCAI_CategorInterv.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.cmbCAI_CategorInterv.Location = new System.Drawing.Point(110, 3);
            this.cmbCAI_CategorInterv.Name = "cmbCAI_CategorInterv";
            this.cmbCAI_CategorInterv.Size = new System.Drawing.Size(395, 27);
            this.cmbCAI_CategorInterv.TabIndex = 11;
            this.cmbCAI_CategorInterv.AfterCloseUp += new System.EventHandler(this.cmbCAI_CategorInterv_AfterCloseUp);
            this.cmbCAI_CategorInterv.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbCAI_CategorInterv_BeforeDropDown);
            this.cmbCAI_CategorInterv.Validating += new System.ComponentModel.CancelEventHandler(this.cmbCAI_CategorInterv_Validating);
            // 
            // cmdGestionMotif
            // 
            this.cmdGestionMotif.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdGestionMotif.FlatAppearance.BorderSize = 0;
            this.cmdGestionMotif.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdGestionMotif.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdGestionMotif.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdGestionMotif.Location = new System.Drawing.Point(60, 5);
            this.cmdGestionMotif.Margin = new System.Windows.Forms.Padding(3, 5, 3, 3);
            this.cmdGestionMotif.Name = "cmdGestionMotif";
            this.cmdGestionMotif.Size = new System.Drawing.Size(25, 20);
            this.cmdGestionMotif.TabIndex = 585;
            this.cmdGestionMotif.Tag = "";
            this.toolTip1.SetToolTip(this.cmdGestionMotif, "Gestion des motifs");
            this.cmdGestionMotif.UseVisualStyleBackColor = false;
            this.cmdGestionMotif.Click += new System.EventHandler(this.cmdGestionMotif_Click);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label23.Location = new System.Drawing.Point(3, 5);
            this.label23.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(41, 18);
            this.label23.TabIndex = 511;
            this.label23.Text = "Motif";
            // 
            // lbllibCAI_CategorInterv
            // 
            this.lbllibCAI_CategorInterv.AccAcceptNumbersOnly = false;
            this.lbllibCAI_CategorInterv.AccAllowComma = false;
            this.lbllibCAI_CategorInterv.AccBackgroundColor = System.Drawing.Color.White;
            this.lbllibCAI_CategorInterv.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lbllibCAI_CategorInterv.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lbllibCAI_CategorInterv.AccHidenValue = "";
            this.lbllibCAI_CategorInterv.AccNotAllowedChars = null;
            this.lbllibCAI_CategorInterv.AccReadOnly = false;
            this.lbllibCAI_CategorInterv.AccReadOnlyAllowDelete = false;
            this.lbllibCAI_CategorInterv.AccRequired = false;
            this.lbllibCAI_CategorInterv.BackColor = System.Drawing.Color.White;
            this.lbllibCAI_CategorInterv.CustomBackColor = System.Drawing.Color.White;
            this.lbllibCAI_CategorInterv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbllibCAI_CategorInterv.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.lbllibCAI_CategorInterv.ForeColor = System.Drawing.Color.Blue;
            this.lbllibCAI_CategorInterv.Location = new System.Drawing.Point(510, 5);
            this.lbllibCAI_CategorInterv.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.lbllibCAI_CategorInterv.MaxLength = 32767;
            this.lbllibCAI_CategorInterv.Multiline = false;
            this.lbllibCAI_CategorInterv.Name = "lbllibCAI_CategorInterv";
            this.lbllibCAI_CategorInterv.ReadOnly = false;
            this.lbllibCAI_CategorInterv.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lbllibCAI_CategorInterv.Size = new System.Drawing.Size(398, 27);
            this.lbllibCAI_CategorInterv.TabIndex = 12;
            this.lbllibCAI_CategorInterv.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lbllibCAI_CategorInterv.UseSystemPasswordChar = false;
            // 
            // frmDevis
            // 
            this.frmDevis.BackColor = System.Drawing.Color.Transparent;
            this.frmDevis.Controls.Add(this.tableLayoutPanel19);
            this.frmDevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.frmDevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.frmDevis.Location = new System.Drawing.Point(3, 414);
            this.frmDevis.Name = "frmDevis";
            this.frmDevis.Size = new System.Drawing.Size(916, 216);
            this.frmDevis.TabIndex = 414;
            this.frmDevis.TabStop = false;
            this.frmDevis.Text = "Devis";
            // 
            // tableLayoutPanel19
            // 
            this.tableLayoutPanel19.ColumnCount = 1;
            this.tableLayoutPanel19.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Controls.Add(this.tableLayoutPanel20, 0, 0);
            this.tableLayoutPanel19.Controls.Add(this.tableLayoutPanel22, 0, 1);
            this.tableLayoutPanel19.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel19.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel19.Name = "tableLayoutPanel19";
            this.tableLayoutPanel19.RowCount = 2;
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel19.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel19.Size = new System.Drawing.Size(910, 195);
            this.tableLayoutPanel19.TabIndex = 0;
            // 
            // tableLayoutPanel20
            // 
            this.tableLayoutPanel20.ColumnCount = 3;
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel20.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40F));
            this.tableLayoutPanel20.Controls.Add(this.txtQuiDevis, 2, 0);
            this.tableLayoutPanel20.Controls.Add(this.tableLayoutPanel21, 1, 0);
            this.tableLayoutPanel20.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel20.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel20.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel20.Name = "tableLayoutPanel20";
            this.tableLayoutPanel20.RowCount = 1;
            this.tableLayoutPanel20.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel20.Size = new System.Drawing.Size(910, 30);
            this.tableLayoutPanel20.TabIndex = 0;
            // 
            // txtQuiDevis
            // 
            this.txtQuiDevis.AccAcceptNumbersOnly = false;
            this.txtQuiDevis.AccAllowComma = false;
            this.txtQuiDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtQuiDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtQuiDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtQuiDevis.AccHidenValue = "";
            this.txtQuiDevis.AccNotAllowedChars = null;
            this.txtQuiDevis.AccReadOnly = false;
            this.txtQuiDevis.AccReadOnlyAllowDelete = false;
            this.txtQuiDevis.AccRequired = false;
            this.txtQuiDevis.BackColor = System.Drawing.Color.White;
            this.txtQuiDevis.CustomBackColor = System.Drawing.Color.White;
            this.txtQuiDevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtQuiDevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtQuiDevis.ForeColor = System.Drawing.Color.Black;
            this.txtQuiDevis.Location = new System.Drawing.Point(548, 2);
            this.txtQuiDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtQuiDevis.MaxLength = 32767;
            this.txtQuiDevis.Multiline = false;
            this.txtQuiDevis.Name = "txtQuiDevis";
            this.txtQuiDevis.ReadOnly = false;
            this.txtQuiDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtQuiDevis.Size = new System.Drawing.Size(360, 27);
            this.txtQuiDevis.TabIndex = 14;
            this.txtQuiDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtQuiDevis.UseSystemPasswordChar = false;
            this.txtQuiDevis.TextChanged += new System.EventHandler(this.txtQuiDevis_TextChanged);
            // 
            // tableLayoutPanel21
            // 
            this.tableLayoutPanel21.ColumnCount = 2;
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 20.23346F));
            this.tableLayoutPanel21.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 79.76654F));
            this.tableLayoutPanel21.Controls.Add(this.ssoleDbQuiDevis, 1, 0);
            this.tableLayoutPanel21.Controls.Add(this.label24, 0, 0);
            this.tableLayoutPanel21.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel21.Location = new System.Drawing.Point(182, 0);
            this.tableLayoutPanel21.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel21.Name = "tableLayoutPanel21";
            this.tableLayoutPanel21.RowCount = 1;
            this.tableLayoutPanel21.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel21.Size = new System.Drawing.Size(364, 30);
            this.tableLayoutPanel21.TabIndex = 0;
            // 
            // ssoleDbQuiDevis
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssoleDbQuiDevis.DisplayLayout.Appearance = appearance37;
            this.ssoleDbQuiDevis.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssoleDbQuiDevis.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.ssoleDbQuiDevis.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssoleDbQuiDevis.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.ssoleDbQuiDevis.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssoleDbQuiDevis.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.ssoleDbQuiDevis.DisplayLayout.MaxColScrollRegions = 1;
            this.ssoleDbQuiDevis.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssoleDbQuiDevis.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssoleDbQuiDevis.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.ssoleDbQuiDevis.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssoleDbQuiDevis.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.ssoleDbQuiDevis.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssoleDbQuiDevis.DisplayLayout.Override.CellAppearance = appearance44;
            this.ssoleDbQuiDevis.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssoleDbQuiDevis.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.ssoleDbQuiDevis.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.ssoleDbQuiDevis.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.ssoleDbQuiDevis.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssoleDbQuiDevis.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.ssoleDbQuiDevis.DisplayLayout.Override.RowAppearance = appearance47;
            this.ssoleDbQuiDevis.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssoleDbQuiDevis.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.ssoleDbQuiDevis.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssoleDbQuiDevis.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssoleDbQuiDevis.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ssoleDbQuiDevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ssoleDbQuiDevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.ssoleDbQuiDevis.Location = new System.Drawing.Point(76, 3);
            this.ssoleDbQuiDevis.Name = "ssoleDbQuiDevis";
            this.ssoleDbQuiDevis.Size = new System.Drawing.Size(285, 27);
            this.ssoleDbQuiDevis.TabIndex = 13;
            this.ssoleDbQuiDevis.ValueChanged += new System.EventHandler(this.ssoleDbQuiDevis_ValueChanged);
            // 
            // label24
            // 
            this.label24.AutoSize = true;
            this.label24.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.label24.Location = new System.Drawing.Point(3, 6);
            this.label24.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label24.Name = "label24";
            this.label24.Size = new System.Drawing.Size(39, 18);
            this.label24.TabIndex = 510;
            this.label24.Text = "Qui :";
            // 
            // tableLayoutPanel22
            // 
            this.tableLayoutPanel22.ColumnCount = 2;
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55.71096F));
            this.tableLayoutPanel22.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.28904F));
            this.tableLayoutPanel22.Controls.Add(this.ChkDmdeDevis, 0, 0);
            this.tableLayoutPanel22.Controls.Add(this.ChkDmdeDevisContrat, 0, 1);
            this.tableLayoutPanel22.Controls.Add(this.tableLayoutPanel23, 0, 2);
            this.tableLayoutPanel22.Controls.Add(this.txtTexte, 0, 3);
            this.tableLayoutPanel22.Controls.Add(this.ChkDevisAcc, 1, 1);
            this.tableLayoutPanel22.Controls.Add(this.tableLayoutPanel24, 1, 2);
            this.tableLayoutPanel22.Controls.Add(this.tableLayoutPanel25, 1, 3);
            this.tableLayoutPanel22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel22.Location = new System.Drawing.Point(0, 30);
            this.tableLayoutPanel22.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel22.Name = "tableLayoutPanel22";
            this.tableLayoutPanel22.RowCount = 5;
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 21.81818F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 17.57576F));
            this.tableLayoutPanel22.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 20F));
            this.tableLayoutPanel22.Size = new System.Drawing.Size(910, 165);
            this.tableLayoutPanel22.TabIndex = 1;
            // 
            // ChkDmdeDevis
            // 
            this.ChkDmdeDevis.AutoSize = true;
            this.ChkDmdeDevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.ChkDmdeDevis.Location = new System.Drawing.Point(3, 3);
            this.ChkDmdeDevis.Name = "ChkDmdeDevis";
            this.ChkDmdeDevis.Size = new System.Drawing.Size(200, 22);
            this.ChkDmdeDevis.TabIndex = 572;
            this.ChkDmdeDevis.Text = "Demande de devis travaux";
            this.ChkDmdeDevis.UseVisualStyleBackColor = true;
            this.ChkDmdeDevis.CheckedChanged += new System.EventHandler(this.ChkDmdeDevis_CheckedChanged);
            // 
            // ChkDmdeDevisContrat
            // 
            this.ChkDmdeDevisContrat.AutoSize = true;
            this.ChkDmdeDevisContrat.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.ChkDmdeDevisContrat.Location = new System.Drawing.Point(3, 36);
            this.ChkDmdeDevisContrat.Name = "ChkDmdeDevisContrat";
            this.ChkDmdeDevisContrat.Size = new System.Drawing.Size(199, 22);
            this.ChkDmdeDevisContrat.TabIndex = 573;
            this.ChkDmdeDevisContrat.Text = "Demande de devis contrat";
            this.ChkDmdeDevisContrat.UseVisualStyleBackColor = true;
            // 
            // tableLayoutPanel23
            // 
            this.tableLayoutPanel23.ColumnCount = 4;
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 32.60869F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 34F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 67.39131F));
            this.tableLayoutPanel23.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 259F));
            this.tableLayoutPanel23.Controls.Add(this.lblComDevis, 2, 0);
            this.tableLayoutPanel23.Controls.Add(this.cmdQuiDevis, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.label25s, 0, 0);
            this.tableLayoutPanel23.Controls.Add(this.cmdVisuDevis, 3, 0);
            this.tableLayoutPanel23.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel23.Location = new System.Drawing.Point(0, 66);
            this.tableLayoutPanel23.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel23.Name = "tableLayoutPanel23";
            this.tableLayoutPanel23.RowCount = 1;
            this.tableLayoutPanel23.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel23.Size = new System.Drawing.Size(506, 36);
            this.tableLayoutPanel23.TabIndex = 574;
            // 
            // lblComDevis
            // 
            this.lblComDevis.AccAcceptNumbersOnly = false;
            this.lblComDevis.AccAllowComma = false;
            this.lblComDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.lblComDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblComDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblComDevis.AccHidenValue = "";
            this.lblComDevis.AccNotAllowedChars = null;
            this.lblComDevis.AccReadOnly = false;
            this.lblComDevis.AccReadOnlyAllowDelete = false;
            this.lblComDevis.AccRequired = false;
            this.lblComDevis.BackColor = System.Drawing.Color.White;
            this.lblComDevis.CustomBackColor = System.Drawing.Color.White;
            this.lblComDevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblComDevis.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblComDevis.ForeColor = System.Drawing.Color.Black;
            this.lblComDevis.Location = new System.Drawing.Point(105, 2);
            this.lblComDevis.Margin = new System.Windows.Forms.Padding(2);
            this.lblComDevis.MaxLength = 32767;
            this.lblComDevis.Multiline = false;
            this.lblComDevis.Name = "lblComDevis";
            this.lblComDevis.ReadOnly = false;
            this.lblComDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblComDevis.Size = new System.Drawing.Size(139, 27);
            this.lblComDevis.TabIndex = 15;
            this.lblComDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblComDevis.UseSystemPasswordChar = false;
            // 
            // cmdQuiDevis
            // 
            this.cmdQuiDevis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdQuiDevis.FlatAppearance.BorderSize = 0;
            this.cmdQuiDevis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdQuiDevis.Font = new System.Drawing.Font("Segoe UI", 9F);
            this.cmdQuiDevis.ForeColor = System.Drawing.Color.White;
            this.cmdQuiDevis.Location = new System.Drawing.Point(72, 3);
            this.cmdQuiDevis.Name = "cmdQuiDevis";
            this.cmdQuiDevis.Size = new System.Drawing.Size(25, 20);
            this.cmdQuiDevis.TabIndex = 586;
            this.cmdQuiDevis.Text = "...";
            this.cmdQuiDevis.UseVisualStyleBackColor = false;
            this.cmdQuiDevis.Click += new System.EventHandler(this.cmdQuiDevis_Click);
            // 
            // label25s
            // 
            this.label25s.AutoSize = true;
            this.label25s.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label25s.Location = new System.Drawing.Point(3, 6);
            this.label25s.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label25s.Name = "label25s";
            this.label25s.Size = new System.Drawing.Size(51, 16);
            this.label25s.TabIndex = 511;
            this.label25s.Text = "Origine";
            // 
            // cmdVisuDevis
            // 
            this.cmdVisuDevis.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdVisuDevis.FlatAppearance.BorderSize = 0;
            this.cmdVisuDevis.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdVisuDevis.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdVisuDevis.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdVisuDevis.Location = new System.Drawing.Point(249, 3);
            this.cmdVisuDevis.Name = "cmdVisuDevis";
            this.cmdVisuDevis.Size = new System.Drawing.Size(25, 20);
            this.cmdVisuDevis.TabIndex = 587;
            this.cmdVisuDevis.UseVisualStyleBackColor = false;
            // 
            // txtTexte
            // 
            this.txtTexte.AccAcceptNumbersOnly = false;
            this.txtTexte.AccAllowComma = false;
            this.txtTexte.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTexte.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTexte.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTexte.AccHidenValue = "";
            this.txtTexte.AccNotAllowedChars = null;
            this.txtTexte.AccReadOnly = true;
            this.txtTexte.AccReadOnlyAllowDelete = false;
            this.txtTexte.AccRequired = false;
            this.txtTexte.BackColor = System.Drawing.Color.White;
            this.txtTexte.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtTexte.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTexte.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtTexte.ForeColor = System.Drawing.Color.Black;
            this.txtTexte.Location = new System.Drawing.Point(2, 104);
            this.txtTexte.Margin = new System.Windows.Forms.Padding(2);
            this.txtTexte.MaxLength = 32767;
            this.txtTexte.Multiline = true;
            this.txtTexte.Name = "txtTexte";
            this.txtTexte.ReadOnly = false;
            this.tableLayoutPanel22.SetRowSpan(this.txtTexte, 2);
            this.txtTexte.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTexte.Size = new System.Drawing.Size(502, 59);
            this.txtTexte.TabIndex = 16;
            this.txtTexte.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTexte.UseSystemPasswordChar = false;
            // 
            // ChkDevisAcc
            // 
            this.ChkDevisAcc.AutoSize = true;
            this.ChkDevisAcc.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.ChkDevisAcc.Location = new System.Drawing.Point(509, 36);
            this.ChkDevisAcc.Name = "ChkDevisAcc";
            this.ChkDevisAcc.Size = new System.Drawing.Size(120, 22);
            this.ChkDevisAcc.TabIndex = 576;
            this.ChkDevisAcc.Text = "Devis accepté";
            this.ChkDevisAcc.UseVisualStyleBackColor = true;
            this.ChkDevisAcc.CheckedChanged += new System.EventHandler(this.ChkDevisAcc_CheckedChanged);
            // 
            // tableLayoutPanel24
            // 
            this.tableLayoutPanel24.ColumnCount = 3;
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.23669F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.76331F));
            this.tableLayoutPanel24.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel24.Controls.Add(this.cmdRechNoDev, 0, 0);
            this.tableLayoutPanel24.Controls.Add(this.txtNoDevis, 0, 0);
            this.tableLayoutPanel24.Controls.Add(this.label26, 0, 0);
            this.tableLayoutPanel24.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel24.Location = new System.Drawing.Point(506, 66);
            this.tableLayoutPanel24.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel24.Name = "tableLayoutPanel24";
            this.tableLayoutPanel24.RowCount = 1;
            this.tableLayoutPanel24.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel24.Size = new System.Drawing.Size(404, 36);
            this.tableLayoutPanel24.TabIndex = 577;
            // 
            // cmdRechNoDev
            // 
            this.cmdRechNoDev.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechNoDev.Enabled = false;
            this.cmdRechNoDev.FlatAppearance.BorderSize = 0;
            this.cmdRechNoDev.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechNoDev.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechNoDev.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechNoDev.Location = new System.Drawing.Point(153, 2);
            this.cmdRechNoDev.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.cmdRechNoDev.Name = "cmdRechNoDev";
            this.cmdRechNoDev.Size = new System.Drawing.Size(25, 20);
            this.cmdRechNoDev.TabIndex = 586;
            this.cmdRechNoDev.UseVisualStyleBackColor = false;
            this.cmdRechNoDev.Click += new System.EventHandler(this.cmdRechNoDev_Click);
            // 
            // txtNoDevis
            // 
            this.txtNoDevis.AccAcceptNumbersOnly = false;
            this.txtNoDevis.AccAllowComma = false;
            this.txtNoDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoDevis.AccHidenValue = "";
            this.txtNoDevis.AccNotAllowedChars = null;
            this.txtNoDevis.AccReadOnly = false;
            this.txtNoDevis.AccReadOnlyAllowDelete = false;
            this.txtNoDevis.AccRequired = false;
            this.txtNoDevis.BackColor = System.Drawing.Color.White;
            this.txtNoDevis.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtNoDevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNoDevis.Enabled = false;
            this.txtNoDevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtNoDevis.ForeColor = System.Drawing.Color.Black;
            this.txtNoDevis.Location = new System.Drawing.Point(63, 2);
            this.txtNoDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoDevis.MaxLength = 32767;
            this.txtNoDevis.Multiline = false;
            this.txtNoDevis.Name = "txtNoDevis";
            this.txtNoDevis.ReadOnly = false;
            this.txtNoDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoDevis.Size = new System.Drawing.Size(88, 27);
            this.txtNoDevis.TabIndex = 17;
            this.txtNoDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoDevis.UseSystemPasswordChar = false;
            // 
            // label26
            // 
            this.label26.AutoSize = true;
            this.label26.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label26.Location = new System.Drawing.Point(3, 0);
            this.label26.Name = "label26";
            this.label26.Size = new System.Drawing.Size(43, 32);
            this.label26.TabIndex = 512;
            this.label26.Text = "No Devis";
            // 
            // tableLayoutPanel25
            // 
            this.tableLayoutPanel25.ColumnCount = 3;
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 40.23669F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 59.76331F));
            this.tableLayoutPanel25.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 250F));
            this.tableLayoutPanel25.Controls.Add(this.txtDateDevis, 0, 0);
            this.tableLayoutPanel25.Controls.Add(this.label27, 0, 0);
            this.tableLayoutPanel25.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel25.Location = new System.Drawing.Point(506, 102);
            this.tableLayoutPanel25.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel25.Name = "tableLayoutPanel25";
            this.tableLayoutPanel25.RowCount = 1;
            this.tableLayoutPanel25.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel25.Size = new System.Drawing.Size(404, 29);
            this.tableLayoutPanel25.TabIndex = 578;
            // 
            // txtDateDevis
            // 
            this.txtDateDevis.AccAcceptNumbersOnly = false;
            this.txtDateDevis.AccAllowComma = false;
            this.txtDateDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateDevis.AccHidenValue = "";
            this.txtDateDevis.AccNotAllowedChars = null;
            this.txtDateDevis.AccReadOnly = false;
            this.txtDateDevis.AccReadOnlyAllowDelete = false;
            this.txtDateDevis.AccRequired = false;
            this.txtDateDevis.BackColor = System.Drawing.Color.White;
            this.txtDateDevis.CustomBackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
            this.txtDateDevis.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDateDevis.Enabled = false;
            this.txtDateDevis.Font = new System.Drawing.Font("Microsoft Sans Serif", 11.25F);
            this.txtDateDevis.ForeColor = System.Drawing.Color.Black;
            this.txtDateDevis.Location = new System.Drawing.Point(63, 2);
            this.txtDateDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateDevis.MaxLength = 32767;
            this.txtDateDevis.Multiline = false;
            this.txtDateDevis.Name = "txtDateDevis";
            this.txtDateDevis.ReadOnly = false;
            this.txtDateDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateDevis.Size = new System.Drawing.Size(88, 27);
            this.txtDateDevis.TabIndex = 18;
            this.txtDateDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateDevis.UseSystemPasswordChar = false;
            // 
            // label27
            // 
            this.label27.AutoSize = true;
            this.label27.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label27.Location = new System.Drawing.Point(3, 0);
            this.label27.Name = "label27";
            this.label27.Size = new System.Drawing.Size(37, 16);
            this.label27.TabIndex = 513;
            this.label27.Text = "Date";
            // 
            // lblGoIntervention
            // 
            this.lblGoIntervention.AutoSize = true;
            this.lblGoIntervention.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblGoIntervention.ForeColor = System.Drawing.Color.Blue;
            this.lblGoIntervention.Location = new System.Drawing.Point(1000, 11);
            this.lblGoIntervention.Name = "lblGoIntervention";
            this.lblGoIntervention.Size = new System.Drawing.Size(50, 16);
            this.lblGoIntervention.TabIndex = 589;
            this.lblGoIntervention.Text = "Contrat";
            this.lblGoIntervention.Visible = false;
            // 
            // optDepannage
            // 
            this.optDepannage.AutoSize = true;
            this.optDepannage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.optDepannage.Location = new System.Drawing.Point(1000, 11);
            this.optDepannage.Name = "optDepannage";
            this.optDepannage.Size = new System.Drawing.Size(98, 20);
            this.optDepannage.TabIndex = 595;
            this.optDepannage.Text = "Dépannage";
            this.optDepannage.UseVisualStyleBackColor = true;
            this.optDepannage.Visible = false;
            this.optDepannage.CheckedChanged += new System.EventHandler(this.optDepannage_CheckedChanged);
            // 
            // optChauffage
            // 
            this.optChauffage.AutoSize = true;
            this.optChauffage.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.optChauffage.Location = new System.Drawing.Point(1000, 11);
            this.optChauffage.Name = "optChauffage";
            this.optChauffage.Size = new System.Drawing.Size(98, 20);
            this.optChauffage.TabIndex = 596;
            this.optChauffage.Text = "Dépannage";
            this.optChauffage.UseVisualStyleBackColor = true;
            this.optChauffage.Visible = false;
            this.optChauffage.CheckedChanged += new System.EventHandler(this.optChauffage_CheckedChanged);
            // 
            // optFuite
            // 
            this.optFuite.AutoSize = true;
            this.optFuite.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.optFuite.Location = new System.Drawing.Point(1000, 11);
            this.optFuite.Name = "optFuite";
            this.optFuite.Size = new System.Drawing.Size(98, 20);
            this.optFuite.TabIndex = 597;
            this.optFuite.Text = "Dépannage";
            this.optFuite.UseVisualStyleBackColor = true;
            this.optFuite.Visible = false;
            this.optFuite.CheckedChanged += new System.EventHandler(this.optFuite_CheckedChanged);
            // 
            // optECS
            // 
            this.optECS.AutoSize = true;
            this.optECS.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.optECS.Location = new System.Drawing.Point(1000, 11);
            this.optECS.Name = "optECS";
            this.optECS.Size = new System.Drawing.Size(98, 20);
            this.optECS.TabIndex = 598;
            this.optECS.Text = "Dépannage";
            this.optECS.UseVisualStyleBackColor = true;
            this.optECS.Visible = false;
            this.optECS.CheckedChanged += new System.EventHandler(this.optECS_CheckedChanged);
            // 
            // optRadiateur
            // 
            this.optRadiateur.AutoSize = true;
            this.optRadiateur.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.optRadiateur.Location = new System.Drawing.Point(1000, 11);
            this.optRadiateur.Name = "optRadiateur";
            this.optRadiateur.Size = new System.Drawing.Size(98, 20);
            this.optRadiateur.TabIndex = 599;
            this.optRadiateur.Text = "Dépannage";
            this.optRadiateur.UseVisualStyleBackColor = true;
            this.optRadiateur.Visible = false;
            this.optRadiateur.CheckedChanged += new System.EventHandler(this.optRadiateur_CheckedChanged);
            // 
            // cmdRechercheRaisonSocial
            // 
            this.cmdRechercheRaisonSocial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(162)))), ((int)(((byte)(211)))));
            this.cmdRechercheRaisonSocial.FlatAppearance.BorderSize = 0;
            this.cmdRechercheRaisonSocial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheRaisonSocial.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheRaisonSocial.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheRaisonSocial.Location = new System.Drawing.Point(1000, 11);
            this.cmdRechercheRaisonSocial.Name = "cmdRechercheRaisonSocial";
            this.cmdRechercheRaisonSocial.Size = new System.Drawing.Size(25, 21);
            this.cmdRechercheRaisonSocial.TabIndex = 588;
            this.cmdRechercheRaisonSocial.UseVisualStyleBackColor = false;
            this.cmdRechercheRaisonSocial.Visible = false;
            this.cmdRechercheRaisonSocial.Click += new System.EventHandler(this.cmdRechercheRaisonSocial_Click);
            // 
            // cmdPremier
            // 
            this.cmdPremier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdPremier.FlatAppearance.BorderSize = 0;
            this.cmdPremier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPremier.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdPremier.Image = global::Axe_interDT.Properties.Resources.first_16;
            this.cmdPremier.Location = new System.Drawing.Point(395, 11);
            this.cmdPremier.Name = "cmdPremier";
            this.cmdPremier.Size = new System.Drawing.Size(25, 23);
            this.cmdPremier.TabIndex = 506;
            this.cmdPremier.Text = " ";
            this.cmdPremier.UseVisualStyleBackColor = false;
            this.cmdPremier.Click += new System.EventHandler(this.cmdPremier_Click);
            // 
            // cmdPrecedent
            // 
            this.cmdPrecedent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdPrecedent.FlatAppearance.BorderSize = 0;
            this.cmdPrecedent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdPrecedent.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdPrecedent.Image = global::Axe_interDT.Properties.Resources.previous_16;
            this.cmdPrecedent.Location = new System.Drawing.Point(422, 11);
            this.cmdPrecedent.Name = "cmdPrecedent";
            this.cmdPrecedent.Size = new System.Drawing.Size(25, 23);
            this.cmdPrecedent.TabIndex = 509;
            this.cmdPrecedent.Text = " ";
            this.cmdPrecedent.UseVisualStyleBackColor = false;
            this.cmdPrecedent.Click += new System.EventHandler(this.cmdPrecedent_Click);
            // 
            // cmdDernier
            // 
            this.cmdDernier.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdDernier.FlatAppearance.BorderSize = 0;
            this.cmdDernier.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdDernier.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdDernier.Image = global::Axe_interDT.Properties.Resources.last_16;
            this.cmdDernier.Location = new System.Drawing.Point(476, 11);
            this.cmdDernier.Name = "cmdDernier";
            this.cmdDernier.Size = new System.Drawing.Size(25, 23);
            this.cmdDernier.TabIndex = 507;
            this.cmdDernier.Text = " ";
            this.cmdDernier.UseVisualStyleBackColor = false;
            this.cmdDernier.Click += new System.EventHandler(this.cmdDernier_Click);
            // 
            // cmdSuivant
            // 
            this.cmdSuivant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdSuivant.FlatAppearance.BorderSize = 0;
            this.cmdSuivant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSuivant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdSuivant.Image = ((System.Drawing.Image)(resources.GetObject("cmdSuivant.Image")));
            this.cmdSuivant.Location = new System.Drawing.Point(449, 11);
            this.cmdSuivant.Name = "cmdSuivant";
            this.cmdSuivant.Size = new System.Drawing.Size(25, 23);
            this.cmdSuivant.TabIndex = 508;
            this.cmdSuivant.Text = " ";
            this.cmdSuivant.UseVisualStyleBackColor = false;
            this.cmdSuivant.Click += new System.EventHandler(this.cmdSuivant_Click);
            // 
            // tableLayoutPanel27
            // 
            this.tableLayoutPanel27.ColumnCount = 1;
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel27.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel27.Controls.Add(this.tableLayoutPanel28, 0, 0);
            this.tableLayoutPanel27.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel27.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel27.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel27.Name = "tableLayoutPanel27";
            this.tableLayoutPanel27.RowCount = 2;
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel27.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel27.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel27.TabIndex = 600;
            // 
            // tableLayoutPanel28
            // 
            this.tableLayoutPanel28.ColumnCount = 2;
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel28.Controls.Add(this.panel3, 0, 0);
            this.tableLayoutPanel28.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel28.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel28.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel28.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel28.Name = "tableLayoutPanel28";
            this.tableLayoutPanel28.RowCount = 1;
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel28.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel28.Size = new System.Drawing.Size(1850, 44);
            this.tableLayoutPanel28.TabIndex = 0;
            // 
            // panel3
            // 
            this.panel3.Controls.Add(this.txtUtilisateur);
            this.panel3.Controls.Add(this.txtDateAjout);
            this.panel3.Controls.Add(this.label33);
            this.panel3.Controls.Add(this.cmdSuivant);
            this.panel3.Controls.Add(this.label1);
            this.panel3.Controls.Add(this.txtNumFicheStandard);
            this.panel3.Controls.Add(this.cmdDernier);
            this.panel3.Controls.Add(this.cmdPrecedent);
            this.panel3.Controls.Add(this.cmdPremier);
            this.panel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.panel3.Location = new System.Drawing.Point(0, 0);
            this.panel3.Margin = new System.Windows.Forms.Padding(0);
            this.panel3.Name = "panel3";
            this.panel3.Size = new System.Drawing.Size(925, 44);
            this.panel3.TabIndex = 577;
            // 
            // txtUtilisateur
            // 
            this.txtUtilisateur.AccAcceptNumbersOnly = false;
            this.txtUtilisateur.AccAllowComma = false;
            this.txtUtilisateur.AccBackgroundColor = System.Drawing.Color.White;
            this.txtUtilisateur.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtUtilisateur.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtUtilisateur.AccHidenValue = "";
            this.txtUtilisateur.AccNotAllowedChars = null;
            this.txtUtilisateur.AccReadOnly = false;
            this.txtUtilisateur.AccReadOnlyAllowDelete = false;
            this.txtUtilisateur.AccRequired = false;
            this.txtUtilisateur.BackColor = System.Drawing.Color.White;
            this.txtUtilisateur.CustomBackColor = System.Drawing.Color.White;
            this.txtUtilisateur.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtUtilisateur.ForeColor = System.Drawing.Color.Black;
            this.txtUtilisateur.Location = new System.Drawing.Point(7, 9);
            this.txtUtilisateur.Margin = new System.Windows.Forms.Padding(2);
            this.txtUtilisateur.MaxLength = 50;
            this.txtUtilisateur.Multiline = false;
            this.txtUtilisateur.Name = "txtUtilisateur";
            this.txtUtilisateur.ReadOnly = false;
            this.txtUtilisateur.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtUtilisateur.Size = new System.Drawing.Size(90, 27);
            this.txtUtilisateur.TabIndex = 502;
            this.txtUtilisateur.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtUtilisateur.UseSystemPasswordChar = false;
            // 
            // txtDateAjout
            // 
            this.txtDateAjout.AccAcceptNumbersOnly = false;
            this.txtDateAjout.AccAllowComma = false;
            this.txtDateAjout.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDateAjout.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDateAjout.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDateAjout.AccHidenValue = "";
            this.txtDateAjout.AccNotAllowedChars = null;
            this.txtDateAjout.AccReadOnly = true;
            this.txtDateAjout.AccReadOnlyAllowDelete = false;
            this.txtDateAjout.AccRequired = false;
            this.txtDateAjout.BackColor = System.Drawing.Color.White;
            this.txtDateAjout.CustomBackColor = System.Drawing.Color.White;
            this.txtDateAjout.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDateAjout.ForeColor = System.Drawing.Color.Blue;
            this.txtDateAjout.Location = new System.Drawing.Point(101, 9);
            this.txtDateAjout.Margin = new System.Windows.Forms.Padding(2);
            this.txtDateAjout.MaxLength = 50;
            this.txtDateAjout.Multiline = false;
            this.txtDateAjout.Name = "txtDateAjout";
            this.txtDateAjout.ReadOnly = false;
            this.txtDateAjout.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDateAjout.Size = new System.Drawing.Size(135, 27);
            this.txtDateAjout.TabIndex = 503;
            this.txtDateAjout.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDateAjout.UseSystemPasswordChar = false;
            // 
            // txtNumFicheStandard
            // 
            this.txtNumFicheStandard.AccAcceptNumbersOnly = false;
            this.txtNumFicheStandard.AccAllowComma = false;
            this.txtNumFicheStandard.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNumFicheStandard.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNumFicheStandard.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNumFicheStandard.AccHidenValue = "";
            this.txtNumFicheStandard.AccNotAllowedChars = null;
            this.txtNumFicheStandard.AccReadOnly = true;
            this.txtNumFicheStandard.AccReadOnlyAllowDelete = false;
            this.txtNumFicheStandard.AccRequired = false;
            this.txtNumFicheStandard.BackColor = System.Drawing.Color.White;
            this.txtNumFicheStandard.CustomBackColor = System.Drawing.Color.White;
            this.txtNumFicheStandard.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtNumFicheStandard.ForeColor = System.Drawing.Color.Blue;
            this.txtNumFicheStandard.Location = new System.Drawing.Point(311, 10);
            this.txtNumFicheStandard.Margin = new System.Windows.Forms.Padding(2);
            this.txtNumFicheStandard.MaxLength = 50;
            this.txtNumFicheStandard.Multiline = false;
            this.txtNumFicheStandard.Name = "txtNumFicheStandard";
            this.txtNumFicheStandard.ReadOnly = false;
            this.txtNumFicheStandard.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNumFicheStandard.Size = new System.Drawing.Size(82, 27);
            this.txtNumFicheStandard.TabIndex = 1;
            this.txtNumFicheStandard.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNumFicheStandard.UseSystemPasswordChar = false;
            // 
            // txtVille
            // 
            this.txtVille.AccAcceptNumbersOnly = false;
            this.txtVille.AccAllowComma = false;
            this.txtVille.AccBackgroundColor = System.Drawing.Color.White;
            this.txtVille.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtVille.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtVille.AccHidenValue = "";
            this.txtVille.AccNotAllowedChars = null;
            this.txtVille.AccReadOnly = false;
            this.txtVille.AccReadOnlyAllowDelete = false;
            this.txtVille.AccRequired = false;
            this.txtVille.BackColor = System.Drawing.Color.White;
            this.txtVille.CustomBackColor = System.Drawing.Color.White;
            this.txtVille.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtVille.ForeColor = System.Drawing.Color.Black;
            this.txtVille.Location = new System.Drawing.Point(1000, 11);
            this.txtVille.Margin = new System.Windows.Forms.Padding(2);
            this.txtVille.MaxLength = 32767;
            this.txtVille.Multiline = false;
            this.txtVille.Name = "txtVille";
            this.txtVille.ReadOnly = false;
            this.txtVille.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtVille.Size = new System.Drawing.Size(33, 27);
            this.txtVille.TabIndex = 594;
            this.txtVille.Text = "Ville";
            this.txtVille.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtVille.UseSystemPasswordChar = false;
            this.txtVille.Visible = false;
            // 
            // txtCodePostal
            // 
            this.txtCodePostal.AccAcceptNumbersOnly = false;
            this.txtCodePostal.AccAllowComma = false;
            this.txtCodePostal.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodePostal.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodePostal.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCodePostal.AccHidenValue = "";
            this.txtCodePostal.AccNotAllowedChars = null;
            this.txtCodePostal.AccReadOnly = false;
            this.txtCodePostal.AccReadOnlyAllowDelete = false;
            this.txtCodePostal.AccRequired = false;
            this.txtCodePostal.BackColor = System.Drawing.Color.White;
            this.txtCodePostal.CustomBackColor = System.Drawing.Color.White;
            this.txtCodePostal.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCodePostal.ForeColor = System.Drawing.Color.Black;
            this.txtCodePostal.Location = new System.Drawing.Point(1000, 11);
            this.txtCodePostal.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodePostal.MaxLength = 32767;
            this.txtCodePostal.Multiline = false;
            this.txtCodePostal.Name = "txtCodePostal";
            this.txtCodePostal.ReadOnly = false;
            this.txtCodePostal.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodePostal.Size = new System.Drawing.Size(33, 27);
            this.txtCodePostal.TabIndex = 593;
            this.txtCodePostal.Text = "CP";
            this.txtCodePostal.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodePostal.UseSystemPasswordChar = false;
            this.txtCodePostal.Visible = false;
            // 
            // txtAdresse2_IMM
            // 
            this.txtAdresse2_IMM.AccAcceptNumbersOnly = false;
            this.txtAdresse2_IMM.AccAllowComma = false;
            this.txtAdresse2_IMM.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAdresse2_IMM.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAdresse2_IMM.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtAdresse2_IMM.AccHidenValue = "";
            this.txtAdresse2_IMM.AccNotAllowedChars = null;
            this.txtAdresse2_IMM.AccReadOnly = false;
            this.txtAdresse2_IMM.AccReadOnlyAllowDelete = false;
            this.txtAdresse2_IMM.AccRequired = false;
            this.txtAdresse2_IMM.BackColor = System.Drawing.Color.White;
            this.txtAdresse2_IMM.CustomBackColor = System.Drawing.Color.White;
            this.txtAdresse2_IMM.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtAdresse2_IMM.ForeColor = System.Drawing.Color.Black;
            this.txtAdresse2_IMM.Location = new System.Drawing.Point(1000, 11);
            this.txtAdresse2_IMM.Margin = new System.Windows.Forms.Padding(2);
            this.txtAdresse2_IMM.MaxLength = 32767;
            this.txtAdresse2_IMM.Multiline = false;
            this.txtAdresse2_IMM.Name = "txtAdresse2_IMM";
            this.txtAdresse2_IMM.ReadOnly = false;
            this.txtAdresse2_IMM.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAdresse2_IMM.Size = new System.Drawing.Size(33, 27);
            this.txtAdresse2_IMM.TabIndex = 592;
            this.txtAdresse2_IMM.Text = "Adresse2";
            this.txtAdresse2_IMM.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAdresse2_IMM.UseSystemPasswordChar = false;
            this.txtAdresse2_IMM.Visible = false;
            // 
            // txtAdresse
            // 
            this.txtAdresse.AccAcceptNumbersOnly = false;
            this.txtAdresse.AccAllowComma = false;
            this.txtAdresse.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAdresse.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAdresse.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtAdresse.AccHidenValue = "";
            this.txtAdresse.AccNotAllowedChars = null;
            this.txtAdresse.AccReadOnly = false;
            this.txtAdresse.AccReadOnlyAllowDelete = false;
            this.txtAdresse.AccRequired = false;
            this.txtAdresse.BackColor = System.Drawing.Color.White;
            this.txtAdresse.CustomBackColor = System.Drawing.Color.White;
            this.txtAdresse.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtAdresse.ForeColor = System.Drawing.Color.Black;
            this.txtAdresse.Location = new System.Drawing.Point(1000, 11);
            this.txtAdresse.Margin = new System.Windows.Forms.Padding(2);
            this.txtAdresse.MaxLength = 32767;
            this.txtAdresse.Multiline = false;
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.ReadOnly = false;
            this.txtAdresse.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAdresse.Size = new System.Drawing.Size(33, 27);
            this.txtAdresse.TabIndex = 591;
            this.txtAdresse.Text = "Adresse1";
            this.txtAdresse.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAdresse.UseSystemPasswordChar = false;
            this.txtAdresse.Visible = false;
            // 
            // txtSujet
            // 
            this.txtSujet.AccAcceptNumbersOnly = false;
            this.txtSujet.AccAllowComma = false;
            this.txtSujet.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSujet.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSujet.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtSujet.AccHidenValue = "";
            this.txtSujet.AccNotAllowedChars = null;
            this.txtSujet.AccReadOnly = false;
            this.txtSujet.AccReadOnlyAllowDelete = false;
            this.txtSujet.AccRequired = false;
            this.txtSujet.BackColor = System.Drawing.Color.White;
            this.txtSujet.CustomBackColor = System.Drawing.Color.White;
            this.txtSujet.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtSujet.ForeColor = System.Drawing.Color.Black;
            this.txtSujet.Location = new System.Drawing.Point(1000, 11);
            this.txtSujet.Margin = new System.Windows.Forms.Padding(2);
            this.txtSujet.MaxLength = 32767;
            this.txtSujet.Multiline = false;
            this.txtSujet.Name = "txtSujet";
            this.txtSujet.ReadOnly = false;
            this.txtSujet.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSujet.Size = new System.Drawing.Size(50, 27);
            this.txtSujet.TabIndex = 590;
            this.txtSujet.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSujet.UseSystemPasswordChar = false;
            this.txtSujet.Visible = false;
            // 
            // txtCodeOrigine3_TO3
            // 
            this.txtCodeOrigine3_TO3.AccAcceptNumbersOnly = false;
            this.txtCodeOrigine3_TO3.AccAllowComma = false;
            this.txtCodeOrigine3_TO3.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeOrigine3_TO3.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeOrigine3_TO3.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCodeOrigine3_TO3.AccHidenValue = "";
            this.txtCodeOrigine3_TO3.AccNotAllowedChars = null;
            this.txtCodeOrigine3_TO3.AccReadOnly = false;
            this.txtCodeOrigine3_TO3.AccReadOnlyAllowDelete = false;
            this.txtCodeOrigine3_TO3.AccRequired = false;
            this.txtCodeOrigine3_TO3.BackColor = System.Drawing.Color.White;
            this.txtCodeOrigine3_TO3.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeOrigine3_TO3.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCodeOrigine3_TO3.ForeColor = System.Drawing.Color.Black;
            this.txtCodeOrigine3_TO3.Location = new System.Drawing.Point(1000, 11);
            this.txtCodeOrigine3_TO3.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeOrigine3_TO3.MaxLength = 32767;
            this.txtCodeOrigine3_TO3.Multiline = false;
            this.txtCodeOrigine3_TO3.Name = "txtCodeOrigine3_TO3";
            this.txtCodeOrigine3_TO3.ReadOnly = false;
            this.txtCodeOrigine3_TO3.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeOrigine3_TO3.Size = new System.Drawing.Size(50, 27);
            this.txtCodeOrigine3_TO3.TabIndex = 587;
            this.txtCodeOrigine3_TO3.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeOrigine3_TO3.UseSystemPasswordChar = false;
            this.txtCodeOrigine3_TO3.Visible = false;
            // 
            // txtCodeContrat
            // 
            this.txtCodeContrat.AccAcceptNumbersOnly = false;
            this.txtCodeContrat.AccAllowComma = false;
            this.txtCodeContrat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeContrat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeContrat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCodeContrat.AccHidenValue = "";
            this.txtCodeContrat.AccNotAllowedChars = null;
            this.txtCodeContrat.AccReadOnly = false;
            this.txtCodeContrat.AccReadOnlyAllowDelete = false;
            this.txtCodeContrat.AccRequired = false;
            this.txtCodeContrat.BackColor = System.Drawing.Color.White;
            this.txtCodeContrat.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeContrat.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCodeContrat.ForeColor = System.Drawing.Color.Black;
            this.txtCodeContrat.Location = new System.Drawing.Point(1000, 11);
            this.txtCodeContrat.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeContrat.MaxLength = 32767;
            this.txtCodeContrat.Multiline = false;
            this.txtCodeContrat.Name = "txtCodeContrat";
            this.txtCodeContrat.ReadOnly = false;
            this.txtCodeContrat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeContrat.Size = new System.Drawing.Size(50, 27);
            this.txtCodeContrat.TabIndex = 586;
            this.txtCodeContrat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeContrat.UseSystemPasswordChar = false;
            this.txtCodeContrat.Visible = false;
            // 
            // txtIntervention
            // 
            this.txtIntervention.AccAcceptNumbersOnly = false;
            this.txtIntervention.AccAllowComma = false;
            this.txtIntervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtIntervention.AccHidenValue = "";
            this.txtIntervention.AccNotAllowedChars = null;
            this.txtIntervention.AccReadOnly = false;
            this.txtIntervention.AccReadOnlyAllowDelete = false;
            this.txtIntervention.AccRequired = false;
            this.txtIntervention.BackColor = System.Drawing.Color.White;
            this.txtIntervention.CustomBackColor = System.Drawing.Color.White;
            this.txtIntervention.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtIntervention.ForeColor = System.Drawing.Color.Black;
            this.txtIntervention.Location = new System.Drawing.Point(1000, 11);
            this.txtIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntervention.MaxLength = 32767;
            this.txtIntervention.Multiline = false;
            this.txtIntervention.Name = "txtIntervention";
            this.txtIntervention.ReadOnly = false;
            this.txtIntervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntervention.Size = new System.Drawing.Size(50, 27);
            this.txtIntervention.TabIndex = 585;
            this.txtIntervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntervention.UseSystemPasswordChar = false;
            this.txtIntervention.Visible = false;
            // 
            // txtDocumentDevis
            // 
            this.txtDocumentDevis.AccAcceptNumbersOnly = false;
            this.txtDocumentDevis.AccAllowComma = false;
            this.txtDocumentDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDocumentDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDocumentDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtDocumentDevis.AccHidenValue = "";
            this.txtDocumentDevis.AccNotAllowedChars = null;
            this.txtDocumentDevis.AccReadOnly = false;
            this.txtDocumentDevis.AccReadOnlyAllowDelete = false;
            this.txtDocumentDevis.AccRequired = false;
            this.txtDocumentDevis.BackColor = System.Drawing.Color.White;
            this.txtDocumentDevis.CustomBackColor = System.Drawing.Color.White;
            this.txtDocumentDevis.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDocumentDevis.ForeColor = System.Drawing.Color.Black;
            this.txtDocumentDevis.Location = new System.Drawing.Point(1000, 11);
            this.txtDocumentDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtDocumentDevis.MaxLength = 32767;
            this.txtDocumentDevis.Multiline = false;
            this.txtDocumentDevis.Name = "txtDocumentDevis";
            this.txtDocumentDevis.ReadOnly = false;
            this.txtDocumentDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDocumentDevis.Size = new System.Drawing.Size(50, 27);
            this.txtDocumentDevis.TabIndex = 584;
            this.txtDocumentDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDocumentDevis.UseSystemPasswordChar = false;
            this.txtDocumentDevis.Visible = false;
            // 
            // txtCodeOriCommentDevis
            // 
            this.txtCodeOriCommentDevis.AccAcceptNumbersOnly = false;
            this.txtCodeOriCommentDevis.AccAllowComma = false;
            this.txtCodeOriCommentDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeOriCommentDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeOriCommentDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCodeOriCommentDevis.AccHidenValue = "";
            this.txtCodeOriCommentDevis.AccNotAllowedChars = null;
            this.txtCodeOriCommentDevis.AccReadOnly = false;
            this.txtCodeOriCommentDevis.AccReadOnlyAllowDelete = false;
            this.txtCodeOriCommentDevis.AccRequired = false;
            this.txtCodeOriCommentDevis.BackColor = System.Drawing.Color.White;
            this.txtCodeOriCommentDevis.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeOriCommentDevis.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCodeOriCommentDevis.ForeColor = System.Drawing.Color.Black;
            this.txtCodeOriCommentDevis.Location = new System.Drawing.Point(1000, 11);
            this.txtCodeOriCommentDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeOriCommentDevis.MaxLength = 32767;
            this.txtCodeOriCommentDevis.Multiline = false;
            this.txtCodeOriCommentDevis.Name = "txtCodeOriCommentDevis";
            this.txtCodeOriCommentDevis.ReadOnly = false;
            this.txtCodeOriCommentDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeOriCommentDevis.Size = new System.Drawing.Size(50, 27);
            this.txtCodeOriCommentDevis.TabIndex = 583;
            this.txtCodeOriCommentDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeOriCommentDevis.UseSystemPasswordChar = false;
            this.txtCodeOriCommentDevis.Visible = false;
            // 
            // txtCodeGerant
            // 
            this.txtCodeGerant.AccAcceptNumbersOnly = false;
            this.txtCodeGerant.AccAllowComma = false;
            this.txtCodeGerant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeGerant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeGerant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCodeGerant.AccHidenValue = "";
            this.txtCodeGerant.AccNotAllowedChars = null;
            this.txtCodeGerant.AccReadOnly = false;
            this.txtCodeGerant.AccReadOnlyAllowDelete = false;
            this.txtCodeGerant.AccRequired = false;
            this.txtCodeGerant.BackColor = System.Drawing.Color.White;
            this.txtCodeGerant.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeGerant.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCodeGerant.ForeColor = System.Drawing.Color.Black;
            this.txtCodeGerant.Location = new System.Drawing.Point(1000, 11);
            this.txtCodeGerant.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeGerant.MaxLength = 32767;
            this.txtCodeGerant.Multiline = false;
            this.txtCodeGerant.Name = "txtCodeGerant";
            this.txtCodeGerant.ReadOnly = false;
            this.txtCodeGerant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeGerant.Size = new System.Drawing.Size(50, 27);
            this.txtCodeGerant.TabIndex = 582;
            this.txtCodeGerant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeGerant.UseSystemPasswordChar = false;
            this.txtCodeGerant.Visible = false;
            // 
            // txtRaisonSocial_IMM
            // 
            this.txtRaisonSocial_IMM.AccAcceptNumbersOnly = false;
            this.txtRaisonSocial_IMM.AccAllowComma = false;
            this.txtRaisonSocial_IMM.AccBackgroundColor = System.Drawing.Color.White;
            this.txtRaisonSocial_IMM.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtRaisonSocial_IMM.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtRaisonSocial_IMM.AccHidenValue = "";
            this.txtRaisonSocial_IMM.AccNotAllowedChars = null;
            this.txtRaisonSocial_IMM.AccReadOnly = false;
            this.txtRaisonSocial_IMM.AccReadOnlyAllowDelete = false;
            this.txtRaisonSocial_IMM.AccRequired = false;
            this.txtRaisonSocial_IMM.BackColor = System.Drawing.Color.White;
            this.txtRaisonSocial_IMM.CustomBackColor = System.Drawing.Color.White;
            this.txtRaisonSocial_IMM.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtRaisonSocial_IMM.ForeColor = System.Drawing.Color.Black;
            this.txtRaisonSocial_IMM.Location = new System.Drawing.Point(1000, 11);
            this.txtRaisonSocial_IMM.Margin = new System.Windows.Forms.Padding(2);
            this.txtRaisonSocial_IMM.MaxLength = 50;
            this.txtRaisonSocial_IMM.Multiline = false;
            this.txtRaisonSocial_IMM.Name = "txtRaisonSocial_IMM";
            this.txtRaisonSocial_IMM.ReadOnly = false;
            this.txtRaisonSocial_IMM.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtRaisonSocial_IMM.Size = new System.Drawing.Size(50, 27);
            this.txtRaisonSocial_IMM.TabIndex = 581;
            this.txtRaisonSocial_IMM.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtRaisonSocial_IMM.UseSystemPasswordChar = false;
            this.txtRaisonSocial_IMM.Visible = false;
            this.txtRaisonSocial_IMM.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtRaisonSocial_IMM_KeyPress);
            this.txtRaisonSocial_IMM.Validating += new System.ComponentModel.CancelEventHandler(this.txtRaisonSocial_IMM_Validating);
            // 
            // txtCodeOrigine2
            // 
            this.txtCodeOrigine2.AccAcceptNumbersOnly = false;
            this.txtCodeOrigine2.AccAllowComma = false;
            this.txtCodeOrigine2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeOrigine2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeOrigine2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCodeOrigine2.AccHidenValue = "";
            this.txtCodeOrigine2.AccNotAllowedChars = null;
            this.txtCodeOrigine2.AccReadOnly = false;
            this.txtCodeOrigine2.AccReadOnlyAllowDelete = false;
            this.txtCodeOrigine2.AccRequired = false;
            this.txtCodeOrigine2.BackColor = System.Drawing.Color.White;
            this.txtCodeOrigine2.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeOrigine2.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCodeOrigine2.ForeColor = System.Drawing.Color.Black;
            this.txtCodeOrigine2.Location = new System.Drawing.Point(1000, 11);
            this.txtCodeOrigine2.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeOrigine2.MaxLength = 32767;
            this.txtCodeOrigine2.Multiline = false;
            this.txtCodeOrigine2.Name = "txtCodeOrigine2";
            this.txtCodeOrigine2.ReadOnly = false;
            this.txtCodeOrigine2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeOrigine2.Size = new System.Drawing.Size(50, 27);
            this.txtCodeOrigine2.TabIndex = 580;
            this.txtCodeOrigine2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeOrigine2.UseSystemPasswordChar = false;
            this.txtCodeOrigine2.Visible = false;
            // 
            // txtDocument
            // 
            this.txtDocument.AccAcceptNumbersOnly = false;
            this.txtDocument.AccAllowComma = false;
            this.txtDocument.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDocument.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDocument.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtDocument.AccHidenValue = "";
            this.txtDocument.AccNotAllowedChars = null;
            this.txtDocument.AccReadOnly = false;
            this.txtDocument.AccReadOnlyAllowDelete = false;
            this.txtDocument.AccRequired = false;
            this.txtDocument.BackColor = System.Drawing.Color.White;
            this.txtDocument.CustomBackColor = System.Drawing.Color.White;
            this.txtDocument.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtDocument.ForeColor = System.Drawing.Color.Black;
            this.txtDocument.Location = new System.Drawing.Point(1000, 11);
            this.txtDocument.Margin = new System.Windows.Forms.Padding(2);
            this.txtDocument.MaxLength = 32767;
            this.txtDocument.Multiline = false;
            this.txtDocument.Name = "txtDocument";
            this.txtDocument.ReadOnly = false;
            this.txtDocument.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDocument.Size = new System.Drawing.Size(50, 27);
            this.txtDocument.TabIndex = 579;
            this.txtDocument.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDocument.UseSystemPasswordChar = false;
            this.txtDocument.Visible = false;
            // 
            // txtCodeOrigine1
            // 
            this.txtCodeOrigine1.AccAcceptNumbersOnly = false;
            this.txtCodeOrigine1.AccAllowComma = false;
            this.txtCodeOrigine1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeOrigine1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeOrigine1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(180)))), ((int)(((byte)(180)))), ((int)(((byte)(180)))));
            this.txtCodeOrigine1.AccHidenValue = "";
            this.txtCodeOrigine1.AccNotAllowedChars = null;
            this.txtCodeOrigine1.AccReadOnly = false;
            this.txtCodeOrigine1.AccReadOnlyAllowDelete = false;
            this.txtCodeOrigine1.AccRequired = false;
            this.txtCodeOrigine1.BackColor = System.Drawing.Color.White;
            this.txtCodeOrigine1.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeOrigine1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCodeOrigine1.ForeColor = System.Drawing.Color.Black;
            this.txtCodeOrigine1.Location = new System.Drawing.Point(1000, 11);
            this.txtCodeOrigine1.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeOrigine1.MaxLength = 32767;
            this.txtCodeOrigine1.Multiline = false;
            this.txtCodeOrigine1.Name = "txtCodeOrigine1";
            this.txtCodeOrigine1.ReadOnly = false;
            this.txtCodeOrigine1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeOrigine1.Size = new System.Drawing.Size(50, 27);
            this.txtCodeOrigine1.TabIndex = 578;
            this.txtCodeOrigine1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeOrigine1.UseSystemPasswordChar = false;
            this.txtCodeOrigine1.Visible = false;
            // 
            // UserDocStandard
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel27);
            this.Controls.Add(this.cmdSupprimer);
            this.Controls.Add(this.optRadiateur);
            this.Controls.Add(this.optECS);
            this.Controls.Add(this.optFuite);
            this.Controls.Add(this.optChauffage);
            this.Controls.Add(this.optDepannage);
            this.Controls.Add(this.txtVille);
            this.Controls.Add(this.txtCodePostal);
            this.Controls.Add(this.txtAdresse2_IMM);
            this.Controls.Add(this.txtAdresse);
            this.Controls.Add(this.txtSujet);
            this.Controls.Add(this.lblGoIntervention);
            this.Controls.Add(this.cmdRechercheRaisonSocial);
            this.Controls.Add(this.txtCodeOrigine3_TO3);
            this.Controls.Add(this.txtCodeContrat);
            this.Controls.Add(this.txtIntervention);
            this.Controls.Add(this.txtDocumentDevis);
            this.Controls.Add(this.txtCodeOriCommentDevis);
            this.Controls.Add(this.txtCodeGerant);
            this.Controls.Add(this.txtRaisonSocial_IMM);
            this.Controls.Add(this.txtCodeOrigine2);
            this.Controls.Add(this.txtDocument);
            this.Controls.Add(this.txtCodeOrigine1);
            this.Name = "UserDocStandard";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Fiche D\'appel";
            this.Load += new System.EventHandler(this.UserDocStandard_Load);
            this.VisibleChanged += new System.EventHandler(this.UserDocStandard_VisibleChanged);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.Frame4.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.tableLayoutPanel6.PerformLayout();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel7.PerformLayout();
            this.Frame1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.tableLayoutPanel30.ResumeLayout(false);
            this.tableLayoutPanel30.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDateOS)).EndInit();
            this.tableLayoutPanel29.ResumeLayout(false);
            this.panel4.ResumeLayout(false);
            this.panel4.PerformLayout();
            this.Picture5.ResumeLayout(false);
            this.tableLayoutPanel31.ResumeLayout(false);
            this.tableLayoutPanel31.PerformLayout();
            this.Frame3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodeParticulier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIntervenantDT)).EndInit();
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel26.PerformLayout();
            this.tableLayoutPanel8.ResumeLayout(false);
            this.tableLayoutPanel17.ResumeLayout(false);
            this.tableLayoutPanel17.PerformLayout();
            this.groupBox3.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel11.ResumeLayout(false);
            this.tableLayoutPanel12.ResumeLayout(false);
            this.tableLayoutPanel13.ResumeLayout(false);
            this.tableLayoutPanel13.PerformLayout();
            this.panel1.ResumeLayout(false);
            this.tableLayoutPanel14.ResumeLayout(false);
            this.tableLayoutPanel15.ResumeLayout(false);
            this.tableLayoutPanel15.PerformLayout();
            this.panel2.ResumeLayout(false);
            this.tableLayoutPanel16.ResumeLayout(false);
            this.tableLayoutPanel16.PerformLayout();
            this.groupBox4.ResumeLayout(false);
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCAI_CategorInterv)).EndInit();
            this.frmDevis.ResumeLayout(false);
            this.tableLayoutPanel19.ResumeLayout(false);
            this.tableLayoutPanel20.ResumeLayout(false);
            this.tableLayoutPanel21.ResumeLayout(false);
            this.tableLayoutPanel21.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssoleDbQuiDevis)).EndInit();
            this.tableLayoutPanel22.ResumeLayout(false);
            this.tableLayoutPanel22.PerformLayout();
            this.tableLayoutPanel23.ResumeLayout(false);
            this.tableLayoutPanel23.PerformLayout();
            this.tableLayoutPanel24.ResumeLayout(false);
            this.tableLayoutPanel24.PerformLayout();
            this.tableLayoutPanel25.ResumeLayout(false);
            this.tableLayoutPanel25.PerformLayout();
            this.tableLayoutPanel27.ResumeLayout(false);
            this.tableLayoutPanel28.ResumeLayout(false);
            this.panel3.ResumeLayout(false);
            this.panel3.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        public iTalk.iTalk_TextBox_Small2 txtUtilisateur;
        public iTalk.iTalk_TextBox_Small2 txtDateAjout;
        private System.Windows.Forms.Label label33;
        public System.Windows.Forms.Button cmdPremier;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmdSupprimer;
        public System.Windows.Forms.Button cmdDossierIntervention;
        public System.Windows.Forms.Button CmdRechercher;
        public System.Windows.Forms.Button cmdAjouter;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button CmdSauver;
        public System.Windows.Forms.Button CmdEditer;
        private System.Windows.Forms.Label label1;
        public iTalk.iTalk_TextBox_Small2 txtNumFicheStandard;
        public System.Windows.Forms.Button cmdDernier;
        public System.Windows.Forms.Button cmdSuivant;
        public System.Windows.Forms.Button cmdPrecedent;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.RadioButton optOriTelephone;
        private System.Windows.Forms.RadioButton optOriRepondeur;
        private System.Windows.Forms.RadioButton optOriDirect;
        private System.Windows.Forms.RadioButton optOriFax;
        private System.Windows.Forms.RadioButton optOriCourrier;
        private System.Windows.Forms.RadioButton optOriRadio;
        private System.Windows.Forms.RadioButton Option1;
        private System.Windows.Forms.RadioButton optOriSiteWeb;
        private System.Windows.Forms.RadioButton optOriMail;
        private System.Windows.Forms.Label lblFaxouCourier;
        private System.Windows.Forms.ListBox file1;
        private System.Windows.Forms.RadioButton OptOriAstreinte;
        private System.Windows.Forms.GroupBox Frame1;
        private System.Windows.Forms.GroupBox Frame3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        private System.Windows.Forms.RadioButton optQuiSociete;
        private System.Windows.Forms.RadioButton optQuiGerant;
        private System.Windows.Forms.RadioButton optQuiGardien;
        private System.Windows.Forms.RadioButton optQuiAutre;
        private System.Windows.Forms.RadioButton optQuiFournisseur;
        private System.Windows.Forms.RadioButton optQuiCopro;
        private System.Windows.Forms.RadioButton OptQuiGestionnaire;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbIntervenantDT;
        public iTalk.iTalk_TextBox_Small2 lbllibCodeParticulier;
        public Infragistics.Win.UltraWinGrid.UltraCombo txtCodeParticulier;
        public iTalk.iTalk_TextBox_Small2 txtSource;
        private System.Windows.Forms.Label lblSociete;
        public iTalk.iTalk_TextBox_Small2 txtSociete;
        public iTalk.iTalk_TextBox_Small2 lblLibIntervenantDT;
        public System.Windows.Forms.Button cmdIntervenant;
        private System.Windows.Forms.Label lblSource;
        public System.Windows.Forms.Button cmdRechercheFour;
        private System.Windows.Forms.Label lblParticulier;
        public System.Windows.Forms.Button cmdEctituresCopro;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.GroupBox Frame4;
        public System.Windows.Forms.Button cmdMail;
        public System.Windows.Forms.Button cmdGMAO;
        public System.Windows.Forms.Button cmdGoIntervention;
        public System.Windows.Forms.Button cmdContrat;
        public System.Windows.Forms.Button cmdMAJ;
        private System.Windows.Forms.Label lblRenovation;
        public System.Windows.Forms.Button cmdDevis1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        public iTalk.iTalk_TextBox_Small2 txtObs;
        public iTalk.iTalk_TextBox_Small2 txtObservations;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        public iTalk.iTalk_TextBox_Small2 txtGardien;
        public iTalk.iTalk_TextBox_Small2 lblConseilSyndical;
        public iTalk.iTalk_TextBox_Small2 lblRespExploit;
        public iTalk.iTalk_TextBox_Small2 lblCommercial;
        public iTalk.iTalk_TextBox_Small2 txtcontrat;
        public iTalk.iTalk_TextBox_Small2 txtCode1;
        private System.Windows.Forms.Label Label16;
        private System.Windows.Forms.Label Label4;
        private System.Windows.Forms.Label Label17;
        private System.Windows.Forms.Label lblGoContrat;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.Label label16s;
        private System.Windows.Forms.Label label17s;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        public System.Windows.Forms.Button cmdRechercheImmeuble;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeuble;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel11;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel12;
        public iTalk.iTalk_TextBox_Small2 txtCodeAcces2;
        public iTalk.iTalk_TextBox_Small2 txtCodeAcces1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel13;
        private System.Windows.Forms.Label label20;
        public System.Windows.Forms.Button cmdFindIntervention;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel14;
        public iTalk.iTalk_TextBox_Small2 Label25;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel15;
        private System.Windows.Forms.Label label21;
        public System.Windows.Forms.Button cmdHistoAppels;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel16;
        private System.Windows.Forms.CheckBox CHKP3;
        private System.Windows.Forms.CheckBox CHKP4;
        private System.Windows.Forms.CheckBox CHKP1;
        private System.Windows.Forms.CheckBox CHKP2;
        public iTalk.iTalk_TextBox_Small2 lblGerant;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel17;
        public iTalk.iTalk_TextBox_Small2 txtObservation;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.Label label22s;
        public iTalk.iTalk_TextBox_Small2 txtObservationComm_IMM;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        public System.Windows.Forms.Button cmdGestionMotif;
        private System.Windows.Forms.Label label23;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbCAI_CategorInterv;
        public iTalk.iTalk_TextBox_Small2 lbllibCAI_CategorInterv;
        private System.Windows.Forms.GroupBox frmDevis;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel19;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel20;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel21;
        public Infragistics.Win.UltraWinGrid.UltraCombo ssoleDbQuiDevis;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel22;
        private System.Windows.Forms.CheckBox ChkDmdeDevis;
        private System.Windows.Forms.CheckBox ChkDmdeDevisContrat;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel23;
        public System.Windows.Forms.Button cmdVisuDevis;
        public System.Windows.Forms.Button cmdQuiDevis;
        private System.Windows.Forms.Label label25s;
        public iTalk.iTalk_TextBox_Small2 txtTexte;
        private System.Windows.Forms.CheckBox ChkDevisAcc;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel24;
        public System.Windows.Forms.Button cmdRechNoDev;
        public iTalk.iTalk_TextBox_Small2 txtNoDevis;
        private System.Windows.Forms.Label label26;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel25;
        public iTalk.iTalk_TextBox_Small2 txtDateDevis;
        private System.Windows.Forms.Label label27;
        public iTalk.iTalk_TextBox_Small2 txtCodeOrigine1;
        public iTalk.iTalk_TextBox_Small2 txtDocument;
        public iTalk.iTalk_TextBox_Small2 txtCodeOrigine2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        public System.Windows.Forms.Button cmdAjoutgestion;
        public System.Windows.Forms.Button cmdEcritures;
        public iTalk.iTalk_TextBox_Small2 txtRaisonSocial_IMM;
        public iTalk.iTalk_TextBox_Small2 txtCodeGerant;
        public iTalk.iTalk_TextBox_Small2 txtQuiDevis;
        public iTalk.iTalk_TextBox_Small2 txtCodeOriCommentDevis;
        public iTalk.iTalk_TextBox_Small2 txtDocumentDevis;
        public iTalk.iTalk_TextBox_Small2 lblComDevis;
        public iTalk.iTalk_TextBox_Small2 txtIntervention;
        public iTalk.iTalk_TextBox_Small2 txtCodeContrat;
        public iTalk.iTalk_TextBox_Small2 txtCodeOrigine3_TO3;
        public System.Windows.Forms.Button cmdRechercheRaisonSocial;
        private System.Windows.Forms.Label lblGoIntervention;
        public iTalk.iTalk_TextBox_Small2 txtSujet;
        public iTalk.iTalk_TextBox_Small2 txtAdresse;
        public iTalk.iTalk_TextBox_Small2 txtAdresse2_IMM;
        public iTalk.iTalk_TextBox_Small2 txtCodePostal;
        public iTalk.iTalk_TextBox_Small2 txtVille;
        private System.Windows.Forms.RadioButton optDepannage;
        private System.Windows.Forms.RadioButton optChauffage;
        private System.Windows.Forms.RadioButton optFuite;
        private System.Windows.Forms.RadioButton optECS;
        private System.Windows.Forms.RadioButton optRadiateur;
        private System.Windows.Forms.Label Label22;
        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panel2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel27;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel28;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel29;
        public iTalk.iTalk_TextBox_Small2 txtNcompte;
        private System.Windows.Forms.PageSetupDialog pageSetupDialog1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel30;
        private System.Windows.Forms.Label Label19;
        private System.Windows.Forms.Label Label29;
        public iTalk.iTalk_TextBox_Small2 txtRefOS;
        private Infragistics.Win.UltraWinEditors.UltraDateTimeEditor txtDateOS;
        private System.Windows.Forms.Panel panel4;
        public System.Windows.Forms.Button cmdStocker;
        public System.Windows.Forms.Button cmdFichierOS;
        private System.Windows.Forms.Label Label30;
        private System.Windows.Forms.RadioButton OptOSfichier;
        private System.Windows.Forms.RadioButton OptOSori;
        public iTalk.iTalk_TextBox_Small2 txtCheminOS;
        private System.Windows.Forms.Panel Picture5;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel31;
        private System.Windows.Forms.Label lblSolde;
    }
}
