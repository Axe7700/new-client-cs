﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Appel.FicheGestionAppel.Forms;
using Axe_interDT.Views.Appel.Forms;
//using Microsoft.VisualBasic;
using Axe_interDT.Views.BaseDeDonnees.Gerant;
using Axe_interDT.Views.BaseDeDonnees.Immeuble;
using Axe_interDT.Views.Intervention;
using Infragistics.Win.UltraWinTree;
using System;
using System.Collections.Generic;
using System.Data;
using System.Drawing;
using System.Net;
using System.Text;
using System.Windows.Forms;
using System.Xml;

namespace Axe_interDT.Views.FicheGestionAppel
{

    public partial class UserDocGestionAppel : UserControl
    {
        private const string cId_Inconnu = "XXXYYYZZZ111112222";
        private const string cIDAxeciel = "id_axeciel";

        public UserDocGestionAppel()
        {
            InitializeComponent();

            imageList1.Images.Add("1", Properties.Resources.talking_call);
            imageList1.Images.Add("2", Properties.Resources.income_call);
            imageList1.Images.Add("3", Properties.Resources.outgoing_call);
            imageList1.Images.Add("4", Properties.Resources.run_call);
        }
        /// <summary>
        /// Testesd
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCreateInter_Click(object sender, EventArgs e)
        {
            bool bWithContrat = false;

            if (fc_ControlDataInter() == false)//tested
                return;

            General.NomImmeuble = txtcodeimmeuble.Text;

            //'frmHistorique.Left = LabelAppel(0).Left + 500
            //'frmHistorique.Top = Label26.Top + Frame5.Top + 400
            //'frmHistorique.Show vbModal

            frmCreateInterViaAppel f = new frmCreateInterViaAppel();
            f.txtCodeimmeuble.Text = txtcodeimmeuble.Text;
            f.txtCode1.Text = txtCode1.Text;
            f.txtCodeEtat.Text = "00";
            f.txtcontrat.Text = ModContrat.fc_RechercheContrat(txtcodeimmeuble.Text, ref bWithContrat);

            if (bWithContrat == true)
            {
                f.txtcontrat.ForeColor = Color.Blue;
            }
            else
            {
                f.txtcontrat.ForeColor = Color.Red;
            }

            f.ShowDialog();

            if (General.lCreateInterViaGestAppel != 0)
            {
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, General.lCreateInterViaGestAppel.ToString());
                View.Theme.Theme.Navigate(typeof(UserIntervention));
            }
        }

        private void fc_LoadData(string sID)
        {

            try
            {
                string sSQL = "";
                DataTable rs = new DataTable();
                ModAdo rsAdo = new ModAdo();
                int LID = 0;
                string sCodeimmeuble;
                string sCodeParticulier;
                string stempX;
                double dbSolde = 0;
                bool bWithContrat = false;

                fraParticulier.Visible = false;
                fc_clean();
                stempX = General.Mid(sID, 3, General.Len(sID) - 2);

                if (General.IsNumeric(stempX))
                {
                    LID = Convert.ToInt32(General.Mid(sID, 3, General.Len(sID) - 2));
                }
                else
                {
                    return;
                }
                sCodeimmeuble = "";
                sCodeParticulier = "";
                if (General.Left(sID, 2) == General.UCase("CL"))
                {
                    // '=== client => table 1.
                    //SELECT     'CL' + CONVERT(varchar(10), ID_LOG) AS id, Nom AS NOM, Telephone AS TEL, '' AS PORTABLE
                    // From Table1
                    // WHERE     (Nom <> N'')
                    sSQL = "SELECT     Immeuble.CodeImmeuble  FROM   Table1 INNER JOIN " + " Immeuble ON Table1.Code1 = Immeuble.Code1 "
                           + " WHERE   Table1.ID_LOG = " + LID;


                    rs = rsAdo.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)
                    {
                        sCodeimmeuble = rs.Rows[0]["CodeImmeuble"] + "";
                    }
                    rs.Dispose();
                    rs = null;
                }
                else if (General.Left(sID, 2) == General.UCase("IM")) // adresse client hors syndics => ImmeublePart.
                {

                    // SELECT     'IM' + CONVERT(varchar(10), ID_LOG) AS ID, Nom AS NOM, Tel AS TEL, TelPortable_IMP AS PORTABLE
                    //From ImmeublePart
                    // WHERE     (Nom <> N'')

                    sSQL = "SELECT      CodeImmeuble, CodeParticulier, Nom,NCompte, CodeQualif_QUA from ImmeublePart "
                           + " WHERE   ID_LOG = " + LID;


                    rs = rsAdo.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)
                    {
                        sCodeimmeuble = rs.Rows[0]["CodeImmeuble"] + "";
                        sCodeParticulier = rs.Rows[0]["CodeParticulier"] + "";
                        fraParticulier.Visible = true;
                        txtCodeParticulier.Text = sCodeParticulier;
                        txtNom.Text = rs.Rows[0]["Nom"] + "";

                        if (General.UCase(General.nz(rs.Rows[0]["CodeQualif_QUA"], "").ToString()) == General.UCase(General.cPart))
                        {
                            dbSolde = ModFinance.fc_Solde(sCodeimmeuble, rs.Rows[0]["NCompte"] + "");
                            lblParticulier.Text = "Particulier - solde " + dbSolde;
                        }

                    }
                    rs.Dispose();
                    rs = null;

                }
                else if (General.Left(sID, 2) == General.UCase("CI"))//=== immeuble correspondant  => IMC_ImmCorrespondant.
                {

                    sSQL = "SELECT      CodeImmeuble_IMM, CodeCorresp_IMC, ID_LOG From IMC_ImmCorrespondant"
                           + " WHERE   CodeCorresp_IMC  = " + LID;


                    rs = rsAdo.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)
                    {
                        sCodeimmeuble = rs.Rows[0]["CodeImmeuble_IMM"] + "";
                        sCodeParticulier = rs.Rows[0]["CodeCorresp_IMC"] + "";
                    }
                    rs.Dispose();
                    rs = null;

                }
                else if (General.Left(sID, 2) == General.UCase("GC"))//'=== GESTIONNAIRE CLIENT => Gestionnaire.
                {

                    sSQL = "SELECT     Immeuble.CodeImmeuble, Gestionnaire.CodeGestinnaire FROM  Gestionnaire INNER JOIN "
                        + " Immeuble ON Gestionnaire.Code1 = Immeuble.Code1 AND Gestionnaire.CodeGestinnaire = Immeuble.CodeGestionnaire  "
                           + " WHERE  Gestionnaire.ID_LOG = " + LID;


                    rs = rsAdo.fc_OpenRecordSet(sSQL);
                    if (rs.Rows.Count > 0)
                    {
                        sCodeimmeuble = rs.Rows[0]["CodeImmeuble"] + "";
                        sCodeParticulier = rs.Rows[0]["CodeGestinnaire"] + "";
                        rs.Dispose();
                        rs = null;
                    }
                    else
                    {
                        sSQL = "SELECT     Gestionnaire.Code1, Gestionnaire.CodeGestinnaire, Table1.Adresse1, Table1.CodePostal, Table1.Ville"
                            + " FROM         Gestionnaire INNER JOIN Table1 ON Gestionnaire.Code1 = Table1.Code1"
                            + " WHERE     Gestionnaire.ID_LOG = " + LID;

                        rs = rsAdo.fc_OpenRecordSet(sSQL);
                        if (rs.Rows.Count > 0)
                        {
                            txtCode1.Text = rs.Rows[0]["Code1"] + "";
                            txtAdresseClient.Text = rs.Rows[0]["Adresse1"] + "";
                            txtCPclient.Text = rs.Rows[0]["CodePostal"] + "";
                            txtVilleClient.Text = rs.Rows[0]["Ville"] + "";
                            rs.Dispose();
                            rs = null;
                        }
                        rs?.Dispose();
                        rs = null;
                        return;
                    }

                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Immeuble non identifié.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                sSQL = "SELECT     Immeuble.CodeImmeuble, Immeuble.Adresse, Immeuble.Adresse2_Imm, Immeuble.AngleRue, Immeuble.CodePostal, Immeuble.Ville, Table1.Code1, Table1.Nom, "
                    + " Table1.Adresse1, Table1.Adresse2, Table1.CodePostal AS CPclient, Table1.Ville AS VilleClient, Gestionnaire.NomGestion, Gestionnaire.CodeGestinnaire, Qualification.CodeQualif, "
                    + " Qualification.Qualification "
                    + " FROM         Immeuble INNER JOIN "
                    + " Table1 ON Immeuble.Code1 = Table1.Code1 LEFT OUTER JOIN "
                    + " Gestionnaire ON Immeuble.Code1 = Gestionnaire.Code1 AND Immeuble.CodeGestionnaire = Gestionnaire.CodeGestinnaire LEFT OUTER JOIN "
                    + " Qualification ON Gestionnaire.CodeQualif_Qua = Qualification.CodeQualif "
                    + " WHERE     (Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeimmeuble) + "')";

                rs = rsAdo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count == 0)
                {
                    rs.Dispose();
                    rs = null;
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Immeuble non identifié.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                txtcodeimmeuble.Text = sCodeimmeuble;
                txtAdresse.Text = rs.Rows[0]["Adresse"] + "";
                txtAdresse2_Imm.Text = rs.Rows[0]["Adresse2_Imm"] + "";
                txtAngleRue.Text = rs.Rows[0]["AngleRue"] + "";
                txtCodePostal.Text = rs.Rows[0]["CodePostal"] + "";
                txtVille.Text = rs.Rows[0]["Ville"] + "";

                if (General.UCase(General.nz(rs.Rows[0]["CodeQualif"], "").ToString()) == General.UCase(General.cPart))
                {
                    fraParticulier.Visible = true;
                    txtCodeParticulier.Text = rs.Rows[0]["CodeGestinnaire"] + "";
                    txtNom.Text = rs.Rows[0]["Nom"] + "";
                    dbSolde = ModFinance.fc_Solde(sCodeimmeuble, "");
                    lblParticulier.Text = "Particulier - solde " + dbSolde;
                }

                txtCode1.Text = rs.Rows[0]["Code1"] + "";
                txtAdresseClient.Text = rs.Rows[0]["Adresse1"] + "";
                txtCPclient.Text = rs.Rows[0]["CodePostal"] + "";
                txtVilleClient.Text = rs.Rows[0]["Ville"] + "";
                txtGestionnaire.Text = rs.Rows[0]["CodeGestinnaire"] + "";

                fc_CptRendu(txtcodeimmeuble.Text);
                txtcontrat.Text = ModContrat.fc_RechercheContrat(txtcodeimmeuble.Text, ref bWithContrat);
                if (bWithContrat)
                {
                    txtcontrat.ForeColor = Color.Blue;
                }
                else
                {
                    txtcontrat.ForeColor = Color.Red;
                }
                rs.Dispose();
                rs = null;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadData");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeimmeuble"></param>
        /// <returns></returns>
        private string fc_CptRendu(string sCodeimmeuble)
        {

            string sSQL = "SELECT CodeImmeuble From TechReleve WHERE CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeimmeuble) + "'";
            using (var tmp = new ModAdo())
            {
                sSQL = tmp.fc_ADOlibelle(sSQL);
            }
            if (!string.IsNullOrEmpty(sSQL))
            {
                btnCpteRendu.BackColor = Color.PaleTurquoise;
            }
            else
            {
                btnCpteRendu.BackColor = Color.PowderBlue;
            }
            return sSQL;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void btnCpteRendu_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(fc_CptRendu(txtcodeimmeuble.Text)))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'existe pas de relevé technique pour cet immeuble.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            General.saveInReg(Variable.cUserDocRelTech, "NewVar", txtcodeimmeuble.Text);
            General.saveInReg(Variable.cUserDocRelTech, "Origine", this.Name);
            View.Theme.Theme.Navigate(typeof(UserDocRelTech));
        }
        private void fc_clean()
        {
            txtcodeimmeuble.Text = "";
            txtAdresse.Text = "";
            txtAdresse2_Imm.Text = "";
            txtAngleRue.Text = "";
            txtCodePostal.Text = "";
            txtVille.Text = "";

            txtCode1.Text = "";
            txtAdresseClient.Text = "";
            txtCPclient.Text = "";
            txtVilleClient.Text = "";

            txtCodeParticulier.Text = "";
            txtNom.Text = "";
            txtGestionnaire.Text = "";
            lblParticulier.Text = "";
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdIntervenant_Click(object sender, EventArgs e)
        {
            try
            {


                string requete = "SELECT CodeImmeuble as \"Code Immeuble \"," + " Adresse as \"Adresse\"," + " Ville as \"Ville\"," +
                    " anglerue  as \"Angle de rue\"," + " Code1 as \"Gerant\" FROM Immeuble ";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un immeuble" };
                fg.SetValues(new Dictionary<string, string> { { "Code1", txtCode1.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        // charge les enregistrements de l'immeuble
                        txtcodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                        fg.Dispose();
                        fg.Close();
                    };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtcodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }

            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervenant_KeyPress");
                throw;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command2_Click(object sender, EventArgs e)
        {
            if (fc_ControlDataInter() == false)//Tested
            {
                return;
            }
            General.NomImmeuble = txtcodeimmeuble.Text;
            General.sNoInterHistorique = General.cOkToNavigue;
            frmHistorique frmH = new frmHistorique();
            frmH.ShowDialog();
            if (General.sNoInterHistorique != "" && General.sNoInterHistorique != General.cOkToNavigue)
            {
                ModParametre.fc_SaveParamPosition(Variable.cUserIntervention, this.Name, General.sNoInterHistorique);
                View.Theme.Theme.Navigate(typeof(UserIntervention));
            }
            General.sNoInterHistorique = "";
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool fc_ControlDataInter()
        {
            bool returnFonction = true;
            if (string.IsNullOrEmpty(txtcodeimmeuble.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un immeuble.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                returnFonction = false;
                return returnFonction;
            }
            return returnFonction;
        }
        private void Command3_Click(object sender, EventArgs e)
        {
            if (ListView1.ActiveNode == null)
                return;

            int i = 0;
            bool bexist = false;
            string stel = "";
            string[] sTab;
            for (i = 0; i < ListView1.Nodes.Count; i++)
            {
                if (General.UCase(ListView1.ActiveNode.Text) == General.UCase(ListView1.Nodes[i].Text))
                {
                    //sTab = Split(ListView1.ListItems(i).Key, "@@@")
                    string[] stringSeparators = new string[] { "@@@" };
                    sTab = ListView1.Nodes[i].Key.Split(stringSeparators, StringSplitOptions.None);
                    bexist = true;
                }
            }

            if (bexist == false)//tested
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un appel.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
            }
            else
            {
                frmAddCorrespandantImm frm = new frmAddCorrespandantImm();
                frm.txtCodeimmeuble.Text = txtcodeimmeuble.Text;
                frm.txtTel_IMC.Text = stel;
                frm.ShowDialog();
            }

        }

        private void Command4_Click(object sender, EventArgs e)
        {
            //ListView1.Nodes.Add("A", "A");
            //ListView1.Nodes.Add("B", "B");
            //ListView1.Nodes.Add("C", "C");



            //return;

            //var strdata = "";
            //var sAddlog = "";

            //fc_ReadData(strdata, ref sAddlog);

            fc_alerte();

            //fc_LoadData("GC3095");

            //fc_LoadData(cId_Inconnu);
        }

        private void UserDocGestionAppel_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
            {
                General.bLoadGestionAppel = false;
                return;
            }

            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocGestionAppel");
            General.bLoadGestionAppel = true;
        }

        private void UserDocGestionAppel_Load(object sender, EventArgs e)
        {
            try
            {
                ChkGestionAppel.Checked = General.getFrmReg(Variable.cUserDocGestionAppel, "ActiveGestionAppel", "0") == "1";

                fc_GestionAppel();

                General.bLoadGestionAppel = true;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        public void fc_LoadGestionAppel()
        {
            try
            {
                //fc_LoadData "GC3822"
                //Exit Sub
                //Set ListView1.ColumnHeaderIcons = ImmTrew
                //ListView1.View = lvwReport
                // ListView1.ColumnHeaders.Add , , "APPELS ENTRANTS", ListView1.Width - 100
                //ListView1.LabelEdit = lvwManual

                ListView1.Nodes.Clear();
                //ListView1.HeaderStyle = ColumnHeaderStyle.Nonclickable;
                ListView1.Nodes.Add("APPELS ENTRANTS");
                //ListView1.LabelEdit = true;
                //ListView1.MultiSelect = true;
                //ListView1.HideSelection = false;
                //sPathAlgoria = "C:\temp\algroria"
                //sPathAlgoria = "\\dt-fic-01\d$\AXECIEL\PACKAGES\INTRANET\Winsock"
                ModAppel.fc_LogTWS();
                //'fc_AddLog "fc_lok = ok"
                //'fc_loadTreev
                if (General.fncUserName().ToLower().Contains("administrateur") || General.fncUserName().ToLower().Contains("axeciel-user") || General.UCase(General.fncUserName()) == General.UCase("admalgoria") && DateTime.Now == Convert.ToDateTime("16/06/2020"))
                {
                    //fc_initAlgoria("w.ramidi");
                    fc_initAlgoria("c.tanfin");
                }
                else
                {
                    //MsgBox "init"
                    fc_initAlgoria(General.fncUserName());
                    //MsgBox "fin init"
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadGestionAppel");
            }
        }

        private void cmdActiverGestionAppel_Click(object sender, EventArgs e)
        {
            if (ChkGestionAppel.Checked)
            {
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous désactiver la gestion des appels ?", "Gestion des appels", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }

                ChkGestionAppel.Checked = false;
            }
            else
            {

                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous activer la gestion des appels ?", "Gestion des appels", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
                ChkGestionAppel.Checked = true;
            }
            fc_GestionAppel();
        }
        private void fc_GestionAppel()
        {
            if (ChkGestionAppel.Checked)
            {
                cmdActiverGestionAppel.Text = "Gestion des appels activées";
                General.saveInReg(Variable.cUserDocGestionAppel, "ActiveGestionAppel", "1");
                cmdActiverGestionAppel.BackColor = Color.PaleTurquoise;
                if (General.UCase(General.fncUserName()) != "RACHID ABBOUCHI")
                {
                    fc_LoadGestionAppel();
                }
                General.bActiveAppelAlgoria = true;
            }
            else
            {
                cmdActiverGestionAppel.Text = "Gestion des appels désactivées";
                General.saveInReg(Variable.cUserDocGestionAppel, "ActiveGestionAppel", "0");
                cmdActiverGestionAppel.BackColor = Color.LightPink;
                timer1.Enabled = false;
                General.bActiveAppelAlgoria = false;
            }
        }
        private void lblGerant_Click(object sender, EventArgs e)
        {
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, txtCode1.Text);
            View.Theme.Theme.Navigate(typeof(UserDocClient));
        }

        private void lblGoImmeuble_Click(object sender, EventArgs e)
        {
            ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocImmeuble, txtcodeimmeuble.Text);
            View.Theme.Theme.Navigate(typeof(UserDocImmeuble));
        }
        private void ListView1_AfterSelect(object sender, Infragistics.Win.UltraWinTree.SelectEventArgs e)
        {
            if (ListView1.ActiveNode == null)
                return;

            int i = 0;
            string[] sTab;
            //'For lnb = 1 To ListView1.ListItems.Count
            // '        ListView1.ListItems(lnb).Selected
            // '        fc_LoadData sID
            // 'Next

            for (i = 1; i < ListView1.Nodes.Count; i++)
            {
                if (General.UCase(ListView1.ActiveNode.Text) == General.UCase(ListView1.Nodes[i].Text))
                {
                    string[] stringSeparators = new string[] { "@@@" };
                    sTab = ListView1.Nodes[i].Key.Split(stringSeparators, StringSplitOptions.None);
                    fc_LoadData(sTab[1]);
                    break;
                }
            }
            //'For i = 1 To ListView1.ListItems.Count
            //'
            //'    If UCase(ListView1.SelectedItem) = UCase(ListView1.ListItems(i).Text) Then
            //'
            //'        fc_LoadData ListView1.ListItems(i).Key
            //'        Exit Sub
            //'    End If
            //'Next
        }

        /// <summary>
        /// Tested le 29.30.2020 par Mondir
        /// </summary>
        /// <param name="sUser"></param>
        private void fc_initAlgoria(string sUser)
        {
            try
            {
                XmlDocument oDvpDOMDocument = new XmlDocument();  // ' As MSXML2.IXMLDOMDocument
                XmlElement oNoeudMembre;   //' As MSXML2.IXMLDOMElement
                XmlNodeList oNoeudList;       // ' As MSXML2.IXMLDOMNodeList
                HttpWebRequest xhrRequest;      //  ' As XMLHTTP60
                string sQuery;
                string stemp;

                //HttpClient xhrRequest = new HttpClient();
                // '=== demande de token.
                sQuery = "http://azdt-cti-01:8000/tws/TWS_UserWebSvc/TWS_UserWebSvc.svc/xml/TWS_GetMyTokenAdmin?apiKey=" + ModAppel.cApiKey +
                    "&adminUsername=" + ModAppel.cAdminTWS +
                    "&adminPassword=" + ModAppel.cadminPSW +
                    "&username=" + sUser;
                //xhrRequest.GetAsync(sQuery);

                xhrRequest = (HttpWebRequest)HttpWebRequest.Create(sQuery);
                xhrRequest.Method = "GET";
                HttpWebResponse response = (HttpWebResponse)xhrRequest.GetResponse();

                if (response.GetResponseStream() != null)
                {
                    var encoding = ASCIIEncoding.ASCII;
                    using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encoding))
                    {
                        string responseText = reader.ReadToEnd();
                        if (!string.IsNullOrEmpty(responseText))
                        {
                            oDvpDOMDocument.LoadXml(responseText);
                            ModAppel.fc_AddLog("Document XML de la demande du token correctement chargé.");
                        }
                        else
                        {
                            ModAppel.fc_AddLog("Erreur de lecture du document XML de la demande du token.");
                        }
                    }
                }

                // 'If oDvpDOMDocument.Load("C:\DATA\Donnee\LONG\xml\test.xml") Then
                //'If oDvpDOMDocument.Load("C:/DATA/Donnee/LONG/xml/0079-CallEvent-09.46.47.716.xml") Then
                oNoeudList = oDvpDOMDocument.GetElementsByTagName(ModAppel.Ctoken);
                ModAppel.sTokenAlgoria = oNoeudList.Item(0).InnerText;
                stemp = "Token : " + ModAppel.sTokenAlgoria;
                ModAppel.fc_AddLog(stemp);
                oNoeudList = null;

                //oNoeudList = oDvpDOMDocument.GetElementsByTagName(ModAppel.cUser);
                //ModAppel.sTokenAlgoria = oNoeudList.Item(0).InnerText;
                //stemp = "Utilisateur  : " + ModAppel.sUserAlogria;
                //ModAppel.fc_AddLog(stemp);
                //oNoeudList = null;

                oNoeudList = oDvpDOMDocument.GetElementsByTagName(ModAppel.cDevice);
                ModAppel.sDeviceAlogoria = oNoeudList.Item(0).InnerText;
                stemp = "Device : " + ModAppel.sDeviceAlogoria;
                ModAppel.fc_AddLog(stemp);
                oNoeudList = null;

                //'MsgBox "001"
                oDvpDOMDocument = null;
                xhrRequest = null;
                response = null;
                //'=== demarre le monitor.
                sQuery = "http://azdt-cti-01:8000/tws/TWS_ToolkitWebSvc/TWS_ToolkitWebSvc.svc/xml/TWS_WebStartMonitor?tokenGuid=" + ModAppel.sTokenAlgoria
                         + "&szApp=TWS-CAL&szDevice=" + ModAppel.sDeviceAlogoria;



                //'sQuery = "http://azdt-cti-01:8000/tws/TWS_ToolkitWebSvc/TWS_ToolkitWebSvc.svc/xml/TWS_WebStartMonitor?tokenGuid=" & sTokenAlgoria _
                // "&szApp=TWS-TLK&szDevice=" & sDeviceAlogoria
                //'MsgBox "01"


                //'MsgBox "02"

                //'MsgBox "03"
                xhrRequest = (HttpWebRequest)HttpWebRequest.Create(sQuery);
                response = (HttpWebResponse)xhrRequest.GetResponse();
                oDvpDOMDocument = new XmlDocument();
                if (response.GetResponseStream() != null)
                {
                    var encoding = ASCIIEncoding.ASCII;
                    using (var reader = new System.IO.StreamReader(response.GetResponseStream(), encoding))
                    {
                        string responseText = reader.ReadToEnd();
                        if (!string.IsNullOrEmpty(responseText))
                        {
                            oDvpDOMDocument.LoadXml(responseText);
                            ModAppel.fc_AddLog("Document XML du moniteur correctement chargé.");
                        }
                        else
                        {
                            ModAppel.fc_AddLog("Erreur de lecture du document XML de la demande du  moniteur.");
                        }
                    }
                }

                oNoeudList = oDvpDOMDocument.GetElementsByTagName(ModAppel.cResultMonitor);
                stemp = oNoeudList.Item(0).InnerText;
                if (stemp == "0")
                {
                    ModAppel.fc_AddLog("Démarrage du moniteur ok." + "\n");
                }
                else
                {
                    ModAppel.fc_AddLog("Le moniteur n' pas démarrée  correctement.");
                }

                oNoeudList = null;
                //'=== recupére les infos sur le device
                //'sQuery = "http://azdt-cti-01:8000/tws/TWS_ToolkitWebSvc/TWS_ToolkitWebSvc.svc/XML/TWS_GetDevicesState?domainGuid={DOMAINGUID}&protocol={PROTOCOL}&firstNumber={FIRSTNUMBER}&lastNumber={LASTNUMBER}"
                // '==== Lance la socket TWS_WaitForGenericEvent.
                //'sQuery = "<TWS_WaitForGenericEvent><tokenGuid>" & sTokenAlgoria & "</tokenGuid></TWS_WaitForGenericEvent>82E51812-45C9-4733"

                //'sMessageAlgoria = "<TWS_WaitForGenericEvent><tokenGuid>" & sTokenAlgoria & "</tokenGuid></TWS_WaitForGenericEvent>82E51812-45C9-4733"
                Winsock2.Close();

                //'MsgBox "4"

                Winsock2.LocalPort = 0;

                // 'MsgBox "5"

                //'=== déclenche l'événement connect du controle winsock.

                Array.Resize(ref ModAppel.sMessageAlgoria, 2);

                ModAppel.sMessageAlgoria[0] = "<TWS_WaitForGenericEvent><tokenGuid>" + ModAppel.sTokenAlgoria + "</tokenGuid></TWS_WaitForGenericEvent>82E51812-45C9-4733" + Convert.ToInt32(0); //'+ cMesAudit
                ModAppel.sMessageAlgoria[1] = ModAppel.cMesAudit + Convert.ToInt32(0);
                //'MsgBox "6"

                Winsock2.Connect(ModAppel.cServAlgoria, ModAppel.cPortAlgoria);

                //'MsgBox "7"
                ModAppel.fc_AddLog("winsock statut = "  /*+Winsock2.State  TODO*/);

                //'MsgBox "8"
                timer1.Interval = 5000;

                timer1.Enabled = true;
                //'   MsgBox "timer ok"
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_initAlgoria");
            }
        }

        private object sMessageAlgoria(int v)
        {
            throw new NotImplementedException();
        }

        private void timer1_Tick(object sender, EventArgs e)
        {
            try
            {
                Array.Resize(ref ModAppel.sMessageAlgoria, 1);
                ModAppel.sMessageAlgoria[0] = ModAppel.cMesAudit + Convert.ToInt32(0);
                //'MsgBox "debut timer"
                Winsock2.SendData(ModAppel.sMessageAlgoria[0]);

                //'MsgBox "fin timer"
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                ModAppel.fc_AddLog(this.Name + ";===ERROR===Timer1_Timer --- " + ex.Message);
            }

        }

        public string fc_ReadData(string strdata, ref string sAddlog)
        {

            string stemp;

            XmlDocument oDvpDOMDocument = new XmlDocument();
            XmlNodeList oNoeudList;
            XmlElement objSub;

            bool bLoadXML;
            bool bIn;
            string sPhoneInfo;
            string sM_callLogId = "";
            string sNoTel = "";
            string sdate = "";
            string sHour = "";
            string sLibStatut = "";
            string sGuid;
            string sCompany;
            int i;
            int j;
            int lnb;
            int lIcone = 0;
            //'  xhrRequest          As XMLHTTP60
            string sDirectoriesNames;
            string sm_eventReason;
            string sFirstname;
            string sLastname;
            string sm_UserDisplay;
            string sm_UserInfoDisplay = "";
            string sm_publicUserNumber;
            string sm_connectionId;
            string[] sTab;
            string sKey;
            string sm_currentState;
            string sidContact = "";



            try
            {
                bLoadXML = false;
                //'===permet de charger entièrement le document en mémoire avant le traitement
                if (!string.IsNullOrEmpty(strdata))
                {
                    oDvpDOMDocument.LoadXml(strdata);
                    //'If oDvpDOMDocument.Load("C:\temp\XML\v7.xml") Then
                    //'If oDvpDOMDocument.Load("C:\DATA\Donnee\LONG\xml\sequence\1.xml") Then

                    // '=== données XML correctement chargé.

                    sAddlog = sAddlog + "fc_ReadData, loadxml = ok " + "\n";
                    bLoadXML = true;
                }
                else
                {
                    sAddlog = sAddlog + "fc_ReadData, loadxml = failed " + "\n";
                    bLoadXML = false;
                }
                if (bLoadXML == false)
                {
                    return "";
                }

                if (General._ExecutionMode == General.ExecutionMode.Dev)
                    oDvpDOMDocument.Load(@"C:\Users\mondir\Documents\test3.xml");

                // create ns manager
                XmlNamespaceManager xmlnsManager = new XmlNamespaceManager(oDvpDOMDocument.NameTable);
                xmlnsManager.AddNamespace("jtr", "http://schemas.datacontract.org/2004/07/TWS_Database");
                xmlnsManager.AddNamespace("jtr2", "http://schemas.datacontract.org/2004/07/Algoria.TWS.GenericClass");


                //=== controle si il s'agit d'un appel entrant.
                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericEvent//jtr2:CGenericConnection//jtr2:m_connectionDirection", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    return "";
                }
                else
                {
                    stemp = oNoeudList.Item(0).InnerText;
                    if (General.UCase(stemp) != General.UCase(ModAppel.cAppelEntrant))
                    {
                        //'=== donnée non entrante.
                        return "";
                    }

                }
                sAddlog = sAddlog + "-----------------------DEBUT-------------------------------" + "\n";
                //'=== controle si il s'agit d'un appel.
                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:genericCallEvent//jtr2:CGenericCCo//jtr2:m_ccoType", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "m_ccoType : failed" + "\n";
                    return "";
                }
                else
                {
                    stemp = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "m_ccoType : failed" + stemp + "\n";

                    if (General.UCase(stemp) != General.UCase(ModAppel.cSimpleAppel))
                    {
                        // '=== donnée ne correspond pas à un appel.
                        // return "";
                    }

                }

                //'=== récupére le statut.
                //'Set oNoeudList = oDvpDOMDocument.selectNodes("//CGenericCCo//m_ccoState")
                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericConnection//jtr2:m_currentState", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "m_currentState failed" + "\n";
                    return "";
                }
                else
                {
                    stemp = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "m_currentState = " + stemp + "\n";
                    sm_currentState = stemp;
                    if (General.UCase(stemp) != General.UCase(ModAppel.cStatutDelivered))
                    {
                        // '=== donnée ne correspond pas à un appel.
                        // return "";
                    }

                }

                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericCCo//jtr:Company", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "PersonGuid Company" + "\n";
                    //return "";
                }
                else
                {
                    sCompany = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "Company = " + sCompany + "\n";

                    if (General.UCase(sCompany) == "")
                    {
                        sCompany = "company Inconnu";
                        //'=== donnée ne correspond pas à un appel.
                        //''''Exit Function
                    }

                }

                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericCCo//jtr:Firstname", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "Firstname failed" + "\n";
                    //return "";
                }
                else
                {
                    sFirstname = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "Firstname =  " + sFirstname + "\n";

                    if (General.UCase(sFirstname) == "")
                    {
                        sFirstname = "";// '"firstname Inconnu"
                        //'=== donnée ne correspond pas à un appel.
                        //''''Exit Function
                    }

                }

                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericCCo//jtr:Lastname", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "Lastname failed" + "\n";
                    //return "";
                }
                else
                {
                    sLastname = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "Lastname =  " + sLastname + "\n";

                    if (General.UCase(sLastname) == "")
                    {
                        sLastname = "";// '""Lastname Inconnu""
                        //'=== donnée ne correspond pas à un appel.
                        //''''Exit Function
                    }

                }
                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericCCo//jtr:PersonGuid", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "PersonGuid failed" + "\n";
                    //return "";
                }
                else
                {
                    sGuid = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "PersonGuid =  " + sGuid + "\n";

                    if (General.UCase(sGuid) == "")
                    {
                        sGuid = "ID Inconnu";
                        //'=== donnée ne correspond pas à un appel.
                        //''''Exit Function
                    }

                }
                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericCCo//jtr:Phones//jtr:CContactInfo//jtr:Key", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "key  failed" + "\n";
                    //return "";
                }
                else
                {
                    sKey = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "key  =  " + sKey + "\n";

                    if (General.UCase(sKey) == "")
                    {
                        //sKey = "ID Inconnu";
                        //'=== donnée ne correspond pas à un appel.
                        //''''Exit Function
                    }

                }
                //'=== récupére le nom dictionnaire (groupe contact).
                //'Set oNoeudList = oDvpDOMDocument.selectNodes("//CGenericCCo//m_ccoState")

                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericCCo//jtr:DirectoriesNames", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "DirectoriesNames  failed" + "\n";
                    //return "";
                }
                else
                {
                    sDirectoriesNames = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "key  =  " + sDirectoriesNames + "\n";

                    if (General.UCase(sDirectoriesNames) == "")
                    {
                        sGuid = "DirectoriesNames Inconnu";
                        //'=== donnée ne correspond pas à un appel.

                    }

                }
                //'Set oNoeudList = oDvpDOMDocument.selectNodes("//CGenericCCo//m_ccoState")
                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericCCo//jtr2:m_eventReason", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "m_eventReason failed" + "\n";
                    //return "";
                }
                else
                {
                    sm_eventReason = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "key  =  " + sm_eventReason + "\n";

                    if (General.UCase(sm_eventReason) == "")
                    {
                        sm_eventReason = "personne Inconnu";
                        //'=== donnée ne correspond pas à un appel.

                    }

                }
                //'Set oNoeudList = oDvpDOMDocument.selectNodes("//CGenericCCo//m_Users//CGenericUser//CContactInfo//Customs//Label")
                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr:Customs//jtr:Label", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "ID AXECIEL  failed" + "\n";
                    //return "";
                }
                else
                {
                    stemp = oNoeudList.Item(0).InnerText;
                    if (General.UCase(stemp) == General.UCase(cIDAxeciel))
                    {
                        oNoeudList = oDvpDOMDocument.SelectNodes("//jtr:Customs//jtr:Value", xmlnsManager);
                        if (oNoeudList.Count == 0)
                        {
                            sAddlog = sAddlog + "ID AXECIEL error " + "\n";
                        }
                        else
                        {
                            sidContact = oNoeudList.Item(0).InnerText;
                            sAddlog = sAddlog + "ID AXECIEL = " + sidContact + "\n";
                        }
                        if (General.UCase(sidContact) == "")
                        {
                            sAddlog = sAddlog + "ID AXECIEL EN ERREUR = " + sidContact + "\n";
                        }
                    }
                    else
                    {
                        sAddlog = sAddlog + "ID AXECIEL = NO " + "\n";
                    }


                    //if (General.UCase(stemp) != General.UCase(ModAppel.cStatutDelivered))
                    //{
                    //    oNoeudList = oDvpDOMDocument.SelectNodes("//jtr:Customs//jtr:Value", xmlnsManager);
                    //    if (oNoeudList.Count == 0)
                    //    {
                    //        sAddlog = sAddlog + "ID AXECIEL error " + "\n";
                    //    }
                    //    else
                    //    {
                    //        sidContact = oNoeudList.Item(0).InnerText;
                    //        sAddlog = sAddlog + "ID AXECIEL = " + sidContact + "\n";
                    //    }
                    //    if (General.UCase(sidContact) == "")
                    //    {
                    //        sAddlog = sAddlog + "ID AXECIEL EN ERREUR = " + sidContact + "\n";
                    //    }
                    //}
                    //else
                    //{
                    //    sAddlog = sAddlog + "ID AXECIEL = NO " + "\n";
                    //}

                }
                if (General.UCase(sm_currentState) == General.UCase(ModAppel.cidle))
                {
                    sLibStatut = ModAppel.cidle;
                    lIcone = 1;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.cinitiated))
                {
                    sLibStatut = ModAppel.cinitiated;
                    lIcone = 1;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.coriginated))
                {
                    sLibStatut = ModAppel.coriginated;
                    lIcone = 1;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.cdelivered))
                {
                    sLibStatut = ModAppel.cdelivered;
                    lIcone = 2;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.cestablished))
                {
                    sLibStatut = ModAppel.cestablished;
                    lIcone = 2;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.ccleared))
                {
                    sLibStatut = ModAppel.ccleared;
                    lIcone = 4;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.cheld))
                {
                    sLibStatut = ModAppel.cheld;
                    lIcone = 1;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.cconferenced))
                {
                    sLibStatut = ModAppel.cconferenced;
                    lIcone = 1;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.ctransfered))
                {
                    sLibStatut = ModAppel.ctransfered;
                    lIcone = 1;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.cdiverted))
                {
                    sLibStatut = ModAppel.cdiverted;
                    lIcone = 1;

                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.cqueued))
                {
                    sLibStatut = ModAppel.cqueued;
                    lIcone = 1;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.cretrieved))
                {
                    sLibStatut = ModAppel.cretrieved;
                    lIcone = 1;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.cblocked))
                {
                    sLibStatut = ModAppel.cblocked;
                    lIcone = 1;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.cpredelivered))
                {
                    sLibStatut = ModAppel.cpredelivered;
                    lIcone = 1;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.cdisconnected))
                {
                    sLibStatut = ModAppel.cdisconnected;
                    lIcone = 0;

                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.cnotified))
                {
                    sLibStatut = ModAppel.cnotified;
                    lIcone = 1;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.cfailed))
                {
                    sLibStatut = ModAppel.cfailed;
                    lIcone = 4;
                }
                else if (General.UCase(sm_currentState) == General.UCase(ModAppel.ccAll))
                {
                    sLibStatut = ModAppel.ccAll;
                    lIcone = 1;
                }
                //                '=== récupére le numéro de tel le 22 02 2 019.
                //'Set oNoeudList = oDvpDOMDocument.selectNodes("//CGenericCCo//m_publicUserNumber")
                //'If oNoeudList.Length = 0 Then
                //'         sAddlog = sAddlog & "m_publicUserNumber failed" & vbCrLf
                //'      '  Exit Function
                //'Else
                //'       ' sNoTel = oNoeudList.Item(0).Text
                //'         sAddlog = sAddlog & "m_publicUserNumber = " & sNoTel & vbCrLf
                //'        If UCase(sNoTel) = "" Then
                //'            ' === pas de numéro de tel, voir comment gérer cette erreur.
                //'
                //'        End If
                //'End If

                //'=== recupére le nom du contact affiché.

                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericCCo//jtr2:m_Users//jtr2:m_UserInfoDisplay", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "m_UserInfoDisplay  failed" + "\n";
                    //return "";
                }
                else
                {
                    sm_UserInfoDisplay = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "m_UserInfoDisplay =  " + sm_UserInfoDisplay + "\n";

                    if (General.UCase(sm_UserInfoDisplay) == "")
                    {

                    }

                }
                //                '=== retourne le numéro de l'appeleant.
                //'=== recupére le nom du contact affiché.
                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericCCo//jtr2:m_Users//jtr2:m_publicUserNumber", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "m_publicUserNumber  failed" + "\n";
                    //return "";
                }
                else
                {
                    sm_publicUserNumber = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "m_UserInfoDisplay =  " + sm_publicUserNumber + "\n";

                    if (General.UCase(sm_publicUserNumber) == "")
                    {
                        //  '=== pas de nom pour le numéro de tel, voir comment gérer cette erreur.
                    }

                }

                //'=== recupére le nom du contact affiché.
                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericCCo//jtr2:m_Users//jtr2:m_UserDisplay", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "m_UserDisplay  failed" + "\n";
                    //return "";
                }
                else
                {
                    sm_UserDisplay = oNoeudList.Item(0).InnerText;
                    sNoTel = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "m_UserDisplay =  " + sm_UserDisplay + "\n";

                    if (General.UCase(sm_UserDisplay) == "")
                    {

                    }

                }
                //'=== recupére le nom du contact affiché.
                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericCCo//jtr2:m_connectionId", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "m_connectionId   failed" + "\n";
                    //return "";
                }
                else
                {
                    sm_connectionId = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "m_UserDisplay =  " + sm_connectionId + "\n";

                    if (General.UCase(sm_connectionId) == "")
                    {

                    }

                }
                //'=== récupére l'haure et la date d'appel.
                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericCCo//jtr2:m_Connections//jtr2:CGenericConnection//jtr2:m_timeStart", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "m_timeStart  failed" + "\n";
                    //return "";
                }
                else
                {
                    stemp = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "m_timeStart =  " + stemp + "\n";

                    if (General.UCase(stemp) == "")
                    {
                        // '=== pas de nom pour le numéro de tel, voir comment gérer cette erreur.
                    }
                    //'=== récupére la date.
                    var splitted = stemp.Split('T');
                    sdate = splitted[0];

                    //'=== récupéré l'heure.
                    splitted = splitted[1].Split('.');
                    var time = splitted[0];
                    sHour = time;
                }

                sdate = Convert.ToDateTime(sdate).ToString("dd/MM/yyyy");

                sPhoneInfo = sm_UserInfoDisplay + " - " + sdate + " - " + sHour;
                //'=== récupére l'id de l'appel
                oNoeudList = oDvpDOMDocument.SelectNodes("//jtr2:CGenericCCo//jtr2:m_callLogId", xmlnsManager);
                if (oNoeudList.Count == 0)
                {
                    sAddlog = sAddlog + "m_callLogId = null" + "\n";
                    //return "";
                }
                else
                {
                    sM_callLogId = oNoeudList.Item(0).InnerText;
                    sAddlog = sAddlog + "m_callLogId  = =  " + sM_callLogId + "\n";

                    if (General.UCase(sPhoneInfo) == "")
                    {

                    }

                }
                bIn = false;
                //'=== boucle sur le treeview si appel déjà existant, si vrai on mets à jour l'icone.
                for (lnb = 1; lnb < ListView1.Nodes.Count; lnb++)
                {
                    string[] stringSeparators = new string[] { "@@@" };
                    sTab = ListView1.Nodes[lnb].Key.Split(stringSeparators, StringSplitOptions.None);

                    if (General.UCase(sTab[0]) == General.UCase(sM_callLogId))
                    {
                        if (lIcone != 0)
                        {
                            //'=== si l'appel a été décroché, on ne met plus à jour l'icone.
                            if (General.UCase(sLibStatut) == General.UCase(ModAppel.cestablished))
                            {
                                ListView1.Nodes[lnb].Tag = ModAppel.cestablished;
                                ListView1.Nodes[lnb].LeftImages.Clear();
                                ListView1.Nodes[lnb].LeftImages.Add(imageList1.Images[$"{lIcone}"]);
                            }
                            else
                            {
                                if (General.UCase(ListView1.Nodes[lnb].Tag) != General.UCase(ModAppel.cestablished))
                                {
                                    ListView1.Nodes[lnb].LeftImages.Clear();
                                    ListView1.Nodes[lnb].LeftImages.Add(imageList1.Images[$"{lIcone}"]);
                                }
                            }
                        }
                        bIn = true;
                    }
                }
                //'=== Si cet appel est inexistant, on l'ajoute au treeview.
                if (bIn == false)
                {
                    if (lIcone != 0)
                    {
                        //  ListView1.ListItems.Add 1, sM_callLogId & "@@@" & nz(sidContact, cId_Inconnu) & "@@@" & sNoTel, sPhoneInfo, , lIcone
                        var node = new UltraTreeNode(sM_callLogId + "@@@" + General.nz(sidContact, cId_Inconnu) + "@@@" + sNoTel, sPhoneInfo);
                        node.LeftImages.Add(imageList1.Images[$"{lIcone}"]);
                        ListView1.Nodes.Add(node);
                    }
                    else
                    {
                        //'=== icone point d'interogation.
                        // ListView1.ListItems.Add 1, sM_callLogId & "@@@" & nz(sidContact, cId_Inconnu) & "@@@" & sNoTel, sPhoneInfo, , 1
                        ListView1.Nodes.Add(sM_callLogId + "@@@" + General.nz(sidContact, cId_Inconnu) + "@@@" + sNoTel, sPhoneInfo);
                    }

                    //===> Mondir le 07.05.2021, demandé pra Cindy chez LONG, Selected the last Cal
                    SelectLastCall();
                    //===> Fin Modif Mondir

                    //=== modif du 27 08 2019.
                    fc_alerte();
                }
                sAddlog = sAddlog + "APPEL: " + sPhoneInfo + "\n";

                return sPhoneInfo;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                ModAppel.fc_AddLog("fc_ReadData: " + ex.Message + "==>" + sM_callLogId + "@@@" + sidContact + "@@@" + sNoTel);
                return "";
            }

        }

        /// <summary>
        /// Mondir le 07.05.2021, demandé pra Cindy chez LONG, Selected the last Cal
        /// </summary>
        private void SelectLastCall()
        {
            //===> Mondir le 07.05.2021, demandé pra Cindy chez LONG, Selected the last Cal
            ListView1.ActiveNode = ListView1.Nodes[ListView1.Nodes.Count - 1];
            ListView1.ActiveNode.Selected = true;
            ListView1.Select();
            //===> Fin Modif Mondir
        }

        private void fc_alerte()
        {
            frmAlerteAppel frmAlerteAppel = null;
            try
            {
                if (!General.bLoadAlertAppel)
                {
                    General.bLoadAlertAppel = true;
                    frmAlerteAppel = new frmAlerteAppel();
                    frmAlerteAppel.Show();
                }
            }
            catch
            {
                General.bLoadAlertAppel = false;
                if (frmAlerteAppel != null)
                    frmAlerteAppel.Close();
            }
        }

        private void Winsock2_DataArrival(object sender, AxMSWinsockLib.DMSWinsockControlEvents_DataArrivalEvent e)
        {
            string strdata = "";
            string sAddlog;
            object data = "";
            string vbString = "8";//Const vbString = 8
            try
            {
                ModAppel.fc_AddLog("Winsock2_DataArrival");
                //Winsock2.GetData(ref data, strdata, vbString);
                Winsock2.GetData(ref data, 8, 10000000);
                sAddlog = "";
                strdata = data.ToString();
                fc_ReadData(strdata, ref sAddlog);
                if (sAddlog != "")
                {
                    ModAppel.fc_AddLog(sAddlog);
                }

                //===> Mondir le 07.05.2021, demandé pra Cindy chez LONG, Selected the last Cal
                SelectLastCall();
                //===> Fin Modif Mondir
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                ModAppel.fc_AddLog("Winsock2_DataArrival: " + ex.Message);
            }
        }

        private void Winsock2_Error(object sender, AxMSWinsockLib.DMSWinsockControlEvents_ErrorEvent e)
        {
            try
            {
                ModAppel.fc_AddLog("Winsock2_Error ==> " + "erreur : " + e.description + ", statut : " + Winsock2.OcxState + ".");
                e.cancelDisplay = true;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                ModAppel.fc_AddLog("Winsock2_Error ==> " + ex.Message);
            }
        }

        private void Winsock2_CloseEvent(object sender, EventArgs e)
        {
            try
            {
                Winsock2.Close();
                ModAppel.fc_AddLog("Winsock2.Close" + "\n");
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                ModAppel.fc_AddLog(this.Name + ";===ERROR===Winsock2_Close --- ==> " + ex.Message);
            }
        }

        private void Winsock2_ConnectEvent(object sender, EventArgs e)
        {
            try
            {
                int i = 0;

                for (i = 0; i < ModAppel.sMessageAlgoria.Length; i++)
                {
                    if (!string.IsNullOrEmpty(ModAppel.sMessageAlgoria[i]))
                    {
                        Winsock2.SendData(ModAppel.sMessageAlgoria[i]);
                        ModAppel.sMessageAlgoria[i] = "";
                    }

                }
                ModAppel.fc_AddLog("Winsock2_Connect, statut :" + Winsock2.OcxState);
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                ModAppel.fc_AddLog(this.Name + ";===ERROR===Winsock2_Close --- ==> " + ex.Message);
            }
        }
    }
}
