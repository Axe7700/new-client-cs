namespace Axe_interDT.Views.FicheGestionAppel
{
    partial class UserDocGestionAppel
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDocGestionAppel));
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.cmdCreateInter = new System.Windows.Forms.Button();
            this.cmdActiverGestionAppel = new System.Windows.Forms.Button();
            this.Command3 = new System.Windows.Forms.Button();
            this.Command2 = new System.Windows.Forms.Button();
            this.btnCpteRendu = new System.Windows.Forms.Button();
            this.Winsock2 = new AxMSWinsockLib.AxWinsock();
            this.Command4 = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.ChkGestionAppel = new System.Windows.Forms.CheckBox();
            this.fraParticulier = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.label7 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCodeParticulier = new iTalk.iTalk_TextBox_Small2();
            this.txtNom = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.txtsss = new iTalk.iTalk_RichTextBox();
            this.label8 = new System.Windows.Forms.Label();
            this.txtcontrat = new iTalk.iTalk_TextBox_Small2();
            this.label6 = new System.Windows.Forms.Label();
            this.label5 = new System.Windows.Forms.Label();
            this.txtGestionnaire = new iTalk.iTalk_TextBox_Small2();
            this.txtCPclient = new iTalk.iTalk_TextBox_Small2();
            this.txtAdresseClient = new iTalk.iTalk_TextBox_Small2();
            this.label = new System.Windows.Forms.Label();
            this.lblGerant = new System.Windows.Forms.Label();
            this.txtCode1 = new iTalk.iTalk_TextBox_Small2();
            this.txtCodePostal = new iTalk.iTalk_TextBox_Small2();
            this.txtAngleRue = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtAdresse2_Imm = new iTalk.iTalk_TextBox_Small2();
            this.txtAdresse = new iTalk.iTalk_TextBox_Small2();
            this.txtcodeimmeuble = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.lblGoImmeuble = new System.Windows.Forms.Label();
            this.txtVille = new iTalk.iTalk_TextBox_Small2();
            this.txtVilleClient = new iTalk.iTalk_TextBox_Small2();
            this.cmdIntervenant = new System.Windows.Forms.Button();
            this.lblParticulier = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.label9 = new System.Windows.Forms.Label();
            this.ListView1 = new Infragistics.Win.UltraWinTree.UltraTree();
            this.timer1 = new System.Windows.Forms.Timer(this.components);
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.imageList1 = new System.Windows.Forms.ImageList(this.components);
            this.flowLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Winsock2)).BeginInit();
            this.tableLayoutPanel1.SuspendLayout();
            this.Frame1.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            this.fraParticulier.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ListView1)).BeginInit();
            this.tableLayoutPanel5.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.cmdCreateInter);
            this.flowLayoutPanel1.Controls.Add(this.cmdActiverGestionAppel);
            this.flowLayoutPanel1.Controls.Add(this.Command3);
            this.flowLayoutPanel1.Controls.Add(this.Command2);
            this.flowLayoutPanel1.Controls.Add(this.btnCpteRendu);
            this.flowLayoutPanel1.Controls.Add(this.Winsock2);
            this.flowLayoutPanel1.Controls.Add(this.Command4);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(1850, 41);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // cmdCreateInter
            // 
            this.cmdCreateInter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdCreateInter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCreateInter.FlatAppearance.BorderSize = 0;
            this.cmdCreateInter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCreateInter.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCreateInter.ForeColor = System.Drawing.Color.White;
            this.cmdCreateInter.Location = new System.Drawing.Point(1703, 2);
            this.cmdCreateInter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdCreateInter.Name = "cmdCreateInter";
            this.cmdCreateInter.Size = new System.Drawing.Size(145, 35);
            this.cmdCreateInter.TabIndex = 581;
            this.cmdCreateInter.Text = "Crée une intervention";
            this.cmdCreateInter.UseVisualStyleBackColor = false;
            this.cmdCreateInter.Click += new System.EventHandler(this.cmdCreateInter_Click);
            // 
            // cmdActiverGestionAppel
            // 
            this.cmdActiverGestionAppel.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdActiverGestionAppel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdActiverGestionAppel.FlatAppearance.BorderSize = 0;
            this.cmdActiverGestionAppel.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdActiverGestionAppel.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdActiverGestionAppel.ForeColor = System.Drawing.Color.White;
            this.cmdActiverGestionAppel.Location = new System.Drawing.Point(1507, 2);
            this.cmdActiverGestionAppel.Margin = new System.Windows.Forms.Padding(2);
            this.cmdActiverGestionAppel.Name = "cmdActiverGestionAppel";
            this.cmdActiverGestionAppel.Size = new System.Drawing.Size(192, 35);
            this.cmdActiverGestionAppel.TabIndex = 579;
            this.cmdActiverGestionAppel.Text = "Activer la Gestion des appels";
            this.cmdActiverGestionAppel.UseVisualStyleBackColor = false;
            this.cmdActiverGestionAppel.Click += new System.EventHandler(this.cmdActiverGestionAppel_Click);
            // 
            // Command3
            // 
            this.Command3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command3.FlatAppearance.BorderSize = 0;
            this.Command3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command3.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command3.ForeColor = System.Drawing.Color.White;
            this.Command3.Location = new System.Drawing.Point(1343, 2);
            this.Command3.Margin = new System.Windows.Forms.Padding(2);
            this.Command3.Name = "Command3";
            this.Command3.Size = new System.Drawing.Size(160, 35);
            this.Command3.TabIndex = 580;
            this.Command3.Text = "Créer un correspondant";
            this.Command3.UseVisualStyleBackColor = false;
            this.Command3.Click += new System.EventHandler(this.Command3_Click);
            // 
            // Command2
            // 
            this.Command2.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command2.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command2.FlatAppearance.BorderSize = 0;
            this.Command2.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command2.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command2.ForeColor = System.Drawing.Color.White;
            this.Command2.Location = new System.Drawing.Point(1223, 2);
            this.Command2.Margin = new System.Windows.Forms.Padding(2);
            this.Command2.Name = "Command2";
            this.Command2.Size = new System.Drawing.Size(116, 35);
            this.Command2.TabIndex = 582;
            this.Command2.Text = "Historique";
            this.Command2.UseVisualStyleBackColor = false;
            this.Command2.Click += new System.EventHandler(this.Command2_Click);
            // 
            // btnCpteRendu
            // 
            this.btnCpteRendu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.btnCpteRendu.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.btnCpteRendu.FlatAppearance.BorderSize = 0;
            this.btnCpteRendu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnCpteRendu.Font = new System.Drawing.Font("Ubuntu", 9F);
            this.btnCpteRendu.ForeColor = System.Drawing.Color.White;
            this.btnCpteRendu.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.btnCpteRendu.Location = new System.Drawing.Point(1117, 2);
            this.btnCpteRendu.Margin = new System.Windows.Forms.Padding(2);
            this.btnCpteRendu.Name = "btnCpteRendu";
            this.btnCpteRendu.Size = new System.Drawing.Size(102, 35);
            this.btnCpteRendu.TabIndex = 584;
            this.btnCpteRendu.Tag = "3";
            this.btnCpteRendu.Text = "Compte rendu";
            this.btnCpteRendu.UseVisualStyleBackColor = false;
            this.btnCpteRendu.Click += new System.EventHandler(this.btnCpteRendu_Click);
            // 
            // Winsock2
            // 
            this.Winsock2.Enabled = true;
            this.Winsock2.Location = new System.Drawing.Point(1084, 3);
            this.Winsock2.Name = "Winsock2";
            this.Winsock2.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("Winsock2.OcxState")));
            this.Winsock2.Size = new System.Drawing.Size(28, 28);
            this.Winsock2.TabIndex = 585;
            this.Winsock2.Error += new AxMSWinsockLib.DMSWinsockControlEvents_ErrorEventHandler(this.Winsock2_Error);
            this.Winsock2.DataArrival += new AxMSWinsockLib.DMSWinsockControlEvents_DataArrivalEventHandler(this.Winsock2_DataArrival);
            this.Winsock2.ConnectEvent += new System.EventHandler(this.Winsock2_ConnectEvent);
            this.Winsock2.CloseEvent += new System.EventHandler(this.Winsock2_CloseEvent);
            // 
            // Command4
            // 
            this.Command4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command4.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command4.FlatAppearance.BorderSize = 0;
            this.Command4.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command4.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Command4.ForeColor = System.Drawing.Color.White;
            this.Command4.Location = new System.Drawing.Point(983, 2);
            this.Command4.Margin = new System.Windows.Forms.Padding(2);
            this.Command4.Name = "Command4";
            this.Command4.Size = new System.Drawing.Size(96, 35);
            this.Command4.TabIndex = 583;
            this.Command4.Text = "Command4";
            this.Command4.UseVisualStyleBackColor = false;
            this.Command4.Visible = false;
            this.Command4.Click += new System.EventHandler(this.Command4_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel1.Controls.Add(this.Frame1, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 44);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1844, 910);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.Transparent;
            this.Frame1.Controls.Add(this.tableLayoutPanel3);
            this.Frame1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Frame1.Location = new System.Drawing.Point(556, 3);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(1285, 904);
            this.Frame1.TabIndex = 411;
            this.Frame1.TabStop = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 4;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel3.Controls.Add(this.ChkGestionAppel, 0, 14);
            this.tableLayoutPanel3.Controls.Add(this.fraParticulier, 0, 13);
            this.tableLayoutPanel3.Controls.Add(this.label33, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.txtsss, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.label8, 0, 11);
            this.tableLayoutPanel3.Controls.Add(this.txtcontrat, 1, 11);
            this.tableLayoutPanel3.Controls.Add(this.label6, 0, 10);
            this.tableLayoutPanel3.Controls.Add(this.label5, 0, 9);
            this.tableLayoutPanel3.Controls.Add(this.txtGestionnaire, 1, 10);
            this.tableLayoutPanel3.Controls.Add(this.txtCPclient, 1, 9);
            this.tableLayoutPanel3.Controls.Add(this.txtAdresseClient, 1, 8);
            this.tableLayoutPanel3.Controls.Add(this.label, 0, 8);
            this.tableLayoutPanel3.Controls.Add(this.lblGerant, 0, 7);
            this.tableLayoutPanel3.Controls.Add(this.txtCode1, 1, 7);
            this.tableLayoutPanel3.Controls.Add(this.txtCodePostal, 1, 6);
            this.tableLayoutPanel3.Controls.Add(this.txtAngleRue, 1, 5);
            this.tableLayoutPanel3.Controls.Add(this.label3, 0, 6);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 5);
            this.tableLayoutPanel3.Controls.Add(this.txtAdresse2_Imm, 1, 4);
            this.tableLayoutPanel3.Controls.Add(this.txtAdresse, 1, 3);
            this.tableLayoutPanel3.Controls.Add(this.txtcodeimmeuble, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.lblGoImmeuble, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtVille, 2, 6);
            this.tableLayoutPanel3.Controls.Add(this.txtVilleClient, 2, 9);
            this.tableLayoutPanel3.Controls.Add(this.cmdIntervenant, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.lblParticulier, 2, 10);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 15;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 17F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 63F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 105F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(1279, 881);
            this.tableLayoutPanel3.TabIndex = 2;
            // 
            // ChkGestionAppel
            // 
            this.ChkGestionAppel.AutoSize = true;
            this.ChkGestionAppel.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.ChkGestionAppel.Location = new System.Drawing.Point(3, 508);
            this.ChkGestionAppel.Name = "ChkGestionAppel";
            this.ChkGestionAppel.Size = new System.Drawing.Size(80, 23);
            this.ChkGestionAppel.TabIndex = 575;
            this.ChkGestionAppel.Text = "Check1";
            this.ChkGestionAppel.UseVisualStyleBackColor = true;
            this.ChkGestionAppel.Visible = false;
            // 
            // fraParticulier
            // 
            this.fraParticulier.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel3.SetColumnSpan(this.fraParticulier, 3);
            this.fraParticulier.Controls.Add(this.tableLayoutPanel4);
            this.fraParticulier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fraParticulier.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.fraParticulier.Location = new System.Drawing.Point(3, 403);
            this.fraParticulier.Name = "fraParticulier";
            this.fraParticulier.Size = new System.Drawing.Size(1232, 99);
            this.fraParticulier.TabIndex = 411;
            this.fraParticulier.TabStop = false;
            this.fraParticulier.Text = "Copro, particulier...";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 2;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel4.Controls.Add(this.label7, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.txtCodeParticulier, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtNom, 1, 2);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 15F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(1226, 76);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label7.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label7.Location = new System.Drawing.Point(3, 15);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(120, 30);
            this.label7.TabIndex = 507;
            this.label7.Text = "Code particulier";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(3, 45);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(120, 31);
            this.label4.TabIndex = 506;
            this.label4.Text = "Nom";
            // 
            // txtCodeParticulier
            // 
            this.txtCodeParticulier.AccAcceptNumbersOnly = false;
            this.txtCodeParticulier.AccAllowComma = false;
            this.txtCodeParticulier.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeParticulier.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeParticulier.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeParticulier.AccHidenValue = "";
            this.txtCodeParticulier.AccNotAllowedChars = null;
            this.txtCodeParticulier.AccReadOnly = false;
            this.txtCodeParticulier.AccReadOnlyAllowDelete = false;
            this.txtCodeParticulier.AccRequired = false;
            this.txtCodeParticulier.BackColor = System.Drawing.Color.White;
            this.txtCodeParticulier.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeParticulier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeParticulier.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeParticulier.ForeColor = System.Drawing.Color.Black;
            this.txtCodeParticulier.Location = new System.Drawing.Point(128, 17);
            this.txtCodeParticulier.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeParticulier.MaxLength = 32767;
            this.txtCodeParticulier.Multiline = false;
            this.txtCodeParticulier.Name = "txtCodeParticulier";
            this.txtCodeParticulier.ReadOnly = false;
            this.txtCodeParticulier.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeParticulier.Size = new System.Drawing.Size(1096, 27);
            this.txtCodeParticulier.TabIndex = 504;
            this.txtCodeParticulier.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeParticulier.UseSystemPasswordChar = false;
            // 
            // txtNom
            // 
            this.txtNom.AccAcceptNumbersOnly = false;
            this.txtNom.AccAllowComma = false;
            this.txtNom.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNom.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNom.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNom.AccHidenValue = "";
            this.txtNom.AccNotAllowedChars = null;
            this.txtNom.AccReadOnly = false;
            this.txtNom.AccReadOnlyAllowDelete = false;
            this.txtNom.AccRequired = false;
            this.txtNom.BackColor = System.Drawing.Color.White;
            this.txtNom.CustomBackColor = System.Drawing.Color.White;
            this.txtNom.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNom.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNom.ForeColor = System.Drawing.Color.Black;
            this.txtNom.Location = new System.Drawing.Point(128, 47);
            this.txtNom.Margin = new System.Windows.Forms.Padding(2);
            this.txtNom.MaxLength = 32767;
            this.txtNom.Multiline = false;
            this.txtNom.Name = "txtNom";
            this.txtNom.ReadOnly = false;
            this.txtNom.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNom.Size = new System.Drawing.Size(1096, 27);
            this.txtNom.TabIndex = 505;
            this.txtNom.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNom.UseSystemPasswordChar = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 17);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(102, 19);
            this.label33.TabIndex = 384;
            this.label33.Text = "Interlocuteur";
            // 
            // txtsss
            // 
            this.txtsss.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtsss.AutoWordSelection = false;
            this.txtsss.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel3.SetColumnSpan(this.txtsss, 2);
            this.txtsss.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtsss.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtsss.ForeColor = System.Drawing.Color.Black;
            this.txtsss.Location = new System.Drawing.Point(127, 20);
            this.txtsss.Name = "txtsss";
            this.txtsss.ReadOnly = false;
            this.txtsss.Size = new System.Drawing.Size(1108, 57);
            this.txtsss.TabIndex = 569;
            this.txtsss.WordWrap = true;
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label8.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label8.Location = new System.Drawing.Point(3, 350);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(118, 30);
            this.label8.TabIndex = 392;
            this.label8.Text = "Contrat";
            // 
            // txtcontrat
            // 
            this.txtcontrat.AccAcceptNumbersOnly = false;
            this.txtcontrat.AccAllowComma = false;
            this.txtcontrat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtcontrat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtcontrat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtcontrat.AccHidenValue = "";
            this.txtcontrat.AccNotAllowedChars = null;
            this.txtcontrat.AccReadOnly = false;
            this.txtcontrat.AccReadOnlyAllowDelete = false;
            this.txtcontrat.AccRequired = false;
            this.txtcontrat.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtcontrat, 2);
            this.txtcontrat.CustomBackColor = System.Drawing.Color.White;
            this.txtcontrat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtcontrat.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtcontrat.ForeColor = System.Drawing.Color.Black;
            this.txtcontrat.Location = new System.Drawing.Point(126, 352);
            this.txtcontrat.Margin = new System.Windows.Forms.Padding(2);
            this.txtcontrat.MaxLength = 32767;
            this.txtcontrat.Multiline = false;
            this.txtcontrat.Name = "txtcontrat";
            this.txtcontrat.ReadOnly = false;
            this.txtcontrat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtcontrat.Size = new System.Drawing.Size(1110, 27);
            this.txtcontrat.TabIndex = 511;
            this.txtcontrat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtcontrat.UseSystemPasswordChar = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 320);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(118, 30);
            this.label6.TabIndex = 390;
            this.label6.Text = "Gestionnaire";
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label5.Location = new System.Drawing.Point(3, 290);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 30);
            this.label5.TabIndex = 389;
            this.label5.Text = "Code Postal";
            // 
            // txtGestionnaire
            // 
            this.txtGestionnaire.AccAcceptNumbersOnly = false;
            this.txtGestionnaire.AccAllowComma = false;
            this.txtGestionnaire.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGestionnaire.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGestionnaire.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGestionnaire.AccHidenValue = "";
            this.txtGestionnaire.AccNotAllowedChars = null;
            this.txtGestionnaire.AccReadOnly = false;
            this.txtGestionnaire.AccReadOnlyAllowDelete = false;
            this.txtGestionnaire.AccRequired = false;
            this.txtGestionnaire.BackColor = System.Drawing.Color.White;
            this.txtGestionnaire.CustomBackColor = System.Drawing.Color.White;
            this.txtGestionnaire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtGestionnaire.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtGestionnaire.ForeColor = System.Drawing.Color.Black;
            this.txtGestionnaire.Location = new System.Drawing.Point(126, 322);
            this.txtGestionnaire.Margin = new System.Windows.Forms.Padding(2);
            this.txtGestionnaire.MaxLength = 32767;
            this.txtGestionnaire.Multiline = false;
            this.txtGestionnaire.Name = "txtGestionnaire";
            this.txtGestionnaire.ReadOnly = false;
            this.txtGestionnaire.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGestionnaire.Size = new System.Drawing.Size(553, 27);
            this.txtGestionnaire.TabIndex = 510;
            this.txtGestionnaire.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGestionnaire.UseSystemPasswordChar = false;
            // 
            // txtCPclient
            // 
            this.txtCPclient.AccAcceptNumbersOnly = false;
            this.txtCPclient.AccAllowComma = false;
            this.txtCPclient.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCPclient.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCPclient.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCPclient.AccHidenValue = "";
            this.txtCPclient.AccNotAllowedChars = null;
            this.txtCPclient.AccReadOnly = false;
            this.txtCPclient.AccReadOnlyAllowDelete = false;
            this.txtCPclient.AccRequired = false;
            this.txtCPclient.BackColor = System.Drawing.Color.White;
            this.txtCPclient.CustomBackColor = System.Drawing.Color.White;
            this.txtCPclient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCPclient.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCPclient.ForeColor = System.Drawing.Color.Black;
            this.txtCPclient.Location = new System.Drawing.Point(126, 292);
            this.txtCPclient.Margin = new System.Windows.Forms.Padding(2);
            this.txtCPclient.MaxLength = 32767;
            this.txtCPclient.Multiline = false;
            this.txtCPclient.Name = "txtCPclient";
            this.txtCPclient.ReadOnly = false;
            this.txtCPclient.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCPclient.Size = new System.Drawing.Size(553, 27);
            this.txtCPclient.TabIndex = 509;
            this.txtCPclient.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCPclient.UseSystemPasswordChar = false;
            // 
            // txtAdresseClient
            // 
            this.txtAdresseClient.AccAcceptNumbersOnly = false;
            this.txtAdresseClient.AccAllowComma = false;
            this.txtAdresseClient.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAdresseClient.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAdresseClient.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtAdresseClient.AccHidenValue = "";
            this.txtAdresseClient.AccNotAllowedChars = null;
            this.txtAdresseClient.AccReadOnly = false;
            this.txtAdresseClient.AccReadOnlyAllowDelete = false;
            this.txtAdresseClient.AccRequired = false;
            this.txtAdresseClient.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtAdresseClient, 2);
            this.txtAdresseClient.CustomBackColor = System.Drawing.Color.White;
            this.txtAdresseClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAdresseClient.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtAdresseClient.ForeColor = System.Drawing.Color.Black;
            this.txtAdresseClient.Location = new System.Drawing.Point(126, 262);
            this.txtAdresseClient.Margin = new System.Windows.Forms.Padding(2);
            this.txtAdresseClient.MaxLength = 32767;
            this.txtAdresseClient.Multiline = false;
            this.txtAdresseClient.Name = "txtAdresseClient";
            this.txtAdresseClient.ReadOnly = false;
            this.txtAdresseClient.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAdresseClient.Size = new System.Drawing.Size(1110, 27);
            this.txtAdresseClient.TabIndex = 508;
            this.txtAdresseClient.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAdresseClient.UseSystemPasswordChar = false;
            // 
            // label
            // 
            this.label.AutoSize = true;
            this.label.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label.Location = new System.Drawing.Point(3, 260);
            this.label.Name = "label";
            this.label.Size = new System.Drawing.Size(118, 30);
            this.label.TabIndex = 388;
            this.label.Text = "Adresse";
            // 
            // lblGerant
            // 
            this.lblGerant.AutoSize = true;
            this.lblGerant.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGerant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGerant.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Underline);
            this.lblGerant.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblGerant.Location = new System.Drawing.Point(3, 233);
            this.lblGerant.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.lblGerant.Name = "lblGerant";
            this.lblGerant.Size = new System.Drawing.Size(118, 27);
            this.lblGerant.TabIndex = 571;
            this.lblGerant.Tag = "17";
            this.lblGerant.Text = "Gérant";
            this.lblGerant.Click += new System.EventHandler(this.lblGerant_Click);
            // 
            // txtCode1
            // 
            this.txtCode1.AccAcceptNumbersOnly = false;
            this.txtCode1.AccAllowComma = false;
            this.txtCode1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCode1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCode1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCode1.AccHidenValue = "";
            this.txtCode1.AccNotAllowedChars = null;
            this.txtCode1.AccReadOnly = false;
            this.txtCode1.AccReadOnlyAllowDelete = false;
            this.txtCode1.AccRequired = false;
            this.txtCode1.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtCode1, 2);
            this.txtCode1.CustomBackColor = System.Drawing.Color.White;
            this.txtCode1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCode1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCode1.ForeColor = System.Drawing.Color.Black;
            this.txtCode1.Location = new System.Drawing.Point(126, 232);
            this.txtCode1.Margin = new System.Windows.Forms.Padding(2);
            this.txtCode1.MaxLength = 32767;
            this.txtCode1.Multiline = false;
            this.txtCode1.Name = "txtCode1";
            this.txtCode1.ReadOnly = false;
            this.txtCode1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCode1.Size = new System.Drawing.Size(1110, 27);
            this.txtCode1.TabIndex = 507;
            this.txtCode1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCode1.UseSystemPasswordChar = false;
            // 
            // txtCodePostal
            // 
            this.txtCodePostal.AccAcceptNumbersOnly = false;
            this.txtCodePostal.AccAllowComma = false;
            this.txtCodePostal.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodePostal.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodePostal.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodePostal.AccHidenValue = "";
            this.txtCodePostal.AccNotAllowedChars = null;
            this.txtCodePostal.AccReadOnly = false;
            this.txtCodePostal.AccReadOnlyAllowDelete = false;
            this.txtCodePostal.AccRequired = false;
            this.txtCodePostal.BackColor = System.Drawing.Color.White;
            this.txtCodePostal.CustomBackColor = System.Drawing.Color.White;
            this.txtCodePostal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodePostal.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodePostal.ForeColor = System.Drawing.Color.Black;
            this.txtCodePostal.Location = new System.Drawing.Point(126, 202);
            this.txtCodePostal.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodePostal.MaxLength = 32767;
            this.txtCodePostal.Multiline = false;
            this.txtCodePostal.Name = "txtCodePostal";
            this.txtCodePostal.ReadOnly = false;
            this.txtCodePostal.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodePostal.Size = new System.Drawing.Size(553, 27);
            this.txtCodePostal.TabIndex = 506;
            this.txtCodePostal.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodePostal.UseSystemPasswordChar = false;
            // 
            // txtAngleRue
            // 
            this.txtAngleRue.AccAcceptNumbersOnly = false;
            this.txtAngleRue.AccAllowComma = false;
            this.txtAngleRue.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAngleRue.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAngleRue.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtAngleRue.AccHidenValue = "";
            this.txtAngleRue.AccNotAllowedChars = null;
            this.txtAngleRue.AccReadOnly = false;
            this.txtAngleRue.AccReadOnlyAllowDelete = false;
            this.txtAngleRue.AccRequired = false;
            this.txtAngleRue.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtAngleRue, 2);
            this.txtAngleRue.CustomBackColor = System.Drawing.Color.White;
            this.txtAngleRue.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAngleRue.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtAngleRue.ForeColor = System.Drawing.Color.Black;
            this.txtAngleRue.Location = new System.Drawing.Point(126, 172);
            this.txtAngleRue.Margin = new System.Windows.Forms.Padding(2);
            this.txtAngleRue.MaxLength = 32767;
            this.txtAngleRue.Multiline = false;
            this.txtAngleRue.Name = "txtAngleRue";
            this.txtAngleRue.ReadOnly = false;
            this.txtAngleRue.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAngleRue.Size = new System.Drawing.Size(1110, 27);
            this.txtAngleRue.TabIndex = 505;
            this.txtAngleRue.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAngleRue.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(3, 200);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(118, 30);
            this.label3.TabIndex = 387;
            this.label3.Text = "Code Postal";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 170);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 30);
            this.label1.TabIndex = 385;
            this.label1.Text = "Angle rue";
            // 
            // txtAdresse2_Imm
            // 
            this.txtAdresse2_Imm.AccAcceptNumbersOnly = false;
            this.txtAdresse2_Imm.AccAllowComma = false;
            this.txtAdresse2_Imm.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAdresse2_Imm.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAdresse2_Imm.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtAdresse2_Imm.AccHidenValue = "";
            this.txtAdresse2_Imm.AccNotAllowedChars = null;
            this.txtAdresse2_Imm.AccReadOnly = false;
            this.txtAdresse2_Imm.AccReadOnlyAllowDelete = false;
            this.txtAdresse2_Imm.AccRequired = false;
            this.txtAdresse2_Imm.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtAdresse2_Imm, 2);
            this.txtAdresse2_Imm.CustomBackColor = System.Drawing.Color.White;
            this.txtAdresse2_Imm.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAdresse2_Imm.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtAdresse2_Imm.ForeColor = System.Drawing.Color.Black;
            this.txtAdresse2_Imm.Location = new System.Drawing.Point(126, 142);
            this.txtAdresse2_Imm.Margin = new System.Windows.Forms.Padding(2);
            this.txtAdresse2_Imm.MaxLength = 32767;
            this.txtAdresse2_Imm.Multiline = false;
            this.txtAdresse2_Imm.Name = "txtAdresse2_Imm";
            this.txtAdresse2_Imm.ReadOnly = false;
            this.txtAdresse2_Imm.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAdresse2_Imm.Size = new System.Drawing.Size(1110, 27);
            this.txtAdresse2_Imm.TabIndex = 504;
            this.txtAdresse2_Imm.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAdresse2_Imm.UseSystemPasswordChar = false;
            // 
            // txtAdresse
            // 
            this.txtAdresse.AccAcceptNumbersOnly = false;
            this.txtAdresse.AccAllowComma = false;
            this.txtAdresse.AccBackgroundColor = System.Drawing.Color.White;
            this.txtAdresse.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtAdresse.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtAdresse.AccHidenValue = "";
            this.txtAdresse.AccNotAllowedChars = null;
            this.txtAdresse.AccReadOnly = false;
            this.txtAdresse.AccReadOnlyAllowDelete = false;
            this.txtAdresse.AccRequired = false;
            this.txtAdresse.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtAdresse, 2);
            this.txtAdresse.CustomBackColor = System.Drawing.Color.White;
            this.txtAdresse.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtAdresse.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtAdresse.ForeColor = System.Drawing.Color.Black;
            this.txtAdresse.Location = new System.Drawing.Point(126, 112);
            this.txtAdresse.Margin = new System.Windows.Forms.Padding(2);
            this.txtAdresse.MaxLength = 32767;
            this.txtAdresse.Multiline = false;
            this.txtAdresse.Name = "txtAdresse";
            this.txtAdresse.ReadOnly = false;
            this.txtAdresse.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtAdresse.Size = new System.Drawing.Size(1110, 27);
            this.txtAdresse.TabIndex = 502;
            this.txtAdresse.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtAdresse.UseSystemPasswordChar = false;
            // 
            // txtcodeimmeuble
            // 
            this.txtcodeimmeuble.AccAcceptNumbersOnly = false;
            this.txtcodeimmeuble.AccAllowComma = false;
            this.txtcodeimmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtcodeimmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtcodeimmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtcodeimmeuble.AccHidenValue = "";
            this.txtcodeimmeuble.AccNotAllowedChars = null;
            this.txtcodeimmeuble.AccReadOnly = false;
            this.txtcodeimmeuble.AccReadOnlyAllowDelete = false;
            this.txtcodeimmeuble.AccRequired = false;
            this.txtcodeimmeuble.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtcodeimmeuble, 2);
            this.txtcodeimmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtcodeimmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtcodeimmeuble.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtcodeimmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtcodeimmeuble.Location = new System.Drawing.Point(126, 82);
            this.txtcodeimmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtcodeimmeuble.MaxLength = 32767;
            this.txtcodeimmeuble.Multiline = false;
            this.txtcodeimmeuble.Name = "txtcodeimmeuble";
            this.txtcodeimmeuble.ReadOnly = false;
            this.txtcodeimmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtcodeimmeuble.Size = new System.Drawing.Size(1110, 27);
            this.txtcodeimmeuble.TabIndex = 512;
            this.txtcodeimmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtcodeimmeuble.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 110);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(118, 30);
            this.label2.TabIndex = 386;
            this.label2.Text = "Adresse";
            // 
            // lblGoImmeuble
            // 
            this.lblGoImmeuble.AutoSize = true;
            this.lblGoImmeuble.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblGoImmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblGoImmeuble.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Underline);
            this.lblGoImmeuble.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblGoImmeuble.Location = new System.Drawing.Point(3, 83);
            this.lblGoImmeuble.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.lblGoImmeuble.Name = "lblGoImmeuble";
            this.lblGoImmeuble.Size = new System.Drawing.Size(118, 27);
            this.lblGoImmeuble.TabIndex = 570;
            this.lblGoImmeuble.Tag = "17";
            this.lblGoImmeuble.Text = "Code immeuble";
            this.lblGoImmeuble.Click += new System.EventHandler(this.lblGoImmeuble_Click);
            // 
            // txtVille
            // 
            this.txtVille.AccAcceptNumbersOnly = false;
            this.txtVille.AccAllowComma = false;
            this.txtVille.AccBackgroundColor = System.Drawing.Color.White;
            this.txtVille.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtVille.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtVille.AccHidenValue = "";
            this.txtVille.AccNotAllowedChars = null;
            this.txtVille.AccReadOnly = false;
            this.txtVille.AccReadOnlyAllowDelete = false;
            this.txtVille.AccRequired = false;
            this.txtVille.BackColor = System.Drawing.Color.White;
            this.txtVille.CustomBackColor = System.Drawing.Color.White;
            this.txtVille.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVille.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtVille.ForeColor = System.Drawing.Color.Black;
            this.txtVille.Location = new System.Drawing.Point(683, 202);
            this.txtVille.Margin = new System.Windows.Forms.Padding(2);
            this.txtVille.MaxLength = 32767;
            this.txtVille.Multiline = false;
            this.txtVille.Name = "txtVille";
            this.txtVille.ReadOnly = false;
            this.txtVille.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtVille.Size = new System.Drawing.Size(553, 27);
            this.txtVille.TabIndex = 572;
            this.txtVille.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtVille.UseSystemPasswordChar = false;
            // 
            // txtVilleClient
            // 
            this.txtVilleClient.AccAcceptNumbersOnly = false;
            this.txtVilleClient.AccAllowComma = false;
            this.txtVilleClient.AccBackgroundColor = System.Drawing.Color.White;
            this.txtVilleClient.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtVilleClient.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtVilleClient.AccHidenValue = "";
            this.txtVilleClient.AccNotAllowedChars = null;
            this.txtVilleClient.AccReadOnly = false;
            this.txtVilleClient.AccReadOnlyAllowDelete = false;
            this.txtVilleClient.AccRequired = false;
            this.txtVilleClient.BackColor = System.Drawing.Color.White;
            this.txtVilleClient.CustomBackColor = System.Drawing.Color.White;
            this.txtVilleClient.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtVilleClient.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtVilleClient.ForeColor = System.Drawing.Color.Black;
            this.txtVilleClient.Location = new System.Drawing.Point(683, 292);
            this.txtVilleClient.Margin = new System.Windows.Forms.Padding(2);
            this.txtVilleClient.MaxLength = 32767;
            this.txtVilleClient.Multiline = false;
            this.txtVilleClient.Name = "txtVilleClient";
            this.txtVilleClient.ReadOnly = false;
            this.txtVilleClient.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtVilleClient.Size = new System.Drawing.Size(553, 27);
            this.txtVilleClient.TabIndex = 573;
            this.txtVilleClient.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtVilleClient.UseSystemPasswordChar = false;
            // 
            // cmdIntervenant
            // 
            this.cmdIntervenant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdIntervenant.FlatAppearance.BorderSize = 0;
            this.cmdIntervenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdIntervenant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdIntervenant.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdIntervenant.Location = new System.Drawing.Point(1241, 83);
            this.cmdIntervenant.Name = "cmdIntervenant";
            this.cmdIntervenant.Size = new System.Drawing.Size(25, 20);
            this.cmdIntervenant.TabIndex = 574;
            this.toolTip1.SetToolTip(this.cmdIntervenant, "Recherche d\'une immeuble");
            this.cmdIntervenant.UseVisualStyleBackColor = false;
            this.cmdIntervenant.Click += new System.EventHandler(this.cmdIntervenant_Click);
            // 
            // lblParticulier
            // 
            this.lblParticulier.AccAcceptNumbersOnly = false;
            this.lblParticulier.AccAllowComma = false;
            this.lblParticulier.AccBackgroundColor = System.Drawing.Color.White;
            this.lblParticulier.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblParticulier.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblParticulier.AccHidenValue = "";
            this.lblParticulier.AccNotAllowedChars = null;
            this.lblParticulier.AccReadOnly = false;
            this.lblParticulier.AccReadOnlyAllowDelete = false;
            this.lblParticulier.AccRequired = false;
            this.lblParticulier.BackColor = System.Drawing.Color.White;
            this.lblParticulier.CustomBackColor = System.Drawing.Color.White;
            this.lblParticulier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblParticulier.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lblParticulier.ForeColor = System.Drawing.Color.Black;
            this.lblParticulier.Location = new System.Drawing.Point(683, 322);
            this.lblParticulier.Margin = new System.Windows.Forms.Padding(2);
            this.lblParticulier.MaxLength = 32767;
            this.lblParticulier.Multiline = false;
            this.lblParticulier.Name = "lblParticulier";
            this.lblParticulier.ReadOnly = false;
            this.lblParticulier.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblParticulier.Size = new System.Drawing.Size(553, 27);
            this.lblParticulier.TabIndex = 576;
            this.lblParticulier.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblParticulier.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 1;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Controls.Add(this.label9, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.ListView1, 0, 1);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 50F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(553, 910);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.label9.BorderStyle = System.Windows.Forms.BorderStyle.FixedSingle;
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.ForeColor = System.Drawing.Color.White;
            this.label9.Location = new System.Drawing.Point(3, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(547, 50);
            this.label9.TabIndex = 384;
            this.label9.Text = "LISTE DES APPELS";
            this.label9.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
            // 
            // ListView1
            // 
            this.ListView1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.ListView1.Location = new System.Drawing.Point(3, 53);
            this.ListView1.Name = "ListView1";
            this.ListView1.Size = new System.Drawing.Size(547, 854);
            this.ListView1.TabIndex = 385;
            this.ListView1.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.ListView1_AfterSelect);
            // 
            // timer1
            // 
            this.timer1.Tick += new System.EventHandler(this.timer1_Tick);
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Controls.Add(this.flowLayoutPanel1, 0, 0);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 41F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel5.TabIndex = 2;
            // 
            // imageList1
            // 
            this.imageList1.ColorDepth = System.Windows.Forms.ColorDepth.Depth8Bit;
            this.imageList1.ImageSize = new System.Drawing.Size(16, 16);
            this.imageList1.TransparentColor = System.Drawing.Color.Transparent;
            // 
            // UserDocGestionAppel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel5);
            this.Name = "UserDocGestionAppel";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "FICHE GESTION DES APPELS";
            this.Load += new System.EventHandler(this.UserDocGestionAppel_Load);
            this.VisibleChanged += new System.EventHandler(this.UserDocGestionAppel_VisibleChanged);
            this.flowLayoutPanel1.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Winsock2)).EndInit();
            this.tableLayoutPanel1.ResumeLayout(false);
            this.Frame1.ResumeLayout(false);
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            this.fraParticulier.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ListView1)).EndInit();
            this.tableLayoutPanel5.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.GroupBox Frame1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.GroupBox fraParticulier;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.Label label7;
        public System.Windows.Forms.Label label4;
        public iTalk.iTalk_TextBox_Small2 txtCodeParticulier;
        public iTalk.iTalk_TextBox_Small2 txtNom;
        public System.Windows.Forms.Label label33;
        public iTalk.iTalk_RichTextBox txtsss;
        public System.Windows.Forms.Label label8;
        public iTalk.iTalk_TextBox_Small2 txtcontrat;
        public System.Windows.Forms.Label label6;
        public System.Windows.Forms.Label label5;
        public iTalk.iTalk_TextBox_Small2 txtGestionnaire;
        public iTalk.iTalk_TextBox_Small2 txtCPclient;
        public iTalk.iTalk_TextBox_Small2 txtAdresseClient;
        public System.Windows.Forms.Label label;
        private System.Windows.Forms.Label lblGerant;
        public iTalk.iTalk_TextBox_Small2 txtCode1;
        public iTalk.iTalk_TextBox_Small2 txtCodePostal;
        public iTalk.iTalk_TextBox_Small2 txtAngleRue;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label1;
        public iTalk.iTalk_TextBox_Small2 txtAdresse2_Imm;
        public iTalk.iTalk_TextBox_Small2 txtAdresse;
        public iTalk.iTalk_TextBox_Small2 txtcodeimmeuble;
        public System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblGoImmeuble;
        public iTalk.iTalk_TextBox_Small2 txtVille;
        public iTalk.iTalk_TextBox_Small2 txtVilleClient;
        public System.Windows.Forms.Button cmdIntervenant;
        public System.Windows.Forms.Label label9;
        public System.Windows.Forms.Button cmdActiverGestionAppel;
        public System.Windows.Forms.Button Command3;
        public System.Windows.Forms.Button cmdCreateInter;
        public System.Windows.Forms.Button Command2;
        private System.Windows.Forms.Timer timer1;
        public System.Windows.Forms.CheckBox ChkGestionAppel;
        public System.Windows.Forms.Button Command4;
        public System.Windows.Forms.Button btnCpteRendu;
        public iTalk.iTalk_TextBox_Small2 lblParticulier;
        private Infragistics.Win.UltraWinTree.UltraTree ListView1;
        private AxMSWinsockLib.AxWinsock Winsock2;
        private System.Windows.Forms.ToolTip toolTip1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.ImageList imageList1;
        //private AxMSWinsockLib.AxWinsock Winsock2;
    }
}
