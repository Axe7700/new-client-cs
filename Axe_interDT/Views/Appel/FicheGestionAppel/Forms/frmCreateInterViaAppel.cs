﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Appel.Forms;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Appel.FicheGestionAppel.Forms
{
    public partial class frmCreateInterViaAppel : Form
    {
        public frmCreateInterViaAppel()
        {
            InitializeComponent();
        }
        private void fc_CreateInterViaAppel()
        {
            try
            {
                DataTable rs = new DataTable();
                ModAdo ModAdors = new ModAdo();
                string sSQL = "";
                rs = ModAdors.fc_OpenRecordSet(sSQL);
                rs.Rows.Add(rs.NewRow());
                ModAdors.Update();
            }
            catch (Exception ex)
            {

                Erreurs.gFr_debug(ex, this.Name + ";fc_CreateInterViaAppel");
            }
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_CodeOrigineQui(string sCode)
        {
            try
            {
                var _with17 = this;
                _with17.cmbIntervenantDT.Text = "";
                _with17.lblLibIntervenantDT.Text = "";
                _with17.cmbIntervenantDT.Visible = false;
                _with17.lblLibIntervenantDT.Visible = false;
                _with17.lblSource.Visible = false;
                _with17.lblSociete.Visible = false;
                _with17.txtSociete.Visible = false;
                _with17.txtSource.Visible = false;
                _with17.txtSociete.Text = "";
                _with17.txtSource.Text = "";
                _with17.txtCodeParticulier.Visible = false;
                _with17.lbllibCodeParticulier.Visible = false;
                _with17.lbllibCodeParticulier.Text = "";
                _with17.txtCodeParticulier.Text = "";
                _with17.lblParticulier.Visible = false;
                _with17.cmdRechercheFour.Visible = false;
                _with17.cmdEctituresCopro.Visible = false;

                _with17.cmdAjoutgestion.Visible = false;

                if (sCode == Convert.ToString(General.cSociete))
                {
                    _with17.txtCodeOrigine2.Text = Convert.ToString(General.cSociete);
                    _with17.cmbIntervenantDT.Visible = true;
                    _with17.lblLibIntervenantDT.Visible = true;
                }
                else if (sCode == Convert.ToString(General.cGerant))
                {
                    _with17.txtCodeOrigine2.Text = Convert.ToString(General.cGerant);
                    _with17.lblSource.Visible = true;
                    _with17.lblSociete.Visible = true;
                    _with17.txtSociete.Visible = true;
                    _with17.txtSource.Visible = true;

                    //.txtSource = fc_ADOlibelle("SELECT Gestionnaire.NomGestion,Gestionnaire.CodeGestinnaire,Immeuble.CodeGestionnaire FROM Gestionnaire INNER JOIN Immeuble ON Gestionnaire.CodeGestinnaire=Immeuble.CodeGestionnaire WHERE Gestionnaire.Code1='" & txtCode1.Text & "'")
                    using (var tmpModAdo = new ModAdo())
                        txtSource.Text = tmpModAdo.fc_ADOlibelle("SELECT Gestionnaire.NomGestion, " + " Gestionnaire.CodeGestinnaire, Immeuble.CodeGestionnaire" +
                            " FROM         Gestionnaire INNER JOIN" + " Immeuble ON Gestionnaire.CodeGestinnaire = Immeuble.CodeGestionnaire" + " WHERE     " +
                            "(Gestionnaire.Code1 = '" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "') " + " AND (Immeuble.CodeImmeuble = '" +
                            StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "')");
                    if (string.IsNullOrEmpty(txtSource.Text))
                    {

                        cmdAjoutgestion.Visible = true;

                    }

                    _with17.txtSociete.Text = txtCode1.Text;
                }
                else if (sCode == Convert.ToString(General.cGardien))
                {
                    _with17.txtCodeOrigine2.Text = Convert.ToString(General.cGardien);
                }
                else if (sCode == Convert.ToString(General.cDivers))
                {
                    _with17.txtCodeOrigine2.Text = Convert.ToString(General.cDivers);
                    _with17.lblSource.Visible = true;
                    _with17.lblSociete.Visible = true;
                    _with17.txtSociete.Visible = true;
                    _with17.txtSource.Visible = true;
                }
                else if (sCode == Convert.ToString(General.cCopro))
                {
                    _with17.txtCodeOrigine2.Text = Convert.ToString(General.cCopro);
                    _with17.txtCodeParticulier.Visible = true;
                    _with17.lbllibCodeParticulier.Visible = true;
                    _with17.lblParticulier.Visible = true;
                    _with17.cmdEctituresCopro.Visible = true;
                }
                else if (sCode == Convert.ToString(General.cFournisseur))
                {
                    _with17.txtCodeOrigine2.Text = Convert.ToString(General.cFournisseur);
                    _with17.lblSource.Visible = true;
                    _with17.lblSociete.Visible = true;
                    _with17.txtSociete.Visible = true;
                    _with17.txtSource.Visible = true;
                    _with17.cmdRechercheFour.Visible = true;
                }
                else
                {
                    //_with17.optOriTelephone.Checked = false;
                    //_with17.optOriTelephone.Checked = true;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_CodeOrigineQui");
            }
        }
        private void fc_libStatut()
        {

            using (var tmp = new ModAdo())
            {
                string sSQL = " SELECT   LibelleCodeEtat From TypeCodeEtat WHERE (CodeEtat = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeEtat.Text) + "')";
                lblLibStatut.Text = tmp.fc_ADOlibelle(sSQL);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_libMatricule()
        {

            using (var tmp = new ModAdo())
            {
                string sSQL = "SELECT     Nom, Prenom From Personnel WHERE (matricule = '" + StdSQLchaine.gFr_DoublerQuote(txtDispatcheur.Text) + "')";
                lbllibDisptach.Text = tmp.fc_ADOlibelle(sSQL);

                sSQL = "SELECT     Nom, Prenom From Personnel WHERE (matricule = '" + StdSQLchaine.gFr_DoublerQuote(txtIntervenant.Text) + "')";
                lblLibInterv.Text = tmp.fc_ADOlibelle(sSQL);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCAI_CategorInterv_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {

                string sSQL = "Select FacArticle.CodeArticle, FacArticle.Designation1 " + "From FacArticle " + "WHERE ((left(FacArticle.CodeArticle,1)='R') AND" +
                     " ((FacArticle.CodeTypeArticle)=0))" + "ORDER BY Designation1 ASC";

                sheridan.InitialiseCombo(cmbCAI_CategorInterv, sSQL, "CodeArticle");
                cmbCAI_CategorInterv.DisplayLayout.Bands[0].Columns[1].Width = cmbCAI_CategorInterv.DisplayLayout.Bands[0].Columns[0].Width * 2;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmbCAI_CategorInterv_DropDown");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCAI_CategorInterv_AfterCloseUp(object sender, EventArgs e)
        {
            cmbCAI_CategorInterv_Validating(cmbCAI_CategorInterv, new System.ComponentModel.CancelEventArgs(false));
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCAI_CategorInterv_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(cmbCAI_CategorInterv.Text))
                {

                    string sSQL = "Select FacArticle.Designation1 " + "From FacArticle " + "WHERE ((left(FacArticle.CodeArticle,1)='R') AND ((FacArticle.CodeTypeArticle)=0))" +
                        " AND FacArticle.CodeArticle='" + cmbCAI_CategorInterv.Text + "'" + "ORDER BY Designation1 ASC";
                    using (var tmpModAdo = new ModAdo())
                        lbllibCAI_CategorInterv.Text = tmpModAdo.fc_ADOlibelle(sSQL);

                    if (string.IsNullOrEmpty(lbllibCAI_CategorInterv.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce type de motif n'éxiste pas.");

                    }
                }
                else
                {
                    lbllibCAI_CategorInterv.Text = "";
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmbCAI_CategorInterv_Validate");

            }
        }
        private void FC_insert()
        {
            try
            {
                int lnumfichestandard = 0;
                string sSQL = "";
                DateTime dtNow = new DateTime();
                DataTable rstmp = new DataTable();
                ModAdo rstmpAdo = new ModAdo();

                //            'sSQL = sSQL & "CodeEtat='" & nz(gFr_DoublerQuote(txtCodeEtat), "00") & "',"
                //'sSQL = sSQL & "CodeParticulier='" & gFr_DoublerQuote(txtCodeParticulier) & "" & "',"
                //'sSQL = sSQL & "Commentaire='" & gFr_DoublerQuote(txtCommentaire) & "" & "',"
                //'sSQL = sSQL & " DateRealise='" & Format(txtDateRealise, FormatDateSQL) & "',"
                //'sSQL = sSQL & " dateprevue='" & Format(txtDatePrevue, FormatDateSQL) & "',"
                //'sSQL = sSQL & "Dispatcheur='" & gFr_DoublerQuote(txtDispatcheur.value) & "',"
                //'sSQL = sSQL & "Designation='" & gFr_DoublerQuote(txtDesignation) & "',"
                //'sSQL = sSQL & "Dispatcheur='" & gFr_DoublerQuote(txtDispatcheur.value) & "',"
                //'sSQL = sSQL & "Intervenant='" & gFr_DoublerQuote(txtMatriculeIntervenant.Text) & "',"
                //'sSQL = sSQL & "Urgence=" & nz(chkUrgence.value, "0") & ","
                //'sSQL = sSQL & "NumFicheStandard=" & gFr_NombreAng(txtNumFicheStandard) & ","
                //'sSQL = sSQL & " ModifierLe=  '" & Now & "',"
                //'sSQL = sSQL & "ModifierPar ='" & Left(gFr_DoublerQuote(fncUserName), 50) & "',"
                //'chkUrgence = nz(rstmp!Urgence, 0)

                dtNow = DateTime.Now;
                sSQL = "SELECT Utilisateur, DateAjout, CodeImmeuble, CodeOrigine1, CodeOrigine2, Source, IntervenantDT, Observations, "
                      + " CodeParticulier, CodeOrigine3_TO3, CAI_Code, intervention, Numfichestandard "
                      + " From GestionStandard"
                      + " WHERE GestionStandard.NumFicheStandard =0";
                rstmp = new DataTable();
                rstmp = rstmpAdo.fc_OpenRecordSet(sSQL);
                var newRow = rstmp.NewRow();

                SqlCommandBuilder cb = new SqlCommandBuilder(rstmpAdo.SDArsAdo);
                rstmpAdo.SDArsAdo.SelectCommand = cb.DataAdapter.SelectCommand;
                rstmpAdo.SDArsAdo.UpdateCommand = cb.GetUpdateCommand();
                SqlCommand insertCmd = (SqlCommand)cb.GetInsertCommand().Clone();
                insertCmd.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                insertCmd.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                rstmpAdo.SDArsAdo.InsertCommand = insertCmd;

                rstmpAdo.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                {

                    if (e.StatementType == StatementType.Insert)
                    {

                        if (lnumfichestandard == 0)
                        {

                            lnumfichestandard =
                                Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                        }
                    }
                });
                newRow["Utilisateur"] = General.fncUserName();
                newRow["DateAjout"] = dtNow;
                newRow["CodeImmeuble"] = General.nz(txtCodeimmeuble.Text, "");
                newRow["CodeOrigine1"] = General.cTel;
                newRow["CodeOrigine2"] = General.nz(txtCodeOrigine2.Text, "");
                //newRow["Source"] = nz(txtSociete, Null)
                //newRow["Societe"] = nz(txtSource, Null)
                newRow["IntervenantDT"] = General.nz(cmbIntervenantDT.Text, "");
                newRow["Observations"] = General.nz(txtObservations.Text, "");
                //newRow["CodeAction "] = nz(cmbCodeAction, Null)
                //newRow["Document"] = nz(txtDocument, Null)
                newRow["CodeParticulier"] = General.nz(txtCodeParticulier.Text, "");
                //newRow["TexteLibre"] = nz(txtTexteLibre, Null)
                //newRow["Intervention"] = nz(txtIntervention, 0)
                //newRow["CodeOrigine3_TO3"] = nz(txtCodeOrigine3_TO3, Null)
                //newRow["CAI_Code"] = "code";//'nz(cmbCAI_CategorInterv, Null)
                newRow["Intervention"] = "1";

                //            '=== ajout du 15 02 2019
                //'rstmp!Document = nz(txtDocument, Null)
                //'rstmp!CodeOrigine3_TO3 = nz(txtCodeOrigine3_TO3, Null)

                newRow["CAI_Code"] = General.nz(cmbCAI_CategorInterv.Text, "");

                sSQL = sSQL + "Societe='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtSociete.Text), "") + "',";
                sSQL = sSQL + "Source='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtSource.Text), "") + "',";

                //'== fin ajout.

                rstmp.Rows.Add(newRow.ItemArray);
                rstmpAdo.Update();

                rstmp.Dispose();
                rstmpAdo.SDArsAdo.Dispose();

                sSQL = "SELECT Numfichestandard,CodeImmeuble, DateSaisie, CodeParticulier, DatePrevue, CreeLe, CreePar, NoIntervention, "
                    + " CodeEtat,Commentaire, DateRealise,Dispatcheur,Designation,Intervenant, Urgence, INT_AnaActivite , CAI_Code ,article    "
                    + " FROM intervention "
                   + " WHERE NoIntervention = 0 ";
                rstmpAdo = new ModAdo();
                rstmp = rstmpAdo.fc_OpenRecordSet(sSQL);
                var newRow1 = rstmp.NewRow();

                int lNoIntervention = 0;
                //SqlCommandBuilder cb1 = new SqlCommandBuilder(rstmpAdo.SDArsAdo);
                //rstmpAdo.SDArsAdo.SelectCommand = cb1.DataAdapter.SelectCommand;
                //rstmpAdo.SDArsAdo.UpdateCommand = cb1.GetUpdateCommand();
                //SqlCommand insertCmd1 = (SqlCommand)cb1.GetInsertCommand().Clone();
                //insertCmd1.CommandText += ";SET @Id = SCOPE_IDENTITY();";
                //insertCmd1.Parameters.Add("@Id", SqlDbType.Int, 0, "Id").Direction = ParameterDirection.Output;
                //rstmpAdo.SDArsAdo.InsertCommand = insertCmd1;

                //rstmpAdo.SDArsAdo.RowUpdated += new SqlRowUpdatedEventHandler(delegate (object sender, SqlRowUpdatedEventArgs e)
                //{

                //    if (e.StatementType == StatementType.Insert)
                //    {

                //        if (lNoIntervention == 0)
                //        {

                //            lNoIntervention =
                //                 Convert.ToInt32(e.Command.Parameters["@ID"].Value.ToString());
                //        }
                //    }
                //});

                newRow1["Numfichestandard"] =lnumfichestandard;
                newRow1["CodeImmeuble"] = General.nz(txtCodeimmeuble.Text, "");
                newRow1["DateSaisie"] = dtNow;
                //newRow["Courrier"] = nz(txtDocument, 0)
                //newRow["CodeParticulier"] = txtCodeParticulier
                //newRow["CAI_Code"] = txtCai_code
                //newRow["DatePrevue"] = Date
                newRow1["CreeLe"] = DateTime.Now;
                newRow1["CreePar"] = General.Left(General.fncUserName(), 50);
                newRow1["CodeEtat"] = "00";
                newRow1["INT_AnaActivite"] = General.nz(txtINT_AnaActivite.Text, "");
                newRow1["Article"] = txtArticle.Text;
                //newRow["CodeParticulier"] = nz(txtCodeParticulier, Null)
                newRow1["Commentaire"] = General.nz(txtCommentaire.Text, "");
                //newRow["DateRealise"] =nz(Format(txtDateRealise, FormatDateSQL), Null)
                //newRow["DatePrevue"] = nz(Format(txtDatePrevue, FormatDateSQL), Null)
                newRow1["dispatcheur"] = General.nz(txtDispatcheur.Text,"");
                newRow1["Designation"] = General.nz(txtDesignation.Text,"");
                newRow1["Intervenant"] = General.nz(txtMatriculeIntervenant.Text, "");
                if (chkUrgence.Checked)
                {
                    newRow1["Urgence"] = 1;
                }
                else
                {
                    newRow1["Urgence"] = 0;
                }
                //'rstmp! = nz(, Null)

                newRow1["CodeParticulier"] = General.nz(txtCodeParticulier.Text, "");
                newRow1["DatePrevue"] = DateTime.Now;
                newRow1["CAI_Code"] = cmbCAI_CategorInterv.Text;
                rstmp.Rows.Add(newRow1.ItemArray);
                rstmpAdo.Update();
                using (var tmp = new ModAdo())
                    txtNoIntervention.Text =tmp.fc_ADOlibelle("select max(NoIntervention) FROM intervention ");
                General.lCreateInterViaGestAppel = Convert.ToInt32(txtNoIntervention.Text);

                rstmp.Dispose();
                rstmpAdo.Dispose();
                this.Close();
                //            'fc_OpenConnSage
                //'AnalytiqueActivite = fc_ADOlibelle("SELECT CA_Num FROM F_COMPTEA WHERE ActiviteTravaux='1'", False, adoSage)
                //'fc_CloseConnSage
                //'adocnn.Execute "Update GESTIONSTANDARD set intervention = '1', AnalytiqueActivite='" & AnalytiqueActivite & "' WHERE NumFicheStandard=" & sCode

            }catch(Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";FC_insert");
            }


        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdCreateInter_Click(object sender, EventArgs e)
        {
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous créer une intervention ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)//tested
            {
                return;
            }
            if (string.IsNullOrEmpty(txtArticle.Text))//tested
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le type d'opération est obligatoire.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            if (string.IsNullOrEmpty(txtDispatcheur.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le dispatcheur est obligatoire.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            FC_insert();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheCodetat_Click(object sender, EventArgs e)
        {
            try
            {
                string sCode = "";
                string requete = "SELECT CodeEtat as \"CodeEtat\"," + " LibelleCodeEtat as \"Libelle\"" + " FROM TypeCodeEtat";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un fournisseur" };

                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    // charge les enregistrements de l'immeuble
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    if (!string.IsNullOrEmpty(fc_ControleEtat(sCode)))
                    {
                        txtCodeEtat.Text = sCode;
                    }
                    else
                    {
                        txtCodeEtat.Text = "";
                    }
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        // charge les enregistrements de l'immeuble
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        if (!string.IsNullOrEmpty(fc_ControleEtat(sCode)))
                        {
                            txtCodeEtat.Text = sCode;
                        }
                        else
                        {
                            txtCodeEtat.Text = "";
                        }
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdrecherche_Click");
            }
        }
        private string fc_ControleEtat(string sEtat)
        {
            string sAnalytiqueActivite = "";
            DataTable rsCodeEtat = new DataTable();
            ModAdo rsCodeEtatAdo = new ModAdo();
            try
            {
                rsCodeEtat = rsCodeEtatAdo.fc_OpenRecordSet("SELECT AnalytiqueDevis, AnalytiqueContrat, AnalytiqueTravaux, RazAnalytique FROM TypeCodeEtat WHERE CodeEtat = '" + sEtat + "'");
                if (rsCodeEtat.Rows.Count > 0)
                {
                    if (rsCodeEtat.Rows[0]["RazAnalytique"] + "" == "1")
                    {
                        txtINT_AnaActivite.Text = "";
                    }
                    else
                    {
                        //===@@@ modif du 29 05 2017, desactive Sage.
                        if (General.sDesActiveSage == "1")
                        {
                            txtINT_AnaActivite.Text = "";
                        }
                        else
                        {
                            SAGE.fc_OpenConnSage();
                            using (var tmp = new ModAdo())
                                sAnalytiqueActivite = tmp.fc_ADOlibelle("SELECT CA_NUM FROM F_COMPTEA WHERE ActiviteDevis=" + General.nz(rsCodeEtat.Rows[0]["AnalytiqueDevis"], "0")
                                    + " AND ActiviteTravaux=" + General.nz(rsCodeEtat.Rows[0]["AnalytiqueTravaux"], "0")
                                    + " AND ActiviteContrat=" + General.nz(rsCodeEtat.Rows[0]["AnalytiqueContrat"], "0"), false, SAGE.adoSage);
                            SAGE.fc_CloseConnSage();
                        }
                    }
                }
                return sEtat;

                rsCodeEtat.Dispose();
                rsCodeEtat = null;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_ControleEtat");
                return "";
            }

        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optQuiSociete_CheckedChanged(object sender, EventArgs e)
        {
            if (optQuiSociete.Checked)
            {
                if (optQuiSociete.Checked == true)
                {
                    fc_CodeOrigineQui(Convert.ToString(General.cSociete));
                }
            }
        }
        /// <summary>
        /// teSTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optQuiGerant_CheckedChanged(object sender, EventArgs e)
        {
            if (optQuiGerant.Checked)
            {
                if (optQuiGerant.Checked == true)
                {
                    fc_CodeOrigineQui(Convert.ToString(General.cGerant));
                }
            }
        }
        /// <summary>
        /// teSTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optQuiGardien_CheckedChanged(object sender, EventArgs e)
        {
            if (optQuiGardien.Checked)
            {
                if (optQuiGardien.Checked == true)
                {
                    fc_CodeOrigineQui(Convert.ToString(General.cGardien));
                }
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optQuiAutre_CheckedChanged(object sender, EventArgs e)
        {
            if (optQuiAutre.Checked)
            {
                if (optQuiAutre.Checked == true)
                {
                    fc_CodeOrigineQui(Convert.ToString(General.cDivers));
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optQuiFournisseur_CheckedChanged(object sender, EventArgs e)
        {
            if (optQuiFournisseur.Checked)
            {
                if (optQuiFournisseur.Checked == true)
                {
                    fc_CodeOrigineQui(Convert.ToString(General.cFournisseur));
                }

            }

        }
        /// <summary>
        /// teSTEED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void optQuiCopro_CheckedChanged(object sender, EventArgs e)
        {
            if (optQuiCopro.Checked)
            {
                if (optQuiCopro.Checked == true)
                {
                    fc_CodeOrigineQui(Convert.ToString(General.cCopro));
                }
            }
        }

        private void OptQuiGestionnaire_CheckedChanged(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenantDT_AfterCloseUp(object sender, EventArgs e)
        {
            fc_libIntervenantDT(cmbIntervenantDT.Text);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenantDT_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                General.sSQL = "";
                General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom" + " FROM Personnel";
                sheridan.InitialiseCombo(cmbIntervenantDT, General.sSQL, "Matricule");
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmbIntervenantDT_DropDown");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sMatricule"></param>
        /// <returns></returns>
        private bool fc_libIntervenantDT(string sMatricule)
        {
            bool functionReturnValue = true;
            try
            {
                if (!string.IsNullOrEmpty(sMatricule))
                {
                    General.sSQL = "";
                    General.sSQL = "SELECT  Personnel.Nom" + " FROM Personnel" + " where Personnel.Matricule='" + sMatricule + "'";

                    if ((General.rstmp != null))
                    {
                        General.rstmp?.Dispose();
                    }

                    General.modAdorstmp = new ModAdo();
                    General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);

                    if (General.rstmp.Rows.Count > 0)
                    {
                        lblLibIntervenantDT.Text = General.rstmp.Rows[0]["Nom"] + "";
                        functionReturnValue = false;
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet intervenant n'éxiste pas ", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        lblLibIntervenantDT.Text = "";
                        functionReturnValue = true;
                    }
                }
                else
                {
                    lblLibIntervenantDT.Text = "";
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_libIntervenantDT");
                return functionReturnValue;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenantDT_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if ((int)e.KeyChar == 13)
                {

                    string requete = "SELECT Personnel.Matricule as \"Matricule \"," + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\"," +
                        " Qualification.Qualification as \"Qualification\"," + " Personnel.Memoguard as \"MemoGuard\"," + " Personnel.NumRadio as \"NumRadio\"" +
                        " FROM Qualification right JOIN Personnel " + " ON Qualification.CodeQualif = Personnel.CodeQualif";
                    string where_order = "";
                    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "" };
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        // charge les enregistrements de l'immeuble
                        cmbIntervenantDT.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        lblLibIntervenantDT.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            cmbIntervenantDT.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            lblLibIntervenantDT.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                            fg.Dispose();
                            fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();
                }
                //        cmbIntervention_Click
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervenantDT_KeyPress");
                throw;
            }
        }
        /// <summary>
        /// Testeed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenantDT_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(cmbIntervenantDT.Text))
                {
                    e.Cancel = fc_libIntervenantDT(cmbIntervenantDT.Text);
                }
                else
                {
                    lblLibIntervenantDT.Text = "";
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmbIntervenantDT_Validate");
            }
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdIntervenant_Click(object sender, EventArgs e)
        {
            cmbIntervenantDT_KeyPress(cmbIntervenantDT, new KeyPressEventArgs((char)13));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheFour_Click(object sender, EventArgs e)
        {
            try
            {
                string requete = "SELECT fournisseurArticle.Code as \"Code\"," + " fournisseurArticle.Raisonsocial as \"Raison social\"" + " FROM fournisseurArticle";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un fournisseur" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtSource.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtSource.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdrecherche_Click");
            }
        }

        private void txtCodeEtat_TextChanged(object sender, EventArgs e)
        {
            fc_libStatut();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeEtat_KeyPress(object sender, KeyPressEventArgs e)
        {
            cmdRechercheCodetat_Click(cmdRechercheCodetat, new System.EventArgs());
        }

        private void txtCodeParticulier_AfterCloseUp(object sender, EventArgs e)
        {
            fc_libParticulier(txtCodeParticulier.Text);
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeParticulier_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                General.sSQL = "";
                General.sSQL = "SELECT ImmeublePart.CodeParticulier, ImmeublePart.Nom,"
                    + " ImmeublePart.Batiment" + " From ImmeublePart" + " WHERE ImmeublePart.CodeImmeuble ='" + txtCodeimmeuble.Text + "'";
                sheridan.InitialiseCombo(txtCodeParticulier, General.sSQL, "CodeParticulier");
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCodeParticulier_DropDown");
            }
        }
        private bool fc_libParticulier(string sCode)
        {
            bool functionReturnValue = false;

            try
            {
                if (!string.IsNullOrEmpty(sCode))
                {
                    General.sSQL = "";
                    General.sSQL = "SELECT  ImmeublePart.Nom" + " From ImmeublePart" + " WHERE ImmeublePart.CodeParticulier ='" + txtCodeParticulier.Text + "'";

                    General.modAdorstmp = new ModAdo();

                    General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);
                    if (General.rstmp.Rows.Count > 0)
                    {
                        lbllibCodeParticulier.Text = General.rstmp.Rows[0]["Nom"].ToString();
                        functionReturnValue = false;
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce particulier n'éxiste pas, allez dans la fiche immeuble pour en créer un" + " nouveau.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lbllibCodeParticulier.Text = "";
                        functionReturnValue = true;
                    }
                }
                else
                {
                    lbllibCodeParticulier.Text = "";
                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_libParticulier");
                return functionReturnValue;
            }
        }

        private void cmdAjoutgestion_Click(object sender, EventArgs e)
        {
            if (txtCodeimmeuble.Text == "")
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un immeuble.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (txtCode1.Text == "")
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un un gérant.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            frmAjoutGestionnaire frm = new frmAjoutGestionnaire();
            frm.txtCodeImmeuble.Text = txtCodeimmeuble.Text;
            frm.ShowDialog();
            fc_CodeOrigineQui(General.cGerant.ToString());
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {

            try
            {
                string sCode = "";
                string requete = "SELECT CodeArticle as \"Code opération\"," + " Designation1 as \"Designation\"" + " FROM FacArticle";
                string where_order = "Masquer IS NULL OR Masquer =   " + General.fc_bln(false);
                if (General.sCaterogieArticleInter == "1")
                {
                    where_order = " CodeCategorieArticle ='I' OR CodeCategorieArticle ='GP' ";
                }
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un fournisseur" };
                fg.SetValues(new Dictionary<string, string> { { "CodeArticle", txtArticle.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    // charge les enregistrements de l'immeuble

                    txtArticle.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtDesignation.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    using (var tmp = new ModAdo())
                    {
                        chkCompteurObli.Checked = General.nz(tmp.fc_ADOlibelle("SELECT SaisieCompteur FROM FacArticle where CodeArticle='" + txtArticle.Text + "'"), 0).ToString() == "1";
                        chkUrgence.Checked = General.nz(tmp.fc_ADOlibelle("SELECT Urgent FROM FacArticle where CodeArticle='" + txtArticle.Text + "'"), 0).ToString() == "1";

                    }

                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        // charge les enregistrements de l'immeuble

                        txtArticle.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtDesignation.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        using (var tmp = new ModAdo())
                        {
                            chkCompteurObli.Checked = General.nz(tmp.fc_ADOlibelle("SELECT SaisieCompteur FROM FacArticle where CodeArticle='" + txtArticle.Text + "'"), 0).ToString() == "1";
                            chkUrgence.Checked = General.nz(tmp.fc_ADOlibelle("SELECT Urgent FROM FacArticle where CodeArticle='" + txtArticle.Text + "'"), 0).ToString() == "1";

                        }

                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdrecherche_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtArticle_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)(e.KeyChar);
            try
            {
                var tmp = new ModAdo();
                if (KeyAscii == 13)
                {
                    if (General.nz(tmp.fc_ADOlibelle("SELECT CodeArticle FROM FacArticle WHERE CodeArticle='" + txtArticle.Text + "'"), "").ToString() == "")
                    {
                        General.blControle = true;
                        Command1_Click(Command1, new System.EventArgs());
                        KeyAscii = 0;
                    }
                    else
                    {
                        txtDesignation.Text = General.nz(tmp.fc_ADOlibelle("SELECT DESIGNATION1 FROM FacArticle where CodeArticle='" + txtArticle.Text + "'"), "").ToString();
                        chkCompteurObli.Checked = General.nz(tmp.fc_ADOlibelle("SELECT SaisieCompteur FROM FacArticle where CodeArticle='" + txtArticle.Text + "'"), 0).ToString() == "1";
                        chkUrgence.Checked = General.nz(tmp.fc_ADOlibelle("SELECT Urgent FROM FacArticle where CodeArticle='" + txtArticle.Text + "'"), 0).ToString() == "1";
                    }

                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheTech_Click(object sender, EventArgs e)
        {
            try
            {
                string sCode = "";
                string requete = "SELECT Matricule as \"Matricule\"," + " Nom  as \"Nom \", Prenom as \"Prenom \"  FROM Personnel";
                string where_order = "(NonActif is null or NonActif = 0)";

                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Associer un dispatcheur" };
                if (General.IsNumeric(txtLibDispatch.Text))
                {
                    fg.SetValues(new Dictionary<string, string> { { "Matricule", txtLibDispatch.Text } });
                }
                else
                {
                    fg.SetValues(new Dictionary<string, string> { { "Nom", txtLibDispatch.Text } });
                }
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {

                    txtDispatcheur.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    txtLibDispatch.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();


                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtDispatcheur.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        txtLibDispatch.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();


                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
                fc_libMatricule();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheTech_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtLibDispatch_TextChanged(object sender, EventArgs e)
        {
            string str = "";
            if (General.IsNumeric(txtLibDispatch.Text))
            {
                txtDispatcheur.Text = txtLibDispatch.Text;
            }
            else
            {
                if(txtDispatcheur.ActiveRow!=null)
                    txtDispatcheur.ActiveRow.Cells["Nom"].Value = txtLibDispatch.Text;

                str = txtLibDispatch.Text;
                if (txtDispatcheur.Text != "")
                {
                    using (var tmp = new ModAdo())
                        txtDispatcheur.Text = tmp.fc_ADOlibelle("SELECT Personnel.Matricule FROM Personnel WHERE Personnel.Nom LIKE '" + txtLibDispatch.Text + "%'");
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtLibDispatch_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdRechercheTech_Click(cmdRechercheTech, new System.EventArgs());
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheIntervenant_Click(object sender, EventArgs e)
        {
            string requete = "SELECT Personnel.Matricule as \"Matricule \"," + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\"," +
                        " Qualification.Qualification as \"Qualification\"," + " Personnel.Memoguard as \"MemoGuard\"," + " Personnel.NumRadio as \"NumRadio\"" +
                        " FROM Qualification right JOIN Personnel " + " ON Qualification.CodeQualif = Personnel.CodeQualif";
            string where_order = "(NonActif is null or NonActif = 0)";

            SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "" };
            if (General.IsNumeric(txtLibDispatch.Text))
            {
                fg.SetValues(new Dictionary<string, string> { { "Matricule", txtIntervenant.Text } });
            }
            else
            {
                fg.SetValues(new Dictionary<string, string> { { "Nom", txtIntervenant.Text } });
            }
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                // charge les enregistrements de l'immeuble
                txtIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                txtMatriculeIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                txtMemoGuard.Text = fg.ugResultat.ActiveRow.Cells[5].Value.ToString();
                fg.Dispose();
                fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {

                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    cmbIntervenantDT.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    lblLibIntervenantDT.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                }
            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();
            fc_libMatricule();
        }

        private void txtIntervenant_TextChanged(object sender, EventArgs e)
        {
            txtMatriculeIntervenant.Text = txtIntervenant.Text;
            using (var tmp = new ModAdo())
            {
                txtMemoGuard.Text = tmp.fc_ADOlibelle("SELECT MemoGuard FROM Personnel where matricule='" + txtMatriculeIntervenant + "'");
                lblNomIntervenant.Text = tmp.fc_ADOlibelle("SELECT NOM FROM PERSONNEL WHERE MATRICULE ='" + txtIntervenant + "'");
            }
        }

        private void txtIntervenant_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdRechercheIntervenant_Click(cmdRechercheIntervenant, new System.EventArgs());
            }
        }

        private void txtCommentaire_TextChanged(object sender, EventArgs e)
        {
            try
            {
                string sSQL = "";
                int lngSMS = 0;
                DataTable rs = new DataTable();
                ModAdo rsAdo = new ModAdo();

                sSQL = "SELECT Adresse, CodePostal, Ville, CodeAcces1, situation FROM Immeuble WHERE CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";

                rs = rsAdo.fc_OpenRecordSet(sSQL);
                lngSMS = General.Len(string.Format(txtNoIntervention.Text, "#####00000") + "/" + General.Left(txtMemoGuard.Text, 4) + "/" + General.Left(rs.Rows[0]["Adresse"] + "", 23) + "/"
                    + General.Left(rs.Rows[0]["CodePostal"] + "", 5) + "/" + General.Left(rs.Rows[0]["codeAcces1"] + "", 8) + "/" + General.Left(rs.Rows[0]["Ville"] + "", 10) + "/"
                    + General.Left(rs.Rows[0]["situation"] + "", 12) + "/" + General.Left(txtDesignation.Text, 25) + "/");
                if (Convert.ToInt32(160 - lngSMS - General.Len(txtCommentaire.Text)) >= 0)
                {
                    Frame12.Text = "Commentaires => " + Convert.ToInt32(160 - lngSMS - General.Len(txtCommentaire.Text)) + " caractères restants pour le SMS";
                }
                else
                {
                    Frame12.Text = "Commentaires => " + Math.Abs(Convert.ToInt32(160 - lngSMS - General.Len(txtCommentaire.Text))) + " caractères restants pour le SMS";
                }
                rs.Dispose();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCommentaire_TextChanged");
            }
        }

        private void txtCommentaire_Leave(object sender, EventArgs e)
        {
            string sSQL = "";
            int lngSMS = 0;
            DataTable rs = new DataTable();
            ModAdo rsAdo = new ModAdo();

            sSQL = "SELECT Adresse, CodePostal, Ville, CodeAcces1, situation FROM Immeuble WHERE CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";

            rs = rsAdo.fc_OpenRecordSet(sSQL);
            lngSMS = General.Len(string.Format(txtNoIntervention.Text, "#####00000") + "/" + General.Left(txtMemoGuard.Text, 4) + "/" + General.Left(rs.Rows[0]["Adresse"] + "", 23) + "/"
                + General.Left(rs.Rows[0]["CodePostal"] + "", 8) + "/" + General.Left(rs.Rows[0]["codeAcces1"] + "", 5) + "/" + General.Left(rs.Rows[0]["Ville"] + "", 10) + "/"
                + General.Left(rs.Rows[0]["situation"] + "", 12) + "/" + General.Left(txtDesignation.Text, 25) + "/");

            if (General.Len(txtCommentaire.Text) > 160 - lngSMS)
            {
                txtCommentaire.SelectionStart = (160 - lngSMS);
                txtCommentaire.SelectionLength = General.Len(txtCommentaire.Text) - (160 - lngSMS);
            }
            //txtCommentaire.ToolTipText = (160 - lngSMS)+ " caractères disponibles pour le SMS";
        }
    }
}
