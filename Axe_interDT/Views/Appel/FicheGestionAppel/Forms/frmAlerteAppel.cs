﻿using Axe_interDT.Shared;
using Axe_interDT.Views.FicheGestionAppel;
using System;
using System.Windows.Forms;

namespace Axe_interDT.Views.Appel.FicheGestionAppel.Forms
{
    public partial class frmAlerteAppel : Form
    {
        public frmAlerteAppel()
        {
            InitializeComponent();
        }

        private void frmAlerteAppel_Activated(object sender, EventArgs e)
        {
            General.SetTopMostWindow(this, true);
        }

        private void frmAlerteAppel_Load(object sender, EventArgs e)
        {
            frmAlerteAppel_Activated(sender, e);
        }

        private void lblNoTel_Click(object sender, EventArgs e)
        {
            fc_LoadAppel();
        }

        private void fc_LoadAppel()
        {
            try
            {
                //===> Mondir le 07.05.2021, commented by Mondir, no need for this shit fu****
                //foreach(Form FormLoad in Application.OpenForms)
                //{
                //    if(FormLoad.Visible)
                //    {
                //        if(FormLoad.Name.ToUpper() != Name.ToUpper())
                //        {
                //            FormLoad.Hide();
                //        }
                //    }
                //}

                Close();

                General.bLoadAlertAppel = false;

                View.Theme.Theme.Navigate(typeof(UserDocGestionAppel));
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }
    }
}
