﻿namespace Axe_interDT.Views.Appel.FicheGestionAppel.Forms
{
    partial class frmCreateInterViaAppel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance37 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance38 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance39 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance40 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance41 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance42 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance43 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance44 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance45 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance46 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance47 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance48 = new Infragistics.Win.Appearance();
            this.Frame3 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCodeOrigine2 = new iTalk.iTalk_TextBox_Small2();
            this.Label22 = new System.Windows.Forms.Label();
            this.lbllibCodeParticulier = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeParticulier = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtSource = new iTalk.iTalk_TextBox_Small2();
            this.lblSociete = new System.Windows.Forms.Label();
            this.txtSociete = new iTalk.iTalk_TextBox_Small2();
            this.lblLibIntervenantDT = new iTalk.iTalk_TextBox_Small2();
            this.optQuiSociete = new System.Windows.Forms.RadioButton();
            this.optQuiGerant = new System.Windows.Forms.RadioButton();
            this.optQuiGardien = new System.Windows.Forms.RadioButton();
            this.optQuiAutre = new System.Windows.Forms.RadioButton();
            this.optQuiFournisseur = new System.Windows.Forms.RadioButton();
            this.optQuiCopro = new System.Windows.Forms.RadioButton();
            this.OptQuiGestionnaire = new System.Windows.Forms.RadioButton();
            this.cmbIntervenantDT = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cmdIntervenant = new System.Windows.Forms.Button();
            this.cmdEctituresCopro = new System.Windows.Forms.Button();
            this.tableLayoutPanel26 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdRechercheFour = new System.Windows.Forms.Button();
            this.lblSource = new System.Windows.Forms.Label();
            this.lblParticulier = new System.Windows.Forms.Label();
            this.cmdAjoutgestion = new System.Windows.Forms.Button();
            this.tableLayoutPanel18 = new System.Windows.Forms.TableLayoutPanel();
            this.label6 = new System.Windows.Forms.Label();
            this.lbllibCAI_CategorInterv = new iTalk.iTalk_TextBox_Small2();
            this.cmbCAI_CategorInterv = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.label23 = new System.Windows.Forms.Label();
            this.txtObservations = new iTalk.iTalk_RichTextBox();
            this.label5 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.cmdCreateInter = new System.Windows.Forms.Button();
            this.txtcontrat = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCodeimmeuble = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.txtDesignation = new iTalk.iTalk_TextBox_Small2();
            this.txtNoIntervention = new iTalk.iTalk_TextBox_Small2();
            this.txtIntervenant = new iTalk.iTalk_TextBox_Small2();
            this.label7 = new System.Windows.Forms.Label();
            this.label8 = new System.Windows.Forms.Label();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.label11 = new System.Windows.Forms.Label();
            this.label10 = new System.Windows.Forms.Label();
            this.txtINT_AnaActivite = new iTalk.iTalk_TextBox_Small2();
            this.cmdRechercheIntervenant = new System.Windows.Forms.Button();
            this.cmdRechercheTech = new System.Windows.Forms.Button();
            this.txtLibDispatch = new iTalk.iTalk_TextBox_Small2();
            this.Command1 = new System.Windows.Forms.Button();
            this.txtArticle = new iTalk.iTalk_TextBox_Small2();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.lblLibInterv = new iTalk.iTalk_TextBox_Small2();
            this.lbllibDisptach = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeEtat = new iTalk.iTalk_TextBox_Small2();
            this.chkCompteurObli = new System.Windows.Forms.CheckBox();
            this.chkUrgence = new System.Windows.Forms.CheckBox();
            this.cmdRechercheCodetat = new System.Windows.Forms.Button();
            this.lblLibStatut = new iTalk.iTalk_TextBox_Small2();
            this.lblNomIntervenant = new iTalk.iTalk_TextBox_Small2();
            this.Frame12 = new System.Windows.Forms.GroupBox();
            this.txtCommentaire = new System.Windows.Forms.RichTextBox();
            this.label9 = new System.Windows.Forms.Label();
            this.txtMatriculeIntervenant = new iTalk.iTalk_TextBox_Small2();
            this.txtMemoGuard = new iTalk.iTalk_TextBox_Small2();
            this.txtDispatcheur = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCode1 = new iTalk.iTalk_TextBox_Small2();
            this.Frame3.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodeParticulier)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIntervenantDT)).BeginInit();
            this.tableLayoutPanel26.SuspendLayout();
            this.tableLayoutPanel18.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCAI_CategorInterv)).BeginInit();
            this.tableLayoutPanel9.SuspendLayout();
            this.Frame12.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtDispatcheur)).BeginInit();
            this.SuspendLayout();
            // 
            // Frame3
            // 
            this.Frame3.BackColor = System.Drawing.Color.Transparent;
            this.Frame3.Controls.Add(this.tableLayoutPanel4);
            this.Frame3.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Frame3.Location = new System.Drawing.Point(19, 116);
            this.Frame3.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.Frame3.Name = "Frame3";
            this.Frame3.Size = new System.Drawing.Size(597, 272);
            this.Frame3.TabIndex = 628;
            this.Frame3.TabStop = false;
            this.Frame3.Text = "Interlocuteur";
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 4;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33444F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33445F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33112F));
            this.tableLayoutPanel4.Controls.Add(this.txtCodeOrigine2, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.Label22, 1, 5);
            this.tableLayoutPanel4.Controls.Add(this.lbllibCodeParticulier, 3, 8);
            this.tableLayoutPanel4.Controls.Add(this.txtCodeParticulier, 1, 8);
            this.tableLayoutPanel4.Controls.Add(this.txtSource, 2, 4);
            this.tableLayoutPanel4.Controls.Add(this.lblSociete, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.txtSociete, 2, 1);
            this.tableLayoutPanel4.Controls.Add(this.lblLibIntervenantDT, 3, 0);
            this.tableLayoutPanel4.Controls.Add(this.optQuiSociete, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.optQuiGerant, 0, 1);
            this.tableLayoutPanel4.Controls.Add(this.optQuiGardien, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.optQuiAutre, 0, 3);
            this.tableLayoutPanel4.Controls.Add(this.optQuiFournisseur, 0, 4);
            this.tableLayoutPanel4.Controls.Add(this.optQuiCopro, 0, 5);
            this.tableLayoutPanel4.Controls.Add(this.OptQuiGestionnaire, 0, 6);
            this.tableLayoutPanel4.Controls.Add(this.cmbIntervenantDT, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.cmdIntervenant, 2, 0);
            this.tableLayoutPanel4.Controls.Add(this.cmdEctituresCopro, 2, 8);
            this.tableLayoutPanel4.Controls.Add(this.tableLayoutPanel26, 1, 4);
            this.tableLayoutPanel4.Controls.Add(this.lblParticulier, 0, 8);
            this.tableLayoutPanel4.Controls.Add(this.cmdAjoutgestion, 2, 7);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 9;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 12F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(591, 249);
            this.tableLayoutPanel4.TabIndex = 0;
            // 
            // txtCodeOrigine2
            // 
            this.txtCodeOrigine2.AccAcceptNumbersOnly = false;
            this.txtCodeOrigine2.AccAllowComma = false;
            this.txtCodeOrigine2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeOrigine2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeOrigine2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeOrigine2.AccHidenValue = "";
            this.txtCodeOrigine2.AccNotAllowedChars = null;
            this.txtCodeOrigine2.AccReadOnly = true;
            this.txtCodeOrigine2.AccReadOnlyAllowDelete = false;
            this.txtCodeOrigine2.AccRequired = false;
            this.txtCodeOrigine2.BackColor = System.Drawing.Color.White;
            this.txtCodeOrigine2.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeOrigine2.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeOrigine2.ForeColor = System.Drawing.Color.Black;
            this.txtCodeOrigine2.Location = new System.Drawing.Point(189, 64);
            this.txtCodeOrigine2.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeOrigine2.MaxLength = 32767;
            this.txtCodeOrigine2.Multiline = false;
            this.txtCodeOrigine2.Name = "txtCodeOrigine2";
            this.txtCodeOrigine2.ReadOnly = false;
            this.txtCodeOrigine2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeOrigine2.Size = new System.Drawing.Size(57, 27);
            this.txtCodeOrigine2.TabIndex = 592;
            this.txtCodeOrigine2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeOrigine2.UseSystemPasswordChar = false;
            this.txtCodeOrigine2.Visible = false;
            // 
            // Label22
            // 
            this.Label22.AutoSize = true;
            this.Label22.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Label22.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label22.Location = new System.Drawing.Point(190, 155);
            this.Label22.Name = "Label22";
            this.Label22.Size = new System.Drawing.Size(181, 31);
            this.Label22.TabIndex = 589;
            this.Label22.Text = "hors syndics";
            this.Label22.TextAlign = System.Drawing.ContentAlignment.MiddleLeft;
            // 
            // lbllibCodeParticulier
            // 
            this.lbllibCodeParticulier.AccAcceptNumbersOnly = false;
            this.lbllibCodeParticulier.AccAllowComma = false;
            this.lbllibCodeParticulier.AccBackgroundColor = System.Drawing.Color.White;
            this.lbllibCodeParticulier.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lbllibCodeParticulier.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lbllibCodeParticulier.AccHidenValue = "";
            this.lbllibCodeParticulier.AccNotAllowedChars = null;
            this.lbllibCodeParticulier.AccReadOnly = false;
            this.lbllibCodeParticulier.AccReadOnlyAllowDelete = false;
            this.lbllibCodeParticulier.AccRequired = false;
            this.lbllibCodeParticulier.BackColor = System.Drawing.Color.White;
            this.lbllibCodeParticulier.CustomBackColor = System.Drawing.Color.White;
            this.lbllibCodeParticulier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbllibCodeParticulier.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lbllibCodeParticulier.ForeColor = System.Drawing.Color.Black;
            this.lbllibCodeParticulier.Location = new System.Drawing.Point(406, 250);
            this.lbllibCodeParticulier.Margin = new System.Windows.Forms.Padding(2);
            this.lbllibCodeParticulier.MaxLength = 32767;
            this.lbllibCodeParticulier.Multiline = false;
            this.lbllibCodeParticulier.Name = "lbllibCodeParticulier";
            this.lbllibCodeParticulier.ReadOnly = false;
            this.lbllibCodeParticulier.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lbllibCodeParticulier.Size = new System.Drawing.Size(183, 27);
            this.lbllibCodeParticulier.TabIndex = 586;
            this.lbllibCodeParticulier.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lbllibCodeParticulier.UseSystemPasswordChar = false;
            this.lbllibCodeParticulier.Visible = false;
            // 
            // txtCodeParticulier
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtCodeParticulier.DisplayLayout.Appearance = appearance1;
            this.txtCodeParticulier.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtCodeParticulier.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.txtCodeParticulier.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtCodeParticulier.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.txtCodeParticulier.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtCodeParticulier.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.txtCodeParticulier.DisplayLayout.MaxColScrollRegions = 1;
            this.txtCodeParticulier.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtCodeParticulier.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txtCodeParticulier.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.txtCodeParticulier.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtCodeParticulier.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.txtCodeParticulier.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtCodeParticulier.DisplayLayout.Override.CellAppearance = appearance8;
            this.txtCodeParticulier.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtCodeParticulier.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.txtCodeParticulier.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.txtCodeParticulier.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.txtCodeParticulier.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtCodeParticulier.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.txtCodeParticulier.DisplayLayout.Override.RowAppearance = appearance11;
            this.txtCodeParticulier.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtCodeParticulier.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.txtCodeParticulier.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtCodeParticulier.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtCodeParticulier.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtCodeParticulier.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeParticulier.Location = new System.Drawing.Point(190, 251);
            this.txtCodeParticulier.Name = "txtCodeParticulier";
            this.txtCodeParticulier.Size = new System.Drawing.Size(181, 27);
            this.txtCodeParticulier.TabIndex = 584;
            this.txtCodeParticulier.Visible = false;
            this.txtCodeParticulier.AfterCloseUp += new System.EventHandler(this.txtCodeParticulier_AfterCloseUp);
            this.txtCodeParticulier.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.txtCodeParticulier_BeforeDropDown);
            // 
            // txtSource
            // 
            this.txtSource.AccAcceptNumbersOnly = false;
            this.txtSource.AccAllowComma = false;
            this.txtSource.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSource.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSource.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtSource.AccHidenValue = "";
            this.txtSource.AccNotAllowedChars = null;
            this.txtSource.AccReadOnly = false;
            this.txtSource.AccReadOnlyAllowDelete = false;
            this.txtSource.AccRequired = false;
            this.txtSource.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel4.SetColumnSpan(this.txtSource, 2);
            this.txtSource.CustomBackColor = System.Drawing.Color.White;
            this.txtSource.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSource.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtSource.ForeColor = System.Drawing.Color.Black;
            this.txtSource.Location = new System.Drawing.Point(376, 126);
            this.txtSource.Margin = new System.Windows.Forms.Padding(2);
            this.txtSource.MaxLength = 32767;
            this.txtSource.Multiline = true;
            this.txtSource.Name = "txtSource";
            this.txtSource.ReadOnly = false;
            this.tableLayoutPanel4.SetRowSpan(this.txtSource, 3);
            this.txtSource.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSource.Size = new System.Drawing.Size(213, 89);
            this.txtSource.TabIndex = 580;
            this.txtSource.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSource.UseSystemPasswordChar = false;
            this.txtSource.Visible = false;
            // 
            // lblSociete
            // 
            this.lblSociete.AutoSize = true;
            this.lblSociete.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblSociete.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSociete.Location = new System.Drawing.Point(310, 36);
            this.lblSociete.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.lblSociete.Name = "lblSociete";
            this.lblSociete.Size = new System.Drawing.Size(61, 26);
            this.lblSociete.TabIndex = 579;
            this.lblSociete.Text = "Société";
            this.lblSociete.Visible = false;
            // 
            // txtSociete
            // 
            this.txtSociete.AccAcceptNumbersOnly = false;
            this.txtSociete.AccAllowComma = false;
            this.txtSociete.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSociete.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSociete.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtSociete.AccHidenValue = "";
            this.txtSociete.AccNotAllowedChars = null;
            this.txtSociete.AccReadOnly = false;
            this.txtSociete.AccReadOnlyAllowDelete = false;
            this.txtSociete.AccRequired = false;
            this.txtSociete.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel4.SetColumnSpan(this.txtSociete, 2);
            this.txtSociete.CustomBackColor = System.Drawing.Color.White;
            this.txtSociete.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtSociete.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtSociete.ForeColor = System.Drawing.Color.Black;
            this.txtSociete.Location = new System.Drawing.Point(376, 33);
            this.txtSociete.Margin = new System.Windows.Forms.Padding(2);
            this.txtSociete.MaxLength = 32767;
            this.txtSociete.Multiline = true;
            this.txtSociete.Name = "txtSociete";
            this.txtSociete.ReadOnly = false;
            this.tableLayoutPanel4.SetRowSpan(this.txtSociete, 3);
            this.txtSociete.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSociete.Size = new System.Drawing.Size(213, 89);
            this.txtSociete.TabIndex = 504;
            this.txtSociete.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSociete.UseSystemPasswordChar = false;
            this.txtSociete.Visible = false;
            // 
            // lblLibIntervenantDT
            // 
            this.lblLibIntervenantDT.AccAcceptNumbersOnly = false;
            this.lblLibIntervenantDT.AccAllowComma = false;
            this.lblLibIntervenantDT.AccBackgroundColor = System.Drawing.Color.White;
            this.lblLibIntervenantDT.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblLibIntervenantDT.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblLibIntervenantDT.AccHidenValue = "";
            this.lblLibIntervenantDT.AccNotAllowedChars = null;
            this.lblLibIntervenantDT.AccReadOnly = false;
            this.lblLibIntervenantDT.AccReadOnlyAllowDelete = false;
            this.lblLibIntervenantDT.AccRequired = false;
            this.lblLibIntervenantDT.BackColor = System.Drawing.Color.White;
            this.lblLibIntervenantDT.CustomBackColor = System.Drawing.Color.White;
            this.lblLibIntervenantDT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibIntervenantDT.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lblLibIntervenantDT.ForeColor = System.Drawing.Color.Blue;
            this.lblLibIntervenantDT.Location = new System.Drawing.Point(406, 2);
            this.lblLibIntervenantDT.Margin = new System.Windows.Forms.Padding(2);
            this.lblLibIntervenantDT.MaxLength = 32767;
            this.lblLibIntervenantDT.Multiline = false;
            this.lblLibIntervenantDT.Name = "lblLibIntervenantDT";
            this.lblLibIntervenantDT.ReadOnly = false;
            this.lblLibIntervenantDT.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblLibIntervenantDT.Size = new System.Drawing.Size(183, 27);
            this.lblLibIntervenantDT.TabIndex = 578;
            this.lblLibIntervenantDT.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblLibIntervenantDT.UseSystemPasswordChar = false;
            // 
            // optQuiSociete
            // 
            this.optQuiSociete.AutoSize = true;
            this.optQuiSociete.Checked = true;
            this.optQuiSociete.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optQuiSociete.Location = new System.Drawing.Point(3, 3);
            this.optQuiSociete.Name = "optQuiSociete";
            this.optQuiSociete.Size = new System.Drawing.Size(78, 23);
            this.optQuiSociete.TabIndex = 543;
            this.optQuiSociete.TabStop = true;
            this.optQuiSociete.Text = "Interne";
            this.optQuiSociete.UseVisualStyleBackColor = true;
            this.optQuiSociete.CheckedChanged += new System.EventHandler(this.optQuiSociete_CheckedChanged);
            // 
            // optQuiGerant
            // 
            this.optQuiGerant.AutoSize = true;
            this.optQuiGerant.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optQuiGerant.Location = new System.Drawing.Point(3, 34);
            this.optQuiGerant.Name = "optQuiGerant";
            this.optQuiGerant.Size = new System.Drawing.Size(68, 23);
            this.optQuiGerant.TabIndex = 544;
            this.optQuiGerant.Text = "Client";
            this.optQuiGerant.UseVisualStyleBackColor = true;
            this.optQuiGerant.CheckedChanged += new System.EventHandler(this.optQuiGerant_CheckedChanged);
            // 
            // optQuiGardien
            // 
            this.optQuiGardien.AutoSize = true;
            this.optQuiGardien.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optQuiGardien.Location = new System.Drawing.Point(3, 65);
            this.optQuiGardien.Name = "optQuiGardien";
            this.optQuiGardien.Size = new System.Drawing.Size(81, 23);
            this.optQuiGardien.TabIndex = 545;
            this.optQuiGardien.Text = "Gardien";
            this.optQuiGardien.UseVisualStyleBackColor = true;
            this.optQuiGardien.CheckedChanged += new System.EventHandler(this.optQuiGardien_CheckedChanged);
            // 
            // optQuiAutre
            // 
            this.optQuiAutre.AutoSize = true;
            this.optQuiAutre.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optQuiAutre.Location = new System.Drawing.Point(3, 96);
            this.optQuiAutre.Name = "optQuiAutre";
            this.optQuiAutre.Size = new System.Drawing.Size(81, 23);
            this.optQuiAutre.TabIndex = 546;
            this.optQuiAutre.Text = "Gardien";
            this.optQuiAutre.UseVisualStyleBackColor = true;
            this.optQuiAutre.CheckedChanged += new System.EventHandler(this.optQuiAutre_CheckedChanged);
            // 
            // optQuiFournisseur
            // 
            this.optQuiFournisseur.AutoSize = true;
            this.optQuiFournisseur.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optQuiFournisseur.Location = new System.Drawing.Point(3, 127);
            this.optQuiFournisseur.Name = "optQuiFournisseur";
            this.optQuiFournisseur.Size = new System.Drawing.Size(109, 23);
            this.optQuiFournisseur.TabIndex = 547;
            this.optQuiFournisseur.Text = "Fournisseur";
            this.optQuiFournisseur.UseVisualStyleBackColor = true;
            this.optQuiFournisseur.CheckedChanged += new System.EventHandler(this.optQuiFournisseur_CheckedChanged);
            // 
            // optQuiCopro
            // 
            this.optQuiCopro.AutoSize = true;
            this.optQuiCopro.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.optQuiCopro.Location = new System.Drawing.Point(3, 158);
            this.optQuiCopro.Name = "optQuiCopro";
            this.optQuiCopro.Size = new System.Drawing.Size(181, 23);
            this.optQuiCopro.TabIndex = 548;
            this.optQuiCopro.Text = "Adresse de facturation\r\n";
            this.optQuiCopro.UseVisualStyleBackColor = true;
            this.optQuiCopro.CheckedChanged += new System.EventHandler(this.optQuiCopro_CheckedChanged);
            // 
            // OptQuiGestionnaire
            // 
            this.OptQuiGestionnaire.AutoSize = true;
            this.OptQuiGestionnaire.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptQuiGestionnaire.Location = new System.Drawing.Point(3, 189);
            this.OptQuiGestionnaire.Name = "OptQuiGestionnaire";
            this.OptQuiGestionnaire.Size = new System.Drawing.Size(115, 23);
            this.OptQuiGestionnaire.TabIndex = 549;
            this.OptQuiGestionnaire.Text = "Gestionnaire";
            this.OptQuiGestionnaire.UseVisualStyleBackColor = true;
            this.OptQuiGestionnaire.CheckedChanged += new System.EventHandler(this.OptQuiGestionnaire_CheckedChanged);
            // 
            // cmbIntervenantDT
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbIntervenantDT.DisplayLayout.Appearance = appearance13;
            this.cmbIntervenantDT.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbIntervenantDT.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenantDT.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbIntervenantDT.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.cmbIntervenantDT.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbIntervenantDT.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.cmbIntervenantDT.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbIntervenantDT.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbIntervenantDT.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbIntervenantDT.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.cmbIntervenantDT.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbIntervenantDT.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenantDT.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbIntervenantDT.DisplayLayout.Override.CellAppearance = appearance20;
            this.cmbIntervenantDT.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbIntervenantDT.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbIntervenantDT.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.cmbIntervenantDT.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.cmbIntervenantDT.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbIntervenantDT.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.cmbIntervenantDT.DisplayLayout.Override.RowAppearance = appearance23;
            this.cmbIntervenantDT.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbIntervenantDT.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.cmbIntervenantDT.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbIntervenantDT.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbIntervenantDT.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbIntervenantDT.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbIntervenantDT.Location = new System.Drawing.Point(190, 3);
            this.cmbIntervenantDT.Name = "cmbIntervenantDT";
            this.cmbIntervenantDT.Size = new System.Drawing.Size(181, 27);
            this.cmbIntervenantDT.TabIndex = 550;
            this.cmbIntervenantDT.AfterCloseUp += new System.EventHandler(this.cmbIntervenantDT_AfterCloseUp);
            this.cmbIntervenantDT.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbIntervenantDT_BeforeDropDown);
            this.cmbIntervenantDT.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.cmbIntervenantDT_KeyPress);
            this.cmbIntervenantDT.Validating += new System.ComponentModel.CancelEventHandler(this.cmbIntervenantDT_Validating);
            // 
            // cmdIntervenant
            // 
            this.cmdIntervenant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdIntervenant.FlatAppearance.BorderSize = 0;
            this.cmdIntervenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdIntervenant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdIntervenant.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdIntervenant.Location = new System.Drawing.Point(376, 2);
            this.cmdIntervenant.Margin = new System.Windows.Forms.Padding(2, 2, 0, 0);
            this.cmdIntervenant.Name = "cmdIntervenant";
            this.cmdIntervenant.Size = new System.Drawing.Size(25, 20);
            this.cmdIntervenant.TabIndex = 551;
            this.cmdIntervenant.UseVisualStyleBackColor = false;
            this.cmdIntervenant.Click += new System.EventHandler(this.cmdIntervenant_Click);
            // 
            // cmdEctituresCopro
            // 
            this.cmdEctituresCopro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdEctituresCopro.FlatAppearance.BorderSize = 0;
            this.cmdEctituresCopro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdEctituresCopro.Font = new System.Drawing.Font("Ubuntu", 9F);
            this.cmdEctituresCopro.ForeColor = System.Drawing.Color.White;
            this.cmdEctituresCopro.Location = new System.Drawing.Point(377, 251);
            this.cmdEctituresCopro.Name = "cmdEctituresCopro";
            this.cmdEctituresCopro.Size = new System.Drawing.Size(24, 6);
            this.cmdEctituresCopro.TabIndex = 585;
            this.cmdEctituresCopro.Text = "?";
            this.cmdEctituresCopro.UseVisualStyleBackColor = false;
            this.cmdEctituresCopro.Visible = false;
            // 
            // tableLayoutPanel26
            // 
            this.tableLayoutPanel26.ColumnCount = 2;
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.Controls.Add(this.cmdRechercheFour, 0, 0);
            this.tableLayoutPanel26.Controls.Add(this.lblSource, 1, 0);
            this.tableLayoutPanel26.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel26.Location = new System.Drawing.Point(187, 124);
            this.tableLayoutPanel26.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel26.Name = "tableLayoutPanel26";
            this.tableLayoutPanel26.RowCount = 1;
            this.tableLayoutPanel26.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel26.Size = new System.Drawing.Size(187, 31);
            this.tableLayoutPanel26.TabIndex = 587;
            // 
            // cmdRechercheFour
            // 
            this.cmdRechercheFour.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheFour.FlatAppearance.BorderSize = 0;
            this.cmdRechercheFour.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheFour.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheFour.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheFour.Location = new System.Drawing.Point(0, 2);
            this.cmdRechercheFour.Margin = new System.Windows.Forms.Padding(0, 2, 0, 0);
            this.cmdRechercheFour.Name = "cmdRechercheFour";
            this.cmdRechercheFour.Size = new System.Drawing.Size(25, 20);
            this.cmdRechercheFour.TabIndex = 582;
            this.cmdRechercheFour.UseVisualStyleBackColor = false;
            this.cmdRechercheFour.Visible = false;
            this.cmdRechercheFour.Click += new System.EventHandler(this.cmdRechercheFour_Click);
            // 
            // lblSource
            // 
            this.lblSource.AutoSize = true;
            this.lblSource.Dock = System.Windows.Forms.DockStyle.Right;
            this.lblSource.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblSource.Location = new System.Drawing.Point(142, 5);
            this.lblSource.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.lblSource.Name = "lblSource";
            this.lblSource.Size = new System.Drawing.Size(42, 26);
            this.lblSource.TabIndex = 581;
            this.lblSource.Text = "Nom";
            this.lblSource.Visible = false;
            // 
            // lblParticulier
            // 
            this.lblParticulier.AutoSize = true;
            this.lblParticulier.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblParticulier.Location = new System.Drawing.Point(3, 254);
            this.lblParticulier.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.lblParticulier.Name = "lblParticulier";
            this.lblParticulier.Size = new System.Drawing.Size(80, 6);
            this.lblParticulier.TabIndex = 583;
            this.lblParticulier.Text = "Particulier";
            this.lblParticulier.Visible = false;
            // 
            // cmdAjoutgestion
            // 
            this.cmdAjoutgestion.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(168)))), ((int)(((byte)(204)))));
            this.tableLayoutPanel4.SetColumnSpan(this.cmdAjoutgestion, 2);
            this.cmdAjoutgestion.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjoutgestion.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmdAjoutgestion.FlatAppearance.BorderSize = 0;
            this.cmdAjoutgestion.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjoutgestion.Font = new System.Drawing.Font("Ubuntu", 9F);
            this.cmdAjoutgestion.ForeColor = System.Drawing.Color.White;
            this.cmdAjoutgestion.Location = new System.Drawing.Point(376, 219);
            this.cmdAjoutgestion.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjoutgestion.Name = "cmdAjoutgestion";
            this.cmdAjoutgestion.Size = new System.Drawing.Size(213, 27);
            this.cmdAjoutgestion.TabIndex = 591;
            this.cmdAjoutgestion.Text = "Ajouter Gestionnaire";
            this.cmdAjoutgestion.UseVisualStyleBackColor = false;
            this.cmdAjoutgestion.Visible = false;
            this.cmdAjoutgestion.Click += new System.EventHandler(this.cmdAjoutgestion_Click);
            // 
            // tableLayoutPanel18
            // 
            this.tableLayoutPanel18.ColumnCount = 3;
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel18.Controls.Add(this.label6, 0, 1);
            this.tableLayoutPanel18.Controls.Add(this.lbllibCAI_CategorInterv, 2, 0);
            this.tableLayoutPanel18.Controls.Add(this.cmbCAI_CategorInterv, 1, 0);
            this.tableLayoutPanel18.Controls.Add(this.label23, 0, 0);
            this.tableLayoutPanel18.Controls.Add(this.txtObservations, 1, 1);
            this.tableLayoutPanel18.Location = new System.Drawing.Point(19, 388);
            this.tableLayoutPanel18.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel18.Name = "tableLayoutPanel18";
            this.tableLayoutPanel18.RowCount = 2;
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel18.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel18.Size = new System.Drawing.Size(597, 80);
            this.tableLayoutPanel18.TabIndex = 629;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label6.Location = new System.Drawing.Point(3, 35);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(171, 19);
            this.label6.TabIndex = 588;
            this.label6.Text = "Commentaire sur appel";
            // 
            // lbllibCAI_CategorInterv
            // 
            this.lbllibCAI_CategorInterv.AccAcceptNumbersOnly = false;
            this.lbllibCAI_CategorInterv.AccAllowComma = false;
            this.lbllibCAI_CategorInterv.AccBackgroundColor = System.Drawing.Color.White;
            this.lbllibCAI_CategorInterv.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lbllibCAI_CategorInterv.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lbllibCAI_CategorInterv.AccHidenValue = "";
            this.lbllibCAI_CategorInterv.AccNotAllowedChars = null;
            this.lbllibCAI_CategorInterv.AccReadOnly = false;
            this.lbllibCAI_CategorInterv.AccReadOnlyAllowDelete = false;
            this.lbllibCAI_CategorInterv.AccRequired = false;
            this.lbllibCAI_CategorInterv.BackColor = System.Drawing.Color.White;
            this.lbllibCAI_CategorInterv.CustomBackColor = System.Drawing.Color.White;
            this.lbllibCAI_CategorInterv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbllibCAI_CategorInterv.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lbllibCAI_CategorInterv.ForeColor = System.Drawing.Color.Blue;
            this.lbllibCAI_CategorInterv.Location = new System.Drawing.Point(389, 5);
            this.lbllibCAI_CategorInterv.Margin = new System.Windows.Forms.Padding(2, 5, 2, 2);
            this.lbllibCAI_CategorInterv.MaxLength = 32767;
            this.lbllibCAI_CategorInterv.Multiline = false;
            this.lbllibCAI_CategorInterv.Name = "lbllibCAI_CategorInterv";
            this.lbllibCAI_CategorInterv.ReadOnly = false;
            this.lbllibCAI_CategorInterv.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lbllibCAI_CategorInterv.Size = new System.Drawing.Size(206, 27);
            this.lbllibCAI_CategorInterv.TabIndex = 587;
            this.lbllibCAI_CategorInterv.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lbllibCAI_CategorInterv.UseSystemPasswordChar = false;
            // 
            // cmbCAI_CategorInterv
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.cmbCAI_CategorInterv.DisplayLayout.Appearance = appearance25;
            this.cmbCAI_CategorInterv.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.cmbCAI_CategorInterv.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCAI_CategorInterv.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCAI_CategorInterv.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.cmbCAI_CategorInterv.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.cmbCAI_CategorInterv.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.cmbCAI_CategorInterv.DisplayLayout.MaxColScrollRegions = 1;
            this.cmbCAI_CategorInterv.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.CellAppearance = appearance32;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.cmbCAI_CategorInterv.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.RowAppearance = appearance35;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.cmbCAI_CategorInterv.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.cmbCAI_CategorInterv.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.cmbCAI_CategorInterv.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.cmbCAI_CategorInterv.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.cmbCAI_CategorInterv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.cmbCAI_CategorInterv.Location = new System.Drawing.Point(180, 3);
            this.cmbCAI_CategorInterv.Name = "cmbCAI_CategorInterv";
            this.cmbCAI_CategorInterv.Size = new System.Drawing.Size(204, 22);
            this.cmbCAI_CategorInterv.TabIndex = 586;
            this.cmbCAI_CategorInterv.AfterCloseUp += new System.EventHandler(this.cmbCAI_CategorInterv_AfterCloseUp);
            this.cmbCAI_CategorInterv.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.cmbCAI_CategorInterv_BeforeDropDown);
            this.cmbCAI_CategorInterv.Validating += new System.ComponentModel.CancelEventHandler(this.cmbCAI_CategorInterv_Validating);
            // 
            // label23
            // 
            this.label23.AutoSize = true;
            this.label23.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label23.Location = new System.Drawing.Point(3, 5);
            this.label23.Margin = new System.Windows.Forms.Padding(3, 5, 3, 0);
            this.label23.Name = "label23";
            this.label23.Size = new System.Drawing.Size(46, 19);
            this.label23.TabIndex = 511;
            this.label23.Text = "Motif";
            // 
            // txtObservations
            // 
            this.txtObservations.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtObservations.AutoWordSelection = false;
            this.txtObservations.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel18.SetColumnSpan(this.txtObservations, 2);
            this.txtObservations.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtObservations.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtObservations.ForeColor = System.Drawing.Color.Black;
            this.txtObservations.Location = new System.Drawing.Point(180, 33);
            this.txtObservations.Name = "txtObservations";
            this.txtObservations.ReadOnly = false;
            this.txtObservations.Size = new System.Drawing.Size(414, 44);
            this.txtObservations.TabIndex = 589;
            this.txtObservations.WordWrap = true;
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Location = new System.Drawing.Point(134, 102);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(118, 13);
            this.label5.TabIndex = 627;
            this.label5.Text = "-------------------------------------";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Location = new System.Drawing.Point(354, 102);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(124, 13);
            this.label4.TabIndex = 626;
            this.label4.Text = "---------------------------------------";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(258, 99);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(96, 17);
            this.label3.TabIndex = 625;
            this.label3.Text = "FICHE APPEL";
            // 
            // cmdCreateInter
            // 
            this.cmdCreateInter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdCreateInter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdCreateInter.FlatAppearance.BorderSize = 0;
            this.cmdCreateInter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdCreateInter.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdCreateInter.ForeColor = System.Drawing.Color.White;
            this.cmdCreateInter.Location = new System.Drawing.Point(489, 15);
            this.cmdCreateInter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdCreateInter.Name = "cmdCreateInter";
            this.cmdCreateInter.Size = new System.Drawing.Size(145, 35);
            this.cmdCreateInter.TabIndex = 624;
            this.cmdCreateInter.Text = "Crée une intervention";
            this.cmdCreateInter.UseVisualStyleBackColor = false;
            this.cmdCreateInter.Click += new System.EventHandler(this.cmdCreateInter_Click);
            // 
            // txtcontrat
            // 
            this.txtcontrat.AccAcceptNumbersOnly = false;
            this.txtcontrat.AccAllowComma = false;
            this.txtcontrat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtcontrat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtcontrat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtcontrat.AccHidenValue = "";
            this.txtcontrat.AccNotAllowedChars = null;
            this.txtcontrat.AccReadOnly = false;
            this.txtcontrat.AccReadOnlyAllowDelete = false;
            this.txtcontrat.AccRequired = false;
            this.txtcontrat.BackColor = System.Drawing.Color.White;
            this.txtcontrat.CustomBackColor = System.Drawing.Color.White;
            this.txtcontrat.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtcontrat.ForeColor = System.Drawing.Color.Black;
            this.txtcontrat.Location = new System.Drawing.Point(133, 70);
            this.txtcontrat.Margin = new System.Windows.Forms.Padding(2);
            this.txtcontrat.MaxLength = 32767;
            this.txtcontrat.Multiline = false;
            this.txtcontrat.Name = "txtcontrat";
            this.txtcontrat.ReadOnly = true;
            this.txtcontrat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtcontrat.Size = new System.Drawing.Size(334, 27);
            this.txtcontrat.TabIndex = 623;
            this.txtcontrat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtcontrat.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(12, 71);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(63, 19);
            this.label2.TabIndex = 622;
            this.label2.Text = "Contrat";
            // 
            // txtCodeimmeuble
            // 
            this.txtCodeimmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeimmeuble.AccAllowComma = false;
            this.txtCodeimmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeimmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeimmeuble.AccHidenValue = "";
            this.txtCodeimmeuble.AccNotAllowedChars = null;
            this.txtCodeimmeuble.AccReadOnly = false;
            this.txtCodeimmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeimmeuble.AccRequired = false;
            this.txtCodeimmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeimmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeimmeuble.Location = new System.Drawing.Point(133, 9);
            this.txtCodeimmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeimmeuble.MaxLength = 32767;
            this.txtCodeimmeuble.Multiline = false;
            this.txtCodeimmeuble.Name = "txtCodeimmeuble";
            this.txtCodeimmeuble.ReadOnly = true;
            this.txtCodeimmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeimmeuble.Size = new System.Drawing.Size(334, 27);
            this.txtCodeimmeuble.TabIndex = 621;
            this.txtCodeimmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeimmeuble.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(12, 13);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 19);
            this.label1.TabIndex = 620;
            this.label1.Text = "Code immeuble";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(12, 39);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(96, 19);
            this.label33.TabIndex = 618;
            this.label33.Text = "Code gérant";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 180);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(78, 19);
            this.label15.TabIndex = 511;
            this.label15.Text = "Code Etat";
            // 
            // txtDesignation
            // 
            this.txtDesignation.AccAcceptNumbersOnly = false;
            this.txtDesignation.AccAllowComma = false;
            this.txtDesignation.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDesignation.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDesignation.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDesignation.AccHidenValue = "";
            this.txtDesignation.AccNotAllowedChars = null;
            this.txtDesignation.AccReadOnly = false;
            this.txtDesignation.AccReadOnlyAllowDelete = false;
            this.txtDesignation.AccRequired = false;
            this.txtDesignation.BackColor = System.Drawing.Color.White;
            this.txtDesignation.CustomBackColor = System.Drawing.Color.White;
            this.txtDesignation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtDesignation.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtDesignation.ForeColor = System.Drawing.Color.Blue;
            this.txtDesignation.Location = new System.Drawing.Point(188, 32);
            this.txtDesignation.Margin = new System.Windows.Forms.Padding(2);
            this.txtDesignation.MaxLength = 32767;
            this.txtDesignation.Multiline = false;
            this.txtDesignation.Name = "txtDesignation";
            this.txtDesignation.ReadOnly = false;
            this.txtDesignation.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDesignation.Size = new System.Drawing.Size(186, 27);
            this.txtDesignation.TabIndex = 517;
            this.txtDesignation.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDesignation.UseSystemPasswordChar = false;
            // 
            // txtNoIntervention
            // 
            this.txtNoIntervention.AccAcceptNumbersOnly = false;
            this.txtNoIntervention.AccAllowComma = false;
            this.txtNoIntervention.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNoIntervention.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNoIntervention.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNoIntervention.AccHidenValue = "";
            this.txtNoIntervention.AccNotAllowedChars = null;
            this.txtNoIntervention.AccReadOnly = true;
            this.txtNoIntervention.AccReadOnlyAllowDelete = false;
            this.txtNoIntervention.AccRequired = false;
            this.txtNoIntervention.BackColor = System.Drawing.Color.White;
            this.txtNoIntervention.CustomBackColor = System.Drawing.Color.White;
            this.txtNoIntervention.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNoIntervention.ForeColor = System.Drawing.Color.Black;
            this.txtNoIntervention.Location = new System.Drawing.Point(577, 57);
            this.txtNoIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.txtNoIntervention.MaxLength = 32767;
            this.txtNoIntervention.Multiline = false;
            this.txtNoIntervention.Name = "txtNoIntervention";
            this.txtNoIntervention.ReadOnly = false;
            this.txtNoIntervention.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNoIntervention.Size = new System.Drawing.Size(57, 27);
            this.txtNoIntervention.TabIndex = 637;
            this.txtNoIntervention.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNoIntervention.UseSystemPasswordChar = false;
            this.txtNoIntervention.Visible = false;
            // 
            // txtIntervenant
            // 
            this.txtIntervenant.AccAcceptNumbersOnly = false;
            this.txtIntervenant.AccAllowComma = false;
            this.txtIntervenant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntervenant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntervenant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntervenant.AccHidenValue = "";
            this.txtIntervenant.AccNotAllowedChars = null;
            this.txtIntervenant.AccReadOnly = true;
            this.txtIntervenant.AccReadOnlyAllowDelete = false;
            this.txtIntervenant.AccRequired = false;
            this.txtIntervenant.BackColor = System.Drawing.Color.White;
            this.txtIntervenant.CustomBackColor = System.Drawing.Color.White;
            this.txtIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntervenant.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtIntervenant.ForeColor = System.Drawing.Color.Black;
            this.txtIntervenant.Location = new System.Drawing.Point(188, 92);
            this.txtIntervenant.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntervenant.MaxLength = 32767;
            this.txtIntervenant.Multiline = false;
            this.txtIntervenant.Name = "txtIntervenant";
            this.txtIntervenant.ReadOnly = false;
            this.txtIntervenant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntervenant.Size = new System.Drawing.Size(186, 27);
            this.txtIntervenant.TabIndex = 519;
            this.txtIntervenant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntervenant.UseSystemPasswordChar = false;
            this.txtIntervenant.TextChanged += new System.EventHandler(this.txtIntervenant_TextChanged);
            this.txtIntervenant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIntervenant_KeyPress);
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Location = new System.Drawing.Point(136, 478);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(94, 13);
            this.label7.TabIndex = 632;
            this.label7.Text = "-----------------------------";
            // 
            // label8
            // 
            this.label8.AutoSize = true;
            this.label8.Location = new System.Drawing.Point(390, 479);
            this.label8.Name = "label8";
            this.label8.Size = new System.Drawing.Size(88, 13);
            this.label8.TabIndex = 631;
            this.label8.Text = "---------------------------";
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 4;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel9.Controls.Add(this.label11, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.label10, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.txtINT_AnaActivite, 3, 0);
            this.tableLayoutPanel9.Controls.Add(this.cmdRechercheIntervenant, 2, 3);
            this.tableLayoutPanel9.Controls.Add(this.cmdRechercheTech, 2, 2);
            this.tableLayoutPanel9.Controls.Add(this.txtIntervenant, 1, 3);
            this.tableLayoutPanel9.Controls.Add(this.txtLibDispatch, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.txtDesignation, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.label15, 0, 6);
            this.tableLayoutPanel9.Controls.Add(this.Command1, 2, 0);
            this.tableLayoutPanel9.Controls.Add(this.txtArticle, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.label12, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.label13, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.lblLibInterv, 3, 3);
            this.tableLayoutPanel9.Controls.Add(this.lbllibDisptach, 3, 2);
            this.tableLayoutPanel9.Controls.Add(this.txtCodeEtat, 1, 6);
            this.tableLayoutPanel9.Controls.Add(this.chkCompteurObli, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.chkUrgence, 0, 5);
            this.tableLayoutPanel9.Controls.Add(this.cmdRechercheCodetat, 2, 6);
            this.tableLayoutPanel9.Controls.Add(this.lblLibStatut, 3, 6);
            this.tableLayoutPanel9.Controls.Add(this.lblNomIntervenant, 3, 1);
            this.tableLayoutPanel9.Location = new System.Drawing.Point(19, 495);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 7;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(597, 216);
            this.tableLayoutPanel9.TabIndex = 633;
            // 
            // label11
            // 
            this.label11.AutoSize = true;
            this.label11.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label11.Location = new System.Drawing.Point(3, 30);
            this.label11.Name = "label11";
            this.label11.Size = new System.Drawing.Size(80, 19);
            this.label11.TabIndex = 590;
            this.label11.Text = "Opération";
            // 
            // label10
            // 
            this.label10.AutoSize = true;
            this.label10.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label10.Location = new System.Drawing.Point(3, 0);
            this.label10.Name = "label10";
            this.label10.Size = new System.Drawing.Size(117, 19);
            this.label10.TabIndex = 589;
            this.label10.Text = "Code opération";
            // 
            // txtINT_AnaActivite
            // 
            this.txtINT_AnaActivite.AccAcceptNumbersOnly = false;
            this.txtINT_AnaActivite.AccAllowComma = false;
            this.txtINT_AnaActivite.AccBackgroundColor = System.Drawing.Color.White;
            this.txtINT_AnaActivite.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtINT_AnaActivite.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtINT_AnaActivite.AccHidenValue = "";
            this.txtINT_AnaActivite.AccNotAllowedChars = null;
            this.txtINT_AnaActivite.AccReadOnly = false;
            this.txtINT_AnaActivite.AccReadOnlyAllowDelete = false;
            this.txtINT_AnaActivite.AccRequired = false;
            this.txtINT_AnaActivite.BackColor = System.Drawing.Color.White;
            this.txtINT_AnaActivite.CustomBackColor = System.Drawing.Color.White;
            this.txtINT_AnaActivite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtINT_AnaActivite.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtINT_AnaActivite.ForeColor = System.Drawing.Color.Blue;
            this.txtINT_AnaActivite.Location = new System.Drawing.Point(408, 2);
            this.txtINT_AnaActivite.Margin = new System.Windows.Forms.Padding(2);
            this.txtINT_AnaActivite.MaxLength = 32767;
            this.txtINT_AnaActivite.Multiline = false;
            this.txtINT_AnaActivite.Name = "txtINT_AnaActivite";
            this.txtINT_AnaActivite.ReadOnly = true;
            this.txtINT_AnaActivite.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtINT_AnaActivite.Size = new System.Drawing.Size(187, 27);
            this.txtINT_AnaActivite.TabIndex = 586;
            this.txtINT_AnaActivite.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtINT_AnaActivite.UseSystemPasswordChar = false;
            this.txtINT_AnaActivite.Visible = false;
            // 
            // cmdRechercheIntervenant
            // 
            this.cmdRechercheIntervenant.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdRechercheIntervenant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheIntervenant.FlatAppearance.BorderSize = 0;
            this.cmdRechercheIntervenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheIntervenant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheIntervenant.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheIntervenant.Location = new System.Drawing.Point(379, 93);
            this.cmdRechercheIntervenant.Name = "cmdRechercheIntervenant";
            this.cmdRechercheIntervenant.Size = new System.Drawing.Size(24, 19);
            this.cmdRechercheIntervenant.TabIndex = 585;
            this.cmdRechercheIntervenant.UseVisualStyleBackColor = false;
            this.cmdRechercheIntervenant.Click += new System.EventHandler(this.cmdRechercheIntervenant_Click);
            // 
            // cmdRechercheTech
            // 
            this.cmdRechercheTech.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdRechercheTech.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheTech.FlatAppearance.BorderSize = 0;
            this.cmdRechercheTech.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheTech.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheTech.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheTech.Location = new System.Drawing.Point(379, 63);
            this.cmdRechercheTech.Name = "cmdRechercheTech";
            this.cmdRechercheTech.Size = new System.Drawing.Size(24, 19);
            this.cmdRechercheTech.TabIndex = 584;
            this.cmdRechercheTech.UseVisualStyleBackColor = false;
            this.cmdRechercheTech.Click += new System.EventHandler(this.cmdRechercheTech_Click);
            // 
            // txtLibDispatch
            // 
            this.txtLibDispatch.AccAcceptNumbersOnly = false;
            this.txtLibDispatch.AccAllowComma = false;
            this.txtLibDispatch.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibDispatch.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibDispatch.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLibDispatch.AccHidenValue = "";
            this.txtLibDispatch.AccNotAllowedChars = null;
            this.txtLibDispatch.AccReadOnly = false;
            this.txtLibDispatch.AccReadOnlyAllowDelete = false;
            this.txtLibDispatch.AccRequired = false;
            this.txtLibDispatch.BackColor = System.Drawing.Color.White;
            this.txtLibDispatch.CustomBackColor = System.Drawing.Color.White;
            this.txtLibDispatch.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLibDispatch.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtLibDispatch.ForeColor = System.Drawing.Color.Blue;
            this.txtLibDispatch.Location = new System.Drawing.Point(188, 62);
            this.txtLibDispatch.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibDispatch.MaxLength = 32767;
            this.txtLibDispatch.Multiline = false;
            this.txtLibDispatch.Name = "txtLibDispatch";
            this.txtLibDispatch.ReadOnly = false;
            this.txtLibDispatch.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibDispatch.Size = new System.Drawing.Size(186, 27);
            this.txtLibDispatch.TabIndex = 518;
            this.txtLibDispatch.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibDispatch.UseSystemPasswordChar = false;
            this.txtLibDispatch.TextChanged += new System.EventHandler(this.txtLibDispatch_TextChanged);
            this.txtLibDispatch.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtLibDispatch_KeyPress);
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Command1.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.Command1.Location = new System.Drawing.Point(379, 3);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(24, 19);
            this.Command1.TabIndex = 584;
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // txtArticle
            // 
            this.txtArticle.AccAcceptNumbersOnly = false;
            this.txtArticle.AccAllowComma = false;
            this.txtArticle.AccBackgroundColor = System.Drawing.Color.White;
            this.txtArticle.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtArticle.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtArticle.AccHidenValue = "";
            this.txtArticle.AccNotAllowedChars = null;
            this.txtArticle.AccReadOnly = false;
            this.txtArticle.AccReadOnlyAllowDelete = false;
            this.txtArticle.AccRequired = false;
            this.txtArticle.BackColor = System.Drawing.Color.White;
            this.txtArticle.CustomBackColor = System.Drawing.Color.White;
            this.txtArticle.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtArticle.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtArticle.ForeColor = System.Drawing.Color.Blue;
            this.txtArticle.Location = new System.Drawing.Point(188, 2);
            this.txtArticle.Margin = new System.Windows.Forms.Padding(2);
            this.txtArticle.MaxLength = 32767;
            this.txtArticle.Multiline = false;
            this.txtArticle.Name = "txtArticle";
            this.txtArticle.ReadOnly = false;
            this.txtArticle.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtArticle.Size = new System.Drawing.Size(186, 27);
            this.txtArticle.TabIndex = 585;
            this.txtArticle.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtArticle.UseSystemPasswordChar = false;
            this.txtArticle.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtArticle_KeyPress);
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label12.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 60);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(180, 30);
            this.label12.TabIndex = 508;
            this.label12.Text = "Dispatcheur";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label13.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 90);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(180, 30);
            this.label13.TabIndex = 509;
            this.label13.Text = "Intervenant";
            // 
            // lblLibInterv
            // 
            this.lblLibInterv.AccAcceptNumbersOnly = false;
            this.lblLibInterv.AccAllowComma = false;
            this.lblLibInterv.AccBackgroundColor = System.Drawing.Color.White;
            this.lblLibInterv.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblLibInterv.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblLibInterv.AccHidenValue = "";
            this.lblLibInterv.AccNotAllowedChars = null;
            this.lblLibInterv.AccReadOnly = true;
            this.lblLibInterv.AccReadOnlyAllowDelete = false;
            this.lblLibInterv.AccRequired = false;
            this.lblLibInterv.BackColor = System.Drawing.Color.White;
            this.lblLibInterv.CustomBackColor = System.Drawing.Color.White;
            this.lblLibInterv.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibInterv.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lblLibInterv.ForeColor = System.Drawing.Color.Black;
            this.lblLibInterv.Location = new System.Drawing.Point(408, 92);
            this.lblLibInterv.Margin = new System.Windows.Forms.Padding(2);
            this.lblLibInterv.MaxLength = 32767;
            this.lblLibInterv.Multiline = false;
            this.lblLibInterv.Name = "lblLibInterv";
            this.lblLibInterv.ReadOnly = true;
            this.lblLibInterv.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblLibInterv.Size = new System.Drawing.Size(187, 27);
            this.lblLibInterv.TabIndex = 520;
            this.lblLibInterv.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblLibInterv.UseSystemPasswordChar = false;
            // 
            // lbllibDisptach
            // 
            this.lbllibDisptach.AccAcceptNumbersOnly = false;
            this.lbllibDisptach.AccAllowComma = false;
            this.lbllibDisptach.AccBackgroundColor = System.Drawing.Color.White;
            this.lbllibDisptach.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lbllibDisptach.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lbllibDisptach.AccHidenValue = "";
            this.lbllibDisptach.AccNotAllowedChars = null;
            this.lbllibDisptach.AccReadOnly = true;
            this.lbllibDisptach.AccReadOnlyAllowDelete = false;
            this.lbllibDisptach.AccRequired = false;
            this.lbllibDisptach.BackColor = System.Drawing.Color.White;
            this.lbllibDisptach.CustomBackColor = System.Drawing.Color.White;
            this.lbllibDisptach.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbllibDisptach.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lbllibDisptach.ForeColor = System.Drawing.Color.Black;
            this.lbllibDisptach.Location = new System.Drawing.Point(408, 62);
            this.lbllibDisptach.Margin = new System.Windows.Forms.Padding(2);
            this.lbllibDisptach.MaxLength = 32767;
            this.lbllibDisptach.Multiline = false;
            this.lbllibDisptach.Name = "lbllibDisptach";
            this.lbllibDisptach.ReadOnly = true;
            this.lbllibDisptach.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lbllibDisptach.Size = new System.Drawing.Size(187, 27);
            this.lbllibDisptach.TabIndex = 521;
            this.lbllibDisptach.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lbllibDisptach.UseSystemPasswordChar = false;
            // 
            // txtCodeEtat
            // 
            this.txtCodeEtat.AccAcceptNumbersOnly = false;
            this.txtCodeEtat.AccAllowComma = false;
            this.txtCodeEtat.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeEtat.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeEtat.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeEtat.AccHidenValue = "";
            this.txtCodeEtat.AccNotAllowedChars = null;
            this.txtCodeEtat.AccReadOnly = false;
            this.txtCodeEtat.AccReadOnlyAllowDelete = false;
            this.txtCodeEtat.AccRequired = false;
            this.txtCodeEtat.BackColor = System.Drawing.Color.White;
            this.txtCodeEtat.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeEtat.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeEtat.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeEtat.ForeColor = System.Drawing.Color.Black;
            this.txtCodeEtat.Location = new System.Drawing.Point(188, 182);
            this.txtCodeEtat.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeEtat.MaxLength = 32767;
            this.txtCodeEtat.Multiline = false;
            this.txtCodeEtat.Name = "txtCodeEtat";
            this.txtCodeEtat.ReadOnly = false;
            this.txtCodeEtat.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeEtat.Size = new System.Drawing.Size(186, 27);
            this.txtCodeEtat.TabIndex = 522;
            this.txtCodeEtat.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeEtat.UseSystemPasswordChar = false;
            this.txtCodeEtat.TextChanged += new System.EventHandler(this.txtCodeEtat_TextChanged);
            this.txtCodeEtat.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeEtat_KeyPress);
            // 
            // chkCompteurObli
            // 
            this.chkCompteurObli.AutoSize = true;
            this.chkCompteurObli.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkCompteurObli.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.chkCompteurObli.Location = new System.Drawing.Point(3, 123);
            this.chkCompteurObli.Name = "chkCompteurObli";
            this.chkCompteurObli.Size = new System.Drawing.Size(180, 24);
            this.chkCompteurObli.TabIndex = 571;
            this.chkCompteurObli.Text = "Compteur Obligatoire";
            this.chkCompteurObli.UseVisualStyleBackColor = true;
            // 
            // chkUrgence
            // 
            this.chkUrgence.AutoSize = true;
            this.chkUrgence.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkUrgence.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.chkUrgence.Location = new System.Drawing.Point(3, 153);
            this.chkUrgence.Name = "chkUrgence";
            this.chkUrgence.Size = new System.Drawing.Size(180, 24);
            this.chkUrgence.TabIndex = 570;
            this.chkUrgence.Text = "Urgent";
            this.chkUrgence.UseVisualStyleBackColor = true;
            // 
            // cmdRechercheCodetat
            // 
            this.cmdRechercheCodetat.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.cmdRechercheCodetat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheCodetat.FlatAppearance.BorderSize = 0;
            this.cmdRechercheCodetat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheCodetat.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheCodetat.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheCodetat.Location = new System.Drawing.Point(379, 183);
            this.cmdRechercheCodetat.Name = "cmdRechercheCodetat";
            this.cmdRechercheCodetat.Size = new System.Drawing.Size(24, 18);
            this.cmdRechercheCodetat.TabIndex = 587;
            this.cmdRechercheCodetat.UseVisualStyleBackColor = false;
            this.cmdRechercheCodetat.Click += new System.EventHandler(this.cmdRechercheCodetat_Click);
            // 
            // lblLibStatut
            // 
            this.lblLibStatut.AccAcceptNumbersOnly = false;
            this.lblLibStatut.AccAllowComma = false;
            this.lblLibStatut.AccBackgroundColor = System.Drawing.Color.White;
            this.lblLibStatut.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblLibStatut.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblLibStatut.AccHidenValue = "";
            this.lblLibStatut.AccNotAllowedChars = null;
            this.lblLibStatut.AccReadOnly = true;
            this.lblLibStatut.AccReadOnlyAllowDelete = false;
            this.lblLibStatut.AccRequired = false;
            this.lblLibStatut.BackColor = System.Drawing.Color.White;
            this.lblLibStatut.CustomBackColor = System.Drawing.Color.White;
            this.lblLibStatut.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibStatut.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lblLibStatut.ForeColor = System.Drawing.Color.Black;
            this.lblLibStatut.Location = new System.Drawing.Point(408, 182);
            this.lblLibStatut.Margin = new System.Windows.Forms.Padding(2);
            this.lblLibStatut.MaxLength = 32767;
            this.lblLibStatut.Multiline = false;
            this.lblLibStatut.Name = "lblLibStatut";
            this.lblLibStatut.ReadOnly = true;
            this.lblLibStatut.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblLibStatut.Size = new System.Drawing.Size(187, 27);
            this.lblLibStatut.TabIndex = 526;
            this.lblLibStatut.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblLibStatut.UseSystemPasswordChar = false;
            // 
            // lblNomIntervenant
            // 
            this.lblNomIntervenant.AccAcceptNumbersOnly = false;
            this.lblNomIntervenant.AccAllowComma = false;
            this.lblNomIntervenant.AccBackgroundColor = System.Drawing.Color.White;
            this.lblNomIntervenant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblNomIntervenant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblNomIntervenant.AccHidenValue = "";
            this.lblNomIntervenant.AccNotAllowedChars = null;
            this.lblNomIntervenant.AccReadOnly = true;
            this.lblNomIntervenant.AccReadOnlyAllowDelete = false;
            this.lblNomIntervenant.AccRequired = false;
            this.lblNomIntervenant.BackColor = System.Drawing.Color.White;
            this.lblNomIntervenant.CustomBackColor = System.Drawing.Color.White;
            this.lblNomIntervenant.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.lblNomIntervenant.ForeColor = System.Drawing.Color.Black;
            this.lblNomIntervenant.Location = new System.Drawing.Point(408, 32);
            this.lblNomIntervenant.Margin = new System.Windows.Forms.Padding(2);
            this.lblNomIntervenant.MaxLength = 32767;
            this.lblNomIntervenant.Multiline = false;
            this.lblNomIntervenant.Name = "lblNomIntervenant";
            this.lblNomIntervenant.ReadOnly = false;
            this.lblNomIntervenant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblNomIntervenant.Size = new System.Drawing.Size(57, 27);
            this.lblNomIntervenant.TabIndex = 595;
            this.lblNomIntervenant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblNomIntervenant.UseSystemPasswordChar = false;
            this.lblNomIntervenant.Visible = false;
            // 
            // Frame12
            // 
            this.Frame12.BackColor = System.Drawing.Color.Transparent;
            this.Frame12.Controls.Add(this.txtCommentaire);
            this.Frame12.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Frame12.Location = new System.Drawing.Point(19, 717);
            this.Frame12.Name = "Frame12";
            this.Frame12.Size = new System.Drawing.Size(594, 83);
            this.Frame12.TabIndex = 634;
            this.Frame12.TabStop = false;
            this.Frame12.Text = "Commentaire sur intervention";
            // 
            // txtCommentaire
            // 
            this.txtCommentaire.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCommentaire.Location = new System.Drawing.Point(3, 20);
            this.txtCommentaire.Name = "txtCommentaire";
            this.txtCommentaire.Size = new System.Drawing.Size(588, 60);
            this.txtCommentaire.TabIndex = 0;
            this.txtCommentaire.Text = "";
            this.txtCommentaire.TextChanged += new System.EventHandler(this.txtCommentaire_TextChanged);
            this.txtCommentaire.Leave += new System.EventHandler(this.txtCommentaire_Leave);
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label9.Location = new System.Drawing.Point(227, 476);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(152, 17);
            this.label9.TabIndex = 630;
            this.label9.Text = "FICHE INTERVENTION";
            // 
            // txtMatriculeIntervenant
            // 
            this.txtMatriculeIntervenant.AccAcceptNumbersOnly = false;
            this.txtMatriculeIntervenant.AccAllowComma = false;
            this.txtMatriculeIntervenant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMatriculeIntervenant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMatriculeIntervenant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMatriculeIntervenant.AccHidenValue = "";
            this.txtMatriculeIntervenant.AccNotAllowedChars = null;
            this.txtMatriculeIntervenant.AccReadOnly = true;
            this.txtMatriculeIntervenant.AccReadOnlyAllowDelete = false;
            this.txtMatriculeIntervenant.AccRequired = false;
            this.txtMatriculeIntervenant.BackColor = System.Drawing.Color.White;
            this.txtMatriculeIntervenant.CustomBackColor = System.Drawing.Color.White;
            this.txtMatriculeIntervenant.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtMatriculeIntervenant.ForeColor = System.Drawing.Color.Black;
            this.txtMatriculeIntervenant.Location = new System.Drawing.Point(593, 83);
            this.txtMatriculeIntervenant.Margin = new System.Windows.Forms.Padding(2);
            this.txtMatriculeIntervenant.MaxLength = 32767;
            this.txtMatriculeIntervenant.Multiline = false;
            this.txtMatriculeIntervenant.Name = "txtMatriculeIntervenant";
            this.txtMatriculeIntervenant.ReadOnly = false;
            this.txtMatriculeIntervenant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMatriculeIntervenant.Size = new System.Drawing.Size(57, 27);
            this.txtMatriculeIntervenant.TabIndex = 638;
            this.txtMatriculeIntervenant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMatriculeIntervenant.UseSystemPasswordChar = false;
            this.txtMatriculeIntervenant.Visible = false;
            // 
            // txtMemoGuard
            // 
            this.txtMemoGuard.AccAcceptNumbersOnly = false;
            this.txtMemoGuard.AccAllowComma = false;
            this.txtMemoGuard.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMemoGuard.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMemoGuard.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMemoGuard.AccHidenValue = "";
            this.txtMemoGuard.AccNotAllowedChars = null;
            this.txtMemoGuard.AccReadOnly = true;
            this.txtMemoGuard.AccReadOnlyAllowDelete = false;
            this.txtMemoGuard.AccRequired = false;
            this.txtMemoGuard.BackColor = System.Drawing.Color.White;
            this.txtMemoGuard.CustomBackColor = System.Drawing.Color.White;
            this.txtMemoGuard.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtMemoGuard.ForeColor = System.Drawing.Color.Black;
            this.txtMemoGuard.Location = new System.Drawing.Point(503, 63);
            this.txtMemoGuard.Margin = new System.Windows.Forms.Padding(2);
            this.txtMemoGuard.MaxLength = 32767;
            this.txtMemoGuard.Multiline = false;
            this.txtMemoGuard.Name = "txtMemoGuard";
            this.txtMemoGuard.ReadOnly = false;
            this.txtMemoGuard.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMemoGuard.Size = new System.Drawing.Size(57, 27);
            this.txtMemoGuard.TabIndex = 636;
            this.txtMemoGuard.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMemoGuard.UseSystemPasswordChar = false;
            this.txtMemoGuard.Visible = false;
            // 
            // txtDispatcheur
            // 
            appearance37.BackColor = System.Drawing.SystemColors.Window;
            appearance37.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtDispatcheur.DisplayLayout.Appearance = appearance37;
            this.txtDispatcheur.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtDispatcheur.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance38.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance38.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance38.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance38.BorderColor = System.Drawing.SystemColors.Window;
            this.txtDispatcheur.DisplayLayout.GroupByBox.Appearance = appearance38;
            appearance39.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtDispatcheur.DisplayLayout.GroupByBox.BandLabelAppearance = appearance39;
            this.txtDispatcheur.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance40.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance40.BackColor2 = System.Drawing.SystemColors.Control;
            appearance40.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance40.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtDispatcheur.DisplayLayout.GroupByBox.PromptAppearance = appearance40;
            this.txtDispatcheur.DisplayLayout.MaxColScrollRegions = 1;
            this.txtDispatcheur.DisplayLayout.MaxRowScrollRegions = 1;
            appearance41.BackColor = System.Drawing.SystemColors.Window;
            appearance41.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtDispatcheur.DisplayLayout.Override.ActiveCellAppearance = appearance41;
            appearance42.BackColor = System.Drawing.SystemColors.Highlight;
            appearance42.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txtDispatcheur.DisplayLayout.Override.ActiveRowAppearance = appearance42;
            this.txtDispatcheur.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtDispatcheur.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance43.BackColor = System.Drawing.SystemColors.Window;
            this.txtDispatcheur.DisplayLayout.Override.CardAreaAppearance = appearance43;
            appearance44.BorderColor = System.Drawing.Color.Silver;
            appearance44.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtDispatcheur.DisplayLayout.Override.CellAppearance = appearance44;
            this.txtDispatcheur.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtDispatcheur.DisplayLayout.Override.CellPadding = 0;
            appearance45.BackColor = System.Drawing.SystemColors.Control;
            appearance45.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance45.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance45.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance45.BorderColor = System.Drawing.SystemColors.Window;
            this.txtDispatcheur.DisplayLayout.Override.GroupByRowAppearance = appearance45;
            appearance46.TextHAlignAsString = "Left";
            this.txtDispatcheur.DisplayLayout.Override.HeaderAppearance = appearance46;
            this.txtDispatcheur.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtDispatcheur.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance47.BackColor = System.Drawing.SystemColors.Window;
            appearance47.BorderColor = System.Drawing.Color.Silver;
            this.txtDispatcheur.DisplayLayout.Override.RowAppearance = appearance47;
            this.txtDispatcheur.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance48.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtDispatcheur.DisplayLayout.Override.TemplateAddRowAppearance = appearance48;
            this.txtDispatcheur.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtDispatcheur.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtDispatcheur.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtDispatcheur.Location = new System.Drawing.Point(507, 88);
            this.txtDispatcheur.Name = "txtDispatcheur";
            this.txtDispatcheur.Size = new System.Drawing.Size(109, 22);
            this.txtDispatcheur.TabIndex = 635;
            this.txtDispatcheur.Visible = false;
            // 
            // txtCode1
            // 
            this.txtCode1.AccAcceptNumbersOnly = false;
            this.txtCode1.AccAllowComma = false;
            this.txtCode1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCode1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCode1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCode1.AccHidenValue = "";
            this.txtCode1.AccNotAllowedChars = null;
            this.txtCode1.AccReadOnly = false;
            this.txtCode1.AccReadOnlyAllowDelete = false;
            this.txtCode1.AccRequired = false;
            this.txtCode1.BackColor = System.Drawing.Color.White;
            this.txtCode1.CustomBackColor = System.Drawing.Color.White;
            this.txtCode1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCode1.ForeColor = System.Drawing.Color.Black;
            this.txtCode1.Location = new System.Drawing.Point(133, 39);
            this.txtCode1.Margin = new System.Windows.Forms.Padding(2);
            this.txtCode1.MaxLength = 32767;
            this.txtCode1.Multiline = false;
            this.txtCode1.Name = "txtCode1";
            this.txtCode1.ReadOnly = true;
            this.txtCode1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCode1.Size = new System.Drawing.Size(334, 27);
            this.txtCode1.TabIndex = 619;
            this.txtCode1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCode1.UseSystemPasswordChar = false;
            // 
            // frmCreateInterViaAppel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(638, 804);
            this.Controls.Add(this.Frame3);
            this.Controls.Add(this.tableLayoutPanel18);
            this.Controls.Add(this.label5);
            this.Controls.Add(this.label4);
            this.Controls.Add(this.label3);
            this.Controls.Add(this.cmdCreateInter);
            this.Controls.Add(this.txtcontrat);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtCodeimmeuble);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.txtNoIntervention);
            this.Controls.Add(this.label7);
            this.Controls.Add(this.label8);
            this.Controls.Add(this.tableLayoutPanel9);
            this.Controls.Add(this.Frame12);
            this.Controls.Add(this.label9);
            this.Controls.Add(this.txtMatriculeIntervenant);
            this.Controls.Add(this.txtMemoGuard);
            this.Controls.Add(this.txtDispatcheur);
            this.Controls.Add(this.txtCode1);
            this.Name = "frmCreateInterViaAppel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Form1";
            this.Frame3.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodeParticulier)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.cmbIntervenantDT)).EndInit();
            this.tableLayoutPanel26.ResumeLayout(false);
            this.tableLayoutPanel26.PerformLayout();
            this.tableLayoutPanel18.ResumeLayout(false);
            this.tableLayoutPanel18.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.cmbCAI_CategorInterv)).EndInit();
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.Frame12.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.txtDispatcheur)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.GroupBox Frame3;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public iTalk.iTalk_TextBox_Small2 txtCodeOrigine2;
        private System.Windows.Forms.Label Label22;
        public iTalk.iTalk_TextBox_Small2 lbllibCodeParticulier;
        public Infragistics.Win.UltraWinGrid.UltraCombo txtCodeParticulier;
        public iTalk.iTalk_TextBox_Small2 txtSource;
        private System.Windows.Forms.Label lblSociete;
        public iTalk.iTalk_TextBox_Small2 txtSociete;
        public iTalk.iTalk_TextBox_Small2 lblLibIntervenantDT;
        private System.Windows.Forms.RadioButton optQuiSociete;
        private System.Windows.Forms.RadioButton optQuiGerant;
        private System.Windows.Forms.RadioButton optQuiGardien;
        private System.Windows.Forms.RadioButton optQuiAutre;
        private System.Windows.Forms.RadioButton optQuiFournisseur;
        private System.Windows.Forms.RadioButton optQuiCopro;
        private System.Windows.Forms.RadioButton OptQuiGestionnaire;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbIntervenantDT;
        public System.Windows.Forms.Button cmdIntervenant;
        public System.Windows.Forms.Button cmdEctituresCopro;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel26;
        public System.Windows.Forms.Button cmdRechercheFour;
        private System.Windows.Forms.Label lblSource;
        private System.Windows.Forms.Label lblParticulier;
        public System.Windows.Forms.Button cmdAjoutgestion;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel18;
        private System.Windows.Forms.Label label6;
        public iTalk.iTalk_TextBox_Small2 lbllibCAI_CategorInterv;
        public Infragistics.Win.UltraWinGrid.UltraCombo cmbCAI_CategorInterv;
        private System.Windows.Forms.Label label23;
        public iTalk.iTalk_RichTextBox txtObservations;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Button cmdCreateInter;
        public iTalk.iTalk_TextBox_Small2 txtcontrat;
        public System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtCodeimmeuble;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label33;
        private System.Windows.Forms.Label label15;
        public iTalk.iTalk_TextBox_Small2 txtDesignation;
        public iTalk.iTalk_TextBox_Small2 txtNoIntervention;
        public iTalk.iTalk_TextBox_Small2 txtIntervenant;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        public iTalk.iTalk_TextBox_Small2 txtINT_AnaActivite;
        public System.Windows.Forms.Button cmdRechercheIntervenant;
        public System.Windows.Forms.Button cmdRechercheTech;
        public iTalk.iTalk_TextBox_Small2 txtLibDispatch;
        public System.Windows.Forms.Button Command1;
        public iTalk.iTalk_TextBox_Small2 txtArticle;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        public iTalk.iTalk_TextBox_Small2 lblLibInterv;
        public iTalk.iTalk_TextBox_Small2 lbllibDisptach;
        public iTalk.iTalk_TextBox_Small2 txtCodeEtat;
        private System.Windows.Forms.CheckBox chkCompteurObli;
        private System.Windows.Forms.CheckBox chkUrgence;
        public System.Windows.Forms.Button cmdRechercheCodetat;
        public iTalk.iTalk_TextBox_Small2 lblLibStatut;
        public iTalk.iTalk_TextBox_Small2 lblNomIntervenant;
        public System.Windows.Forms.GroupBox Frame12;
        private System.Windows.Forms.RichTextBox txtCommentaire;
        public System.Windows.Forms.Label label9;
        public iTalk.iTalk_TextBox_Small2 txtMatriculeIntervenant;
        public iTalk.iTalk_TextBox_Small2 txtMemoGuard;
        public Infragistics.Win.UltraWinGrid.UltraCombo txtDispatcheur;
        public iTalk.iTalk_TextBox_Small2 txtCode1;
    }
}