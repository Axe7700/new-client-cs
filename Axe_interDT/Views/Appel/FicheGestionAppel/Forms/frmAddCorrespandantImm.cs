﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Appel.FicheGestionAppel.Forms
{
    public partial class frmAddCorrespandantImm : Form
    {
        public frmAddCorrespandantImm()
        {
            InitializeComponent();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            this.Close();
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheImmeuble_Click(object sender, EventArgs e)
        {
            txtCodeimmeuble_KeyPress(txtCodeimmeuble, new KeyPressEventArgs((char)13));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeimmeuble_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if ((int)e.KeyChar == 13)
                {

                    string requete = "SELECT CodeImmeuble as \"Code Immeuble\"," + " Adresse as \"Adresse\"," + " Ville as \"Ville\"," +
                        " anglerue  as \"Angle de rue \"," + "Code1  as \"Gerant\"  FROM  Immeuble";
                    string where_order = "";
                    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un immeuble" };
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        // charge les enregistrements de l'immeuble
                        txtCodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    
                        fg.Dispose();
                        fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            txtCodeimmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            fg.Dispose();
                            fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();
                }
               
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCodeimmeuble_KeyPress");
                throw;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdSauver_Click(object sender, EventArgs e)
        {
            fc_Save();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Save()
        {
            DataTable rs = new DataTable();
            ModAdo modAdors = new ModAdo();

            string sSQL = "SELECT      CodeImmeuble_IMM, CodeCorresp_IMC, Nom_IMC, CodeQualif_QUA, Tel_IMC, TelPortable_IMC, Fax_IMC, Email_IMC, Note_IMC, " +
                " PresidentCS_IMC from IMC_ImmCorrespondant WHERE     CodeImmeuble_IMM = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeimmeuble.Text) + "'";
            rs = modAdors.fc_OpenRecordSet(sSQL);
            var newRow = rs.NewRow();
            newRow["Codeimmeuble_IMM"] = txtCodeimmeuble.Text;
            newRow["nom_imc"] = General.nz(txtNom_IMC.Text,"");
            newRow["CodeQualif_QUA"] = General.nz(txtCodeQualif_QUA.Text, "");
            newRow["tel_imc"] = General.nz(txtTel_IMC.Text, "");
            newRow["telportable_imc"] = General.nz(txtTelPortable_IMC.Text, "");
            newRow["Fax_IMC"] = General.nz(txtFax_IMC.Text, "");
            newRow["email_imc"] = General.nz(txtEmail_IMC.Text, "");
            newRow["Note_imc"] = General.nz(txtNote_IMC.Text, "");
            newRow["PresidentCS_IMC"] = chkPresidentCS_IMC.Checked ? "1" :"-1";
            rs.Rows.Add(newRow.ItemArray);
            modAdors.Update();

            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Correspondant enregistré.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeQualif_QUA_TextChanged(object sender, EventArgs e)
        {
            fc_libQualite();
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeQualif_QUA_AfterCloseUp(object sender, EventArgs e)
        {
            fc_libQualite();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeQualif_QUA_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                General.sSQL = "";
                General.sSQL = "SELECT Qualification.CodeQualif as [Code Qualif], Qualification.Qualification from Qualification where Correspondant_COR = 1";
                sheridan.InitialiseCombo(txtCodeQualif_QUA, General.sSQL, "Code Qualif");
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "txtCodeQualif_QUA_DropDown");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_libQualite()
        {

            using (var tmp = new ModAdo())
            {
                string sSQL ="SELECT Qualification.CodeQualif as [Code Qualif], Qualification.Qualification from Qualification where CodeQualif ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeQualif_QUA.Text) + "'";
                txtLibQualif.Text = tmp.fc_ADOlibelle(sSQL);
            }
        }
    }
}
