﻿namespace Axe_interDT.Views.Appel.FicheGestionAppel.Forms
{
    partial class frmAlerteAppel
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.lblNoTel = new System.Windows.Forms.Label();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.Location = new System.Drawing.Point(12, 9);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(463, 46);
            this.label1.TabIndex = 0;
            this.label1.Text = "Vous avez un appel entrant, veuillez cliquer sur le lien ci-dessous pour afficher" +
    " la fiche Gestion des appels.";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Bold);
            this.label2.Location = new System.Drawing.Point(1, 69);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(548, 19);
            this.label2.TabIndex = 1;
            this.label2.Text = "Veuillez d\'abord enregistrer votre travail avant de cliquer sur ce lien.\r\n";
            // 
            // lblNoTel
            // 
            this.lblNoTel.AutoSize = true;
            this.lblNoTel.Cursor = System.Windows.Forms.Cursors.Hand;
            this.lblNoTel.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Underline);
            this.lblNoTel.ForeColor = System.Drawing.Color.DodgerBlue;
            this.lblNoTel.Location = new System.Drawing.Point(99, 112);
            this.lblNoTel.Name = "lblNoTel";
            this.lblNoTel.Size = new System.Drawing.Size(327, 19);
            this.lblNoTel.TabIndex = 2;
            this.lblNoTel.Text = "Naviguer vers le formulaire gestion des appels\r\n";
            this.lblNoTel.Click += new System.EventHandler(this.lblNoTel_Click);
            // 
            // frmAlerteAppel
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(9F, 19F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(546, 159);
            this.Controls.Add(this.lblNoTel);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.label1);
            this.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Margin = new System.Windows.Forms.Padding(4);
            this.Name = "frmAlerteAppel";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Appel entrant";
            this.Activated += new System.EventHandler(this.frmAlerteAppel_Activated);
            this.Load += new System.EventHandler(this.frmAlerteAppel_Load);
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.Label lblNoTel;
    }
}