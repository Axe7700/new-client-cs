﻿namespace Axe_interDT.Views.Appel.FicheGestionAppel.Forms
{
    partial class frmAddCorrespandantImm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.Frame1 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel9 = new System.Windows.Forms.TableLayoutPanel();
            this.txtNom_IMC = new iTalk.iTalk_TextBox_Small2();
            this.label12 = new System.Windows.Forms.Label();
            this.label13 = new System.Windows.Forms.Label();
            this.label14 = new System.Windows.Forms.Label();
            this.label15 = new System.Windows.Forms.Label();
            this.tableLayoutPanel10 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCodeimmeuble = new iTalk.iTalk_TextBox_Small2();
            this.cmdRechercheImmeuble = new System.Windows.Forms.Button();
            this.label33 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.txtCodeQualif_QUA = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtLibQualif = new iTalk.iTalk_TextBox_Small2();
            this.chkPresidentCS_IMC = new System.Windows.Forms.CheckBox();
            this.Label16 = new System.Windows.Forms.Label();
            this.txtNote_IMC = new iTalk.iTalk_RichTextBox();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.txtEmail_IMC = new iTalk.iTalk_TextBox_Small2();
            this.txtTel_IMC = new iTalk.iTalk_TextBox_Small2();
            this.txtTelPortable_IMC = new iTalk.iTalk_TextBox_Small2();
            this.txtFax_IMC = new iTalk.iTalk_TextBox_Small2();
            this.Frame1.SuspendLayout();
            this.tableLayoutPanel9.SuspendLayout();
            this.tableLayoutPanel10.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodeQualif_QUA)).BeginInit();
            this.flowLayoutPanel1.SuspendLayout();
            this.SuspendLayout();
            // 
            // Frame1
            // 
            this.Frame1.BackColor = System.Drawing.Color.Transparent;
            this.Frame1.Controls.Add(this.tableLayoutPanel9);
            this.Frame1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame1.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Frame1.Location = new System.Drawing.Point(0, 0);
            this.Frame1.Margin = new System.Windows.Forms.Padding(3, 3, 3, 0);
            this.Frame1.Name = "Frame1";
            this.Frame1.Size = new System.Drawing.Size(401, 411);
            this.Frame1.TabIndex = 412;
            this.Frame1.TabStop = false;
            // 
            // tableLayoutPanel9
            // 
            this.tableLayoutPanel9.ColumnCount = 2;
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30.30303F));
            this.tableLayoutPanel9.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 69.69697F));
            this.tableLayoutPanel9.Controls.Add(this.txtNom_IMC, 1, 1);
            this.tableLayoutPanel9.Controls.Add(this.label12, 0, 3);
            this.tableLayoutPanel9.Controls.Add(this.label13, 0, 4);
            this.tableLayoutPanel9.Controls.Add(this.label14, 0, 5);
            this.tableLayoutPanel9.Controls.Add(this.label15, 0, 6);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel10, 1, 0);
            this.tableLayoutPanel9.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel9.Controls.Add(this.label2, 0, 1);
            this.tableLayoutPanel9.Controls.Add(this.label1, 0, 2);
            this.tableLayoutPanel9.Controls.Add(this.tableLayoutPanel1, 1, 2);
            this.tableLayoutPanel9.Controls.Add(this.chkPresidentCS_IMC, 1, 7);
            this.tableLayoutPanel9.Controls.Add(this.Label16, 0, 8);
            this.tableLayoutPanel9.Controls.Add(this.txtNote_IMC, 1, 8);
            this.tableLayoutPanel9.Controls.Add(this.flowLayoutPanel1, 1, 9);
            this.tableLayoutPanel9.Controls.Add(this.txtEmail_IMC, 1, 6);
            this.tableLayoutPanel9.Controls.Add(this.txtTel_IMC, 1, 3);
            this.tableLayoutPanel9.Controls.Add(this.txtTelPortable_IMC, 1, 4);
            this.tableLayoutPanel9.Controls.Add(this.txtFax_IMC, 1, 5);
            this.tableLayoutPanel9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel9.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel9.Name = "tableLayoutPanel9";
            this.tableLayoutPanel9.RowCount = 10;
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 31F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 52F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 73F));
            this.tableLayoutPanel9.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 37F));
            this.tableLayoutPanel9.Size = new System.Drawing.Size(395, 388);
            this.tableLayoutPanel9.TabIndex = 0;
            // 
            // txtNom_IMC
            // 
            this.txtNom_IMC.AccAcceptNumbersOnly = false;
            this.txtNom_IMC.AccAllowComma = false;
            this.txtNom_IMC.AccBackgroundColor = System.Drawing.Color.White;
            this.txtNom_IMC.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtNom_IMC.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNom_IMC.AccHidenValue = "";
            this.txtNom_IMC.AccNotAllowedChars = null;
            this.txtNom_IMC.AccReadOnly = false;
            this.txtNom_IMC.AccReadOnlyAllowDelete = false;
            this.txtNom_IMC.AccRequired = false;
            this.txtNom_IMC.BackColor = System.Drawing.Color.White;
            this.txtNom_IMC.CustomBackColor = System.Drawing.Color.White;
            this.txtNom_IMC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNom_IMC.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNom_IMC.ForeColor = System.Drawing.Color.Blue;
            this.txtNom_IMC.Location = new System.Drawing.Point(121, 33);
            this.txtNom_IMC.Margin = new System.Windows.Forms.Padding(2);
            this.txtNom_IMC.MaxLength = 32767;
            this.txtNom_IMC.Multiline = false;
            this.txtNom_IMC.Name = "txtNom_IMC";
            this.txtNom_IMC.ReadOnly = false;
            this.txtNom_IMC.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtNom_IMC.Size = new System.Drawing.Size(272, 27);
            this.txtNom_IMC.TabIndex = 517;
            this.txtNom_IMC.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtNom_IMC.UseSystemPasswordChar = false;
            // 
            // label12
            // 
            this.label12.AutoSize = true;
            this.label12.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label12.Location = new System.Drawing.Point(3, 93);
            this.label12.Name = "label12";
            this.label12.Size = new System.Drawing.Size(30, 19);
            this.label12.TabIndex = 508;
            this.label12.Text = "Tel";
            // 
            // label13
            // 
            this.label13.AutoSize = true;
            this.label13.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label13.Location = new System.Drawing.Point(3, 124);
            this.label13.Name = "label13";
            this.label13.Size = new System.Drawing.Size(69, 19);
            this.label13.TabIndex = 509;
            this.label13.Text = "Portable";
            // 
            // label14
            // 
            this.label14.AutoSize = true;
            this.label14.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label14.Location = new System.Drawing.Point(3, 155);
            this.label14.Name = "label14";
            this.label14.Size = new System.Drawing.Size(32, 19);
            this.label14.TabIndex = 510;
            this.label14.Text = "Fax";
            // 
            // label15
            // 
            this.label15.AutoSize = true;
            this.label15.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label15.Location = new System.Drawing.Point(3, 186);
            this.label15.Name = "label15";
            this.label15.Size = new System.Drawing.Size(46, 19);
            this.label15.TabIndex = 511;
            this.label15.Text = "Email";
            // 
            // tableLayoutPanel10
            // 
            this.tableLayoutPanel10.ColumnCount = 2;
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel10.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel10.Controls.Add(this.txtCodeimmeuble, 0, 0);
            this.tableLayoutPanel10.Controls.Add(this.cmdRechercheImmeuble, 1, 0);
            this.tableLayoutPanel10.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel10.Location = new System.Drawing.Point(119, 0);
            this.tableLayoutPanel10.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel10.Name = "tableLayoutPanel10";
            this.tableLayoutPanel10.RowCount = 1;
            this.tableLayoutPanel10.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel10.Size = new System.Drawing.Size(276, 31);
            this.tableLayoutPanel10.TabIndex = 516;
            // 
            // txtCodeimmeuble
            // 
            this.txtCodeimmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeimmeuble.AccAllowComma = false;
            this.txtCodeimmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeimmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeimmeuble.AccHidenValue = "";
            this.txtCodeimmeuble.AccNotAllowedChars = null;
            this.txtCodeimmeuble.AccReadOnly = false;
            this.txtCodeimmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeimmeuble.AccRequired = false;
            this.txtCodeimmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeimmeuble.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeimmeuble.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeimmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeimmeuble.Location = new System.Drawing.Point(2, 2);
            this.txtCodeimmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeimmeuble.MaxLength = 32767;
            this.txtCodeimmeuble.Multiline = false;
            this.txtCodeimmeuble.Name = "txtCodeimmeuble";
            this.txtCodeimmeuble.ReadOnly = false;
            this.txtCodeimmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeimmeuble.Size = new System.Drawing.Size(232, 27);
            this.txtCodeimmeuble.TabIndex = 506;
            this.txtCodeimmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeimmeuble.UseSystemPasswordChar = false;
            this.txtCodeimmeuble.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeimmeuble_KeyPress);
            // 
            // cmdRechercheImmeuble
            // 
            this.cmdRechercheImmeuble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheImmeuble.FlatAppearance.BorderSize = 0;
            this.cmdRechercheImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheImmeuble.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheImmeuble.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheImmeuble.Location = new System.Drawing.Point(239, 3);
            this.cmdRechercheImmeuble.Name = "cmdRechercheImmeuble";
            this.cmdRechercheImmeuble.Size = new System.Drawing.Size(25, 19);
            this.cmdRechercheImmeuble.TabIndex = 583;
            this.cmdRechercheImmeuble.UseVisualStyleBackColor = false;
            this.cmdRechercheImmeuble.Click += new System.EventHandler(this.cmdRechercheImmeuble_Click);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(78, 31);
            this.label33.TabIndex = 529;
            this.label33.Text = "Code immeuble";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 31);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(42, 19);
            this.label2.TabIndex = 530;
            this.label2.Text = "Nom";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 62);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(97, 19);
            this.label1.TabIndex = 528;
            this.label1.Text = "Qualification";
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Controls.Add(this.txtCodeQualif_QUA, 0, 0);
            this.tableLayoutPanel1.Controls.Add(this.txtLibQualif, 1, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(119, 62);
            this.tableLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 1;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(276, 31);
            this.tableLayoutPanel1.TabIndex = 531;
            // 
            // txtCodeQualif_QUA
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.txtCodeQualif_QUA.DisplayLayout.Appearance = appearance1;
            this.txtCodeQualif_QUA.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.txtCodeQualif_QUA.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.txtCodeQualif_QUA.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtCodeQualif_QUA.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.txtCodeQualif_QUA.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.txtCodeQualif_QUA.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.txtCodeQualif_QUA.DisplayLayout.MaxColScrollRegions = 1;
            this.txtCodeQualif_QUA.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.txtCodeQualif_QUA.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.txtCodeQualif_QUA.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.txtCodeQualif_QUA.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.txtCodeQualif_QUA.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.txtCodeQualif_QUA.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.txtCodeQualif_QUA.DisplayLayout.Override.CellAppearance = appearance8;
            this.txtCodeQualif_QUA.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.txtCodeQualif_QUA.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.txtCodeQualif_QUA.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.txtCodeQualif_QUA.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.txtCodeQualif_QUA.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.txtCodeQualif_QUA.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.txtCodeQualif_QUA.DisplayLayout.Override.RowAppearance = appearance11;
            this.txtCodeQualif_QUA.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.txtCodeQualif_QUA.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.txtCodeQualif_QUA.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.txtCodeQualif_QUA.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.txtCodeQualif_QUA.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.txtCodeQualif_QUA.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeQualif_QUA.Location = new System.Drawing.Point(3, 3);
            this.txtCodeQualif_QUA.Name = "txtCodeQualif_QUA";
            this.txtCodeQualif_QUA.Size = new System.Drawing.Size(132, 27);
            this.txtCodeQualif_QUA.TabIndex = 508;
            this.txtCodeQualif_QUA.AfterCloseUp += new System.EventHandler(this.txtCodeQualif_QUA_AfterCloseUp);
            this.txtCodeQualif_QUA.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.txtCodeQualif_QUA_BeforeDropDown);
            this.txtCodeQualif_QUA.TextChanged += new System.EventHandler(this.txtCodeQualif_QUA_TextChanged);
            // 
            // txtLibQualif
            // 
            this.txtLibQualif.AccAcceptNumbersOnly = false;
            this.txtLibQualif.AccAllowComma = false;
            this.txtLibQualif.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibQualif.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibQualif.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLibQualif.AccHidenValue = "";
            this.txtLibQualif.AccNotAllowedChars = null;
            this.txtLibQualif.AccReadOnly = false;
            this.txtLibQualif.AccReadOnlyAllowDelete = false;
            this.txtLibQualif.AccRequired = false;
            this.txtLibQualif.BackColor = System.Drawing.Color.White;
            this.txtLibQualif.CustomBackColor = System.Drawing.Color.White;
            this.txtLibQualif.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLibQualif.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtLibQualif.ForeColor = System.Drawing.Color.Blue;
            this.txtLibQualif.Location = new System.Drawing.Point(140, 2);
            this.txtLibQualif.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibQualif.MaxLength = 32767;
            this.txtLibQualif.Multiline = false;
            this.txtLibQualif.Name = "txtLibQualif";
            this.txtLibQualif.ReadOnly = false;
            this.txtLibQualif.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibQualif.Size = new System.Drawing.Size(134, 27);
            this.txtLibQualif.TabIndex = 507;
            this.txtLibQualif.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibQualif.UseSystemPasswordChar = false;
            // 
            // chkPresidentCS_IMC
            // 
            this.chkPresidentCS_IMC.AutoSize = true;
            this.chkPresidentCS_IMC.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.chkPresidentCS_IMC.Location = new System.Drawing.Point(122, 220);
            this.chkPresidentCS_IMC.Name = "chkPresidentCS_IMC";
            this.chkPresidentCS_IMC.Size = new System.Drawing.Size(226, 23);
            this.chkPresidentCS_IMC.TabIndex = 572;
            this.chkPresidentCS_IMC.Text = "Président du conseil syndical";
            this.chkPresidentCS_IMC.UseVisualStyleBackColor = true;
            // 
            // Label16
            // 
            this.Label16.AutoSize = true;
            this.Label16.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label16.Location = new System.Drawing.Point(3, 269);
            this.Label16.Name = "Label16";
            this.Label16.Size = new System.Drawing.Size(44, 19);
            this.Label16.TabIndex = 515;
            this.Label16.Text = "Note";
            // 
            // txtNote_IMC
            // 
            this.txtNote_IMC.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtNote_IMC.AutoWordSelection = false;
            this.txtNote_IMC.BackColor = System.Drawing.Color.White;
            this.txtNote_IMC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtNote_IMC.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtNote_IMC.ForeColor = System.Drawing.Color.Black;
            this.txtNote_IMC.Location = new System.Drawing.Point(122, 272);
            this.txtNote_IMC.Name = "txtNote_IMC";
            this.txtNote_IMC.ReadOnly = false;
            this.txtNote_IMC.Size = new System.Drawing.Size(270, 67);
            this.txtNote_IMC.TabIndex = 573;
            this.txtNote_IMC.WordWrap = true;
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Anchor = ((System.Windows.Forms.AnchorStyles)((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Right)));
            this.flowLayoutPanel1.Controls.Add(this.CmdSauver);
            this.flowLayoutPanel1.Controls.Add(this.cmdAnnuler);
            this.flowLayoutPanel1.Location = new System.Drawing.Point(260, 345);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(132, 39);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(2, 2);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 574;
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.cmdAnnuler.Location = new System.Drawing.Point(66, 2);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(60, 35);
            this.cmdAnnuler.TabIndex = 575;
            this.cmdAnnuler.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // txtEmail_IMC
            // 
            this.txtEmail_IMC.AccAcceptNumbersOnly = false;
            this.txtEmail_IMC.AccAllowComma = false;
            this.txtEmail_IMC.AccBackgroundColor = System.Drawing.Color.White;
            this.txtEmail_IMC.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtEmail_IMC.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtEmail_IMC.AccHidenValue = "";
            this.txtEmail_IMC.AccNotAllowedChars = null;
            this.txtEmail_IMC.AccReadOnly = false;
            this.txtEmail_IMC.AccReadOnlyAllowDelete = false;
            this.txtEmail_IMC.AccRequired = false;
            this.txtEmail_IMC.BackColor = System.Drawing.Color.White;
            this.txtEmail_IMC.CustomBackColor = System.Drawing.Color.White;
            this.txtEmail_IMC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtEmail_IMC.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtEmail_IMC.ForeColor = System.Drawing.Color.Black;
            this.txtEmail_IMC.Location = new System.Drawing.Point(121, 188);
            this.txtEmail_IMC.Margin = new System.Windows.Forms.Padding(2);
            this.txtEmail_IMC.MaxLength = 32767;
            this.txtEmail_IMC.Multiline = false;
            this.txtEmail_IMC.Name = "txtEmail_IMC";
            this.txtEmail_IMC.ReadOnly = false;
            this.txtEmail_IMC.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtEmail_IMC.Size = new System.Drawing.Size(272, 27);
            this.txtEmail_IMC.TabIndex = 522;
            this.txtEmail_IMC.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtEmail_IMC.UseSystemPasswordChar = false;
            // 
            // txtTel_IMC
            // 
            this.txtTel_IMC.AccAcceptNumbersOnly = false;
            this.txtTel_IMC.AccAllowComma = false;
            this.txtTel_IMC.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTel_IMC.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTel_IMC.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTel_IMC.AccHidenValue = "";
            this.txtTel_IMC.AccNotAllowedChars = null;
            this.txtTel_IMC.AccReadOnly = false;
            this.txtTel_IMC.AccReadOnlyAllowDelete = false;
            this.txtTel_IMC.AccRequired = false;
            this.txtTel_IMC.BackColor = System.Drawing.Color.White;
            this.txtTel_IMC.CustomBackColor = System.Drawing.Color.White;
            this.txtTel_IMC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTel_IMC.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtTel_IMC.ForeColor = System.Drawing.Color.Blue;
            this.txtTel_IMC.Location = new System.Drawing.Point(121, 95);
            this.txtTel_IMC.Margin = new System.Windows.Forms.Padding(2);
            this.txtTel_IMC.MaxLength = 32767;
            this.txtTel_IMC.Multiline = false;
            this.txtTel_IMC.Name = "txtTel_IMC";
            this.txtTel_IMC.ReadOnly = false;
            this.txtTel_IMC.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTel_IMC.Size = new System.Drawing.Size(272, 27);
            this.txtTel_IMC.TabIndex = 574;
            this.txtTel_IMC.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTel_IMC.UseSystemPasswordChar = false;
            // 
            // txtTelPortable_IMC
            // 
            this.txtTelPortable_IMC.AccAcceptNumbersOnly = false;
            this.txtTelPortable_IMC.AccAllowComma = false;
            this.txtTelPortable_IMC.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTelPortable_IMC.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTelPortable_IMC.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTelPortable_IMC.AccHidenValue = "";
            this.txtTelPortable_IMC.AccNotAllowedChars = null;
            this.txtTelPortable_IMC.AccReadOnly = false;
            this.txtTelPortable_IMC.AccReadOnlyAllowDelete = false;
            this.txtTelPortable_IMC.AccRequired = false;
            this.txtTelPortable_IMC.BackColor = System.Drawing.Color.White;
            this.txtTelPortable_IMC.CustomBackColor = System.Drawing.Color.White;
            this.txtTelPortable_IMC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtTelPortable_IMC.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtTelPortable_IMC.ForeColor = System.Drawing.Color.Blue;
            this.txtTelPortable_IMC.Location = new System.Drawing.Point(121, 126);
            this.txtTelPortable_IMC.Margin = new System.Windows.Forms.Padding(2);
            this.txtTelPortable_IMC.MaxLength = 32767;
            this.txtTelPortable_IMC.Multiline = false;
            this.txtTelPortable_IMC.Name = "txtTelPortable_IMC";
            this.txtTelPortable_IMC.ReadOnly = false;
            this.txtTelPortable_IMC.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTelPortable_IMC.Size = new System.Drawing.Size(272, 27);
            this.txtTelPortable_IMC.TabIndex = 575;
            this.txtTelPortable_IMC.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTelPortable_IMC.UseSystemPasswordChar = false;
            // 
            // txtFax_IMC
            // 
            this.txtFax_IMC.AccAcceptNumbersOnly = false;
            this.txtFax_IMC.AccAllowComma = false;
            this.txtFax_IMC.AccBackgroundColor = System.Drawing.Color.White;
            this.txtFax_IMC.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtFax_IMC.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtFax_IMC.AccHidenValue = "";
            this.txtFax_IMC.AccNotAllowedChars = null;
            this.txtFax_IMC.AccReadOnly = false;
            this.txtFax_IMC.AccReadOnlyAllowDelete = false;
            this.txtFax_IMC.AccRequired = false;
            this.txtFax_IMC.BackColor = System.Drawing.Color.White;
            this.txtFax_IMC.CustomBackColor = System.Drawing.Color.White;
            this.txtFax_IMC.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtFax_IMC.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtFax_IMC.ForeColor = System.Drawing.Color.Blue;
            this.txtFax_IMC.Location = new System.Drawing.Point(121, 157);
            this.txtFax_IMC.Margin = new System.Windows.Forms.Padding(2);
            this.txtFax_IMC.MaxLength = 32767;
            this.txtFax_IMC.Multiline = false;
            this.txtFax_IMC.Name = "txtFax_IMC";
            this.txtFax_IMC.ReadOnly = false;
            this.txtFax_IMC.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtFax_IMC.Size = new System.Drawing.Size(272, 27);
            this.txtFax_IMC.TabIndex = 576;
            this.txtFax_IMC.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtFax_IMC.UseSystemPasswordChar = false;
            // 
            // frmAddCorrespandantImm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(401, 411);
            this.Controls.Add(this.Frame1);
            this.MaximumSize = new System.Drawing.Size(417, 450);
            this.MinimumSize = new System.Drawing.Size(417, 450);
            this.Name = "frmAddCorrespandantImm";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmAddCorrespandantImm";
            this.Frame1.ResumeLayout(false);
            this.tableLayoutPanel9.ResumeLayout(false);
            this.tableLayoutPanel9.PerformLayout();
            this.tableLayoutPanel10.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.txtCodeQualif_QUA)).EndInit();
            this.flowLayoutPanel1.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox Frame1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel9;
        public iTalk.iTalk_TextBox_Small2 txtEmail_IMC;
        public iTalk.iTalk_TextBox_Small2 txtNom_IMC;
        private System.Windows.Forms.Label Label16;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel10;
        public System.Windows.Forms.Button cmdRechercheImmeuble;
        public iTalk.iTalk_TextBox_Small2 txtCodeimmeuble;
        public iTalk.iTalk_TextBox_Small2 txtLibQualif;
        private System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public Infragistics.Win.UltraWinGrid.UltraCombo txtCodeQualif_QUA;
        private System.Windows.Forms.CheckBox chkPresidentCS_IMC;
        public iTalk.iTalk_RichTextBox txtNote_IMC;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button CmdSauver;
        public System.Windows.Forms.Button cmdAnnuler;
        public iTalk.iTalk_TextBox_Small2 txtTel_IMC;
        public iTalk.iTalk_TextBox_Small2 txtTelPortable_IMC;
        public iTalk.iTalk_TextBox_Small2 txtFax_IMC;
    }
}