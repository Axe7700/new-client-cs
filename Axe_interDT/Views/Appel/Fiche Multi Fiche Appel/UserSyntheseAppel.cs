﻿using Axe_interDT.Shared;
using System;
using System.Data;
using System.Windows.Forms;

namespace Axe_interDT.Views.Appel.Fiche_Multi_Fiche_Appel
{
    public partial class UserSyntheseAppel : UserControl
    {
        private DataTable DataSource;

        public UserSyntheseAppel()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserSyntheseAppel_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;

            DataSource = new DataTable();
            DataSource.Columns.Add("Commun/dédié");
            DataSource.Columns.Add("NumFicheStandard");
            DataSource.Columns.Add("Achats");
            DataSource.Columns.Add("MO");
            DataSource.Columns.Add("ssTraitanceCap");
            DataSource.Columns.Add("ssTraitanceComp");
            DataSource.Columns.Add("FO");
            DataSource.Columns.Add("VentesReels");
            GridMulti.DataSource = DataSource;

            txtNumFicheStandard.Text = General.getFrmReg(Variable.cUserSyntheseAppel, "NewVar", "");
            fc_LoadMulti(Convert.ToInt32(General.nz(txtNumFicheStandard.Text, 0)));
        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="lNumficheStandard"></param>
        private void fc_LoadMulti(int lNumficheStandard)
        {
            int[] tAffaire = new int[1];
            string sAdd = "";
            string stemp = "";
            double dbAchatReel;
            double dbMo;
            double dbVentesReel;
            double dbFo;
            double dbSSTCap;
            double dbSSTComp;
            int lETAE_NoAuto;
            int[] sTab = null;
            int[] sTabTemp = null;
            int X;

            //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
            double totdbAchatReel = 0;
            double totdbMo = 0;
            double dbdbVentesReel = 0;
            double totdbFo = 0;
            double totdbSSTCap = 0;
            double totdbSSTComp = 0;
            double totdbVentesReel = 0;
            string sCommun = "";
            //===> Fin Modif Mondir

            try
            {
                GridMulti.DataSource = null;
                Cursor = Cursors.WaitCursor;

                tAffaire[0] = Convert.ToInt32(General.nz(txtNumFicheStandard.Text, 0));

                lETAE_NoAuto = ModAnalytique.fc_AnalyAffaire(tAffaire, "", "", 0, false, "");

                if (!string.IsNullOrEmpty(ModAnalytique.sWhereMulti))
                {
                    //=== extrait les numéros d'interventions.
                    stemp = ModAnalytique.sWhereMulti.Replace("WHERE", "");
                    stemp = stemp.Replace("'", "");
                    stemp = stemp.Replace("=", "");
                    stemp = stemp.Replace(")", "");
                    stemp = stemp.Replace("(", "");
                    stemp = stemp.Replace(".", "");
                    stemp = stemp.Replace("Intervention", "");
                    stemp = stemp.Replace("NumFicheStandard", "");
                    stemp = stemp.Replace(" ", "");
                    stemp = stemp.Replace("OR", "-");
                }

                //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> ligne commenté remplacé
                //dbAchatReel = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbAchatReel, 2);
                totdbAchatReel = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbAchatReel, 2);
                //dbMo = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbMoReel, 2);
                totdbMo = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbMoReel, 2);
                //dbVentesReel = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbVentesreel, 2);
                totdbVentesReel = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbVentesreel, 2);
                //dbFo = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbHTBCD, 2);
                totdbFo = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbHTBCD, 2);
                //dbSSTCap = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbSstCapacite, 2);
                totdbSSTCap = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbSstCapacite, 2);
                //dbSSTComp = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbSstCompetence, 2);
                totdbSSTComp = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbSstCompetence, 2);
                //===> Fin Modif Mondir

                var newRow = DataSource.NewRow();

                //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
                //newRow["Commun/dédié"] = "Commun";
                //newRow["VentesReels"] = totdbVentesReel;

                //DataSource.Rows.Add(newRow);
                //===> Fin Modif Mondir

                Cursor = Cursors.WaitCursor;

                string[] sTabX;

                sTabX = stemp.Split('-');

                for (X = 0; X <= sTabX.Length - 1; X++)
                {
                    Array.Resize(ref sTab, X + 1);
                    sTab[X] = Convert.ToInt32(sTabX[X]);
                }

                for (X = 0; X <= sTab.Length - 1; X++)
                {
                    Array.Resize(ref sTabTemp, 1);
                    sTabTemp[0] = sTab[X];
                    lETAE_NoAuto = ModAnalytique.fc_AnalyAffaire(sTabTemp, "", "", 0, false, "", true);

                    dbAchatReel = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbAchatReel, 2);
                    dbMo = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbMoReel, 2);
                    dbVentesReel = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbVentesreel, 2);
                    dbFo = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbHTBCD, 2);
                    dbSSTCap = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbSstCapacite, 2);
                    dbSSTComp = General.FncArrondir(ModAnalytique.tCalcaffaire[0].dbSstCompetence, 2);

                    newRow = DataSource.NewRow();
                    newRow["Commun/dédié"] = "Dédié";
                    newRow["Achats"] = dbAchatReel;
                    newRow["MO"] = dbMo;
                    newRow["ssTraitanceCap"] = dbSSTCap;
                    newRow["ssTraitanceComp"] = dbSSTComp;
                    newRow["FO"] = dbFo;
                    //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021, commented line below replaced
                    //newRow["VentesReels"] = dbVentesReel;
                    newRow["VentesReels"] = DBNull.Value;
                    //===> Fin Modif Mondir
                    DataSource.Rows.Add(newRow);

                    Cursor = Cursors.WaitCursor;
                }

                Cursor = Cursors.Default;

                //===Total.
                //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021 ===> Commented lines are replaced
                newRow = DataSource.NewRow();
                newRow["Commun/dédié"] = "Commun";
                newRow["NumFicheStandard"] = "Total";
                //newRow["Achats"] = dbAchatReel;
                newRow["Achats"] = totdbAchatReel;
                //newRow["MO"] = dbMo;
                newRow["MO"] = totdbMo;
                //newRow["ssTraitanceCap"] = dbSSTCap;
                newRow["ssTraitanceCap"] = totdbSSTCap;
                //newRow["ssTraitanceComp"] = dbSSTComp;
                newRow["ssTraitanceComp"] = totdbSSTComp;
                //newRow["FO"] = dbFo;
                newRow["FO"] = totdbFo;
                //newRow["VentesReels"] = dbVentesReel;
                newRow["VentesReels"] = totdbVentesReel;
                //===> Fin Modif Mondir
                DataSource.Rows.Add(newRow);

                GridMulti.DataSource = DataSource;

            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Mondir le 16.11.2020 pour ajouter les modfis de la version V12.11.2020
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridMulti_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            e.Layout.Bands[0].Columns["Commun/dédié"].Header.Caption = "N° Fiche d'appel";
            e.Layout.Bands[0].Columns["Achats"].Header.Caption = "Achats Réels";
            e.Layout.Bands[0].Columns["ssTraitanceCap"].Header.Caption = "SS Traitance Capacité";
            e.Layout.Bands[0].Columns["ssTraitanceComp"].Header.Caption = "SS Traitance Compétence";
            e.Layout.Bands[0].Columns["VentesReels"].Header.Caption = "Ventes Réels";
            //e.Layout.Bands[0].Columns["NumFicheStandard"].Header.Caption = "";

            e.Layout.Bands[0].Columns["Commun/dédié"].Header.VisiblePosition = 0;
            e.Layout.Bands[0].Columns["NumFicheStandard"].Header.VisiblePosition = 1;
            e.Layout.Bands[0].Columns["Achats"].Header.VisiblePosition = 2;
            e.Layout.Bands[0].Columns["MO"].Header.VisiblePosition = 3;
            e.Layout.Bands[0].Columns["ssTraitanceCap"].Header.VisiblePosition = 4;
            e.Layout.Bands[0].Columns["ssTraitanceComp"].Header.VisiblePosition = 5;
            e.Layout.Bands[0].Columns["FO"].Header.VisiblePosition = 5;
            e.Layout.Bands[0].Columns["VentesReels"].Header.VisiblePosition = 6;

        }
    }
}
