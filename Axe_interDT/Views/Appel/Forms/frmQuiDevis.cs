﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Appel.Forms
{
    public partial class frmQuiDevis : Form
    {
        public frmQuiDevis()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdMAJ_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            General.BlnMajFrmQui = true;
            General.strCodeOriCommentDevis = txtCodeOriCommentDevis.Text;
            General.strDocument = txtDoc.Text;
            this.Visible = false;
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void File1_DoubleClick(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (File1.SelectedIndex == -1)
                return;
            object UserDocument = null;
            ModuleAPI.Ouvrir(File1.Tag + "\\" + File1.SelectedItem);
        }

        private void frmQuiDevis_Load(System.Object eventSender, System.EventArgs eventArgs)
        {
            General.BlnMajFrmQui = false;
            General.strCodeOriCommentDevis = "";
            General.strDocument = "";
        }

        private void Option1_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option1.Checked)
            {
                txtCodeOriCommentDevis.Text = "0";
                cmdVisu.Visible = false;
                File1.Visible = false;
            }
        }
        private void Option2_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option2.Checked)
            {
                txtCodeOriCommentDevis.Text = "1";
                if (string.IsNullOrEmpty(txtDoc.Text))
                {
                    var _with1 = File1;
                    _with1.Tag = General.CHEMINFAX;
                    General.RefreshListBox(_with1);
                    _with1.Visible = true;
                }
                else
                {
                    File1.Visible = false;
                    cmdVisu.Visible = true;
                }
            }
        }
        private void Option3_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option3.Checked)
            {
                txtCodeOriCommentDevis.Text = "2";
                cmdVisu.Visible = false;
                File1.Visible = false;
            }
        }
        private void Option4_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option4.Checked)
            {
                txtCodeOriCommentDevis.Text = "3";
                cmdVisu.Visible = false;
                File1.Visible = false;
            }
        }
        private void Option5_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option5.Checked)
            {
                txtCodeOriCommentDevis.Text = "4";
                if (string.IsNullOrEmpty(txtDoc.Text))
                {
                    var _with2 = File1;
                    _with2.Tag = General.CHEMINCOURRIER;
                    General.RefreshListBox(_with2);
                    _with2.Visible = true;
                }
                else
                {
                    File1.Visible = false;
                    cmdVisu.Visible = true;
                }
            }
        }
        private void Option6_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option6.Checked)
            {
                txtCodeOriCommentDevis.Text = "5";
                cmdVisu.Visible = false;
                File1.Visible = false;
            }
        }
        private void Option7_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (Option7.Checked)
            {
                txtCodeOriCommentDevis.Text = "6";
                cmdVisu.Visible = false;
                File1.Visible = false;
            }
        }

        private void txtCodeOriCommentDevis_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            switch (txtCodeOriCommentDevis.Text)
            {
                case "0":
                    //téléphone
                    Option1.Checked = false;
                    Option1.Checked = true;
                    break;
                case "1":
                    //fax
                    Option2.Checked = false;
                    Option2.Checked = true;
                    break;
                case "2":
                    //internet
                    Option3.Checked = false;
                    Option3.Checked = true;
                    break;
                case "3":
                    //Répondeur
                    Option4.Checked = false;
                    Option4.Checked = true;
                    break;
                case "4":
                    //Courrier
                    Option5.Checked = false;
                    Option5.Checked = true;
                    break;
                case "5":
                    //Radio
                    Option6.Checked = false;
                    Option6.Checked = true;
                    break;
                case "6":
                    //Direct
                    Option7.Checked = false;
                    Option7.Checked = true;
                    break;
            }

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void File1_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)(eventArgs.KeyChar);
            bool boolFolder = false;

            try
            {

                //----correspondance
                if (Option5.Checked)
                {
                    if (KeyAscii == 13)
                    {
                        General.giMsg = (short)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous allez transferer une correspondance dans le repertoire " + txtnom.Text.ToUpper() + "\n" + "Ceci impliquera une validation automatique.", "Transfert de correspondance", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (General.giMsg == 6)
                        {

                            //----verif de l'existence de l immeuble
                            if (string.IsNullOrEmpty(txtnom.Text))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ref. Immeuble obligatoire");
                                eventArgs.Handled = true;
                                return;
                            }
                            else
                            {

                                //----Verifie si le dossier existe.
                                boolFolder = Directory.Exists(General.sCheminDossier + "\\" + txtnom.Text + "\\correspondance\\in\\");//tested
                                if (boolFolder == false)
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le dossier" + General.sCheminDossier + "\\" + txtnom.Text + "\\correspondance\\in\\ n'existe pas", "Transfert annulé", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    eventArgs.Handled = true;
                                    return;
                                }

                                //-----Vérifie si le fichier éxiste.
                                boolFolder = File.Exists(General.sCheminDossier + "\\" + txtnom.Text + "\\correspondance\\in\\" + File1.SelectedItem?.ToString());
                                if (boolFolder == true)//tested
                                {
                                    General.giMsg = (short)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce fichier éxiste déjà, voulez-vous le remplacer", "", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                                    if (General.giMsg == 6)
                                    {
                                        File.Delete(General.sCheminDossier + "\\" + txtnom.Text + "\\correspondance\\in\\" + File1.SelectedItem?.ToString());
                                    }
                                    else
                                    {
                                        eventArgs.Handled = true;
                                        return;
                                    }
                                }
                                General.BlnMajFrmQui = true;
                                File.Move(File1.Tag + "\\" + File1.SelectedItem?.ToString(), General.sCheminDossier + "\\" + txtnom.Text + "\\correspondance\\in\\" + File1.SelectedItem?.ToString());
                                txtDoc.Text = General.sCheminDossier + "\\" + txtnom.Text + "\\correspondance\\in\\" + File1.SelectedItem?.ToString();
                                General.BlnMajFrmQui = true;
                                General.strCodeOriCommentDevis = txtCodeOriCommentDevis.Text;
                                General.strDocument = txtDoc.Text;
                                // cmdMAJ_Click
                            }
                            File1.Visible = false;
                            cmdVisu.Visible = true;
                        }
                    }

                }

                //----transfert de fax
                if (Option2.Checked)
                {
                    if (KeyAscii == 13)
                    {
                        General.giMsg = (short)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous allez transferer un fax dans le repertoire " + txtnom.Text.ToUpper() + "\n" + "Ceci impliquera une validation automatique.", "Transfert de fax", MessageBoxButtons.YesNo, MessageBoxIcon.Question);
                        if (General.giMsg == 6)
                        {

                            //verif de l'existence de l immeuble
                            if (string.IsNullOrEmpty(txtnom.Text))
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ref. Immeuble obligatoire", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                txtnom.Focus();
                                eventArgs.Handled = true;
                                return;
                            }
                            else
                            {
                                //----Verifie si le dossier existe.
                                boolFolder = Directory.Exists(General.sCheminDossier + "\\" + txtnom.Text + "\\fax\\in\\");
                                if (boolFolder == false)
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le dossier" + General.sCheminDossier + "\\" + txtnom.Text + "\\fax\\in\\ n'existe pas", "Transfert annulé", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    eventArgs.Handled = true;
                                    return;
                                }

                                //----Verifie si le fichier éxiste déjà.
                                boolFolder = File.Exists(General.sCheminDossier + "\\" + txtnom.Text + "\\fax\\in\\" + File1.SelectedItem?.ToString());
                                if (boolFolder == true)
                                {
                                    General.giMsg = (short)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce fichier éxiste déjà, voulez-vous le remplacer", "", MessageBoxButtons.YesNo, MessageBoxIcon.Exclamation);
                                    if (General.giMsg == 6)
                                    {
                                        File.Delete(General.sCheminDossier + "\\" + txtnom.Text + "\\fax\\in\\" + File1.SelectedItem?.ToString());
                                    }
                                    else
                                    {
                                        eventArgs.Handled = true;
                                        return;
                                    }
                                }
                                General.BlnMajFrmQui = true;
                                //----transfert du fichier dans la zone fax de l'immeuble
                                File.Move(File1.Tag + "\\" + File1.SelectedItem?.ToString(), General.sCheminDossier + "\\" + txtnom.Text + "\\fax\\in\\" + File1.SelectedItem?.ToString());
                                txtDoc.Text = General.sCheminDossier + "\\" + txtnom.Text + "\\fax\\in\\" + File1.SelectedItem?.ToString();
                                General.BlnMajFrmQui = true;
                                General.strCodeOriCommentDevis = txtCodeOriCommentDevis.Text;
                                General.strDocument = txtDoc.Text;
                                // cmdMAJ_Click

                            }
                            File1.Visible = false;
                            cmdVisu.Visible = true;
                        }
                    }
                }
                General.fso = null;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
        }

        private void txtnom_TextChanged(object sender, EventArgs e)
        {

        }
    }
}
