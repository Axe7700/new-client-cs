﻿namespace Axe_interDT.Views.Appel.Forms
{
    partial class frmQuiDevis
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.cmdMAJ = new System.Windows.Forms.Button();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.cmdVisu = new System.Windows.Forms.Button();
            this.txtDoc = new iTalk.iTalk_TextBox_Small2();
            this.File1 = new System.Windows.Forms.ListBox();
            this.Option3 = new System.Windows.Forms.RadioButton();
            this.Option5 = new System.Windows.Forms.RadioButton();
            this.Option2 = new System.Windows.Forms.RadioButton();
            this.Option7 = new System.Windows.Forms.RadioButton();
            this.Option6 = new System.Windows.Forms.RadioButton();
            this.Option4 = new System.Windows.Forms.RadioButton();
            this.Option1 = new System.Windows.Forms.RadioButton();
            this.txtCodeOriCommentDevis = new iTalk.iTalk_TextBox_Small2();
            this.txtnom = new iTalk.iTalk_TextBox_Small2();
            this.groupBox6.SuspendLayout();
            this.SuspendLayout();
            // 
            // cmdMAJ
            // 
            this.cmdMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJ.FlatAppearance.BorderSize = 0;
            this.cmdMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMAJ.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdMAJ.Location = new System.Drawing.Point(419, 11);
            this.cmdMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJ.Name = "cmdMAJ";
            this.cmdMAJ.Size = new System.Drawing.Size(60, 35);
            this.cmdMAJ.TabIndex = 360;
            this.cmdMAJ.UseVisualStyleBackColor = false;
            this.cmdMAJ.Click += new System.EventHandler(this.cmdMAJ_Click);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.cmdVisu);
            this.groupBox6.Controls.Add(this.txtDoc);
            this.groupBox6.Controls.Add(this.File1);
            this.groupBox6.Controls.Add(this.Option3);
            this.groupBox6.Controls.Add(this.Option5);
            this.groupBox6.Controls.Add(this.Option2);
            this.groupBox6.Controls.Add(this.Option7);
            this.groupBox6.Controls.Add(this.Option6);
            this.groupBox6.Controls.Add(this.Option4);
            this.groupBox6.Controls.Add(this.Option1);
            this.groupBox6.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.groupBox6.Location = new System.Drawing.Point(12, 51);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(466, 170);
            this.groupBox6.TabIndex = 411;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "Origine de la demande du devis";
            // 
            // cmdVisu
            // 
            this.cmdVisu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdVisu.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdVisu.FlatAppearance.BorderSize = 0;
            this.cmdVisu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdVisu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdVisu.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmdVisu.Location = new System.Drawing.Point(400, 21);
            this.cmdVisu.Margin = new System.Windows.Forms.Padding(2);
            this.cmdVisu.Name = "cmdVisu";
            this.cmdVisu.Size = new System.Drawing.Size(60, 35);
            this.cmdVisu.TabIndex = 546;
            this.cmdVisu.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.cmdVisu.UseVisualStyleBackColor = false;
            this.cmdVisu.Visible = false;
            // 
            // txtDoc
            // 
            this.txtDoc.AccAcceptNumbersOnly = false;
            this.txtDoc.AccAllowComma = false;
            this.txtDoc.AccBackgroundColor = System.Drawing.Color.White;
            this.txtDoc.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtDoc.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtDoc.AccHidenValue = "";
            this.txtDoc.AccNotAllowedChars = null;
            this.txtDoc.AccReadOnly = false;
            this.txtDoc.AccReadOnlyAllowDelete = false;
            this.txtDoc.AccRequired = false;
            this.txtDoc.BackColor = System.Drawing.Color.White;
            this.txtDoc.CustomBackColor = System.Drawing.Color.White;
            this.txtDoc.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtDoc.ForeColor = System.Drawing.Color.Black;
            this.txtDoc.Location = new System.Drawing.Point(5, 135);
            this.txtDoc.Margin = new System.Windows.Forms.Padding(2);
            this.txtDoc.MaxLength = 32767;
            this.txtDoc.Multiline = false;
            this.txtDoc.Name = "txtDoc";
            this.txtDoc.ReadOnly = false;
            this.txtDoc.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtDoc.Size = new System.Drawing.Size(192, 27);
            this.txtDoc.TabIndex = 504;
            this.txtDoc.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtDoc.UseSystemPasswordChar = false;
            this.txtDoc.Visible = false;
            // 
            // File1
            // 
            this.File1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.File1.FormattingEnabled = true;
            this.File1.ItemHeight = 19;
            this.File1.Location = new System.Drawing.Point(203, 102);
            this.File1.Name = "File1";
            this.File1.Size = new System.Drawing.Size(257, 42);
            this.File1.TabIndex = 545;
            this.File1.Visible = false;
            this.File1.DoubleClick += new System.EventHandler(this.File1_DoubleClick);
            this.File1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.File1_KeyPress);
            // 
            // Option3
            // 
            this.Option3.AutoSize = true;
            this.Option3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Option3.Location = new System.Drawing.Point(214, 75);
            this.Option3.Name = "Option3";
            this.Option3.Size = new System.Drawing.Size(84, 23);
            this.Option3.TabIndex = 544;
            this.Option3.TabStop = true;
            this.Option3.Text = "Internet";
            this.Option3.UseVisualStyleBackColor = true;
            this.Option3.CheckedChanged += new System.EventHandler(this.Option3_CheckedChanged);
            // 
            // Option5
            // 
            this.Option5.AutoSize = true;
            this.Option5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Option5.Location = new System.Drawing.Point(214, 48);
            this.Option5.Name = "Option5";
            this.Option5.Size = new System.Drawing.Size(85, 23);
            this.Option5.TabIndex = 543;
            this.Option5.TabStop = true;
            this.Option5.Text = "Courrier";
            this.Option5.UseVisualStyleBackColor = true;
            this.Option5.CheckedChanged += new System.EventHandler(this.Option5_CheckedChanged);
            // 
            // Option2
            // 
            this.Option2.AutoSize = true;
            this.Option2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Option2.Location = new System.Drawing.Point(214, 21);
            this.Option2.Name = "Option2";
            this.Option2.Size = new System.Drawing.Size(50, 23);
            this.Option2.TabIndex = 542;
            this.Option2.TabStop = true;
            this.Option2.Text = "Fax";
            this.Option2.UseVisualStyleBackColor = true;
            this.Option2.CheckedChanged += new System.EventHandler(this.Option2_CheckedChanged);
            // 
            // Option7
            // 
            this.Option7.AutoSize = true;
            this.Option7.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Option7.Location = new System.Drawing.Point(6, 102);
            this.Option7.Name = "Option7";
            this.Option7.Size = new System.Drawing.Size(70, 23);
            this.Option7.TabIndex = 541;
            this.Option7.TabStop = true;
            this.Option7.Text = "Direct";
            this.Option7.UseVisualStyleBackColor = true;
            this.Option7.CheckedChanged += new System.EventHandler(this.Option7_CheckedChanged);
            // 
            // Option6
            // 
            this.Option6.AutoSize = true;
            this.Option6.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Option6.Location = new System.Drawing.Point(6, 75);
            this.Option6.Name = "Option6";
            this.Option6.Size = new System.Drawing.Size(64, 23);
            this.Option6.TabIndex = 540;
            this.Option6.TabStop = true;
            this.Option6.Text = "Radio";
            this.Option6.UseVisualStyleBackColor = true;
            this.Option6.CheckedChanged += new System.EventHandler(this.Option6_CheckedChanged);
            // 
            // Option4
            // 
            this.Option4.AutoSize = true;
            this.Option4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Option4.Location = new System.Drawing.Point(6, 48);
            this.Option4.Name = "Option4";
            this.Option4.Size = new System.Drawing.Size(104, 23);
            this.Option4.TabIndex = 539;
            this.Option4.TabStop = true;
            this.Option4.Text = "Répondeur";
            this.Option4.UseVisualStyleBackColor = true;
            this.Option4.CheckedChanged += new System.EventHandler(this.Option4_CheckedChanged);
            // 
            // Option1
            // 
            this.Option1.AutoSize = true;
            this.Option1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Option1.Location = new System.Drawing.Point(6, 21);
            this.Option1.Name = "Option1";
            this.Option1.Size = new System.Drawing.Size(102, 23);
            this.Option1.TabIndex = 538;
            this.Option1.TabStop = true;
            this.Option1.Text = "Téléphone";
            this.Option1.UseVisualStyleBackColor = true;
            this.Option1.CheckedChanged += new System.EventHandler(this.Option1_CheckedChanged);
            // 
            // txtCodeOriCommentDevis
            // 
            this.txtCodeOriCommentDevis.AccAcceptNumbersOnly = false;
            this.txtCodeOriCommentDevis.AccAllowComma = false;
            this.txtCodeOriCommentDevis.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeOriCommentDevis.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeOriCommentDevis.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeOriCommentDevis.AccHidenValue = "";
            this.txtCodeOriCommentDevis.AccNotAllowedChars = null;
            this.txtCodeOriCommentDevis.AccReadOnly = false;
            this.txtCodeOriCommentDevis.AccReadOnlyAllowDelete = false;
            this.txtCodeOriCommentDevis.AccRequired = false;
            this.txtCodeOriCommentDevis.BackColor = System.Drawing.Color.White;
            this.txtCodeOriCommentDevis.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeOriCommentDevis.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCodeOriCommentDevis.ForeColor = System.Drawing.Color.Black;
            this.txtCodeOriCommentDevis.Location = new System.Drawing.Point(150, 11);
            this.txtCodeOriCommentDevis.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeOriCommentDevis.MaxLength = 32767;
            this.txtCodeOriCommentDevis.Multiline = false;
            this.txtCodeOriCommentDevis.Name = "txtCodeOriCommentDevis";
            this.txtCodeOriCommentDevis.ReadOnly = false;
            this.txtCodeOriCommentDevis.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeOriCommentDevis.Size = new System.Drawing.Size(135, 27);
            this.txtCodeOriCommentDevis.TabIndex = 503;
            this.txtCodeOriCommentDevis.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeOriCommentDevis.UseSystemPasswordChar = false;
            this.txtCodeOriCommentDevis.Visible = false;
            this.txtCodeOriCommentDevis.TextChanged += new System.EventHandler(this.txtCodeOriCommentDevis_TextChanged);
            // 
            // txtnom
            // 
            this.txtnom.AccAcceptNumbersOnly = false;
            this.txtnom.AccAllowComma = false;
            this.txtnom.AccBackgroundColor = System.Drawing.Color.White;
            this.txtnom.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtnom.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtnom.AccHidenValue = "";
            this.txtnom.AccNotAllowedChars = null;
            this.txtnom.AccReadOnly = false;
            this.txtnom.AccReadOnlyAllowDelete = false;
            this.txtnom.AccRequired = false;
            this.txtnom.BackColor = System.Drawing.Color.White;
            this.txtnom.CustomBackColor = System.Drawing.Color.White;
            this.txtnom.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtnom.ForeColor = System.Drawing.Color.Black;
            this.txtnom.Location = new System.Drawing.Point(11, 11);
            this.txtnom.Margin = new System.Windows.Forms.Padding(2);
            this.txtnom.MaxLength = 32767;
            this.txtnom.Multiline = false;
            this.txtnom.Name = "txtnom";
            this.txtnom.ReadOnly = false;
            this.txtnom.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtnom.Size = new System.Drawing.Size(135, 27);
            this.txtnom.TabIndex = 502;
            this.txtnom.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtnom.UseSystemPasswordChar = false;
            this.txtnom.Visible = false;
            this.txtnom.TextChanged += new System.EventHandler(this.txtnom_TextChanged);
            // 
            // frmQuiDevis
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(490, 233);
            this.Controls.Add(this.txtCodeOriCommentDevis);
            this.Controls.Add(this.txtnom);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.cmdMAJ);
            this.Name = "frmQuiDevis";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Origine du devis";
            this.Load += new System.EventHandler(this.frmQuiDevis_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        public System.Windows.Forms.Button cmdMAJ;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.RadioButton Option3;
        private System.Windows.Forms.RadioButton Option5;
        private System.Windows.Forms.RadioButton Option2;
        private System.Windows.Forms.RadioButton Option7;
        private System.Windows.Forms.RadioButton Option6;
        private System.Windows.Forms.RadioButton Option4;
        private System.Windows.Forms.RadioButton Option1;
        public iTalk.iTalk_TextBox_Small2 txtDoc;
        private System.Windows.Forms.ListBox File1;
        public iTalk.iTalk_TextBox_Small2 txtnom;
        public iTalk.iTalk_TextBox_Small2 txtCodeOriCommentDevis;
        public System.Windows.Forms.Button cmdVisu;
    }
}