﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Appel.Forms
{
    public partial class frmAjoutGestionnaire : Form
    {
        public frmAjoutGestionnaire()
        {
            InitializeComponent();
        }

        string sSQL;
        DataTable rsCorrespondants = null;
        ModAdo modAdorsCorrespondants = null;
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_ChargeGrilleGestionnaire(string sCode)
        {


            sSQL = "SELECT Gestionnaire.CodeGestinnaire, Gestionnaire.NomGestion," 
                + " Gestionnaire.CodeQualif_Qua, Gestionnaire.Qualification as Qualification," 
                + " Gestionnaire.TelGestion, Gestionnaire.TelPortable_GES," 
                + " Gestionnaire.FaxGestion,  Gestionnaire.EMail_GES," 
                + " Gestionnaire.Horaire_GES, Gestionnaire.MotdePasse_GES, Gestionnaire.code1" 
                + " , AncienEmployeur, ID_LOGIN "
                + " FROM Gestionnaire ";
            sSQL = sSQL + " WHERE Gestionnaire.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";

            modAdorsCorrespondants = new ModAdo();
            rsCorrespondants = modAdorsCorrespondants.fc_OpenRecordSet(sSQL);
            this.ssGridGestionnaire.DataSource = rsCorrespondants;

            fc_DropQualifGest();//tested

        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_DropQualifGest()
        {
            // alimente la combo liée a la grille des intervenants avce la liste du personnel
            sSQL = "";
            sSQL = "SELECT Qualification.CodeQualif,Qualification.Qualification " + " FROM Qualification WHERE Correspondant_COR=1";

            if (ComboQualifGest.Rows.Count == 0)
            {
                sheridan.InitialiseCombo((this.ComboQualifGest), sSQL, "CodeQualif", true);
                this.ssGridGestionnaire.DisplayLayout.Bands[0].Columns["CodeQualif_QUA"].ValueList = this.ComboQualifGest;
            }

        }

        private void CmdSauver_Click(object sender, EventArgs e)
        {
            ssGridGestionnaire.UpdateData();
            fc_InfoGestion((ssDropGestionnaire.Text), txtCode1.Text);

            if (string.IsNullOrEmpty(lblNameGestionnaire.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce code Gestionnaire n'existe pas", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            General.Execute("UPDATE immeuble Set CodeGestionnaire ='" + StdSQLchaine.gFr_DoublerQuote(ssDropGestionnaire.Text) + "'" +
                " WHERE CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'");
        }
        /// <summary>
        /// tesed
        /// </summary>
        /// <param name="sCodeGestionnaire"></param>
        /// <param name="sCode1"></param>
        private void fc_InfoGestion(string sCodeGestionnaire, string sCode1)
        {
            sSQL = "SELECT NomGestion" 
                + " FROM Gestionnaire WHERE CodeGestinnaire = '" + StdSQLchaine.gFr_DoublerQuote(sCodeGestionnaire) + "'" 
                + " and Code1='" + StdSQLchaine.gFr_DoublerQuote(sCode1) + "'";

            using (var tmpModAdo = new ModAdo())
                lblNameGestionnaire.Text = tmpModAdo.fc_ADOlibelle(sSQL);

        }
        //tested
        private void frmAjoutGestionnaire_Activated(object sender, EventArgs e)
        {
            using (var tmpModAdo = new ModAdo())
                txtGerant.Text = tmpModAdo.fc_ADOlibelle("SELECT Nom FROM Table1 " + " WHERE Code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'");

            fc_ChargeGrilleGestionnaire(txtCode1.Text);
            View.Theme.Theme.recursiveLoopOnFrms(this);
        }

        private void ssDropGestionnaire_ValueChanged(object sender, EventArgs e)
        {
            fc_InfoGestion((ssDropGestionnaire.Value.ToString()), txtCode1.Text);
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDropGestionnaire_AfterCloseUp(object sender, EventArgs e)
        {
            lblNameGestionnaire.Text = ssDropGestionnaire.ActiveRow!=null ? ssDropGestionnaire.ActiveRow.Cells["NomGestion"].Text.ToString():"";
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssDropGestionnaire_BeforeDropDown(object sender, CancelEventArgs e)
        {
            //initialise les listes déroulantes en fonction du client
            sSQL = "SELECT Gestionnaire.CodeGestinnaire, Gestionnaire.NomGestion," 
                + " Gestionnaire.CodeQualif_Qua, Gestionnaire.Qualification as Qualification" 
                + " FROM Gestionnaire ";
            sSQL = sSQL + " WHERE Gestionnaire.Code1 ='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";

            sheridan.InitialiseCombo(ssDropGestionnaire, sSQL, "CodeGestinnaire");
        }

        private void ssGridGestionnaire_AfterExitEditMode(object sender, EventArgs e)
        {

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_RechercheQualite(string sCode)
        {
            // récupére dans le controle adodc1 les informations liées au matricule.
            if ((ComboQualifGest.DataSource != null))
            {
                for (int i = 0; i < ComboQualifGest.Rows.Count; i++)
                {
                    if (ComboQualifGest.Rows[i].Cells["CodeQualif"].Value.ToString().ToUpper() == sCode.ToUpper())
                    {

                        ssGridGestionnaire.ActiveRow.Cells["Qualification"].Value = ComboQualifGest.Rows[i].Cells["Qualification"].Value;

                        break;
                    }
                }

            }
        }

        private void ssGridGestionnaire_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            string sCode = null;

            string sLogin = null;
            var _with1 = this.ssGridGestionnaire;

            if (string.IsNullOrEmpty(_with1.ActiveRow.Cells["code1"].Value.ToString()) || string.IsNullOrEmpty(_with1.ActiveRow.Cells["code1"].Text))//tested
            {
                _with1.ActiveRow.Cells["code1"].Value = txtCode1.Text;
            }

            if (string.IsNullOrEmpty(_with1.ActiveRow.Cells["CodeQualif_Qua"].Value.ToString()))//tested
            {
                _with1.ActiveRow.Cells["CodeQualif_Qua"].Value = General.cGestionnaire;
                sSQL = "";
                sSQL = "SELECT Qualification.Qualification" + " From Qualification" 
                    + " WHERE Qualification.CodeQualif ='" + ssGridGestionnaire.ActiveRow.Cells["CodeQualif_QUA"].Value + "'";

                General.modAdorstmp = new ModAdo();
                General.rstmp = General.modAdorstmp.fc_OpenRecordSet(sSQL);
                if (General.rstmp.Rows.Count > 0)
                {
                    ssGridGestionnaire.ActiveRow.Cells["Qualification"].Value = General.nz(General.rstmp.Rows[0]["Qualification"], "");
                }
            }

            if (!string.IsNullOrEmpty(_with1.ActiveRow.Cells["CodeQualif_QUA"].Value.ToString()))
            {
                fc_RechercheQualite((_with1.ActiveRow.Cells["CodeQualif_QUA"].Value.ToString()));//tested
            }

            if (_with1.ActiveCell.Column.Key.ToUpper() == "CodeGestinnaire".ToUpper() && !string.IsNullOrEmpty(_with1.ActiveRow.Cells["CodeGestinnaire"].Text))//tested
            {
                sCode = General.fc_FormatCode(_with1.ActiveRow.Cells["CodeGestinnaire"].Text);
                if (string.IsNullOrEmpty(sCode))
                {
                    e.Cancel = true;

                }
                else
                {
                    _with1.ActiveRow.Cells["CodeGestinnaire"].Value = sCode;
                }

            }

            //==== Creation du compte extranet.
            if (_with1.ActiveCell.Column.Key.ToUpper() == "MotdePasse_GES".ToUpper())
            {
                using (var tmpModAdo = new ModAdo())
                    sLogin = tmpModAdo.fc_ADOlibelle("SELECT Login.Login FROM Login WHERE Login='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'");
                if (_with1.ActiveRow.Cells["MotdePasse_GES"].Text.ToUpper() == sLogin.ToUpper())
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention le mot de passe du gestionnaire doit être différent " + " du mot de passe du gérant.", "Mot de passe", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    e.Cancel = true;
                }
                else
                {
                    fc_LogGestExtranet();//tested
                }
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        private void fc_LogGestExtranet()
        {
            DataTable rslogin = default(DataTable);
            ModAdo modAdorslogin = null;
            string sSQL = null;

            try
            {
                var _with2 = ssGridGestionnaire;

                modAdorslogin = new ModAdo();
                sSQL = "SELECT Login,PassWord,Gerant, ID_LOgin FROM Login  ";

                if (!string.IsNullOrEmpty(_with2.ActiveRow.Cells["ID_LOgin"].Text))//tested
                {

                    rslogin = modAdorslogin.fc_OpenRecordSet(sSQL + " WHERE ID_LOgin=" + _with2.ActiveRow.Cells["ID_LOgin"].Text);

                    if (rslogin.Rows.Count > 0)
                    {
                        //== si log existant et mot de passe vide,on supprime
                        if (string.IsNullOrEmpty(_with2.ActiveRow.Cells["MotdePasse_GES"].Text))//tested
                        {
                            // rslogin.Rows.RemoveAt(0);
                            rslogin.Rows.Remove(rslogin.Rows[0]);
                            int d=General.Execute("Delete From Login WHERE ID_LOgin=" +_with2.ActiveRow.Cells["ID_LOgin"].Text);
                            modAdorslogin.Update();
                            //== le compte
                            _with2.ActiveRow.Cells["ID_LOgin"].Value=DBNull.Value;
                        }
                        else//tested
                        {
                            rslogin.Rows[0]["Password"] = _with2.ActiveRow.Cells["MotdePasse_GES"].Text;
                            modAdorslogin.Update();
                        }

                        //== si log inexsistant et presence
                    }
                    else if (!string.IsNullOrEmpty(_with2.ActiveRow.Cells["MotdePasse_GES"].Text))//tested
                    {
                        var NewRow = rslogin.Rows[0];
                        //== du mot de passe, on crée le compte.
                        NewRow["Login"] = txtCode1.Text;
                        NewRow["Gerant"] = 0;
                        NewRow["Password"] = _with2.ActiveRow.Cells["MotdePasse_GES"].Text;
                        modAdorslogin.Update();
                        using (var tmpModAdo = new ModAdo())
                            _with2.ActiveRow.Cells["ID_LOgin"].Value = tmpModAdo.fc_ADOlibelle("SELEXT MAX(ID_LOgin) FROM Login");
                    }



                }
                else if (!string.IsNullOrEmpty(_with2.ActiveRow.Cells["MotdePasse_GES"].Text))//tested
                {

                    //            rslogin.Open "SELECT Login.Login,Login.PassWord,Login.Gerant" _
                    //'            & " FROM Login WHERE Login.Login='" & gFr_DoublerQuote(txtCode1) _
                    //'            & "' AND Gerant=0 and password='" & .Columns("MotdePasse_GES").Text & "'", adocnn, adOpenKeyset, adLockOptimistic
                    modAdorslogin = new ModAdo();
                    rslogin = modAdorslogin.fc_OpenRecordSet(sSQL + " WHERE ID_Login = 0");

                    //            If Not rslogin.EOF And Not rslogin.BOF Then
                    //                 rslogin!Password = .Columns("MotdePasse_GES").Text
                    //                 rslogin.Update
                    //            Else
                    var NewRow = rslogin.NewRow();
                    NewRow["Login"] = txtCode1.Text;
                    NewRow["Gerant"] = 0;
                    NewRow["Password"] = _with2.ActiveRow.Cells["MotdePasse_GES"].Text;
                    rslogin.Rows.Add(NewRow);
                    modAdorslogin.Update();
                    using (var tmpModAdo = new ModAdo())
                        _with2.ActiveRow.Cells["ID_LOgin"].Value = tmpModAdo.fc_ADOlibelle("SELECT MAX(ID_LOgin) FROM Login");
                    // .Update
                    //            End If

                }

                modAdorslogin?.Dispose();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_LogExtranet");
            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGestionnaire_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            sSQL = "";
            sSQL = "SELECT Qualification.Qualification" + " From Qualification" + " WHERE Qualification.CodeQualif ='" + e.Row.Cells["CodeQualif_QUA"].Value + "'";
            General.modAdorstmp = new ModAdo();
            General.rstmp = General.modAdorstmp.fc_OpenRecordSet(sSQL);
            if (General.rstmp.Rows.Count > 0)
            {
                e.Row.Cells["Qualification"].Value = General.nz(General.rstmp.Rows[0]["Qualification"], "");
            }
            
        }

        private void ssGridGestionnaire_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {           
            ssGridGestionnaire.DisplayLayout.Bands[0].Columns["ID_Login"].Hidden = true;
            ssGridGestionnaire.DisplayLayout.Bands[0].Columns["code1"].Hidden = true;
            ssGridGestionnaire.DisplayLayout.Bands[0].Columns["MotdePasse_Ges"].Header.Caption = "Mot de Passe";
            ssGridGestionnaire.DisplayLayout.Bands[0].Columns["Horaire_Ges"].Header.Caption = "Horaire";
            ssGridGestionnaire.DisplayLayout.Bands[0].Columns["EMail_GES"].Header.Caption = "EMail";
            ssGridGestionnaire.DisplayLayout.Bands[0].Columns["FaxGestion"].Header.Caption = "Fax";
            ssGridGestionnaire.DisplayLayout.Bands[0].Columns["TelPortable_GES"].Header.Caption = "Tel Portable";
            ssGridGestionnaire.DisplayLayout.Bands[0].Columns["TelGestion"].Header.Caption = "Tel.";
            ssGridGestionnaire.DisplayLayout.Bands[0].Columns["CodeQualif_Qua"].Header.Caption = "CodeQualif";
            ssGridGestionnaire.DisplayLayout.Bands[0].Columns["NomGestion"].Header.Caption = "Nom";
            ssGridGestionnaire.DisplayLayout.Bands[0].Columns["Qualification"].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            ssGridGestionnaire.DisplayLayout.Bands[0].Columns["Qualification"].CellAppearance.BackColor = Color.Gray;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGestionnaire_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {
           
            if (e.Row.Cells["CodeGestinnaire"].DataChanged)
            {
                if (e.Row.Cells["CodeGestinnaire"].OriginalValue.ToString() != e.Row.Cells["CodeGestinnaire"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM Gestionnaire WHERE CodeGestinnaire= '{e.Row.Cells["CodeGestinnaire"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }

            }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGestionnaire_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            modAdorsCorrespondants.Update();

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssGridGestionnaire_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdorsCorrespondants.Update();
        }

        private void frmAjoutGestionnaire_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);
        }
    }
}