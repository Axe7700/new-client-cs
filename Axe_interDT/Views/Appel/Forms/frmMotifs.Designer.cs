﻿namespace Axe_interDT.Views.Appel.Forms
{
    partial class frmMotifs
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMotifs));
            this.label33 = new System.Windows.Forms.Label();
            this.cmdSauver = new System.Windows.Forms.Button();
            this.txtCodeMotif = new iTalk.iTalk_TextBox_Small2();
            this.SSOleDBGrid1 = new Infragistics.Win.UltraWinGrid.UltraGrid();
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).BeginInit();
            this.SuspendLayout();
            // 
            // label33
            // 
            this.label33.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(12, 9);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(659, 56);
            this.label33.TabIndex = 384;
            this.label33.Text = resources.GetString("label33.Text");
            // 
            // cmdSauver
            // 
            this.cmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdSauver.FlatAppearance.BorderSize = 0;
            this.cmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdSauver.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdSauver.Location = new System.Drawing.Point(611, 5);
            this.cmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.cmdSauver.Name = "cmdSauver";
            this.cmdSauver.Size = new System.Drawing.Size(60, 35);
            this.cmdSauver.TabIndex = 413;
            this.cmdSauver.UseVisualStyleBackColor = false;
            this.cmdSauver.Click += new System.EventHandler(this.cmdSauver_Click);
            // 
            // txtCodeMotif
            // 
            this.txtCodeMotif.AccAcceptNumbersOnly = false;
            this.txtCodeMotif.AccAllowComma = false;
            this.txtCodeMotif.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeMotif.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeMotif.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeMotif.AccHidenValue = "";
            this.txtCodeMotif.AccNotAllowedChars = null;
            this.txtCodeMotif.AccReadOnly = false;
            this.txtCodeMotif.AccReadOnlyAllowDelete = false;
            this.txtCodeMotif.AccRequired = false;
            this.txtCodeMotif.BackColor = System.Drawing.Color.White;
            this.txtCodeMotif.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeMotif.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCodeMotif.ForeColor = System.Drawing.Color.Black;
            this.txtCodeMotif.Location = new System.Drawing.Point(536, 44);
            this.txtCodeMotif.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeMotif.MaxLength = 32767;
            this.txtCodeMotif.Multiline = false;
            this.txtCodeMotif.Name = "txtCodeMotif";
            this.txtCodeMotif.ReadOnly = false;
            this.txtCodeMotif.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeMotif.Size = new System.Drawing.Size(135, 27);
            this.txtCodeMotif.TabIndex = 502;
            this.txtCodeMotif.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeMotif.UseSystemPasswordChar = false;
            this.txtCodeMotif.Visible = false;
            // 
            // SSOleDBGrid1
            // 
            this.SSOleDBGrid1.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.SSOleDBGrid1.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid1.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid1.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSOleDBGrid1.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSOleDBGrid1.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.SSOleDBGrid1.DisplayLayout.UseFixedHeaders = true;
            this.SSOleDBGrid1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSOleDBGrid1.Location = new System.Drawing.Point(15, 76);
            this.SSOleDBGrid1.Name = "SSOleDBGrid1";
            this.SSOleDBGrid1.Size = new System.Drawing.Size(656, 481);
            this.SSOleDBGrid1.TabIndex = 572;
            this.SSOleDBGrid1.Text = "Libelle du Motif";
            this.SSOleDBGrid1.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.SSOleDBGrid1_InitializeLayout);
            this.SSOleDBGrid1.AfterRowsDeleted += new System.EventHandler(this.SSOleDBGrid1_AfterRowsDeleted);
            this.SSOleDBGrid1.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.SSOleDBGrid1_AfterRowUpdate);
            this.SSOleDBGrid1.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.SSOleDBGrid1_BeforeRowUpdate);
            this.SSOleDBGrid1.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.SSOleDBGrid1_BeforeRowsDeleted);
            this.SSOleDBGrid1.DoubleClickRow += new Infragistics.Win.UltraWinGrid.DoubleClickRowEventHandler(this.SSOleDBGrid1_DoubleClickRow);
            this.SSOleDBGrid1.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.SSOleDBGrid1_KeyPress);
            // 
            // frmMotifs
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(683, 569);
            this.Controls.Add(this.SSOleDBGrid1);
            this.Controls.Add(this.txtCodeMotif);
            this.Controls.Add(this.cmdSauver);
            this.Controls.Add(this.label33);
            this.Name = "frmMotifs";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Gestion des Motifs d\'appels";
            this.Load += new System.EventHandler(this.frmMotifs_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SSOleDBGrid1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label33;
        public System.Windows.Forms.Button cmdSauver;
        public iTalk.iTalk_TextBox_Small2 txtCodeMotif;
        public Infragistics.Win.UltraWinGrid.UltraGrid SSOleDBGrid1;
    }
}