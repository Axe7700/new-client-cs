﻿using Axe_interDT.Shared;
using Axe_interDT.Views.SharedViews;
using CrystalDecisions.CrystalReports.Engine;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Appel.Forms
{
    public partial class frmHistorique : Form
    {
        DataTable SSOleDBGridAppelsHistoSource = null;
        ReportDocument CR = null;

        public frmHistorique()
        {
            InitializeComponent();
        }

        private void Command1_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            this.Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        public void FourniGrilleInterHisto()
        {

            DataTable rs2 = default(DataTable);
            ModAdo modAdors2 = null;
            string sql2 = null;
            SSOleDBGridInterHisto.DataSource = null;

            //    sql2 = "SELECT dbo.Intervention.CodeImmeuble, dbo.Intervention.Commentaire, dbo.Intervention.Commentaire2, dbo.Intervention.Commentaire3, " _
            //'        & "dbo.Intervention.Intervenant,dbo.Intervention.DateSaisie, dbo.Intervention.DatePrevue, dbo.Intervention.DateRealise, dbo.Intervention.CodeEtat, dbo.Interventoin.Designation " _
            //'        & "dbo.TypeCodeEtat.CodeEtat , dbo.TypeCodeEtat.LibelleCodeEtat, dbo.Personnel.Nom, dbo.Personnel.Prenom, dbo.Personnel.Matricule " _
            //'        & "FROM dbo.Intervention LEFT JOIN " _
            //'        & "dbo.TypeCodeEtat ON dbo.Intervention.CodeEtat = dbo.TypeCodeEtat.CodeEtat LEFT JOIN " _
            //'        & "dbo.Personnel ON dbo.Intervention.Intervenant = dbo.Personnel.Matricule " _
            //'        & "WHERE (dbo.Intervention.CodeImmeuble = '" & NomImmeuble & "') " _
            //'        & "ORDER BY dbo.Intervention.DateSaisie DESC"

            sql2 = "SELECT dbo.Intervention.DateSaisie, dbo.Intervention.DateRealise,dbo.Intervention.CodeEtat," +
                " dbo.Intervention.Intervenant,dbo.Intervention.CodeImmeuble, dbo.Intervention.Designation,"
                + " dbo.Intervention.Commentaire, dbo.Intervention.Commentaire2," +
                "dbo.Intervention.Commentaire3,dbo.Intervention.DatePrevue, " +
                " dbo.Intervention.NoIntervention " + "FROM dbo.Intervention "
                + "WHERE (dbo.Intervention.CodeImmeuble = '" + General.NomImmeuble + "') ";

            //voir https://groupe-dt.mantishub.io/view.php?id=1619 sur mantice
            //sql2 = sql2 + " ORDER BY dbo.Intervention.DateRealise DESC";
            sql2 = sql2 + " ORDER BY dbo.Intervention.DateSaisie DESC";
            
            //=== modif du 02 04 2019.
            //sql2 = sql2 & " ORDER BY dbo.Intervention.DateSaisie DESC"

            modAdors2 = new ModAdo();
            rs2 = modAdors2.fc_OpenRecordSet(sql2);
            SSOleDBGridInterHisto.DataSource = rs2;
        }

        /// <summary>
        /// tested
        /// </summary>
        public void FourniGrilleDevisHisto()
        {

            DataTable rs2 = default(DataTable);
            ModAdo modAdors2 = null;
            string sql2 = null;

            SSOleDBGridDevisHisto.DataSource = null;

            sql2 = "SELECT  DevisEntete.TitreDevis, DevisCodeEtat.Libelle, DevisEntete.Observations, DevisEntete.DateCreation," +
                "DevisEntete.numerodevis, DevisEntete.CodeEtat,DevisCodeEtat.Code FROM DevisEntete LEFT JOIN " 
                + "DevisCodeEtat ON DevisEntete.CodeEtat = DevisCodeEtat.Code " 
                + "WHERE (DevisEntete.CodeImmeuble = '" + General.NomImmeuble + "') " +
                "ORDER BY DevisEntete.DateCreation DESC";

            modAdors2 = new ModAdo();
            rs2 = modAdors2.fc_OpenRecordSet(sql2);
            SSOleDBGridDevisHisto.DataSource = rs2;
          
        }
        /// <summary>
        /// tested
        /// </summary>
        public void FourniGrilleAppelsHisto()
        {

            DataTable rs2 = default(DataTable);
            ModAdo modAdors2 = new ModAdo();
            string sql2 = null;


            SSOleDBGridAppelsHisto.DataSource = null;

            //SQL2 = "SELECT CAI_CategoriInterv.CAI_Libelle, CAI_CategoriInterv.CAI_Code, GestionStandard.CAI_Code AS Expr1, GestionStandard.DateAjout, " _
            //'    & "GestionStandard.Utilisateur, GestionStandard.IntervenantDT, GestionStandard.Observations, GestionStandard.CodeImmeuble, " _
            //'    & "Personnel.Nom , Personnel.Prenom, Personnel.Initiales, GestionStandard.CodeOrigine2, " _
            //'    & "GestionStandard.Source FROM GestionStandard LEFT JOIN " _
            //'    & "Personnel ON GestionStandard.IntervenantDT = Personnel.Matricule LEFT JOIN " _
            //'    & "CAI_CategoriInterv ON GestionStandard.CAI_Code = CAI_CategoriInterv.CAI_Code " _
            //'    & "WHERE (GestionStandard.CodeImmeuble = '" & NomImmeuble & "') " _
            //'    & "ORDER BY DateAjout DESC"

            sql2 = "SELECT GestionStandard.DateAjout, GestionStandard.CodeOrigine2, GestionStandard.Source, " + "GestionStandard.Utilisateur, GestionStandard.IntervenantDT, GestionStandard.Observations," +
                " GestionStandard.CodeImmeuble, " + "Personnel.Nom , Personnel.Prenom, Personnel.Initiales, FacArticle.Designation1 " + "FROM GestionStandard LEFT JOIN " +
                "Personnel ON dbo.GestionStandard.IntervenantDT = Personnel.Matricule AND Personnel.Matricule<>''" + " LEFT JOIN FacArticle ON GestionStandard.CAI_Code = FacArticle.CodeArticle WHERE " +
                "(GestionStandard.CodeImmeuble = '" + General.NomImmeuble + "') " + "ORDER BY DateAjout DESC";

            SSOleDBGridAppelsHistoSource = new DataTable();
            SSOleDBGridAppelsHistoSource.Columns.Add("Date");
            SSOleDBGridAppelsHistoSource.Columns.Add("Motif");
            SSOleDBGridAppelsHistoSource.Columns.Add("Interlocuteur");
            SSOleDBGridAppelsHistoSource.Columns.Add("Observations");
            SSOleDBGridAppelsHistoSource.Columns.Add("Interlocuteur DT");

            rs2 = modAdors2.fc_OpenRecordSet(sql2);

            if (rs2.Rows.Count > 0)
            {
                foreach (DataRow rs2Row in rs2.Rows)
                {
                    if (rs2Row["CodeOrigine2"].ToString() == General.cCopro.ToString())
                    {
                        SSOleDBGridAppelsHistoSource.Rows.Add(rs2Row["DateAjout"], rs2Row["Designation1"], rs2Row["Source"], rs2Row["Observations"], rs2Row["Utilisateur"]);
                    }
                    else if (rs2Row["CodeOrigine2"].ToString() == General.cGerant.ToString())
                    {
                        SSOleDBGridAppelsHistoSource.Rows.Add(rs2Row["DateAjout"], rs2Row["Designation1"], rs2Row["Source"], rs2Row["Observations"], rs2Row["Utilisateur"]);
                    }
                    else if (rs2Row["CodeOrigine2"].ToString() == General.cFournisseur.ToString())
                    {
                        SSOleDBGridAppelsHistoSource.Rows.Add(rs2Row["DateAjout"], rs2Row["Designation1"], rs2Row["Source"], rs2Row["Observations"], rs2Row["Utilisateur"]);
                    }
                    else if (rs2Row["CodeOrigine2"].ToString() == General.cGardien.ToString())
                    {
                        SSOleDBGridAppelsHistoSource.Rows.Add(rs2Row["DateAjout"], rs2Row["Designation1"], rs2Row["Source"], rs2Row["Observations"], rs2Row["Utilisateur"]);
                    }
                    else if (rs2Row["CodeOrigine2"].ToString() == General.cDivers.ToString())
                    {
                        SSOleDBGridAppelsHistoSource.Rows.Add(rs2Row["DateAjout"], rs2Row["Designation1"], rs2Row["Source"], rs2Row["Observations"], rs2Row["Utilisateur"]);
                    }
                    else if (rs2Row["CodeOrigine2"].ToString() == General.cSociete.ToString())
                    {
                        SSOleDBGridAppelsHistoSource.Rows.Add(rs2Row["DateAjout"], rs2Row["Designation1"], rs2Row["Nom"] + " " + rs2Row["Prenom"], rs2Row["Observations"], rs2Row["Utilisateur"]);
                    }
                    else
                    {
                        SSOleDBGridAppelsHistoSource.Rows.Add(rs2Row["DateAjout"], rs2Row["Designation1"], rs2Row["Source"], rs2Row["Observations"], rs2Row["Utilisateur"]);
                    }
                }
            }

            SSOleDBGridAppelsHisto.DataSource = SSOleDBGridAppelsHistoSource;

        }



        private void frmHistorique_Activated(System.Object eventSender, System.EventArgs eventArgs)
        {
            this.Text = "Historique des interventions, des appels et des devis pour l'immeuble " + General.NomImmeuble;
            System.Windows.Forms.Application.DoEvents();
            FourniGrilleInterHisto();
            FourniGrilleAppelsHisto();
            FourniGrilleDevisHisto();


            SSOleDBGridAppelsHisto.DisplayLayout.Bands[0].Columns["Interlocuteur DT"].Header.Caption = General.sInterlocuteurHistoAppel;
        }

        private void SSOleDBGridDevisHisto_DblClick(System.Object eventSender, System.EventArgs eventArgs)
        {
            fc_VisuDevis();
        }
        private void fc_VisuDevis()
        {
            object UserDocument = null;

            if (SSOleDBGridDevisHisto.ActiveRow == null)
                return;

            try
            {
                CR = new ReportDocument();
                if (OptDebourseAff.Checked == true)
                {
                    if (CR.FileName != General.strDTDebourseAffaire)
                    {
                        CR.Load(General.gsRpt + General.strDTDebourseAffaire);

                        CrystalReportFormView Form = new CrystalReportFormView(CR, "{DevisEnTete.NumeroDevis} ='" + SSOleDBGridDevisHisto.ActiveRow.Cells["NumeroDevis"].Value + "'");
                        Form.Show();
                    }

                }
                else if (optForfaitairePos.Checked == true)
                {
                    General.tabParametresDevis = new General.ParamDevis[3];

                    //        prcImpression strDTDevis, 0, CInt(Text4)
                    General.tabParametresDevis[0].sName = "Parametre";
                    General.tabParametresDevis[0].sValue = "";
                    General.tabParametresDevis[1].sName = "MasqueSomme ";
                    General.tabParametresDevis[1].sValue = "";
                    General.fc_ShowDevis((SSOleDBGridDevisHisto.ActiveRow.Cells["NumeroDevis"].Value.ToString()));
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, Name + ";btnDebourse_Click");
            }
        }
        int i = 0;
        private void SSOleDBGridAppelsHisto_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {  
            SSOleDBGridAppelsHisto.DisplayLayout.Bands[0].Columns["Observations"].Hidden = true;
           
            for (i = 0; i <= (SSOleDBGridAppelsHisto.DisplayLayout.Bands[0].Columns.Count - 1); i += 1)
            {
                SSOleDBGridAppelsHisto.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }
            
        }

        private void SSOleDBGridInterHisto_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

            SSOleDBGridInterHisto.DisplayLayout.Bands[0].Columns["Commentaire2"].Hidden = true;
            SSOleDBGridInterHisto.DisplayLayout.Bands[0].Columns["Commentaire3"].Hidden = true;
            SSOleDBGridInterHisto.DisplayLayout.Bands[0].Columns["CodeImmeuble"].Hidden = true;
            SSOleDBGridInterHisto.DisplayLayout.Bands[0].Columns["DatePrevue"].Hidden = true;
            SSOleDBGridInterHisto.DisplayLayout.Bands[0].Columns["Intervenant"].Header.Caption = "Technicien";
            SSOleDBGridInterHisto.DisplayLayout.Bands[0].Columns["CodeEtat"].Header.Caption ="Statut";

            for (i = 0; i <= (SSOleDBGridInterHisto.DisplayLayout.Bands[0].Columns.Count - 1); i += 1)
            {
                SSOleDBGridInterHisto.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }

        }

        private void SSOleDBGridDevisHisto_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns["numerodevis"].Header.Caption = "Numero Devis";
            SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns["TitreDevis"].Header.Caption = "Titre";
            SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns["DateCreation"].Header.Caption = "Date de création";
            SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns["Libelle"].Header.Caption = "Etat";
            SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns["Observations"].Hidden = true;
            SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns["CodeEtat"].Hidden = true;
            SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns["Code"].Hidden = true;

            for (i = 0; i <= (SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns.Count - 1); i += 1)
            {
                SSOleDBGridDevisHisto.DisplayLayout.Bands[0].Columns[i].CellActivation = Infragistics.Win.UltraWinGrid.Activation.NoEdit;
            }

        }

        private void SSOleDBGridInterHisto_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)
        {
            if(General.sNoInterHistorique == General.cOkToNavigue)
            {
                General.sNoInterHistorique = e.Row.Cells["NoIntervention"].Value.ToString();
                Close();
            }
        }

        private void frmHistorique_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);
        }
    }
}
