﻿namespace Axe_interDT.Views.Appel.Forms
{
    partial class frmDossierMail
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.lblIntervention = new System.Windows.Forms.Label();
            this.cmdQuitter = new System.Windows.Forms.Button();
            this.FileIntervention = new System.Windows.Forms.ListBox();
            this.SuspendLayout();
            // 
            // lblIntervention
            // 
            this.lblIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblIntervention.Location = new System.Drawing.Point(12, 9);
            this.lblIntervention.Name = "lblIntervention";
            this.lblIntervention.Size = new System.Drawing.Size(323, 38);
            this.lblIntervention.TabIndex = 384;
            this.lblIntervention.Text = "Mails presents pour ";
            this.lblIntervention.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            // 
            // cmdQuitter
            // 
            this.cmdQuitter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdQuitter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdQuitter.FlatAppearance.BorderSize = 0;
            this.cmdQuitter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdQuitter.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdQuitter.ForeColor = System.Drawing.Color.White;
            this.cmdQuitter.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.cmdQuitter.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdQuitter.Location = new System.Drawing.Point(127, 457);
            this.cmdQuitter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdQuitter.MaximumSize = new System.Drawing.Size(85, 35);
            this.cmdQuitter.MinimumSize = new System.Drawing.Size(85, 35);
            this.cmdQuitter.Name = "cmdQuitter";
            this.cmdQuitter.Size = new System.Drawing.Size(85, 35);
            this.cmdQuitter.TabIndex = 567;
            this.cmdQuitter.Text = "   Quitter";
            this.cmdQuitter.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdQuitter.UseVisualStyleBackColor = false;
            this.cmdQuitter.Click += new System.EventHandler(this.cmdQuitter_Click);
            // 
            // FileIntervention
            // 
            this.FileIntervention.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.FileIntervention.FormattingEnabled = true;
            this.FileIntervention.ItemHeight = 19;
            this.FileIntervention.Location = new System.Drawing.Point(12, 50);
            this.FileIntervention.Name = "FileIntervention";
            this.FileIntervention.Size = new System.Drawing.Size(323, 384);
            this.FileIntervention.TabIndex = 568;
            this.FileIntervention.DoubleClick += new System.EventHandler(this.FileIntervention_DoubleClick);
            // 
            // frmDossierMail
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(347, 496);
            this.Controls.Add(this.FileIntervention);
            this.Controls.Add(this.cmdQuitter);
            this.Controls.Add(this.lblIntervention);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(363, 535);
            this.Name = "frmDossierMail";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Dossier Mail";
            this.Load += new System.EventHandler(this.frmDossierMail_Load);
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Button cmdQuitter;
        public System.Windows.Forms.ListBox FileIntervention;
        public System.Windows.Forms.Label lblIntervention;
    }
}