﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Appel.Forms
{
    public partial class frmFournOuClient : Form
    {
        public frmFournOuClient()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            General.sFournOuClient = "";
            this.Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdClient_Click(object sender, EventArgs e)
        {
            General.sFournOuClient = General.cClient;
            this.Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFourn_Click(object sender, EventArgs e)
        {
            General.sFournOuClient = General.cFourn;
            this.Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmFournOuClient_Activated(object sender, EventArgs e)
        {
            General.sFournOuClient = "";
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmFournOuClient_Load(object sender, EventArgs e)
        {
            General.sFournOuClient = "";
        }
    }
}
