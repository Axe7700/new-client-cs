﻿namespace Axe_interDT.Views.Appel.Forms
{
    partial class frmHistoEcritures
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.optEcrituresNonLetrees = new System.Windows.Forms.RadioButton();
            this.optToutesEcritures = new System.Windows.Forms.RadioButton();
            this.txtSoldeCompte = new iTalk.iTalk_TextBox_Small2();
            this.label33 = new System.Windows.Forms.Label();
            this.txtTotalFacture = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.txtTotalRegle = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.SSGridSituation = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cmdClose = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.SSGridSituation)).BeginInit();
            this.SuspendLayout();
            // 
            // optEcrituresNonLetrees
            // 
            this.optEcrituresNonLetrees.AutoSize = true;
            this.optEcrituresNonLetrees.Checked = true;
            this.optEcrituresNonLetrees.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optEcrituresNonLetrees.Location = new System.Drawing.Point(160, 12);
            this.optEcrituresNonLetrees.Name = "optEcrituresNonLetrees";
            this.optEcrituresNonLetrees.Size = new System.Drawing.Size(179, 23);
            this.optEcrituresNonLetrees.TabIndex = 538;
            this.optEcrituresNonLetrees.TabStop = true;
            this.optEcrituresNonLetrees.Text = "Ecritures non lettrées";
            this.optEcrituresNonLetrees.UseVisualStyleBackColor = true;
            this.optEcrituresNonLetrees.CheckedChanged += new System.EventHandler(this.optEcrituresNonLetrees_CheckedChanged);
            // 
            // optToutesEcritures
            // 
            this.optToutesEcritures.AutoSize = true;
            this.optToutesEcritures.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.optToutesEcritures.Location = new System.Drawing.Point(418, 12);
            this.optToutesEcritures.Name = "optToutesEcritures";
            this.optToutesEcritures.Size = new System.Drawing.Size(164, 23);
            this.optToutesEcritures.TabIndex = 539;
            this.optToutesEcritures.Text = "Toutes les écritures";
            this.optToutesEcritures.UseVisualStyleBackColor = true;
            this.optToutesEcritures.CheckedChanged += new System.EventHandler(this.optToutesEcritures_CheckedChanged);
            // 
            // txtSoldeCompte
            // 
            this.txtSoldeCompte.AccAcceptNumbersOnly = false;
            this.txtSoldeCompte.AccAllowComma = false;
            this.txtSoldeCompte.AccBackgroundColor = System.Drawing.Color.White;
            this.txtSoldeCompte.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtSoldeCompte.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtSoldeCompte.AccHidenValue = "";
            this.txtSoldeCompte.AccNotAllowedChars = null;
            this.txtSoldeCompte.AccReadOnly = false;
            this.txtSoldeCompte.AccReadOnlyAllowDelete = false;
            this.txtSoldeCompte.AccRequired = false;
            this.txtSoldeCompte.BackColor = System.Drawing.Color.White;
            this.txtSoldeCompte.CustomBackColor = System.Drawing.Color.White;
            this.txtSoldeCompte.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtSoldeCompte.ForeColor = System.Drawing.Color.Black;
            this.txtSoldeCompte.Location = new System.Drawing.Point(129, 46);
            this.txtSoldeCompte.Margin = new System.Windows.Forms.Padding(2);
            this.txtSoldeCompte.MaxLength = 32767;
            this.txtSoldeCompte.Multiline = false;
            this.txtSoldeCompte.Name = "txtSoldeCompte";
            this.txtSoldeCompte.ReadOnly = false;
            this.txtSoldeCompte.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtSoldeCompte.Size = new System.Drawing.Size(135, 27);
            this.txtSoldeCompte.TabIndex = 0;
            this.txtSoldeCompte.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtSoldeCompte.UseSystemPasswordChar = false;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(0, 45);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(126, 19);
            this.label33.TabIndex = 540;
            this.label33.Text = "Solde du compte";
            // 
            // txtTotalFacture
            // 
            this.txtTotalFacture.AccAcceptNumbersOnly = false;
            this.txtTotalFacture.AccAllowComma = false;
            this.txtTotalFacture.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTotalFacture.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTotalFacture.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTotalFacture.AccHidenValue = "";
            this.txtTotalFacture.AccNotAllowedChars = null;
            this.txtTotalFacture.AccReadOnly = false;
            this.txtTotalFacture.AccReadOnlyAllowDelete = false;
            this.txtTotalFacture.AccRequired = false;
            this.txtTotalFacture.BackColor = System.Drawing.Color.White;
            this.txtTotalFacture.CustomBackColor = System.Drawing.Color.White;
            this.txtTotalFacture.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTotalFacture.ForeColor = System.Drawing.Color.Black;
            this.txtTotalFacture.Location = new System.Drawing.Point(418, 46);
            this.txtTotalFacture.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotalFacture.MaxLength = 32767;
            this.txtTotalFacture.Multiline = false;
            this.txtTotalFacture.Name = "txtTotalFacture";
            this.txtTotalFacture.ReadOnly = false;
            this.txtTotalFacture.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTotalFacture.Size = new System.Drawing.Size(135, 27);
            this.txtTotalFacture.TabIndex = 1;
            this.txtTotalFacture.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTotalFacture.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(313, 45);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(100, 19);
            this.label1.TabIndex = 542;
            this.label1.Text = "Total Facturé";
            // 
            // txtTotalRegle
            // 
            this.txtTotalRegle.AccAcceptNumbersOnly = false;
            this.txtTotalRegle.AccAllowComma = false;
            this.txtTotalRegle.AccBackgroundColor = System.Drawing.Color.White;
            this.txtTotalRegle.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtTotalRegle.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtTotalRegle.AccHidenValue = "";
            this.txtTotalRegle.AccNotAllowedChars = null;
            this.txtTotalRegle.AccReadOnly = false;
            this.txtTotalRegle.AccReadOnlyAllowDelete = false;
            this.txtTotalRegle.AccRequired = false;
            this.txtTotalRegle.BackColor = System.Drawing.Color.White;
            this.txtTotalRegle.CustomBackColor = System.Drawing.Color.White;
            this.txtTotalRegle.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtTotalRegle.ForeColor = System.Drawing.Color.Black;
            this.txtTotalRegle.Location = new System.Drawing.Point(699, 45);
            this.txtTotalRegle.Margin = new System.Windows.Forms.Padding(2);
            this.txtTotalRegle.MaxLength = 32767;
            this.txtTotalRegle.Multiline = false;
            this.txtTotalRegle.Name = "txtTotalRegle";
            this.txtTotalRegle.ReadOnly = false;
            this.txtTotalRegle.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtTotalRegle.Size = new System.Drawing.Size(135, 27);
            this.txtTotalRegle.TabIndex = 2;
            this.txtTotalRegle.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtTotalRegle.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(608, 45);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(86, 19);
            this.label2.TabIndex = 544;
            this.label2.Text = "Total Réglé";
            // 
            // SSGridSituation
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.SSGridSituation.DisplayLayout.Appearance = appearance1;
            this.SSGridSituation.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.SSGridSituation.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.SSGridSituation.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSGridSituation.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.SSGridSituation.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.SSGridSituation.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.SSGridSituation.DisplayLayout.MaxColScrollRegions = 1;
            this.SSGridSituation.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.SSGridSituation.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.SSGridSituation.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.SSGridSituation.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.SSGridSituation.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.SSGridSituation.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.SSGridSituation.DisplayLayout.Override.CellAppearance = appearance8;
            this.SSGridSituation.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.SSGridSituation.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.SSGridSituation.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.SSGridSituation.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.SSGridSituation.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.SSGridSituation.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.SSGridSituation.DisplayLayout.Override.RowAppearance = appearance11;
            this.SSGridSituation.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.SSGridSituation.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.SSGridSituation.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.SSGridSituation.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.SSGridSituation.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.SSGridSituation.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.SSGridSituation.Location = new System.Drawing.Point(15, 77);
            this.SSGridSituation.Name = "SSGridSituation";
            this.SSGridSituation.Size = new System.Drawing.Size(819, 554);
            this.SSGridSituation.TabIndex = 546;
            this.SSGridSituation.Text = "ultraGrid1";
            // 
            // cmdClose
            // 
            this.cmdClose.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdClose.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdClose.FlatAppearance.BorderSize = 0;
            this.cmdClose.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdClose.ForeColor = System.Drawing.Color.White;
            this.cmdClose.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.cmdClose.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdClose.Location = new System.Drawing.Point(749, 5);
            this.cmdClose.Margin = new System.Windows.Forms.Padding(2);
            this.cmdClose.Name = "cmdClose";
            this.cmdClose.Size = new System.Drawing.Size(85, 35);
            this.cmdClose.TabIndex = 567;
            this.cmdClose.Text = "   Fermer";
            this.cmdClose.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            this.cmdClose.UseVisualStyleBackColor = false;
            this.cmdClose.Click += new System.EventHandler(this.cmdClose_Click);
            // 
            // frmHistoEcritures
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(847, 636);
            this.Controls.Add(this.cmdClose);
            this.Controls.Add(this.SSGridSituation);
            this.Controls.Add(this.txtTotalRegle);
            this.Controls.Add(this.label2);
            this.Controls.Add(this.txtTotalFacture);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtSoldeCompte);
            this.Controls.Add(this.label33);
            this.Controls.Add(this.optToutesEcritures);
            this.Controls.Add(this.optEcrituresNonLetrees);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(863, 675);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(863, 675);
            this.Name = "frmHistoEcritures";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ecritures non lettrées";
            this.Load += new System.EventHandler(this.frmHistoEcritures_Load);
            ((System.ComponentModel.ISupportInitialize)(this.SSGridSituation)).EndInit();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.RadioButton optEcrituresNonLetrees;
        private System.Windows.Forms.RadioButton optToutesEcritures;
        public iTalk.iTalk_TextBox_Small2 txtSoldeCompte;
        private System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtTotalFacture;
        private System.Windows.Forms.Label label1;
        public iTalk.iTalk_TextBox_Small2 txtTotalRegle;
        private System.Windows.Forms.Label label2;
        private Infragistics.Win.UltraWinGrid.UltraGrid SSGridSituation;
        public System.Windows.Forms.Button cmdClose;
    }
}