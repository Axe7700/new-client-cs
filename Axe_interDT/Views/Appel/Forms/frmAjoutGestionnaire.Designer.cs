﻿namespace Axe_interDT.Views.Appel.Forms
{
    partial class frmAjoutGestionnaire
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance25 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance26 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance27 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance28 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance29 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance30 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance31 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance32 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance33 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance34 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance35 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance36 = new Infragistics.Win.Appearance();
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.ComboQualifGest = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCode1 = new iTalk.iTalk_TextBox_Small2();
            this.label4 = new System.Windows.Forms.Label();
            this.ssGridGestionnaire = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.txtGerant = new iTalk.iTalk_TextBox_Small2();
            this.label3 = new System.Windows.Forms.Label();
            this.CmdSauver = new System.Windows.Forms.Button();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.lblNameGestionnaire = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.ssDropGestionnaire = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtCodeImmeuble = new iTalk.iTalk_TextBox_Small2();
            this.label1 = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.groupBox6.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComboQualifGest)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridGestionnaire)).BeginInit();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropGestionnaire)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.ComboQualifGest);
            this.groupBox6.Controls.Add(this.txtCode1);
            this.groupBox6.Controls.Add(this.label4);
            this.groupBox6.Controls.Add(this.ssGridGestionnaire);
            this.groupBox6.Controls.Add(this.txtGerant);
            this.groupBox6.Controls.Add(this.label3);
            this.groupBox6.Controls.Add(this.CmdSauver);
            this.groupBox6.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.groupBox6.Location = new System.Drawing.Point(12, 12);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(813, 469);
            this.groupBox6.TabIndex = 411;
            this.groupBox6.TabStop = false;
            // 
            // ComboQualifGest
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ComboQualifGest.DisplayLayout.Appearance = appearance1;
            this.ComboQualifGest.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ComboQualifGest.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.ComboQualifGest.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ComboQualifGest.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.ComboQualifGest.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ComboQualifGest.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.ComboQualifGest.DisplayLayout.MaxColScrollRegions = 1;
            this.ComboQualifGest.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ComboQualifGest.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ComboQualifGest.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.ComboQualifGest.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ComboQualifGest.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.ComboQualifGest.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ComboQualifGest.DisplayLayout.Override.CellAppearance = appearance8;
            this.ComboQualifGest.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ComboQualifGest.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.ComboQualifGest.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.ComboQualifGest.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.ComboQualifGest.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ComboQualifGest.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.ComboQualifGest.DisplayLayout.Override.RowAppearance = appearance11;
            this.ComboQualifGest.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ComboQualifGest.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.ComboQualifGest.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ComboQualifGest.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ComboQualifGest.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ComboQualifGest.Location = new System.Drawing.Point(282, 222);
            this.ComboQualifGest.Name = "ComboQualifGest";
            this.ComboQualifGest.Size = new System.Drawing.Size(249, 25);
            this.ComboQualifGest.TabIndex = 507;
            this.ComboQualifGest.Visible = false;
            // 
            // txtCode1
            // 
            this.txtCode1.AccAcceptNumbersOnly = false;
            this.txtCode1.AccAllowComma = false;
            this.txtCode1.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCode1.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCode1.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCode1.AccHidenValue = "";
            this.txtCode1.AccNotAllowedChars = null;
            this.txtCode1.AccReadOnly = false;
            this.txtCode1.AccReadOnlyAllowDelete = false;
            this.txtCode1.AccRequired = false;
            this.txtCode1.BackColor = System.Drawing.Color.White;
            this.txtCode1.CustomBackColor = System.Drawing.Color.White;
            this.txtCode1.Font = new System.Drawing.Font("Tahoma", 10F);
            this.txtCode1.ForeColor = System.Drawing.Color.Black;
            this.txtCode1.Location = new System.Drawing.Point(31, 29);
            this.txtCode1.Margin = new System.Windows.Forms.Padding(2);
            this.txtCode1.MaxLength = 32767;
            this.txtCode1.Multiline = false;
            this.txtCode1.Name = "txtCode1";
            this.txtCode1.ReadOnly = false;
            this.txtCode1.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCode1.Size = new System.Drawing.Size(61, 27);
            this.txtCode1.TabIndex = 506;
            this.txtCode1.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCode1.UseSystemPasswordChar = false;
            this.txtCode1.Visible = false;
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label4.Location = new System.Drawing.Point(354, 58);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(114, 19);
            this.label4.TabIndex = 505;
            this.label4.Text = "Gestionnaire(s)";
            // 
            // ssGridGestionnaire
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssGridGestionnaire.DisplayLayout.Appearance = appearance13;
            this.ssGridGestionnaire.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssGridGestionnaire.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridGestionnaire.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridGestionnaire.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.ssGridGestionnaire.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssGridGestionnaire.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.ssGridGestionnaire.DisplayLayout.MaxColScrollRegions = 1;
            this.ssGridGestionnaire.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssGridGestionnaire.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssGridGestionnaire.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.ssGridGestionnaire.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.ssGridGestionnaire.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridGestionnaire.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridGestionnaire.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssGridGestionnaire.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.ssGridGestionnaire.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssGridGestionnaire.DisplayLayout.Override.CellAppearance = appearance20;
            this.ssGridGestionnaire.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssGridGestionnaire.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.ssGridGestionnaire.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.ssGridGestionnaire.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.ssGridGestionnaire.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssGridGestionnaire.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.ssGridGestionnaire.DisplayLayout.Override.RowAppearance = appearance23;
            this.ssGridGestionnaire.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.ssGridGestionnaire.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssGridGestionnaire.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.ssGridGestionnaire.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssGridGestionnaire.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssGridGestionnaire.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssGridGestionnaire.Location = new System.Drawing.Point(6, 78);
            this.ssGridGestionnaire.Name = "ssGridGestionnaire";
            this.ssGridGestionnaire.Size = new System.Drawing.Size(801, 385);
            this.ssGridGestionnaire.TabIndex = 504;
            this.ssGridGestionnaire.Text = "ultraGrid1";
            this.ssGridGestionnaire.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.ssGridGestionnaire_InitializeLayout);
            this.ssGridGestionnaire.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.ssGridGestionnaire_InitializeRow);
            this.ssGridGestionnaire.AfterRowsDeleted += new System.EventHandler(this.ssGridGestionnaire_AfterRowsDeleted);
            this.ssGridGestionnaire.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.ssGridGestionnaire_AfterRowUpdate);
            this.ssGridGestionnaire.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.ssGridGestionnaire_BeforeRowUpdate);
            this.ssGridGestionnaire.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.ssGridGestionnaire_BeforeExitEditMode);
            // 
            // txtGerant
            // 
            this.txtGerant.AccAcceptNumbersOnly = false;
            this.txtGerant.AccAllowComma = false;
            this.txtGerant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGerant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGerant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGerant.AccHidenValue = "";
            this.txtGerant.AccNotAllowedChars = null;
            this.txtGerant.AccReadOnly = false;
            this.txtGerant.AccReadOnlyAllowDelete = false;
            this.txtGerant.AccRequired = false;
            this.txtGerant.BackColor = System.Drawing.Color.White;
            this.txtGerant.CustomBackColor = System.Drawing.Color.White;
            this.txtGerant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtGerant.ForeColor = System.Drawing.Color.Black;
            this.txtGerant.Location = new System.Drawing.Point(232, 29);
            this.txtGerant.Margin = new System.Windows.Forms.Padding(2);
            this.txtGerant.MaxLength = 32767;
            this.txtGerant.Multiline = false;
            this.txtGerant.Name = "txtGerant";
            this.txtGerant.ReadOnly = false;
            this.txtGerant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGerant.Size = new System.Drawing.Size(380, 27);
            this.txtGerant.TabIndex = 503;
            this.txtGerant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGerant.UseSystemPasswordChar = false;
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(170, 32);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(57, 19);
            this.label3.TabIndex = 502;
            this.label3.Text = "Gerant";
            // 
            // CmdSauver
            // 
            this.CmdSauver.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdSauver.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdSauver.FlatAppearance.BorderSize = 0;
            this.CmdSauver.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdSauver.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdSauver.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.CmdSauver.Location = new System.Drawing.Point(740, 20);
            this.CmdSauver.Margin = new System.Windows.Forms.Padding(2);
            this.CmdSauver.Name = "CmdSauver";
            this.CmdSauver.Size = new System.Drawing.Size(60, 35);
            this.CmdSauver.TabIndex = 360;
            this.CmdSauver.UseVisualStyleBackColor = false;
            this.CmdSauver.Click += new System.EventHandler(this.CmdSauver_Click);
            // 
            // groupBox1
            // 
            this.groupBox1.BackColor = System.Drawing.Color.Transparent;
            this.groupBox1.Controls.Add(this.lblNameGestionnaire);
            this.groupBox1.Controls.Add(this.label2);
            this.groupBox1.Controls.Add(this.ssDropGestionnaire);
            this.groupBox1.Controls.Add(this.txtCodeImmeuble);
            this.groupBox1.Controls.Add(this.label1);
            this.groupBox1.Controls.Add(this.label33);
            this.groupBox1.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.groupBox1.Location = new System.Drawing.Point(12, 487);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(813, 116);
            this.groupBox1.TabIndex = 412;
            this.groupBox1.TabStop = false;
            // 
            // lblNameGestionnaire
            // 
            this.lblNameGestionnaire.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblNameGestionnaire.Location = new System.Drawing.Point(459, 76);
            this.lblNameGestionnaire.Name = "lblNameGestionnaire";
            this.lblNameGestionnaire.Size = new System.Drawing.Size(265, 17);
            this.lblNameGestionnaire.TabIndex = 507;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(81, 86);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(97, 19);
            this.label2.TabIndex = 506;
            this.label2.Text = "Gestionnaire";
            // 
            // ssDropGestionnaire
            // 
            appearance25.BackColor = System.Drawing.SystemColors.Window;
            appearance25.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.ssDropGestionnaire.DisplayLayout.Appearance = appearance25;
            this.ssDropGestionnaire.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.ssDropGestionnaire.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance26.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance26.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance26.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance26.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropGestionnaire.DisplayLayout.GroupByBox.Appearance = appearance26;
            appearance27.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropGestionnaire.DisplayLayout.GroupByBox.BandLabelAppearance = appearance27;
            this.ssDropGestionnaire.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance28.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance28.BackColor2 = System.Drawing.SystemColors.Control;
            appearance28.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance28.ForeColor = System.Drawing.SystemColors.GrayText;
            this.ssDropGestionnaire.DisplayLayout.GroupByBox.PromptAppearance = appearance28;
            this.ssDropGestionnaire.DisplayLayout.MaxColScrollRegions = 1;
            this.ssDropGestionnaire.DisplayLayout.MaxRowScrollRegions = 1;
            appearance29.BackColor = System.Drawing.SystemColors.Window;
            appearance29.ForeColor = System.Drawing.SystemColors.ControlText;
            this.ssDropGestionnaire.DisplayLayout.Override.ActiveCellAppearance = appearance29;
            appearance30.BackColor = System.Drawing.SystemColors.Highlight;
            appearance30.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.ssDropGestionnaire.DisplayLayout.Override.ActiveRowAppearance = appearance30;
            this.ssDropGestionnaire.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.ssDropGestionnaire.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance31.BackColor = System.Drawing.SystemColors.Window;
            this.ssDropGestionnaire.DisplayLayout.Override.CardAreaAppearance = appearance31;
            appearance32.BorderColor = System.Drawing.Color.Silver;
            appearance32.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.ssDropGestionnaire.DisplayLayout.Override.CellAppearance = appearance32;
            this.ssDropGestionnaire.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.ssDropGestionnaire.DisplayLayout.Override.CellPadding = 0;
            appearance33.BackColor = System.Drawing.SystemColors.Control;
            appearance33.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance33.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance33.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance33.BorderColor = System.Drawing.SystemColors.Window;
            this.ssDropGestionnaire.DisplayLayout.Override.GroupByRowAppearance = appearance33;
            appearance34.TextHAlignAsString = "Left";
            this.ssDropGestionnaire.DisplayLayout.Override.HeaderAppearance = appearance34;
            this.ssDropGestionnaire.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.ssDropGestionnaire.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance35.BackColor = System.Drawing.SystemColors.Window;
            appearance35.BorderColor = System.Drawing.Color.Silver;
            this.ssDropGestionnaire.DisplayLayout.Override.RowAppearance = appearance35;
            this.ssDropGestionnaire.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance36.BackColor = System.Drawing.SystemColors.ControlLight;
            this.ssDropGestionnaire.DisplayLayout.Override.TemplateAddRowAppearance = appearance36;
            this.ssDropGestionnaire.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.ssDropGestionnaire.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.ssDropGestionnaire.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.ssDropGestionnaire.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ssDropGestionnaire.Location = new System.Drawing.Point(204, 83);
            this.ssDropGestionnaire.Name = "ssDropGestionnaire";
            this.ssDropGestionnaire.Size = new System.Drawing.Size(249, 27);
            this.ssDropGestionnaire.TabIndex = 505;
            this.ssDropGestionnaire.AfterCloseUp += new System.EventHandler(this.ssDropGestionnaire_AfterCloseUp);
            this.ssDropGestionnaire.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.ssDropGestionnaire_BeforeDropDown);
            this.ssDropGestionnaire.ValueChanged += new System.EventHandler(this.ssDropGestionnaire_ValueChanged);
            // 
            // txtCodeImmeuble
            // 
            this.txtCodeImmeuble.AccAcceptNumbersOnly = false;
            this.txtCodeImmeuble.AccAllowComma = false;
            this.txtCodeImmeuble.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeImmeuble.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeImmeuble.AccHidenValue = "";
            this.txtCodeImmeuble.AccNotAllowedChars = null;
            this.txtCodeImmeuble.AccReadOnly = false;
            this.txtCodeImmeuble.AccReadOnlyAllowDelete = false;
            this.txtCodeImmeuble.AccRequired = false;
            this.txtCodeImmeuble.BackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeImmeuble.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeImmeuble.ForeColor = System.Drawing.Color.Black;
            this.txtCodeImmeuble.Location = new System.Drawing.Point(204, 46);
            this.txtCodeImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeImmeuble.MaxLength = 32767;
            this.txtCodeImmeuble.Multiline = false;
            this.txtCodeImmeuble.Name = "txtCodeImmeuble";
            this.txtCodeImmeuble.ReadOnly = false;
            this.txtCodeImmeuble.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeImmeuble.Size = new System.Drawing.Size(520, 27);
            this.txtCodeImmeuble.TabIndex = 504;
            this.txtCodeImmeuble.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeImmeuble.UseSystemPasswordChar = false;
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(81, 46);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(118, 19);
            this.label1.TabIndex = 503;
            this.label1.Text = "Code Immeuble";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(300, 18);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(272, 19);
            this.label33.TabIndex = 502;
            this.label33.Text = "Attacher un gestionnaire à l\'immeuble";
            // 
            // frmAjoutGestionnaire
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(837, 615);
            this.Controls.Add(this.groupBox1);
            this.Controls.Add(this.groupBox6);
            this.MaximizeBox = false;
            this.MaximumSize = new System.Drawing.Size(853, 654);
            this.MinimumSize = new System.Drawing.Size(853, 654);
            this.Name = "frmAjoutGestionnaire";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Ajouter Un Gestionnaire";
            this.Activated += new System.EventHandler(this.frmAjoutGestionnaire_Activated);
            this.Load += new System.EventHandler(this.frmAjoutGestionnaire_Load);
            this.groupBox6.ResumeLayout(false);
            this.groupBox6.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ComboQualifGest)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.ssGridGestionnaire)).EndInit();
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ssDropGestionnaire)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label label33;
        public iTalk.iTalk_TextBox_Small2 txtCodeImmeuble;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label label2;
        public Infragistics.Win.UltraWinGrid.UltraCombo ssDropGestionnaire;
        public System.Windows.Forms.Button CmdSauver;
        public iTalk.iTalk_TextBox_Small2 txtGerant;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.Label label4;
        private Infragistics.Win.UltraWinGrid.UltraGrid ssGridGestionnaire;
        public iTalk.iTalk_TextBox_Small2 txtCode1;
        private System.Windows.Forms.Label lblNameGestionnaire;
        public Infragistics.Win.UltraWinGrid.UltraCombo ComboQualifGest;
    }
}