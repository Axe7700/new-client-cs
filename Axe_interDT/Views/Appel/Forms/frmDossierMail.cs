﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Appel.Forms
{
    public partial class frmDossierMail : Form
    {
        public frmDossierMail()
        {
            InitializeComponent();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdQuitter_Click(object sender, EventArgs e)
        {
            Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void FileIntervention_DoubleClick(object sender, EventArgs e)
        {
            if (FileIntervention.SelectedIndex != -1)
            {
                ModuleAPI.Ouvrir(FileIntervention.Tag + "\\" + FileIntervention.Items[FileIntervention.SelectedIndex]);
            }
        }
        public void RefreshListBox(ListBox FileIntervention)
        {
            foreach (string File in Directory.GetFiles(FileIntervention.Tag.ToString()))
            {
                FileIntervention.Items.Add(Path.GetFileName(File));
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmDossierMail_Load(object sender, EventArgs e)
        {
            RefreshListBox(FileIntervention);
        }
    }
}
