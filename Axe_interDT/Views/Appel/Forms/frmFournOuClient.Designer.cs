﻿namespace Axe_interDT.Views.Appel.Forms
{
    partial class frmFournOuClient
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.Label1 = new System.Windows.Forms.Label();
            this.cmdAnnuler = new System.Windows.Forms.Button();
            this.cmdClient = new System.Windows.Forms.Button();
            this.cmdFourn = new System.Windows.Forms.Button();
            this.SuspendLayout();
            // 
            // Label1
            // 
            this.Label1.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Label1.Location = new System.Drawing.Point(12, 9);
            this.Label1.Name = "Label1";
            this.Label1.Size = new System.Drawing.Size(421, 42);
            this.Label1.TabIndex = 384;
            this.Label1.Text = "Veuillez cliquer sur le bouton fournisseur si il s\'agit d\'un document provenant d" +
    "\'un fournisseur sinon cliquez sur le bouton Client. ";
            // 
            // cmdAnnuler
            // 
            this.cmdAnnuler.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAnnuler.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAnnuler.FlatAppearance.BorderSize = 0;
            this.cmdAnnuler.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAnnuler.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdAnnuler.ForeColor = System.Drawing.Color.White;
            this.cmdAnnuler.Image = global::Axe_interDT.Properties.Resources.Cancel_16x16;
            this.cmdAnnuler.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAnnuler.Location = new System.Drawing.Point(282, 53);
            this.cmdAnnuler.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAnnuler.Name = "cmdAnnuler";
            this.cmdAnnuler.Size = new System.Drawing.Size(117, 35);
            this.cmdAnnuler.TabIndex = 567;
            this.cmdAnnuler.Text = "    Annuler";
            this.cmdAnnuler.UseVisualStyleBackColor = false;
            this.cmdAnnuler.Click += new System.EventHandler(this.cmdAnnuler_Click);
            // 
            // cmdClient
            // 
            this.cmdClient.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdClient.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdClient.FlatAppearance.BorderSize = 0;
            this.cmdClient.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClient.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdClient.ForeColor = System.Drawing.Color.White;
            this.cmdClient.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdClient.Location = new System.Drawing.Point(161, 53);
            this.cmdClient.Margin = new System.Windows.Forms.Padding(2);
            this.cmdClient.Name = "cmdClient";
            this.cmdClient.Size = new System.Drawing.Size(117, 35);
            this.cmdClient.TabIndex = 568;
            this.cmdClient.Text = "Client";
            this.cmdClient.UseVisualStyleBackColor = false;
            this.cmdClient.Click += new System.EventHandler(this.cmdClient_Click);
            // 
            // cmdFourn
            // 
            this.cmdFourn.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdFourn.FlatAppearance.BorderColor = System.Drawing.Color.Silver;
            this.cmdFourn.FlatAppearance.BorderSize = 0;
            this.cmdFourn.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFourn.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdFourn.ForeColor = System.Drawing.Color.White;
            this.cmdFourn.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdFourn.Location = new System.Drawing.Point(40, 53);
            this.cmdFourn.Margin = new System.Windows.Forms.Padding(2);
            this.cmdFourn.Name = "cmdFourn";
            this.cmdFourn.Size = new System.Drawing.Size(117, 35);
            this.cmdFourn.TabIndex = 569;
            this.cmdFourn.Text = "Fournisseur";
            this.cmdFourn.UseVisualStyleBackColor = false;
            this.cmdFourn.Click += new System.EventHandler(this.cmdFourn_Click);
            // 
            // frmFournOuClient
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(445, 96);
            this.Controls.Add(this.cmdFourn);
            this.Controls.Add(this.cmdClient);
            this.Controls.Add(this.cmdAnnuler);
            this.Controls.Add(this.Label1);
            this.MaximumSize = new System.Drawing.Size(461, 135);
            this.MinimizeBox = false;
            this.MinimumSize = new System.Drawing.Size(461, 135);
            this.Name = "frmFournOuClient";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Classement";
            this.Activated += new System.EventHandler(this.frmFournOuClient_Activated);
            this.Load += new System.EventHandler(this.frmFournOuClient_Load);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label Label1;
        public System.Windows.Forms.Button cmdAnnuler;
        public System.Windows.Forms.Button cmdClient;
        public System.Windows.Forms.Button cmdFourn;
    }
}