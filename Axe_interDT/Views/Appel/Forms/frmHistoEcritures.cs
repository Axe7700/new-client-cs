﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Appel.Forms
{
    public partial class frmHistoEcritures : Form
    {
        public frmHistoEcritures()
        {
            InitializeComponent();
        }
        private void cmdClose_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            this.Visible = false;
            Close();
        }

        private void frmHistoEcritures_Load(System.Object eventSender, System.EventArgs eventArgs)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_ChargeFinance();

        }
        private object fc_ChargeFinance()
        {
            object functionReturnValue = null;
            DataTable rsAdoFinance = default(DataTable);
            ModAdo modAdorsAdoFinance = new ModAdo();
            double MontantCredit = 0;
            double MontantDebit = 0;

            string DateMini = null;
            string DateMaxi = null;
            int JourMaxiMois = 0;

            try
            {
                JourMaxiMois = General.CalculMaxiJourMois(Convert.ToInt32(General.MoisFinExercice));
                if (General.MoisFinExercice == "12" | !General.IsNumeric(General.MoisFinExercice))
                {
                    DateMini = "01/" + "01" + "/" + Convert.ToString(DateTime.Today.Year);
                    DateMaxi = JourMaxiMois + "/" + General.MoisFinExercice + "/" + Convert.ToString(DateTime.Today.Year);
                }
                else
                {
                    if (DateTime.Today.Month >= 1 && DateTime.Today.Month <= Convert.ToInt16(General.MoisFinExercice))
                    {
                        DateMini = "01/" + Convert.ToInt32(Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year - 1);
                        //MoisFinExercice + 1
                        DateMaxi = JourMaxiMois + "/" + Convert.ToInt32(General.MoisFinExercice).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year);
                        //MoisFinExercice
                    }
                    else
                    {
                        DateMini = "01/" + Convert.ToInt32(Convert.ToInt16(General.MoisFinExercice) + 1).ToString("00") + "/" + Convert.ToString(DateTime.Today.Year);
                        //MoisFinExercice - 1
                        DateMaxi = JourMaxiMois + "/" + DateTime.Today.Month.ToString("00") + "/" + Convert.ToString(DateTime.Today.Year);
                        //MoisFinExercice
                    }
                }


                var SSGridSituationSource = new DataTable();
                SSGridSituationSource.Columns.Add("PIECE");
                SSGridSituationSource.Columns.Add("SENS");
                SSGridSituationSource.Columns.Add("MONTANT");
                SSGridSituationSource.Columns.Add("DATE");
                SSGridSituationSource.Columns.Add("ECHEANCE");
                SSGridSituationSource.Columns.Add("Num");
                SSGridSituationSource.Columns.Add("JM_Date");


                SAGE.fc_OpenConnSage();
                if (optEcrituresNonLetrees.Checked == true)
                {

                    //        sSQl = ""
                    //        sSQl = "SELECT EC_SENS,EC_PIECE,EC_MONTANT,EC_DATE,EC_ECHEANCE FROM F_ECRITUREC WHERE "
                    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                    //        sSQl = sSQl & " AND ((F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                    General.sSQL = "";
                    General.sSQL = "SELECT EC_PIECE,EC_SENS,EC_MONTANT,EC_DATE,EC_ECHEANCE,JO_Num,JM_Date FROM F_ECRITUREC ";
                    General.sSQL = General.sSQL + "WHERE ";
                    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    General.sSQL = General.sSQL + "CT_NUM='" + General.CompteEcriture + "'";
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%') or (F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'))";

                }
                else
                {
                    //        sSQl = ""
                    //        sSQl = "SELECT EC_SENS,EC_PIECE,EC_MONTANT,EC_DATE,EC_ECHEANCE FROM F_ECRITUREC WHERE "
                    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                    //        sSQl = sSQl & " AND ((F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                    General.sSQL = "";
                    General.sSQL = "SELECT EC_SENS,EC_PIECE,EC_MONTANT,EC_DATE,EC_ECHEANCE,JO_Num,JM_Date  FROM F_ECRITUREC WHERE ";
                    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    General.sSQL = General.sSQL + "CT_NUM='" + General.CompteEcriture + "'";
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.CG_NUM  Like '411%' ) or (F_ECRITUREC.CG_NUM  Like '411%'))";
                }
                rsAdoFinance = modAdorsAdoFinance.fc_OpenRecordSet(General.sSQL, null, "", SAGE.adoSage);
                SSGridSituation.DataSource = null;
                if (rsAdoFinance.Rows.Count > 0)
                {
                    foreach (DataRow rsAdoFinanceRow in rsAdoFinance.Rows)
                    {
                        //Colonne du montant dans les crédits
                        if (rsAdoFinanceRow["EC_Sens"].ToString() == "1")
                        {
                            SSGridSituationSource.Rows.Add(rsAdoFinanceRow["JO_Num"], rsAdoFinanceRow["JM_Date"], rsAdoFinanceRow["EC_Date"], rsAdoFinanceRow["EC_Piece"], rsAdoFinanceRow["EC_Echeance"], "", rsAdoFinanceRow["EC_Montant"]);
                        }
                        else
                        {
                            SSGridSituationSource.Rows.Add(rsAdoFinanceRow["JO_Num"], rsAdoFinanceRow["JM_Date"], rsAdoFinanceRow["EC_Date"], rsAdoFinanceRow["EC_Piece"], rsAdoFinanceRow["EC_Echeance"], rsAdoFinanceRow["EC_Montant"], "");
                        }
                    }
                }
                SSGridSituation.DataSource = SSGridSituationSource;
                modAdorsAdoFinance?.Dispose();
                if (optEcrituresNonLetrees.Checked == true)
                {
                    //        sSQl = ""
                    //        sSQl = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE "
                    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                    //        sSQl = sSQl & " AND ((F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                    General.sSQL = "";
                    General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    General.sSQL = General.sSQL + "CT_NUM='" + General.CompteEcriture + "'";
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%' ) or (F_ECRITUREC.EC_SENS=1 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'))";

                }
                else
                {
                    //        sSQl = ""
                    //        sSQl = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE "
                    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                    //        sSQl = sSQl & " AND ((F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                    General.sSQL = "";
                    General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    General.sSQL = General.sSQL + "CT_NUM='" + General.CompteEcriture + "'";
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%' ) or (F_ECRITUREC.EC_SENS=1 and F_ECRITUREC.CG_NUM  Like '411%'))";

                }
                modAdorsAdoFinance = new ModAdo();
                rsAdoFinance = modAdorsAdoFinance.fc_OpenRecordSet(General.sSQL, null, "", SAGE.adoSage);
                if (rsAdoFinance.Rows.Count > 0)
                {
                    MontantCredit = Convert.ToDouble(General.nz(rsAdoFinance.Rows[0]["Montant_Total"], 0));
                }
                modAdorsAdoFinance?.Dispose();

                if (optEcrituresNonLetrees.Checked == true)
                {
                    //        sSQl = ""
                    //        sSQl = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE "
                    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                    //        sSQl = sSQl & " AND ((F_ECRITUREC.JO_NUM<>'AN' F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.JO_NUM<>'AN' F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                    General.sSQL = "";
                    General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    General.sSQL = General.sSQL + "CT_NUM='" + General.CompteEcriture + "'";
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%') or (F_ECRITUREC.EC_SENS=0 AND F_ECRITUREC.EC_LETTRE = 0 and F_ECRITUREC.CG_NUM  Like '411%'))";
                }
                else
                {
                    //        sSQl = ""
                    //        sSQl = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE "
                    //        sSQl = sSQl & "CT_NUM='" & fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" & txtCodeImmeuble.Text & "'") & "'"
                    //        sSQl = sSQl & " AND ((F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0 ) or (F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'  AND F_ECRITUREC.EC_NOLINK = 0))"

                    General.sSQL = "";
                    General.sSQL = "SELECT SUM(EC_MONTANT) AS [Montant_Total] FROM F_ECRITUREC WHERE ";
                    General.sSQL = General.sSQL + "JM_Date>='" + DateMini + "' AND JM_Date<='" + DateMaxi + "' AND ";
                    General.sSQL = General.sSQL + "CT_NUM='" + General.CompteEcriture + "'";
                    General.sSQL = General.sSQL + " AND ((F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%') or (F_ECRITUREC.EC_SENS=0 and F_ECRITUREC.CG_NUM  Like '411%'))";

                }
                modAdorsAdoFinance = new ModAdo();
                rsAdoFinance = modAdorsAdoFinance.fc_OpenRecordSet(General.sSQL, null, "", SAGE.adoSage);
                if (rsAdoFinance.Rows.Count > 0)
                {
                    MontantDebit = Convert.ToDouble(General.nz(rsAdoFinance.Rows[0]["Montant_Total"], 0));
                }
                modAdorsAdoFinance?.Dispose();

                txtSoldeCompte.Text = Convert.ToString(MontantDebit - MontantCredit);
                txtTotalFacture.Text = Convert.ToString(MontantDebit);
                txtTotalRegle.Text = Convert.ToString(MontantCredit);


                SAGE.fc_CloseConnSage();
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "frmHistoEcritures ; fc_ChargeFinance");
                return functionReturnValue;
            }
        }


        private void optEcrituresNonLetrees_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optEcrituresNonLetrees.Checked)
            {
                fc_ChargeFinance();
                this.Text = "Ecritures non lettrées";
            }
        }

        private void optToutesEcritures_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optToutesEcritures.Checked)
            {
                fc_ChargeFinance();
                this.Text = "Toutes les écritures";
            }
        }
    }
}
