﻿using Axe_interDT.Shared;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace Axe_interDT.Views.Appel.Forms
{
    public partial class frmMotifs : Form
    {
        ModAdo modAdoDataTable = null;
        public frmMotifs()
        {
            InitializeComponent();
        }

        public DataTable rsMotifs;
        ModAdo modAdorsMotifs = null;
        string sqlMotifs;
        int NoMotifs;

        /// <summary>
        /// tested
        /// </summary>
        private void loadMotifs()
        {

            sqlMotifs = "Select FacArticle.CodeArticle, FacArticle.Designation1, CodeTypeArticle " 
                + "From FacArticle " 
                + "WHERE ((left(FacArticle.CodeArticle,1)='R') AND ((FacArticle.CodeTypeArticle)=0))" 
                + "ORDER BY Designation1 ASC";

            modAdorsMotifs = new ModAdo();
            rsMotifs = modAdorsMotifs.fc_OpenRecordSet(sqlMotifs);
            SSOleDBGrid1.DataSource = rsMotifs;

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdSauver_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            SSOleDBGrid1.UpdateData();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void frmMotifs_Load(System.Object eventSender, System.EventArgs eventArgs)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            loadMotifs();
        }

      /// <summary>
      /// tested
      /// </summary>
      /// <returns></returns>
        public string SelectMaxCodeArticle()
        {
            DataTable rsNoMotif = default(DataTable);
           
            int lngNoMotif = 0;
            modAdoDataTable = new ModAdo();
            rsNoMotif = modAdoDataTable.fc_OpenRecordSet("SELECT NumMotif.NoMotif, NumMotif.Autorise FROM NumMotif");
            if (rsNoMotif.Rows.Count > 0)
            {
                rsNoMotif.Rows[rsNoMotif.Rows.Count - 1]["Autorise"] = true;
                lngNoMotif = Convert.ToInt32(General.nz(rsNoMotif.Rows[rsNoMotif.Rows.Count - 1]["NoMotif"], "0"));
                lngNoMotif = lngNoMotif + 1;
                modAdoDataTable.Update();
                rsNoMotif.Rows.Add(rsNoMotif.NewRow());
                rsNoMotif.Rows[rsNoMotif.Rows.Count - 1]["NoMotif"] = lngNoMotif;
                modAdoDataTable.Update();
            }

            modAdoDataTable?.Dispose();

            return "R" + Convert.ToString(lngNoMotif);

        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            using (var tmpModAdo = new ModAdo())
                if (!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT CAI_CODE FROM GestionStandard WHERE CAI_Code='" + e.Rows[0].Cells["CodeArticle"].Text+ "'"), "").ToString()))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Un ou plusieurs appels font références au motif que vous souhaitez suprimer." + "\n" + "La suppression est impossible", "Annulation de la suppression d'un motif", MessageBoxButtons.OK);
                    e.Cancel = true;
                }
                else
                {
                    if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression du motif sélectionné.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                    {
                        e.Cancel = true;
                    }
                    else
                    {
                        e.Cancel = false;
                    }
                }
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_DoubleClickRow(object sender, Infragistics.Win.UltraWinGrid.DoubleClickRowEventArgs e)

        {
            SSOleDBGrid1.UpdateData();
            txtCodeMotif.Text = SSOleDBGrid1.ActiveRow.Cells["CodeArticle"].Value.ToString();
            this.Visible = false;
            Close();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_KeyPress(object sender, KeyPressEventArgs e)
        {
            if ((int)e.KeyChar == 13)
            {
                SSOleDBGrid1.UpdateData();
                txtCodeMotif.Text = SSOleDBGrid1.ActiveRow.Cells["CodeArticle"].Value.ToString();
                this.Visible = false;
                Close();
            }
        }
        
        private void SSOleDBGrid1_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
                        
            modAdorsMotifs.Update();
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_AfterRowsDeleted(object sender, EventArgs e)
        {
            modAdorsMotifs.Update();
        }

        private void SSOleDBGrid1_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["CodeArticle"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["CodeTypeArticle"].Hidden = true;
            SSOleDBGrid1.DisplayLayout.Bands[0].Columns["Designation1"].Header.Caption = "Libelle du Motif";
        }
        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void SSOleDBGrid1_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["CodeArticle"].Value.ToString()) &&
               string.IsNullOrEmpty(e.Row.Cells["CodeTypeArticle"].Value.ToString())){

                e.Row.Cells["Designation1"].Value = SSOleDBGrid1.ActiveRow.Cells["Designation1"].Text;
                e.Row.Cells["CodeArticle"].Value = SelectMaxCodeArticle();
                e.Row.Cells["CodeTypeArticle"].Value = 0;         
            }
        }
    }
}
