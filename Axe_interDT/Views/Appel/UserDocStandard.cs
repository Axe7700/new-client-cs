﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Appel.Forms;
using Axe_interDT.Views.Appel.Mail;
using Axe_interDT.Views.BaseDeDonnees.Gerant;
using Axe_interDT.Views.BaseDeDonnees.Immeuble;
using Axe_interDT.Views.Contrat;
using Axe_interDT.Views.Contrat.FicheGMAO;
using Axe_interDT.Views.Devis;
using Axe_interDT.Views.Devis.Forms;
using Axe_interDT.Views.Intervention;
using Axe_interDT.Views.Theme.CustomMessageBox;
using Infragistics.Win.UltraWinEditors;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.IO;
using System.Windows.Forms;

namespace Axe_interDT.Views.Appel
{
    public partial class UserDocStandard : UserControl
    {
        bool bCheminOS;
        const short cChauffage = 21;
        const short cDepannage = 22;
        const short cFuite = 23;
        const short cECS = 24;
        const short cRadiateur = 25;
        const string cLibelleFax = "Fax en attente";
        const string cLibelleMail = "Mail en attente";
        const string cLibelleCourier = "Courrier en attente";
        bool blModif;
        bool blnAjout;
        bool blnFichierExist;
        string sUtilisateur;
        bool charge;
        bool blnContrat;
        bool showAlerteSuccess = false;
        private struct OSVERSIONINFO
        {
            public int dwOSVersionInfoSize;
            public int dwMajorVersion;
            public int dwMinorVersion;
            public int dwBuildNumber;
            public int dwPlatformId;
            //  Maintenance string for PSS usage
            public char[] szCSDVersion;
        }

        Color cBackColorInter = Color.FromArgb(192, 255, 192);

        bool bload;
        public UserDocStandard()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChkDevisAcc_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if ((int)ChkDevisAcc.CheckState == 1)
                {

                    //    If charge = False Then
                    //            If txtNoDevis.Locked = True Then
                    //                MsgBox "Opération annulée, vous ne pouvez pas attacher un devis sur une" _
                    //'                & " fiche d'appel liée à une intervention.", vbInformation, "Opération annulée"
                    //                ChkDevisAcc.value = 0
                    //                Exit Sub
                    //            End If
                    //    End If

                    blnFichierExist = false;
                    ChkDmdeDevis.CheckState = System.Windows.Forms.CheckState.Unchecked;
                    if (blnFichierExist == true)
                    {
                        ChkDevisAcc.CheckState = System.Windows.Forms.CheckState.Unchecked;
                        return;
                    }
                    txtNoDevis.Enabled = true;
                    txtNoDevis.CustomBackColor = System.Drawing.ColorTranslator.FromOle(-2147483643);
                    txtDateDevis.Enabled = true;
                    txtDateDevis.CustomBackColor = System.Drawing.ColorTranslator.FromOle(-2147483643);
                    cmdRechNoDev.Enabled = true;
                    if (charge == false)
                    {
                        txtDateDevis.Text = DateTime.Today.ToString("dd/MM/yyyy");
                    }

                }
                else if ((int)ChkDevisAcc.CheckState == 0)
                {
                    //    If charge = False Then
                    //            If txtNoDevis.Locked = True Then
                    //                MsgBox "Opération annulée, ce devis est lié à une intervention.", vbInformation, "Opération annulée"
                    //                ChkDevisAcc.value = 1
                    //                Exit Sub
                    //            End If
                    //    End If

                    txtNoDevis.Enabled = false;
                    txtNoDevis.CustomBackColor = System.Drawing.ColorTranslator.FromOle(14737632);
                    txtDateDevis.Enabled = false;
                    txtDateDevis.CustomBackColor = System.Drawing.ColorTranslator.FromOle(14737632);
                    cmdRechNoDev.Enabled = false;
                    txtNoDevis.Text = "";
                    txtDateDevis.Text = "";

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ChkDevisAcc_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ChkDmdeDevis_CheckedChanged(object sender, EventArgs e)
        {
            try
            {
                if ((int)ChkDmdeDevis.CheckState == 1)
                {
                    ChkDevisAcc.CheckState = System.Windows.Forms.CheckState.Unchecked;
                    txtTexte.AccReadOnly = false;
                    txtTexte.CustomBackColor = System.Drawing.ColorTranslator.FromOle(-2147483643);
                    txtQuiDevis.AccReadOnly = false;
                    txtQuiDevis.CustomBackColor = System.Drawing.ColorTranslator.FromOle(-2147483643);
                    cmdQuiDevis.Enabled = true;
                    //ssoleDbQuiDevis.Enabled = True
                }
                else if ((int)ChkDmdeDevis.CheckState == 0)
                {
                    if ((txtCodeOriCommentDevis.Text == "1" || txtCodeOriCommentDevis.Text == "4"))
                    {
                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention vous aller annuler une demande de devis" + "\n" + " alors qu'un document a déjà été transféré dans le" + " répertoire" +
                            txtCodeImmeuble.Text + "\n" + "Vouler vous continuer?", "Attention", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            ChkDmdeDevis.CheckState = System.Windows.Forms.CheckState.Checked;
                            blnFichierExist = true;
                            return;
                        }
                    }
                    //ssoleDbQuiDevis.Enabled = False
                    txtTexte.AccReadOnly = true;
                    txtTexte.CustomBackColor = System.Drawing.ColorTranslator.FromOle(14737632);
                    txtTexte.Text = "";
                    txtQuiDevis.AccReadOnly = true;
                    txtQuiDevis.CustomBackColor = System.Drawing.ColorTranslator.FromOle(14737632);
                    cmdQuiDevis.Visible = true;
                    cmdVisuDevis.Visible = false;
                    cmdQuiDevis.Enabled = false;
                    txtQuiDevis.Text = "";
                    lblComDevis.Text = "";
                    txtDocumentDevis.Text = "";
                    txtCodeOriCommentDevis.Text = "";
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ChkDmdeDevis_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCAI_CategorInterv_AfterCloseUp(object sender, EventArgs e)
        {
            try
            {
                cmbCAI_CategorInterv_Validating(cmbCAI_CategorInterv, new System.ComponentModel.CancelEventArgs(false));
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmbCAI_CategorInterv_CloseUp");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmbCAI_CategorInterv_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            try
            {
                if (!string.IsNullOrEmpty(cmbCAI_CategorInterv.Text))
                {
                    General.sSQL = "";
                    //            sSQl = "SELECT CAI_Libelle" _
                    //'            & " FROM CAI_CategoriInterv" _
                    //'            & " where CAI_Code ='" & cmbCAI_CategorInterv & "'"
                    General.sSQL = "Select FacArticle.Designation1 " + "From FacArticle " + "WHERE ((left(FacArticle.CodeArticle,1)='R') AND ((FacArticle.CodeTypeArticle)=0))" +
                        " AND FacArticle.CodeArticle='" + StdSQLchaine.gFr_DoublerQuote(cmbCAI_CategorInterv.Text) + "'" + "ORDER BY Designation1 ASC";
                    using (var tmpModAdo = new ModAdo())
                        lbllibCAI_CategorInterv.Text = tmpModAdo.fc_ADOlibelle(General.sSQL);
                    if (string.IsNullOrEmpty(lbllibCAI_CategorInterv.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce type de motif n'éxiste pas.");
                        eventArgs.Cancel = true;
                    }
                }
                else
                {
                    lbllibCAI_CategorInterv.Text = "";
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmbCAI_CategorInterv_Validate");
                eventArgs.Cancel = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbCAI_CategorInterv_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                General.sSQL = "";
                //sSQl = "SELECT CAI_Code, CAI_Libelle" _
                //& " FROM CAI_CategoriInterv WHERE CAI_Code <>'FAC'"
                //        sSQl = "SELECT CAI_Code, CAI_Libelle" _
                //'            & " From ICA_ImmCategorieInterv"
                //         '   & " WHERE CodeImmeuble ='" & txtCodeImmeuble & "'"
                General.sSQL = "Select FacArticle.CodeArticle, FacArticle.Designation1 " + "From FacArticle " + "WHERE ((left(FacArticle.CodeArticle,1)='R') AND" +
                    " ((FacArticle.CodeTypeArticle)=0))" + "ORDER BY Designation1 ASC";

                sheridan.InitialiseCombo(cmbCAI_CategorInterv, General.sSQL, "CodeArticle");
                cmbCAI_CategorInterv.DisplayLayout.Bands[0].Columns[1].Width = cmbCAI_CategorInterv.DisplayLayout.Bands[0].Columns[0].Width * 2;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmbCAI_CategorInterv_DropDown");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenantDT_AfterCloseUp(object sender, EventArgs e)
        {
            try
            {
                fc_libIntervenantDT(cmbIntervenantDT.Text);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmbIntervenantDT_CloseUp");
                throw;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sMatricule"></param>
        /// <returns></returns>
        private bool fc_libIntervenantDT(string sMatricule)
        {
            bool functionReturnValue = true;
            try
            {
                if (!string.IsNullOrEmpty(sMatricule))
                {
                    General.sSQL = "";
                    General.sSQL = "SELECT  Personnel.Nom" + " FROM Personnel" + " where Personnel.Matricule='" + sMatricule + "'";

                    if ((General.rstmp != null))
                    {
                        General.rstmp?.Dispose();
                    }

                    General.modAdorstmp = new ModAdo();
                    General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);

                    if (General.rstmp.Rows.Count > 0)
                    {
                        lblLibIntervenantDT.Text = General.rstmp.Rows[0]["Nom"] + "";
                        functionReturnValue = false;
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet intervenant n'éxiste pas ", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        lblLibIntervenantDT.Text = "";
                        functionReturnValue = true;
                    }
                }
                else
                {
                    lblLibIntervenantDT.Text = "";
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_libIntervenantDT");
                return functionReturnValue;
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenantDT_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                General.sSQL = "";
                General.sSQL = "SELECT Personnel.Matricule, Personnel.Nom, Personnel.Prenom" + " FROM Personnel";
                sheridan.InitialiseCombo(cmbIntervenantDT, General.sSQL, "Matricule");
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmbIntervenantDT_DropDown");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenantDT_KeyPress(object sender, KeyPressEventArgs e)
        {
            try
            {
                if ((int)e.KeyChar == 13)
                {

                    string requete = "SELECT Personnel.Matricule as \"Matricule \"," + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\"," +
                        " Qualification.Qualification as \"Qualification\"," + " Personnel.Memoguard as \"MemoGuard\"," + " Personnel.NumRadio as \"NumRadio\"" +
                        " FROM Qualification right JOIN Personnel " + " ON Qualification.CodeQualif = Personnel.CodeQualif";
                    string where_order = "";
                    SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "" };
                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {
                        // charge les enregistrements de l'immeuble
                        cmbIntervenantDT.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        lblLibIntervenantDT.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            cmbIntervenantDT.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            lblLibIntervenantDT.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                            fg.Dispose();
                            fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();
                }
                //        cmbIntervention_Click
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmbIntervenantDT_KeyPress");
                throw;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervenantDT_Validating(object sender, CancelEventArgs e)
        {
            try
            {
                if (!string.IsNullOrEmpty(cmbIntervenantDT.Text))
                {
                    e.Cancel = fc_libIntervenantDT(cmbIntervenantDT.Text);
                }
                else
                {
                    lblLibIntervenantDT.Text = "";
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmbIntervenantDT_Validate");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            fc_Ajouter();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Ajouter()
        {
            try
            {
                var _with2 = this;
                blModif = false;
                // a modifier temporaire
                if (blModif == true)
                {
                    switch (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous enregistrer vos modifications ?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                    {
                        case DialogResult.Yes:
                            fc_sauver();
                            break;
                        case DialogResult.No:
                            break;
                        // Vider devis en cours
                        default:
                            return;

                            //Reprendre saisie en cours
                            break;
                    }
                }
                //rend le lien vers les interventions invisible.
                lblGoIntervention.Enabled = false;
                //cmdDevis(0).BackColor = &HC0C0C0
                cmdDevis1.BackColor = Color.FromArgb(85, 115, 128); //System.Drawing.ColorTranslator.FromOle(0xc0c0c0);
                cmdDevis1.ForeColor = Color.White;
                cmdGoIntervention.BackColor = System.Drawing.ColorTranslator.FromOle(0x8080ff);
                cmdGoIntervention.ForeColor = Color.White;
                cmdGMAO.BackColor = Color.FromArgb(55, 84, 96);
                cmdGMAO.ForeColor = Color.White;
                this.cmdMail.BackColor = Color.FromArgb(55, 84, 96);//System.Drawing.ColorTranslator.FromOle(0xc0c0c0);
                this.cmdMail.ForeColor = Color.White;
                txtcontrat.Text = "";
                txtCodeContrat.Text = "";
                //==== modif du 11 09 2013, active le frame devis.
                frmDevis.Enabled = true;
                fc_clear();
                fc_LockDevisAc(false);
                fc_BloqueForm("Ajouter");
                txtDateAjout.Text = Convert.ToString(DateTime.Now);
                txtUtilisateur.Text = sUtilisateur;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_Ajouter");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCommande"></param>
        private void fc_BloqueForm(string sCommande = "")
        {
            try
            {
                var _with21 = this;

                _with21.cmdSupprimer.Enabled = false;
                // mode ajout
                if (sCommande.ToUpper() == "Ajouter".ToUpper())
                {
                    _with21.cmdAjouter.Enabled = true;
                    _with21.CmdRechercher.Enabled = false;
                    _with21.CmdSauver.Enabled = true;
                    _with21.txtCodeImmeuble.ReadOnly = false;
                    _with21.txtRaisonSocial_IMM.ReadOnly = false;
                    _with21.txtCodeImmeuble.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cNoir);
                    _with21.txtRaisonSocial_IMM.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cNoir);
                    _with21.cmdRechercheImmeuble.Enabled = true;
                    _with21.cmdRechercheRaisonSocial.Enabled = true;
                    //active le mode modeAjout
                    blnAjout = true;
                }
                else
                {
                    _with21.txtCodeImmeuble.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cBleu);
                    _with21.txtRaisonSocial_IMM.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cBleu);
                    _with21.txtCodeImmeuble.ReadOnly = true;
                    _with21.txtRaisonSocial_IMM.ReadOnly = true;
                    _with21.cmdRechercheImmeuble.Enabled = false;
                    _with21.cmdRechercheRaisonSocial.Enabled = false;
                    _with21.CmdRechercher.Enabled = true;
                    _with21.CmdSauver.Enabled = false;
                    //desactive le mode modeAjout
                    blnAjout = false;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_BloqueForm");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="bLocked"></param>
        private void fc_LockDevisAc(bool bLocked)
        {

            txtNoDevis.ReadOnly = bLocked;
            txtDateDevis.ReadOnly = bLocked;
            ChkDevisAcc.Enabled = !(bLocked);
            cmdRechNoDev.Enabled = !(bLocked);
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool fc_sauver()
        {
            bool functionReturnValue = false;
            string sCode = null;
            int lMsgbox = 0;
            string sSQlx = null;

            try
            {
                if (string.IsNullOrEmpty(txtDateAjout.Text))
                {
                    functionReturnValue = true;
                    return functionReturnValue;
                }
                functionReturnValue = true;

                sCode = txtCodeImmeuble.Text;
                // controle si code client saisie

                if (string.IsNullOrEmpty(sCode))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un code Immeuble.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    functionReturnValue = false;
                    return functionReturnValue;
                }

                using (var tmpModAdo = new ModAdo())
                    if (string.IsNullOrEmpty(tmpModAdo.fc_ADOlibelle("SELECT Immeuble.CodeImmeuble FROM Immeuble WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'")))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un code Immeuble existant.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        functionReturnValue = false;
                        return functionReturnValue;
                    }

                if ((int)ChkDevisAcc.CheckState == 1 && Convert.ToDateTime(txtDateAjout.Text) >= Convert.ToDateTime("09/04/2018"))
                {
                    //=== controle si le la fiche d'appel correspond au bon immeuble.
                    if (!string.IsNullOrEmpty(txtNoDevis.Text))
                    {
                        sSQlx = "SELECT      CodeImmeuble From DevisEnTete WHERE  NumeroDevis = '" + StdSQLchaine.gFr_DoublerQuote(txtNoDevis.Text) + "'";
                        using (var tmpModAdo = new ModAdo())
                            sSQlx = tmpModAdo.fc_ADOlibelle(sSQlx);
                        if (!string.IsNullOrEmpty(sSQlx))
                        {
                            if (sSQlx.ToUpper() != txtCodeImmeuble.Text.ToUpper())
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le code immeuble du devis que vous avez accepté (" + sSQlx + ") ne correspond pas au code immeuble de cette fiche d'appel."
                                    , "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                return functionReturnValue;
                            }
                        }
                        //=== controle si ce devis a déja ete accepté.
                        sSQlx = "SELECT     NumFicheStandard, DevAccep, NoDevis From GestionStandard" + " WHERE     (DevAccep = 1) AND NoDevis = '" + StdSQLchaine.gFr_DoublerQuote(txtNoDevis.Text) + "' AND NumFicheStandard <> " + General.nz(txtNumFicheStandard.Text, 0);
                        using (var tmpModAdo = new ModAdo())
                            sSQlx = tmpModAdo.fc_ADOlibelle(sSQlx);
                        if (!string.IsNullOrEmpty(sSQlx))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le devis " + txtNoDevis.Text + " a déjà été accepté depuis la fiche d'appel n°" + sSQlx + ".", "Erreur", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return functionReturnValue;
                        }
                    }
                }

                //== si devis accepté un os est obligatoire
                if (fc_ctrlOsDevis() == false)
                {
                    return functionReturnValue;
                }

                //===> Mondir le 14.01.2021 pour ajouter les modifs de la version V07.01.2021
                if (txtDateOS.Text.IsDate())
                {
                    if (txtDateOS.Text.ToDate().Date > DateTime.Today)
                    {
                        CustomMessageBox.Show("Vous ne pouvez pas saisir de date d'OS postérieur à la date du jour.", "Opération annulée", MessageBoxIcon.Information);
                        return functionReturnValue;
                    }
                }
                //===> Fin Modif Mondir

                //=== modif du 25 03 2020, si ref os saisi date et fichier obligatoire.
                if (!string.IsNullOrEmpty(txtRefOS.Text))
                {
                    if (string.IsNullOrEmpty(txtDateOS.Value?.ToString()) || (!OptOSori.Checked && !OptOSfichier.Checked))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi une référence de l'OS, la date est obligatoire et indiquez si l'OS correspond au fichier origine" + "\n" + "(fax ou courrier) ou enregistrez un nouveau fichier", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return functionReturnValue;
                    }
                    else if (OptOSfichier.Checked && string.IsNullOrEmpty(txtCheminOS.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez saisi une référence de l'OS, indiquez si l'OS correspond au fichier origine (fax ou courrier)  ou enregistrez un nouveau fichier.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        return functionReturnValue;
                    }
                    else if (OptOSori.Checked && string.IsNullOrEmpty(txtCheminOS.Text))
                    {
                        if (!string.IsNullOrEmpty(txtDocument.Text))
                        {
                            txtCheminOS.Text = txtDocument.Text;
                        }
                        else
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez indiqué que l'OS correspondait à la demande d'origine, vous devez attacher un courrier ou un fax.", "Données manquantes", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return functionReturnValue;
                        }
                    }
                    //ElseIf CDate(txtDateOS.value) > Date Then
                    //                    MsgBox "Vous ne pouvez pas saisir de date d'OS postérieur à la date du jour.", vbInformation, "Opération annulée"
                    //                    Exit Function
                    //End If
                }
                //    If cmbCAI_CategorInterv = "" Then
                //        MsgBox "Vous devez un type d'opération.", vbCritical, "Validation annulée"
                //        fc_sauver = False
                //        Exit Function
                //    End If

                if (string.IsNullOrEmpty(txtNumFicheStandard.Text))
                {

                    FC_insert();
                    fc_maj(txtNumFicheStandard.Text);

                }
                else
                {
                    fc_maj(txtNumFicheStandard.Text);
                }

                if (string.IsNullOrEmpty(sCode))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez saisir un motif.", "Validation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    functionReturnValue = false;
                    return functionReturnValue;
                }

                //Sauve les paramètres de la fiche d'appel
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocStandard, txtNumFicheStandard.Text);

                // debloque les controles du formulaires
                fc_DeBloqueForm();

                // deflage le controle de modifs
                blModif = false;
                View.Theme.Theme.AfficheAlertSucces();
                return functionReturnValue;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_sauver");
                return functionReturnValue;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool fc_ctrlOsDevis()
        {
            bool functionReturnValue = false;
            //== modif du 12 novembre, la creation d 'un devis neccesite un
            //== os (fax ou courrier).
            //== retourne true si os present ( date en dure temporaire).
            System.DateTime dtTemp = default(System.DateTime);
            System.DateTime dtdateAjout = default(System.DateTime);
            string sSQlDev = "";

            //=== modif du 01 08 2019
            sSQlDev = "SELECT     CodeEtat From DevisEnTete WHERE     NumeroDevis = '" + StdSQLchaine.gFr_DoublerQuote(txtNoDevis.Text) + "'";
            using (var tmpModAdo = new ModAdo())
                sSQlDev = tmpModAdo.fc_ADOlibelle(sSQlDev);

            if (sSQlDev.ToUpper() != "04".ToUpper())
            {
                if (sSQlDev.ToUpper() == "03".ToUpper() || General.UCase(sSQlDev) == General.UCase("02") || General.UCase(sSQlDev) == General.UCase("01"))
                {
                    CustomMessageBox.Show("Vous ne pouvez pas accepter un devis avec le statut " + sSQlDev + ", seul le statut 04 permet d'effectuer cette opération.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return functionReturnValue;
                }
            }

            dtTemp = Convert.ToDateTime("11/11/2006");

            if (!General.IsDate(txtDateAjout.Text))
            {
                return functionReturnValue;
            }

            functionReturnValue = true;

            if (Convert.ToDateTime(txtDateAjout.Text) >= dtTemp)
            {
                if (General.sDocObligatoirePourDevis == "1")
                {
                    if ((int)ChkDevisAcc.CheckState == 1 && string.IsNullOrEmpty(txtDocument.Text))
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Validation annulée, " + " un Ordre de service (fax ou courrier)  est obligatoire lors d'une acceptation de devis.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        functionReturnValue = false;
                    }
                }
            }
            return functionReturnValue;

        }
        /// <summary>
        /// Tested
        /// </summary>
        private void FC_insert()
        {
            try
            {
                General.sSQL = "";
                General.sSQL = "SELECT GestionStandard.*" + " From GestionStandard" + " WHERE GestionStandard.NumFicheStandard =0";

                if ((General.rstmp != null))
                {
                    General.modAdorstmp?.Dispose();
                }

                General.modAdorstmp = new ModAdo();
                General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);
                General.rstmp.Rows.Add(General.rstmp.NewRow());
                //rsTmp! = nz(txtDdeDeDevis, Null)
                // rsTmp! = nz(txtDdeDevisObs, Null)
                //rsTmp! = nz(txtDevAccep, Null)
                //rsTmp! = nz(txtNoDevis, Null)
                // rsTmp! = nz(txtDateDevis, Null)
                //rsTmp! = nz(txtcodeOriCommentDevis, Null)
                //rsTmp! = nz(txtDocumentDevis, Null)
                //rsTmp! = nz(txtQuiDevis, Null)
                //rsTmp! = nz(txtNumFicheStandard, Null)
                General.rstmp.Rows[0]["Utilisateur"] = General.nz(txtUtilisateur.Text, System.DBNull.Value);
                General.rstmp.Rows[0]["DateAjout"] = General.nz(txtDateAjout.Text, System.DBNull.Value);
                General.rstmp.Rows[0]["CodeImmeuble"] = General.nz(txtCodeImmeuble.Text, System.DBNull.Value);
                General.rstmp.Rows[0]["CodeOrigine1"] = General.nz(txtCodeOrigine1.Text, System.DBNull.Value);
                General.rstmp.Rows[0]["CodeOrigine2"] = General.nz(txtCodeOrigine2.Text, System.DBNull.Value);
                General.rstmp.Rows[0]["Societe"] = General.nz(txtSociete.Text, System.DBNull.Value);
                General.rstmp.Rows[0]["Source"] = General.nz(txtSource.Text, System.DBNull.Value);
                General.rstmp.Rows[0]["IntervenantDT"] = General.nz(cmbIntervenantDT.Text, System.DBNull.Value);
                General.rstmp.Rows[0]["Observations"] = General.nz(txtObservations.Text, System.DBNull.Value);
                //rsTmp!CodeAction = nz(cmbCodeAction, Null)
                General.rstmp.Rows[0]["Document"] = General.nz(txtDocument.Text, System.DBNull.Value);
                General.rstmp.Rows[0]["CodeParticulier"] = General.nz(txtCodeParticulier.Text, System.DBNull.Value);
                //rsTmp!TexteLibre = nz(txtTexteLibre, Null)
                General.rstmp.Rows[0]["Intervention"] = General.nz(txtIntervention.Text, 0);
                General.rstmp.Rows[0]["CodeOrigine3_TO3"] = General.nz(txtCodeOrigine3_TO3.Text, System.DBNull.Value);
                General.rstmp.Rows[0]["CAI_Code"] = General.nz(cmbCAI_CategorInterv.Text, System.DBNull.Value);
                int xx = General.modAdorstmp.Update();
                using (var tmpModAdo = new ModAdo())
                    txtNumFicheStandard.Text = tmpModAdo.fc_ADOlibelle("SELECT MAX(Numfichestandard) FROM GestionStandard");
                General.modAdorstmp?.Dispose();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";FC_Insert");
            }
        }

        private void fc_maj(string sCode)
        {
            double nbHeuresDevis = 0;
            int nbInterventions = 0;
            string AnalytiqueActivite = null;

            short i = 0;

            try
            {
                General.sSQL = "";
                General.sSQL = "Update GestionStandard set ";
                //sSQl = sSQl & "CodeAction='" & nz(gFr_DoublerQuote(txtCodeAction), "") & "',"
                General.sSQL = General.sSQL + "CodeImmeuble='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text), "") + "',";
                General.sSQL = General.sSQL + "codeOriCommentDevis=" + General.nz(txtCodeOriCommentDevis, "99") + ",";
                General.sSQL = General.sSQL + "CodeOrigine1='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCodeOrigine1.Text), "") + "',";
                General.sSQL = General.sSQL + "CodeOrigine2='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCodeOrigine2.Text), "") + "',";
                General.sSQL = General.sSQL + "CodeOrigine3_TO3='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCodeOrigine3_TO3.Text), "") + "',";
                General.sSQL = General.sSQL + "CodeParticulier='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCodeParticulier.Text), "") + "',";
                General.sSQL = General.sSQL + "DdeDeDevis=" + General.nz(StdSQLchaine.gFr_NombreAng(ChkDmdeDevis.CheckState == CheckState.Checked ? 1 : 0), "0") + ",";
                General.sSQL = General.sSQL + "DdeDevisObs='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtTexte.Text), "") + "',";
                General.sSQL = General.sSQL + "DevAccep=" + General.nz(StdSQLchaine.gFr_NombreAng(ChkDevisAcc.CheckState == CheckState.Checked ? 1 : 0), "0") + ",";
                General.sSQL = General.sSQL + "Document='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtDocument.Text), "") + "',";
                General.sSQL = General.sSQL + "DocumentDevis='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtDocumentDevis.Text), "") + "',";
                General.sSQL = General.sSQL + "IntervenantDT='" + General.nz(StdSQLchaine.gFr_DoublerQuote(cmbIntervenantDT.Text), "") + "',";
                General.sSQL = General.sSQL + "Intervention='" + General.nz(StdSQLchaine.gFr_NombreAng(txtIntervention.Text), "0") + "',";
                General.sSQL = General.sSQL + "NoDevis='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtNoDevis.Text), "") + "',";
                ///==================> Tested
                if (!string.IsNullOrEmpty(txtDateDevis.Text) && General.IsDate(txtDateDevis.Text))
                {
                    General.sSQL = General.sSQL + "DateDevis='" + Convert.ToDateTime(txtDateDevis.Text).ToString(General.FormatDateSQL) + "',";
                }
                else
                {
                    General.sSQL = General.sSQL + "DateDevis=null,";
                }
                General.sSQL = General.sSQL + "Observations='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtObservations.Text), "") + "',";
                General.sSQL = General.sSQL + "QuiDevis='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtQuiDevis.Text), "") + "',";
                General.sSQL = General.sSQL + "Societe='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtSociete.Text), "") + "',";
                General.sSQL = General.sSQL + "Source='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtSource.Text), "") + "',";
                //sSql = sSql & "TexteLibre='" & nz(gFr_DoublerQuote(txtTextelibre), "") & "',"
                General.sSQL = General.sSQL + "CAI_Code='" + General.nz(StdSQLchaine.gFr_DoublerQuote(cmbCAI_CategorInterv.Text), "") + "',";

                //=== modif du 25 03 2020.
                General.sSQL = General.sSQL + "RefOS='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtRefOS.Text), "") + "',";
                General.sSQL = General.sSQL + "DateOS=" + (txtDateOS.Value != DBNull.Value ? "'" + txtDateOS.Text + "'" : "Null") + ",";

                if (OptOSori.Checked)
                {
                    General.sSQL = General.sSQL + "FichierOSorigine= 1 ,";
                }
                else if (OptOSfichier.Checked)
                {
                    General.sSQL = General.sSQL + "FichierOSorigine = 2 ,";
                }
                else
                {
                    General.sSQL = General.sSQL + "FichierOSorigine = 0 ,";
                }

                General.sSQL = General.sSQL + "CheminOS='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtCheminOS.Text), "") + "',";
                // '=== fin modif du 25 03 2020.

                General.sSQL = General.sSQL + "Utilisateur='" + General.nz(StdSQLchaine.gFr_DoublerQuote(txtUtilisateur.Text), "") + "'";
                General.sSQL = General.sSQL + " where NumFicheStandard=" + sCode;

                int xx = General.Execute(General.sSQL);

                // mise à jours des champs de l'immeuble.
                General.sSQL = "Update Immeuble set ";
                //sSQl = sSQl & "Adresse='" & gFr_DoublerQuote(txtAdresse) & "',"
                //sSQl = sSQl & "Adresse2_IMM='" & gFr_DoublerQuote(txtAdresse2_IMM) & "',"
                //sSQl = sSQl & "Ville='" & gFr_DoublerQuote(txtVille) & "',"
                //sSQl = sSQl & "CodePostal='" & gFr_DoublerQuote(txtCodePostal) & "',"
                General.sSQL = General.sSQL + "Gardien='" + StdSQLchaine.gFr_DoublerQuote(txtGardien.Text) + "' ,";
                General.sSQL = General.sSQL + "Observations='" + StdSQLchaine.gFr_DoublerQuote(txtObservation.Text) + "',";
                General.sSQL = General.sSQL + "CodeAcces1='" + StdSQLchaine.gFr_DoublerQuote(txtCodeAcces1.Text) + "',";
                General.sSQL = General.sSQL + "CodeAcces2='" + StdSQLchaine.gFr_DoublerQuote(txtCodeAcces2.Text) + "'";
                General.sSQL = General.sSQL + " where codeimmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
                xx = General.Execute(General.sSQL);

                //Mise à jour du code etat du devis (accepté) et de la date d'acceptation
                //Création en automatique des interventions en fonction du nombre d'heures
                //de main d'oeuvre
                if (!string.IsNullOrEmpty(txtNoDevis.Text) && General.IsDate(txtDateDevis.Text))
                {
                    //== modif du 12 novembre

                    SAGE.VerifCreeAnalytique((txtNumFicheStandard.Text), (txtNumFicheStandard.Text), (txtCodeImmeuble.Text));

                    ///============> Tested
                    if ((General.rstmp != null))
                    {
                        General.modAdorstmp?.Dispose();
                    }

                    General.modAdorstmp = new ModAdo();
                    General.rstmp = General.modAdorstmp.fc_OpenRecordSet("SELECT DevisEntete.DateAcceptation, codeetat "
                        + " FROM DevisEntete WHERE DevisEntete.NumeroDevis='" + StdSQLchaine.gFr_DoublerQuote(txtNoDevis.Text) + "'");
                    //If IsNull(rstmp!DateAcceptation) Or rstmp!DateAcceptation = "" Then

                    if (General.nz(General.rstmp.Rows[0]["CodeEtat"], "").ToString().ToUpper() == "04".ToUpper())
                    {

                        //===> Mondir le 23.11.2020, regarder la signature de la fonction
                        General.fc_HistoStatutDevis(StdSQLchaine.gFr_DoublerQuote(txtNoDevis.Text), "A", "UserDocStandard");

                        using (var tmpModAdo = new ModAdo())
                        {
                            var req = "SELECT NumeroDevis, codeetat FROM DevisEntete  WHERE Annee='" +
                                      General.Left(txtNoDevis.Text, 4)
                                      + "' AND Mois='" + General.Mid(txtNoDevis.Text, 5, 2) + "' AND Ordre='" + General.Mid(txtNoDevis.Text, 7, 3)
                                      + "' AND NumeroDevis<>'" + txtNoDevis.Text + "'";
                            var dt = tmpModAdo.fc_OpenRecordSet(req);
                            foreach (DataRow row in dt.Rows)
                            {
                                General.fc_HistoStatutDevis(General.nz(row["NumeroDevis"], "").ToString(), "R", "UserDocStandard");
                            }
                        }
                        //===> Fin Modif Mondir

                        General.sSQL = "UPDATE DevisEntete set DateAcceptation ='"
                            + Convert.ToDateTime(txtDateDevis.Text).ToString(General.FormatDateSQL) + "', CodeEtat = 'A' WHERE NumeroDevis ='"
                            + txtNoDevis.Text + "'";

                        General.Execute(General.sSQL);


                        //==== maj du statut en R des version pour ce devis.
                        General.sSQL = "UPDATE DevisEntete set DateAcceptation ='" + Convert.ToDateTime(txtDateDevis.Text).ToString(General.FormatDateSQL) + "'"
                            + ", CodeEtat = 'R' WHERE Annee='" +
                            General.Left(txtNoDevis.Text, 4)
                            + "' AND Mois='" + General.Mid(txtNoDevis.Text, 5, 2) + "' AND Ordre='" + General.Mid(txtNoDevis.Text, 7, 3)
                            + "' AND NumeroDevis<>'" + txtNoDevis.Text + "'";


                        General.Execute(General.sSQL);

                        //cmdDevis(0).BackColor = &HC0FFC0                       
                        cmdDevis1.BackColor = Color.FromArgb(85, 115, 128);//System.Drawing.ColorTranslator.FromOle(0xc0ffc0);
                        cmdDevis1.ForeColor = Color.White;
                    }

                    General.modAdorstmp?.Dispose();

                    ///============> Tested
                    if ((General.rstmp != null))
                    {
                        General.modAdorstmp?.Dispose();
                    }

                    General.modAdorstmp = new ModAdo();
                    General.rstmp = General.modAdorstmp.fc_OpenRecordSet("SELECT DevisDetail.Mohud,DevisDetail.Quantite FROM DevisDetail "
                                 + " WHERE DevisDetail.NumeroDevis='" + txtNoDevis.Text + "'");

                    ///============> Tested
                    if (General.rstmp.Rows.Count > 0)
                    {
                        foreach (DataRow rstmpRow in General.rstmp.Rows)
                        {
                            if (rstmpRow["Mohud"] != DBNull.Value && rstmpRow["Quantite"] != DBNull.Value)
                            {
                                nbHeuresDevis = nbHeuresDevis + (Convert.ToDouble(rstmpRow["Mohud"]) * Convert.ToInt32(rstmpRow["Quantite"]));
                            }
                        }
                    }
                    General.modAdorstmp?.Dispose();

                    if ((nbHeuresDevis % (8 + 1)) > 0)
                    {
                        nbInterventions = (int)((nbHeuresDevis / (8 + 1)) - ((nbHeuresDevis % (8 + 1)) / (8 + 1)) + 1);
                    }
                    else
                    {
                        nbInterventions = (int)(nbHeuresDevis / (8 + 1));
                    }

                    if (nbInterventions <= 1)
                    {
                        nbInterventions = 1;
                    }

                    General.modAdorstmp = new ModAdo();
                    General.rstmp = General.modAdorstmp.fc_OpenRecordSet("SELECT Intervention.NoIntervention FROM Intervention WHERE Intervention.NumFicheStandard='" + txtNumFicheStandard.Text + "'");
                    if (General.rstmp.Rows.Count == 0)
                    {
                        //== modif du 10 octobre 2007, flag autorisant la création que d'une seule
                        //== intervention par devis.

                        for (i = 1; i <= nbInterventions; i++)
                        {
                            if (General.sMonoIntervParDevis == "1" & i > 1)
                                break;

                            General.sSQL = "INSERT INTO Intervention (NumFicheStandard,CodeEtat,CodeImmeuble,DateSaisie,Achat,NoDevis,Article,Designation,Commentaire,Intervenant) VALUES ('" + txtNumFicheStandard.Text + "','00',";
                            General.sSQL = General.sSQL + "'" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "','" + DateTime.Today.ToString(General.FormatDateSQL) + "',1,'" + txtNoDevis.Text + "',";
                            using (var tmpModAdo = new ModAdo())
                                General.sSQL = General.sSQL + "'" + StdSQLchaine.gFr_DoublerQuote(tmpModAdo.fc_ADOlibelle("SELECT CodeTitre FROM DevisEntete WHERE DevisEntete.NumeroDevis='" + txtNoDevis.Text + "'")) + "',";
                            using (var tmpModAdo = new ModAdo())
                                General.sSQL = General.sSQL + "'" + StdSQLchaine.gFr_DoublerQuote(tmpModAdo.fc_ADOlibelle("SELECT TitreDevis FROM DevisEntete WHERE DevisEntete.NumeroDevis='" + txtNoDevis.Text + "'")) + "',";
                            General.sSQL = General.sSQL + "'Suite Devis N°: " + txtNoDevis.Text + "',";
                            General.sSQL = General.sSQL + "'" + "000" + "')";
                            xx = General.Execute(General.sSQL);

                        }
                    }

                    General.modAdorstmp?.Dispose();
                    cmdGoIntervention.BackColor = Color.FromArgb(192, 255, 192);
                    cmdGoIntervention.ForeColor = Color.Black;
                    txtIntervention.Text = Convert.ToString(nbInterventions);

                    //===@@@ modif du 29 05 2017, desactive Sage.
                    ///============> Tested
                    if (General.sDesActiveSage == "1")
                    {

                    }
                    else
                    {
                        //=== mise à jour de l'analytique.
                        SAGE.fc_OpenConnSage();
                        using (var tmpModAdo = new ModAdo())
                            AnalytiqueActivite = tmpModAdo.fc_ADOlibelle("SELECT CA_Num FROM F_COMPTEA WHERE ActiviteDevis='1'", false, SAGE.adoSage);
                        SAGE.fc_CloseConnSage();
                        xx = General.Execute("UPDATE GestionStandard SET Intervention=1,AnalytiqueActivite='" + StdSQLchaine.gFr_DoublerQuote(AnalytiqueActivite) + "' WHERE GestionStandard.NumFicheStandard='" + txtNumFicheStandard.Text + "'");
                    }

                    //==Fonction en cours de developpement.
                    if (General.sPrecommande == "1")
                    {
                        fc_BcmdNew();
                    }
                    else
                    {
                        fc_Bcmd();
                        //Crée un bon de commande en fonction des fournitures du devis
                    }

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_maj");
            }
        }

        /// <summary>
        ///
        /// Modif de la version V26.06.2020 Ajouté par Mondir
        /// </summary>
        /// <returns></returns>
        public object fc_BcmdNew()
        {
            object functionReturnValue = null;

            string strInsert = null;
            string strRequete = null;
            DataTable rsBcmd = default(DataTable);
            ModAdo modAdorsBcmd = null;
            DataTable rsFindFourn = default(DataTable);
            ModAdo modAdorsFindFourn = null;
            DataTable rsG7 = default(DataTable);
            ModAdo modAdorsG7 = null;
            int i = 0;
            string NoBcmd = null;
            string strPrix = null;
            string sFourDT = null;
            int lCleFourDT = 0;
            string sChrono = null;
            double dbQte = 0;
            string sRefFournBDD2 = null;
            string sRefFabBDD2 = null;
            string sTempId = null;
            int lIDGEC = 0;
            DataTable rsBDD2 = default(DataTable);
            ModAdo modAdorsBDD2 = null;
            string sFournBDD2 = null;

            //Mondir le 27.06.2020, modif de la version VB6 V26.06.2020
            string sSQlFournGecet = "";
            //Fin modif mondir

            try
            {
                rsBcmd = new DataTable();

                //Si un bon de commande existe alors on sort
                strRequete = "SELECT  PRE_NoAuto FROM PRE_PreCmdEnTete WHERE NumeroDevis='" + txtNoDevis.Text + "'";
                modAdorsBcmd = new ModAdo();
                rsBcmd = modAdorsBcmd.fc_OpenRecordSet(strRequete);
                if (rsBcmd.Rows.Count > 0)
                {
                    modAdorsBcmd?.Dispose();
                    return functionReturnValue;
                }

                modAdorsBcmd?.Dispose();

                strInsert = "INSERT INTO PRE_PreCmdEnTete (";
                strInsert = strInsert + "CodeImmeuble";
                strInsert = strInsert + ",NoIntervention";
                strInsert = strInsert + ",NumeroDevis";
                strInsert = strInsert + ",NumFicheStandard";
                strInsert = strInsert + ",PRE_CreeLe";
                strInsert = strInsert + ",PRE_CreePar";
                strInsert = strInsert + ") VALUES (";
                strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "',";
                using (var tmpModAdo = new ModAdo())
                    strInsert = strInsert + tmpModAdo.fc_ADOlibelle("SELECT TOP 1 Intervention.NoIntervention FROM Intervention WHERE NumFicheStandard=" + txtNumFicheStandard.Text +
                        " ORDER BY NoIntervention") + ",";
                strInsert = strInsert + "'" + txtNoDevis.Text + "',";
                strInsert = strInsert + txtNumFicheStandard.Text + ",";
                strInsert = strInsert + "'" + DateTime.Now + "',";
                strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'";
                strInsert = strInsert + ")";

                int xx = General.Execute(strInsert);

                modAdorsBcmd = new ModAdo();

                rsBcmd = modAdorsBcmd.fc_OpenRecordSet("SELECT PRE_NoAuto FROM PRE_PreCmdEnTete WHERE NumeroDevis='" + txtNoDevis.Text + "'");
                if (rsBcmd.Rows.Count > 0)
                {
                    NoBcmd = rsBcmd.Rows[0]["PRE_NoAuto"] + "";
                }
                modAdorsBcmd?.Dispose();

                //=== recherche des codes articles dans le devis.
                strRequete = "SELECT CodeSousArticle, TexteLigne, Quantite, FOud";
                strRequete = strRequete + "  FROM         DevisDetail";
                strRequete = strRequete + " WHERE DevisDetail.NumeroDevis='" + txtNoDevis.Text + "' ";
                strRequete = strRequete + " AND NOT DevisDetail.CodeSousArticle IS NULL ";
                strRequete = strRequete + " AND DevisDetail.CodeSousArticle<>'' ";
                strRequete = strRequete + " ORDER BY DevisDetail.CodeSousArticle";

                modAdorsBcmd = new ModAdo();
                rsBcmd = modAdorsBcmd.fc_OpenRecordSet(strRequete);

                sChrono = Convert.ToString(0);
                i = 1;
                if (rsBcmd.Rows.Count > 0)
                {
                    foreach (DataRow rsBcmdRow in rsBcmd.Rows)
                    {
                        sFourDT = "";
                        lCleFourDT = 0;
                        if (General.nz(sChrono, 0) != General.nz(rsBcmdRow["CodeSousArticle"], 0) || General.nz(sChrono, 0).ToString() == "80000"
                            || General.UCase(General.Left(General.nz(sChrono, 0).ToString(), General.cArtiNonQualifie.Length)) == General.UCase(General.cArtiNonQualifie))
                        {

                            //== stoque le code chrono.
                            sChrono = General.nz(rsBcmdRow["CodeSousArticle"], 0).ToString();

                            //== calcul les quantités totale pour chaque article.
                            dbQte = 0;
                            if (General.nz(sChrono, 0).ToString() != "80000")
                            {
                                using (var tmpModAdo = new ModAdo())
                                    dbQte = Convert.ToDouble(General.nz(tmpModAdo.fc_ADOlibelle("SELECT sum(Quantite) as qte FROM DevisDetail " + " Where CodeSousArticle ='" + StdSQLchaine.gFr_DoublerQuote(sChrono) + "'" + " and DevisDetail.NumeroDevis='" + txtNoDevis.Text + "'"), 0));
                            }
                            else
                            {
                                dbQte = Convert.ToDouble(General.nz(rsBcmdRow["Quantite"], 0));
                            }

                            if (General.sGecetV3 == "1")
                            {
                                modAdorsG7 = new ModAdo();
                                rsG7 = modAdorsG7.fc_OpenRecordSet("SELECT    AXECIEL_GECET.dbo.ArticleTarif.CODE_UNITE_DEVIS AS fouunite, "
                                    + " AXECIEL_GECET.dbo.ArticleTarif.NOM_FOURNISSEUR AS fournisseur,"
                                    + " AXECIEL_GECET.dbo.ArticleTarif.REF_FOURNISSEUR AS fourref, "
                                    + " AXECIEL_GECET.dbo.ArticleTarif.PRX_DEVIS_NET_UD AS Prix, "
                                    + " AXECIEL_GECET.dbo.ArticleTarif.IDGEC_LIGNE_PRIX As NoAuto, "
                                    + " AXECIEL_GECET.dbo.ArticleTarif.REF_FABRICANT As fabref,  "
                                    + " AXECIEL_GECET.dbo.ArticleTarif.CHRONO_INITIAL As chrono , QTE_Unite.QTE_No, AXECIEL_GECET.dbo.ArticleTarif.NOM_FOURNISSEUR "
                                    + " FROM         AXECIEL_GECET.dbo.ArticleTarif LEFT OUTER JOIN"
                                    + " QTE_Unite ON AXECIEL_GECET.dbo.ArticleTarif.CODE_UNITE_DEVIS = QTE_Unite.QTE_Libelle"
                                    + " WHERE AXECIEL_GECET.dbo.ArticleTarif.CHRONO_INITIAL ='" + StdSQLchaine.gFr_DoublerQuote(sChrono) + "'");
                            }
                            else if (General.sGecetV2 == "1")
                            {
                                //Set rsG7 = fc_OpenRecordSet("SELECT    BaseGecet.dbo.ARTP_ArticlePrix.ARTP_UniteFourn AS fouunite, " _
                                //& " BaseGecet.dbo.ARTP_ArticlePrix.ARTP_Nofourn AS fournisseur," _
                                //& " BaseGecet.dbo.ARTP_ArticlePrix.ARTP_Fourn AS four, " _
                                //& " BaseGecet.dbo.ARTP_ArticlePrix.ARTP_PrixNetDevis AS Prix, " _
                                //& " BaseGecet.dbo.ARTP_ArticlePrix.ARTP_NoAuto As NoAuto, " _
                                //& " BaseGecet.dbo.ARTP_ArticlePrix.ARTP_Fab As fab,  " _
                                //& " BaseGecet.dbo.ARTP_ArticlePrix.ART_Chrono As chrono , QTE_Unite.QTE_No " _
                                //& " FROM         BaseGecet.dbo.ARTP_ArticlePrix LEFT OUTER JOIN" _
                                //& " QTE_Unite ON BaseGecet.dbo.ARTP_ArticlePrix.ARTP_UniteFourn = QTE_Unite.QTE_Libelle" _
                                //& " WHERE BaseGecet.dbo.ARTP_ArticlePrix.ART_Chrono ='" & gFr_DoublerQuote(sChrono) & "'")

                                //==== modif du 17 11 2014, changmenet de l'unité devis.
                                modAdorsG7 = new ModAdo();
                                rsG7 = modAdorsG7.fc_OpenRecordSet("SELECT    BaseGecet.dbo.ARTP_ArticlePrix.ARTP_UniteDevis AS fouunite, "
                                    + " BaseGecet.dbo.ARTP_ArticlePrix.ARTP_Nofourn AS fournisseur,"
                                    + " BaseGecet.dbo.ARTP_ArticlePrix.ARTP_RefFourn AS fourref, "
                                    + " BaseGecet.dbo.ARTP_ArticlePrix.ARTP_PrixNetDevis AS Prix, "
                                    + " BaseGecet.dbo.ARTP_ArticlePrix.ARTP_NoAuto As NoAuto, "
                                    + " BaseGecet.dbo.ARTP_ArticlePrix.ARTP_RefFab As fabref,  "
                                    + " BaseGecet.dbo.ARTP_ArticlePrix.ART_Chrono As chrono , QTE_Unite.QTE_No , BaseGecet.dbo.ARTP_ArticlePrix.ARTP_Nomfourn"
                                    + " FROM         BaseGecet.dbo.ARTP_ArticlePrix LEFT OUTER JOIN"
                                    + " QTE_Unite ON BaseGecet.dbo.ARTP_ArticlePrix.ARTP_UniteDevis = QTE_Unite.QTE_Libelle"
                                    + " WHERE BaseGecet.dbo.ARTP_ArticlePrix.ART_Chrono ='" + StdSQLchaine.gFr_DoublerQuote(sChrono) + "'");
                            }
                            else
                            {
                                //== recupéres toutes les erences fournisseurs pour chaque article.
                                modAdorsG7 = new ModAdo();
                                rsG7 = modAdorsG7.fc_OpenRecordSet("SELECT BaseGecet.dbo.Dtiprix.fouunite,"
                                    + " BaseGecet.dbo.Dtiprix.fournisseur,"
                                    + " BaseGecet.dbo.Dtiprix.fourref, BaseGecet.dbo.Dtiprix.[prix net achat] AS Prix, "
                                    + " BaseGecet.dbo.Dtiprix.NoAuto, BaseGecet.dbo.Dtiprix.fabref, "
                                    + " BaseGecet.dbo.Dtiprix.fourref AS Expr1, QTE_Unite.QTE_No,"
                                    + " BaseGecet.dbo.Dtiprix.Chrono"
                                    + " FROM         BaseGecet.dbo.Dtiprix LEFT OUTER JOIN"
                                    + " QTE_Unite ON BaseGecet.dbo.Dtiprix.fouunite = QTE_Unite.QTE_Libelle"
                                    + " WHERE     (BaseGecet.dbo.Dtiprix.Chrono =" + StdSQLchaine.gFr_DoublerQuote(sChrono) + ")");
                            }

                            if (rsG7.Rows.Count == 0)
                            {

                                sFourDT = "";
                                sRefFournBDD2 = "";
                                sRefFabBDD2 = "";
                                // '==== modif du 07 01 2020, controle si dans la nouvelle base BDD2, le fournisseur est existant.
                                if (General.UCase(General.Left(General.nz(sChrono, 0).ToString(), General.cArtiNonQualifie.Length)) == General.UCase(General.cArtiNonQualifie))
                                {
                                    sTempId = General.Mid(sChrono, (General.cArtiNonQualifie.Length) + 1, sChrono.Length - (General.cArtiNonQualifie.Length) + 1);
                                    if (General.IsNumeric(sTempId))
                                    {
                                        lIDGEC = Convert.ToInt32(sTempId);
                                        //'sFournBDD2 = fc_ADOlibelle("SELECT     AXECIEL_GECET.dbo.BDD2.FOURNISSEUR " _
                                        //               & " From AXECIEL_GECET.dbo.BDD2   WHERE     AXECIEL_GECET.dbo.BDD2.IDGEC_DONNEES_NON_QUALIFIEES = " & lIDGEC)
                                        modAdorsBDD2 = new ModAdo();
                                        rsBDD2 = new DataTable();
                                        rsBDD2 = modAdorsBDD2.fc_OpenRecordSet("SELECT     AXECIEL_GECET.dbo.BDD2.FOURNISSEUR, "

                                                       // =======> Old before 26.06.2020+ " AXECIEL_GECET.dbo.BDD2.REF_FOURNISSEUR, AXECIEL_GECET.dbo.BDD2.REF_FABRICANT "
                                                       //Mondir le 27.06.2020 ajouté des modfis de la version vb6 V26.06.2020
                                                       + " AXECIEL_GECET.dbo.BDD2.REF_FOURNISSEUR, AXECIEL_GECET.dbo.BDD2.REF_FABRICANT, AXECIEL_GECET.dbo.BDD2.CODE_FOURNISSEUR_ADHERENT "
                                                       + " From AXECIEL_GECET.dbo.BDD2   WHERE     AXECIEL_GECET.dbo.BDD2.IDGEC_DONNEES_NON_QUALIFIEES = " + lIDGEC);
                                        if (rsBDD2.Rows.Count > 0)
                                        {
                                            sRefFournBDD2 = rsBDD2.Rows[0]["REF_FOURNISSEUR"] + "";
                                            sRefFabBDD2 = rsBDD2.Rows[0]["REF_FABRICANT"] + "";

                                            //======> Old before 26.06.2020 sFournBDD2 = rsBDD2.Rows[0]["FOURNISSEUR"] + "";
                                            //Mondir 27.06.2020 ajouté des modfis de la version vb6 V26.06.2020
                                            if (General.nz(rsBDD2.Rows[0]["CODE_FOURNISSEUR_ADHERENT"], "")?.ToString() != "")
                                            {
                                                sFournBDD2 = rsBDD2.Rows[0]["CODE_FOURNISSEUR_ADHERENT"]?.ToString();

                                                sSQlFournGecet = "SELECT Code, cleAuto From fournisseurArticle "
                                                                 + " WHERE NCompte = '" + StdSQLchaine.gFr_DoublerQuote(sFournBDD2) + "'";
                                            }
                                            else
                                            {
                                                sFournBDD2 = rsBDD2.Rows[0]["Fournisseur"]?.ToString();

                                                sSQlFournGecet = "SELECT Code, cleAuto From fournisseurArticle "
                                                                 + " WHERE CodeFournGecet = '" + StdSQLchaine.gFr_DoublerQuote(sFournBDD2) + "'";
                                            }

                                            rsFindFourn = new DataTable();
                                            modAdorsFindFourn = new ModAdo();
                                            //This 3 commented lines are from an old version before V26.06.2020
                                            //rsFindFourn = modAdorsFindFourn.fc_OpenRecordSet("SELECT Code, cleAuto From fournisseurArticle "
                                            //                        + " WHERE CodeFournGecet = '" + StdSQLchaine.gFr_DoublerQuote(sFournBDD2) + "'");
                                            //Mondir 27.06.2020 ajouté des modfis de la version vb6 V26.06.2020
                                            rsFindFourn = modAdorsFindFourn.fc_OpenRecordSet(sSQlFournGecet);
                                            //Find Modfis Mondir

                                            if (rsFindFourn.Rows.Count > 0)
                                            {
                                                sFourDT = General.nz(rsFindFourn.Rows[0]["Code"], "").ToString();
                                                lCleFourDT = Convert.ToInt32(General.nz(rsFindFourn.Rows[0]["cleAuto"], 0));
                                            }
                                        }
                                        modAdorsBDD2?.Dispose();
                                        rsBDD2 = null;
                                    }
                                }
                                if (sFourDT == "")
                                {
                                    //== recupere le fournisseur par defaut.
                                    sFourDT = General.sDefautFournisseur;
                                    using (var tmpModAdo = new ModAdo())
                                        lCleFourDT = Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Cleauto FROM fournisseurArticle" + " WHERE CODE ='" + StdSQLchaine.gFr_DoublerQuote(sFourDT) + "'"), 0));

                                }

                                //== Prepare la requete d'insertion.
                                strInsert = "INSERT INTO PRC_PreCmdCorps (";

                                strInsert = strInsert + "PRE_NoAuto";
                                strInsert = strInsert + ",PRC_References";
                                strInsert = strInsert + ",PRC_Designation";
                                strInsert = strInsert + ",PRC_Quantite";
                                strInsert = strInsert + ",PRC_PrixHT";
                                strInsert = strInsert + ",PRC_TotHT";
                                strInsert = strInsert + ",PRC_Unite";
                                strInsert = strInsert + ",PRC_NoLigne";
                                strInsert = strInsert + ",PRC_Fournisseur";
                                strInsert = strInsert + ",PRC_Chrono";
                                strInsert = strInsert + ",NoAutoGecet";
                                strInsert = strInsert + ",PRC_fabref";
                                strInsert = strInsert + ",PRC_fourref";
                                strInsert = strInsert + ",PRC_FournisseurRef";
                                strInsert = strInsert + ",PRC_FournisseurDT";
                                strInsert = strInsert + ",PRC_FournDTCle";

                                strInsert = strInsert + ") VALUES (";

                                strInsert = strInsert + NoBcmd + ",";
                                //PRE_NoAuto
                                strInsert = strInsert + "'" + General.nz(sRefFournBDD2, StdSQLchaine.gFr_DoublerQuote(rsBcmdRow["CodeSousArticle"].ToString())) + "',";
                                //PRC_erences
                                strInsert = strInsert + "'" + General.Left(StdSQLchaine.gFr_DoublerQuote(rsBcmdRow["TexteLigne"].ToString()), 250) + "',";
                                //PRC_Designation
                                strInsert = strInsert + "" + dbQte + ",";
                                //PRC_Quantite
                                strInsert = strInsert + "" + General.nz(rsBcmdRow["Foud"], "0") + ",";
                                //PRC_PrixHT
                                strInsert = strInsert + General.FncArrondir(dbQte * Convert.ToDouble(General.nz(rsBcmdRow["Foud"], "0")), 2) + ",";
                                //PRC_TotHT
                                strInsert = strInsert + "0,";
                                //PRC_Unite
                                strInsert = strInsert + i + ", ";
                                //PRC_NoLigne
                                strInsert = strInsert + "'',";
                                //PRC_Fournisseur
                                strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(rsBcmdRow["CodeSousArticle"].ToString()) + "',";
                                //PRC_Chrono
                                strInsert = strInsert + "'',";
                                //NoAutoGecet
                                strInsert = strInsert + "'" + sRefFabBDD2 + "',";
                                //PRC_fab
                                strInsert = strInsert + "'" + sRefFournBDD2 + "',";
                                //PRC_four
                                strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(sFourDT) + "',";
                                //PRC_Fournisseur
                                strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(sFourDT) + "',";
                                strInsert = strInsert + lCleFourDT + "";
                                strInsert = strInsert + ")";
                                i = i + 1;
                                General.Execute(strInsert);
                            }
                            else
                            {
                                foreach (DataRow rsG7Row in rsG7.Rows)
                                {

                                    //== recherche si le fournisseur gecet a une correspondance
                                    //== avec le fournisseur de la base de prod.
                                    modAdorsFindFourn = new ModAdo();
                                    rsFindFourn = modAdorsFindFourn.fc_OpenRecordSet("SELECT Code, cleAuto From fournisseurArticle " + " WHERE CodeFournGecet = '" +
                                        StdSQLchaine.gFr_DoublerQuote(rsG7Row["Fournisseur"].ToString()) + "'");

                                    sFourDT = "";

                                    if (rsFindFourn.Rows.Count > 0)
                                    {
                                        sFourDT = General.nz(rsFindFourn.Rows[0]["Code"], "").ToString();
                                        lCleFourDT = Convert.ToInt32(General.nz(rsFindFourn.Rows[0]["cleAuto"], ""));
                                    }
                                    //else if (string.IsNullOrEmpty(General.nz(rsG7Row["Fournisseur"], "").ToString()))
                                    //{
                                    //    sFourDT = General.sDefautFournisseur;
                                    //    using (var tmpModAdo = new ModAdo())
                                    //        lCleFourDT = Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Cleauto FROM fournisseurArticle" + " WHERE CODE ='" + sFourDT + "'"), 0));
                                    //}
                                    //'=== modif du 16 05 2019, si le fournisseur n'existe pas dans la table Fournssieur article(Axeciel),
                                    //'=== le fournisseur du Gecet est pris par defaut
                                    else
                                    {
                                        using (var tmpModAdo = new ModAdo())
                                            lCleFourDT = Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Cleauto FROM fournisseurArticle" + " WHERE CODE ='" + StdSQLchaine.gFr_DoublerQuote(sFourDT) + "'"), 0));
                                        if (string.IsNullOrEmpty(General.nz(rsG7Row["Fournisseur"], "").ToString()))
                                        {
                                            sFourDT = General.sDefautFournisseur;
                                        }
                                        else
                                        {
                                            //sFourDT = General.Left(General.sDefautFournisseur + " - a créer " + rsG7Row["ARTP_Nomfourn"] + "", 50);
                                            //sFourDT = General.Left(General.nz(rsG7Row["Fournisseur"], "").ToString() + "  - à créer dans l'intranet" + General.nz(rsG7Row["ARTP_Nomfourn"], "").ToString(), 50);
                                            if (General.sGecetV3 == "1")
                                            {
                                                sFourDT = General.Left(General.nz(rsG7Row["Fournisseur"], "").ToString() + "  - à créer dans l'intranet" + General.nz(rsG7Row["NOM_FOURNISSEUR"], "").ToString(), 50);
                                            }
                                            else
                                            {
                                                sFourDT = General.Left(General.nz(rsG7Row["Fournisseur"], "").ToString() + "  - à créer dans l'intranet" + General.nz(rsG7Row["ARTP_Nomfourn"], "").ToString(), 50);
                                            }


                                        }
                                    }

                                    strInsert = "INSERT INTO PRC_PreCmdCorps (";
                                    strInsert = strInsert + "PRE_NoAuto";
                                    strInsert = strInsert + ",PRC_References";
                                    strInsert = strInsert + ",PRC_Designation";
                                    strInsert = strInsert + ",PRC_Quantite";
                                    strInsert = strInsert + ",PRC_PrixHT";
                                    strInsert = strInsert + ",PRC_TotHT";
                                    strInsert = strInsert + ",PRC_Unite";
                                    strInsert = strInsert + ",PRC_NoLigne";
                                    strInsert = strInsert + ",PRC_Fournisseur";
                                    strInsert = strInsert + ",PRC_Chrono";
                                    strInsert = strInsert + ",NoAutoGecet";
                                    strInsert = strInsert + ",PRC_fabref";
                                    strInsert = strInsert + ",PRC_fourref";
                                    strInsert = strInsert + ",PRC_FournisseurRef";
                                    if (!string.IsNullOrEmpty(sFourDT))
                                    {
                                        strInsert = strInsert + ",PRC_FournisseurDT";
                                        strInsert = strInsert + ",PRC_FournDTCle";
                                    }

                                    strInsert = strInsert + ") VALUES (";

                                    strInsert = strInsert + NoBcmd + ",";
                                    strInsert = strInsert + "'" + General.nz(StdSQLchaine.gFr_DoublerQuote(rsG7Row["fourref"].ToString()), StdSQLchaine.gFr_DoublerQuote(rsBcmdRow["CodeSousArticle"].ToString())) + "',";
                                    strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(rsBcmdRow["TexteLigne"].ToString()) + "',";
                                    strInsert = strInsert + "" + dbQte + ",";
                                    if (rsG7Row["Prix"] == DBNull.Value || string.IsNullOrEmpty(rsG7Row["Prix"].ToString()))
                                    {
                                        strInsert = strInsert + "" + General.nz(rsBcmdRow["Foud"], "0").ToString().Replace(",", ".") + ",";
                                        strInsert = strInsert + Math.Round(dbQte * Convert.ToDouble(General.nz(rsBcmdRow["Foud"], "0")), 2).ToString().Replace(",", ".") + ",";
                                    }
                                    else
                                    {
                                        strPrix = General.fc_FormatNumber(rsG7Row["Prix"] + "");
                                        strInsert = strInsert + "" + strPrix + ",";

                                        if (!General.IsNumeric(strPrix))
                                            strPrix = Convert.ToString(0);

                                        strInsert = strInsert + Math.Round(dbQte * Convert.ToDouble(strPrix), 2) + ",";
                                    }

                                    strInsert = strInsert + "" + General.nz(rsG7Row["Qte_No"], "0") + ",";
                                    strInsert = strInsert + i + ", ";
                                    strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(rsG7Row["Fournisseur"].ToString()) + "" + "',";
                                    strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(rsBcmdRow["CodeSousArticle"].ToString()) + "',";
                                    strInsert = strInsert + "'" + rsG7Row["Noauto"] + "',";
                                    strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(rsG7Row["fabref"].ToString()) + "',";
                                    strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(rsG7Row["fourref"].ToString()) + "',";
                                    if (!string.IsNullOrEmpty(sFourDT))
                                    {
                                        strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(sFourDT) + "',";
                                        strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(sFourDT) + "',";
                                        strInsert = strInsert + lCleFourDT + "";
                                    }
                                    else
                                    {
                                        strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(rsG7Row["Fournisseur"].ToString()) + "'";
                                    }

                                    strInsert = strInsert + ")";
                                    var xxx = General.Execute(strInsert);

                                    i = i + 1;
                                }
                            }




                        }
                    }
                }

                modAdorsBcmd?.Dispose();
                modAdorsBcmd?.Dispose();
                modAdorsFindFourn?.Dispose();
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " fc_Bcmd ");
                return functionReturnValue;
            }
        }

        public object fc_Bcmd()
        {
            object functionReturnValue = null;
            string strInsert = null;
            string strRequete = null;
            DataTable rsBcmd = default(DataTable);
            ModAdo modAdorsBcmd = null;
            int i = 0;
            string NoBcmd = null;
            string strPrix = null;

            try
            {
                modAdorsBcmd = new ModAdo();

                //Si un bon de commande existe alors on sort
                strRequete = "SELECT  NoBonDeCommande FROM BonDeCommande WHERE NumeroDevis='" + txtNoDevis.Text + "'";
                rsBcmd = modAdorsBcmd.fc_OpenRecordSet(strRequete);
                if (rsBcmd.Rows.Count == 0)
                {
                    modAdorsBcmd?.Dispose();
                    return functionReturnValue;
                }

                modAdorsBcmd?.Dispose();

                strInsert = "INSERT INTO BonDeCommande (";
                strInsert = strInsert + "Acheteur";
                strInsert = strInsert + ",CodeImmeuble";
                strInsert = strInsert + ",Commentaire1";
                strInsert = strInsert + ",DateCommande";
                strInsert = strInsert + ",Fournisseur";
                strInsert = strInsert + ",NoIntervention";
                strInsert = strInsert + ",NumeroDevis";
                strInsert = strInsert + ",NumFicheStandard";
                strInsert = strInsert + ",CodeFourn";
                strInsert = strInsert + ",CodeE";
                strInsert = strInsert + ",Verrou";
                strInsert = strInsert + ",Ordre";
                strInsert = strInsert + ",EtatLivraison";
                strInsert = strInsert + ",EtatFacturation";
                strInsert = strInsert + ",Technicien";
                strInsert = strInsert + ",Estimation";
                strInsert = strInsert + ",Priorite";
                strInsert = strInsert + ") VALUES (";
                using (var tmpModAdo = new ModAdo())
                {
                    strInsert = strInsert + "'" + tmpModAdo.fc_ADOlibelle("SELECT Personnel.Initiales FROM Personnel INNER JOIN DevisEntete ON Personnel.Matricule=DevisEntete.CodeDeviseur WHERE DevisEntete.NumeroDevis='" + txtNoDevis.Text + "'") + "',";
                    strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "',";
                    strInsert = strInsert + "'Suite Devis N°: " + txtNoDevis.Text + " :" + "\n" + "";
                    strInsert = strInsert + StdSQLchaine.gFr_DoublerQuote(tmpModAdo.fc_ADOlibelle("SELECT TitreDevis FROM DevisEntete WHERE NumeroDevis='" + txtNoDevis.Text + "'")) + "',";
                    strInsert = strInsert + "'" + DateTime.Today + "',";
                    strInsert = strInsert + "'" + General.nz(tmpModAdo.fc_ADOlibelle("SELECT FournisseurArticle.Nom FROM FournisseurArticle WHERE RaisonSocial LIKE '%" + StdSQLchaine.gFr_DoublerQuote(General.strFournisseurInterne) + "%'"), StdSQLchaine.gFr_DoublerQuote(General.strFournisseurInterne)) + "',";
                    strInsert = strInsert + tmpModAdo.fc_ADOlibelle("SELECT TOP 1 Intervention.NoIntervention FROM Intervention WHERE NumFicheStandard=" + txtNumFicheStandard.Text + " ORDER BY NoIntervention") + ",";
                    strInsert = strInsert + "'" + txtNoDevis.Text + "',";
                    strInsert = strInsert + txtNumFicheStandard.Text + ",";
                    strInsert = strInsert + General.nz(tmpModAdo.fc_ADOlibelle("SELECT CleAuto FROM FournisseurArticle WHERE Nom LIKE '%" + StdSQLchaine.gFr_DoublerQuote(General.strFournisseurInterne) + "%'"), "''") + ",";
                }
                //CodeFourn
                strInsert = strInsert + "'CA',";
                //Code Etat
                strInsert = strInsert + "0,";
                //Verrou
                strInsert = strInsert + "1,";
                //Ordre
                strInsert = strInsert + "'NL',";
                //Etat Livraison
                strInsert = strInsert + "'NF',";
                //EtatFacturation
                strInsert = strInsert + "'" + "000" + "',";
                //Technicien : 000 en attente d'affectation
                strInsert = strInsert + "0,";
                //Estimation
                strInsert = strInsert + "0";
                //Priorité
                strInsert = strInsert + ")";

                General.Execute(strInsert);

                modAdorsBcmd = new ModAdo();
                rsBcmd = modAdorsBcmd.fc_OpenRecordSet("SELECT BonDeCommande.NoBonDeCommande FROM BonDeCommande WHERE NumeroDevis='" + txtNoDevis.Text + "'");
                if (rsBcmd.Rows.Count > 0)
                {
                    NoBcmd = rsBcmd.Rows[0]["NoBonDeCommande"] + "";
                }
                modAdorsBcmd?.Dispose();

                strRequete = "SELECT DevisDetail.CodeSousArticle,DevisDetail.TexteLigne,";
                strRequete = strRequete + "DevisDetail.Quantite,DevisDetail.Foud,";
                strRequete = strRequete + "BaseGecet.dbo.DtiPrix.FouUnite,";
                strRequete = strRequete + "BaseGecet.dbo.DtiPrix.Fournisseur,";
                strRequete = strRequete + "BaseGecet.dbo.DtiPrix.Fourref, ";
                strRequete = strRequete + "BaseGecet.dbo.DtiPrix.[prix net achat] as Prix, ";
                strRequete = strRequete + "QTE_Unite.Qte_No ";
                strRequete = strRequete + "FROM DevisDetail LEFT JOIN BaseGecet.dbo.DtiPrix ON ";
                strRequete = strRequete + "DevisDetail.CodeSousArticle=BaseGecet.dbo.DtiPrix.Chrono ";
                strRequete = strRequete + " LEFT JOIN QTE_Unite ON BaseGecet.dbo.DtiPrix.FouUnite=QTE_Unite.QTE_Libelle ";
                strRequete = strRequete + " WHERE DevisDetail.NumeroDevis='" + txtNoDevis.Text + "' ";
                strRequete = strRequete + " AND NOT DevisDetail.CodeSousArticle IS NULL ";
                strRequete = strRequete + " AND DevisDetail.CodeSousArticle<>'' ";
                strRequete = strRequete + " ORDER BY DevisDetail.NumeroLigne";
                rsBcmd = modAdorsBcmd.fc_OpenRecordSet(strRequete);
                i = 1;
                if (rsBcmd.Rows.Count > 0)
                {
                    foreach (DataRow rsBcmdRow in rsBcmd.Rows)
                    {
                        strInsert = "INSERT INTO BCD_DETAIL (";
                        strInsert = strInsert + "BCD_Cle";
                        strInsert = strInsert + ",BCD_References";
                        strInsert = strInsert + ",BCD_Designation";
                        strInsert = strInsert + ",BCD_Quantite";
                        strInsert = strInsert + ",BCD_PrixHT";
                        strInsert = strInsert + ",BCD_Unite";
                        strInsert = strInsert + ",BCD_NoLigne";
                        strInsert = strInsert + ",BCD_Fournisseur";
                        strInsert = strInsert + ") VALUES (";
                        strInsert = strInsert + NoBcmd + ",";
                        strInsert = strInsert + "'" + General.nz(rsBcmdRow["four"], rsBcmdRow["CodeSousArticle"]) + "" + "',";
                        strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(rsBcmdRow["TexteLigne"].ToString()) + "',";
                        strInsert = strInsert + "" + General.nz(rsBcmdRow["Quantite"].ToString(), "0") + ",";
                        if (rsBcmdRow["Prix"] == DBNull.Value || string.IsNullOrEmpty(rsBcmdRow["Prix"].ToString()))
                        {
                            strInsert = strInsert + "" + General.nz(rsBcmdRow["Foud"], "0") + ",";
                        }
                        else
                        {
                            strPrix = General.fc_FormatNumber(rsBcmdRow["Prix"] + "");
                            strInsert = strInsert + "" + strPrix + ",";
                        }
                        strInsert = strInsert + "" + General.nz(rsBcmdRow["Qte_No"], "0") + ",";
                        strInsert = strInsert + i + ", ";
                        strInsert = strInsert + "'" + StdSQLchaine.gFr_DoublerQuote(rsBcmdRow["Fournisseur"].ToString()) + "" + "')";
                        General.Execute(strInsert);
                        i = i + 1;
                    }
                }
                modAdorsBcmd?.Dispose();
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " fc_Bcmd ");
                return functionReturnValue;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCommande"></param>
        private void fc_DeBloqueForm(string sCommande = "")
        {
            try
            {
                var _with22 = this;
                _with22.txtCodeImmeuble.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cBleu);
                _with22.txtRaisonSocial_IMM.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cBleu);
                _with22.txtCodeImmeuble.ReadOnly = true;
                _with22.txtRaisonSocial_IMM.ReadOnly = true;
                _with22.cmdAjouter.Enabled = true;
                _with22.CmdRechercher.Enabled = true;
                _with22.cmdSupprimer.Enabled = true;
                _with22.CmdSauver.Enabled = true;
                _with22.cmdRechercheImmeuble.Enabled = false;
                _with22.cmdRechercheRaisonSocial.Enabled = false;
                //desactive le mode modeAjout
                blnAjout = false;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_DeBloqueForm");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_clear()
        {
            try
            {
                //ME.txtCodeAction = nz(rstmp!CodeAction, "")
                this.txtCodeImmeuble.Text = "";
                this.cmbCAI_CategorInterv.Text = "";
                this.lbllibCAI_CategorInterv.Text = "";
                this.txtSujet.Text = "";
                this.txtCodeOriCommentDevis.Text = "15";
                this.txtDocumentDevis.Text = "";
                this.txtNoDevis.Text = "";
                this.txtDateDevis.Text = "";
                this.txtQuiDevis.Text = "";
                this.txtTexte.Text = "";
                this.ChkDmdeDevis.CheckState = System.Windows.Forms.CheckState.Unchecked;
                this.ChkDevisAcc.CheckState = System.Windows.Forms.CheckState.Unchecked;
                this.cmdQuiDevis.Visible = true;
                this.lblComDevis.Text = "";
                //ME.txtDdeDevisObs = nz(rstmp!DdeDevisObs, "")

                this.txtCodeContrat.Text = "";
                this.txtcontrat.Text = "";

                this.txtCodeOrigine1.Text = "";
                this.txtCodeOrigine2.Text = "";
                this.txtCodeOrigine3_TO3.Text = "";
                this.txtCodeParticulier.Text = "";
                this.txtDateAjout.Text = "";

                this.txtDocument.Text = "";
                this.cmbIntervenantDT.Text = "";
                this.txtIntervention.Text = "";
                this.txtNumFicheStandard.Text = "";
                this.txtObservations.Text = "";
                this.txtSociete.Text = "";
                this.txtSource.Text = "";

                this.txtUtilisateur.Text = "";
                this.txtAdresse.Text = "";
                this.txtAdresse2_IMM.Text = "";
                this.txtCodePostal.Text = "";
                this.txtVille.Text = "";
                this.txtObservations.Text = "";
                this.txtObservation.Text = "";
                this.txtObservationComm_IMM.Text = "";
                this.txtObs.Text = "";
                this.txtRaisonSocial_IMM.Text = "";
                this.txtAdresse.Text = "";
                this.txtAdresse2_IMM.Text = "";
                this.txtCodePostal.Text = "";
                this.txtVille.Text = "";
                this.txtObservations.Text = "";
                this.txtObservationComm_IMM.Text = "";
                this.txtCode1.Text = "";
                this.txtCodeAcces1.Text = "";
                this.txtCodeAcces2.Text = "";
                this.lblCommercial.Text = "";
                this.lblConseilSyndical.Text = "";
                this.txtGardien.Text = "";
                this.CHKP1.CheckState = System.Windows.Forms.CheckState.Unchecked;
                this.CHKP2.CheckState = System.Windows.Forms.CheckState.Unchecked;
                this.CHKP3.CheckState = System.Windows.Forms.CheckState.Unchecked;
                this.CHKP4.CheckState = System.Windows.Forms.CheckState.Unchecked;
                lblRenovation.Visible = false;

                // met les valeurs par défaut des boutons options
                fc_CodeOrigineQui("");
                // met les valeurs par défaut des boutons options
                fc_origine("");
                // met les valeurs par défaut des boutons options
                fc_Motif("");
                blnContrat = false;

                txtRefOS.Text = "";
                txtDateOS.Value = "";
                txtCheminOS.Text = "";
                OptOSori.Checked = false;
                OptOSfichier.Checked = false;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_clear");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_CodeOrigineQui(string sCode)
        {
            try
            {
                var _with17 = this;
                _with17.cmbIntervenantDT.Text = "";
                _with17.lblLibIntervenantDT.Text = "";
                _with17.cmbIntervenantDT.Visible = false;
                _with17.lblLibIntervenantDT.Visible = false;
                _with17.lblSource.Visible = false;
                _with17.lblSociete.Visible = false;
                _with17.txtSociete.Visible = false;
                _with17.txtSource.Visible = false;
                _with17.txtSociete.Text = "";
                _with17.txtSource.Text = "";
                _with17.txtCodeParticulier.Visible = false;
                _with17.lbllibCodeParticulier.Visible = false;
                _with17.lbllibCodeParticulier.Text = "";
                _with17.txtCodeParticulier.Text = "";
                _with17.lblParticulier.Visible = false;
                _with17.cmdRechercheFour.Visible = false;
                _with17.cmdEctituresCopro.Visible = false;
                txtNcompte.Visible = false;
                _with17.cmdAjoutgestion.Visible = false;

                if (sCode == Convert.ToString(General.cSociete))
                {
                    _with17.txtCodeOrigine2.Text = Convert.ToString(General.cSociete);
                    _with17.cmbIntervenantDT.Visible = true;
                    _with17.lblLibIntervenantDT.Visible = true;
                }
                else if (sCode == Convert.ToString(General.cGerant))
                {
                    _with17.txtCodeOrigine2.Text = Convert.ToString(General.cGerant);
                    _with17.lblSource.Visible = true;
                    _with17.lblSociete.Visible = true;
                    _with17.txtSociete.Visible = true;
                    _with17.txtSource.Visible = true;

                    //.txtSource = fc_ADOlibelle("SELECT Gestionnaire.NomGestion,Gestionnaire.CodeGestinnaire,Immeuble.CodeGestionnaire FROM Gestionnaire INNER JOIN Immeuble ON Gestionnaire.CodeGestinnaire=Immeuble.CodeGestionnaire WHERE Gestionnaire.Code1='" & txtCode1.Text & "'")
                    using (var tmpModAdo = new ModAdo())
                        txtSource.Text = tmpModAdo.fc_ADOlibelle("SELECT Gestionnaire.NomGestion, " + " Gestionnaire.CodeGestinnaire, Immeuble.CodeGestionnaire" +
                            " FROM         Gestionnaire INNER JOIN" + " Immeuble ON Gestionnaire.CodeGestinnaire = Immeuble.CodeGestionnaire" + " WHERE     " +
                            "(Gestionnaire.Code1 = '" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "') " + " AND (Immeuble.CodeImmeuble = '" +
                            StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "')");
                    if (string.IsNullOrEmpty(txtSource.Text))
                    {

                        cmdAjoutgestion.Visible = true;

                    }

                    _with17.txtSociete.Text = txtCode1.Text;
                }
                else if (sCode == Convert.ToString(General.cGardien))
                {
                    _with17.txtCodeOrigine2.Text = Convert.ToString(General.cGardien);
                }
                else if (sCode == Convert.ToString(General.cDivers))
                {
                    _with17.txtCodeOrigine2.Text = Convert.ToString(General.cDivers);
                    _with17.lblSource.Visible = true;
                    _with17.lblSociete.Visible = true;
                    _with17.txtSociete.Visible = true;
                    _with17.txtSource.Visible = true;
                }
                else if (sCode == Convert.ToString(General.cCopro))
                {
                    _with17.txtCodeOrigine2.Text = Convert.ToString(General.cCopro);
                    _with17.txtCodeParticulier.Visible = true;
                    _with17.lbllibCodeParticulier.Visible = true;
                    _with17.lblParticulier.Visible = true;
                    _with17.cmdEctituresCopro.Visible = true;
                    txtNcompte.Visible = true;
                }
                else if (sCode == Convert.ToString(General.cFournisseur))
                {
                    _with17.txtCodeOrigine2.Text = Convert.ToString(General.cFournisseur);
                    _with17.lblSource.Visible = true;
                    _with17.lblSociete.Visible = true;
                    _with17.txtSociete.Visible = true;
                    _with17.txtSource.Visible = true;
                    _with17.cmdRechercheFour.Visible = true;
                }
                else
                {
                    _with17.optOriTelephone.Checked = false;
                    _with17.optOriTelephone.Checked = true;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_CodeOrigineQui");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_origine(string sCode)
        {
            try
            {
                // met en forme le frame origine
                var _with16 = this;
                _with16.file1.Visible = false;
                _with16.optOriFax.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cForeColorStandard);
                _with16.optOriFax.Font = new Font(_with16.optOriFax.Font.FontFamily, _with16.optOriFax.Font.Size, FontStyle.Regular);
                _with16.optOriCourrier.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cForeColorStandard);
                _with16.optOriCourrier.Font = new Font(_with16.optOriCourrier.Font.FontFamily, _with16.optOriCourrier.Font.Size, FontStyle.Regular);
                _with16.lblFaxouCourier.Text = "";
                _with16.cmdStocker.Visible = false;
                _with16.optOriMail.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cForeColorStandard);
                _with16.optOriMail.Font = new Font(_with16.optOriMail.Font.FontFamily, _with16.optOriMail.Font.Size, FontStyle.Regular);

                if (sCode == Convert.ToString(General.cTel))
                {
                    _with16.txtCodeOrigine1.Text = Convert.ToString(General.cTel);
                }
                else if (sCode == Convert.ToString(General.cFax))
                {
                    _with16.txtCodeOrigine1.Text = Convert.ToString(General.cFax);
                    if (!string.IsNullOrEmpty(_with16.txtDocument.Text))
                    {

                        _with16.cmdStocker.Visible = false;
                        _with16.optOriFax.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cForeColorLien);
                        _with16.optOriFax.Font = new Font(_with16.optOriFax.Font.FontFamily, _with16.optOriFax.Font.Size, General.cUnderline ? FontStyle.Underline : FontStyle.Regular);
                        _with16.file1.Visible = false;
                    }
                    else
                    {
                        _with16.lblFaxouCourier.Text = cLibelleFax;
                        _with16.cmdStocker.Visible = true;
                        _with16.file1.Visible = true;
                        file1.Tag = General.CHEMINFAX;
                        General.RefreshListBox(file1);
                    }
                }
                else if (sCode == Convert.ToString(General.cInternet))
                {
                    _with16.txtCodeOrigine1.Text = Convert.ToString(General.cInternet);
                }
                else if (sCode == Convert.ToString(General.cRepondeur))
                {
                    _with16.txtCodeOrigine1.Text = Convert.ToString(General.cRepondeur);
                }
                else if (sCode == Convert.ToString(General.cCourrier))
                {
                    _with16.txtCodeOrigine1.Text = Convert.ToString(General.cCourrier);
                    if (!string.IsNullOrEmpty(_with16.txtDocument.Text))
                    {
                        _with16.cmdStocker.Visible = false;
                        _with16.file1.Visible = false;
                        _with16.optOriCourrier.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cForeColorLien);
                        _with16.optOriCourrier.Font = new Font(_with16.optOriCourrier.Font, General.cUnderline ? FontStyle.Underline : FontStyle.Regular);
                    }
                    else
                    {
                        _with16.lblFaxouCourier.Text = cLibelleCourier;
                        _with16.cmdStocker.Visible = true;
                        _with16.file1.Visible = true;
                        file1.Tag = General.CHEMINCOURRIER;
                        General.RefreshListBox(file1);
                    }
                }
                else if (sCode == Convert.ToString(General.cRadio))
                {
                    _with16.txtCodeOrigine1.Text = Convert.ToString(General.cRadio);
                }
                else if (sCode == Convert.ToString(General.cDirect))
                {
                    _with16.txtCodeOrigine1.Text = Convert.ToString(General.cDirect);
                }
                else if (sCode == Convert.ToString(General.CEmail))
                {
                    _with16.txtCodeOrigine1.Text = Convert.ToString(General.CEmail);
                    if (!string.IsNullOrEmpty(_with16.txtDocument.Text))
                    {

                        _with16.cmdStocker.Visible = false;
                        _with16.optOriMail.ForeColor = System.Drawing.ColorTranslator.FromOle(General.cForeColorLien);
                        _with16.optOriMail.Font = new Font(_with16.optOriMail.Font, General.cUnderline ? FontStyle.Underline : FontStyle.Regular);
                        _with16.file1.Visible = false;
                    }
                    else
                    {
                        _with16.lblFaxouCourier.Text = cLibelleMail;
                        _with16.cmdStocker.Visible = true;
                        _with16.file1.Visible = true;
                        if (Dossier.fc_ControleDossier(General.CHEMINMAIL) == false)
                        {
                            Dossier.fc_CreateDossier(General.CHEMINMAIL);
                        }
                        file1.Tag = General.CHEMINMAIL;
                        General.RefreshListBox(file1);
                    }
                }
                else if (sCode == Convert.ToString(General.CAstreinte))
                {
                    _with16.txtCodeOrigine1.Text = Convert.ToString(General.CAstreinte);
                }
                else
                {
                    _with16.optQuiSociete.Checked = false;
                    _with16.optQuiSociete.Checked = true;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_origine");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_Motif(string sCode)
        {
            try
            {
                var _with15 = this;
                if (sCode == Convert.ToString(cChauffage))
                {
                    _with15.txtCodeOrigine3_TO3.Text = Convert.ToString(cChauffage);
                }
                else if (sCode == Convert.ToString(cDepannage))
                {
                    _with15.txtCodeOrigine3_TO3.Text = Convert.ToString(cDepannage);
                }
                else if (sCode == Convert.ToString(cFuite))
                {
                    _with15.txtCodeOrigine3_TO3.Text = Convert.ToString(cFuite);
                }
                else if (sCode == Convert.ToString(cECS))
                {
                    _with15.txtCodeOrigine3_TO3.Text = Convert.ToString(cECS);
                }
                else if (sCode == Convert.ToString(cRadiateur))
                {
                    _with15.txtCodeOrigine3_TO3.Text = Convert.ToString(cRadiateur);
                }
                else
                {
                    _with15.optDepannage.Checked = true;
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_Motif");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjoutgestion_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtCodeImmeuble.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un immeuble.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            if (string.IsNullOrEmpty(txtCode1.Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez sélectionner un gérant.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            frmAjoutGestionnaire frmAjoutGestionnaire = new frmAjoutGestionnaire();
            frmAjoutGestionnaire.txtCode1.Text = txtCode1.Text;
            frmAjoutGestionnaire.txtCodeImmeuble.Text = txtCodeImmeuble.Text;
            frmAjoutGestionnaire.ShowDialog();
            fc_CodeOrigineQui(Convert.ToString(General.cGerant));
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAnnuler_Click(object sender, EventArgs e)
        {
            fc_annuler();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_annuler()
        {
            try
            {
                var _with3 = this;
                blModif = false;
                // a modifier
                if (blModif == true)
                {
                    switch (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous enregistrer vos modifications ?", "", MessageBoxButtons.YesNoCancel, MessageBoxIcon.Question))
                    {
                        case DialogResult.Yes:
                            fc_sauver();
                            break;

                        case DialogResult.No:
                            break;
                        // Vider devis en cours
                        default:
                            return;

                            //Reprendre saisie en cours
                            break;
                    }
                }
                //         fc_clear
                //         fc_BloqueForm
                //         cmdGoIntervention.BackColor = &H8080FF
                //         'cmdDevis(0).BackColor = &HC0C0C0
                //         cmdDevis(1).BackColor = &HC0C0C0
                //         'rend le lien vers les interventions invisible.
                //         lblGoIntervention.Enabled = False
                if (ModParametre.fc_RecupParam(this.Name) == true)
                {
                    // charge les enregistremants
                    fc_ChargeEnregistrement(ModParametre.sNewVar);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_annuler");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="Index"></param>
        private void RechLibelleQui(int Index)
        {

            try
            {
                cmdQuiDevis.Visible = false;
                lblComDevis.Text = "";
                cmdVisuDevis.Visible = false;
                if (Index == Convert.ToDecimal("0"))
                {
                    //téléphone
                    lblComDevis.Text = "Telephone";
                }
                else if (Index == Convert.ToDecimal("1"))
                {
                    //fax+
                    lblComDevis.Text = "Fax";
                    cmdVisuDevis.Visible = true;
                }
                else if (Index == Convert.ToDecimal("2"))
                {
                    //internet
                    lblComDevis.Text = "Internet";
                }
                else if (Index == Convert.ToDecimal("3"))
                {
                    //Répondeur
                    lblComDevis.Text = "Répondeur";
                }
                else if (Index == Convert.ToDecimal("4"))
                {
                    //Courrier
                    lblComDevis.Text = "Courrier";
                    cmdVisuDevis.Visible = true;
                }
                else if (Index == Convert.ToDecimal("5"))
                {
                    //Radio
                    lblComDevis.Text = "Radio";
                }
                else if (Index == Convert.ToDecimal("6"))
                {
                    //Direct
                    lblComDevis.Text = "Direct";
                }
                else
                {
                    cmdQuiDevis.Visible = true;
                    lblComDevis.Text = "";
                    txtCodeOriCommentDevis.Text = "";
                }
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";RechLibelleQui");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_ChargeEnregistrement(string sCode)
        {
            string sCodeImmeuble = null;
            DataTable rstmp = default(DataTable);
            ModAdo modAdorstmp = null;

            try
            {
                charge = true;
                txtSujet.Text = "";
                General.sSQL = "";
                General.sSQL = "select * from gestionstandard where NumFichestandard='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
                if ((rstmp != null))
                {
                    modAdorstmp?.Dispose();
                }
                modAdorstmp = new ModAdo();
                rstmp = modAdorstmp.fc_OpenRecordSet(General.sSQL);
                if (rstmp.Rows.Count > 0)
                {
                    this.txtNumFicheStandard.Text = General.nz(rstmp.Rows[0]["Numfichestandard"], "0.00").ToString();
                    this.txtDocument.Text = General.nz(rstmp.Rows[0]["Document"], "").ToString();
                    // met à jour les boutons d'options
                    fc_Option(General.nz(rstmp.Rows[0]["CodeOrigine1"], "").ToString());
                    // met à jour le frame qui
                    fc_Option(General.nz(rstmp.Rows[0]["CodeOrigine2"], "").ToString());
                    // met à jour le frame motif
                    fc_Option(General.nz(rstmp.Rows[0]["CodeOrigine3_TO3"], "").ToString());
                    //ME.txtCodeAction = nz(rstmp!CodeAction, "")
                    this.txtCodeImmeuble.Text = General.nz(rstmp.Rows[0]["CodeImmeuble"], "").ToString();
                    sCodeImmeuble = General.nz(rstmp.Rows[0]["CodeImmeuble"], "").ToString();
                    this.txtCodeOrigine1.Text = General.nz(rstmp.Rows[0]["CodeOrigine1"], "").ToString();
                    this.txtCodeOrigine2.Text = General.nz(rstmp.Rows[0]["CodeOrigine2"], "").ToString();
                    this.txtCodeOrigine3_TO3.Text = General.nz(rstmp.Rows[0]["CodeOrigine3_TO3"], "").ToString();
                    this.txtCodeParticulier.Text = General.nz(rstmp.Rows[0]["CodeParticulier"], "").ToString();
                    this.txtDateAjout.Text = General.nz(rstmp.Rows[0]["DateAjout"], "").ToString();

                    // Informations sur Devis
                    this.txtCodeOriCommentDevis.Text = General.nz(rstmp.Rows[0]["codeOriCommentDevis"], "99").ToString();
                    RechLibelleQui((Convert.ToInt32(General.nz(rstmp.Rows[0]["codeOriCommentDevis"], "99"))));
                    this.txtDateDevis.Text = rstmp.Rows[0]["DateDevis"] + "";
                    this.ChkDmdeDevis.CheckState = General.ConvertBool(General.nz(rstmp.Rows[0]["DdeDeDevis"], false).ToString()) == 1 ? CheckState.Checked : CheckState.Unchecked;
                    this.ChkDevisAcc.CheckState = General.ConvertBool(General.nz(rstmp.Rows[0]["DevAccep"], false).ToString()) == 1 ? CheckState.Checked : CheckState.Unchecked;

                    //=== modif du 11 septembre 2013, bloque le frame si devis
                    //=== aceppté
                    if ((int)ChkDevisAcc.CheckState == 1 || (int)ChkDmdeDevis.CheckState == 1)
                    {
                        frmDevis.Enabled = false;
                    }
                    else
                    {
                        frmDevis.Enabled = true;
                    }

                    this.txtDocumentDevis.Text = General.nz(rstmp.Rows[0]["DocumentDevis"], "").ToString();
                    this.txtNoDevis.Text = General.nz(rstmp.Rows[0]["NoDevis"], "").ToString();
                    if (rstmp.Rows[0]["NoDevis"] != DBNull.Value && !string.IsNullOrEmpty(rstmp.Rows[0]["NoDevis"].ToString()))
                    {
                        //cmdDevis(0).BackColor = &HC0FFC0
                        cmdDevis1.BackColor = Color.FromArgb(192, 255, 192);//System.Drawing.ColorTranslator.FromOle(0xc0ffc0);
                        cmdDevis1.ForeColor = Color.Black;
                    }
                    else
                    {
                        //cmdDevis(0).BackColor = &HC0C0C0
                        cmdDevis1.BackColor = Color.FromArgb(85, 115, 128);//System.Drawing.ColorTranslator.FromOle(0xc0c0c0);
                        cmdDevis1.ForeColor = Color.White;
                    }
                    this.txtQuiDevis.Text = General.nz(rstmp.Rows[0]["QuiDevis"], "").ToString();
                    this.txtTexte.Text = General.nz(rstmp.Rows[0]["DdeDevisObs"], "").ToString();

                    // Fin informations sur devis

                    if (!string.IsNullOrEmpty(rstmp.Rows[0]["DossierAppel"].ToString()) ||
                        !Convert.ToBoolean(rstmp.Rows[0]["DossierAppel"].ToString() == "1" || rstmp.Rows[0]["DossierAppel"].ToString() == "true" ? true : false))
                    {
                        cmdDossierIntervention.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0c0c0);
                    }
                    else
                    {
                        cmdDossierIntervention.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0ffc0);
                    }

                    //ME.txtTexteLibre = nz(rsTmp!TexteLibre, "")
                    this.cmbIntervenantDT.Text = General.nz(rstmp.Rows[0]["IntervenantDT"], "").ToString();
                    this.txtIntervention.Text = General.nz(rstmp.Rows[0]["Intervention"], "0").ToString();
                    //-si une intervention existe alors on affiche le lien intervention
                    using (var tmpModAdo = new ModAdo())
                        if (!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT Intervention.NoIntervention FROM Intervention WHERE Intervention.NumFicheStandard=" + txtNumFicheStandard.Text), "").ToString()))
                        {
                            //If txtIntervention.Text <> "0" Then
                            cmdGoIntervention.BackColor = cBackColorInter;
                            cmdGoIntervention.ForeColor = Color.Black;
                            //&HC0FFC0
                            lblGoIntervention.Enabled = true;
                            fc_LockDevisAc(true);
                        }
                        else
                        {
                            cmdGoIntervention.BackColor = System.Drawing.ColorTranslator.FromOle(0x8080ff);
                            cmdGoIntervention.ForeColor = Color.White;
                            lblGoIntervention.Enabled = false;
                            fc_LockDevisAc(false);
                        }


                    //=== modif du 11/01/2016, fiche GMAO.
                    using (var tmpModAdo = new ModAdo())
                        if (!string.IsNullOrEmpty(General.nz(tmpModAdo.fc_ADOlibelle("SELECT NumFicheStandard From COP_ContratP2 Where NumFicheStandard =" + txtNumFicheStandard.Text), "").ToString()))
                        {
                            cmdGMAO.BackColor = cBackColorInter;
                            cmdGMAO.ForeColor = Color.Black;
                            //&HC0FFC0
                        }
                        else
                        {
                            cmdGMAO.BackColor = Color.FromArgb(55, 84, 96);
                            cmdGMAO.ForeColor = Color.White;

                        }


                    this.txtObservations.Text = General.nz(rstmp.Rows[0]["Observations"], "").ToString();
                    this.txtSociete.Text = General.nz(rstmp.Rows[0]["Societe"], "").ToString();
                    this.txtSource.Text = General.nz(rstmp.Rows[0]["Source"], "").ToString();
                    this.txtUtilisateur.Text = General.nz(rstmp.Rows[0]["Utilisateur"], "").ToString();
                    this.cmbCAI_CategorInterv.Text = General.nz(rstmp.Rows[0]["CAI_Code"], "").ToString();
                    if (Convert.ToBoolean(General.nz(rstmp.Rows[0]["Messagerie"], false)) == true)
                    {
                        this.cmdMail.BackColor = Color.FromArgb(192, 255, 192);//System.Drawing.ColorTranslator.FromOle(0xc0ffc0);
                        this.cmdMail.ForeColor = Color.Black;
                    }
                    else
                    {
                        this.cmdMail.BackColor = Color.FromArgb(55, 84, 96); //System.Drawing.ColorTranslator.FromOle(0xc0c0c0);
                        this.cmdMail.ForeColor = Color.White;
                    }
                    if (!string.IsNullOrEmpty(cmbCAI_CategorInterv.Text))
                    {
                        cmbCAI_CategorInterv_Validating(cmbCAI_CategorInterv, new System.ComponentModel.CancelEventArgs(false));
                    }
                    else
                    {
                        this.lbllibCAI_CategorInterv.Text = "";
                    }
                    if (!string.IsNullOrEmpty(cmbIntervenantDT.Text))
                    {
                        //recherche le libelle de l'intervenant.
                        fc_libIntervenantDT(cmbIntervenantDT.Text);
                    }

                    if (!string.IsNullOrEmpty(txtCodeParticulier.Text))
                    {
                        //recherche le libelle du particulier.
                        fc_libParticulier(txtCodeParticulier.Text);
                    }
                    txtRefOS.Text = rstmp.Rows[0]["RefOS"] + "";

                    txtDateOS.Value = General.nz(rstmp.Rows[0]["DateOS"], DBNull.Value);
                    //===> Mondir le 22.01.2021, cocher le checkbox du datetime picker, en vb6 c'est automatiquement mais pas en C#, https://groupe-dt.mantishub.io/view.php?id=2222
                    if (rstmp.Rows[0]["DateOS"].IsDate())
                    {
                        (txtDateOS.ButtonsLeft[0] as StateEditorButton).Checked = true;
                    }
                    else
                    {
                        (txtDateOS.ButtonsLeft[0] as StateEditorButton).Checked = false;
                    }
                    //===> Fin Modif MPondr

                    Label19.ForeColor = ColorTranslator.FromOle(General.cForeColorStandard);
                    Label19.Font = new Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));

                    //Mondir 25.06.2020 : J'ai mis cette ligne avant la condtion car le else va exceuter l'evenement _CheckedChanged,
                    //trouvé par Fabiola, envoyé par mail
                    txtCheminOS.Text = rstmp.Rows[0]["CheminOS"] + "";

                    if (Convert.ToInt32(General.nz(rstmp.Rows[0]["FichierOSorigine"], 0)) == 1)
                    {
                        OptOSori.Checked = true;

                    }
                    else if (Convert.ToInt32(General.nz(rstmp.Rows[0]["FichierOSorigine"], 0)) == 2)
                    {
                        OptOSfichier.Checked = true;
                        Label19.ForeColor = ColorTranslator.FromOle(General.cForeColorLien);
                        Label19.Font = new Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    }
                    else
                    {
                        OptOSori.Checked = false;
                        OptOSfichier.Checked = false;
                    }

                    // recherche des informations sur l'immeuble
                    fc_RechercheImmeuble(sCodeImmeuble);

                    StockCmdQuiDevis();

                    // recherche des observations techniques sur l' immeuble.
                    General.sSQL = "";
                    General.sSQL = "SELECT TechReleve.Obs,TechReleve.CodeFluide,TypeFluide.IntituleFluide " + " From TechReleve LEFT JOIN TypeFluide ON TechReleve.CodeFluide=TypeFluide.CodeFluide " + " WHERE TechReleve.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";


                    modAdorstmp = new ModAdo();
                    rstmp = modAdorstmp.fc_OpenRecordSet(General.sSQL);
                    if (rstmp.Rows.Count > 0)
                    {
                        this.txtObs.Text = rstmp.Rows[0]["Obs"] + "";
                        this.Label25.Text = rstmp.Rows[0]["IntituleFluide"] + "";
                    }
                    modAdorstmp?.Dispose();

                    General.sSQL = "SELECT Intervention.* FROM Intervention WHERE Intervention.NumFicheStandard='" + txtNumFicheStandard.Text + "'";


                    modAdorstmp = new ModAdo();
                    rstmp = modAdorstmp.fc_OpenRecordSet(General.sSQL);
                    if (rstmp.Rows.Count > 0)
                    {
                        lblGoIntervention.Enabled = true;
                    }
                    else
                    {
                        lblGoIntervention.Enabled = false;
                    }
                    modAdorstmp?.Dispose();

                    fc_DeBloqueForm();
                    charge = false;
                    fc_Renovation();

                    //===> Mondir le 02.04.2021, https://groupe-dt.mantishub.io/view.php?id=2189
                    //===> Mondir le 16.04.2021, moved to txtCode1_TextChanged https://groupe-dt.mantishub.io/view.php?id=2189#c5882
                    //fc_ChargeSoldeClient();
                    //===> Fin Modif Mondir
                }
                else
                {
                    // initialise à blanc tous les champs
                    fc_clear();
                    fc_BloqueForm();
                    charge = false;
                    return;
                }
                blModif = true;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "fc_ChargeEnregistrement");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        private void fc_Option(string sCode)
        {
            // positionne les boutons options
            // en donnant la valeur true au bouton option on active l'événement click de celui ci,
            // dans cet événement une fonction est lancée, mais si ce bouton a déja la valeur true
            // l'événement ne sera pas activé, pour le forcer on initialise sa valeur à false.

            try
            {
                var _with14 = this;
                if (sCode == Convert.ToString(General.cCopro))
                {
                    _with14.optQuiCopro.Checked = false;
                    _with14.optQuiCopro.Checked = true;
                }
                else if (sCode == Convert.ToString(General.cGerant))
                {
                    _with14.optQuiGerant.Checked = false;
                    _with14.optQuiGerant.Checked = true;
                }
                else if (sCode == Convert.ToString(General.cFournisseur))
                {
                    _with14.optQuiFournisseur.Checked = false;
                    _with14.optQuiFournisseur.Checked = true;
                }
                else if (sCode == Convert.ToString(General.cGardien))
                {
                    _with14.optQuiGardien.Checked = false;
                    _with14.optQuiGardien.Checked = true;
                }
                else if (sCode == Convert.ToString(General.cDivers))
                {
                    _with14.optQuiAutre.Checked = false;
                    _with14.optQuiAutre.Checked = true;
                }
                else if (sCode == Convert.ToString(General.cSociete))
                {
                    _with14.optQuiSociete.Checked = false;
                    _with14.optQuiSociete.Checked = true;
                }
                else if (sCode == Convert.ToString(General.cTel))
                {
                    _with14.optOriTelephone.Checked = false;
                    _with14.optOriTelephone.Checked = true;
                }
                else if (sCode == Convert.ToString(General.cFax))
                {
                    _with14.optOriFax.Checked = false;
                    _with14.optOriFax.Checked = true;
                }
                else if (sCode == Convert.ToString(General.cInternet))
                {
                    _with14.optOriSiteWeb.Checked = false;
                    _with14.optOriSiteWeb.Checked = true;
                }
                else if (sCode == Convert.ToString(General.cRepondeur))
                {
                    _with14.optOriRepondeur.Checked = false;
                    _with14.optOriRepondeur.Checked = true;
                }
                else if (sCode == Convert.ToString(General.cCourrier))
                {
                    _with14.optOriCourrier.Checked = false;
                    _with14.optOriCourrier.Checked = true;
                }
                else if (sCode == Convert.ToString(General.cRadio))
                {
                    _with14.optOriRadio.Checked = false;
                    _with14.optOriRadio.Checked = true;
                }
                else if (sCode == Convert.ToString(General.cDirect))
                {
                    _with14.optOriDirect.Checked = false;
                    _with14.optOriDirect.Checked = true;
                }
                else if (sCode == Convert.ToString(General.CEmail))
                {
                    _with14.optOriMail.Checked = false;
                    _with14.optOriMail.Checked = true;
                }
                else if (sCode == Convert.ToString(General.CAstreinte))
                {
                    _with14.OptOriAstreinte.Checked = false;
                    _with14.OptOriAstreinte.Checked = true;
                }
                else if (sCode == Convert.ToString(cChauffage))
                {
                    _with14.optChauffage.Checked = false;
                    _with14.optChauffage.Checked = true;
                }
                else if (sCode == Convert.ToString(cDepannage))
                {
                    _with14.optDepannage.Checked = false;
                    _with14.optDepannage.Checked = true;
                }
                else if (sCode == Convert.ToString(cFuite))
                {
                    _with14.optFuite.Checked = false;
                    _with14.optFuite.Checked = true;
                }
                else if (sCode == Convert.ToString(cECS))
                {
                    _with14.optECS.Checked = false;
                    _with14.optECS.Checked = true;
                }
                else if (sCode == Convert.ToString(cRadiateur))
                {
                    _with14.optRadiateur.Checked = false;
                    _with14.optRadiateur.Checked = true;
                }
                else
                {
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + " fc_Option ");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        /// <returns></returns>
        private bool fc_libParticulier(string sCode)
        {
            bool functionReturnValue = false;

            try
            {
                if (!string.IsNullOrEmpty(sCode))
                {
                    General.sSQL = "";
                    General.sSQL = "SELECT  ImmeublePart.Nom, ImmeublePart.Ncompte" + " From ImmeublePart" +
                                   " WHERE ImmeublePart.CodeParticulier ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeParticulier.Text) + "'" +
                                   //===> Mondir le 20.11.2020, ajou de cette conditio pout corrigé le ticket https://groupe-dt.mantishub.io/view.php?id=2089
                                   $" AND ImmeublePart.CodeImmeuble = '{txtCodeImmeuble.Text}'";
                    //===> Fin Modif Mondir

                    General.modAdorstmp = new ModAdo();

                    General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);
                    if (General.rstmp.Rows.Count > 0)
                    {
                        lbllibCodeParticulier.Text = General.rstmp.Rows[0]["Nom"].ToString();
                        txtNcompte.Text = General.rstmp.Rows[0]["Ncompte"].ToString();
                        functionReturnValue = false;
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ce particulier n'éxiste pas, allez dans la fiche immeuble pour en créer un" + " nouveau.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        lbllibCodeParticulier.Text = "";
                        txtNcompte.Text = "";
                        functionReturnValue = true;
                    }
                }
                else
                {
                    lbllibCodeParticulier.Text = "";
                }
                return functionReturnValue;
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";fc_libParticulier");
                return functionReturnValue;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCode"></param>
        /// <param name="blRaisonSocial_IMM"></param>
        private void fc_RechercheImmeuble(string sCode, bool blRaisonSocial_IMM = false)
        {
            try
            {
                General.sSQL = "";
                // sSQl = "SELECT Immeuble.Adresse, Immeuble.Adresse2_IMM," _
                //& "Immeuble.CodePostal, Immeuble.Ville," _
                //& " Immeuble.Observations, Immeuble.ObservationComm_IMM," _
                //& " RaisonSocial_IMM, codeAcces1, CodeAcces2," _
                //& " code1 , Codeimmeuble, Immeuble.Gardien, Immeuble.CodeCommercial, " _
                //& " Immeuble.CS1 From Immeuble"

                //== modif du 10 fevrier 2005, ajout du responsable d'exploit.
                General.sSQL = "SELECT Immeuble.Adresse, Immeuble.Adresse2_Imm, Immeuble.CodePostal,"
                    + " Immeuble.Ville, Immeuble.Observations, Immeuble.ObservationComm_IMM, " +
                               " Immeuble.RaisonSocial_IMM, Immeuble.CodeAcces1, Immeuble.CodeAcces2,"
                               + " Immeuble.Code1, Immeuble.CodeImmeuble, Immeuble.Gardien," +
                               " Immeuble.CodeCommercial , Immeuble.CS1, Personnel.CRespExploit,"
                               + " immeuble.P1,immeuble.P2,immeuble.P3,immeuble.P4,Immeuble.CodeRespExploit "
                               + " FROM Immeuble LEFT OUTER JOIN" +
                               " Personnel ON Immeuble.CodeDepanneur = Personnel.Matricule";

                if (blRaisonSocial_IMM == false)
                {
                    General.sSQL = General.sSQL + " WHERE Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCode) + "'";
                }
                else
                {
                    General.sSQL = General.sSQL + " WHERE Immeuble.RaisonSocial_IMM like '" + StdSQLchaine.gFr_DoublerQuote(sCode) + "%'";

                }

                General.modAdorstmp = new ModAdo();
                General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);

                if (General.rstmp.Rows.Count == 1)
                {
                    this.txtCodeImmeuble.Text = General.nz(General.rstmp.Rows[0]["CodeImmeuble"], "").ToString();
                    this.txtAdresse.Text = General.nz(General.rstmp.Rows[0]["Adresse"], "").ToString();
                    this.txtAdresse2_IMM.Text = General.nz(General.rstmp.Rows[0]["Adresse2_Imm"], "").ToString();
                    this.txtCodePostal.Text = General.nz(General.rstmp.Rows[0]["CodePostal"], "").ToString();
                    this.txtVille.Text = General.nz(General.rstmp.Rows[0]["Ville"], "").ToString();
                    this.txtObservation.Text = General.nz(General.rstmp.Rows[0]["Observations"], "").ToString();
                    //            ME.txtObservationComm_IMM = nz(rstmp!ObservationComm_IMM, "")
                    this.txtCode1.Text = General.nz(General.rstmp.Rows[0]["Code1"], "").ToString();
                    this.txtRaisonSocial_IMM.Text = General.nz(General.rstmp.Rows[0]["RaisonSocial_IMM"], "").ToString();
                    this.txtCodeAcces1.Text = General.nz(General.rstmp.Rows[0]["codeAcces1"], "").ToString();
                    this.txtCodeAcces2.Text = General.nz(General.rstmp.Rows[0]["CodeAcces2"], "").ToString();
                    this.txtGardien.Text = General.nz(General.rstmp.Rows[0]["Gardien"], "").ToString();
                    this.CHKP1.CheckState = (General.nz(General.rstmp.Rows[0]["P1"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["P1"], "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked);
                    this.CHKP2.CheckState = (General.nz(General.rstmp.Rows[0]["P2"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["P2"], "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked);
                    this.CHKP3.CheckState = (General.nz(General.rstmp.Rows[0]["P3"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["P3"], "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked);
                    this.CHKP4.CheckState = (General.nz(General.rstmp.Rows[0]["P4"], "0").ToString() == "1" || General.nz(General.rstmp.Rows[0]["P4"], "0").ToString() == "True" ? CheckState.Checked : CheckState.Unchecked);


                    if (!string.IsNullOrEmpty(General.rstmp.Rows[0]["CodeCommercial"] + ""))
                    {
                        using (var tmpModAdo = new ModAdo())
                            this.lblCommercial.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + General.rstmp.Rows[0]["CodeCommercial"] + "" + "'");
                    }
                    else
                    {
                        using (var tmpModAdo = new ModAdo())
                            this.lblCommercial.Text = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel INNER JOIN Table1 ON Personnel.Matricule = Table1.commercial WHERE Table1.Code1='" + StdSQLchaine.gFr_DoublerQuote(General.rstmp.Rows[0]["Code1"].ToString()) + "" + "'");
                    }

                    if (General.sVersionImmParComm == "1")
                    {
                        if (!string.IsNullOrEmpty(General.nz(General.rstmp.Rows[0]["CodeRespExploit"], "").ToString()))
                        {
                            using (var tmpModAdo = new ModAdo())
                                lblRespExploit.Text = tmpModAdo.fc_ADOlibelle("SELECT Nom FROM Personnel" + " WHERE matricule='" + General.rstmp.Rows[0]["CodeRespExploit"] + "'");
                        }
                        else
                        {
                            lblRespExploit.Text = "";
                        }
                    }
                    else
                    {
                        if (!string.IsNullOrEmpty(General.nz(General.rstmp.Rows[0]["CRespExploit"], "").ToString()))
                        {
                            using (var tmpModAdo = new ModAdo())
                                lblRespExploit.Text = tmpModAdo.fc_ADOlibelle("SELECT Nom FROM Personnel" + " WHERE initiales='" + StdSQLchaine.gFr_DoublerQuote(General.rstmp.Rows[0]["CRespExploit"].ToString()) + "'");
                        }
                        else
                        {
                            lblRespExploit.Text = "";
                        }
                    }

                    if (!string.IsNullOrEmpty(General.nz(General.rstmp.Rows[0]["Code1"], "").ToString()))
                    {
                        using (var tmpModAdo = new ModAdo())
                            this.txtObservationComm_IMM.Text = tmpModAdo.fc_ADOlibelle("SELECT Observations FROM Table1 WHERE Code1='" + StdSQLchaine.gFr_DoublerQuote(General.rstmp.Rows[0]["Code1"].ToString()) + "" + "'");
                    }

                    //            frmHistorique.Left = Frame1.Left
                    //            frmHistorique.Width = ME.Width - Frame9.Width
                    //            frmHistorique.Top = Frame5.Top + txtcontrat.Top + txtcontrat.Height
                    //            frmHistorique.Show vbModal
                    General.modAdorstmp?.Dispose();

                    General.sSQL = "";
                    General.sSQL = "SELECT Nom_IMC FROM IMC_ImmCorrespondant WHERE CodeImmeuble_IMM='" + StdSQLchaine.gFr_DoublerQuote(sCode) + "' AND PresidentCS_IMC='-1'";

                    General.modAdorstmp = new ModAdo();

                    General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);

                    if (General.rstmp.Rows.Count > 0)
                    {
                        this.lblConseilSyndical.Text = General.rstmp.Rows[0]["nom_imc"] + "";
                    }
                    General.modAdorstmp?.Dispose();
                    if (!string.IsNullOrEmpty(sCode))
                    {
                        fc_RechercheContrat(sCode);
                    }

                }
                else
                {
                    //If rsTmp.RecordCount >= 1 Then
                    //   MsgBox "Le systeme a trouvé plusieurs enregistrements commençant par " & sCode & ". Allez dans la fiche de recherche pour affiner votre sélection.", vbExclamation, ""
                    //Else
                    //    MsgBox "Cet enregistrement n'éxiste pas.", vbExclamation, ""

                    // End If
                    if (blRaisonSocial_IMM == false)
                    {
                        this.txtRaisonSocial_IMM.Text = "";
                    }
                    this.txtAdresse.Text = "";
                    this.txtAdresse2_IMM.Text = "";
                    this.txtCodePostal.Text = "";
                    this.txtVille.Text = "";
                    this.txtObservations.Text = "";
                    this.txtObservationComm_IMM.Text = "";
                    this.txtCode1.Text = "";
                    this.txtCodeAcces1.Text = "";
                    this.txtCodeAcces2.Text = "";
                    this.txtGardien.Text = "";
                    this.lblCommercial.Text = "";
                    this.lblConseilSyndical.Text = "";
                    this.CHKP1.CheckState = CheckState.Unchecked;
                    this.CHKP2.CheckState = CheckState.Unchecked;
                    this.CHKP3.CheckState = CheckState.Unchecked;
                    this.CHKP4.CheckState = CheckState.Unchecked;
                }

                //=== modif du 19  10 2017.
                fc_AfficheGerant(sCode);
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + "fc_RechercheImmeuble");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        private void fc_RechercheContrat(string sCodeImmeuble)
        {
            try
            {
                string LenNumContrat = null;

                using (var tmpModAdo = new ModAdo())
                    LenNumContrat = tmpModAdo.fc_ADOlibelle("SELECT MAX(LEN(NUMCONTRAT)) AS MaxNum FROM Contrat WHERE CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'");

                General.sSQL = "";
                //General.sSQL = "SELECT MAX(Contrat.DateEffet) as MaxiDate,Contrat.NumContrat AS MaxiNum, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1 "
                //    + " From Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle ";
                //General.sSQL = General.sSQL + " WHERE Contrat.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "' AND Avenant=0";
                //General.sSQL = General.sSQL + " GROUP BY Contrat.NumContrat, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1";

                General.sSQL = "SELECT MAX(Contrat.DateEffet) as MaxiDate,Contrat.NumContrat AS MaxiNum, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1 , Contrat.Resiliee"
                        + " From Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle ";

                //General.sSQL = General.sSQL + " WHERE Contrat.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "' AND Avenant=0";
                //Mondir : The line above is commented by Mondir, to remove the condtion " AND Avenant=0 " tofix this https://groupe-dt.mantishub.io/view.php?id=1688
                General.sSQL = General.sSQL + " WHERE Contrat.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "' ";


                General.sSQL = General.sSQL + " GROUP BY Contrat.NumContrat, Contrat.LibelleCont1, Contrat.CodeArticle, FacArticle.Designation1, Contrat.Resiliee";
                // Ahmed: Recuperation du contrat la plus reçent pour resoudre le ticket 
                //General.sSQL = General.sSQL + " order by MaxiDate desc";
                //Mondir 01.04.2020 : Even we order by date wont solve the probleme coz there is some contract are Resiliee and there date is > , so i
                //                    added order by Resiliee ASC to show not Resiliee first
                //                    To fix : https://groupe-dt.mantishub.io/view.php?id=1749


                General.sSQL = General.sSQL + " order by Resiliee ASC, MaxiDate desc";
                General.modAdorstmp = new ModAdo();
                General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);
                if (General.rstmp.Rows.Count > 0)
                {
                    txtcontrat.Text = General.rstmp.Rows[0]["Designation1"] + "";
                    txtCodeContrat.Text = General.rstmp.Rows[0]["MaxiNum"] + "";
                    if (txtNumFicheStandard.Text == "")
                    {
                        if (Convert.ToBoolean(General.nz(General.rstmp.Rows[0]["Resiliee"], false)) == true)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention le contrat pour cet immeuble est résilié..", "Contrat résilié", MessageBoxButtons.OK, MessageBoxIcon.Information);

                        }
                        if (General.IsDate(General.rstmp.Rows[0]["MaxiDate"].ToString()))
                        {
                            if (Convert.ToDateTime(General.rstmp.Rows[0]["MaxiDate"]) > DateTime.Now && Convert.ToBoolean(General.nz(General.rstmp.Rows[0]["Resiliee"], false)) == true)
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention la date de prise d'effet du contrat commence le " + General.rstmp.Rows[0]["MaxiDate"] + ".", "Contrat résilié", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            }
                        }
                    }

                }
                else
                {
                    if (txtNumFicheStandard.Text == "")
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention cet immeuble n'a pas de contrat..", "Contrat ", MessageBoxButtons.OK, MessageBoxIcon.Information);

                    }
                    txtcontrat.Text = "";
                    txtCodeContrat.Text = "";
                }

                General.modAdorstmp?.Dispose();
                txtcontrat.ForeColor = ColorTranslator.FromOle(0xff0000);
                //Si j'ai un contrat, je vérifie qu'il n'est pas résilié
                if (!string.IsNullOrEmpty(txtCodeContrat.Text))
                {
                    General.sSQL = "SELECT Contrat.NumContrat,FacArticle.Designation1 FROM Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle " +
                                   "WHERE (Contrat.Resiliee=0 OR (Contrat.Resiliee=1 AND (Contrat.DateFin>'" + DateTime.Today + "' OR Contrat.DateFin IS NULL))) AND " +
                                   "Contrat.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "' order by avenant";
                    //sSQl = "SELECT Contrat.NumContrat,FacArticle.Designation1 FROM Contrat LEFT JOIN FacArticle ON Contrat.CodeArticle=FacArticle.CodeArticle WHERE (Contrat.Resiliee=0 OR (Contrat.Resiliee=1 AND (Contrat.DateFin>'" & Date & "' OR Contrat.DateFin IS NULL))) AND Contrat.CodeImmeuble='" & sCodeImmeuble & "' order by avenant"
                    General.modAdorstmp = new ModAdo();
                    General.rstmp = General.modAdorstmp.fc_OpenRecordSet(General.sSQL);
                    if (General.rstmp.Rows.Count > 0)
                    {
                        txtCodeContrat.Text = General.rstmp.Rows[0]["NumContrat"] + "";
                        txtcontrat.Text = General.rstmp.Rows[0]["Designation1"] + "";
                        blnContrat = true;
                        txtcontrat.ForeColor = ColorTranslator.FromOle(0xff0000);
                    }
                    else
                    {
                        blnContrat = false;
                        txtcontrat.ForeColor = ColorTranslator.FromOle(0xc0);
                    }
                }

                General.modAdorstmp?.Dispose();
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + "RechercheContrat");
            }


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        public object StockCmdQuiDevis()
        {

            string SqlDevis = null;
            DataTable rsTmpDevis = default(DataTable);
            ModAdo modAdorsTmpDevis = null;
            ssoleDbQuiDevis.DataSource = null;

            //ssoleDbQuiDevis.DataFieldList = "Numero"
            //ssoleDbQuiDevis.DataFieldToDisplay = "Numero"

            SqlDevis = "SELECT CodeGestinnaire,NomGestion FROM Gestionnaire WHERE Gestionnaire.Code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";

            modAdorsTmpDevis = new ModAdo();
            rsTmpDevis = modAdorsTmpDevis.fc_OpenRecordSet(SqlDevis);

            var ssoleDbQuiDevisSource = new DataTable();
            ssoleDbQuiDevisSource.Columns.Add("Numero");
            ssoleDbQuiDevisSource.Columns.Add("Nom");
            ssoleDbQuiDevisSource.Columns.Add("Prenom");

            if (rsTmpDevis.Rows.Count > 0)
            {
                foreach (DataRow rsTmpDevisRow in rsTmpDevis.Rows)
                    ssoleDbQuiDevisSource.Rows.Add(rsTmpDevisRow[0], rsTmpDevisRow[1]);
            }

            modAdorsTmpDevis?.Dispose();


            SqlDevis = "SELECT CodeParticulier,Nom FROM ImmeublePart WHERE ImmeublePart.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";

            modAdorsTmpDevis = new ModAdo();
            rsTmpDevis = modAdorsTmpDevis.fc_OpenRecordSet(SqlDevis);


            if (rsTmpDevis.Rows.Count > 0)
            {
                foreach (DataRow rsTmpDevisRow in rsTmpDevis.Rows)
                {
                    ssoleDbQuiDevisSource.Rows.Add(rsTmpDevisRow[0], rsTmpDevisRow[1]);
                }
            }

            modAdorsTmpDevis?.Dispose();

            SqlDevis = "SELECT Gardien FROM Immeuble WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";

            modAdorsTmpDevis = new ModAdo();
            rsTmpDevis = modAdorsTmpDevis.fc_OpenRecordSet(SqlDevis);


            if (rsTmpDevis.Rows.Count > 0)
            {
                foreach (DataRow rsTmpDevisRow in rsTmpDevis.Rows)
                {
                    ssoleDbQuiDevisSource.Rows.Add("", rsTmpDevisRow[0]);
                }
            }

            modAdorsTmpDevis?.Dispose();


            SqlDevis = "SELECT Matricule,Nom,Prenom FROM Personnel";

            modAdorsTmpDevis = new ModAdo();
            rsTmpDevis = modAdorsTmpDevis.fc_OpenRecordSet(SqlDevis);


            if (rsTmpDevis.Rows.Count > 0)
            {


                foreach (DataRow rsTmpDevisRow in rsTmpDevis.Rows)
                {
                    ssoleDbQuiDevisSource.Rows.Add(rsTmpDevisRow[0], rsTmpDevisRow[1], rsTmpDevisRow[2]);
                }
            }

            modAdorsTmpDevis?.Dispose();

            ssoleDbQuiDevis.DataSource = ssoleDbQuiDevisSource;

            return null;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        private void fc_AfficheGerant(string sCodeImmeuble)
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            try
            {
                sSQL = "SELECT     Gestionnaire.NomGestion, Qualification.Qualification" + " FROM         Gestionnaire INNER JOIN " + " Qualification ON Gestionnaire.CodeQualif_Qua = Qualification.CodeQualif INNER JOIN " + " Immeuble ON Gestionnaire.CodeGestinnaire = Immeuble.CodeGestionnaire AND Gestionnaire.Code1 = Immeuble.Code1" + " WHERE     Immeuble.CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'";

                modAdors = new ModAdo();
                rs = modAdors.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count > 0)
                {

                    lblGerant.Text = rs.Rows[0]["NomGestion"] + " - " + rs.Rows[0]["Qualification"];
                }
                else
                {
                    lblGerant.Text = "";
                }

                modAdors?.Dispose();
            }
            catch (Exception e)
            {
                Erreurs.gFr_debug(e, this.Name + ";");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Renovation()
        {
            string sSQL = null;

            lblRenovation.Visible = false;

            if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
            {
                sSQL = "SELECT NumeroDevis  From DevisEnTete " + " WHERE CodeImmeuble = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'" + " AND (RenovationChauff = 1) ";
                using (var tmpModAdo = new ModAdo())
                    sSQL = tmpModAdo.fc_ADOlibelle(sSQL);
                if (!string.IsNullOrEmpty(sSQL))
                {
                    lblRenovation.Visible = true;
                }

            }


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdContrat_Click(object sender, EventArgs e)
        {
            string strNodevis = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            DataTable rstmp = default(DataTable);
            ModAdo modAdorstmp = null;
            string sSQlx = null;
            string sDoc = null;

            try
            {
                //cmdMAJ_Click
                strNodevis = "";

                if (fc_sauver() == true)
                {
                    //       If sFicheContratV2 = "" Then
                    //===demande de devis.
                    if ((int)ChkDmdeDevisContrat.CheckState == 1)
                    {
                        modAdors = new ModAdo();
                        var _with4 = rs;
                        General.SQL = "SELECT Numerodevis FROM DevisEntete " + " WHERE NumFicheStandard=" + txtNumFicheStandard.Text + " order by numeroDevis desc";
                        _with4 = modAdors.fc_OpenRecordSet(General.SQL);
                        if (_with4.Rows.Count > 0)
                        {
                            strNodevis = _with4.Rows[0]["numerodevis"] + "";
                        }
                        else
                        {
                            strNodevis = "";
                        }
                        modAdors?.Dispose();
                        //prcLiaison PROGDEVIS, txtNumFicheStandard.Text, strNodevis
                        //====devis accepté.
                    }
                    else if ((int)ChkDevisAcc.CheckState == 1)
                    {
                        if (!string.IsNullOrEmpty(txtNoDevis.Text))
                        {
                            strNodevis = txtNoDevis.Text;
                            //prcLiaison PROGDEVIS, txtNumFicheStandard.Text, txtNoDevis
                        }
                        else
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Un Numero de devis est obligatoire.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtNoDevis.Focus();
                            return;
                        }
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le cadre reservé au devis n'est pas fournis.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    //= stocke le position de fiche d'appel
                    ModParametre.fc_SaveParamPosition(Variable.cUserDocStandard, this.Name, this.txtNumFicheStandard.Text);

                    //= stocke le numero de fiche d'appel pour positionner la fiche devis
                    ModParametre.fc_SaveParamPosition(Variable.cUserDocStandard, Variable.cUserDocDevis, txtNumFicheStandard.Text, txtCodeImmeuble.Text, strNodevis);

                    View.Theme.Theme.Navigate(typeof(UserDocDevis));

                    //        ElseIf sFicheContratV2 = "1" Then
                    //                    '=== modif du 30 11 2015, nouvelle fiche P2.
                    //                    '=== controle si ilexiste deja un contrat pour cet immeuble.
                    //                    sSQLx = "Select Codeimmeuble,NumFicheStandard FROM COP_ContratP2 where codeimmeuble='" & gFr_DoublerQuote(txtCodeimmeuble) & "'"
                    //
                    //                    Set rstmp = fc_OpenRecordSet(sSQLx)
                    //
                    //                    If rstmp.EOF = False Then
                    //                          If txtNumFichestandard <> nz(rstmp!numficheStandard, "") Then
                    //
                    //                                If MsgBox("Attention il existe déjà un contrat pour cet immeuble," _
                    //'                                  & " Voulez vous créer un nouveau contrat ?", vbYesNo + vbCritical, "Avertissement") = vbNo Then
                    //                                      rstmp.Close
                    //                                      Exit Sub
                    //                                End If
                    //
                    //                                If MsgBox("Confirmez vous la création d'un nouveau contrat pour cet immeuble ?" _
                    //'                                      & "", vbYesNo + vbCritical, "Création d'un nouveau contrat") = vbNo Then
                    //                                     ' rstmp.Close
                    //                                    Exit Sub
                    //                                End If
                    //                            End If
                    //                    End If
                    //                    rstmp.Close
                    //                    Set rs = Nothing
                    //                    '=== stocke le position de fiche d'appel
                    //                    fc_SaveParamPosition cUserDocStandard, Me.Name, Me.txtNumFichestandard
                    //
                    //                    '= stocke le numero de fiche d'appel pour positionner la fiche d'intervention
                    //                    fc_SaveParamPosition cUserDocStandard, cUserDocP2, txtNumFichestandard, _
                    //'                        txtCodeimmeuble, txtDateAjout, "", sDoc
                    //
                    //                    fc_Navigue Me, gsCheminPackage & PROGUserDocP2 ', , Me.Left
                    //        End If
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez d'abord valider la fiche d'appel.", "Navigation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdDevis_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdPremier_Click(object sender, EventArgs e)
        {
            string sCodeAppel = null;

            using (var tmpModAdo = new ModAdo())
                sCodeAppel = tmpModAdo.fc_ADOlibelle("SELECT TOP 1 GestionStandard.NumFicheStandard FROM GestionStandard ORDER BY GestionStandard.NumFicheStandard ASC");

            fc_ChargeEnregistrement(sCodeAppel);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDevis1_Click(object sender, EventArgs e)
        {
            string strNodevis = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            try
            {
                //cmdMAJ_Click
                strNodevis = "";

                if (fc_sauver() == true)
                {
                    //===demande de devis.
                    if ((int)ChkDmdeDevis.CheckState == 1)
                    {
                        rs = new DataTable();
                        var _with5 = rs;
                        General.SQL = "SELECT Numerodevis FROM DevisEntete " + " WHERE NumFicheStandard=" + txtNumFicheStandard.Text + " order by numeroDevis desc";
                        modAdors = new ModAdo();
                        _with5 = modAdors.fc_OpenRecordSet(General.SQL);
                        if (_with5.Rows.Count > 0)
                        {
                            strNodevis = _with5.Rows[0]["numerodevis"] + "";
                        }
                        else
                        {
                            strNodevis = "";
                        }
                        modAdors?.Dispose();
                        //prcLiaison PROGDEVIS, txtNumFicheStandard.Text, strNodevis
                        //====devis accepté.
                    }
                    else if ((int)ChkDevisAcc.CheckState == 1)
                    {
                        if (!string.IsNullOrEmpty(txtNoDevis.Text))
                        {
                            strNodevis = txtNoDevis.Text;
                            //prcLiaison PROGDEVIS, txtNumFicheStandard.Text, txtNoDevis
                        }
                        else
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Un Numero de devis est obligatoire.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtNoDevis.Focus();
                            return;
                        }
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le cadre reservé au devis n'est pas fournis.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                    //= stocke le position de fiche d'appel
                    ModParametre.fc_SaveParamPosition(Variable.cUserDocStandard, this.Name, this.txtNumFicheStandard.Text);

                    //= stocke le numero de fiche d'appel pour positionner la fiche devis
                    ModParametre.fc_SaveParamPosition(Variable.cUserDocStandard, Variable.cUserDocDevis, txtNumFicheStandard.Text, txtCodeImmeuble.Text, strNodevis);

                    View.Theme.Theme.Navigate(typeof(UserDocDevis));


                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez d'abord valider la fiche d'appel.", "Navigation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdDevis_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdDossierIntervention_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            OpenFrmDossierInterv();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sChemin"></param>
        /// <returns></returns>
        private object OpenFrmDossierInterv(string sChemin = "")
        {
            object functionReturnValue = null;
            try
            {
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.AppStarting;

                General.File1EnCours = General.getFrmReg("App", "File1EnCours", "");
                General.Drive1EnCours = General.getFrmReg("App", "Drive1EnCours", "");
                General.Dir1EnCours = General.getFrmReg("App", "Dir1EnCours", "");
                if (string.IsNullOrEmpty(sChemin))
                {
                    sChemin = General.DossierAppel + txtNumFicheStandard.Text;
                }

                if (!string.IsNullOrEmpty(txtNumFicheStandard.Text))
                {
                    if (Dossier.fc_ControleDossier(sChemin) == false)
                    {
                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Souhaitez-vous créer un dossier pour cette fiche d'appel ?", "Création d'un dossier d'intervention", MessageBoxButtons.YesNo, MessageBoxIcon.Question)
                            == DialogResult.Yes)
                        {
                            Dossier.fc_CreateDossier(sChemin);
                            cmdDossierIntervention.BackColor = System.Drawing.ColorTranslator.FromOle(0xc0ffc0);
                            int xx = General.Execute("UPDATE GestionStandard set GestionStandard.DossierAppel=1 WHERE GestionStandard.NumFicheStandard='" + txtNumFicheStandard.Text + "'");
                        }
                        else
                        {
                            return functionReturnValue;
                        }
                    }

                    frmDossierIntervention frmDossierIntervention = new frmDossierIntervention();

                    var _with6 = frmDossierIntervention;
                    //_with6.Drive1.DataSource = System.IO.DriveInfo.GetDrives();
                    try
                    {
                        //foreach (var drive in System.IO.DriveInfo.GetDrives())
                        //{
                        //    _with6.Drive1.Items.Add(drive.Name);
                        //}

                        if (string.IsNullOrEmpty(General.Drive1EnCours))
                        {
                            if (General._ExecutionMode == General.ExecutionMode.Dev)
                                _with6.Drive1.Drive = "C:\\";
                            else
                                _with6.Drive1.Drive = "W:\\";
                        }
                        else
                        {
                            _with6.Drive1.Drive = General.Drive1EnCours;
                        }
                    }
                    catch (Exception ex)
                    {
                        Program.SaveException(ex);
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Lecteur " + General.Drive1EnCours);
                        General.saveInReg("App", "Drive1EnCours", "");
                    }


                    try
                    {
                        if (string.IsNullOrEmpty(General.Dir1EnCours))
                        {
                            if (General._ExecutionMode == General.ExecutionMode.Dev)
                                _with6.Dir1.Path = "C:\\";
                            else
                                _with6.Dir1.Path = "W:\\";
                        }
                        else
                        {
                            _with6.Dir1.Path = General.Dir1EnCours;
                        }
                    }
                    catch (Exception ex)
                    {
                        Program.SaveException(ex);
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Dossier " + General.Dir1EnCours);
                        General.saveInReg("App", "Dir1EnCours", "");
                    }

                    //General.ListDirectory(_with6.Dir1111, _with6.Dir1.Tag.ToString());

                    try
                    {
                        if (string.IsNullOrEmpty(General.File1EnCours))
                        {
                            _with6.File1.Path = _with6.Dir1.Path;

                        }
                        else
                        {
                            _with6.File1.Path = General.File1EnCours;

                        }
                    }
                    catch (Exception ex)
                    {
                        Program.SaveException(ex);
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Dossier " + General.File1EnCours);
                        General.saveInReg("App", "File1EnCours", "");
                    }
                    _with6.DirNoInter.Tag = sChemin;
                    //General.FilesInDirectory(_with6.File1, new DirectoryInfo(_with6.File1.Tag.ToString()));

                    //General.ListDirectory(_with6.DirNoInter1111, Environment.GetFolderPath(Environment.SpecialFolder.Desktop));
                    //General.ListDirectory(_with6.DirNoInter, "D:\\");

                    _with6.FileIntervention.Path = sChemin;
                    _with6.File1.Pattern = "*.doc;*.xls;*.tif;*.pcx;*.htm;*.html;*.txt;*.bmp;*.gif;*.jpg;*.jpeg;*.pdf";
                    //General.FilesInDirectory(_with6.File1, new DirectoryInfo(_with6.FileIntervention.Tag.ToString()));

                    //General.FilesInDirectory(_with6.FileIntervention, new DirectoryInfo(_with6.FileIntervention.Tag.ToString()));

                    _with6.lblIntervention.Text = _with6.lblIntervention.Text + " la fiche d'appel N° : " + txtNumFicheStandard.Text;
                    _with6.Text = _with6.Text + " Appel";
                    _with6.ShowDialog();
                    //txtDossierIntervention.Text = "1"

                    frmDossierIntervention.Close();
                }
                return functionReturnValue;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, "cmdDossierIntervention_Click");
                return functionReturnValue;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdEcritures_Click(object sender, EventArgs e)
        {
            using (var tmpModAdo = new ModAdo())
                General.CompteEcriture = tmpModAdo.fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'");
            frmHistoEcritures frmHistoEcritures = new frmHistoEcritures();
            frmHistoEcritures.ShowDialog();
            General.CompteEcriture = "";
            frmHistoEcritures.Close();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdEctituresCopro_Click(object sender, EventArgs e)
        {
            using (var tmpModAdo = new ModAdo())
                General.CompteEcriture = tmpModAdo.fc_ADOlibelle("SELECT NCompte FROM ImmeublePart WHERE ImmeublePart.CodeParticulier='" + StdSQLchaine.gFr_DoublerQuote(txtCodeParticulier.Text) + "'");
            frmHistoEcritures frmHistoEcritures = new frmHistoEcritures();
            frmHistoEcritures.ShowDialog();
            General.CompteEcriture = "";
            frmHistoEcritures.Close();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFindIntervention_Click(object sender, EventArgs e)
        {
            try
            {
                string requete = "SELECT Intervention.NoIntervention as \"NoIntervention\"," + " Intervention.Designation as \"Designation\"," + " Intervention.Commentaire as \"Commentaire\"," +
                    " Intervention.CodeEtat as \"Code Etat\", Personnel.Nom as \"Nom\"," + " intervention.DateSaisie as \"Date de saisie\"" + " FROM Intervention LEFT OUTER JOIN " +
                    " Personnel ON Intervention.Intervenant = Personnel.Matricule";
                string where_order = " (Intervention.codeimmeuble = '" + txtCodeImmeuble.Text + " ') ";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Historique des intervention - " + txtCodeImmeuble.Text };
                //TODO : Mondir - Check This txtRafraichir
                //fg.txtRafraichir.Text = Convert.ToString(1);
                //fg.ugResultat.DoubleClickRow += (se, ev) =>
                //{
                //    iTalk_TextBox_Small21.Text = fg.ugResultat.ActiveRow.Cells["Code SousAdresse"].Value.ToString();
                //    fg.Dispose();
                //    fg.Close();
                //};

                //fg.ugResultat.KeyDown += (se, ev) =>
                //{

                //    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                //    {
                //        iTalk_TextBox_Small21.Text = fg.ugResultat.ActiveRow.Cells["Code SousAdresse"].Value.ToString();
                //        fg.Dispose();
                //        fg.Close();
                //    }
                //};
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdrecherche_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdGestionMotif_Click(object sender, EventArgs e)
        {
            frmMotifs frmMotifs = new frmMotifs();
            frmMotifs.ShowDialog();
            if (!string.IsNullOrEmpty(frmMotifs.txtCodeMotif.Text))
            {
                cmbCAI_CategorInterv.Value = frmMotifs.SSOleDBGrid1.ActiveRow.Cells["CodeArticle"].Value;
                cmbCAI_CategorInterv_Validating(cmbCAI_CategorInterv, new System.ComponentModel.CancelEventArgs(false));
            }
            frmMotifs.Close();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdGMAO_Click(System.Object eventSender, System.EventArgs eventArgs)
        {

            string sSQlx = null;
            DataTable rs = default(DataTable);

            string sDoc = null;

            try
            {
                if (General.sFicheContratV2 == "1")
                {
                    //=== modif du 30 11 2015, nouvelle fiche P2.
                    //=== controle si ilexiste deja un contrat pour cet immeuble.
                    sSQlx = "Select Codeimmeuble,NumFicheStandard FROM COP_ContratP2 where codeimmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";

                    General.modAdorstmp = new ModAdo();
                    General.rstmp = General.modAdorstmp.fc_OpenRecordSet(sSQlx);

                    if (General.rstmp.Rows.Count > 0)
                    {
                        if (txtNumFicheStandard.Text != General.nz(General.rstmp.Rows[0]["Numfichestandard"], "").ToString())
                        {

                            //If MsgBox("Attention il existe déjà une fiche GMAO pour cet immeuble," _
                            //'  & " Voulez vous créer un nouveau contrat ?", vbYesNo + vbCritical, "Avertissement") = vbNo Then
                            //      rstmp.Close
                            //      Exit Sub
                            //End If

                            //If MsgBox("Confirmez vous la création d'un nouveau contrat pour cet immeuble ?" _
                            //'      & "", vbYesNo + vbCritical, "Création d'un nouveau contrat") = vbNo Then
                            //     ' rstmp.Close
                            //    Exit Sub
                            //End If

                            //=== modif du 13 06 2016,on ne peur creer qu une seule fiche pâr immeuble.
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention il existe déjà une fiche GMAO pour cet immeuble (fiche appel n°" +
                                General.rstmp.Rows[0]["Numfichestandard"] + ")," + "\n" + " Vous ne pouvez pas créer deux fiches GMAO pour un même immeuble", "Avertissement", MessageBoxButtons.OK, MessageBoxIcon.Information);

                            General.modAdorstmp.Close();
                            return;

                        }
                    }
                    General.modAdorstmp?.Close();

                    //=== controler si cette fiche d'appel est lié à une intervention.
                    if (cmdGoIntervention.BackColor == cBackColorInter)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas créer de fiche GMAO sur une fiche d'appel liée à une intervention.");
                        return;
                    }

                    //=== stocke le position de fiche d'appel
                    ModParametre.fc_SaveParamPosition(Variable.cUserDocStandard, this.Name, this.txtNumFicheStandard.Text);

                    //= stocke le numero de fiche d'appel pour positionner la fiche d'intervention
                    ModParametre.fc_SaveParamPosition(Variable.cUserDocStandard, Variable.cUserDocP2, txtNumFicheStandard.Text, txtCodeImmeuble.Text, txtDateAjout.Text, "", sDoc);

                    View.Theme.Theme.Navigate(typeof(UserDocP2V2));


                    //, , Me.Left
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdGMAO_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdGoIntervention_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            string sParticulier = null;
            string sDocument = null;
            string sDoc = null;

            try
            {
                if (!string.IsNullOrEmpty(txtNumFicheStandard.Text))
                {
                    if (!string.IsNullOrEmpty(txtCodeContrat.Text) && blnContrat == false && (string.IsNullOrEmpty(txtIntervention.Text) || txtIntervention.Text == "0"))
                    {
                        if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention le contrat de cet immeuble a été résilié." + "\n" + "Souhaitez-vous créer cette intervention quand même ?", "Intervention sans contrat", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                        {
                            return;
                        }
                    }

                    if (General.nz(txtIntervention.Text, "0").ToString() == "0")
                    {
                        if (fc_ctrlOsOrListeRouge() == true)
                            return;
                    }

                    //=== controler si cette fiche d'appel est lié à une intervention.
                    if (cmdGMAO.BackColor == cBackColorInter)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous ne pouvez pas créer de fiche d'intervention sur une fiche d'appel liée à une fiche GMAO.");
                        return;
                    }

                    txtIntervention.Text = "1";

                    if (fc_sauver() == true)
                    {
                        //= stocke le position de fiche d'appel
                        ModParametre.fc_SaveParamPosition(Variable.cUserDocStandard, this.Name, this.txtNumFicheStandard.Text);

                        //stocke le copro si existant
                        if (optQuiCopro.Checked == true && !string.IsNullOrEmpty(txtCodeParticulier.Text))
                        {
                            sParticulier = txtCodeParticulier.Text;
                        }

                        // stocke le chemin du
                        if ((optOriFax.Checked == true || optOriCourrier.Checked == true) && !string.IsNullOrEmpty(txtDocument.Text))
                        {
                            sDoc = Convert.ToString(1);
                        }

                        if (string.IsNullOrEmpty(txtCodeImmeuble.Text))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ref immeuble obligatoire", "Navigation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            return;
                        }

                        if ((int)ChkDevisAcc.CheckState == 1 && !string.IsNullOrEmpty(txtNoDevis.Text))
                        {
                            General.strTabParam[3] = txtNoDevis.Text;
                        }
                        else if ((int)ChkDevisAcc.CheckState == 1 && string.IsNullOrEmpty(txtNoDevis.Text))
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous avez coché une acceptation de devis," + "\n" + "un numero de devis est obligatoire.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            txtNoDevis.Focus();
                            return;
                        }
                        else
                        {
                            General.strTabParam[3] = "";
                        }

                        //            'Une demande d'intervention est faite : charge pour l'entreprise.
                        //            'Un compte analytique est créé portant le numéro de fiche standard
                        //            'précédé des lettres "AF" pour 'AFFAIRE'
                        //            VerifCreeAnalytique txtNumFicheStandard.Text, txtNumFicheStandard.Text, txtCodeImmeuble.Text

                        //= stocke le numero de fiche d'appel pour positionner la fiche d'intervention
                        SAGE.VerifCreeAnalytique((txtNumFicheStandard.Text), (txtNumFicheStandard.Text), (txtCodeImmeuble.Text));

                        ModParametre.fc_SaveParamPosition(Variable.cUserDocStandard, Variable.cUserIntervention, txtNumFicheStandard.Text, txtCodeImmeuble.Text, txtDateAjout.Text, sParticulier, sDoc, cmbCAI_CategorInterv.Text);

                        View.Theme.Theme.Navigate(typeof(UserIntervention));

                        //fc_Navigue Me, "C:\InterlibDT\" & PROGUserIntervention

                    }
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Fiche d'appel inexistante, validez d'abord.", "Navigation annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "cmdGoIntervention_Click");
            }


            //***********************************************************************
            //***********************************************************************
            //***********************************************************************


            //     On Error GoTo Erreur
            //   strTabParam(1) = ""
            //    'PROGINTERVENTION contient le chemin vers le programme intervention
            //    If gbAjout = False Then
            //       If txtnom <> "" Then
            //
            //            '-----si un copropriétaire est choisis.
            //            If Option8.value = True And Text1 <> "" Then
            //               strTabParam(1) = Text1
            //            End If
            //            strTabParam(0) = CStr(txtDate)
            //            If Option2.value = True And txtDoc <> "" Or Option5.value = True And txtDoc <> "" Then
            //                strTabParam(2) = "Courrier"
            //            Else
            //                strTabParam(2) = ""
            //            End If
            //            If ChkDevisAcc.value = 1 And txtNoDevis <> "" Then
            //                strTabParam(3) = txtNoDevis
            //            ElseIf ChkDevisAcc.value = 1 And txtNoDevis = "" Then
            //                MsgBox "Vous avez coché une acceptation de devis," _
            //'                & vbCrLf & "un numero de devis est obligatoire.", vbCritical, "Opération annulée"
            //                txtNoDevis.SetFocus
            //                Exit Sub
            //            End If
            //            BISprcLiaison PROGINTERVENTION, Text5, txtnom
            //            fc_Navigue Me, gsCheminPackage & PROGINTERVENTION
            //       Else
            //            MsgBox "Vous Devez saisir un codeimmeuble", vbCritical, "Opération annulée"
            //       End If
            //    ElseIf gbAjout = True Then
            //        MsgBox "Vous devez d'abord valider", vbCritical, "Navigation annulée"
            //    End If
            //     Exit Sub
            //Erreur:
            //    gFr_debug ME.Name & ";cmdIntervention_Click"




        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdHistoAppels_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            try
            {
                string requete = "SELECT GestionStandard.NumFicheStandard as \"NumFicheStandard\"," + " GestionStandard.DateAjout as \"DateAjout\"," + " GestionStandard.CAI_Code as \"CodeMotif\"," +
                    " GestionStandard.Source as \"Sourc\"," + " GestionStandard.IntervenantDT as \"IntervenantDT\"," + " GestionStandard.Observations as \"Observations\" FROM GestionStandard";
                string where_order = " (GestionStandard.CodeImmeuble = '" + txtCodeImmeuble.Text + "')  ORDER BY DateAjout DESC";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Historique des appels - " + txtCodeImmeuble.Text };
                //TODO : Mondir - Look For This : txtRafraichir
                //fg.txtRafraichir.Text = Convert.ToString(1);
                //fg.ugResultat.DoubleClickRow += (se, ev) =>
                //{
                //    iTalk_TextBox_Small21.Text = fg.ugResultat.ActiveRow.Cells["Code SousAdresse"].Value.ToString();
                //    fg.Dispose();
                //    fg.Close();
                //};

                //fg.ugResultat.KeyDown += (se, ev) =>
                //{

                //    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                //    {
                //        iTalk_TextBox_Small21.Text = fg.ugResultat.ActiveRow.Cells["Code SousAdresse"].Value.ToString();
                //        fg.Dispose();
                //        fg.Close();
                //    }
                //};
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdrecherche_Click");
            }


            //        .txtWhere = ", CAI_CategoriInterv.CAI_Libelle, CAI_CategoriInterv.CAI_Code,  " _
            //'            & "GestionStandard.Utilisateur, GestionStandard.IntervenantDT, GestionStandard.CodeImmeuble, " _
            //'            & "Personnel.Nom , Personnel.Prenom, Personnel.Initiales, GestionStandard.CodeOrigine2, " _
            //'            & "GestionStandard.Source as [Interlocuteur] FROM GestionStandard LEFT JOIN " _
            //'            & "Personnel ON GestionStandard.IntervenantDT = Personnel.Matricule LEFT JOIN " _
            //'            & "CAI_CategoriInterv ON GestionStandard.CAI_Code = CAI_CategoriInterv.CAI_Code "



        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdIntervenant_Click(System.Object eventSender, System.EventArgs eventArgs)
        {

            cmbIntervenantDT_KeyPress(cmbIntervenantDT, new KeyPressEventArgs((char)13));

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdMail_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //    MsgBox "Fonction non instalée", vbCritical, ""
            try
            {
                if (fc_sauver() == true)
                {

                    if (!string.IsNullOrEmpty(txtNumFicheStandard.Text))
                    {

                        // stocke la position de la fiche d'appel
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocStandard, txtNumFicheStandard.Text);

                        if (Dossier.fc_ControleDossier(General.DossierAppel + txtNumFicheStandard.Text + "\\Mail") == true)
                        {
                            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Un mail a déjà été envoyé, souhaitez-vous le consulter ?", "Mail existant", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.Yes)
                            {
                                frmDossierMail frmDossierMail = new frmDossierMail();
                                var _with9 = frmDossierMail;
                                _with9.FileIntervention.Tag = General.DossierAppel + txtNumFicheStandard.Text + "\\Mail";
                                General.RefreshListBox(_with9.FileIntervention);
                                _with9.lblIntervention.Text = _with9.lblIntervention.Text + " la fiche d'appel N°" + txtNumFicheStandard.Text;
                                _with9.ShowDialog();

                                frmDossierMail.Close();
                                return;
                            }
                        }
                        if (ModCourrier.InitialiseEtOuvreOutlook() == false)
                        {
                            //MsgBox "Impossible d'initialiser Outlook", vbCritical, "Initialisation d'outlook impossible"
                            return;
                        }

                        General.NumFicheStdMsg = txtNumFicheStandard.Text;

                        if (optOriTelephone.Checked == true)
                        {
                            General.MesOrigineCom = "Telephone";
                        }
                        else if (optOriFax.Checked == true)
                        {
                            General.MesOrigineCom = "Fax";
                        }
                        else if (optOriRepondeur.Checked == true)
                        {
                            General.MesOrigineCom = "Repondeur";
                        }
                        else if (optOriCourrier.Checked == true)
                        {
                            General.MesOrigineCom = "Courrier";
                        }
                        else if (optOriRadio.Checked == true)
                        {
                            General.MesOrigineCom = "Radio";
                        }
                        else if (optOriSiteWeb.Checked)
                        {
                            General.MesOrigineCom = "Site Web";
                        }
                        else if (optOriDirect.Checked == true)
                        {
                            General.MesOrigineCom = "Interne";
                        }
                        else if (optOriMail.Checked == true)
                        {
                            General.MesOrigineCom = "E-Mail";
                        }
                        else if (Option1.Checked == true)
                        {
                            General.MesOrigineCom = "Transmetteur";
                        }
                        else if (OptOriAstreinte.Checked == true)
                        {
                            General.MesOrigineCom = "Astreinte";
                        }

                        General.MesUtilFichStd = txtUtilisateur.Text;
                        if (string.IsNullOrEmpty(txtDocument.Text))
                        {
                            General.MesEmplacement = " Pas de document attaché";
                            General.saveInReg(Variable.cUserDocMail, "Documents", "");
                        }
                        else
                        {
                            General.MesEmplacement = txtDocument.Text;
                            General.saveInReg(Variable.cUserDocMail, "Documents", General.MesEmplacement);
                        }

                        General.MesSociete = txtSociete.Text;
                        General.MesNom = txtSource.Text;
                        General.MesCodeImmeuble = txtCodeImmeuble.Text;
                        General.MsgCodProd = lbllibCAI_CategorInterv.Text;
                        General.MsgCommentaire = txtObservations.Text;
                        if (optQuiSociete.Checked == true)
                        {
                            General.MesQui = lblLibIntervenantDT.Text;
                            General.MesSociete = General.NomSociete;
                            View.Theme.Theme.Navigate(typeof(UserDocMail));
                            //ModMain.fc_Navigue(this, (General.gsCheminPackage + "UserDocMail.vbd");
                        }
                        else if (optQuiGardien.Checked == true)
                        {
                            General.MesQui = txtGardien.Text;
                            General.MesSociete = "Gardien immeuble " + txtCodeImmeuble.Text;
                            View.Theme.Theme.Navigate(typeof(UserDocMail));
                            //ModMain.fc_Navigue(this, General.gsCheminPackage + "UserDocMail.vbd");
                        }
                        else if (optQuiCopro.Checked == true)
                        {
                            General.MesQui = lbllibCodeParticulier.Text;
                            General.MesSociete = "Particulier";
                            View.Theme.Theme.Navigate(typeof(UserDocMail));
                            //ModMain.fc_Navigue(this, General.gsCheminPackage + "UserDocMail.vbd");
                        }
                        else
                        {
                            General.MesQui = txtSource.Text;
                            //ModMain.fc_Navigue(this, General.gsCheminPackage + "UserDocMail.vbd");
                            View.Theme.Theme.Navigate(typeof(UserDocMail));
                        }
                    }

                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous devez d'abord valider", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdMessagerie_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdMAJ_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            fc_sauver();

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdPrecedent_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            string sCodeAppel = null;

            using (var tmpModAdo = new ModAdo())
                sCodeAppel = tmpModAdo.fc_ADOlibelle("SELECT TOP 1 GestionStandard.NumFicheStandard FROM GestionStandard WHERE NumFicheStandard<" + txtNumFicheStandard.Text + " ORDER BY GestionStandard.NumFicheStandard DESC");
            if (string.IsNullOrEmpty(sCodeAppel))
            {
                return;
            }
            fc_ChargeEnregistrement(sCodeAppel);

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdQuiDevis_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            try
            {
                if (string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Vous Devez saisir un code immeuble", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {
                    frmQuiDevis frmQuiDevis = new frmQuiDevis();
                    var _with10 = frmQuiDevis;
                    _with10.txtnom.Text = txtCodeImmeuble.Text;
                    _with10.txtCodeOriCommentDevis.Text = txtCodeOriCommentDevis.Text;
                    _with10.ShowDialog();
                    //==== si le feuille frmQuiDevis a été valider.
                    if (General.BlnMajFrmQui == true)
                    {
                        txtCodeOriCommentDevis.Text = General.strCodeOriCommentDevis;
                        txtDocumentDevis.Text = General.strDocument;
                        RechLibelleQui((Convert.ToInt32(General.strCodeOriCommentDevis)));
                        frmQuiDevis.Close();
                        cmdMAJ_Click(cmdMAJ, new System.EventArgs());
                    }
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdQuiDevis_Click");
            }
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdRechercheFour_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            try
            {
                string requete = "SELECT fournisseurArticle.Code as \"Code\"," + " fournisseurArticle.Raisonsocial as \"Raison social\"" + " FROM fournisseurArticle";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un fournisseur" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtSource.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtSource.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdrecherche_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdRechercheImmeuble_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            try
            {
                string requete = "SELECT CodeImmeuble as \"Ref Immeuble\"," + " Adresse as \"Adresse\"," + " Ville as \"Ville\" , anglerue as \"Angle de rue\", Code1 as \"Gerant\"" +
                    ", Immatriculation as \"Immatriculation\" FROM Immeuble";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un immeuble" };
                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeuble.Text } });

                var bshowFrmHistorique = false;

                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtCodeImmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                    if (fc_controleInter(txtCodeImmeuble.Text, Convert.ToDateTime(txtDateAjout.Text)) == true)
                    {

                        fc_RechercheImmeuble(txtCodeImmeuble.Text);
                        fc_Renovation();
                        General.NomImmeuble = txtCodeImmeuble.Text;

                        bshowFrmHistorique = true;
                    }
                    else
                    {

                        cmdAnnuler_Click(cmdAnnuler, new System.EventArgs());
                        return;
                    }

                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {
                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCodeImmeuble.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                        if (fc_controleInter(txtCodeImmeuble.Text, Convert.ToDateTime(txtDateAjout.Text)) == true)
                        {

                            fc_RechercheImmeuble(txtCodeImmeuble.Text);
                            fc_Renovation();
                            General.NomImmeuble = txtCodeImmeuble.Text;

                            bshowFrmHistorique = true;
                        }
                        else
                        {

                            cmdAnnuler_Click(cmdAnnuler, new System.EventArgs());
                            return;
                        }

                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();

                if (bshowFrmHistorique)
                    showFrmHistorique();

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdrecherche_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void showFrmHistorique()
        {
            frmHistorique frmHistorique = new frmHistorique();
            frmHistorique.ShowDialog();
            StockCmdQuiDevis();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sCodeImmeuble"></param>
        /// <param name="dDate"></param>
        /// <returns></returns>
        private bool fc_controleInter(string sCodeImmeuble, System.DateTime dDate)
        {
            bool functionReturnValue = false;
            // controle si il existe déja une intervention  prévue pour cet immeuble à la date
            //date de création de la fiche standard.
            string sNoInter = null;
            System.DateTime dtyesterday = default(System.DateTime);
            string sTexte = null;
            dtyesterday = DateTime.Now.AddDays(-1);

            General.sSQL = "Select datesaisie from Intervention where CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(sCodeImmeuble) + "'" +
                " and (datesaisie >='" + dtyesterday.Date.ToShortDateString() + " 00:00:01' and datesaisie <='" + dDate.Date.ToShortDateString() + " 23:59:59') ORDER BY datesaisie DESC";

            functionReturnValue = true;

            using (var tmpModAdo = new ModAdo())
                sNoInter = tmpModAdo.fc_ADOlibelle(General.sSQL);
            if (!string.IsNullOrEmpty(sNoInter))
            {
                if (Convert.ToDateTime(sNoInter).Date == DateTime.Now.Date)
                {
                    sTexte = "Il existe déjà une intervention créée aujourd'hui pour cet immeuble.";
                }
                else
                {
                    sTexte = "Une intervention a déjà été créée hier pour cet immeuble.";

                }

                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sTexte + " Voulez vous annuler cette fiche ?", "Avertissement", MessageBoxButtons.YesNo, MessageBoxIcon.Information) == DialogResult.Yes)
                {
                    functionReturnValue = false;

                }
                else
                {
                    functionReturnValue = true;
                }
            }
            return functionReturnValue;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void CmdRechercher_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            fc_Rechercher();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_Rechercher()
        {
            string sCode = null;

            try
            {
                string requete = "SELECT GestionStandard.NumFicheStandard as \"No Fiche standard\"," + " GestionStandard.Utilisateur as \"Utilisateur\"," +
                    " GestionStandard.DateAjout as \"date\", GestionStandard.CodeParticulier as \"Client\"," + " GestionStandard.CodeImmeuble as \"Référence immeuble\" From GestionStandard";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'une fiche d'appel" };
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    fc_ChargeEnregistrement(sCode);
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        fc_ChargeEnregistrement(sCode);
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
                frmDevis.Enabled = true;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_Rechercher");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <returns></returns>
        private bool fc_ctrlOsOrListeRouge()
        {
            bool functionReturnValue = false;
            string sSQL = null;
            DataTable rs = default(DataTable);
            ModAdo modAdors = null;
            //== cette fonction controle si un Ordre de service est obligatoire ou
            //== si l'immeuble est en liste rouge.
            functionReturnValue = false;

            //== controle si un ordre de service est obliagtoire si oui controle qu'il
            //== y a un fax ou un fichier de correspandance attaché
            sSQL = "SELECT OSoblig_TAB FROM Table1 WHERE Code1='" + StdSQLchaine.gFr_DoublerQuote(txtCode1.Text) + "'";
            modAdors = new ModAdo();
            rs = modAdors.fc_OpenRecordSet(sSQL);
            if (rs.Rows.Count > 0 && General.IsNumeric(rs.Rows[0]["OSoblig_TAB"].ToString()) && Convert.ToInt32(General.nz(rs.Rows[0]["OSoblig_TAB"], 0)) == 1)
            {
                if (string.IsNullOrEmpty(txtDocument.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Création de l'intervention annulée," + " un ordre de service(OS) est obligatoire pour ce gérant," +
                        " Veuillez d'abord attacher cet ordre de service en tant que courrier ou fax.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    functionReturnValue = true;

                    modAdors?.Dispose();
                    return functionReturnValue;
                }
            }

            //== controle si l'immeuble est en liste rouge.
            //== controle si un ordre de service est obliagtoire si oui controle qu'il
            //== y a un fax ou un fichier de correspandance attaché
            sSQL = "SELECT ListeRouge FROM Immeuble WHERE CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
            modAdors = new ModAdo();
            rs = modAdors.fc_OpenRecordSet(sSQL);
            if (Convert.ToInt32(General.nz(rs.Rows[0]["ListeRouge"], 0)) == 1)
            {
                MessageBox
                    .Show("Création de l'intervention annulée," + " Cet immeuble a été pointé en liste rouge.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                functionReturnValue = true;
            }


            modAdors?.Dispose();
            return functionReturnValue;


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdRechercheRaisonSocial_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            try
            {
                string requete = "SELECT CodeImmeuble as \"Ref Immeuble\"," + " RaisonSocial_IMM as \"Mnémo\"," + " Adresse as \"Adresse\"," + " Ville as \"Ville\" FROM Immeuble";
                string where_order = "";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche des immeuble" };
                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtRaisonSocial_IMM.Text } });
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtCodeImmeuble.Text = fg.ugResultat.ActiveRow.Cells[0].Value.ToString(); ;
                    fc_RechercheImmeuble(txtCodeImmeuble.Text);
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtCodeImmeuble.Text = fg.ugResultat.ActiveRow.Cells[0].Value.ToString(); ;
                        fc_RechercheImmeuble(txtCodeImmeuble.Text);
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheRaisonSocial_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdRechNoDev_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            try
            {
                string requete = "SELECT CodeImmeuble as \"Immeuble\"," + " NumeroDevis as \"No Devis\"," + " TitreDevis AS \"Titre Devis\"," + " DateCreation as \"Date creation\"," +
                    " NumFicheStandard as \"No appel\"," + "  CodeEtat as \"Code Etat\"" + " FROM  DevisEntete";
                string where_order = " DevisEntete.CodeEtat = '04' ";
                SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, " DevisEntete.DateCreation DESC ") { Text = "Recherche des devis" };
                fg.SetValues(new Dictionary<string, string> { { "CodeImmeuble", txtCodeImmeuble.Text } });
                //TODO : Mondir - First TextBox Must Be ReadOnly
                //fg.Champs0.ReadOnly = true;
                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    txtNoDevis.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                    fc_RechercheImmeuble(fg.ugResultat.ActiveRow.Cells["Immeuble"].Value.ToString());
                    fc_Renovation();
                    General.NomImmeuble = fg.ugResultat.ActiveRow.Cells["Immeuble"].Value.ToString();
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        txtNoDevis.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                        fc_RechercheImmeuble(fg.ugResultat.ActiveRow.Cells["Immeuble"].Value.ToString());
                        fc_Renovation();
                        General.NomImmeuble = fg.ugResultat.ActiveRow.Cells["Immeuble"].Value.ToString();
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechNoDev_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void CmdSauver_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (fc_sauver())
                View.Theme.Theme.AfficheAlertSucces();
        }



        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdStocker_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            bool boolFolder = false;
            int giMsg = 0;
            string sMessage = null;


            try
            {
                //- controle l' existence du code immeuble
                if (string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Ref. Immeuble obligatoire");
                    return;
                }
                frmFournOuClient frmFournOuClient = new frmFournOuClient();
                frmFournOuClient.ShowDialog();
                if (string.IsNullOrEmpty(General.sFournOuClient))
                {
                    return;
                }

                //----courrier
                if (optOriCourrier.Checked)
                {
                    if (!string.IsNullOrEmpty(file1.SelectedItem?.ToString()))
                    {

                        sMessage = "Vous allez transferer une correspondance dans le repertoire " + txtCodeImmeuble.Text.ToUpper();

                        //If optQuiFournisseur.value = True Then
                        if (General.sFournOuClient.ToUpper() == General.cFourn.ToUpper())
                        {
                            sMessage = sMessage + " dans le dossier fournisseur";
                        }

                        sMessage = sMessage + " ?" + "\n" + "Ceci impliquera une validation automatique.";

                        giMsg = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMessage, "Transfert de correspondance", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (giMsg == 6)
                        {

                            if (Dossier.fc_ControleDossier(General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\correspondance\\in") == false)
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le dossier de correspondance pour cet immeuble n'existe pas, rendez-vous dans la fiche immeuble pour le créer.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                                return;
                            }

                            //If optQuiFournisseur.value = True Then
                            if (General.sFournOuClient.ToUpper() == General.cFourn.ToUpper())
                            {

                                // - deplace le fichier
                                Dossier.fc_DeplaceFichier(file1.Tag + "\\" + file1.SelectedItem, General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\Fournisseurs\\in\\" + file1.SelectedItem);
                                txtDocument.Text = General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\Fournisseurs\\in\\" + file1.SelectedItem;
                            }
                            else
                            {
                                if (General.Right(file1.SelectedItem.ToString(), 3).ToUpper() == "PDF")
                                {
                                    if (string.IsNullOrEmpty(txtSujet.Text))
                                    {
                                        while (string.IsNullOrEmpty(txtSujet.Text))
                                        {
                                            txtSujet.Text = Microsoft.VisualBasic.Interaction.InputBox("Veuillez saisir le sujet de ce courrier : ", "Sujet");
                                        }
                                    }
                                    fc_StockDocument(General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\correspondance\\in\\" + file1.SelectedItem, (txtSujet.Text));
                                }
                                // - deplace le fichier
                                Dossier.fc_DeplaceFichier(file1.Tag + "\\" + file1.SelectedItem, General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\correspondance\\in\\" + file1.SelectedItem);
                                txtDocument.Text = General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\correspondance\\in\\" + file1.SelectedItem;
                            }
                            // enregistre la fiche d'appel
                            fc_sauver();
                            if (optOriCourrier.Checked)
                            {
                                optOriCourrier.Checked = false;
                                optOriCourrier.Checked = true;
                            }
                        }
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun courrier n'est sélectionné." + "\n" + "Vous ne pouvez pas transférer de correspondance", "Transfert de correspondance annulé", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }
                // fax
                if (optOriFax.Checked)
                {
                    if (!string.IsNullOrEmpty(file1.SelectedItem?.ToString()))
                    {

                        sMessage = "Vous allez transferer un fax dans le repertoire " + txtCodeImmeuble.Text.ToUpper();
                        //If optQuiFournisseur.value = True Then
                        if (General.sFournOuClient.ToUpper() == General.cFourn.ToUpper())
                        {
                            sMessage = sMessage + " dans le dossier fournisseur";

                        }
                        sMessage = sMessage + " ?" + "\n" + "Ceci impliquera une validation automatique.";
                        giMsg = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMessage, "Transfert de fax", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (giMsg == 6)
                        {

                            if (Dossier.fc_ControleDossier(General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\FAX\\in") == false)
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le dossier de fax pour cet immeuble n'existe pas, allez dans la fiche immeuble pour le créer.");
                                return;
                            }

                            //If optQuiFournisseur.value = True Then
                            if (General.sFournOuClient.ToUpper() == General.cFourn.ToUpper())
                            {
                                // - deplace le fichier
                                Dossier.fc_DeplaceFichier(file1.Tag + "\\" + file1.SelectedItem, General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\Fournisseurs\\in\\" + file1.SelectedItem);
                                txtDocument.Text = General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\Fournisseurs\\in\\" + file1.SelectedItem;

                            }
                            else
                            {
                                if (General.Right(file1.SelectedItem.ToString(), 3).ToUpper() == "PDF")
                                {
                                    if (string.IsNullOrEmpty(txtSujet.Text))
                                    {
                                        while (string.IsNullOrEmpty(txtSujet.Text))
                                        {
                                            txtSujet.Text = Microsoft.VisualBasic.Interaction.InputBox("Veuillez saisir le sujet de ce courrier : ", "Sujet");
                                        }
                                    }
                                }
                                fc_StockDocument(General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\correspondance\\in\\" + file1.SelectedItem, (txtSujet.Text));
                                // - deplace le fichier
                                Dossier.fc_DeplaceFichier(file1.Tag + "\\" + file1.SelectedItem, General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\FAX\\in\\" + file1.SelectedItem);
                                txtDocument.Text = General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\FAX\\in\\" + file1.SelectedItem;

                            }

                            // enregistre la fiche d'appel
                            fc_sauver();
                            if (optOriFax.Checked)
                            {
                                optOriFax.Checked = false;
                                optOriFax.Checked = true;
                            }
                        }
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun fax n'est sélectionné." + "\n" + "Vous ne pouvez pas transférer de fax", "Transfert de fax annulé", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }


                if (optOriMail.Checked)
                {
                    if (!string.IsNullOrEmpty(file1.SelectedItem?.ToString()))
                    {

                        sMessage = "Vous allez transferer un mail dans le repertoire " + txtCodeImmeuble.Text.ToUpper();
                        //If optQuiFournisseur.value = True Then
                        if (General.sFournOuClient.ToUpper() == General.cFourn.ToUpper())
                        {
                            sMessage = sMessage + " dans le dossier fournisseur";

                        }
                        sMessage = sMessage + " ?" + "\n" + "Ceci impliquera une validation automatique.";
                        giMsg = (int)Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(sMessage, "Transfert de Mail", MessageBoxButtons.YesNo, MessageBoxIcon.Question);

                        if (giMsg == 6)
                        {

                            if (Dossier.fc_ControleDossier(General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\email\\in") == false)
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Le dossier de mail pour cet immeuble n'existe pas, allez dans la fiche immeuble pour le créer.");
                                return;
                            }

                            //If optQuiFournisseur.value = True Then
                            if (General.sFournOuClient.ToUpper() == General.cFourn.ToUpper())
                            {
                                // - deplace le fichier
                                Dossier.fc_DeplaceFichier(file1.Tag + "\\" + file1.SelectedItem, General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\Fournisseurs\\in\\" + file1.SelectedItem);
                                txtDocument.Text = General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\Fournisseurs\\in\\" + file1.SelectedItem;

                            }
                            else
                            {
                                if (General.Right(file1.SelectedItem.ToString(), 3).ToUpper() == "PDF")
                                {
                                    if (string.IsNullOrEmpty(txtSujet.Text))
                                    {
                                        while (!!string.IsNullOrEmpty(txtSujet.Text))
                                        {
                                            txtSujet.Text = Microsoft.VisualBasic.Interaction.InputBox("Veuillez saisir le sujet de ce courrier : ", "Sujet");
                                        }
                                    }
                                    fc_StockDocument(General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\correspondance\\in\\" + file1.SelectedItem.ToString(), (txtSujet.Text));
                                }
                                // - deplace le fichier
                                Dossier.fc_DeplaceFichier(file1.Tag + "\\" + file1.SelectedItem, General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\email\\in\\" + file1.SelectedItem);
                                txtDocument.Text = General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\email\\in\\" + file1.SelectedItem;

                            }

                            // enregistre la fiche d'appel
                            fc_sauver();
                            if (optOriMail.Checked)
                            {
                                optOriMail.Checked = false;
                                optOriMail.Checked = true;
                            }
                        }
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun Mail n'est sélectionné." + "\n" + "Vous ne pouvez pas transférer de mail.", "Transfert de Mail annulé", MessageBoxButtons.OK, MessageBoxIcon.Exclamation);
                        return;
                    }
                }




                General.RefreshListBox(file1);
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";File1_KeyPress");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="strFile"></param>
        /// <param name="strSujet"></param>
        /// <returns></returns>
        public object fc_StockDocument(string strFile, string strSujet)
        {
            string strInsert = null;
            return null;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdSuivant_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            string sCodeAppel = null;

            using (var tmpModAdo = new ModAdo())
                sCodeAppel = tmpModAdo.fc_ADOlibelle("SELECT TOP 1 GestionStandard.NumFicheStandard FROM GestionStandard WHERE NumFicheStandard>" +
                    txtNumFicheStandard.Text + " ORDER BY GestionStandard.NumFicheStandard ASC");
            if (string.IsNullOrEmpty(sCodeAppel))
            {
                return;
            }
            fc_ChargeEnregistrement(sCodeAppel);

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void cmdSupprimer_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            fc_supprimer();
        }
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_supprimer()
        {
            string sSQL = null;
            try
            {
                if (!string.IsNullOrEmpty(General.nz(txtNumFicheStandard, "").ToString()))
                {
                    switch (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("êtes-vous sûr de vouloir supprimer cette fiche?", "", MessageBoxButtons.OKCancel, MessageBoxIcon.Information))
                    {
                        case DialogResult.OK:
                            break;

                        default:
                            return;

                            //Suppression annulée
                            break;
                    }
                }
                else
                {
                    return;
                }

                // supprime le client
                sSQL = "delete  from gestionstandard where numfichestandard =" + txtNumFicheStandard.Text;
                int xx = General.Execute(sSQL);

                // initialise ablancs les controles
                fc_clear();
                // bloque les onglets
                fc_BloqueForm();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_supprimer");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void File1_DoubleClick(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (file1.SelectedItem == null)
                return;
            if (File.Exists(file1.Tag + "\\" + file1.SelectedItem) && File.Exists(file1.Tag + "\\" + file1.SelectedItem))
                ModuleAPI.Ouvrir(file1.Tag + "\\" + file1.SelectedItem);
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Label17_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            try
            {
                if (fc_sauver() == true)
                {
                    if (!string.IsNullOrEmpty(txtNumFicheStandard.Text))
                    {
                        // stocke la position de la fiche d'appel
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocStandard, txtNumFicheStandard.Text);
                        // stocke les paramétres pour la feuille de destination.
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocClient, txtCode1.Text);
                    }
                    View.Theme.Theme.Navigate(typeof(UserDocClient));
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";lblGoClient_DblClick");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void Label22_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            optQuiCopro.Checked = true;
            fc_CodeOrigineQui(Convert.ToString(General.cCopro));
        }

        //TODO : Mondir - Has Somethig To Do With The Menu
        //private void lblFicheContrat_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    short Index = lblFicheContrat.GetIndex(eventSender);
        //    int lCop_NoAuto = 0;
        //    if (Index == 4)
        //    {
        //        General.sSQL = "Select COP_NoAuto FROM COP_ContratP2 WHERE codeimmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
        //        using (var tmpModAdo = new ModAdo())
        //            lCop_NoAuto = Convert.ToInt32(General.nz(tmpModAdo.fc_ADOlibelle(General.sSQL), 0));

        //        if (lCop_NoAuto == 0)
        //        {
        //            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Il n'existe aucun contrat pour cet immeuble.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
        //        }
        //        else
        //        {
        //            //= stocke le position de fiche d'appel
        //            //   fc_SaveParamPosition cUserDocStandard, ME.Name, Me.txtNumFicheStandard
        //            ModParametre.fc_SaveParamPosition("", Variable.cUserDocP2, Convert.ToString(lCop_NoAuto));
        //            //TODO : Mondir - Must Develope : PROGUserDocP2
        //            //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocP2);
        //        }
        //    }
        //}
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void lblGoContrat_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            //    MsgBox "Fonctionnalité non installée", vbInformation, "Contrat"
            //    Exit Sub
            try
            {
                if (fc_sauver() == true && !string.IsNullOrEmpty(txtCodeContrat.Text))
                {
                    if (!string.IsNullOrEmpty(txtNumFicheStandard.Text))
                    {
                        // stocke la position de la fiche d'appel
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocStandard, txtNumFicheStandard.Text);
                        // stocke les paramétres pour la feuille de destination.
                        ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserDocFicheContrat, txtCodeContrat.Text);
                    }
                    View.Theme.Theme.Navigate(typeof(UserDocFicheContrat));
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";lblGoContrat_Click");
            }
        }


        //TODO : Mondir - Has Something To Do With The Menu
        //private void LabelAppel_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    short Index = LabelAppel.GetIndex(eventSender);
        //    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        //    //TODO : Mondir - Must Develope : PROGUserDocStandard
        //    //ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocStandard);
        //}

        //private void LabelDispatchDevis_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    short Index = LabelDispatchDevis.GetIndex(eventSender);
        //    ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDispatchDevis);
        //}

        //private void LabelDispatchIntervention_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        //    ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDispatch);
        //}

        //private void LabelGerant_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    short Index = LabelGerant.GetIndex(eventSender);
        //    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        //    ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocClient);
        //}

        //private void LabelImmeuble_Click(System.Object eventSender, System.EventArgs eventArgs)
        //{
        //    short Index = LabelImmeuble.GetIndex(eventSender);
        //    System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;
        //    ModMain.fc_Navigue(this, General.gsCheminPackage + General.PROGUserDocImmeuble);
        //}
        /// ////////////////////////


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void lblCourier_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            this.optOriCourrier.Checked = false;
            this.optOriCourrier.Checked = true;
            if (!string.IsNullOrEmpty(txtDocument.Text) && File.Exists(txtDocument.Text))
            {
                ModuleAPI.Ouvrir(txtDocument.Text);
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void lblFax_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            this.optOriFax.Checked = false;
            this.optOriFax.Checked = true;
            if (!string.IsNullOrEmpty(txtDocument.Text) && File.Exists(txtDocument.Text))
            {
                ModuleAPI.Ouvrir(txtDocument.Text);
            }
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void lblMail_Click(System.Object eventSender, System.EventArgs eventArgs)
        {

            try
            {
                this.optOriMail.Checked = false;
                this.optOriMail.Checked = true;
                if (!string.IsNullOrEmpty(txtDocument.Text) && File.Exists(txtDocument.Text))
                {
                    if (File.Exists(txtDocument.Text))
                        ModuleAPI.Ouvrir(txtDocument.Text);
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";lblMail_Click");
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optChauffage_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optChauffage.Checked)
            {
                if (optChauffage.Checked == true)
                {
                    fc_Motif(Convert.ToString(cChauffage));
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optDepannage_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optDepannage.Checked)
            {
                if (optDepannage.Checked == true)
                {
                    fc_Motif(Convert.ToString(cChauffage));
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optECS_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optECS.Checked)
            {
                if (optECS.Checked == true)
                {
                    fc_Motif(Convert.ToString(cECS));
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optFuite_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optFuite.Checked)
            {
                if (optFuite.Checked == true)
                {
                    fc_Motif(Convert.ToString(cFuite));
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void OptOriAstreinte_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (OptOriAstreinte.Checked)
            {
                if (OptOriAstreinte.Checked == true)
                {
                    fc_origine(Convert.ToString(General.CAstreinte));
                }

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optOriCourrier_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optOriCourrier.Checked)
            {
                if (optOriCourrier.Checked == true)
                {
                    fc_origine(Convert.ToString(General.cCourrier));
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optOriDirect_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optOriDirect.Checked)
            {
                if (optOriDirect.Checked == true)
                {
                    fc_origine(Convert.ToString(General.cDirect));
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optOriFax_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optOriFax.Checked)
            {
                if (optOriFax.Checked)
                {
                    fc_origine(Convert.ToString(General.cFax));
                }

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optOriMail_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optOriMail.Checked)
            {
                if (optOriMail.Checked == true)
                {
                    fc_origine(Convert.ToString(General.CEmail));
                }

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optOriRadio_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optOriRadio.Checked)
            {
                if (optOriRadio.Checked == true)
                {
                    fc_origine(Convert.ToString(General.cRadio));
                }

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optOriRepondeur_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optOriRepondeur.Checked)
            {
                if (optOriRepondeur.Checked == true)
                {
                    fc_origine(Convert.ToString(General.cRepondeur));
                }

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optOriSiteWeb_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optOriSiteWeb.Checked)
            {
                if (optOriSiteWeb.Checked == true)
                {
                    fc_origine(Convert.ToString(General.cInternet));
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optOriTelephone_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optOriTelephone.Checked)
            {
                if (optOriTelephone.Checked == true)
                {
                    fc_origine(Convert.ToString(General.cTel));
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optQuiAutre_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optQuiAutre.Checked)
            {
                if (optQuiAutre.Checked == true)
                {
                    fc_CodeOrigineQui(Convert.ToString(General.cDivers));
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optQuiCopro_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optQuiCopro.Checked)
            {
                if (optQuiCopro.Checked == true)
                {
                    fc_CodeOrigineQui(Convert.ToString(General.cCopro));
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optQuiFournisseur_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optQuiFournisseur.Checked)
            {
                if (optQuiFournisseur.Checked == true)
                {
                    fc_CodeOrigineQui(Convert.ToString(General.cFournisseur));
                }

            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optQuiGardien_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optQuiGardien.Checked)
            {
                if (optQuiGardien.Checked == true)
                {
                    fc_CodeOrigineQui(Convert.ToString(General.cGardien));
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optQuiGerant_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optQuiGerant.Checked)
            {
                if (optQuiGerant.Checked == true)
                {
                    fc_CodeOrigineQui(Convert.ToString(General.cGerant));
                }
            }
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optQuiSociete_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optQuiSociete.Checked)
            {
                if (optQuiSociete.Checked == true)
                {
                    fc_CodeOrigineQui(Convert.ToString(General.cSociete));
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void optRadiateur_CheckedChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (optRadiateur.Checked)
            {
                if (optRadiateur.Checked == true)
                {
                    fc_Motif(Convert.ToString(cRadiateur));
                }
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeimmeuble_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)(eventArgs.KeyChar);
            if (cmdRechercheImmeuble.Enabled == true)
            {

                if (KeyAscii == 13)
                {

                    using (var tmpModAdo = new ModAdo())
                    {
                        //===> Mondir le 18.06.2021 https://groupe-dt.mantishub.io/view.php?id=2216
                        var count = tmpModAdo.fc_ADOlibelle("SELECT COUNT(*) FROM Immeuble WHERE Immeuble.CodeImmeuble LIKE '%" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "%'").ToInt();
                        //===> Fin Modif Mondir

                        if (count == 1 && !string.IsNullOrEmpty(General.nz(
                                tmpModAdo.fc_ADOlibelle(
                                    "SELECT Immeuble.CodeImmeuble FROM Immeuble WHERE CodeImmeuble like '" +
                                    txtCodeImmeuble.Text + "'"), "").ToString()))
                        {
                            if (fc_controleInter(txtCodeImmeuble.Text, Convert.ToDateTime(txtDateAjout.Text)) == false)
                            {

                                cmdAnnuler_Click(cmdAnnuler, new System.EventArgs());
                                eventArgs.Handled = true;
                                return;
                            }

                            fc_RechercheImmeuble(txtCodeImmeuble.Text);
                            General.NomImmeuble = txtCodeImmeuble.Text;
                            if (!string.IsNullOrEmpty(txtCodeImmeuble.Text))
                            {
                                fc_RechercheContrat((txtCodeImmeuble.Text));
                            }

                            frmHistorique frmHistorique = new frmHistorique();
                            frmHistorique.ShowDialog();
                            StockCmdQuiDevis();

                        }
                        else
                        {
                            cmdRechercheImmeuble_Click(cmdRechercheImmeuble, new System.EventArgs());
                            KeyAscii = 0;
                        }
                    }
                }
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeImmeuble_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            fc_RechercheImmeuble(txtCodeImmeuble.Text);
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeParticulier_ClickEvent(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (!string.IsNullOrEmpty(txtCodeParticulier.Text))
            {
                cmdEctituresCopro.Enabled = true;
            }
            else
            {
                cmdEctituresCopro.Enabled = false;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtCodeParticulier_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            bool Cancel = eventArgs.Cancel;
            if (!string.IsNullOrEmpty(txtCodeParticulier.Text))
            {
                Cancel = fc_libParticulier(txtCodeParticulier.Text);
            }
            eventArgs.Cancel = Cancel;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtQuiDevis_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            if (string.IsNullOrEmpty(txtQuiDevis.Text))
            {
                ChkDmdeDevis.CheckState = System.Windows.Forms.CheckState.Unchecked;
                ChkDmdeDevis_CheckedChanged(ChkDmdeDevis, new System.EventArgs());
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtRaisonSocial_IMM_KeyPress(System.Object eventSender, System.Windows.Forms.KeyPressEventArgs eventArgs)
        {
            short KeyAscii = (short)(eventArgs.KeyChar);
            if (KeyAscii == 13)
            {
                cmdRechercheRaisonSocial_Click(cmdRechercheRaisonSocial, new System.EventArgs());
                KeyAscii = 0;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtRaisonSocial_IMM_Validating(System.Object eventSender, System.ComponentModel.CancelEventArgs eventArgs)
        {
            bool Cancel = eventArgs.Cancel;
            fc_RechercheImmeuble(txtRaisonSocial_IMM.Text, true);
            eventArgs.Cancel = Cancel;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtSociete_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="eventSender"></param>
        /// <param name="eventArgs"></param>
        private void txtSource_TextChanged(System.Object eventSender, System.EventArgs eventArgs)
        {
            blModif = true;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeParticulier_ValueChanged(object sender, EventArgs e)
        {
            blModif = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeParticulier_AfterCloseUp(object sender, EventArgs e)
        {
            fc_libParticulier(txtCodeParticulier.Text);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocStandard_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;

            View.Theme.Theme.MainForm.mainMenu1.ShowHomeMenu("UserDocClient");

            System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Default;

            bload = true;

            //ouvre la connection avec la base
            General.open_conn();

            General.strFournisseurInterne = General.gfr_liaison("FournisseurInterne");

            //=== version dédié à pz.
            if (General.sActiveVersionPz == "1")
            {
                CHKP1.Visible = false;
                CHKP2.Visible = false;
                CHKP3.Visible = false;
                CHKP4.Visible = false;
            }
            else
            {
                CHKP1.Visible = true;
                CHKP2.Visible = true;
                CHKP3.Visible = true;
                CHKP4.Visible = true;
            }

            //récupére le nom de l'utilisateur.
            sUtilisateur = General.fncUserName();

            if (General.AffDevis == "0")
            {
                frmDevis.Visible = false;
                //cmdDevis(0).Visible = False
                cmdDevis1.Visible = false;
            }
            else
            {
                frmDevis.Visible = true;
                //cmdDevis(0).Visible = True
                cmdDevis1.Visible = true;
            }

            var _with20 = this;
            //- positionne sur le dernier enregistrement.
            if (ModParametre.fc_RecupParam(this.Name) == true)
            {
                // charge les enregistremants
                fc_ChargeEnregistrement(ModParametre.sNewVar);
            }
            else
            {
                fc_BloqueForm();
            }
            bload = false;


            if (General.sFicheContratV2 == "1")
            {
                cmdGMAO.Enabled = true;
            }
            else
            {
                cmdGMAO.Enabled = false;
            }
            //txtDateAjout.BackColor = Color.Red;

            if (General.sEvolution == "1")
            {
                Label19.Visible = true;
                Label29.Visible = true;
                txtRefOS.Visible = true;
                txtDateOS.Visible = true;
                //'txtCheminOS.Visible = true;
                Picture5.Visible = true;
                Label30.Visible = true;
            }
            else
            {
                Label19.Visible = false;
                Label29.Visible = false;
                txtRefOS.Visible = false;
                txtDateOS.Visible = false;
                //'txtCheminOS.Visible = true;
                Picture5.Visible = false;
                Label30.Visible = false;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeParticulier_BeforeDropDown(object sender, CancelEventArgs e)
        {
            try
            {
                General.sSQL = "";
                General.sSQL = "SELECT ImmeublePart.CodeParticulier, ImmeublePart.Nom,"
                                + " ImmeublePart.Batiment, ImmeublePart.Ncompte "
                                + " From ImmeublePart"
                                + " WHERE ImmeublePart.CodeImmeuble ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'";
                sheridan.InitialiseCombo(txtCodeParticulier, General.sSQL, "CodeParticulier");
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCodeParticulier_DropDown");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdDernier_Click(object sender, EventArgs e)
        {
            string sCodeAppel = "";
            using (var tmpModAdo = new ModAdo())
                sCodeAppel = tmpModAdo.fc_ADOlibelle("SELECT TOP 1 GestionStandard.NumFicheStandard FROM GestionStandard ORDER BY GestionStandard.NumFicheStandard DESC");
            fc_ChargeEnregistrement(sCodeAppel);
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void ssoleDbQuiDevis_ValueChanged(object sender, EventArgs e)
        {
            if (ssoleDbQuiDevis.ActiveRow == null)
                return;
            txtQuiDevis.Text = ssoleDbQuiDevis.ActiveRow.Cells["Nom"].Value.ToString();
            ChkDmdeDevis.CheckState = System.Windows.Forms.CheckState.Checked;
            ChkDmdeDevis_CheckedChanged(ChkDmdeDevis, new System.EventArgs());
        }

        private void Label4_Click(object sender, EventArgs e)
        {
            if (fc_sauver())
            {
                if (txtNumFicheStandard.Text != "")
                {
                    //stocke la position de la fiche immeuble
                    ModParametre.fc_SaveParamPosition(Name, Variable.cUserDocStandard, txtNumFicheStandard.Text);
                    //stocke les paramétres pour la feuille de destination.
                    ModParametre.fc_SaveParamPosition(Name, Variable.cUserDocImmeuble, txtCodeImmeuble.Text);
                }
                View.Theme.Theme.Navigate(typeof(UserDocImmeuble));
            }
        }

        private void UserDocStandard_Load(object sender, EventArgs e)
        {
            this.ChkDevisAcc.Checked = false;
        }

        private void cmdFichierOS_Click(object sender, EventArgs e)
        {
            string sDesPathFile = null;
            string sNomfichier = null;

            try
            {
                if (string.IsNullOrEmpty(txtCodeImmeuble.Text))
                {
                    CustomMessageBox.Show("Code immeuble obligatoire", "Code immeuble", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }
                OpenFileDialog CD1 = new OpenFileDialog();
                CD1.InitialDirectory = @"C:\";
                CD1.DefaultExt = "txt";
                //CD1.Filter = "Text files (*.txt)|*.txt|All files (*.*)|*.*";
                //CD1.FilterIndex = 2;
                CD1.ShowReadOnly = true;
                if (CD1.ShowDialog() == DialogResult.OK)
                {
                    if (string.IsNullOrEmpty(CD1.FileName))
                    {
                        if ((CustomMessageBox.Show("Vous allez enregistrer un fichier pour l'OS n°" + txtRefOS.Text + " ceci impliquera une validation automatique de la fiche d''appel.", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No))
                        {
                            return;
                        }
                        sNomfichier = Dossier.ExtractFileName(CD1.FileName);

                        if (Dossier.fc_ControleDossier(General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\correspondance\\in\\" + sNomfichier) == false)
                        {
                            CustomMessageBox.Show("Le dossier de correspondance pour cet immeuble n'existe pas, rendez-vous dans la fiche immeuble pour le créer.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            return;
                        }


                        Dossier.fc_DeplaceFichier(CD1.FileName, General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\correspondance\\in\\" + sNomfichier);

                        txtCheminOS.Text = General.sCheminDossier + "\\" + txtCodeImmeuble.Text + "\\correspondance\\in\\" + sNomfichier;
                        fc_sauver();

                        Label19.ForeColor = ColorTranslator.FromOle(General.cForeColorLien);
                        Label19.Font = new Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Underline, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
                    }

                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdFichierOS_Click");
            }
        }

        private void Label19_Click(System.Object eventSender, System.EventArgs eventArgs)
        {
            ModuleAPI.Ouvrir(txtCheminOS.Text);
        }

        private void OptOSfichier_CheckedChanged(object sender, EventArgs e)
        {
            if (txtCheminOS.Text == txtDocument.Text && bCheminOS == false)
            {
                txtCheminOS.Text = "";
            }
            bCheminOS = false;
        }

        private void OptOSori_CheckedChanged(object sender, EventArgs e)
        {
            if (txtCheminOS.Text != txtDocument.Text && !string.IsNullOrEmpty(txtCheminOS.Text))
            {
                if ((CustomMessageBox.Show("Attention il y a déjà un fichier attaché à cette fiche d'appel (" + txtCheminOS.Text + ")." + "\t\t" + "Si vous cliquez sur origine celui-ci sera perdu." + "\n" + " Voulez-vous continuer ? ", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No))
                {
                    bCheminOS = true;
                    OptOSfichier.Checked = true;
                }
                else
                {
                    txtCheminOS.Text = txtDocument.Text;
                }
            }
            else
            {
                txtCheminOS.Text = txtDocument.Text;
            }
        }

        private void OptOSfichier_Click(object sender, EventArgs e)
        {
            if (OptOSfichier.Checked)
                cmdFichierOS_Click(cmdFichierOS, null);
        }

        private void fc_ChargeSoldeClient()
        {
            try
            {
                var compteTiers = "";
                using (var tmpModAdo = new ModAdo())
                    compteTiers = tmpModAdo.fc_ADOlibelle("SELECT NCompte FROM Immeuble WHERE Immeuble.CodeImmeuble='" + StdSQLchaine.gFr_DoublerQuote(txtCodeImmeuble.Text) + "'");

                var critere = SAGE.GetCritere();

                var soleSearchParams = SAGE.GetSoldeClientSearchParams();
                var soleClient = SAGE.GetSoldeClient(false, soleSearchParams.DateMini, soleSearchParams.DateMaxi, compteTiers, critere);

                //===> Mondir le 16.04.2021, commented line is an old line https://groupe-dt.mantishub.io/view.php?id=2189#c5817
                //lblSolde.Text = soleClient.Solde.ToString();
                lblSolde.Text = soleClient.Solde.ToString("#,##0.00");
                //===> Fin Modif Mondir
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Mondir le 02.04.2021 https://groupe-dt.mantishub.io/view.php?id=2189
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblSolde_TextChanged(object sender, EventArgs e)
        {
            try
            {
                if (lblSolde.Text.ToDouble() < 0)
                {
                    lblSolde.BackColor = Color.Red;
                }
                else
                {
                    lblSolde.BackColor = Color.FromArgb(55, 84, 96);
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Mondir le 16.04.2021 https://groupe-dt.mantishub.io/view.php?id=2189#c5882
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCode1_TextChanged(object sender, EventArgs e)
        {
            fc_ChargeSoldeClient();
        }

        /// <summary>
        /// Mondir le 16.04.2021, https://groupe-dt.mantishub.io/view.php?id=2189#c5817
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void lblSolde_Click(object sender, EventArgs e)
        {
            UserDocImmeuble.ActivateFinanceTab = true;
            Label4_Click(sender, e);
            UserDocImmeuble.ActivateFinanceTab = false;
        }

        private void CmdEditer_Click(object sender, EventArgs e)
        {

        }
    }
}
