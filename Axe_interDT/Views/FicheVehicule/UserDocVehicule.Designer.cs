namespace Axe_interDT.Views.FicheVehicule
{
    partial class UserDocVehicule
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.CmdVehicule = new System.Windows.Forms.Button();
            this.cmdClean = new System.Windows.Forms.Button();
            this.cmdExportHisto = new System.Windows.Forms.Button();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.GridVehicule = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.label33 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.txtImmatriculation = new iTalk.iTalk_TextBox_Small2();
            this.txtPRV = new iTalk.iTalk_TextBox_Small2();
            this.txtCodeArval = new iTalk.iTalk_TextBox_Small2();
            this.cmdRechercheCommercial = new System.Windows.Forms.Button();
            this.Command1 = new System.Windows.Forms.Button();
            this.cmdFindArval = new System.Windows.Forms.Button();
            this.label3 = new System.Windows.Forms.Label();
            this.label4 = new System.Windows.Forms.Label();
            this.txtCodeFlocage = new iTalk.iTalk_TextBox_Small2();
            this.txtIntervenant = new iTalk.iTalk_TextBox_Small2();
            this.chkVoyant = new System.Windows.Forms.CheckBox();
            this.Frame2 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.OptPrise = new System.Windows.Forms.RadioButton();
            this.OptRest = new System.Windows.Forms.RadioButton();
            this.OptSans = new System.Windows.Forms.RadioButton();
            this.cmdFindFlocage = new System.Windows.Forms.Button();
            this.cmdFndInterv = new System.Windows.Forms.Button();
            this.lblLibProbleme = new System.Windows.Forms.Label();
            this.lblLibArval = new System.Windows.Forms.Label();
            this.lblLibFlocage = new System.Windows.Forms.Label();
            this.lbllibIntervenant = new System.Windows.Forms.Label();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.flowLayoutPanel1.SuspendLayout();
            this.tableLayoutPanel1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridVehicule)).BeginInit();
            this.Frame2.SuspendLayout();
            this.tableLayoutPanel2.SuspendLayout();
            this.SuspendLayout();
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.CmdVehicule);
            this.flowLayoutPanel1.Controls.Add(this.cmdClean);
            this.flowLayoutPanel1.Controls.Add(this.cmdExportHisto);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Right;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(1654, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(196, 45);
            this.flowLayoutPanel1.TabIndex = 0;
            // 
            // CmdVehicule
            // 
            this.CmdVehicule.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdVehicule.Cursor = System.Windows.Forms.Cursors.Hand;
            this.CmdVehicule.FlatAppearance.BorderSize = 0;
            this.CmdVehicule.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdVehicule.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.CmdVehicule.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.CmdVehicule.Location = new System.Drawing.Point(2, 2);
            this.CmdVehicule.Margin = new System.Windows.Forms.Padding(2);
            this.CmdVehicule.Name = "CmdVehicule";
            this.CmdVehicule.Size = new System.Drawing.Size(60, 35);
            this.CmdVehicule.TabIndex = 359;
            this.CmdVehicule.Tag = "";
            this.CmdVehicule.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.CmdVehicule, "Rechercher");
            this.CmdVehicule.UseVisualStyleBackColor = false;
            this.CmdVehicule.Click += new System.EventHandler(this.CmdVehicule_Click);
            // 
            // cmdClean
            // 
            this.cmdClean.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdClean.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdClean.FlatAppearance.BorderSize = 0;
            this.cmdClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClean.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdClean.Image = global::Axe_interDT.Properties.Resources.Refresh_24x24;
            this.cmdClean.Location = new System.Drawing.Point(66, 2);
            this.cmdClean.Margin = new System.Windows.Forms.Padding(2);
            this.cmdClean.Name = "cmdClean";
            this.cmdClean.Size = new System.Drawing.Size(60, 35);
            this.cmdClean.TabIndex = 383;
            this.cmdClean.Tag = "";
            this.cmdClean.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdClean, "Effacer les champs .");
            this.cmdClean.UseVisualStyleBackColor = false;
            this.cmdClean.Click += new System.EventHandler(this.cmdClean_Click);
            // 
            // cmdExportHisto
            // 
            this.cmdExportHisto.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdExportHisto.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdExportHisto.FlatAppearance.BorderSize = 0;
            this.cmdExportHisto.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdExportHisto.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdExportHisto.Image = global::Axe_interDT.Properties.Resources.Excel_32x32;
            this.cmdExportHisto.Location = new System.Drawing.Point(130, 2);
            this.cmdExportHisto.Margin = new System.Windows.Forms.Padding(2);
            this.cmdExportHisto.Name = "cmdExportHisto";
            this.cmdExportHisto.Size = new System.Drawing.Size(60, 35);
            this.cmdExportHisto.TabIndex = 382;
            this.cmdExportHisto.Tag = "";
            this.cmdExportHisto.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdExportHisto, "Exporter la sélection");
            this.cmdExportHisto.UseVisualStyleBackColor = false;
            this.cmdExportHisto.Click += new System.EventHandler(this.cmdExportHisto_Click);
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 8;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 22.22222F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel1.Controls.Add(this.GridVehicule, 0, 5);
            this.tableLayoutPanel1.Controls.Add(this.flowLayoutPanel1, 7, 0);
            this.tableLayoutPanel1.Controls.Add(this.label33, 0, 1);
            this.tableLayoutPanel1.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel1.Controls.Add(this.label1, 0, 3);
            this.tableLayoutPanel1.Controls.Add(this.txtImmatriculation, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.txtPRV, 1, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtCodeArval, 1, 3);
            this.tableLayoutPanel1.Controls.Add(this.cmdRechercheCommercial, 2, 1);
            this.tableLayoutPanel1.Controls.Add(this.Command1, 2, 2);
            this.tableLayoutPanel1.Controls.Add(this.cmdFindArval, 2, 3);
            this.tableLayoutPanel1.Controls.Add(this.label3, 4, 3);
            this.tableLayoutPanel1.Controls.Add(this.label4, 4, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtCodeFlocage, 5, 2);
            this.tableLayoutPanel1.Controls.Add(this.txtIntervenant, 5, 3);
            this.tableLayoutPanel1.Controls.Add(this.chkVoyant, 2, 4);
            this.tableLayoutPanel1.Controls.Add(this.Frame2, 4, 4);
            this.tableLayoutPanel1.Controls.Add(this.cmdFindFlocage, 6, 2);
            this.tableLayoutPanel1.Controls.Add(this.cmdFndInterv, 6, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblLibProbleme, 3, 2);
            this.tableLayoutPanel1.Controls.Add(this.lblLibArval, 3, 3);
            this.tableLayoutPanel1.Controls.Add(this.lblLibFlocage, 7, 2);
            this.tableLayoutPanel1.Controls.Add(this.lbllibIntervenant, 7, 3);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 6;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 45F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 56F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel1.TabIndex = 1;
            // 
            // GridVehicule
            // 
            this.tableLayoutPanel1.SetColumnSpan(this.GridVehicule, 8);
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridVehicule.DisplayLayout.Appearance = appearance1;
            this.GridVehicule.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridVehicule.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.GridVehicule.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridVehicule.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.GridVehicule.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridVehicule.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.GridVehicule.DisplayLayout.MaxColScrollRegions = 1;
            this.GridVehicule.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridVehicule.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridVehicule.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.GridVehicule.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridVehicule.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridVehicule.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.GridVehicule.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridVehicule.DisplayLayout.Override.CellAppearance = appearance8;
            this.GridVehicule.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridVehicule.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.GridVehicule.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.GridVehicule.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.GridVehicule.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridVehicule.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.GridVehicule.DisplayLayout.Override.RowAppearance = appearance11;
            this.GridVehicule.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridVehicule.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridVehicule.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.GridVehicule.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridVehicule.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridVehicule.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridVehicule.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.GridVehicule.Location = new System.Drawing.Point(3, 194);
            this.GridVehicule.Name = "GridVehicule";
            this.GridVehicule.Size = new System.Drawing.Size(1844, 760);
            this.GridVehicule.TabIndex = 586;
            this.GridVehicule.Text = "ultraGrid1";
            this.GridVehicule.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridVehicule_InitializeLayout);
            this.GridVehicule.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.GridVehicule_InitializeRow);
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 45);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(117, 30);
            this.label33.TabIndex = 384;
            this.label33.Text = "Immatriculation";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label2.Location = new System.Drawing.Point(3, 75);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(117, 30);
            this.label2.TabIndex = 386;
            this.label2.Text = "Problème";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(3, 105);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(117, 30);
            this.label1.TabIndex = 385;
            this.label1.Text = "Arval";
            // 
            // txtImmatriculation
            // 
            this.txtImmatriculation.AccAcceptNumbersOnly = false;
            this.txtImmatriculation.AccAllowComma = false;
            this.txtImmatriculation.AccBackgroundColor = System.Drawing.Color.White;
            this.txtImmatriculation.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtImmatriculation.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtImmatriculation.AccHidenValue = "";
            this.txtImmatriculation.AccNotAllowedChars = null;
            this.txtImmatriculation.AccReadOnly = false;
            this.txtImmatriculation.AccReadOnlyAllowDelete = false;
            this.txtImmatriculation.AccRequired = false;
            this.txtImmatriculation.BackColor = System.Drawing.Color.White;
            this.txtImmatriculation.CustomBackColor = System.Drawing.Color.White;
            this.txtImmatriculation.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtImmatriculation.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtImmatriculation.ForeColor = System.Drawing.Color.Black;
            this.txtImmatriculation.Location = new System.Drawing.Point(125, 47);
            this.txtImmatriculation.Margin = new System.Windows.Forms.Padding(2);
            this.txtImmatriculation.MaxLength = 32767;
            this.txtImmatriculation.Multiline = false;
            this.txtImmatriculation.Name = "txtImmatriculation";
            this.txtImmatriculation.ReadOnly = false;
            this.txtImmatriculation.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtImmatriculation.Size = new System.Drawing.Size(345, 27);
            this.txtImmatriculation.TabIndex = 574;
            this.txtImmatriculation.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtImmatriculation.UseSystemPasswordChar = false;
            this.txtImmatriculation.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtImmatriculation_KeyPress);
            // 
            // txtPRV
            // 
            this.txtPRV.AccAcceptNumbersOnly = false;
            this.txtPRV.AccAllowComma = false;
            this.txtPRV.AccBackgroundColor = System.Drawing.Color.White;
            this.txtPRV.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtPRV.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtPRV.AccHidenValue = "";
            this.txtPRV.AccNotAllowedChars = null;
            this.txtPRV.AccReadOnly = false;
            this.txtPRV.AccReadOnlyAllowDelete = false;
            this.txtPRV.AccRequired = false;
            this.txtPRV.BackColor = System.Drawing.Color.White;
            this.txtPRV.CustomBackColor = System.Drawing.Color.White;
            this.txtPRV.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtPRV.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtPRV.ForeColor = System.Drawing.Color.Black;
            this.txtPRV.Location = new System.Drawing.Point(125, 77);
            this.txtPRV.Margin = new System.Windows.Forms.Padding(2);
            this.txtPRV.MaxLength = 32767;
            this.txtPRV.Multiline = false;
            this.txtPRV.Name = "txtPRV";
            this.txtPRV.ReadOnly = false;
            this.txtPRV.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtPRV.Size = new System.Drawing.Size(345, 27);
            this.txtPRV.TabIndex = 576;
            this.txtPRV.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtPRV.UseSystemPasswordChar = false;
            this.txtPRV.TextChanged += new System.EventHandler(this.txtPRV_TextChanged);
            this.txtPRV.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtPRV_KeyPress);
            // 
            // txtCodeArval
            // 
            this.txtCodeArval.AccAcceptNumbersOnly = false;
            this.txtCodeArval.AccAllowComma = false;
            this.txtCodeArval.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeArval.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeArval.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeArval.AccHidenValue = "";
            this.txtCodeArval.AccNotAllowedChars = null;
            this.txtCodeArval.AccReadOnly = false;
            this.txtCodeArval.AccReadOnlyAllowDelete = false;
            this.txtCodeArval.AccRequired = false;
            this.txtCodeArval.BackColor = System.Drawing.Color.White;
            this.txtCodeArval.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeArval.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeArval.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeArval.ForeColor = System.Drawing.Color.Black;
            this.txtCodeArval.Location = new System.Drawing.Point(125, 107);
            this.txtCodeArval.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeArval.MaxLength = 32767;
            this.txtCodeArval.Multiline = false;
            this.txtCodeArval.Name = "txtCodeArval";
            this.txtCodeArval.ReadOnly = false;
            this.txtCodeArval.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeArval.Size = new System.Drawing.Size(345, 27);
            this.txtCodeArval.TabIndex = 578;
            this.txtCodeArval.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeArval.UseSystemPasswordChar = false;
            this.txtCodeArval.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeArval_KeyPress);
            // 
            // cmdRechercheCommercial
            // 
            this.cmdRechercheCommercial.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheCommercial.FlatAppearance.BorderSize = 0;
            this.cmdRechercheCommercial.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheCommercial.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheCommercial.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheCommercial.Location = new System.Drawing.Point(475, 48);
            this.cmdRechercheCommercial.Name = "cmdRechercheCommercial";
            this.cmdRechercheCommercial.Size = new System.Drawing.Size(24, 20);
            this.cmdRechercheCommercial.TabIndex = 475;
            this.cmdRechercheCommercial.UseVisualStyleBackColor = false;
            this.cmdRechercheCommercial.Click += new System.EventHandler(this.cmdRechercheCommercial_Click);
            // 
            // Command1
            // 
            this.Command1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command1.FlatAppearance.BorderSize = 0;
            this.Command1.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command1.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Command1.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.Command1.Location = new System.Drawing.Point(475, 78);
            this.Command1.Name = "Command1";
            this.Command1.Size = new System.Drawing.Size(24, 20);
            this.Command1.TabIndex = 477;
            this.Command1.UseVisualStyleBackColor = false;
            this.Command1.Click += new System.EventHandler(this.Command1_Click);
            // 
            // cmdFindArval
            // 
            this.cmdFindArval.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFindArval.FlatAppearance.BorderSize = 0;
            this.cmdFindArval.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFindArval.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdFindArval.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdFindArval.Location = new System.Drawing.Point(475, 108);
            this.cmdFindArval.Name = "cmdFindArval";
            this.cmdFindArval.Size = new System.Drawing.Size(24, 20);
            this.cmdFindArval.TabIndex = 479;
            this.cmdFindArval.UseVisualStyleBackColor = false;
            this.cmdFindArval.Click += new System.EventHandler(this.cmdFindArval_Click);
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label3.Location = new System.Drawing.Point(854, 105);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(90, 30);
            this.label3.TabIndex = 387;
            this.label3.Text = "Intervenant";
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label4.Location = new System.Drawing.Point(854, 75);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(90, 30);
            this.label4.TabIndex = 388;
            this.label4.Text = "Flocage";
            // 
            // txtCodeFlocage
            // 
            this.txtCodeFlocage.AccAcceptNumbersOnly = false;
            this.txtCodeFlocage.AccAllowComma = false;
            this.txtCodeFlocage.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeFlocage.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeFlocage.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeFlocage.AccHidenValue = "";
            this.txtCodeFlocage.AccNotAllowedChars = null;
            this.txtCodeFlocage.AccReadOnly = false;
            this.txtCodeFlocage.AccReadOnlyAllowDelete = false;
            this.txtCodeFlocage.AccRequired = false;
            this.txtCodeFlocage.BackColor = System.Drawing.Color.White;
            this.txtCodeFlocage.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeFlocage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeFlocage.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtCodeFlocage.ForeColor = System.Drawing.Color.Black;
            this.txtCodeFlocage.Location = new System.Drawing.Point(949, 77);
            this.txtCodeFlocage.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeFlocage.MaxLength = 32767;
            this.txtCodeFlocage.Multiline = false;
            this.txtCodeFlocage.Name = "txtCodeFlocage";
            this.txtCodeFlocage.ReadOnly = false;
            this.txtCodeFlocage.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeFlocage.Size = new System.Drawing.Size(345, 27);
            this.txtCodeFlocage.TabIndex = 580;
            this.txtCodeFlocage.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtCodeFlocage.UseSystemPasswordChar = false;
            this.txtCodeFlocage.TextChanged += new System.EventHandler(this.txtCodeFlocage_TextChanged);
            this.txtCodeFlocage.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeFlocage_KeyPress);
            // 
            // txtIntervenant
            // 
            this.txtIntervenant.AccAcceptNumbersOnly = false;
            this.txtIntervenant.AccAllowComma = false;
            this.txtIntervenant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntervenant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntervenant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntervenant.AccHidenValue = "";
            this.txtIntervenant.AccNotAllowedChars = null;
            this.txtIntervenant.AccReadOnly = false;
            this.txtIntervenant.AccReadOnlyAllowDelete = false;
            this.txtIntervenant.AccRequired = false;
            this.txtIntervenant.BackColor = System.Drawing.Color.White;
            this.txtIntervenant.CustomBackColor = System.Drawing.Color.White;
            this.txtIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntervenant.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.txtIntervenant.ForeColor = System.Drawing.Color.Black;
            this.txtIntervenant.Location = new System.Drawing.Point(949, 107);
            this.txtIntervenant.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntervenant.MaxLength = 32767;
            this.txtIntervenant.Multiline = false;
            this.txtIntervenant.Name = "txtIntervenant";
            this.txtIntervenant.ReadOnly = false;
            this.txtIntervenant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntervenant.Size = new System.Drawing.Size(345, 27);
            this.txtIntervenant.TabIndex = 581;
            this.txtIntervenant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntervenant.UseSystemPasswordChar = false;
            this.txtIntervenant.TextChanged += new System.EventHandler(this.txtIntervenant_TextChanged);
            this.txtIntervenant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIntervenant_KeyPress);
            // 
            // chkVoyant
            // 
            this.chkVoyant.AutoSize = true;
            this.tableLayoutPanel1.SetColumnSpan(this.chkVoyant, 2);
            this.chkVoyant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.chkVoyant.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.chkVoyant.Location = new System.Drawing.Point(475, 138);
            this.chkVoyant.Name = "chkVoyant";
            this.chkVoyant.Size = new System.Drawing.Size(373, 50);
            this.chkVoyant.TabIndex = 585;
            this.chkVoyant.Text = "Voyant orange";
            this.chkVoyant.UseVisualStyleBackColor = true;
            // 
            // Frame2
            // 
            this.Frame2.BackColor = System.Drawing.Color.Transparent;
            this.tableLayoutPanel1.SetColumnSpan(this.Frame2, 4);
            this.Frame2.Controls.Add(this.tableLayoutPanel2);
            this.Frame2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame2.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Frame2.Location = new System.Drawing.Point(851, 135);
            this.Frame2.Margin = new System.Windows.Forms.Padding(0);
            this.Frame2.Name = "Frame2";
            this.Frame2.Size = new System.Drawing.Size(999, 56);
            this.Frame2.TabIndex = 582;
            this.Frame2.TabStop = false;
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 33.33333F));
            this.tableLayoutPanel2.Controls.Add(this.OptPrise, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.OptRest, 0, 0);
            this.tableLayoutPanel2.Controls.Add(this.OptSans, 1, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(3, 20);
            this.tableLayoutPanel2.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 1;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 33F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(993, 33);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // OptPrise
            // 
            this.OptPrise.AutoSize = true;
            this.OptPrise.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptPrise.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptPrise.Location = new System.Drawing.Point(334, 3);
            this.OptPrise.Name = "OptPrise";
            this.OptPrise.Size = new System.Drawing.Size(325, 27);
            this.OptPrise.TabIndex = 540;
            this.OptPrise.Text = "Prise véhicule";
            this.OptPrise.UseVisualStyleBackColor = true;
            // 
            // OptRest
            // 
            this.OptRest.AutoSize = true;
            this.OptRest.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptRest.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptRest.Location = new System.Drawing.Point(3, 3);
            this.OptRest.Name = "OptRest";
            this.OptRest.Size = new System.Drawing.Size(325, 27);
            this.OptRest.TabIndex = 538;
            this.OptRest.Text = "Restitution vhéhicule";
            this.OptRest.UseVisualStyleBackColor = true;
            // 
            // OptSans
            // 
            this.OptSans.AutoSize = true;
            this.OptSans.Checked = true;
            this.OptSans.Dock = System.Windows.Forms.DockStyle.Fill;
            this.OptSans.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.OptSans.Location = new System.Drawing.Point(665, 3);
            this.OptSans.Name = "OptSans";
            this.OptSans.Size = new System.Drawing.Size(325, 27);
            this.OptSans.TabIndex = 568;
            this.OptSans.TabStop = true;
            this.OptSans.Text = "Sans";
            this.OptSans.UseVisualStyleBackColor = true;
            // 
            // cmdFindFlocage
            // 
            this.cmdFindFlocage.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFindFlocage.FlatAppearance.BorderSize = 0;
            this.cmdFindFlocage.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFindFlocage.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdFindFlocage.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdFindFlocage.Location = new System.Drawing.Point(1299, 78);
            this.cmdFindFlocage.Name = "cmdFindFlocage";
            this.cmdFindFlocage.Size = new System.Drawing.Size(24, 20);
            this.cmdFindFlocage.TabIndex = 481;
            this.cmdFindFlocage.UseVisualStyleBackColor = false;
            this.cmdFindFlocage.Click += new System.EventHandler(this.cmdFindFlocage_Click);
            // 
            // cmdFndInterv
            // 
            this.cmdFndInterv.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdFndInterv.FlatAppearance.BorderSize = 0;
            this.cmdFndInterv.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFndInterv.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdFndInterv.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdFndInterv.Location = new System.Drawing.Point(1299, 108);
            this.cmdFndInterv.Name = "cmdFndInterv";
            this.cmdFndInterv.Size = new System.Drawing.Size(24, 20);
            this.cmdFndInterv.TabIndex = 484;
            this.cmdFndInterv.UseVisualStyleBackColor = false;
            this.cmdFndInterv.Click += new System.EventHandler(this.cmdFndInterv_Click);
            // 
            // lblLibProbleme
            // 
            this.lblLibProbleme.AutoSize = true;
            this.lblLibProbleme.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibProbleme.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLibProbleme.Location = new System.Drawing.Point(505, 75);
            this.lblLibProbleme.Name = "lblLibProbleme";
            this.lblLibProbleme.Size = new System.Drawing.Size(343, 30);
            this.lblLibProbleme.TabIndex = 583;
            // 
            // lblLibArval
            // 
            this.lblLibArval.AutoSize = true;
            this.lblLibArval.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibArval.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLibArval.Location = new System.Drawing.Point(505, 105);
            this.lblLibArval.Name = "lblLibArval";
            this.lblLibArval.Size = new System.Drawing.Size(343, 30);
            this.lblLibArval.TabIndex = 584;
            // 
            // lblLibFlocage
            // 
            this.lblLibFlocage.AutoSize = true;
            this.lblLibFlocage.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblLibFlocage.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblLibFlocage.Location = new System.Drawing.Point(1329, 75);
            this.lblLibFlocage.Name = "lblLibFlocage";
            this.lblLibFlocage.Size = new System.Drawing.Size(518, 30);
            this.lblLibFlocage.TabIndex = 582;
            // 
            // lbllibIntervenant
            // 
            this.lbllibIntervenant.AutoSize = true;
            this.lbllibIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbllibIntervenant.Font = new System.Drawing.Font("Ubuntu", 11F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lbllibIntervenant.Location = new System.Drawing.Point(1329, 105);
            this.lbllibIntervenant.Name = "lbllibIntervenant";
            this.lbllibIntervenant.Size = new System.Drawing.Size(518, 30);
            this.lbllibIntervenant.TabIndex = 585;
            // 
            // UserDocVehicule
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel1);
            this.Name = "UserDocVehicule";
            this.Size = new System.Drawing.Size(1850, 957);
            this.VisibleChanged += new System.EventHandler(this.UserDocVehicule_VisibleChanged);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.tableLayoutPanel1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridVehicule)).EndInit();
            this.Frame2.ResumeLayout(false);
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button CmdVehicule;
        public System.Windows.Forms.Button cmdExportHisto;
        public System.Windows.Forms.Button cmdClean;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.CheckBox chkVoyant;
        public iTalk.iTalk_TextBox_Small2 txtImmatriculation;
        public iTalk.iTalk_TextBox_Small2 txtPRV;
        public iTalk.iTalk_TextBox_Small2 txtCodeArval;
        public iTalk.iTalk_TextBox_Small2 txtCodeFlocage;
        public iTalk.iTalk_TextBox_Small2 txtIntervenant;
        public System.Windows.Forms.Button cmdRechercheCommercial;
        public System.Windows.Forms.Button Command1;
        public System.Windows.Forms.Button cmdFindArval;
        public System.Windows.Forms.Button cmdFindFlocage;
        public System.Windows.Forms.Button cmdFndInterv;
        public System.Windows.Forms.GroupBox Frame2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        public System.Windows.Forms.RadioButton OptPrise;
        public System.Windows.Forms.RadioButton OptRest;
        public System.Windows.Forms.RadioButton OptSans;
        public System.Windows.Forms.Label lblLibProbleme;
        public System.Windows.Forms.Label lblLibArval;
        public System.Windows.Forms.Label lblLibFlocage;
        public System.Windows.Forms.Label lbllibIntervenant;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridVehicule;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}
