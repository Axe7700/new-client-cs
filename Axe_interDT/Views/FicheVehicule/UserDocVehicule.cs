﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using System.Diagnostics;
using Axe_interDT.View.SharedViews;

namespace Axe_interDT.Views.FicheVehicule
{
    public partial class UserDocVehicule : UserControl
    {
        DataTable rsVeh = new DataTable();
        ModAdo ModAdorsVeh = new ModAdo();

        string sOrderBy;
        string sAscDesc;
        string sWhere;

        const short cCol = 0;

        public UserDocVehicule()
        {
            InitializeComponent();
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocVehicule_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
                return;

            View.Theme.Theme.MainMenu.ShowHomeMenu("UserDocVehicule");
            fc_GetParam();
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdClean_Click(object sender, EventArgs e)
        {

            txtImmatriculation.Text = "";
            txtPRV.Text = "";
            lblLibProbleme.Text = "";
            txtCodeArval.Text = "";
            lblLibArval.Text = "";
            txtCodeFlocage.Text = "";
            lblLibFlocage.Text = "";
            txtIntervenant.Text = "";
            lbllibIntervenant.Text = "";
            chkVoyant.CheckState = System.Windows.Forms.CheckState.Unchecked;
            OptSans.Checked = true;

        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void FC_SaveParam()
        {

            try
            {
                General.saveInReg("UserDocVehicule", txtImmatriculation.Name, txtImmatriculation.Text);
                General.saveInReg("UserDocVehicule", txtPRV.Name, txtPRV.Text);
                General.saveInReg("UserDocVehicule", lblLibProbleme.Name, lblLibProbleme.Text);
                General.saveInReg("UserDocVehicule", txtCodeArval.Name, txtCodeArval.Text);
                General.saveInReg("UserDocVehicule", lblLibArval.Name, lblLibArval.Text);
                General.saveInReg("UserDocVehicule", txtCodeFlocage.Name, txtCodeFlocage.Text);
                General.saveInReg("UserDocVehicule", lblLibFlocage.Name, lblLibFlocage.Text);
                General.saveInReg("UserDocVehicule", txtIntervenant.Name, txtIntervenant.Text);
                General.saveInReg("UserDocVehicule", lbllibIntervenant.Name, lbllibIntervenant.Text);

                General.saveInReg("UserDocVehicule", chkVoyant.Name, Convert.ToString(chkVoyant.Checked));
                General.saveInReg("UserDocVehicule", OptRest.Name, Convert.ToString(OptRest.Checked));
                General.saveInReg("UserDocVehicule", OptPrise.Name, Convert.ToString(OptPrise.Checked));
                General.saveInReg("UserDocVehicule", OptSans.Name, Convert.ToString(OptSans.Checked));

            } catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";FC_SaveParam");
            }



        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_GetParam()
        {
            try
            {

                txtImmatriculation.Text = General.getFrmReg("UserDocVehicule", "txtImmatriculation", "");
                txtPRV.Text = General.getFrmReg("UserDocVehicule", "txtPRV", "");
                lblLibProbleme.Text = General.getFrmReg("UserDocVehicule", "lblLibProbleme", "");
                txtCodeArval.Text = General.getFrmReg("UserDocVehicule", "txtCodeArval", "");
                lblLibArval.Text = General.getFrmReg("UserDocVehicule", "lblLibArval", "");
                txtCodeFlocage.Text = General.getFrmReg("UserDocVehicule", "txtCodeFlocage", "");
                lblLibFlocage.Text = General.getFrmReg("UserDocVehicule", "lblLibFlocage", "");
                txtIntervenant.Text = General.getFrmReg("UserDocVehicule", "txtIntervenant", "");
                lbllibIntervenant.Text = General.getFrmReg("UserDocVehicule", "lbllibIntervenant", "");
                chkVoyant.Checked = General.getFrmReg("UserDocVehicule", "chkVoyant", "0").ToString()== "True";

                OptRest.Checked = General.getFrmReg("UserDocVehicule", "OptRest", Convert.ToString(false)) == "True";
                OptPrise.Checked = General.getFrmReg("UserDocVehicule", "OptPrise", Convert.ToString(false)) == "True";
                OptSans.Checked = General.getFrmReg("UserDocVehicule", "OptSans", Convert.ToString(false)) == "True";

                if (OptRest.Checked == false && OptPrise.Checked == false && OptSans.Checked == false)
                {
                    OptSans.Checked = true;
                }

                return;
            } catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_GetParam");

            }

        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdExportHisto_Click(object sender, EventArgs e)
        {
            Microsoft.Office.Interop.Excel.Application oXL = default(Microsoft.Office.Interop.Excel.Application);
            Microsoft.Office.Interop.Excel.Workbook oWB = default(Microsoft.Office.Interop.Excel.Workbook);
            Microsoft.Office.Interop.Excel.Worksheet oSheet = default(Microsoft.Office.Interop.Excel.Worksheet);
            Microsoft.Office.Interop.Excel.Range oRng = default(Microsoft.Office.Interop.Excel.Range);
            Microsoft.Office.Interop.Excel.Range oResizeRange = default(Microsoft.Office.Interop.Excel.Range);

            DataTable rsExport = default(DataTable);
            //ADODB.Field oField = default(ADODB.Field);

            int i = 0;
            int j = 0;
            string sMatricule = null;
            int lLigne = 0;



            try
            {

                General.sSQL = "SELECT     VEH_VEHICULE.VEH_ID, VEH_VEHICULE.VEI_Immatriculation AS Immatriculation, VEH_VEHICULE.VEH_Kilometrage AS Kilométrage, "
                    + " VEH_VEHICULE.VEH_PritOuRestitution AS [Code Prise/rest], '' AS [Prise/Restitution], VEH_VEHICULE.Matricule, "
                    + " VEH_VEHICULE.PRV_ID AS [Code Probleme], PRV_ProblemeVehicule.PRV_Libelle AS Problème, "
                    + " VEH_VEHICULE.VEH_CommSurProbleme AS Commentaire, VEH_VEHICULE.VEH_VoyantOrange, '' AS [Voyant Orange], "
                    + " VEH_VEHICULE.ARV_ID AS [Code Arval], ARV_ARVAL.ARV_Libelle AS Arval, VEH_VEHICULE.VEH_NettoyageInterieur AS [Nettoyage interieur], "
                    + " VEH_VEHICULE.VEH_NettoyageInterieurDate AS [Date Net. Int.], VEH_VEHICULE.VEH_NettoyageExterieur AS [Nettoyage Exterieur], "
                    + " VEH_VEHICULE.VEH_NettoyageExterieurDate AS [Date Net. Int.], VEH_VEHICULE.FLO_ID AS [Code Flocage], FLO_FLOCAGE.FLO_Libelle AS Focage, "
                    + " VEH_VEHICULE.VEH_CreeLe , VEH_VEHICULE.VEH_CreePar, VEH_VEHICULE.VEH_ModifieLe, VEH_VEHICULE.VEH_ModifierPar "
                    + " FROM         VEH_VEHICULE LEFT OUTER JOIN " + " FLO_FLOCAGE ON VEH_VEHICULE.FLO_ID = FLO_FLOCAGE.FLO_ID LEFT OUTER JOIN"
                    + " PRV_ProblemeVehicule ON VEH_VEHICULE.PRV_ID = PRV_ProblemeVehicule.PRV_ID LEFT OUTER JOIN" + " ARV_ARVAL ON VEH_VEHICULE.ARV_ID = ARV_ARVAL.ARV_ID";

                General.sSQL = General.sSQL + sWhere + sOrderBy;





                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                // Start Excel and get Application object.
                oXL = new Microsoft.Office.Interop.Excel.Application();
                oXL.Visible = false;

                // Get a new workbook.
                oWB = oXL.Workbooks.Add();
                oSheet = oWB.ActiveSheet;
                ModAdo tmp = new ModAdo();
                rsExport = tmp.fc_OpenRecordSet(General.sSQL);


                if (rsExport.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucun enregistrement pour ces critères.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }


                //== affiche les noms des champs.

                i = 1;
                foreach (DataColumn oField in rsExport.Columns)
                {
                    oSheet.Cells[1, i].VerticalAlignment = Microsoft.Office.Interop.Excel.XlVAlign.xlVAlignCenter;
                    oSheet.Cells[1, i].value = oField.ColumnName;
                    i = i + 1;
                }


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                // Starting at E1, fill headers for the number of columns selected.
                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];

                oResizeRange.Interior.ColorIndex = 15;
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oResizeRange = oSheet.Range["A1", "B1"].Resize[ColumnSize: i - 1];
                oResizeRange.Borders.Weight = Microsoft.Office.Interop.Excel.XlBorderWeight.xlThin;

                lLigne = 3;

                //_with1.MoveFirst();

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                foreach (DataRow rsExportRows in rsExport.Rows)
                {
                    j = 1;
                    //  oSheet.Cells(lLigne, 1).value = sANT_MatCode
                    foreach (DataColumn rsExportCol in rsExport.Columns)
                    {

                        Debug.Print(rsExportCol.ColumnName);


                        if (General.UCase(rsExportCol.ColumnName) == General.UCase("Code Prise/rest"))
                        {
                            if (Convert.ToInt32(rsExportRows["Code Prise/rest"]) == 1)
                            {

                                oSheet.Cells[lLigne, j + 0].value = "prise véhicule";
                            }
                            else if (Convert.ToInt32(rsExportRows["Code Prise/rest"]) == 2)
                            {

                                oSheet.Cells[lLigne, j + 0].value = "restitution véhicule";
                            }
                            else
                            {

                                oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];
                            }

                        }
                        else
                        {
                            oSheet.Cells[lLigne, j + 0].value = rsExportRows[rsExportCol.ColumnName];

                        }
                        j = j + 1;
                    }
                    lLigne = lLigne + 1;
                    // _with1.MoveNext();
                }

                ///oResizeRange.BorderAround LineStyle:=xlDot, ColorIndex:=3, Weight:=xlThick, color:=xlColorIndexAutomatic
                //formate la ligne en gras.
                oResizeRange.Font.Bold = true;

                oXL.Visible = true;
                oXL.UserControl = true;

                oRng = null;

                oSheet = null;

                oWB = null;

                oXL = null;

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                rsExport.Dispose();

                rsExport = null;


                return;
            }
            catch (Exception ex)
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(ex.Message, "Error: " + ex.Source, MessageBoxButtons.OK, MessageBoxIcon.Error);

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFindArval_Click(object sender, EventArgs e)
        {
            txtCodeArval_KeyPress(txtCodeArval, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFindFlocage_Click(object sender, EventArgs e)
        {
            txtCodeFlocage_KeyPress(txtCodeFlocage, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheCommercial_Click(object sender, EventArgs e)
        {
            txtImmatriculation_KeyPress(txtImmatriculation, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmdVehicule_Click(object sender, EventArgs e)
        {
            fc_LoadVehicule();
        }
        /// <summary>
        /// TESTED
        /// </summary>
        private void fc_LoadVehicule()
        {
            string sSQL = null;

            DataTable rs = default(DataTable);
            try
            {

                sSQL = "SELECT     VEH_VEHICULE.VEH_ID, VEH_VEHICULE.VEI_Immatriculation, VEH_VEHICULE.VEH_Kilometrage, VEH_VEHICULE.VEH_PritOuRestitution, "
                        + " '' AS PrisRestLibelle, VEH_VEHICULE.Matricule, Personnel.Nom, Personnel.Prenom, VEH_VEHICULE.PRV_ID, PRV_ProblemeVehicule.PRV_Libelle, "
                        + " VEH_VEHICULE.VEH_CommSurProbleme, VEH_VEHICULE.VEH_VoyantOrange, VEH_VEHICULE.ARV_ID, ARV_ARVAL.ARV_Libelle, "
                        + " VEH_VEHICULE.VEH_NettoyageInterieur, VEH_VEHICULE.VEH_NettoyageInterieurDate, VEH_VEHICULE.VEH_NettoyageExterieur, "
                        + " VEH_VEHICULE.VEH_NettoyageExterieurDate, VEH_VEHICULE.FLO_ID, FLO_FLOCAGE.FLO_Libelle, VEH_VEHICULE.VEH_CreeLe,"
                        + " VEH_VEHICULE.VEH_CreePar , VEH_VEHICULE.VEH_ModifieLe, VEH_VEHICULE.VEH_ModifierPar ";

                sSQL = sSQL + " FROM         VEH_VEHICULE LEFT OUTER JOIN "
                        + " Personnel ON VEH_VEHICULE.Matricule = Personnel.Matricule LEFT OUTER JOIN "
                        + " FLO_FLOCAGE ON VEH_VEHICULE.FLO_ID = FLO_FLOCAGE.FLO_ID LEFT OUTER JOIN "
                        + " PRV_ProblemeVehicule ON VEH_VEHICULE.PRV_ID = PRV_ProblemeVehicule.PRV_ID LEFT OUTER JOIN "
                        + " ARV_ARVAL ON VEH_VEHICULE.ARV_ID = ARV_ARVAL.ARV_ID";


                sWhere = "";

                if (!string.IsNullOrEmpty(txtImmatriculation.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE VEH_VEHICULE.VEI_Immatriculation ='" + StdSQLchaine.gFr_DoublerQuote(txtImmatriculation.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND VEH_VEHICULE.VEI_Immatriculation ='" + StdSQLchaine.gFr_DoublerQuote(txtImmatriculation.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtPRV.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE VEH_VEHICULE.PRV_ID ='" + StdSQLchaine.gFr_DoublerQuote(txtPRV.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND VEH_VEHICULE.PRV_ID ='" + StdSQLchaine.gFr_DoublerQuote(txtPRV.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtCodeArval.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE VEH_VEHICULE.ARV_ID ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeArval.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND VEH_VEHICULE.ARV_ID ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeArval.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtCodeFlocage.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE VEH_VEHICULE.FLO_ID ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeFlocage.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND VEH_VEHICULE.FLO_ID ='" + StdSQLchaine.gFr_DoublerQuote(txtCodeFlocage.Text) + "'";
                    }
                }

                if (!string.IsNullOrEmpty(txtIntervenant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {

                        sWhere = " WHERE VEH_VEHICULE.Matricule ='" + StdSQLchaine.gFr_DoublerQuote(txtIntervenant.Text) + "'";
                    }
                    else
                    {

                        sWhere = sWhere + " AND VEH_VEHICULE.Matricule ='" + StdSQLchaine.gFr_DoublerQuote(txtIntervenant.Text) + "'";
                    }
                }


                if (chkVoyant.Checked)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE VEH_VEHICULE.VEH_VoyantOrange = 1 ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND VEH_VEHICULE.VEH_VoyantOrange = 1";
                    }
                }


                if (OptPrise.Checked == true)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE VEH_VEHICULE.VEH_PritOuRestitution = 1 ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND VEH_VEHICULE.VEH_PritOuRestitution = 1";
                    }

                }
                else if (OptRest.Checked == true)
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE VEH_VEHICULE.VEH_PritOuRestitution = 2 ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND VEH_VEHICULE.VEH_PritOuRestitution = 2";
                    }
                }


                rsVeh = ModAdorsVeh.fc_OpenRecordSet(sSQL + sWhere + sOrderBy);


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                GridVehicule.DataSource = rsVeh;
                GridVehicule.UpdateData();
                FC_SaveParam();

                return;
            } catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadVehicule");
            }


        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFndInterv_Click(object sender, EventArgs e)
        {
            txtIntervenant_KeyPress(txtIntervenant, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));

        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command1_Click(object sender, EventArgs e)
        {
            txtPRV_KeyPress(txtPRV, new System.Windows.Forms.KeyPressEventArgs((Char)(13)));

        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridVehicule_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            var _with2 = GridVehicule;
            try
            {
                if (e.Row.Cells["VEH_PritOuRestitution"].Text == "1")
                {
                    e.Row.Cells["PrisRestLibelle"].Value = "prise véhicule";
                }
                else if (e.Row.Cells["VEH_PritOuRestitution"].Text == "2")
                {
                    e.Row.Cells["PrisRestLibelle"].Value = "restitution véhicule";
                }
                GridVehicule.UpdateData();
                return;
            } catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";GridVehicule_RowLoaded");
            }


        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeArval_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                ModAdo tmp = new ModAdo();
                string req = "SELECT     ARV_ID AS \"Code Arval\", ARV_Libelle AS \"Libellè\" " + " FROM         ARV_ARVAL ";
                string where = "";
                if (KeyAscii == 13)
                {

                    if (!!string.IsNullOrEmpty(General.nz(tmp.fc_ADOlibelle("SELECT ARV_ID From ARV_ARVAL WHERE ARV_ID = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeArval.Text) + "'"), "").ToString()))
                    {
                        SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche du code arval" };

                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtCodeArval.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            lblLibArval.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();

                            fg.Dispose(); fg.Close();
                        };
                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                txtCodeArval.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                lblLibArval.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();

                                fg.Dispose(); fg.Close();
                            }

                        };
                        fg.StartPosition = FormStartPosition.CenterScreen;
                        fg.ShowDialog();

                    }
                    //=== charge la grille.
                    CmdVehicule_Click(CmdVehicule, new System.EventArgs());
                }
            } catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCodeArval_KeyPress");

            }



        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFlocage_TextChanged(object sender, EventArgs e)
        {
            using (var tmp = new ModAdo()) { 
                lblLibFlocage.Text = tmp.fc_ADOlibelle("SELECT FLO_Libelle From FLO_FLOCAGE WHERE FLO_ID = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeFlocage.Text) + "'");
        }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtCodeFlocage_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                ModAdo tmp = new ModAdo();
                string req = "SELECT     FLO_ID AS \"Code Flocage\", FLO_Libelle AS \"Libellè\" " + " FROM         FLO_FLOCAGE ";
                string where = "";
                if (KeyAscii == 13)
                {

                    if (!!string.IsNullOrEmpty(General.nz(tmp.fc_ADOlibelle("SELECT FLO_ID From FLO_FLOCAGE WHERE FLO_ID  = '" + StdSQLchaine.gFr_DoublerQuote(txtCodeFlocage.Text) + "'"), "").ToString()))
                    {
                        SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche de code flocage" };

                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtCodeFlocage.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            lblLibFlocage.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();

                            fg.Dispose(); fg.Close();
                        };
                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                txtCodeFlocage.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                lblLibFlocage.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();

                                fg.Dispose(); fg.Close();
                            }

                        };
                        fg.StartPosition = FormStartPosition.CenterScreen;
                        fg.ShowDialog();

                    }
                    //=== charge la grille.
                    CmdVehicule_Click(CmdVehicule, new System.EventArgs());
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCodeArval_KeyPress");

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtImmatriculation_KeyPress(object sender, KeyPressEventArgs e)
        {

            short KeyAscii = (short)e.KeyChar;
            try
            {
                ModAdo tmp = new ModAdo();
                string req = "SELECT     VEI_Immatriculation AS \"Immatriculation\" " + " From VEI_Immatriculation ";
                string where = "";
                if (KeyAscii == 13)
                {

                    if (!!string.IsNullOrEmpty(General.nz(tmp.fc_ADOlibelle("SELECT VEI_Immatriculation From VEI_Immatriculation WHERE VEI_Immatriculation  = '" 
                        + StdSQLchaine.gFr_DoublerQuote(txtImmatriculation.Text) + "'"), "").ToString()))
                    {
                        SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'immatriculation" };
                        fg.SetValues(new Dictionary<string, string> { { "VEI_Immatriculation", txtImmatriculation.Text  } });

                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtImmatriculation.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();


                            fg.Dispose(); fg.Close();
                        };
                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                txtImmatriculation.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                             
                                fg.Dispose(); fg.Close();
                            }

                        };
                        fg.StartPosition = FormStartPosition.CenterScreen;
                        fg.ShowDialog();

                    }
                    //=== charge la grille.
                    CmdVehicule_Click(CmdVehicule, new System.EventArgs());
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCodeArval_KeyPress");

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIntervenant_TextChanged(object sender, EventArgs e)
        {
            using (var tmp = new ModAdo())
            {
                lbllibIntervenant.Text = tmp.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel " + " WHERE Personnel.Matricule = '" + StdSQLchaine.gFr_DoublerQuote(txtIntervenant.Text) + "'");
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIntervenant_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)e.KeyChar;
            try
            {
                ModAdo tmp = new ModAdo();
                string req = "SELECT Personnel.Matricule as \"Matricule\"," 
                    + " Personnel.Nom as \"Nom\"," + " Personnel.prenom as \"Prenom\"," 
                    + " Qualification.Qualification as \"Qualification\"," 
                    + " Personnel.Memoguard as \"MemoGuard\"," 
                    + " Personnel.NumRadio as \"NumRadio\""
                    + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif ";

                string where = "(NonActif is null or NonActif = 0)";

                if (KeyAscii == 13)
                {

                    if (!!string.IsNullOrEmpty(General.nz(tmp.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule  = '"
                        + StdSQLchaine.gFr_DoublerQuote(txtIntervenant.Text) + "'"), "").ToString()))
                    {
                        SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche d'intervenant" };
                       
                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                            lbllibIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();

                            fg.Dispose(); fg.Close();
                        };
                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                txtIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();

                                lbllibIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();

                                fg.Dispose(); fg.Close();
                            }

                        };
                        fg.StartPosition = FormStartPosition.CenterScreen;
                        fg.ShowDialog();

                    }
                    //=== charge la grille.
                    CmdVehicule_Click(CmdVehicule, new System.EventArgs());
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCodeArval_KeyPress");

            }
        }
        /// <summary>
        /// tESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPRV_KeyPress(object sender, KeyPressEventArgs e)
        {

            short KeyAscii = (short)e.KeyChar;
            try
            {
                ModAdo tmp = new ModAdo();
                string req = "SELECT     PRV_ID AS \"Code Problème\", PRV_Libelle AS \"Libellé\" " + " FROM PRV_ProblemeVehicule ";

                string where = "";

                if (KeyAscii == 13)
                {

                    if (!!string.IsNullOrEmpty(General.nz(tmp.fc_ADOlibelle("SELECT PRV_ID From PRV_ProblemeVehicule WHERE PRV_ID  = '"
                        + StdSQLchaine.gFr_DoublerQuote(txtPRV.Text) + "'"), "").ToString()))
                    {
                        SearchTemplate fg = new SearchTemplate(this, null, req, where, "") { Text = "Recherche " };

                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {
                            txtPRV.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            lblLibProbleme.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();
                           
                            fg.Dispose(); fg.Close();
                        };
                        fg.ugResultat.KeyDown += (se, ev) =>
                        {
                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                txtPRV.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                lblLibProbleme.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString();

                                fg.Dispose(); fg.Close();
                            }

                        };
                        fg.StartPosition = FormStartPosition.CenterScreen;
                        fg.ShowDialog();

                    }
                    //=== charge la grille.
                    CmdVehicule_Click(CmdVehicule, new System.EventArgs());
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";txtCodeArval_KeyPress");

            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtPRV_TextChanged(object sender, EventArgs e)
        {
            using (var tmp = new ModAdo())
            {
                lblLibProbleme.Text = tmp.fc_ADOlibelle("SELECT PRV_Libelle From PRV_ProblemeVehicule WHERE PRV_ID= '" + StdSQLchaine.gFr_DoublerQuote(txtPRV.Text) + "'");
            }
        }
        /// <summary>
        /// TESTED
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridVehicule_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
           
            GridVehicule.DisplayLayout.Bands[0].Columns["VEH_ID"].Header.Caption = "ID";
            GridVehicule.DisplayLayout.Bands[0].Columns["VEI_Immatriculation"].Header.Caption = "Immatriculation";
            GridVehicule.DisplayLayout.Bands[0].Columns["VEH_Kilometrage"].Header.Caption = "Kilometrage";
            GridVehicule.DisplayLayout.Bands[0].Columns["VEH_PritOuRestitution"].Hidden = true;
            GridVehicule.DisplayLayout.Bands[0].Columns["PrisRestLibelle"].Header.Caption = "Restitution/Prise";
            GridVehicule.DisplayLayout.Bands[0].Columns["PRV_ID"].Header.Caption = "Code Probème";
            GridVehicule.DisplayLayout.Bands[0].Columns["PRV_Libelle"].Header.Caption = "Probème";
            GridVehicule.DisplayLayout.Bands[0].Columns["VEH_CommSurProbleme"].Header.Caption = "Commentaire";
            GridVehicule.DisplayLayout.Bands[0].Columns["VEH_VoyantOrange"].Header.Caption = "VoyantOrange";
            GridVehicule.DisplayLayout.Bands[0].Columns["ARV_ID"].Header.Caption = "Code arval";
            GridVehicule.DisplayLayout.Bands[0].Columns["ARV_Libelle"].Header.Caption = "Arval";
            GridVehicule.DisplayLayout.Bands[0].Columns["VEH_NettoyageInterieur"].Header.Caption = "Nettoyage Interieur";
            GridVehicule.DisplayLayout.Bands[0].Columns["VEH_NettoyageInterieurDate"].Header.Caption = "Date Nettoyage Interieur";
            GridVehicule.DisplayLayout.Bands[0].Columns["VEH_NettoyageExterieur"].Header.Caption = "Nettoyage Exterieur";
            GridVehicule.DisplayLayout.Bands[0].Columns["VEH_NettoyageExterieurDate"].Header.Caption = "Date Nettoyage Exterieur";
            GridVehicule.DisplayLayout.Bands[0].Columns["FLO_ID"].Header.Caption = "Code Flocage";
            GridVehicule.DisplayLayout.Bands[0].Columns["FLO_Libelle"].Header.Caption = "Flocage";
            GridVehicule.DisplayLayout.Bands[0].Columns["VEH_CreeLe"].Header.Caption = "Crée Le";
            GridVehicule.DisplayLayout.Bands[0].Columns["VEH_CreePar"].Header.Caption = "Crée Par";
            GridVehicule.DisplayLayout.Bands[0].Columns["VEH_ModifieLe"].Header.Caption = "Modifié Le";
            GridVehicule.DisplayLayout.Bands[0].Columns["VEH_ModifierPar"].Header.Caption = "Modifié Par";
         

        }
    }
    }




