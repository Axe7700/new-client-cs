﻿using Axe_interDT.Shared;
using System;
using System.Data;
using System.Net.Http;
using System.Text;
using System.Web.Helpers;
using System.Windows.Forms;

namespace Axe_interDT.Views.Planning.Forms
{
    /// <summary>
    /// Mondir le 01.07.2021 demande de Rachid pour le nouveau client
    /// </summary>
    public partial class frmTriImmeublesParDistance : Form
    {
        public DataTable Adodc3;
        public string sSQL;
        public string ApiUrl;

        public frmTriImmeublesParDistance()
        {
            InitializeComponent();
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            try
            {
                string JSonRequest = "";
                string sAdd = "";
                string point = "";

                JSonRequest = "{";
                JSonRequest = JSonRequest + "\"Starts\": [" + cmbDepart.Rows[cmbDepart.Rows.Count - 1].Cells[0].Text + "], ";
                JSonRequest = JSonRequest + "\"Ends\": [" + cmbArrive.Rows[cmbDepart.Rows.Count - 1].Cells[0].Text + "],";
                JSonRequest = JSonRequest + "\"Avoids\": [],"; // Valeurs possible pour eviter les autoroutes : tolls,highways,ferries,indoor
                JSonRequest = JSonRequest + "\"Points\": [";

                for (int i = 0; i < Adodc3.Rows.Count; i++)
                {
                    var Adodc3Row = Adodc3.Rows[0];

                    JSonRequest = JSonRequest + "{";
                    JSonRequest = JSonRequest + "\"IsStart\": " + (Adodc3Row["CodeImmeuble"].ToString() == cmbDepart.Text ? "true" : "false") + ",";
                    JSonRequest = JSonRequest + "\"IsEnd\": " + (Adodc3Row["CodeImmeuble"].ToString() == cmbArrive.Text ? "true" : "false") + ",";
                    JSonRequest = JSonRequest + "\"DistanceFromPreviousInKm\": \"\",";
                    JSonRequest = JSonRequest + "\"DistanceFromPreviousInMeter\": 0,";
                    JSonRequest = JSonRequest + "\"Adresse\": \"" + Adodc3Row["Adresse"] + "\",";
                    JSonRequest = JSonRequest + "\"CodeImmeuble\": \"" + Adodc3Row["CodeImmeuble"] + "\",";
                    JSonRequest = JSonRequest + "\"Location\": \"" + Adodc3Row["latitude"] + ", " + Adodc3Row["longitude"] + "\"";
                    JSonRequest = JSonRequest + "}";

                    if (i == Adodc3.Rows.Count - 1)
                    {
                        // last record!
                        JSonRequest = JSonRequest + ",";
                    }

                    //sAdd = "§" & Adodc3.Recordset!CodeImmeuble & "§@"
                    //sAdd = sAdd & "§" & Adodc3.Recordset!NoIntervention & "§@"
                    //sAdd = sAdd & "§" & Adodc3.Recordset!Intervenant & "§@"
                    //sAdd = sAdd & "§" & Adodc3.Recordset!Nom & "§@"
                    //sAdd = sAdd & "§" & Adodc3.Recordset!DateSaisie & "§@"
                    //sAdd = sAdd & "§" & Adodc3.Recordset!DatePrevue & "§@"
                    //sAdd = sAdd & "§" & Adodc3.Recordset!DateVisite & "$@"
                }

                JSonRequest = JSonRequest + "]}";

                GetImmeubles(JSonRequest);

            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
            }
        }

        private async void GetImmeubles(string point)
        {
            using (var httpClient = new HttpClient())
            {
                var response = await httpClient.PostAsync(ApiUrl, new StringContent(point, Encoding.UTF8, "application/json"));

                var r = await response.Content.ReadAsStringAsync();

                dynamic data = Json.Decode(r);


                //For Each immeuble In Response.Item("immeubles")
                //Debug.Print("codeImmeuble=>" & immeuble.Item("codeImmeuble") & "[" & immeuble.Item("distanceFromPreviousInKm") & "]")

                //Next
            }
        }

        private void frmTriImmeublesParDistance_Load(object sender, EventArgs e)
        {
            //a recupere depuis la table line
            ApiUrl = "http://localhost:49259/api/GetShortestPath";
        }

        private void cmbDepart_BeforeDropDown(object sender, System.ComponentModel.CancelEventArgs e)
        {
            sheridan.InitialiseCombo(cmbArrive, sSQL, "CodeImmeuble");
        }

        private void cmbArrive_BeforeDropDown(object sender, System.ComponentModel.CancelEventArgs e)
        {
            sheridan.InitialiseCombo(cmbDepart, sSQL, "CodeImmeuble");
        }
    }
}
