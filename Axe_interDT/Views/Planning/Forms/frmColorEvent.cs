﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Axe_interDT.Views.Planning.Forms
{
    public partial class frmColorEvent : Form
    {
        public frmColorEvent()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMAJ_Click(object sender, EventArgs e)
        {
            GridCdEtat.UpdateData();
        }

        private DataTable rsCdEtat;
        private ModAdo ModAdoUpdate;

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadCdEtat()
        {
            string sSQL;
            string sCd;
            try
            {
                sSQL = "SELECT     CodeEtat, LibelleCodeEtat, CouleurStatut From TypeCodeEtat  ORDER BY CodeEtat";
                Cursor.Current = Cursors.WaitCursor;
                ModAdoUpdate = new ModAdo();
                rsCdEtat = ModAdoUpdate.fc_OpenRecordSet(sSQL);
                //foreach (DataRow dr in rsCdEtat.Rows)
                //{
                //    if (General.nz(dr["CouleurStatut"], "") + "" != "")
                //    {
                //        sCd = dr["CodeEtat"].ToString();
                //        //GridCdEtat.StyleSets(sCd).BackColor = CLng(nz(rsCdEtat!CouleurStatut, 0))
                //        GridCdEtat.BackColor = Color.FromArgb(Convert.ToInt32(dr["CouleurStatut"]));
                //    }
                //}
                Cursor.Current = Cursors.Default;
                GridCdEtat.DataSource = rsCdEtat;
                GridCdEtat.UpdateData();
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, Name + ";fc_LoadCdEtat");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCdEtat_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            if (e.Cell.Column.Index == GridCdEtat.ActiveRow.Cells["CouleurStatut"].Column.Index)
            {
                CD1.ShowDialog();
                e.Cell.Value = ColorTranslator.ToOle(CD1.Color);
            }
        }

        /// <summary>
        /// Function Not Used ==============> Code Changed By Mondir
        /// </summary>
        private void fc_SetColor()
        {
            int i;
            bool bexist;
            string sCd;
            try
            {
                bexist = false;
                if (GridCdEtat.ActiveRow.Cells["CouleurStatut"].Text != "")
                {
                    //sCd = GridCdEtat.ActiveRow.Cells["CodeEtat"].Value.ToString();



                    //        For i = 0 To GridCdEtat.StyleSets.Count - 1


                    //    If UCase(.Columns("CodeEtat").value) = UCase(GridCdEtat.StyleSets(i).Name) Then
                    //        bexist = True
                    //        GridCdEtat.StyleSets(sCd).BackColor = CLng(nz(.Columns("CouleurStatut").value, 0))
                    //        .Columns(.Columns("CouleurStatut").Position).CellStyleSet GridCdEtat.StyleSets(i).Name, .Row
                    //    End If
                    //Next


                    //If bexist = False Then


                    //     GridCdEtat.StyleSets(sCd).BackColor = CLng(nz(.Columns("CouleurStatut").value, 0))
                    //     .Columns(.Columns("CouleurStatut").Position).CellStyleSet sCd, .Row
                    //End If
                }
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, Name + ";fc_SetColor");
            }
        }

        /// <summary>
        /// Delete is Not Allowed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCdEtat_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "",
                    MessageBoxButtons.YesNo, MessageBoxIcon.Question, MessageBoxDefaultButton.Button2) ==
                DialogResult.No)
                e.Cancel = true;
        }

        /// <summary>
        /// Delete is Not Allowed
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCdEtat_AfterRowsDeleted(object sender, EventArgs e)
        {
            ModAdoUpdate.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCdEtat_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            var xx = ModAdoUpdate.Update();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCdEtat_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            try
            {
                //==============> Code Modifié par mondir !
                //fc_SetColor();

                if (e.Row.Cells["CouleurStatut"].Text != "")
                {
                    var color = ColorTranslator.FromOle(Convert.ToInt32(e.Row.Cells["CouleurStatut"].Text));
                    e.Row.Cells["CouleurStatut"].Appearance.BackColor = color;
                    e.Row.Cells["CouleurStatut"].Appearance.BackColor2 = color;
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridCdEtat_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridCdEtat.DisplayLayout.Bands[0].Columns["CouleurStatut"].Style = ColumnStyle.Button;
            GridCdEtat.DisplayLayout.Bands[0].Columns["CouleurStatut"].Header.Caption = "Couleur De Fond";

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmColorEvent_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_LoadCdEtat();
        }
    }
}
