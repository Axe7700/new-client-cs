﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using ColumnStyle = Infragistics.Win.UltraWinGrid.ColumnStyle;

namespace Axe_interDT.Views.Planning.Forms
{
    public partial class frmImmColor : Form
    {
        ModAdo modAdorsBcImm;
        DataTable rsBcImm;
        public frmImmColor()
        {
            InitializeComponent();
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAddImmeuble_Click(object sender, EventArgs e)
        {
            string requete = "SELECT     CodeImmeuble AS \"Code immeuble\", Adresse as \"Adresse\", Ville as \"Ville\", Code1 AS [Code Gérant]  From Immeuble";
            string where_order = "";
            SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "Recherche d'un immeuble" };
            fg.ugResultat.DoubleClickRow += (se, ev) =>
            {
                var newRow = rsBcImm.NewRow();
                newRow["CodeImmeuble"] = fg.ugResultat.ActiveRow.Cells["Code immeuble"].Value;
                rsBcImm.Rows.Add(newRow);
                var xx = modAdorsBcImm.Update();
                fc_LoadImmColor();
                fg.Dispose();
                fg.Close();
            };

            fg.ugResultat.KeyDown += (se, ev) =>
            {

                if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                {
                    var newRow = rsBcImm.NewRow();
                    newRow["CodeImmeuble"] = fg.ugResultat.ActiveRow.Cells["Code immeuble"].Value;
                    rsBcImm.Rows.Add(newRow);
                    modAdorsBcImm.Update();
                    fc_LoadImmColor();
                    fg.Dispose();
                    fg.Close();
                }
            };
            fg.StartPosition = FormStartPosition.CenterParent;
            fg.ShowDialog();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdMAJ_Click(object sender, EventArgs e)
        {
            GridBCimm.UpdateData();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmImmColor_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_LoadImmColor();
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadImmColor()
        {
            var sSQL = "";
            var sCd = "";
            try
            {
                sSQL = "SELECT     CodeImmeuble, CouleurDeFond"
                    + " From ImmeubleColor "
                    + " ORDER BY CodeImmeuble";
                modAdorsBcImm = new ModAdo();
                rsBcImm = modAdorsBcImm.fc_OpenRecordSet(sSQL);
                GridBCimm.DataSource = rsBcImm;
                GridBCimm.UpdateData();
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridBCimm_ClickCellButton(object sender, Infragistics.Win.UltraWinGrid.CellEventArgs e)
        {
            if (e.Cell.Column.Index == GridBCimm.ActiveRow.Cells["CouleurDeFond"].Column.Index)
            {
                CD1.ShowDialog();
                e.Cell.Value = ColorTranslator.ToOle(CD1.Color);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridBCimm_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            //fc_SetColor();
            try
            {
                //==============> Code Modifié par mondir !
                //fc_SetColor();

                if (e.Row.Cells["CouleurDeFond"].Text != "")
                {
                    var color = ColorTranslator.FromOle(Convert.ToInt32(e.Row.Cells["CouleurDeFond"].Text));
                    e.Row.Cells["CouleurDeFond"].Appearance.BackColor = color;
                    e.Row.Cells["CouleurDeFond"].Appearance.BackColor2 = color;
                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridBCimm_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {
            GridBCimm.DisplayLayout.Bands[0].Columns["CouleurDeFond"].Style = ColumnStyle.Button;
            GridBCimm.DisplayLayout.Bands[0].Columns["CouleurDeFond"].Header.Caption = "Couleur De Fond";
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void GridBCimm_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            var xx = modAdorsBcImm.Update();
        }

        private void GridBCimm_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
            {
                e.Cancel = true;
                return;
            }
        }

        private void GridBCimm_AfterRowsDeleted(object sender, EventArgs e)
        {
            var xx = modAdorsBcImm.Update();
        }
    }
}
