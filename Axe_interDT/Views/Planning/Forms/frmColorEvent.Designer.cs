﻿namespace Axe_interDT.Views.Planning.Forms
{
    partial class frmColorEvent
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.cmdMAJ = new System.Windows.Forms.Button();
            this.GridCdEtat = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.CD1 = new System.Windows.Forms.ColorDialog();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.GridCdEtat)).BeginInit();
            this.SuspendLayout();
            // 
            // cmdMAJ
            // 
            this.cmdMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJ.FlatAppearance.BorderSize = 0;
            this.cmdMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMAJ.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdMAJ.Location = new System.Drawing.Point(515, 11);
            this.cmdMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJ.Name = "cmdMAJ";
            this.cmdMAJ.Size = new System.Drawing.Size(60, 35);
            this.cmdMAJ.TabIndex = 413;
            this.cmdMAJ.Tag = "";
            this.toolTip1.SetToolTip(this.cmdMAJ, "Enregistrer");
            this.cmdMAJ.UseVisualStyleBackColor = false;
            this.cmdMAJ.Click += new System.EventHandler(this.cmdMAJ_Click);
            // 
            // GridCdEtat
            // 
            this.GridCdEtat.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.GridCdEtat.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridCdEtat.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridCdEtat.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.False;
            this.GridCdEtat.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridCdEtat.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridCdEtat.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridCdEtat.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.GridCdEtat.DisplayLayout.UseFixedHeaders = true;
            this.GridCdEtat.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.GridCdEtat.Location = new System.Drawing.Point(2, 51);
            this.GridCdEtat.Name = "GridCdEtat";
            this.GridCdEtat.Size = new System.Drawing.Size(572, 342);
            this.GridCdEtat.TabIndex = 572;
            this.GridCdEtat.Text = "Code état";
            this.GridCdEtat.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridCdEtat_InitializeLayout);
            this.GridCdEtat.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.GridCdEtat_InitializeRow);
            this.GridCdEtat.AfterRowsDeleted += new System.EventHandler(this.GridCdEtat_AfterRowsDeleted);
            this.GridCdEtat.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.GridCdEtat_AfterRowUpdate);
            this.GridCdEtat.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.GridCdEtat_ClickCellButton);
            this.GridCdEtat.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridCdEtat_BeforeRowsDeleted);
            // 
            // frmColorEvent
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(586, 394);
            this.Controls.Add(this.GridCdEtat);
            this.Controls.Add(this.cmdMAJ);
            this.Name = "frmColorEvent";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Code état";
            this.Load += new System.EventHandler(this.frmColorEvent_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridCdEtat)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion
        public System.Windows.Forms.Button cmdMAJ;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridCdEtat;
        private System.Windows.Forms.ColorDialog CD1;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}