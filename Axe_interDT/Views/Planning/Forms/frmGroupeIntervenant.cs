﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Infragistics.Win;
using Infragistics.Win.UltraWinTree;
using Microsoft.VisualBasic;

namespace Axe_interDT.Views.Planning.Forms
{
    public partial class frmGroupeIntervenant : Form
    {
        public frmGroupeIntervenant()
        {
            InitializeComponent();
        }
        const string cNiv1 = "A";
        DataTable rsGrid = new DataTable();
        ModAdo ModAdoUpdate = new ModAdo();
        /// <summary>
        /// Tested
        /// </summary>
        private void fc_treevieuw()
        {
            string sSQL = null;
            DataTable rs = default(DataTable);
            int lGRI_ID = 0;

            UltraTreeNode node = new UltraTreeNode();

            try
            {
                treev.Nodes.Clear();
                lGRI_ID = 0;


                sSQL = "SELECT     GRI_ID, GRI_Nom From GRI_GroupeIntervenant ORDER BY GRI_Nom";
                var ModAdo = new ModAdo();
                rs = ModAdo.fc_OpenRecordSet(sSQL);

                if (rs.Rows.Count == 0)
                {
                    FraTrew.Visible = false;
                    ModAdo.fc_CloseRecordset(rs);
                    rs = null;
                    return;
                }

                FraTrew.Visible = true;

                foreach (DataRow dr in rs.Rows)
                {

                    //=== Ajout du premier niveau de hiérarchie des localisations.
                    if (lGRI_ID != Convert.ToInt32(dr["GRI_ID"]))
                    {
                        node = treev.Nodes.Add(cNiv1 + dr["GRI_ID"], dr["GRI_Nom"] + "");
                        node.Override.NodeAppearance.Image = Properties.Resources.close_folder;
                        treev.Nodes[cNiv1 + dr["GRI_ID"]].Expanded = true;


                        lGRI_ID = Convert.ToInt32(dr["GRI_ID"]);
                    }

                }

                ModAdo.fc_CloseRecordset(rs);
                rs = null;
                //treev(0).LabelEdit =

                //System.Windows.Forms.TreeNode oNode = null;
                foreach (UltraTreeNode oNodeParent in treev.Nodes)
                {

                    oNodeParent.Selected = true;
                    //treev_NodeClick(treev, new System.Windows.Forms.TreeNodeMouseClickEventArgs(oNode, System.Windows.Forms.MouseButtons.None, 0, 0, 0));//TODO
                    break;

                }


                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_treevCompteur");
            }

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cmd_Click(object sender, EventArgs e)
        {

            string sSQL = null;

            int lGRI_ID = 0;
            DataTable rs = default(DataTable);
            try
            {
                Grid_Grid.UpdateData();
                if (treev.Nodes.Count == 0)
                {
                    return;
                }
                foreach (UltraTreeNode oNodeParent in treev.Nodes)
                {

                    if (oNodeParent.Selected == true)
                    {
                        if (General.UCase(General.Left(oNodeParent.Key, 1)) == General.UCase(cNiv1))
                        {
                            lGRI_ID = Convert.ToInt32(General.Right(oNodeParent.Key, General.Len(oNodeParent.Key) - 1));
                            break;
                        }
                        else
                        {
                            return;
                        }
                    }

                }
                if (lGRI_ID == 0)
                {
                    return;
                }
                //==== modif du 01 03 2017; ajout des champs CodeUnite et UniteDELecture
                sSQL = " SELECT GRI_ID, GRI_Nom From GRI_GroupeIntervenant Where GRI_ID = " + lGRI_ID;
                var ModAdo = new ModAdo();
                rs = ModAdo.fc_OpenRecordSet(sSQL);
                if (rs.Rows.Count == 0)
                {
                    ModAdo.fc_CloseRecordset(rs);
                    rs = null;
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Aucune modification effectuée.", "Erreur", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    return;
                }
                txtGRI_Nom.Text = General.Trim(txtGRI_Nom.Text);
                rs.Rows[0]["GRI_Nom"] = General.Trim(txtGRI_Nom.Text);
                ModAdo.Update();
                ModAdo.fc_CloseRecordset(rs);
                rs = null;
                foreach (UltraTreeNode oNodeParent in treev.Nodes)
                {

                    if (oNodeParent.Selected == true)
                    {
                        oNodeParent.Text = General.Trim(txtGRI_Nom.Text);

                        break;
                    }

                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Cmd_Click");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAjouter_Click(object sender, EventArgs e)
        {
            string sNameGroup = null;
            string sSQL = null;
            DataTable rs = default(DataTable);
            try
            {
                if (Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Voulez-vous créer un nouveau groupe d'intervenant ?", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question) == DialogResult.No)
                {
                    return;
                }
                sNameGroup = Interaction.InputBox("Veuillez saisir un nom pour le nouveau groupe d'intervenant.", "Ajout d'un groupe");
                if (string.IsNullOrEmpty(sNameGroup))
                {
                    return;
                }
                sNameGroup = General.Trim(sNameGroup);

                sSQL = "SELECT GRI_ID, GRI_Nom From GRI_GroupeIntervenant WHERE GRI_ID = 0";
                var ModAdo = new ModAdo();
                rs = ModAdo.fc_OpenRecordSet(sSQL);
                var dr = rs.NewRow();
                dr["GRI_Nom"] = sNameGroup;
                rs.Rows.Add(dr);
                ModAdo.Update();
                ModAdo.Close();
                rs = null;
                fc_treevieuw();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdAjouter_Click");
            }
        }
        /// <summary>
        /// TEsted
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmGroupeIntervenant_Load(object sender, EventArgs e)
        {
            View.Theme.Theme.recursiveLoopOnFrms(this);

            fc_LoadDropInterv();
            fc_treevieuw();
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void fc_LoadDropInterv()
        {
            string sSQL = null;


            sSQL = "SELECT Matricule, Nom, Prenom";
            sSQL = sSQL + " FROM Personnel LEFT JOIN SpecifQualif ON ";
            sSQL = sSQL + " Personnel.CodeQualif=SpecifQualif.CodeQualif ";


            sheridan.InitialiseCombo((this.DropINterv), sSQL, "Matricule", true);

            //Grid_Grid.Columns["GRID_Mat"].DropDownHwnd = this.DropINterv.hWnd;

        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Grid_Grid_BeforeExitEditMode(object sender, Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventArgs e)
        {
            string sSQL = null;

            int lGRI_ID = 0;
            DataTable rs = default(DataTable);
            try
            {
                if (treev.Nodes.Count == 0)
                {

                    return;
                }
                foreach (UltraTreeNode oNodeParent in treev.Nodes)
                {


                    if (oNodeParent.Selected == true)
                    {
                        if (General.UCase(General.Left(oNodeParent.Key, 1)) == General.UCase(cNiv1))
                        {
                            lGRI_ID = Convert.ToInt32(General.Right(oNodeParent.Key, General.Len(oNodeParent.Key) - 1));
                            break;
                        }
                        else
                        {
                            return;
                        }
                    }

                }
                if (string.IsNullOrEmpty(Grid_Grid.ActiveRow.Cells["GRI_ID"].Text))
                {
                    Grid_Grid.ActiveRow.Cells["GRI_ID"].Value = lGRI_ID;

                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Grid_Grid_BeforeColUpdate");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Grid_Grid_InitializeRow(object sender, Infragistics.Win.UltraWinGrid.InitializeRowEventArgs e)
        {
            string sSQL = null;
            try
            {

                sSQL = "SELECT   Matricule , Nom, Prenom From Personnel " + " WHERE  Matricule = '" + e.Row.Cells["GRID_Mat"].Text + "'";

                using (ModAdo ModAdo = new ModAdo())
                    e.Row.Cells["Nom"].Value = ModAdo.fc_ADOlibelle(sSQL, true);
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Grid_Grid_RowLoaded");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void treev_AfterSelect(object sender, SelectEventArgs e)
        {
            var Node = treev.ActiveNode;
            Node.Override.NodeAppearance.Image = Properties.Resources.folder_opened16;
            try
            {
                //=== compteur.
                if (General.sPriseEnCompteLocalP2 == "1")
                {

                    if (General.UCase(General.Left(Node.Key, 1)) == General.UCase(cNiv1))
                    {
                        fc_LoadGRID(Convert.ToInt32(General.nz(General.Right(Node.Key, General.Len(Node.Key) - 1), 0)));
                        fc_BoldNode(ref treev);
                        txtGRI_Nom.Text = Node.Text;
                    }
                    else
                    {

                    }

                    fc_BoldNode(ref treev);

                }
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }


        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="lGRID_ID"></param>
        private void fc_LoadGRID(int lGRID_ID)
        {
            string sSQL = null;


            try
            {
                sSQL = "SELECT   GRI_ID, GRID_Mat, '' as Nom,GRID_Ordre,  GRID_CreeLe, GRID_CreePat, GRID_ModifieLe, GRID_ModifiePar"
                    + " From GRID_GroupeIntervenantDetail " + " WHERE   GRI_ID =" + lGRID_ID + " ORDER BY GRID_Ordre";

                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                rsGrid = ModAdoUpdate.fc_OpenRecordSet(sSQL);
                if (rsGrid.Rows.Count == 0)
                {
                    Grid_Grid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
                }
                Grid_Grid.DataSource = rsGrid;
                Grid_Grid.UpdateData();
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.Arrow;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadXXX");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="treev"></param>
        private void fc_BoldNode(ref UltraTree treev)
        {

            foreach (UltraTreeNode oNodeParent in treev.Nodes)
            {
                if (oNodeParent.Selected == true)
                {
                    oNodeParent.Override.NodeAppearance.FontData.Bold = DefaultableBoolean.True;

                }
                else
                {
                    oNodeParent.Override.NodeAppearance.FontData.Bold = DefaultableBoolean.False;
                }

            }
        }

        private void Grid_Grid_BeforeRowsDeleted(object sender, Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventArgs e)
        {
            e.DisplayPromptMsg = false;
            if (
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Confirmez vous la suppression de l'(des) enregistrement(s) sélectionné(s).", "", MessageBoxButtons.YesNo, MessageBoxIcon.Question,
                    MessageBoxDefaultButton.Button2) == DialogResult.No)
                e.Cancel = true;
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Grid_Grid_AfterRowUpdate(object sender, Infragistics.Win.UltraWinGrid.RowEventArgs e)
        {
            ModAdoUpdate.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Grid_Grid_AfterRowsDeleted(object sender, EventArgs e)
        {
            ModAdoUpdate.Update();
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Grid_Grid_InitializeLayout(object sender, Infragistics.Win.UltraWinGrid.InitializeLayoutEventArgs e)
        {

            Grid_Grid.DisplayLayout.Bands[0].Columns["GRID_CreeLe"].Header.Caption = "Crée Le";
            Grid_Grid.DisplayLayout.Bands[0].Columns["GRID_CreePat"].Header.Caption = "Crée Par";
            Grid_Grid.DisplayLayout.Bands[0].Columns["GRID_ModifieLe"].Header.Caption = "Modifié Le";
            Grid_Grid.DisplayLayout.Bands[0].Columns["GRID_ModifiePar"].Header.Caption = "Modifié Par";
            Grid_Grid.DisplayLayout.Bands[0].Columns["GRID_Ordre"].Header.Caption = "N°Ordre";
            Grid_Grid.DisplayLayout.Bands[0].Columns["GRI_ID"].Hidden = true;
            Grid_Grid.DisplayLayout.Bands[0].Columns["GRID_Mat"].ValueList = DropINterv;


        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Grid_Grid_BeforeRowUpdate(object sender, Infragistics.Win.UltraWinGrid.CancelableRowEventArgs e)
        {
            if (string.IsNullOrEmpty(e.Row.Cells["GRID_Mat"].Text))
            {
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("champs GRID_Mat est obliagtoire", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                e.Cancel = true;
                e.Row.Cells["GRID_Mat"].Activate();
                return;
            }
            if (e.Row.DataChanged)
            {
                if (e.Row.Cells["GRID_Mat"].OriginalValue.ToString() != e.Row.Cells["GRID_Mat"].Text)
                {
                    using (var tmpModAdo = new ModAdo())
                    {
                        var inDb = Convert.ToInt32(tmpModAdo.fc_ADOlibelle($"SELECT COUNT(*) FROM GRID_GroupeIntervenantDetail  WHERE GRID_Mat= '{e.Row.Cells["GRID_Mat"].Text}' and GRI_ID= '{e.Row.Cells["GRI_ID"].Text}' "));
                        if (inDb > 0)
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Cet enregistrement éxiste déjà", "", MessageBoxButtons.OK, MessageBoxIcon.Error);
                            e.Row.CancelUpdate();
                            e.Cancel = true;
                        }
                    }
                }
            }
        }

        private void Grid_Grid_Error(object sender, Infragistics.Win.UltraWinGrid.ErrorEventArgs e)
        {
            if (e.ErrorType == Infragistics.Win.UltraWinGrid.ErrorType.Data)
            {
                e.Cancel = true;
                e.ErrorText = "Erreur de saisie dans la colonne " + e.DataErrorInfo.Cell.Column.Header.Caption;
                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show(e.ErrorText);
            }
        }
    }
}
