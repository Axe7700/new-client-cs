﻿namespace Axe_interDT.Views.Planning.Forms
{
    partial class frmPropertyCalandar
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cmbTimeScale = new System.Windows.Forms.ComboBox();
            this.label33 = new System.Windows.Forms.Label();
            this.ultraGroupBox2 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cmbEndTime = new System.Windows.Forms.ComboBox();
            this.cmbStartTime = new System.Windows.Forms.ComboBox();
            this.label2 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.ultraGroupBox3 = new Infragistics.Win.Misc.UltraGroupBox();
            this.cmdImmeuble = new System.Windows.Forms.Button();
            this.cmdFrmColorEvent = new System.Windows.Forms.Button();
            this.Command3 = new System.Windows.Forms.Button();
            this.cmdAppliquer = new System.Windows.Forms.Button();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).BeginInit();
            this.ultraGroupBox2.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).BeginInit();
            this.ultraGroupBox3.SuspendLayout();
            this.SuspendLayout();
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.cmbTimeScale);
            this.ultraGroupBox1.Controls.Add(this.label33);
            this.ultraGroupBox1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ultraGroupBox1.Location = new System.Drawing.Point(23, 13);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(521, 62);
            this.ultraGroupBox1.TabIndex = 0;
            this.ultraGroupBox1.Text = "Vue de la journée";
            // 
            // cmbTimeScale
            // 
            this.cmbTimeScale.FormattingEnabled = true;
            this.cmbTimeScale.Location = new System.Drawing.Point(165, 28);
            this.cmbTimeScale.Name = "cmbTimeScale";
            this.cmbTimeScale.Size = new System.Drawing.Size(291, 27);
            this.cmbTimeScale.TabIndex = 0;
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label33.Location = new System.Drawing.Point(6, 28);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(129, 19);
            this.label33.TabIndex = 384;
            this.label33.Text = "Echelle de temps";
            // 
            // ultraGroupBox2
            // 
            this.ultraGroupBox2.Controls.Add(this.cmbEndTime);
            this.ultraGroupBox2.Controls.Add(this.cmbStartTime);
            this.ultraGroupBox2.Controls.Add(this.label2);
            this.ultraGroupBox2.Controls.Add(this.label1);
            this.ultraGroupBox2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ultraGroupBox2.Location = new System.Drawing.Point(23, 81);
            this.ultraGroupBox2.Name = "ultraGroupBox2";
            this.ultraGroupBox2.Size = new System.Drawing.Size(521, 68);
            this.ultraGroupBox2.TabIndex = 0;
            this.ultraGroupBox2.Text = "Echelles des heures";
            // 
            // cmbEndTime
            // 
            this.cmbEndTime.FormattingEnabled = true;
            this.cmbEndTime.Location = new System.Drawing.Point(339, 31);
            this.cmbEndTime.Name = "cmbEndTime";
            this.cmbEndTime.Size = new System.Drawing.Size(117, 27);
            this.cmbEndTime.TabIndex = 1;
            // 
            // cmbStartTime
            // 
            this.cmbStartTime.FormattingEnabled = true;
            this.cmbStartTime.Location = new System.Drawing.Point(115, 31);
            this.cmbStartTime.Name = "cmbStartTime";
            this.cmbStartTime.Size = new System.Drawing.Size(113, 27);
            this.cmbStartTime.TabIndex = 0;
            this.cmbStartTime.SelectedValueChanged += new System.EventHandler(this.cmbStartTime_SelectedValueChanged);
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(244, 34);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(89, 19);
            this.label2.TabIndex = 384;
            this.label2.Text = "Heure max.";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(15, 31);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(85, 19);
            this.label1.TabIndex = 384;
            this.label1.Text = "Heure min.";
            // 
            // ultraGroupBox3
            // 
            this.ultraGroupBox3.Controls.Add(this.cmdImmeuble);
            this.ultraGroupBox3.Controls.Add(this.cmdFrmColorEvent);
            this.ultraGroupBox3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.ultraGroupBox3.Location = new System.Drawing.Point(23, 157);
            this.ultraGroupBox3.Name = "ultraGroupBox3";
            this.ultraGroupBox3.Size = new System.Drawing.Size(274, 62);
            this.ultraGroupBox3.TabIndex = 0;
            this.ultraGroupBox3.Text = "Couleur";
            // 
            // cmdImmeuble
            // 
            this.cmdImmeuble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdImmeuble.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdImmeuble.FlatAppearance.BorderSize = 0;
            this.cmdImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdImmeuble.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdImmeuble.ForeColor = System.Drawing.Color.White;
            this.cmdImmeuble.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdImmeuble.Location = new System.Drawing.Point(153, 22);
            this.cmdImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.cmdImmeuble.Name = "cmdImmeuble";
            this.cmdImmeuble.Size = new System.Drawing.Size(116, 35);
            this.cmdImmeuble.TabIndex = 397;
            this.cmdImmeuble.Text = "Immmeuble";
            this.cmdImmeuble.UseVisualStyleBackColor = false;
            this.cmdImmeuble.Click += new System.EventHandler(this.cmdImmeuble_Click);
            // 
            // cmdFrmColorEvent
            // 
            this.cmdFrmColorEvent.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdFrmColorEvent.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdFrmColorEvent.FlatAppearance.BorderSize = 0;
            this.cmdFrmColorEvent.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdFrmColorEvent.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdFrmColorEvent.ForeColor = System.Drawing.Color.White;
            this.cmdFrmColorEvent.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdFrmColorEvent.Location = new System.Drawing.Point(19, 22);
            this.cmdFrmColorEvent.Margin = new System.Windows.Forms.Padding(2);
            this.cmdFrmColorEvent.Name = "cmdFrmColorEvent";
            this.cmdFrmColorEvent.Size = new System.Drawing.Size(116, 35);
            this.cmdFrmColorEvent.TabIndex = 397;
            this.cmdFrmColorEvent.Text = "Statut";
            this.cmdFrmColorEvent.UseVisualStyleBackColor = false;
            this.cmdFrmColorEvent.Click += new System.EventHandler(this.cmdFrmColorEvent_Click);
            // 
            // Command3
            // 
            this.Command3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command3.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command3.FlatAppearance.BorderSize = 0;
            this.Command3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command3.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.Command3.ForeColor = System.Drawing.Color.White;
            this.Command3.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command3.Location = new System.Drawing.Point(312, 179);
            this.Command3.Margin = new System.Windows.Forms.Padding(2);
            this.Command3.Name = "Command3";
            this.Command3.Size = new System.Drawing.Size(116, 35);
            this.Command3.TabIndex = 397;
            this.Command3.Text = "Annuler";
            this.Command3.UseVisualStyleBackColor = false;
            this.Command3.Click += new System.EventHandler(this.Command3_Click);
            // 
            // cmdAppliquer
            // 
            this.cmdAppliquer.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAppliquer.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAppliquer.FlatAppearance.BorderSize = 0;
            this.cmdAppliquer.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAppliquer.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.cmdAppliquer.ForeColor = System.Drawing.Color.White;
            this.cmdAppliquer.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAppliquer.Location = new System.Drawing.Point(432, 179);
            this.cmdAppliquer.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAppliquer.Name = "cmdAppliquer";
            this.cmdAppliquer.Size = new System.Drawing.Size(116, 35);
            this.cmdAppliquer.TabIndex = 397;
            this.cmdAppliquer.Text = "Appliquer";
            this.cmdAppliquer.UseVisualStyleBackColor = false;
            this.cmdAppliquer.Click += new System.EventHandler(this.cmdAppliquer_Click);
            // 
            // frmPropertyCalandar
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(556, 231);
            this.Controls.Add(this.cmdAppliquer);
            this.Controls.Add(this.Command3);
            this.Controls.Add(this.ultraGroupBox3);
            this.Controls.Add(this.ultraGroupBox2);
            this.Controls.Add(this.ultraGroupBox1);
            this.Name = "frmPropertyCalandar";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Propriété";
            this.Load += new System.EventHandler(this.frmPropertyCalandar_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox2)).EndInit();
            this.ultraGroupBox2.ResumeLayout(false);
            this.ultraGroupBox2.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox3)).EndInit();
            this.ultraGroupBox3.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox2;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox3;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Button cmdImmeuble;
        public System.Windows.Forms.Button cmdFrmColorEvent;
        public System.Windows.Forms.Button Command3;
        public System.Windows.Forms.Button cmdAppliquer;
        public System.Windows.Forms.ComboBox cmbTimeScale;
        public System.Windows.Forms.ComboBox cmbEndTime;
        public System.Windows.Forms.ComboBox cmbStartTime;
    }
}