﻿namespace Axe_interDT.Views.Planning.Forms
{
    partial class frmImmColor
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            this.CD1 = new System.Windows.Forms.ColorDialog();
            this.GridBCimm = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.cmdMAJ = new System.Windows.Forms.Button();
            this.cmdAddImmeuble = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.GridBCimm)).BeginInit();
            this.SuspendLayout();
            // 
            // GridBCimm
            // 
            this.GridBCimm.Anchor = ((System.Windows.Forms.AnchorStyles)(((System.Windows.Forms.AnchorStyles.Top | System.Windows.Forms.AnchorStyles.Bottom) 
            | System.Windows.Forms.AnchorStyles.Left)));
            this.GridBCimm.DisplayLayout.AutoFitStyle = Infragistics.Win.UltraWinGrid.AutoFitStyle.ResizeAllColumns;
            this.GridBCimm.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.No;
            this.GridBCimm.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.GridBCimm.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.False;
            this.GridBCimm.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridBCimm.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridBCimm.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            this.GridBCimm.DisplayLayout.UseFixedHeaders = true;
            this.GridBCimm.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.GridBCimm.Location = new System.Drawing.Point(12, 61);
            this.GridBCimm.Name = "GridBCimm";
            this.GridBCimm.Size = new System.Drawing.Size(572, 356);
            this.GridBCimm.TabIndex = 574;
            this.GridBCimm.Text = "Code état";
            this.GridBCimm.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.GridBCimm_InitializeLayout);
            this.GridBCimm.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.GridBCimm_InitializeRow);
            this.GridBCimm.AfterRowsDeleted += new System.EventHandler(this.GridBCimm_AfterRowsDeleted);
            this.GridBCimm.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.GridBCimm_AfterRowUpdate);
            this.GridBCimm.ClickCellButton += new Infragistics.Win.UltraWinGrid.CellEventHandler(this.GridBCimm_ClickCellButton);
            this.GridBCimm.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.GridBCimm_BeforeRowsDeleted);
            // 
            // cmdMAJ
            // 
            this.cmdMAJ.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdMAJ.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdMAJ.FlatAppearance.BorderSize = 0;
            this.cmdMAJ.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdMAJ.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdMAJ.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.cmdMAJ.Location = new System.Drawing.Point(525, 21);
            this.cmdMAJ.Margin = new System.Windows.Forms.Padding(2);
            this.cmdMAJ.Name = "cmdMAJ";
            this.cmdMAJ.Size = new System.Drawing.Size(60, 35);
            this.cmdMAJ.TabIndex = 573;
            this.cmdMAJ.Tag = "";
            this.toolTip1.SetToolTip(this.cmdMAJ, "Enregistrer");
            this.cmdMAJ.UseVisualStyleBackColor = false;
            this.cmdMAJ.Click += new System.EventHandler(this.cmdMAJ_Click);
            // 
            // cmdAddImmeuble
            // 
            this.cmdAddImmeuble.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmdAddImmeuble.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAddImmeuble.FlatAppearance.BorderSize = 0;
            this.cmdAddImmeuble.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAddImmeuble.Font = new System.Drawing.Font("Ubuntu", 9.749999F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAddImmeuble.ForeColor = System.Drawing.Color.White;
            this.cmdAddImmeuble.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdAddImmeuble.Location = new System.Drawing.Point(356, 21);
            this.cmdAddImmeuble.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAddImmeuble.Name = "cmdAddImmeuble";
            this.cmdAddImmeuble.Size = new System.Drawing.Size(165, 35);
            this.cmdAddImmeuble.TabIndex = 575;
            this.cmdAddImmeuble.Text = "Ajouter un Immmeuble";
            this.cmdAddImmeuble.UseVisualStyleBackColor = false;
            this.cmdAddImmeuble.Click += new System.EventHandler(this.cmdAddImmeuble_Click);
            // 
            // frmImmColor
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(601, 429);
            this.Controls.Add(this.cmdAddImmeuble);
            this.Controls.Add(this.GridBCimm);
            this.Controls.Add(this.cmdMAJ);
            this.Name = "frmImmColor";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Couleur de fond immeuble";
            this.Load += new System.EventHandler(this.frmImmColor_Load);
            ((System.ComponentModel.ISupportInitialize)(this.GridBCimm)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.ColorDialog CD1;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridBCimm;
        public System.Windows.Forms.Button cmdMAJ;
        public System.Windows.Forms.Button cmdAddImmeuble;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}