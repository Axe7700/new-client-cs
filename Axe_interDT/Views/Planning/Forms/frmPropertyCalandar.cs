﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using Axe_interDT.Shared;
using Axe_interDT.Views.Fournisseurs.IntegrationDesFacturesFournisseurs.Forms;

namespace Axe_interDT.Views.Planning.Forms
{
    public partial class frmPropertyCalandar : Form
    {
        public frmPropertyCalandar()
        {
            InitializeComponent();
        }

        public UserDocPlanningCodeJock userDocPlanningCodeJock;

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdAppliquer_Click(object sender, EventArgs e)
        {
            ApplySettings();

            this.Close();
        }

        /// <summary>
        /// Tested
        /// </summary>
        public void ApplySettings()
        {

            try
            {
                userDocPlanningCodeJock.Cal.DayView.TimeScale = Convert.ToInt32(cmbTimeScale.SelectedValue);

                ModPlanOutlook.fc_SaveSettingCodejock(userDocPlanningCodeJock);

                if (!string.IsNullOrEmpty(cmbStartTime.Text))
                {
                    userDocPlanningCodeJock.Cal.DayView.TimeScaleMinTime = Convert.ToDateTime(cmbStartTime.Text);
                }

                if (!string.IsNullOrEmpty(cmbEndTime.Text))
                {
                    userDocPlanningCodeJock.Cal.DayView.TimeScaleMaxTime = Convert.ToDateTime(cmbEndTime.Text);
                }

                ModPlanOutlook.fc_SaveSettingCodejock(userDocPlanningCodeJock);

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";ApplySettings");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdFrmColorEvent_Click(object sender, EventArgs e)
        {
            if (ModPlanOutlook.bPlanUpdate == false)
            {
                MessageBox.Show("Vous n'avez pas les autorisations pour mettre à les couleurs.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }
            Forms.frmColorEvent frm = new Forms.frmColorEvent();
            frm.ShowDialog();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command3_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void frmPropertyCalandar_Load(object sender, EventArgs e)
        {
            int lTimeScale = 0;

            var tmpDataTable = new DataTable();
            tmpDataTable.Columns.Add("0");
            tmpDataTable.Columns.Add("1");
            cmbTimeScale.DataSource = tmpDataTable;
            cmbTimeScale.DisplayMember = "0";
            cmbTimeScale.ValueMember = "1";

            AddTimeScale(5);
            AddTimeScale(6);
            AddTimeScale(10);
            AddTimeScale(15);
            AddTimeScale(30);
            AddTimeScale(60);
            lTimeScale = Convert.ToInt32(General.getFrmReg("App", ModPlanOutlook.cCJEchelleTemp, Convert.ToString(30)));

            switch (lTimeScale)
            {
                case 5:
                    cmbTimeScale.SelectedItem = cmbTimeScale.Items[0];
                    break;
                case 6:
                    cmbTimeScale.SelectedItem = cmbTimeScale.Items[1];
                    break;
                case 10:
                    cmbTimeScale.SelectedItem = cmbTimeScale.Items[2];
                    break;
                case 15:
                    cmbTimeScale.SelectedItem = cmbTimeScale.Items[3];
                    break;
                case 30:
                    cmbTimeScale.SelectedItem = cmbTimeScale.Items[4];
                    break;
                case 60:
                    cmbTimeScale.SelectedItem = cmbTimeScale.Items[5];
                    break;
            }
            InitStartTimeCombo();
            UpdateEndTimeCombo();
            //cmbStartTime.Text = "";
            //cmbEndTime.Text = "";
            
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void InitStartTimeCombo()
        {
            try
            {
                int i = 0;
                for (i = 0; i < cmbStartTime.Items.Count - 1; i++)
                {
                    cmbStartTime.Items.RemoveAt(i);
                }

                //System.DateTime BeginTime = new DateTime(0001, 01, 01, 00, 00, 00);
                ////BeginTime = 01 / 01 / 0001 00:00:00;
                //cmbStartTime.Text = Convert.ToString(BeginTime);
                //for (i = 1; i <= 47; i++)
                //{
                //    // cmbStartTime.AddItem TimeValue(BeginTime + i / 24 / 2)
                //    cmbStartTime.Items.Add(Convert.ToString(BeginTime.Hour + i / 24 / 2));
                //}

                //TimeSpan BeginTime = new TimeSpan(12, 00, 00);
                //cmbStartTime.Text = BeginTime.ToString();
                //while(BeginTime.Hours < 24)
                //{
                //    cmbStartTime.Items.Add(Convert.ToString(BeginTime));
                //    BeginTime = BeginTime.Add(new TimeSpan(00, 30, 00));
                //}

                for (i = 0; i <= 23; i++)
                    for (int j = 0; j < 2; j++)
                        cmbStartTime.Items.Add($"{i.ToString("D2")}:{(30 * j).ToString("D2")}:00");
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="TimeScale"></param>
        public void AddTimeScale(int TimeScale)
        {
            var tmpDataTable = cmbTimeScale.DataSource as DataTable;
            tmpDataTable.Rows.Add(TimeScale + " minutes", TimeScale);

            cmbTimeScale.DataSource = tmpDataTable;

            if (userDocPlanningCodeJock.Cal.DayView.TimeScale == TimeScale)
                cmbTimeScale.SelectedIndex = cmbTimeScale.Items.Count - 1;
        }

        /// <summary>
        /// Tested
        /// </summary>
        private void UpdateEndTimeCombo()
        {
            try
            {

                if (!General.IsDate(cmbStartTime.Text) || cmbStartTime.Text == "")
                    return;

                int i = 0;
                //for (i = 0; i < cmbEndTime.Items.Count; i++)
                //{
                //    cmbEndTime.Items.RemoveAt(i);
                //}

                cmbEndTime.Items.Clear();

                System.DateTime BeginTime = default(System.DateTime);
                ////BeginTime = TimeValue(cmbStartTime.Text)
                //BeginTime = Convert.ToDateTime(cmbStartTime.Text);

                //cmbStartTime.Items.Add(Convert.ToString(BeginTime));
                ////& " (0 minutes)"
                //cmbStartTime.Items.Add(Convert.ToString((BeginTime.Hour + 1 / 24 / 2)));
                //// & " (30 minutes)"
                //cmbStartTime.Items.Add(Convert.ToString((BeginTime.Hour) + 1 / 24));
                //// & " (1 hour)"

                //for (i = 3; i <= 47; i++)
                //{
                //    cmbStartTime.Items.Add(Convert.ToString((BeginTime.Hour + i / 24 / 2)));
                //    //& " (" & i / 2 & " hours)"
                //}

                BeginTime = Convert.ToDateTime(cmbStartTime.Text);
                var tmpDateTime = Convert.ToDateTime(cmbStartTime.Text);
                tmpDateTime = tmpDateTime.AddDays(-1);
                cmbEndTime.Items.Add(tmpDateTime.ToString("HH:mm:ss"));
                while (tmpDateTime < BeginTime)
                {
                    tmpDateTime = tmpDateTime.AddMinutes(30);
                    cmbEndTime.Items.Add(tmpDateTime.ToString("HH:mm:ss"));
                }

                //ModuleAPI.SendMessage(cmbEndTime.Handle.ToInt32(), ModuleAPI.CB_SETDROPPEDWIDTH, 200,0);/TODO this function is a library in ModuleAPI.
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }

        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdImmeuble_Click(object sender, EventArgs e)
        {
            if (ModPlanOutlook.bPlanUpdate == false)
            {
                MessageBox.Show("Vous n'avez pas les autorisations pour mettre à les couleurs.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                return;
            }

            frmImmColor frm = new frmImmColor();
            frm.ShowDialog();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbStartTime_SelectedValueChanged(object sender, EventArgs e)
        {
            UpdateEndTimeCombo();
        }
    }
}
