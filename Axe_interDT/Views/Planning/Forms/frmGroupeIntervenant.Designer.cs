﻿namespace Axe_interDT.Views.Planning.Forms
{
    partial class frmGroupeIntervenant
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.label1 = new System.Windows.Forms.Label();
            this.ultraGroupBox1 = new Infragistics.Win.Misc.UltraGroupBox();
            this.FraTrew = new Infragistics.Win.Misc.UltraGroupBox();
            this.Grid_Grid = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.Cmd = new System.Windows.Forms.Button();
            this.DropINterv = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.txtGRI_Nom = new iTalk.iTalk_TextBox_Small2();
            this.label2 = new System.Windows.Forms.Label();
            this.treev = new Infragistics.Win.UltraWinTree.UltraTree();
            this.cmdAjouter = new System.Windows.Forms.Button();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).BeginInit();
            this.ultraGroupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FraTrew)).BeginInit();
            this.FraTrew.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_Grid)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DropINterv)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.treev)).BeginInit();
            this.SuspendLayout();
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label1.Location = new System.Drawing.Point(16, 15);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(67, 19);
            this.label1.TabIndex = 0;
            this.label1.Text = "GROUPE";
            // 
            // ultraGroupBox1
            // 
            this.ultraGroupBox1.Controls.Add(this.FraTrew);
            this.ultraGroupBox1.Controls.Add(this.treev);
            this.ultraGroupBox1.Controls.Add(this.cmdAjouter);
            this.ultraGroupBox1.Controls.Add(this.label1);
            this.ultraGroupBox1.Location = new System.Drawing.Point(12, 12);
            this.ultraGroupBox1.Name = "ultraGroupBox1";
            this.ultraGroupBox1.Size = new System.Drawing.Size(798, 484);
            this.ultraGroupBox1.TabIndex = 1;
            // 
            // FraTrew
            // 
            this.FraTrew.Controls.Add(this.Grid_Grid);
            this.FraTrew.Controls.Add(this.Cmd);
            this.FraTrew.Controls.Add(this.DropINterv);
            this.FraTrew.Controls.Add(this.txtGRI_Nom);
            this.FraTrew.Controls.Add(this.label2);
            this.FraTrew.Location = new System.Drawing.Point(219, 0);
            this.FraTrew.Name = "FraTrew";
            this.FraTrew.Size = new System.Drawing.Size(573, 478);
            this.FraTrew.TabIndex = 363;
            // 
            // Grid_Grid
            // 
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.Grid_Grid.DisplayLayout.Appearance = appearance1;
            this.Grid_Grid.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.Grid_Grid.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.Grid_Grid.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.Grid_Grid.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.Grid_Grid.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.Grid_Grid.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.Grid_Grid.DisplayLayout.MaxColScrollRegions = 1;
            this.Grid_Grid.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.Grid_Grid.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.Grid_Grid.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.Grid_Grid.DisplayLayout.Override.AllowAddNew = Infragistics.Win.UltraWinGrid.AllowAddNew.TemplateOnBottom;
            this.Grid_Grid.DisplayLayout.Override.AllowDelete = Infragistics.Win.DefaultableBoolean.True;
            this.Grid_Grid.DisplayLayout.Override.AllowUpdate = Infragistics.Win.DefaultableBoolean.True;
            this.Grid_Grid.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.Grid_Grid.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.Grid_Grid.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.Grid_Grid.DisplayLayout.Override.CellAppearance = appearance8;
            this.Grid_Grid.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.Grid_Grid.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.Grid_Grid.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.Grid_Grid.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.Grid_Grid.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.Grid_Grid.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.Grid_Grid.DisplayLayout.Override.RowAppearance = appearance11;
            this.Grid_Grid.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.Grid_Grid.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.Grid_Grid.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.Grid_Grid.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.Grid_Grid.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.Grid_Grid.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.Grid_Grid.Location = new System.Drawing.Point(12, 77);
            this.Grid_Grid.Name = "Grid_Grid";
            this.Grid_Grid.RowUpdateCancelAction = Infragistics.Win.UltraWinGrid.RowUpdateCancelAction.RetainDataAndActivation;
            this.Grid_Grid.Size = new System.Drawing.Size(555, 395);
            this.Grid_Grid.TabIndex = 574;
            this.Grid_Grid.Text = "ultraGrid1";
            this.Grid_Grid.InitializeLayout += new Infragistics.Win.UltraWinGrid.InitializeLayoutEventHandler(this.Grid_Grid_InitializeLayout);
            this.Grid_Grid.InitializeRow += new Infragistics.Win.UltraWinGrid.InitializeRowEventHandler(this.Grid_Grid_InitializeRow);
            this.Grid_Grid.AfterRowsDeleted += new System.EventHandler(this.Grid_Grid_AfterRowsDeleted);
            this.Grid_Grid.AfterRowUpdate += new Infragistics.Win.UltraWinGrid.RowEventHandler(this.Grid_Grid_AfterRowUpdate);
            this.Grid_Grid.BeforeRowUpdate += new Infragistics.Win.UltraWinGrid.CancelableRowEventHandler(this.Grid_Grid_BeforeRowUpdate);
            this.Grid_Grid.BeforeExitEditMode += new Infragistics.Win.UltraWinGrid.BeforeExitEditModeEventHandler(this.Grid_Grid_BeforeExitEditMode);
            this.Grid_Grid.BeforeRowsDeleted += new Infragistics.Win.UltraWinGrid.BeforeRowsDeletedEventHandler(this.Grid_Grid_BeforeRowsDeleted);
            this.Grid_Grid.Error += new Infragistics.Win.UltraWinGrid.ErrorEventHandler(this.Grid_Grid_Error);
            // 
            // Cmd
            // 
            this.Cmd.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Cmd.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Cmd.FlatAppearance.BorderSize = 0;
            this.Cmd.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Cmd.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.Cmd.Image = global::Axe_interDT.Properties.Resources.Save_24x24;
            this.Cmd.Location = new System.Drawing.Point(507, 5);
            this.Cmd.Margin = new System.Windows.Forms.Padding(2);
            this.Cmd.Name = "Cmd";
            this.Cmd.Size = new System.Drawing.Size(60, 35);
            this.Cmd.TabIndex = 364;
            this.Cmd.Tag = "";
            this.toolTip1.SetToolTip(this.Cmd, "Enregistrer");
            this.Cmd.UseVisualStyleBackColor = false;
            this.Cmd.Click += new System.EventHandler(this.Cmd_Click);
            // 
            // DropINterv
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.DropINterv.DisplayLayout.Appearance = appearance13;
            this.DropINterv.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.DropINterv.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.DropINterv.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.DropINterv.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.DropINterv.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.DropINterv.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.DropINterv.DisplayLayout.MaxColScrollRegions = 1;
            this.DropINterv.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.DropINterv.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.DropINterv.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.DropINterv.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.DropINterv.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.DropINterv.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.DropINterv.DisplayLayout.Override.CellAppearance = appearance20;
            this.DropINterv.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.DropINterv.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.DropINterv.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.DropINterv.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.DropINterv.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.DropINterv.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.DropINterv.DisplayLayout.Override.RowAppearance = appearance23;
            this.DropINterv.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.DropINterv.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.DropINterv.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.DropINterv.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.DropINterv.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.DropINterv.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.DropINterv.Location = new System.Drawing.Point(32, 15);
            this.DropINterv.Name = "DropINterv";
            this.DropINterv.Size = new System.Drawing.Size(186, 27);
            this.DropINterv.TabIndex = 573;
            this.DropINterv.Visible = false;
            // 
            // txtGRI_Nom
            // 
            this.txtGRI_Nom.AccAcceptNumbersOnly = false;
            this.txtGRI_Nom.AccAllowComma = false;
            this.txtGRI_Nom.AccBackgroundColor = System.Drawing.Color.White;
            this.txtGRI_Nom.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtGRI_Nom.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtGRI_Nom.AccHidenValue = "";
            this.txtGRI_Nom.AccNotAllowedChars = null;
            this.txtGRI_Nom.AccReadOnly = false;
            this.txtGRI_Nom.AccReadOnlyAllowDelete = false;
            this.txtGRI_Nom.AccRequired = false;
            this.txtGRI_Nom.BackColor = System.Drawing.Color.White;
            this.txtGRI_Nom.CustomBackColor = System.Drawing.Color.White;
            this.txtGRI_Nom.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtGRI_Nom.ForeColor = System.Drawing.Color.Black;
            this.txtGRI_Nom.Location = new System.Drawing.Point(95, 45);
            this.txtGRI_Nom.Margin = new System.Windows.Forms.Padding(2);
            this.txtGRI_Nom.MaxLength = 32767;
            this.txtGRI_Nom.Multiline = false;
            this.txtGRI_Nom.Name = "txtGRI_Nom";
            this.txtGRI_Nom.ReadOnly = false;
            this.txtGRI_Nom.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtGRI_Nom.Size = new System.Drawing.Size(472, 27);
            this.txtGRI_Nom.TabIndex = 502;
            this.txtGRI_Nom.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtGRI_Nom.UseSystemPasswordChar = false;
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(9, 49);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(45, 19);
            this.label2.TabIndex = 0;
            this.label2.Text = "NOM";
            // 
            // treev
            // 
            this.treev.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.treev.Location = new System.Drawing.Point(6, 45);
            this.treev.Name = "treev";
            this.treev.Size = new System.Drawing.Size(206, 433);
            this.treev.TabIndex = 362;
            this.treev.AfterSelect += new Infragistics.Win.UltraWinTree.AfterNodeSelectEventHandler(this.treev_AfterSelect);
            // 
            // cmdAjouter
            // 
            this.cmdAjouter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdAjouter.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdAjouter.FlatAppearance.BorderSize = 0;
            this.cmdAjouter.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdAjouter.Font = new System.Drawing.Font("Microsoft Sans Serif", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmdAjouter.Image = global::Axe_interDT.Properties.Resources.Add_File_24x24;
            this.cmdAjouter.Location = new System.Drawing.Point(152, 5);
            this.cmdAjouter.Margin = new System.Windows.Forms.Padding(2);
            this.cmdAjouter.Name = "cmdAjouter";
            this.cmdAjouter.Size = new System.Drawing.Size(60, 35);
            this.cmdAjouter.TabIndex = 361;
            this.cmdAjouter.Tag = "";
            this.cmdAjouter.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmdAjouter, "Ajouter");
            this.cmdAjouter.UseVisualStyleBackColor = false;
            this.cmdAjouter.Click += new System.EventHandler(this.cmdAjouter_Click);
            // 
            // frmGroupeIntervenant
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.ClientSize = new System.Drawing.Size(812, 508);
            this.Controls.Add(this.ultraGroupBox1);
            this.Name = "frmGroupeIntervenant";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "frmGroupeIntervenant";
            this.Load += new System.EventHandler(this.frmGroupeIntervenant_Load);
            ((System.ComponentModel.ISupportInitialize)(this.ultraGroupBox1)).EndInit();
            this.ultraGroupBox1.ResumeLayout(false);
            this.ultraGroupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.FraTrew)).EndInit();
            this.FraTrew.ResumeLayout(false);
            this.FraTrew.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Grid_Grid)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DropINterv)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.treev)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Label label1;
        private Infragistics.Win.Misc.UltraGroupBox ultraGroupBox1;
        public System.Windows.Forms.Button cmdAjouter;
        private Infragistics.Win.Misc.UltraGroupBox FraTrew;
        private Infragistics.Win.UltraWinTree.UltraTree treev;
        public System.Windows.Forms.Button Cmd;
        private System.Windows.Forms.Label label2;
        public iTalk.iTalk_TextBox_Small2 txtGRI_Nom;
        public Infragistics.Win.UltraWinGrid.UltraCombo DropINterv;
        public Infragistics.Win.UltraWinGrid.UltraGrid Grid_Grid;
        private System.Windows.Forms.ToolTip toolTip1;
    }
}