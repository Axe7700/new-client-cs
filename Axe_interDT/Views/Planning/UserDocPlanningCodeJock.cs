﻿using Axe_interDT.Shared;
using Axe_interDT.View.SharedViews;
using Axe_interDT.Views.Intervention;
using Axe_interDT.Views.Planning.Forms;
using Axe_interDT.Views.Theme.CustomMessageBox;
using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Threading.Tasks;
using System.Windows.Forms;
using XtremeCalendarControl;

namespace Axe_interDT.Views.Planning
{
    public partial class UserDocPlanningCodeJock : UserControl
    {
        bool bload;
        bool LoadExecuted = false;
        const string cCal_Width = "14895";
        const string cCal_Height = "8175";
        const string cFraConteneur_Width = "15135";
        const string cFraConteneur_Height = "8295";
        const string cPicForm_Width = "15135";
        const string cPicForm_Height = "36680.02";
        bool isAppInModeClosing = false;
        const string cSQLPlann =
            " SELECT     Intervention.NumFicheStandard, Intervention.NoIntervention, Intervention.CodeImmeuble,intervention.DateSaisie, Intervention.Article, Intervention.Designation, "
            +
            " Intervention.Commentaire, Intervention.Intervenant,  Intervention.DatePrevue, Intervention.HeurePrevue, Intervention.DureePrevue , Intervention.DateRealise, "
            +
            " Intervention.HeureDebut , Intervention.HeureFin, Intervention.Duree, Intervention.CodeEtat, Personnel.Nom, Personnel.Prenom ,"
            + "  intervention.NoMaj,       TypeCodeEtat.CouleurStatut, ImmeubleColor.CouleurDeFond, immeuble.ville  "
            + " FROM         Intervention INNER JOIN "
            + " Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble LEFT OUTER JOIN "
            + " ImmeubleColor ON Intervention.CodeImmeuble = ImmeubleColor.Codeimmeuble LEFT OUTER JOIN "
            + " TypeCodeEtat ON Intervention.CodeEtat = TypeCodeEtat.CodeEtat LEFT OUTER JOIN "
            + " Personnel ON Intervention.Intervenant = Personnel.Matricule";

        private DataTable rsVisite = null;
        private ModAdo rsVisiteModAdo = null;

        public UserDocPlanningCodeJock()
        {
            InitializeComponent();
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cal_BeforeEditOperation(object sender, AxXtremeCalendarControl._DCalendarControlEvents_BeforeEditOperationEvent e)
        {
            object xtpCalendarEO_DragResizeEnd = null;
            object xtpCalendarEO_DragResizeBegin = null;
            object xtpCalendarEO_DragMove = null;
            object xtpCalendarEO_EditSubject_ByTab = null;
            object xtpCalendarEO_EditSubject_ByF2 = null;
            object xtpCalendarEO_EditSubject_ByMouseClick = null;
            object xtpCalendarEO_DeleteSelectedEvents = null;
            object XtremeCalendarControl = null;
            try
            {
                string sStatut = null;
                //=== empeche la suppression des rendez vous (events).
                if (e.opParams.Operation == CalendarEditOperation.xtpCalendarEO_DeleteSelectedEvents)
                {
                    e.bCancelOperation = true;
                    return;
                }
                fc_LockTImer(true);

                //=== empeche la saisie du sujet de l'event.
                if (e.opParams.Operation == CalendarEditOperation.xtpCalendarEO_EditSubject_ByMouseClick ||
                    e.opParams.Operation == CalendarEditOperation.xtpCalendarEO_EditSubject_ByF2 ||
                    e.opParams.Operation == CalendarEditOperation.xtpCalendarEO_EditSubject_ByTab)
                {
                    e.bCancelOperation = true;
                    fc_LockTImer(false);
                    return;
                }

                if (e.opParams.EventViews != null)
                {
                    if (e.opParams.EventViews.Count > 1)
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("MULTI EVENTS " + e.opParams.EventViews.Count);
                    }
                    else
                    {
                        //'=== extrait le statut de l'intervention.
                        //sStatut = Cal.ActiveView.GetSelectedEvents[0].Event.CustomProperties[ModPlanOutlook.cChampsStatut].ToString();
                        sStatut = e.opParams.EventViews[0].Event.CustomProperties[ModPlanOutlook.cChampsStatut].ToString();
                        if (General.UCase(sStatut) == "00" || General.UCase(sStatut) == "01" || General.UCase(sStatut) == "02" || General.UCase(sStatut) == "0A")
                        {
                            //'=== autorise le changement
                        }
                        else
                        {
                            Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Seules les interventions ayant le statut 0A, 00, 01, 02 peuvent être déplacées.");
                            e.bCancelOperation = true;
                            fc_LockTImer(false);
                            return;
                        }
                    }
                }

                if (Cal.ActiveView.GetSelectedEvents.Count != 0)
                {
                    sStatut = Cal.ActiveView.GetSelectedEvents[0].Event.CustomProperties[ModPlanOutlook.cChampsStatut].ToString();

                    ///==============> Tested First
                    if (General.UCase(sStatut) == "00" || General.UCase(sStatut) == "01" || General.UCase(sStatut) == "02" || General.UCase(sStatut) == "0A")
                    {
                        fc_LockTImer(false);
                    }
                    else
                    {
                        Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Seules les interventions ayant le statut 0A, 00, 01, 02 peuvent être déplacées.");
                        e.bCancelOperation = true;
                        fc_LockTImer(false);
                        return;
                    }
                }

                fc_LockTImer(false);
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Cal_DoUpdateEvent");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cal_DblClick(object sender, EventArgs e)
        {

            CalendarHitTestInfo HitTest = default(CalendarHitTestInfo);
            string sNoInter = null;

            HitTest = Cal.ActiveView.HitTest();

            CalendarEvents Events = default(CalendarEvents);

            if (HitTest.HitCode != CalendarHitTestCode.xtpCalendarHitTestUnknown)
            {
                //   Set Events = CalendarControl.DataProvider.RetrieveDayEvents(HitTest.ViewDay.Date)
            }

            if (HitTest.ViewEvent == null)
            {
                //=== nefait rien.
            }
            else
            {
                sNoInter = HitTest.ViewEvent.Event.CustomProperties[ModPlanOutlook.cCustomPlanInt].ToString();
                ModParametre.fc_SaveParamPosition(this.Name, Variable.cUserIntervention, sNoInter);
                View.Theme.Theme.Navigate(typeof(UserIntervention));

            }
        }
        private void Cal_DoGetUpcomingEvents(object sender, AxXtremeCalendarControl._DCalendarControlEvents_DoGetUpcomingEventsEvent e)
        {
            //''Debug.Print "Cal_DoGetUpcomingEvents " & dtFrom
        }

        private void Cal_DoRetrieveDayEvents(object sender, AxXtremeCalendarControl._DCalendarControlEvents_DoRetrieveDayEventsEvent e)
        {
            //''Debug.Print "Cal_DoRetrieveDayEvents " & dtDay
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cal_DoUpdateEvent(object sender, AxXtremeCalendarControl._DCalendarControlEvents_DoUpdateEventEvent e)
        {
            object XtremeCalendarControl = null;
            string sSQL = null;
            string sDuree = null;
            string sDebour = null;
            string sIntervenant = null;
            double dbDuree = 0;
            bool bMajDuree;
            string stemp;
            DataTable rs = new DataTable();
            ModAdo rsAdo = new ModAdo();

            try
            {
                //=== si il n existe pas d'heure dans pEvent.StartTime signifie que cet event est situé en dehors du planning(parti dans le controle planning,
                //=== situé juste au dessus de début de la journée).
                fc_LockTImer(true);
                stemp = e.pEvent.StartTime.ToString();

                ///=============> Tested First
                if (stemp.IndexOf(":", 1) > 0)
                {
                    bMajDuree = true;

                    dbDuree = (e.pEvent.EndTime - e.pEvent.StartTime).TotalMinutes;
                    dbDuree = dbDuree / 60;
                    sDuree = General.fc_calcDuree(e.pEvent.StartTime, e.pEvent.EndTime, 0);
                }
                else
                {
                    bMajDuree = false;

                }

                sIntervenant = e.pEvent.DataProvider.Schedules[e.pEvent.ScheduleID - 1].Name;

                sSQL = "SELECT NoIntervention, DateRealise, HeureDebut, HeureFin, Intervenant, DatePrevue, HeurePrevue, Debourse, Duree, DureePrevue FROM intervention ";
                sSQL = sSQL + " WHERE   NoIntervention = " + General.nz(e.pEvent.CustomProperties["NoIntervention"], 0);

                rs = rsAdo.fc_OpenRecordSet(sSQL);
                ///=============> Tested
                if (rs.Rows.Count == 0)
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Attention cette intervention n'existe plus.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    rs.Dispose();
                    fc_LockTImer(false);
                    return;
                }
                rs.Rows[0]["Intervenant"] = sIntervenant;

                ///==============> Tested First
                if (General.IsDate(rs.Rows[0]["DateRealise"]))
                {
                    rs.Rows[0]["DateRealise"] = e.pEvent.StartTime;
                    if (bMajDuree == true)
                    {
                        if (!General.IsDate(rs.Rows[0]["HeureDebut"]))
                        {
                            rs.Rows[0]["HeurePrevue"] = e.pEvent.StartTime;
                            if (General.IsDate(sDuree))
                            {
                                rs.Rows[0]["DureePrevue"] = dbDuree;
                            }
                        }
                        else
                        {
                            rs.Rows[0]["HeureDebut"] = e.pEvent.StartTime;
                            rs.Rows[0]["HeureFin"] = e.pEvent.EndTime;
                            if (General.IsDate(sDuree))
                            {
                                rs.Rows[0]["Duree"] = sDuree;
                                sDebour = General.fc_CalcDebourse(Convert.ToDateTime(sDuree), sIntervenant, e.pEvent.EndTime);
                                if (General.IsNumeric(sDebour))
                                {
                                    rs.Rows[0]["Debourse"] = Convert.ToDouble(sDebour);
                                }
                            }

                        }
                    }
                }
                else
                {
                    rs.Rows[0]["DatePrevue"] = e.pEvent.StartTime;
                    if (bMajDuree == true)
                    {
                        rs.Rows[0]["HeurePrevue"] = e.pEvent.StartTime;

                        if (General.IsDate(sDuree))
                        {
                            // '=== modif du 09 05 2019, si l'heure de fin n'est pas null on met à jour le champ heure fin, plutot que la duree prévue 0.
                            if (!string.IsNullOrEmpty(rs.Rows[0]["HeureFin"].ToString()))
                            {
                                rs.Rows[0]["HeureFin"] = e.pEvent.EndTime;
                                rs.Rows[0]["Duree"] = sDuree;
                                sDebour = General.fc_CalcDebourse(Convert.ToDateTime(sDuree), sIntervenant, e.pEvent.EndTime);
                                if (General.IsNumeric(sDebour))
                                {
                                    rs.Rows[0]["Debourse"] = Convert.ToDouble(sDebour);
                                }
                            }
                            else
                            {
                                rs.Rows[0]["DureePrevue"] = dbDuree;
                            }
                        }
                    }
                }

                rsAdo.Update();
                rs.Dispose();
                fc_LockTImer(false);
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Cal_DoUpdateEvent");
                fc_LockTImer(false);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="bStopTimer"></param>
        private void fc_LockTImer(bool bStopTimer)
        {
            if (bStopTimer == true)
            {
                ModPlanOutlook.bStopTimerPlan = true;
                Timer1.Enabled = false;
                lblTimer.BackColor = System.Drawing.Color.Black;

            }
            else
            {
                ModPlanOutlook.bStopTimerPlan = false;
                Timer1.Enabled = true;
                Timer1.Interval = ModPlanOutlook.cDureeTimer;
                lblTimer.BackColor = System.Drawing.Color.Lime;
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Cal_IsEditOperationDisabled(object sender, AxXtremeCalendarControl._DCalendarControlEvents_IsEditOperationDisabledEvent e)
        {
            //'=== cet evenement se produit avant l'event BeforeEditOperations sauf si bDisableOperation est affecté à true.

            string sStatut;
            try
            {
                //'=== empeche la suppression des rendez vous (events).
                if (e.opParams.Operation == CalendarEditOperation.xtpCalendarEO_DeleteSelectedEvents)
                {
                    e.bDisableOperation = true;
                    return;
                }

                if (e.opParams.Operation == CalendarEditOperation.xtpCalendarEO_EditSubject_ByMouseClick
                                || e.opParams.Operation == CalendarEditOperation.xtpCalendarEO_EditSubject_ByF2
                                || e.opParams.Operation == CalendarEditOperation.xtpCalendarEO_EditSubject_ByTab
                                /*|| e.opParams.Operation == CalendarEditOperation.xtpCalendarEO_DragCopy*/)
                {


                    e.bDisableOperation = true;
                    return;
                }

                if (Cal.ActiveView.GetSelectedEvents != null)
                {
                    //if (Cal.ActiveView.GetSelectedEvents.Count>1)
                    //{

                    //    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("MULTI EVENTS " + e.opParams.EventViews.Count);
                    //}
                    if (Cal.ActiveView.GetSelectedEvents.Count > 0)
                    {

                        //'=== extrait le statut de l'intervention.
                        sStatut = Cal.ActiveView.GetSelectedEvents[0].Event.CustomProperties[ModPlanOutlook.cChampsStatut].ToString();


                        if (General.sActiveDroitPlan == "1" && ModPlanOutlook.bPlanUpdate == false)
                        {
                            //=== planning en lecture seule.
                            e.bDisableOperation = true;

                        }
                        else if (General.UCase(sStatut) == "00" || General.UCase(sStatut) == "01" || General.UCase(sStatut) == "02" || General.UCase(sStatut) == "0A")
                        {
                            //'=== autorise le changement
                            return;
                        }
                        else
                        {
                            e.bDisableOperation = true;

                        }

                    }
                    else
                    {
                        ///added by mohammed to disable drag and drop of intervention that has statut different of 00 01 02 0A
                        if (e.opParams.Operation == CalendarEditOperation.xtpCalendarEO_DragCopy)
                            e.bDisableOperation = true;
                    }
                }


            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }

        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        public void Cal_ViewChanged(object sender, EventArgs e)
        {
            //'Debug.Print "Cal_ViewChanged"

            if (!LoadExecuted)
                return;

            int i = 0;
            int DaysCount = 0;
            try
            {
                if (bload == false)
                {
                    FC_SaveParam();

                    //'=== apres selection de la vue semaine, on applique par defaut la semaine de travail..

                    if ((ModPlanOutlook.lSaveOldViewType == Convert.ToInt32(CalendarViewType.xtpCalendarMonthView) || ModPlanOutlook.lSaveOldViewType == Convert.ToInt32(CalendarViewType.xtpCalendarDayView))
                        && Convert.ToInt32(Cal.ViewType) == Convert.ToInt32(CalendarViewType.xtpCalendarWeekView))
                    {
                        ModPlanOutlook.lSaveOldViewType = Convert.ToInt32(Cal.ViewType);
                        Cal.ViewType = CalendarViewType.xtpCalendarWorkWeekView;
                    }

                    ModPlanOutlook.lSaveOldViewType = (int)Cal.ViewType;

                }
                //                'If (dtFirstDayVisible <> DateCal.FirstVisibleDay Or dtLastDayVisible <> DateCal.LastVisibleDay) And bload = False Then
                //'    'dtFirstDayVisible = DateCal.FirstVisibleDay
                //'    'dtLastDayVisible = DateCal.LastVisibleDay
                //'    fc_refresh
                i = 1;
                //'End If
                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        /// <summary>
        /// Tested 
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cdmDayView_Click(object sender, EventArgs e)
        {
            try
            {
                //                'dtFirstDayVisible = 1 & "/" & Month(Date) & "/" & Year(Date)
                //'dtLastDayVisible = 30 & "/" & Month(Date) & "/" & Year(Date)

                DateCal.SelectRange(DateTime.Now, DateTime.Now);
                DateCal.ResyncCalendar();
                Cal.ViewType = CalendarViewType.xtpCalendarDayView;
                DateCal.SelectRange(DateTime.Now, DateTime.Now);
                DateCal.ResyncCalendar();

                // 'DateCal.FirstVisibleDay = dtFirstDayVisible
                //'DateCal.LastVisibleDay = dtLastDayVisible


                //'fc_refresh

                //'CalendarControl.DayView.ShowDays Date, Date

                //'CalendarControl.ViewType = xtpCalendarDayView


                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, Name + ";cdmDayView_Click");
            }
        }

        private void CmbGroupe_TextChanged(object sender, EventArgs e)
        {
            // 'If bload = False Then
            //  ' fc_refresh
            //'End If
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmbGroupe_AfterCloseUp(object sender, EventArgs e)
        {
            fc_refresh(true, true, DateCal.Selection[0].DateBegin, DateCal.Selection[0].DateEnd);
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmbGroupe_BeforeDropDown(object sender, CancelEventArgs e)
        {
            string sSQL = null;
            try
            {
                sSQL = "SELECT     GRI_Nom as Nom" + " From GRI_GroupeIntervenant " + " ORDER BY GRI_Nom";


                sheridan.InitialiseCombo(CmbGroupe, sSQL, "Nom");

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmbGroupe_DropDown");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void CmbGroupe_KeyPress(object sender, KeyPressEventArgs e)
        {
            if (Convert.ToInt32(e.KeyChar) == 13)
            {
                cmdRechercheGroupe_Click(cmdRechercheGroupe, null);
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmbIntervention_Click(object sender, EventArgs e)
        {
            fc_refresh(true, true, DateCal.Selection[0].DateBegin, DateCal.Selection[0].DateEnd);
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdClean_Click(object sender, EventArgs e)
        {
            txtIntervenant.Text = "";
            CmbGroupe.Text = "";
            txtCodeEtatsInclus.Text = "";
        }
        private void cmdColMoins_Click(object sender, EventArgs e)
        {
            //TODO : Mondir - ask rachid if i can delete this button
            //if (Cal.DayView.MinColumnWidth > 0)
            //{
            //    Cal.DayView.MinColumnWidth = Cal.DayView.MinColumnWidth - 10;
            //    Cal.RedrawControl();
            //    Cal.Populate();
            //}

        }

        private void cmdColplus_Click(object sender, EventArgs e)
        {
            //            'Cal.DayView.MinColumnWidth = -1
            //'Cal.RedrawControl
            //'Cal.Populate
            //'
            //'Exit Sub

            //TODO : Mondir - ask rachid if i can delete this button
            //if (Cal.DayView.MinColumnWidth < 30)
            //{
            //    Cal.DayView.MinColumnWidth = 30;
            //}

            //else
            //{
            //    Cal.DayView.MinColumnWidth = Cal.DayView.MinColumnWidth + 10;
            //}
            //Cal.RedrawControl();
            //Cal.Populate();

            //'DateCal.SelectRange DateCal.Selection(0).DateBegin, DateCal.Selection(0).DateEnd

            //'Cal.Visible = False
            //'Cal.Visible = True
            //'Cal.RedrawControl
            //'Cal.RedrawControl
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdEctituresCopro_Click(object sender, EventArgs e)
        {
            try
            {
                if (ModPlanOutlook.bPlanUpdate == false)
                {
                    CustomMessageBox.Show("Vous n'avez pas les autorisations pour créer/modifier les groupes.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                Timer1.Enabled = false;
                frmGroupeIntervenant frm = new frmGroupeIntervenant();
                frm.ShowDialog();
                fc_refresh(true);
                Timer1.Enabled = true;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdEctituresCopro_Click");
                Timer1.Enabled = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdintervenant_Click(object sender, EventArgs e)
        {
            txtIntervenant_KeyPress(txtIntervenant, new System.Windows.Forms.KeyPressEventArgs((char)13));
        }
        private void CmdLargMoins_Click(object sender, EventArgs e)
        {
            try
            {    //TODO PicForm DE TYPE pictureBox dans vb6 contint tt la fiche
                //PicForm.Width = Microsoft.VisualBasic.Compatibility.VB6.Support.ToPixelsUserWidth(Microsoft.VisualBasic.Compatibility.VB6.Support.FromPixelsUserWidth(PicForm.Width, 15375, 1025) - 100, 15375, 1025);
                //FraConteneur.Width = FraConteneur.Width - 100;
                //Cal.Width = Cal.Width - 100;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdLargPlus_Click");
            }
        }

        private void cmdLargPlus_Click(object sender, EventArgs e)
        {
            try
            { //TODO PicForm DE TYPE pictureBox dans vb6 contint tt la fiche
                //PicForm.Width = Microsoft.VisualBasic.Compatibility.VB6.Support.ToPixelsUserWidth(Microsoft.VisualBasic.Compatibility.VB6.Support.FromPixelsUserWidth(PicForm.Width, 15375, 1025) + 100, 15375, 1025);
                //FraConteneur.Width = (FraConteneur.Width) + 100;
                //Cal.Width = Cal.Width + 100;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdLargPlus_Click");
            }
        }

        private void CmdLongMoins_Click(object sender, EventArgs e)
        {
            try
            {//TODO PicForm DE TYPE pictureBox dans vb6 contint tt la fiche
                //PicForm.Height = Microsoft.VisualBasic.Compatibility.VB6.Support.ToPixelsUserHeight(Microsoft.VisualBasic.Compatibility.VB6.Support.FromPixelsUserHeight(PicForm.Height, 35573.1, 707) - 100, 35573.1, 707);
                //FraConteneur.Height = (FraConteneur.Height) - 100;
                //Cal.Height = Cal.Height - 100;
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdLongMoins_Click");
            }
        }

        private void cmdLongPlus_Click(object sender, EventArgs e)
        {
            try
            {

                //PicForm.Height = Microsoft.VisualBasic.Compatibility.VB6.Support.ToPixelsUserHeight(Microsoft.VisualBasic.Compatibility.VB6.Support.FromPixelsUserHeight(PicForm.Height, 35573.1, 707) + 100, 35573.1, 707);
                //Debug.Print("pic => " + PicForm.Height);

                FraConteneur.Height = (FraConteneur.Height) + 100;
                //Debug.Print("fra => " + FraConteneur.Height);

                Cal.Height = Cal.Height + 100;
                //Debug.Print("Cal => " + Cal.Height);

                //Debug.Print("");
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";CmdLongMoins_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdRechercheGroupe_Click(object sender, EventArgs e)
        {

            string sCode = null;
            try
            {
                string sSQL = "SELECT GRI_Nom From GRI_GroupeIntervenant  WHERE GRI_Nom ='" + StdSQLchaine.gFr_DoublerQuote(CmbGroupe.Text) + "'";

                var ModAdo = new ModAdo();
                sSQL = ModAdo.fc_ADOlibelle(sSQL);

                if (string.IsNullOrEmpty(sSQL))
                {
                    string requete = "SELECT GRI_Nom as \"Groupe\" From GRI_GroupeIntervenant ";

                    string order = "ORDER BY GRI_Nom";
                    SearchTemplate fg = new SearchTemplate(this, null, requete, "", order) { Text = "" };
                    fg.SetValues(new Dictionary<string, string> { { "Groupe", CmbGroupe.Text } });

                    fg.ugResultat.DoubleClickRow += (se, ev) =>
                    {

                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        if (!string.IsNullOrEmpty(sCode))
                            CmbGroupe.Text = General.Trim(sCode);

                        fg.Dispose();
                        fg.Close();
                    };

                    fg.ugResultat.KeyDown += (se, ev) =>
                    {

                        if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                        {
                            sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            if (!string.IsNullOrEmpty(sCode))
                                CmbGroupe.Text = General.Trim(sCode);
                            fg.Dispose();
                            fg.Close();
                        }
                    };
                    fg.StartPosition = FormStartPosition.CenterParent;
                    fg.ShowDialog();
                }
                fc_refresh(true, true, DateCal.Selection[0].DateBegin, DateCal.Selection[0].DateEnd);
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdRechercheGroupe_Click");

            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdStatutInclus_Click(object sender, EventArgs e)
        {
            try
            {
                string sCode = null;

                string requete = "SELECT CodeEtat AS \"CodeEtat\", LibelleCodeEtat as \"Libelle\"  From TypeCodeEtat";

                SearchTemplate fg = new SearchTemplate(this, null, requete, "", "") { Text = "" };


                fg.ugResultat.DoubleClickRow += (se, ev) =>
                {
                    //' charge les enregistrements de l'immeuble
                    sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                    if (!string.IsNullOrEmpty(General.Trim(txtCodeEtatsInclus.Text)))
                        txtCodeEtatsInclus.Text = General.Trim(txtCodeEtatsInclus.Text + ";" + sCode);
                    else
                        txtCodeEtatsInclus.Text = sCode;
                    fg.Dispose();
                    fg.Close();
                };

                fg.ugResultat.KeyDown += (se, ev) =>
                {

                    if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                    {
                        sCode = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                        if (!string.IsNullOrEmpty(General.Trim(txtCodeEtatsInclus.Text)))
                            txtCodeEtatsInclus.Text = General.Trim(txtCodeEtatsInclus.Text + ";" + sCode);
                        else
                            txtCodeEtatsInclus.Text = sCode;
                        fg.Dispose();
                        fg.Close();
                    }
                };
                fg.StartPosition = FormStartPosition.CenterParent;
                fg.ShowDialog();

                fc_refresh(true, true, DateCal.Selection[0].DateBegin, DateCal.Selection[0].DateEnd);
                return;
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }
        }

        private void cmdZoomDef_Click(object sender, EventArgs e)
        {
            try
            {//TODO
                //PicForm.Width = Microsoft.VisualBasic.Compatibility.VB6.Support.ToPixelsUserWidth(cPicForm_Width, 15375, 1025);
                //PicForm.Height = Microsoft.VisualBasic.Compatibility.VB6.Support.ToPixelsUserHeight(cPicForm_Height, 35573.1, 707);

                FraConteneur.Width = Convert.ToInt32(cFraConteneur_Width);
                FraConteneur.Height = Convert.ToInt32(cFraConteneur_Height);

                Cal.Width = Convert.ToInt32(cCal_Width);
                Cal.Height = Convert.ToInt32(cCal_Height);

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";cmdZoomDef_Click");
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command3_Click(object sender, EventArgs e)
        {
            frmPropertyCalandar frm = new frmPropertyCalandar();
            frm.cmbStartTime.Text = General.getFrmReg("App", ModPlanOutlook.cCJJourHeureMin, "08:00:00");//TODO AM
            frm.cmbEndTime.Text = General.getFrmReg("App", ModPlanOutlook.cCJJourHeureMax, "19:00:00");//TODO PM           
            frm.userDocPlanningCodeJock = this;
            frm.ShowDialog();
        }

        /// <summary>
        /// Control is always hidden
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Command5_Click(object sender, EventArgs e)
        {
            Cal.ViewType = CalendarViewType.xtpCalendarWorkWeekView;
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DateCal_MonthChanged(object sender, EventArgs e)
        {
            fc_refresh();
        }

        /// <summary>
        /// tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void DateCal_SelectionChanged(object sender, EventArgs e)
        {
            try
            {
                // fc_Load

                //'Dim i As Long
                //'i = 0
                //'DTDateRealiseDe = DateCal.Selection(0).DateBegin
                //'DTDateRealiseAu = DateCal.Selection(0).DateEnd
                //'
                //'Screen.MousePointer = 11
                //'fc_LoadDataPlanning DateCal.Selection(0).DateBegin, DateCal.Selection(0).DateEnd, txtCodeEtatsInclus
                //'Screen.MousePointer = 1
                //'Cal.RedrawControl

                DTDateRealiseDe.Value = DateCal.Selection[0].DateBegin;
                DTDateRealiseAu.Value = DateCal.Selection[0].DateEnd;
                FC_SaveParam();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + "DateCal_SelectionChanged");
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void UserDocPlanningCodeJock_VisibleChanged(object sender, EventArgs e)
        {
            if (!Visible)
            {
                Timer1.Enabled = false;
                return;
            }

            Timer1.Interval = ModPlanOutlook.cDureeTimer;
            Timer1.Enabled = true;
            Timer1_Tick(Timer1, null);
        }
        private void UserDocPlanningCodeJock_Load(object sender, EventArgs e)
        {
            try
            {
                string sSQl = "";

                LoadExecuted = true;

                bload = true;

                Timer1.Enabled = true;

                //fc_GetParam();

                ModPlanOutlook.fc_GetSettingCodejock(this);

                //fc_LoadDataPlanning(DateCal.Selection[0].DateBegin.ToString(), DateCal.Selection[0].DateEnd.ToString(), txtCodeEtatsInclus.Text,  true);

                //fc_refresh();

                bload = false;

                if (General.sActiveDroitPlan == "1")
                {
                    sSQl = "SELECT     AUT_Formulaire From AUT_Autorisation "

                    + " WHERE   AUT_Formulaire = '" + Variable.cUserDocPlanningCodeJock + "' AND AUT_Nom = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName()) + "'"

                    + "  AND AUT_Objet = 'MajPLanning'";



                    using (ModAdo tmpModAdo = new ModAdo())
                        sSQl = tmpModAdo.fc_ADOlibelle(sSQl);


                    if (sSQl != "")
                        ModPlanOutlook.bPlanUpdate = true;
                    else
                        ModPlanOutlook.bPlanUpdate = false;
                }

                ///added by mohammed ==> i added this variable isAppInModeClosing (true or false) it takes true when user want to close the app. why did i add this? because we have an event call Timer1_Tick.this event fires every 2 seconds to do something.  sometimes this event takes more than 2 seconds to finish his task and inside this event we have Application.DoEvent() this function don't let app to wait until the current event finish his task so Timer1_Tick event fires again although the previous event didn't finish his task. so when we want to close the app this event still working and it uses controls likes Cal,DateCal ... so when the app disposed an exception occurs and block the app for some min. that's why i added this variable to know when the app is gonna be closed by the user to prevent Timer1_Tick from firing.
                View.Theme.Theme.MainForm.FormClosed += (se, ev) =>
                {
                    isAppInModeClosing = true;
                };
            }
            catch (Exception ex)
            {
                Program.SaveException(ex);
            }

        }
        private void fc_LoadDataPlanning(string sDateDeb, string sDateFin, string sCodeEtat = "", bool bFistLoad = false,
            bool bDateSelect = false, DateTime dtDebSel = default(DateTime), DateTime dtFinSel = default(DateTime))
        {

            string sSQL = null;

            string sWhere = null;
            string sWhere2 = null;
            string sOrderBy = null;
            string sIntervenant = null;
            string sUser = null;
            string sWhereStatut = null;

            bool bCreateResNull = false;
            bool bFirstSetData = false;
            bool bFirsCodeEtat = false;
            bool bEof = false;

            DataTable rs = default(DataTable);
            ModAdo rsAdo = new ModAdo();
            //Dim rsP             As ADODB.Recordset


            System.DateTime dtDateRVend = default(System.DateTime);
            System.DateTime dtDateHeureRv = default(System.DateTime);
            System.DateTime dtDate = default(System.DateTime);
            System.DateTime dtDateDeb = default(System.DateTime);
            System.DateTime dtDateFin = default(System.DateTime);
            System.DateTime dtTemp = default(System.DateTime);
            DateTime dtTempX = default(DateTime);


            CalendarEvent pevent = default(CalendarEvent);
            int lEventID = 0;

            int iRes = 0;
            int lIdShedule = 0;
            int i = 0;
            int iTab = 0;
            int xi = 0;
            int lViewType;
            int xi1;
            int w;



            CalendarResources arResources = new CalendarResources();


            CalendarResource[] pRes = null;


            CalendarSchedules pSchedules = default(CalendarSchedules);


            CalendarSchedule[] pSchedule = null;

            string[] tabStatut = null;
            DataTable vtab = new DataTable();


            //fc_PropertyCalandar
            try
            {
                ///================> Tested
                if (General.IsDate(sDateDeb))
                {
                    dtDateDeb = Convert.ToDateTime(sDateDeb);
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date Incorrecte", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                ///==================> Tested
                if (General.IsDate(sDateFin))
                {
                    dtDateFin = Convert.ToDateTime(sDateFin);
                }
                else
                {
                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Date Incorrecte", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                //DateCal.MonthDelta = Month(DTDateRealiseDe.value)


                if (string.IsNullOrEmpty(txtIntervenant.Text) && string.IsNullOrEmpty(CmbGroupe.Text))
                {
                    lblMes.Visible = true;
                    if (bFistLoad == true)
                    {
                        lblMes.Text = "Vous devez sélectionner à minima un intervenant ou un groupe d'intervenant.";
                        lbltot.Text = "Total : 0";
                        return;
                    }
                    else
                    {
                        lblMes.Text = "Vous devez sélectionner à minima un intervenant ou un groupe d'intervenant.";
                        lbltot.Text = "Total : 0";
                        return;
                    }
                }
                lblMes.Visible = false;

                //'fc_addResource

                //'fc_MajList

                if (!string.IsNullOrEmpty(sCodeEtat))
                {
                    sCodeEtat = sCodeEtat.Trim();
                    sCodeEtat = sCodeEtat.Replace(" ", "");

                    if (sCodeEtat.IndexOf(";") > 0)
                    {
                        tabStatut = sCodeEtat.Split(';');
                    }
                    else
                    {
                        tabStatut = new string[1];
                        tabStatut[0] = sCodeEtat;
                    }
                }
                ///==================> Tested
                else
                {
                    tabStatut = new string[1];
                    tabStatut[0] = "";
                }
                //
                //DateCal.SelectRange dtDateDeb, dtDateFin
                //
                //DateCal.ResyncCalendar
                //
                //DateCal.AttachToCalendar Cal


                DateCal.BoldDaysWithEvents = true;


                //=== supprime tous les events
                //Cal.DataProvider.RemoveAllEvents

                sUser = General.fncUserName();
                dtDate = DateTime.Now;

                ModPlanOutlook.tpEvent = null;
                Array.Resize(ref ModPlanOutlook.tpEvent, 1);


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;


                sSQL = "DELETE FROM PLANM_PLanMemoire WHERE  PLANM_Utilisateur = '" + StdSQLchaine.gFr_DoublerQuote(sUser) + "'";

                var xx = General.Execute(sSQL);

                //Cal.DataProvider.RemoveAllEvents


                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                //System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                //sSQL = "SELECT     PLANM_ID, Codeimmeuble, NoIntervention, PLANM_Date, PLANM_Utilisateur, PLANM_CreePar, PLANM_CreeLe, PLANM_PLanOuvert, PLANM_NoMaj, " _
                //& " PLANM_DateDuSelect , PLANM_DateAuSelect " _
                //& " From PLANM_PLanMemoire " _
                //& " WHERE     (PLANM_ID = 0)"

                //Set rsP = fc_OpenRecordSet(sSQL)


                //sSQL = "SELECT     Intervention.NumFicheStandard, Intervention.NoIntervention, Intervention.CodeImmeuble, intervention.DateSaisie, Intervention.Article," + " Intervention.Designation, " + " Intervention.Commentaire, Intervention.Intervenant, Intervention.DateSaisie, Intervention.DatePrevue, Intervention.HeurePrevue, Intervention.DureePrevue " + " ,Intervention.DateRealise, " + " Intervention.HeureDebut , Intervention.HeureFin, Intervention.Duree, Intervention.CodeEtat, Personnel.Nom, Personnel.Prenom " + " , intervention.NoMaj, TypeCodeEtat.CouleurStatut, Intervention.CodeEtat " + " FROM  Intervention LEFT OUTER JOIN " + " TypeCodeEtat ON Intervention.CodeEtat = TypeCodeEtat.CodeEtat LEFT OUTER JOIN " + " Personnel ON Intervention.Intervenant = Personnel.Matricule ";

                sSQL = cSQLPlann;
                sWhere = "";

                //If DTDatePrevueDe <> "" Then
                //    If sWhere = "" Then
                //            sWhere = " WHERE Intervention.DatePrevue >='" & DTDatePrevueDe & "'"
                //    Else
                //            sWhere = sWhere & " AND Intervention.DatePrevue >='" & DTDatePrevueDe & "'"
                //    End If
                //End If
                //If DTDatePrevueAu <> "" Then
                //    If sWhere = "" Then
                //            sWhere = " WHERE Intervention.DatePrevue <='" & DTDatePrevueAu & "'"
                //    Else
                //            sWhere = sWhere & " AND Intervention.DatePrevue <='" & DTDatePrevueAu & "'"
                //    End If
                //End If


                if (General.IsDate(sDateDeb) && General.IsDate(sDateFin))
                {
                    //=== reprend la prémière date du date picker.
                    //dtDateDeb = DateCal.FirstVisibleDay    'DateCal.Selection.Blocks(0).DateBegin

                    ///=================> Tested First
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        //sWhere = " WHERE ((Intervention.DateRealise >='" + dtDateDeb + "'" + "AND  Intervention.DateRealise <='" + dtDateFin + "')" + " OR " + " (Intervention.DatePrevue  >='" + dtDateDeb + "'" + " AND Intervention.DatePrevue  <='" + dtDateFin + "'" + " AND Intervention.DateRealise IS NULL) " + " OR " + " (Intervention.DateSaisie  >='" + dtDateDeb + "'" + "AND Intervention.DateSaisie  <='" + dtDateFin + "'" + " AND Intervention.DateRealise IS NULL " + " AND Intervention.DatePrevue IS NULL)) ";
                        sWhere = " WHERE ((Intervention.DateRealise >='" + dtDateDeb + "'"
                            + "AND  Intervention.DateRealise <='" + dtDateFin + "')"
                            + " OR "
                            + " (Intervention.DatePrevue  >='" + dtDateDeb + "'"
                            + " AND Intervention.DatePrevue  <='" + dtDateFin + "'"
                            + " AND Intervention.DateRealise IS NULL))";

                    }
                    else
                    {
                        //sWhere = " AND ((Intervention.DateRealise >='" + dtDateDeb + "'" + "AND  Intervention.DateRealise <='" + dtDateFin + "')" + " OR " + " (Intervention.DatePrevue  >='" + dtDateDeb + "'" + " AND Intervention.DatePrevue  <='" + dtDateFin + "')" + " OR " + " (Intervention.DateSaisie  >='" + dtDateDeb + "'" + "AND Intervention.DateSaisie  <='" + dtDateFin + "'))";
                        sWhere = " AND ((Intervention.DateRealise >='" + dtDateDeb + "'"
                            + "AND  Intervention.DateRealise <='" + dtDateFin + "')"
                            + " OR "
                            + " (Intervention.DatePrevue  >='" + dtDateDeb + "'"
                            + " AND Intervention.DatePrevue  <='" + dtDateFin + "'))";

                        //sWhere = sWhere & " AND (Intervention.DateRealise >='" & dtDateDeb & "'" _
                        //& " or Intervention.DatePrevue  >='" & dtDateDeb & "'" _
                        //& " or Intervention.DateSaisie  >='" & dtDateDeb & "')"
                    }
                }


                //If IsDate(sDateFin) Then
                //    '=== reprend la derniére date du date picker.
                //    'dtDateFin = DateCal.LastVisibleDay   'DateCal.Selection.Blocks(0).DateEnd
                //    If sWhere = "" Then
                //            sWhere = " WHERE (Intervention.DateRealise <='" & dtDateFin & "'" _
                //'                & " or Intervention.DatePrevue  <='" & dtDateFin & "'" _
                //'                & " or Intervention.DateSaisie  <='" & dtDateFin & "')"
                //
                //    Else
                //            sWhere = sWhere & " AND (Intervention.DateRealise <='" & dtDateFin & "'" _
                //'                & " or Intervention.DatePrevue  <='" & dtDateFin & "'" _
                //'                & " or Intervention.DateSaisie  <='" & dtDateFin & "')"
                //    End If
                //End If

                //==== Gestion des statuts multiples.
                bFirsCodeEtat = true;
                sWhereStatut = "";

                for (xi = 0; xi <= tabStatut.Length - 1; xi++)
                {
                    if (!string.IsNullOrEmpty(tabStatut[xi]) && tabStatut[xi] != ";")
                    {
                        if (string.IsNullOrEmpty(sWhere) && string.IsNullOrEmpty(sWhereStatut))
                        {
                            sWhereStatut = " WHERE ( Intervention.CodeEtat  ='" + StdSQLchaine.gFr_DoublerQuote(tabStatut[xi]) + "'";
                        }
                        else
                        {
                            if (string.IsNullOrEmpty(sWhere) && !string.IsNullOrEmpty(sWhereStatut))
                            {
                                sWhereStatut = sWhereStatut + " OR Intervention.CodeEtat ='" + StdSQLchaine.gFr_DoublerQuote(tabStatut[xi]) + "'";
                            }
                            else
                            {
                                if (bFirsCodeEtat == true)
                                {
                                    bFirsCodeEtat = false;
                                    sWhereStatut = sWhereStatut + " AND ( Intervention.CodeEtat ='" + StdSQLchaine.gFr_DoublerQuote(tabStatut[xi]) + "'";
                                }
                                else if (bFirsCodeEtat == false)
                                {
                                    sWhereStatut = sWhereStatut + " or Intervention.CodeEtat ='" + StdSQLchaine.gFr_DoublerQuote(tabStatut[xi]) + "'";
                                }
                            }
                        }
                    }
                }

                if (!string.IsNullOrEmpty(sWhereStatut))
                {
                    sWhereStatut = sWhereStatut + ")";
                    sWhere = sWhere + sWhereStatut;
                }

                if (!string.IsNullOrEmpty(CmbGroupe.Text))
                {
                    sWhere2 = "SELECT     GRID_GroupeIntervenantDetail.GRID_Mat"
                        + " FROM         GRI_GroupeIntervenant INNER JOIN "
                        + " GRID_GroupeIntervenantDetail ON GRI_GroupeIntervenant.GRI_ID = GRID_GroupeIntervenantDetail.GRI_ID "
                        + " WHERE  GRI_GroupeIntervenant.GRI_Nom = '" + StdSQLchaine.gFr_DoublerQuote(CmbGroupe.Text) + "'";


                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE (Intervention.Intervenant  IN (" + sWhere2 + ") ";
                    }
                    else
                    {
                        sWhere = sWhere + " AND (Intervention.Intervenant  IN (" + sWhere2 + ") ";
                    }

                    if (!string.IsNullOrEmpty(CmbGroupe.Text) && !string.IsNullOrEmpty(txtIntervenant.Text))
                    {
                        sWhere = sWhere + " OR Intervention.Intervenant ='" + StdSQLchaine.gFr_DoublerQuote(txtIntervenant.Text) + "'";
                    }

                    sWhere = sWhere + ")";

                }


                if (string.IsNullOrEmpty(CmbGroupe.Text) && !string.IsNullOrEmpty(txtIntervenant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.Intervenant  ='" + StdSQLchaine.gFr_DoublerQuote(txtIntervenant.Text) + "'";
                    }
                    ///===============> Tested
                    else
                    {
                        sWhere = sWhere + " AND Intervention.Intervenant ='" + StdSQLchaine.gFr_DoublerQuote(txtIntervenant.Text) + "'";
                    }
                    //    If sWhere = "" Then
                    //            sWhere = " WHERE (Intervention.Intervenant  ='XX1' OR Intervention.Intervenant  ='XX2')"
                    //    Else
                    //            sWhere = sWhere & " AND (Intervention.Intervenant  ='XX1' OR Intervention.Intervenant  ='XX2')"
                    //    End If


                }
                else if (string.IsNullOrEmpty(CmbGroupe.Text) && string.IsNullOrEmpty(txtIntervenant.Text))
                {
                    if (string.IsNullOrEmpty(sWhere))
                    {
                        sWhere = " WHERE Intervention.Intervenant  is not null";
                    }
                    else
                    {
                        sWhere = sWhere + " AND  (Intervention.Intervenant is not null and  Intervention.Intervenant <> '')";
                    }


                }


                sOrderBy = " ORDER BY Intervention.Intervenant, Intervention.DateRealise ";

                ModPlanOutlook.fc_SetWherePlanOut(sWhere);
                System.Windows.Forms.Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                var ModAdo = new ModAdo();
                rs = ModAdo.fc_OpenRecordSet(sSQL + sWhere + sOrderBy, ChangeCursor: false);

                Label567.Text = "";
                //Debug.Print sSQL & sWhere & sOrderBy
                sIntervenant = "";
                bCreateResNull = false;
                iRes = 0;
                bFirstSetData = true;
                ///=============> Tested First
                if (rs.Rows.Count > 0)
                {
                    bEof = false;
                    vtab = rs;
                    lbltot.Text = "Total : " + rs.Rows.Count;
                }
                else
                {
                    //'=== modif du 28 03 2019, ajout de.
                    Cal.DataProvider.RemoveAllEvents();
                    if (bDateSelect == true)
                        DateCal.SelectRange(dtDebSel, dtFinSel);
                    else
                    {
                        fc_selectRange(ref dtDateDeb, ref dtDateFin);
                        DateCal.SelectRange(dtDateDeb, dtDateFin);
                    }

                    Cal.Populate();
                    Cal.RedrawControl();
                    DateCal.ResyncCalendar();

                    if (bload == false)
                        FC_SaveParam();

                    bEof = true;
                    //  '=== modif du 28 03 2019, fin ajout.
                }

                fc_addResource(vtab);

                //=== attention la mise a jour des couleurs doit toujours se faire apres la création des ressources.
                fc_MajList();

                rs.Dispose();
                rs = null;

                if (bEof == false)
                {
                    Cal.DataProvider.RemoveAllEvents();

                    ModAdo rsPModAdo = null;

                    Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;

                    //==== INSERT LES EVENEMENTS PLANINFIES.
                    Parallel.For(0, vtab.Rows.Count, new ParallelOptions { MaxDegreeOfParallelism = 2 }, ww =>
                    {
                        ModPlanOutlook.fc_InsertEventADO(lIdShedule, true, this, dtDateDeb, dtDateFin, vtab, ww, ref rsPModAdo);
                    });

                    //for (w = 0; w < vtab.Rows.Count; w++)
                    //{
                    //Cursor.Current = System.Windows.Forms.Cursors.WaitCursor;
                    //'==== INSERT LES EVENEMENTS PLANINFIES.
                    //General.sSQL = "SELECT     PLANM_ID, Codeimmeuble, NoIntervention, PLANM_Date, PLANM_Utilisateur, PLANM_CreePar, PLANM_CreeLe, PLANM_PLanOuvert, PLANM_NoMaj, "
                    //+ " PLANM_DateDuSelect , PLANM_DateAuSelect " + " From PLANM_PLanMemoire WHERE     (PLANM_ID = 0)";
                    //ModPlanOutlook.fc_InsertEventADO(lIdShedule, true, this, dtDateDeb, dtDateFin, vtab, w, ref rsPModAdo);
                    //}

                    var aa = rsPModAdo.Update();
                    w = vtab.Rows.Count;

                    if (w == 1)
                    {
                        Label567.Text = w + " intervention du " + DateCal.FirstVisibleDay.Day + "/" +
                                        DateCal.FirstVisibleDay.Month + " au " + DateCal.LastVisibleDay.Day + "/" +
                                        DateCal.LastVisibleDay.Month;
                    }
                    ///====================> Tested
                    else
                    {
                        Label567.Text = w + " intervention du " + DateCal.FirstVisibleDay.Day + "/" +
                                       DateCal.FirstVisibleDay.Month + " au " + DateCal.LastVisibleDay.Day + "/" +
                                       DateCal.LastVisibleDay.Month;
                    }
                }

                if (bEof == false)
                {
                    if (bDateSelect == true)
                    {
                        DateCal.SelectRange(dtDebSel, dtFinSel);
                    }
                    else
                    {
                        fc_selectRange(ref dtDateDeb, ref dtDateFin);
                        DateCal.SelectRange(dtDateDeb, dtDateFin);
                    }

                    DateCal.ResyncCalendar();
                    Cal.Populate();
                    Cal.RedrawControl();
                }

                ///=============> Tested
                if (bload == false)
                {
                    FC_SaveParam();
                }

                Cursor.Current = Cursors.Default;
                DateCal.BoldDaysWithEvents = true;
                Timer1.Enabled = true;

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_LoadDataPlanning");
                Timer1.Enabled = true;
            }


        }

        private void fc_selectRange(ref DateTime dtDateDeb, ref DateTime dtDateFin)
        {
            DateTime dtTempX;
            int lNbMonth;
            string sMonth;
            int i;
            int[] tabNbdays = default(int[]);
            int[] tabNbMonth = default(int[]);
            int[] tabNbYear = default(int[]);
            int lMonthIn = 0;
            int lNbDaysIn = 0;
            int lTemp;
            int lYearIn = 0;
            int lday;
            try
            {
                //'=== sur le datepicker, des mois peuvent peuvent se chevaucher (max 3)
                //'=== determine quel est le mois en cours sur le DatePicker.
                //'===On considere que c est le nombre de jour le plus elevé des mois qui nous donne cette donnée.
                //'lNbMonth = DateDiff("m", dtDateDeb, dtDateFin)
                dtTempX = dtDateDeb;
                i = 0;
                Array.Resize(ref tabNbdays, i + 1);
                Array.Resize(ref tabNbMonth, i + 1);
                Array.Resize(ref tabNbYear, i + 1);
                tabNbdays[i] = 0;
                tabNbMonth[i] = dtTempX.Month;
                tabNbYear[i] = dtTempX.Year;

                while (dtTempX <= dtDateFin)
                {
                    if (dtTempX.Month != tabNbMonth[i])
                    {
                        i = i + 1;
                        Array.Resize(ref tabNbdays, i + 1);
                        Array.Resize(ref tabNbMonth, i + 1);
                        Array.Resize(ref tabNbYear, i + 1);
                        tabNbdays[i] = 1;
                        tabNbMonth[i] = dtTempX.Month;
                        tabNbYear[i] = dtTempX.Year;
                    }
                    ///===============> Tested
                    else
                    {
                        tabNbdays[i] = tabNbdays[i] + 1;
                    }

                    dtTempX = dtTempX.AddDays(1);
                }

                lNbDaysIn = 0;
                lTemp = 0;

                for (int j = 0; j < tabNbMonth.Length; j++)
                {
                    if (tabNbdays[j] >= lTemp)
                    {
                        lTemp = tabNbdays[j];
                        lNbDaysIn = tabNbdays[j];
                        lMonthIn = tabNbMonth[j];
                        lYearIn = tabNbYear[j];
                    }
                }
                if (Cal.ViewType == CalendarViewType.xtpCalendarWorkWeekView)
                {
                    dtDateDeb = Convert.ToDateTime("01/" + lMonthIn + "/" + lYearIn);
                    dtDateDeb = fDate.fc_ReturnDayNext(dtDateDeb, modP2.cSLundi);
                    dtDateFin = dtDateDeb.AddDays(4);
                }
                else if (Cal.ViewType == CalendarViewType.xtpCalendarFullWeekView || Cal.ViewType == CalendarViewType.xtpCalendarWeekView)
                {
                    dtDateDeb = Convert.ToDateTime("01" + "/" + lMonthIn + "/" + lYearIn);
                    dtDateDeb = fDate.fc_ReturnDayNext(dtDateDeb, modP2.cSLundi);
                    dtDateFin = dtDateDeb.AddDays(6);
                }
                else if (Cal.ViewType == CalendarViewType.xtpCalendarDayView)
                {
                    if (DateCal.Selection.BlocksCount != 0)
                    {
                        lday = DateCal.Selection[0].DateBegin.Day;
                        // '=== gere le mois de fevrier.
                        if (lday <= 28)
                        {
                            dtDateDeb = Convert.ToDateTime(lday + "/" + lMonthIn + "/" + lYearIn);
                        }

                        else if (General.IsDate(lday + "/" + lMonthIn + "/" + lYearIn))
                        {
                            dtDateDeb = Convert.ToDateTime(lday + "/" + lMonthIn + "/" + lYearIn);
                        }

                        else
                        {
                            while (!General.IsDate(lday + "/" + lMonthIn + "/" + lYearIn))
                            {
                                lday = lday - 1;
                            }
                            dtDateDeb = Convert.ToDateTime(lday + "/" + lMonthIn + "/" + lYearIn);
                        }
                    }
                    ///====================> Tested
                    else
                    {
                        dtDateDeb = Convert.ToDateTime("01" + "/" + lMonthIn + "/" + lYearIn);
                    }
                    dtDateFin = dtDateDeb;
                }
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, Name + ";fc_selectRange");
            }
        }

        private void FC_SaveParam()
        {
            try
            {
                object functionReturnValue = null;
                object xtpCalendarTimeLineView = null;


                General.saveInReg(this.Name, txtIntervenant.Name, txtIntervenant.Text);
                General.saveInReg(this.Name, CmbGroupe.Name, CmbGroupe.Text);
                General.saveInReg(this.Name, txtCodeEtatsInclus.Name, txtCodeEtatsInclus.Text);

                //TODO 
                //General.saveInReg(this.Name, PicForm.Name + "_Width", Convert.ToString(Microsoft.VisualBasic.Compatibility.VB6.Support.FromPixelsUserWidth(PicForm.Width, 15375, 1025)));
                //General.saveInReg(this.Name, PicForm.Name + "_Height", Convert.ToString(Microsoft.VisualBasic.Compatibility.VB6.Support.FromPixelsUserHeight(PicForm.Height, 35573.1, 707)));

                General.saveInReg(this.Name, FraConteneur.Name + "_Width", FraConteneur.Width.ToString());
                General.saveInReg(this.Name, FraConteneur.Name + "_Height", FraConteneur.Height.ToString());



                General.saveInReg(this.Name, Cal.Name + "_Width", Cal.Width.ToString());
                General.saveInReg(this.Name, Cal.Name + "_Height", Cal.Height.ToString());

                General.saveInReg(this.Name, "Col_Width", Cal.DayView.MinColumnWidth.ToString());


                if (General.IsDate(DTDateRealiseDe.Value))
                {
                    General.saveInReg(this.Name, DTDateRealiseDe.Name, DTDateRealiseDe.Value.ToString());
                }

                if (General.IsDate(DTDateRealiseAu.Value))
                {
                    General.saveInReg(this.Name, DTDateRealiseAu.Name, DTDateRealiseAu.Value.ToString());
                }

                //SaveSetting cFrNomApp, Me.Name, "DateCal.Selection(0).DateBegin", DateCal.Selection(0).DateBegin
                //SaveSetting cFrNomApp, Me.Name, "DateCal.Selection(0).DateEnd", DateCal.Selection(0).DateEnd


                int DaysCount = 0;

                //if (Cal.ViewType != CalendarViewType.xtpCalendarTimeLineView)
                //{
                //    DaysCount = ModPlanOutlook.CalendarControl.ActiveView.DaysCount;

                //    General.saveInReg(this.Name, "DateDebutVue", Cal.ActiveView..Date);
                //    General.saveInReg(this.Name, "DateFinVue", Cal.ActiveView.Days(DaysCount - 1).Date);
                //}
                //else if (Cal.ViewType == xtpCalendarTimeLineView)
                //{

                //    General.saveInReg(this.Name, "DateDebutVue", Cal.DayView.Selection.Begin);

                //    General.saveInReg(this.Name, "DateFinVue", Cal.DayView.Selection.End);
                //}

                if (DateCal.Selection.BlocksCount != 0)
                {
                    General.saveInReg(this.Name, "DateDebutPickerSelecttion", DateCal.Selection[0].DateBegin.ToString());
                    General.saveInReg(this.Name, "DateFinPickerSelection", DateCal.Selection[0].DateEnd.ToString());
                }

                General.saveInReg(this.Name, "DateDebutPicker", DateCal.FirstVisibleDay.ToString());
                General.saveInReg(this.Name, "DateFinPicker", DateCal.LastVisibleDay.ToString());
                General.saveInReg(this.Name, "ViewType_Cal", Convert.ToInt32(Cal.ViewType).ToString());

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";FC_SaveParam");

            }

        }


        private void fc_GetParam()
        {
            try
            {

                string stemp = null;

                txtIntervenant.Text = General.getFrmReg(this.Name, txtIntervenant.Name, "");
                CmbGroupe.Text = General.getFrmReg(this.Name, CmbGroupe.Name, "");
                txtCodeEtatsInclus.Text = General.getFrmReg(this.Name, txtCodeEtatsInclus.Name, "");

                //todo
                //PicForm.Width = Microsoft.VisualBasic.Compatibility.VB6.Support.ToPixelsUserWidth(Convert.ToSingle(Interaction.GetSetting(General.cFrNomApp, this.Name, PicForm.Name + "_Width", Convert.ToString(cPicForm_Width))), 15375, 1025);
                //PicForm.Height = Microsoft.VisualBasic.Compatibility.VB6.Support.ToPixelsUserHeight(Convert.ToSingle(Interaction.GetSetting(General.cFrNomApp, this.Name, PicForm.Name + "_Height", Convert.ToString(cPicForm_Height))), 35573.1, 707);

                FraConteneur.Width = Convert.ToInt32(General.getFrmReg(this.Name, FraConteneur.Name + "_Width", Convert.ToString(cFraConteneur_Width)));
                FraConteneur.Height = Convert.ToInt32(General.getFrmReg(this.Name, FraConteneur.Name + "_Height", Convert.ToString(cFraConteneur_Height)));


                Cal.Width = Convert.ToInt32(General.getFrmReg(this.Name, Cal.Name + "_Width", cCal_Width));
                Cal.Height = Convert.ToInt32(General.getFrmReg(this.Name, Cal.Name + "_Height", cCal_Height));


                //stemp = GetSetting(cFrNomApp, Me.Name, DTDateRealiseDe.Name, "")
                //If stemp = "" Then
                //
                //Else
                //    DTDateRealiseDe.value = GetSetting(cFrNomApp, Me.Name, DTDateRealiseDe.Name, Date)
                //End If
                //
                //DTDateRealiseAu.value = GetSetting(cFrNomApp, Me.Name, DTDateRealiseAu.Name, Date + 5)
                //
                //DateCal.SelectRange GetSetting(cFrNomApp, Me.Name, "DateCal.Selection(0).DateBegin", Date), GetSetting(cFrNomApp, Me.Name, "DateCal.Selection(0).DateEnd", Date + 5)

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_GetParam");
            }


        }

        /// <summary>
        /// Tested
        /// </summary>
        public void fc_MajList()
        {
            object functionReturnValue = null;
            CalendarEventLabels oLists = default(CalendarEventLabels);
            CalendarEventLabel oList = default(CalendarEventLabel);
            int i = 0;
            string sColor = null;
            bool bexist = false;
            string sSQL;
            DataTable rs;

            try
            {
                sSQL = "SELECT     CouleurStatut From TypeCodeEtat  WHERE     (CouleurStatut IS NOT NULL)";
                var ModAdo = new ModAdo();
                rs = ModAdo.fc_OpenRecordSet(sSQL);
                foreach (DataRow dr in rs.Rows)
                {
                    sColor = dr["CouleurStatut"].ToString();
                    oList = Cal.DataProvider.LabelList().Find(Convert.ToInt32(sColor));
                    if (oList == null)
                        Cal.DataProvider.LabelList().AddLabel(Convert.ToInt32(sColor), Convert.ToUInt32(sColor), sColor);
                }
                ModAdo.fc_CloseRecordset(rs);
                sSQL = "SELECT     Codeimmeuble, CouleurDeFond From ImmeubleColor WHERE     (CouleurDeFond IS NOT NULL)";
                rs = ModAdo.fc_OpenRecordSet(sSQL);
                foreach (DataRow dr in rs.Rows)
                {
                    sColor = dr["CouleurDeFond"].ToString();
                    oList = Cal.DataProvider.LabelList().Find(Convert.ToInt32(sColor));
                    if (oList == null)
                        Cal.DataProvider.LabelList().AddLabel(Convert.ToInt32(sColor), Convert.ToUInt32(sColor), sColor);
                }
                ModAdo.fc_CloseRecordset(rs);
                rs = null;
                return;

            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, ";fc_MajList");
                //return functionReturnValue;
            }
        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void Timer1_Tick(object sender, EventArgs e)
        {
            string sSQL = null;
            string stable = null;
            int LID = 0;
            int lIdShedule = 0;
            int i = 0;
            DataTable rs = default(DataTable);
            ModAdo modAdoRs = new ModAdo();
            int x1 = 0;
            bool bInsert = false;
            DataTable vtab = default(DataTable);
            if (!Visible) return;
            //MsgBox "ok"
            try
            {

                //Application.DoEvents();
                //'=== test si le tableau est vide equivaut à aucune intervention dans le planning.

                if (ModPlanOutlook.tpEvent != null && ModPlanOutlook.tpEvent.Length == 0)
                    return;


                ModPlanOutlook.fc_CtrlEventMaj(General.fncUserName());

                lblTimer.BackColor = System.Drawing.Color.Lime;


                lblTimer.Text = (Convert.ToInt32(General.nz(lblTimer.Text, 0)) + 1).ToString();

                bInsert = false;
                //Timer1.Enabled = false;
                for (i = 0; i <= ModPlanOutlook.iTabEvent.Length - 1; i++)
                {

                    ////=================> Ttested
                    if (ModPlanOutlook.iTabEvent[0] == 0)
                    {
                        break;
                    }
                    else
                    {
                        if (isAppInModeClosing)///Added by Mohammed to cancel the event when app is closed
                            return;

                        for (x1 = 0; x1 <= ModPlanOutlook.iTabEvent.Length - 1; x1++)
                        {
                            if (isAppInModeClosing)///Added by Mohammed to cancel the event when app is closed
                                return;
                            LID = ModPlanOutlook.fc_ReturnIdEvent(ModPlanOutlook.iTabEvent[x1]);

                            if (LID != 0)
                            {
                                var eventID = Cal.DataProvider.GetEvent(LID);
                                //This Condition added to catch an exception
                                if (eventID != null)
                                {
                                    //=== supprime l'événement afin de le recréer.
                                    Cal.DataProvider.DeleteEvent(eventID);
                                    Cal.RedrawControl();
                                }
                            }
                            else
                            {
                                //== si LID = 0 on considére qu'il faut ajouter cette inyterevntion dans le planning.
                            }

                            sSQL = cSQLPlann;

                            //If iTabEvent(x1) = 907464 Then Stop

                            sSQL = sSQL + " WHERE Intervention.NoIntervention = " + ModPlanOutlook.iTabEvent[x1];

                            rs = new DataTable();

                            //System.Windows.Forms.Application.DoEvents();

                            rs = modAdoRs.fc_OpenRecordSet(sSQL, ChangeCursor: false);
                            vtab = rs;


                            if (vtab.Rows.Count > 0)
                            {

                                ///This condition  added by mohammed to catch an exception -- System.IndexOutOfRangeException. error message ==> L'index se trouve en dehors des limites du tableau. 
                                /// sometimes the value of x1 variable is greater than ModPlanOutlook.iTabEvent.Length. i don't know why. I think System.Windows.Forms.Application.DoEvents() is responsable on this error
                                if (ModPlanOutlook.iTabEvent != null && x1 >= ModPlanOutlook.iTabEvent.Length)
                                    x1 = ModPlanOutlook.iTabEvent.Length - 1;

                                ModAdo modAdo = null;
                                ModPlanOutlook.fc_InsertEventADO(0, false, this, DateCal.FirstVisibleDay, DateCal.LastVisibleDay, vtab, 0, ref modAdo);

                                bInsert = true;

                                FC_SaveParam();
                                //==== modif du 26 06 2018, mets à jour le nuémro de version.

                                sSQL = "UPDATE    PLANM_PLanMemoire SET PLANM_NoMaj = " + General.nz(vtab.Rows[0][ModPlanOutlook.cNoMaj], 0) + "  WHERE   NoIntervention = "
                                    + General.nz(ModPlanOutlook.iTabEvent[x1], 0) + " AND PLANM_Utilisateur = '" + StdSQLchaine.gFr_DoublerQuote(General.fncUserName())
                                    + "' AND Codeimmeuble = '" + StdSQLchaine.gFr_DoublerQuote(vtab.Rows[0][ModPlanOutlook.cCodeImmeuble2].ToString()) + "'";
                                var xxx = General.Execute(sSQL);

                            }

                            rs.Dispose();

                            rs = null;
                            System.Windows.Forms.Application.DoEvents();
                            //fc_MajEvent Cal, iTabEvent(i), LID
                        }
                    }
                }
                //Timer1.Enabled = true;
                ///================> Tested
                if (bInsert == true)
                {
                    Cal.Populate();
                    Cal.RedrawControl();
                }

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";Timer1_Timer");

            }



        }


        private void txtCodeEtatsInclus_TextChanged(object sender, EventArgs e)
        {
            //if (bload == false)
            //{
            //    fc_refresh();
            //}
        }

        private void txtCodeEtatsInclus_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)(e.KeyChar);
            if (KeyAscii == 13)
            {
                cmdStatutInclus_Click(cmdStatutInclus, new System.EventArgs());

                KeyAscii = 0;
            }
        }

        private void txtIntervenant_TextChanged(object sender, EventArgs e)
        {
            if (bload == false)
            {
                //FC_intervenant();
                ////Cal.DataProvider.RemoveAllEvents
                //fc_refresh();

            }
            if (txtIntervenant.Text == "")
                lbllibIntervenant.Text = "";
        }
        private void FC_intervenant()
        {
            try
            {
                using (var tmp = new ModAdo())

                    lbllibIntervenant.Text = tmp.fc_ADOlibelle("SELECT Personnel.Nom FROM" + " Personnel WHERE Personnel.Matricule='" + txtIntervenant.Text + "'");
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_intervenant");
            }


        }

        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="bForce"></param>
        /// <param name="bDateSelect"></param>
        /// <param name="dtDebSel"></param>
        /// <param name="dtFinSel"></param>
        public void fc_refresh(bool bForce = false, bool bDateSelect = false, DateTime dtDebSel = default(DateTime), DateTime dtFinSel = default(DateTime))
        {
            DateTime dtSelecBegin;
            DateTime dtSelecEnd;
            int lTypeView;
            DateTime dfBebView;
            DateTime dtEndView;
            try
            {
                if (ModPlanOutlook.dtFirstDayVisible != DateCal.FirstVisibleDay
                    && ModPlanOutlook.dtLastDayVisible != DateCal.LastVisibleDay
                    && bload == false
                    || General.UCase(ModPlanOutlook.sPlanningStatut) != General.UCase(txtCodeEtatsInclus.Text)
                    || General.UCase(ModPlanOutlook.sPlanningInterv) != General.UCase(txtIntervenant.Text)
                    || General.UCase(ModPlanOutlook.sPlanningGroupe) != General.UCase(CmbGroupe.Text) || bForce == true)
                {

                    ModPlanOutlook.sPlanningStatut = txtCodeEtatsInclus.Text;
                    ModPlanOutlook.sPlanningInterv = txtIntervenant.Text;
                    ModPlanOutlook.sPlanningGroupe = CmbGroupe.Text;


                    ModPlanOutlook.dtFirstDayVisible = DateCal.FirstVisibleDay;
                    ModPlanOutlook.dtLastDayVisible = DateCal.LastVisibleDay;

                    //=== enregiste le type de vue (semaine, mois, jour)

                    lTypeView = Convert.ToInt32(Cal.ViewType);
                    dfBebView = ModPlanOutlook.dtFirstDayVisible;
                    Timer1.Enabled = false;
                    fc_LoadDataPlanning(ModPlanOutlook.dtFirstDayVisible.ToString(), ModPlanOutlook.dtLastDayVisible.ToString(), txtCodeEtatsInclus.Text, bload, bDateSelect, dtDebSel, dtFinSel);
                    Timer1.Enabled = true;
                }
                //fc_LoadDataPlanning(DateCal.Selection[0].DateBegin.ToString(), DateCal.Selection[0].DateEnd.ToString(), txtCodeEtatsInclus.ToString());

                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, this.Name + ";fc_refresh");
                Timer1.Enabled = true;
            }
        }
        /// <summary>
        /// Tested
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void txtIntervenant_KeyPress(object sender, KeyPressEventArgs e)
        {
            short KeyAscii = (short)(e.KeyChar);
            string sSQl = "";
            if (KeyAscii == 13)
            {
                using (var tmpModAdo = new ModAdo())
                {
                    sSQl = tmpModAdo.fc_ADOlibelle("SELECT Personnel.Nom FROM Personnel WHERE Personnel.Matricule='" + txtIntervenant.Text + "'");

                    if (string.IsNullOrEmpty(sSQl))
                    {
                        string requete = "SELECT Personnel.Matricule as \"Matricule \"," + " Personnel.Nom as \"Nom\","
                                    + " Personnel.prenom as \"Prenom\"," + " Qualification.Qualification as \"Qualification\"," +
                                      " Personnel.Memoguard as \"MemoGuard\","
                                   + " Personnel.NumRadio as \"NumRadio\""
                                   + " FROM Qualification right JOIN Personnel ON Qualification.CodeQualif = Personnel.CodeQualif";

                        string where_order = " (NonActif is null or NonActif = 0) ";
                        SearchTemplate fg = new SearchTemplate(this, null, requete, where_order, "") { Text = "" };

                        if (General.IsNumeric(txtIntervenant.Text))
                            fg.SetValues(new Dictionary<string, string> { { "Matricule", txtIntervenant.Text } });
                        else
                            fg.SetValues(new Dictionary<string, string> { { "Nom", txtIntervenant.Text } });

                        fg.ugResultat.DoubleClickRow += (se, ev) =>
                        {

                            txtIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                            lbllibIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString();

                            fg.Dispose();
                            fg.Close();
                            //'=== fc_refresh

                            fc_refresh(true, true, DateCal.Selection[0].DateBegin, DateCal.Selection[0].DateEnd);

                        };

                        fg.ugResultat.KeyDown += (se, ev) =>
                        {

                            if (ev.KeyCode == Keys.Enter && fg.ugResultat.ActiveRow != null)
                            {
                                txtIntervenant.Text = fg.ugResultat.ActiveRow.Cells[1].Value.ToString();
                                lbllibIntervenant.Text = fg.ugResultat.ActiveRow.Cells[2].Value.ToString() + " " + fg.ugResultat.ActiveRow.Cells[3].Value.ToString();

                                fg.Dispose();
                                fg.Close();
                                //'=== fc_refresh

                                fc_refresh(true, true, DateCal.Selection[0].DateBegin, DateCal.Selection[0].DateEnd);

                            }
                        };
                        fg.StartPosition = FormStartPosition.CenterParent;
                        fg.ShowDialog();

                    }
                    else
                    {
                        lbllibIntervenant.Text = sSQl;
                        //'=== fc_refresh

                        fc_refresh(true, true, DateCal.Selection[0].DateBegin, DateCal.Selection[0].DateEnd);

                    }

                }

            }


        }

        private void fc_addResource(DataTable vtab)
        {
            int i = 0;
            int X;
            string sSQL;
            DataTable rs = default(DataTable);
            bool bIn = false;
            CalendarResource[] pRes = default(CalendarResource[]);
            int iRes;
            bool bFirstSetData;
            bool bSup = true;
            CalendarSchedules pSchedules = null;
            CalendarSchedule[] pSchedule = default(CalendarSchedule[]);
            CalendarResources arResources = new CalendarResources();
            string[] tpInterv = default(string[]);
            string stemp;
            int lWidth;
            int w;
            string sIntervenant;
            bool bForce;
            try
            {
                X = 0;
                bForce = false;
                var ModAdo = new ModAdo();
                if (!string.IsNullOrEmpty(CmbGroupe.Text) || !string.IsNullOrEmpty(txtIntervenant.Text))
                {
                    ////===============> Tested
                    if (CmbGroupe.Text != "")
                    {
                        //sSQL = "SELECT     GRID_GroupeIntervenantDetail.GRID_Mat"
                        //       + " FROM         GRI_GroupeIntervenant INNER JOIN "
                        //       + " GRID_GroupeIntervenantDetail ON GRI_GroupeIntervenant.GRI_ID = GRID_GroupeIntervenantDetail.GRI_ID "
                        //       + " WHERE     GRI_GroupeIntervenant.GRI_Nom = '" +
                        //       StdSQLchaine.gFr_DoublerQuote(CmbGroupe.Text) + "'";

                        sSQL = "SELECT     GRID_GroupeIntervenantDetail.GRID_Mat, Personnel.Nom, Personnel.Noauto"

                        + " FROM         GRI_GroupeIntervenant INNER JOIN "

                        + " GRID_GroupeIntervenantDetail ON GRI_GroupeIntervenant.GRI_ID = GRID_GroupeIntervenantDetail.GRI_ID INNER JOIN "

                        + " Personnel ON GRID_GroupeIntervenantDetail.GRID_Mat = Personnel.Matricule "

                        + " WHERE     GRI_GroupeIntervenant.GRI_Nom = '" + StdSQLchaine.gFr_DoublerQuote(CmbGroupe.Text) + "'"

                        + " order by GRID_Ordre, GRID_Mat  ";


                        rs = ModAdo.fc_OpenRecordSet(sSQL);
                        foreach (DataRow dr in rs.Rows)
                        {
                            Array.Resize(ref tpInterv, X + 1);
                            tpInterv[X] = dr["GRID_Mat"].ToString();
                            X++;
                        }
                    }

                    if (txtIntervenant.Text != "")
                    {
                        sSQL = "SELECT    Matricule From Personnel WHERE     Matricule = '" +
                               StdSQLchaine.gFr_DoublerQuote(txtIntervenant.Text) + "'";
                        sSQL = ModAdo.fc_ADOlibelle(sSQL);
                        if (sSQL != "")
                        {
                            //if (X == 0)
                            Array.Resize(ref tpInterv, X + 1);
                            //else
                            //    Array.Resize(ref tpInterv, X);
                            tpInterv[X] = txtIntervenant.Text;
                            X++;
                        }
                    }
                }
                else if (vtab.Rows.Count > 0)
                {
                    sIntervenant = "";
                    for (w = 0; w < vtab.Rows[2].ItemArray.Length; w++)
                    {
                        if (General.UCase(sIntervenant) != General.UCase(vtab.Rows[w][ModPlanOutlook.cIntervenant]))
                        {
                            sIntervenant = vtab.Rows[w][ModPlanOutlook.cIntervenant].ToString();
                            //if (X == 0)
                            Array.Resize(ref tpInterv, X + 1);
                            //else
                            //    Array.Resize(ref tpInterv, X);
                            tpInterv[X] = sIntervenant;
                            X++;
                        }
                    }
                }
                else
                {
                    //if (X == 0)
                    Array.Resize(ref tpInterv, X + 1);
                    //else
                    //    Array.Resize(ref tpInterv, X);
                    tpInterv[X] = "";
                    bForce = true;
                }
                //'=== controle si il manque une ressource.
                if (tpInterv != null)
                {

                    for (X = 0; X < tpInterv.Length; X++)
                    {
                        bIn = false;
                        for (i = 0; i < Cal.MultipleResources.Count; i++)
                            if (General.UCase(Cal.MultipleResources[i].Name) == General.UCase(tpInterv[X]))
                                bIn = true;

                        ///============> Tested
                        if (bIn == false)
                            break;
                    }
                }

                //'=== controle si une ressource a été suprimé.
                if (bIn == true)
                {
                    for (i = 0; i < Cal.MultipleResources.Count; i++)
                    {
                        bSup = true;
                        if (tpInterv != null)
                        {
                            for (X = 0; X < tpInterv.Length; X++)
                            {
                                if (General.UCase(tpInterv[X]) == General.UCase(Cal.MultipleResources[i].Name))
                                    bSup = false;
                            }
                            if (bSup == true)
                                break;
                        }

                    }

                }
                if (bIn == false || bSup == true || bForce == true)
                {
                    Timer1.Enabled = false;
                    ///============> Tested
                    while (i > 0)
                    {
                        Cal.MultipleResources.Remove(i - 1);
                        i = i - 1;
                    }
                    iRes = 0;
                    bFirstSetData = true;
                    lWidth = Cal.DayView.MinColumnWidth = 0;
                    if (tpInterv != null)
                    {
                        for (X = 0; X < tpInterv.Length; X++)
                        {
                            Array.Resize(ref pRes, iRes + 1);
                            pRes[iRes] = new CalendarResource();
                            ///==============> Tested
                            if (bFirstSetData == true)
                            {
                                pRes[iRes].SetDataProvider2(ModPlanOutlook.cCalProvider, true);
                                bFirstSetData = false;
                            }
                            else
                            {
                                pRes[iRes].SetDataProvider(pRes[0].DataProvider, false);
                            }

                            ///=============> Tested
                            if (!pRes[iRes].DataProvider.Open())
                            {
                                if (!pRes[iRes].DataProvider.Create())
                                {
                                    Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur sur connection.", "", MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                                    return;
                                }
                            }
                            //'=== // ** schedules.
                            if (pSchedules == null)
                                pSchedules = pRes[iRes].DataProvider.Schedules;

                            ///==================> Tested
                            if (pSchedules == null)
                            {
                                Axe_interDT.Views.Theme.CustomMessageBox.CustomMessageBox.Show("Erreur sur calendrier.", "", MessageBoxButtons.OK,
                                        MessageBoxIcon.Information);
                                return;
                            }
                            //'=== charge les shedules.
                            pSchedules.AddNewSchedule(tpInterv[X]);
                            pRes[iRes].DataProvider.Save();
                            //'===// ** resources.

                            pRes[iRes].Name = pSchedules[iRes].Name;
                            pRes[iRes].ScheduleIDs.Add(pSchedules[iRes].Id);

                            arResources.Add(pRes[iRes]);
                            Cal.SetMultipleResources(arResources);

                            iRes = iRes + 1;
                        }
                    }

                    Timer1.Enabled = true;
                }
                Cal.Populate();
                Cal.RedrawControl();
                return;
            }
            catch (Exception ex)
            {
                Erreurs.gFr_debug(ex, Name + ";fc_addResource");
                Timer1.Enabled = true;
            }
        }

        /// <summary>
        /// Mondir le 01.07.2021 demande de Rachid pour le nouveau client
        /// </summary>
        /// <param name="sender"></param>
        /// <param name="e"></param>
        private void cmdVisite_Click(object sender, EventArgs e)
        {
            try
            {
                if (!fraVisite.Visible)
                {
                    fraVisite.Visible = true;
                    cmdVisite.Text = "Masquer Visite d'entretien";
                    tableLayoutPanel1.ColumnStyles[0].Width = 260;
                }
                else
                {
                    fraVisite.Visible = false;
                    cmdVisite.Text = "Afficher Visite d'entretien";
                    tableLayoutPanel1.ColumnStyles[0].Width = 0;
                }
            }
            catch (Exception exception)
            {
                Program.SaveException(exception);
            }
        }

        private void btnTrierParDistance_Click(object sender, EventArgs e)
        {
            string sCode;
            double nCode;

            var frm = new frmTriImmeublesParDistance();
            frm.sSQL = fc_loadImmeublesParVisites();
            frm.ShowDialog();

        }

        private string fc_loadImmeublesParVisites()
        {
            try
            {
                string sSQL = "";

                string sTemp = "";

                DateTime dtDeb = DateTime.Now;

                DateTime dtFin = DateTime.Now;

                if (txtInterP2.Text == "")
                {
                    CustomMessageBox.Show("L''intervenant est obligatoire.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return "";
                }

                sTemp = "01" + "/" + txtMoisPlanif.Text;

                if (txtMoisPlanif.Text == "" || sTemp.IsDate())
                {
                    CustomMessageBox.Show("La date au format mm/yyyy est obligatoire.", "Opération annulée", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return "";
                }

                //txtMoisPlanif_Validate False
                if (sTemp.IsDate())
                {
                    dtDeb = Convert.ToDateTime("01" + "/" + txtMoisPlanif.Text);
                    dtFin = fDate.fc_FinDeMois(dtDeb.Month.ToString(), dtDeb.Year.ToString()); //'& "/" & txtMoisPlanif
                }

                sSQL = "SELECT      distinct Intervention.CodeImmeuble,Immeuble.Latitude,Immeuble.Longitude,Immeuble.Adresse"
                       + " FROM            Intervention INNER JOIN"
                       + " Personnel ON Intervention.Intervenant = Personnel.Matricule "
                       + "INNER JOIN Immeuble ON Intervention.CodeImmeuble = Immeuble.CodeImmeuble ";

                sSQL = sSQL + " WHERE Intervention.Intervenant = '" + StdSQLchaine.gFr_DoublerQuote(txtInterP2.Text) + "'"
                       + " and Intervention.DateVisite >= '" + dtDeb + "'"
                       + " and Intervention.DateVisite <= '" + dtFin + "'";

                sSQL = sSQL + " Group by Intervention.CodeImmeuble,Immeuble.Latitude,Immeuble.Longitude,Immeuble.Adresse";

                return sSQL;
            }
            catch (Exception e)
            {
                Program.SaveException(e);
                return "";
            }
        }

        private void Command7_Click(object sender, EventArgs e)
        {
            fc_loadVisite();
        }

        private void fc_loadVisite()
        {
            string sSQL;
            string sAdd;
            string sTemp;
            DateTime dtDeb = DateTime.Now;
            DateTime dtFin = DateTime.Now;

            try
            {
                if (txtInterP2.Text == "")
                {
                    CustomMessageBox.Show("L'intervenant est obligatoire.", "", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    return;
                }

                sTemp = "01" + "/" + txtMoisPlanif.Text;

                if (txtMoisPlanif.Text == "" || !sTemp.IsDate())
                {
                    CustomMessageBox.Show("La date au format mm/yyyy est obligatoire.", "", MessageBoxButtons.OK,
                        MessageBoxIcon.Information);
                    return;
                }

                //txtMoisPlanif_Validate False
                if (sTemp.IsDate())
                {
                    dtDeb = Convert.ToDateTime("01" + "/" + txtMoisPlanif.Text);
                    dtFin = fDate.fc_FinDeMois(dtDeb.Month.ToString(), dtDeb.Year.ToString()); //& "/" & txtMoisPlanif
                }

                sSQL = "SELECT  top 25    Intervention.CodeImmeuble,  Intervention.NoIntervention, Intervention.Intervenant, Personnel.Nom," +
                     " Intervention.DateSaisie, Intervention.DatePrevue, Intervention.DateVisite " +
                     " FROM            Intervention INNER JOIN" +
                     " Personnel ON Intervention.Intervenant = Personnel.Matricule ";

                sSQL = sSQL + " WHERE Intervention.Intervenant = '" + StdSQLchaine.gFr_DoublerQuote(txtInterP2.Text) + "'" +
                    " and Intervention.DateVisite >= '" + dtDeb + "'" +
                    " and Intervention.DateVisite <= '" + dtFin + "'";

                //& " WHERE        (Intervention.Intervenant = '314') AND (Intervention.DateVisite >= '01/03/2021')"

                Cursor = Cursors.WaitCursor;

                rsVisiteModAdo = new ModAdo();
                rsVisite = rsVisiteModAdo.fc_OpenRecordSet(sSQL);

                Cursor = Cursors.Default;

                GridVisite.DataSource = rsVisite;

                //Me.GridVisite.Rows = rsVisite.RecordCount
                //GridVisite.AllowAddNew = False
                //GridVisite.AllowDelete = False
                //GridVisite.AllowUpdate = False
                //GridVisite.Drag
            }
            catch (Exception e)
            {
                Program.SaveException(e);
            }
        }
    }
}