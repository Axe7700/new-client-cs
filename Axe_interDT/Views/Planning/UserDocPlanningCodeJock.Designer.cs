namespace Axe_interDT.Views.Planning
{
    partial class UserDocPlanningCodeJock
    {
        /// <summary> 
        /// Variable nécessaire au concepteur.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary> 
        /// Nettoyage des ressources utilisées.
        /// </summary>
        /// <param name="disposing">true si les ressources managées doivent être supprimées ; sinon, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Code généré par le Concepteur de composants

        /// <summary> 
        /// Méthode requise pour la prise en charge du concepteur - ne modifiez pas 
        /// le contenu de cette méthode avec l'éditeur de code.
        /// </summary>
        private void InitializeComponent()
        {
            this.components = new System.ComponentModel.Container();
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(UserDocPlanningCodeJock));
            Infragistics.Win.Appearance appearance1 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance2 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance3 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance4 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance5 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance6 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance7 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance8 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance9 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance10 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance11 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance12 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance13 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance14 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance15 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance16 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance17 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance18 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance19 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance20 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance21 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance22 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance23 = new Infragistics.Win.Appearance();
            Infragistics.Win.Appearance appearance24 = new Infragistics.Win.Appearance();
            this.tableLayoutPanel1 = new System.Windows.Forms.TableLayoutPanel();
            this.FraConteneur = new System.Windows.Forms.GroupBox();
            this.Cal = new AxXtremeCalendarControl.AxCalendarControl();
            this.tableLayoutPanel2 = new System.Windows.Forms.TableLayoutPanel();
            this.Frame4 = new System.Windows.Forms.GroupBox();
            this.tableLayoutPanel4 = new System.Windows.Forms.TableLayoutPanel();
            this.lbltot = new iTalk.iTalk_TextBox_Small2();
            this.label9 = new System.Windows.Forms.Label();
            this.label3 = new System.Windows.Forms.Label();
            this.CmdLargMoins = new System.Windows.Forms.Button();
            this.cmdLargPlus = new System.Windows.Forms.Button();
            this.CmdLongMoins = new System.Windows.Forms.Button();
            this.cmdLongPlus = new System.Windows.Forms.Button();
            this.cmdColplus = new System.Windows.Forms.Button();
            this.cmdZoomDef = new System.Windows.Forms.Button();
            this.label4 = new System.Windows.Forms.Label();
            this.cmdColMoins = new System.Windows.Forms.Button();
            this.label5 = new System.Windows.Forms.Label();
            this.Label567 = new System.Windows.Forms.Label();
            this.tableLayoutPanel3 = new System.Windows.Forms.TableLayoutPanel();
            this.cmdStatutInclus = new System.Windows.Forms.Button();
            this.cmdintervenant = new System.Windows.Forms.Button();
            this.lbllibIntervenant = new iTalk.iTalk_TextBox_Small2();
            this.txtIntervenant = new iTalk.iTalk_TextBox_Small2();
            this.lblMes = new System.Windows.Forms.Label();
            this.label33 = new System.Windows.Forms.Label();
            this.label1 = new System.Windows.Forms.Label();
            this.label2 = new System.Windows.Forms.Label();
            this.txtCodeEtatsInclus = new iTalk.iTalk_TextBox_Small2();
            this.CmbGroupe = new Infragistics.Win.UltraWinGrid.UltraCombo();
            this.cmdRechercheGroupe = new System.Windows.Forms.Button();
            this.cmdEctituresCopro = new System.Windows.Forms.Button();
            this.DateCal = new AxXtremeCalendarControl.AxDatePicker();
            this.fraVisite = new System.Windows.Forms.TableLayoutPanel();
            this.GridVisite = new Infragistics.Win.UltraWinGrid.UltraGrid();
            this.label7 = new System.Windows.Forms.Label();
            this.txtMoisPlanif = new iTalk.iTalk_TextBox_Small2();
            this.label6 = new System.Windows.Forms.Label();
            this.tableLayoutPanel7 = new System.Windows.Forms.TableLayoutPanel();
            this.Command8 = new System.Windows.Forms.Button();
            this.txtLibInterP2 = new iTalk.iTalk_TextBox_Small2();
            this.txtInterP2 = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel8 = new System.Windows.Forms.TableLayoutPanel();
            this.Command7 = new System.Windows.Forms.Button();
            this.btnTrierParDistance = new System.Windows.Forms.Button();
            this.cmdVisite = new System.Windows.Forms.Button();
            this.flowLayoutPanel1 = new System.Windows.Forms.FlowLayoutPanel();
            this.Command3 = new System.Windows.Forms.Button();
            this.cdmDayView = new System.Windows.Forms.Button();
            this.cmdClean = new System.Windows.Forms.Button();
            this.cmbIntervention = new System.Windows.Forms.Button();
            this.Command6 = new System.Windows.Forms.Button();
            this.Command5 = new System.Windows.Forms.Button();
            this.Timer1 = new System.Windows.Forms.Timer(this.components);
            this.groupBox6 = new System.Windows.Forms.GroupBox();
            this.DTDateRealiseAu = new System.Windows.Forms.DateTimePicker();
            this.DTDateRealiseDe = new System.Windows.Forms.DateTimePicker();
            this.toolTip1 = new System.Windows.Forms.ToolTip(this.components);
            this.tableLayoutPanel5 = new System.Windows.Forms.TableLayoutPanel();
            this.tableLayoutPanel6 = new System.Windows.Forms.TableLayoutPanel();
            this.lblTimer = new iTalk.iTalk_TextBox_Small2();
            this.tableLayoutPanel1.SuspendLayout();
            this.FraConteneur.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.Cal)).BeginInit();
            this.tableLayoutPanel2.SuspendLayout();
            this.Frame4.SuspendLayout();
            this.tableLayoutPanel4.SuspendLayout();
            this.tableLayoutPanel3.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CmbGroupe)).BeginInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCal)).BeginInit();
            this.fraVisite.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridVisite)).BeginInit();
            this.tableLayoutPanel7.SuspendLayout();
            this.tableLayoutPanel8.SuspendLayout();
            this.flowLayoutPanel1.SuspendLayout();
            this.groupBox6.SuspendLayout();
            this.tableLayoutPanel5.SuspendLayout();
            this.tableLayoutPanel6.SuspendLayout();
            this.SuspendLayout();
            // 
            // tableLayoutPanel1
            // 
            this.tableLayoutPanel1.ColumnCount = 2;
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 260F));
            this.tableLayoutPanel1.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Controls.Add(this.FraConteneur, 1, 1);
            this.tableLayoutPanel1.Controls.Add(this.tableLayoutPanel2, 1, 0);
            this.tableLayoutPanel1.Controls.Add(this.fraVisite, 0, 0);
            this.tableLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel1.Location = new System.Drawing.Point(3, 43);
            this.tableLayoutPanel1.Name = "tableLayoutPanel1";
            this.tableLayoutPanel1.RowCount = 2;
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 147F));
            this.tableLayoutPanel1.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel1.Size = new System.Drawing.Size(1844, 911);
            this.tableLayoutPanel1.TabIndex = 0;
            // 
            // FraConteneur
            // 
            this.FraConteneur.BackColor = System.Drawing.Color.Transparent;
            this.FraConteneur.Controls.Add(this.Cal);
            this.FraConteneur.Dock = System.Windows.Forms.DockStyle.Fill;
            this.FraConteneur.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.FraConteneur.Location = new System.Drawing.Point(263, 150);
            this.FraConteneur.Name = "FraConteneur";
            this.FraConteneur.Size = new System.Drawing.Size(1578, 758);
            this.FraConteneur.TabIndex = 506;
            this.FraConteneur.TabStop = false;
            // 
            // Cal
            // 
            this.Cal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Cal.Location = new System.Drawing.Point(3, 18);
            this.Cal.Name = "Cal";
            this.Cal.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("Cal.OcxState")));
            this.Cal.Size = new System.Drawing.Size(1572, 737);
            this.Cal.TabIndex = 0;
            this.Cal.DblClick += new System.EventHandler(this.Cal_DblClick);
            this.Cal.ViewChanged += new System.EventHandler(this.Cal_ViewChanged);
            this.Cal.IsEditOperationDisabled += new AxXtremeCalendarControl._DCalendarControlEvents_IsEditOperationDisabledEventHandler(this.Cal_IsEditOperationDisabled);
            this.Cal.BeforeEditOperation += new AxXtremeCalendarControl._DCalendarControlEvents_BeforeEditOperationEventHandler(this.Cal_BeforeEditOperation);
            this.Cal.DoRetrieveDayEvents += new AxXtremeCalendarControl._DCalendarControlEvents_DoRetrieveDayEventsEventHandler(this.Cal_DoRetrieveDayEvents);
            this.Cal.DoUpdateEvent += new AxXtremeCalendarControl._DCalendarControlEvents_DoUpdateEventEventHandler(this.Cal_DoUpdateEvent);
            this.Cal.DoGetUpcomingEvents += new AxXtremeCalendarControl._DCalendarControlEvents_DoGetUpcomingEventsEventHandler(this.Cal_DoGetUpcomingEvents);
            // 
            // tableLayoutPanel2
            // 
            this.tableLayoutPanel2.ColumnCount = 3;
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 44.77435F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 55.22565F));
            this.tableLayoutPanel2.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 347F));
            this.tableLayoutPanel2.Controls.Add(this.Frame4, 2, 0);
            this.tableLayoutPanel2.Controls.Add(this.Label567, 2, 1);
            this.tableLayoutPanel2.Controls.Add(this.tableLayoutPanel3, 1, 0);
            this.tableLayoutPanel2.Controls.Add(this.DateCal, 0, 0);
            this.tableLayoutPanel2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel2.Location = new System.Drawing.Point(263, 3);
            this.tableLayoutPanel2.Name = "tableLayoutPanel2";
            this.tableLayoutPanel2.RowCount = 2;
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 120F));
            this.tableLayoutPanel2.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel2.Size = new System.Drawing.Size(1578, 141);
            this.tableLayoutPanel2.TabIndex = 0;
            // 
            // Frame4
            // 
            this.Frame4.BackColor = System.Drawing.Color.Transparent;
            this.Frame4.Controls.Add(this.tableLayoutPanel4);
            this.Frame4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Frame4.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.Frame4.Location = new System.Drawing.Point(1233, 3);
            this.Frame4.Name = "Frame4";
            this.Frame4.Size = new System.Drawing.Size(342, 114);
            this.Frame4.TabIndex = 411;
            this.Frame4.TabStop = false;
            this.Frame4.Text = "Zoom";
            this.Frame4.Visible = false;
            // 
            // tableLayoutPanel4
            // 
            this.tableLayoutPanel4.ColumnCount = 8;
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 10F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 140F));
            this.tableLayoutPanel4.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel4.Controls.Add(this.lbltot, 5, 2);
            this.tableLayoutPanel4.Controls.Add(this.label9, 1, 0);
            this.tableLayoutPanel4.Controls.Add(this.label3, 5, 0);
            this.tableLayoutPanel4.Controls.Add(this.CmdLargMoins, 1, 1);
            this.tableLayoutPanel4.Controls.Add(this.cmdLargPlus, 3, 1);
            this.tableLayoutPanel4.Controls.Add(this.CmdLongMoins, 5, 1);
            this.tableLayoutPanel4.Controls.Add(this.cmdLongPlus, 7, 1);
            this.tableLayoutPanel4.Controls.Add(this.cmdColplus, 3, 2);
            this.tableLayoutPanel4.Controls.Add(this.cmdZoomDef, 0, 0);
            this.tableLayoutPanel4.Controls.Add(this.label4, 0, 2);
            this.tableLayoutPanel4.Controls.Add(this.cmdColMoins, 1, 2);
            this.tableLayoutPanel4.Controls.Add(this.label5, 0, 1);
            this.tableLayoutPanel4.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel4.Location = new System.Drawing.Point(3, 18);
            this.tableLayoutPanel4.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel4.Name = "tableLayoutPanel4";
            this.tableLayoutPanel4.RowCount = 3;
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel4.Size = new System.Drawing.Size(336, 93);
            this.tableLayoutPanel4.TabIndex = 2;
            // 
            // lbltot
            // 
            this.lbltot.AccAcceptNumbersOnly = false;
            this.lbltot.AccAllowComma = false;
            this.lbltot.AccBackgroundColor = System.Drawing.Color.White;
            this.lbltot.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lbltot.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lbltot.AccHidenValue = "";
            this.lbltot.AccNotAllowedChars = null;
            this.lbltot.AccReadOnly = false;
            this.lbltot.AccReadOnlyAllowDelete = false;
            this.lbltot.AccRequired = false;
            this.lbltot.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel4.SetColumnSpan(this.lbltot, 3);
            this.lbltot.CustomBackColor = System.Drawing.Color.White;
            this.lbltot.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbltot.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lbltot.ForeColor = System.Drawing.Color.Black;
            this.lbltot.Location = new System.Drawing.Point(157, 62);
            this.lbltot.Margin = new System.Windows.Forms.Padding(2);
            this.lbltot.MaxLength = 32767;
            this.lbltot.Multiline = false;
            this.lbltot.Name = "lbltot";
            this.lbltot.ReadOnly = false;
            this.lbltot.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lbltot.Size = new System.Drawing.Size(177, 27);
            this.lbltot.TabIndex = 503;
            this.lbltot.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lbltot.UseSystemPasswordChar = false;
            this.lbltot.Visible = false;
            // 
            // label9
            // 
            this.label9.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label9, 3);
            this.label9.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label9.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label9.Location = new System.Drawing.Point(78, 0);
            this.label9.Name = "label9";
            this.label9.Size = new System.Drawing.Size(64, 30);
            this.label9.TabIndex = 386;
            this.label9.Text = "Largeur";
            // 
            // label3
            // 
            this.label3.AutoSize = true;
            this.tableLayoutPanel4.SetColumnSpan(this.label3, 3);
            this.label3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label3.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label3.Location = new System.Drawing.Point(158, 0);
            this.label3.Name = "label3";
            this.label3.Size = new System.Drawing.Size(175, 30);
            this.label3.TabIndex = 389;
            this.label3.Text = "Hauteur";
            // 
            // CmdLargMoins
            // 
            this.CmdLargMoins.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdLargMoins.FlatAppearance.BorderSize = 0;
            this.CmdLargMoins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdLargMoins.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.CmdLargMoins.ForeColor = System.Drawing.Color.White;
            this.CmdLargMoins.Location = new System.Drawing.Point(78, 33);
            this.CmdLargMoins.Name = "CmdLargMoins";
            this.CmdLargMoins.Size = new System.Drawing.Size(24, 24);
            this.CmdLargMoins.TabIndex = 510;
            this.CmdLargMoins.Text = "-";
            this.CmdLargMoins.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.CmdLargMoins.UseVisualStyleBackColor = false;
            this.CmdLargMoins.Click += new System.EventHandler(this.CmdLargMoins_Click);
            // 
            // cmdLargPlus
            // 
            this.cmdLargPlus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdLargPlus.FlatAppearance.BorderSize = 0;
            this.cmdLargPlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdLargPlus.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdLargPlus.ForeColor = System.Drawing.Color.White;
            this.cmdLargPlus.Location = new System.Drawing.Point(118, 33);
            this.cmdLargPlus.Name = "cmdLargPlus";
            this.cmdLargPlus.Size = new System.Drawing.Size(24, 24);
            this.cmdLargPlus.TabIndex = 511;
            this.cmdLargPlus.Text = "+";
            this.cmdLargPlus.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdLargPlus.UseVisualStyleBackColor = false;
            this.cmdLargPlus.Click += new System.EventHandler(this.cmdLargPlus_Click);
            // 
            // CmdLongMoins
            // 
            this.CmdLongMoins.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.CmdLongMoins.FlatAppearance.BorderSize = 0;
            this.CmdLongMoins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.CmdLongMoins.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.CmdLongMoins.ForeColor = System.Drawing.Color.White;
            this.CmdLongMoins.Location = new System.Drawing.Point(158, 33);
            this.CmdLongMoins.Name = "CmdLongMoins";
            this.CmdLongMoins.Size = new System.Drawing.Size(24, 24);
            this.CmdLongMoins.TabIndex = 512;
            this.CmdLongMoins.Text = "-";
            this.CmdLongMoins.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.CmdLongMoins.UseVisualStyleBackColor = false;
            this.CmdLongMoins.Click += new System.EventHandler(this.CmdLongMoins_Click);
            // 
            // cmdLongPlus
            // 
            this.cmdLongPlus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdLongPlus.FlatAppearance.BorderSize = 0;
            this.cmdLongPlus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdLongPlus.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdLongPlus.ForeColor = System.Drawing.Color.White;
            this.cmdLongPlus.Location = new System.Drawing.Point(198, 33);
            this.cmdLongPlus.Name = "cmdLongPlus";
            this.cmdLongPlus.Size = new System.Drawing.Size(24, 24);
            this.cmdLongPlus.TabIndex = 513;
            this.cmdLongPlus.Text = "+";
            this.cmdLongPlus.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdLongPlus.UseVisualStyleBackColor = false;
            this.cmdLongPlus.Click += new System.EventHandler(this.cmdLongPlus_Click);
            // 
            // cmdColplus
            // 
            this.cmdColplus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdColplus.FlatAppearance.BorderSize = 0;
            this.cmdColplus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdColplus.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdColplus.ForeColor = System.Drawing.Color.White;
            this.cmdColplus.Location = new System.Drawing.Point(118, 63);
            this.cmdColplus.Name = "cmdColplus";
            this.cmdColplus.Size = new System.Drawing.Size(24, 24);
            this.cmdColplus.TabIndex = 514;
            this.cmdColplus.Text = "+";
            this.cmdColplus.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdColplus.UseVisualStyleBackColor = false;
            this.cmdColplus.Click += new System.EventHandler(this.cmdColplus_Click);
            // 
            // cmdZoomDef
            // 
            this.cmdZoomDef.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdZoomDef.FlatAppearance.BorderSize = 0;
            this.cmdZoomDef.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdZoomDef.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdZoomDef.ForeColor = System.Drawing.Color.White;
            this.cmdZoomDef.Location = new System.Drawing.Point(2, 2);
            this.cmdZoomDef.Margin = new System.Windows.Forms.Padding(2);
            this.cmdZoomDef.Name = "cmdZoomDef";
            this.cmdZoomDef.Size = new System.Drawing.Size(66, 25);
            this.cmdZoomDef.TabIndex = 516;
            this.cmdZoomDef.Text = "défaut";
            this.cmdZoomDef.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdZoomDef.UseVisualStyleBackColor = false;
            this.cmdZoomDef.Click += new System.EventHandler(this.cmdZoomDef_Click);
            // 
            // label4
            // 
            this.label4.AutoSize = true;
            this.label4.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label4.Location = new System.Drawing.Point(3, 60);
            this.label4.Name = "label4";
            this.label4.Size = new System.Drawing.Size(68, 19);
            this.label4.TabIndex = 390;
            this.label4.Text = "Colonne";
            // 
            // cmdColMoins
            // 
            this.cmdColMoins.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdColMoins.FlatAppearance.BorderSize = 0;
            this.cmdColMoins.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdColMoins.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdColMoins.ForeColor = System.Drawing.Color.White;
            this.cmdColMoins.Location = new System.Drawing.Point(78, 63);
            this.cmdColMoins.Name = "cmdColMoins";
            this.cmdColMoins.Size = new System.Drawing.Size(24, 24);
            this.cmdColMoins.TabIndex = 515;
            this.cmdColMoins.Text = "-";
            this.cmdColMoins.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdColMoins.UseVisualStyleBackColor = false;
            this.cmdColMoins.Click += new System.EventHandler(this.cmdColMoins_Click);
            // 
            // label5
            // 
            this.label5.AutoSize = true;
            this.label5.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label5.Location = new System.Drawing.Point(3, 30);
            this.label5.Name = "label5";
            this.label5.Size = new System.Drawing.Size(69, 19);
            this.label5.TabIndex = 388;
            this.label5.Text = "Planning";
            // 
            // Label567
            // 
            this.Label567.AutoSize = true;
            this.Label567.Font = new System.Drawing.Font("Ubuntu", 11F);
            this.Label567.Location = new System.Drawing.Point(1233, 120);
            this.Label567.Name = "Label567";
            this.Label567.Size = new System.Drawing.Size(0, 19);
            this.Label567.TabIndex = 503;
            this.Label567.Visible = false;
            // 
            // tableLayoutPanel3
            // 
            this.tableLayoutPanel3.ColumnCount = 5;
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle());
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50.0025F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 49.9975F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 32F));
            this.tableLayoutPanel3.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 143F));
            this.tableLayoutPanel3.Controls.Add(this.cmdStatutInclus, 3, 2);
            this.tableLayoutPanel3.Controls.Add(this.cmdintervenant, 3, 0);
            this.tableLayoutPanel3.Controls.Add(this.lbllibIntervenant, 2, 0);
            this.tableLayoutPanel3.Controls.Add(this.txtIntervenant, 1, 0);
            this.tableLayoutPanel3.Controls.Add(this.lblMes, 0, 3);
            this.tableLayoutPanel3.Controls.Add(this.label33, 0, 0);
            this.tableLayoutPanel3.Controls.Add(this.label1, 0, 1);
            this.tableLayoutPanel3.Controls.Add(this.label2, 0, 2);
            this.tableLayoutPanel3.Controls.Add(this.txtCodeEtatsInclus, 1, 2);
            this.tableLayoutPanel3.Controls.Add(this.CmbGroupe, 1, 1);
            this.tableLayoutPanel3.Controls.Add(this.cmdRechercheGroupe, 3, 1);
            this.tableLayoutPanel3.Controls.Add(this.cmdEctituresCopro, 4, 1);
            this.tableLayoutPanel3.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel3.Location = new System.Drawing.Point(554, 3);
            this.tableLayoutPanel3.Name = "tableLayoutPanel3";
            this.tableLayoutPanel3.RowCount = 4;
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.tableLayoutPanel3.Size = new System.Drawing.Size(673, 114);
            this.tableLayoutPanel3.TabIndex = 1;
            // 
            // cmdStatutInclus
            // 
            this.cmdStatutInclus.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdStatutInclus.FlatAppearance.BorderSize = 0;
            this.cmdStatutInclus.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdStatutInclus.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdStatutInclus.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdStatutInclus.Location = new System.Drawing.Point(500, 63);
            this.cmdStatutInclus.Name = "cmdStatutInclus";
            this.cmdStatutInclus.Size = new System.Drawing.Size(24, 20);
            this.cmdStatutInclus.TabIndex = 508;
            this.cmdStatutInclus.Tag = "";
            this.cmdStatutInclus.UseVisualStyleBackColor = false;
            this.cmdStatutInclus.Click += new System.EventHandler(this.cmdStatutInclus_Click);
            // 
            // cmdintervenant
            // 
            this.cmdintervenant.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdintervenant.FlatAppearance.BorderSize = 0;
            this.cmdintervenant.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdintervenant.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdintervenant.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdintervenant.Location = new System.Drawing.Point(500, 3);
            this.cmdintervenant.Name = "cmdintervenant";
            this.cmdintervenant.Size = new System.Drawing.Size(24, 20);
            this.cmdintervenant.TabIndex = 507;
            this.cmdintervenant.Tag = "";
            this.toolTip1.SetToolTip(this.cmdintervenant, "Recherche d\'un intervenant");
            this.cmdintervenant.UseVisualStyleBackColor = false;
            this.cmdintervenant.Click += new System.EventHandler(this.cmdintervenant_Click);
            // 
            // lbllibIntervenant
            // 
            this.lbllibIntervenant.AccAcceptNumbersOnly = false;
            this.lbllibIntervenant.AccAllowComma = false;
            this.lbllibIntervenant.AccBackgroundColor = System.Drawing.Color.White;
            this.lbllibIntervenant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lbllibIntervenant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lbllibIntervenant.AccHidenValue = "";
            this.lbllibIntervenant.AccNotAllowedChars = null;
            this.lbllibIntervenant.AccReadOnly = false;
            this.lbllibIntervenant.AccReadOnlyAllowDelete = false;
            this.lbllibIntervenant.AccRequired = false;
            this.lbllibIntervenant.BackColor = System.Drawing.Color.White;
            this.lbllibIntervenant.CustomBackColor = System.Drawing.Color.White;
            this.lbllibIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lbllibIntervenant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lbllibIntervenant.ForeColor = System.Drawing.Color.Black;
            this.lbllibIntervenant.Location = new System.Drawing.Point(301, 2);
            this.lbllibIntervenant.Margin = new System.Windows.Forms.Padding(2);
            this.lbllibIntervenant.MaxLength = 32767;
            this.lbllibIntervenant.Multiline = false;
            this.lbllibIntervenant.Name = "lbllibIntervenant";
            this.lbllibIntervenant.ReadOnly = false;
            this.lbllibIntervenant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lbllibIntervenant.Size = new System.Drawing.Size(194, 27);
            this.lbllibIntervenant.TabIndex = 503;
            this.lbllibIntervenant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lbllibIntervenant.UseSystemPasswordChar = false;
            // 
            // txtIntervenant
            // 
            this.txtIntervenant.AccAcceptNumbersOnly = false;
            this.txtIntervenant.AccAllowComma = false;
            this.txtIntervenant.AccBackgroundColor = System.Drawing.Color.White;
            this.txtIntervenant.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtIntervenant.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtIntervenant.AccHidenValue = "";
            this.txtIntervenant.AccNotAllowedChars = null;
            this.txtIntervenant.AccReadOnly = false;
            this.txtIntervenant.AccReadOnlyAllowDelete = false;
            this.txtIntervenant.AccRequired = false;
            this.txtIntervenant.BackColor = System.Drawing.Color.White;
            this.txtIntervenant.CustomBackColor = System.Drawing.Color.White;
            this.txtIntervenant.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtIntervenant.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtIntervenant.ForeColor = System.Drawing.Color.Black;
            this.txtIntervenant.Location = new System.Drawing.Point(103, 2);
            this.txtIntervenant.Margin = new System.Windows.Forms.Padding(2);
            this.txtIntervenant.MaxLength = 32767;
            this.txtIntervenant.Multiline = false;
            this.txtIntervenant.Name = "txtIntervenant";
            this.txtIntervenant.ReadOnly = false;
            this.txtIntervenant.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtIntervenant.Size = new System.Drawing.Size(194, 27);
            this.txtIntervenant.TabIndex = 502;
            this.txtIntervenant.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtIntervenant.UseSystemPasswordChar = false;
            this.txtIntervenant.TextChanged += new System.EventHandler(this.txtIntervenant_TextChanged);
            this.txtIntervenant.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtIntervenant_KeyPress);
            // 
            // lblMes
            // 
            this.lblMes.AutoSize = true;
            this.tableLayoutPanel3.SetColumnSpan(this.lblMes, 3);
            this.lblMes.Dock = System.Windows.Forms.DockStyle.Fill;
            this.lblMes.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.lblMes.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(192)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
            this.lblMes.Location = new System.Drawing.Point(3, 90);
            this.lblMes.Name = "lblMes";
            this.lblMes.Size = new System.Drawing.Size(491, 30);
            this.lblMes.TabIndex = 388;
            this.lblMes.Text = "Statut inclus";
            // 
            // label33
            // 
            this.label33.AutoSize = true;
            this.label33.Font = new System.Drawing.Font("Ubuntu", 11.25F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.label33.Location = new System.Drawing.Point(3, 0);
            this.label33.Name = "label33";
            this.label33.Size = new System.Drawing.Size(90, 19);
            this.label33.TabIndex = 384;
            this.label33.Text = "Intervenant";
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label1.Location = new System.Drawing.Point(3, 30);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(61, 19);
            this.label1.TabIndex = 385;
            this.label1.Text = "Groupe";
            // 
            // label2
            // 
            this.label2.AutoSize = true;
            this.label2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.label2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.label2.Location = new System.Drawing.Point(3, 60);
            this.label2.Name = "label2";
            this.label2.Size = new System.Drawing.Size(95, 30);
            this.label2.TabIndex = 386;
            this.label2.Text = "Statut inclus";
            // 
            // txtCodeEtatsInclus
            // 
            this.txtCodeEtatsInclus.AccAcceptNumbersOnly = false;
            this.txtCodeEtatsInclus.AccAllowComma = false;
            this.txtCodeEtatsInclus.AccBackgroundColor = System.Drawing.Color.White;
            this.txtCodeEtatsInclus.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtCodeEtatsInclus.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtCodeEtatsInclus.AccHidenValue = "";
            this.txtCodeEtatsInclus.AccNotAllowedChars = null;
            this.txtCodeEtatsInclus.AccReadOnly = false;
            this.txtCodeEtatsInclus.AccReadOnlyAllowDelete = false;
            this.txtCodeEtatsInclus.AccRequired = false;
            this.txtCodeEtatsInclus.BackColor = System.Drawing.Color.White;
            this.tableLayoutPanel3.SetColumnSpan(this.txtCodeEtatsInclus, 2);
            this.txtCodeEtatsInclus.CustomBackColor = System.Drawing.Color.White;
            this.txtCodeEtatsInclus.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtCodeEtatsInclus.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtCodeEtatsInclus.ForeColor = System.Drawing.Color.Black;
            this.txtCodeEtatsInclus.Location = new System.Drawing.Point(103, 62);
            this.txtCodeEtatsInclus.Margin = new System.Windows.Forms.Padding(2);
            this.txtCodeEtatsInclus.MaxLength = 32767;
            this.txtCodeEtatsInclus.Multiline = false;
            this.txtCodeEtatsInclus.Name = "txtCodeEtatsInclus";
            this.txtCodeEtatsInclus.ReadOnly = false;
            this.txtCodeEtatsInclus.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtCodeEtatsInclus.Size = new System.Drawing.Size(392, 27);
            this.txtCodeEtatsInclus.TabIndex = 504;
            this.txtCodeEtatsInclus.TextAlignment = Infragistics.Win.HAlign.Left;
            this.toolTip1.SetToolTip(this.txtCodeEtatsInclus, "Ajouter les statuts avec le separateur ;");
            this.txtCodeEtatsInclus.UseSystemPasswordChar = false;
            this.txtCodeEtatsInclus.TextChanged += new System.EventHandler(this.txtCodeEtatsInclus_TextChanged);
            this.txtCodeEtatsInclus.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.txtCodeEtatsInclus_KeyPress);
            // 
            // CmbGroupe
            // 
            this.tableLayoutPanel3.SetColumnSpan(this.CmbGroupe, 2);
            appearance1.BackColor = System.Drawing.SystemColors.Window;
            appearance1.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.CmbGroupe.DisplayLayout.Appearance = appearance1;
            this.CmbGroupe.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.CmbGroupe.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance2.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance2.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance2.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance2.BorderColor = System.Drawing.SystemColors.Window;
            this.CmbGroupe.DisplayLayout.GroupByBox.Appearance = appearance2;
            appearance3.ForeColor = System.Drawing.SystemColors.GrayText;
            this.CmbGroupe.DisplayLayout.GroupByBox.BandLabelAppearance = appearance3;
            this.CmbGroupe.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance4.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance4.BackColor2 = System.Drawing.SystemColors.Control;
            appearance4.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance4.ForeColor = System.Drawing.SystemColors.GrayText;
            this.CmbGroupe.DisplayLayout.GroupByBox.PromptAppearance = appearance4;
            this.CmbGroupe.DisplayLayout.MaxColScrollRegions = 1;
            this.CmbGroupe.DisplayLayout.MaxRowScrollRegions = 1;
            appearance5.BackColor = System.Drawing.SystemColors.Window;
            appearance5.ForeColor = System.Drawing.SystemColors.ControlText;
            this.CmbGroupe.DisplayLayout.Override.ActiveCellAppearance = appearance5;
            appearance6.BackColor = System.Drawing.SystemColors.Highlight;
            appearance6.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.CmbGroupe.DisplayLayout.Override.ActiveRowAppearance = appearance6;
            this.CmbGroupe.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.CmbGroupe.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance7.BackColor = System.Drawing.SystemColors.Window;
            this.CmbGroupe.DisplayLayout.Override.CardAreaAppearance = appearance7;
            appearance8.BorderColor = System.Drawing.Color.Silver;
            appearance8.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.CmbGroupe.DisplayLayout.Override.CellAppearance = appearance8;
            this.CmbGroupe.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.CmbGroupe.DisplayLayout.Override.CellPadding = 0;
            appearance9.BackColor = System.Drawing.SystemColors.Control;
            appearance9.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance9.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance9.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance9.BorderColor = System.Drawing.SystemColors.Window;
            this.CmbGroupe.DisplayLayout.Override.GroupByRowAppearance = appearance9;
            appearance10.TextHAlignAsString = "Left";
            this.CmbGroupe.DisplayLayout.Override.HeaderAppearance = appearance10;
            this.CmbGroupe.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.CmbGroupe.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance11.BackColor = System.Drawing.SystemColors.Window;
            appearance11.BorderColor = System.Drawing.Color.Silver;
            this.CmbGroupe.DisplayLayout.Override.RowAppearance = appearance11;
            this.CmbGroupe.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.False;
            appearance12.BackColor = System.Drawing.SystemColors.ControlLight;
            this.CmbGroupe.DisplayLayout.Override.TemplateAddRowAppearance = appearance12;
            this.CmbGroupe.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.CmbGroupe.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.CmbGroupe.DisplayLayout.ViewStyleBand = Infragistics.Win.UltraWinGrid.ViewStyleBand.OutlookGroupBy;
            this.CmbGroupe.Dock = System.Windows.Forms.DockStyle.Fill;
            this.CmbGroupe.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.CmbGroupe.Location = new System.Drawing.Point(104, 33);
            this.CmbGroupe.Name = "CmbGroupe";
            this.CmbGroupe.Size = new System.Drawing.Size(390, 27);
            this.CmbGroupe.TabIndex = 505;
            this.CmbGroupe.AfterCloseUp += new System.EventHandler(this.CmbGroupe_AfterCloseUp);
            this.CmbGroupe.BeforeDropDown += new System.ComponentModel.CancelEventHandler(this.CmbGroupe_BeforeDropDown);
            this.CmbGroupe.TextChanged += new System.EventHandler(this.CmbGroupe_TextChanged);
            this.CmbGroupe.KeyPress += new System.Windows.Forms.KeyPressEventHandler(this.CmbGroupe_KeyPress);
            // 
            // cmdRechercheGroupe
            // 
            this.cmdRechercheGroupe.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdRechercheGroupe.FlatAppearance.BorderSize = 0;
            this.cmdRechercheGroupe.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdRechercheGroupe.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdRechercheGroupe.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.cmdRechercheGroupe.Location = new System.Drawing.Point(500, 33);
            this.cmdRechercheGroupe.Name = "cmdRechercheGroupe";
            this.cmdRechercheGroupe.Size = new System.Drawing.Size(24, 20);
            this.cmdRechercheGroupe.TabIndex = 506;
            this.cmdRechercheGroupe.Tag = "";
            this.cmdRechercheGroupe.UseVisualStyleBackColor = false;
            this.cmdRechercheGroupe.Click += new System.EventHandler(this.cmdRechercheGroupe_Click);
            // 
            // cmdEctituresCopro
            // 
            this.cmdEctituresCopro.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdEctituresCopro.FlatAppearance.BorderSize = 0;
            this.cmdEctituresCopro.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdEctituresCopro.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.cmdEctituresCopro.ForeColor = System.Drawing.Color.White;
            this.cmdEctituresCopro.Location = new System.Drawing.Point(532, 33);
            this.cmdEctituresCopro.Name = "cmdEctituresCopro";
            this.cmdEctituresCopro.Size = new System.Drawing.Size(24, 18);
            this.cmdEctituresCopro.TabIndex = 509;
            this.cmdEctituresCopro.Text = "+";
            this.cmdEctituresCopro.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdEctituresCopro.UseVisualStyleBackColor = false;
            this.cmdEctituresCopro.Click += new System.EventHandler(this.cmdEctituresCopro_Click);
            // 
            // DateCal
            // 
            this.DateCal.Dock = System.Windows.Forms.DockStyle.Fill;
            this.DateCal.Location = new System.Drawing.Point(3, 3);
            this.DateCal.Name = "DateCal";
            this.DateCal.OcxState = ((System.Windows.Forms.AxHost.State)(resources.GetObject("DateCal.OcxState")));
            this.tableLayoutPanel2.SetRowSpan(this.DateCal, 2);
            this.DateCal.Size = new System.Drawing.Size(545, 135);
            this.DateCal.TabIndex = 412;
            this.DateCal.SelectionChanged += new System.EventHandler(this.DateCal_SelectionChanged);
            this.DateCal.MonthChanged += new System.EventHandler(this.DateCal_MonthChanged);
            // 
            // fraVisite
            // 
            this.fraVisite.ColumnCount = 1;
            this.fraVisite.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fraVisite.Controls.Add(this.GridVisite, 0, 5);
            this.fraVisite.Controls.Add(this.label7, 0, 2);
            this.fraVisite.Controls.Add(this.txtMoisPlanif, 0, 1);
            this.fraVisite.Controls.Add(this.label6, 0, 0);
            this.fraVisite.Controls.Add(this.tableLayoutPanel7, 0, 3);
            this.fraVisite.Controls.Add(this.tableLayoutPanel8, 0, 4);
            this.fraVisite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.fraVisite.Location = new System.Drawing.Point(3, 3);
            this.fraVisite.Name = "fraVisite";
            this.fraVisite.RowCount = 6;
            this.tableLayoutPanel1.SetRowSpan(this.fraVisite, 2);
            this.fraVisite.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.fraVisite.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.fraVisite.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.fraVisite.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 30F));
            this.fraVisite.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 36F));
            this.fraVisite.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.fraVisite.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.fraVisite.Size = new System.Drawing.Size(254, 905);
            this.fraVisite.TabIndex = 507;
            // 
            // GridVisite
            // 
            appearance13.BackColor = System.Drawing.SystemColors.Window;
            appearance13.BorderColor = System.Drawing.SystemColors.InactiveCaption;
            this.GridVisite.DisplayLayout.Appearance = appearance13;
            this.GridVisite.DisplayLayout.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            this.GridVisite.DisplayLayout.CaptionVisible = Infragistics.Win.DefaultableBoolean.False;
            appearance14.BackColor = System.Drawing.SystemColors.ActiveBorder;
            appearance14.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance14.BackGradientStyle = Infragistics.Win.GradientStyle.Vertical;
            appearance14.BorderColor = System.Drawing.SystemColors.Window;
            this.GridVisite.DisplayLayout.GroupByBox.Appearance = appearance14;
            appearance15.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridVisite.DisplayLayout.GroupByBox.BandLabelAppearance = appearance15;
            this.GridVisite.DisplayLayout.GroupByBox.BorderStyle = Infragistics.Win.UIElementBorderStyle.Solid;
            appearance16.BackColor = System.Drawing.SystemColors.ControlLightLight;
            appearance16.BackColor2 = System.Drawing.SystemColors.Control;
            appearance16.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance16.ForeColor = System.Drawing.SystemColors.GrayText;
            this.GridVisite.DisplayLayout.GroupByBox.PromptAppearance = appearance16;
            this.GridVisite.DisplayLayout.MaxColScrollRegions = 1;
            this.GridVisite.DisplayLayout.MaxRowScrollRegions = 1;
            appearance17.BackColor = System.Drawing.SystemColors.Window;
            appearance17.ForeColor = System.Drawing.SystemColors.ControlText;
            this.GridVisite.DisplayLayout.Override.ActiveCellAppearance = appearance17;
            appearance18.BackColor = System.Drawing.SystemColors.Highlight;
            appearance18.ForeColor = System.Drawing.SystemColors.HighlightText;
            this.GridVisite.DisplayLayout.Override.ActiveRowAppearance = appearance18;
            this.GridVisite.DisplayLayout.Override.BorderStyleCell = Infragistics.Win.UIElementBorderStyle.Dotted;
            this.GridVisite.DisplayLayout.Override.BorderStyleRow = Infragistics.Win.UIElementBorderStyle.Dotted;
            appearance19.BackColor = System.Drawing.SystemColors.Window;
            this.GridVisite.DisplayLayout.Override.CardAreaAppearance = appearance19;
            appearance20.BorderColor = System.Drawing.Color.Silver;
            appearance20.TextTrimming = Infragistics.Win.TextTrimming.EllipsisCharacter;
            this.GridVisite.DisplayLayout.Override.CellAppearance = appearance20;
            this.GridVisite.DisplayLayout.Override.CellClickAction = Infragistics.Win.UltraWinGrid.CellClickAction.EditAndSelectText;
            this.GridVisite.DisplayLayout.Override.CellPadding = 0;
            appearance21.BackColor = System.Drawing.SystemColors.Control;
            appearance21.BackColor2 = System.Drawing.SystemColors.ControlDark;
            appearance21.BackGradientAlignment = Infragistics.Win.GradientAlignment.Element;
            appearance21.BackGradientStyle = Infragistics.Win.GradientStyle.Horizontal;
            appearance21.BorderColor = System.Drawing.SystemColors.Window;
            this.GridVisite.DisplayLayout.Override.GroupByRowAppearance = appearance21;
            appearance22.TextHAlignAsString = "Left";
            this.GridVisite.DisplayLayout.Override.HeaderAppearance = appearance22;
            this.GridVisite.DisplayLayout.Override.HeaderClickAction = Infragistics.Win.UltraWinGrid.HeaderClickAction.SortMulti;
            this.GridVisite.DisplayLayout.Override.HeaderStyle = Infragistics.Win.HeaderStyle.WindowsXPCommand;
            appearance23.BackColor = System.Drawing.SystemColors.Window;
            appearance23.BorderColor = System.Drawing.Color.Silver;
            this.GridVisite.DisplayLayout.Override.RowAppearance = appearance23;
            this.GridVisite.DisplayLayout.Override.RowSelectors = Infragistics.Win.DefaultableBoolean.True;
            this.GridVisite.DisplayLayout.Override.SelectTypeRow = Infragistics.Win.UltraWinGrid.SelectType.Single;
            appearance24.BackColor = System.Drawing.SystemColors.ControlLight;
            this.GridVisite.DisplayLayout.Override.TemplateAddRowAppearance = appearance24;
            this.GridVisite.DisplayLayout.ScrollBounds = Infragistics.Win.UltraWinGrid.ScrollBounds.ScrollToFill;
            this.GridVisite.DisplayLayout.ScrollStyle = Infragistics.Win.UltraWinGrid.ScrollStyle.Immediate;
            this.GridVisite.Dock = System.Windows.Forms.DockStyle.Fill;
            this.GridVisite.Font = new System.Drawing.Font("Microsoft Sans Serif", 8.25F);
            this.GridVisite.Location = new System.Drawing.Point(3, 159);
            this.GridVisite.Name = "GridVisite";
            this.GridVisite.Size = new System.Drawing.Size(248, 743);
            this.GridVisite.TabIndex = 507;
            this.GridVisite.Text = "ultraGrid1";
            // 
            // label7
            // 
            this.label7.AutoSize = true;
            this.label7.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label7.Location = new System.Drawing.Point(3, 66);
            this.label7.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label7.Name = "label7";
            this.label7.Size = new System.Drawing.Size(90, 20);
            this.label7.TabIndex = 504;
            this.label7.Text = "Intervenant";
            // 
            // txtMoisPlanif
            // 
            this.txtMoisPlanif.AccAcceptNumbersOnly = false;
            this.txtMoisPlanif.AccAllowComma = false;
            this.txtMoisPlanif.AccBackgroundColor = System.Drawing.Color.White;
            this.txtMoisPlanif.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtMoisPlanif.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtMoisPlanif.AccHidenValue = "";
            this.txtMoisPlanif.AccNotAllowedChars = null;
            this.txtMoisPlanif.AccReadOnly = false;
            this.txtMoisPlanif.AccReadOnlyAllowDelete = false;
            this.txtMoisPlanif.AccRequired = false;
            this.txtMoisPlanif.BackColor = System.Drawing.Color.White;
            this.txtMoisPlanif.CustomBackColor = System.Drawing.Color.White;
            this.txtMoisPlanif.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtMoisPlanif.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtMoisPlanif.ForeColor = System.Drawing.Color.Black;
            this.txtMoisPlanif.Location = new System.Drawing.Point(2, 32);
            this.txtMoisPlanif.Margin = new System.Windows.Forms.Padding(2);
            this.txtMoisPlanif.MaxLength = 32767;
            this.txtMoisPlanif.Multiline = false;
            this.txtMoisPlanif.Name = "txtMoisPlanif";
            this.txtMoisPlanif.ReadOnly = false;
            this.txtMoisPlanif.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtMoisPlanif.Size = new System.Drawing.Size(250, 27);
            this.txtMoisPlanif.TabIndex = 503;
            this.txtMoisPlanif.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtMoisPlanif.UseSystemPasswordChar = false;
            // 
            // label6
            // 
            this.label6.AutoSize = true;
            this.label6.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F);
            this.label6.Location = new System.Drawing.Point(3, 6);
            this.label6.Margin = new System.Windows.Forms.Padding(3, 6, 3, 0);
            this.label6.Name = "label6";
            this.label6.Size = new System.Drawing.Size(186, 20);
            this.label6.TabIndex = 0;
            this.label6.Text = "Mois (au format mm/yyyy)";
            // 
            // tableLayoutPanel7
            // 
            this.tableLayoutPanel7.ColumnCount = 3;
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 30F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 70F));
            this.tableLayoutPanel7.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 44F));
            this.tableLayoutPanel7.Controls.Add(this.Command8, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtLibInterP2, 0, 0);
            this.tableLayoutPanel7.Controls.Add(this.txtInterP2, 0, 0);
            this.tableLayoutPanel7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel7.Location = new System.Drawing.Point(0, 90);
            this.tableLayoutPanel7.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel7.Name = "tableLayoutPanel7";
            this.tableLayoutPanel7.RowCount = 1;
            this.tableLayoutPanel7.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel7.Size = new System.Drawing.Size(254, 30);
            this.tableLayoutPanel7.TabIndex = 505;
            // 
            // Command8
            // 
            this.Command8.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command8.FlatAppearance.BorderSize = 0;
            this.Command8.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command8.Font = new System.Drawing.Font("Segoe UI", 12F);
            this.Command8.Image = global::Axe_interDT.Properties.Resources.Search_14x14;
            this.Command8.Location = new System.Drawing.Point(213, 3);
            this.Command8.Name = "Command8";
            this.Command8.Size = new System.Drawing.Size(30, 24);
            this.Command8.TabIndex = 507;
            this.Command8.Tag = "";
            this.Command8.UseVisualStyleBackColor = false;
            // 
            // txtLibInterP2
            // 
            this.txtLibInterP2.AccAcceptNumbersOnly = false;
            this.txtLibInterP2.AccAllowComma = false;
            this.txtLibInterP2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtLibInterP2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtLibInterP2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtLibInterP2.AccHidenValue = "";
            this.txtLibInterP2.AccNotAllowedChars = null;
            this.txtLibInterP2.AccReadOnly = false;
            this.txtLibInterP2.AccReadOnlyAllowDelete = false;
            this.txtLibInterP2.AccRequired = false;
            this.txtLibInterP2.BackColor = System.Drawing.Color.White;
            this.txtLibInterP2.CustomBackColor = System.Drawing.Color.White;
            this.txtLibInterP2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtLibInterP2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtLibInterP2.ForeColor = System.Drawing.Color.Black;
            this.txtLibInterP2.Location = new System.Drawing.Point(65, 2);
            this.txtLibInterP2.Margin = new System.Windows.Forms.Padding(2);
            this.txtLibInterP2.MaxLength = 32767;
            this.txtLibInterP2.Multiline = false;
            this.txtLibInterP2.Name = "txtLibInterP2";
            this.txtLibInterP2.ReadOnly = false;
            this.txtLibInterP2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtLibInterP2.Size = new System.Drawing.Size(143, 27);
            this.txtLibInterP2.TabIndex = 505;
            this.txtLibInterP2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtLibInterP2.UseSystemPasswordChar = false;
            // 
            // txtInterP2
            // 
            this.txtInterP2.AccAcceptNumbersOnly = false;
            this.txtInterP2.AccAllowComma = false;
            this.txtInterP2.AccBackgroundColor = System.Drawing.Color.White;
            this.txtInterP2.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.txtInterP2.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.txtInterP2.AccHidenValue = "";
            this.txtInterP2.AccNotAllowedChars = null;
            this.txtInterP2.AccReadOnly = false;
            this.txtInterP2.AccReadOnlyAllowDelete = false;
            this.txtInterP2.AccRequired = false;
            this.txtInterP2.BackColor = System.Drawing.Color.White;
            this.txtInterP2.CustomBackColor = System.Drawing.Color.White;
            this.txtInterP2.Dock = System.Windows.Forms.DockStyle.Fill;
            this.txtInterP2.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.txtInterP2.ForeColor = System.Drawing.Color.Black;
            this.txtInterP2.Location = new System.Drawing.Point(2, 2);
            this.txtInterP2.Margin = new System.Windows.Forms.Padding(2);
            this.txtInterP2.MaxLength = 32767;
            this.txtInterP2.Multiline = false;
            this.txtInterP2.Name = "txtInterP2";
            this.txtInterP2.ReadOnly = false;
            this.txtInterP2.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.txtInterP2.Size = new System.Drawing.Size(59, 27);
            this.txtInterP2.TabIndex = 504;
            this.txtInterP2.TextAlignment = Infragistics.Win.HAlign.Left;
            this.txtInterP2.UseSystemPasswordChar = false;
            // 
            // tableLayoutPanel8
            // 
            this.tableLayoutPanel8.ColumnCount = 2;
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Controls.Add(this.Command7, 0, 0);
            this.tableLayoutPanel8.Controls.Add(this.btnTrierParDistance, 0, 0);
            this.tableLayoutPanel8.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel8.Location = new System.Drawing.Point(0, 120);
            this.tableLayoutPanel8.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel8.Name = "tableLayoutPanel8";
            this.tableLayoutPanel8.RowCount = 1;
            this.tableLayoutPanel8.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel8.Size = new System.Drawing.Size(254, 36);
            this.tableLayoutPanel8.TabIndex = 506;
            // 
            // Command7
            // 
            this.Command7.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command7.Dock = System.Windows.Forms.DockStyle.Fill;
            this.Command7.FlatAppearance.BorderSize = 0;
            this.Command7.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command7.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.Command7.ForeColor = System.Drawing.Color.White;
            this.Command7.Location = new System.Drawing.Point(129, 2);
            this.Command7.Margin = new System.Windows.Forms.Padding(2);
            this.Command7.Name = "Command7";
            this.Command7.Size = new System.Drawing.Size(123, 32);
            this.Command7.TabIndex = 518;
            this.Command7.Text = "Lancer";
            this.Command7.UseVisualStyleBackColor = false;
            this.Command7.Click += new System.EventHandler(this.Command7_Click);
            // 
            // btnTrierParDistance
            // 
            this.btnTrierParDistance.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.btnTrierParDistance.Dock = System.Windows.Forms.DockStyle.Fill;
            this.btnTrierParDistance.FlatAppearance.BorderSize = 0;
            this.btnTrierParDistance.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.btnTrierParDistance.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.btnTrierParDistance.ForeColor = System.Drawing.Color.White;
            this.btnTrierParDistance.Location = new System.Drawing.Point(2, 2);
            this.btnTrierParDistance.Margin = new System.Windows.Forms.Padding(2);
            this.btnTrierParDistance.Name = "btnTrierParDistance";
            this.btnTrierParDistance.Size = new System.Drawing.Size(123, 32);
            this.btnTrierParDistance.TabIndex = 517;
            this.btnTrierParDistance.Text = "Trier par distance";
            this.btnTrierParDistance.UseVisualStyleBackColor = false;
            this.btnTrierParDistance.Click += new System.EventHandler(this.btnTrierParDistance_Click);
            // 
            // cmdVisite
            // 
            this.cmdVisite.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdVisite.FlatAppearance.BorderSize = 0;
            this.cmdVisite.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdVisite.Font = new System.Drawing.Font("Ubuntu", 11.25F);
            this.cmdVisite.ForeColor = System.Drawing.Color.White;
            this.cmdVisite.Location = new System.Drawing.Point(2, 2);
            this.cmdVisite.Margin = new System.Windows.Forms.Padding(2);
            this.cmdVisite.Name = "cmdVisite";
            this.cmdVisite.Size = new System.Drawing.Size(258, 30);
            this.cmdVisite.TabIndex = 517;
            this.cmdVisite.Text = "Afficher Visite d\'entretien";
            this.cmdVisite.TextAlign = System.Drawing.ContentAlignment.TopCenter;
            this.cmdVisite.UseVisualStyleBackColor = false;
            this.cmdVisite.Click += new System.EventHandler(this.cmdVisite_Click);
            // 
            // flowLayoutPanel1
            // 
            this.flowLayoutPanel1.Controls.Add(this.Command3);
            this.flowLayoutPanel1.Controls.Add(this.cdmDayView);
            this.flowLayoutPanel1.Controls.Add(this.cmdClean);
            this.flowLayoutPanel1.Controls.Add(this.cmbIntervention);
            this.flowLayoutPanel1.Controls.Add(this.Command6);
            this.flowLayoutPanel1.Controls.Add(this.Command5);
            this.flowLayoutPanel1.Dock = System.Windows.Forms.DockStyle.Fill;
            this.flowLayoutPanel1.FlowDirection = System.Windows.Forms.FlowDirection.RightToLeft;
            this.flowLayoutPanel1.Location = new System.Drawing.Point(925, 0);
            this.flowLayoutPanel1.Margin = new System.Windows.Forms.Padding(0);
            this.flowLayoutPanel1.Name = "flowLayoutPanel1";
            this.flowLayoutPanel1.Size = new System.Drawing.Size(925, 40);
            this.flowLayoutPanel1.TabIndex = 1;
            // 
            // Command3
            // 
            this.Command3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.Command3.FlatAppearance.BorderSize = 0;
            this.Command3.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command3.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.Command3.ForeColor = System.Drawing.Color.White;
            this.Command3.Location = new System.Drawing.Point(817, 3);
            this.Command3.Name = "Command3";
            this.Command3.Size = new System.Drawing.Size(105, 35);
            this.Command3.TabIndex = 361;
            this.Command3.Text = "Paramètres";
            this.Command3.UseVisualStyleBackColor = false;
            this.Command3.Click += new System.EventHandler(this.Command3_Click);
            // 
            // cdmDayView
            // 
            this.cdmDayView.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cdmDayView.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cdmDayView.FlatAppearance.BorderSize = 0;
            this.cdmDayView.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cdmDayView.Font = new System.Drawing.Font("Ubuntu", 10F);
            this.cdmDayView.ForeColor = System.Drawing.Color.White;
            this.cdmDayView.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cdmDayView.Location = new System.Drawing.Point(696, 2);
            this.cdmDayView.Margin = new System.Windows.Forms.Padding(2);
            this.cdmDayView.Name = "cdmDayView";
            this.cdmDayView.Size = new System.Drawing.Size(116, 35);
            this.cdmDayView.TabIndex = 394;
            this.cdmDayView.Text = "Aujourd\'hui";
            this.cdmDayView.UseVisualStyleBackColor = false;
            this.cdmDayView.Click += new System.EventHandler(this.cdmDayView_Click);
            // 
            // cmdClean
            // 
            this.cmdClean.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(55)))), ((int)(((byte)(84)))), ((int)(((byte)(96)))));
            this.cmdClean.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmdClean.FlatAppearance.BorderSize = 0;
            this.cmdClean.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmdClean.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.749999F);
            this.cmdClean.ForeColor = System.Drawing.Color.White;
            this.cmdClean.Image = global::Axe_interDT.Properties.Resources.Eraser_16x16;
            this.cmdClean.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.cmdClean.Location = new System.Drawing.Point(607, 2);
            this.cmdClean.Margin = new System.Windows.Forms.Padding(2);
            this.cmdClean.Name = "cmdClean";
            this.cmdClean.Size = new System.Drawing.Size(85, 35);
            this.cmdClean.TabIndex = 403;
            this.cmdClean.Text = "   Clean";
            this.cmdClean.UseVisualStyleBackColor = false;
            this.cmdClean.Click += new System.EventHandler(this.cmdClean_Click);
            // 
            // cmbIntervention
            // 
            this.cmbIntervention.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.cmbIntervention.Cursor = System.Windows.Forms.Cursors.Hand;
            this.cmbIntervention.FlatAppearance.BorderSize = 0;
            this.cmbIntervention.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.cmbIntervention.Font = new System.Drawing.Font("Ubuntu", 9F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.cmbIntervention.Image = global::Axe_interDT.Properties.Resources.Search_24x24;
            this.cmbIntervention.Location = new System.Drawing.Point(543, 2);
            this.cmbIntervention.Margin = new System.Windows.Forms.Padding(2);
            this.cmbIntervention.Name = "cmbIntervention";
            this.cmbIntervention.Size = new System.Drawing.Size(60, 35);
            this.cmbIntervention.TabIndex = 359;
            this.cmbIntervention.Tag = "";
            this.cmbIntervention.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
            this.toolTip1.SetToolTip(this.cmbIntervention, "Rechercher");
            this.cmbIntervention.UseVisualStyleBackColor = false;
            this.cmbIntervention.Click += new System.EventHandler(this.cmbIntervention_Click);
            // 
            // Command6
            // 
            this.Command6.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command6.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command6.FlatAppearance.BorderSize = 0;
            this.Command6.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command6.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.Command6.ForeColor = System.Drawing.Color.White;
            this.Command6.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command6.Location = new System.Drawing.Point(445, 2);
            this.Command6.Margin = new System.Windows.Forms.Padding(2);
            this.Command6.Name = "Command6";
            this.Command6.Size = new System.Drawing.Size(94, 35);
            this.Command6.TabIndex = 397;
            this.Command6.Text = "Command6";
            this.Command6.UseVisualStyleBackColor = false;
            this.Command6.Visible = false;
            // 
            // Command5
            // 
            this.Command5.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(85)))), ((int)(((byte)(115)))), ((int)(((byte)(128)))));
            this.Command5.Cursor = System.Windows.Forms.Cursors.Hand;
            this.Command5.FlatAppearance.BorderSize = 0;
            this.Command5.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
            this.Command5.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.Command5.ForeColor = System.Drawing.Color.White;
            this.Command5.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
            this.Command5.Location = new System.Drawing.Point(346, 2);
            this.Command5.Margin = new System.Windows.Forms.Padding(2);
            this.Command5.Name = "Command5";
            this.Command5.Size = new System.Drawing.Size(95, 35);
            this.Command5.TabIndex = 396;
            this.Command5.Text = "Command5";
            this.Command5.UseVisualStyleBackColor = false;
            this.Command5.Visible = false;
            this.Command5.Click += new System.EventHandler(this.Command5_Click);
            // 
            // Timer1
            // 
            this.Timer1.Interval = 7000;
            this.Timer1.Tick += new System.EventHandler(this.Timer1_Tick);
            // 
            // groupBox6
            // 
            this.groupBox6.BackColor = System.Drawing.Color.Transparent;
            this.groupBox6.Controls.Add(this.DTDateRealiseAu);
            this.groupBox6.Controls.Add(this.DTDateRealiseDe);
            this.groupBox6.Font = new System.Drawing.Font("Ubuntu", 9.749999F);
            this.groupBox6.Location = new System.Drawing.Point(65, 662);
            this.groupBox6.Name = "groupBox6";
            this.groupBox6.Size = new System.Drawing.Size(208, 180);
            this.groupBox6.TabIndex = 503;
            this.groupBox6.TabStop = false;
            this.groupBox6.Text = "abc";
            this.groupBox6.Visible = false;
            // 
            // DTDateRealiseAu
            // 
            this.DTDateRealiseAu.Location = new System.Drawing.Point(4, 79);
            this.DTDateRealiseAu.Name = "DTDateRealiseAu";
            this.DTDateRealiseAu.Size = new System.Drawing.Size(200, 22);
            this.DTDateRealiseAu.TabIndex = 1;
            this.DTDateRealiseAu.Visible = false;
            // 
            // DTDateRealiseDe
            // 
            this.DTDateRealiseDe.Location = new System.Drawing.Point(8, 35);
            this.DTDateRealiseDe.Name = "DTDateRealiseDe";
            this.DTDateRealiseDe.Size = new System.Drawing.Size(200, 22);
            this.DTDateRealiseDe.TabIndex = 0;
            this.DTDateRealiseDe.Visible = false;
            // 
            // tableLayoutPanel5
            // 
            this.tableLayoutPanel5.ColumnCount = 1;
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Absolute, 20F));
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel1, 0, 1);
            this.tableLayoutPanel5.Controls.Add(this.tableLayoutPanel6, 0, 0);
            this.tableLayoutPanel5.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel5.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel5.Name = "tableLayoutPanel5";
            this.tableLayoutPanel5.RowCount = 2;
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Absolute, 40F));
            this.tableLayoutPanel5.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 100F));
            this.tableLayoutPanel5.Size = new System.Drawing.Size(1850, 957);
            this.tableLayoutPanel5.TabIndex = 504;
            // 
            // tableLayoutPanel6
            // 
            this.tableLayoutPanel6.ColumnCount = 2;
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.ColumnStyles.Add(new System.Windows.Forms.ColumnStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Controls.Add(this.flowLayoutPanel1, 1, 0);
            this.tableLayoutPanel6.Controls.Add(this.cmdVisite, 0, 0);
            this.tableLayoutPanel6.Dock = System.Windows.Forms.DockStyle.Fill;
            this.tableLayoutPanel6.Location = new System.Drawing.Point(0, 0);
            this.tableLayoutPanel6.Margin = new System.Windows.Forms.Padding(0);
            this.tableLayoutPanel6.Name = "tableLayoutPanel6";
            this.tableLayoutPanel6.RowCount = 1;
            this.tableLayoutPanel6.RowStyles.Add(new System.Windows.Forms.RowStyle(System.Windows.Forms.SizeType.Percent, 50F));
            this.tableLayoutPanel6.Size = new System.Drawing.Size(1850, 40);
            this.tableLayoutPanel6.TabIndex = 1;
            // 
            // lblTimer
            // 
            this.lblTimer.AccAcceptNumbersOnly = false;
            this.lblTimer.AccAllowComma = false;
            this.lblTimer.AccBackgroundColor = System.Drawing.Color.White;
            this.lblTimer.AccBackGroundColorText = System.Drawing.Color.Empty;
            this.lblTimer.AccBorderColor = System.Drawing.Color.FromArgb(((int)(((byte)(32)))), ((int)(((byte)(58)))), ((int)(((byte)(69)))));
            this.lblTimer.AccHidenValue = "";
            this.lblTimer.AccNotAllowedChars = null;
            this.lblTimer.AccReadOnly = false;
            this.lblTimer.AccReadOnlyAllowDelete = false;
            this.lblTimer.AccRequired = false;
            this.lblTimer.BackColor = System.Drawing.Color.White;
            this.lblTimer.CustomBackColor = System.Drawing.Color.White;
            this.lblTimer.Font = new System.Drawing.Font("Tahoma", 10F);
            this.lblTimer.ForeColor = System.Drawing.Color.Black;
            this.lblTimer.Location = new System.Drawing.Point(9, 16);
            this.lblTimer.Margin = new System.Windows.Forms.Padding(2);
            this.lblTimer.MaxLength = 32767;
            this.lblTimer.Multiline = false;
            this.lblTimer.Name = "lblTimer";
            this.lblTimer.ReadOnly = false;
            this.lblTimer.ScrollBar = System.Windows.Forms.ScrollBars.None;
            this.lblTimer.Size = new System.Drawing.Size(144, 27);
            this.lblTimer.TabIndex = 502;
            this.lblTimer.TextAlignment = Infragistics.Win.HAlign.Left;
            this.lblTimer.UseSystemPasswordChar = false;
            this.lblTimer.Visible = false;
            // 
            // UserDocPlanningCodeJock
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.BackColor = System.Drawing.Color.White;
            this.Controls.Add(this.tableLayoutPanel5);
            this.Controls.Add(this.groupBox6);
            this.Controls.Add(this.lblTimer);
            this.Name = "UserDocPlanningCodeJock";
            this.Size = new System.Drawing.Size(1850, 957);
            this.Tag = "Planning";
            this.Load += new System.EventHandler(this.UserDocPlanningCodeJock_Load);
            this.VisibleChanged += new System.EventHandler(this.UserDocPlanningCodeJock_VisibleChanged);
            this.tableLayoutPanel1.ResumeLayout(false);
            this.FraConteneur.ResumeLayout(false);
            ((System.ComponentModel.ISupportInitialize)(this.Cal)).EndInit();
            this.tableLayoutPanel2.ResumeLayout(false);
            this.tableLayoutPanel2.PerformLayout();
            this.Frame4.ResumeLayout(false);
            this.tableLayoutPanel4.ResumeLayout(false);
            this.tableLayoutPanel4.PerformLayout();
            this.tableLayoutPanel3.ResumeLayout(false);
            this.tableLayoutPanel3.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.CmbGroupe)).EndInit();
            ((System.ComponentModel.ISupportInitialize)(this.DateCal)).EndInit();
            this.fraVisite.ResumeLayout(false);
            this.fraVisite.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.GridVisite)).EndInit();
            this.tableLayoutPanel7.ResumeLayout(false);
            this.tableLayoutPanel8.ResumeLayout(false);
            this.flowLayoutPanel1.ResumeLayout(false);
            this.groupBox6.ResumeLayout(false);
            this.tableLayoutPanel5.ResumeLayout(false);
            this.tableLayoutPanel6.ResumeLayout(false);
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel3;
        public System.Windows.Forms.Label lblMes;
        public System.Windows.Forms.Label label33;
        public System.Windows.Forms.Label label1;
        public System.Windows.Forms.Label label2;
        public System.Windows.Forms.GroupBox Frame4;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel4;
        public System.Windows.Forms.Label label5;
        public System.Windows.Forms.Label label9;
        public iTalk.iTalk_TextBox_Small2 lbllibIntervenant;
        public iTalk.iTalk_TextBox_Small2 txtIntervenant;
        public iTalk.iTalk_TextBox_Small2 txtCodeEtatsInclus;
        public System.Windows.Forms.Button cmdStatutInclus;
        public System.Windows.Forms.Button cmdintervenant;
        public System.Windows.Forms.Button cmdRechercheGroupe;
        public System.Windows.Forms.Button cmdEctituresCopro;
        public System.Windows.Forms.Button cmdColMoins;
        public System.Windows.Forms.Label label4;
        public System.Windows.Forms.Label label3;
        public System.Windows.Forms.Button CmdLargMoins;
        public System.Windows.Forms.Button cmdLargPlus;
        public System.Windows.Forms.Button CmdLongMoins;
        public System.Windows.Forms.Button cmdLongPlus;
        public System.Windows.Forms.Button cmdColplus;
        private System.Windows.Forms.FlowLayoutPanel flowLayoutPanel1;
        public System.Windows.Forms.Button cmbIntervention;
        public System.Windows.Forms.Button Command3;
        public System.Windows.Forms.Button cdmDayView;
        public System.Windows.Forms.Button Command6;
        public System.Windows.Forms.Button Command5;
        public System.Windows.Forms.Button cmdZoomDef;
        public iTalk.iTalk_TextBox_Small2 lblTimer;
        public System.Windows.Forms.Label Label567;
        public System.Windows.Forms.GroupBox FraConteneur;
        public iTalk.iTalk_TextBox_Small2 lbltot;
        public System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DateTimePicker DTDateRealiseAu;
        private System.Windows.Forms.DateTimePicker DTDateRealiseDe;
        public AxXtremeCalendarControl.AxCalendarControl Cal;
        public AxXtremeCalendarControl.AxDatePicker DateCal;
        public Infragistics.Win.UltraWinGrid.UltraCombo CmbGroupe;
        private System.Windows.Forms.ToolTip toolTip1;
        public System.Windows.Forms.Button cmdClean;
        public System.Windows.Forms.Timer Timer1;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel5;
        private System.Windows.Forms.TableLayoutPanel fraVisite;
        private System.Windows.Forms.Label label7;
        public iTalk.iTalk_TextBox_Small2 txtMoisPlanif;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel7;
        public System.Windows.Forms.Button Command8;
        public iTalk.iTalk_TextBox_Small2 txtLibInterP2;
        public iTalk.iTalk_TextBox_Small2 txtInterP2;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel8;
        public System.Windows.Forms.Button Command7;
        public System.Windows.Forms.Button btnTrierParDistance;
        public Infragistics.Win.UltraWinGrid.UltraGrid GridVisite;
        public System.Windows.Forms.Button cmdVisite;
        private System.Windows.Forms.TableLayoutPanel tableLayoutPanel6;
    }
}
